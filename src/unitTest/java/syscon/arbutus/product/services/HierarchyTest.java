package syscon.arbutus.product.services;

import org.testng.Assert;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.facilitycountactivity.contract.ejb.Constants;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.HierarchyType;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.UsageCategory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HierarchyTest {

    private static final String CURRENT_LEVELA = "HIERARCHY_LEVEL_A";
    private static final String CURRENT_LEVELB = "HIERARCHY_LEVEL_B";
    private static final String CURRENT_LEVELC = "HIERARCHY_LEVEL_C";
    private static final String CURRENT_LEVELD = "HIERARCHY_LEVEL_D";

    @Test()
    public void sort() {
        Map<String, HierarchyType> list = new HashMap<>();
        list.put(CURRENT_LEVELA, new HierarchyType(CURRENT_LEVELA, CURRENT_LEVELB, UsageCategory.STO, new Long(183)));
        list.put(Constants.BASEPARENT, new HierarchyType(Constants.BASEPARENT, CURRENT_LEVELA, UsageCategory.STO, new Long(183)));
        list.put(CURRENT_LEVELB, new HierarchyType(CURRENT_LEVELB, CURRENT_LEVELC, UsageCategory.STO, new Long(183)));
        list.put(CURRENT_LEVELC, new HierarchyType(CURRENT_LEVELC, CURRENT_LEVELD, UsageCategory.STO, new Long(183)));

        List<HierarchyType> sorted = HierarchyType.sortHierarchy(list);
        Assert.assertEquals(sorted.size(), 4);
        Assert.assertEquals(sorted.get(1).getCurrentLevel(), CURRENT_LEVELB);
        Assert.assertEquals(sorted.get(2).getCurrentLevel(), CURRENT_LEVELC);
        Assert.assertEquals(sorted.get(3).getCurrentLevel(), CURRENT_LEVELD);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void sort1() {
        Map<String, HierarchyType> list = new HashMap<>();
        list.put(Constants.BASEPARENT, new HierarchyType(Constants.BASEPARENT, CURRENT_LEVELA, UsageCategory.STO, new Long(183)));
        list.put(CURRENT_LEVELA+1, new HierarchyType(CURRENT_LEVELA, CURRENT_LEVELB, UsageCategory.STO, new Long(183)));
        list.put(CURRENT_LEVELB+2, new HierarchyType(CURRENT_LEVELB, CURRENT_LEVELC, UsageCategory.STO, new Long(183)));
        list.put(CURRENT_LEVELC, new HierarchyType(CURRENT_LEVELC, CURRENT_LEVELD, UsageCategory.STO, new Long(183)));

        HierarchyType.sortHierarchy(list);
    }

}