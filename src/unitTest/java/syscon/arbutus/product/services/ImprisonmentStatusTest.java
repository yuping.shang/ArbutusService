package syscon.arbutus.product.services;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertTrue;

import java.lang.reflect.Method;
import java.util.Date;

import org.hibernate.Query;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseUnitTest;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.ErrorCodes;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.supervision.contract.dto.ImprisonmentStatusType;
import syscon.arbutus.product.services.supervision.contract.ejb.SupervisionServiceBean;
import syscon.arbutus.product.services.supervision.realization.persistence.SupervisionEntity;
import syscon.arbutus.product.services.utility.ical.util.TimeUtils;

/**
 * Created by bseshadri on 22/07/2015.
 */
public class ImprisonmentStatusTest extends BaseUnitTest {
	
	private SupervisionServiceBean supervisionServiceBean = new SupervisionServiceBean();
	
	@Mock
    private UserContext userContext;

    @Mock
    private Query query;
    
    @BeforeMethod
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.supervisionServiceBean.setSession(sessionMock);
    }
    
    @DataProvider(name = "addImprisonmentStatus")
    public Object[][] addImprisonmentStatus(Method method) {
        Object[][] result = null;

    	Date currentDate = TimeUtils.getCurrentDate();
    	
    	Date imprisonmentStartCase1 = TimeUtils.getDateWithoutTime(currentDate);
    	Date imprisonmentEndCase1 = TimeUtils.getDateAfter(imprisonmentStartCase1, 3);
    	Date supervisionStartCase1 = TimeUtils.getDateAfter(imprisonmentStartCase1, 2);
    	Date supervisionEndCase1 = TimeUtils.getDateAfter(imprisonmentStartCase1, 4);
    	
        ImprisonmentStatusType impStatusTypeCase1 = new ImprisonmentStatusType();
        impStatusTypeCase1.setImprisonmentStatus("DOC");
        impStatusTypeCase1.setImprisonmentStatusStartDate(imprisonmentStartCase1);
        impStatusTypeCase1.setImprisonmentStatusEndDate(imprisonmentEndCase1);
        
        SupervisionEntity supEntityCase1 = new SupervisionEntity();
        supEntityCase1.setSupervisionStartDate(supervisionStartCase1);
        supEntityCase1.setSupervisionEndDate(supervisionEndCase1);
       
        Date supervisionStartCase2 = TimeUtils.getDateWithoutTime(currentDate);
        Date supervisionEndCase2 = TimeUtils.getDateAfter(supervisionStartCase2, 3);
    	Date imprisonmentStartCase2 = TimeUtils.getDateAfter(supervisionStartCase2, 2);
    	Date imprisonmentEndCase2 = TimeUtils.getDateAfter(supervisionStartCase2, 4);    	
    	
        ImprisonmentStatusType impStatusTypeCase2 = new ImprisonmentStatusType();
        impStatusTypeCase2.setImprisonmentStatus("DOC");
        impStatusTypeCase2.setImprisonmentStatusStartDate(imprisonmentStartCase2);
        impStatusTypeCase2.setImprisonmentStatusEndDate(imprisonmentEndCase2);
        
        SupervisionEntity supEntityCase2 = new SupervisionEntity();
        supEntityCase2.setSupervisionStartDate(supervisionStartCase2);
        supEntityCase2.setSupervisionEndDate(supervisionEndCase2);      

        if (method.getName().contains("Fail")) {
            result = new Object[][] {
                    { impStatusTypeCase1, supEntityCase1, ErrorCodes.SUP_IMPRISONMENT_STARTDATE_PRIOR_SUPERVISION_STARTDATE },
                    { impStatusTypeCase2, supEntityCase2, ErrorCodes.SUP_IMPRISONMENT_ENDDATE_AFTER_SUPERVISION_ENDDATE}};
        }
        return result;
    }
    
    @Test(expectedExceptions = InvalidInputException.class)
    public void testAddInmateImprisonmentNullSupervision() {
    	supervisionServiceBean.addImprisonmentStatus(userContext, null, new ImprisonmentStatusType());
    }
    
    //@Test(dataProvider = "addImprisonmentStatus")
    public void testAddImprisonmentDataFail(ImprisonmentStatusType impStatusType, SupervisionEntity supEntity, 
    		ErrorCodes code){           
        when(sessionMock.get(eq(SupervisionEntity.class), anyInt())).thenReturn(supEntity);
		try {
			supervisionServiceBean.addImprisonmentStatus(userContext, 1L, impStatusType);
		} catch (ArbutusRuntimeException e) {
			assertTrue(e.getCode() == code, "error codes must match up");
		}         
    }
}
