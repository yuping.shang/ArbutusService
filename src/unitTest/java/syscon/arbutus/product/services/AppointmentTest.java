package syscon.arbutus.product.services;

import java.util.Date;

import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.appointment.contract.dto.Appointment;
import syscon.arbutus.product.services.appointment.contract.dto.AppointmentSearchType;
import syscon.arbutus.product.services.appointment.contract.ejb.AppointmentServiceBean;
import syscon.arbutus.product.services.appointment.realization.persistence.AppointmentEntity;
import syscon.arbutus.product.services.common.BaseUnitTest;
import syscon.arbutus.product.services.core.exception.ArbutusOptimisticLockException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.core.exception.ValidationException;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.FacilityInternalLocation;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationServiceLocal;
import syscon.arbutus.product.services.utility.ical.util.TimeUtils;
import syscon.arbutus.product.testutil.Generator;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

/**
 * Created and modified by chuan.pan, bseshadri on 27/04/2015, 03/07/2015.
 */
public class AppointmentTest extends BaseUnitTest {
    private AppointmentServiceBean appointmentServiceBean = new AppointmentServiceBean();

    private Generator<Appointment> apptTypeGenerator;

    @Mock
    private FacilityInternalLocationServiceLocal facilityInternalLocationService;

    @Mock
    private UserContext userContext;

    @Mock
    private Query query;

    @Rule
    private ExpectedException expectException = ExpectedException.none();

    @BeforeClass
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.injector.assign(FacilityInternalLocationServiceLocal.class, facilityInternalLocationService);
        this.injector.assign(Session.class, sessionMock);
        this.injector.inject(appointmentServiceBean);
        apptTypeGenerator = new Generator<>();        
    }

    @BeforeMethod
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        apptTypeGenerator = new Generator<>();
    }

    /**
     * Test validation for appointment service.
     * 1. Should throw InvalidInputException for null appointment
     * 2. Should throw InvalidInputException null value for required attribute
     * 3. Should throw InvalidInputException if the FacilityLocationType is not active
     *
     * @throws Exception
     */
    @Test
    public void testValidateAppointment() throws Exception {
        // 1. Should throw InvalidInputException for null appointment
        try {
            appointmentServiceBean.createAppointment(userContext, null);
            fail("Null appointment should not ");
        } catch (Exception e) {
            assertEquals(InvalidInputException.class, e.getClass());
            assertTrue(e.getMessage().contains("argument must not be null"));
        }

        // 2.Should throw InvalidInputException null value for required attribute [locationId]
        Appointment apppointmentType = new Appointment();
        apppointmentType.setSupervisionId(1L);
        apppointmentType.setFacilityId(1L);
        apppointmentType.setLocationId(null);
        apppointmentType.setStartTime(new Date((new Date()).getTime() + 10000));
        apppointmentType.setEndTime(new Date(apppointmentType.getStartTime().getTime() + 10000));
        apppointmentType.setComments("This is a testing appointment");
        apppointmentType.setType("Others");

        try {
            appointmentServiceBean.createAppointment(userContext, apppointmentType);
        } catch (Exception e) {
            assertEquals(InvalidInputException.class, e.getClass());
            assertTrue(e.getMessage().contains("locationId may not be null"));
        }

        // 3. Should throw InvalidInputException if the FacilityLocationType is not active
        // Set location to 1
        apppointmentType.setLocationId(1L);

        // Mock the location type to inActive
        FacilityInternalLocation facilityInternalLocationTypeMock = mock(FacilityInternalLocation.class);
        when(facilityInternalLocationTypeMock.isActive()).thenReturn(false);
        when(facilityInternalLocationService.get(any(UserContext.class), anyLong())).thenReturn(facilityInternalLocationTypeMock);

        // Inject the service to the bean
        injector.assign(FacilityInternalLocationServiceLocal.class, facilityInternalLocationService);
        injector.inject(appointmentServiceBean);
        try {
            appointmentServiceBean.createAppointment(userContext, apppointmentType);
        } catch (Exception e) {
            assertEquals(ValidationException.class, e.getClass());
            assertTrue(e.getMessage().contains("is invalid or not active"));
        }
    }

    @Test
    public void testCreateSearchTypeForGetAppointmentsForInmate() {
        Date currentDate = TimeUtils.getCurrentDate();
        Date startDate = TimeUtils.getDateWithoutTime(currentDate);
        Date endDateForSearch = TimeUtils.getDateAfter(currentDate, 2);
        int numAppointments = 3;

        when(sessionMock.createQuery(anyString())).thenReturn(query);
        when(query.list()).thenReturn(apptTypeGenerator.generateItems(numAppointments, Appointment.class));

        AppointmentSearchType searchType = appointmentServiceBean.createSearchTypeForGetApptsForInmate(2L, startDate, endDateForSearch, null);

        Assert.assertEquals(searchType.getSupervisionId().longValue(), 2L);
        Date dateWithoutTime = TimeUtils.getDateWithoutTime(startDate);
        Assert.assertEquals(searchType.getStartTime(), dateWithoutTime);
        Assert.assertEquals(searchType.getType(), null);
    }

    @Test
    public void testQueryParametersForGetAppointmentsForInmate() {
        Date currentDate = TimeUtils.getCurrentDate();
        Date startDate = TimeUtils.getDateWithoutTime(currentDate);
        Date endDateForSearch = TimeUtils.getDateAfter(currentDate, 2);
        Date dateWithoutTime = TimeUtils.getDateWithoutTime(startDate);
        int numAppointments = 3;

        when(sessionMock.createQuery(anyString())).thenReturn(query);
        when(query.list()).thenReturn(apptTypeGenerator.generateItems(numAppointments, Appointment.class));

        appointmentServiceBean.getAppointmentsForInmate(userContext, 2L, startDate, endDateForSearch, "Church");
        verify(query).setParameter("supervisionId", 2L);
        verify(query).setParameter("appointmentType", "Church");
        verify(query).setParameter("startTime", dateWithoutTime);
    }

    @Test(expectedExceptions = InvalidInputException.class)
    public void testCancelEmptyAppointment() {
        appointmentServiceBean.cancelAppointment(userContext, null, null);
    }

    @Test(expectedExceptions = ArbutusOptimisticLockException.class)
    public void testCancelNotExistsAppointment() {
        when(sessionMock.get(eq(AppointmentEntity.class), anyInt())).thenReturn(null);
        appointmentServiceBean.cancelAppointment(userContext, 2L, 0L);

        verify(sessionMock).get(eq(AppointmentEntity.class), anyInt());
    }
}
