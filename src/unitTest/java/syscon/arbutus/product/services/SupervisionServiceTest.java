package syscon.arbutus.product.services;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.SimpleExpression;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseUnitTest;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.supervision.contract.dto.ImprisonmentStatusType;
import syscon.arbutus.product.services.supervision.contract.ejb.SupervisionServiceBean;
import syscon.arbutus.product.services.supervision.realization.persistence.SupervisionEntity;
import syscon.arbutus.product.testutil.Generator;

/**
 * Created and modified by bseshadri on 31/07/2015.
 */
public class SupervisionServiceTest extends BaseUnitTest {
    private SupervisionServiceBean supervisionServiceBean = new SupervisionServiceBean();
    private Generator<SupervisionEntity> supervisionGenerator;

    @Mock
    private UserContext userContext;

    @Mock
    private Query query;
    
    @Mock
    private Criteria criteria;

    @Rule
    private ExpectedException expectException = ExpectedException.none();
    
    @BeforeClass
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.injector.assign(Session.class, sessionMock);
        this.injector.inject(supervisionServiceBean);
        supervisionGenerator = new Generator<>();
    }
    
    @BeforeMethod
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        supervisionGenerator = new Generator<>();
    }

    @Test
    public void testGetSupervision() {
    	Long numSupervisions = 3L;
        when(sessionMock.createCriteria(eq(SupervisionEntity.class))).thenReturn(criteria); 
        when(criteria.setProjection(any())).thenReturn(criteria);
        when(criteria.setProjection(Projections.countDistinct(anyString())).uniqueResult()).thenReturn(
        		numSupervisions);
        when(query.list()).thenReturn(supervisionGenerator.generateItems(2, SupervisionEntity.class));
        
        Long personIdentityId = Long.valueOf(0);
        supervisionServiceBean.getSupervision(userContext, personIdentityId);
        ArgumentCaptor<SimpleExpression> peopleCaptor = ArgumentCaptor.forClass(SimpleExpression.class);
        verify(criteria).add((Criterion)peopleCaptor.capture());
        SimpleExpression criterion = peopleCaptor.getAllValues().get(0);
        
        Assert.assertTrue(criterion.toString().contains(personIdentityId.toString()));        	
    } 
    
    /*
     * Commenting out this test case for now since it is causing a build failure inside 
     * the 'supervisionServiceBean.getActiveSupervision(userContext, personIdentityId);' 
     * method call as this method is calling a PersonServiceAdapter.getPersonIdentityByPersonIdentityId
     * method which is a static method call and we need to use PowerMockito to mock this
     * static method call and PowerMockito is not available for download now. Will research into
     * this test at a later stage to install PowerMockito.
     */
    
    /*@Test
    public void testGetActiveSupervision() {
    	Long numSupervisions = 3L;
        when(sessionMock.createCriteria(eq(SupervisionEntity.class))).thenReturn(criteria); 
        when(criteria.setProjection(any())).thenReturn(criteria);
        when(criteria.setProjection(Projections.countDistinct(anyString())).uniqueResult()).thenReturn(
        		numSupervisions);
        when(query.list()).thenReturn(supervisionGenerator.generateItems(2, SupervisionEntity.class));
        
        Long personIdentityId = Long.valueOf(0);
        supervisionServiceBean.getActiveSupervision(userContext, personIdentityId);
        ArgumentCaptor<SimpleExpression> peopleCaptor = ArgumentCaptor.forClass(SimpleExpression.class);
        verify(criteria, times(2)).add((Criterion)peopleCaptor.capture());
        List<SimpleExpression> criterions = peopleCaptor.getAllValues();
        
        Criterion firstCriterion = criterions.get(0);
        Criterion secondCriterion = criterions.get(1);
           
        Assert.assertTrue(firstCriterion.toString().contains(personIdentityId.toString()));  
        Assert.assertTrue(secondCriterion.toString().contains("supervisionEndDate is null"));
    }*/

/*
    @Test(expectedExceptions = InvalidInputException.class)
    public void testNullAddImprisonmentStatus() {
    	supervisionServiceBean.addImprisonmentStatus(userContext, null, 
    			new ImprisonmentStatusType());
    }
*/
}
