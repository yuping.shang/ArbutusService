package syscon.arbutus.product.services;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.facility.realization.persistence.FacilityEntity;
import syscon.arbutus.product.services.organization.contract.dto.Organization;
import syscon.arbutus.product.services.organization.contract.ejb.OrganizationHelper;
import syscon.arbutus.product.services.organization.realization.persistence.OrganizationEntity;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertTrue;

/**
 * Created by chuan.pan on 28/04/2015.
 */
public class OrganizationTest {
    /**
     * Test OrganizationHelper conversion method
     * Steps:
     * Create a OrganizationEntity{@link OrganizationEntity} with a TerminationDate that is earlier than today
     * Verification: getOrganizationStatus should return TRUE
     *
     * @throws Exception
     */
    @Test
    public void testOrganizationConversion() throws Exception {

    }
}
