package syscon.arbutus.product.services.facilityinternallocation.contract.ejb;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;

import syscon.arbutus.product.services.core.exception.ErrorCodes;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;

import syscon.arbutus.product.services.facilityinternallocation.contract.dto.FacilityInternalLocation;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.UsageCategory;
import syscon.arbutus.product.services.housing.contract.interfaces.HousingServiceLocal;
import syscon.arbutus.product.services.housing.realization.persistence.HousingBedMgmtActivityEntity;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementServiceLocal;
import syscon.arbutus.product.services.property.contract.interfaces.PropertyServiceLocal;

import java.util.Date;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

public class FacilityInternalLocationServiceBeanTest {

    private FacilityInternalLocationServiceBean service = new FacilityInternalLocationServiceBean();

    @Mock UserContext userContext;
    @Mock Session session;
    @Mock Query query;


    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);
        service.session = session;
        service.facilityInternalLocationDAO = new FacilityInternalLocationDAO(null, session);
    }

    @Test(dataProvider = "createFacilityInternalLocationFailDataProvider")
    public void createFail(UserContext userContext, FacilityInternalLocation internalLocationType, UsageCategory usageCategory, ErrorCodes errorCode) {
        try {
            FacilityInternalLocation facilityInternalLocation = service.create(userContext, internalLocationType, usageCategory);
            fail("Not supposed to reach here");
        }catch(InvalidInputException e){
            assertEquals(errorCode, e.getCode());
        }
    }

    @Test(dataProvider = "createFacilityInternalLocationDataProvider")
    public void create(UserContext userContext, FacilityInternalLocation internalLocationType, UsageCategory usageCategory, ErrorCodes errorCode) {
        when(session.createQuery(anyString())).thenReturn(query);
        when(query.uniqueResult()).thenReturn(0l);
        try {
            FacilityInternalLocation facilityInternalLocation = service.create(userContext, internalLocationType, usageCategory);
            //fail("Not supposed to reach here");
        }catch(IllegalStateException e){
            e.printStackTrace();
        }
    }

    //===============  data providers
    @DataProvider(name = "createFacilityInternalLocationFailDataProvider")
    public Object[][] createFacilityInternalLocationFailDataProvider() {
        Object[][] result;

        UserContext userContext = new UserContext();
        FacilityInternalLocation internalLocationType = new FacilityInternalLocation();
        UsageCategory usageCategory = UsageCategory.ACT;

        result = new Object[][] {
                { userContext, internalLocationType, usageCategory, ErrorCodes.GENERIC_VALIDATION_ERROR },
        };

        return result;
    }

    @DataProvider(name = "createFacilityInternalLocationDataProvider")
    public Object[][] createFacilityInternalLocationDataProvider() {
        Object[][] result;

        UserContext userContext = new UserContext();
        FacilityInternalLocation internalLocationType = new FacilityInternalLocation();
        internalLocationType.setFacilityId(1l);
        internalLocationType.setLocationCode("bla");
        UsageCategory usageCategory = UsageCategory.ACT;

        result = new Object[][] {
                { userContext, internalLocationType, usageCategory, ErrorCodes.GENERIC_VALIDATION_ERROR },
        };

        return result;
    }
}
