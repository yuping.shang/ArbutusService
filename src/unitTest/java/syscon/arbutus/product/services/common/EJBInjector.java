package syscon.arbutus.product.services.common;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceContext;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class provides a way to inject EJB for mock objects
 */
public class EJBInjector {

    private final List<Class<? extends Annotation>> annotations;

    public EJBInjector() {
        annotations = new ArrayList<>();
        annotations.add(EJB.class);
        annotations.add(PersistenceContext.class);
        annotations.add(Resource.class);
        annotations.add(Inject.class);
    }

    final Map<Class<?>, Object> mappings = new HashMap<Class<?>, Object>();

    private boolean hasAnnotation(final Field field) {
        for (final Class<? extends Annotation> annotation : annotations) {
            if (field.isAnnotationPresent(annotation)) {
                return true;
            }
        }
        return false;
    }

    public <T> void inject(final Object bean, Class<? extends T> toInjectClass, T toInject) {
        for (final Field field : bean.getClass().getDeclaredFields()) {
            if (field.getType().equals(toInjectClass)) {
                try {
                    setFieldValue(field, bean, toInject);
                } catch (Throwable ex) {
                    throw new RuntimeException(ex.getMessage(), ex);
                }
                break;
            }
        }
    }


    public <T> void inject(final Object bean, Class<? extends T> toInjectClass, T toInject, String name) {
        for (final Field field : bean.getClass().getDeclaredFields()) {
            if (field.getType().equals(toInjectClass)) {
                final String annotationName = getAnnotationName(field);
                if (!annotationName.isEmpty() && annotationName.equals(name)) {
                    try {
                        setFieldValue(field, bean, toInject);
                    } catch (Throwable ex) {
                        throw new RuntimeException(ex.getMessage(), ex);
                    }
                    break;
                }
            }
        }
    }


    private String getAnnotationName(final Field field) {
        String name = "";
        if (field.isAnnotationPresent(EJB.class)) {
            name = field.getAnnotation(EJB.class).mappedName();
        } else if (field.isAnnotationPresent(Resource.class)) {
            name = field.getAnnotation(Resource.class).mappedName();
        } else if (field.isAnnotationPresent(PersistenceContext.class)) {
            name = field.getAnnotation(PersistenceContext.class).unitName();
        } else if (field.isAnnotationPresent(Inject.class) && field.isAnnotationPresent(Named.class)) {
            name = field.getAnnotation(Named.class).value();
        }
        return name;
    }

    public void inject(final Object bean) throws Exception {
        for (final Field field : getAnnotatedFields(bean)) {
            injectField(field, bean);
        }
    }

    public void assign(final Class<?> type, final Object instance) {
        mappings.put(type, instance);
    }

    private void injectField(final Field field, final Object bean) throws Exception {
        final Object instanceToInject = mappings.get(field.getType());
        setFieldValue(field, bean, instanceToInject);
    }

    private void setFieldValue(final Field field, final Object bean, final Object instanceToInject) throws IllegalAccessException {
        if (!field.isAccessible()) {
            field.setAccessible(true);
        }
        field.set(bean, instanceToInject);
    }

    private List<Field> getAnnotatedFields(final Object bean) {
        final List<Field> annotatedFields = new ArrayList<Field>();
        for (final Field field : bean.getClass().getDeclaredFields()) {
            if (hasAnnotation(field)) {
                annotatedFields.add(field);
            }
        }
        return annotatedFields;
    }
}
