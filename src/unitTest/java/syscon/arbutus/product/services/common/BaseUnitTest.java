package syscon.arbutus.product.services.common;

import org.hibernate.Session;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;

import javax.ejb.SessionContext;

import static org.mockito.Mockito.mock;

/**
 * Created by chuan.pan on 13/05/2015.
 */
public class BaseUnitTest {
    final public EJBInjector injector = new EJBInjector();
    protected Session sessionMock = mock(Session.class);
    SessionContext contextMock = mock(SessionContext.class);

    @BeforeMethod(alwaysRun = true)
    public void initMocks() {
        injector.assign(Session.class, sessionMock);
        injector.assign(SessionContext.class, contextMock);

        MockitoAnnotations.initMocks(this);
    }
}
