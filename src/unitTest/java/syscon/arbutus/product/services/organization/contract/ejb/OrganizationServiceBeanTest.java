package syscon.arbutus.product.services.organization.contract.ejb;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.ErrorCodes;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;
import syscon.arbutus.product.services.location.contract.interfaces.LocationServiceLocal;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;
import syscon.arbutus.product.services.organization.contract.dto.Organization;
import syscon.arbutus.product.services.program.contract.interfaces.ProgramService;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

/**
 * For WOR-17869 & WOR-21182
 */

public class OrganizationServiceBeanTest {
    @Mock
    private UserContext context;

    @Mock
    private FacilityService facilityService;

    @Mock
    private MovementService movementService;

    @Mock
    private ProgramService programService;

    @Mock
    private LegalService legalService;

    @Mock
    private SupervisionService supervisionService;

    @Mock
    private LocationServiceLocal locationService;

    private OrganizationServiceBean sut;
    private Organization organizationStub;
    private static String GOOD_ORI = "1234G6789  ";
    private static String BAD_ORI_TOO_LONG = "1234567892  ";
    private static String BAD_ORI_TOO_SHORT = "78B2   ";


    @BeforeMethod
    public void init() {
        sut = new OrganizationServiceBean();
        sut.setLocationService(locationService);
        organizationStub = new Organization();
        organizationStub.setOrganizationORIIdentification(GOOD_ORI);

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void validateOrganizationWithInvalidORI() {
        try {
            organizationStub.setOrganizationORIIdentification(BAD_ORI_TOO_LONG);
            sut.validateORI(organizationStub);
            fail("ORI validation failed");
        } catch (ArbutusRuntimeException e) {
            assertEquals(e.getCode(), ErrorCodes.ORG_ORGANIZATION_ORI_CODE_LENGTH_EXCEEDED);
        }
    }

    @Test
    public void validateOrganizationWithInvalidORI2() {
        try {
            organizationStub.setOrganizationORIIdentification(BAD_ORI_TOO_SHORT);
            sut.validateORI(organizationStub);
            fail("ORI validation failed");
        } catch (ArbutusRuntimeException e) {
            assertEquals(e.getCode(), ErrorCodes.ORG_ORGANIZATION_ORI_CODE_LENGTH_BELOW_REQUIRED);
        }
    }

    @Test
    public void validateOrganizationORI() {
        try {
            organizationStub.setOrganizationORIIdentification(GOOD_ORI);
            sut.validateORI(organizationStub);
        } catch (ArbutusRuntimeException e) {
            fail("ORI validation failed");
        }
    }

    @Test
    public void shouldNotValidateOrganizationORI() {
        organizationStub.setOrganizationCategories(Collections.<String>emptySet());
        assertFalse(organizationStub.isORIApplicable());
    }

    @Test
    public void shouldNotValidateOrganizationORI2() {
        Set<String> set = new TreeSet<>();
        set.add("TRASH");
        set.add("RUBBISH");
        organizationStub.setOrganizationCategories(set);
        assertFalse(organizationStub.isORIApplicable());
    }

    @Test
    public void shouldValidateOrganizationORI() {
        organizationStub.setOrganizationCategories(Organization.ORI_VALIDATION_SET);
        assertTrue(organizationStub.isORIApplicable());
    }
}

