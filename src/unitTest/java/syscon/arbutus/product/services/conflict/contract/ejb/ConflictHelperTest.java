package syscon.arbutus.product.services.conflict.contract.ejb;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.jboss.weld.util.collections.ArraySet;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.realization.persistence.ScheduleEntity;
import syscon.arbutus.product.services.conflict.contract.dto.ConflictQueryDataHolder;
import syscon.arbutus.product.services.program.realization.persistence.ProgramAssignmentEntity;
import syscon.arbutus.product.services.supervision.realization.persistence.SupervisionEntity;

import java.util.HashMap;

import static org.mockito.Mockito.*;
/**
 * Created by ian.rivers on 29/06/2015.
 */
public class ConflictHelperTest {

    ConflictHelper conflictHelper;

    @Mock
    ConflictQueryDataHolder dataHolder;

    @Mock
    Criteria criteria;

    @Mock
    Session session;

    @BeforeMethod
    public void setUp() {
        conflictHelper = new ConflictHelper();

        MockitoAnnotations.initMocks(this);

        dataHolder.session = session;

        dataHolder.supervisionEntityMap = new HashMap<>();
        dataHolder.facilityId = 1L;
        dataHolder.supervisionEntityMap.put(1L, mock(SupervisionEntity.class));

        dataHolder.scheduleEntityMap = new HashMap<>();
        dataHolder.scheduleEntityMap.put(1L, mock(ScheduleEntity.class));

        dataHolder.facilityInternalLocationIdSet = new ArraySet<>();
        dataHolder.facilityInternalLocationIdSet.add(1L);

        dataHolder.sessionDate = new LocalDate().plusDays(1);
        dataHolder.startTime = new LocalTime();
        dataHolder.endTime = new LocalTime();
    }

    @Test
    public void testGetProgramAssignmentList() {

        when(dataHolder.session.createCriteria(ProgramAssignmentEntity.class)).thenReturn(criteria);

        // the business
        conflictHelper.getProgramAssignmentList(dataHolder);

        verify(criteria, times(6)).add((Criterion)any());
        verify(criteria).list();
    }

    // TODO: this test is in bad shape. It isn't fully covering the method.
    @Test
    public void getTransfromMovementActivityList() throws Exception {


//        when(dataHolder.session.createCriteria(LastCompletedSupervisionMovementEntity.class)).thenReturn(criteria);
//        when(dataHolder.session.get(MovementActivityEntity.class, 1L)).thenReturn(mock(MovementActivityEntity.class));
//        // the business
//        conflictHelper.getTransfromMovementActivityList(dataHolder, null);
//
//        verify(criteria, times(1)).add(any());
//        verify(criteria).list();

    }
}