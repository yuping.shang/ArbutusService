package syscon.arbutus.product.services.activity.contract.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.contract.dto.ActivityCategory;
import syscon.arbutus.product.services.activity.contract.dto.DailyWeeklyCustomRecurrencePatternType;
import syscon.arbutus.product.services.activity.realization.persistence.ScheduleEntity;
import syscon.arbutus.product.services.facilitycountactivity.contract.ejb.QueryUtil;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.*;

/**
 * Created by ian on 9/22/15.
 */
public class ActivityQueryUtilTest {
    @Mock
    Session session;
    @Mock
    Criteria criteria;

    Long facilityId;
    Long internalLocationId;
    Set<Long> internalLocationIdSet;
    Set<Long> facilityIdSet;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        facilityId = 1L;
        facilityIdSet = new HashSet<>(Arrays.asList(new Long[] { 1L, 2L, 3L, 4L, 5L }));
        internalLocationId = 23L;
        internalLocationIdSet = new HashSet<>(Arrays.asList(new Long[]{1L, 2L, 3L, 4L, 5L}));
    }

    @Test
    public void getScheduleCriteriaByFacilityIdAndInternalLocationId() {
        when(session.createCriteria(ScheduleEntity.class)).thenReturn(criteria);
        Criteria criteriaRet = ActivityQueryUtil.getScheduleCriteria(session, facilityId, internalLocationId, ActivityCategory.FACOUNT.name(), new LocalDate(), new LocalTime(),
                null, new DailyWeeklyCustomRecurrencePatternType());
        assertNotNull(criteriaRet);
    }

    @Test
    public void getScheduleCriteriaByFacilityIdSetAndInternalLocationIdSet() {
        when(session.createCriteria(ScheduleEntity.class)).thenReturn(criteria);

        Criteria criteriaRet = ActivityQueryUtil.getScheduleCriteria(session, facilityIdSet, internalLocationIdSet, ActivityCategory.FACOUNT.name(), new LocalDate(), new LocalTime(), null, new DailyWeeklyCustomRecurrencePatternType(), false);
        assertNotNull(criteriaRet);

        verify(session, times(1)).createCriteria(ScheduleEntity.class);
        verify(criteria, times(1)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        verify(criteria, times(1)).setCacheable(true);
        verify(criteria, atLeastOnce()).add(any());
        verify(criteria, times(1)).createCriteria("actSchPtn", "pattern");
    }

}