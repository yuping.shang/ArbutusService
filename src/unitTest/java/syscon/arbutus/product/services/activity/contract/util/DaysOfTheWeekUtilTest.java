package syscon.arbutus.product.services.activity.contract.util;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.contract.dto.DailyWeeklyCustomRecurrencePatternType;
import syscon.arbutus.product.services.activity.contract.dto.DayOfWeekEnum;
import syscon.arbutus.product.services.activity.contract.dto.DaysOfTheWeek;

import static org.testng.Assert.assertTrue;

/**
 * Created by ian on 9/16/15.
 */
public class DaysOfTheWeekUtilTest {

    DaysOfTheWeekUtil daysOfTheWeekUtil;

    @BeforeMethod
    public void setup() {
        daysOfTheWeekUtil = new DaysOfTheWeekUtil();
    }

    @Test
    public void testGenerateDaysOfTheWeekFromRecurrencePattern() {
        DailyWeeklyCustomRecurrencePatternType recPattern = new DailyWeeklyCustomRecurrencePatternType();
        recPattern.setRecurOnDays(new HashSet<>());
        recPattern.getRecurOnDays().add(DayOfWeekEnum.MON.getName());
        DaysOfTheWeek days = daysOfTheWeekUtil.generateDaysOfTheWeekFromRecurrencePattern(recPattern).get();
        assertTrue(days.monday, "days of the week should have correct dates toggled");
    }

    @Test
    public void testGenerateStringSet() throws Exception {
        DaysOfTheWeek daysOfTheWeek = new DaysOfTheWeek();
        daysOfTheWeek.monday = true;
        Set<String> dayStrings = daysOfTheWeekUtil.generateStringSet(daysOfTheWeek);
        assertTrue(dayStrings.contains(DayOfWeekEnum.MON.getName()));
    }

}