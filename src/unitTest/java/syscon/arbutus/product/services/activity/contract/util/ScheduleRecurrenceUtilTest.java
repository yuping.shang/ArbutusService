package syscon.arbutus.product.services.activity.contract.util;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.contract.dto.*;

import static org.testng.Assert.*;

/**
 * Created by ian on 9/22/15.
 */
public class ScheduleRecurrenceUtilTest {

    ScheduleRecurrenceUtil scheduleRecurrenceUtil;

    @BeforeMethod
    public void setup() {
        scheduleRecurrenceUtil = new ScheduleRecurrenceUtil();
    }

    @Test(dataProvider = "recurrenceEnumTester")
    public void testCalculateRecurrenceEnum(DailyWeeklyCustomRecurrencePatternType pattern, RecurrenceEnum expectedEnum) throws Exception {
        RecurrenceEnum result = scheduleRecurrenceUtil.calculateRecurrenceEnum(pattern);
        assertTrue(result.equals(expectedEnum), "enums should match up");
    }

    @DataProvider(name = "recurrenceEnumTester")
    public Object[][] dataProviderForTest() {

        DailyWeeklyCustomRecurrencePatternType daily = new DailyWeeklyCustomRecurrencePatternType();

        DailyWeeklyCustomRecurrencePatternType weekdays = new DailyWeeklyCustomRecurrencePatternType(true);

        DailyWeeklyCustomRecurrencePatternType custom = new DailyWeeklyCustomRecurrencePatternType();
        Set<String> recurOnDays = new HashSet<>();
        recurOnDays.add(DayOfWeekEnum.MON.getName());
        custom.setRecurOnDays(recurOnDays);
        Object[][] result = { { daily, new DailyRecurrenceObject().recurrenceEnum }, { weekdays, new WeekdayRecurrenceObject().recurrenceEnum },
                { custom, new CustomRecurrenceObject().recurrenceEnum } };

        return result;
    }

}