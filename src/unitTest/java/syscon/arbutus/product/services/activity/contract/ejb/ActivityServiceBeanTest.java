package syscon.arbutus.product.services.activity.contract.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.DaysOfTheWeek;
import syscon.arbutus.product.services.activity.contract.dto.RecurrenceEnum;
import syscon.arbutus.product.services.activity.contract.dto.Schedule;
import syscon.arbutus.product.services.activity.contract.util.DaysOfTheWeekUtil;
import syscon.arbutus.product.services.activity.contract.util.RecurrencePatternFactory;
import syscon.arbutus.product.services.realization.util.BeanHelper;

import static org.testng.Assert.*;
import static org.mockito.Mockito.*;
/**
 * Created by ian on 9/22/15.
 */
public class ActivityServiceBeanTest {

    ActivityServiceBean activityServiceBean;

    RecurrencePatternFactory recurrencePatternFactory = new RecurrencePatternFactory();
    @Mock
    UserContext userContext;

    @Mock
    Session session;
    @Mock
    ActivityServiceDAO handler;

    String DEFAULT_START_TIME = "08:00";

    Date DEFAULT_START_DATE_TODAY = BeanHelper.createDateWithoutTime();



    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);
        activityServiceBean = new ActivityServiceBean();
        activityServiceBean.session = session;
        activityServiceBean.handler = handler;
    }

    @Test(dataProvider = "testScheduleAlreadyExistsProvider_fail")
    public void testHasDuplicateOverlappingSchedule_fail(Schedule inputSchedule, List<Schedule> existingDBSchedules) throws Exception {
        when(handler.getSchedulesFromConstraints(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean())).thenReturn(existingDBSchedules);
        Boolean returnVar = activityServiceBean.hasDuplicateOverlappingSchedule(userContext, inputSchedule);
        assertTrue(returnVar, "should have returned true!");
    }

    @Test(dataProvider = "testScheduleAlreadyExistsProvider")
    public void testHasDuplicateOverlappingSchedule(Schedule inputSchedule, List<Schedule> existingDBSchedules) throws Exception {
        when(handler.getSchedulesFromConstraints(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean())).thenReturn(existingDBSchedules);
        Boolean returnVar = activityServiceBean.hasDuplicateOverlappingSchedule(userContext, inputSchedule);
        assertFalse(returnVar, "should have returned false!");
    }

    @DataProvider(name = "testScheduleAlreadyExistsProvider")
    public Object[][]  testDuplicates() {
        DaysOfTheWeek days = new DaysOfTheWeek();

        days.monday = true;
        Schedule newCustom = generateCustom(days);
        newCustom.setEffectiveDate(BeanHelper.nextNthDays(DEFAULT_START_DATE_TODAY, 7));
        newCustom.setTerminationDate(BeanHelper.nextNthDays(DEFAULT_START_DATE_TODAY, 14));
        newCustom.setScheduleIdentification(null);

        List<Schedule> oneSchedule_Custom2 = new ArrayList<>();
        Schedule existingCustom = generateCustom(days);

        existingCustom.setTerminationDate(BeanHelper.nextNthDays(DEFAULT_START_DATE_TODAY, 7));

        return new Object[][] {
                {newCustom, oneSchedule_Custom2 } };
    }


    @DataProvider(name = "testScheduleAlreadyExistsProvider_fail")
    public Object[][]  testDuplicates_fail() {
        // Daily
        Schedule newDaily = generateDaily();
        newDaily.setScheduleIdentification(null);
        // Weekdays
        Schedule newWeekdays = generateWeekdays();
        newWeekdays.setScheduleIdentification(null);
        // Custom
        DaysOfTheWeek days = new DaysOfTheWeek();
        days.monday = true;
        Schedule newCustom = generateCustom(days);
        newCustom.setScheduleIdentification(null);


        // empty case
        List<Schedule> emptyCase = new ArrayList<>();
        List<Schedule> oneSchedule_Daily = new ArrayList<>();
        oneSchedule_Daily.add(generateDaily());

        List<Schedule> oneSchedule_Weekdays = new ArrayList<>();
        oneSchedule_Weekdays.add(generateWeekdays());

        List<Schedule> oneSchedule_Custom = new ArrayList<>();
        oneSchedule_Custom.add(generateCustom(days));


        return new Object[][] {
                {newDaily, oneSchedule_Daily },
                {newDaily, oneSchedule_Weekdays },
                 {newDaily, oneSchedule_Custom },
               {newWeekdays, oneSchedule_Daily},
                {newWeekdays, oneSchedule_Weekdays},
                {newCustom, oneSchedule_Custom} };

    }

    private Schedule generateDaily() {
        Schedule newDaily = generateFacilityCountSchedule(DEFAULT_START_TIME, DEFAULT_START_DATE_TODAY);
        newDaily.setRecurrencePattern(recurrencePatternFactory.generate(RecurrenceEnum.DAILY, null));
        return newDaily;
    }

    private Schedule generateWeekdays() {
        Schedule newWeekdays = generateFacilityCountSchedule(DEFAULT_START_TIME, DEFAULT_START_DATE_TODAY);
        newWeekdays.setRecurrencePattern(recurrencePatternFactory.generate(RecurrenceEnum.WEEKDAYS, null));
        return newWeekdays;
    }

    private Schedule generateCustom(DaysOfTheWeek days) {
        Schedule newCustom = generateFacilityCountSchedule(DEFAULT_START_TIME, DEFAULT_START_DATE_TODAY);
        newCustom.setRecurrencePattern(recurrencePatternFactory.generate(RecurrenceEnum.CUSTOM, new DaysOfTheWeekUtil().generateStringSet(days)));
        return newCustom;
    }

    private Schedule generateFacilityCountSchedule(String startTime, Date startDate) {
        Schedule schedule = new Schedule();
        schedule.setScheduleIdentification(54321L);
        schedule.setFacilityId(1L);
        schedule.setScheduleCategory("");
        schedule.setScheduleStartTime(startTime);
        schedule.setEffectiveDate(startDate);
        return schedule;
    }

}