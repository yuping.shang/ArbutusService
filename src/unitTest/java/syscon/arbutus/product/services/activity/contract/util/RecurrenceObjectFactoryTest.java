package syscon.arbutus.product.services.activity.contract.util;

import java.util.HashSet;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.contract.dto.AbstractRecurrenceObject;
import syscon.arbutus.product.services.activity.contract.dto.CustomRecurrenceObject;
import syscon.arbutus.product.services.activity.contract.dto.DailyWeeklyCustomRecurrencePatternType;
import syscon.arbutus.product.services.activity.contract.dto.DayOfWeekEnum;

import static org.testng.Assert.assertTrue;

/**
 * Created by ian on 9/16/15.
 */
public class RecurrenceObjectFactoryTest {

    RecurrenceObjectFactory recurrenceObjectFactory;

    @BeforeMethod
    public void setup() {
        recurrenceObjectFactory = new RecurrenceObjectFactory();
    }

    @Test
    public void testGenerate() throws Exception {
        DailyWeeklyCustomRecurrencePatternType pattern = new DailyWeeklyCustomRecurrencePatternType();
        pattern.setRecurOnDays(new HashSet<>());
        pattern.getRecurOnDays().add(DayOfWeekEnum.MON.getName());
        AbstractRecurrenceObject aro = recurrenceObjectFactory.generate(pattern);
        assertTrue(aro instanceof CustomRecurrenceObject, "should return a custom recurrence object");
    }
}