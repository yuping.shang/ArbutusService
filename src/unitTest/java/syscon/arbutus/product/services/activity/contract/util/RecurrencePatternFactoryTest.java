package syscon.arbutus.product.services.activity.contract.util;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.contract.dto.*;

import static org.testng.Assert.assertTrue;

/**
 * Created by ian on 9/16/15.
 */
public class RecurrencePatternFactoryTest {

    RecurrencePatternFactory recurrencePatternFactory;

    @BeforeMethod
    public void setup() {
        recurrencePatternFactory = new RecurrencePatternFactory();
    }

    @Test
    public void testGenerateRecurrencePattern() throws Exception {
        CustomRecurrenceObject recurrenceObject = new CustomRecurrenceObject();
        DaysOfTheWeek days = new DaysOfTheWeek();
        days.monday = true;
        recurrenceObject.setDaysOfTheWeek(days);
        RecurrencePatternType pattern = recurrencePatternFactory.generate(recurrenceObject);
        assertTrue(pattern instanceof DailyWeeklyCustomRecurrencePatternType, "should be returned as correct type");
        assertTrue(((DailyWeeklyCustomRecurrencePatternType) pattern).getRecurOnDays().contains(DayOfWeekEnum.MON.getName()));
    }


}