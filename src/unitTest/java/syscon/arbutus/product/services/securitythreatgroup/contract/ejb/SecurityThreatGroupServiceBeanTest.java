package syscon.arbutus.product.services.securitythreatgroup.contract.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.clientcustomfield.contract.interfaces.ClientCustomFieldServiceLocal;
import syscon.arbutus.product.services.common.BaseUnitTest;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.*;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

/**
 * Created by hshen on 7/15/15.
 */
public class SecurityThreatGroupServiceBeanTest extends BaseUnitTest {

    @Mock
    protected UserContext userContext;
    @Mock
    protected SecurityThreatGroupDAO dao;
    @Mock
    protected ClientCustomFieldServiceLocal ccfService;
    private SecurityThreatGroupServiceBean service = new SecurityThreatGroupServiceBean();
    //gangs hierarchy config
    private List<Long> stgIdList;
    private List<Long> empty;
    private List<Long> gangOne;
    private List<Long> gangTwo;   
    private List<Long> childrenOfGangOne;
    private List<Long> childrenOfGangTwo;
    private List<Long> familyOne;
    private List<Long> all;
    private List<Long> enemies;

    @BeforeClass
    public void setup() throws Exception {
    }

    @BeforeMethod
    public void init() throws Exception {
        //MockitoAnnotations.initMocks(this);
        //this.injector.assign(SecurityThreatGroupDAO.class, dao);
        //this.injector.inject(service);
        dao = Mockito.mock(SecurityThreatGroupDAO.class);
        service.setSecurityThreatGroupDAO(dao);
    }

    /**
     * Verity the call behavior, the exception catch for reference only
     */
    @Test(enabled = false)
    public void createAffiliation() {
        Affiliation affiliation = new Affiliation();
        affiliation.setSTGId(9L);
        affiliation.setPersonId(99L);
        affiliation.setAffiliationReason("reason");
        affiliation.setAffiliationDate(new Date());
        affiliation.setAffiliatedBy(999L);

        Long output = new Long(1000L);

        when(dao.createAffiliation(any(UserContext.class), any(Affiliation.class))).thenReturn(9L);

        try {
            output = service.createAffiliation(userContext, affiliation);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            assertEquals(InvalidInputException.class, e.getClass());

        }
        System.out.println("Operation result :" + output.toString());

        // verify(securityThreatGroupDAO).createAffiliation(userContext,affiliation);
    }

    @Test(enabled = true)
    public void testNullDto() throws Exception {
        try {
            service.createRelationship(userContext, null);
            fail("Null STGRelationshipType should not be able to call service w/o errors");
        } catch (Exception e) {
            assertEquals(InvalidInputException.class, e.getClass());
            assertTrue(e.getMessage().contains("DTO validation failure, STGRelationshipType=null."));
        }
    }

    @Test(enabled = true)
    public void testEmptyDto() throws Exception {
        STGRelationship stgRelationshipType = new STGRelationship();
        try {
            service.createRelationship(userContext, stgRelationshipType);
            fail("Should not get here");
        } catch (Exception e) {
            assertEquals(InvalidInputException.class, e.getClass());
            //assertTrue(e.getMessage().contains("EJBCLIENT000025: No EJB receiver available for handling"));
        }
    }

    // additional testing of createRelationship is blocked by SecurityThreatGroupHelper using lookup for service
    // SecurityThreatGroupHelper.line 176: StaffType staff = StaffServiceAdapter.get(uc, entity.getAffiliatedBy());

    @Test(enabled = true)
    public void testGetEligibleSTGForRelationshipType_NullInput() {
        try {
            service.getEligibleSTGForRelationshipType(userContext, null, null, null, null, null);
            fail("Should not get here");
        } catch (Exception e) {
            assertEquals(InvalidInputException.class, e.getClass());
            assertTrue(e.getMessage().contains("Invalid user context"));
        }
    }

    @Test(enabled = true)
    public void testGetEligibleSTGForRelationshipType_Empty() {
        STGRelationshipTypeEnum alliance = STGRelationshipTypeEnum.ALLIANCE;
        List<SecurityThreatGroup> result = service.getEligibleSTGForRelationshipType(userContext, Mockito.any(Long.class), alliance, null, null, null);
        assertTrue(result.isEmpty());
    }

    /* parents alliance, find eligible for a child
     * When an STG has been added as an Alliance/Enem...
     * When an STG has been added as an Alliance/Enemy, it cannot be re-selected again
     * If an STG has been added as an Alliance , it cannot be added as an Enemy
     * If the group of interest is an alliance at a parent level group then all their child groups will inherit the alliance relationships
     * If the group of interest is an enemy at a parent level group then all their child groups will inherit the enemy relationships
     * If the group of interest is a child level group, it can have a different inherited relationship with another group child level group. For example, Gang A has a child Set A
     * and Gang B has a child Set B. Gang A and Gang B are enemies, however Set A and Set B can be in alliances.
     * The same created relationship should reciprocally be displayed when viewing the other related STG
     */
    @Test(enabled = true)
    public void testGetEligibleSTGForRelationshipType_Alliance_Parents_findAlliance() {
        Long stgId = 11l;
        initGangsHierarchy(stgId);

        Mockito.when(dao.findAllianceOrEnemies(familyOne, STGRelationshipTypeEnum.ENEMY.toString())).thenReturn(enemies);
        List<SecurityThreatGroup> result = service.getEligibleSTGForRelationshipType(userContext, stgId, STGRelationshipTypeEnum.ALLIANCE, null, null, null);

        //should return stgs 2 , 21 
        Long[] expectedResultIds = new Long[]{2L, 21L};
        verifyEligibility(result, expectedResultIds);
    }

    @Test(enabled = true)
    public void testGetEligibleSTGForRelationshipType_Alliance_Parents_findEnemies() {
        Long stgId = 11l;
        initGangsHierarchy(stgId);

        Mockito.when(dao.findAllianceOrEnemies(familyOne, STGRelationshipTypeEnum.ALLIANCE.toString())).thenReturn(familyOne);
        List<SecurityThreatGroup> result = service.getEligibleSTGForRelationshipType(userContext, stgId, STGRelationshipTypeEnum.ENEMY, null, null, null);

        //should return stgs 2 , 21 
        Long[] expectedResultIds = new Long[]{2L, 21L};
        verifyEligibility(result, expectedResultIds);
    }

    @Test(enabled = true)
    public void testGetEligibleSTGForRelationshipType_Enemies_Parents_findAlliance() {
        Long stgId = 11l;
        initGangsHierarchy(stgId);

        enemies.addAll(this.gangTwo);
        enemies.addAll(this.childrenOfGangTwo);

        Mockito.when(dao.findAllianceOrEnemies(familyOne, STGRelationshipTypeEnum.ENEMY.toString())).thenReturn(enemies);
        List<SecurityThreatGroup> result = service.getEligibleSTGForRelationshipType(userContext, stgId, STGRelationshipTypeEnum.ALLIANCE, null, null, null);

        Long[] expectedResultIds = new Long[]{};
        verifyEligibility(result, expectedResultIds);
    }

    @Test(enabled = true)
    public void testGetEligibleSTGForRelationshipType_Enemies_Parents_findEnemies() {
        Long stgId = 11l;
        initGangsHierarchy(stgId);

        Mockito.when(dao.findAllianceOrEnemies(familyOne, STGRelationshipTypeEnum.ALLIANCE.toString())).thenReturn(familyOne);
        List<SecurityThreatGroup> result = service.getEligibleSTGForRelationshipType(userContext, stgId, STGRelationshipTypeEnum.ENEMY, null, null, null);

        //should return stgs 2 , 21 
        Long[] expectedResultIds = new Long[]{2L, 21L};
        verifyEligibility(result, expectedResultIds);
    }

    private void verifyEligibility(List<SecurityThreatGroup> result, Long[] expectedResultIds) {
        assertEquals(expectedResultIds.length, result.size());
        int i = 0;
        for (SecurityThreatGroup stg : result) {
            assertEquals(expectedResultIds[i++], stg.getId());           
        }
    }

    private void initGangsHierarchy(Long stgId) {

        stgIdList = new ArrayList<Long>();
        empty = new ArrayList<Long>();
        gangOne = new ArrayList<Long>();
        gangTwo = new ArrayList<Long>();   
        childrenOfGangOne = new ArrayList<Long>();
        childrenOfGangTwo = new ArrayList<Long>();
        familyOne = new ArrayList<Long>();
        all = new ArrayList<Long>();
        enemies = new ArrayList<Long>();

        stgIdList.add(stgId);

        gangOne.add(1l);

        childrenOfGangOne.add(11l);
        childrenOfGangOne.add(12L);

        gangTwo.add(2L);

        childrenOfGangTwo.add(21l);

        familyOne.addAll(childrenOfGangOne);
        familyOne.addAll(gangOne);


        all.addAll(gangOne);
        all.addAll(gangTwo);
        all.addAll(childrenOfGangOne);
        all.addAll(childrenOfGangTwo);    
        
        Mockito.when(dao.findChildren(Matchers.anyListOf(Long.class))).thenReturn(empty).thenReturn(childrenOfGangOne);
        Mockito.when(dao.findParents(Matchers.anyListOf(Long.class))).thenReturn(gangOne);

        Mockito.when(dao.findAllSTG()).thenReturn(all);
        Mockito.when(dao.getElligibleStg(Mockito.any(UserContext.class), Matchers.anyListOf(Long.class), Mockito.any(Long.class), Mockito.any(Long.class), Mockito.any(String.class))).thenAnswer(createDto());

    }

    //TODO: Add more tests to this eligibility
    //


    private Answer<List<SecurityThreatGroup>> createDto() {
        return new Answer<List<SecurityThreatGroup>>() {
            public List<SecurityThreatGroup> answer(InvocationOnMock invocation) {
                @SuppressWarnings("unchecked")
                List<Long> eligibles = (List<Long>) invocation.getArguments()[1];
                List<SecurityThreatGroup> returnDtos = new ArrayList<SecurityThreatGroup>();
                for (Long id : eligibles) {
                    SecurityThreatGroup stg = new SecurityThreatGroup();
                    stg.setId(id);
                    returnDtos.add(stg);
                }
                return returnDtos;
            }
        };
    }


    @Test
    public void testCreateOrUpdateSTGHierarchies_invalidInputAllowedLevelsCheck() {
        List<SecurityThreatGroupHierarchy> stgHierarchies = new ArrayList<>(3);
        stgHierarchies.add(new SecurityThreatGroupHierarchy());
        stgHierarchies.add(new SecurityThreatGroupHierarchy());
        stgHierarchies.add(new SecurityThreatGroupHierarchy());

        try {
            service.createOrUpdateSTGHierarchies(userContext, stgHierarchies);
            fail("you will never execute me");
        } catch (Exception ex) {
            assertEquals(InvalidInputException.class, ex.getClass());
        }

    }

    @Test
    public void testCreateOrUpdateSTGHierarchies_invalidInputNoInput() {
        List<SecurityThreatGroupHierarchy> stgHierarchies = new ArrayList<>(3);
        try {
            service.createOrUpdateSTGHierarchies(userContext, stgHierarchies);
            fail("you will never execute me");
        } catch (Exception ex) {
            assertEquals(InvalidInputException.class, ex.getClass());
        }

    }


}
