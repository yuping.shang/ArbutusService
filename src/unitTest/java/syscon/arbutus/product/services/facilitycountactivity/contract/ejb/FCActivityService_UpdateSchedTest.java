package syscon.arbutus.product.services.facilitycountactivity.contract.ejb;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.*;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.activity.contract.util.DaysOfTheWeekUtil;
import syscon.arbutus.product.services.activity.contract.util.RecurrenceObjectFactory;
import syscon.arbutus.product.services.activity.contract.util.ScheduleRecurrenceUtil;
import syscon.arbutus.product.services.activity.realization.persistence.ScheduleEntity;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.ErrorCodes;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.realization.util.BeanHelper;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 *
 */
public class FCActivityService_UpdateSchedTest {

    ConfigureFacilityCountHelper configureFacilityCountHelper;

    @Mock
    UserContext userContext;

    @Mock
    ActivityService activityService;

    @Mock
    FacilityService facilityService;

    @Mock
    Session session;

    @Mock
    FacilityCountActivityDAO handler;

    @BeforeMethod
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        Criteria c = mock(Criteria.class);
        when(c.list()).thenReturn(new ArrayList());
        when(session.createCriteria(ScheduleEntity.class)).thenReturn(c);


        SimpleSchedule schedule = new SimpleSchedule();
        schedule.setScheduleCategory("");
        schedule.setFacilityId(1L);
        schedule.setScheduleIdentification(1L);
        schedule.setEffectiveDate(BeanHelper.createDateWithoutTime());
        when(activityService.getSchedule(any(), any())).thenReturn(schedule);

        configureFacilityCountHelper = new ConfigureFacilityCountHelper(activityService, facilityService, handler);
    }

    @Test(dataProvider = "provideExistingSchedules")
    public void testUpdateFCSchedule(Schedule schedule, Long countsToReturn, SimpleSchedule originalSchedule) {

        List<ActivityType> tempList = new ArrayList<>();
        ActivityType activityType = new ActivityType();
        activityType.setScheduleID(1L);
        tempList.add(activityType);
        Query mockQuery = mock(Query.class);
        when(mockQuery.uniqueResult()).thenReturn(new Long(countsToReturn));
        when(mockQuery.setParameter(anyString(), anyObject())).thenReturn(mockQuery);

        when(session.createQuery(any())).thenReturn(mockQuery);
        when(activityService.getFromScheduleId(any(), any(), any())).thenReturn(tempList);
        when(activityService.update(any(), any(), any(), any())).thenReturn(new ActivityType());
        when(activityService.getSchedule(any(), any())).thenReturn(originalSchedule);

        Optional<DaysOfTheWeek> days = new DaysOfTheWeekUtil().generateDaysOfTheWeekFromRecurrencePattern(schedule.getRecurrencePattern());


        try {
            configureFacilityCountHelper.updateFacilityCountSchedule(userContext, session,
                    schedule.getScheduleIdentification(),
                    schedule.getScheduleStartTime(),
                    schedule.getEffectiveDate(),
                    schedule.getTerminationDate(),
                    new RecurrenceObjectFactory().generate(new ScheduleRecurrenceUtil().calculateRecurrenceEnum(schedule.getRecurrencePattern()),
                            days.orElseGet(() -> new DaysOfTheWeek())));
            assertTrue(true);
            verify(activityService).update(any(), any(), any(), any());
        } catch (ArbutusRuntimeException e) {
            fail("not supposed to catch an exception");
        }
    }

    @Test(dataProvider = "provideExistingSchedules")
    public void testUpdateFCSchedule_fail(Schedule schedule, Long countsToReturn, SimpleSchedule originalSchedule, ErrorCodes code) {
        try {
            when(handler.getNumberOfCountsPerformedForSchedule(any(), any(), any())).thenReturn(countsToReturn);
            when(activityService.getSchedule(any(), any())).thenReturn(originalSchedule);
            Optional<DaysOfTheWeek> days = new DaysOfTheWeekUtil().generateDaysOfTheWeekFromRecurrencePattern(schedule.getRecurrencePattern());

            configureFacilityCountHelper.updateFacilityCountSchedule(userContext, session, schedule.getScheduleIdentification(), schedule.getScheduleStartTime(),
                    schedule.getEffectiveDate(), schedule.getTerminationDate(),
                    new RecurrenceObjectFactory().generate(new ScheduleRecurrenceUtil().calculateRecurrenceEnum(schedule.getRecurrencePattern()),
                             days.orElseGet(() -> new DaysOfTheWeek())));
            fail("not supposed to get here");
        } catch (ArbutusRuntimeException e) {
            assertTrue(e.getCode().equals(code), "error codes must match up");
        }
    }


    @DataProvider(name = "provideExistingSchedules")
    public Object[][] provideExistingSchedules(Method method) {
        Object[][] result = null;



        Date effectiveDate = BeanHelper.getPastDate(BeanHelper.createDateWithoutTime(), 2);
        SimpleSchedule originalSchedule = new SimpleSchedule();
        originalSchedule.setFacilityId(1L);
        originalSchedule.setScheduleCategory("");
        originalSchedule.setEffectiveDate(effectiveDate);
        originalSchedule.setTerminationDate(BeanHelper.nextNthDays(BeanHelper.createDateWithoutTime(), 2));
        originalSchedule.setScheduleStartTime("08:00");
        originalSchedule.setScheduleIdentification(1L);



        SimpleSchedule schedule = new SimpleSchedule();
        schedule.setFacilityId(1L);
        schedule.setScheduleCategory("");
        schedule.setEffectiveDate(BeanHelper.createDateWithoutTime());
        schedule.setTerminationDate(BeanHelper.nextNthDays(BeanHelper.createDateWithoutTime(), 2));
        schedule.setScheduleStartTime("08:00");
        schedule.setScheduleIdentification(1L);

        SimpleSchedule failSchedule1 = new SimpleSchedule();
        failSchedule1.setScheduleCategory("");
        failSchedule1.setFacilityId(1L);
        failSchedule1.setEffectiveDate(BeanHelper.createDateWithoutTime());
        failSchedule1.setScheduleStartTime("08:00");
        failSchedule1.setScheduleIdentification(1L);
        failSchedule1.setTerminationDate(BeanHelper.getPastDate(BeanHelper.createDateWithoutTime(), 1));
        failSchedule1.setRecurrenceObject(new DailyRecurrenceObject());

        SimpleSchedule failSchedule2 = new SimpleSchedule(failSchedule1);
        failSchedule2.setScheduleIdentification(null);


        SimpleSchedule failSchedule3 = new SimpleSchedule();
        failSchedule3.setFacilityId(1L);
        failSchedule3.setScheduleCategory("");
        failSchedule3.setEffectiveDate(BeanHelper.getPastDate(BeanHelper.createDateWithoutTime(), 2));
        failSchedule3.setTerminationDate(BeanHelper.nextNthDays(BeanHelper.createDateWithoutTime(), 2));
        failSchedule3.setScheduleStartTime("08:00");
        failSchedule3.setScheduleIdentification(1L);

        if (method.getName().contains("fail")) {


            result = new Object[][] { /*{ failSchedule2, 0L, failSchedule2, ErrorCodes.GENERIC_NULL_EMPTY_INPUT },
                    { failSchedule1, 0L, failSchedule1, ErrorCodes.FCA_SCHEDULE_EXPIRY_BEFORE_START },*/
                    { schedule, 1L, originalSchedule, ErrorCodes.FCA_CANNOT_EDIT_STARTDATE_AFTER_COUNT_PERFORMED }/*,
                    { failSchedule3, 0L, schedule, ErrorCodes.FCA_UPDATE_SCHEDULE_START_BEFORE_TODAY }*/};
        }
        else {


            result = new Object[][] { { originalSchedule, 0L, originalSchedule},
                                         { originalSchedule, 1L, originalSchedule},
                                         { schedule, 0L, originalSchedule}};
        }

        return result;
    }

}
