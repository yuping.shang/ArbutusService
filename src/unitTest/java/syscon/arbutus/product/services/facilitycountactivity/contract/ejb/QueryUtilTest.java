package syscon.arbutus.product.services.facilitycountactivity.contract.ejb;

import javax.ejb.SessionContext;
import java.lang.reflect.Method;
import java.util.*;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.UsageCategory;
import syscon.arbutus.product.services.facilityinternallocation.realization.persistence.InternalLocationEntity;
import syscon.arbutus.product.services.housing.realization.persistence.HousingBedMgmtActivityEntity;
import syscon.arbutus.product.services.housing.realization.persistence.HousingChangeRequestEntity;
import syscon.arbutus.product.services.movement.realization.persistence.LastCompletedSupervisionMovementEntity;
import syscon.arbutus.product.testutil.Generator;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by yshang on 8/24/15.
 */
public class QueryUtilTest {
    @Mock Session session;
    @Mock Criteria criteria;
    @Mock Restrictions restrictions;
    @Mock Disjunction disjunction;
    @Mock UserContext userContext;
    @Mock
    SessionContext context;

    Long count;
    Long facilityId;
    Set<Long> facilityIdSet;
    Long internalLocationId;
    List<Long> internalLocationIdList;
    Set<Long> internalLocationIdSet;
    Generator<InternalLocationEntity> internalLocationGenerator;
    FacilityCountActivityDAO handler;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        count = 5L;
        facilityId = 1L;
        facilityIdSet = new HashSet<>(Arrays.asList(new Long[]{1L, 2L, 3L, 4L, 5L}));
        internalLocationId = 23L;
        internalLocationIdList = Arrays.asList(new Long[]{1L, 2L, 3L, 4L, 5L});
        internalLocationIdSet = new HashSet<>(internalLocationIdList);
        internalLocationGenerator = new Generator<>();

        when(session.createCriteria(HousingBedMgmtActivityEntity.class)).thenReturn(criteria);
        when(session.createCriteria(InternalLocationEntity.class)).thenReturn(criteria);
        when(session.createCriteria(LastCompletedSupervisionMovementEntity.class)).thenReturn(criteria);
        when(criteria.list()).thenReturn(internalLocationIdList);
        when(criteria.uniqueResult()).thenReturn(count);

        handler = new FacilityCountActivityDAO();
    }

    @DataProvider(name = "getChildLocationsData")
    public Object[][] getChildLocationsData(Method method) {
        Object[][] data = null;
        if (method.getName().equals("getChildLocations_byInternalLocation")) {
            data = new Object[][] {
                    { null, Long.valueOf(23L), UsageCategory.HOU }
            };
        } else if (method.getName().equals("getChildLocations_byFacility")) {
            data = new Object[][] {
                    { 1L, null, UsageCategory.HOU }
            };
        }
        return data;
    }

    @Test(dataProvider = "getChildLocationsData")
    public void getChildLocations_byInternalLocation(Long facilityId, Long internalLocationId, UsageCategory usageCategory) {
        when(session.createCriteria(InternalLocationEntity.class)).thenReturn(criteria);

        handler.getChildLocations(userContext, session, facilityId, internalLocationId, usageCategory);
        verify(session, times(1)).createCriteria(InternalLocationEntity.class);
        verify(criteria, times(1)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        verify(criteria, times(1)).createCriteria("capacities", "capacity");
        verify(criteria, times(3)).add(any());
    }

    @Test(dataProvider = "getChildLocationsData")
    public void getChildLocations_byFacility(Long facilityId, Long internalLocationId, UsageCategory usageCategory) {
        when(session.createCriteria(InternalLocationEntity.class)).thenReturn(criteria);
        when(handler.getInternalLocationByFacilityId(userContext, session, facilityId)).thenReturn(anyLong());
        List<InternalLocationEntity> internalLocationEntityList = handler.getChildLocations(userContext, session, facilityId, internalLocationId, usageCategory);
        verify(session, times(3)).createCriteria(InternalLocationEntity.class);
        verify(criteria, times(1)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        verify(criteria, times(1)).createCriteria("capacities", "capacity");
        verify(criteria, times(8)).add(any());
        assertNotNull(internalLocationEntityList);

        // when getInternalLocationByFacilityId() => null
        when(handler.getInternalLocationByFacilityId(userContext, session, facilityId)).thenReturn(null);
        internalLocationEntityList = handler.getChildLocations(userContext, session, facilityId, internalLocationId, usageCategory);
        assertNotNull(internalLocationEntityList);
    }

    @Test(enabled = false)
    public void getHousingReserved() {
        when(session.createCriteria(HousingChangeRequestEntity.class)).thenReturn(criteria);
        Long reservedCount = handler.getHousingReserved(userContext, session, facilityId, internalLocationId);
        assertNotNull(reservedCount);

        verify(criteria).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        verify(criteria, times(2)).setProjection(any());
        verify(criteria, times(5)).add(any());
        verify(criteria).list();
        verify(criteria).uniqueResult();
    }

    @Test
    public void disjunctIdSet() {
        Collection<Long> collection =null;
        disjunction = QueryUtil.disjunctIdSet("propertyName", collection);
        assertNull(disjunction);

        collection = new HashSet<>();
        disjunction = QueryUtil.disjunctIdSet("propertyName", collection);
        assertNull(disjunction);

        collection = new HashSet<>();
        collection.add(0L);
        disjunction = QueryUtil.disjunctIdSet("propertyName", collection);
        assertNotNull(disjunction);

        for (int i = 0; i < 5; i++) {
            collection.add(Long.valueOf(i));
        }
        disjunction = QueryUtil.disjunctIdSet("propertyName", collection);
        assertNotNull(disjunction);
    }


}
