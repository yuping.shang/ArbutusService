package syscon.arbutus.product.services.facilitycountactivity.contract.ejb;

import javax.ejb.SessionContext;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facilitycountactivity.contract.dto.SubmittedLocationCount;
import syscon.arbutus.product.services.facilitycountactivity.realization.persistence.ConsolidatedLocationCountEntity;
import syscon.arbutus.product.services.facilitycountactivity.realization.persistence.RegularFCActivityEntity;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationServiceLocal;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by ian on 9/15/15.
 */
public class FCActivityService_SubmitCountTest {

    FacilityCountActivityDAO handler;

    @Mock
    UserContext userContext;

    @Mock
    SessionContext context;

    @Mock
    Session session;

    @Mock
    FacilityInternalLocationService facilityInternalLocationService;

    @Mock
    ActivityService activityService;



    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);
        handler = new FacilityCountActivityDAO(activityService, facilityInternalLocationService);

    }



    @Test(expectedExceptions = { InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*argument must not be null.*")
    public void testSubmitLocationCount_InvalidFacilityId() {
        handler.submitCount(userContext, session, context, null);
    }

    @Test(expectedExceptions = {
            InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*argument must not be null.*")
    public void testSubmitLocationCount_NotExistRegularFacilityCount() {
        handler.submitCount(userContext, session, context, null);
    }

    @Test(expectedExceptions = { InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*argument must not be null.*")
    public void testSubmitLocationCount_InvalidSubmittedLocationCountType() {
        RegularFCActivityEntity regularFCActivityEntityMock = mock(RegularFCActivityEntity.class);
        when(session.get(eq(RegularFCActivityEntity.class), any())).thenReturn(regularFCActivityEntityMock);
        handler.submitCount(userContext, session, context, null);
    }

    @Test(expectedExceptions = {
            InvalidInputException.class })
    public void testSubmitLocationCount_InvalidCountLocation() {
        SubmittedLocationCount submitLocationCount = new SubmittedLocationCount();
        submitLocationCount.setInternalLocation(1l);

        RegularFCActivityEntity regularFCActivityEntityMock = mock(RegularFCActivityEntity.class);
        Set<ConsolidatedLocationCountEntity> consolidatedSet = new HashSet<ConsolidatedLocationCountEntity>();
        ConsolidatedLocationCountEntity consolidateEntity = new ConsolidatedLocationCountEntity();
        consolidateEntity.setInternalLocation(2l);
        consolidatedSet.add(consolidateEntity);
        when(session.get(eq(RegularFCActivityEntity.class), any())).thenReturn(regularFCActivityEntityMock);
        when(regularFCActivityEntityMock.getConsolidatedLocationCounts()).thenReturn(consolidatedSet);
        handler.submitCount(userContext, session, context, submitLocationCount);
    }

    @Test(expectedExceptions = { InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*submittedInCount may not be null, submissionStaff may not be null, conductingStaff may not be null.*")
    public void testSubmitLocationCount_NullResubmitReason() {
        SubmittedLocationCount submitLocationCount = new SubmittedLocationCount();
        submitLocationCount.setInternalLocation(1l);


        RegularFCActivityEntity regularFCActivityEntityMock = mock(RegularFCActivityEntity.class);
        Set<ConsolidatedLocationCountEntity> consolidatedSet = new HashSet<ConsolidatedLocationCountEntity>();
        ConsolidatedLocationCountEntity consolidateEntity = new ConsolidatedLocationCountEntity();
        consolidateEntity.setInternalLocation(1l);
        consolidateEntity.setSubmittedInCount(1l);
        consolidatedSet.add(consolidateEntity);
        when(session.get(eq(RegularFCActivityEntity.class), any())).thenReturn(regularFCActivityEntityMock);
        when(regularFCActivityEntityMock.getConsolidatedLocationCounts()).thenReturn(consolidatedSet);
        handler.submitCount(userContext, session, context, submitLocationCount);
    }

}
