package syscon.arbutus.product.services.facilitycountactivity.contract.ejb;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.drools.core.command.assertion.AssertEquals;
import org.hibernate.Session;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.ErrorCodes;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.core.util.DateUtil;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilitycountactivity.contract.dto.*;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.InternalLocationIdentifierType;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.person.contract.dto.StaffPersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * Created by ian.rivers on 13/07/2015.
 */
public class FacilityCountActivityServiceBeanTest {

    FacilityCountActivityServiceBean facilityCountActivityServiceBean;

    @Mock
    UserContext userContext;

    @Mock
    FacilityCountActivityDAO handler;

    @Mock
    PersonService personService;

    @Mock
    FacilityInternalLocationService facilityInternalLocationService;

    @Mock
    FacilityService facilityService;

    @Mock
    Session session;


    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);
        facilityCountActivityServiceBean = new FacilityCountActivityServiceBean();
        facilityCountActivityServiceBean.session = session;
        facilityCountActivityServiceBean.handler = handler;
        facilityCountActivityServiceBean.personService = personService;
        facilityCountActivityServiceBean.facilityInternalLocationService = facilityInternalLocationService;
        facilityCountActivityServiceBean.facilityService = facilityService;
    }


    @Test(dataProvider = "searchHistoricalDataProvider_fail")
    public void searchHistoricalCountsTest_fail(Long facilityId, Date fromDate, Date toDate, ErrorCodes errorCode) {

        FacilityCountSearchCriteria searchCriteria = new FacilityCountSearchCriteria();
        searchCriteria.setFacilityId(facilityId);
        searchCriteria.setFromDate(fromDate);
        searchCriteria.setToDate(toDate);
        try {
            facilityCountActivityServiceBean.searchHistoricalCounts(userContext, searchCriteria);
            fail("Not supposed to reach here");
        } catch (ArbutusRuntimeException e) {
            assertEquals(errorCode, e.getCode());
        }

    }

    @Test(dataProvider = "searchHistoricalDataProvider")
    public void searchHistoricalCountsTest(Long facilityId, Date fromDate, Date toDate, Date expectedFromDate, Date expectedToDate) {
        FacilityCountSearchCriteria searchCriteria = new FacilityCountSearchCriteria();
        searchCriteria.setFacilityId(facilityId);
        searchCriteria.setFromDate(fromDate);
        searchCriteria.setToDate(toDate);
        when(handler.searchHistoricalCounts(any(), any(), any())).thenReturn(new SearchReturnDTO<FacilityCountSearchResult>());
        when(personService.getPersonsByStaffIds(any(), any())).thenReturn(new ArrayList<StaffPersonIdentityType>());
        when(facilityInternalLocationService.getLocationCodes(any(), any())).thenReturn(new ArrayList<InternalLocationIdentifierType>());
        when(facilityService.getFacilityNameMap(any(), any(), any())).thenReturn(new HashMap<Long, String>());
        try {
            facilityCountActivityServiceBean.searchHistoricalCounts(userContext, searchCriteria);
            assertEquals(searchCriteria.getFromDate(), expectedFromDate);
            assertEquals(searchCriteria.getToDate(), expectedToDate);
        } catch (ArbutusRuntimeException e) {
            fail("Not supposed to reach here");
        }
    }

    @DataProvider(name = "searchHistoricalDataProvider")
    public Object[][] searchHistoricalDataProvider() {
        Object[][] result = null;
        Date today = DateUtil.getDateWithoutTime(new Date());

        result = new Object[][] {
                { 1L, null, today, null, today},
                { 1L, today, null, today, today }
        };

        return result;
    }

    @DataProvider(name = "searchHistoricalDataProvider_fail")
    public Object[][] searchHistoricalDataProvider_fail() {
        Object[][] result = null;
        Date today = DateUtil.getDateWithoutTime(new Date());
        Date afterToday = DateUtil.addDays(new Date(), 5);
        Date beforeToday = DateUtil.subtractDays(new Date(), 5);
        Date almostToday = DateUtil.subtractDays(new Date(), 3);

        result = new Object[][] {
                { null, new Date(), new Date(), ErrorCodes.GENERIC_NULL_EMPTY_INPUT },
                { 1L, afterToday, new Date(), ErrorCodes.GENERIC_FROM_DATE_AFTER_TODAY },
                { 1L, beforeToday, afterToday, ErrorCodes.GENERIC_TO_DATE_AFTER_CURRENT_DATE },
                { 1L, almostToday, beforeToday, ErrorCodes.GENERIC_FROM_DATE_BEFORE_TO_DATE }
        };

        return result;
    }



}