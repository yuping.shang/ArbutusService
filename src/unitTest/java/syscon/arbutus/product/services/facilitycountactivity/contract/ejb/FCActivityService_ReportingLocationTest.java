package syscon.arbutus.product.services.facilitycountactivity.contract.ejb;

import java.lang.reflect.Method;
import java.util.*;

import org.hibernate.Query;
import org.hibernate.Session;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.activity.contract.dto.Schedule;
import syscon.arbutus.product.services.activity.contract.dto.SimpleSchedule;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.ErrorCodes;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilitycountactivity.contract.dto.*;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.FacilityInternalLocation;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;


public class FCActivityService_ReportingLocationTest {

    ConfigureFacilityCountHelper configureFacilityCountHelper;

    @Mock
    UserContext userContext;

    @Mock
    ActivityService activityService;

    @Mock
    FacilityService facilityService;

    @Mock
    Session session;

    @Mock
    FacilityCountActivityDAO handler;

    // magic numbers
    Long SCHEDULE_ID = 1L;
    Long FACILITY_ID = 1L;
    Long EXISTING_ACTIVITY_ID = 1L;
    Long EXISTING_FIL_ID = 345L;
    Long EXISTING_PARENT_FIL_ID = 350L;
    Long NEW_LOCATION_FIL_L1 = 320L;
    Long EXISTING_INTLOCREF_ID = 375L; // represents internallocationreferenceentity id
    Long EXISTING_FIL_INTLOC_ID = 675L; // represents FIL id


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        configureFacilityCountHelper = new ConfigureFacilityCountHelper(activityService, facilityService, handler);
    }

    @Test(dataProvider = "provideReportingLocations_sched")
    public void testAddReportingLocation_existingActivity_existingSchedule_noLocations(Long facilityId, Long scheduleId, String countType, Long locationId, Boolean isDefaultCount,
            Boolean isActive) {
        try {
            SetupFCActivity setup = new SetupFCActivity();
            setup.setActivityId(EXISTING_ACTIVITY_ID);
            setup.setInternalLocations(new HashSet<>());

            prepareMock_findSetupFCActivity(setup, scheduleId, EXISTING_ACTIVITY_ID);
            prepareMock_isLocationRelatedToOthers(new ArrayList<>());
            prepareMock_handlerUpdate(setup);

            configureFacilityCountHelper.addReportingLocation(userContext, session, facilityId, scheduleId, countType, locationId, isDefaultCount, isActive);
            verify(activityService, times(1)).updateSchedule(any(), any());
        } catch (ArbutusRuntimeException e) {
            fail("not supposed to reach here");
        }
    }


    @Test(dataProvider = "provideReportingLocations_sched")
    public void testAddReportingLocation_existingActivity_existingSchedule_withLocations(Long facilityId, Long scheduleId, String countType, Long locationId, Boolean isDefaultCount,
            Boolean isActive) {
        try {
            SetupFCActivity setup = prepareMocks_setupFCActivityAndLocations(scheduleId, locationId, EXISTING_FIL_INTLOC_ID);
            prepareMock_handlerUpdate(setup);

            configureFacilityCountHelper.addReportingLocation(userContext, session, facilityId, scheduleId, countType, locationId, isDefaultCount, isActive);

            verify(activityService, times(1)).updateSchedule(any(), any());
        } catch (ArbutusRuntimeException e) {
            fail("not supposed to reach here");
        }
    }




    @DataProvider(name = "provideReportingLocations_sched")
    public Object[][] provideReportingLocations_sched() {
        Object[][] result = null;
        Long facilityId = 1L;
        Long scheduleId = 1L;
        Long locationId = 2L;
        Boolean isDefaultCount = true;
        Boolean isActive = true;
        result = new Object[][] { { facilityId, scheduleId, Constants.COUNT_TYPE_SCH, locationId, isDefaultCount, isActive } };
        return result;
    }

    @Test(dataProvider = "provideReportingLocations_nonsched")
    public void testAddReportingLocation_existingActivity_noSchedule(Long facilityId, Long scheduleId, String countType, Long locationId, Boolean isDefaultCount, Boolean isActive) {
        try {
            Long MAGIC_NUMBER = 12345L;
            SetupFCActivity returnActivity = new SetupFCActivity();
            returnActivity.setFacilityCountId(MAGIC_NUMBER);
            when(handler.create(any(), any(), any(SetupFCActivity.class))).thenReturn(returnActivity);
            prepareMock_updateSchedule(false);

            Long returnNumber = configureFacilityCountHelper.addReportingLocation(userContext, session, facilityId, scheduleId, countType, locationId, isDefaultCount, isActive);

            assertEquals(returnNumber, MAGIC_NUMBER, "Numbers should match");
        } catch (ArbutusRuntimeException e) {
            fail("not supposed to reach here");
        }
    }



    @DataProvider(name = "provideReportingLocations_nonsched")
    public Object[][] provideReportingLocations_nonsched() {
        Object[][] result = null;
        Long facilityId = 1L;
        Long locationId = 1L;
        Boolean isDefaultCount = true;
        Boolean isActive = true;
        result = new Object[][] { { facilityId, null, "EMG", locationId, isDefaultCount, isActive } };
        return result;
    }

    @Test(dataProvider = "provideReportingLocations_fail")
    public void testAddReportingLocation_fail(Long facilityId, Long scheduleId, String countType, Long locationId, Boolean isDefaultCount, Boolean isActive,
            ErrorCodes code) {
        try {

            SetupFCActivity setup = prepareMocks_setupFCActivityAndLocations(scheduleId, locationId, EXISTING_FIL_ID);
            when(handler.update(any(), any(), any(SetupFCActivity.class))).thenReturn(setup);
            when(handler.getActiveInternalLocationIds(session, facilityId)).thenReturn(new ArrayList<Long>());

            configureFacilityCountHelper.addReportingLocation(userContext, session, facilityId, scheduleId, countType, locationId, isDefaultCount, isActive);
            fail("not supposed to reach here");
        } catch (ArbutusRuntimeException e) {
            assertTrue(e.getCode() == code, "error codes must match up");
        }
    }


    @DataProvider(name = "provideReportingLocations_fail")
    public Object[][] provideReportingLocations_fail(Method method) {
        Object[][] result = null;

        Long facilityId = 1L;
        Long scheduleId = 1L;
        Long locationId = 1L;
        Boolean isDefaultCount = true;
        Boolean isActive = true;

        result = new Object[][] {
                { null, scheduleId, Constants.COUNT_TYPE_SCH, locationId, isDefaultCount, isActive, ErrorCodes.GENERIC_NULL_EMPTY_INPUT },
                { facilityId, scheduleId, null, locationId, isDefaultCount, isActive, ErrorCodes.GENERIC_NULL_EMPTY_INPUT },
                { facilityId, scheduleId, Constants.COUNT_TYPE_SCH, null, isDefaultCount, isActive, ErrorCodes.GENERIC_NULL_EMPTY_INPUT },
                { facilityId, null, Constants.COUNT_TYPE_SCH, locationId, isDefaultCount, isActive, ErrorCodes.FCA_NEED_SCH_ID_FOR_REP_LOC },
                { facilityId, null, "UNSCH", EXISTING_FIL_INTLOC_ID, isDefaultCount, isActive, ErrorCodes.FCA_REPORTING_LOCATION_ALREADY_EXISTS },
                { facilityId, null, "UNSCH", EXISTING_PARENT_FIL_ID, isDefaultCount, isActive, ErrorCodes.FCA_CANNOT_ADD_RELATED_REPORTING_LOCATION }
        };
        return result;
    }

    /************************************************************
     *
     * MOCK PREPARATION
     *
     ************************************************************/

    private void prepareMock_handlerUpdate(SetupFCActivity setup) {
        when(handler.update(any(), any(), any(SetupFCActivity.class))).thenReturn(setup);
        prepareMock_updateSchedule(true);
    }



    private SetupFCActivity prepareMocks_setupFCActivityAndLocations(Long scheduleId, Long locationId, Long existingLocationId) {
        SetupFCActivity setup = new SetupFCActivity();
        setup.setActivityId(EXISTING_ACTIVITY_ID);
        Set<InternalLocation> locations = new HashSet<>();
        locations.add(new InternalLocation(EXISTING_INTLOCREF_ID, EXISTING_FIL_INTLOC_ID, true, true));
        setup.setInternalLocations(locations);
        prepareMock_findSetupFCActivity(setup, scheduleId, EXISTING_ACTIVITY_ID);


        List<FacilityInternalLocation> existingLocations = new ArrayList<>();
        FacilityInternalLocation existingLocation = new FacilityInternalLocation();
        existingLocation.setId(existingLocationId);
        existingLocation.setLevel1(EXISTING_PARENT_FIL_ID);
        FacilityInternalLocation newLocation = new FacilityInternalLocation();
        newLocation.setId(locationId);
        newLocation.setLevel1(NEW_LOCATION_FIL_L1);
        existingLocations.add(existingLocation);
        existingLocations.add(newLocation);// because our query is cheeky and gets the FIL of the candidate location too
        prepareMock_isLocationRelatedToOthers(existingLocations);
        return setup;
    }


    private void prepareMock_findSetupFCActivity(SetupFCActivity toReturn, Long scheduleId, Long activityId) {
        List<SetupFCActivity> resultList = new ArrayList<>();

        SearchReturnDTO<SetupFCActivity> returnDTO = new SearchReturnDTO<>();
        resultList.add(toReturn);
        returnDTO.setResultList(resultList);

        if (scheduleId != null) {
            List<ActivityType> activityTypeList = new ArrayList<>();
            ActivityType activityType = new ActivityType();
            activityType.setActivityIdentification(activityId);
            activityTypeList.add(activityType);
            when(activityService.getFromScheduleId(any(), any(), any())).thenReturn(activityTypeList);
        }

        when(handler.search(any(), any(), any(), any(SetupFCActivitySearch.class), any(), any(), any())).thenReturn(returnDTO);
    }

    private void prepareMock_isLocationRelatedToOthers(List<FacilityInternalLocation> listToReturn) {
        Query query = mock(Query.class);
        when(session.createQuery(any())).thenReturn(query);
        when(query.setParameterList(anyString(), anyCollection())).thenReturn(query);
        when(query.setResultTransformer(any())).thenReturn(query);
        when(query.list()).thenReturn(listToReturn);
    }

    private void prepareMock_updateSchedule(Boolean hasSchedule) {
        if (hasSchedule) {
            ActivityType activityType = new ActivityType();
            activityType.setScheduleID(SCHEDULE_ID);
            when(activityService.get(any(), any())).thenReturn(activityType);

            SimpleSchedule schedule = new SimpleSchedule();
            schedule.setScheduleCategory("");
            schedule.setFacilityId(FACILITY_ID);
            schedule.setScheduleIdentification(SCHEDULE_ID);
            when(activityService.getSchedule(any(), any())).thenReturn(schedule);
        } else {
            when(activityService.get(any(), any())).thenReturn(null);
        }

    }
}
