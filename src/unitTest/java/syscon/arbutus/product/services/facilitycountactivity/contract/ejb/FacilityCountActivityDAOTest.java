package syscon.arbutus.product.services.facilitycountactivity.contract.ejb;

import org.hibernate.Session;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.BaseUnitTest;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilitycountactivity.contract.dto.InternalLocationCountResult;
import syscon.arbutus.product.services.facilitycountactivity.contract.dto.SetupFCActivity;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.UsageCategory;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.person.contract.interfaces.PersonServiceLocal;
import syscon.arbutus.product.services.referencedata.contract.interfaces.ReferenceDataService;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;
import syscon.arbutus.product.testutil.Generator;

import javax.ejb.SessionContext;

import static org.mockito.Mockito.spy;

/**
 * Created by yshang on 7/24/15.
 */
public class FacilityCountActivityDAOTest extends BaseUnitTest {

    FacilityCountActivityDAO dao;
    @Mock UserContext userContext;
    @Mock ActivityService activityService;
    @Mock Session session;
    @Mock SessionContext context;
    @Mock FacilityInternalLocationService filService;
    @Mock FacilityService facilityService;
    @Mock PersonServiceLocal personService;
    @Mock SupervisionService supervisionService;
    @Mock ReferenceDataService referenceDataService;

    Generator<SetupFCActivity> generator;

    @BeforeMethod
    public void setUp() {
        generator = new Generator<>();
        MockitoAnnotations.initMocks(this);
        dao =  new FacilityCountActivityDAO();
        dao = spy(dao);
    }



    @Test(expectedExceptions = { InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*input parameters are null or empty*")
    public void testGetActivityLocationCount_NullParameter() {
        InternalLocationCountResult ret = dao.getLocationCount(userContext, session, null, null, UsageCategory.ACT);
    }

    @Test(expectedExceptions = { InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*input parameters are null or empty*")
    public void testGetHousingLocationCount_NullParameter() {
        InternalLocationCountResult ret = dao.getLocationCount(userContext, session, null, null, UsageCategory.HOU);
    }
}
