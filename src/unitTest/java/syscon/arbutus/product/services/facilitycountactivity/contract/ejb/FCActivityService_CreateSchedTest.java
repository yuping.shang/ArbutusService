package syscon.arbutus.product.services.facilitycountactivity.contract.ejb;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.activity.contract.dto.DaysOfTheWeek;
import syscon.arbutus.product.services.activity.contract.dto.Schedule;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.activity.contract.util.DaysOfTheWeekUtil;
import syscon.arbutus.product.services.activity.contract.util.RecurrenceObjectFactory;
import syscon.arbutus.product.services.activity.contract.util.ScheduleRecurrenceUtil;
import syscon.arbutus.product.services.activity.realization.persistence.ScheduleEntity;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.ErrorCodes;
import syscon.arbutus.product.services.activity.contract.dto.SimpleSchedule;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilitycountactivity.contract.dto.FacilityCountScheduleSummary;
import syscon.arbutus.product.services.facilitycountactivity.contract.dto.SetupFCActivity;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.realization.util.BeanHelper;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

/**
 * Created by ian on 8/13/15.
 */
public class FCActivityService_CreateSchedTest {


    ConfigureFacilityCountHelper configureFacilityCountHelper;

    @Mock
    UserContext userContext;

    @Mock
    ActivityService activityService;

    @Mock
    FacilityService facilityService;

    @Mock
    Session session;

    @Mock
    FacilityCountActivityDAO handler;

    @Mock
    FacilityInternalLocationService facilityInternalLocationService;

    RecurrenceObjectFactory recurrenceObjectFactory = new RecurrenceObjectFactory();



    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Criteria c = mock(Criteria.class);
        when(c.list()).thenReturn(new ArrayList());
        when(session.createCriteria(ScheduleEntity.class)).thenReturn(c);
        configureFacilityCountHelper = new ConfigureFacilityCountHelper(activityService, facilityService, handler);
    }



    @Test(dataProvider = "provideNewSchedules")
    public void testCreateFCSchedule(Schedule schedule) {

        ArgumentCaptor<Schedule> argumentCaptor = ArgumentCaptor.forClass(Schedule.class);

        when(activityService.createSchedule(any(), argumentCaptor.capture())).thenReturn(new Schedule());
        when(activityService.create(any(), any(), any(), any())).thenReturn(new ActivityType());
        when(handler.create(any(), any(), any(SetupFCActivity.class))).thenReturn(new SetupFCActivity());
        Optional<DaysOfTheWeek> days = new DaysOfTheWeekUtil().generateDaysOfTheWeekFromRecurrencePattern(schedule.getRecurrencePattern());

        try {
            FacilityCountScheduleSummary facilityCountScheduleSummary = configureFacilityCountHelper.createFacilityCountSchedule(userContext, session,
                    schedule.getFacilityId(), schedule.getScheduleStartTime(), schedule.getEffectiveDate(), schedule.getTerminationDate(),
                    new RecurrenceObjectFactory().generate(new ScheduleRecurrenceUtil().calculateRecurrenceEnum(schedule.getRecurrencePattern()),
                            days.orElseGet(() -> new DaysOfTheWeek()))
                            );
        } catch (ArbutusRuntimeException e) {
            fail("Not supposed to catch an exception");
        }
    }

    @DataProvider(name = "provideNewSchedules")
    public Object[][] provideNewSchedules(Method method) {
        Object[][] result = null;
        // minimum standards for schedule
        if (method.getName().contains("fail")) {
            result = new Object[][] { { generateSchedule(), ErrorCodes.GENERIC_NULL_EMPTY_INPUT } };
        } else {
            result = new Object[][] { { generateSchedule() } };
        }
        return result;
    }



    @Test
    public void testCFCS_fail_FCA_SCHEDULE_START_BEFORE_TODAY() {
        Schedule schedule = generateSchedule();
        schedule.setEffectiveDate(BeanHelper.getPastDate(BeanHelper.createDateWithoutTime(), 1));
        schedule.setTerminationDate(BeanHelper.nextNthDays(schedule.getEffectiveDate(), 1));
        doRunCreateSchedule(schedule, ErrorCodes.FCA_SCHEDULE_START_BEFORE_TODAY);
    }

    public void doRunCreateSchedule(Schedule schedule, ErrorCodes code) {
        try {
            Optional<DaysOfTheWeek> days = new DaysOfTheWeekUtil().generateDaysOfTheWeekFromRecurrencePattern(schedule.getRecurrencePattern());
            FacilityCountScheduleSummary facilityCountScheduleSummary = configureFacilityCountHelper.createFacilityCountSchedule(userContext, session,
                    schedule.getFacilityId(), schedule.getScheduleStartTime(), schedule.getEffectiveDate(), schedule.getTerminationDate(),
                    new RecurrenceObjectFactory().generate(new ScheduleRecurrenceUtil().calculateRecurrenceEnum(schedule.getRecurrencePattern()),
                            days.orElseGet(() -> new DaysOfTheWeek())));
            fail("not supposed to get here");
        } catch (ArbutusRuntimeException e) {
            assertEquals(e.getCode(), code, "error codes must match up");
        }
    }

    private SimpleSchedule generateFacilityCountSchedule(String startTime, Date startDate) {
        SimpleSchedule schedule = new SimpleSchedule();
        schedule.setFacilityId(1L);
        schedule.setScheduleCategory("");
        schedule.setScheduleStartTime(startTime);
        schedule.setEffectiveDate(startDate);
        return schedule;
    }


    private Schedule generateSchedule() {
        Schedule schedule = new Schedule();
        schedule.setFacilityId(1L);
        schedule.setScheduleCategory("");
        schedule.setEffectiveDate(BeanHelper.createDateWithoutTime());
        schedule.setScheduleStartTime("08:00");
        return schedule;
    }

}
