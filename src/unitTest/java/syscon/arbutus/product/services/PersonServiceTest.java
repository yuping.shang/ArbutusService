package syscon.arbutus.product.services;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseUnitTest;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.exception.DataExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.person.contract.dto.LanguageType;
import syscon.arbutus.product.services.person.contract.dto.personidentity.OffNumConfigType;
import syscon.arbutus.product.services.person.contract.ejb.PersonServiceBean;
import syscon.arbutus.product.services.person.realization.persistence.person.LanguageEntity;
import syscon.arbutus.product.services.person.realization.persistence.person.PersonEntity;
import syscon.arbutus.product.services.person.realization.persistence.personidentity.OffNumConfigEntity;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.fail;

/**
 * Created and modified by bseshadri on 29/07/2015.
 */
public class PersonServiceTest extends BaseUnitTest {
    private PersonServiceBean personServiceBean = new PersonServiceBean();

    @Mock
    private UserContext userContext;

    @Mock
    private Query query;

    @Rule
    private ExpectedException expectException = ExpectedException.none();

    @BeforeClass
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.injector.assign(Session.class, sessionMock);
        this.injector.inject(personServiceBean);
    }

    @DataProvider(name = "getOffNumConfigFormat")
    public Object[][] getOffNumConfigFormat(Method method) {
        return new Object[][]{{null}, {"Format"}};
    }

    @Test(dataProvider = "getOffNumConfigFormat")
    public void testGetOffNumConfigNullFormat(String format) {

        when(sessionMock.createQuery(anyString())).thenReturn(query);
        OffNumConfigEntity offNumConfigEntityForNullFormat = new OffNumConfigEntity(null, false);
        when(query.list()).thenReturn(Arrays.asList(offNumConfigEntityForNullFormat));
        OffNumConfigEntity offNumConfigEntityForNonNullFormat = new OffNumConfigEntity("Format", true);
        when(sessionMock.get(eq(OffNumConfigEntity.class), anyInt())).thenReturn(offNumConfigEntityForNonNullFormat);

        OffNumConfigType searchType = personServiceBean.getOffNumConfig(userContext, format);

        if (format == null) {
            Assert.assertFalse(searchType.isGeneration());
            Assert.assertNull(searchType.getFormat());
        } else {
            Assert.assertTrue(searchType.isGeneration());
            Assert.assertEquals(offNumConfigEntityForNonNullFormat.getFormat(), "Format");
        }
    }

    @Test(expectedExceptions = InvalidInputException.class)
    public void testNullCreateCautionType() {
        personServiceBean.createCautionType(userContext, null);
    }

    @Test(expectedExceptions = InvalidInputException.class)
    public void testNullUpdateCautionType() {
        personServiceBean.updateCautionType(userContext, null);
    }

    @Test(expectedExceptions = InvalidInputException.class)
    public void testNullgetCautionTypeStamp() {
        personServiceBean.getCautionTypeStamp(userContext, null);
    }

    @Test
    public void testgetStaffNiemNullCaution() {
        String str = personServiceBean.getStaffNiem(userContext, null);
        Assert.assertTrue(str.contains(ReturnCode.CInvalidInput2002.returnCode().toString()));
    }

    /*
    * Language Unit Test
    * */

    @Test(expectedExceptions = InvalidInputException.class, expectedExceptionsMessageRegExp = ".*PersonId is null.")
    public void testSwapLanguage_InvalidInputException_PersonId_Is_Null() {
        Long personId = null;
        LanguageType languageType = getBasicLanguageType(1L,Boolean.TRUE, personId);
        personServiceBean.addLanguage(userContext, languageType);
        fail("Expected InvalidInputException when person id is null");
    }

    @Test(expectedExceptions = InvalidInputException.class)
    public void testDeleteLanguage_InvalidInputException_Last_Language() {
        Long personId = 1L;
        Long languageId = 1L;
        LanguageType languageType = getBasicLanguageType(languageId, Boolean.TRUE, personId);

        PersonEntity personEntity = mock(PersonEntity.class);
        LanguageEntity lang1 = getBasicLanguageEntity(languageId, Boolean.TRUE, personId);
        LanguageEntity lang2 = getBasicLanguageEntity(2L, Boolean.FALSE, personId);

        Set<LanguageEntity> languageSet = new HashSet<>();
        languageSet.add(lang1);
        languageSet.add(lang2);

        when(sessionMock.get(eq(PersonEntity.class), anyLong())).thenReturn(personEntity);
        when(personEntity.getLanguages()).thenReturn(languageSet);

        personServiceBean.deleteLanguage(userContext, languageType);
        fail("Expected InvalidInputException when deleting a primary language that is not the only language");
    }

    @Test(expectedExceptions = DataExistException.class)
    public void testValidateLanguage_DataExistException_Multiple_Primary_Language() {
        Long personId = 1L;
        // new primary language
        LanguageType languageType = getBasicLanguageType(1L, Boolean.TRUE, personId);

        PersonEntity personEntity = mock(PersonEntity.class);
        LanguageEntity lang1 = getBasicLanguageEntity(2L, Boolean.TRUE, personId);
        LanguageEntity lang2 = getBasicLanguageEntity(3L, Boolean.FALSE, personId);

        Set<LanguageEntity> languageSet = new HashSet<>();
        languageSet.add(lang1);
        languageSet.add(lang2);

        when(sessionMock.get(eq(PersonEntity.class), anyLong())).thenReturn(personEntity);
        when(personEntity.getLanguages()).thenReturn(languageSet);

        personServiceBean.addLanguage(userContext, languageType);
        fail("Expected DataExistException when adding more than one primary language");
    }

    @Test(expectedExceptions = DataExistException.class)
    public void testValidateLanguage_DataExistException_Duplicate_Language() {
        Long personId = 1L;
        // current language
        LanguageType languageType = getBasicLanguageType(2L, Boolean.FALSE, personId);

        PersonEntity personEntity = mock(PersonEntity.class);
        LanguageEntity lang1 = getBasicLanguageEntity(1L, Boolean.TRUE, personId);
        LanguageEntity lang2 = getBasicLanguageEntity(2L, Boolean.FALSE, personId);

        Set<LanguageEntity> languageSet = new HashSet<>();
        languageSet.add(lang1);
        languageSet.add(lang2);

        when(sessionMock.get(eq(PersonEntity.class), anyLong())).thenReturn(personEntity);
        when(personEntity.getLanguages()).thenReturn(languageSet);

        personServiceBean.addLanguage(userContext, languageType);
        fail("Expected DataExistException when adding duplicate language");
    }

    @Test(expectedExceptions = InvalidInputException.class)
    public void testCreatePersonType_with_null_data() {
        personServiceBean.createPersonType(userContext, null, null, null, null);
        fail("Expected InvalidInputException when creating personType with null data");
    }

    private LanguageType getBasicLanguageType(Long languageId, Boolean isPrimary, Long personId) {
        LanguageType languageType = new LanguageType();
        languageType.setPrimary(isPrimary);
        languageType.setLanguageIdentification(languageId);
        languageType.setPersonId(personId);
        return languageType;
    }

    private LanguageEntity getBasicLanguageEntity(Long languageId, Boolean isPrimary, Long personId) {
        LanguageEntity languageEntity = new LanguageEntity();
        languageEntity.setPrimary(isPrimary);
        languageEntity.setLanguageId(languageId);
        languageEntity.setPersonId(personId);
        return languageEntity;
    }
}
