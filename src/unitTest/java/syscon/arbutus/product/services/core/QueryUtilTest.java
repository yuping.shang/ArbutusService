package syscon.arbutus.product.services.core;

import java.util.Collection;
import java.util.HashSet;

import org.hibernate.criterion.Disjunction;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.util.QueryUtil;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.spy;

/**
 * Created by ian on 9/4/15.
 */
public class QueryUtilTest {

    QueryUtil queryUtil;

    @Mock
    Disjunction disjunction;

    @BeforeMethod
    public void setUp() {
        queryUtil = new QueryUtil();
        queryUtil = spy(queryUtil);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void disjunctIdSet() {
        Collection<Long> collection = null;
        disjunction = queryUtil.disjunctIdSet("propertyName", collection);
        assertNull(disjunction);

        collection = new HashSet<>();
        disjunction = queryUtil.disjunctIdSet("propertyName", collection);
        assertNull(disjunction);

        collection = new HashSet<>();
        collection.add(0L);
        disjunction = queryUtil.disjunctIdSet("propertyName", collection);
        assertNotNull(disjunction);

        for (int i = 0; i < 5; i++) {
            collection.add(Long.valueOf(i));
        }
        disjunction = queryUtil.disjunctIdSet("propertyName", collection);
        assertNotNull(disjunction);
    }
}
