package syscon.arbutus.product.services.supervision.contract.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseUnitTest;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionConfigVariablesType;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.realization.persistence.ConfigVarsEntity;
import syscon.arbutus.product.services.supervision.realization.persistence.FacilityOfInterestEntity;
import syscon.arbutus.product.services.supervision.realization.persistence.ImprisonmentStatusEntity;
import syscon.arbutus.product.services.supervision.realization.persistence.SupervisionEntity;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.testng.Assert.fail;

/**
 * Created by byu on 9/4/15.
 */
public class SupervisionServiceBeanTest extends BaseUnitTest {

    private SupervisionServiceBean supervisionServiceBean = new SupervisionServiceBean();

    @Mock
    private UserContext userContext;

    @BeforeClass
    public void setup() throws Exception {
        //MockitoAnnotations.initMocks(this);
        //this.injector.assign(Session.class, sessionMock);
        //this.injector.inject(supervisionServiceBean);
    }

    @BeforeMethod
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        sessionMock = mock(Session.class);
        this.injector.assign(Session.class, sessionMock);
        this.injector.inject(supervisionServiceBean);
    }

    @Test(expectedExceptions = { InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*argument must not be null.*")
    public void testCreateConfigVars_NullParameter() {
        SupervisionConfigVariablesType ret = supervisionServiceBean.createConfigVars(userContext, null);
        fail("Should throw the InvalidInputException!");
    }

    @Test(expectedExceptions = { InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*")
    public void testCreateConfigVars_InvalidParameter() {

        SupervisionConfigVariablesType dto = new SupervisionConfigVariablesType();
        dto.setSupervisionDisplayIDFormat("99-99999");
        SupervisionConfigVariablesType ret = supervisionServiceBean.createConfigVars(userContext, dto);
        fail("Should throw the InvalidInputException!");
    }

    @Test
    public void testCreateConfigVars() {
        String format = "99-99999";
        SupervisionConfigVariablesType dto = new SupervisionConfigVariablesType();
        dto.setSupervisionDisplayIDFormat(format);
        dto.setSupervisionDisplayIDGenerationFlag(true);

        SupervisionConfigVariablesType ret = supervisionServiceBean.createConfigVars(userContext, dto);
        verify(sessionMock).save(any());
        assert (ret.getSupervisionDisplayIDFormat().equals(format));

    }

    @Test
    public void testUpdateConfigVars() {
        String format = "99-99998";
        SupervisionConfigVariablesType dto = new SupervisionConfigVariablesType();
        dto.setSupervisionDisplayIDFormat(format);
        dto.setSupervisionDisplayIDGenerationFlag(true);
        ConfigVarsEntity entity = new ConfigVarsEntity();
        entity.setSupervisionDisplayIDFormat(format);
        when(sessionMock.get(eq(ConfigVarsEntity.class), anyLong())).thenReturn(entity);

        SupervisionConfigVariablesType ret = supervisionServiceBean.updateConfigVars(userContext, dto);
        verify(sessionMock).update(any(ConfigVarsEntity.class));
        assert (ret.getSupervisionDisplayIDFormat().equals(format));
    }

    @Test(expectedExceptions = { InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*SupervisionType.*")
    public void testUpdate_NullParameter() {
        SupervisionType ret = supervisionServiceBean.update(userContext, null);
        fail("Should throw the InvalidInputException!");
    }

    @Test(expectedExceptions = { InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*SupervisionType.*")
    public void testUpdate_InvalidParameter() {
        SupervisionType dto = new SupervisionType();
        dto.setFacilityId(1l);
        SupervisionType ret = supervisionServiceBean.update(userContext, dto);
        fail("Should throw the InvalidInputException!");
    }

    @Test
    public void testUpdate() {
        String displayId = "15-999999";
        SupervisionType dto = new SupervisionType();
        dto.setSupervisionIdentification(1l);
        dto.setFacilityId(1l);
        dto.setPersonIdentityId(1l);
        dto.setSupervisionDisplayID(displayId);
        dto.setSupervisionStartDate(new Date());

        SupervisionEntity entity = new SupervisionEntity();
        when(sessionMock.get(eq(SupervisionEntity.class), anyLong())).thenReturn(entity);

        Criteria criteria = mock(Criteria.class);
        when(sessionMock.createCriteria(FacilityOfInterestEntity.class)).thenReturn(criteria);

        SupervisionType ret = supervisionServiceBean.update(userContext, dto);
        verify(sessionMock).update(any(SupervisionEntity.class));

        assert (ret.getSupervisionDisplayID().equals(displayId));
    }

    @Test(expectedExceptions = { InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*supervisionId is null.*")
    public void testClose_NullParameter() {
        SupervisionType ret = supervisionServiceBean.close(userContext, null, null);
    }

    @Test(expectedExceptions = { DataNotExistException.class }, expectedExceptionsMessageRegExp = ".*Could not find the supervision by SupervisionId.*")
    public void testClose_InvalidParameter() {
        Long supervisionId = 1l;
        Date endDate = new Date();
        SupervisionType ret = supervisionServiceBean.close(userContext, supervisionId, endDate);
        fail("Should throw the DataNotExistException!");
    }

    @Test(expectedExceptions = { InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*is closed already.*")
    public void testClose_InvalidParameter_SupervisionAlreadyClosed() {
        Long supervisionId = 1l;
        Date endDate = new Date();

        SupervisionEntity entity = new SupervisionEntity();
        entity.setSupervisionStartDate(BeanHelper.getPastDate(endDate, 100));
        entity.setSupervisionEndDate(BeanHelper.getPastDate(endDate, 5));
        when(sessionMock.get(eq(SupervisionEntity.class), anyLong())).thenReturn(entity);

        SupervisionType ret = supervisionServiceBean.close(userContext, supervisionId, endDate);

        fail("Should throw the InvalidInputException!");
    }

    @Test(expectedExceptions = { InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*Supervision endDate is before the Supervision Start Date.*")
    public void testClose_InvalidParameter_EndDateBeforeStartDate() {
        Long supervisionId = 1l;
        Date startDate = BeanHelper.getPastDate(new Date(), 10);
        Date endDate = BeanHelper.getPastDate(new Date(), 12);

        SupervisionEntity entity = new SupervisionEntity();
        entity.setSupervisionStartDate(startDate);
        when(sessionMock.get(eq(SupervisionEntity.class), anyLong())).thenReturn(entity);

        SupervisionType ret = supervisionServiceBean.close(userContext, supervisionId, endDate);

        fail("Should throw the InvalidInputException!");
    }

    @Test(expectedExceptions = { InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*Supervision End Date must be current date or past date.*")
    public void testClose_InvalidParameter_EndDateCannotBeFuture() {
        Long supervisionId = 1l;
        Date startDate = BeanHelper.getPastDate(new Date(), 10);
        Date endDate = BeanHelper.nextNthDays(new Date(), 5);

        SupervisionEntity entity = new SupervisionEntity();
        entity.setSupervisionStartDate(startDate);
        when(sessionMock.get(eq(SupervisionEntity.class), anyLong())).thenReturn(entity);

        SupervisionType ret = supervisionServiceBean.close(userContext, supervisionId, endDate);

        fail("Should throw the InvalidInputException!");
    }

    @Test(expectedExceptions = { InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*Supervision End Date cannot be a past date from the Start Date.*")
    public void testClose_InvalidParameter_EndDateCannotBeforeStartDateOfImprisonmentStatus() {
        Long supervisionId = 1l;
        Date startDate = BeanHelper.getPastDate(new Date(), 100);
        Date endDate = BeanHelper.getPastDate(new Date(), 15);
        Date impStartDate = BeanHelper.getPastDate(new Date(), 10);

        SupervisionEntity entity = new SupervisionEntity();
        entity.setSupervisionStartDate(startDate);

        ImprisonmentStatusEntity imprisonmentStatusEntity = new ImprisonmentStatusEntity();
        imprisonmentStatusEntity.setImprisonmentStatusStartDate(impStartDate);
        List<ImprisonmentStatusEntity> lstImprEntity = new ArrayList<ImprisonmentStatusEntity>();
        lstImprEntity.add(imprisonmentStatusEntity);
        entity.setImprisonmentStatus(lstImprEntity);
        when(sessionMock.get(eq(SupervisionEntity.class), anyLong())).thenReturn(entity);
        SupervisionType ret = supervisionServiceBean.close(userContext, supervisionId, endDate);

        fail("Should throw the InvalidInputException!");
    }

    @Test
    public void testClose() throws Exception {
        Long supervisionId = 1l;
        Date startDate = BeanHelper.getPastDate(new Date(), 50);
        Date endDate = BeanHelper.getPastDate(new Date(), 45);

        SupervisionEntity entity = new SupervisionEntity();
        entity.setSupervisionIdentification(supervisionId);
        entity.setSupervisionStartDate(startDate);
        when(sessionMock.get(eq(SupervisionEntity.class), anyLong())).thenReturn(entity);

        Criteria criteria = mock(Criteria.class);
        when(sessionMock.createCriteria(FacilityOfInterestEntity.class)).thenReturn(criteria);

        SupervisionType ret = supervisionServiceBean.close(userContext, supervisionId, endDate);
        verify(sessionMock).update(any(SupervisionEntity.class));

        assert (ret.getSupervisionIdentification().equals(supervisionId));
    }

    @Test(expectedExceptions = { InvalidInputException.class }, expectedExceptionsMessageRegExp = ".*is opened already..*")
    public void testReOpen_InvalidParameter_SupervisionAlreadyOpen() {
        Long supervisionId = 1l;
        Date startDate = BeanHelper.getPastDate(new Date(), 10);

        SupervisionEntity entity = new SupervisionEntity();
        entity.setSupervisionStartDate(startDate);
        when(sessionMock.get(eq(SupervisionEntity.class), anyLong())).thenReturn(entity);

        SupervisionType ret = supervisionServiceBean.reopen(userContext, supervisionId);

        fail("Should throw the InvalidInputException!");
    }

    @Test
    public void testReOpen() {
        Long supervisionId = 1l;
        Date startDate = BeanHelper.getPastDate(new Date(), 50);
        Date endDate = BeanHelper.getPastDate(new Date(), 45);

        SupervisionEntity entity = new SupervisionEntity();
        entity.setSupervisionIdentification(supervisionId);
        entity.setSupervisionStartDate(startDate);
        when(sessionMock.get(eq(SupervisionEntity.class), anyLong())).thenReturn(entity);

        SupervisionType ret = supervisionServiceBean.reopen(userContext, supervisionId, endDate);
        verify(sessionMock).update(any(SupervisionEntity.class));

        assert (ret.getSupervisionIdentification().equals(supervisionId));
    }


}
