package syscon.arbutus.product.testutil;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by ian.rivers on 29/06/2015.
 */
public class Generator<T> {

    /**
     * assuming we are dealing with noARG constructors!
     * @param tClass
     * @return instance of tClass
     */
    public T generate(Class<T> tClass) {
        try {
            return tClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * just populating string properties for now, because its easy.
     * @param tClass
     * @return instance of tClass
     */
    public T generateWithDefaults(Class<T> tClass) {
        try {
            T instance = generate(tClass);
            final PropertyDescriptor properties[] = Introspector.getBeanInfo(tClass).getPropertyDescriptors();

            for (final PropertyDescriptor property : properties) {
                if (property.getPropertyType() == String.class) {
//                   Method m =
                   property.getWriteMethod().invoke(instance, "test data");
                }
                else if (property.getPropertyType() == Long.class)
                    property.getWriteMethod().invoke(instance, new Random().nextLong());
                else if (property.getPropertyType() == Date.class)
                    property.getWriteMethod().invoke(instance, new Date());
            }
            return instance;
        } catch (IllegalAccessException | IntrospectionException | InvocationTargetException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * for numEntries, call generateWithDefaults(tClass)
     * @param numEntries
     * @param tClass
     * @return
     */
    public List<T> generateItems(int numEntries, Class<T> tClass) {
        List<T> entities = new ArrayList<>();

        for (int i = 0; i < numEntries; i++)
            entities.add(generate(tClass));

        return entities;
    }

    public List<T> generateUniqueItems(int numEntries, Class<T> tClass) {
        List<T> entities = new ArrayList<>();

        for (int i = 0; i < numEntries; i++)
            entities.add(generateWithDefaults(tClass));

        return entities;
    }

}
