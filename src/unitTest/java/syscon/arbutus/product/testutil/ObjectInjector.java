package syscon.arbutus.product.testutil;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceContext;
import java.lang.reflect.Field;

/**
 * This class provides a way to inject Objects
 */
public class ObjectInjector {

    private final Object bean;

    public ObjectInjector(Object bean) {
        this.bean = bean;
    }

    public <T> void injectField(final Class<? extends T> toInjectClass, final T toInject) {
        for (final Field field : bean.getClass().getDeclaredFields()) {
            if (field.getType().equals(toInjectClass)) {
                try {
                    setFieldValue(field, toInject);
                } catch (Throwable ex) {
                    throw new RuntimeException(ex.getMessage(), ex);
                }
                break;
            }
        }
    }

    public <T> void injectField(final Class<? extends T> toInjectClass, final T toInject, String name) {
        for (final Field field : bean.getClass().getDeclaredFields()) {
            if (field.getType().equals(toInjectClass)) {
                final String annotationName = getAnnotationName(field);
                if (!annotationName.isEmpty() && annotationName.equals(name)) {
                    try {
                        setFieldValue(field, toInject);
                    } catch (Throwable ex) {
                        throw new RuntimeException(ex.getMessage(), ex);
                    }
                    break;
                }
            }
        }
    }

    private String getAnnotationName(final Field field) {
        String name = "";
        if (field.isAnnotationPresent(EJB.class)) {
            name = field.getAnnotation(EJB.class).mappedName();
        } else if (field.isAnnotationPresent(Resource.class)) {
            name = field.getAnnotation(Resource.class).mappedName();
        } else if (field.isAnnotationPresent(PersistenceContext.class)) {
            name = field.getAnnotation(PersistenceContext.class).unitName();
        } else if (field.isAnnotationPresent(Inject.class) && field.isAnnotationPresent(Named.class)) {
            name = field.getAnnotation(Named.class).value();
        }
        return name;
    }

    public void injectField(final Object toInject) throws Exception {
        this.injectField(toInject.getClass(), toInject);
    }

    private void setFieldValue(final Field field, final Object instanceToInject) throws IllegalAccessException {
        if (!field.isAccessible()) {
            field.setAccessible(true);
        }
        field.set(bean, instanceToInject);
    }

}
