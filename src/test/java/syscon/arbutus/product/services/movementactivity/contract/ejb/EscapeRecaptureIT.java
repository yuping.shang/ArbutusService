package syscon.arbutus.product.services.movementactivity.contract.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.approval.contract.dto.*;
import syscon.arbutus.product.services.approval.contract.interfaces.ApprovalService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.core.exception.ArbutusOptimisticLockException;
import syscon.arbutus.product.services.movement.contract.dto.EscapeRecapture;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;

/**
 * Created by jliang on 12/1/15.
 */
@ModuleConfig
public class EscapeRecaptureIT extends BaseIT {

    private static Logger log = Logger.getLogger(EscapeRecaptureIT.class);

    private MovementService movementService;

    @AfterMethod
    public void afterMethod() {
    }

    @BeforeClass
    public void beforeClass() {

        log.info("beforeClass Begin - JNDI lookup");
        movementService = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
        uc = super.initUserContext();

        SecurityClient client;
        try {
            client = SecurityClientFactory.getSecurityClient();
            client.setSimple("devtest", "devtest"); // nice regular service user
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }

        init();
    }

    @AfterClass
    public void afterTest() {

    }

    public void init() {

    }

    @Test (enabled = true)
    public void addEscape() {

        EscapeRecapture escapeRecapture = new EscapeRecapture();
        escapeRecapture.setSupervisionId(2L);
        escapeRecapture.setEscapeDate(new Date());
        escapeRecapture.setEscapeTime(new Date());
        escapeRecapture.setEscapeReason("ESCP");

        movementService.addEscape(uc, escapeRecapture);
    }


    @Test (enabled = true)
    public void getEscapesBySupervisionId() {

        EscapeRecapture escapeRecapture = new EscapeRecapture();
        escapeRecapture.setSupervisionId(2L);
        escapeRecapture.setEscapeDate(new Date());
        escapeRecapture.setEscapeReason("ESCP");

        List<EscapeRecapture> escapeRecaptureList = movementService.getEscapesBySupervisionId(uc, 2L);
        assert(escapeRecaptureList.size() > 0);
    }

    @Test (enabled = true)
    public void updateEscape() {

        EscapeRecapture escapeRecapture = new EscapeRecapture();
        escapeRecapture.setSupervisionId(2L);
        escapeRecapture.setEscapeDate(new Date());
        escapeRecapture.setEscapeTime(new Date());
        escapeRecapture.setEscapeReason("ESCP");

        Long escapeId = movementService.addEscape(uc, escapeRecapture);
        escapeRecapture = movementService.getEscapeDetailsById(uc, escapeId);
        movementService.updateEscape(uc, escapeRecapture);

    }

    @Test (enabled = true)
    public void isOffenderEscaped() {

        EscapeRecapture escapeRecapture = new EscapeRecapture();
        escapeRecapture.setSupervisionId(2L);
        escapeRecapture.setEscapeDate(new Date());
        escapeRecapture.setEscapeTime(new Date());
        escapeRecapture.setEscapeReason("ESCP");

        Long escapeId = movementService.addEscape(uc, escapeRecapture);
        boolean isOffenderEscaped = movementService.isOffenderEscaped(uc, 2L);
        assert(isOffenderEscaped);
    }
}