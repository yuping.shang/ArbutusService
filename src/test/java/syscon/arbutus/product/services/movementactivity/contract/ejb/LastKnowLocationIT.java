package syscon.arbutus.product.services.movementactivity.contract.ejb;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.movement.contract.dto.LastKnowLocationGridEntryType;
import syscon.arbutus.product.services.movement.contract.dto.LastKnowLocationSearchType;
import syscon.arbutus.product.services.movement.contract.dto.LastKnowLocationType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;

/**
 * Created by dev on 5/21/14.
 */
public class LastKnowLocationIT extends BaseIT {

    private static Long facilityId = 1L;
    private MovementService service;

    @BeforeClass
    public void beforeClass() {
        service = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
        uc = super.initUserContext();
    }

    @Test(enabled = false)
    public void create() {
        List<LastKnowLocationType> locations = new ArrayList<LastKnowLocationType>();
        locations.add(new LastKnowLocationType("14-6438", 1L, 112L));
        List<LastKnowLocationGridEntryType> result = service.createLastKnowLocation(uc, locations);
        Assert.assertFalse(result.isEmpty());
        Assert.assertTrue(result.size() == 1L);

    }

    @Test(enabled = false)
    public void search() {

        LastKnowLocationSearchType search = new LastKnowLocationSearchType();
        search.setFacilityId(1L);
        List<LastKnowLocationType> result = service.searchLastKnowLocation(uc, search);
        Assert.assertTrue(result.size() == 3);
        search.setSupervisionId(2L);
        result = service.searchLastKnowLocation(uc, search);
        Assert.assertTrue(result.size() == 2);
        search.setActive(true);
        result = service.searchLastKnowLocation(uc, search);
        Assert.assertEquals(result.size(), 1);
        System.out.println(result.get(0).toString());

    }

}
