package syscon.arbutus.product.services.movementactivity.contract.ejb;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.movement.contract.dto.EscapeRecapture;
import syscon.arbutus.product.services.movement.contract.dto.MovementCategoryConfiguration;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;

/**
 * Created by jliang on 12/22/15.
 */
@ModuleConfig()
public class CategoryConfigurationIT extends BaseIT {

    private static Logger log = Logger.getLogger(CategoryConfigurationIT.class);

    private MovementService movementService;

    @AfterMethod
    public void afterMethod() {
    }

    @BeforeClass
    public void beforeClass() {

        log.info("beforeClass Begin - JNDI lookup");
        movementService = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
        uc = super.initUserContext();

        SecurityClient client;
        try {
            client = SecurityClientFactory.getSecurityClient();
            client.setSimple("devtest", "devtest"); // nice regular service user
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }

        init();
    }

    @AfterClass
    public void afterTest() {

    }

    public void init() {

    }

    @Test(enabled = true)
    public void createMovementCategoryConfiguration() {

        MovementCategoryConfiguration configuration = new MovementCategoryConfiguration();
        configuration.setMovementType("A");
        configuration.setMovementReason("B");
        configuration.setMovementCategory("C");

        movementService.createMovementCategoryConfiguration(uc, configuration);
    }


    @Test(enabled = true)
    public void getMovementCategoryConfiguration() {

        MovementCategoryConfiguration configuration = new MovementCategoryConfiguration();
        configuration.setMovementType("B");
        configuration.setMovementReason("C");
        configuration.setMovementCategory("D");

        movementService.createMovementCategoryConfiguration(uc, configuration);
        configuration = movementService.getMovementCategoryConfiguration(uc, "B", "C");
        assert(configuration != null);
    }

    @Test(enabled = true)
    public void updateMovementCategoryConfiguration() {

        MovementCategoryConfiguration configuration = new MovementCategoryConfiguration();
        configuration.setMovementType("D");
        configuration.setMovementReason("E");
        configuration.setMovementCategory("F");
        movementService.deleteMovementCategoryConfiguration(uc, configuration);
        movementService.createMovementCategoryConfiguration(uc, configuration);
        configuration = movementService.getMovementCategoryConfiguration(uc, "D", "E");
        configuration.setMovementCategory("F1");

        movementService.updateMovementCategoryConfiguration(uc, configuration);
        configuration = movementService.getMovementCategoryConfiguration(uc, "D", "E");
        assert(configuration != null);
        assert(configuration.getMovementCategory().equals("F1"));
    }

    @Test(enabled = true)
    public void getAllCategoryConfigurations() {

        MovementCategoryConfiguration configuration = new MovementCategoryConfiguration();
        configuration.setMovementType("D");
        configuration.setMovementReason("E");
        configuration.setMovementCategory("F");
        movementService.deleteMovementCategoryConfiguration(uc, configuration);
        movementService.createMovementCategoryConfiguration(uc, configuration);
        configuration = movementService.getMovementCategoryConfiguration(uc, "D", "E");
        configuration.setMovementCategory("F1");
        configuration.setActive(true);
        movementService.updateMovementCategoryConfiguration(uc, configuration);
        configuration = movementService.getMovementCategoryConfiguration(uc, "D", "E");
        assert(configuration != null);
        assert(configuration.getMovementCategory().equals("F1"));

        List<MovementCategoryConfiguration> configurationList = movementService.getAllCategoryConfigurations(uc);
        assert(configurationList != null && configurationList.size() > 0);
    }
}
