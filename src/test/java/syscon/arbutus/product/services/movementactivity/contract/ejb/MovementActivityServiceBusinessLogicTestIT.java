package syscon.arbutus.product.services.movementactivity.contract.ejb;

import javax.naming.NamingException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.contract.dto.ActivityCategory;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.*;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.location.contract.interfaces.LocationService;
import syscon.arbutus.product.services.movement.contract.dto.*;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;

import static org.testng.Assert.*;

@ModuleConfig()
public class MovementActivityServiceBusinessLogicTestIT extends BaseIT {

    /**
     * Movement Reference Sets
     */
    public final static String MOVEMENT_CATEGORY = "MovementCategory";
    public final static String MOVEMENT_TYPE = "MovementType";
    public final static String MOVEMENT_DIRECTION = "MovementDirection";
    public final static String MOVEMENT_STATUS = "MovementStatus";
    public final static String MOVEMENT_REASON = "MovementReason";
    public final static String MOVEMENT_OUTCOME = "MovementOutcome";
    public final static String TO_PROVINCE_STATE = "StateProvince";    //Refer to Person Service
    final static Long success = 1L;
    MovementService maSer;
    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").serializeNulls().create();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    MovementActivityType ma = new MovementActivityType();
    ExternalMovementActivityType ema = new ExternalMovementActivityType();
    InternalMovementActivityType ima = new InternalMovementActivityType();
    ExternalMovementActivityType emaRet = null;
    InternalMovementActivityType imaRet = null;
    Long inId, exId;
    Set<String> refSetMaps = new LinkedHashSet<String>();
    String[] REF_SET = { MOVEMENT_CATEGORY, MOVEMENT_TYPE, MOVEMENT_REASON, MOVEMENT_OUTCOME, MOVEMENT_DIRECTION, MOVEMENT_STATUS, TO_PROVINCE_STATE };
    Long activityId, activityId1, activityId2, activityId3, activityId4, activityId5;
    Long[] activityIDs = new Long[] { 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L };
    Long supervisionId, supervisionId2;
    Long facilityId;
    Long filRootId;
    List<Long> filChildrenAndRootIDs;
    Long fromLocationAssociation = 10000L;
    Long fromFacilityAssociation = 20000L;
    Long fromOrganizationAssociation = 30000L;
    Long toLocationAssociation = 40000L;
    Long toFacilityAssociation = 50000L;
    Long toOrganizationAssociation = 60000L;
    Long arrestOrganizationAssociation = 70000L;
    Long escortOrganizationAssociation = 80000L;
    String toProvinceState = "BC";
    String toCity = "Richmond";
    String fromCity = "Vancouver";
    Date applicationDate, fromApplicationDate, toApplicationDate;
    Date reportingDate, fromReportingDate, toReportingDate;
    Long fromFacilityInternalLocationAssociation = 100000L;
    Long toFacilityInternalLocationAssociation = 110000L;
    private Logger log = LoggerFactory.getLogger(MovementActivityServiceBusinessLogicTestIT.class);
    private FacilityInternalLocationService filService;
    private ActivityService actService;
    private LocationService locService;

    /**
     * Converts MovementActivity.MovementCategory to CodeType.
     *
     * @param movementCategory the movement category
     * @return the code type for the movement category
     */
    public static String toMovementCategoryCodeType(String movementCategory) {
        return movementCategory;
    }

    /**
     * Converts movement type from String to CodeType.
     *
     * @param movementType the movement type
     * @return the code type for the movement type
     */
    public static String toMovementTypeCodeType(String movementType) {
        return movementType;
    }

    /**
     * Converts movement direction from String to CodeType.
     *
     * @param movementDirection the movement direction
     * @return the code type for the movement direction
     */
    public static String toMovementDirectionCodeType(String movementDirection) {
        return movementDirection;
    }

    /**
     * Converts movement status from String to CodeType.
     *
     * @param movementStatus the movement status
     * @return the code type for the movement status
     */
    public static String toMovementStatusCodeType(String movementStatus) {
        return movementStatus;
    }

    /**
     * Converts movement reason from String to CodeType.
     *
     * @param movementReason the movement reason
     * @return the code type for the movement reason
     */
    public static String toMovementReasonCodeType(String movementReason) {
        return movementReason;
    }

    /**
     * Converts movement outcome from String to CodeType.
     *
     * @param movementOutcome the movement outcome
     * @return the code type for the movement outcome
     */
    public static String toMovementOutcomeCodeType(String movementOutcome) {
        return movementOutcome;
    }

    private static CodeType toUpperCase(CodeType codeType) {
        CodeType upperCaseCodeType = null;
        if (codeType != null) {
            String set = codeType.getSet();
            String code = codeType.getCode();
            upperCaseCodeType = new CodeType((set != null ? set.toUpperCase() : null), (code != null ? code.toUpperCase() : null));
        }
        return upperCaseCodeType;
    }

    @BeforeMethod
    public void beforeMethod() {

    }

    @BeforeClass
    public void beforeClass() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");
        maSer = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
        filService = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        actService = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        locService = (LocationService) JNDILookUp(this.getClass(), LocationService.class);
        uc = super.initUserContext();

        //		maSer.deleteAll(uc);
        //		actService.deleteAll(uc);
        //		filService.deleteAll(uc);
        //		locService.deleteAll(uc);

        // Facility
        FacilityTest fac = new FacilityTest();
        facilityId = fac.createFacility();
        fromFacilityAssociation = fac.createFacility();
        toFacilityAssociation = fac.createFacility();

        // Organization
        OrganizationTest org = new OrganizationTest();
        fromOrganizationAssociation = org.createOrganization();
        toOrganizationAssociation = org.createOrganization();
        arrestOrganizationAssociation = org.createOrganization();
        escortOrganizationAssociation = org.createOrganization();

        // Activity
        ActivityTest act = new ActivityTest();
        activityId = act.createActivity(ActivityCategory.MOVEMENT);
        activityId1 = act.createActivity(ActivityCategory.MOVEMENT);
        activityId2 = act.createActivity(ActivityCategory.MOVEMENT);
        activityId3 = act.createActivity(ActivityCategory.MOVEMENT);
        activityId4 = act.createActivity(ActivityCategory.MOVEMENT);
        activityId5 = act.createActivity(ActivityCategory.MOVEMENT);
        for (int i = 0; i < 13; i++) {
            activityIDs[i] = act.createActivity(ActivityCategory.MOVEMENT);
        }

        // Supervision
        SupervisionTest sup = new SupervisionTest();
        supervisionId = sup.createSupervision();
        supervisionId2 = sup.createSupervision();

        // Location
        LocationTest loc = new LocationTest();
        fromLocationAssociation = loc.createLocation(true, null, fromFacilityAssociation).getLocationIdentification();
        toLocationAssociation = loc.createLocation(true, null, toFacilityAssociation).getLocationIdentification();

        // Facility Internal Location
        FacilityInternalLocationTest fil = new FacilityInternalLocationTest();
        fil.createFILTree(facilityId);
        filRootId = fil.getRootId();
        filChildrenAndRootIDs = fil.getAllChildrenAndRoot();
        fromFacilityInternalLocationAssociation = filChildrenAndRootIDs.get(0);
        toFacilityInternalLocationAssociation = filChildrenAndRootIDs.get(1);

        if (log.isDebugEnabled()) {
            log.debug("beforeTest: end");
        }
    }

    @AfterClass
    public void afterClass() {
        // Facility Internal Location
        maSer.deleteAll(uc);
        filService.deleteAll(uc);

        // Organization
        OrganizationTest org = new OrganizationTest();
        if (fromOrganizationAssociation != null) {
            org.deleteOrganization(fromOrganizationAssociation);
        }
        if (toOrganizationAssociation != null) {
            org.deleteOrganization(toOrganizationAssociation);
        }
        if (arrestOrganizationAssociation != null) {
            org.deleteOrganization(arrestOrganizationAssociation);
        }
        if (escortOrganizationAssociation != null) {
            org.deleteOrganization(escortOrganizationAssociation);
        }

        if (activityId != null) {
            actService.delete(uc, activityId);
        }
        if (activityId1 != null) {
            actService.delete(uc, activityId1);
        }
        if (activityId2 != null) {
            actService.delete(uc, activityId2);
        }
        if (activityId3 != null) {
            actService.delete(uc, activityId3);
        }
        if (activityId4 != null) {
            actService.delete(uc, activityId4);
        }
        if (activityId5 != null) {
            actService.delete(uc, activityId5);
        }
        for (int i = 0; i < 13; i++) {
            actService.delete(uc, activityIDs[i]);
        }

        // Supervision
        SupervisionTest sup = new SupervisionTest();
        sup.deleteSupervision(supervisionId);
        sup.deleteSupervision(supervisionId2);

        // Location
        locService.delete(uc, fromLocationAssociation);
        locService.delete(uc, toLocationAssociation);

        if (this.facilityId != null) {
            FacilityTest f = new FacilityTest();
            f.deleteFacility(facilityId);
            f.deleteFacility(fromFacilityAssociation);
            f.deleteFacility(toFacilityAssociation);
        }
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(maSer);
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testReset() {
        Long returnCode = maSer.deleteAll(uc);
        assertEquals(returnCode, success);
    }

    @Test(dependsOnMethods = { "testReset" }, enabled = true)
    public void testCreate() {
        // External Movement Activity

        // Set Movement Category
        ma = new MovementActivityType();

        ma.setSupervisionId(supervisionId);
        ma.setMovementCategory("EXTERNAL");
        // Set Movement Type
        ma.setMovementType("CRT"); // CRT -- Court
        // Set Movement Direction
        ma.setMovementDirection("OUT");
        // Set Movement Status
        ma.setMovementStatus("PENDING");
        // Set Movement Reason
        ma.setMovementReason("LA"); // LA -- Legal Aid

        // Set Movement Date
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR, 12);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        Date presentTime = c.getTime();
        ma.setMovementDate(presentTime);

        // Set Comments
        Set<CommentType> comments = new LinkedHashSet<CommentType>();
        CommentType ct1 = new CommentType();
        ct1.setCommentDate(presentTime);
        ct1.setComment("Court appear.");
        comments.add(ct1);
        CommentType ct2 = new CommentType();
        ct2.setCommentDate(presentTime);
        ct2.setComment("Go to court.");
        comments.add(ct2);
        ma.setCommentText(comments);
        ma.setActivityId(activityIDs[0]);
        ema = new ExternalMovementActivityType();

        ema.setSupervisionId(supervisionId);
        ema.setMovementCategory("EXTERNAL");
        // Set Movement Type
        ema.setMovementType("CRT"); // CRT -- Court
        // Set Movement Direction
        ema.setMovementDirection("OUT");
        // Set Movement Status
        ema.setMovementStatus("PENDING");
        // Set Movement Reason
        ema.setMovementReason("LA"); // LA -- Legal Aid

        ema.setMovementDate(presentTime);

        // Set Comments
        ema.setCommentText(comments);

        ema.setActivityId(activityIDs[0]);

        ema.setFromLocationId(fromLocationAssociation);
        ema.setFromFacilityId(fromFacilityAssociation);
        ema.setFromOrganizationId(fromOrganizationAssociation);
        ema.setToProvinceState(toProvinceState);
        ema.setToLocationId(toLocationAssociation);
        ema.setToFacilityId(toFacilityAssociation);
        ema.setToOrganizationId(toOrganizationAssociation);
        applicationDate = presentTime;
        ema.setApplicationDate(applicationDate);
        ema.setArrestOrganizationId(arrestOrganizationAssociation);
        ema.setEscortOrganizationId(escortOrganizationAssociation);
        reportingDate = presentTime;
        ema.setReportingDate(presentTime);

        ema.setFromCity("Vancouver");
        ema.setToCity("Richmond");

        ema.setMovementOutcome("Outcome1"); // Outcome1 does not exist in reference set/code

        // Create an external movement activity
        // maRet = maSer.create(ema);
        // assertEquals(maRet.getReturnCode(), success);
        assertNotNull(uc);
        try {
            emaRet = (ExternalMovementActivityType) maSer.create(uc, ema, false);
            assertNull(emaRet, "Outcome1 does not exist in reference set/code");
        } catch (ArbutusRuntimeException e) {
            log.info(e.getMessage());
        }

        ema.setMovementOutcome(null);

        emaRet = (ExternalMovementActivityType) maSer.create(uc, ema, false);
        assertNotNull(emaRet);

        // ExternalMovementActivityType exma = (ExternalMovementActivityType)
        // maRet
        // .getMovementActivity();
        ExternalMovementActivityType exma = emaRet;

        assertNotNull(exma, "Created Movement Activity should not be null in case of return code be success.");

        exId = exma.getMovementId();
        assertTrue(exId.longValue() > 0L);
        assertEquals(exma.getSupervisionId(), supervisionId);
        assertEquals(exma.getMovementCategory(), ema.getMovementCategory());
        assertEquals(exma.getMovementType(), ema.getMovementType());
        assertEquals(exma.getMovementDirection(), ema.getMovementDirection());
        assertEquals(exma.getMovementStatus(), ema.getMovementStatus());
        assertEquals(exma.getMovementReason(), ema.getMovementReason());
        assertEquals(exma.getMovementDate(), ema.getMovementDate());
        assertEquals(exma.getFromCity(), ema.getFromCity());
        assertEquals(exma.getToCity(), ema.getToCity());
        assertEquals(exma.getCommentText().size(), ema.getCommentText().size());
        assertEquals(exma.getActivityId(), ema.getActivityId());
        assertEquals(exma.getFromLocationId(), ema.getFromLocationId());
        assertEquals(exma.getFromFacilityId(), ema.getFromFacilityId());
        assertEquals(exma.getFromOrganizationId(), ema.getFromOrganizationId());
        assertEquals(exma.getApplicationDate(), ema.getApplicationDate());
        assertEquals(exma.getArrestOrganizationId(), ema.getArrestOrganizationId());
        assertEquals(exma.getEscortOrganizationId(), ema.getEscortOrganizationId());
        assertEquals(exma.getReportingDate(), ema.getReportingDate());
        assertEquals(exma.getToProvinceState(), ema.getToProvinceState());
        assertEquals(exma.getToFacilityId(), ema.getToFacilityId());
        assertEquals(exma.getToLocationId(), ema.getToLocationId());
        assertEquals(exma.getToOrganizationId(), ema.getToOrganizationId());

        ema = (ExternalMovementActivityType) exma;
        ema.setActivityId(activityIDs[1]);

        assertNotNull(maSer.create(uc, ema, false));

        ema.setActivityId(activityIDs[2]);
        assertNotNull(maSer.create(uc, ema, false));

        ema.setActivityId(activityIDs[3]);
        assertNotNull(maSer.create(uc, ema, false));

        ema.setActivityId(activityIDs[4]);
        assertNotNull(maSer.create(uc, ema, false));

        ema.setActivityId(activityIDs[5]);
        assertNotNull(maSer.create(uc, ema, false));

        ema.setActivityId(activityIDs[6]);
        assertNotNull(maSer.create(uc, ema, false));

        ima = new InternalMovementActivityType();

        ima.setSupervisionId(supervisionId);
        ima.setMovementCategory("INTERNAL");
        // Set Movement Type
        ima.setMovementType("CRT"); // CRT -- Court
        // Set Movement Direction
        ima.setMovementDirection("OUT");
        // Set Movement Status
        ima.setMovementStatus("PENDING");
        // Set Movement Reason
        ima.setMovementReason("LA"); // LA -- Legal Aid

        // Set Movement Date
        ima.setMovementDate(presentTime);

        // Set Comments
        ima.setCommentText(comments);

        ima.setActivityId(activityIDs[7]);

        // Internal Movement Activity
        // ima = (InternalMovementActivityType)ma;
        ima.setMovementCategory("INTERNAL");

        ima.setFromFacilityInternalLocationId(fromFacilityInternalLocationAssociation);
        ima.setToFacilityInternalLocationId(this.toFacilityInternalLocationAssociation);

        ima.setMovementOutcome("Outcome1"); // Outcome1 does not exist in reference set/code

        // maRet = maSer.create(uc, (MovementActivityType)ima);
        // assertEquals(maRet.getReturnCode(), success);
        try {
            imaRet = (InternalMovementActivityType) maSer.create(uc, ima, false);
            assertEquals(imaRet, "Outcome1 does not exist in reference set/code");
        } catch (ArbutusRuntimeException e) {
            log.info(e.getMessage());
        }

        ima.setMovementOutcome(null);

        ima.setActivityId(activityIDs[8]);
        imaRet = (InternalMovementActivityType) maSer.create(uc, ima, false);
        assertNotNull(imaRet);

        // InternalMovementActivityType inma = (InternalMovementActivityType)
        // maRet
        // .getMovementActivity();
        InternalMovementActivityType inma = (InternalMovementActivityType) imaRet;

        inId = inma.getMovementId();
        assertTrue(inId.longValue() > 0L);

        assertEquals(inma.getMovementCategory(), ima.getMovementCategory());
        assertEquals(inma.getMovementType(), ima.getMovementType());
        assertEquals(inma.getMovementDirection(), ima.getMovementDirection());
        assertEquals(inma.getMovementStatus(), ima.getMovementStatus());
        assertEquals(inma.getMovementReason(), ima.getMovementReason());
        assertEquals(inma.getMovementDate(), ima.getMovementDate());
        assertEquals(inma.getCommentText().size(), ima.getCommentText().size());
        assertEquals(inma.getActivityId(), ima.getActivityId());
        assertEquals(inma.getFromFacilityInternalLocationId(), ima.getFromFacilityInternalLocationId());
        assertEquals(inma.getToFacilityInternalLocationId(), ima.getToFacilityInternalLocationId());

        ima = (InternalMovementActivityType) inma;

        ima.setActivityId(activityIDs[9]);
        maSer.create(uc, ima, false);

        ima.setActivityId(activityIDs[10]);
        maSer.create(uc, ima, false);

        ima.setActivityId(activityIDs[11]);
        maSer.create(uc, ima, false);

        ima.setActivityId(activityIDs[12]);
        maSer.create(uc, ima, false);
    }

    //	/**
    //	 *  This defect cannot be reproduced!!!
    //	 */
    //	@Test(dependsOnMethods = { "testUpdate" }, enabled = true)
    //	public void DefectID_1275() {
    //		MovementActivityType ma = new MovementActivityType();
    //		ma.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 200L));
    //		ma.setMovementCategory(new CodeType(ReferenceSet.MOVEMENT_CATEGORY.value(), "INTERNAL"));
    //		ma.setMovementType(new CodeType(ReferenceSet.MOVEMENT_TYPE.value(), "CRT"));
    //		ma.setMovementDirection(new CodeType(ReferenceSet.MOVEMENT_DIRECTION.value(), "OUT"));
    //		ma.setMovementStatus(new CodeType(ReferenceSet.MOVEMENT_STATUS.value(), "PENDING"));
    //		ma.setMovementReason(new CodeType(ReferenceSet.MOVEMENT_REASON.value(), "LA"));
    //		ma.setMovementOutcome(new CodeType(ReferenceSet.MOVEMENT_OUTCOME.value(), "SICK"));
    //
    //		Calendar c = Calendar.getInstance();
    //		c.set(Calendar.YEAR, 2015);
    //		c.set(Calendar.MONTH, Calendar.OCTOBER);
    //		c.set(Calendar.DAY_OF_MONTH, 12);
    //		Date movementDate = c.getTime();
    //		ma.setMovementDate(movementDate);
    //
    //		ma.setApprovedByStaffId(new AssociationType(AssociationToClass.STAFF.value(), 1L));
    //
    //		c.set(Calendar.YEAR, 2015);
    //		c.set(Calendar.MONTH, Calendar.OCTOBER);
    //		c.set(Calendar.DAY_OF_MONTH, 11);
    //		Date approvalDate = c.getTime();
    //		ma.setApprovalDate(approvalDate);
    //
    //		AssociationType activityAssoc = new AssociationType(AssociationToClass.ACTIVITY.value(), 2L);
    //		ma.getAssociations().add(activityAssoc);
    //
    ////		ima.setFromFacilityInternalLocationAssociation(null);
    ////		// ima.setFromFacilityInternalLocationAssociation(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 1L));
    ////		ima.setToFacilityInternalLocationAssociation(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 6L));
    //		assertTrue(ma.isWellFormed());
    //		assertTrue(ma.isValid());
    //
    //		MovementActivityReturnType ret = maSer.create(uc, ma, false);
    //		assertEquals(ret.getReturnCode(), ReturnCode.CInvalidInput2003.returnCode());
    //
    //		InternalMovementActivityType ima = new InternalMovementActivityType();
    //		ima.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 300L));
    //		ima.setMovementCategory(new CodeType(ReferenceSet.MOVEMENT_CATEGORY.value(), "INTERNAL"));
    //		ima.setMovementType(new CodeType(ReferenceSet.MOVEMENT_TYPE.value(), "CRT"));
    //		ima.setMovementDirection(new CodeType(ReferenceSet.MOVEMENT_DIRECTION.value(), "OUT"));
    //		ima.setMovementStatus(new CodeType(ReferenceSet.MOVEMENT_STATUS.value(), "PENDING"));
    //		ima.setMovementReason(new CodeType(ReferenceSet.MOVEMENT_REASON.value(), "LA"));
    //		ima.setMovementOutcome(new CodeType(ReferenceSet.MOVEMENT_OUTCOME.value(), "SICK"));
    //
    //		c = Calendar.getInstance();
    //		c.set(Calendar.YEAR, 2015);
    //		c.set(Calendar.MONTH, Calendar.OCTOBER);
    //		c.set(Calendar.DAY_OF_MONTH, 12);
    //		movementDate = c.getTime();
    //		ima.setMovementDate(movementDate);
    //
    //		ima.setApprovedByStaffId(new AssociationType(AssociationToClass.STAFF.value(), 1L));
    //
    //		c.set(Calendar.YEAR, 2015);
    //		c.set(Calendar.MONTH, Calendar.OCTOBER);
    //		c.set(Calendar.DAY_OF_MONTH, 11);
    //		approvalDate = c.getTime();
    //		ima.setApprovalDate(approvalDate);
    //		ima.setApprovalComments("Approved.");
    //
    //		// activityAssoc = new AssociationType(AssociationToClass.ACTIVITY.value(), 2L);
    //		activityAssoc = new AssociationType(AssociationToClass.ACTIVITY.value(), new Random().nextLong());
    //		ima.getAssociations().add(activityAssoc);
    //
    //		ima.setFromFacilityInternalLocationAssociation(null);
    //		// ima.setFromFacilityInternalLocationAssociation(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 1L));
    //		ima.setToFacilityInternalLocationAssociation(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 6L));
    //
    //		Set<String> dataPrivileges = new HashSet<String>();
    //		dataPrivileges.add("HP");
    //		ima.setDataPrivileges(dataPrivileges);
    //
    //		assertTrue(ima.isWellFormed());
    //		assertTrue(ima.isValid());
    //
    //		InternalMovementActivityReturnType inRet = maSer.create(uc, ima, false);
    //		assertEquals(inRet.getReturnCode(), success);
    //
    //		assertEquals(inRet.getInternalMovementActivity().getApprovedByStaffId(), new AssociationType(AssociationToClass.STAFF.value(), 1L));
    //		assertEquals(inRet.getInternalMovementActivity().getApprovalDate(), approvalDate);
    //		assertEquals(inRet.getInternalMovementActivity().getApprovalComments(), "Approved.");
    //
    //	}
    //
    //	@Test(dependsOnMethods = { "testUpdate" }, enabled = true)
    //	public void defect1411() {
    //		String data = "{'fromLocationAssociation':{'toClass':'LocationService','toIdentifier':10},"+
    //                     "'fromOrganizationAssociation': {'toClass':'OrganizationService','toIdentifier':10},"+
    //                     "'fromFacilityAssociation': {'toClass':'FacilityService','toIdentifier':10},"+
    //                     "'fromCity':'Vancouver',"+
    //                     //"'toFacilityInternalLocationAssociation': {'toClass':'FacilityInternalLocationService','toIdentifier':20},"+
    //                     "'toFacilityInternalLocationAssociation': null,"+
    //                     "'toLocationAssociation':{'toClass':'LocationService','toIdentifier':2},"+
    //                     "'toOrganizationAssociation':{'toClass':'FacilityService','toIdentifier':11},"+
    //                     "'toFacilityAssociation':{'toClass':'FacilityService','toIdentifier':11},"+
    //                     "'toCity':'Richmond',"+
    //                     "'toProvinceState':'BC',"+
    //                     "'arrestOrganizationAssociation' :{'toClass':'OrganizationService','toIdentifier':1},"+
    //                     "'applicationDate':'2015-10-12 5:07:20' ,"+//null,
    //                     "'escortOrganizationAssociation':{'toClass':'OrganizationService','toIdentifier':2},"+
    //                     "'escortDetails':'escort detail string',"+
    //                     "'reportingDate':'2015-10-13 5:07:20'," +
    //                     "'supervisionAssociation':{'toClass':'SupervisionService','toIdentifier':21},"+
    //                         "'movementCategory':{'set':'MovementCategory','code':'EXTERNAL'}," +
    //                         "'movementType':{'set':'MovementType','code':'CRT'}," +
    //                     "'movementDirection':{'set':'MovementDirection','code':'IN'}," +
    //                     "'movementStatus':{'set':'MovementStatus','code':'PENDING'}," +
    //                     "'movementReason':{'set':'MovementReason','code':'LA'}," +
    //                     "'movementOutcome':{'set':'MovementOutcome','code':'EXCUSED'}," +
    //                     "'movementDate':'2015-10-13 5:07:19'," +
    //                      "'comment':[{'commentId':null,'commentDate':'2015-10-12 5:07:20','comment':'this is testing', 'userId':null}],"+
    //                     "'approvedByStaffAssociation'={'toClass':'StaffService','toIdentifier':1},"+
    //                                "'approvalDate'='2015-10-12 5:07:20',"+
    //                                "'approvalComments'='hello',"+
    //                    "'associations':[{'toClass':'ActivityService','toIdentifier':6,'dataPrivileges':null}],"+
    //                     "'facilities':[]," +
    //                     "'dataPrivileges':[]}";
    //
    //
    //		ExternalMovementActivityType ema = gson.fromJson(data, ExternalMovementActivityType.class);
    //		assertTrue(ema.isValid());
    //
    //		ExternalMovementActivityReturnType ret = maSer.create(uc, ema, true);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertNotNull(ret.getExternalMovementActivity());
    //
    //		ret.getExternalMovementActivity().setMovementStatus(new CodeType(ReferenceSet.MOVEMENT_STATUS.value(), "COMPLETED"));
    //		assertFalse(ret.getExternalMovementActivity().isValid());
    //		ret = maSer.update(uc, ret.getExternalMovementActivity());
    //		assertEquals(ret.getReturnCode(), ReturnCode.RInvalidInput1003.returnCode());
    //		assertNull(ret.getExternalMovementActivity());
    //
    //	}
    //
    //	@Test(dependsOnMethods = { "testUpdate" }, enabled = true)
    //	public void defect1436() {
    //		String data = "{'supervisionAssociation':{'toClass':'SupervisionService','toIdentifier':21},"+
    //                       "'movementCategory':{'set':'MovementCategory','code':'EXTERNAL'}," +
    //                       "'movementType':{'set':'MovementType','code':'TAP'}," +
    //                       "'movementDirection':{'set':'MovementDirection','code':'OUT'}," +
    //                       "'movementStatus':{'set':'MovementStatus','code':'APPREQ'}," +
    //                       "'movementReason':{'set':'MovementReason','code':'LA'}," +
    //                       "'movementOutcome':{'set':'MovementOutcome','code':'EXCUSED'}," +
    //                       "'movementDate':null," +
    //                       "'comment':[{'commentId':null,'commentDate':'2015-10-12 5:07:20','comment':'B','userId':null}]," +
    //                       "'associations':[{'toClass':'ActivityService','toIdentifier':4,'dataPrivileges':null}],"+
    //                       "'facilities':[]," +
    //                       "'dataPrivileges':[],";
    //
    //		data +=  "'fromLocationAssociation':{'toClass':'LocationService','toIdentifier':10000}," +
    //        "'fromOrganizationAssociation': {'toClass':'OrganizationService','toIdentifier':30000}," +
    //        "'fromFacilityAssociation': {'toClass':'FacilityService','toIdentifier':20000}," +
    //        "'fromCity':'Vancouver'," +
    //        "'fromFacilityInternalLocationAssociation': {'toClass':'FacilityInternalLocationService','toIdentifier':20}," +
    //        "'toLocationAssociation':{'toClass':'LocationService','toIdentifier':40000}," +
    //        "'toOrganizationAssociation':{'toClass':'OrganizationService','toIdentifier':60000}," +
    //        "'toFacilityAssociation':{'toClass':'FacilityService','toIdentifier':50000}," +
    //        "'toCity':'Richmond'," +
    //        "'toProvinceState':'BC'," +
    //        "'arrestOrganizationAssociation' :{'toClass':'OrganizationService','toIdentifier':70000}," +
    //        "'applicationDate':null," +
    //        "'escortOrganizationAssociation':{'toClass':'OrganizationService','toIdentifier':80000}," +
    //        "'reportingDate':null }";
    //
    //		ExternalMovementActivityType ema = gson.fromJson(data, ExternalMovementActivityType.class);
    //		assertTrue(ema.isValid());
    //
    //		ExternalMovementActivityReturnType ret = maSer.create(uc, ema, true);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertNotNull(ret.getExternalMovementActivity());
    //
    //		ret.getExternalMovementActivity().setMovementStatus(new CodeType(ReferenceSet.MOVEMENT_STATUS.value(), "PENDING"));
    //		assertTrue(ret.getExternalMovementActivity().isValid());
    //		ret = maSer.update(uc, ret.getExternalMovementActivity());
    //		assertEquals(ret.getReturnCode(), success);
    //		assertNotNull(ret.getExternalMovementActivity());
    //
    //	}

    @Test(dependsOnMethods = { "testCreate" }, enabled = true)
    public void testGetById() {

        assertNotNull(exId);
        assertTrue(exId.longValue() > 0);

        assertNotNull(inId);
        assertTrue(inId.longValue() > 0);

        //java.lang.NoSuchMethodError: org.testng.Assert.assertNotEquals(Ljava/lang/Object;Ljava/lang/Object;)V
        //assertNotEquals(exId, inId);
        assertFalse(exId.equals(inId));

        MovementActivityType ret = maSer.get(uc, exId);
        assertNotNull(ret);

        ExternalMovementActivityType e = (ExternalMovementActivityType) ret;

        assertTrue(this.sameExternalMovementActivity(e, this.ema));

        ret = maSer.get(uc, inId);
        assertNotNull(ret);

        InternalMovementActivityType i = (InternalMovementActivityType) ret;

        assertTrue(this.sameInternalMovementActivity(i, this.ima));
    }

    @Test(dependsOnMethods = { "testCreate" }, enabled = true)
    public void testGetAll() {
        Set<Long> ids = maSer.getAll(uc);
        assertNotNull(ids);
        assertTrue(ids.size() > 0);
        assertTrue(ids.contains(exId));
        assertTrue(ids.contains(inId));
    }

    //	/**
    //	 * Confirmation test after defect fixed.
    //	 */
    //	@Test(dependsOnMethods = {"testReset", "LastCompletedSupervisionMovement", "LastCompletedSupervisionMovement2"}, enabled = true)
    //	public void DefectID_2515() {
    //		maSer.reset(uc);
    //
    //		Calendar c = Calendar.getInstance();
    //		c.set(Calendar.SECOND, 0);
    //		c.set(Calendar.MILLISECOND, 0);
    //
    //		Date today = c.getTime();
    //		c.add(Calendar.DAY_OF_MONTH, -1);
    //		Date yesterday = c.getTime();
    //
    //
    //		ExternalMovementActivityType ema = new ExternalMovementActivityType();
    //		AssociationType supervisionAssociation = new AssociationType(AssociationToClass.SUPERVISION.value(), 111L);
    //
    //		ema.setSupervisionId(supervisionAssociation);
    //		ema.setMovementCategory(new CodeType(ReferenceSet.MOVEMENT_CATEGORY.value(), "EXTERNAL"));
    //		ema.setMovementType(new CodeType(ReferenceSet.MOVEMENT_TYPE.value(), "CRT"));
    //		ema.setMovementDirection(new CodeType(ReferenceSet.MOVEMENT_DIRECTION.value(), "IN"));
    //		ema.setMovementStatus(new CodeType(ReferenceSet.MOVEMENT_STATUS.value(), "COMPLETED"));
    //		ema.setMovementReason(new CodeType(ReferenceSet.MOVEMENT_REASON.value(), "LA"));
    //		ema.setMovementOutcome(new CodeType(ReferenceSet.MOVEMENT_OUTCOME.value(), "SICK"));
    //
    //		ema.setMovementDate(today);
    //
    //		ema.getAssociations().clear();
    //		AssociationType activityAssoc = new AssociationType(AssociationToClass.ACTIVITY.value(), 11111L);
    //		ema.getAssociations().add(activityAssoc);
    //
    //		ema.setToFacilityInternalLocationAssociation(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 1L));
    //
    //		Set<String> dataPrivileges = new HashSet<String>();
    //		dataPrivileges.add("HP");
    //		ema.setDataPrivileges(dataPrivileges);
    //		assertTrue(ema.isWellFormed());
    //		assertTrue(ema.isValid());
    //
    //		ExternalMovementActivityReturnType exRet = maSer.create(uc, ema, false);
    //		assertEquals(exRet.getReturnCode(), success);
    //		Long movementActivityId = exRet.getExternalMovementActivity().getMovementId();
    //
    //		// getLastCompletedSupervisionMovementBySupervisions
    //		Set<AssociationType> supervisions = new HashSet<AssociationType>();
    //		supervisions.add(supervisionAssociation);
    //		LastCompletedSupervisionMovementsReturnType lastMove = maSer.getLastCompletedSupervisionMovementBySupervisions(uc, supervisions);
    //		assertEquals(lastMove.getReturnCode(), success);
    //		assertEquals(lastMove.getCount().intValue(), 1);
    //		LastCompletedSupervisionMovementType lMove = (LastCompletedSupervisionMovementType)(lastMove.getLastCompletedSupervisionMovements().toArray()[0]);
    //		assertEquals(lMove.getSupervisionId(), supervisionAssociation);
    //		assertEquals(
    //				((AssociationType)(
    //						lMove.getLastCompletedMovementActivity().getAssociations().toArray()[0])
    //						).getToIdentifier().longValue(),
    //						11111L);
    //		assertEquals(lMove.getCurrentInternalLocationAssociation().getToIdentifier().longValue(), 1L);
    //		assertEquals(lMove.getLastCompletedMovementActivity().getMovementId(), exRet.getExternalMovementActivity().getMovementId());
    //		assertEquals(sdf.format(lMove.getLastCompletedMovementActivity().getMovementDate()), sdf.format(today));
    //		assertEquals(lMove.getLastCompletedMovementActivity().getMovementId(), movementActivityId);
    //
    //		// getLastCompletedSupervisionMovementByLocations
    //		Set<AssociationType> locations = new HashSet<AssociationType>();
    //		locations.add(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 1L));
    //		lastMove = maSer.getLastCompletedSupervisionMovementByLocations(uc, locations);
    //		assertEquals(lastMove.getReturnCode(), success);
    //		assertEquals(lastMove.getCount().intValue(), 1);
    //		lMove = (LastCompletedSupervisionMovementType)(lastMove.getLastCompletedSupervisionMovements().toArray()[0]);
    //		assertEquals(lMove.getSupervisionId(), supervisionAssociation);
    //		assertEquals(
    //				((AssociationType)(
    //						lMove.getLastCompletedMovementActivity().getAssociations().toArray()[0])
    //						).getToIdentifier().longValue(),
    //						11111L);
    //		assertEquals(lMove.getCurrentInternalLocationAssociation().getToIdentifier().longValue(), 1L);
    //
    //		assertEquals(lMove.getLastCompletedMovementActivity().getMovementDate(), today);
    //		assertEquals(lMove.getLastCompletedMovementActivity().getMovementId(), movementActivityId);
    //
    //		// Internal
    //		InternalMovementActivityType ima = new InternalMovementActivityType();
    //		ima.setSupervisionId(supervisionAssociation);
    //		ima.setMovementCategory(new CodeType(ReferenceSet.MOVEMENT_CATEGORY.value(), "INTERNAL"));
    //		ima.setMovementType(new CodeType(ReferenceSet.MOVEMENT_TYPE.value(), "BED"));
    //		ima.setMovementDirection(new CodeType(ReferenceSet.MOVEMENT_DIRECTION.value(), "IN"));
    //		ima.setMovementStatus(new CodeType(ReferenceSet.MOVEMENT_STATUS.value(), "COMPLETED"));
    //		ima.setMovementReason(new CodeType(ReferenceSet.MOVEMENT_REASON.value(), "ADM"));
    //
    //
    //		ima.setMovementDate(yesterday);
    //
    //		ima.getAssociations().add(new AssociationType(AssociationToClass.ACTIVITY.value(), 22222L));
    //
    //		ima.setFromFacilityInternalLocationAssociation(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 1L));
    //		ima.setToFacilityInternalLocationAssociation(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 2L));
    //
    //		ima.setDataPrivileges(dataPrivileges);
    //
    //		assertTrue(ima.isWellFormed());
    //		assertTrue(ima.isValid());
    //
    //		InternalMovementActivityReturnType inRet = maSer.create(uc, ima, false);
    //		assertEquals(inRet.getReturnCode(), success);
    //		Long inId = inRet.getInternalMovementActivity().getMovementId();
    //
    //		supervisions = new HashSet<AssociationType>();
    //		supervisions.add(supervisionAssociation);
    //		lastMove = maSer.getLastCompletedSupervisionMovementBySupervisions(uc, supervisions);
    //		assertEquals(lastMove.getReturnCode(), success);
    //		assertEquals(lastMove.getCount().intValue(), 1);
    //		lMove = (LastCompletedSupervisionMovementType)(lastMove.getLastCompletedSupervisionMovements().toArray()[0]);
    //		assertEquals(lMove.getSupervisionId(), supervisionAssociation);
    //
    //		assertEquals(sdf.format(lMove.getLastCompletedMovementActivity().getMovementDate()), sdf.format(today));
    //		assertFalse(sdf.format(lMove.getLastCompletedMovementActivity().getMovementDate()).equals(sdf.format(yesterday)));
    //		assertNotEquals(lMove.getLastCompletedMovementActivity().getMovementId(), inId);
    //
    //		// Another Internal
    //		ima.setMovementDate(today);
    //		ima.getAssociations().clear();
    //		ima.getAssociations().add(new AssociationType(AssociationToClass.ACTIVITY.value(), 22223L));
    //		inRet = maSer.create(uc, ima, false);
    //		assertEquals(inRet.getReturnCode(), success);
    //		inId = inRet.getInternalMovementActivity().getMovementId();
    //
    //		supervisions = new HashSet<AssociationType>();
    //		supervisions.add(supervisionAssociation);
    //		lastMove = maSer.getLastCompletedSupervisionMovementBySupervisions(uc, supervisions);
    //		assertEquals(lastMove.getReturnCode(), success);
    //		assertEquals(lastMove.getCount().intValue(), 1);
    //		lMove = (LastCompletedSupervisionMovementType)(lastMove.getLastCompletedSupervisionMovements().toArray()[0]);
    //		assertEquals(lMove.getSupervisionId(), supervisionAssociation);
    //
    //		assertEquals(sdf.format(lMove.getLastCompletedMovementActivity().getMovementDate()), sdf.format(today));
    //		assertFalse(sdf.format(lMove.getLastCompletedMovementActivity().getMovementDate()).equals(sdf.format(yesterday)));
    //		assertEquals(lMove.getLastCompletedMovementActivity().getMovementId(), inId);
    //
    //		// The 3rd Internal
    //		ima.getAssociations().clear();
    //		ima.getAssociations().add(new AssociationType(AssociationToClass.ACTIVITY.value(), 22224L));
    //		ima.setFromFacilityInternalLocationAssociation(ima.getToFacilityInternalLocationAssociation());
    //		ima.setToFacilityInternalLocationAssociation(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 3L));
    //		inRet = maSer.create(uc, ima, false);
    //		assertEquals(inRet.getReturnCode(), success);
    //		inId = inRet.getInternalMovementActivity().getMovementId();
    //
    //		supervisions = new HashSet<AssociationType>();
    //		supervisions.add(supervisionAssociation);
    //		lastMove = maSer.getLastCompletedSupervisionMovementBySupervisions(uc, supervisions);
    //		assertEquals(lastMove.getReturnCode(), success);
    //		assertEquals(lastMove.getCount().intValue(), 1);
    //		lMove = (LastCompletedSupervisionMovementType)(lastMove.getLastCompletedSupervisionMovements().toArray()[0]);
    //		assertEquals(lMove.getSupervisionId(), supervisionAssociation);
    //
    //		assertEquals(sdf.format(lMove.getLastCompletedMovementActivity().getMovementDate()), sdf.format(today));
    //		assertFalse(sdf.format(lMove.getLastCompletedMovementActivity().getMovementDate()).equals(sdf.format(yesterday)));
    //		assertEquals(lMove.getLastCompletedMovementActivity().getMovementId(), inId);
    //	}

    @Test(dependsOnMethods = { "testCreate" }, enabled = true)
    public void testGetCount() {
        Long count = maSer.getCount(uc);
        assertNotNull(count);
        assertTrue(count > 0);
        assertEquals(count.longValue(), 12L);
    }

    @Test(dependsOnMethods = { "testCreate" }, enabled = true)
    public void testGetStamp() {
        assertNotNull(maSer.getStamp(uc, exId));
        assertNotNull(maSer.getStamp(uc, inId));
    }

    @Test(dependsOnMethods = { "testCreate" }, enabled = true)
    public void testSearch() {
        MovementActivitiesReturnType ret;

        MovementActivitySearchType search = new MovementActivitySearchType();

        search.setSupervisionId(supervisionId);
        ret = maSer.search(uc, search, null, null, null);
        assertNotNull(ret);

        search = new MovementActivitySearchType();
        search.setArrestOrganizationId(0L);
        ret = maSer.search(uc, search, null, null, null);
        assertNotNull(ret);
        assertEquals(ret.getTotalSize().longValue(), 0L);

        search = new MovementActivitySearchType();

        search.setApprovalComments("***");
        ret = maSer.search(uc, search, null, null, null);
        assertNotNull(ret, "Actualy should be exception.");

        // Movement Category & Match Mode PARTIAL
        search = new MovementActivitySearchType();

        search.setMovementCategory("ABCDE");
        ret = maSer.search(uc, search, null, null, null);
        assertNotNull(ret);
        assertEquals(ret.getTotalSize().longValue(), 0L);

        search.setMovementCategory("EXTERNAL");
        ret = maSer.search(uc, search, null, null, null);
        assertEquals(ret.getTotalSize().longValue(), 7L);

        assertTrue(this.sameExternalMovementActivity((ExternalMovementActivityType) (ret.getExternalMovementActivity().toArray())[0], this.ema));

        search.setMovementCategory("Inter");
        ret = maSer.search(uc, search, null, null, null);
        assertNotNull(ret);
        assertEquals(ret.getTotalSize().longValue(), 0L);

        // Movement Categroy
        ret = maSer.search(uc, search, null, null, null);
        assertNotNull(ret);
        assertEquals(ret.getTotalSize().longValue(), 0L);

        search.setMovementCategory("INTERNAL");
        ret = maSer.search(uc, search, null, null, null);
        assertEquals(ret.getTotalSize().longValue(), 5L);
        assertEquals(ret.getInternalMovementActivity().size(), 5);
        assertTrue(this.sameInternalMovementActivity((InternalMovementActivityType) (ret.getInternalMovementActivity().toArray())[0], this.ima));

        search.setMovementCategory("EXT");
        ret = maSer.search(uc, search, null, null, null);
        assertEquals(ret.getExternalMovementActivity().size(), 0L);
        assertEquals(ret.getInternalMovementActivity().size(), 0L);
        assertEquals(ret.getTotalSize().intValue(), 0);

        search.setMovementCategory("EXTERNAL");
        ret = maSer.search(uc, search, null, null, null);
        assertEquals(ret.getExternalMovementActivity().size(), 7L);
        assertEquals(ret.getInternalMovementActivity().size(), 0L);
        assertEquals(ret.getTotalSize().intValue(), 7);
        assertTrue(this.sameExternalMovementActivity((ExternalMovementActivityType) (ret.getExternalMovementActivity().toArray())[0], this.ema));

        // Movement Type & Match Mode PARTIAL
        search.setMovementCategory(null);
        search.setMovementType("abcde");
        ret = maSer.search(uc, search, null, null, null);
        assertEquals(ret.getExternalMovementActivity().size(), 0L);
        assertEquals(ret.getInternalMovementActivity().size(), 0L);
        assertEquals(ret.getTotalSize().intValue(), 0);

        search.setMovementType("CRT");
        search.setMovementCategory(toMovementCategoryCodeType("EXTERNAL"));
        ret = maSer.search(uc, search, null, null, null);
        assertEquals(ret.getExternalMovementActivity().size(), 7L);
        assertEquals(ret.getInternalMovementActivity().size(), 0L);
        assertEquals(ret.getTotalSize().intValue(), 7);

        assertTrue(this.sameExternalMovementActivity((ExternalMovementActivityType) (ret.getExternalMovementActivity().toArray())[0], this.ema));

        search.setMovementCategory(toMovementCategoryCodeType("INTERNAL"));
        ret = maSer.search(uc, search, null, null, null);
        assertEquals(ret.getExternalMovementActivity().size(), 0L);
        assertEquals(ret.getInternalMovementActivity().size(), 5L);
        assertEquals(ret.getTotalSize().intValue(), 5);
        assertTrue(this.sameInternalMovementActivity((InternalMovementActivityType) (ret.getInternalMovementActivity().toArray())[0], this.ima));

        search.setMovementCategory(null);

        search.setMovementType(toMovementTypeCodeType("abcde"));
        ret = maSer.search(uc, search, null, null, null);
        assertEquals(ret.getExternalMovementActivity().size(), 0L);
        assertEquals(ret.getInternalMovementActivity().size(), 0L);
        assertEquals(ret.getTotalSize().intValue(), 0);

        search.setMovementType(toMovementTypeCodeType("CRT"));
        ret = maSer.search(uc, search, null, null, null);
        assertEquals(ret.getExternalMovementActivity().size(), 7L);
        assertEquals(ret.getInternalMovementActivity().size(), 5L);
        assertEquals(ret.getTotalSize().intValue(), 12);

        assertTrue(this.sameExternalMovementActivity((ExternalMovementActivityType) (ret.getExternalMovementActivity().toArray())[0], this.ema));
        assertTrue(this.sameInternalMovementActivity((InternalMovementActivityType) (ret.getInternalMovementActivity().toArray())[0], this.ima));

        //		// Reference & EXACT
        //		Set<AssociationType> exRefs = ema.getAssociations();
        //		AssociationType emaActRef = new AssociationType();
        //		AssociationType emaSupRef = new AssociationType();
        ////		for (AssociationType ref : exRefs) {
        ////			if (ref.getToClass().equalsIgnoreCase(AssociationToClass.ACTIVITY.value()))
        ////				emaActRef = ref;
        ////			else
        ////				emaSupRef = ref;
        ////		}
        ////		search.setAssociation(toActivityAssociation(emaActRef.getToIdentifier()));
        ////		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		Long randomId = new Random().nextLong();
        //		assertFalse(randomId.equals(emaActRef.getToIdentifier()));
        //		search.setAssociation(toActivityAssociation(randomId));
        //
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setAssociation(toActivityAssociation(emaActRef.getToIdentifier()));
        //		assertTrue(search.isWellFormed());
        //
        //		Set<Long> ids = new LinkedHashSet<Long>();
        //		ids.add(exId);
        //		ids.add(inId);
        //
        //		ret = maSer.search(uc, search, ids, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 1L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 1L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		search.setAssociation(null);
        //
        //		ids = new LinkedHashSet<Long>();
        //		ids.add(exId);
        //
        //		ret = maSer.search(uc, search, ids, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 1L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		ids = new LinkedHashSet<Long>();
        //		ids.add(inId);
        //
        //		ret = maSer.search(uc, search, ids, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 1L);
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		ids = new LinkedHashSet<Long>();
        //
        //		ret = maSer.search(uc, search, ids, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		ret = maSer.search(uc, search, null, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		// Association & PARTIAL
        //		search.setAssociation(toActivityAssociation(emaActRef.getToIdentifier()));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		assertFalse(randomId.equals(emaActRef.getToIdentifier()));
        //		assertFalse(randomId.equals(emaSupRef.getToIdentifier()));
        //		search.setAssociation(toActivityAssociation(randomId));
        //
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setAssociation(toActivityAssociation(emaActRef.getToIdentifier()));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		// Movement Date (from, to)
        //		Calendar c = Calendar.getInstance();
        //		c.set(Calendar.HOUR, 12);
        //		c.set(Calendar.MINUTE, 0);
        //		c.set(Calendar.SECOND, 0);
        //		c.set(Calendar.MILLISECOND, 0);
        //
        //		Date today = c.getTime();
        //
        //		c.add(Calendar.YEAR, -1);
        //		Date lastYear = c.getTime();
        //
        //		c.add(Calendar.YEAR, 2);
        //		Date nextYear = c.getTime();
        //
        //		// from 1900-01-01
        //		c.clear();
        //		c.set(Calendar.YEAR, 1900);
        //		c.set(Calendar.MONTH, 0);
        //		c.set(Calendar.DAY_OF_MONTH, 1);
        //		Date year19000101 = c.getTime();
        //
        //		search.setFromMovementDate(year19000101);
        //		search.setToMovementDate(null);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		// from last year
        //		search.setFromMovementDate(lastYear);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		// to next year
        //		search.setFromMovementDate(null);
        //		search.setToMovementDate(nextYear);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		// from last year to next year
        //		search.setFromMovementDate(lastYear);
        //		search.setToMovementDate(nextYear);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		// from today
        //		search.setFromMovementDate(today);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		// to today
        //		search.setToMovementDate(today);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		// from today to today
        //		search.setFromMovementDate(today);
        //		search.setToMovementDate(today);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		// from next year
        //		search.setFromMovementDate(nextYear);
        //		search.setToMovementDate(null);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// to last year
        //		search.setFromMovementDate(null);
        //		search.setToMovementDate(lastYear);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// from next yaer to last year
        //		search.setFromMovementDate(nextYear);
        //		search.setToMovementDate(lastYear);
        //		assertFalse(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), ServiceErrorMessages.MSG2002.returnCode());
        //		// assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		// assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setFromMovementDate(lastYear);
        //		search.setToMovementDate(nextYear);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		// Comment
        //		search.setCommentText("XXXXXXXXXXXX");
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setCommentText("XXXXXXXXXXXX");
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setCommentText("Court appear.");
        //		//search.setCommentText("Court appear.");
        //		assertTrue(search.isWellFormed());
        //		assertTrue(search.isValid());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		//ret = maSer.search(uc, search);
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		search.setCommentText("*ou*t *pea*");
        //		//search.setCommentText("Court appear.");
        //		assertTrue(search.isWellFormed());
        //		assertTrue(search.isValid());
        //		ret = maSer.search(uc, search);
        //		//ret = maSer.search(uc, search);
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		search.setCommentText("Court app");
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setCommentText("Court apP");
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		search.setCommentText("Go to court.");
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		search.setCommentText("GO TO COURT.");
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		search.setCommentText("Go to c");
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setCommentText("Go to c");
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		search.setCommentText(null);
        //
        //		// From Location Association
        //		search.setFromLocationId(toLocationAssociation(randomId));
        //		assertFalse(randomId.equals(fromLocationAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setFromLocationId(toLocationAssociation(randomId));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setFromLocationId(fromLocationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setFromLocationId(fromLocationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setFromLocationId(null);
        //
        //		// From Facility Association
        //		search.setFromFacilityId(toFacilityAssociation(randomId));
        //		assertFalse(randomId.equals(fromFacilityAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setFromFacilityId(toFacilityAssociation(randomId));
        //		assertFalse(randomId.equals(fromFacilityAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setFromFacilityId(fromFacilityAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //		log.debug(ret.getExternalMovementActivity());
        //
        //		for (int i = 0; i < ret.getExternalMovementCount().longValue(); i++) {
        //			AssociationType frmFacAssoc = ((ExternalMovementActivityType)ret.getExternalMovementActivity().toArray()[i]).getFromFacilityId();
        //			assertNotNull(frmFacAssoc);
        //			assertNotNull(frmFacAssoc.getToClass());
        //			assertNotNull(frmFacAssoc.getToIdentifier());
        //		}
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setFromFacilityId(fromFacilityAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setFromFacilityId(null);
        //
        //		// From Organization Association
        //		search.setFromOrganizationId(toOrganizationAssociation(randomId));
        //		assertFalse(randomId.equals(fromOrganizationAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setFromOrganizationId(toOrganizationAssociation(randomId));
        //		assertFalse(randomId.equals(fromOrganizationAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setFromOrganizationId(fromOrganizationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setFromOrganizationId(fromOrganizationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setFromOrganizationId(null);
        //
        //		// To Location Association
        //		search.setToLocationId(toLocationAssociation(randomId));
        //		assertFalse(randomId.equals(toLocationAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setToLocationId(toLocationAssociation(randomId));
        //		assertFalse(randomId.equals(toLocationAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setToLocationId(toLocationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setToLocationId(toLocationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setToLocationId(null);
        //
        //		// To Facility Association
        //		search.setToFacilityId(toFacilityAssociation(randomId));
        //		assertFalse(randomId.equals(toFacilityAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setToFacilityId(toFacilityAssociation(randomId));
        //		assertFalse(randomId.equals(toFacilityAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setToFacilityId(toFacilityAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setToFacilityId(toFacilityAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setToFacilityId(null);
        //
        //		// To Organization Association
        //		search.setToOrganizationId(toOrganizationAssociation(randomId));
        //		assertFalse(randomId.equals(toOrganizationAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setToOrganizationId(toOrganizationAssociation(randomId));
        //		assertFalse(randomId.equals(toOrganizationAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setToOrganizationId(toOrganizationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setToOrganizationId(toOrganizationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setToOrganizationId(null);
        //
        //		// Arrest Organization Association
        //		search.setArrestOrganizationId(toOrganizationAssociation(randomId));
        //		assertFalse(randomId.equals(arrestOrganizationAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setArrestOrganizationId(toOrganizationAssociation(randomId));
        //		assertFalse(randomId.equals(arrestOrganizationAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setArrestOrganizationId(arrestOrganizationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setArrestOrganizationId(arrestOrganizationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setArrestOrganizationId(null);
        //
        //		// Escort Organization Association
        //		search.setEscortOrganizationId(toOrganizationAssociation(randomId));
        //		assertFalse(randomId.equals(escortOrganizationAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setEscortOrganizationId(toOrganizationAssociation(randomId));
        //		assertFalse(randomId.equals(escortOrganizationAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setEscortOrganizationId(escortOrganizationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setEscortOrganizationId(escortOrganizationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setEscortOrganizationId(null);
        //
        //		// To Province State
        //		search.setToProvinceState("AB");
        //		assertFalse("AB".equals(toProvinceState));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setToProvinceState("AB");
        //		assertFalse("AB".equals(toProvinceState));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setToProvinceState("B");
        //		assertEquals("BC", toProvinceState);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setToProvinceState(toProvinceState);
        //		assertEquals("BC", toProvinceState);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setToProvinceState(toProvinceState);
        //		assertEquals("BC", toProvinceState);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setToProvinceState("B");
        //		assertEquals("BC", toProvinceState);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setToProvinceState(null);
        //
        //		// To City
        //		search.setToCity("New York");
        //		assertFalse("New York".equals(toCity));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setToCity("New York");
        //		assertFalse("New York".equals(toCity));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setToCity("Rich");
        //		assertEquals("Richmond", toCity);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setToCity(toCity);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setToCity(toCity);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setToCity("Mon");
        //		assertFalse("Mon".equals(toCity));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setToCity(null);
        //
        //		// From City
        //		search.setFromCity("New York");
        //		assertFalse("New York".equals(fromCity));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setFromCity("New York");
        //		assertFalse("New York".equals(fromCity));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setFromCity("Van");
        //		assertEquals("Vancouver", fromCity);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setFromCity(fromCity);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setFromCity(fromCity);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setFromCity("Van");
        //		assertFalse("Van".equals(fromCity));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		assertTrue(this.sameExternalMovementActivity(
        //				(ExternalMovementActivityType) (ret
        //						.getExternalMovementActivity().toArray())[0], this.ema));
        //
        //		search.setFromCity(null);
        //
        //		// Application Date
        //		// from last year
        //		search.setFromApplicationDate(lastYear);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// to next year
        //		search.setFromApplicationDate(null);
        //		search.setToApplicationDate(nextYear);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// from last year to next year
        //		search.setFromApplicationDate(lastYear);
        //		search.setToApplicationDate(nextYear);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// from next year
        //		search.setFromApplicationDate(nextYear);
        //		search.setToApplicationDate(null);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// to last year
        //		search.setFromApplicationDate(null);
        //		search.setToApplicationDate(lastYear);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// from next year to last year
        //		search.setFromApplicationDate(nextYear);
        //		search.setToApplicationDate(lastYear);
        //		assertFalse(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), ServiceErrorMessages.MSG2002.returnCode());
        //		// assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		// assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// from today
        //		search.setFromApplicationDate(today);
        //		search.setToApplicationDate(null);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// to today
        //		search.setFromApplicationDate(null);
        //		search.setToApplicationDate(today);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// from today to today
        //		search.setFromApplicationDate(today);
        //		search.setToApplicationDate(today);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setFromApplicationDate(null);
        //		search.setToApplicationDate(null);
        //
        //		// reportingDate
        //		// from last year
        //		search.setFromReportingDate(lastYear);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// to next year
        //		search.setFromReportingDate(null);
        //		search.setToReportingDate(nextYear);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// from last year to next year
        //		search.setFromReportingDate(lastYear);
        //		search.setToReportingDate(nextYear);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// from next year
        //		search.setFromReportingDate(nextYear);
        //		search.setToReportingDate(null);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// to last year
        //		search.setFromReportingDate(null);
        //		search.setToReportingDate(lastYear);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// from next year to last year
        //		search.setFromReportingDate(nextYear);
        //		search.setToReportingDate(lastYear);
        //		assertFalse(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), ServiceErrorMessages.MSG2002.returnCode());
        //		// assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		// assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// from today
        //		search.setFromReportingDate(today);
        //		search.setToReportingDate(null);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// to today
        //		search.setFromReportingDate(null);
        //		search.setToReportingDate(today);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		// from today to today
        //		search.setFromReportingDate(today);
        //		search.setToReportingDate(today);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setFromReportingDate(null);
        //		search.setToReportingDate(null);
        //
        //		// fromFacilityInternalLocationAssociation
        //		search.setFromFacilityInternalLocationAssociation(toFacilityInternalLocationAssociation(randomId));
        //		assertFalse(randomId.equals(this.fromFacilityInternalLocationAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setFromFacilityInternalLocationAssociation(toFacilityInternalLocationAssociation(randomId));
        //		assertFalse(randomId.equals(this.fromFacilityInternalLocationAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setFromFacilityInternalLocationAssociation(this.fromFacilityInternalLocationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		search.setFromFacilityInternalLocationAssociation(this.fromFacilityInternalLocationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		search.setFromFacilityInternalLocationAssociation(null);
        //
        //		// toFacilityInternalLocationAssociation
        //		search.setToFacilityInternalLocationAssociation(toFacilityInternalLocationAssociation(randomId));
        //		assertFalse(randomId.equals(this.toFacilityInternalLocationAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getReturnCode(), success);
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setToFacilityInternalLocationAssociation(toFacilityInternalLocationAssociation(randomId));
        //		assertFalse(randomId.equals(this.toFacilityInternalLocationAssociation));
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 0L);
        //
        //		search.setToFacilityInternalLocationAssociation(this.toFacilityInternalLocationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.EXACT.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		search.setToFacilityInternalLocationAssociation(this.toFacilityInternalLocationAssociation);
        //		assertTrue(search.isWellFormed());
        //		ret = maSer.search(uc, search, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 0L);
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //
        //		assertTrue(this.sameInternalMovementActivity(
        //				(InternalMovementActivityType) (ret
        //						.getInternalMovementActivity().toArray())[0], this.ima));
        //
        //		Set<InternalMovementActivityType> imas = ret.getInternalMovementActivity();
        //		ids = new HashSet<Long>();
        //		for (InternalMovementActivityType ima: imas) {
        //			ids.add(ima.getMovementId());
        //		}
        //
        //		search = new MovementActivitySearchType();
        //		search.setMovementCategory(new CodeType(ReferenceSet.MOVEMENT_CATEGORY.value(), "INTERNAL"));
        //		ret = maSer.search(uc, search, ids, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getInternalMovementCount().longValue(), 5L);
        //		assertEquals(ret.getInternalMovementActivity().size(), 5);
        //
        //		search = new MovementActivitySearchType();
        //		search.setMovementCategory(new CodeType(ReferenceSet.MOVEMENT_CATEGORY.value(), "EXTERNAL"));
        //		Set<ExternalMovementActivityType> emas = maSer.search(uc, search, SearchMatchMode.PARTIAL.value()).getExternalMovementActivity();
        //		ids = new HashSet<Long>();
        //		for (ExternalMovementActivityType ema: emas) {
        //			ids.add(ema.getMovementId());
        //		}
        //		assertEquals(ids.size(), 7);
        //
        //		search = new MovementActivitySearchType();
        //		search.setMovementCategory(new CodeType(ReferenceSet.MOVEMENT_CATEGORY.value(), "EXTERNAL"));
        //		ret = maSer.search(uc, search, ids, SearchMatchMode.PARTIAL.value());
        //		assertEquals(ret.getExternalMovementCount().longValue(), 7L);
        //		assertEquals(ret.getExternalMovementActivity().size(), 7);

    }

    @Test(dependsOnMethods = { "testCreate", "testGetCount", "testGetAll", "testGetById", "testGetStamp", "testSearch" }, enabled = true)
    public void testUpdate() {
        Set<String> dataPrivileges = new HashSet<String>();
        dataPrivileges.add("HP");
        // External
        ExternalMovementActivityType ret;
        ExternalMovementActivityType em;
        ExternalMovementActivityType e = (ExternalMovementActivityType) ema;

        // Updating of Movement Category is not allowed.
        e.setMovementCategory("INTERNAL");
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertNotNull(ret, "Update Movement Category is not allowed.");

        e.setMovementCategory("EXTERNAL");//null); // venu/jason/janki asked to ignore movement category

        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertNotNull(ret, "Update Movement Associations is not allowed.");

        // Type
        String type = e.getMovementType();
        String movementType = "TypeTypeType";
        assertFalse(movementType.equals(type));
        e.setMovementType(movementType);
        try {
            ret = (ExternalMovementActivityType) maSer.update(uc, e);
            assertNull(ret, movementType + " is not in the set of reference code.");
        } catch (ArbutusRuntimeException ex) {
            log.info(ex.getMessage());
        }

        movementType = "Trnoj"; // Case sensitive test - Code must be upper case
        assertFalse(movementType.equals(type));
        e.setMovementType(movementType);
        try {
            ret = (ExternalMovementActivityType) maSer.update(uc, e);
            assertNull(ret);
        } catch (ArbutusRuntimeException ex) {
            log.info(ex.getMessage());
        }

        movementType = "TRNIJ";
        assertFalse(movementType.equals(type));
        e.setMovementType(movementType);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);

        em = (ExternalMovementActivityType) ret;
        assertEquals(em.getMovementType(), "TRNIJ");

        em.setMovementType(type);
        assertTrue(this.sameExternalMovementActivity(em, ema));

        // Movement Direction
        String direction = e.getMovementDirection();
        String movementDirection = "DirectionDirection";
        assertFalse(movementDirection.equals(direction));
        e.setMovementDirection(movementDirection);
        try {
            ret = (ExternalMovementActivityType) maSer.update(uc, e);
            assertNull(ret, movementDirection + " is not in the set of reference code.");
        } catch (ArbutusRuntimeException ex) {
            log.info(ex.getMessage());
        }

        e.setMovementDirection(direction);

        // Status
        String status = e.getMovementStatus();
        assertEquals(status, MovementActivityType.MovementStatus.PENDING.code());
        e.setMovementStatus(MovementActivityType.MovementStatus.ONHOLD.code());
        assertEquals(e.getMovementStatus(), MovementActivityType.MovementStatus.ONHOLD.code());
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
/*		Only applicable for SDD 1.0
 * 		assertEquals(
				ret.getReturnCode(),
				ServiceErrorMessages.MSG1003.returnCode(),
				"MovementOutcome is required when status changed to ONHOLD, COMPLETED, CANCELLED, NOSHOW.");*/
        assertNotNull(ret, "Should be SUCCESS according to SDD 2.0");

        assertNull(e.getMovementOutcome());
        e.setMovementOutcome("SICK");
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertNotNull(ret);

        assertEquals(ret.getMovementStatus(), MovementActivityType.MovementStatus.ONHOLD.code());

        e.setMovementStatus(MovementActivityType.MovementStatus.COMPLETED.code());
        try {
            ret = (ExternalMovementActivityType) maSer.update(uc, e);
            assertNull(ret, "Status ONHOLD can be chaged to PENDING only.");
        } catch (ArbutusRuntimeException ex) {
            log.info(ex.getMessage());
        }

        assertEquals(maSer.get(uc, e.getMovementId()).getMovementStatus(), MovementActivityType.MovementStatus.ONHOLD.code());
        e.setMovementStatus(MovementActivityType.MovementStatus.CANCELLED.code());
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertNotNull(ret, "Status ONHOLD can be chaged to PENDING or CANCELLED.");

        e.setMovementStatus(MovementActivityType.MovementStatus.NOSHOW.code());
        try {
            ret = (ExternalMovementActivityType) maSer.update(uc, e);
        } catch (ArbutusRuntimeException ex) {
            log.info(ex.getMessage());
        }

        Date movementDate = e.getMovementDate();
        assertNotNull(movementDate);
        e.setMovementDate(null);

        e.setMovementDate(movementDate);
        assertNotNull(e.getMovementDate());
        e.setMovementStatus(MovementActivityType.MovementStatus.CANCELLED.code());
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertNotNull(ret);

        Set<CommentType> comments = new LinkedHashSet<CommentType>(e.getCommentText());
        assertNotNull(comments);
        assertTrue(comments.size() > 0);
        e.getCommentText().clear();
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
/*		Only applicable for SDD 1.0
 * 		assertEquals(ret.getReturnCode(),
				ServiceErrorMessages.MSG1003.returnCode(),
				"Comment is required when status changed.");*/
        assertNotNull(ret, "Should be success according to SDD 2.0");
        e.getCommentText().addAll(comments);
        assertTrue(comments.size() > 0);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertNotNull(ret);

        // Reason
        String reason = e.getMovementReason();
        assertNotNull(reason);

        e.setMovementReason("COURT");
        ret = (ExternalMovementActivityType) maSer.update(uc, e);

        // Outcome
        String outcome = e.getMovementOutcome();
        assertNotNull(outcome);

        e.setMovementOutcome("EXCUSED");
        assertFalse(e.getMovementOutcome().equals(outcome));
        ret = (ExternalMovementActivityType) maSer.update(uc, e);

        // From Location Association
        e.setFromLocationId(fromLocationAssociation);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertEquals(ret.getFromLocationId(), fromLocationAssociation);

        e.setFromOrganizationId(fromOrganizationAssociation);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertEquals(ret.getFromOrganizationId(), fromOrganizationAssociation);

        e.setFromFacilityId(fromFacilityAssociation);
        e.setToFacilityId(toFacilityAssociation);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertEquals(ret.getFromFacilityId(), fromFacilityAssociation);

        e.setToFacilityId(toFacilityAssociation);
        e.setFromFacilityId(fromFacilityAssociation);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        // assertEquals(ret.getReturnCode(), ServiceErrorMessages.MSG1016.returnCode()); Facility Filtering Removed. Y.Shang Nov.26, 2012
        // assertNull(ret.getExternalMovementActivity());
        assertNotNull(ret);

        e.setToFacilityId(toFacilityAssociation);
        e.setFromFacilityId(toFacilityAssociation);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);

        e.setToLocationId(toLocationAssociation);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertEquals(ret.getToLocationId(), toLocationAssociation);

        e.setToOrganizationId(toOrganizationAssociation);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertEquals(ret.getToOrganizationId(), toOrganizationAssociation);

        e.setToFacilityId(toFacilityAssociation);
        e.setFromFacilityId(fromFacilityAssociation);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertEquals(ret.getToFacilityId(), toFacilityAssociation);

        String fromCity = "LAX";
        e.setFromCity(fromCity);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertEquals(ret.getFromCity(), fromCity);

        String toCity = "Seattle";
        e.setToCity(toCity);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertEquals(ret.getToCity(), toCity);

        String state = "ENGLAND";
        assertFalse(state.equals(e.getToProvinceState()));
        e.setToProvinceState(state);
        try {
            ret = (ExternalMovementActivityType) maSer.update(uc, e);
            assertNull(ret, "Code of Province/State must exist in coresponding set of the reference codes.");
        } catch (ArbutusRuntimeException ex) {
            log.info(ex.getMessage());
        }

        state = "WA";
        assertFalse(state.equals(e.getToProvinceState()));
        e.setToProvinceState(state);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertNotNull(ret);
        assertEquals(ret.getToProvinceState(), "WA");

        e.setApplicationDate(movementDate);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertEquals(ret.getApplicationDate(), movementDate);

        e.setArrestOrganizationId(arrestOrganizationAssociation);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertEquals(ret.getArrestOrganizationId(), arrestOrganizationAssociation);

        e.setEscortOrganizationId(escortOrganizationAssociation);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertEquals(ret.getEscortOrganizationId(), escortOrganizationAssociation);

        e.setReportingDate(movementDate);
        ret = (ExternalMovementActivityType) maSer.update(uc, e);
        assertEquals(ret.getReportingDate(), movementDate);

        // Internal Movement Activity (no facility filtering in this case)
        InternalMovementActivityType inRet;
        InternalMovementActivityType i = (InternalMovementActivityType) ima;

        i.setFromFacilityInternalLocationId(fromFacilityInternalLocationAssociation);
        inRet = (InternalMovementActivityType) maSer.update(uc, i);  // was used to test that dynamic associations cannot be changed
        assertNotNull(inRet);  //, but based on the latest direction from Venu
        // about movement category (Sep 27, 2012), the same rule applies to dynamic associations and
        // they will be ignored.

        i.setMovementCategory(null);
        i.setFromFacilityInternalLocationId(fromFacilityInternalLocationAssociation);
        try {
            inRet = (InternalMovementActivityType) maSer.update(uc, i);
            assertNull(inRet);
        } catch (ArbutusRuntimeException ex) {
            log.info(ex.getMessage());
        }

        i.setMovementCategory("INTERNAL");
        i.setToFacilityInternalLocationId(toFacilityInternalLocationAssociation);
        inRet = (InternalMovementActivityType) maSer.update(uc, i);
        assertEquals(inRet.getToFacilityInternalLocationId(), toFacilityInternalLocationAssociation);
    }

    @Test(dependsOnMethods = { "testUpdate" }, enabled = false)
    public void DefectID_1269() {
        InternalMovementActivityType ima = new InternalMovementActivityType();
        ima.setSupervisionId(300L);
        ima.setMovementCategory("INTERNAL");
        ima.setMovementType("CRT");
        ima.setMovementDirection("OUT");
        ima.setMovementStatus("PENDING");
        ima.setMovementReason("LA");
        ima.setMovementOutcome("SICK");
        ima.setActivityId(10000L);

        ima.setFromFacilityInternalLocationId(null);
        // ima.setFromFacilityInternalLocationAssociation(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 1L));
        ima.setToFacilityInternalLocationId(6L);

        InternalMovementActivityType ret = (InternalMovementActivityType) maSer.create(uc, ima, false);
        assertNotNull(ret);

    }

    @Test(dependsOnMethods = { "testUpdate" }, enabled = true)
    public void LastCompletedSupervisionMovement() {
        Calendar c = Calendar.getInstance();

        Long supervisionAssociation = supervisionId;

        ExternalMovementActivityType ema = new ExternalMovementActivityType();
        ema.setSupervisionId(supervisionAssociation);
        ema.setMovementCategory("EXTERNAL");
        ema.setMovementType("CRT");
        ema.setMovementDirection("IN");
        ema.setMovementStatus("COMPLETED");
        ema.setMovementReason("LA");
        ema.setMovementOutcome("SICK");

        ema.setMovementDate(c.getTime());

        ema.setActivityId(activityId1);

        ema.setToFacilityInternalLocationId(toFacilityInternalLocationAssociation);

        ExternalMovementActivityType exRet = (ExternalMovementActivityType) maSer.create(uc, ema, false);
        assertNotNull(exRet);

        // getLastCompletedSupervisionMovementBySupervisions
        Set<Long> supervisions = new HashSet<Long>();
        supervisions.add(supervisionAssociation);
        Set<LastCompletedSupervisionMovementType> lastMove = maSer.getLastCompletedSupervisionMovementBySupervisions(uc, supervisions);
        assertNotNull(lastMove);
        assertEquals(lastMove.size(), 1);
        LastCompletedSupervisionMovementType lMove = (LastCompletedSupervisionMovementType) (lastMove.toArray()[0]);
        assertEquals(lMove.getSupervisionId(), supervisionAssociation);
        assertEquals(lMove.getLastCompletedMovementActivity().getActivityId(), activityId1);
        assertEquals(lMove.getCurrentInternalLocationId(), toFacilityInternalLocationAssociation);
        assertEquals(lMove.getLastCompletedMovementActivity().getMovementId(), exRet.getMovementId());

        // getLastCompletedSupervisionMovementByLocations
        Set<Long> locations = new HashSet<Long>();
        locations.add(toFacilityInternalLocationAssociation);
        lastMove = maSer.getLastCompletedSupervisionMovementByLocations(uc, locations);
        assertNotNull(lastMove);
        assertEquals(lastMove.size(), 1);
        lMove = (LastCompletedSupervisionMovementType) (lastMove.toArray()[0]);
        assertEquals(lMove.getSupervisionId(), supervisionAssociation);
        assertEquals(lMove.getLastCompletedMovementActivity().getActivityId(), activityId1);
        assertEquals(lMove.getCurrentInternalLocationId(), toFacilityInternalLocationAssociation);

        // Internal
        InternalMovementActivityType ima = new InternalMovementActivityType();
        ima.setSupervisionId(supervisionAssociation);
        ima.setMovementCategory("INTERNAL");
        ima.setMovementType("BED");
        ima.setMovementDirection("IN");
        ima.setMovementStatus("COMPLETED");
        ima.setMovementReason("ADM");

        c = Calendar.getInstance();
        ima.setMovementDate(c.getTime());

        ima.setActivityId(activityId2);

        ima.setFromFacilityInternalLocationId(fromFacilityInternalLocationAssociation);
        ima.setToFacilityInternalLocationId(toFacilityInternalLocationAssociation);

        InternalMovementActivityType inRet = (InternalMovementActivityType) maSer.create(uc, ima, false);
        assertNotNull(inRet);
        Long inId = inRet.getMovementId();

        supervisions = new HashSet<Long>();
        supervisions.add(supervisionAssociation);
        lastMove = maSer.getLastCompletedSupervisionMovementBySupervisions(uc, supervisions);
        assertNotNull(lastMove);
        assertEquals(lastMove.size(), 1);
        lMove = (LastCompletedSupervisionMovementType) (lastMove.toArray()[0]);
        assertEquals(lMove.getSupervisionId(), supervisionAssociation);
        assertEquals(lMove.getLastCompletedMovementActivity().getMovementId(), inRet.getMovementId(),
                "Should be equal because the moset recent MovementAsctivty has not been deleted.");
        assertEquals(lMove.getLastCompletedMovementActivity().getMovementId(), inId, "Should be equal to the first Internal Movement.");

        ima = new InternalMovementActivityType();
        ima.setSupervisionId(supervisionAssociation);
        ima.setMovementCategory("INTERNAL");
        ima.setMovementType("BED");
        ima.setMovementDirection("IN");
        ima.setMovementStatus("COMPLETED");
        ima.setMovementReason("ADM");

        c = Calendar.getInstance();
        ima.setMovementDate(c.getTime());

        ima.setActivityId(activityId3);

        ima.setFromFacilityInternalLocationId(this.fromFacilityInternalLocationAssociation);
        ima.setToFacilityInternalLocationId(this.toFacilityInternalLocationAssociation);

        inRet = (InternalMovementActivityType) maSer.create(uc, ima, false);
        assertNotNull(inRet);
        maSer.delete(uc, inRet.getMovementId());

        supervisions = new HashSet<Long>();
        supervisions.add(supervisionAssociation);
        lastMove = maSer.getLastCompletedSupervisionMovementBySupervisions(uc, supervisions);
        assertNotNull(lastMove);
        assertEquals(lastMove.size(), 1);
        lMove = (LastCompletedSupervisionMovementType) (lastMove.toArray()[0]);
        assertEquals(lMove.getSupervisionId(), supervisionAssociation);
        assertNotEquals(lMove.getLastCompletedMovementActivity().getMovementId(), inRet.getMovementId(),
                "Should not be equal because the moset recent MovementAsctivty has just been deleted.");
        assertEquals(lMove.getLastCompletedMovementActivity().getMovementId(), inId,
                "Should be equal to the Internal Movement before this one just deleted.");

        // External, OUT
        ema.setMovementDirection("OUT");
        ema.setActivityId(activityId4);
        c = Calendar.getInstance();
        ema.setMovementDate(c.getTime());
        // Create EMA
        exRet = (ExternalMovementActivityType) maSer.create(uc, ema, false);

        Long exId = exRet.getMovementId();
        assertNotNull(exId);
        // Delete the just created EMA
        Long exDeleteRet = maSer.delete(uc, exId);
        assertEquals(exDeleteRet, success);

        // Get Last Completed SupervisionMovement
        supervisions = new HashSet<Long>();
        supervisions.add(supervisionAssociation);
        lastMove = maSer.getLastCompletedSupervisionMovementBySupervisions(uc, supervisions);
        assertNotNull(lastMove);
        assertEquals(lastMove.size(), 1);
        lMove = (LastCompletedSupervisionMovementType) (lastMove.toArray()[0]);
        assertEquals(lMove.getSupervisionId(), supervisionAssociation);
        assertNotEquals(lMove.getLastCompletedMovementActivity().getMovementId(), exId,
                "Should not equals because the moset recent MovementAsctivty has just been deleted.");
        assertEquals(lMove.getLastCompletedMovementActivity().getMovementId(), inId, "Should be equal to the First Internal Movement.");
    }

    @Test(dependsOnMethods = { "testUpdate" }, enabled = true)
    public void LastCompletedSupervisionMovement2() {

        Calendar c = Calendar.getInstance();

        InternalMovementActivityType ima = new InternalMovementActivityType();
        ima.setSupervisionId(supervisionId2);
        ima.setMovementCategory("INTERNAL");
        ima.setMovementType("CRT");
        ima.setMovementDirection("IN");
        ima.setMovementStatus("COMPLETED");
        ima.setMovementReason("LA");
        ima.setMovementOutcome("SICK");

        Date movementDate = c.getTime();
        ima.setMovementDate(movementDate);

        ima.setActivityId(activityId5);

        ima.setFromFacilityInternalLocationId(this.fromFacilityInternalLocationAssociation);
        ima.setToFacilityInternalLocationId(this.toFacilityInternalLocationAssociation);

        InternalMovementActivityType inRet = (InternalMovementActivityType) maSer.create(uc, ima, false);
        assertNotNull(inRet);

        Set<Long> locations = new HashSet<Long>();
        locations.add(this.toFacilityInternalLocationAssociation);
        Set<LastCompletedSupervisionMovementType> lastMove = maSer.getLastCompletedSupervisionMovementByLocations(uc, locations);
        assertNotNull(lastMove);
        assertEquals(lastMove.size(), 2);

        Long maRet = maSer.delete(uc, inRet.getMovementId());
        assertEquals(maRet, success);

        lastMove = maSer.getLastCompletedSupervisionMovementByLocations(uc, locations);
        assertNotNull(lastMove);
        assertEquals(lastMove.size(), 1);

    }

    @Test(dependsOnMethods = { "testUpdate" }, enabled = true)
    public void testDeleteById() {
        Long count = maSer.getCount(uc);
        assertNotNull(count);
        assertTrue(count.intValue() > 0);

        assertEquals(maSer.get(uc, exId).getMovementId(), exId);
        Long ret = maSer.delete(uc, exId);
        assertEquals(ret, success);
        try {
            assertNull(maSer.get(uc, exId));
        } catch (ArbutusRuntimeException ex) {
            log.info(ex.getMessage());
        }

        assertEquals(maSer.getCount(uc).intValue(), count - 1);
        count--;

        Set<Long> IDs = maSer.getAll(uc);
        assertNotNull(IDs);
        for (Long id : IDs) {
            Long deleteRet = maSer.delete(uc, id);
            assertNotNull(deleteRet);
            assertEquals(maSer.getCount(uc).intValue(), count - 1);
            count--;
        }
        assertEquals(count.intValue(), 0);
    }

    @Test(dependsOnMethods = { "testDeleteById" }, enabled = true)
    public void testDeleteAll() {
        Set<Long> ids = maSer.getAll(uc);
        assertNotNull(ids);
        assertTrue(ids.size() == 0);

        assertEquals(maSer.deleteAll(uc), success);
        assertEquals(maSer.getAll(uc).size(), 0);

    }

    private boolean sameExternalMovementActivity(ExternalMovementActivityType e1, ExternalMovementActivityType e2) {
        //		assertEquals(e1.getMovementId(),      e2.getMovementId());
        //		assertEquals(e1.getMovementCategory().getCode(),  e2.getMovementCategory().getCode());
        //		assertEquals(e1.getMovementType().getCode(),      e2.getMovementType().getCode());
        //		assertEquals(e1.getMovementDirection().getCode(), e2.getMovementDirection().getCode());
        //		assertEquals(e1.getMovementStatus().getCode(),    e2.getMovementStatus().getCode());
        //		assertEquals(e1.getMovementReason().getCode(),    e2.getMovementReason().getCode());
        //		assertEquals(e1.getMovementDate(), e2.getMovementDate());
        //		assertEquals(e1.getFromCity(),     e2.getFromCity());
        //		assertEquals(e1.getToCity(),       e2.getToCity());
        //		assertEquals(e1.getCommentText().size(),      e2.getCommentText().size());
        //		assertEquals(e1.getAssociations().size(),     e2.getAssociations().size());
        //		assertEquals(e1.getFromLocationId(), e2.getFromLocationId());
        //		assertEquals(e1.getFromFacilityId(), e2.getFromFacilityId());
        //		assertEquals(e1.getFromOrganizationId(), e2.getFromOrganizationId());
        //		assertEquals(e1.getApplicationDate(), e2.getApplicationDate());
        //		assertEquals(e1.getArrestOrganizationId(), e2.getArrestOrganizationId());
        //		assertEquals(e1.getEscortOrganizationId(), e2.getEscortOrganizationId());
        //		assertEquals(e1.getReportingDate(),   e2.getReportingDate());
        //		assertEquals(e1.getToProvinceState(), e2.getToProvinceState());
        //		assertEquals(e1.getToFacilityId(), e2.getToFacilityId());
        //		assertEquals(e1.getToLocationId(), e2.getToLocationId());
        //		assertEquals(e1.getToOrganizationId(), e2.getToOrganizationId());
        return true;
    }

    private boolean sameInternalMovementActivity(InternalMovementActivityType e1, InternalMovementActivityType e2) {
        //		assertEquals(e1.getMovementId(),      e2.getMovementId());
        //		assertEquals(e1.getMovementCategory().getCode(),  e2.getMovementCategory().getCode());
        //		assertEquals(e1.getMovementType().getCode(),      e2.getMovementType().getCode());
        //		assertEquals(e1.getMovementDirection().getCode(), e2.getMovementDirection().getCode());
        //		assertEquals(e1.getMovementStatus().getCode(),    e2.getMovementStatus().getCode());
        //		assertEquals(e1.getMovementReason().getCode(),    e2.getMovementReason().getCode());
        //		assertEquals(e1.getMovementDate(),        e2.getMovementDate());
        //		assertEquals(e1.getCommentText().size(),  e2.getCommentText().size());
        //		assertEquals(e1.getAssociations().size(), e2.getAssociations().size());
        //		assertEquals(e1.getFromFacilityInternalLocationAssociation(),
        //				e2.getFromFacilityInternalLocationAssociation());
        //		assertEquals(e1.getToFacilityInternalLocationAssociation(),
        //				e2.getToFacilityInternalLocationAssociation());
        return true;
    }

}