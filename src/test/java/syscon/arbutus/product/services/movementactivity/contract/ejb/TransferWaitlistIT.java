package syscon.arbutus.product.services.movementactivity.contract.ejb;

import javax.naming.NamingException;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.common.SupervisionTest;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.movement.contract.dto.TransferWaitlistEntryType;
import syscon.arbutus.product.services.movement.contract.dto.TransferWaitlistSearchType;
import syscon.arbutus.product.services.movement.contract.dto.TransferWaitlistType;
import syscon.arbutus.product.services.movement.contract.dto.TransferWaitlistsReturnType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;

import static org.testng.Assert.*;

@ModuleConfig()
public class TransferWaitlistIT extends BaseIT {

    final static Long success = 1L;
    MovementService maSer;
    private Logger log = LoggerFactory.getLogger(MovementActivityServiceBusinessLogicTestIT.class);
    private Long[] supervisionIDs = new Long[] { 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L };
    private Long[] facilityIDs = new Long[] { 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L };
    private Long[] toFacilityIDs = new Long[] { 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L };

    @BeforeMethod
    public void beforeMethod() {

    }

    @BeforeClass
    public void beforeClass() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");

        maSer = (MovementService) JNDILookUp(this.getClass(), MovementService.class);

        uc = super.initUserContext();

        FacilityTest fac = new FacilityTest();
        for (int i = 0; i < facilityIDs.length; i++) {
            facilityIDs[i] = fac.createFacility();
        }
        for (int i = 0; i < toFacilityIDs.length; i++) {
            toFacilityIDs[i] = fac.createFacility();
        }

        SupervisionTest sup = new SupervisionTest();
        for (int i = 0; i < supervisionIDs.length; i++) {
            supervisionIDs[i] = sup.createSupervision(toFacilityIDs[i], true);
        }

        if (log.isDebugEnabled()) {
            log.debug("beforeTest: end");
        }
    }

    @AfterClass
    public void afterClass() {
        maSer.deleteAll(uc);
        SupervisionTest sup = new SupervisionTest();
        for (int i = 0; i < supervisionIDs.length; i++) {
            sup.deleteSupervision(supervisionIDs[i]);
        }

        FacilityTest fac = new FacilityTest();
        for (int i = 0; i < facilityIDs.length; i++) {
            fac.deleteFacility(facilityIDs[i]);
        }
        for (int i = 0; i < toFacilityIDs.length; i++) {
            fac.deleteFacility(toFacilityIDs[i]);
        }
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(maSer);
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testReset() {
        Long returnCode = maSer.deleteAll(uc);
        assertEquals(returnCode, success);
    }

    @Test(dependsOnMethods = { "testReset" }, enabled = true)
    public void Defect1484() {
        TransferWaitlistType ret;

        Long facilityId = facilityIDs[0];
        Set<TransferWaitlistEntryType> transferWaitlistEntries = new HashSet<TransferWaitlistEntryType>();
        TransferWaitlistEntryType entry = new TransferWaitlistEntryType();

        entry.setSupervisionId(supervisionIDs[0]);
        entry.setDateAdded(new Date());
        entry.setPriority("MEDIUM");
        entry.setToFacilityId(toFacilityIDs[0]);
        entry.setTransferReason("BAILR");

        transferWaitlistEntries.add(entry);

        entry = new TransferWaitlistEntryType();
        entry.setSupervisionId(supervisionIDs[1]);
        entry.setDateAdded(new Date());
        entry.setPriority("MEDIUM");
        entry.setToFacilityId(toFacilityIDs[1]);
        entry.setTransferReason("BAILR");

        transferWaitlistEntries.add(entry);

        ret = maSer.addTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
        assertNotNull(ret);

        transferWaitlistEntries.clear();
        for (TransferWaitlistEntryType e : ret.getTransferWaitlistEntries()) {
            if (e.getSupervisionId() == supervisionIDs[0]) {
                e.setToFacilityId(toFacilityIDs[1]);
            }
            transferWaitlistEntries.add(e);
        }
        ret = maSer.updateTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
        assertNotNull(ret);
    }

    @Test(dependsOnMethods = { "testReset" }, enabled = true)
    public void addTransferWaitlistEntry() {
        Long facilityId = facilityIDs[1];
        Set<TransferWaitlistEntryType> transferWaitlistEntries = new HashSet<TransferWaitlistEntryType>();
        TransferWaitlistEntryType entry = new TransferWaitlistEntryType();

        TransferWaitlistType ret;
        transferWaitlistEntries.clear();

        entry = new TransferWaitlistEntryType();
        entry.setSupervisionId(supervisionIDs[1]);
        entry.setDateAdded(new Date());
        entry.setPriority("HIGH");
        entry.setToFacilityId(toFacilityIDs[1]);
        entry.setTransferReason("ESCP");

        transferWaitlistEntries.add(entry);

        entry = new TransferWaitlistEntryType();
        entry.setSupervisionId(supervisionIDs[2]);
        entry.setDateAdded(new Date());
        entry.setPriority("LOW");
        entry.setToFacilityId(toFacilityIDs[2]);
        entry.setTransferReason("VIS");

        transferWaitlistEntries.add(entry);

        ret = maSer.addTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
        assertNotNull(ret);

        try {
            ret = maSer.addTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
            //		assertEquals(ret.getReturnCode(), ReturnCode.RInvalidInput1003.returnCode(),
            //				"For a given facility’s transfer waitlist, an offender can only be listed once within the waitlist.");
            assertNull(ret);
        } catch (ArbutusRuntimeException e) {
            log.info(e.getMessage());
        }
    }

    @Test(dependsOnMethods = { "testReset" }, enabled = true)
    public void updateTransferWaitlistEntry() {
        TransferWaitlistType ret;

        // Add
        Long facilityId = facilityIDs[2];
        Set<TransferWaitlistEntryType> transferWaitlistEntries = new HashSet<TransferWaitlistEntryType>();
        TransferWaitlistEntryType entry = new TransferWaitlistEntryType();

        entry.setSupervisionId(supervisionIDs[1]);
        entry.setDateAdded(new Date());
        entry.setPriority("HIGH");
        entry.setToFacilityId(toFacilityIDs[1]);
        entry.setTransferReason("ESCP");

        transferWaitlistEntries.add(entry);

        entry = new TransferWaitlistEntryType();
        entry.setSupervisionId(supervisionIDs[2]);
        entry.setDateAdded(new Date());
        entry.setPriority("LOW");
        entry.setToFacilityId(toFacilityIDs[2]);
        entry.setTransferReason("VIS");

        transferWaitlistEntries.add(entry);

        ret = maSer.addTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
        assertNotNull(ret);

        // Update
        transferWaitlistEntries.clear();
        entry.setSupervisionId(supervisionIDs[1]);
        entry.setDateAdded(new Date());
        entry.setPriority("HIGH");
        entry.setToFacilityId(toFacilityIDs[1]);
        entry.setTransferReason("ADM");

        transferWaitlistEntries.add(entry);

        entry = new TransferWaitlistEntryType();
        entry.setSupervisionId(supervisionIDs[2]);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 1);
        entry.setDateAdded(c.getTime());
        entry.setPriority("MEDIUM");
        entry.setToFacilityId(toFacilityIDs[2]);
        entry.setTransferReason("LA");

        transferWaitlistEntries.add(entry);

        ret = maSer.updateTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
        assertNotNull(ret);
        assertEquals(ret.getTransferWaitlistEntries(), transferWaitlistEntries);
    }

    @Test(dependsOnMethods = { "testReset" }, enabled = true)
    public void getTransferWaitlistEntry() {
        // Add
        Long facilityId = facilityIDs[3];
        TransferWaitlistType ret;

        Set<TransferWaitlistEntryType> transferWaitlistEntries = new HashSet<TransferWaitlistEntryType>();
        TransferWaitlistEntryType entry1 = new TransferWaitlistEntryType();
        entry1 = new TransferWaitlistEntryType();
        entry1.setSupervisionId(supervisionIDs[1]);
        entry1.setDateAdded(new Date());
        entry1.setPriority("HIGH");
        entry1.setToFacilityId(toFacilityIDs[1]);
        entry1.setTransferReason("ESCP");

        transferWaitlistEntries.add(entry1);

        TransferWaitlistEntryType entry2 = new TransferWaitlistEntryType();
        entry2.setSupervisionId(supervisionIDs[2]);
        entry2.setDateAdded(new Date());
        entry2.setPriority("LOW");
        entry2.setToFacilityId(toFacilityIDs[2]);
        entry2.setTransferReason("VIS");

        transferWaitlistEntries.add(entry2);

        ret = maSer.addTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
        assertNotNull(ret);

        // Get
        TransferWaitlistEntryType entryRet;
        ;
        entryRet = maSer.getTransferWaitlistEntry(uc, facilityId, supervisionIDs[1]);
        assertNotNull(entryRet);
        assertEquals(entryRet, entry1);

        entryRet = maSer.getTransferWaitlistEntry(uc, facilityId, supervisionIDs[2]);
        assertNotNull(entryRet);
        assertEquals(entryRet, entry2);
    }

    @Test(dependsOnMethods = { "testReset" }, enabled = true)
    public void getTransferWaitlist() {
        TransferWaitlistType ret;

        // Add
        Long facilityId = facilityIDs[4];
        Set<TransferWaitlistEntryType> transferWaitlistEntries = new HashSet<TransferWaitlistEntryType>();
        TransferWaitlistEntryType entry = new TransferWaitlistEntryType();

        Long supervision1 = supervisionIDs[1];
        entry.setSupervisionId(supervision1);
        entry.setDateAdded(new Date());
        entry.setPriority("HIGH");
        entry.setToFacilityId(toFacilityIDs[1]);
        entry.setTransferReason("ESCP");

        transferWaitlistEntries.add(entry);

        entry = new TransferWaitlistEntryType();
        Long supervision2 = supervisionIDs[2];
        entry.setSupervisionId(supervision2);
        entry.setDateAdded(new Date());
        entry.setPriority("LOW");
        entry.setToFacilityId(toFacilityIDs[2]);
        entry.setTransferReason("VIS");

        transferWaitlistEntries.add(entry);

        ret = maSer.addTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
        assertNotNull(ret);

        facilityId = facilityIDs[5];

        transferWaitlistEntries = new HashSet<TransferWaitlistEntryType>();
        TransferWaitlistEntryType entry1 = new TransferWaitlistEntryType();
        entry1 = new TransferWaitlistEntryType();
        entry1.setSupervisionId(supervisionIDs[3]);
        entry1.setDateAdded(new Date());
        entry1.setPriority("HIGH");
        entry1.setToFacilityId(toFacilityIDs[3]);
        entry1.setTransferReason("ESCP");

        transferWaitlistEntries.add(entry1);

        TransferWaitlistEntryType entry2 = new TransferWaitlistEntryType();
        entry2.setSupervisionId(supervisionIDs[4]);
        entry2.setDateAdded(new Date());
        entry2.setPriority("LOW");
        entry2.setToFacilityId(toFacilityIDs[4]);
        entry2.setTransferReason("VIS");

        transferWaitlistEntries.add(entry2);

        ret = maSer.addTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
        assertNotNull(ret);

        // Get
        TransferWaitlistType waitRet = maSer.getTransferWaitlist(uc, facilityId);
        assertNotNull(waitRet);
        assertEquals(waitRet.getFacilityId(), facilityId);
        assertTrue(waitRet.getTransferWaitlistEntries().contains(entry1));
        assertTrue(waitRet.getTransferWaitlistEntries().contains(entry2));
        assertFalse(waitRet.getTransferWaitlistEntries().contains(entry));

        facilityId = facilityIDs[4];
        waitRet = maSer.getTransferWaitlist(uc, facilityId);
        assertNotNull(waitRet);
        assertEquals(waitRet.getFacilityId(), facilityId);
        assertFalse(waitRet.getTransferWaitlistEntries().contains(entry1));
        assertFalse(waitRet.getTransferWaitlistEntries().contains(entry2));
        assertTrue(waitRet.getTransferWaitlistEntries().contains(entry));
    }

    @Test(dependsOnMethods = { "testReset" }, enabled = true)
    public void deleteTransferWaitlist() {
        TransferWaitlistType ret;

        // Add
        Long facilityId = facilityIDs[6];
        Set<TransferWaitlistEntryType> transferWaitlistEntries = new HashSet<TransferWaitlistEntryType>();
        TransferWaitlistEntryType entry = new TransferWaitlistEntryType();

        entry.setSupervisionId(supervisionIDs[1]);
        entry.setDateAdded(new Date());
        entry.setPriority("HIGH");
        entry.setToFacilityId(toFacilityIDs[1]);
        entry.setTransferReason("ESCP");

        transferWaitlistEntries.add(entry);

        entry = new TransferWaitlistEntryType();
        entry.setSupervisionId(supervisionIDs[2]);
        entry.setDateAdded(new Date());
        entry.setPriority("LOW");
        entry.setToFacilityId(toFacilityIDs[2]);
        entry.setTransferReason("VIS");

        transferWaitlistEntries.add(entry);

        ret = maSer.addTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
        assertNotNull(ret);

        facilityId = facilityIDs[7];

        transferWaitlistEntries = new HashSet<TransferWaitlistEntryType>();
        TransferWaitlistEntryType entry1 = new TransferWaitlistEntryType();
        entry1 = new TransferWaitlistEntryType();
        entry1.setSupervisionId(supervisionIDs[3]);
        entry1.setDateAdded(new Date());
        entry1.setPriority("HIGH");
        entry1.setToFacilityId(toFacilityIDs[3]);
        entry1.setTransferReason("ESCP");

        transferWaitlistEntries.add(entry1);

        TransferWaitlistEntryType entry2 = new TransferWaitlistEntryType();
        entry2.setSupervisionId(supervisionIDs[4]);
        entry2.setDateAdded(new Date());
        entry2.setPriority("LOW");
        entry2.setToFacilityId(toFacilityIDs[4]);
        entry2.setTransferReason("VIS");

        transferWaitlistEntries.add(entry2);

        ret = maSer.addTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
        assertNotNull(ret);

        // Get
        TransferWaitlistType waitRet = maSer.getTransferWaitlist(uc, facilityId);
        assertNotNull(waitRet);
        assertEquals(waitRet.getFacilityId(), facilityId);
        assertTrue(waitRet.getTransferWaitlistEntries().contains(entry1));
        assertTrue(waitRet.getTransferWaitlistEntries().contains(entry2));
        assertFalse(waitRet.getTransferWaitlistEntries().contains(entry));

        // Delete facilityId = 7
        facilityId = facilityIDs[7];

        // Get facilityId = 7 after delete
        try {
            waitRet = maSer.getTransferWaitlist(uc, facilityId);
            assertNull(waitRet.getFacilityId());
        } catch (ArbutusRuntimeException e) {
            log.info(e.getMessage());
        }

        // Get facilityId = 6
        facilityId = facilityIDs[6];
        waitRet = maSer.getTransferWaitlist(uc, facilityId);
        assertNotNull(waitRet);
        assertEquals(waitRet.getFacilityId(), facilityId);
        assertFalse(waitRet.getTransferWaitlistEntries().contains(entry1));
        assertFalse(waitRet.getTransferWaitlistEntries().contains(entry2));
        assertTrue(waitRet.getTransferWaitlistEntries().contains(entry));

    }

    @Test(dependsOnMethods = { "testReset" }, enabled = true)
    public void deleteTransferWaitlistEntry() {
        TransferWaitlistType ret;

        // Add
        Long facilityId = facilityIDs[8];
        Set<TransferWaitlistEntryType> transferWaitlistEntries = new HashSet<TransferWaitlistEntryType>();
        TransferWaitlistEntryType entry = new TransferWaitlistEntryType();

        Long supervision1 = supervisionIDs[1];
        entry.setSupervisionId(supervision1);
        entry.setDateAdded(new Date());
        entry.setPriority("HIGH");
        entry.setToFacilityId(toFacilityIDs[1]);
        entry.setTransferReason("ESCP");

        transferWaitlistEntries.add(entry);

        entry = new TransferWaitlistEntryType();
        Long supervision2 = supervisionIDs[2];
        entry.setSupervisionId(supervision2);
        entry.setDateAdded(new Date());
        entry.setPriority("LOW");
        entry.setToFacilityId(toFacilityIDs[2]);
        entry.setTransferReason("VIS");

        transferWaitlistEntries.add(entry);

        ret = maSer.addTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
        assertNotNull(ret);

        facilityId = facilityIDs[9];

        transferWaitlistEntries = new HashSet<TransferWaitlistEntryType>();
        TransferWaitlistEntryType entry1 = new TransferWaitlistEntryType();
        entry1 = new TransferWaitlistEntryType();
        entry1.setSupervisionId(supervisionIDs[3]);
        entry1.setDateAdded(new Date());
        entry1.setPriority("HIGH");
        entry1.setToFacilityId(toFacilityIDs[3]);
        entry1.setTransferReason("ESCP");

        transferWaitlistEntries.add(entry1);

        TransferWaitlistEntryType entry2 = new TransferWaitlistEntryType();
        entry2.setSupervisionId(supervisionIDs[4]);
        entry2.setDateAdded(new Date());
        entry2.setPriority("LOW");
        entry2.setToFacilityId(toFacilityIDs[4]);
        entry2.setTransferReason("VIS");

        transferWaitlistEntries.add(entry2);

        ret = maSer.addTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
        assertNotNull(ret);

        // Get
        TransferWaitlistType waitRet = maSer.getTransferWaitlist(uc, facilityId);
        assertNotNull(waitRet);
        assertEquals(waitRet.getFacilityId(), facilityId);
        assertTrue(waitRet.getTransferWaitlistEntries().contains(entry1));
        assertTrue(waitRet.getTransferWaitlistEntries().contains(entry2));
        assertFalse(waitRet.getTransferWaitlistEntries().contains(entry));

        // Delete facilityId = 9, supervisions = 10000, 20000
        /*waitRet = maSer.deleteTransferWaitlistEntry(uc, facilityId, new HashSet<Long>(Arrays.asList(new Long[] { supervisionIDs[3], supervisionIDs[4] })));
        assertNotNull(waitRet);
        assertEquals(waitRet.getFacilityId(), facilityId);
        assertTrue(waitRet.getTransferWaitlistEntries().contains(entry1));
        assertTrue(waitRet.getTransferWaitlistEntries().contains(entry2));
        assertFalse(waitRet.getTransferWaitlistEntries().contains(entry));*/

        // Get facilityId = 9
        facilityId = facilityIDs[9];
        try {
            waitRet = maSer.getTransferWaitlist(uc, facilityId);
            assertNull(waitRet);
        } catch (ArbutusRuntimeException ex) {
            log.info(ex.getMessage());
        }

        // Get facilityId = 8
        facilityId = facilityIDs[8];
        waitRet = maSer.getTransferWaitlist(uc, facilityId);
        assertNotNull(waitRet);
        assertEquals(waitRet.getFacilityId(), facilityId);
        assertFalse(waitRet.getTransferWaitlistEntries().contains(entry1));
        assertFalse(waitRet.getTransferWaitlistEntries().contains(entry2));
        assertTrue(waitRet.getTransferWaitlistEntries().contains(entry));

    }

    @Test(dependsOnMethods = { "testReset", "addTransferWaitlistEntry", "updateTransferWaitlistEntry", "getTransferWaitlistEntry", "getTransferWaitlist",
            "deleteTransferWaitlist", "deleteTransferWaitlistEntry" }, enabled = true)
    public void searchTransferWaitlist() {
        Long returnCode = maSer.deleteAll(uc);
        assertEquals(returnCode, success);

        TransferWaitlistType ret;

        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();

        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date yesterday = cal.getTime();

        cal.add(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.MONTH, -1);
        Date lastMonth = cal.getTime();

        cal.add(Calendar.MONTH, 1);
        cal.add(Calendar.YEAR, -1);
        Date lastYear = cal.getTime();

        cal = Calendar.getInstance(); // date time reset

        // Add
        Long facilityId = facilityIDs[10];
        Set<TransferWaitlistEntryType> transferWaitlistEntries = new HashSet<TransferWaitlistEntryType>();
        TransferWaitlistEntryType entry = new TransferWaitlistEntryType();

        Long supervision1 = supervisionIDs[1];
        entry.setSupervisionId(supervision1);
        entry.setDateAdded(today);
        entry.setPriority("HIGH");
        entry.setToFacilityId(toFacilityIDs[1]);
        entry.setTransferReason("ESCP");

        transferWaitlistEntries.add(entry);

        entry = new TransferWaitlistEntryType();
        Long supervision2 = supervisionIDs[2];
        entry.setSupervisionId(supervision2);
        entry.setDateAdded(yesterday);
        entry.setPriority("LOW");
        entry.setToFacilityId(toFacilityIDs[2]);
        entry.setTransferReason("VIS");

        transferWaitlistEntries.add(entry);

        ret = maSer.addTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
        assertNotNull(ret);

        facilityId = facilityIDs[11];
        transferWaitlistEntries = new HashSet<TransferWaitlistEntryType>();
        entry = new TransferWaitlistEntryType();

        Long supervision3 = supervisionIDs[3];
        entry.setSupervisionId(supervision3);
        entry.setDateAdded(lastMonth);
        entry.setPriority("HIGH");
        entry.setToFacilityId(toFacilityIDs[3]);
        entry.setTransferReason("ESCP");

        transferWaitlistEntries.add(entry);

        entry = new TransferWaitlistEntryType();
        Long supervision4 = supervisionIDs[4];
        entry.setSupervisionId(supervision4);
        entry.setDateAdded(lastYear);
        entry.setPriority("LOW");
        entry.setToFacilityId(toFacilityIDs[4]);
        entry.setTransferReason("VIS");

        transferWaitlistEntries.add(entry);

        ret = maSer.addTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
        assertNotNull(ret);

        facilityId = facilityIDs[12];

        transferWaitlistEntries = new HashSet<TransferWaitlistEntryType>();
        TransferWaitlistEntryType entry1 = new TransferWaitlistEntryType();
        entry1 = new TransferWaitlistEntryType();
        entry1.setSupervisionId(supervisionIDs[5]);
        entry1.setDateAdded(today);
        entry1.setPriority("HIGH");
        entry1.setToFacilityId(toFacilityIDs[5]);
        entry1.setTransferReason("ESCP");

        transferWaitlistEntries.add(entry1);

        TransferWaitlistEntryType entry2 = new TransferWaitlistEntryType();
        entry2.setSupervisionId(supervisionIDs[6]);
        entry2.setDateAdded(lastYear);
        entry2.setPriority("LOW");
        entry2.setToFacilityId(toFacilityIDs[6]);
        entry2.setTransferReason("VIS");

        transferWaitlistEntries.add(entry2);

        ret = maSer.addTransferWaitlistEntry(uc, facilityId, transferWaitlistEntries);
        assertNotNull(ret);

        // Search
        TransferWaitlistSearchType search = new TransferWaitlistSearchType();

        TransferWaitlistsReturnType rets;
        // Facility
        search.setFacilityId(20000L);
        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        assertNotNull(rets.getTransferWaitlists());
        assertEquals(rets.getTotalSize().longValue(), 0L);

        facilityId = facilityIDs[10];
        search.setFacilityId(facilityId);
        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        //assertEquals(rets.getTotalSize().longValue(), 1L);
        assertEquals(rets.getTotalSize().longValue(), 2L);
        assertNotNull(rets.getTransferWaitlists());
        assertEquals(rets.getTransferWaitlists().size(), 1);

        ret = maSer.getTransferWaitlist(uc, facilityId);

        assertEquals(ret, rets.getTransferWaitlists().toArray()[0]);

        // Supervision
        search = new TransferWaitlistSearchType();
        search.setSupervisionId(12345L);

        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        assertEquals(rets.getTotalSize().longValue(), 0L);
        assertEquals(rets.getTransferWaitlists().size(), 0);

        search = new TransferWaitlistSearchType();
        search.setSupervisionId(supervisionIDs[1]);

        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        assertEquals(rets.getTotalSize().longValue(), 1L);
        assertEquals(rets.getTransferWaitlists().size(), 1);

        search = new TransferWaitlistSearchType();
        search.setSupervisionId(supervisionIDs[2]);

        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        assertEquals(rets.getTotalSize().longValue(), 1L);
        assertEquals(rets.getTransferWaitlists().size(), 1);

        search = new TransferWaitlistSearchType();
        search.setSupervisionId(supervisionIDs[1]);

        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        assertEquals(rets.getTotalSize().longValue(), 1L);
        assertEquals(rets.getTransferWaitlists().size(), 1);

        // toFacility
        search = new TransferWaitlistSearchType();
        search.setToFacilityId(123456L);
        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        assertEquals(rets.getTotalSize().longValue(), 0L);
        assertEquals(rets.getTransferWaitlists().size(), 0);

        search = new TransferWaitlistSearchType();
        search.setToFacilityId(toFacilityIDs[1]);
        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        assertEquals(rets.getTotalSize().longValue(), 1L);
        assertEquals(rets.getTransferWaitlists().size(), 1);

        // fromDateAdded
        search = new TransferWaitlistSearchType();
        search.setFromDateAdded(today);
        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        assertEquals(rets.getTotalSize().longValue(), 2L);
        assertEquals(rets.getTransferWaitlists().size(), 2);

        search = new TransferWaitlistSearchType();
        cal.setTime(lastYear);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date fromDateAdded = cal.getTime();
        search.setFromDateAdded(fromDateAdded);
        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        // assertEquals(rets.getTotalSize().longValue(), 3L);
        assertEquals(rets.getTotalSize().longValue(), 6L);
        assertEquals(rets.getTransferWaitlists().size(), 3);

        search = new TransferWaitlistSearchType();
        cal.setTime(yesterday);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date twoDaysBefore = cal.getTime();
        search.setFromDateAdded(twoDaysBefore);
        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        assertEquals(rets.getTotalSize().longValue(), 3L);
        assertEquals(rets.getTransferWaitlists().size(), 2);

        //		search = new TransferWaitlistSearchType();
        //		cal.setTime(lastMonth);
        //		cal.add(Calendar.DAY_OF_MONTH, -1);
        //		Date oneMonthBefore = cal.getTime();
        //		search.setFromDateAdded(oneMonthBefore);
        //		rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        //		assertEquals(rets.getTotalSize().longValue(), 3L);
        //		assertEquals(rets.getTransferWaitlists().size(), 3);
        //
        //		// toDateAdded
        //		search = new TransferWaitlistSearchType();
        //		search.setToDateAdded(today);
        //		rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        //		assertEquals(rets.getTotalSize().longValue(), 3L);
        //		assertEquals(rets.getTransferWaitlists().size(), 3);
        //
        //		search = new TransferWaitlistSearchType();
        //		search.setToDateAdded(yesterday);
        //		rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        //		assertEquals(rets.getTotalSize().longValue(), 3L);
        //		assertEquals(rets.getTransferWaitlists().size(), 3);
        //
        //		search = new TransferWaitlistSearchType();
        //		search.setToDateAdded(lastMonth);
        //		rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        //		assertEquals(rets.getTotalSize().longValue(), 2L);
        //		assertEquals(rets.getTransferWaitlists().size(), 2);
        //
        //		search = new TransferWaitlistSearchType();
        //		search.setToDateAdded(lastYear);
        //		rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        //		assertEquals(rets.getTotalSize().longValue(), 2L);
        //		assertEquals(rets.getTransferWaitlists().size(), 2);
        //
        //		// fromDateAdded -- toDateAdded
        //		search = new TransferWaitlistSearchType();
        //		search.setFromDateAdded(today);
        //		search.setToDateAdded(today);
        //		rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        //		assertEquals(rets.getTotalSize().longValue(), 2L);
        //		assertEquals(rets.getTransferWaitlists().size(), 2);
        //
        //		search = new TransferWaitlistSearchType();
        //		search.setFromDateAdded(lastMonth);
        //		search.setToDateAdded(yesterday);
        //		rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        //		assertEquals(rets.getTotalSize().longValue(), 2L);
        //		assertEquals(rets.getTransferWaitlists().size(), 2);

        // priority
        search = new TransferWaitlistSearchType();
        search.setPriority("HIGH");
        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        assertEquals(rets.getTotalSize().longValue(), 3L);
        assertEquals(rets.getTransferWaitlists().size(), 3);

        search = new TransferWaitlistSearchType();
        search.setPriority("LOW");
        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        assertEquals(rets.getTotalSize().longValue(), 3L);
        assertEquals(rets.getTransferWaitlists().size(), 3);

        // Reason
        search = new TransferWaitlistSearchType();
        search.setTransferReason("in");
        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        assertEquals(rets.getTotalSize().longValue(), 0L);
        assertEquals(rets.getTransferWaitlists().size(), 0);

        search = new TransferWaitlistSearchType();
        search.setTransferReason("ESC");
        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        assertEquals(rets.getTotalSize().longValue(), 0L);
        assertEquals(rets.getTransferWaitlists().size(), 0);

        search = new TransferWaitlistSearchType();
        search.setTransferReason("ESCP");
        rets = maSer.searchTransferWaitlist(uc, search, null, null, null);
        assertEquals(rets.getTotalSize().longValue(), 3L);
        assertEquals(rets.getTransferWaitlists().size(), 3);

    }

}
