package syscon.arbutus.product.services.movementactivity.contract.ejb;

import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.ModuleConfig;

@ModuleConfig()
public class ProblemReproduceIT extends BaseIT {

    //	private Logger log = LoggerFactory.getLogger(ProblemReproduceIT.class);
    //	MovementService maSer;
    //
    //	Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").serializeNulls().create();
    //
    //	/**
    //	 * Movement Reference Sets
    //	 */
    //	public final static String MOVEMENT_CATEGORY  = "MovementCategory";
    //	public final static String MOVEMENT_TYPE      = "MovementType";
    //	public final static String MOVEMENT_DIRECTION = "MovementDirection";
    //	public final static String MOVEMENT_STATUS    = "MovementStatus";
    //	public final static String MOVEMENT_REASON    = "MovementReason";
    //	public final static String MOVEMENT_OUTCOME   = "MovementOutcome";
    //	public final static String TO_PROVINCE_STATE  = "StateProvince";
    //
    //	final static Long success = 1L;
    //
    //	MovementActivityType ma = new MovementActivityType();
    //	ExternalMovementActivityType ema = new ExternalMovementActivityType();
    //	InternalMovementActivityType ima = new InternalMovementActivityType();
    //	MovementActivityReturnType maRet = new MovementActivityReturnType();
    //	ExternalMovementActivityReturnType emaRet = new ExternalMovementActivityReturnType();
    //	InternalMovementActivityReturnType imaRet = new InternalMovementActivityReturnType();
    //	Long inId, exId;
    //	Set<String> refSetMaps = new LinkedHashSet<String>();
    //	String[] REF_SET = { MOVEMENT_CATEGORY, MOVEMENT_TYPE, MOVEMENT_REASON,
    //			MOVEMENT_OUTCOME, MOVEMENT_DIRECTION, MOVEMENT_STATUS,
    //			TO_PROVINCE_STATE };
    //
    //	AssociationType fromLocationAssociation       = toLocationAssociation(10000L);
    //	AssociationType fromFacilityAssociation       = toFacilityAssociation(20000L);
    //	AssociationType fromOrganizationAssociation   = toOrganizationAssociation(30000L);
    //	AssociationType toLocationAssociation         = toLocationAssociation(40000L);
    //	AssociationType toFacilityAssociation         = toFacilityAssociation(50000L);
    //	AssociationType toOrganizationAssociation     = toOrganizationAssociation(60000L);
    //	AssociationType arrestOrganizationAssociation = toOrganizationAssociation(70000L);
    //	AssociationType escortOrganizationAssociation = toOrganizationAssociation(80000L);
    //
    //	String toProvinceState = "BC";
    //	String toCity = "Richmond";
    //	String fromCity = "Vancouver";
    //	Date applicationDate, fromApplicationDate, toApplicationDate;
    //	Date reportingDate, fromReportingDate, toReportingDate;
    //
    //	AssociationType fromFacilityInternalLocationAssociation = toFacilityInternalLocationAssociation(100000L);
    //	AssociationType toFacilityInternalLocationAssociation   = toFacilityInternalLocationAssociation(110000L);
    //
    //	@BeforeMethod
    //	public void beforeMethod() {
    //
    //	}
    //
    //	@BeforeClass
    //	public void beforeClass() throws NamingException, Exception {
    //		log.info("beforeClass Begin - JNDI lookup");
    //		maSer = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
    //
    //        uc = super.initUserContext();
    //
    //		if (log.isDebugEnabled())
    //			log.debug("beforeTest: end");
    //	}
    //
    //	@Test
    //	public void testJNDILookup() {
    //		assertNotNull(maSer);
    //	}
    //
    //	@Test(dependsOnMethods = { "testJNDILookup" }, enabled = true)
    //	public void defectID2191() {
    //		maSer.reset(uc);
    //
    //		Date now = Calendar.getInstance().getTime();
    //		MovementActivitiesReturnType ret;
    //		ExternalMovementActivityReturnType emaRet;
    //		MovementActivitySearchType search;
    //		ExternalMovementActivityType ema;
    //		InternalMovementActivityType ima;
    //		AssociationType assoc;
    //		Set<AssociationType> activityAssociations;
    //
    //		ema = new ExternalMovementActivityType();
    //		ema.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 10L));
    //		ema.setMovementCategory(new CodeType(ReferenceSet.MOVEMENT_CATEGORY.value(), "EXTERNAL"));
    //		ema.setMovementType(new CodeType(ReferenceSet.MOVEMENT_TYPE.value(), "TRNIJ"));
    //		ema.setMovementDirection(new CodeType(ReferenceSet.MOVEMENT_DIRECTION.value(), "OUT"));
    //		ema.setMovementStatus(new CodeType(ReferenceSet.MOVEMENT_STATUS.value(), "PENDING"));
    //		ema.setMovementReason(new CodeType(ReferenceSet.MOVEMENT_REASON.value(), "DISC"));
    //		ema.setMovementOutcome(new CodeType(ReferenceSet.MOVEMENT_OUTCOME.value(), "NOTEXCUSED"));
    //		ema.setMovementDate(now);
    //		Set<CommentType> comments = new HashSet<CommentType>();
    //		CommentType comment = new CommentType();
    //		comment.setCommentDate(now);
    //		comment.setComment("Random count reproduce test.");
    //		comments.add(comment);
    //		ema.setCommentText(comments);
    //		assoc = new AssociationType(AssociationToClass.ACTIVITY.value(), 1L);
    //		activityAssociations = new HashSet<AssociationType>();
    //		activityAssociations.add(assoc);
    //		ema.setAssociations(activityAssociations);
    //
    //		emaRet = maSer.create(uc, ema, false);
    //		assertEquals(emaRet.getReturnCode(), success);
    //
    //		search = new MovementActivitySearchType();
    //		search.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 10L));
    //		ret = maSer.search(uc, search);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertEquals(ret.getExternalMovementActivity().size(), 1);
    //		assertEquals(ret.getInternalMovementActivity().size(), 0);
    //
    //		// ---------------------------------------
    //
    //		ema.setMovementStatus(new CodeType(ReferenceSet.MOVEMENT_STATUS.value(), "COMPLETED"));
    //		assoc = new AssociationType(AssociationToClass.ACTIVITY.value(), 2L);
    //		activityAssociations = new HashSet<AssociationType>();
    //		activityAssociations.add(assoc);
    //		ema.setAssociations(activityAssociations);
    //
    //		emaRet = maSer.create(uc, ema, false);
    //		assertEquals(emaRet.getReturnCode(), success);
    //
    //		search.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 10L));
    //		ret = maSer.search(uc, search);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertEquals(ret.getExternalMovementActivity().size(), 2);
    //		assertEquals(ret.getInternalMovementActivity().size(), 0);
    //
    //
    //		// ---------------------------------------------
    //
    //
    //		ema.setMovementStatus(new CodeType(ReferenceSet.MOVEMENT_STATUS.value(), "COMPLETED"));
    //		assoc = new AssociationType(AssociationToClass.ACTIVITY.value(), 3L);
    //		activityAssociations = new HashSet<AssociationType>();
    //		activityAssociations.add(assoc);
    //		ema.setAssociations(activityAssociations);
    //
    //		emaRet = maSer.create(uc, ema, false);
    //		assertEquals(emaRet.getReturnCode(), success);
    //
    //		search.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 10L));
    //		ret = maSer.search(uc, search);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertEquals(ret.getExternalMovementActivity().size(), 3);
    //		assertEquals(ret.getInternalMovementActivity().size(), 0);
    //
    //		// --------------------------------------
    //
    //		ima = new InternalMovementActivityType();
    //		ima.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 10L));
    //		ima.setMovementCategory(new CodeType(ReferenceSet.MOVEMENT_CATEGORY.value(), "INTERNAL"));
    //		ima.setMovementType(new CodeType(ReferenceSet.MOVEMENT_TYPE.value(), "BED"));
    //		ima.setMovementDirection(new CodeType(ReferenceSet.MOVEMENT_DIRECTION.value(), "IN"));
    //		ima.setMovementStatus(new CodeType(ReferenceSet.MOVEMENT_STATUS.value(), "PENDING"));
    //		ima.setMovementReason(new CodeType(ReferenceSet.MOVEMENT_REASON.value(), "MED"));
    //		ima.setMovementOutcome(new CodeType(ReferenceSet.MOVEMENT_OUTCOME.value(), "SICK"));
    //		ima.setMovementDate(now);
    //		comments = new HashSet<CommentType>();
    //		comment = new CommentType();
    //		comment.setCommentDate(now);
    //		comment.setComment("Random count reproduce test.");
    //		comments.add(comment);
    //		ima.setCommentText(comments);
    //		assoc = new AssociationType(AssociationToClass.ACTIVITY.value(), 4L);
    //		activityAssociations = new HashSet<AssociationType>();
    //		activityAssociations.add(assoc);
    //		ima.setAssociations(activityAssociations);
    //
    //		ima.setFromFacilityInternalLocationAssociation(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 100L));
    //		ima.setToFacilityInternalLocationAssociation(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 110L));
    //
    //		imaRet = maSer.create(uc, ima, false);
    //		assertEquals(imaRet.getReturnCode(), success);
    //
    //		search.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 10L));
    //		ret = maSer.search(uc, search);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertEquals(ret.getExternalMovementActivity().size(), 3);
    //		assertEquals(ret.getInternalMovementActivity().size(), 1);
    //
    //		// ----------------------------------
    //		ima = new InternalMovementActivityType();
    //		ima.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 10L));
    //		ima.setMovementCategory(new CodeType(ReferenceSet.MOVEMENT_CATEGORY.value(), "INTERNAL"));
    //		ima.setMovementType(new CodeType(ReferenceSet.MOVEMENT_TYPE.value(), "BED"));
    //		ima.setMovementDirection(new CodeType(ReferenceSet.MOVEMENT_DIRECTION.value(), "IN"));
    //		ima.setMovementStatus(new CodeType(ReferenceSet.MOVEMENT_STATUS.value(), "PENDING"));
    //		ima.setMovementReason(new CodeType(ReferenceSet.MOVEMENT_REASON.value(), "MED"));
    //		ima.setMovementOutcome(new CodeType(ReferenceSet.MOVEMENT_OUTCOME.value(), "SICK"));
    //		ima.setMovementDate(now);
    //		comments = new HashSet<CommentType>();
    //		comment = new CommentType();
    //		comment.setCommentDate(now);
    //		comment.setComment("Random count reproduce test.");
    //		comments.add(comment);
    //		ima.setCommentText(comments);
    //		assoc = new AssociationType(AssociationToClass.ACTIVITY.value(), 5L);
    //		activityAssociations = new HashSet<AssociationType>();
    //		activityAssociations.add(assoc);
    //		ima.setAssociations(activityAssociations);
    //
    //		ima.setFromFacilityInternalLocationAssociation(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 200L));
    //		ima.setToFacilityInternalLocationAssociation(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 210L));
    //
    //		imaRet = maSer.create(uc, ima, false);
    //		assertEquals(imaRet.getReturnCode(), success);
    //
    //		search.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 10L));
    //		ret = maSer.search(uc, search);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertEquals(ret.getExternalMovementActivity().size(), 3);
    //		assertEquals(ret.getInternalMovementActivity().size(), 2);
    //
    //		// -----------------------------------------------
    //
    //		ema.setMovementCategory(new CodeType(ReferenceSet.MOVEMENT_CATEGORY.value(), "EXTERNAL"));
    //		ema.setMovementDate(now);
    //		ema.setMovementReason(new CodeType(ReferenceSet.MOVEMENT_REASON.value(), "ADMN"));
    //		ema.setMovementDirection(new CodeType(ReferenceSet.MOVEMENT_DIRECTION.value(), "OUT"));
    //		ema.setMovementStatus(new CodeType(ReferenceSet.MOVEMENT_STATUS.value(), "PENDING"));
    //		ema.setMovementType(new CodeType(ReferenceSet.MOVEMENT_TYPE.value(), "TRNIJ"));
    //		ema.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 131L));
    //		ema.setFromFacilityId(new AssociationType(AssociationToClass.FACILITY.value(), 409L));
    //		ema.setToFacilityId(new AssociationType(AssociationToClass.FACILITY.value(), 410L));
    //		assoc = new AssociationType(AssociationToClass.ACTIVITY.value(), 6L);
    //		activityAssociations = new HashSet<AssociationType>();
    //		activityAssociations.add(assoc);
    //		ema.setAssociations(activityAssociations);
    //
    //		emaRet = maSer.create(uc, ema, false);
    //		assertEquals(emaRet.getReturnCode(), success);
    //
    //		ema.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 129L));
    //		ema.setFromFacilityId(new AssociationType(AssociationToClass.FACILITY.value(), 403L));
    //		ema.setToFacilityId(new AssociationType(AssociationToClass.FACILITY.value(), 404L));
    //		assoc = new AssociationType(AssociationToClass.ACTIVITY.value(), 7L);
    //		activityAssociations = new HashSet<AssociationType>();
    //		activityAssociations.add(assoc);
    //		ema.setAssociations(activityAssociations);
    //
    //		emaRet = maSer.create(uc, ema, false);
    //		assertEquals(emaRet.getReturnCode(), success);
    //
    //		search.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 131L));
    //		search.setMovementStatus(new CodeType(ReferenceSet.MOVEMENT_STATUS.value(), "PENDING"));
    //
    //		ret = maSer.search(uc, search);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertEquals(ret.getExternalMovementCount().intValue(), 1);
    //		assertEquals(ret.getExternalMovementActivity().size(), 1);
    //	}
    //
    //	@Test(dependsOnMethods = { "testJNDILookup" }, enabled = true)
    //	public void create() {
    //		maSer.reset(uc);
    //		ExternalMovementActivityReturnType emaRet;
    //		ExternalMovementActivityType ema = new ExternalMovementActivityType();
    //		ema.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 108L));
    //		ema.setMovementCategory(new CodeType(ReferenceSet.MOVEMENT_CATEGORY.value(), "EXTERNAL"));
    //		ema.setMovementType(new CodeType(ReferenceSet.MOVEMENT_TYPE.value(), "REL"));
    //		ema.setMovementDirection(new CodeType(ReferenceSet.MOVEMENT_DIRECTION.value(), "OUT"));
    //		ema.setMovementStatus(new CodeType(ReferenceSet.MOVEMENT_STATUS.value(), "COMPLETED"));
    //		ema.setMovementReason(new CodeType(ReferenceSet.MOVEMENT_REASON.value(), "DISC"));
    //		ema.setMovementDate(new Date());
    //		ema.setEscortDetails("EscortDetail");
    //
    //		CommentType comment = new CommentType();
    //		comment.setCommentDate(new Date());
    //		comment.setComment("ABcdef");
    //		Set<CommentType> comments = new HashSet<CommentType>();
    //		comments.add(comment);
    //		ema.setCommentText(comments);
    //
    //		AssociationType activityAccoc = new AssociationType(AssociationToClass.ACTIVITY.value(), 152L);
    //		Set<AssociationType> assocs = new HashSet<AssociationType>();
    //		assocs.add(activityAccoc);
    //		ema.setAssociations(assocs);
    //
    //		assertTrue(ema.isWellFormed());
    //		assertTrue(ema.isValid());
    //
    //		emaRet = maSer.create(uc, ema, false);
    //		assertEquals(emaRet.getReturnCode(), success);
    //		assertNotNull(emaRet.getExternalMovementActivity());
    //
    //		assertEquals(emaRet.getExternalMovementActivity().getSupervisionId(), new AssociationType(AssociationToClass.SUPERVISION.value(), 108L));
    //
    //		String escortDetails = emaRet.getExternalMovementActivity().getEscortDetails();
    //		assertEquals(escortDetails, "EscortDetail");
    //
    //		ExternalMovementActivityType ema2 = emaRet.getExternalMovementActivity();
    //		ema2.setEscortDetails("Escort2.");
    //		emaRet = maSer.update(uc, ema2);
    //		assertEquals(emaRet.getReturnCode(), success);
    //		assertEquals(emaRet.getExternalMovementActivity().getEscortDetails(), "Escort2.");
    //	}
    //
    //	@Test(dependsOnMethods = { "testJNDILookup" }, enabled = false)
    //	public void searchBySupervision() {
    //		MovementActivitiesReturnType maRet;
    //		MovementActivitySearchType search = new MovementActivitySearchType();
    //		search.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 131L));
    //		search.setMovementStatus(new CodeType(ReferenceSet.MOVEMENT_STATUS.value(), "PENDING"));
    //
    //		maRet = maSer.search(uc, search);
    //		assertEquals(maRet.getReturnCode(), success);
    //		// assertTrue(maRet.getExternalMovementCount().intValue() > 0);
    //		// assertTrue(maRet.getInternalMovementCount().intValue() > 0);
    //		assertEquals(maRet.getExternalMovementCount().intValue(), 1);
    //		assertEquals(maRet.getInternalMovementCount().intValue(), 0);
    //
    //	}
    //
    //	@Test(dependsOnMethods = { "testJNDILookup" }, enabled = true)
    //	public void search() {
    //		maSer.reset(uc);
    //		MovementActivitiesReturnType maRet;
    //		ExternalMovementActivityReturnType emaRet;
    //
    //		String data = "{'fromLocationAssociation':{'toClass':'LocationService','toIdentifier':10},"+
    //        "'fromOrganizationAssociation': {'toClass':'OrganizationService','toIdentifier':10},"+
    //        "'fromFacilityAssociation': {'toClass':'FacilityService','toIdentifier':10},"+
    //        "'fromCity':'Vancouver',"+
    //        //"'toFacilityInternalLocationAssociation': {'toClass':'FacilityInternalLocationService','toIdentifier':20},"+
    //        "'toFacilityInternalLocationAssociation': null,"+
    //        "'toLocationAssociation':{'toClass':'LocationService','toIdentifier':2},"+
    //        "'toOrganizationAssociation':{'toClass':'FacilityService','toIdentifier':11},"+
    //        "'toFacilityAssociation':{'toClass':'FacilityService','toIdentifier':11},"+
    //        "'toCity':'Richmond',"+
    //        "'toProvinceState':'BC',"+
    //        "'arrestOrganizationAssociation' :{'toClass':'OrganizationService','toIdentifier':1},"+
    //        "'applicationDate':'2015-10-12 5:07:20' ,"+//null,
    //        "'escortOrganizationAssociation':{'toClass':'OrganizationService','toIdentifier':2},"+
    //        "'escortDetails':'escort detail string',"+
    //        "'reportingDate':'2015-10-13 5:07:20'," +
    //        "'supervisionAssociation':{'toClass':'SupervisionService','toIdentifier':2111},"+
    //            "'movementCategory':{'set':'MovementCategory','code':'EXTERNAL'}," +
    //            "'movementType':{'set':'MovementType','code':'CRT'}," +
    //        "'movementDirection':{'set':'MovementDirection','code':'IN'}," +
    //        "'movementStatus':{'set':'MovementStatus','code':'PENDING'}," +
    //        "'movementReason':{'set':'MovementReason','code':'LA'}," +
    //        "'movementOutcome':{'set':'MovementOutcome','code':'EXCUSED'}," +
    //        "'movementDate':'2015-10-13 5:07:19'," +
    //         "'comment':[{'commentId':null,'commentDate':'2015-10-12 5:07:20','comment':'this is testing','userId':null}],"+
    //        "'approvedByStaffAssociation'={'toClass':'StaffService','toIdentifier':1},"+
    //                   "'approvalDate'='2015-10-12 5:07:20',"+
    //                   "'approvalComments'='hello',"+
    //       "'associations':[{'toClass':'ActivityService','toIdentifier':666,'dataPrivileges':null}],"+
    //        "'facilities':[]," +
    //        "'dataPrivileges':[]}";
    //
    //
    //		ExternalMovementActivityType ema = gson.fromJson(data, ExternalMovementActivityType.class);
    //		assertTrue(ema.isValid());
    //
    //		emaRet = maSer.create(uc, ema, true);
    //		assertEquals(emaRet.getReturnCode(), success);
    //		assertNotNull(emaRet.getExternalMovementActivity());
    //
    //		MovementActivitySearchType search = new MovementActivitySearchType();
    //
    //		search.setSupervisionId(new AssociationType("SupervisionService", 2111L));
    //		maRet = maSer.search(uc, search);
    //		assertEquals(maRet.getReturnCode(), success);
    //		assertEquals(maRet.getExternalMovementCount().intValue(), 1);
    //		assertEquals(maRet.getInternalMovementCount().intValue(), 0);
    //		assertEquals(((ExternalMovementActivityType)(maRet.getExternalMovementActivity().toArray()[0])).getSupervisionId().getToIdentifier().intValue(), 2111);
    //
    //
    //		data = "{'fromLocationAssociation':{'toClass':'LocationService','toIdentifier':10},"+
    //        "'fromOrganizationAssociation': {'toClass':'OrganizationService','toIdentifier':10},"+
    //        "'fromFacilityAssociation': {'toClass':'FacilityService','toIdentifier':10},"+
    //        "'fromCity':'Vancouver',"+
    //        //"'toFacilityInternalLocationAssociation': {'toClass':'FacilityInternalLocationService','toIdentifier':20},"+
    //        "'toFacilityInternalLocationAssociation': null,"+
    //        "'toLocationAssociation':{'toClass':'LocationService','toIdentifier':2},"+
    //        "'toOrganizationAssociation':{'toClass':'FacilityService','toIdentifier':11},"+
    //        "'toFacilityAssociation':{'toClass':'FacilityService','toIdentifier':11},"+
    //        "'toCity':'Richmond',"+
    //        "'toProvinceState':'BC',"+
    //        "'arrestOrganizationAssociation' :{'toClass':'OrganizationService','toIdentifier':1},"+
    //        "'applicationDate':'2015-10-12 5:07:20' ,"+//null,
    //        "'escortOrganizationAssociation':{'toClass':'OrganizationService','toIdentifier':2},"+
    //        "'escortDetails':'escort detail string',"+
    //        "'reportingDate':'2015-10-13 5:07:20'," +
    //        "'supervisionAssociation':{'toClass':'SupervisionService','toIdentifier':21},"+
    //            "'movementCategory':{'set':'MovementCategory','code':'EXTERNAL'}," +
    //            "'movementType':{'set':'MovementType','code':'CRT'}," +
    //        "'movementDirection':{'set':'MovementDirection','code':'IN'}," +
    //        "'movementStatus':{'set':'MovementStatus','code':'PENDING'}," +
    //        "'movementReason':{'set':'MovementReason','code':'LA'}," +
    //        "'movementOutcome':{'set':'MovementOutcome','code':'EXCUSED'}," +
    //        "'movementDate':'2015-10-13 5:07:19'," +
    //         "'comment':[{'commentId':null,'commentDate':'2015-10-12 5:07:20','comment':'this is testing','userId':null}],"+
    //        "'approvedByStaffAssociation'={'toClass':'StaffService','toIdentifier':1},"+
    //                   "'approvalDate'='2015-10-12 5:07:20',"+
    //                   "'approvalComments'='hello',"+
    //       "'associations':[{'toClass':'ActivityService','toIdentifier':6,'dataPrivileges':null}],"+
    //        "'facilities':[]," +
    //        "'dataPrivileges':[]}";
    //
    //
    //		ema = gson.fromJson(data, ExternalMovementActivityType.class);
    //		assertTrue(ema.isValid());
    //
    //		emaRet = maSer.create(uc, ema, true);
    //		assertEquals(emaRet.getReturnCode(), success);
    //		assertNotNull(emaRet.getExternalMovementActivity());
    //
    //		search = new MovementActivitySearchType();
    //
    //		search.setSupervisionId(new AssociationType("SupervisionService", 21L));
    //		maRet = maSer.search(uc, search);
    //		assertEquals(maRet.getReturnCode(), success);
    //		assertEquals(maRet.getExternalMovementCount().intValue(), 1);
    //		assertEquals(maRet.getInternalMovementCount().intValue(), 0);
    //		assertEquals(((ExternalMovementActivityType)(maRet.getExternalMovementActivity().toArray()[0])).getSupervisionId().getToIdentifier().intValue(), 21);
    //
    //		data = "{'fromLocationAssociation':{'toClass':'LocationService','toIdentifier':10},"+
    //        "'fromOrganizationAssociation': {'toClass':'OrganizationService','toIdentifier':10},"+
    //        "'fromFacilityAssociation': {'toClass':'FacilityService','toIdentifier':10},"+
    //        "'fromCity':'Vancouver',"+
    //        //"'toFacilityInternalLocationAssociation': {'toClass':'FacilityInternalLocationService','toIdentifier':20},"+
    //        "'toFacilityInternalLocationAssociation': null,"+
    //        "'toLocationAssociation':{'toClass':'LocationService','toIdentifier':2},"+
    //        "'toOrganizationAssociation':{'toClass':'FacilityService','toIdentifier':11},"+
    //        "'toFacilityAssociation':{'toClass':'FacilityService','toIdentifier':11},"+
    //        "'toCity':'Richmond',"+
    //        "'toProvinceState':'BC',"+
    //        "'arrestOrganizationAssociation' :{'toClass':'OrganizationService','toIdentifier':1},"+
    //        "'applicationDate':'2015-10-12 5:07:20' ,"+//null,
    //        "'escortOrganizationAssociation':{'toClass':'OrganizationService','toIdentifier':2},"+
    //        "'escortDetails':'escort detail string',"+
    //        "'reportingDate':'2015-10-13 5:07:20'," +
    //        "'supervisionAssociation':{'toClass':'SupervisionService','toIdentifier':21},"+
    //            "'movementCategory':{'set':'MovementCategory','code':'EXTERNAL'}," +
    //            "'movementType':{'set':'MovementType','code':'CRT'}," +
    //        "'movementDirection':{'set':'MovementDirection','code':'IN'}," +
    //        "'movementStatus':{'set':'MovementStatus','code':'PENDING'}," +
    //        "'movementReason':{'set':'MovementReason','code':'LA'}," +
    //        "'movementOutcome':{'set':'MovementOutcome','code':'EXCUSED'}," +
    //        "'movementDate':'2015-10-13 5:07:19'," +
    //         "'comment':[{'commentId':null,'commentDate':'2015-10-12 5:07:20','comment':'this is testing','userId':null}],"+
    //        "'approvedByStaffAssociation'={'toClass':'StaffService','toIdentifier':1},"+
    //                   "'approvalDate'='2015-10-12 5:07:20',"+
    //                   "'approvalComments'='hello',"+
    //       "'associations':[{'toClass':'ActivityService','toIdentifier':7,'dataPrivileges':null}],"+
    //        "'facilities':[]," +
    //        "'dataPrivileges':[]}";
    //
    //
    //		ema = gson.fromJson(data, ExternalMovementActivityType.class);
    //		assertTrue(ema.isValid());
    //
    //		emaRet = maSer.create(uc, ema, true);
    //		assertEquals(emaRet.getReturnCode(), success);
    //		assertNotNull(emaRet.getExternalMovementActivity());
    //
    //		search = new MovementActivitySearchType();
    //
    //		search.setSupervisionId(new AssociationType("SupervisionService", 21L));
    //		maRet = maSer.search(uc, search);
    //		assertEquals(maRet.getReturnCode(), success);
    //		assertEquals(maRet.getExternalMovementCount().intValue(), 2);
    //		assertEquals(maRet.getInternalMovementCount().intValue(), 0);
    //		assertEquals(maRet.getExternalMovementActivity().size(), 2);
    //		assertEquals(maRet.getInternalMovementActivity().size(), 0);
    //		assertEquals(((ExternalMovementActivityType)(maRet.getExternalMovementActivity().toArray()[0])).getSupervisionId().getToIdentifier().intValue(), 21);
    //		assertEquals(((ExternalMovementActivityType)(maRet.getExternalMovementActivity().toArray()[1])).getSupervisionId().getToIdentifier().intValue(), 21);
    //
    //
    //		data = "{'fromFacilityInternalLocationAssociation':{'toClass':'FacilityInternalLocationService','toIdentifier':10},"+
    //        "'toFacilityInternalLocationAssociation': {'toClass':'FacilityInternalLocationService','toIdentifier':11},"+
    //        "'supervisionAssociation':{'toClass':'SupervisionService','toIdentifier':21},"+
    //            "'movementCategory':{'set':'MovementCategory','code':'INTERNAL'}," +
    //            "'movementType':{'set':'MovementType','code':'CRT'}," +
    //        "'movementDirection':{'set':'MovementDirection','code':'IN'}," +
    //        "'movementStatus':{'set':'MovementStatus','code':'PENDING'}," +
    //        "'movementReason':{'set':'MovementReason','code':'LA'}," +
    //        "'movementOutcome':{'set':'MovementOutcome','code':'EXCUSED'}," +
    //        "'movementDate':'2015-10-13 5:07:19'," +
    //         "'comment':[{'commentId':null,'commentDate':'2015-10-12 5:07:20','comment':'this is testing','userId':null}],"+
    //        "'approvedByStaffAssociation'={'toClass':'StaffService','toIdentifier':1},"+
    //                   "'approvalDate'='2015-10-12 5:07:20',"+
    //                   "'approvalComments'='hello',"+
    //       "'associations':[{'toClass':'ActivityService','toIdentifier':8,'dataPrivileges':null}],"+
    //        "'facilities':[]," +
    //        "'dataPrivileges':[]}";
    //
    //
    //		InternalMovementActivityType ima = gson.fromJson(data, InternalMovementActivityType.class);
    //		assertTrue(ema.isValid());
    //
    //		InternalMovementActivityReturnType imaRet = maSer.create(uc, ima, true);
    //		assertEquals(imaRet.getReturnCode(), success);
    //		assertNotNull(imaRet.getInternalMovementActivity());
    //
    //		search.setSupervisionId(new AssociationType("SupervisionService", 21L));
    //		maRet = maSer.search(uc, search);
    //		assertEquals(maRet.getReturnCode(), success);
    //		assertEquals(maRet.getExternalMovementCount().intValue(), 2);
    //		assertEquals(maRet.getInternalMovementCount().intValue(), 1);
    //		assertEquals(maRet.getExternalMovementActivity().size(), 2);
    //		assertEquals(maRet.getInternalMovementActivity().size(), 1);
    //		assertEquals(((ExternalMovementActivityType)(maRet.getExternalMovementActivity().toArray()[0])).getSupervisionId().getToIdentifier().intValue(), 21);
    //		assertEquals(((ExternalMovementActivityType)(maRet.getExternalMovementActivity().toArray()[1])).getSupervisionId().getToIdentifier().intValue(), 21);
    //		assertEquals(((InternalMovementActivityType)(maRet.getInternalMovementActivity().toArray()[0])).getSupervisionId().getToIdentifier().intValue(), 21);
    //
    //		search.setSupervisionId(new AssociationType("SupervisionService", 22L));
    //		maRet = maSer.search(uc, search);
    //		assertEquals(maRet.getReturnCode(), success);
    //		assertEquals(maRet.getExternalMovementCount().intValue(), 0);
    //		assertEquals(maRet.getInternalMovementCount().intValue(), 0);
    //
    //		search = new MovementActivitySearchType();
    //		maRet = maSer.search(uc, search, new HashSet<Long>());
    //		assertEquals(maRet.getReturnCode(), ReturnCode.CInvalidInput2002.returnCode());
    //
    //		search = new MovementActivitySearchType();
    //		search.setSupervisionId(new AssociationType("SupervisionService", 21L));
    //		maRet = maSer.search(uc, search, new HashSet<Long>());
    //		assertEquals(maRet.getReturnCode(), success);
    //		assertEquals(maRet.getExternalMovementCount().intValue(), 2);
    //		assertEquals(maRet.getInternalMovementCount().intValue(), 1);
    //		assertEquals(maRet.getExternalMovementActivity().size(), 2);
    //		assertEquals(maRet.getInternalMovementActivity().size(), 1);
    //		assertEquals(((ExternalMovementActivityType)(maRet.getExternalMovementActivity().toArray()[0])).getSupervisionId().getToIdentifier().intValue(), 21);
    //		assertEquals(((ExternalMovementActivityType)(maRet.getExternalMovementActivity().toArray()[1])).getSupervisionId().getToIdentifier().intValue(), 21);
    //		assertEquals(((InternalMovementActivityType)(maRet.getInternalMovementActivity().toArray()[0])).getSupervisionId().getToIdentifier().intValue(), 21);
    //
    //		data = "{'fromFacilityInternalLocationAssociation':{'toClass':'FacilityInternalLocationService','toIdentifier':10},"+
    //        "'toFacilityInternalLocationAssociation': {'toClass':'FacilityInternalLocationService','toIdentifier':11},"+
    //        "'supervisionAssociation':{'toClass':'SupervisionService','toIdentifier':21},"+
    //            "'movementCategory':{'set':'MovementCategory','code':'INTERNAL'}," +
    //            "'movementType':{'set':'MovementType','code':'CRT'}," +
    //        "'movementDirection':{'set':'MovementDirection','code':'IN'}," +
    //        "'movementStatus':{'set':'MovementStatus','code':'PENDING'}," +
    //        "'movementReason':{'set':'MovementReason','code':'LA'}," +
    //        "'movementOutcome':{'set':'MovementOutcome','code':'EXCUSED'}," +
    //        "'movementDate':'2015-10-13 5:07:19'," +
    //         "'comment':[{'commentId':null,'commentDate':'2015-10-12 5:07:20','comment':'this is testing','userId':null}],"+
    //        "'approvedByStaffAssociation'={'toClass':'StaffService','toIdentifier':1},"+
    //                   "'approvalDate'='2015-10-12 5:07:20',"+
    //                   "'approvalComments'='hello',"+
    //       "'associations':[{'toClass':'ActivityService','toIdentifier':88,'dataPrivileges':null}],"+
    //        "'facilities':[]," +
    //        "'dataPrivileges':[]}";
    //
    //
    //		ima = gson.fromJson(data, InternalMovementActivityType.class);
    //		assertTrue(ema.isValid());
    //
    //		imaRet = maSer.create(uc, ima, true);
    //		assertEquals(imaRet.getReturnCode(), success);
    //		assertNotNull(imaRet.getInternalMovementActivity());
    //
    //		search = new MovementActivitySearchType();
    //		search.setSupervisionId(new AssociationType("SupervisionService", 21L));
    //		maRet = maSer.search(uc, search, new HashSet<Long>());
    //		assertEquals(maRet.getReturnCode(), success);
    //		assertEquals(maRet.getExternalMovementCount().intValue(), 2);
    //		assertEquals(maRet.getInternalMovementCount().intValue(), 2);
    //		assertEquals(maRet.getExternalMovementActivity().size(), 2);
    //		assertEquals(maRet.getInternalMovementActivity().size(), 2);
    //		assertEquals(((ExternalMovementActivityType)(maRet.getExternalMovementActivity().toArray()[0])).getSupervisionId().getToIdentifier().intValue(), 21);
    //		assertEquals(((ExternalMovementActivityType)(maRet.getExternalMovementActivity().toArray()[1])).getSupervisionId().getToIdentifier().intValue(), 21);
    //		assertEquals(((InternalMovementActivityType)(maRet.getInternalMovementActivity().toArray()[0])).getSupervisionId().getToIdentifier().intValue(), 21);
    //		assertEquals(((InternalMovementActivityType)(maRet.getInternalMovementActivity().toArray()[1])).getSupervisionId().getToIdentifier().intValue(), 21);
    //
    //		search = new MovementActivitySearchType();
    //		search.setSupervisionId(new AssociationType("SupervisionService", 22L));
    //		maRet = maSer.search(uc, search, new HashSet<Long>());
    //		assertEquals(maRet.getReturnCode(), success);
    //		assertEquals(maRet.getExternalMovementCount().intValue(), 0);
    //		assertEquals(maRet.getInternalMovementCount().intValue(), 0);
    //		assertEquals(maRet.getExternalMovementActivity().size(), 0);
    //		assertEquals(maRet.getInternalMovementActivity().size(), 0);
    //
    //		Set<Long> ids = maSer.getAll(uc);
    //		search = new MovementActivitySearchType();
    //		search.setSupervisionId(new AssociationType("SupervisionService", 21L));
    //		maRet = maSer.search(uc, search, ids);
    //		assertEquals(maRet.getReturnCode(), success);
    //		assertEquals(maRet.getExternalMovementCount().intValue(), 2);
    //		assertEquals(maRet.getInternalMovementCount().intValue(), 2);
    //		assertEquals(maRet.getExternalMovementActivity().size(), 2);
    //		assertEquals(maRet.getInternalMovementActivity().size(), 2);
    //		assertEquals(((ExternalMovementActivityType)(maRet.getExternalMovementActivity().toArray()[0])).getSupervisionId().getToIdentifier().intValue(), 21);
    //		assertEquals(((ExternalMovementActivityType)(maRet.getExternalMovementActivity().toArray()[1])).getSupervisionId().getToIdentifier().intValue(), 21);
    //		assertEquals(((InternalMovementActivityType)(maRet.getInternalMovementActivity().toArray()[0])).getSupervisionId().getToIdentifier().intValue(), 21);
    //		assertEquals(((InternalMovementActivityType)(maRet.getInternalMovementActivity().toArray()[1])).getSupervisionId().getToIdentifier().intValue(), 21);
    //
    //}
    //
    //	@Test(dependsOnMethods = { "testJNDILookup" }, enabled = true)
    //	public void getLastCompletedSupervisionMovementBySupervisions() {
    //		Long returnCode = maSer.reset(uc);
    //		assertEquals(returnCode, success);
    //
    //		ema = new ExternalMovementActivityType();
    //		ema.setMovementCategory(new CodeType(MOVEMENT_CATEGORY, MovementActivityType.MovementCategory.EXTERNAL.value()));
    //		ema.setMovementDate(new Date());
    //		ema.setMovementDirection(new CodeType(MOVEMENT_DIRECTION, MovementActivityType.MovementDirection.IN.code()));
    //		ema.setMovementOutcome(new CodeType(MOVEMENT_OUTCOME, "EXCUSED"));
    //		ema.setMovementReason(new CodeType(MOVEMENT_REASON, "ADM"));
    //		ema.setMovementStatus(new CodeType(MOVEMENT_STATUS, "COMPLETED"));
    //		ema.setMovementType(new CodeType(MOVEMENT_TYPE, "ADM"));
    //		ema.setSupervisionId(new AssociationType(AssociationToClass.SUPERVISION.value(), 55L));
    //		ema.setFromFacilityId(new AssociationType(AssociationToClass.FACILITY.value(), 57L));
    //		ema.setToFacilityId(new AssociationType(AssociationToClass.FACILITY.value(), 58L));
    //
    //		assertFalse(ema.isWellFormed(), "Invalid Data!!!");
    //		assertFalse(ema.isValid(), "Invalid Data!!!");
    //
    //		emaRet = maSer.create(uc, ema, true);
    //		assertEquals(emaRet.getReturnCode(), ReturnCode.CInvalidInput2002.returnCode(), "Should be 2002 because of Invalid Data.");
    //		assertNull(emaRet.getExternalMovementActivity(), "should not be created.");
    //	}
    //
    //	private boolean sameExternalMovementActivity(
    //			ExternalMovementActivityType e1, ExternalMovementActivityType e2) {
    ////		assertEquals(e1.getMovementId(),      e2.getMovementId());
    ////		assertEquals(e1.getMovementCategory().getCode(),  e2.getMovementCategory().getCode());
    ////		assertEquals(e1.getMovementType().getCode(),      e2.getMovementType().getCode());
    ////		assertEquals(e1.getMovementDirection().getCode(), e2.getMovementDirection().getCode());
    ////		assertEquals(e1.getMovementStatus().getCode(),    e2.getMovementStatus().getCode());
    ////		assertEquals(e1.getMovementReason().getCode(),    e2.getMovementReason().getCode());
    ////		assertEquals(e1.getMovementDate(), e2.getMovementDate());
    ////		assertEquals(e1.getFromCity(),     e2.getFromCity());
    ////		assertEquals(e1.getToCity(),       e2.getToCity());
    ////		assertEquals(e1.getCommentText().size(),      e2.getCommentText().size());
    ////		assertEquals(e1.getAssociations().size(),     e2.getAssociations().size());
    ////		assertEquals(e1.getFromLocationId(), e2.getFromLocationId());
    ////		assertEquals(e1.getFromFacilityId(), e2.getFromFacilityId());
    ////		assertEquals(e1.getFromOrganizationId(), e2.getFromOrganizationId());
    ////		assertEquals(e1.getApplicationDate(), e2.getApplicationDate());
    ////		assertEquals(e1.getArrestOrganizationId(), e2.getArrestOrganizationId());
    ////		assertEquals(e1.getEscortOrganizationId(), e2.getEscortOrganizationId());
    ////		assertEquals(e1.getReportingDate(),   e2.getReportingDate());
    ////		assertEquals(e1.getToProvinceState(), e2.getToProvinceState());
    ////		assertEquals(e1.getToFacilityId(), e2.getToFacilityId());
    ////		assertEquals(e1.getToLocationId(), e2.getToLocationId());
    ////		assertEquals(e1.getToOrganizationId(), e2.getToOrganizationId());
    //		return true;
    //	}
    //
    //	private boolean sameInternalMovementActivity(
    //			InternalMovementActivityType e1, InternalMovementActivityType e2) {
    ////		assertEquals(e1.getMovementId(),      e2.getMovementId());
    ////		assertEquals(e1.getMovementCategory().getCode(),  e2.getMovementCategory().getCode());
    ////		assertEquals(e1.getMovementType().getCode(),      e2.getMovementType().getCode());
    ////		assertEquals(e1.getMovementDirection().getCode(), e2.getMovementDirection().getCode());
    ////		assertEquals(e1.getMovementStatus().getCode(),    e2.getMovementStatus().getCode());
    ////		assertEquals(e1.getMovementReason().getCode(),    e2.getMovementReason().getCode());
    ////		assertEquals(e1.getMovementDate(),        e2.getMovementDate());
    ////		assertEquals(e1.getCommentText().size(),  e2.getCommentText().size());
    ////		assertEquals(e1.getAssociations().size(), e2.getAssociations().size());
    ////		assertEquals(e1.getFromFacilityInternalLocationAssociation(),
    ////				e2.getFromFacilityInternalLocationAssociation());
    ////		assertEquals(e1.getToFacilityInternalLocationAssociation(),
    ////				e2.getToFacilityInternalLocationAssociation());
    //		return true;
    //	}
    //
    //	/**
    //	 * Converts MovementActivity.MovementCategory to CodeType.
    //	 *
    //	 * @param movementCategory	the movement category
    //	 * @return the code type for the movement category
    //	 */
    //	public static CodeType toMovementCategoryCodeType(String movementCategory) {
    //		return new CodeType(MOVEMENT_CATEGORY, movementCategory);
    //	}
    //
    //	/**
    //	 * Converts movement type from String to CodeType.
    //	 *
    //	 * @param movementType	the movement type
    //	 * @return the code type for the movement type
    //	 */
    //	public static CodeType toMovementTypeCodeType(String movementType) {
    //		return new CodeType(MOVEMENT_TYPE, movementType);
    //	}
    //
    //	/**
    //	 * Converts movement direction from String to CodeType.
    //	 *
    //	 * @param movementDirection	the movement direction
    //	 * @return the code type for the movement direction
    //	 */
    //	public static CodeType toMovementDirectionCodeType(String movementDirection) {
    //		return new CodeType(MOVEMENT_DIRECTION, movementDirection);
    //	}
    //
    //	/**
    //	 * Converts movement status from String to CodeType.
    //	 *
    //	 * @param movementStatus	the movement status
    //	 * @return the code type for the movement status
    //	 */
    //	public static CodeType toMovementStatusCodeType(String movementStatus) {
    //		return new CodeType(MOVEMENT_STATUS, movementStatus);
    //	}
    //
    //	/**
    //	 * Converts movement reason from String to CodeType.
    //	 *
    //	 * @param movementReason	the movement reason
    //	 * @return the code type for the movement reason
    //	 */
    //	public static CodeType toMovementReasonCodeType(String movementReason) {
    //		return new CodeType(MOVEMENT_REASON, movementReason);
    //	}
    //
    //	/**
    //	 * Converts movement outcome from String to CodeType.
    //	 *
    //	 * @param movementOutcome	the movement outcome
    //	 * @return the code type for the movement outcome
    //	 */
    //	public static CodeType toMovementOutcomeCodeType(String movementOutcome) {
    //		return new CodeType(MOVEMENT_OUTCOME, movementOutcome);
    //	}
    //
    //	private static CodeType toUpperCase(CodeType codeType) {
    //		CodeType upperCaseCodeType = null;
    //		if (codeType != null) {
    //			String set  = codeType.getSet();
    //			String code = codeType.getCode();
    //			upperCaseCodeType = new CodeType(
    //				(set  != null ? set.toUpperCase()  : null),
    //				(code != null ? code.toUpperCase() : null)
    //			);
    //		}
    //		return upperCaseCodeType;
    //	}
    //
    //	private AssociationType toLocationAssociation(Long identifier) {
    //		return new AssociationType(AssociationToClass.LOCATION.value(), identifier);
    //	}
    //
    //	private AssociationType toFacilityAssociation(Long identifier) {
    //		return new AssociationType(AssociationToClass.FACILITY.value(), identifier);
    //	}
    //
    //	private AssociationType toFacilityInternalLocationAssociation(Long identifier) {
    //		return new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), identifier);
    //	}
    //
    //	private AssociationType toOrganizationAssociation(Long identifier) {
    //		return new AssociationType(AssociationToClass.ORGANIZATION.value(), identifier);
    //	}
    //
    //	private AssociationType toActivityAssociation(Long identifier) {
    //		return new AssociationType(AssociationToClass.ACTIVITY.value(), identifier);
    //	}
    //
    //	private AssociationType toSupervisionAssociation(Long identifier) {
    //		return new AssociationType(AssociationToClass.SUPERVISION.value(), identifier);
    //	}
    //
    //	private Date getDateWithoutTime(Date date) {
    //		Calendar cal = Calendar.getInstance();
    //		cal.setTime(date);
    //		cal.set(Calendar.HOUR_OF_DAY, 0);
    //		cal.set(Calendar.MINUTE, 0);
    //		cal.set(Calendar.SECOND, 0);
    //		cal.set(Calendar.MILLISECOND, 0);
    //
    //		return cal.getTime();
    //	}

}