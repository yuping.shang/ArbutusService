package syscon.arbutus.product.services.movementactivity.contract.ejb;

import javax.naming.NamingException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.movement.contract.dto.*;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;

import static org.testng.Assert.*;

@ModuleConfig()
public class ConfigurationIT extends BaseIT {

    final static Long success = 1L;
    MovementService maSer;
    private Logger log = LoggerFactory.getLogger(MovementActivityServiceBusinessLogicTestIT.class);

    @BeforeMethod
    public void beforeMethod() {

    }

    @BeforeClass
    public void beforeClass() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");
        maSer = (MovementService) JNDILookUp(this.getClass(), MovementService.class);

        uc = super.initUserContext();

        if (log.isDebugEnabled()) {
            log.debug("beforeTest: end");
        }
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(maSer);
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testReset() {
        Long returnCode = maSer.deleteAll(uc);
        assertEquals(returnCode, success);
    }

    @Test(dependsOnMethods = { "testJNDILookup", "testReset" }, enabled = true)
    public void setAndGetCourtMovementReturnTimeConfiguration() {

        CourtMovementReturnTimeConfigurationType ret = null;

        CourtMovementReturnTimeConfigurationType config = new CourtMovementReturnTimeConfigurationType();

        config.setReturnTime("14:15");
        ret = maSer.setCourtMovementReturnTimeConfiguration(uc, config);
        assertNotNull(ret);
        assertNotNull(ret.getReturnTime());
        assertEquals(ret.getReturnTime(), "14:15");

        ret = maSer.getCourtMovementReturnTimeConfiguration(uc);
        assertNotNull(ret);
        assertNotNull(ret.getReturnTime());
        assertEquals(ret.getReturnTime(), "14:15");

        config.setReturnTime("16:15");
        ret = maSer.setCourtMovementReturnTimeConfiguration(uc, config);
        assertNotNull(ret);
        assertNotNull(ret.getReturnTime());
        assertEquals(ret.getReturnTime(), "16:15");

        ret = maSer.getCourtMovementReturnTimeConfiguration(uc);
        assertNotNull(ret);
        assertNotNull(ret.getReturnTime());
        assertEquals(ret.getReturnTime(), "16:15");
    }

    @Test(dependsOnMethods = { "testJNDILookup", "testReset" }, enabled = true)
    public void setSupervisionClosureConfiguration() {
        Set<SupervisionClosureConfigurationType> configurations = new HashSet<SupervisionClosureConfigurationType>(Arrays.asList(
                new SupervisionClosureConfigurationType[] { new SupervisionClosureConfigurationType("CRT", "HOSP", true),
                        new SupervisionClosureConfigurationType("TRNOJ", "ESC", true), new SupervisionClosureConfigurationType("APP", "CLA", true) }));

        maSer.setSupervisionClosureConfiguration(uc, configurations);

        configurations = new HashSet<SupervisionClosureConfigurationType>(Arrays.asList(
                new SupervisionClosureConfigurationType[] { new SupervisionClosureConfigurationType("CRT", "HOSP", false),
                        new SupervisionClosureConfigurationType("TRNOJ", "ESC", true), new SupervisionClosureConfigurationType("APP", "CLA", false) }));

        maSer.setSupervisionClosureConfiguration(uc, configurations);
    }

    @Test(dependsOnMethods = { "testJNDILookup", "testReset", "setSupervisionClosureConfiguration" }, enabled = true)
    public void searchSupervisionClosureConfiguration() {
        SupervisionClosureConfigurationsReturnType ret;

        SupervisionClosureConfigurationSearchType search = new SupervisionClosureConfigurationSearchType();

        try {
            ret = maSer.searchSupervisionClosureConfiguration(uc, null, null, null, null);
            assertNull(ret);
        } catch (ArbutusRuntimeException e) {
            log.info(e.getMessage());
        }

        search = new SupervisionClosureConfigurationSearchType();
        search.setMovementType("R");
        ret = maSer.searchSupervisionClosureConfiguration(uc, search, null, null, null);
        assertNotNull(ret);
        assertEquals(ret.getTotalSize().intValue(), 0);

        search = new SupervisionClosureConfigurationSearchType();
        search.setMovementType("CRT");
        ret = maSer.searchSupervisionClosureConfiguration(uc, search, null, null, null);
        assertNotNull(ret);
        assertEquals(ret.getTotalSize().intValue(), 1);

        search = new SupervisionClosureConfigurationSearchType();
        search.setMovementReason("ESC");
        ret = maSer.searchSupervisionClosureConfiguration(uc, search, null, null, null);
        assertNotNull(ret);
        assertEquals(ret.getTotalSize().intValue(), 1);

        search = new SupervisionClosureConfigurationSearchType();
        search.setMovementReason("ES");
        ret = maSer.searchSupervisionClosureConfiguration(uc, search, null, null, null);
        assertNotNull(ret);
        assertEquals(ret.getTotalSize().intValue(), 0);

        search = new SupervisionClosureConfigurationSearchType();
        search.setIsCloseSupervisionFlag(true);
        ret = maSer.searchSupervisionClosureConfiguration(uc, search, null, null, null);
        assertNotNull(ret);
        assert (ret.getTotalSize().intValue() > 0);

        search = new SupervisionClosureConfigurationSearchType();
        search.setIsCloseSupervisionFlag(false);
        ret = maSer.searchSupervisionClosureConfiguration(uc, search, null, null, null);
        assertNotNull(ret);
        assert (ret.getTotalSize().intValue() > 0);
    }

    @Test(dependsOnMethods = { "testJNDILookup", "testReset" })
    public void setTransferWaitlistPriorityConfiguration() {
        TransferWaitlistPriorityConfigurationType config = new TransferWaitlistPriorityConfigurationType();
        Set<TransferWaitlistPriorityConfigurationType> configs = new HashSet<TransferWaitlistPriorityConfigurationType>();

        TransferWaitlistPrioritiesConfigurationReturnType ret;

        config = new TransferWaitlistPriorityConfigurationType();
        config.setTransferWaitlistEntryPriority("HIG");
        config.setPriorityLevel(1L);
        configs.clear();
        try {
            maSer.setTransferWaitlistPriorityConfiguration(uc, configs);
            configs.add(config);
        } catch (ArbutusRuntimeException e) {
            log.info(e.getMessage());
        }

        config = new TransferWaitlistPriorityConfigurationType();
        configs.clear();
        config.setTransferWaitlistEntryPriority("HIGH");
        config.setPriorityLevel(1L);
        configs.add(config);

        TransferWaitlistPriorityConfigurationType config2 = new TransferWaitlistPriorityConfigurationType();
        config2.setTransferWaitlistEntryPriority("MEDIUM");
        config2.setPriorityLevel(2L);
        configs.add(config2);

        TransferWaitlistPriorityConfigurationType config3 = new TransferWaitlistPriorityConfigurationType();
        config3.setTransferWaitlistEntryPriority("Low");
        config3.setPriorityLevel(4L);
        configs.add(config3);

        maSer.setTransferWaitlistPriorityConfiguration(uc, configs);

        configs.remove(config3);
        config3 = new TransferWaitlistPriorityConfigurationType();
        config3.setTransferWaitlistEntryPriority("Low");
        config3.setPriorityLevel(3L);
        configs.add(config3);

        maSer.setTransferWaitlistPriorityConfiguration(uc, configs);
    }

    @Test(dependsOnMethods = { "testJNDILookup", "testReset", "setTransferWaitlistPriorityConfiguration" })
    public void searchTransferWaitlistPriorityConfiguration() {
        TransferWaitlistPriorityConfigurationSearchType search = new TransferWaitlistPriorityConfigurationSearchType();

        TransferWaitlistPrioritiesConfigurationReturnType ret;
        search.setTransferWaitlistEntryPriority("HIGH");
        ret = maSer.searchTransferWaitlistPriorityConfiguration(uc, search, null, null, null);
        assertNotNull(ret);
        assertEquals(ret.getTotalSize().intValue(), 1);

        search.setTransferWaitlistEntryPriority("HI");
        ret = maSer.searchTransferWaitlistPriorityConfiguration(uc, search, null, null, null);
        assertNotNull(ret);
        assertEquals(ret.getTotalSize().intValue(), 0);

        search = new TransferWaitlistPriorityConfigurationSearchType();
        search.setPriorityLevel(1L);
        ret = maSer.searchTransferWaitlistPriorityConfiguration(uc, search, null, null, null);
        assertNotNull(ret);
        assertEquals(ret.getTotalSize().intValue(), 1);

        search = new TransferWaitlistPriorityConfigurationSearchType();
        search.setPriorityLevel(2L);
        ret = maSer.searchTransferWaitlistPriorityConfiguration(uc, search, null, null, null);
        assertNotNull(ret);
        assertEquals(ret.getTotalSize().intValue(), 1);

        search = new TransferWaitlistPriorityConfigurationSearchType();
        search.setPriorityLevel(1111L);
        ret = maSer.searchTransferWaitlistPriorityConfiguration(uc, search, null, null, null);
        assertNotNull(ret);
        assertEquals(ret.getTotalSize().intValue(), 0);
    }
}
