package syscon.arbutus.product.services.audit.contract.ejb;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.audit.contract.dto.AuditControlType;
import syscon.arbutus.product.services.audit.contract.dto.AuditModule;
import syscon.arbutus.product.services.audit.contract.dto.AuditTrackingRevisionSearchType;
import syscon.arbutus.product.services.audit.contract.dto.AuditTrackingRevisionsReturnType;
import syscon.arbutus.product.services.audit.contract.dto.eventaudit.UserPageViewInmateType;
import syscon.arbutus.product.services.audit.contract.interfaces.AuditService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.realization.util.BeanHelper;

import static org.testng.Assert.assertNotNull;

public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);

    private AuditService service;

    @BeforeClass
    public void beforeClass() {

        log.info("beforeClass Begin - JNDI lookup");
        service = (AuditService) JNDILookUp(this.getClass(), AuditService.class);
        uc = super.initUserContext();
    }

    @AfterClass
    public void afterClass() {
    }

    @Test(enabled = false)
    public void lookupJNDI() {
        log.info("lookupJNDI Begin");
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "lookupJNDI" }, enabled = false)
    public void reset() {
        service.deleteAll(uc);
    }

    @Test(enabled = false)
    public void create() {

        Date presentDate = BeanHelper.createDate();
        AuditControlType auditControl = new AuditControlType();
        auditControl.setAuditModule(AuditModule.PERSON);
        auditControl.setOnAudit(true);
        auditControl.setUpdatedTime(presentDate);
        AuditControlType ret = service.create(uc, auditControl);
        assert (ret != null);

        AuditControlType auditControl2 = new AuditControlType();
        auditControl2.setAuditModule(AuditModule.HOUSING);
        auditControl2.setOnAudit(false);
        auditControl2.setUpdatedTime(presentDate);
        AuditControlType ret2 = service.create(uc, auditControl2);
        assert (ret2 != null);

    }

    @Test(enabled = false)
    public void isOnAudit() {

        boolean ret = service.isAuditOn(uc, AuditModule.PERSON);
        assert (ret == true);

    }

    @Test(enabled = false)
    public void recordPageView() {
        UserPageViewInmateType inmateInContext = new UserPageViewInmateType();
        inmateInContext.setInmateFirstName("aa");
        inmateInContext.setInmateLastName("bb");
        inmateInContext.setInmatePersonId(1L);
        inmateInContext.setInmatePersonIdentityId(2L);
        inmateInContext.setSupervisionDisplayId("sup1");
        inmateInContext.setViewedPageInfo("view visitor");

        service.recordUserPageView("lhan", inmateInContext);
    }

    @Test(enabled = false)
    public void getAuditRecordDetails() {
        Long revId = 10564L;
        String serviceName = "";

        service.getAuditRecordDetails(uc, revId, serviceName);

        revId = 10568L;
        serviceName = "";

        service.getAuditRecordDetails(uc, revId, serviceName);

		/*revId = 8430L;
        serviceName = "";
		
		service.getAuditRecordDetails(uc, revId, serviceName); */
    }

    @Test(enabled = false)
    public void searchAuditTrails() {

        AuditTrackingRevisionSearchType search = new AuditTrackingRevisionSearchType();
        search.setStartindex(0L);
        search.setResultsize(15L);

        AuditTrackingRevisionsReturnType rtn = service.searchAuditTrails(uc, search);

        search.setUserId("lhan");

        rtn = service.searchAuditTrails(uc, search);

        search.setService("supervision");

        rtn = service.searchAuditTrails(uc, search);

    }

}
