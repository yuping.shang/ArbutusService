package syscon.arbutus.product.services.appointment.contract.ejb;

import org.junit.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseTestJMockIt;
import syscon.arbutus.product.services.common.HibernateUtil;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;

/**
 * Created by os on 02/07/15.
 */
public class LightweightIT extends BaseTestJMockIt {
    IT superIT = new IT(appointmentService, personIdentityTest, supervisionService, supervisionTest, facilityService,
            (FacilityInternalLocationService) facilityInternalLocationService);

    @BeforeClass
    public void beforeClass() {
        super.beforeClass();
        superIT.supervisionId = supervisionTest.createSupervision();
        HibernateUtil.commitAndClose();
    }

    @AfterClass
    public void afterClass() {
        super.afterClass();
        superIT.afterClass();
        HibernateUtil.commitAndClose();
    }

    @Test
    public void createAppointment() {
        superIT.createAppointment();
    }

    @Test
    public void updateAppointment() {
        superIT.updateAppointment();
    }

    @Test
    public void search() {
        superIT.search();
    }

    @Test
    public void deleteAppointment() {
        superIT.deleteAppointment();
    }
}
