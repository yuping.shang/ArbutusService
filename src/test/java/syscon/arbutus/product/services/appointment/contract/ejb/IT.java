package syscon.arbutus.product.services.appointment.contract.ejb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.appointment.contract.dto.AppointmentSearchType;
import syscon.arbutus.product.services.appointment.contract.dto.Appointment;
import syscon.arbutus.product.services.appointment.contract.interfaces.AppointmentService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.PersonIdentityTest;
import syscon.arbutus.product.services.common.SupervisionTest;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

import java.util.Date;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * Created on 6/11/14.
 *
 * @author hshen
 */

public class IT extends BaseIT {

    static Logger log = LoggerFactory.getLogger(IT.class);
    private AppointmentService service;
    private PersonIdentityTest piTest;
    private SupervisionService supService;
    private SupervisionTest supervisionTest;
    private FacilityService facservice;
    private FacilityInternalLocationService facInService;

    protected Long supervisionId;

    public IT() {
    }

    public IT(AppointmentService service, PersonIdentityTest piTest, SupervisionService supService, SupervisionTest supervisionTest, FacilityService facservice,
            FacilityInternalLocationService facInService) {
        this.service = service;
        this.piTest = piTest;
        this.supService = supService;
        this.supervisionTest = supervisionTest;
        this.facservice = facservice;
        this.facInService = facInService;
    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (AppointmentService) JNDILookUp(this.getClass(), AppointmentService.class);
        supService = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        facservice = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        facInService = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        piTest = new PersonIdentityTest();
        uc = super.initUserContext();

        supervisionId = supervisionTest.createSupervision();
    }

    @AfterClass
    public void afterClass() {
        supervisionTest.deleteSupervision(supervisionId);
    }

    @Test(enabled = true)
    public void createAppointment() {
        Long appointmentId = createAppointmentType();
        assert (appointmentId != null);
        Appointment retApp = service.getAppointment(uc, appointmentId);
    }

    private Long createAppointmentType() {
        Appointment apppointmentType = new Appointment();
        apppointmentType.setSupervisionId(supervisionId);
        apppointmentType.setFacilityId(1L);
        apppointmentType.setLocationId(1L);
        apppointmentType.setStartTime(new Date());
        apppointmentType.setEndTime(new Date());
        apppointmentType.setComments("This is a testing dddd");
        apppointmentType.setType("Church");
        return service.createAppointment(uc, apppointmentType);
    }

    private void deleteAppointmentType(Long appointmentTypeId, Long version) {
        service.cancelAppointment(uc, appointmentTypeId, version);
    }

    @Test
    public void updateAppointment() {
        Appointment appointment = service.getAppointment(uc, createAppointmentType());

        appointment.setType("Classification");
        appointment.setComments("This is updated");
        service.updateAppointment(uc, appointment);

        Appointment retApp = service.getAppointment(uc, appointment.getAppointmentId());
        assertNotNull(retApp);
        assertEquals(appointment.getComments(), "This is updated");
        assertEquals(appointment.getType(), "Classification");

        appointment = service.getAppointment(uc, appointment.getAppointmentId());

        deleteAppointmentType(appointment.getAppointmentId(), appointment.getVersion());

    }

    @Test
    public void search() {
        Long appointmentTypeId = createAppointmentType();
        AppointmentSearchType searchType = new AppointmentSearchType();
        searchType.setSupervisionId(supervisionId);
        List<Appointment> resutls = service.search(uc, searchType);
        assertEquals(resutls.size(), 1);
        log.info(resutls.get(0).toString());

        deleteAppointmentType(appointmentTypeId, service.getAppointment(uc, appointmentTypeId).getVersion());
    }

    public void deleteAppointment() {
        Long appointmentTypeId = createAppointmentType();
        service.cancelAppointment(uc, appointmentTypeId, service.getAppointment(uc, appointmentTypeId).getVersion());
    }
}
