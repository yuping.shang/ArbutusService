package syscon.arbutus.product.services.registry.contract.ejb;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.registry.contract.dto.*;
import syscon.arbutus.product.services.registry.contract.interfaces.RegistryService;

@ModuleConfig
public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);
    private RegistryService service;

    @AfterMethod
    public void afterMethod() {
    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (RegistryService) JNDILookUp(this.getClass(), RegistryService.class);
        uc = super.initUserContext();
    }

    @BeforeMethod
    public void beforeMethod() {
        service.deleteAllUserData(uc, true);
    }

    @Test
    public void createSystemProfile() {
        SystemProfileType systemProfile = new SystemProfileType(SystemProfileCategoryEnum.DATE_FORMAT.value(), "yyyymmdd", "Short Date Format", false);
        SystemProfileType ret = service.createSystemProfile(uc, systemProfile);
        assert (ret != null);

    }

    @Test
    public void setPreferredSystemProfile() {
        SystemProfileType systemProfile = new SystemProfileType(SystemProfileCategoryEnum.DATE_FORMAT.value(), "yyyymmddhhss", "Long Date Format", false);
        SystemProfileType ret = service.createSystemProfile(uc, systemProfile);
        service.setPreferredSystemProfile(uc, SystemProfileCategoryEnum.DATE_FORMAT.value(), "yyyymmddhhss");
    }

    @Test
    public void getPreferredSystemProfile() {
        SystemProfileType inSystemProfile = new SystemProfileType(SystemProfileCategoryEnum.DATE_FORMAT.value(), "yyyymmddhhss", "Long Date Format", false);
        SystemProfileType outSystemProfile = service.createSystemProfile(uc, inSystemProfile);
        service.setPreferredSystemProfile(uc, SystemProfileCategoryEnum.DATE_FORMAT.value(), "yyyymmddhhss");
        //
        SystemProfileType outSystemProfile1 = service.getPreferredSystemProfile(uc, SystemProfileCategoryEnum.DATE_FORMAT.value());
        assert (outSystemProfile1 != null);

    }

    @Test
    public void getSystemProfilesByCategory() {
        SystemProfileType inSystemProfile = new SystemProfileType(SystemProfileCategoryEnum.DATE_FORMAT.value(), "yyyymmdd", "Short Date Format", false);
        SystemProfileType outSystemProfile = service.createSystemProfile(uc, inSystemProfile);
        //
        List<SystemProfileType> ret = service.getSystemProfilesByCategory(uc, SystemProfileCategoryEnum.DATE_FORMAT.value());
        assert (ret != null);
    }

    @Test
    public void updateSystemProfile() {
        SystemProfileType inSystemProfile = new SystemProfileType(SystemProfileCategoryEnum.DATE_FORMAT.value(), "yyyymmdd", "Short Date Format", false);
        SystemProfileType outSystemProfile = service.createSystemProfile(uc, inSystemProfile);
        //
        outSystemProfile.setDescription("New Short Date Format");
        SystemProfileType outSystemProfile1 = service.updateSystemProfile(uc, outSystemProfile);
        assert (outSystemProfile1 != null);
        assert (outSystemProfile1.getDescription().equals(outSystemProfile.getDescription()));
    }

    @Test
    public void deleteSystemProfile() {
        SystemProfileType inSystemProfile = new SystemProfileType(SystemProfileCategoryEnum.DATE_FORMAT.value(), "yyyymmdd", "Short Date Format", false);
        SystemProfileType outSystemProfile = service.createSystemProfile(uc, inSystemProfile);
        //
        service.deleteSystemProfile(uc, outSystemProfile.getProfileId());
        SystemProfileType outSystemProfile1 = service.getSystemProfile(uc, outSystemProfile.getProfileId());
        assert (outSystemProfile1 == null);
    }

    @Test
    public void createComponent() {
        ComponentType component = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        component.setIsActiveIn(true);
        component.setIsActiveOut(false);
        component.setIsInActive(null);
        component.setEntityType("Test");
        ComponentType ret = service.createComponent(uc, component);
        assert (ret != null);

    }

    @Test
    public void getComponent() {

        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        ComponentType outComponent = service.createComponent(uc, inComponent);
        assert (outComponent != null);

        ComponentType outComponent1 = service.getComponent(uc, outComponent.getComponentId(), false);
        assert (outComponent1 != null);
        assert (outComponent1.getComponentId().equals(outComponent.getComponentId()));
        assert (outComponent1.getParameterMap().size() == 0);
    }

    @Test
    public void getComponentByName() {

        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        inComponent.setIsInActive(true);
        inComponent.setIsActiveOut(true);
        inComponent.setIsActiveIn(true);
        ComponentType outComponent = service.createComponent(uc, inComponent);
        assert (outComponent != null);

        ComponentType outComponent1 = service.getComponentByName(uc, outComponent.getName(), false);
        assert (outComponent1 != null);
        assert (outComponent1.getComponentId().equals(outComponent.getComponentId()));
        assert (outComponent1.getParameterMap().size() == 0);

        ComponentType outComponent2 = service.getComponentByName(uc, ComponentCategoryEnum.ACTIVITY.value(), outComponent.getName(), false);
        assert (outComponent2 != null);
        assert (outComponent2.getComponentId().equals(outComponent.getComponentId()));
        assert (outComponent2.getParameterMap().size() == 0);

    }

    @Test
    public void createComponentParameter() {

        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        ComponentType outComponent = service.createComponent(uc, inComponent);
        assert (outComponent != null);

        ComponentParameterType inComponentParameter = new ComponentParameterType(outComponent.getComponentId(), "pname", "Process Name");
        ComponentParameterType outComponentParameter = service.createComponentParameter(uc, inComponentParameter);
        assert (outComponentParameter != null);

        inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryperson", "Search Person Page",
                Long.valueOf(AttributeType.ATTRIBUTE_USER));
        outComponent = service.createComponent(uc, inComponent);

        assert (outComponent != null);

        inComponentParameter = new ComponentParameterType(outComponent.getComponentId(), "pname", "Process Name");
        inComponentParameter.getValueList().add("AdmitOffender");
        inComponentParameter.getValueList().add("SearchHousing");
        outComponentParameter = service.createComponentParameter(uc, inComponentParameter);
        assert (outComponentParameter != null);
        assert (outComponentParameter.getValueList().size() == 2);
    }

    @Test
    public void createComponentAlias() {

        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        ComponentType outComponent = service.createComponent(uc, inComponent);
        assert (outComponent != null);

        ComponentAliasType inComponentAlias = new ComponentAliasType(outComponent.getComponentId(), "inqueryperson", "Search Person Page", AttributeType.ATTRIBUTE_USER);
        inComponentAlias.getParameterMap().put("pname", "AdmitOffender");
        ComponentAliasType outComponentAlias = service.createComponentAlias(uc, inComponentAlias);

        assert (outComponentAlias != null);
        assert (outComponentAlias.getParameterMap().size() == 1);
    }

    @Test
    public void getComponentAlias() {

        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        ComponentType outComponent = service.createComponent(uc, inComponent);
        assert (outComponent != null);
        //
        ComponentAliasType inComponentAlias = new ComponentAliasType(outComponent.getComponentId(), "inqueryperson", "Search Person Page", AttributeType.ATTRIBUTE_USER);
        inComponentAlias.getParameterMap().put("pname", "AdmitOffender");
        ComponentAliasType outComponentAlias = service.createComponentAlias(uc, inComponentAlias);
        assert (outComponentAlias != null);
        //
        ComponentAliasType outComponentAlias1 = service.getComponentAlias(uc, outComponentAlias.getComponentAliasId(), true);
        assert (outComponentAlias1 != null);
        assert (outComponentAlias1.getComponentAliasId().equals(outComponentAlias.getComponentAliasId()));
        assert (outComponentAlias1.getParameterMap().size() == 1);

    }

    @Test
    public void getComponentAliasByName() {

        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        ComponentType outComponent = service.createComponent(uc, inComponent);
        assert (outComponent != null);
        //
        ComponentAliasType inComponentAlias = new ComponentAliasType(outComponent.getComponentId(), "inqueryperson", "Search Person Page", AttributeType.ATTRIBUTE_USER);
        inComponentAlias.getParameterMap().put("pname", "AdmitOffender");
        ComponentAliasType outComponentAlias = service.createComponentAlias(uc, inComponentAlias);
        assert (outComponentAlias != null);
        //
        ComponentAliasType outComponentAlias1 = service.getComponentAliasByName(uc, outComponentAlias.getName(), true);
        assert (outComponentAlias1 != null);
        assert (outComponentAlias1.getComponentAliasId().equals(outComponentAlias.getComponentAliasId()));
        assert (outComponentAlias1.getParameterMap().size() == 1);
        //
        ComponentAliasType outComponentAlias2 = service.getComponentAliasByName(uc, ComponentCategoryEnum.ACTIVITY.value(), outComponentAlias.getName(), true);
        assert (outComponentAlias2 != null);
        assert (outComponentAlias2.getComponentAliasId().equals(outComponentAlias.getComponentAliasId()));
        assert (outComponentAlias2.getParameterMap().size() == 1);

    }

    @Test
    public void createComponentAliasParameter() {

        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        ComponentType outComponent = service.createComponent(uc, inComponent);
        assert (outComponent != null);
        //
        ComponentAliasType inComponentAlias = new ComponentAliasType(outComponent.getComponentId(), "inqueryperson", "Search Person Page", AttributeType.ATTRIBUTE_USER);
        ComponentAliasType outComponentAlias = service.createComponentAlias(uc, inComponentAlias);
        assert (outComponentAlias != null);
        //
        ComponentAliasParameterType outComponentAliasParameter = service.createComponentAliasParameter(uc, outComponentAlias.getComponentAliasId(), "pname",
                "AdmitOffender");
        assert (outComponentAliasParameter != null);
        //
        ComponentAliasType outComponentAlias1 = service.getComponentAlias(uc, outComponentAlias.getComponentAliasId(), true);
        assert (outComponentAlias1 != null);
        assert (outComponentAlias1.getComponentAliasId().equals(outComponentAlias.getComponentAliasId()));
        assert (outComponentAlias1.getParameterMap().size() == 1);
        //
        service.deleteComponentAliasParameter(uc, outComponentAlias.getComponentAliasId(), "pname");
        ComponentAliasType outComponentAlias2 = service.getComponentAlias(uc, outComponentAlias.getComponentAliasId(), true);
        assert (outComponentAlias2 != null);
        assert (outComponentAlias2.getComponentAliasId().equals(outComponentAlias.getComponentAliasId()));
        assert (outComponentAlias2.getParameterMap().size() == 0);

    }

    @Test
    public void createFacilitySet() {

        FacilitySetType inFacilitySet = new FacilitySetType("facilityset-a", "Facility Set A", AttributeType.ATTRIBUTE_USER);
        inFacilitySet.getFacilityList().add(1L);
        FacilitySetType outFacilitySet = service.createFacilitySet(uc, inFacilitySet);
        assert (outFacilitySet != null);
        assert (outFacilitySet.getFacilityList().size() == 1);

    }

    @Test
    public void deleteFacilitySet() {

        FacilitySetType inFacilitySet = new FacilitySetType("facilityset-a", "Facility Set A", AttributeType.ATTRIBUTE_USER);
        inFacilitySet.getFacilityList().add(1L);
        FacilitySetType outFacilitySet = service.createFacilitySet(uc, inFacilitySet);
        assert (outFacilitySet != null);
        assert (outFacilitySet.getFacilityList().size() == 1);
        //
        service.deleteFacilitySet(uc, outFacilitySet.getFacilitySetId());
        //
        FacilitySetType outFacilitySet1 = service.getFacilitySet(uc, outFacilitySet.getFacilitySetId(), true);
        assert (outFacilitySet1 == null);
    }

    @Test
    public void createRole() {

        RoleType inRole = new RoleType(RoleCategoryEnum.SERVICE.value(), "Arbutust", "Arbutus", AttributeType.ATTRIBUTE_USER);
        RoleType outRole = service.createRole(uc, inRole);
        assert (outRole != null);
    }

    @Test
    public void getRole() {

        RoleType inRole = new RoleType(RoleCategoryEnum.SERVICE.value(), "Arbutust", "Arbutus", AttributeType.ATTRIBUTE_USER);
        RoleType outRole = service.createRole(uc, inRole);
        assert (outRole != null);
        //
        RoleType outRole1 = service.getRole(uc, outRole.getRoleId(), false);
        assert (outRole1 != null);
        assert (outRole.getName().equals(outRole.getName()));
    }

    @Test
    public void getRoleByName() {

        RoleType inRole = new RoleType(RoleCategoryEnum.SERVICE.value(), "Arbutust", "Arbutus", AttributeType.ATTRIBUTE_USER);
        RoleType outRole = service.createRole(uc, inRole);
        assert (outRole != null);
        //
        RoleType outRole1 = service.getRoleByName(uc, outRole.getName(), false);
        assert (outRole1 != null);
        assert (outRole.getName().equals(outRole.getName()));
    }

    @Test
    public void addComponentAliasToRoleByName() {

        RoleType inRole = new RoleType(RoleCategoryEnum.SERVICE.value(), "Arbutust", "Arbutus", AttributeType.ATTRIBUTE_USER);
        RoleType outRole = service.createRole(uc, inRole);
        assert (outRole != null);
        //
        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        ComponentType outComponent = service.createComponent(uc, inComponent);
        assert (outComponent != null);
        //
        ComponentAliasType inComponentAlias = new ComponentAliasType(outComponent.getComponentId(), "inqueryperson", "Search Person Page", AttributeType.ATTRIBUTE_USER);
        ComponentAliasType outComponentAlias = service.createComponentAlias(uc, inComponentAlias);
        assert (outComponentAlias != null);
        //
        service.addComponentAliasToRoleByName(uc, outRole.getRoleId(), outComponentAlias.getName());
    }

    @Test
    public void getComponentAliasFromRoleByName() {

        RoleType inRole = new RoleType(RoleCategoryEnum.SERVICE.value(), "Arbutust", "Arbutus", AttributeType.ATTRIBUTE_USER);
        RoleType outRole = service.createRole(uc, inRole);
        assert (outRole != null);
        //
        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        ComponentType outComponent = service.createComponent(uc, inComponent);
        assert (outComponent != null);
        //
        ComponentAliasType inComponentAlias = new ComponentAliasType(outComponent.getComponentId(), "inqueryperson", "Search Person Page", AttributeType.ATTRIBUTE_USER);
        ComponentAliasType outComponentAlias = service.createComponentAlias(uc, inComponentAlias);
        assert (outComponentAlias != null);
        //
        service.addComponentAliasToRoleByName(uc, outRole.getRoleId(), outComponentAlias.getName());
        //
        ComponentAliasType outComponentAlias1 = service.getComponentAliasFromRoleByName(uc, outRole.getRoleId(), outComponentAlias.getName());
        assert (outComponentAlias1 != null);
        assert (outComponentAlias1.getName().equals(outComponentAlias.getName()));
    }

    @Test
    public void removeComponentAliasFromRoleByName() {

        RoleType inRole = new RoleType(RoleCategoryEnum.SERVICE.value(), "Arbutust", "Arbutus", AttributeType.ATTRIBUTE_USER);
        RoleType outRole = service.createRole(uc, inRole);
        assert (outRole != null);
        //
        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        ComponentType outComponent = service.createComponent(uc, inComponent);
        assert (outComponent != null);
        //
        ComponentAliasType inComponentAlias = new ComponentAliasType(outComponent.getComponentId(), "inqueryperson", "Search Person Page", AttributeType.ATTRIBUTE_USER);
        ComponentAliasType outComponentAlias = service.createComponentAlias(uc, inComponentAlias);
        assert (outComponentAlias != null);
        //
        service.addComponentAliasToRoleByName(uc, outRole.getRoleId(), outComponentAlias.getName());
        //
        service.removeComponentAliasFromRoleByName(uc, outRole.getRoleId(), outComponentAlias.getName());
    }

    @Test
    public void getComponentParameterByName() {
        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        ComponentType outComponent = service.createComponent(uc, inComponent);
        assert (outComponent != null);

        ComponentParameterType inComponentParameter = new ComponentParameterType(outComponent.getComponentId(), "pname", "Process Name");
        ComponentParameterType outComponentParameter = service.createComponentParameter(uc, inComponentParameter);
        assert (outComponentParameter != null);

        inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryperson", "Search Person Page",
                Long.valueOf(AttributeType.ATTRIBUTE_USER));
        outComponent = service.createComponent(uc, inComponent);

        assert (outComponent != null);

        inComponentParameter = new ComponentParameterType(outComponent.getComponentId(), "pname", "Process Name");
        inComponentParameter.getValueList().add("AdmitOffender");
        inComponentParameter.getValueList().add("SearchHousing");
        outComponentParameter = service.createComponentParameter(uc, inComponentParameter);
        assert (outComponentParameter != null);
        assert (outComponentParameter.getValueList().size() == 2);
        //
        ComponentParameterType outComponentParameter1 = service.getComponentParameterByName(uc, outComponent.getComponentId(), "pname", true);
        assert (outComponentParameter1 != null);
        assert (outComponentParameter1.getValueList().size() == 2);
    }

    @Test
    public void getComponentParameterValue() {
        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        ComponentType outComponent = service.createComponent(uc, inComponent);
        assert (outComponent != null);

        ComponentParameterType inComponentParameter = new ComponentParameterType(outComponent.getComponentId(), "pname", "Process Name");
        ComponentParameterType outComponentParameter = service.createComponentParameter(uc, inComponentParameter);
        assert (outComponentParameter != null);

        inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryperson", "Search Person Page",
                Long.valueOf(AttributeType.ATTRIBUTE_USER));
        outComponent = service.createComponent(uc, inComponent);

        assert (outComponent != null);

        inComponentParameter = new ComponentParameterType(outComponent.getComponentId(), "pname", "Process Name");
        inComponentParameter.getValueList().add("AdmitOffender");
        inComponentParameter.getValueList().add("SearchHousing");
        outComponentParameter = service.createComponentParameter(uc, inComponentParameter);
        assert (outComponentParameter != null);
        assert (outComponentParameter.getValueList().size() == 2);

        //
        String parameterValueOut = service.getComponentParameterValue(uc, outComponentParameter.getComponentParameterId(), "AdmitOffender");
        assert (parameterValueOut != null);
        assert (parameterValueOut.equals("AdmitOffender"));
    }

    @Test
    public void createComponentParameterValue() {
        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        ComponentType outComponent = service.createComponent(uc, inComponent);
        assert (outComponent != null);

        ComponentParameterType inComponentParameter = new ComponentParameterType(outComponent.getComponentId(), "pname", "Process Name");
        ComponentParameterType outComponentParameter = service.createComponentParameter(uc, inComponentParameter);
        assert (outComponentParameter != null);

        inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryperson", "Search Person Page",
                Long.valueOf(AttributeType.ATTRIBUTE_USER));
        outComponent = service.createComponent(uc, inComponent);

        assert (outComponent != null);

        inComponentParameter = new ComponentParameterType(outComponent.getComponentId(), "pname", "Process Name");
        inComponentParameter.getValueList().add("AdmitOffender");
        inComponentParameter.getValueList().add("SearchHousing");
        outComponentParameter = service.createComponentParameter(uc, inComponentParameter);
        assert (outComponentParameter != null);
        assert (outComponentParameter.getValueList().size() == 2);

        //
        service.createComponentParameterValue(null, outComponentParameter.getComponentParameterId(), "OffenderCaution");
        ComponentParameterType outComponentParameter1 = service.getComponentParameterByName(uc, outComponent.getComponentId(), "pname", true);
        assert (outComponentParameter1 != null);
        assert (outComponentParameter1.getValueList().size() == 3);

    }

    @Test
    public void deleteComponentParameterValue() {
        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        ComponentType outComponent = service.createComponent(uc, inComponent);
        assert (outComponent != null);

        ComponentParameterType inComponentParameter = new ComponentParameterType(outComponent.getComponentId(), "pname", "Process Name");
        ComponentParameterType outComponentParameter = service.createComponentParameter(uc, inComponentParameter);
        assert (outComponentParameter != null);

        inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryperson", "Search Person Page",
                Long.valueOf(AttributeType.ATTRIBUTE_USER));
        outComponent = service.createComponent(uc, inComponent);

        assert (outComponent != null);

        inComponentParameter = new ComponentParameterType(outComponent.getComponentId(), "pname", "Process Name");
        inComponentParameter.getValueList().add("AdmitOffender");
        inComponentParameter.getValueList().add("SearchHousing");
        outComponentParameter = service.createComponentParameter(uc, inComponentParameter);
        assert (outComponentParameter != null);
        assert (outComponentParameter.getValueList().size() == 2);

        //
        service.deleteComponentParameterValue(null, outComponentParameter.getComponentParameterId(), "AdmitOffender");
        ComponentParameterType outComponentParameter1 = service.getComponentParameterByName(uc, outComponent.getComponentId(), "pname", true);
        assert (outComponentParameter1 != null);
        assert (outComponentParameter1.getValueList().size() == 1);
    }

    @Test
    public void getComponentAliasParameterByName() {
        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        ComponentType outComponent = service.createComponent(uc, inComponent);
        assert (outComponent != null);
        //
        ComponentAliasType inComponentAlias = new ComponentAliasType(outComponent.getComponentId(), "inqueryperson", "Search Person Page", AttributeType.ATTRIBUTE_USER);
        ComponentAliasType outComponentAlias = service.createComponentAlias(uc, inComponentAlias);
        assert (outComponentAlias != null);
        //
        ComponentAliasParameterType outComponentAliasParameter = service.createComponentAliasParameter(uc, outComponentAlias.getComponentAliasId(), "pname",
                "AdmitOffender");
        assert (outComponentAliasParameter != null);
        //
        ComponentAliasParameterType outComponentAliasParameter1 = service.getComponentAliasParameterByName(uc, outComponentAlias.getComponentAliasId(), "pname");
        assert (outComponentAliasParameter1 != null);
        assert (outComponentAliasParameter1.getName().equals(outComponentAliasParameter.getName()));
    }

    @Test
    public void getFacilitySetByName() {
        //
        FacilitySetType inFacilitySet = new FacilitySetType("facilityset-a", "Facility Set A", AttributeType.ATTRIBUTE_USER);
        inFacilitySet.getFacilityList().add(1L);
        FacilitySetType outFacilitySet = service.createFacilitySet(uc, inFacilitySet);
        assert (outFacilitySet != null);
        assert (outFacilitySet.getFacilityList().size() == 1);
        //
        FacilitySetType outFacilitySet1 = service.getFacilitySetByName(uc, "facilityset-a", true);
        assert (outFacilitySet1 != null);
        assert (outFacilitySet1.getFacilityList().size() == 1);

    }
}
