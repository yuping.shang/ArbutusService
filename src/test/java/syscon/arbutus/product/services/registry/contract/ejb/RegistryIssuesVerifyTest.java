package syscon.arbutus.product.services.registry.contract.ejb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.registry.contract.dto.ComponentAliasType;
import syscon.arbutus.product.services.registry.contract.interfaces.RegistryService;

/**
 * Only run this test manually to verify some issues.
 *
 * @author LHan
 */
@ModuleConfig
public class RegistryIssuesVerifyTest extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(RegistryIssuesVerifyTest.class);
    private RegistryService service;

    @AfterMethod
    public void afterMethod() {
    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (RegistryService) JNDILookUp(this.getClass(), RegistryService.class);
        uc = super.initUserContext();
    }

    @BeforeMethod
    public void beforeMethod() {
    }

    @Test
    public void getComponentAliasByName() {

        String name1 = "offendercautionparam1";

        ComponentAliasType outComponentAlias1 = service.getComponentAliasByName(uc, name1, true);
        assert (outComponentAlias1 != null);
        assert (outComponentAlias1.getParameterMap().size() == 2);

        //
        String name2 = "offendercautionparam2";

        ComponentAliasType outComponentAlias2 = service.getComponentAliasByName(uc, name2, true);
        assert (outComponentAlias2 != null);
        assert (outComponentAlias2.getParameterMap().size() == 2);

    }
}
