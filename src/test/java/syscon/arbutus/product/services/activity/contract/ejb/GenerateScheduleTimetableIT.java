package syscon.arbutus.product.services.activity.contract.ejb;

import javax.naming.NamingException;
import java.math.BigInteger;
import java.util.*;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;
import syscon.arbutus.product.services.activity.contract.dto.*;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class GenerateScheduleTimetableIT extends BaseIT {

    final static Long success = 1L;

    private static Logger log = LoggerFactory.getLogger(ActivityServiceIT.class);

    private Long facilityId;
    private ActivityService service;

    @BeforeMethod
    public void beforeMethod() {

    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        uc = super.initUserContext();

        BasicConfigurator.configure();

        // Facility
        FacilityTest f = new FacilityTest();
        facilityId = f.createFacility();
    }

    @BeforeTest
    public void beforeTest() throws NamingException, Exception {

    }

    @AfterClass
    public void afterClass() {
        //		if (this.facilityId != null) {
        //			FacilityTest f = new FacilityTest();
        //			f.deleteFacility(facilityId);
        //		}
    }

    @Test(enabled = true)
    public void once() {

        Long ret = service.deleteAll(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());

        ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, ActivityCategory.DISCIPLINE.value(), 31L);
        ScheduleConfigVariableType configRet = service.createScheduleConfigVars(uc, config);
        assertNotNull(configRet);
        config = configRet;

        Schedule sch = new Schedule();

        sch.setScheduleCategory(ActivityCategory.DISCIPLINE.value());
        sch.setFacilityId(facilityId);
        sch.setCapacity(20L);
        sch.setEffectiveDate(getDateWithoutTime(2012, 1, 1));
        sch.setScheduleStartTime("11:00");

        OnceRecurrencePatternType oncePatternType = new OnceRecurrencePatternType(getDateWithoutTime(2012, 2, 2));
        sch.setRecurrencePattern(oncePatternType);

        Schedule schRet = service.createSchedule(uc, sch);
        assertNotNull(schRet);
        sch = schRet;

        Set<ScheduleTimeslotType> timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 1, 0, 0, 0), getDate(2012, 2, 3, 0, 0, 0), true);
        assertNotNull(timeslotsRet);
        assertEquals(timeslotsRet.size(), 1);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 2, 0, 0, 0), getDate(2012, 2, 2, 0, 0, 0), true);
        assertNotNull(timeslotsRet);
        assertEquals(timeslotsRet.size(), 1);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 3, 0, 0, 0), getDate(2012, 2, 4, 0, 0, 0), true);
        assertEquals(timeslotsRet.size(), 0);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 1, 0, 0, 0), getDate(2012, 2, 2, 0, 0, 0), true);
        assertEquals(timeslotsRet.size(), 1);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 2, 0, 0, 0), getDate(2012, 2, 3, 0, 0, 0), true);
        assertEquals(timeslotsRet.size(), 1);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 1, 1, 0, 0, 0), null, false);
        assertEquals(timeslotsRet.size(), 1);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 1, 0, 0, 0), null, false);
        assertEquals(timeslotsRet.size(), 1);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 2, 0, 0, 0), null, false);
        assertEquals(timeslotsRet.size(), 1);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 3, 0, 0, 0), null, false);
        assertEquals(timeslotsRet.size(), 0);

        sch.setScheduleEndTime("15:25");
        schRet = service.updateSchedule(uc, sch);
        sch = schRet;

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 1, 0, 0, 0), getDate(2012, 2, 3, 0, 0, 0), true);
        assertEquals(timeslotsRet.size(), 1);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 2, 0, 0, 0), getDate(2012, 2, 2, 0, 0, 0), true);
        assertEquals(timeslotsRet.size(), 1);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 3, 0, 0, 0), getDate(2012, 2, 4, 0, 0, 0), true);
        assertEquals(timeslotsRet.size(), 0);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 1, 0, 0, 0), getDate(2012, 2, 2, 0, 0, 0), true);
        assertEquals(timeslotsRet.size(), 1);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 2, 0, 0, 0), getDate(2012, 2, 3, 0, 0, 0), true);
        assertEquals(timeslotsRet.size(), 1);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 1, 1, 0, 0, 0), null, false);
        assertEquals(timeslotsRet.size(), 1);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 1, 0, 0, 0), null, false);
        assertEquals(timeslotsRet.size(), 1);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 2, 0, 0, 0), null, false);
        assertEquals(timeslotsRet.size(), 1);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 2, 3, 0, 0, 0), null, false);
        assertEquals(timeslotsRet.size(), 0);

        service.deleteAllSchedule(uc);
        service.deleteAllScheduleConfigVar(uc);

    }

    @Test(dependsOnMethods = { "once" }, enabled = true)
    public void daily() {

        Long ret = service.deleteAll(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());

        int thisYear = Calendar.getInstance().get(Calendar.YEAR);

        ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, ActivityCategory.DISCIPLINE.value(), 31L);
        ScheduleConfigVariableType configRet = service.createScheduleConfigVars(uc, config);
        assertNotNull(configRet);
        config = configRet;

        Schedule sch = new Schedule();
        sch.setScheduleCategory(ActivityCategory.DISCIPLINE.value());
        sch.setFacilityId(facilityId);
        sch.setCapacity(20L);
        sch.setEffectiveDate(getDateWithoutTime(2012, 1, 1));
        sch.setScheduleStartTime("00:00");
        sch.setRecurrencePattern(new DailyWeeklyCustomRecurrencePatternType(1L, false, null));

        Schedule schRet = service.createSchedule(uc, sch);
        assertNotNull(schRet);
        sch = schRet;

        Set<ScheduleTimeslotType> timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(thisYear, 1, 1, 0, 0, 0), getDate(thisYear, 12, 31, 23, 0, 0),
                true);
        List<Date> dates = new ArrayList<>();
        assertEquals(timeslotsRet.size(), 365);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(thisYear, 1, 1, 0, 0, 0), null, false);
        assertEquals(timeslotsRet.size(), config.getNumOfNextAvailableTimeslots().intValue());

        sch.setRecurrencePattern(new DailyWeeklyCustomRecurrencePatternType(1L, true, null));
        schRet = service.updateSchedule(uc, sch);
        assertNotNull(schRet);
        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(thisYear, 1, 1, 0, 0, 0), getDate(thisYear, 12, 31, 0, 0, 0), true);
        assertEquals(timeslotsRet.size(), 261);

        sch.setRecurrencePattern(new DailyWeeklyCustomRecurrencePatternType(1L, false, null));
        schRet = service.updateSchedule(uc, sch);
        assertNotNull(schRet);

        sch.setTerminationDate(this.getDateWithoutTime(thisYear, 12, 31));
        sch.setScheduleEndTime("13:00");
        schRet = service.updateSchedule(uc, sch);
        assertNotNull(schRet);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(thisYear, 1, 1, 0, 0, 0), getDate(thisYear, 12, 31, 0, 0, 0), true);
        assertNotNull(timeslotsRet);
        assertEquals(timeslotsRet.size(), 365);

        service.deleteAllSchedule(uc);
        // service.deleteAllScheduleConfigVar(uc);

        sch = new Schedule();
        sch.setScheduleCategory(ActivityCategory.DISCIPLINE.value());
        sch.setFacilityId(facilityId);
        sch.setCapacity(20L);
        sch.setEffectiveDate(getDateWithoutTime(2012, 1, 1));
        sch.setScheduleStartTime("11:00");
        sch.setRecurrencePattern(new DailyWeeklyCustomRecurrencePatternType(1L, false, new HashSet<String>(Arrays.asList(new String[] { "FRI", "SAT", "SUN" }))));

        schRet = service.createSchedule(uc, sch);
        assertNotNull(schRet);
        sch = schRet;

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(thisYear, 1, 1, 0, 0, 0), getDate(thisYear, 12, 31, 23, 0, 0), true);

        assertNotNull(timeslotsRet);
        // assertEquals(timeslotsRet.size(), 156);
        assertEquals(timeslotsRet.size(), 157);

        service.deleteAllSchedule(uc);
        service.deleteAllScheduleConfigVar(uc);
    }

    @Test(enabled = true)
    public void weekly() {

        Long ret = service.deleteAll(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());

        ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, ActivityCategory.CASE.value(), 20L);
        ScheduleConfigVariableType configRet = service.createScheduleConfigVars(uc, config);
        assertNotNull(configRet);
        config = configRet;

        Schedule sch = new Schedule();
        sch.setScheduleCategory(ActivityCategory.CASE.value());
        sch.setFacilityId(facilityId);
        sch.setCapacity(20L);
        sch.setEffectiveDate(getDateWithoutTime(2012, 5, 1));
        sch.setScheduleStartTime("11:00");
        sch.setRecurrencePattern(new WeeklyRecurrencePatternType(1L, new HashSet<String>(Arrays.asList(new String[] { "MON", "WED", "FRI" }))));

        Schedule schRet = service.createSchedule(uc, sch);
        assertNotNull(schRet);

        Set<ScheduleTimeslotType> timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 1, 1, 0, 0, 0), getDate(2012, 12, 31, 0, 0, 0), true);
        assertNotNull(timeslotsRet);
        // assertEquals(timeslotsRet.size(), 104);
        assertEquals(timeslotsRet.size(), 105);

        service.deleteAllSchedule(uc);
        service.deleteAllScheduleConfigVar(uc);
    }

    @Test(enabled = true)
    public void monthly() {

        Long ret = service.deleteAll(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());

        ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, ActivityCategory.HBDMGMT.value(), 31L);
        ScheduleConfigVariableType configRet = service.createScheduleConfigVars(uc, config);
        assertNotNull(configRet);
        config = configRet;

        Schedule sch = new Schedule();
        sch.setScheduleCategory(ActivityCategory.HBDMGMT.value());
        sch.setFacilityId(facilityId);
        sch.setCapacity(31L);
        sch.setEffectiveDate(getDateWithoutTime(2012, 1, 1));
        sch.setScheduleStartTime("11:00");
        sch.setRecurrencePattern(new MonthlyRecurrencePatternType(1L, 2L, null, null));

        Schedule schRet = service.createSchedule(uc, sch);
        assertNotNull(schRet);

        sch = schRet;

        Set<ScheduleTimeslotType> timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 1, 1, 0, 0, 0), getDate(2012, 12, 31, 0, 0, 0), true);
        assertNotNull(timeslotsRet);
        // assertEquals(timeslotsRet.size(), 12);
        assertEquals(timeslotsRet.size(), 13);

        sch.setRecurrencePattern(new MonthlyRecurrencePatternType(1L, null, "MON", "FIRST"));

        schRet = service.updateSchedule(uc, sch);
        assertNotNull(schRet);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 1, 1, 0, 0, 0), getDate(2012, 12, 31, 0, 0, 0), true);
        assertNotNull(timeslotsRet);
        // assertEquals(timeslotsRet.size(), 12);
        assertEquals(timeslotsRet.size(), 13);

        service.deleteAllSchedule(uc);
        service.deleteAllScheduleConfigVar(uc);
    }

    @Test(enabled = true)
    public void yearly() {

        Long ret = service.deleteAll(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());

        ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, ActivityCategory.INCIDENT.value(), 31L);
        ScheduleConfigVariableType configRet = service.createScheduleConfigVars(uc, config);
        assertNotNull(configRet);
        config = configRet;

        Schedule sch = new Schedule();
        sch.setScheduleCategory(ActivityCategory.INCIDENT.value());
        sch.setFacilityId(facilityId);
        sch.setCapacity(31L);
        sch.setEffectiveDate(getDateWithoutTime(2012, 1, 1));
        sch.setScheduleStartTime("11:00");
        sch.setRecurrencePattern(new YearlyRecurrencePatternType(1L, 2L, "DEC", null, null));

        Schedule schRet = service.createSchedule(uc, sch);
        assertNotNull(schRet);

        sch = schRet;

        Set<ScheduleTimeslotType> timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 1, 1, 0, 0, 0), getDate(2014, 12, 31, 0, 0, 0), true);
        assertNotNull(timeslotsRet);
        // assertEquals(timeslotsRet.size(), 3);
        assertEquals(timeslotsRet.size(), 4);

        sch.setRecurrencePattern(new YearlyRecurrencePatternType(1L, null, "DEC", "MON", "FIRST"));

        schRet = service.updateSchedule(uc, sch);
        assertNotNull(schRet);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 1, 1, 0, 0, 0), getDate(2012, 12, 31, 0, 0, 0), true);
        assertNotNull(timeslotsRet);
        // assertEquals(timeslotsRet.size(), 1);
        assertEquals(timeslotsRet.size(), 2);
        service.deleteAllSchedule(uc);
        service.deleteAllScheduleConfigVar(uc);
    }

    //	@Test(enabled = false)
    //	public void testGenerateScheduleTimetable_Defect_1006() {
    //
    //		Long ret = service.reset(uc);
    //		assertEquals(ret, ReturnCode.Success.returnCode());
    //
    //		// Defect ID 1006
    //		ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, new CodeType("ActivityCategory", ActivityCategory.VISITATION.value()), 20L);
    //		ScheduleConfigVariableReturnType configRet = service.createScheduleConfigVars(uc, config);
    //		assertEquals(configRet.getReturnCode(), success);
    //		assertNotNull(configRet.getScheduleConfigVariable());
    //		config = configRet.getScheduleConfigVariable();
    //
    //		ScheduleType sch = new ScheduleType(null,
    //				new CodeType("ActivityCategory", ActivityCategory.VISITATION.value()),
    //				new AssociationType(ScheduleType.AssociationToClass.FacilityService.value(), 3L),
    //				20L,
    //				getDateWithoutTime(2012, 5, 1),
    //				null,
    //				"11:00",
    //				null,
    //				new WeeklyRecurrencePatternType(1L,
    //						new HashSet<CodeType>(
    //								Arrays.asList(new CodeType[] {
    //										new CodeType("Weekday", Weekday.MON.value()),
    //										new CodeType("Weekday", Weekday.WED.value()),
    //										new CodeType("Weekday", Weekday.FRI.value())
    //								}))),
    //				null,
    //				null,
    //				null);
    //
    //		assertTrue(sch.isValid());
    //
    //		ScheduleReturnType schRet = service.createSchedule(uc, sch);
    //		assertEquals(schRet.getReturnCode(), success);
    //		assertNotNull(schRet.getSchedule());
    //
    //		ScheduleTimeslotsReturnType timeslotsRet = service.generateTimeslotIterator(
    //				uc,
    //				config,
    //				getDate(2012, 1, 1, 0, 0, 0),
    //				getDate(2012, 12, 31, 0, 0, 0),
    //				true);
    //		assertEquals(timeslotsRet.getReturnCode(), success);
    //		assertEquals(timeslotsRet.getCount().intValue(), 104);
    //	}
    //
    //	@Test(enabled = false)
    //	public void testGenerateScheduleTimetable_Defect_686() {
    //
    //		Long ret = service.reset(uc);
    //		assertEquals(ret, ReturnCode.Success.returnCode());
    //
    //		// Defect ID 686
    //		ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, new CodeType("ActivityCategory", ActivityCategory.FACOUNT.value()), 100L);
    //		ScheduleConfigVariableReturnType configRet = service.createScheduleConfigVars(uc, config);
    //		assertEquals(configRet.getReturnCode(), success);
    //		assertNotNull(configRet.getScheduleConfigVariable());
    //		config = configRet.getScheduleConfigVariable();
    //
    //		ScheduleType sch = new ScheduleType(null,
    //				new CodeType("ActivityCategory", ActivityCategory.FACOUNT.value()),
    //				new AssociationType(ScheduleType.AssociationToClass.FacilityService.value(), 3L),
    //				20L,
    //				getDateWithoutTime(2012, 5, 1),
    //				getDateWithoutTime(2015, 12, 31),
    //				"15:00",
    //				"17:00",
    //				null,
    //				null,
    //				null,
    //				null);
    //		assertTrue(sch.isValid());
    //
    //		ScheduleReturnType schRet = service.createSchedule(uc, sch);
    //		schRet = service.createSchedule(uc, sch);
    //		schRet = service.createSchedule(uc, sch);
    //		schRet = service.createSchedule(uc, sch);
    //		schRet = service.createSchedule(uc, sch);
    //
    //		schRet = service.createSchedule(uc, sch);
    //		assertEquals(schRet.getReturnCode(), success);
    //		assertNotNull(schRet.getSchedule());
    //
    //		ScheduleTimeslotsReturnType timeslotsRet = service.generateTimeslotIterator(
    //				uc,
    //				config,
    //				getDate(2013, 1, 1, 0, 0, 0),
    //				getDate(2013, 2, 1, 0, 0, 0),
    //				false);
    //		assertEquals(timeslotsRet.getReturnCode(), success);
    //		log.debug(timeslotsRet.getScheduleTimeslots());
    //		assertEquals(timeslotsRet.getCount().intValue(), 6);
    //	}
    //
    //	@Test(enabled = false)
    //	public void testGetScheduleTimeslotCapacity_Defect_928() {
    //
    //		Long ret = service.reset(uc);
    //		assertEquals(ret, ReturnCode.Success.returnCode());
    //
    //		// Defect ID 928
    //		ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, new CodeType("ActivityCategory", ActivityCategory.ASSESSMENT.value()), 30L);
    //		ScheduleConfigVariableReturnType configRet = service.createScheduleConfigVars(uc, config);
    //		assertEquals(configRet.getReturnCode(), success);
    //		assertNotNull(configRet.getScheduleConfigVariable());
    //		config = configRet.getScheduleConfigVariable();
    //
    //		ScheduleType sch = new ScheduleType(null,
    //				new CodeType("ActivityCategory", ActivityCategory.ASSESSMENT.value()),
    //				new AssociationType(ScheduleType.AssociationToClass.FacilityService.value(), 3L),
    //				30L,
    //				getDateWithoutTime(2012, 5, 1),
    //				getDateWithoutTime(2015, 12, 31),
    //				"15:00",
    //				"17:00",
    //				null,
    //				null,
    //				null,
    //				null);
    //		assertTrue(sch.isValid());
    //
    //		ScheduleReturnType schRet = service.createSchedule(uc, sch);
    //		assertEquals(schRet.getReturnCode(), success);
    //		assertNotNull(schRet.getSchedule());
    //
    //		ScheduleTimeslotsReturnType timeslotsRet = service.generateTimeslotIterator(
    //				uc,
    //				config,
    //				getDate(2013, 1, 1, 0, 0, 0),
    //				getDate(2013, 2, 1, 0, 0, 0),
    //				false);
    //		assertEquals(timeslotsRet.getReturnCode(), success);
    //		log.debug(timeslotsRet.getScheduleTimeslots());
    //		assertEquals(timeslotsRet.getCount().intValue(), 1);
    //
    //		ScheduleTimeslotType timeslot = (ScheduleTimeslotType)timeslotsRet.getScheduleTimeslots().toArray()[0];
    //
    //		// ActivityType
    //		ActivityType activity = new ActivityType();
    //		assertFalse(activity.isWellFormed());
    //
    //		// Required - ActiveStatusFlag & ActivityCategory
    //		activity.setActiveStatusFlag(Boolean.TRUE);
    //		activity.setActivityCategory(new CodeType(ReferenceSets.ActivityCategory.value(), ActivityCategory.ASSESSMENT.value()));
    //		Set<AssociationType> facilities = new HashSet<AssociationType>();
    //		assertNotNull(uc.getFacilityId());
    //		AssociationType facility = new AssociationType(AssociationToClass.FACILITY.value(), uc.getFacilityId());
    //		facilities.add(facility);
    //		activity.setFacilities(facilities);
    //		assertTrue(activity.isWellFormed());
    //		assertTrue(activity.isValid());
    //
    //		// Planned end date is required if planned start date is populated
    //		Calendar cal = Calendar.getInstance();
    //		activity.setPlannedStartDate(cal.getTime());
    //		assertFalse(activity.isWellFormed());
    //		assertFalse(activity.isValid());
    //
    //		cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1);
    //		activity.setPlannedEndDate(cal.getTime());
    //		assertTrue(activity.isWellFormed());
    //		assertTrue(activity.isValid());
    //
    //		// scheduleID
    //		activity.setScheduleID(schRet.getSchedule().getScheduleIdentification());
    //		assertTrue(activity.isWellFormed());
    //		for (int i = 1; i <= config.getNumOfNextAvailableTimeslots().intValue(); i++) {
    //			ActivityReturnType actRet = service.create(uc, activity, timeslot, false);
    //			assertEquals(actRet.getReturnCode(), success);
    //
    //			TimeslotCapacityReturnType capacityRet = service.getTimeslotCapacity(uc, timeslot);
    //			assertEquals(capacityRet.getReturnCode(), success);
    //			assertNotNull(capacityRet.getTimeslotCapacity());
    //			assertEquals(capacityRet.getTimeslotCapacity().getCapacityTaken().intValue(), i);
    //			assertEquals(
    //					capacityRet.getTimeslotCapacity().getCapacityRemaining().intValue(),
    //					config.getNumOfNextAvailableTimeslots()
    //					- capacityRet.getTimeslotCapacity().getCapacityTaken().intValue());
    //		}
    //
    //		ActivityReturnType actRet = service.create(uc, activity, timeslot, false);
    //		assertEquals(actRet.getReturnCode(), ReturnCode.RInvalidInput1003.returnCode());
    //
    //		actRet = service.create(uc, activity, timeslot, true);
    //		assertEquals(actRet.getReturnCode(), success);
    //
    //		Set<Long> ids = service.getAll(uc);
    //		assertNotNull(ids);
    //		assertEquals(ids.size(), config.getNumOfNextAvailableTimeslots().intValue() + 1);
    //
    //		Long id = (Long)ids.toArray()[0];
    //		TimeslotCapacityReturnType capacityRet = service.getTimeslotCapacity(uc, timeslot);
    //		assertEquals(capacityRet.getReturnCode(), success);
    //		assertNotNull(capacityRet.getTimeslotCapacity());
    //		assertEquals(capacityRet.getTimeslotCapacity().getCapacityTaken().intValue(),
    //					config.getNumOfNextAvailableTimeslots().intValue() + 1);
    //		assertEquals(capacityRet.getTimeslotCapacity().getCapacityRemaining().intValue(), 0);
    //
    //		ActivityReturnType activityRet = service.delete(uc, id);
    //		assertEquals(activityRet.getReturnCode(), success);
    //
    //		capacityRet = service.getTimeslotCapacity(uc, timeslot);
    //		assertEquals(capacityRet.getReturnCode(), success);
    //		assertNotNull(capacityRet.getTimeslotCapacity());
    //		assertEquals(capacityRet.getTimeslotCapacity().getCapacityTaken().intValue(),
    //					config.getNumOfNextAvailableTimeslots().intValue());
    //		assertEquals(capacityRet.getTimeslotCapacity().getCapacityRemaining().intValue(), 0);
    //
    //		assertTrue(ids.remove(id));
    //
    //		List<Long> IDs = new ArrayList<Long>(ids);
    //		for (int i = 0; i < IDs.size(); i++) {
    //			capacityRet = service.getTimeslotCapacity(uc, timeslot);
    //			assertEquals(capacityRet.getReturnCode(), success);
    //			assertNotNull(capacityRet.getTimeslotCapacity());
    //			assertEquals(capacityRet.getTimeslotCapacity().getCapacityTaken().intValue(),
    //						config.getNumOfNextAvailableTimeslots().intValue()-i);
    //			assertEquals(
    //					capacityRet.getTimeslotCapacity().getCapacityRemaining().intValue(),
    //					config.getNumOfNextAvailableTimeslots()
    //					- capacityRet.getTimeslotCapacity().getCapacityTaken().intValue());
    //
    //			activityRet = service.delete(uc, IDs.get(i));
    //			assertEquals(activityRet.getReturnCode(), success);
    //
    //			capacityRet = service.getTimeslotCapacity(uc, timeslot);
    //			assertEquals(capacityRet.getReturnCode(), success);
    //			assertNotNull(capacityRet.getTimeslotCapacity());
    //			assertEquals(capacityRet.getTimeslotCapacity().getCapacityTaken().intValue(),
    //						config.getNumOfNextAvailableTimeslots().intValue()-i-1);
    //			assertEquals(
    //					capacityRet.getTimeslotCapacity().getCapacityRemaining().intValue(),
    //					config.getNumOfNextAvailableTimeslots()
    //					- capacityRet.getTimeslotCapacity().getCapacityTaken().intValue());
    //		}
    //
    //	}
    //
    //	@Test(enabled = false)
    //	public void testGetScheduleTimeslotCapacity_Defect_1018() {
    //
    //		Long ret = service.reset(uc);
    //		assertEquals(ret, ReturnCode.Success.returnCode());
    //
    //		// Defect ID 1018
    //		ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, new CodeType("ActivityCategory", ActivityCategory.MOVEMENT.value()), 30L);
    //		ScheduleConfigVariableReturnType configRet = service.createScheduleConfigVars(uc, config);
    //		assertEquals(configRet.getReturnCode(), success);
    //		assertNotNull(configRet.getScheduleConfigVariable());
    //		config = configRet.getScheduleConfigVariable();
    //
    //
    //		ScheduleType sch = new ScheduleType(null,
    //				new CodeType("ActivityCategory", ActivityCategory.MOVEMENT.value()),
    //				new AssociationType(ScheduleType.AssociationToClass.FacilityService.value(), 3L),
    //				30L,
    //				getDateWithoutTime(2012, 5, 1),
    //				getDateWithoutTime(2015, 12, 31),
    //				"15:00",
    //				"17:00",
    //				new DailyRecurrencePatternType(1L, false),
    //				null,
    //				null,
    //				null);
    //		assertTrue(sch.isValid());
    //
    //		ScheduleReturnType schRet = service.createSchedule(uc, sch);
    //		assertEquals(schRet.getReturnCode(), success);
    //		assertNotNull(schRet.getSchedule());
    //
    //		ScheduleTimeslotsReturnType timeslotsRet = service.generateTimeslotIterator(
    //				uc,
    //				config,
    //				getDate(2013, 1, 1, 0, 0, 0),
    //				getDate(2013, 2, 1, 0, 0, 0),
    //				false);
    //		assertEquals(timeslotsRet.getReturnCode(), success);
    //		assertEquals(timeslotsRet.getCount().intValue(), 31);
    //
    //		sch = new ScheduleType(null,
    //				new CodeType("ActivityCategory", ActivityCategory.MOVEMENT.value()),
    //				new AssociationType(ScheduleType.AssociationToClass.FacilityService.value(), 3L),
    //				30L,
    //				getDateWithoutTime(2012, 5, 1),
    //				getDateWithoutTime(2015, 12, 31),
    //				"15:00",
    //				"17:00",
    //				new WeeklyRecurrencePatternType(1L,
    //						new HashSet<CodeType>(
    //								Arrays.asList(new CodeType[] {
    //										new CodeType("Weekday", Weekday.MON.value()),
    //										new CodeType("Weekday", Weekday.WED.value())
    //								}))),
    //				null,
    //				null,
    //				null);
    //		assertTrue(sch.isValid());
    //
    //		schRet = service.createSchedule(uc, sch);
    //		assertEquals(schRet.getReturnCode(), success);
    //		assertNotNull(schRet.getSchedule());
    //
    //		timeslotsRet = service.generateTimeslotIterator(
    //				uc,
    //				config,
    //				getDate(2013, 1, 1, 0, 0, 0),
    //				getDate(2013, 2, 1, 0, 0, 0),
    //				false);
    //		assertEquals(timeslotsRet.getReturnCode(), success);
    //		assertEquals(timeslotsRet.getCount().intValue(), 40);
    //
    //		sch = new ScheduleType(null,
    //				new CodeType("ActivityCategory", ActivityCategory.MOVEMENT.value()),
    //				new AssociationType(ScheduleType.AssociationToClass.FacilityService.value(), 3L),
    //				30L,
    //				getDateWithoutTime(2012, 5, 1),
    //				getDateWithoutTime(2015, 12, 31),
    //				"15:00",
    //				"17:00",
    //				new MonthlyRecurrencePatternType(
    //						1L,
    //						2L,
    //						null,
    //						null),
    //				null,
    //				null,
    //				null);
    //		assertTrue(sch.isValid());
    //
    //		schRet = service.createSchedule(uc, sch);
    //		assertEquals(schRet.getReturnCode(), success);
    //		assertNotNull(schRet.getSchedule());
    //
    //		timeslotsRet = service.generateTimeslotIterator(
    //				uc,
    //				config,
    //				getDate(2013, 1, 1, 0, 0, 0),
    //				getDate(2013, 2, 1, 0, 0, 0),
    //				false);
    //		assertEquals(timeslotsRet.getReturnCode(), success);
    //		assertEquals(timeslotsRet.getCount().intValue(), 41);
    //
    //		sch = new ScheduleType(null,
    //				new CodeType("ActivityCategory", ActivityCategory.MOVEMENT.value()),
    //				new AssociationType(ScheduleType.AssociationToClass.FacilityService.value(), 3L),
    //				30L,
    //				getDateWithoutTime(2012, 5, 1),
    //				getDateWithoutTime(2015, 12, 31),
    //				"15:00",
    //				"17:00",
    //				new MonthlyRecurrencePatternType(
    //						1L,
    //						29L,
    //						null,
    //						null),
    //				null,
    //				null,
    //				null);
    //		assertTrue(sch.isValid());
    //
    //		schRet = service.createSchedule(uc, sch);
    //		assertEquals(schRet.getReturnCode(), success);
    //		assertNotNull(schRet.getSchedule());
    //
    //		timeslotsRet = service.generateTimeslotIterator(
    //				uc,
    //				config,
    //				getDate(2013, 1, 1, 0, 0, 0),
    //				getDate(2013, 2, 1, 0, 0, 0),
    //				false);
    //		assertEquals(timeslotsRet.getReturnCode(), success);
    //		assertEquals(timeslotsRet.getCount().intValue(), 42);
    //
    //		sch = new ScheduleType(null,
    //				new CodeType("ActivityCategory", ActivityCategory.MOVEMENT.value()),
    //				new AssociationType(ScheduleType.AssociationToClass.FacilityService.value(), 3L),
    //				30L,
    //				getDateWithoutTime(2012, 5, 1),
    //				getDateWithoutTime(2015, 12, 31),
    //				"15:00",
    //				"17:00",
    //				new YearlyRecurrencePatternType(
    //						1L,
    //						2L,
    //						new CodeType("Month", Month.DEC.value()),
    //						null,
    //						null),
    //				null,
    //				null,
    //				null);
    //		assertTrue(sch.isValid());
    //
    //		schRet = service.createSchedule(uc, sch);
    //		assertEquals(schRet.getReturnCode(), success);
    //		assertNotNull(schRet.getSchedule());
    //
    //		sch = new ScheduleType(null,
    //				new CodeType("ActivityCategory", ActivityCategory.MOVEMENT.value()),
    //				new AssociationType(ScheduleType.AssociationToClass.FacilityService.value(), 3L),
    //				30L,
    //				getDateWithoutTime(2012, 5, 1),
    //				getDateWithoutTime(2015, 12, 31),
    //				"15:00",
    //				"17:00",
    //				new YearlyRecurrencePatternType(
    //						1L,
    //						12L,
    //						new CodeType("Month", Month.DEC.value()),
    //						null,
    //						null),
    //				null,
    //				null,
    //				null);
    //		assertTrue(sch.isValid());
    //
    //		schRet = service.createSchedule(uc, sch);
    //		assertEquals(schRet.getReturnCode(), success);
    //		assertNotNull(schRet.getSchedule());
    //
    //
    //		timeslotsRet = service.generateTimeslotIterator(
    //				uc,
    //				config,
    //				getDate(2013, 1, 1, 0, 0, 0),
    //				getDate(2013, 2, 1, 0, 0, 0),
    //				false);
    //		assertEquals(timeslotsRet.getReturnCode(), success);
    //		assertEquals(timeslotsRet.getCount().intValue(), 42);
    //
    //	}
    //
    //	@Test(enabled = false)
    //	public void GenerateScheduleTimetable_Defect_1015() {
    //
    //		Long ret = service.reset(uc);
    //		assertEquals(ret, ReturnCode.Success.returnCode());
    //
    //		ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, new CodeType("ActivityCategory", ActivityCategory.SHIFTLOG.value()), 31L);
    //		ScheduleConfigVariableReturnType configRet = service.createScheduleConfigVars(uc, config);
    //		assertEquals(configRet.getReturnCode(), success);
    //		assertNotNull(configRet.getScheduleConfigVariable());
    //		config = configRet.getScheduleConfigVariable();
    //
    //		ScheduleType sch = new ScheduleType(null,
    //				new CodeType("ActivityCategory", ActivityCategory.SHIFTLOG.value()),
    //				new AssociationType(ScheduleType.AssociationToClass.FacilityService.value(), 3L),
    //				20L,
    //				getDateWithoutTime(2012, 05, 1),
    //				getDateWithoutTime(2015, 12, 31),
    //				"11:00",
    //				null,
    //				new DailyRecurrencePatternType(1L, false),
    //				null,
    //				null,
    //				null);
    //
    //		assertTrue(sch.isValid());
    //
    //		ScheduleReturnType schRet = service.createSchedule(uc, sch);
    //		assertEquals(schRet.getReturnCode(), success);
    //		assertNotNull(schRet.getSchedule());
    //		sch = schRet.getSchedule();
    //
    //		ScheduleTimeslotsReturnType timeslotsRet = service.generateTimeslotIterator(
    //				uc,
    //				config,
    //				getDate(2012, 1, 1, 0, 0, 0),
    //				getDate(2016, 12, 31, 0, 0, 0),
    //				false);
    //		assertEquals(timeslotsRet.getReturnCode(), success);
    //		// assertEquals(timeslotsRet.getCount().intValue(), 1339); // 365
    //
    //
    //	}

    private Date getEffectiveDate() {
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.HOUR_OF_DAY, randLong(23).intValue());
        cal.set(Calendar.MINUTE, randLong(59).intValue());
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, randLong(2012).intValue());

        return cal.getTime();
    }

    private Long randLong(int i) {
        return BigInteger.valueOf(new Random().nextInt(i)).abs().longValue();
    }

    private Long randLong(int from, int to) {
        Long rand = BigInteger.valueOf(new Random().nextInt(to)).abs().longValue();
        while (rand < from) {
            rand = BigInteger.valueOf(new Random().nextInt(to)).abs().longValue();
        }
        return rand;
    }

    private Date getDateWithoutTime(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.DAY_OF_MONTH, day);

        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    private Date getDateWithoutTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Date newDate = cal.getTime();
        return newDate;
    }

    private Date getDate(int year, int month, int day, int hour, int minute, int second) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.DAY_OF_MONTH, day);

        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, second);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }
}
