package syscon.arbutus.product.services.activity.contract.ejb;

import javax.naming.NamingException;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.contract.dto.ScheduleConfigVariableSearchType;
import syscon.arbutus.product.services.activity.contract.dto.ScheduleConfigVariableType;
import syscon.arbutus.product.services.activity.contract.dto.ScheduleConfigVariablesReturnType;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;

import static org.testng.Assert.*;

public class ActivityServiceScheduleConfigVariableIT extends BaseIT {

    final static Long success = 1L;
    private static Logger log = LoggerFactory.getLogger(ActivityServiceScheduleConfigVariableIT.class);
    final long n = 10;
    Set<ScheduleConfigVariableType> configs = new HashSet<ScheduleConfigVariableType>();
    private ActivityService service;

    @BeforeMethod
    public void beforeMethod() {

    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        uc = super.initUserContext();
    }

    @BeforeTest
    public void beforeTest() throws NamingException, Exception {

        BasicConfigurator.configure();

    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void reset() {
        Long ret = null;
        try {
            ret = service.deleteAll(uc);
            assert (ret == 1L);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    @Test(dependsOnMethods = { "reset" }, enabled = true)
    public void createAndGetScheduleConfigVars() {
        if (log.isDebugEnabled()) {
			log.debug("createAndGetScheduleConfigVars: start");
		}

        ScheduleConfigVariableType ret = null;

        try {
            ret = service.createScheduleConfigVars(uc, null);
        } catch (ArbutusRuntimeException e) {
            assertNull(ret);
        }
        ScheduleConfigVariableType config = new ScheduleConfigVariableType();

        try {
            service.createScheduleConfigVars(uc, config);
        } catch (ArbutusRuntimeException e) {
            assertNull(ret);
        }

        for (int i = 0; i < n; i++) {
            config = new ScheduleConfigVariableType(null, null, 100L);
            try {
                ret = service.createScheduleConfigVars(uc, config);
            } catch (ArbutusRuntimeException e) {
                log.info(e.getMessage());
            }

        }

        for (ActivityCategory c : ActivityCategory.values()) {

            config = new ScheduleConfigVariableType(null, c.toString(), 100L);
            ret = service.createScheduleConfigVars(uc, config);
            assertNotNull(ret);
            assertEquals(service.getScheduleConfigVar(uc, ret.getScheduleConfigIdentification()).getScheduleConfigIdentification(),
                    ret.getScheduleConfigIdentification());
            assertEquals(service.getScheduleConfigVar(uc, ret.getScheduleConfigIdentification()).getScheduleCategory(), ret.getScheduleCategory());
            assertEquals(service.getScheduleConfigVar(uc, ret.getScheduleConfigIdentification()).getNumOfNextAvailableTimeslots(), ret.getNumOfNextAvailableTimeslots());
            configs.add(ret);

        }

        if (log.isDebugEnabled()) {
			log.debug("createAndGetScheduleConfigVars: end");
		}
    }

    @Test(dependsOnMethods = { "createAndGetScheduleConfigVars" })
    public void searchScheduleConfigVars() {
        if (log.isDebugEnabled()) {
			log.debug("searchScheduleConfigVars: start");
		}
        ScheduleConfigVariablesReturnType ret = null;
        try {
            ret = service.searchScheduleConfigVers(uc, new ScheduleConfigVariableSearchType(null, null), null, null, null);
        } catch (ArbutusRuntimeException e) {
            log.info(e.getMessage());
        }
        for (ActivityCategory c : ActivityCategory.values()) {
            ret = service.searchScheduleConfigVers(uc, new ScheduleConfigVariableSearchType(c.value(), null), null, null, null);
            assertNotNull(ret);
            assertEquals(ret.getTotalSize().longValue(), 1L);
            for (ScheduleConfigVariableType conf : ret.getScheduleConfigVariables()) {
                assertEquals(conf.getScheduleConfigIdentification(),
                        service.getScheduleConfigVar(uc, conf.getScheduleConfigIdentification()).getScheduleConfigIdentification());
                assertEquals(conf.getScheduleCategory(), service.getScheduleConfigVar(uc, conf.getScheduleConfigIdentification()).getScheduleCategory());
                assertEquals(conf.getNumOfNextAvailableTimeslots(),
                        service.getScheduleConfigVar(uc, conf.getScheduleConfigIdentification()).getNumOfNextAvailableTimeslots());
            }

            ret = service.searchScheduleConfigVers(uc, new ScheduleConfigVariableSearchType(c.value() + new Random().nextInt(), null), null, null, null);
            assertNotNull(ret);
            assertEquals(ret.getScheduleConfigVariables().size(), 0);
            assertEquals(ret.getTotalSize().intValue(), 0);
        }

        for (ScheduleConfigVariableType c : configs) {

            if (c.getNumOfNextAvailableTimeslots() != null) {
                ret = service.searchScheduleConfigVers(uc, new ScheduleConfigVariableSearchType(null, c.getNumOfNextAvailableTimeslots()), null, null, null);
                assertNotNull(ret.getScheduleConfigVariables());
                assertTrue(ret.getTotalSize().intValue() > 0);
                ScheduleConfigVariableType conf = (ScheduleConfigVariableType) ret.getScheduleConfigVariables().toArray()[0];
                assertEquals(conf.getScheduleConfigIdentification(),
                        service.getScheduleConfigVar(uc, conf.getScheduleConfigIdentification()).getScheduleConfigIdentification());
                assertEquals(conf.getScheduleCategory(), service.getScheduleConfigVar(uc, conf.getScheduleConfigIdentification()).getScheduleCategory());
                assertEquals(conf.getNumOfNextAvailableTimeslots(),
                        service.getScheduleConfigVar(uc, conf.getScheduleConfigIdentification()).getNumOfNextAvailableTimeslots());
            }
        }

        if (log.isDebugEnabled()) {
			log.debug("searchScheduleConfigVars: end");
		}
    }

    @Test(dependsOnMethods = { "searchScheduleConfigVars" })
    public void updateScheduleConfigVars() {
        if (log.isDebugEnabled()) {
			log.debug("updateScheduleConfigVars: start");
		}

        for (ScheduleConfigVariableType c : configs) {
            // Category cannot be updated
            ScheduleConfigVariableType update = new ScheduleConfigVariableType(c);
            update.setScheduleCategory(c.getScheduleCategory());
            ScheduleConfigVariableType ret = service.updateScheduleConfigVars(uc, update);
            assertNotNull(ret);
            assertTrue(ret.getScheduleCategory().equals(update.getScheduleCategory()));
            assertEquals(ret.getScheduleCategory(), c.getScheduleCategory(), "The same as the original one");
            assertEquals(ret.getScheduleConfigIdentification(), c.getScheduleConfigIdentification(), "The same as the original one");
            assertEquals(ret.getNumOfNextAvailableTimeslots(), c.getNumOfNextAvailableTimeslots(), "The same as the original one");
            for (ActivityCategory cat : ActivityCategory.values()) {
                if (!c.getScheduleCategory().equals(cat.value())) {
                    update = new ScheduleConfigVariableType(c);
                    update.setScheduleCategory(cat.value());
                    ret = service.updateScheduleConfigVars(uc, update);
                    assertNotNull(ret);
                    assertTrue(!ret.getScheduleCategory().equals(update.getScheduleCategory()));
                    assertEquals(ret.getScheduleCategory(), c.getScheduleCategory(), "The same as the original one");
                    assertEquals(ret.getScheduleConfigIdentification(), c.getScheduleConfigIdentification(), "The same as the original one");
                    assertEquals(ret.getNumOfNextAvailableTimeslots(), c.getNumOfNextAvailableTimeslots(), "The same as the original one");
                }
            }

            // Id cannot be updated
            update = new ScheduleConfigVariableType(c);
            Long id = BigInteger.valueOf(new Random().nextLong()).abs().longValue();
            update.setScheduleConfigIdentification(id);
            try {
                ret = service.updateScheduleConfigVars(uc, update);
            } catch (ArbutusRuntimeException e) {
                log.info(e.getMessage());
            }

            // numOfNextAvailableTimeslots can be updated
            update = new ScheduleConfigVariableType(c);
            Long numOfSlots = BigInteger.valueOf(new Random().nextLong()).abs().longValue();
            update.setNumOfNextAvailableTimeslots(numOfSlots);
            ret = service.updateScheduleConfigVars(uc, update);
            assertTrue(ret.getNumOfNextAvailableTimeslots() != c.getNumOfNextAvailableTimeslots());
            assertEquals(ret.getNumOfNextAvailableTimeslots(), numOfSlots, "The same as the new one");
            assertEquals(ret.getScheduleConfigIdentification(), c.getScheduleConfigIdentification(), "The same as the original one");
            assertEquals(ret.getScheduleCategory(), c.getScheduleCategory(), "The same as the original one");
        }

        if (log.isDebugEnabled()) {
			log.debug("updateScheduleConfigVars: end");
		}
    }

    @Test(dependsOnMethods = { "updateScheduleConfigVars" })
    public void deleteAllScheduleConfigVar() {
        Long ret = service.deleteAllScheduleConfigVar(uc);
        assertEquals(ret, success);
    }

    public enum ActivityCategory {

        /**
         * Movement
         */
        MOVEMENT,
        /**
         * Housing and Bed Management
         */
        HBDMGMT,
        /**
         * Facility Count
         */
        FACOUNT,
        /**
         * Visitation
         */
        VISITATION,
        /**
         * Shift Log
         */
        SHIFTLOG,
        /**
         * Case Note
         */
        CASENOTE,
        /**
         * Assessment
         */
        ASSESSMENT,
        /**
         * Case
         */
        CASE,
        /**
         * Incident
         */
        INCIDENT,
        /**
         * Discipline
         */
        DISCIPLINE;

        public static ActivityCategory fromValue(String v) {
            return valueOf(v);
        }

        public String value() {
            return name();
        }

    }

}
