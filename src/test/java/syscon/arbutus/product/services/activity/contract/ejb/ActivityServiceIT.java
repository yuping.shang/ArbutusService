package syscon.arbutus.product.services.activity.contract.ejb;

import javax.naming.NamingException;
import java.math.BigInteger;
import java.util.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;
import syscon.arbutus.product.services.activity.contract.dto.*;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;

import static org.testng.Assert.*;

public class ActivityServiceIT extends BaseIT {

    final static Long success = 1L;
    private static Logger log = LoggerFactory.getLogger(ActivityServiceIT.class);
    Long ID;
    ActivityType ACTIVITY = new ActivityType();
    Set<ActivityType> activities = new HashSet<ActivityType>();
    Set<ActivityType> updateActivities = new HashSet<ActivityType>();
    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").serializeNulls().create();
    private Long facilityId;
    private Long supervisionId = null;
    private ActivityService service;

    @BeforeMethod
    public void beforeMethod() {

    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        uc = super.initUserContext();

        // Facility
        FacilityTest f = new FacilityTest();
        facilityId = f.createFacility();
    }

    @BeforeTest
    public void beforeTest() throws NamingException, Exception {

        BasicConfigurator.configure();

    }

    @AfterClass
    public void afterClass() {
        //		if (this.facilityId != null) {
        //			FacilityTest f = new FacilityTest();
        //			f.deleteFacility(facilityId);
        //		}
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    @Test
    public void testGetVersion() {
        String version = service.getVersion(null);
        assert ("2.0".equals(version));
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void reset() {
        //		Long ret = service.deleteAll(uc);
        //		assertNotNull(ret);
    }

    @Test(dependsOnMethods = { "reset" })
    public void testCreate() {
        activities = new HashSet<ActivityType>();

        for (ActivityCategory category : ActivityCategory.values()) {
            ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, category.value(), randLong(1, 10));
            ScheduleConfigVariableType confRet = service.createScheduleConfigVars(uc, config);
        }

        //		createSchedule();

        Schedule sch = new Schedule();
        sch.setScheduleCategory(ActivityCategory.MOVEMENT.value());
        sch.setFacilityId(facilityId);
        sch.setCapacity(10L);
        sch.setEffectiveDate(getEffectiveDate());
        sch.setScheduleStartTime("07:00");
        sch.setScheduleEndTime("17:00");

        Schedule schRet = service.createSchedule(uc, sch);

        // ActivityType
        ActivityType activityType = new ActivityType();

        // Required - ActiveStatusFlag & ActivityCategory
        activityType.setActiveStatusFlag(Boolean.TRUE);
        activityType.setActivityCategory("MOVEMENT");

        // Planned end date is required if planned start date is populated
        Calendar cal = Calendar.getInstance();
        activityType.setPlannedStartDate(cal.getTime());

        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1);
        activityType.setPlannedEndDate(cal.getTime());
        activityType.setSupervisionId(supervisionId);

        // scheduleID
        activityType.setScheduleID(schRet.getScheduleIdentification());

        // ScheduleTimeslotType
        ScheduleTimeslotType scheduleTimeslot = new ScheduleTimeslotType();
        scheduleTimeslot.setScheduleID(schRet.getScheduleIdentification());

        scheduleTimeslot.setTimeslotStartDateTime(Calendar.getInstance().getTime());

        ActivityType ret = service.create(uc, activityType, null, Boolean.TRUE);
        assertNotNull(ret);

        ID = ret.getActivityIdentification();

        assertEquals(ret.getActiveStatusFlag(), activityType.getActiveStatusFlag());
        assertEquals(ret.getActivityAntecedentId(), activityType.getActivityAntecedentId());
        assertEquals(ret.getActivityDescendantIds(), activityType.getActivityDescendantIds());
        //if (scheduleTimeslot != null) -> entity.setScheduleID(null);
        //assertEquals(ret.getActivity().getScheduleID(), activityType.getScheduleID());

        activities.add(ret);

        scheduleTimeslot.setTimeslotEndDateTime(Calendar.getInstance().getTime());

        ret = service.create(uc, activityType, scheduleTimeslot, Boolean.TRUE);
        assertNotNull(ret);

        //Facility-set is always empty.

        assertEquals(ret.getActiveStatusFlag(), Boolean.TRUE);
        assertEquals(ret.getActiveStatusFlag(), activityType.getActiveStatusFlag());
        assertEquals(ret.getActivityAntecedentId(), activityType.getActivityAntecedentId());
        assertEquals(ret.getActivityDescendantIds(), activityType.getActivityDescendantIds());
        assertEquals(ret.getScheduleID(), activityType.getScheduleID());

        activities.add(ret);

        // Test create(UserContext, Long scheduleId, Date lookupEndDate)
        Set<Schedule> schesRet = service.getAllSchedules(uc, true);
        assertTrue(schesRet.size() > 0);
        Schedule schedule = (Schedule) schesRet.toArray()[0];
        Set<ActivityType> actsRet = service.create(uc, schedule.getScheduleIdentification(), null);
        assertTrue(actsRet.size() > 0);
        for (ActivityType act : actsRet) {
            activities.add(act);
        }

        // create with Antecedent
        Set<Long> ids = service.getAll(uc);
        assertTrue(ids.size() > 0);
        Long antecedentId = ((Long) ids.toArray()[randLong(0, ids.size() - 1).intValue()]);

        ActivityType activity = new ActivityType();
        activity.setActiveStatusFlag(true);
        activity.setPlannedStartDate(getDate(2012, Calendar.JUNE, 1, 11, 0, 0));
        activity.setPlannedEndDate(getDate(2012, Calendar.DECEMBER, 31, 11, 30, 0));
        activity.setActivityCategory(ActivityCategory.MOVEMENT.value().toUpperCase());
        activity.setActivityAntecedentId(antecedentId);

        ret = service.create(uc, activity, null, false);
        assertNotNull(ret);
        assertEquals(ret.getActivityAntecedentId(), antecedentId);

        activities.add(ret);

        // Descendants
        ActivityType antecedentRet = service.get(uc, antecedentId);
        ActivityType antecedent = antecedentRet;
        Set<ActivityType> decRet = service.getDecendants(uc, antecedent);
        assertEquals(decRet.size(), 1);
        ActivityType decendantAct = (ActivityType) decRet.toArray()[0];

        Long decendantId = antecedentId;
        do {
            decendantId = ((Long) ids.toArray()[randLong(0, ids.size() - 1).intValue()]);
        } while (decendantId == antecedentId);
        Long decendant = decendantId;
        assertEquals(decendantAct.getActivityDescendantIds().size(), 0);
        decendantAct.getActivityDescendantIds().add(decendant);
        ret = service.update(uc, decendantAct, null, false);
        assertNotNull(ret);
        decRet = service.getDecendants(uc, antecedent);
        assertEquals(decRet.size(), 2);

    }

    @Test(dependsOnMethods = { "testCreate" })
    public void testGetAll() {
        Set<Long> ret = service.getAll(uc);
        assertTrue(ret.size() > 0);
        for (Long id : ret) {
            ActivityType activityRet = service.get(uc, id);
            assertEquals(activityRet.getActivityIdentification(), id);
        }
        assertEquals(ret.size(), service.getCount(uc).intValue());
    }

    @Test(dependsOnMethods = { "testGetAll" })
    public void testGet() {

        for (ActivityType act : activities) {
            Long id = act.getActivityIdentification();
            ActivityType ret = service.get(uc, id);
            assertNotNull(ret);

            if (act.getActivityAntecedentId() != null) {
                Set<ActivityType> antRet = service.getAntecedents(uc, act);
                assertNotNull(antRet);
            }

            if (act.getActivityDescendantIds() != null && act.getActivityDescendantIds().size() != 0 && act.getScheduleID() != null) {
                Set<ActivityType> decRet = service.getDecendants(uc, act);

                assertTrue(decRet.size() > 0);
            }
        }

        ActivityType act = (ActivityType) activities.toArray()[randLong(0, activities.size() - 1).intValue()];

        Set<ActivityType> rets = service.getDecendants(uc, act);
        assertNotNull(rets);

        Set<Long> ids = service.getAll(uc);
        assertNotNull(ids);
        assertTrue(ids.size() > 0);

        Long count = service.getCount(uc);
        assertNotNull(count);

        assertEquals(count.intValue(), ids.size());
    }

    @Test(dependsOnMethods = { "testCreate", "testGetAll", "testGet" }, enabled = true)
    public void testSearch() {
        ActivitySearchType search = new ActivitySearchType();
        search.setActivityCategory("NONEXIST");
        ActivitiesReturnType ret = service.search(uc, null, search, null, null, null);
        assertEquals(ret.getTotalSize().longValue(), 0L);
        assertEquals(ret.getActivities().size(), 0);

        search = new ActivitySearchType();
        int count = 0;
        for (ActivityType activity : activities) {
            if (activity.getActivityAntecedentId() != null) {
                search.setActivityAntecedentId(activity.getActivityAntecedentId());
                ret = service.search(uc, null, search, null, null, null);
                assertEquals(ret.getTotalSize().longValue(), 1L);
                assertEquals(ret.getActivities().size(), 1);
                count++;
            }
        }
        assertTrue(count > 0);

        //		//search by supervisionIdentifications.
        //		search = new ActivitySearchType();
        //		Set<Long> supIds = new HashSet<Long>();
        //		supIds.add(supervisionId);
        //		search.setSupervisionIds(supIds);
        //		ret = service.search(uc, null, search, null, null, null);
        //		assertEquals(ret.getTotalSize().longValue(), 2L);
    }

    //
    //	/** Confirmation Test Defect ID 1374:
    //	 *
    //	 *  This test is to prove that the DEFECT ID 1374 is not reproducable.
    //	 *
    //	 * Defect Reported:
    //	 * Activity2.0: Error code 1015 is returned when searching for records
    //	 *
    //	 * The issue exists in the latest svn# 14154.
    //		At the time the search is performed there are only 42 records in the databse, searchMode = "ONLY"
    //		Fails with the following search parameters:
    //
    //		ActivitySearchType [supervisionIdentifications=null, fromPlannedStartDate=null, toPlannedStartDate=Sun Dec 30 00:00:00 PST 2012, fromPlannedEndDate=null, toPlannedEndDate=null, activityAntecedentAssociation=null, activityDescendantAssociations=null, associations=null, activityCategory=null, scheduleID=null, activeStatusFlag=null]
    //
    //		ActivitySearchType [supervisionIdentifications=null, fromPlannedStartDate=null, toPlannedStartDate=null, fromPlannedEndDate=null, toPlannedEndDate=Thu Dec 27 00:00:00 PST 2012, activityAntecedentAssociation=null, activityDescendantAssociations=null, associations=null, activityCategory=null, scheduleID=null, activeStatusFlag=null]
    //
    //		ActivitySearchType [supervisionIdentifications=null, fromPlannedStartDate=Tue May 01 00:00:00 PDT 2012, toPlannedStartDate=null, fromPlannedEndDate=null, toPlannedEndDate=Mon Dec 31 00:00:00 PST 2012, activityAntecedentAssociation=null, activityDescendantAssociations=null, associations=null, activityCategory=null, scheduleID=null, activeStatusFlag=null]
    //
    //		ActivitySearchType [supervisionIdentifications=null, fromPlannedStartDate=null, toPlannedStartDate=null, fromPlannedEndDate=null, toPlannedEndDate=null, activityAntecedentAssociation=null, activityDescendantAssociations=null, associations=null, activityCategory=null, scheduleID=86, activeStatusFlag=null]
    //
    //		ActivitySearchType [supervisionIdentifications=null, fromPlannedStartDate=null, toPlannedStartDate=null, fromPlannedEndDate=null, toPlannedEndDate=null, activityAntecedentAssociation=null, activityDescendantAssociations=null, associations=null, activityCategory=null, scheduleID=null, activeStatusFlag=false]
    //	 *
    //	 */
    //	@Test(dependsOnMethods = { "testCreate", "testGetAll", "testGet", "testCreateAssociation", "testGetAssociations" }, enabled = true)
    //	public void testDefect1374() {
    //		ActivitySearchType search = new ActivitySearchType();
    //		ActivitiesReturnType ret;
    //
    //		String data;
    //
    //		// QA's test data: ActivitySearchType [supervisionIdentifications=null, fromPlannedStartDate=null, toPlannedStartDate=Sun Dec 30 00:00:00 PST 2012, fromPlannedEndDate=null, toPlannedEndDate=null, activityAntecedentAssociation=null, activityDescendantAssociations=null, associations=null, activityCategory=null, scheduleID=null, activeStatusFlag=null]
    //		search = new ActivitySearchType();
    //		Calendar c = Calendar.getInstance();
    //		c.set(Calendar.YEAR, 2012);
    //		c.set(Calendar.MONTH, 12);
    //		c.set(Calendar.DAY_OF_MONTH, 30);
    //		Date date = c.getTime();
    //		search.setToPlannedStartDate(date);
    //
    //		assertTrue(search.isWellFormed());
    //		assertTrue(search.isValid());
    //		ret = service.search(uc, null, search, SearchSetMode.ONLY.value(), null);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertNotEquals(ret.getReturnCode(), ReturnCode.RTooManyResults1015.returnCode(),
    //				"Proved: the DEFECT ID 1374 is not reproducable.");
    //
    //		data = "{'supervisionIdentifications':null, 'fromPlannedStartDate':null, 'toPlannedStartDate':'2012-12-30 00:00:00', 'fromPlannedEndDate':null, 'toPlannedEndDate':null, 'activityAntecedentAssociation':null, 'activityDescendantAssociations':null, 'associations':null, 'activityCategory':null, 'scheduleID':null, 'activeStatusFlag':null}";
    //		search = gson.fromJson(data, ActivitySearchType.class);
    //		assertTrue(search.isWellFormed());
    //		assertTrue(search.isValid());
    //		ret = service.search(uc, null, search, SearchSetMode.ONLY.value(), null);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertNotEquals(ret.getReturnCode(), ReturnCode.RTooManyResults1015.returnCode(),
    //				"Proved: the DEFECT ID 1374 is not reproducable.");
    //
    //		// QA's test data: ActivitySearchType [supervisionIdentifications=null, fromPlannedStartDate=null, toPlannedStartDate=null, fromPlannedEndDate=null, toPlannedEndDate=Thu Dec 27 00:00:00 PST 2012, activityAntecedentAssociation=null, activityDescendantAssociations=null, associations=null, activityCategory=null, scheduleID=null, activeStatusFlag=null]
    //		search = new ActivitySearchType();
    //		c.set(Calendar.YEAR, 2012);
    //		c.set(Calendar.MONTH, 12);
    //		c.set(Calendar.DAY_OF_MONTH, 27);
    //		date = c.getTime();
    //		search.setToPlannedEndDate(date);
    //
    //		assertTrue(search.isWellFormed());
    //		assertTrue(search.isValid());
    //		ret = service.search(uc, null, search, SearchSetMode.ONLY.value(), null);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertNotEquals(ret.getReturnCode(), ReturnCode.RTooManyResults1015.returnCode(),
    //				"Proved: the DEFECT ID 1374 is not reproducable.");
    //
    //		data = "{'supervisionIdentifications':null, 'fromPlannedStartDate':null, 'toPlannedStartDate':null, 'fromPlannedEndDate':null, 'toPlannedEndDate':'2012-12-27 00:00:00', 'activityAntecedentAssociation':null, 'activityDescendantAssociations':null, 'associations':null, 'activityCategory':null, 'scheduleID':null, 'activeStatusFlag':null}";
    //		search = gson.fromJson(data, ActivitySearchType.class);
    //		assertTrue(search.isWellFormed());
    //		assertTrue(search.isValid());
    //		ret = service.search(uc, null, search, SearchSetMode.ONLY.value(), null);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertNotEquals(ret.getReturnCode(), ReturnCode.RTooManyResults1015.returnCode(),
    //				"Proved: the DEFECT ID 1374 is not reproducable.");
    //
    //		// QA's test data: ActivitySearchType [supervisionIdentifications=null, fromPlannedStartDate=Tue May 01 00:00:00 PDT 2012, toPlannedStartDate=null, fromPlannedEndDate=null, toPlannedEndDate=Mon Dec 31 00:00:00 PST 2012, activityAntecedentAssociation=null, activityDescendantAssociations=null, associations=null, activityCategory=null, scheduleID=null, activeStatusFlag=null]
    //		search = new ActivitySearchType();
    //		c.set(Calendar.YEAR, 2012);
    //		c.set(Calendar.MONTH, 5);
    //		c.set(Calendar.DAY_OF_MONTH, 1);
    //		date = c.getTime();
    //		search.setFromPlannedStartDate(date);
    //		c.set(Calendar.YEAR, 2012);
    //		c.set(Calendar.MONTH, 12);
    //		c.set(Calendar.DAY_OF_MONTH, 31);
    //		date = c.getTime();
    //		search.setToPlannedEndDate(date);
    //
    //		assertTrue(search.isWellFormed());
    //		assertTrue(search.isValid());
    //		ret = service.search(uc, null, search, SearchSetMode.ONLY.value(), null);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertNotEquals(ret.getReturnCode(), ReturnCode.RTooManyResults1015.returnCode(),
    //				"Proved: the DEFECT ID 1374 is not reproducable.");
    //
    //
    //		data = "{'supervisionIdentifications':null, 'fromPlannedStartDate':null, 'toPlannedStartDate':'2012-05-01 00:00:00', 'fromPlannedEndDate':null, 'toPlannedEndDate':'2012-12-31 00:00:00', 'activityAntecedentAssociation':null, 'activityDescendantAssociations':null, 'associations':null, 'activityCategory':null, 'scheduleID':null, 'activeStatusFlag':null}";
    //		search = gson.fromJson(data, ActivitySearchType.class);
    //		assertTrue(search.isWellFormed());
    //		assertTrue(search.isValid());
    //		ret = service.search(uc, null, search, SearchSetMode.ONLY.value(), null);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertNotEquals(ret.getReturnCode(), ReturnCode.RTooManyResults1015.returnCode(),
    //				"Proved: the DEFECT ID 1374 is not reproducable.");
    //
    //		// QA's test data: ActivitySearchType [supervisionIdentifications=null, fromPlannedStartDate=null, toPlannedStartDate=null, fromPlannedEndDate=null, toPlannedEndDate=null, activityAntecedentAssociation=null, activityDescendantAssociations=null, associations=null, activityCategory=null, scheduleID=86, activeStatusFlag=null]
    //		search = new ActivitySearchType();
    //		SchedulesReturnType schRet = service.getAllSchedules(uc, true);
    //		assertEquals(schRet.getReturnCode(), success);
    //		assertTrue(schRet.getSchedules().size() > 0);
    //		ScheduleType sch = (ScheduleType)(schRet.getSchedules().toArray()[0]);
    //		search.setScheduleID(sch.getScheduleIdentification());
    //		assertTrue(search.isWellFormed());
    //		assertTrue(search.isValid());
    //		ret = service.search(uc, null, search, SearchSetMode.ONLY.value(), null);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertNotEquals(ret.getReturnCode(), ReturnCode.RTooManyResults1015.returnCode(),
    //				"Proved: the DEFECT ID 1374 is not reproducable.");
    //
    //		data = "{'supervisionIdentifications':null, 'fromPlannedStartDate':null, 'toPlannedStartDate':null, 'fromPlannedEndDate':null, 'toPlannedEndDate':null, 'activityAntecedentAssociation':null, 'activityDescendantAssociations':null, 'associations':null, 'activityCategory':null, 'scheduleID':86, 'activeStatusFlag':null}";
    //		search = gson.fromJson(data, ActivitySearchType.class);
    //		assertTrue(search.isWellFormed());
    //		assertTrue(search.isValid());
    //		ret = service.search(uc, null, search, SearchSetMode.ONLY.value(), null);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertNotEquals(ret.getReturnCode(), ReturnCode.RTooManyResults1015.returnCode(),
    //				"Proved: the DEFECT ID 1374 is not reproducable.");
    //
    //		// QA's test data: ActivitySearchType [supervisionIdentifications=null, fromPlannedStartDate=null, toPlannedStartDate=null, fromPlannedEndDate=null, toPlannedEndDate=null, activityAntecedentAssociation=null, activityDescendantAssociations=null, associations=null, activityCategory=null, scheduleID=null, activeStatusFlag=false]
    //		search = new ActivitySearchType();
    //		search.setActiveStatusFlag(true);
    //		assertTrue(search.isWellFormed());
    //		assertTrue(search.isValid());
    //		ret = service.search(uc, null, search, SearchSetMode.ONLY.value(), null);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertNotEquals(ret.getReturnCode(), ReturnCode.RTooManyResults1015.returnCode(),
    //				"Proved: the DEFECT ID 1374 is not reproducable.");
    //
    //		data = "{'supervisionIdentifications':null, 'fromPlannedStartDate':null, 'toPlannedStartDate':null, 'fromPlannedEndDate':null, 'toPlannedEndDate':null, 'activityAntecedentAssociation':null, 'activityDescendantAssociations':null, 'associations':null, 'activityCategory':null, 'scheduleID':null, 'activeStatusFlag':false}";
    //		search = gson.fromJson(data, ActivitySearchType.class);
    //		assertTrue(search.isWellFormed());
    //		assertTrue(search.isValid());
    //		ret = service.search(uc, null, search, SearchSetMode.ONLY.value(), null);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertNotEquals(ret.getReturnCode(), ReturnCode.RTooManyResults1015.returnCode(),
    //				"Proved: the DEFECT ID 1374 is not reproducable.");
    //	}
    //
    //
    @Test(dependsOnMethods = { "testCreate", "testGetAll", "testGet", "testSearch" }, enabled = true)
    public void testUpdate() {

        for (ActivityType act : activities) {

            // ActiveStatusFlag
            ActivityType update = new ActivityType(act);
            ActivityType ret;

            update.setActiveStatusFlag(act.getActiveStatusFlag() == true ? false : true);
            update.setActivityIdentification(act.getActivityIdentification());
            ret = service.update(uc, update, null, true);
            assertNotNull(ret);
            assertEquals(ret.getActiveStatusFlag().booleanValue(), !act.getActiveStatusFlag().booleanValue());
            updateActivities.add(ret);

            // ActivityAntecedentAssociation
            Long antecedentId = ((ActivityType) activities.toArray()[randLong(0, activities.size() - 1).intValue()]).getActivityIdentification();
            while (antecedentId == update.getActivityIdentification()) {
                antecedentId = ((ActivityType) activities.toArray()[randLong(0, activities.size() - 1).intValue()]).getActivityIdentification();
            }
            update.setActivityAntecedentId(antecedentId);
            update.setActivityIdentification(act.getActivityIdentification());
            ret = service.update(uc, update, null, true);
            assertNotNull(ret);
            assertEquals(ret.getActivityAntecedentId(), antecedentId);
            updateActivities.add(ret);

            // ActivityDescendantAssociations
            Long descendantId = ((ActivityType) activities.toArray()[randLong(0, activities.size() - 1).intValue()]).getActivityIdentification();
            while (descendantId == update.getActivityIdentification() || descendantId == antecedentId) {
                descendantId = ((ActivityType) activities.toArray()[randLong(0, activities.size() - 1).intValue()]).getActivityIdentification();
            }
            Set<Long> activityDescendantIds = new HashSet<Long>();
            if (service.get(uc, descendantId).getActivityAntecedentId() == null) {
                assertFalse(update.getActivityDescendantIds().contains(descendantId));
                activityDescendantIds.add(descendantId);
                update.setActivityDescendantIds(activityDescendantIds);
                ret = service.update(uc, update, null, true);
                assertNotNull(ret);
                assertTrue(ret.getActivityDescendantIds().contains(descendantId));
                assertTrue(update.getActivityDescendantIds().contains(descendantId));
                updateActivities.add(ret);
            }

            // Supervision
            Set<Long> ids = service.getAll(uc);
            for (Long actId : ids) {
                ActivityType a = service.get(uc, actId);
                if (a.getSupervisionId() == null) {
                    a.setSupervisionId(supervisionId);
                    ActivityType activity = service.update(uc, a, null, true);
                    assertEquals(activity.getSupervisionId(), supervisionId);
                }
            }

            // ScheduleConfigVariableType & ScheduleType
            //			ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, "MOVEMENT", randLong(1, 10));
            //			service.createScheduleConfigVars(uc, config);

            Schedule sch = new Schedule();
            sch.setScheduleCategory("MOVEMENT");
            sch.setCapacity((long) new Random().nextInt(100));
            sch.setEffectiveDate(getEffectiveDate());
            sch.setScheduleStartTime("06:00");
            sch.setScheduleEndTime("16:00");
            sch.setFacilityId(facilityId);

            Schedule schRet = service.createSchedule(uc, sch);
            assertNotNull(schRet);
            assertEquals(schRet.getActiveScheduleFlag(), sch.getActiveScheduleFlag());
            assertEquals(schRet.getScheduleEndTime(), sch.getScheduleEndTime());
            assertEquals(schRet.getScheduleStartTime(), sch.getScheduleStartTime());
            assertEquals(schRet.getEffectiveDate(), this.getDateWithoutTime(sch.getEffectiveDate()));

            // ScheduleTimeslotType
            ScheduleTimeslotType scheduleTimeslot = new ScheduleTimeslotType();
            scheduleTimeslot.setScheduleID(schRet.getScheduleIdentification());
            scheduleTimeslot.setTimeslotStartDateTime(Calendar.getInstance().getTime());
            scheduleTimeslot.setTimeslotEndDateTime(Calendar.getInstance().getTime());

            if (update.getPlannedEndDate() == null) {
                update.setPlannedEndDate(update.getPlannedStartDate());
            }

            update.setActivityIdentification(act.getActivityIdentification());
            assertNotNull(act.getActivityIdentification());

            Long remaining = service.getTimeslotCapacity(uc, scheduleTimeslot).getCapacityRemaining();
            if (remaining > 0) {
                ret = service.update(uc, update, scheduleTimeslot, false);
                assertNotNull(ret);
                assertEquals(ret.getActivityAntecedentId(), update.getActivityAntecedentId());
                updateActivities.add(ret);
            }
        }
    }

    @Test(dependsOnMethods = { "testUpdate" }, enabled = true)
    public void testCancel() {

        /**
         * <li>All activities are canceled if they meet the following criteria,
         *  <ol>The planned start dates of the activity is after the CancellationDate;</ol>
         *  <ol>Activity’s ScheduleID = ScheduleIdentification;</ol>
         * <li>Status of a canceled activity is set to inactive;</li>
         * <li>If DeleteActivityFlag = True, canceled activity will be deleted. The activity augmentation referenced by the activity must be deleted as well (out of scope of this service).</li>
         */

        for (ActivityType act : updateActivities) {
            if (act.getScheduleID() != null && act.getActiveStatusFlag() == true) {
                Date plannedStartDate = act.getPlannedStartDate();
                Calendar cal = Calendar.getInstance();
                cal.setTime(plannedStartDate);
                cal.add(Calendar.YEAR, -1);
                Date cancellationDate = cal.getTime();
                Set<ActivityType> ret = service.cancel(uc, act.getScheduleID(), cancellationDate, false);

                assertNotNull(ret);
                assertEquals(ret.size(), 1);
                for (ActivityType actRet : ret) {
                    assertEquals(actRet.getActiveStatusFlag(), Boolean.FALSE);
                    assertEquals(actRet.getScheduleID(), act.getScheduleID());
                }
            }

            /**
             * <li>All activities are canceled if they meet the following criteria,
             *  <ol>The activity’s planned start date = timeslot’s TimeslotStartDateTime;</ol>
             *  <ol>The activity’s planned end date = timeslot’s TimeslotEndDateTime;</ol>
             *  <ol>Activity’s ScheduleID = timeslot’s ScheduleID;</ol>
             * </li>
             * <li>Status of a canceled activity is set to inactive;</li>
             * <li>If DeleteActivityFlag = True, canceled activity will be deleted. The activity augmentation referenced by the activity must be deleted as well (out of scope of this service).</li>
             */
            else if (act.getScheduleID() != null && act.getActiveStatusFlag() == true && act.getPlannedEndDate() != null) {

                ScheduleTimeslotType timeslot = new ScheduleTimeslotType(act.getPlannedStartDate(), act.getPlannedEndDate(), act.getScheduleID());
                Set<ActivityType> ret = service.cancel(uc, timeslot, false);

                assertNotNull(ret);
                assertEquals(ret.size(), 1);
                for (ActivityType actRet : ret) {
                    assertEquals(actRet.getActiveStatusFlag(), Boolean.FALSE);
                    assertEquals(actRet.getScheduleID(), act.getScheduleID());
                }
            }
        }
    }

    @Test(dependsOnMethods = { "testCancel" }, enabled = true)
    public void testDelete() {

        Set<Long> retAll = service.getAll(uc);
        assertEquals(retAll.size(), activities.size());
        for (Long id : retAll) {
            ActivityType act = service.get(uc, id);
            if (act.getActivityDescendantIds().size() > 0) {
                try {
                    Long ret = service.delete(uc, id);
                    assertNull(ret);
                } catch (ArbutusRuntimeException e) {
                    log.info(e.getMessage());
                }
            } else {
                Long ret = service.delete(uc, id);
                assertEquals(ret, success);
            }
        }

        service.deleteAllScheduleConfigVar(uc);
        service.deleteAllSchedule(uc);
        service.deleteAll(uc);
    }

    private Date getEffectiveDate() {
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.HOUR_OF_DAY, randLong(23).intValue());
        cal.set(Calendar.MINUTE, randLong(59).intValue());
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, randLong(2012).intValue());

        return cal.getTime();
    }

    private Long randLong(int i) {
        return BigInteger.valueOf(new Random().nextInt(i)).abs().longValue();
    }

    private Long randLong(int from, int to) {
        Long rand = BigInteger.valueOf(new Random().nextInt(to)).abs().longValue();
        while (rand < from) {
            rand = BigInteger.valueOf(new Random().nextInt(to)).abs().longValue();
        }
        return rand;
    }

    private Date getDateWithoutTime(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.DAY_OF_MONTH, day);

        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    private Date getDateWithoutTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Date newDate = cal.getTime();
        return newDate;
    }

    private Date getDate(int year, int month, int day, int hour, int minute, int second) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.DAY_OF_MONTH, day);

        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, second);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }
}
