package syscon.arbutus.product.services.activity.contract.ejb;

import javax.naming.NamingException;
import java.math.BigInteger;
import java.util.*;

import org.apache.log4j.BasicConfigurator;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;
import syscon.arbutus.product.services.activity.contract.dto.*;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.common.OrganizationTest;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;

import static org.testng.Assert.*;

public class ActivityServiceScheduleIT extends BaseIT {
    final static Long success = 1L;
    private static Logger log = LoggerFactory.getLogger(ActivityServiceScheduleIT.class);
    final long n = 1;
    Set<Schedule> SCHEDULES = new HashSet<Schedule>();
    private Long facilityId;
    private Long facilityId2;
    private Long forInternalLocationId = null;
    private Set<Long> organizationIds;
    private Set<Long> organizationIds2;
    private Set<Long> organizationIds3;
    private ActivityService service;

    @BeforeMethod
    public void beforeMethod() {

    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        uc = super.initUserContext();

        BasicConfigurator.configure();

        // Facility
        FacilityTest f = new FacilityTest();
        facilityId = f.createFacility();
        facilityId2 = f.createFacility();

        // Organization
        OrganizationTest org = new OrganizationTest();
        organizationIds = new HashSet<>();
        organizationIds.add(org.createOrganization());
        organizationIds.add(org.createOrganization());

        organizationIds2 = new HashSet<>();
        organizationIds2.add(org.createOrganization());
        organizationIds2.add(org.createOrganization());

        organizationIds3 = new HashSet<>();
        organizationIds3.add(org.createOrganization());
        organizationIds3.add(org.createOrganization());
    }

    @BeforeTest
    public void beforeTest() throws NamingException, Exception {
    }

    @AfterClass
    public void afterClass() {
        //		OrganizationTest org = new OrganizationTest();
        //		if (organizationIds != null) {
        //			for (Long id: organizationIds) {
        //				org.deleteOrganization(id);O
        //			}
        //		}
        //		if (organizationIds2 != null) {
        //			for (Long id: organizationIds2) {
        //				org.deleteOrganization(id);
        //			}
        //		}
        //		if (organizationIds3 != null) {
        //			for (Long id: organizationIds3) {
        //				org.deleteOrganization(id);
        //			}
        //		}
        //
        //		if (this.facilityId != null) {
        //			FacilityTest f = new FacilityTest();
        //			f.deleteFacility(facilityId);
        //			f.deleteFacility(facilityId2);
        //		}
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void reset() {

        service.deleteAllScheduleConfigVar(uc);
        service.deleteAllSchedule(uc);
        service.deleteAll(uc);

        Long ret = null;
        try {
            ret = service.deleteAll(uc);
            assert (ret == 1L);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    @Test(dependsOnMethods = { "reset" })
    public void testCreateAndGet() {
        if (log.isDebugEnabled()) {
			log.debug("testCreateAndGet: start");
		}

        Schedule retNull = null;
        try {
            retNull = service.createSchedule(uc, null);
        } catch (ArbutusRuntimeException e) {
            assertNull(retNull);
        }
        try {
            assertNull(service.createSchedule(uc, new Schedule()));
        } catch (ArbutusRuntimeException e) {
            assertNull(retNull);
        }

        Schedule qaData = new Schedule();
        qaData.setScheduleIdentification(444L);
        qaData.setScheduleCategory(ActivityCategory.MOVEMENT.value());
        qaData.setFacilityId(facilityId);
        qaData.setOrganizationIds(this.organizationIds);
        Calendar cal = Calendar.getInstance();
        qaData.setEffectiveDate(Calendar.getInstance().getTime());
        qaData.setScheduleStartTime("10:00");
        cal.add(Calendar.YEAR, 10);
        cal.set(Calendar.MONTH, 12);
        qaData.setTerminationDate(cal.getTime());
        qaData.setScheduleEndTime("17:30");
        DailyWeeklyCustomRecurrencePatternType pattern = new DailyWeeklyCustomRecurrencePatternType(1L, Boolean.TRUE, null);
        qaData.setRecurrencePattern(pattern);

        Schedule qaRet = service.createSchedule(uc, qaData);
        assertNotNull(qaRet);
        for (ActivityCategory category : ActivityCategory.values()) {

            ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, category.value(), randLong(10, 20));
            service.createScheduleConfigVars(uc, config);

            for (int i = 0; i < n; i++) {

                // Null Recurrence Pattern, Forinternallocation reference and FacilityScheduleReason.
                Schedule sch = new Schedule();

                sch.setScheduleCategory(category.value());
                sch.setFacilityId(facilityId);
                // sch.setForInternalLocationId(forInternalLocationId);
                sch.setEffectiveDate(getEffectiveDate());
                sch.setScheduleStartTime("08:00");
                sch.setScheduleEndTime("16:00");
                sch.setOrganizationIds(organizationIds);

                Schedule ret = service.createSchedule(uc, sch);
                assertNotNull(ret);

                SCHEDULES.add(ret);

                // Forinternallocation reference and FacilityScheduleReason have values.
                sch = new Schedule();

                sch.setScheduleCategory(category.value());
                sch.setFacilityId(facilityId);
                // sch.setForInternalLocationId(forInternalLocationId);
                sch.setEffectiveDate(getEffectiveDate());
                sch.setScheduleStartTime("08:00");
                sch.setScheduleEndTime("16:00");
                sch.setOrganizationIds(organizationIds);

                sch.setFacilityScheduleReason("SEN");

                ret = service.createSchedule(uc, sch);
                assertNotNull(ret);

                SCHEDULES.add(ret);

                // FacilityScheduleReason has wrong code value.
                sch = new Schedule();

                sch.setScheduleCategory(category.value());
                sch.setFacilityId(facilityId);
                // sch.setForInternalLocationId(forInternalLocationId);
                sch.setEffectiveDate(getEffectiveDate());
                sch.setScheduleStartTime("08:00");
                sch.setScheduleEndTime("16:00");
                sch.setOrganizationIds(organizationIds);

                sch.setFacilityScheduleReason("BAIL");
                try {
                    ret = service.createSchedule(uc, sch);
                    SCHEDULES.add(ret);
                } catch (ArbutusRuntimeException e) {
                    log.info(e.getMessage());
                }

                // Association can be made to only one of the location service,
                // organization service or internal location service at a time.

                // AtInternalLocation Associations have NO same value as ForInternalLocation Association. Positive.
                sch = new Schedule();

                sch.setScheduleCategory(category.value());
                sch.setFacilityId(facilityId2);
                // sch.setForInternalLocationId(forInternalLocationId);
                sch.setEffectiveDate(Calendar.getInstance().getTime());
                sch.setScheduleStartTime("08:00");
                sch.setScheduleEndTime("16:00");
                sch.setAtInternalLocationIds(organizationIds2);

                ret = service.createSchedule(uc, sch);
                assertNotNull(ret);

                SCHEDULES.add(ret);

                // Once Recurrence Pattern
                Date effectiveDate = getEffectiveDate();
                Date expirationDate = null;
                sch = new Schedule();

                sch.setScheduleCategory(category.value());
                sch.setFacilityId(facilityId);
                // sch.setForInternalLocationId(forInternalLocationId);
                sch.setEffectiveDate(effectiveDate);
                sch.setTerminationDate(expirationDate);
                sch.setScheduleStartTime("08:00");
                sch.setScheduleEndTime("16:00");
                sch.setRecurrencePattern(new OnceRecurrencePatternType());

                try {
                    ret = service.createSchedule(uc, sch);
                    SCHEDULES.add(ret);
                } catch (ArbutusRuntimeException e) {
                    log.info(e.getMessage());
                }

                // Once Recurrence Pattern has mandatory OccurrenceDate value.
                sch.setRecurrencePattern(new OnceRecurrencePatternType(new Date()));
                ret = service.createSchedule(uc, sch);
                assertNotNull(ret);
                SCHEDULES.add(ret);

                assertNotNull(service.getSchedule(uc, ret.getScheduleIdentification()));

                // Daily Recurrence Pattern
                effectiveDate = getEffectiveDate();
                expirationDate = getExpirationDate();
                sch = new Schedule();

                sch.setScheduleCategory(category.value());
                sch.setFacilityId(facilityId);
                // sch.setForInternalLocationId(forInternalLocationId);
                sch.setEffectiveDate(effectiveDate);
                sch.setTerminationDate(expirationDate);
                sch.setScheduleStartTime("08:00");
                sch.setScheduleEndTime("16:00");
                sch.setRecurrencePattern(new DailyWeeklyCustomRecurrencePatternType(1L, true, null));

                ret = service.createSchedule(uc, sch);
                assertNotNull(ret);
                SCHEDULES.add(ret);

                // Recur On Days -- Fri, Sat, Sun
                DailyWeeklyCustomRecurrencePatternType recurrencePattern = new DailyWeeklyCustomRecurrencePatternType(1L, true,
                        new HashSet<String>(Arrays.asList(new String[] { "FRI", "SAT", "SUN" })));
                LocalDate now = new LocalDate();
                Set<ExDateType> includeDates = new HashSet<>();
                includeDates.add(new ExDateType(now, new LocalTime(8, 10), new LocalTime(10, 20)));
                recurrencePattern.setIncludeDateTimes(includeDates);
                sch.setRecurrencePattern(recurrencePattern);

                Set<ExDateType> excludeDates = new HashSet<>();
                excludeDates.add(new ExDateType(now, new LocalTime(8, 10), new LocalTime(10, 20)));
                recurrencePattern.setExcludeDateTimes(excludeDates);
                sch.setRecurrencePattern(recurrencePattern);

                ret = service.createSchedule(uc, sch);
                assertNotNull(ret);
                RecurrencePatternType recurPattern = ret.getRecurrencePattern();
                assertNotNull(recurPattern);
                DailyWeeklyCustomRecurrencePatternType dailyRecurrencePattern = (DailyWeeklyCustomRecurrencePatternType) recurPattern;
                assertTrue(dailyRecurrencePattern.getRecurOnDays().contains("FRI"));
                assertTrue(dailyRecurrencePattern.getRecurOnDays().contains("SAT"));
                assertTrue(dailyRecurrencePattern.getRecurOnDays().contains("SUN"));
                assertFalse(dailyRecurrencePattern.getRecurOnDays().contains("MON"));
                assertTrue(recurPattern.getIncludeDateTimes().contains(new ExDateType(now, new LocalTime(8, 10), new LocalTime(10, 20))));
                assertTrue(recurPattern.getExcludeDateTimes().contains(new ExDateType(now, new LocalTime(8, 10), new LocalTime(10, 20))));
                SCHEDULES.add(ret);

                // Weekly Recurrence Pattern
                effectiveDate = getEffectiveDate();
                expirationDate = getExpirationDate();

                sch = new Schedule();

                sch.setScheduleCategory(category.value());
                sch.setFacilityId(facilityId);
                // sch.setForInternalLocationId(forInternalLocationId);
                sch.setEffectiveDate(effectiveDate);
                sch.setTerminationDate(expirationDate);
                sch.setScheduleStartTime("08:00");
                sch.setScheduleEndTime("16:00");
                sch.setRecurrencePattern(new WeeklyRecurrencePatternType(1L, new HashSet<String>(Arrays.asList(new String[] { "MON", "FRI" }))));
                ret = service.createSchedule(uc, sch);
                assertNotNull(ret);
                SCHEDULES.add(ret);

                // Monthly Recurrence Pattern
                effectiveDate = getEffectiveDate();
                expirationDate = getExpirationDate();
                sch = new Schedule();

                sch.setScheduleCategory(category.value());
                sch.setFacilityId(facilityId);
                // sch.setForInternalLocationId(forInternalLocationId);
                sch.setEffectiveDate(effectiveDate);
                sch.setTerminationDate(expirationDate);
                sch.setScheduleStartTime("08:00");
                sch.setScheduleEndTime("16:00");
                sch.setRecurrencePattern(new MonthlyRecurrencePatternType(1L, 2L, null, null));

                ret = service.createSchedule(uc, sch);
                assertNotNull(ret);
                SCHEDULES.add(ret);

                effectiveDate = getEffectiveDate();
                expirationDate = getExpirationDate();
                sch = new Schedule();

                sch.setScheduleCategory(category.value());
                sch.setFacilityId(facilityId);
                // sch.setForInternalLocationId(forInternalLocationId);
                sch.setEffectiveDate(effectiveDate);
                sch.setTerminationDate(expirationDate);
                sch.setScheduleStartTime("08:00");
                sch.setScheduleEndTime("16:00");
                sch.setRecurrencePattern(new MonthlyRecurrencePatternType(1L, null, "MON", "FIRST"));

                ret = service.createSchedule(uc, sch);
                assertNotNull(ret);
                SCHEDULES.add(ret);

                // Yearly Recurrence Pattern
                effectiveDate = getEffectiveDate();
                expirationDate = getExpirationDate();
                sch = new Schedule();

                sch.setScheduleCategory(category.value());
                sch.setFacilityId(facilityId);
                // sch.setForInternalLocationId(forInternalLocationId);
                sch.setEffectiveDate(effectiveDate);
                sch.setTerminationDate(expirationDate);
                sch.setScheduleStartTime("08:00");
                sch.setScheduleEndTime("16:00");
                sch.setRecurrencePattern(new YearlyRecurrencePatternType(1L, 2L, "DEC", null, null));

                ret = service.createSchedule(uc, sch);
                assertNotNull(ret);
                SCHEDULES.add(ret);

                effectiveDate = getEffectiveDate();
                expirationDate = getExpirationDate();
                sch = new Schedule();

                sch.setScheduleCategory(category.value());
                sch.setFacilityId(facilityId);
                // sch.setForInternalLocationId(forInternalLocationId);
                sch.setEffectiveDate(effectiveDate);
                sch.setTerminationDate(expirationDate);
                sch.setScheduleStartTime("08:00");
                sch.setScheduleEndTime("16:00");
                sch.setRecurrencePattern(new YearlyRecurrencePatternType(1L, null, "DEC", "MON", "FIRST"));

                ret = service.createSchedule(uc, sch);
                assertNotNull(ret);
                SCHEDULES.add(ret);
            }
        }
    }

    @Test(dependsOnMethods = { "testCreateAndGet" }, enabled = true)
    public void testGetAll() {
        Set<Schedule> ret = service.getAllSchedules(uc, Boolean.TRUE);
        assertNotNull(ret);
        assertTrue(ret.size() > 0);
    }

    @Test(dependsOnMethods = { "testCreateAndGet" }, enabled = true)
    public void testSearch() {
        ScheduleSearchType search = new ScheduleSearchType();

        // Daily Recurrence Pattern
        search = new ScheduleSearchType();
        search.setRecurrencePattern(new DailyWeeklyCustomRecurrencePatternType(1L, true, null));
        SchedulesReturnType ret = service.searchSchedule(uc, null, search, null, null, null);
        assertNotNull(ret);
        assertTrue(ret.getTotalSize().intValue() > 0);

        search = new ScheduleSearchType();
        search.setRecurrencePattern(new DailyWeeklyCustomRecurrencePatternType(1L, true, new HashSet<String>(Arrays.asList(new String[] { "FRI", "SUN" }))));
        ret = service.searchSchedule(uc, null, search, null, null, null);
        assertNotNull(ret);
        assertTrue(ret.getTotalSize().intValue() > 0);

        // Weekly Recurrence Pattern
        search = new ScheduleSearchType();
        search.setRecurrencePattern(new WeeklyRecurrencePatternType(1L, new HashSet<String>(Arrays.asList(new String[] { "MON", "FRI" }))));
        ret = service.searchSchedule(uc, null, search, null, null, null);
        assertNotNull(ret);
        assertTrue(ret.getTotalSize().intValue() > 0);

        // Monthly Recurrence Pattern
        search = new ScheduleSearchType();
        search.setRecurrencePattern(new MonthlyRecurrencePatternType(1L, 2L, null, null));
        ret = service.searchSchedule(uc, null, search, null, null, null);
        assertNotNull(ret.getSchedules());
        assertTrue(ret.getTotalSize().intValue() > 0);

        // Yearly Recurrence Pattern
        search = new ScheduleSearchType();
        search.setRecurrencePattern(new YearlyRecurrencePatternType(1L, 2L, "DEC", null, null));
        ret = service.searchSchedule(uc, null, search, null, null, null);
        assertNotNull(ret.getSchedules());
        assertTrue(ret.getTotalSize().intValue() > 0);

        search.setRecurrencePattern(null); // Clear Recurrence Pattern

        for (Schedule sch : SCHEDULES) {
            // Search by Once Recurrence Pattern.
            if (sch.getRecurrencePattern() instanceof OnceRecurrencePatternType) {
                search = new ScheduleSearchType();
                search.setRecurrencePattern(sch.getRecurrencePattern());
                ret = service.searchSchedule(uc, null, search, null, null, null);
                assertTrue(ret.getSchedules().size() > 0);
                assertTrue(ret.getTotalSize().intValue() > 0);
            }

            //Search by FacilityScheduleReason
            if (sch.getFacilityScheduleReason() != null) {
                search = new ScheduleSearchType();
                search.setFacilityScheduleReason(sch.getFacilityScheduleReason());
                ret = service.searchSchedule(uc, null, search, null, null, null);
                assertTrue(ret.getSchedules().size() > 0);
                assertTrue(ret.getTotalSize().intValue() > 0);
            }

            //Search by ForInternalLocationId
            if (sch.getForInternalLocationId() != null) {
                search = new ScheduleSearchType();
                search.setForInternalLocationId(sch.getForInternalLocationId());
                ret = service.searchSchedule(uc, null, search, null, null, null);
                assertTrue(ret.getSchedules().size() > 0);
                assertTrue(ret.getTotalSize().intValue() > 0);
            }

            // FacilityId
            if (sch.getFacilityId() != null) {
                search = new ScheduleSearchType();
                search.setFacilityId(sch.getFacilityId());
                ret = service.searchSchedule(uc, null, search, null, null, null);
                assertTrue(ret.getSchedules().size() > 0);
                assertTrue(ret.getTotalSize().intValue() > 0);
            }

            // Capacity
            if (sch.getCapacity() != null) {
                search = new ScheduleSearchType();
                search.setCapacity(sch.getCapacity());
                ret = service.searchSchedule(uc, null, search, null, null, null);
                assertTrue(ret.getSchedules().size() > 0);
                assertTrue(ret.getTotalSize().intValue() > 0);
            }

            // Effective Date
            if (sch.getEffectiveDate() != null) {
                search = new ScheduleSearchType();
                search.setFromEffectiveDate(getDateAndTime(sch.getEffectiveDate(), sch.getScheduleStartTime()));
                ret = service.searchSchedule(uc, null, search, null, null, null);
                assertTrue(ret.getSchedules().size() > 0);
                assertTrue(ret.getTotalSize().intValue() > 0);

                search.setToEffectiveDate(getDateAndTime(sch.getEffectiveDate(), sch.getScheduleStartTime()));
                ret = service.searchSchedule(uc, null, search, null, null, null);
                assertTrue(ret.getSchedules().size() > 0);
                assertTrue(ret.getTotalSize().intValue() > 0);
            }

            // Termination Date
            if (sch.getTerminationDate() != null) {
                search = new ScheduleSearchType();
                search.setFromTerminationDate(getDateAndTime(sch.getTerminationDate(), sch.getScheduleEndTime()));
                ret = service.searchSchedule(uc, null, search, null, null, null);
                assertTrue(ret.getSchedules().size() > 0);
                assertTrue(ret.getTotalSize().intValue() > 0);

                search.setToTerminationDate(getDateAndTime(sch.getTerminationDate(), sch.getScheduleEndTime()));
                ret = service.searchSchedule(uc, null, search, null, null, null);
                assertTrue(ret.getSchedules().size() > 0);
                assertTrue(ret.getTotalSize().intValue() > 0);
            }

            // Start Time
            if (sch.getScheduleStartTime() != null) {
                search = new ScheduleSearchType();
                search.setFromScheduleStartTime(sch.getScheduleStartTime());
                ret = service.searchSchedule(uc, null, search, null, null, null);
                assertTrue(ret.getSchedules().size() > 0);
                assertTrue(ret.getTotalSize().intValue() > 0);

                search.setToScheduleStartTime(sch.getScheduleStartTime());
                ret = service.searchSchedule(uc, null, search, null, null, null);
                assertTrue(ret.getSchedules().size() > 0);
                assertTrue(ret.getTotalSize().intValue() > 0);
            }

            // End Time
            if (sch.getScheduleEndTime() != null) {
                search = new ScheduleSearchType();
                search.setFromScheduleEndTime(sch.getScheduleEndTime());
                ret = service.searchSchedule(uc, null, search, null, null, null);
                assertTrue(ret.getSchedules().size() > 0);
                assertTrue(ret.getTotalSize().intValue() > 0);

                search.setToScheduleEndTime(sch.getScheduleEndTime());
                ret = service.searchSchedule(uc, null, search, null, null, null);
                assertTrue(ret.getSchedules().size() > 0);
                assertTrue(ret.getTotalSize().intValue() > 0);
            }

            // AtInternalLocationIds
            if (sch.getAtInternalLocationIds() != null && sch.getAtInternalLocationIds().size() > 0) {
                sch.getAtInternalLocationIds().remove(null);
                if (sch.getAtInternalLocationIds().size() > 0) {
                    search = new ScheduleSearchType();
                    search.setAtInternalLocationIds(sch.getAtInternalLocationIds());
                    log.info(search.toString());
                    ret = service.searchSchedule(uc, null, search, null, null, null);
                    assertTrue(ret.getSchedules().size() > 0);
                    assertTrue(ret.getTotalSize().intValue() > 0);
                }
            }

            // LocationIds
            if (sch.getLocationIds() != null && sch.getLocationIds().size() > 0) {
                sch.getLocationIds().remove(null);
                if (sch.getLocationIds().size() > 0) {
                    search = new ScheduleSearchType();
                    search.setLocationIds(sch.getLocationIds());
                    log.info(search.toString());
                    ret = service.searchSchedule(uc, null, search, null, null, null);
                    assertTrue(ret.getSchedules().size() > 0);
                    assertTrue(ret.getTotalSize().intValue() > 0);
                }
            }

            // OrganizationIds
            if (sch.getOrganizationIds() != null && sch.getOrganizationIds().size() > 0) {
                sch.getOrganizationIds().remove(null);
                if (sch.getOrganizationIds().size() > 0) {
                    search = new ScheduleSearchType();
                    search.setOrganizationIds(sch.getOrganizationIds());
                    log.info(search.toString());
                    ret = service.searchSchedule(uc, null, search, null, null, null);
                    assertTrue(ret.getSchedules().size() > 0);
                    assertTrue(ret.getTotalSize().intValue() > 0);
                }
            }
        }
    }

    @Test(dependsOnMethods = { "testSearch" }, enabled = true)
    public void testUpdate() {
        Set<Schedule> retAll = service.getAllSchedules(uc, Boolean.TRUE);
        assertNotNull(retAll);
        assertTrue(retAll.size() > 0);
        Schedule update;
        Schedule ret;

        for (Schedule sch : retAll) {

            update = new Schedule();

            update.setScheduleCategory(sch.getScheduleCategory());
            update.setFacilityId(facilityId);
            update.setForInternalLocationId(forInternalLocationId);
            update.setEffectiveDate(getEffectiveDate());
            update.setScheduleStartTime("08:00");
            update.setScheduleEndTime("16:00");
            update.setOrganizationIds(organizationIds3);

            update.setScheduleIdentification(sch.getScheduleIdentification());

            // Forinternallocation has wrong toClass.
            update.setForInternalLocationId(forInternalLocationId);
            ret = service.updateSchedule(uc, update);
            assertNotNull(ret);

            // One of AtInternalLocation Associations has the same value as ForInternalLocation Association. Negative. return code: 2002
            update.setOrganizationIds(null);
            update.setAtInternalLocationIds(organizationIds3);
            ret = service.updateSchedule(uc, update);
            assertNotNull(ret);
            assertEquals(ret.getAtInternalLocationIds(), organizationIds3);

        }
    }

    @Test(dependsOnMethods = { "testUpdate" }, enabled = true)
    public void testDelete() {
        int count1 = 0;
        int count2 = 0;
        for (Schedule sch : service.getAllSchedules(uc, false)) {
            Long id = sch.getScheduleIdentification();
            Schedule sch2 = service.getSchedule(uc, id);
            if (Boolean.FALSE.equals(sch2.getActiveScheduleFlag())) {
                Long ret = service.deleteSchedule(uc, id);
                assertEquals(ret, success);
                count1++;
            } else {
                count2++;
            }
        }
        //assertTrue(count1 > 0);
        //assertTrue(count2 > 0);

        Long ret = service.deleteAllSchedule(uc);
        assertEquals(ret, success);

        service.deleteAllScheduleConfigVar(uc);
        service.deleteAllSchedule(uc);
        service.deleteAll(uc);
    }

    //	/**
    //	 * The testing result of the test case reproduceTestDefectID_663_and_279()
    //	 * proves that Defect ID 663 and 279 cannot be reproduced.
    //	 */
    //	@Test(dependsOnMethods = { "testDelete" }, enabled = false)
    //	public void reproduceTestDefectID_663_and_279() {
    //		Long resetRet = service.reset(uc);
    //		assertEquals(resetRet, success);
    //
    //		for (ActivityCategory category : ActivityCategory.values()) {
    //			ScheduleConfigVariableType config = new ScheduleConfigVariableType(
    //					null, new CodeType("ActivityCategory", category.value()), randLong(1, 10));
    //			assertTrue(config.isValid());
    //			ScheduleConfigVariableReturnType confRet = service.createScheduleConfigVars(uc, config);
    //			assertEquals(confRet.getReturnCode(), success);
    //
    //			ScheduleType schedule = new ScheduleType();
    //			for (int year = 2010; year < 2020; year++) {
    //				schedule = new ScheduleType(
    //						null,
    //						config.getScheduleCategory(),  		// category
    //						new AssociationType(AssociationToClass.FACILITY.value(), uc.getFacilityId()), // facility
    //						null, 			// capacity
    //						getDateWithoutTime(year, Calendar.MAY, 1), 	// effectiveDate
    //						getDateWithoutTime(year + 3, Calendar.DECEMBER, 31),// terminationDate
    //						"08:15",		// scheduleStartTime
    //						"16:45",    	// scheduleEndTime
    //						null,			// recurrencePattern
    //						null,
    //						null,
    //						null			// associations
    //						);
    //				ScheduleReturnType scheduleRet = service.createSchedule(uc, schedule);
    //				assertEquals(scheduleRet.getReturnCode(), success);
    //			}
    //		}
    //
    //		ScheduleSearchType search = new ScheduleSearchType(
    //				null, // scheduleCategory
    //				null, // facility
    //				null, // capacity
    //				getDateWithoutTime(2012, Calendar.MAY, 1), // fromEffectiveDate
    //				getDateWithoutTime(2012, Calendar.MAY, 2), // toEffectiveDate);
    //				null, // Date fromTerminationDate
    //				null, // Date toTerminationDate,
    //				null, // Boolean activeScheduleFlag,
    //				null, // String fromScheduleStartTime,
    //				null, // String toScheduleStartTime,
    //				null, // String fromScheduleEndTime,
    //				null, // String toScheduleEndTime,
    //				null, // RecurrencePatternType recurrencePattern,
    //				null,
    //				null,
    //				null //Set<AssociationType> associations
    //				);
    //		assertTrue(search.isWellFormed());
    //		assertTrue(search.isValid());
    //
    //		SchedulesReturnType ret = service.searchSchedule(uc, search, MatchMode.EXACT.value());
    //		assertEquals(ret.getReturnCode(), success);
    //		assertTrue(ret.getCount() > 0);
    //		assertEquals(ret.getCount().intValue(), ActivityCategory.values().length);
    //
    //		search = new ScheduleSearchType(
    //				null, // scheduleCategory
    //				null, // facility
    //				null, // capacity
    //				getDateWithoutTime(2012, Calendar.MAY, 1), // fromEffectiveDate
    //				getDateWithoutTime(2012, Calendar.MAY, 1), // toEffectiveDate);
    //				null, // Date fromTerminationDate
    //				null, // Date toTerminationDate,
    //				null, // Boolean activeScheduleFlag,
    //				null, // String fromScheduleStartTime,
    //				null, // String toScheduleStartTime,
    //				null, // String fromScheduleEndTime,
    //				null, // String toScheduleEndTime,
    //				null, // RecurrencePatternType recurrencePattern,
    //				null,
    //				null,
    //				null //Set<AssociationType> associations
    //				);
    //		assertTrue(search.isWellFormed());
    //		assertTrue(search.isValid());
    //
    //		ret = service.searchSchedule(uc, search, MatchMode.EXACT.value());
    //		assertEquals(ret.getReturnCode(), success);
    //		assertTrue(ret.getCount() > 0);
    //		assertEquals(ret.getCount().intValue(), ActivityCategory.values().length);
    //
    //		search = new ScheduleSearchType(
    //				null, // scheduleCategory
    //				null, // facility
    //				null, // capacity
    //				null, // fromEffectiveDate
    //				null, // toEffectiveDate);
    //				getDateWithoutTime(2013, Calendar.DECEMBER, 31), // Date fromTerminationDate
    //				getDateWithoutTime(2014, Calendar.JANUARY, 1), // Date toTerminationDate,
    //				null, // Boolean activeScheduleFlag,
    //				null, // String fromScheduleStartTime,
    //				null, // String toScheduleStartTime,
    //				null, // String fromScheduleEndTime,
    //				null, // String toScheduleEndTime,
    //				null, // RecurrencePatternType recurrencePattern,
    //				null,
    //				null,
    //				null //Set<AssociationType> associations
    //				);
    //		assertTrue(search.isWellFormed());
    //		assertTrue(search.isValid());
    //
    //		ret = service.searchSchedule(uc, search, MatchMode.EXACT.value());
    //		assertEquals(ret.getReturnCode(), success);
    //		assertTrue(ret.getCount() > 0);
    //		assertEquals(ret.getCount().intValue(), ActivityCategory.values().length);
    //
    //		search = new ScheduleSearchType(
    //				null, // scheduleCategory
    //				null, // facility
    //				null, // capacity
    //				null, // fromEffectiveDate
    //				null, // toEffectiveDate);
    //				getDateWithoutTime(2013, Calendar.DECEMBER, 31), // Date fromTerminationDate
    //				getDateWithoutTime(2013, Calendar.DECEMBER, 31), // Date toTerminationDate,
    //				null, // Boolean activeScheduleFlag,
    //				null, // String fromScheduleStartTime,
    //				null, // String toScheduleStartTime,
    //				null, // String fromScheduleEndTime,
    //				null, // String toScheduleEndTime,
    //				null, // RecurrencePatternType recurrencePattern,
    //				null,
    //				null,
    //				null //Set<AssociationType> associations
    //				);
    //		assertTrue(search.isWellFormed());
    //		assertTrue(search.isValid());
    //
    //		ret = service.searchSchedule(uc, search, MatchMode.EXACT.value());
    //		assertEquals(ret.getReturnCode(), success);
    //		assertTrue(ret.getCount() > 0);
    //		assertEquals(ret.getCount().intValue(), ActivityCategory.values().length);
    //	}
    //
    //	@Test(dependsOnMethods = { "reproduceTestDefectID_663_and_279" }, enabled = false)
    //	public void reproduceTestDefectID_665() {
    //		Long resetRet = service.reset(uc);
    //		assertEquals(resetRet, success);
    //
    //		for (ActivityCategory category : ActivityCategory.values()) {
    //			ScheduleConfigVariableType config = new ScheduleConfigVariableType(
    //					null, new CodeType("ActivityCategory", category.value()), randLong(1, 10));
    //			assertTrue(config.isValid());
    //			ScheduleConfigVariableReturnType confRet = service.createScheduleConfigVars(uc, config);
    //			assertEquals(confRet.getReturnCode(), success);
    //		}
    //
    //		ScheduleType schedule;
    //		ScheduleReturnType schRet;
    //
    //		schedule = new ScheduleType(
    //				null, // Id
    //				new CodeType("ActivityCategory", "ASSESSMENT"), // category
    //				new AssociationType(AssociationToClass.FACILITY.value(), uc.getFacilityId()), // Facility
    //				null, 			// capacity
    //				getDateWithoutTime(2012, Calendar.MAY, 1), 	// effectiveDate
    //				getDateWithoutTime(2015, Calendar.DECEMBER, 31),// terminationDate
    //				"10:00",		// scheduleStartTime
    //				"11:30",    	// scheduleEndTime
    //				null,			// recurrencePattern
    //				null,
    //				null,
    //				null);
    //		assertTrue(schedule.isWellFormed());
    //		assertTrue(schedule.isValid());
    //		schRet = service.createSchedule(uc, schedule);
    //		assertEquals(schRet.getReturnCode(), success);
    //
    //		schedule = new ScheduleType(
    //				null, // Id
    //				new CodeType("ActivityCategory", "VISITATION"), // category
    //				new AssociationType(AssociationToClass.FACILITY.value(), uc.getFacilityId()), // Facility
    //				null, 			// capacity
    //				getDateWithoutTime(2012, Calendar.MAY, 1), 	// effectiveDate
    //				null,// terminationDate
    //				"11:00",		// scheduleStartTime
    //				null,    	// scheduleEndTime
    //				null,			// recurrencePattern
    //				null,
    //				null,
    //				null);
    //		assertTrue(schedule.isWellFormed());
    //		assertTrue(schedule.isValid());
    //		schRet = service.createSchedule(uc, schedule);
    //		assertEquals(schRet.getReturnCode(), success);
    //
    //		schedule = new ScheduleType(
    //				null, // Id
    //				new CodeType("ActivityCategory", "DISCIPLINE"), // category
    //				new AssociationType(AssociationToClass.FACILITY.value(), uc.getFacilityId()), // Facility
    //				null, 			// capacity
    //				getDateWithoutTime(2010, Calendar.MAY, 1), 	// effectiveDate
    //				getDateWithoutTime(2011, Calendar.DECEMBER, 31),// terminationDate
    //				"10:00",		// scheduleStartTime
    //				"11:30",    	// scheduleEndTime
    //				null,			// recurrencePattern
    //				null,
    //				null,
    //				null);
    //		assertTrue(schedule.isWellFormed());
    //		assertTrue(schedule.isValid());
    //		schRet = service.createSchedule(uc, schedule);
    //		assertEquals(schRet.getReturnCode(), success);
    //
    //		schedule = new ScheduleType(
    //				null, // Id
    //				new CodeType("ActivityCategory", "MOVEMENT"), // category
    //				new AssociationType(AssociationToClass.FACILITY.value(), uc.getFacilityId()), // Facility
    //				null, 			// capacity
    //				getDateWithoutTime(2013, Calendar.MAY, 1), 	// effectiveDate
    //				getDateWithoutTime(2015, Calendar.DECEMBER, 31),// terminationDate
    //				"10:00",		// scheduleStartTime
    //				"17:30",    	// scheduleEndTime
    //				null,			// recurrencePattern
    //				null,
    //				null,
    //				null);
    //		assertTrue(schedule.isWellFormed());
    //		assertTrue(schedule.isValid());
    //		schRet = service.createSchedule(uc, schedule);
    //		assertEquals(schRet.getReturnCode(), success);
    //
    //		schedule = new ScheduleType(
    //				null, // Id
    //				new CodeType("ActivityCategory", "FACOUNT"), // category
    //				new AssociationType(AssociationToClass.FACILITY.value(), uc.getFacilityId()), // Facility
    //				null, 			// capacity
    //				getDateWithoutTime(2012, Calendar.MAY, 1), 	// effectiveDate
    //				getDateWithoutTime(2015, Calendar.DECEMBER, 31),// terminationDate
    //				"15:00",		// scheduleStartTime
    //				"17:00",    	// scheduleEndTime
    //				null,			// recurrencePattern
    //				null,
    //				null,
    //				null);
    //		assertTrue(schedule.isWellFormed());
    //		assertTrue(schedule.isValid());
    //		schRet = service.createSchedule(uc, schedule);
    //		assertEquals(schRet.getReturnCode(), success);
    //
    //		schedule = new ScheduleType(
    //				null, // Id
    //				new CodeType("ActivityCategory", "FACOUNT"), // category
    //				new AssociationType(AssociationToClass.FACILITY.value(), uc.getFacilityId()), // Facility
    //				null, 			// capacity
    //				getDateWithoutTime(2012, Calendar.MAY, 1), 	// effectiveDate
    //				getDateWithoutTime(2015, Calendar.DECEMBER, 31),// terminationDate
    //				"15:00",		// scheduleStartTime
    //				"17:00",    	// scheduleEndTime
    //				null,			// recurrencePattern
    //				null,
    //				null,
    //				null);
    //		assertTrue(schedule.isWellFormed());
    //		assertTrue(schedule.isValid());
    //		schRet = service.createSchedule(uc, schedule);
    //		assertEquals(schRet.getReturnCode(), success);
    //
    //		schedule = new ScheduleType(
    //				null, // Id
    //				new CodeType("ActivityCategory", "FACOUNT"), // category
    //				new AssociationType(AssociationToClass.FACILITY.value(), uc.getFacilityId()), // Facility
    //				null, 			// capacity
    //				getDateWithoutTime(2012, Calendar.MAY, 1), 	// effectiveDate
    //				getDateWithoutTime(2015, Calendar.DECEMBER, 31),// terminationDate
    //				"15:00",		// scheduleStartTime
    //				"17:00",    	// scheduleEndTime
    //				null,			// recurrencePattern
    //				null,
    //				null,
    //				null);
    //		assertTrue(schedule.isWellFormed());
    //		assertTrue(schedule.isValid());
    //		schRet = service.createSchedule(uc, schedule);
    //		assertEquals(schRet.getReturnCode(), success);
    //
    //		schedule = new ScheduleType(
    //				null, // Id
    //				new CodeType("ActivityCategory", "FACOUNT"), // category
    //				new AssociationType(AssociationToClass.FACILITY.value(), uc.getFacilityId()), // Facility
    //				null, 			// capacity
    //				getDateWithoutTime(2012, Calendar.MAY, 1), 	// effectiveDate
    //				getDateWithoutTime(2015, Calendar.DECEMBER, 31),// terminationDate
    //				"15:00",		// scheduleStartTime
    //				"17:00",    	// scheduleEndTime
    //				null,			// recurrencePattern
    //				null,
    //				null,
    //				null);
    //		assertTrue(schedule.isWellFormed());
    //		assertTrue(schedule.isValid());
    //		schRet = service.createSchedule(uc, schedule);
    //		assertEquals(schRet.getReturnCode(), success);
    //
    //		schedule = new ScheduleType(
    //				null, // Id
    //				new CodeType("ActivityCategory", "FACOUNT"), // category
    //				new AssociationType(AssociationToClass.FACILITY.value(), uc.getFacilityId()), // Facility
    //				null, 			// capacity
    //				getDateWithoutTime(2012, Calendar.MAY, 1), 	// effectiveDate
    //				getDateWithoutTime(2015, Calendar.DECEMBER, 31),// terminationDate
    //				"15:00",		// scheduleStartTime
    //				"17:00",    	// scheduleEndTime
    //				null,			// recurrencePattern
    //				null,
    //				null,
    //				null);
    //		assertTrue(schedule.isWellFormed());
    //		assertTrue(schedule.isValid());
    //		schRet = service.createSchedule(uc, schedule);
    //		assertEquals(schRet.getReturnCode(), success);
    //
    //		schedule = new ScheduleType(
    //				null, // Id
    //				new CodeType("ActivityCategory", "FACOUNT"), // category
    //				new AssociationType(AssociationToClass.FACILITY.value(), uc.getFacilityId()), // Facility
    //				null, 			// capacity
    //				getDateWithoutTime(2012, Calendar.MAY, 1), 	// effectiveDate
    //				getDateWithoutTime(2015, Calendar.DECEMBER, 31),// terminationDate
    //				"15:00",		// scheduleStartTime
    //				"17:00",    	// scheduleEndTime
    //				null,			// recurrencePattern
    //				null,
    //				null,
    //				null);
    //		assertTrue(schedule.isWellFormed());
    //		assertTrue(schedule.isValid());
    //		schRet = service.createSchedule(uc, schedule);
    //		assertEquals(schRet.getReturnCode(), success);
    //
    //		SchedulesReturnType ret;
    //
    //		ScheduleSearchType search = new ScheduleSearchType();
    //		search.setFromScheduleEndTime("09:00");
    //
    //		ret = service.searchSchedule(uc, search, MatchMode.EXACT.value());
    //		assertEquals(ret.getReturnCode(), success);
    //		assertEquals(ret.getCount().intValue(), 9);
    //
    //		search = new ScheduleSearchType();
    //		search.setFromScheduleEndTime("17:00");
    //
    //		ret = service.searchSchedule(uc, search, MatchMode.EXACT.value());
    //		assertEquals(ret.getReturnCode(), success);
    //		assertEquals(ret.getCount().intValue(), 7);
    //
    //		search = new ScheduleSearchType();
    //		search.setToScheduleEndTime("12:00");
    //
    //		ret = service.searchSchedule(uc, search, MatchMode.EXACT.value());
    //		assertEquals(ret.getReturnCode(), success);
    //		assertEquals(ret.getCount().intValue(), 2);
    //
    //	}
    //
    //	@Test(dependsOnMethods = { "reproduceTestDefectID_665" }, enabled = false)
    //	public void reproduceTestDefectID_686() {
    //
    //		ScheduleConfigVariablesReturnType configRet = service.searchScheduleConfigVers(
    //				uc,
    //				new ScheduleConfigVariableSearchType(new CodeType("ActivityCategory", ActivityCategory.FACOUNT.value()), null),
    //				MatchMode.EXACT.value());
    //		assertEquals(configRet.getReturnCode(), success);
    //		ScheduleConfigVariableType config = (ScheduleConfigVariableType)configRet.getScheduleConfigVariables().toArray()[0];
    //		assertNotNull(config);
    //		assertTrue(config.getNumOfNextAvailableTimeslots() > 0);
    //
    //		Date lookupDate = this.getDateAndTime(this.getDateWithoutTime(2012, Calendar.MAY, 1), "15:00");
    //		Date lookupEndDate = this.getDateAndTime(this.getDateWithoutTime(2015, Calendar.DECEMBER, 31), "17:00");
    //		ScheduleTimeslotsReturnType ret = service.generateScheduleTimetable(uc, config, lookupDate, lookupEndDate, false);
    //		assertEquals(ret.getReturnCode(), success);
    //		assertEquals(ret.getCount().intValue(), 6);
    //	}

    private Date getDateWithoutTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Date newDate = cal.getTime();
        return newDate;
    }

    private Date getToday() {
        return getDateWithoutTime(Calendar.getInstance().getTime());
    }

    private Date getNextYear() {
        Calendar cal = Calendar.getInstance();

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.YEAR, 1);

        return cal.getTime();
    }

    private Date getDateWithoutTime(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);

        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    private Date getEffectiveDate() {
        Calendar cal = Calendar.getInstance();

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, randLong(23).intValue());
        cal.set(Calendar.MINUTE, randLong(59).intValue());
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, randLong(2000, 2014).intValue());

        return cal.getTime();
    }

    private Date getExpirationDate() {
        Calendar cal = Calendar.getInstance();

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, randLong(23).intValue());
        cal.set(Calendar.MINUTE, randLong(59).intValue());
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, randLong(2015, 2020).intValue());

        return cal.getTime();
    }

    private Long randLong() {
        return BigInteger.valueOf(new Random().nextLong()).abs().longValue();
    }

    private Long randLong(int i) {
        return BigInteger.valueOf(new Random().nextInt(i)).abs().longValue();
    }

    private Long randLong(int from, int to) {
        Long rand = BigInteger.valueOf(new Random().nextInt(to)).abs().longValue();
        while (rand < from) {
            rand = BigInteger.valueOf(new Random().nextInt(to)).abs().longValue();
        }
        return rand;
    }

    private Date getDateAndTime(Date date, String time) {
        if (date == null) {
			return null;
		}

        Calendar c = Calendar.getInstance();
        c.setTime(getDateWithoutTime(date));

        String[] hhmm;
        if (time == null) {
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
        } else {
            hhmm = time.split(":");
            c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(hhmm[0]));
            c.set(Calendar.MINUTE, Integer.valueOf(hhmm[1]));
        }
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }
}
