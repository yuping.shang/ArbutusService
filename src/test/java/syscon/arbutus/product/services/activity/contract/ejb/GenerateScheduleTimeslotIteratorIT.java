package syscon.arbutus.product.services.activity.contract.ejb;

import javax.naming.NamingException;
import java.math.BigInteger;
import java.util.*;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.testng.annotations.*;
import syscon.arbutus.product.services.activity.contract.dto.*;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;

import static org.testng.Assert.*;

public class GenerateScheduleTimeslotIteratorIT extends BaseIT {

    final static Long success = 1L;

    private static Logger log = LoggerFactory.getLogger(ActivityServiceIT.class);

    private Long facilityId;
    private ActivityService service;

    @BeforeMethod
    public void beforeMethod() {

    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        uc = super.initUserContext();

        BasicConfigurator.configure();

        // Facility
        FacilityTest f = new FacilityTest();
        facilityId = f.createFacility();
    }

    @BeforeTest
    public void beforeTest() throws NamingException, Exception {

    }

    @AfterClass
    public void afterClass() {
        //		if (this.facilityId != null) {
        //			FacilityTest f = new FacilityTest();
        //			f.deleteFacility(facilityId);
        //		}
    }

    @Test(enabled = true)
    public void once() {

        Long ret = service.deleteAll(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());

        ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, ActivityCategory.DISCIPLINE.value(), 31L);
        ScheduleConfigVariableType configRet = service.createScheduleConfigVars(uc, config);
        assertNotNull(configRet);
        config = configRet;

        Schedule sch = new Schedule();

        sch.setScheduleCategory(ActivityCategory.DISCIPLINE.value());
        sch.setFacilityId(facilityId);
        sch.setCapacity(20L);
        sch.setEffectiveDate(getDateWithoutTime(2012, 1, 1));
        sch.setScheduleStartTime("11:00");

        OnceRecurrencePatternType oncePatternType = new OnceRecurrencePatternType(getDateWithoutTime(2012, 2, 2));
        sch.setRecurrencePattern(oncePatternType);

        Schedule schRet = service.createSchedule(uc, sch);
        assertNotNull(schRet);
        sch = schRet;

        TimeslotIterator it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 1, 0, 0, 0), getDate(2012, 2, 3, 0, 0, 0), null);
        assertNotNull(it);
        int count = 0;
        while (it.hasNext()) {
            count++;
            ScheduleTimeslotType timeslot = it.next();
            assertNotNull(timeslot);
            assertEquals(timeslot.getScheduleID(), sch.getScheduleIdentification());
            assertEquals(timeslot.getTimeslotStartDateTime(), getDate(2012, 2, 2, 11, 0, 0));
            assertEquals(timeslot.getTimeslotEndDateTime(), null);
        }
        assertEquals(count, 1);

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 2, 0, 0, 0), getDate(2012, 2, 2, 0, 0, 0), null);
        assertNotNull(it);
        assertNotNull(it);
        assertTrue(it.hasNext());
        assertEquals(it.next().getTimeslotStartDateTime(), getDate(2012, 2, 2, 11, 0, 0));
        assertFalse(it.hasNext());

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 3, 0, 0, 0), getDate(2012, 2, 4, 0, 0, 0), null);
        assertNull(it);

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 1, 0, 0, 0), getDate(2012, 2, 2, 0, 0, 0), null);
        assertNotNull(it);
        count = 0;
        while (it.hasNext()) {
            count++;
            ScheduleTimeslotType timeslot = it.next();
            assertNotNull(timeslot);
            assertEquals(timeslot.getScheduleID(), sch.getScheduleIdentification());
            assertEquals(timeslot.getTimeslotStartDateTime(), getDate(2012, 2, 2, 11, 0, 0));
            assertEquals(timeslot.getTimeslotEndDateTime(), null);
        }
        assertEquals(count, 1);

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 2, 0, 0, 0), getDate(2012, 2, 3, 0, 0, 0), null);
        assertNotNull(it);
        count = 0;
        while (it.hasNext()) {
            count++;
            ScheduleTimeslotType timeslot = it.next();
            assertNotNull(timeslot);
            assertEquals(timeslot.getScheduleID(), sch.getScheduleIdentification());
            assertEquals(timeslot.getTimeslotStartDateTime(), getDate(2012, 2, 2, 11, 0, 0));
            assertEquals(timeslot.getTimeslotEndDateTime(), null);
        }
        assertEquals(count, 1);

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 1, 1, 0, 0, 0), null, null);
        assertNotNull(it);
        count = 0;
        while (it.hasNext()) {
            count++;
            ScheduleTimeslotType timeslot = it.next();
            assertNotNull(timeslot);
            assertEquals(timeslot.getScheduleID(), sch.getScheduleIdentification());
            assertEquals(timeslot.getTimeslotStartDateTime(), getDate(2012, 2, 2, 11, 0, 0));
            assertEquals(timeslot.getTimeslotEndDateTime(), null);
        }
        assertEquals(count, 1);

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 1, 0, 0, 0), null, null);
        assertNotNull(it);
        count = 0;
        while (it.hasNext()) {
            count++;
            ScheduleTimeslotType timeslot = it.next();
            assertNotNull(timeslot);
            assertEquals(timeslot.getScheduleID(), sch.getScheduleIdentification());
            assertEquals(timeslot.getTimeslotStartDateTime(), getDate(2012, 2, 2, 11, 0, 0));
            assertEquals(timeslot.getTimeslotEndDateTime(), null);
        }
        assertEquals(count, 1);

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 2, 0, 0, 0), null, null);
        assertNotNull(it);
        assertTrue(it.hasNext());
        assertEquals(it.next().getTimeslotStartDateTime(), getDate(2012, 2, 2, 11, 0, 0));
        assertFalse(it.hasNext());

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 3, 0, 0, 0), null, null);
        assertNull(it);

        sch.setScheduleEndTime("15:25");
        schRet = service.updateSchedule(uc, sch);
        sch = schRet;

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 1, 0, 0, 0), getDate(2012, 2, 3, 0, 0, 0), null);
        assertNotNull(it);
        assertTrue(it.hasNext());
        assertEquals(it.next().getTimeslotStartDateTime(), getDate(2012, 2, 2, 11, 0, 0));
        assertFalse(it.hasNext());

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 2, 0, 0, 0), getDate(2012, 2, 2, 0, 1, 0), null);
        assertNotNull(it);
        assertTrue(it.hasNext());
        assertEquals(it.next().getTimeslotStartDateTime(), getDate(2012, 2, 2, 11, 0, 0));
        assertFalse(it.hasNext());

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 3, 0, 0, 0), getDate(2012, 2, 4, 0, 0, 0), null);
        assertNull(it);

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 1, 0, 0, 0), getDate(2012, 2, 2, 0, 0, 0), null);
        assertNotNull(it);
        assertTrue(it.hasNext());
        assertEquals(it.next().getTimeslotStartDateTime(), getDate(2012, 2, 2, 11, 0, 0));
        assertFalse(it.hasNext());

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 2, 0, 0, 0), getDate(2012, 2, 3, 0, 0, 0), null);
        assertNotNull(it);
        assertTrue(it.hasNext());
        assertEquals(it.next().getTimeslotStartDateTime(), getDate(2012, 2, 2, 11, 0, 0));
        assertFalse(it.hasNext());

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 1, 1, 0, 0, 0), null, null);
        assertNotNull(it);
        assertTrue(it.hasNext());
        assertEquals(it.next().getTimeslotStartDateTime(), getDate(2012, 2, 2, 11, 0, 0));
        assertFalse(it.hasNext());

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 1, 0, 0, 0), null, null);
        assertNotNull(it);
        assertTrue(it.hasNext());
        assertEquals(it.next().getTimeslotStartDateTime(), getDate(2012, 2, 2, 11, 0, 0));
        assertFalse(it.hasNext());

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 2, 0, 0, 0), null, null);
        assertNotNull(it);
        assertTrue(it.hasNext());
        assertEquals(it.next().getTimeslotStartDateTime(), getDate(2012, 2, 2, 11, 0, 0));
        assertFalse(it.hasNext());

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 2, 3, 0, 0, 0), null, null);
        assertNull(it);

        service.deleteAllSchedule(uc);
        service.deleteAllScheduleConfigVar(uc);

    }

    @Test(dependsOnMethods = { "once" }, enabled = true)
    public void daily() {

        Long ret = service.deleteAll(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());

        int thisYear = Calendar.getInstance().get(Calendar.YEAR);

        ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, ActivityCategory.DISCIPLINE.value(), 31L);
        ScheduleConfigVariableType configRet = service.createScheduleConfigVars(uc, config);
        assertNotNull(configRet);
        config = configRet;

        Schedule sch = new Schedule();
        sch.setScheduleCategory(ActivityCategory.DISCIPLINE.value());
        sch.setFacilityId(facilityId);
        sch.setCapacity(20L);
        sch.setEffectiveDate(getDateWithoutTime(2012, 1, 1));
        sch.setScheduleStartTime("00:00");
        sch.setRecurrencePattern(new DailyWeeklyCustomRecurrencePatternType(1L, false, null));

        Schedule schRet = service.createSchedule(uc, sch);
        assertNotNull(schRet);
        sch = schRet;

        TimeslotIterator it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(thisYear, 1, 1, 0, 0, 0), getDate(thisYear, 12, 31, 23, 0, 0),
                null);
        int count = 0;
        while (it.hasNext()) {
            count++;
            assertNotNull(it.next().getTimeslotStartDateTime());
        }
        assertEquals(count, 365);

        // Iterator
        it = service.generateTimeslotIterator(uc, schRet.getScheduleIdentification(), getDate(thisYear, 1, 1, 0, 0, 0), getDate(thisYear, 12, 31, 0, 0, 0), null);
        count = 0;
        while (it.hasNext() && ++count <= 1000) {
            ScheduleTimeslotType scheduleTimeslot = it.next();
            assertNotNull(scheduleTimeslot);
            assertNotNull(scheduleTimeslot.getTimeslotStartDateTime());
            assertNotNull(scheduleTimeslot.getScheduleID());
            // System.out.println(count + ": " + it.next());
        }
        assertEquals(count, 365);

        it = service.generateTimeslotIterator(uc, schRet.getScheduleIdentification(), getDate(thisYear, 1, 1, 0, 0, 0), null, 365L);
        count = 0;
        while (it.hasNext() && ++count <= 1000) {
            ScheduleTimeslotType scheduleTimeslot = it.next();
            assertNotNull(scheduleTimeslot);
            assertNotNull(scheduleTimeslot.getTimeslotStartDateTime());
            assertNotNull(scheduleTimeslot.getScheduleID());
            // System.out.println(count + ": " + it.next());
        }
        assertEquals(count, 365);

        it = service.generateTimeslotIterator(uc, schRet.getScheduleIdentification(), getDate(thisYear, 1, 1, 0, 0, 0), null, null);
        count = 0;
        while (it.hasNext() && count < 365) {
            count = count + 1;
            ScheduleTimeslotType scheduleTimeslot = it.next();
            assertNotNull(scheduleTimeslot);
            assertNotNull(scheduleTimeslot.getTimeslotStartDateTime());
            assertNotNull(scheduleTimeslot.getScheduleID());
            // System.out.println(count + ": " + scheduleTimeslot.getTimeslotStartDateTime());
        }
        assertEquals(count, 365);

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(thisYear, 1, 1, 0, 0, 0), null, null);
        assertNotNull(it);
        count = 0;
        while (it.hasNext() && count < 1000) {
            count++;
            assertNotNull(it.next().getTimeslotStartDateTime());
        }
        assertEquals(count, 1000);

        sch.setRecurrencePattern(new DailyWeeklyCustomRecurrencePatternType(1L, true, null));
        schRet = service.updateSchedule(uc, sch);
        assertNotNull(schRet);
        sch = schRet;
        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(thisYear, 1, 1, 0, 0, 0), getDate(thisYear, 12, 31, 0, 0, 0), null);
        count = 0;
        while (it.hasNext()) {
            count++;
            assertNotNull(it.next().getTimeslotStartDateTime());
        }
        assertEquals(count, 261);

        sch.setRecurrencePattern(new DailyWeeklyCustomRecurrencePatternType(1L, false, null));
        schRet = service.updateSchedule(uc, sch);
        assertNotNull(schRet);

        sch.setTerminationDate(this.getDateWithoutTime(thisYear, 12, 31));
        sch.setScheduleEndTime("13:00");
        schRet = service.updateSchedule(uc, sch);
        assertNotNull(schRet);

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(thisYear, 1, 1, 0, 0, 0), getDate(thisYear, 12, 31, 0, 0, 0), null);
        count = 0;
        while (it.hasNext()) {
            count++;
            assertNotNull(it.next().getTimeslotStartDateTime());
        }
        assertEquals(count, 365);

        service.deleteAllSchedule(uc);
        // service.deleteAllScheduleConfigVar(uc);

        sch = new Schedule();
        sch.setScheduleCategory(ActivityCategory.DISCIPLINE.value());
        sch.setFacilityId(facilityId);
        sch.setCapacity(20L);
        sch.setEffectiveDate(getDateWithoutTime(2012, 1, 1));
        sch.setScheduleStartTime("11:00");
        sch.setRecurrencePattern(new DailyWeeklyCustomRecurrencePatternType(1L, false, new HashSet<String>(Arrays.asList(new String[] { "FRI", "SAT", "SUN" }))));

        schRet = service.createSchedule(uc, sch);
        assertNotNull(schRet);
        sch = schRet;

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(thisYear, 1, 1, 0, 0, 0), getDate(thisYear, 12, 31, 23, 0, 0), null);

        count = 0;
        while (it.hasNext()) {
            count++;
            ScheduleTimeslotType slot = it.next();
            assertNotNull(slot.getTimeslotStartDateTime());
        }
        assertEquals(count, 157);

        assertNotNull(service.createScheduleConfigVars(uc, new ScheduleConfigVariableType(null, ActivityCategory.PROGRAM.value(), 365L)));
        sch = new Schedule();
        sch.setScheduleCategory(ActivityCategory.PROGRAM.value());
        sch.setFacilityId(facilityId);
        // sch.setCapacity(20L);
        sch.setEffectiveDate(getDateWithoutTime(2014, 1, 1));
        sch.setScheduleStartTime("08:10");
        sch.setScheduleEndTime("10:50");
        DailyWeeklyCustomRecurrencePatternType recurrencePattern = new DailyWeeklyCustomRecurrencePatternType(1L, false, null);
        Set<ExDateType> excludeDateTimes = new HashSet<>();
        excludeDateTimes.add(new ExDateType(new LocalDate(2014, 2, 1), new LocalTime(8, 10), new LocalTime(10, 50)));
        recurrencePattern.setExcludeDateTimes(excludeDateTimes);
        sch.setRecurrencePattern(recurrencePattern);

        schRet = service.createSchedule(uc, sch);
        assertNotNull(schRet);
        sch = schRet;

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(thisYear, 1, 1, 0, 0, 0), getDate(thisYear, 12, 31, 23, 0, 0), null);
        count = 0;
        while (it.hasNext()) {
            count++;
            ScheduleTimeslotType slot = it.next();
            System.out.println(count + ": " + slot.getTimeslotStartDateTime() + " -- " + slot.getTimeslotEndDateTime());
            assertNotNull(slot.getTimeslotStartDateTime());
        }
        assertEquals(count, 364);

        sch = new Schedule();
        sch.setScheduleCategory(ActivityCategory.PROGRAM.value());
        sch.setFacilityId(facilityId);
        // sch.setCapacity(20L);
        sch.setEffectiveDate(getDateWithoutTime(2014, 1, 1));
        sch.setScheduleStartTime("08:10");
        sch.setScheduleEndTime("10:50");
        recurrencePattern = new DailyWeeklyCustomRecurrencePatternType(1L, false, null);
        Set<ExDateType> includeDateTimes = new HashSet<>();
        includeDateTimes.add(new ExDateType(new LocalDate(2014, 2, 1), new LocalTime(9, 10), new LocalTime(11, 50)));
        recurrencePattern.setIncludeDateTimes(includeDateTimes);
        sch.setRecurrencePattern(recurrencePattern);

        schRet = service.createSchedule(uc, sch);
        assertNotNull(schRet);
        sch = schRet;

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(thisYear, 1, 1, 0, 0, 0), getDate(thisYear, 12, 31, 23, 0, 0), null);
        count = 0;
        while (it.hasNext()) {
            count++;
            ScheduleTimeslotType slot = it.next();
            Date exDate = service.getIncludeDateTime(uc, sch.getScheduleIdentification(), slot.getTimeslotStartDateTime());
            if (exDate == null) {
                System.out.println(count + ": " + slot.getTimeslotStartDateTime() + " -- " + slot.getTimeslotEndDateTime());
            } else {
                System.out.println(count + ": " + slot.getTimeslotStartDateTime() + " -- " + exDate);
            }
            assertNotNull(slot.getTimeslotStartDateTime());
        }
        assertEquals(count, 366);

        // service.deleteAllSchedule(uc);
        // service.deleteAllScheduleConfigVar(uc);
    }

    @Test(enabled = false)
    public void weekly() {

        Long ret = service.deleteAll(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());

        ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, ActivityCategory.CASE.value(), 20L);
        ScheduleConfigVariableType configRet = service.createScheduleConfigVars(uc, config);
        assertNotNull(configRet);
        config = configRet;

        Schedule sch = new Schedule();
        sch.setScheduleCategory(ActivityCategory.CASE.value());
        sch.setFacilityId(facilityId);
        sch.setCapacity(20L);
        sch.setEffectiveDate(getDateWithoutTime(2012, 5, 1));
        sch.setScheduleStartTime("11:00");
        sch.setRecurrencePattern(new WeeklyRecurrencePatternType(1L, new HashSet<String>(Arrays.asList(new String[] { "MON", "WED", "FRI" }))));

        Schedule schRet = service.createSchedule(uc, sch);
        assertNotNull(schRet);

        Set<ScheduleTimeslotType> timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 1, 1, 0, 0, 0), getDate(2012, 12, 31, 0, 0, 0), true);
        assertNotNull(timeslotsRet);
        assertEquals(timeslotsRet.size(), 105);

        TimeslotIterator it = service.generateTimeslotIterator(uc, schRet.getScheduleIdentification(), getDate(2012, 1, 1, 0, 0, 0), getDate(2012, 12, 31, 0, 0, 0),
                null);
        int count = 0;
        while (it.hasNext()) {
            count++;
            assertNotNull(it.next().getTimeslotStartDateTime());
        }
        assertEquals(count, 105);

        service.deleteAllSchedule(uc);
        service.deleteAllScheduleConfigVar(uc);
    }

    @Test(enabled = false)
    public void monthly() {

        Long ret = service.deleteAll(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());

        ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, ActivityCategory.HBDMGMT.value(), 31L);
        ScheduleConfigVariableType configRet = service.createScheduleConfigVars(uc, config);
        assertNotNull(configRet);
        config = configRet;

        Schedule sch = new Schedule();
        sch.setScheduleCategory(ActivityCategory.HBDMGMT.value());
        sch.setFacilityId(facilityId);
        sch.setCapacity(31L);
        sch.setEffectiveDate(getDateWithoutTime(2012, 1, 1));
        sch.setScheduleStartTime("11:00");
        sch.setRecurrencePattern(new MonthlyRecurrencePatternType(1L, 2L, null, null));

        Schedule schRet = service.createSchedule(uc, sch);
        assertNotNull(schRet);

        sch = schRet;

        Set<ScheduleTimeslotType> timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 1, 1, 0, 0, 0), getDate(2012, 12, 31, 0, 0, 0), true);
        assertNotNull(timeslotsRet);
        // assertEquals(timeslotsRet.size(), 12);
        assertEquals(timeslotsRet.size(), 13);

        TimeslotIterator it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 1, 1, 0, 0, 0), getDate(2012, 12, 31, 0, 0, 0), null);
        int count = 0;
        while (it.hasNext()) {
            count++;
            Date d = it.next().getTimeslotStartDateTime();
            assertNotNull(d);
        }
        assertEquals(count, 13);

        sch.setRecurrencePattern(new MonthlyRecurrencePatternType(1L, null, "MON", "FIRST"));

        schRet = service.updateSchedule(uc, sch);
        assertNotNull(schRet);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 1, 1, 0, 0, 0), getDate(2012, 12, 31, 0, 0, 0), true);
        assertNotNull(timeslotsRet);
        // assertEquals(timeslotsRet.size(), 12);
        assertEquals(timeslotsRet.size(), 13);

        it = service.generateTimeslotIterator(uc, schRet.getScheduleIdentification(), getDate(2012, 1, 1, 0, 0, 0), getDate(2012, 12, 31, 0, 0, 0), null);
        count = 0;
        while (it.hasNext()) {
            count++;
            Date d = it.next().getTimeslotStartDateTime();
            assertNotNull(d);
        }
        assertEquals(count, 13);

        service.deleteAllSchedule(uc);
        service.deleteAllScheduleConfigVar(uc);
    }

    @Test(enabled = false)
    public void yearly() {

        Long ret = service.deleteAll(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());

        ScheduleConfigVariableType config = new ScheduleConfigVariableType(null, ActivityCategory.INCIDENT.value(), 31L);
        ScheduleConfigVariableType configRet = service.createScheduleConfigVars(uc, config);
        assertNotNull(configRet);
        config = configRet;

        Schedule sch = new Schedule();
        sch.setScheduleCategory(ActivityCategory.INCIDENT.value());
        sch.setFacilityId(facilityId);
        sch.setCapacity(31L);
        sch.setEffectiveDate(getDateWithoutTime(2012, 1, 1));
        sch.setScheduleStartTime("11:00");
        sch.setRecurrencePattern(new YearlyRecurrencePatternType(1L, 2L, "DEC", null, null));

        Schedule schRet = service.createSchedule(uc, sch);
        assertNotNull(schRet);

        sch = schRet;

        Set<ScheduleTimeslotType> timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 1, 1, 0, 0, 0), getDate(2014, 12, 31, 0, 0, 0), true);
        assertNotNull(timeslotsRet);
        // assertEquals(timeslotsRet.size(), 3);
        assertEquals(timeslotsRet.size(), 4);

        TimeslotIterator it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 1, 1, 0, 0, 0), getDate(2014, 12, 31, 0, 0, 0), null);
        int count = 0;
        while (it.hasNext()) {
            count++;
            assertNotNull(it.next().getTimeslotStartDateTime());
        }
        assertEquals(count, 4);

        sch.setRecurrencePattern(new YearlyRecurrencePatternType(1L, null, "DEC", "MON", "FIRST"));

        schRet = service.updateSchedule(uc, sch);
        assertNotNull(schRet);

        timeslotsRet = service.generateScheduleTimetable(uc, config, getDate(2012, 1, 1, 0, 0, 0), getDate(2012, 12, 31, 0, 0, 0), true);
        assertNotNull(timeslotsRet);
        // assertEquals(timeslotsRet.size(), 1);
        assertEquals(timeslotsRet.size(), 2);

        it = service.generateTimeslotIterator(uc, sch.getScheduleIdentification(), getDate(2012, 1, 1, 0, 0, 0), getDate(2012, 12, 31, 0, 0, 0), null);
        count = 0;
        while (it.hasNext()) {
            count++;
            assertNotNull(it.next().getTimeslotStartDateTime());
        }
        assertEquals(count, 2);

        service.deleteAllSchedule(uc);
        service.deleteAllScheduleConfigVar(uc);
    }

    private Date getEffectiveDate() {
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.HOUR_OF_DAY, randLong(23).intValue());
        cal.set(Calendar.MINUTE, randLong(59).intValue());
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, randLong(2012).intValue());

        return cal.getTime();
    }

    private Long randLong(int i) {
        return BigInteger.valueOf(new Random().nextInt(i)).abs().longValue();
    }

    private Long randLong(int from, int to) {
        Long rand = BigInteger.valueOf(new Random().nextInt(to)).abs().longValue();
        while (rand < from) {
            rand = BigInteger.valueOf(new Random().nextInt(to)).abs().longValue();
        }
        return rand;
    }

    private Date getDateWithoutTime(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.DAY_OF_MONTH, day);

        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    private Date getDateWithoutTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Date newDate = cal.getTime();
        return newDate;
    }

    private Date getDate(int year, int month, int day, int hour, int minute, int second) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.DAY_OF_MONTH, day);

        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, second);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }
}
