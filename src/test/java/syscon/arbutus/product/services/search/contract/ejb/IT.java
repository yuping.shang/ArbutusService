package syscon.arbutus.product.services.search.contract.ejb;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.common.PersonTest;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.search.contract.dto.AdvSearchContext;
import syscon.arbutus.product.services.search.contract.dto.PersonIdentitiesAdvanceSearchReturnType;
import syscon.arbutus.product.services.search.contract.interfaces.SearchService;

@ModuleConfig
public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);
    private PersonService service;
    private SearchService searchservice;

    private PersonTest psnTest;

    private Long personId;

    @BeforeMethod
    public void beforeMethod() {

        //reset();
        try {
            login();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        uc = super.initUserContext();
        searchservice = (SearchService) JNDILookUp(this.getClass(), SearchService.class);

    }

    @AfterClass
    public void afterClass() {
        //service.deleteAll(uc);

    }

    @Test
    public void testSounddex() {
        String name = "testnnnnnnnn";
        String ret = SearchUtils.getNameCode(name, false);
        assert (ret != null);
    }

    //@Test
    public void advPersonSearch() {
        //This test is working on specific data only, like QA3, if switch database, please change your search criteria

        AdvSearchContext ctx = new AdvSearchContext();
        ctx.setFacilityHashMap(getCurrentAllowedFacilityIds());
        ctx.setStartindex(0l);
        ctx.setResultsize(15l);
        ctx.setSearchType(1);
        //ctx.setFirstName("A*");
        ctx.setLastName("TE*");
        ctx.setOffenderNum("00001*");
        ctx.setSupDisplayId("15-7*");
        //ctx.setDateOfBirthFrom();
        //ctx.setDateOfBirthTo();

        PersonIdentitiesAdvanceSearchReturnType ret = searchservice.personSearch(uc, ctx);
        assert (ret != null);
    }

    //@Test
    public void advInmateSearch() {

        //This test is working on specific data only, like QA3, if switch database, please change your search criteria

        AdvSearchContext ctx = new AdvSearchContext();
        ctx.setFacilityHashMap(getCurrentAllowedFacilityIds());
        ctx.setStartindex(0l);
        ctx.setResultsize(15l);
        ctx.setSearchType(1);
        //ctx.setFirstName("A*");
        ctx.setLastName("TE*");
        ctx.setOffenderNum("00001*");
        ctx.setSupDisplayId("15-7*");
        //ctx.setDateOfBirthFrom();
        //ctx.setDateOfBirthTo();

        PersonIdentitiesAdvanceSearchReturnType ret = searchservice.inmateSearch(uc, ctx);
        assert (ret != null);
    }

    @Test
    public void quickSearch() {

        //This test is working on specific data only, like QA3, if switch database, please change your search criteria

        AdvSearchContext ctx = new AdvSearchContext();
        ctx.setFacilityHashMap(getCurrentAllowedFacilityIds());
        ctx.setStartindex(0l);
        ctx.setResultsize(15l);
        ctx.setSearchType(1);
        //ctx.setQuickSearch("A*,TE*, 15-78*");
        ctx.setQuickSearch("A*,TE*, 199112*");
        /*
        ctx.setFirstName("A*");
        ctx.setLastName("TE*");
        ctx.setOffenderNum("00001*");
        ctx.setSupDisplayId("15-7*");
        ctx.setDateOfBirthFrom(toBirthDateFrom(34l));
        ctx.setDateOfBirthTo(toBirthDateTo(34l));
        */

        PersonIdentitiesAdvanceSearchReturnType ret = searchservice.quickSearch(uc, ctx);
        assert (ret != null);

    }

    @Test
    public void testDateFormat() {
        String dateString = "19911225";
        Date ret = BeanHelper.convertToDate(dateString, "yyyyMMdd");
        assert (ret != null);
    }

    @Test
    public void testQuickSearchBirthDate() {

        String dateString = "19780202";
        Date retFrom = toQuickSearchBirthDate(dateString, true);
        Date retTo = toQuickSearchBirthDate(dateString, false);

        System.out.println(dateString + " From:" + retFrom);
        System.out.println(dateString + " To:" + retTo);
        /*
        dateString = "19700";
        retFrom = toQuickSearchBirthDate(dateString, true);
        retTo = toQuickSearchBirthDate(dateString, false);

        System.out.println(dateString + " From:" + retFrom);
        System.out.println(dateString + " To:" + retTo);

        dateString = "197002";
        retFrom = toQuickSearchBirthDate(dateString, true);
        retTo = toQuickSearchBirthDate(dateString, false);

        System.out.println(dateString + " From:" + retFrom);
        System.out.println(dateString + " To:" + retTo);

        dateString = "197007";
        retFrom = toQuickSearchBirthDate(dateString, true);
        retTo = toQuickSearchBirthDate(dateString, false);

        System.out.println(dateString + " From:" + retFrom);
        System.out.println(dateString + " To:" + retTo);

        dateString = "1970021";
        retFrom = toQuickSearchBirthDate(dateString, true);
        retTo = toQuickSearchBirthDate(dateString, false);

        System.out.println(dateString + " From:" + retFrom);
        System.out.println(dateString + " To:" + retTo);

        dateString = "1970022";
        retFrom = toQuickSearchBirthDate(dateString, true);
        retTo = toQuickSearchBirthDate(dateString, false);

        System.out.println(dateString + " From:" + retFrom);
        System.out.println(dateString + " To:" + retTo);

        dateString = "1970023";
        retFrom = toQuickSearchBirthDate(dateString, true);
        retTo = toQuickSearchBirthDate(dateString, false);

        System.out.println(dateString + " From:" + retFrom);
        System.out.println(dateString + " To:" + retTo);
        */
    }

    private Map<Long, String> getCurrentAllowedFacilityIds() {
        Map<Long, String> ret = new HashMap<Long, String>();
        ret.put(1l, "Denver County Jail");
        ret.put(3l, "Denver Downtown Detention Center");
        ret.put(29l, "Denver District Court");
        ret.put(28l, "Denver County Court");
        return ret;
    }

    private Date toBirthDateFrom(Long age) {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.YEAR, -Integer.valueOf(age.toString()));
        cal.set(Calendar.DAY_OF_YEAR, 1);
        return cal.getTime();
    }

    private Date toBirthDateTo(Long age) {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.YEAR, -Integer.valueOf(age.toString()));
        cal.set(Calendar.MONTH, 11); // 11 = december
        cal.set(Calendar.DAY_OF_MONTH, 31); // new years eve
        return cal.getTime();
    }

    private Date toQuickSearchBirthDate(String param, boolean isFrom) {
        if (param == null || param.trim().length() == 0) {
            return null;
        }

        param = param.trim();
        Date ret = null;
        try {
            int num = Integer.parseInt(param);
            int length = param.length();

            //handle the year start with 1700
            String validate = param + "000";
            validate = validate.substring(0, 4);
            int intValidate = Integer.parseInt(validate);
            if (intValidate < 1700) {
                return null;
            }

            if (length <= 4) {
                int year = Integer.parseInt(param);
                if (isFrom) {
                    if (year < 100) {
                        year = year * 100;
                    } else if (year < 1000) {
                        year = year * 10;
                    }
                } else {
                    if (year < 100) {
                        year = year * 100 + 99;
                    } else if (year < 1000) {
                        year = year * 10 + 9;
                    }
                }
                ret = getBirthDate(year, -1, -1, isFrom, false);
            } else if (length <= 6) {
                String strYear = param.substring(0, 4);
                String strMonth = param.substring(4, length);
                int year = Integer.parseInt(strYear);
                int month = Integer.parseInt(strMonth);
                if (length == 5) {
                    if (month == 0) {
                        ret = getBirthDate(year, month, -1, isFrom, true);
                    } else if (month == 1) {
                        ret = getBirthDate(year, month, -1, isFrom, true);
                    }
                } else {
                    if (month <= 12) {
                        ret = getBirthDate(year, month, -1, isFrom, false);
                    }
                }

            } else if (length < 8) {
                String strYear = param.substring(0, 4);
                String strMonth = param.substring(4, 6);
                String strDay = param.substring(6, length);
                int year = Integer.parseInt(strYear);
                int month = Integer.parseInt(strMonth);
                int day = Integer.parseInt(strDay);
                if (month <= 12 && day <= 3) {
                    ret = getBirthDate(year, month, day, isFrom, false);
                }

            }
        } catch (Exception ex) {
            //ex.printStackTrace();
        }
        return ret;
    }

    /**
     * Return the birth date from or to.
     *
     * @param year    int start with 1*
     * @param month   int only accept the number which less than 12 and great than 1, -1 means not consider the month
     * @param day     int only accept 1, 2, 3 and -1 means not consider the day
     * @param isFrom  boolean is birth date from
     * @param isRange boolean is range search
     * @return
     */
    private Date getBirthDate(int year, int month, int day, boolean isFrom, boolean isRange) {
        Date ret = null;
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.YEAR, year);
        if (month == -1 && day == -1) {
            if (isFrom) {
                cal.set(Calendar.DAY_OF_YEAR, 1);
            } else {
                cal.set(Calendar.MONTH, 11); // 11 = december
                cal.set(Calendar.DAY_OF_MONTH, 31); // new years eve
            }
        } else if (month == 0 && day == -1) {
            //month range
            if (isFrom) {
                cal.set(Calendar.MONTH, 0);
                cal.set(Calendar.DAY_OF_MONTH, 1);
            } else {
                cal.set(Calendar.MONTH, 8);
                cal.set(Calendar.DAY_OF_MONTH, 30);
            }
        } else if (month > 0 && day == -1) {
            //exact month
            if (isRange && month == 1) {
                if (isFrom) {
                    cal.set(Calendar.MONTH, 1 * 10 - 1);
                    cal.set(Calendar.DAY_OF_MONTH, 1);
                } else {
                    cal.set(Calendar.MONTH, 11);
                    cal.set(Calendar.DAY_OF_MONTH, 31);
                }
            } else {
                if (isFrom) {
                    cal.set(Calendar.MONTH, month - 1);
                    cal.set(Calendar.DAY_OF_MONTH, 1);
                } else {
                    cal.set(Calendar.MONTH, month);
                    cal.set(Calendar.DAY_OF_MONTH, 0);
                }
            }

        } else if (month >= 0 && day >= 0) {
            cal.set(Calendar.MONTH, month - 1);
            if (day <= 3) {
                if (isFrom) {
                    if (month == 2 && day == 3) {
                        cal.set(Calendar.DAY_OF_MONTH, 1);
                    } else {
                        day = day * 10;
                        cal.set(Calendar.DATE, day);
                    }
                } else {
                    if (day == 3 || (month == 2 && day == 2)) {
                        cal.set(Calendar.MONTH, month);
                        cal.set(Calendar.DAY_OF_MONTH, 0);
                    } else {
                        day = day * 10 + 9;
                        cal.set(Calendar.DATE, day);
                    }
                }
            }
        }
        ret = cal.getTime();
        return ret;
    }
}
