package syscon.arbutus.product.services.housingassignment.contract.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingassignment.HousingChangeRequestType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingAttributeMismatchType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.OffenderHousingAttributeType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingChangeRequestTypeTest {

    @Test
    public void isValid() {

        HousingChangeRequestType data = new HousingChangeRequestType();
        assert (validate(data) == false);

        data.setOffenderSupervisionId(1L);
        assert (validate(data) == false);

        data.setLocationRequested(1L);
        assert (validate(data) == false);

        data.setIssuedBy(1L);
        assert (validate(data) == false);

        data.setMovementReason("Test");
        assert (validate(data) == false);

        data.setCrPriority("Test");
        assert (validate(data) == false);

        data.setCrType("Test");
        assert (validate(data) == false);

        OffenderHousingAttributeType housingAttributesRequired = new OffenderHousingAttributeType();
        data.setHousingAttributesRequired(housingAttributesRequired);
        assert (validate(data) == false);

        housingAttributesRequired = new OffenderHousingAttributeType("Test", "Test", 1L, "Test", null, null);
        data.setHousingAttributesRequired(housingAttributesRequired);
        assert (validate(data) == true);

        data.setOverallOutcome(null);
        assert (validate(data) == true);

        data.setOverallOutcome("Test");
        assert (validate(data) == true);

        data.setComment(new CommentType());
        assert (validate(data) == false);

        data.setComment(new CommentType(null, "Test", new Date(), "Test"));
        assert (validate(data) == true);

        Set<HousingAttributeMismatchType> mismatches = new HashSet<HousingAttributeMismatchType>();
        data.setAttributeMismatches(mismatches);
        assert (validate(data) == true);

        HousingAttributeMismatchType mismatch1 = new HousingAttributeMismatchType();
        mismatches = new HashSet<HousingAttributeMismatchType>();
        mismatches.add(mismatch1);
        data.setAttributeMismatches(mismatches);
        assert (validate(data) == false);
    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }

}
