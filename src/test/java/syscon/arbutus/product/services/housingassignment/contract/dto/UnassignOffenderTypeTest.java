package syscon.arbutus.product.services.housingassignment.contract.dto;

import java.util.Date;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingassignment.UnassignOffenderType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class UnassignOffenderTypeTest {

    @Test
    public void isValid() {

        UnassignOffenderType data = new UnassignOffenderType();
        assert (validate(data) == false);

        data.setOffenderSupervisionId(1L);
        assert (validate(data) == false);

        data.setUnAssignedByStaffId(1L);
        assert (validate(data) == false);

        data.setFacilityId(1L);
        assert (validate(data) == false);

        data.setUnAssignmentDate(new Date());
        assert (validate(data) == false);

        data.setHousingMovementReason(null);
        assert (validate(data) == false);

        data.setHousingMovementReason("");
        assert (validate(data) == false);

        data.setHousingMovementReason("Test");
        assert (validate(data) == true);

        data.setMovementOutcome(null);
        assert (validate(data) == true);

        data.setMovementOutcome("Test");
        assert (validate(data) == true);

        data.setComment(new CommentType());
        assert (validate(data) == false);

        data.setComment(new CommentType(null, "Test", new Date(), "Test"));
        assert (validate(data) == true);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
