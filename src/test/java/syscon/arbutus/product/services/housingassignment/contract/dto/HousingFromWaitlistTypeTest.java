package syscon.arbutus.product.services.housingassignment.contract.dto;

import java.util.Date;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingassignment.HousingFromWaitlistType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingFromWaitlistTypeTest {

    @Test
    public void isValid() {

        HousingFromWaitlistType data = new HousingFromWaitlistType();
        assert (validate(data) == false);

        data.setHousingBedMgmtActivityId(1L);
        assert (validate(data) == false);

        data.setToHousingLocationId(1L);
        assert (validate(data) == false);

        data.setAssignedByStaffId(1L);
        assert (validate(data) == false);

        data.setAssignmentDate(new Date());
        assert (validate(data) == false);

        data.setOverallOutcome(null);
        assert (validate(data) == false);

        data.setOverallOutcome("Test");
        assert (validate(data) == true);

        data.setMovementOutcome(null);
        assert (validate(data) == true);

        data.setMovementOutcome("Test");
        assert (validate(data) == true);

        data.setComment(new CommentType());
        assert (validate(data) == false);

        data.setComment(new CommentType(null, "Test", new Date(), "Test"));
        assert (validate(data) == true);
    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
