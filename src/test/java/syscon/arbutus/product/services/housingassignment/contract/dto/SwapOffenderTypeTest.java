package syscon.arbutus.product.services.housingassignment.contract.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingassignment.SwapOffenderType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingAttributeMismatchType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class SwapOffenderTypeTest {

    @Test
    public void isValid() {

        SwapOffenderType data = new SwapOffenderType();
        assert (validate(data) == false);

        data.setOffenderASupervisionId(1L);
        assert (validate(data) == false);

        data.setOffenderBSupervisionId(1L);
        assert (validate(data) == false);

        data.setSwappedByStaffId(1L);
        assert (validate(data) == false);

        data.setFacilityId(1L);
        assert (validate(data) == false);

        data.setSwapDate(new Date());
        assert (validate(data) == false);

        data.setOffenderAOverallOutcome("Test");
        assert (validate(data) == false);

        data.setOffenderBOverallOutcome("Test");
        assert (validate(data) == false);

        data.setMovementReason("Test");
        assert (validate(data) == true);

        data.setMovementOutcome(null);
        assert (validate(data) == true);

        data.setMovementOutcome("Test");
        assert (validate(data) == true);

        data.setComment(new CommentType());
        assert (validate(data) == false);

        data.setComment(new CommentType(null, "Test", new Date(), "Test"));
        assert (validate(data) == true);

        Set<HousingAttributeMismatchType> mismatches = new HashSet<HousingAttributeMismatchType>();
        data.setAttributeMismatchesA(mismatches);
        assert (validate(data) == true);

        HousingAttributeMismatchType mismatch1 = new HousingAttributeMismatchType();
        mismatches = new HashSet<HousingAttributeMismatchType>();
        mismatches.add(mismatch1);
        data.setAttributeMismatchesA(mismatches);
        assert (validate(data) == false);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }

}
