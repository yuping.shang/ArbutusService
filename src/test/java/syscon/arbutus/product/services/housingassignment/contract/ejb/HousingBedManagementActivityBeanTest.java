package syscon.arbutus.product.services.housingassignment.contract.ejb;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.*;
import syscon.arbutus.product.services.housing.contract.interfaces.HousingService;

import static org.testng.Assert.assertNotNull;

public class HousingBedManagementActivityBeanTest extends BaseIT {

    private static final CodeType SECURITY_LEVEL_MIN = new CodeType(ReferenceSet.SECURITY_LEVEL.value(), "MIN");
    private static final CodeType SECURITY_LEVEL_MAX = new CodeType(ReferenceSet.SECURITY_LEVEL.value(), "MAX");

    private static final String ASSESSMENT_RESULT_MIN = "MIN";
    private static final String SUITABILITY_SECLEVEL = "SECLEVEL";
    private static final String SEX_F = "F";

    private static final CodeType CODE_ASSESSMENT_RESULT_MIN = new CodeType(ReferenceSet.ASSESSMENT_RESULT.value(), "MIN");

    private static final String OUTCOME_SUTBL = "SUTBL";
    private static final String CASE_SENTENCE_STATUS_SENT = "SENTENCED";
    private static final String MOVEMENT_REASON_MED = "MED";

    private HousingService service;

    private UserContext uc = null; //new UserContext();

    /**
     *
     */
    public HousingBedManagementActivityBeanTest() {

        lookupJNDILogin();
        //Clean data and reload reference code
        //service.reset(uc);
    }

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();

    }

    //for release
    private void lookupJNDILogin() {

        //JNDI lookup
        service = (HousingService) JNDILookUp(this.getClass(), HousingService.class);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();

    }

    @Test
    public void reset() {
        service.deleteAll(uc);
    }

    public void delete(HousingBedMgmtActivityType dto) {
        Long ret = service.delete(uc, dto);
        assert (ret.equals(ReturnCode.Success.returnCode()));
    }

    public Long assignHousingLocation(Long offenderId, Long locationId, Long activityId, Long assignedBy, Long facilityId) {

        String movementReason = MOVEMENT_REASON_MED;

        // call assignHousingLocation
        HousingBedMgmtActivityType ret = assignHousingLocation(offenderId, locationId, movementReason, activityId, assignedBy, facilityId);

        return ret.getHousingActivityId();

    }

    public Long createChangeRequest(Long offenderId, Long locationId, Long activityId, Long issuedBy) {

        HousingBedMgmtActivityType ret = createChangeRequest(offenderId, locationId, activityId, MOVEMENT_REASON_MED, true, issuedBy);
        return ret.getHousingActivityId();
    }

    private HousingBedMgmtActivityType assignHousingLocation(Long offenderId, Long locationId, String movementReasonCode, Long activityId, Long assignedBy,
            Long facilityId) {

        String movementReason = movementReasonCode;

        String movementComment = "Housing Assinged comments" + offenderId;

        Long offenderReference = offenderId;
        Long locationTo = locationId;

        // build HousingAttributeMismatch for assignment
        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, getSecurityLevelCodes(),
                OUTCOME_SUTBL, 1L);

        Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        assignmentMismatches.add(mismatch);

        // build HousingLocationAssignType
        Date assignedDate = new Date();
        HousingLocationAssignType housingAssign = new HousingLocationAssignType(activityId, offenderReference, movementReason, locationTo, assignedBy, assignedDate,
                assignmentMismatches, OUTCOME_SUTBL, movementComment, facilityId);

        // call assignHousingLocation
        HousingBedMgmtActivityType ret = service.assignHousingLocation(uc, housingAssign);

        return ret;

    }

    private HousingBedMgmtActivityType createChangeRequest(Long offenderId, Long locationId, Long activityId, String movementReason, boolean isReservationFlag,
            Long issuedBy) {

        // Assign the offender to locationId + 100 first
        //assignHousingLocation(offenderId, locationId + 100);

        // build housing bed mgmt activity properties
        //CodeType movementReason = MOVEMENT_REASON_OFFREQ;	//Approval Required
        //CodeType movementReason = MOVEMENT_REASON_MED; //Approval Not Required
        Long offenderReference = offenderId;

        // build static association properties
        Long locationRequested = locationId;

        // build code type properties
        String crPriority = "HIGH";
        String crType = "INTRA";

        // build OffenderHousingAttribute
        String securityLevel = ASSESSMENT_RESULT_MIN;
        String gender = SEX_F;

        String sentenceStatuses = CASE_SENTENCE_STATUS_SENT;

        OffenderHousingAttributeType housingAttributesRequired = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatuses, null, null);

        String movementComment = "createChangeRequest comments" + offenderId;

        // build HousingLocationCRType
        HousingLocationCRType housingLocationCR = new HousingLocationCRType(activityId, offenderReference, movementReason, locationRequested, issuedBy, crPriority,
                crType, isReservationFlag, housingAttributesRequired, null, OUTCOME_SUTBL, movementComment);

        HousingBedMgmtActivityType retCreate = service.createChangeRequest(uc, housingLocationCR);

        return retCreate;

    }

    private Set<CodeType> getSecurityLevelCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(SECURITY_LEVEL_MIN);
        ret.add(SECURITY_LEVEL_MAX);
        return ret;
    }
}
