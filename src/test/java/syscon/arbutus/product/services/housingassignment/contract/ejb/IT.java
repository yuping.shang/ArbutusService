package syscon.arbutus.product.services.housingassignment.contract.ejb;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.*;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingassignment.*;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingAttributeMismatchType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.OffenderHousingAttributeType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.ReferenceSet;
import syscon.arbutus.product.services.housing.contract.ejb.HousingServiceBean;
import syscon.arbutus.product.services.housing.contract.interfaces.HousingService;

import static org.testng.Assert.assertNotNull;

public class IT extends BaseIT {

    //Code for housing bed management activity

    private static final String SUITABILITY_SECLEVEL = "SECLEVEL";
    private static final String SUITABILITY_GENDER = "GENDER";
    private static final String SUITABILITY_OFFCAT = "OFFCAT";
    private static final String SUITABILITY_SENTENCE = "SENTENCE";
    private static final String SUITABILITY_LOCPROP = "LOCPROP";
    private static final String SUITABILITY_AGE = "AGE";

    private static final CodeType CODE_AGE_RANGE_ADULT = new CodeType(ReferenceSet.AGE_RANGE_DESCRIPTION.value(), "ADULT");
    private static final CodeType CODE_AGE_RANGE_JUVENILE = new CodeType(ReferenceSet.AGE_RANGE_DESCRIPTION.value(), "JUVENILE");

    private static final CodeType CODE_SECURITY_LEVEL_MIN = new CodeType(ReferenceSet.SECURITY_LEVEL.value(), "MIN");
    private static final CodeType CODE_SECURITY_LEVEL_MAX = new CodeType(ReferenceSet.SECURITY_LEVEL.value(), "MAX");

    private static final String ASSESSMENT_RESULT_MIN = "MIN";
    private static final CodeType CODE_ASSESSMENT_RESULT_MIN = new CodeType(ReferenceSet.ASSESSMENT_RESULT.value(), "MIN");

    private static final CodeType OFFENDER_GENDER_F = new CodeType(ReferenceSet.OFFENDER_GENDER.value(), "F");
    private static final CodeType OFFENDER_GENDER_M = new CodeType(ReferenceSet.OFFENDER_GENDER.value(), "M");

    private static final String SEX_F = "F";
    private static final CodeType CODE_SEX_F = new CodeType(ReferenceSet.SEX.value(), "F");

    private static final String HEALTH_ISSUES_D = "D";
    private static final String HEALTH_ISSUES_DI = "DI";

    private static final CodeType CODE_HEALTH_ISSUES_D = new CodeType(ReferenceSet.HEALTH_ISSUES.value(), "D");
    //private static final CodeType CODE_HEALTH_ISSUES_DI = new CodeType(ReferenceSet.HEALTH_ISSUES.value(), "DI");

    private static final String OUTCOME_SUTBL = "SUTBL";
    private static final String OUTCOME_WRNG = "WRNG";
    private static final String OUTCOME_NOTPD = "NOTPD";
    private static final String OUTCOME_APPR = "APPR";

    private static final CodeType SENTENCE_STATUS_SENTC = new CodeType(ReferenceSet.SENTENCE_STATUS.value(), "SENTENCED");
    private static final CodeType SENTENCE_STATUS_UNSTC = new CodeType(ReferenceSet.SENTENCE_STATUS.value(), "UNSENTENCED");

    private static final String CASE_SENTENCE_STATUS_SENT = "SENTENCED";
    private static final CodeType CODE_CASE_SENTENCE_STATUS_SENT = new CodeType(ReferenceSet.CASE_SENTENCE_STATUS.value(), "SENTENCED");

    private static final CodeType OFFENDER_CATEGORY_SUCD = new CodeType(ReferenceSet.OFFENDER_CATEGORY.value(), "SUIC");
    private static final CodeType OFFENDER_CATEGORY_PRCUS = new CodeType(ReferenceSet.OFFENDER_CATEGORY.value(), "PRCUS");

    private static final String CAUTION_CODE_S = "90";
    private static final String CAUTION_CODE_V = "70";

    private static final CodeType CODE_CAUTION_CODE_S = new CodeType(ReferenceSet.CAUTION_CODE.value(), "90");
    //private static final CodeType CODE_CAUTION_CODE_V = new CodeType(ReferenceSet.CAUTION_CODE.value(), "70");

    private static final CodeType LOCATION_PROPERTY_WHEELCHAIR = new CodeType(ReferenceSet.LOCATION_PROPERTY.value(), "WHEELCHAIR");
    private static final CodeType LOCATION_PROPERTY_HSENS = new CodeType(ReferenceSet.LOCATION_PROPERTY.value(), "HSENS");
    private static final CodeType LOCATION_PROPERTY_SHOWER = new CodeType(ReferenceSet.LOCATION_PROPERTY.value(), "SHOWER");

    private static final String MOVEMENT_REASON_MED = "MED";

    private static final String CR_PRIORITY_HIGH = "HIGH";
    private static final String CR_TYPE_INTRA = "INTRA";

    //Code for movement activity
    private static final String MOVEMENT_OUTCOME_SICK = "SICK";

    private HousingService service;

    private Long locationToA = 299L;
    private Long locationToB = 300L;
    private Long locationToC = 303L;
    private Long locationToD = 304L;

    private Long offenderSupervisionIdA = 11L;
    private Long offenderSupervisionIdB = 12L;

    private Long activityId;
    private Long staffId;
    private Long facilityId;
    private Long housingChangeCausingActivityId;

    private FacilityTest facTest;
    private StaffTest stfTest;
    private SupervisionTest supTest;
    private FacilityInternalLocationTest filTest;
    private ActivityTest actTest;

    //for clean test data only
    private PersonIdentityTest piTest;
    private PersonTest psnTest;
    private MovementActivityTest maTest;
    private HousingBedActivityTest haTest;

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();

        facTest = new FacilityTest();
        stfTest = new StaffTest();
        supTest = new SupervisionTest();
        filTest = new FacilityInternalLocationTest();
        actTest = new ActivityTest();

        //localLogin();
        //cleanUp();

        testDateCreation();

    }

    @AfterClass
    public void afterClass() {

        cleanUp();

    }

    private void cleanUp() {

        //for clean test data only
        piTest = new PersonIdentityTest();
        psnTest = new PersonTest();
        maTest = new MovementActivityTest();
        haTest = new HousingBedActivityTest();

        maTest.deleteAll();
        haTest.deleteAll();
        actTest.deleteAll();

        filTest.deleteAll();
        supTest.deleteAll();
        stfTest.deleteAll();
        facTest.deleteAll();

        piTest.deleteAll();
        psnTest.deleteAll();

    }

    //for local debug use only
    @SuppressWarnings("unused")
    private void localLogin() {

        service = new HousingServiceBean();
        //init user context and login
        uc = super.initUserContext();
    }

    //for release
    private void lookupJNDILogin() {

        //JNDI lookup
        service = (HousingService) JNDILookUp(this.getClass(), HousingService.class);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();
    }

    //@Test
    public void reset() {

        HousingBedManagementActivityBeanTest housingTest = new HousingBedManagementActivityBeanTest();
        housingTest.reset();

        actTest.deleteAll();

        //MovementActivityBeanTest movementTest = new MovementActivityBeanTest();
        //movementTest.reset();
    }

    //@Test
    public void testDateCreation() {

        reset();

        facilityId = facTest.createFacility();
        staffId = stfTest.createStaff();
        offenderSupervisionIdA = supTest.createSupervision(facilityId, true);
        offenderSupervisionIdB = supTest.createSupervision(facilityId, true);

        activityId = actTest.createActivityForHousing();

        //Setting facilityInternalLocation
        filTest.createFILTree(facilityId);
        locationToA = filTest.getCell1();
        locationToB = filTest.getCell2();
        locationToC = filTest.getCell3();
        locationToD = filTest.getCell4();

    }

    @Test
    public void assignHousingToOffenderDirectlyNegative() {

        //Empty parameters
        verifyAssignHousingToOffenderDirectlyNegative(null, InvalidInputException.class);

        //Empty dto
        HousingDirectlyType dto = new HousingDirectlyType();
        verifyAssignHousingToOffenderDirectlyNegative(dto, InvalidInputException.class);

        //Invalid
        dto = new HousingDirectlyType();
        dto.setOffenderSupervisionId(1L);
        dto.setToHousingLocationId(2L);
        verifyAssignHousingToOffenderDirectlyNegative(dto, InvalidInputException.class);

    }

    @Test
    public void assignHousingToOffenderDirectlyPositive() {

        //Long housingChangeCausingActivityId = 6513L;
        housingChangeCausingActivityId = actTest.createActivityFromHousing(offenderSupervisionIdA);

        Date assignmentDate = new Date();
        Set<HousingAttributeMismatchType> attributeMismatches = getAllMismatches();
        CommentType comment = getComment(assignmentDate, "Test assignHousingToOffenderDirectly", "1");
        HousingDirectlyType dto = new HousingDirectlyType(offenderSupervisionIdA, locationToA, staffId, housingChangeCausingActivityId, facilityId, assignmentDate,
                MOVEMENT_REASON_MED, MOVEMENT_OUTCOME_SICK, OUTCOME_SUTBL, attributeMismatches, comment);
        //dto.setFacilities(getFacilities(facilityId));

        Long ret = service.assignHousingToOffenderDirectly(uc, dto);

        assert (ret != null);

        //Set mismatches
        housingChangeCausingActivityId = actTest.createActivityFromHousing(offenderSupervisionIdB);
        assignmentDate = new Date();
        attributeMismatches = getMismatches();
        dto = new HousingDirectlyType(offenderSupervisionIdB, locationToB, staffId, housingChangeCausingActivityId, facilityId, assignmentDate, MOVEMENT_REASON_MED,
                MOVEMENT_OUTCOME_SICK, OUTCOME_SUTBL, attributeMismatches, comment);
        //dto.setFacilities(getFacilities(facilityId));

        ret = service.assignHousingToOffenderDirectly(uc, dto);

        assert (ret != null);

    }

    @Test
    public void createHousingChangeRequestNegative() {

        //Empty parameters
        verifyCreateHousingChangeRequestNegative(null, InvalidInputException.class);

        //Empty dto
        HousingChangeRequestType dto = new HousingChangeRequestType();
        verifyCreateHousingChangeRequestNegative(dto, InvalidInputException.class);

        //Invalid
        dto = new HousingChangeRequestType();
        dto.setOffenderSupervisionId(1L);
        dto.setLocationRequested(1L);
        verifyCreateHousingChangeRequestNegative(dto, InvalidInputException.class);

    }

    //@Test
    @Test(dependsOnMethods = { "assignHousingToOffenderDirectlyPositive" })
    public void createHousingChangeRequestPositive() {

        String crPriority = CR_PRIORITY_HIGH;
        String crType = CR_TYPE_INTRA;

        OffenderHousingAttributeType housingAttributesRequired = getOffenderHousingAttributes();

        CommentType comment = getComment(new Date(), "Test createHousingChangeRequest", "1");

        Long locationTo = locationToD;
        HousingChangeRequestType dto = new HousingChangeRequestType(offenderSupervisionIdB, locationTo, staffId, MOVEMENT_REASON_MED, crPriority, crType,
                housingAttributesRequired, getAllMismatches(), OUTCOME_WRNG, comment, null);

        Long ret = service.createHousingChangeRequest(uc, dto);

        assert (ret != null);

    }

    @Test
    public void assignHousingFromWaitlistNegative() {

        //Empty parameters
        verifyAssignHousingFromWaitlistNegative(null, InvalidInputException.class);

        //Empty dto
        HousingFromWaitlistType dto = new HousingFromWaitlistType();
        verifyAssignHousingFromWaitlistNegative(dto, InvalidInputException.class);

        //Invalid
        dto = new HousingFromWaitlistType();
        dto.setHousingBedMgmtActivityId(1L);
        dto.setToHousingLocationId(2L);
        verifyAssignHousingFromWaitlistNegative(dto, InvalidInputException.class);

    }

    //@Test
    @Test(dependsOnMethods = { "createHousingChangeRequestPositive" })
    public void assignHousingFromWaitlistPositive() {

		/*
        HousingBedManagementActivityBeanTest housingTest = new HousingBedManagementActivityBeanTest();
		housingTest.assignHousingLocation(offenderSupervisionIdA, locationToB);
		*/

        Long locationTo = locationToC;

        Long activityId = actTest.createActivityFromHousing(offenderSupervisionIdA);

        HousingBedManagementActivityBeanTest housingTest = new HousingBedManagementActivityBeanTest();
        Long housingChangeRequestActivityId = housingTest.createChangeRequest(offenderSupervisionIdA, locationTo, activityId, staffId);

        Long assignedBy = staffId;
        Date assignedDate = new Date();
        Long housingChangeCausingActivityId = actTest.createActivityFromHousing(offenderSupervisionIdA);

        Set<HousingAttributeMismatchType> attributeMismatches = getAllMismatches();
        CommentType comment = getComment(assignedDate, "Test assignHousingFromWaitlist", "1");

        locationTo = locationToD;    //add this to testing defect 2757
        HousingFromWaitlistType dto = new HousingFromWaitlistType(housingChangeRequestActivityId, locationTo, assignedBy, housingChangeCausingActivityId, assignedDate,
                MOVEMENT_OUTCOME_SICK, OUTCOME_SUTBL, attributeMismatches, comment);
        //dto.setFacilities(getFacilities(facilityId)); // must be set

        Long ret = service.assignHousingFromWaitlist(uc, dto);

        assert (ret != null);

        //housingTest.delete(housingActivityId);

    }

    @Test
    public void swapOffenderHousingAssignmentsNegative() {

        //Empty parameters
        verifySwapOffenderHousingAssignmentsNegative(null, InvalidInputException.class);

        //Empty dto
        SwapOffenderType dto = new SwapOffenderType();
        verifySwapOffenderHousingAssignmentsNegative(dto, InvalidInputException.class);

        //Invalid
        dto = new SwapOffenderType();
        dto.setOffenderASupervisionId(1L);
        verifySwapOffenderHousingAssignmentsNegative(dto, InvalidInputException.class);

    }

    //@Test
    @Test(dependsOnMethods = { "assignHousingFromWaitlistPositive" })
    public void swapOffenderHousingAssignmentsPositive() {
		
		/*
		//Assign housing location first
		HousingBedManagementActivityBeanTest housingTest = new HousingBedManagementActivityBeanTest();
		housingTest.assignHousingLocation(offenderSupervisionIdA, locationToA);
		housingTest.assignHousingLocation(offenderSupervisionIdB, locationToB);
		*/
        String offenderAOverallOutcome = OUTCOME_SUTBL;
        String offenderBOverallOutcome = OUTCOME_SUTBL;
        Set<HousingAttributeMismatchType> attributeMismatchesA = getMismatches();
        Set<HousingAttributeMismatchType> attributeMismatchesB = getAllMismatches();

        String movementReason = MOVEMENT_REASON_MED;
        String movementOutcome = null; //MOVEMENT_OUTCOME_SICK;
        Long swappedByStaffId = staffId;
        Date swapDate = new Date();
        CommentType comment = null; //getComment(swapDate, "Test swapOffenderHousingAssignments", "1");

        SwapOffenderType dto = new SwapOffenderType(offenderSupervisionIdA, offenderAOverallOutcome, attributeMismatchesA, offenderSupervisionIdB,
                offenderBOverallOutcome, attributeMismatchesB, facilityId, movementReason, movementOutcome, swappedByStaffId, swapDate, comment);
        //dto.setFacilities(getFacilities(facilityId)); // must be set

        HousingAssignmentsReturnType ret = service.swapOffenderHousingAssignments(uc, dto);

        assert (ret.getHousingBedMgmtActivityIds() != null);
    }

    @Test
    public void unAssignOffenderFromHousingLocationNegative() {

        //Empty parameters
        verifyUnAssignOffenderFromHousingLocationNegative(null, InvalidInputException.class);

        //Empty dto
        UnassignOffenderType dto = new UnassignOffenderType();
        verifyUnAssignOffenderFromHousingLocationNegative(dto, InvalidInputException.class);

        //Invalid
        dto = new UnassignOffenderType();
        dto.setOffenderSupervisionId(1L);
        verifyUnAssignOffenderFromHousingLocationNegative(dto, InvalidInputException.class);

    }

    //@Test
    @Test(dependsOnMethods = { "swapOffenderHousingAssignmentsPositive" })
    public void unAssignOffenderFromHousingLocationPositive() {
		
		/*
		//Assign housing location first
		HousingBedManagementActivityBeanTest housingTest = new HousingBedManagementActivityBeanTest();
		housingTest.assignHousingLocation(offenderSupervisionIdB, locationToB);
		*/

        Long unAssignedByStaffId = staffId;

        Long housingChangeCausingActivityId = actTest.createActivityFromHousing(offenderSupervisionIdB);
        //Long housingChangeCausingActivityId = 6577L;
        Date unAssignmentDate = new Date();
        CommentType comment = getComment(unAssignmentDate, "Test unAssignOffenderFromHousingLocation", "1");

        UnassignOffenderType dto = new UnassignOffenderType(offenderSupervisionIdB, unAssignedByStaffId, housingChangeCausingActivityId, facilityId, unAssignmentDate,
                MOVEMENT_REASON_MED, MOVEMENT_OUTCOME_SICK, comment);
        //dto.setFacilities(getFacilities(facilityId)); // must be set

        Long ret = service.unAssignOffenderFromHousingLocation(uc, dto);

        assert (ret != null);

    }

    //@Test
    @Test(dependsOnMethods = { "unAssignOffenderFromHousingLocationPositive" })
    public void testCR1532() {

        //Test for movementOutcome and Comment optional

        Long offenderId = supTest.createSupervision(facilityId, true);

        ////////////////////////////////////////////////////////////////
        //Test Assign directly
        ///////////////////////////////////////////////////////////////

        //Long housingChangeCausingActivityId = 6513L;
        Long housingChangeCausingActivityId = actTest.createActivityFromHousing(offenderId);

        Date assignmentDate = new Date();
        HousingDirectlyType dto = new HousingDirectlyType(offenderId, locationToA, staffId, housingChangeCausingActivityId, facilityId, assignmentDate,
                MOVEMENT_REASON_MED, null, OUTCOME_SUTBL, null, null);
        //dto.setFacilities(getFacilities(facilityId));

        Long ret = service.assignHousingToOffenderDirectly(uc, dto);
        assert (ret != null);

        ////////////////////////////////////////////////////////////////
        //Test create CR
        ///////////////////////////////////////////////////////////////
        String crPriority = CR_PRIORITY_HIGH;
        String crType = CR_TYPE_INTRA;

        OffenderHousingAttributeType housingAttributesRequired = getOffenderHousingAttributes();

        Long locationTo = locationToD;
        HousingChangeRequestType dtoCR = new HousingChangeRequestType(offenderId, locationTo, staffId, MOVEMENT_REASON_MED, crPriority, crType, housingAttributesRequired,
                null, OUTCOME_WRNG, null, null);

        Long ret2 = service.createHousingChangeRequest(uc, dtoCR);
        assert (ret2 != null);

        ////////////////////////////////////////////////////////////////
        //Test Assign from waitlist
        ///////////////////////////////////////////////////////////////

        Long housingChangeRequestActivityId = ret2;
        Long assignedBy = staffId;
        Date assignedDate = new Date();
        housingChangeCausingActivityId = actTest.createActivityFromHousing(offenderSupervisionIdA);

        HousingFromWaitlistType dtoWL = new HousingFromWaitlistType(housingChangeRequestActivityId, locationTo, assignedBy, housingChangeCausingActivityId, assignedDate,
                null, OUTCOME_SUTBL, null, null);

        Long ret3 = service.assignHousingFromWaitlist(uc, dtoWL);
        assert (ret3 != null);

        ////////////////////////////////////////////////////////////////
        //Test unAssign
        ///////////////////////////////////////////////////////////////
        Long unAssignedByStaffId = staffId;
        housingChangeCausingActivityId = actTest.createActivityFromHousing(offenderId);
        //Long housingChangeCausingActivityId = 6577L;
        Date unAssignmentDate = new Date();
        UnassignOffenderType dtoUN = new UnassignOffenderType(offenderId, unAssignedByStaffId, housingChangeCausingActivityId, facilityId, unAssignmentDate,
                MOVEMENT_REASON_MED, null, null);

        Long ret4 = service.unAssignOffenderFromHousingLocation(uc, dtoUN);
        assert (ret4 != null);

    }

    private OffenderHousingAttributeType getOffenderHousingAttributes() {

        //build OffenderHousingAttributeType
        String securityLevel = ASSESSMENT_RESULT_MIN;
        String gender = SEX_F;

        String sentenceStatuses = CASE_SENTENCE_STATUS_SENT;

        Set<String> locationProperties = new HashSet<String>();
        locationProperties.add(HEALTH_ISSUES_D);
        locationProperties.add(HEALTH_ISSUES_DI);

        Set<String> categories = new HashSet<String>();
        categories.add(CAUTION_CODE_S);
        categories.add(CAUTION_CODE_V);

        OffenderHousingAttributeType ret = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatuses, locationProperties, categories);

        return ret;
    }

    private Set<CodeType> getSecurityLevelCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(CODE_SECURITY_LEVEL_MIN);
        ret.add(CODE_SECURITY_LEVEL_MAX);
        return ret;
    }

    private Set<CodeType> getGenderCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(OFFENDER_GENDER_F);
        ret.add(OFFENDER_GENDER_M);
        return ret;
    }

    private Set<CodeType> getAgeRangeCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(CODE_AGE_RANGE_ADULT);
        ret.add(CODE_AGE_RANGE_JUVENILE);
        return ret;
    }

    private Set<CodeType> getSentenceStatusCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(SENTENCE_STATUS_SENTC);
        ret.add(SENTENCE_STATUS_UNSTC);
        return ret;
    }

    private Set<CodeType> getOffenderCategoryCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(OFFENDER_CATEGORY_SUCD);
        ret.add(OFFENDER_CATEGORY_PRCUS);
        return ret;
    }

    private Set<CodeType> getLocationPropertyCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(LOCATION_PROPERTY_HSENS);
        ret.add(LOCATION_PROPERTY_SHOWER);
        ret.add(LOCATION_PROPERTY_WHEELCHAIR);
        return ret;
    }

    private Set<HousingAttributeMismatchType> getAllMismatches() {

        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, getSecurityLevelCodes(),
                OUTCOME_SUTBL, activityId);
        HousingAttributeMismatchType mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_GENDER, CODE_SEX_F, getGenderCodes(), OUTCOME_SUTBL, activityId);
        HousingAttributeMismatchType mismatch3 = new HousingAttributeMismatchType(null, SUITABILITY_AGE, CODE_AGE_RANGE_JUVENILE, getAgeRangeCodes(), OUTCOME_WRNG,
                activityId);
        HousingAttributeMismatchType mismatch4 = new HousingAttributeMismatchType(null, SUITABILITY_SENTENCE, CODE_CASE_SENTENCE_STATUS_SENT, getSentenceStatusCodes(),
                OUTCOME_WRNG, activityId);
        HousingAttributeMismatchType mismatch5 = new HousingAttributeMismatchType(null, SUITABILITY_OFFCAT, CODE_CAUTION_CODE_S, getOffenderCategoryCodes(), OUTCOME_APPR,
                activityId);
        HousingAttributeMismatchType mismatch6 = new HousingAttributeMismatchType(null, SUITABILITY_LOCPROP, CODE_HEALTH_ISSUES_D, getLocationPropertyCodes(),
                OUTCOME_NOTPD, activityId);

        Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        assignmentMismatches.add(mismatch);
        assignmentMismatches.add(mismatch2);
        assignmentMismatches.add(mismatch3);
        assignmentMismatches.add(mismatch4);
        assignmentMismatches.add(mismatch5);
        assignmentMismatches.add(mismatch6);

        return assignmentMismatches;

    }

    private Set<HousingAttributeMismatchType> getMismatches() {

        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, getSecurityLevelCodes(),
                OUTCOME_SUTBL, activityId);

        HousingAttributeMismatchType mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_GENDER, CODE_SEX_F, getGenderCodes(), OUTCOME_SUTBL, activityId);

        HousingAttributeMismatchType mismatch3 = new HousingAttributeMismatchType(null, SUITABILITY_AGE, CODE_AGE_RANGE_ADULT, getAgeRangeCodes(), OUTCOME_SUTBL,
                activityId);

        Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        assignmentMismatches.add(mismatch);
        assignmentMismatches.add(mismatch2);
        assignmentMismatches.add(mismatch3);

        return assignmentMismatches;

    }

    private CommentType getComment(Date commentDate, String commentText, String userId) {

        CommentType ret = new CommentType();
        ret.setCommentDate(commentDate);
        ret.setComment(commentText);
        ret.setUserId(userId);

        return ret;
    }

    private void verifyUnAssignOffenderFromHousingLocationNegative(UnassignOffenderType dto, Class<?> expectionClazz) {
        try {
            Long ret = service.unAssignOffenderFromHousingLocation(uc, dto);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifySwapOffenderHousingAssignmentsNegative(SwapOffenderType dto, Class<?> expectionClazz) {
        try {
            HousingAssignmentsReturnType ret = service.swapOffenderHousingAssignments(uc, dto);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyAssignHousingFromWaitlistNegative(HousingFromWaitlistType dto, Class<?> expectionClazz) {
        try {
            Long ret = service.assignHousingFromWaitlist(uc, dto);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyCreateHousingChangeRequestNegative(HousingChangeRequestType dto, Class<?> expectionClazz) {
        try {
            Long ret = service.createHousingChangeRequest(uc, dto);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyAssignHousingToOffenderDirectlyNegative(HousingDirectlyType dto, Class<?> expectionClazz) {
        try {
            Long ret = service.assignHousingToOffenderDirectly(uc, dto);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }
}
