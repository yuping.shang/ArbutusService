package syscon.arbutus.product.services.housingbedmanagementactivity.contract.ejb;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.*;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.LinkCodeType;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.*;
import syscon.arbutus.product.services.housing.contract.interfaces.HousingService;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.referencedata.contract.interfaces.ReferenceDataService;

import static org.testng.Assert.assertNotNull;

@ModuleConfig
public class IT extends BaseIT {

    private static final String ACTIVITY_STATE_WAITL = "WAITL";
    private static final String ACTIVITY_STATE_CANCL = "CANCL";
    private static final String ACTIVITY_STATE_ASSGN = "ASSGN";
    private static final String SUITABILITY_SECLEVEL = "SECLEVEL";
    private static final String SUITABILITY_GENDER = "GENDER";
    private static final String SUITABILITY_OFFCAT = "OFFCAT";
    private static final String SUITABILITY_SENTENCE = "SENTENCE";
    private static final String SUITABILITY_LOCPROP = "LOCPROP";
    private static final String SUITABILITY_AGE = "AGE";
    private static final CodeType AGE_RANGE_ADULT = new CodeType(ReferenceSet.AGE_RANGE_DESCRIPTION.value(), "ADULT");
    private static final CodeType AGE_RANGE_JUVENILE = new CodeType(ReferenceSet.AGE_RANGE_DESCRIPTION.value(), "JUVENILE");
    private static final CodeType AGE_RANGE_NUM = new CodeType(ReferenceSet.AGE_RANGE_DESCRIPTION.value(), "70");
    private static final CodeType SECURITY_LEVEL_MIN = new CodeType(ReferenceSet.SECURITY_LEVEL.value(), "MIN");
    private static final CodeType SECURITY_LEVEL_MAX = new CodeType(ReferenceSet.SECURITY_LEVEL.value(), "MAX");
    private static final String ASSESSMENT_RESULT_MIN = "MIN";
    private static final CodeType CODE_ASSESSMENT_RESULT_MIN = new CodeType(ReferenceSet.ASSESSMENT_RESULT.value(), "MIN");
    private static final CodeType OFFENDER_GENDER_F = new CodeType(ReferenceSet.OFFENDER_GENDER.value(), "F");
    private static final CodeType OFFENDER_GENDER_M = new CodeType(ReferenceSet.OFFENDER_GENDER.value(), "M");
    private static final String SEX_F = "F";
    private static final String SEX_M = "M";
    private static final CodeType CODE_SEX_F = new CodeType(ReferenceSet.SEX.value(), "F");
    private static final String HEALTH_ISSUES_D = "D";
    private static final String HEALTH_ISSUES_DI = "DI";
    private static final CodeType CODE_HEALTH_ISSUES_D = new CodeType(ReferenceSet.HEALTH_ISSUES.value(), "D");
    private static final String OUTCOME_SUTBL = "SUTBL";
    private static final String OUTCOME_WRNG = "WRNG";
    private static final String OUTCOME_NOTPD = "NOTPD";
    private static final String OUTCOME_APPR = "APPR";
    private static final CodeType SENTENCE_STATUS_SENTC = new CodeType(ReferenceSet.SENTENCE_STATUS.value(), "SENTENCED");
    private static final CodeType SENTENCE_STATUS_UNSTC = new CodeType(ReferenceSet.SENTENCE_STATUS.value(), "UNSENTENCED");
    private static final String CASE_SENTENCE_STATUS_SENT = "SENTENCED";
    private static final String CASE_SENTENCE_STATUS_UNSENT = "UNSENTENCED";
    private static final CodeType CODE_CASE_SENTENCE_STATUS_SENT = new CodeType(ReferenceSet.CASE_SENTENCE_STATUS.value(), "SENTENCED");
    private static final CodeType OFFENDER_CATEGORY_SUCD = new CodeType(ReferenceSet.OFFENDER_CATEGORY.value(), "SUIC");
    private static final CodeType OFFENDER_CATEGORY_PRCUS = new CodeType(ReferenceSet.OFFENDER_CATEGORY.value(), "PRCUS");
    private static final String CAUTION_CODE_V = "70";
    private static final String CAUTION_CODE_S = "90";
    private static final CodeType CODE_CAUTION_CODE_S = new CodeType(ReferenceSet.CAUTION_CODE.value(), "90");
    private static final CodeType LOCATION_PROPERTY_WHEELCHAIR = new CodeType(ReferenceSet.LOCATION_PROPERTY.value(), "WHEELCHAIR");
    private static final CodeType LOCATION_PROPERTY_HSENS = new CodeType(ReferenceSet.LOCATION_PROPERTY.value(), "HSENS");
    private static final CodeType LOCATION_PROPERTY_SHOWER = new CodeType(ReferenceSet.LOCATION_PROPERTY.value(), "SHOWER");
    private static final String CR_PRIORITY_LOW = "LOW";
    private static final String CR_PRIORITY_HIGH = "HIGH";
    private static final String CR_PRIORITY_MED = "MED";
    private static final String CR_TYPE_INTRA = "INTRA";
    private static final String MOVEMENT_REASON_MED = "MED";
    private static final String MOVEMENT_REASON_OFFREQ = "OFFREQ";
    private static final String MOVEMENT_REASON_BEH = "BEH";
    private static final String MOVEMENT_REASON_MISHNG = "MISHNG";
    private static final String MOVEMENT_REASON_ADM = "ADM";
    //Add some comments
    private static Logger log = LoggerFactory.getLogger(IT.class);
    protected Long supervisionId = 1L;
    protected Long locationRequested = 1L;
    protected Long locationTo = 1L;
    protected Long assignedBy = 1L;
    protected Long issuedBy = 1L;
    protected Long unassignedBy = 1L;
    protected Long facilityId = 1L;
    protected Long activityId = 1L;

    protected HousingService service;
    protected ReferenceDataService refService;

    protected StaffTest stfTest;
    protected ActivityTest actTest;
    protected FacilityTest facTest;
    protected SupervisionTest supTest;
    protected FacilityInternalLocationTest filTest;

    //for clean test data only
    protected PersonIdentityTest piTest;
    protected PersonTest psnTest;

    public IT(){

    }

    public IT(HousingService service, ReferenceDataService refService, StaffTest stfTest, ActivityTest actTest, FacilityTest facTest, SupervisionTest supTest,
            FacilityInternalLocationTest filTest, PersonIdentityTest piTest, PersonTest psnTest) {
        this.service = service;
        this.refService = refService;
        this.stfTest = stfTest;
        this.actTest = actTest;
        this.facTest = facTest;
        this.supTest = supTest;
        this.filTest = filTest;
        this.piTest = piTest;
        this.psnTest = psnTest;
    }

    @BeforeClass
    public void beforeClass() {

        log.info("beforeClass Begin - JNDI lookup");
        service = (HousingService) JNDILookUp(this.getClass(), HousingService.class);
        refService = (ReferenceDataService) JNDILookUp(this.getClass(), ReferenceDataService.class);
        uc = super.initUserContext();

        initData();

        stfTest = new StaffTest();
        actTest = new ActivityTest();
        facTest = new FacilityTest();
        supTest = new SupervisionTest();
        filTest = new FacilityInternalLocationTest();

        //cleanUp();

        //Initial other service data
        initOtherServiceData();

    }

    @AfterClass
    public void afterClass() {
        cleanUp();
    }

    private void cleanUp() {
        service.deleteAll(uc);

        filTest.deleteAll();
        supTest.deleteAll();
        actTest.deleteAll();
        stfTest.deleteAll();
        facTest.deleteAll();

        //for clean test data only
        piTest = new PersonIdentityTest();
        psnTest = new PersonTest();

        piTest.deleteAll();
        psnTest.deleteAll();
    }

    void initOtherServiceData() {

        //Setting staff Id		
        Long staffId = stfTest.createStaff();
        assignedBy = staffId;
        issuedBy = staffId;
        unassignedBy = staffId;

        //Setting activityId
        activityId = actTest.createActivityForHousing();

        //Setting facilityId
        facilityId = facTest.createFacility();

        //Setting supervisionId
        supervisionId = supTest.createSupervision(facilityId, true);

        //Setting facilityInternalLocation
        filTest.createFILTree(facilityId);
        locationRequested = filTest.getCell1();
        locationTo = filTest.getCell2();
    }

    @Test
    public void lookupJNDI() {
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "lookupJNDI" })
    public void reset() {

        Long ret = service.deleteAll(uc);
        assert (ret.equals(ReturnCode.Success.returnCode()));
    }

    @Test
    public void assignHousingLocationNegative() {

        //clean db and reload meta data
        reset();

        //DTO is null
        verifyAssignHousingLocationNegative(null, InvalidInputException.class);

        //DTO is null
        HousingLocationAssignType housingAssign = new HousingLocationAssignType();
        verifyAssignHousingLocationNegative(housingAssign, InvalidInputException.class);

        //supervisionId is null, will fail
        Date assignedDate = new Date();
        String movementComment = "assignHousingLocation comment";
        String movementReason = MOVEMENT_REASON_MED;
        housingAssign = new HousingLocationAssignType(activityId, null, movementReason, locationTo, assignedBy, assignedDate, null, OUTCOME_SUTBL, movementComment,
                facilityId);
        verifyAssignHousingLocationNegative(housingAssign, InvalidInputException.class);

        // build HousingAttributeMismatch for assignment, the mismatchSinceDate is null
        Set<CodeType> securityLocCodes = new HashSet<CodeType>();
        securityLocCodes.add(SECURITY_LEVEL_MIN);
        securityLocCodes.add(SECURITY_LEVEL_MAX);
        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, securityLocCodes, OUTCOME_SUTBL,
                activityId);

        Set<CodeType> genderLocCodes = new HashSet<CodeType>();
        genderLocCodes.add(OFFENDER_GENDER_F);
        genderLocCodes.add(OFFENDER_GENDER_M);
        HousingAttributeMismatchType mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_GENDER, CODE_SEX_F, genderLocCodes, OUTCOME_SUTBL, activityId);
        Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        assignmentMismatches.add(mismatch);
        assignmentMismatches.add(mismatch2);

        // build HousingAttributeMismatch for assignment for correct value
        mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, securityLocCodes, OUTCOME_SUTBL, activityId);
        mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_GENDER, CODE_SEX_F, genderLocCodes, OUTCOME_SUTBL, activityId);
        assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        assignmentMismatches.add(mismatch);
        assignmentMismatches.add(mismatch2);

        // Wrong offenderValue code
        mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, securityLocCodes, OUTCOME_SUTBL, activityId);

        Set<CodeType> ageRangeLocCodes = new HashSet<CodeType>();
        ageRangeLocCodes.add(AGE_RANGE_ADULT);
        ageRangeLocCodes.add(AGE_RANGE_JUVENILE);
        mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_AGE, AGE_RANGE_NUM, ageRangeLocCodes, OUTCOME_SUTBL, activityId);
        assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        assignmentMismatches.add(mismatch);
        assignmentMismatches.add(mismatch2);

        housingAssign = new HousingLocationAssignType(activityId, supervisionId, movementReason, locationTo, assignedBy, assignedDate, assignmentMismatches,
                OUTCOME_SUTBL, movementComment, facilityId);
        verifyAssignHousingLocationNegative(housingAssign, DataNotExistException.class);

        // Assinged to facility 1, will success.
        Long locationTo = filTest.getCell3();
        Long supervisionId = createSupervision();

        housingAssign = new HousingLocationAssignType(activityId, supervisionId, movementReason, locationTo, assignedBy, assignedDate, getAllMismatches(), OUTCOME_SUTBL,
                movementComment, facilityId);

        HousingBedMgmtActivityType ret = service.assignHousingLocation(uc, housingAssign);
        assert (ret != null);

    }

    @Test
    public void assignHousingLocationPositive() {

        //clean db and reload meta data
        reset();

        //Will success		
        Long locationTo = filTest.getCell3();
        Long supervisionId = createSupervision();

        String movementComment = "assignHousingLocation comment";
        String movementReason = MOVEMENT_REASON_MED;

        // build HousingAttributeMismatch for assignment		
        //Set<HousingAttributeMismatchType> assignmentMismatches = null;//getAllMismatches();	// test for CR966 persist code for Age offender value.
        Set<HousingAttributeMismatchType> assignmentMismatches = getAllMismatches();

        Date assignedDate = BeanHelper.getPastDate(new Date(), 1);

        HousingLocationAssignType housingAssign = new HousingLocationAssignType(activityId, supervisionId, movementReason, locationTo, assignedBy, assignedDate,
                assignmentMismatches, OUTCOME_SUTBL, movementComment, facilityId);

        HousingBedMgmtActivityType ret = service.assignHousingLocation(uc, housingAssign);
        assert (ret != null);

    }

    @Test
    public void unassignHousingLocationNegative() {

        //empty parameters, will fail.
        verifyUnassignHousingLocationNegative(null, InvalidInputException.class);

        //empty DTO, will fail
        HousingLocationUnassignType housingUnassign = new HousingLocationUnassignType();
        verifyUnassignHousingLocationNegative(housingUnassign, InvalidInputException.class);

        //supervisionId is null, will fail		
        Date unassignedDate = new Date();
        String movementComment = "unassignHousingLocation comments";
        String movementReason = MOVEMENT_REASON_MED;
        housingUnassign = new HousingLocationUnassignType(activityId, null, movementReason, unassignedBy, unassignedDate, movementComment);
        verifyUnassignHousingLocationNegative(housingUnassign, InvalidInputException.class);

    }

    @Test
    public void unassignHousingLocationPositive() {

        //clean db and reload meta data
        reset();

        HousingBedMgmtActivityType ret = assignHousingLocation(createSupervision());
        assert (ret != null);

        //this will success for housing unassign location		
        String movementComment = "unassignHousingLocationPositive comments";
        String movementReason = MOVEMENT_REASON_MED;

        // Update for defect 1019, reference facilityId from DTO
        Date unassignedDate = BeanHelper.nextNthDays(new Date(), 1);
        Long supervisionId = ret.getSupervisionId();
        HousingLocationUnassignType housingUnassign = new HousingLocationUnassignType(activityId, supervisionId, movementReason, unassignedBy, unassignedDate,
                movementComment);

        ret = service.unassignHousingLocation(uc, housingUnassign);
        assert (ret != null);

    }

    @Test
    public void createChangeRequestNegative() {

        //empty parameters, will fail.

        verifyCreateChangeRequestNegative(null, InvalidInputException.class);

        //empty DTO, will fail.
        HousingLocationCRType housingLocationCR = new HousingLocationCRType();
        verifyCreateChangeRequestNegative(housingLocationCR, InvalidInputException.class);

        // build HousingLocationCRType type
        String movementReason = MOVEMENT_REASON_MED;
        String crPriority = CR_PRIORITY_HIGH;
        String crType = CR_TYPE_INTRA;

        //build OffenderHousingAttributeType
        String securityLevel = ASSESSMENT_RESULT_MIN;
        String gender = SEX_F;

        String sentenceStatuses = CASE_SENTENCE_STATUS_SENT;

        Set<String> locationProperties = new HashSet<String>();
        locationProperties.add(HEALTH_ISSUES_D);
        locationProperties.add(HEALTH_ISSUES_DI);

        Set<String> categories = new HashSet<String>();
        categories.add(CAUTION_CODE_S);
        categories.add(CAUTION_CODE_V);

        OffenderHousingAttributeType housingAttributesRequired = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatuses, locationProperties,
                categories);

        housingLocationCR = new HousingLocationCRType(activityId, null, movementReason, locationRequested, issuedBy, crPriority, crType, true, housingAttributesRequired,
                null, null, "createChangeRequest comments");

        //supervisionId is null, will fail
        verifyCreateChangeRequestNegative(null, InvalidInputException.class);

        //Failed to create change request because the offender doesn't have housing assignment
        Long supervisionId = 500L;
        Long locationRequested = 100L;
        housingLocationCR = new HousingLocationCRType(activityId, supervisionId, movementReason, locationRequested, issuedBy, crPriority, crType, true,
                housingAttributesRequired, null, null, "createChangeRequest comments");
        verifyCreateChangeRequestNegative(null, InvalidInputException.class);

    }

    @Test
    public void createChangeRequestPositive() {

        //clean db and reload meta data
        reset();

        String movementReason = MOVEMENT_REASON_MED;
        String crPriority = CR_PRIORITY_HIGH;
        String crType = CR_TYPE_INTRA;

        //build OffenderHousingAttributeType
        String securityLevel = ASSESSMENT_RESULT_MIN;
        String gender = SEX_F;

        String sentenceStatuses = CASE_SENTENCE_STATUS_SENT;

        Set<String> locationProperties = new HashSet<String>();
        locationProperties.add(HEALTH_ISSUES_D);
        locationProperties.add(HEALTH_ISSUES_DI);

        Set<String> categories = new HashSet<String>();
        categories.add(CAUTION_CODE_S);
        categories.add(CAUTION_CODE_V);

        OffenderHousingAttributeType housingAttributesRequired = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatuses, locationProperties,
                categories);

        // Assign the offender 3 to location 2
        Long supervisionId = createSupervision();
        assignHousingLocation(supervisionId);

        //this will success for housing change request
        Long locationRequested = filTest.getCell1();
        HousingLocationCRType housingLocationCR = new HousingLocationCRType(activityId, supervisionId, movementReason, locationRequested, issuedBy, crPriority, crType,
                true, housingAttributesRequired, getMismatches(), OUTCOME_WRNG, "createChangeRequest comments");
        HousingBedMgmtActivityType ret = service.createChangeRequest(uc, housingLocationCR);
        assert (ret != null);

    }

    @Test
    public void updateNegative() {

        String crPriority = CR_PRIORITY_LOW;
        locationRequested = filTest.getCell1();
        String movementComment = "Movement by update";

        // housingActivityId is null, will fail
        verifyUpdateNegative(getHousingUpdateType(null, crPriority, locationRequested, movementComment), InvalidInputException.class);

        // At least provide one parameter of crPriority, locationTo and movementComment
        verifyUpdateNegative(getHousingUpdateType(-1L, null, null, null), InvalidInputException.class);

        // wrong housingActivityId, will fail
        verifyUpdateNegative(getHousingUpdateType(-1L, null, null, movementComment), DataNotExistException.class);

        //Create change request for supervisionId 4, //Approval Not Required
        Long supervisionId = createSupervision();
        HousingBedMgmtActivityType retCreate = createChangeRequest(supervisionId, MOVEMENT_REASON_MED, true);

        // Will fail because the activitySate is not CR Approval Required.
        Long housingActivityId = retCreate.getHousingActivityId();

        verifyUpdateNegative(getHousingUpdateType(housingActivityId, crPriority, locationRequested, movementComment), InvalidInputException.class);

    }

    @Test
    public void updatePositive() {

        //clean db and reload meta data
        reset();

        //Create change request for supervisionId 4, //Approval Not Required
        Long supId = createSupervision();
        HousingBedMgmtActivityType retCreate = createChangeRequest(supId, MOVEMENT_REASON_MED, true);

        // Will fail because the activitySate is not CR Approval Required.
        Long housingActivityId = retCreate.getHousingActivityId();
        String crPriority = CR_PRIORITY_MED;
        locationRequested = locationTo;
        String movementComment = "Movement by update";

        HousingUpdateType housingUpdateType = getHousingUpdateType(housingActivityId, crPriority, null, movementComment);
        housingUpdateType.setHousingActivityVersion(retCreate.getVersion());
        housingUpdateType.setHousingChangeRequestVersion(retCreate.getHousingChangeRequest().getVersion());
        // Will success, not pass the location To
        HousingBedMgmtActivityType ret = service.update(uc, housingUpdateType);
        assert (ret != null);

    }

    @Test
    public void updateHousingMismatchesNegative() {

        //housingActivityId is null, will fail.
        Long housingActivityId = null;
        Set<HousingAttributeMismatchType> housingAttributeMismatches = null;
        String outcome = OUTCOME_SUTBL;
        verifyUpdateHousingMismatchesNegative(housingActivityId, housingAttributeMismatches, outcome, InvalidInputException.class);

        //Test for defect 1033
        //housingAttributeMismatches is empty, will pass to realization, get 1003 because Cannot find the housingActivityId
        housingActivityId = -1L;
        housingAttributeMismatches = new HashSet<HousingAttributeMismatchType>();
        verifyUpdateHousingMismatchesNegative(housingActivityId, housingAttributeMismatches, outcome, DataNotExistException.class);

        //Empty HousingAttributeMismatchType, DTO validation failure.
        housingAttributeMismatches = new HashSet<HousingAttributeMismatchType>();
        housingAttributeMismatches.add(new HousingAttributeMismatchType());
        verifyUpdateHousingMismatchesNegative(housingActivityId, housingAttributeMismatches, outcome, InvalidInputException.class);

        // build HousingAttributeMismatch, mismatchSinceDate is null
        Set<CodeType> securityLocCodes = new HashSet<CodeType>();
        securityLocCodes.add(SECURITY_LEVEL_MIN);
        securityLocCodes.add(SECURITY_LEVEL_MAX);
        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, securityLocCodes, OUTCOME_SUTBL,
                activityId);

        Set<CodeType> genderLocCodes = new HashSet<CodeType>();
        genderLocCodes.add(OFFENDER_GENDER_F);
        genderLocCodes.add(OFFENDER_GENDER_M);
        HousingAttributeMismatchType mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_GENDER, CODE_SEX_F, genderLocCodes, OUTCOME_SUTBL, activityId);
        Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        assignmentMismatches.add(mismatch);
        assignmentMismatches.add(mismatch2);

        // Test for defect 1022
        // fail because mismatchSinceDate is null
        verifyUpdateHousingMismatchesNegative(housingActivityId, housingAttributeMismatches, outcome, InvalidInputException.class);

        // build HousingAttributeMismatch, value are correct
        housingAttributeMismatches = getMismatches();

        //Cannot find the housingActivityId, will fail
        verifyUpdateHousingMismatchesNegative(housingActivityId, housingAttributeMismatches, outcome, DataNotExistException.class);

    }

    @Test
    public void updateHousingMismatchesPositive() {

        //clean db and reload meta data
        reset();

        //ReservationFlag is true
        //Create change request for supervisionId 5, //Approval Not Required
        Long supervisionId = createSupervision();
        HousingBedMgmtActivityType retCreate = createChangeRequest(supervisionId, MOVEMENT_REASON_MED, true);

        // build HousingAttributeMismatch
        Set<HousingAttributeMismatchType> housingAttributeMismatches = getAllMismatches();

        //Update mismatch for change request
        Long housingActivityId = retCreate.getHousingActivityId();
        HousingBedMgmtActivityType ret = service.updateHousingMismatches(uc, housingActivityId, housingAttributeMismatches, OUTCOME_SUTBL);

        assert (ret != null);

        //Create change request for supervisionId 6, //Approval Required
        Long supId = createSupervision();
        retCreate = createChangeRequest(supId, MOVEMENT_REASON_OFFREQ, true);

        //Update mismatch for change request
        housingActivityId = retCreate.getHousingActivityId();
        ret = service.updateHousingMismatches(uc, housingActivityId, housingAttributeMismatches, OUTCOME_SUTBL);

        assert (ret != null);

        //Test for defect 1033
        // update mismatch to empty
        Set<HousingAttributeMismatchType> housingAttributeMismatches2 = new HashSet<HousingAttributeMismatchType>();
        ret = service.updateHousingMismatches(uc, housingActivityId, housingAttributeMismatches2, OUTCOME_SUTBL);
        assert (ret != null);

        //Create housing assignment for supervisionId 7
        supervisionId = createSupervision();
        retCreate = assignHousingLocation(supervisionId);

        //Update mismatch for assignment
        housingActivityId = retCreate.getHousingActivityId();
        ret = service.updateHousingMismatches(uc, housingActivityId, housingAttributeMismatches, OUTCOME_SUTBL);
        assert (ret != null);

        //Create change request for supervisionId 8, //Approval Required
        supervisionId = createSupervision();
        retCreate = createChangeRequest(supervisionId, MOVEMENT_REASON_OFFREQ, false);

        //Update mismatch for change request
        housingAttributeMismatches = getMismatches();
        housingActivityId = retCreate.getHousingActivityId();
        ret = service.updateHousingMismatches(uc, housingActivityId, housingAttributeMismatches, OUTCOME_SUTBL);
        assert (ret != null);

        //Create housing assignment for supervisionId 8
        supervisionId = createSupervision();
        retCreate = assignHousingLocation(supervisionId);

        //Test for defect 1033
        //Update mismatch to empty
        housingActivityId = retCreate.getHousingActivityId();
        ret = service.updateHousingMismatches(uc, housingActivityId, housingAttributeMismatches2, OUTCOME_SUTBL);
        assert (ret != null);
    }

    @Test
    public void approveChangeRequestNegative() {

        reset();

        //housingActivityId is null, will fail.
        Long housingActivityId = null;
        HousingChangeRequestApprovalType housingCRApproval = null;
        verifyApproveChangeRequestNegative(getApproveChangeRequestType(housingActivityId, housingCRApproval), InvalidInputException.class);

        //HousingChangeRequestApprovalType is null, will fail.
        housingActivityId = -1L;
        housingCRApproval = new HousingChangeRequestApprovalType();
        verifyApproveChangeRequestNegative(getApproveChangeRequestType(housingActivityId, housingCRApproval), InvalidInputException.class);

        //HousingChangeRequestApprovalType dto validation failure.
        housingCRApproval = new HousingChangeRequestApprovalType(null, null, "Approval Comments");
        verifyApproveChangeRequestNegative(getApproveChangeRequestType(housingActivityId, housingCRApproval), InvalidInputException.class);

        //Cannot find the housingActivityId, will fail
        housingCRApproval = new HousingChangeRequestApprovalType(issuedBy, null, "Approval Comments");
        verifyApproveChangeRequestNegative(getApproveChangeRequestType(housingActivityId, housingCRApproval), DataNotExistException.class);

        //Create change request with Approval Not Required
        Long supervisionId = createSupervision();
        HousingBedMgmtActivityType retCreate = createChangeRequest(supervisionId, MOVEMENT_REASON_MED, true);

        //ActivityState is not Approval Required, will fail.
        housingActivityId = retCreate.getHousingActivityId();
        verifyApproveChangeRequestNegative(getApproveChangeRequestType(housingActivityId, housingCRApproval), InvalidInputException.class);

    }

    @Test
    public void approveChangeRequestPositive() {

        //clean db and reload meta data
        reset();

        //Create change request with Approval Required
        Long supervisionId = createSupervision();
        HousingBedMgmtActivityType retCreate = createChangeRequest(supervisionId, MOVEMENT_REASON_OFFREQ, true);

        //ActivityState is Approval Required, will Success.
        Long housingActivityId = retCreate.getHousingActivityId();

        HousingChangeRequestApprovalType housingCRApproval = new HousingChangeRequestApprovalType(issuedBy, null, "Approval Comments");

        ApproveChangeRequestType dtoApp = getApproveChangeRequestType(housingActivityId, housingCRApproval);
        dtoApp.setHousingActivityVersion(retCreate.getVersion());
        dtoApp.setHousingChangeRequestVersion(retCreate.getHousingChangeRequest().getVersion());
        HousingBedMgmtActivityType ret = service.approveChangeRequest(uc, dtoApp);

        assert (ret != null);

    }

    @Test
    public void cancelChangeRequestNegative() {

        reset();

        //housingActivityId is null, will fail.
        Long housingActivityId = null;
        HousingActivityClosureType housingActivityClosure = null;
        verifyCancelChangeRequestNegative(getCancelChangeRequestType(housingActivityId, housingActivityClosure), InvalidInputException.class);

        //HousingActivityClosureType is null, will fail.
        housingActivityId = -1L;
        housingActivityClosure = new HousingActivityClosureType();
        verifyCancelChangeRequestNegative(getCancelChangeRequestType(housingActivityId, housingActivityClosure), InvalidInputException.class);

        //HousingActivityClosureType dto validation failure.
        housingActivityClosure = new HousingActivityClosureType(null, null, "Closed Comments");
        verifyCancelChangeRequestNegative(getCancelChangeRequestType(housingActivityId, housingActivityClosure), InvalidInputException.class);

        //Cannot find the housingActivityId, will fail
        housingActivityClosure = new HousingActivityClosureType(2L, null, "Closed Comments");
        verifyCancelChangeRequestNegative(getCancelChangeRequestType(housingActivityId, housingActivityClosure), DataNotExistException.class);

        //Create housing assignment activity
        Long supervisionId = createSupervision();
        HousingBedMgmtActivityType retCreate = assignHousingLocation(supervisionId);

        //Not housing change request activity, will fail
        housingActivityId = retCreate.getHousingActivityId();
        verifyCancelChangeRequestNegative(getCancelChangeRequestType(housingActivityId, housingActivityClosure), InvalidInputException.class);

    }

    @Test
    public void cancelChangeRequestPositive() {

        //clean db and reload meta data
        reset();

        //Create change request with Approval Required
        Long supervisionId = createSupervision();
        HousingBedMgmtActivityType retCreate = createChangeRequest(supervisionId, MOVEMENT_REASON_OFFREQ, true);
        Long housingActivityId = retCreate.getHousingActivityId();

        HousingActivityClosureType housingActivityClosure = new HousingActivityClosureType(issuedBy, null, "Closed Comments");

        CancelChangeRequestType dto = getCancelChangeRequestType(housingActivityId, housingActivityClosure);
        dto.setHousingActivityVersion(retCreate.getVersion());
        dto.setHousingChangeRequestVersion(retCreate.getHousingChangeRequest().getVersion());

        HousingBedMgmtActivityType ret = service.cancelChangeRequest(uc, dto);

        assert (ret != null);

    }

    @Test
    public void validteMismatchNegative() {

        //clean db and reload meta data
        reset();

        //housingActivityId is null, will fail.
        Long housingActivityId = null;
        String movementComment = "validateMismatch comments";
        verifyValidateMismatchNegative(getHousingUpdateType(housingActivityId, movementComment), InvalidInputException.class);

        //Cannot find the housingActivityId, will fail
        housingActivityId = -1L;
        verifyValidateMismatchNegative(getHousingUpdateType(housingActivityId, movementComment), DataNotExistException.class);

        //Create change request for Approval Not Required
        Long supervisionId = createSupervision();
        HousingBedMgmtActivityType retCreate = createChangeRequest(supervisionId, MOVEMENT_REASON_MED, true);
        housingActivityId = retCreate.getHousingActivityId();

        // build HousingAttributeMismatch with Not Permitted
        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, getSecurityLevelCodes(),
                OUTCOME_SUTBL, activityId);
        HousingAttributeMismatchType mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_GENDER, CODE_SEX_F, getGenderCodes(), OUTCOME_NOTPD, activityId);
        Set<HousingAttributeMismatchType> housingAttributeMismatches = new HashSet<HousingAttributeMismatchType>();
        housingAttributeMismatches.add(mismatch);
        housingAttributeMismatches.add(mismatch2);

        //Update mismatch for change request.
        HousingBedMgmtActivityType ret = service.updateHousingMismatches(uc, housingActivityId, housingAttributeMismatches, OUTCOME_SUTBL);
        assert (ret.getHousingChangeRequest().getChangeRequestMismatches() != null);

        //HousingAttributeMismatch with Not Permitted, will fail.
        verifyValidateMismatchNegative(getHousingUpdateType(housingActivityId, movementComment), InvalidInputException.class);

        //Update mismatch for change request.
        housingAttributeMismatches = getAllMismatches();
        ret = service.updateHousingMismatches(uc, housingActivityId, housingAttributeMismatches, OUTCOME_NOTPD);

        assert (ret.getHousingChangeRequest().getChangeRequestMismatches() != null);

        //HousingAttributeMismatch with Not Permitted, will fail.
        verifyValidateMismatchNegative(getHousingUpdateType(housingActivityId, movementComment), InvalidInputException.class);

    }

    @Test
    public void validateMismatchPositive() {

        //clean db and reload meta data
        reset();

        //Create change request for Approval Required

        Long supervisionId = createSupervision();
        HousingBedMgmtActivityType retCreate = createChangeRequest(supervisionId, MOVEMENT_REASON_OFFREQ, true);

        String movementComment = "validateMismatch comments";
        Long housingActivityId = retCreate.getHousingActivityId();
        HousingUpdateType housingUpdateType = getHousingUpdateType(housingActivityId, movementComment);
        housingUpdateType.setHousingActivityVersion(retCreate.getVersion());
        housingUpdateType.setHousingChangeRequestVersion(retCreate.getHousingChangeRequest().getVersion());
        HousingBedMgmtActivityType ret = service.validateMismatch(uc, housingUpdateType);

        assert (ret != null);

        // build HousingAttributeMismatch with Non - Not Permitted
        Set<HousingAttributeMismatchType> housingAttributeMismatches = getMismatches();
        ret = service.updateHousingMismatches(uc, housingActivityId, housingAttributeMismatches, OUTCOME_SUTBL);

        assert (ret.getHousingChangeRequest().getChangeRequestMismatches() != null);

        //HousingAttributeMismatch with Non - Not Permitted, will success.
        housingUpdateType = getHousingUpdateType(housingActivityId, movementComment);
        housingUpdateType.setHousingActivityVersion(ret.getVersion());
        housingUpdateType.setHousingChangeRequestVersion(ret.getHousingChangeRequest().getVersion());
        ret = service.validateMismatch(uc, housingUpdateType);

        assert (ret != null);

    }

    @Test
    public void applyChangeRequestNegative() {

        //clean db and reload meta data
        //reset();

        Long housingActivityId = -1L;
        Long assignedBy = 1L;
        Long locationTo = 3L;
        Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();

        //housingActivityId is null, will fail.

        Date assignedDate = new Date();
        verifyApplyChangeRequestNegative(null, assignedBy, locationTo, assignmentMismatches, OUTCOME_SUTBL, assignedDate, InvalidInputException.class);

        //assignedBy is null, will fail.
        verifyApplyChangeRequestNegative(housingActivityId, null, locationTo, assignmentMismatches, OUTCOME_SUTBL, assignedDate, InvalidInputException.class);

        //HousingAttributeMismatchType DTO validation failure.
        locationTo = 1L;
        assignmentMismatches.add(new HousingAttributeMismatchType());
        verifyApplyChangeRequestNegative(housingActivityId, assignedBy, locationTo, assignmentMismatches, OUTCOME_SUTBL, assignedDate, InvalidInputException.class);

        // Test for defect 1022
        //HousingAttributeMismatchType DTO validation failure, because mismatchSinceDate is null
        assignmentMismatches.add(
                new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, getSecurityLevelCodes(), OUTCOME_SUTBL, activityId));
        verifyApplyChangeRequestNegative(housingActivityId, assignedBy, locationTo, assignmentMismatches, OUTCOME_SUTBL, assignedDate, InvalidInputException.class);

        // build HousingAttributeMismatch with Non - Not Permitted
        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, getSecurityLevelCodes(),
                OUTCOME_SUTBL, activityId);
        HousingAttributeMismatchType mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_GENDER, CODE_SEX_F, getGenderCodes(), OUTCOME_SUTBL, activityId);
        assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        assignmentMismatches.add(mismatch);
        assignmentMismatches.add(mismatch2);

        //Cannot find the housingActivityId, will fail
        verifyApplyChangeRequestNegative(housingActivityId, assignedBy, locationTo, assignmentMismatches, OUTCOME_SUTBL, assignedDate, DataNotExistException.class);

    }

    @Test
    public void applyChangeRequestPositive() {

        //clean db and reload meta data
        reset();

        //Create change request for Approval Required
        Long supervisionId = createSupervision();
        HousingBedMgmtActivityType retCreate = createChangeRequest(supervisionId, MOVEMENT_REASON_OFFREQ, true);
        Long housingActivityId = retCreate.getHousingActivityId();

        Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        // build HousingAttributeMismatch with Non - Not Permitted
        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, getSecurityLevelCodes(),
                OUTCOME_SUTBL, activityId);
        HousingAttributeMismatchType mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_GENDER, CODE_SEX_F, getGenderCodes(), OUTCOME_SUTBL, activityId);
        assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        assignmentMismatches.add(mismatch);
        assignmentMismatches.add(mismatch2);

        //ActivityState is not On Waitlist, will fail
        Date assignedDate = BeanHelper.nextNthDays(new Date(), 1);
        verifyApplyChangeRequestNegative(housingActivityId, assignedBy, locationTo, assignmentMismatches, OUTCOME_SUTBL, assignedDate, InvalidInputException.class);

        //Create change request for Approval Not Required
        Long supId = createSupervision();
        retCreate = createChangeRequest(supId, MOVEMENT_REASON_MED, false);
        housingActivityId = retCreate.getHousingActivityId();

        //isReservationFlag is false, the locationTo must be inputed. will fail
        //ActivityState is not On Waitlist, will fail
        verifyApplyChangeRequestNegative(housingActivityId, assignedBy, null, assignmentMismatches, OUTCOME_SUTBL, assignedDate, InvalidInputException.class);

        //update the mismatch, make isNeedValidationFlag is true.
        HousingBedMgmtActivityType ret = service.updateHousingMismatches(uc, housingActivityId, assignmentMismatches, OUTCOME_SUTBL);
        assert (ret != null);

        //NeedsValidationFlag is true, will fail.
        verifyApplyChangeRequestNegative(housingActivityId, assignedBy, locationTo, assignmentMismatches, OUTCOME_SUTBL, assignedDate, InvalidInputException.class);

        //remove mismatch
        HousingUpdateType housingUpdateType = getHousingUpdateType(housingActivityId, "validateMismatch comments");
        housingUpdateType.setHousingActivityVersion(ret.getVersion());
        housingUpdateType.setHousingChangeRequestVersion(ret.getHousingChangeRequest().getVersion());
        ret = service.validateMismatch(uc, housingUpdateType);

        //this will success.
        Long locId = filTest.getCell2();
        ret = service.get(uc, housingActivityId);
        ret = service.applyChangeRequest(uc, housingActivityId, assignedBy, locId, assignmentMismatches, OUTCOME_SUTBL, assignedDate, ret.getVersion());

        assert (ret != null);

    }

    //@Test
    public List<Long> searchCreation() {

        //clean db and reload meta data
        reset();

        Long toIdentifer = null;

        List<Long> leafLoations = filTest.getLeafIds();

        Date assignedDate = new Date();
        List<Long> supIds = new ArrayList<Long>();

        // Create housing assigned activity
        for (int i = 1; i <= 5; i++) {

            toIdentifer = Long.valueOf(i);

            // build housing bed mgmt activity properties		
            Long supervisionId = createSupervision();
            supIds.add(supervisionId);
            String movementReason = MOVEMENT_REASON_BEH;

            // build housing assignment properties
            Long locationTo = leafLoations.get(i - 1);

            // build HousingAttributeMismatch for assignment
            HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, getSecurityLevelCodes(),
                    OUTCOME_SUTBL, activityId);
            HousingAttributeMismatchType mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_GENDER, CODE_SEX_F, getGenderCodes(), OUTCOME_SUTBL, activityId);

            Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
            assignmentMismatches.add(mismatch);
            assignmentMismatches.add(mismatch2);

            /////////////////////////////////////////////////////////////////

            String movementComment = "Housing Assinged comments" + toIdentifer;
            HousingLocationAssignType housingAssign = new HousingLocationAssignType(activityId, supervisionId, movementReason, locationTo, assignedBy, assignedDate,
                    assignmentMismatches, OUTCOME_SUTBL, movementComment, facilityId);

            HousingBedMgmtActivityType ret = service.assignHousingLocation(uc, housingAssign);

            assert (ret != null);

        }

        // Create housing assigned activity with new locationTo
        for (int i = 1; i <= 5; i++) {

            toIdentifer = Long.valueOf(i);

            // build housing bed mgmt activity properties		
            //Long supervisionId = toIdentifer;		
            Long supervisionId = supIds.get(i - 1);
            String movementReason = MOVEMENT_REASON_MISHNG;

            // build housing assignment properties
            //Long locationTo = toIdentifer+1;
            Long locationTo = leafLoations.get(i);

            // build HousingAttributeMismatch for assignment
            HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, getSecurityLevelCodes(),
                    OUTCOME_SUTBL, activityId);
            HousingAttributeMismatchType mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_GENDER, CODE_SEX_F, getGenderCodes(), OUTCOME_SUTBL, activityId);

            Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
            assignmentMismatches.add(mismatch);
            assignmentMismatches.add(mismatch2);

            String movementComment = "Housing Assinged comments" + toIdentifer;
            HousingLocationAssignType housingAssign = new HousingLocationAssignType(activityId, supervisionId, movementReason, locationTo, assignedBy, assignedDate,
                    assignmentMismatches, OUTCOME_SUTBL, movementComment, facilityId);

            HousingBedMgmtActivityType ret = service.assignHousingLocation(uc, housingAssign);

            assert (ret != null);

        }

        // Create housing unassigned activity
        for (int i = 1; i <= 5; i++) {

            toIdentifer = Long.valueOf(i);

            String movementComment = "Housing unassinged comments" + toIdentifer;

            //Long supervisionId = toIdentifer;
            Long supervisionId = supIds.get(i - 1);
            String movementReason = MOVEMENT_REASON_ADM;

            Date unassignedDate = new Date();
            HousingLocationUnassignType housingUnassign = new HousingLocationUnassignType(activityId, supervisionId, movementReason, unassignedBy, unassignedDate,
                    movementComment);

            HousingBedMgmtActivityType ret = service.unassignHousingLocation(uc, housingUnassign);

            assert (ret != null);

        }

        // Create housing change request activity - Waitlist
        for (int i = 1; i <= 5; i++) {

            toIdentifer = Long.valueOf(i + 10);

            Long supervisionId = createSupervision();
            supIds.add(supervisionId);
            //assignHousingLocation(toIdentifer, toIdentifer + 100);
            assignHousingLocation(supervisionId, leafLoations.get(i + 1));

            //Long supervisionId = toIdentifer;	

            String movementReason = MOVEMENT_REASON_MED;    //Approval Not Required

            // build housing change request properties
            Long locationRequested = leafLoations.get(i + 2);

            String crPriority = CR_PRIORITY_HIGH;
            String crType = CR_TYPE_INTRA;

            // build OffenderHousingAttribute
            String securityLevel = ASSESSMENT_RESULT_MIN;
            String gender = SEX_F;

            String sentenceStatuses = CASE_SENTENCE_STATUS_UNSENT;

            Set<String> locationProperties = new HashSet<String>();
            locationProperties.add(HEALTH_ISSUES_D);
            locationProperties.add(HEALTH_ISSUES_DI);

            Set<String> categories = new HashSet<String>();
            categories.add(CAUTION_CODE_S);
            categories.add(CAUTION_CODE_V);

            OffenderHousingAttributeType housingAttributesRequired = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatuses, locationProperties,
                    categories);

            String movementComment = "ChangeRquest-Waitlist comments" + toIdentifer;

            HousingLocationCRType housingLocationCR = new HousingLocationCRType(activityId, supervisionId, movementReason, locationRequested, issuedBy, crPriority,
                    crType, true, housingAttributesRequired, null, null, movementComment);

            HousingBedMgmtActivityType ret = service.createChangeRequest(uc, housingLocationCR);

            assert (ret != null);

        }

        // Create housing change request activity - Approval required
        for (int i = 1; i <= 5; i++) {

            toIdentifer = Long.valueOf(i + 15);

            Long supervisionId = createSupervision();
            supIds.add(supervisionId);
            assignHousingLocation(supervisionId, leafLoations.get(i));

            //Long supervisionId = toIdentifer;	

            String movementReason = MOVEMENT_REASON_OFFREQ;    //Approval Required

            // build housing change request properties
            Long locationRequested = leafLoations.get(i + 1);
            String crPriority = CR_PRIORITY_HIGH;
            String crType = CR_TYPE_INTRA;

            // build OffenderHousingAttribute
            String securityLevel = ASSESSMENT_RESULT_MIN;
            String gender = SEX_F;

            String sentenceStatuses = CASE_SENTENCE_STATUS_SENT;

            Set<String> locationProperties = new HashSet<String>();
            locationProperties.add(HEALTH_ISSUES_D);
            locationProperties.add(HEALTH_ISSUES_DI);

            Set<String> categories = new HashSet<String>();
            categories.add(CAUTION_CODE_S);
            categories.add(CAUTION_CODE_V);

            OffenderHousingAttributeType housingAttributesRequired = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatuses, locationProperties,
                    categories);

            String movementComment = "ChangeRquest-Approval Required comments" + toIdentifer;

            HousingLocationCRType housingLocationCR = new HousingLocationCRType(activityId, supervisionId, movementReason, locationRequested, issuedBy, crPriority,
                    crType, false, housingAttributesRequired, null, null, movementComment);

            HousingBedMgmtActivityType retCreate = service.createChangeRequest(uc, housingLocationCR);
            assert (retCreate != null);

            //Approval for change request  			
            Long housingActivityId = retCreate.getHousingActivityId();
            HousingChangeRequestApprovalType housingCRApproval = new HousingChangeRequestApprovalType(issuedBy, null, "approveChangeRequest comment" + toIdentifer);

            ApproveChangeRequestType dtoApp = getApproveChangeRequestType(housingActivityId, housingCRApproval);
            dtoApp.setHousingActivityVersion(retCreate.getVersion());
            dtoApp.setHousingChangeRequestVersion(retCreate.getHousingChangeRequest().getVersion());
            HousingBedMgmtActivityType ret = service.approveChangeRequest(uc, dtoApp);

            assert (ret != null);

            //Cancel the change request  			
            housingActivityId = retCreate.getHousingActivityId();
            HousingActivityClosureType activityClosure = new HousingActivityClosureType(issuedBy, null, "cancelChangeRequest comment" + toIdentifer);
            ret = service.get(uc, housingActivityId);
            CancelChangeRequestType dto = getCancelChangeRequestType(housingActivityId, activityClosure);
            dto.setHousingActivityVersion(ret.getVersion());
            dto.setHousingChangeRequestVersion(ret.getHousingChangeRequest().getVersion());
            ret = service.cancelChangeRequest(uc, dto);

            assert (ret != null);
        }

        return supIds;

    }

    @Test
    public void searchNegative() {

        /////////////////////////////////////////////////////////////////
        //Search for failure
        /////////////////////////////////////////////////////////////////

        //the HousingBedMgmtActivitySearchType is null, this will fail.
        verifySearchNegative(null, null, InvalidInputException.class);

        //the HousingBedMgmtActivitySearchType is empty, this will fail.
        HousingBedMgmtActivitySearchType search = new HousingBedMgmtActivitySearchType();
        verifySearchNegative(null, search, InvalidInputException.class);

        //Empty movementReason, this will fail.
        search = new HousingBedMgmtActivitySearchType();
        search.setMovementReason("");
        verifySearchNegative(null, search, InvalidInputException.class);

    }

    @Test
    public void searchPositive() {

        //create search data
        List<Long> supIds = searchCreation();
        List<Long> locIds = filTest.getLeafIds();

        /////////////////////////////////////////////////////////////////
        //Search by single condition
        /////////////////////////////////////////////////////////////////

        //search by supervisionId only
        Long supervisionId = supIds.get(0);
        HousingBedMgmtActivitySearchType search = new HousingBedMgmtActivitySearchType();
        search.setSupervisionId(supervisionId);
        HousingBedMgmtActivitiesReturnType ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 3L);

        //search by facility Reference only
        //Long facilityId = 1L;
        search = new HousingBedMgmtActivitySearchType();
        search.setFacilityId(facilityId);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 35L);

        //search by ActivityState only
        search = new HousingBedMgmtActivitySearchType();
        search.setActivityState(ACTIVITY_STATE_ASSGN);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 20L);

        //search by movementReason only
        search = new HousingBedMgmtActivitySearchType();
        search.setMovementReason(MOVEMENT_REASON_ADM);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 5L);

        //search by locationFrom only
        Long locationFrom = locIds.get(1);
        search = new HousingBedMgmtActivitySearchType();
        search.setLocationFrom(locationFrom);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 1L);

        //search by locationTo only
        Long locationTo = locIds.get(1);
        search = new HousingBedMgmtActivitySearchType();
        search.setLocationTo(locationTo);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 3L);

        //search by assignedDateFrom only - future day.
        Date assignedDateFrom = BeanHelper.nextNthDays(new Date(), 1);
        search = new HousingBedMgmtActivitySearchType();
        search.setAssignedDateFrom(assignedDateFrom);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 0L);

        //search by assignedDateFrom only - today.
        assignedDateFrom = BeanHelper.createDate();
        search = new HousingBedMgmtActivitySearchType();
        search.setAssignedDateFrom(assignedDateFrom);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 0L);

        //search by assignedDateTo only - past day.
        Date assignedDateTo = BeanHelper.getPastDate(new Date(), 1);
        search = new HousingBedMgmtActivitySearchType();
        search.setAssignedDateTo(assignedDateTo);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 0L);

        //search by assignedDateTo only - future day
        assignedDateTo = BeanHelper.nextNthDays(new Date(), 1);
        search = new HousingBedMgmtActivitySearchType();
        search.setAssignedDateTo(assignedDateTo);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 20L);

        //search by assignedBy only
        //Long assignedBy = issuedBy;
        search = new HousingBedMgmtActivitySearchType();
        search.setAssignedBy(assignedBy);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 20L);

        //search by unassignedBy only
        //Long unassignedBy = 1L;
        search = new HousingBedMgmtActivitySearchType();
        search.setUnassignedBy(unassignedBy);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 5L);

        //search by unassignedDateFrom only - future day.
        Date unassignedDateFrom = BeanHelper.nextNthDays(new Date(), 1);
        search = new HousingBedMgmtActivitySearchType();
        search.setUnassignedDateFrom(unassignedDateFrom);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 0L);

        //search by unassignedDateFrom only - today.
        unassignedDateFrom = BeanHelper.createDate();
        search = new HousingBedMgmtActivitySearchType();
        search.setUnassignedDateFrom(unassignedDateFrom);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 0L);

        //search by unassignedDateFrom only - past date.
        unassignedDateFrom = BeanHelper.getPastDate(new Date(), 1);
        search = new HousingBedMgmtActivitySearchType();
        search.setUnassignedDateFrom(unassignedDateFrom);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 5L);

        //search by unassignedDateTo only - past day.
        Date unassignedDateTo = BeanHelper.getPastDate(new Date(), 1);
        search = new HousingBedMgmtActivitySearchType();
        search.setUnassignedDateTo(unassignedDateTo);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 0L);

        //search by assignedDateTo only - today.
        unassignedDateTo = BeanHelper.createDate();
        search = new HousingBedMgmtActivitySearchType();
        search.setUnassignedDateTo(unassignedDateTo);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 5L);

        //search by ActiveFlag only
        search = new HousingBedMgmtActivitySearchType();
        search.setActiveFlag(true);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 5L);

        //search by ActiveFlag only
        search = new HousingBedMgmtActivitySearchType();
        search.setActiveFlag(false);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 30L);

        //search by movementComment only
        String movementComment = "Housing unassinged comments1";
        search = new HousingBedMgmtActivitySearchType();
        search.setMovementComment(movementComment);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 1L);

        //search by movementComment only
        movementComment = "unassinged";
        search = new HousingBedMgmtActivitySearchType();
        search.setMovementComment(movementComment);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 0L);

        //search by movementComment only - partial
        movementComment = "unassinged*";
        search = new HousingBedMgmtActivitySearchType();
        search.setMovementComment(movementComment);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 5L);

        /////////////////////////////////////////////////////////////////
        //Multiple condition search 
        /////////////////////////////////////////////////////////////////

        //search by supervisionId, facilityId and activityState 
        //supervisionId = supIds.get(0);
        String activityState = ACTIVITY_STATE_ASSGN;
        search = new HousingBedMgmtActivitySearchType(supervisionId, facilityId, activityState, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 2L);

        //search by supervisionId, activityState and movementReason 
        String movementReason = MOVEMENT_REASON_MISHNG;
        search = new HousingBedMgmtActivitySearchType(supervisionId, null, activityState, movementReason, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 1L);

        //search by supervisionId, null, activityState, movementReason and movementComment
        //partial search
        movementComment = "Assinged*";
        search = new HousingBedMgmtActivitySearchType(supervisionId, null, activityState, movementReason, movementComment, null, null, null, null, null, null, null, null,
                null);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 1L);

        //search by supervisionId, null, activityState, movementReason and movementComment
        movementComment = "Housing Assinged comments1";
        search = new HousingBedMgmtActivitySearchType(supervisionId, null, activityState, movementReason, movementComment, null, null, null, null, null, null, null, null,
                null);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment,
        //locationFrom
        locationFrom = locIds.get(0);
        search = new HousingBedMgmtActivitySearchType(supervisionId, null, activityState, movementReason, movementComment, locationFrom, null, null, null, null, null,
                null, null, null);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment,
        //locationFrom, locationTo
        locationTo = locIds.get(1);
        search = new HousingBedMgmtActivitySearchType(supervisionId, null, activityState, movementReason, movementComment, locationFrom, locationTo, null, null, null,
                null, null, null, null);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment,
        //locationFrom, locationTo, assignedDateFrom
        assignedDateFrom = BeanHelper.getPastDate(new Date(), 1); // past date
        search = new HousingBedMgmtActivitySearchType(supervisionId, null, activityState, movementReason, movementComment, locationFrom, locationTo, assignedDateFrom,
                null, null, null, null, null, null);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment,
        //locationFrom, locationTo, assignedDateFrom, assignedDateTo
        assignedDateTo = BeanHelper.nextNthDays(new Date(), 1); //future day
        search = new HousingBedMgmtActivitySearchType(supervisionId, null, activityState, movementReason, movementComment, locationFrom, locationTo, assignedDateFrom,
                assignedDateTo, null, null, null, null, null);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment,
        //locationFrom, locationTo, assignedDateFrom, assignedDateTo, assignedBy
        //assignedBy = 1L;
        search = new HousingBedMgmtActivitySearchType(supervisionId, null, activityState, movementReason, movementComment, locationFrom, locationTo, assignedDateFrom,
                assignedDateTo, assignedBy, null, null, null, null);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment,
        //locationFrom, locationTo, assignedDateFrom, assignedDateTo, assignedBy, 
        //unassignedBy
        //unassignedBy = 1L;
        search = new HousingBedMgmtActivitySearchType(supervisionId, null, activityState, movementReason, movementComment, locationFrom, locationTo, assignedDateFrom,
                assignedDateTo, assignedBy, null, null, unassignedBy, null);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 0L);

        //search by supervisionId, null, activityState, movementReason, movementComment,
        //locationFrom, locationTo, assignedDateFrom, assignedDateTo, assignedBy, 
        //unassignedBy, unassignedDateFrom
        unassignedDateFrom = BeanHelper.getPastDate(new Date(), 1);    // past date
        search = new HousingBedMgmtActivitySearchType(supervisionId, null, activityState, movementReason, movementComment, locationFrom, locationTo, assignedDateFrom,
                assignedDateTo, assignedBy, unassignedDateFrom, null, unassignedBy, null);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 0L);

        //search by supervisionId, null, activityState, movementReason, movementComment,
        //locationFrom, locationTo, assignedDateFrom, assignedDateTo, assignedBy, 
        //unassignedBy, unassignedDateFrom, unassignedDateTo
        unassignedDateTo = BeanHelper.createDate();    // today
        search = new HousingBedMgmtActivitySearchType(supervisionId, null, activityState, movementReason, movementComment, locationFrom, locationTo, assignedDateFrom,
                assignedDateTo, assignedBy, unassignedDateFrom, unassignedDateTo, unassignedBy, null);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 0L);

        //search by supervisionId, unassignedBy, unassignedDateFrom, unassignedDateTo
        search = new HousingBedMgmtActivitySearchType(supervisionId, null, null, null, null, null, null, null, null, null, unassignedDateFrom, unassignedDateTo,
                unassignedBy, null);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment,
        //locationFrom, locationTo, unassignedDateFrom, assignedDateTo, assignedBy, 
        //activeFlag
        search = new HousingBedMgmtActivitySearchType(supervisionId, null, activityState, movementReason, movementComment, locationFrom, locationTo, assignedDateFrom,
                assignedDateTo, assignedBy, null, null, null, false);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment,
        //locationFrom, locationTo, unassignedDateFrom, assignedDateTo, assignedBy, 
        //activeFlag
        search = new HousingBedMgmtActivitySearchType(supervisionId, null, activityState, movementReason, movementComment, locationFrom, locationTo, assignedDateFrom,
                assignedDateTo, assignedBy, null, null, null, true);
        ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 0L);

        // invoke search change request 
        searchChangeRequestPositive(supIds);

        // invoke for wildcard search
        searchByWildCard();

    }

    //@Test
    public void searchByWildCard() {

        //search by movementComment only - partial
        String movementComment = "unas*si*nged*";
        HousingBedMgmtActivitySearchType search = new HousingBedMgmtActivitySearchType();
        search.setMovementComment(movementComment);
        HousingBedMgmtActivitiesReturnType ret = service.search(uc, null, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 5L);

        //search by movementComment for Change Request
        movementComment = "Cha*ngeR*qu*est*";
        HousingChangeRequestSearchType searchCR = new HousingChangeRequestSearchType();
        searchCR.setMovementComment(movementComment);
        HousingChangeRequestsReturnType retCR = service.searchChangeRequest(uc, searchCR, 0L, 200L, null);

        assert (retCR.getChangeRequestWaitlists().size() == 10L);

        //search by approvedComment only - partial
        String approvedComment = "approve*Change*Request";
        searchCR = new HousingChangeRequestSearchType();
        searchCR.setApprovedComment(approvedComment);
        retCR = service.searchChangeRequest(uc, searchCR, 0L, 200L, null);

        assert (retCR.getChangeRequestWaitlists().size() == 5L);

        //search by closedComment only - partial
        String closedComment = "cancelC*hangeRequest*";
        searchCR = new HousingChangeRequestSearchType();
        searchCR.setClosedComment(closedComment);
        retCR = service.searchChangeRequest(uc, searchCR, 0L, 200L, null);

        assert (retCR.getChangeRequestWaitlists().size() == 5L);
    }

    //@Test
    public void searchSubSet() {

        Set<Long> subsetSearch = new HashSet<Long>();

        //Need to update the id based on the searchCreation data.
        subsetSearch.add(825L);
        subsetSearch.add(826L);
        subsetSearch.add(null);
        subsetSearch.add(827L);
        subsetSearch.add(638L);
        subsetSearch.add(812L);

        HousingBedMgmtActivitySearchType search = new HousingBedMgmtActivitySearchType();
        search.setActiveFlag(true);
        HousingBedMgmtActivitiesReturnType ret = service.search(uc, subsetSearch, search, 0L, 200L, null);

        assert (ret.getHousingBedMgmtActivities().size() == 3L);

    }

    @Test
    public void searchChangeRequestNegative() {

        /////////////////////////////////////////////////////////////////
        //Search for failure
        /////////////////////////////////////////////////////////////////

        //the HousingChangeRequestSearchType is null, this will fail.
        HousingChangeRequestSearchType search = null;
        verifySearchChangeRequestNegative(search, InvalidInputException.class);

        //the HousingChangeRequestSearchType is empty, this will fail.
        search = new HousingChangeRequestSearchType();
        verifySearchChangeRequestNegative(search, InvalidInputException.class);

        //Empty movementReason, this will fail.
        search = new HousingChangeRequestSearchType();
        search.setMovementReason("");
        verifySearchChangeRequestNegative(search, InvalidInputException.class);

    }

    //@Test
    public void searchChangeRequestPositive(List<Long> supIds) {

        //create search data
        //searchCreation();		

        /////////////////////////////////////////////////////////////////
        //Search by single condition
        /////////////////////////////////////////////////////////////////
        List<Long> locIds = filTest.getLeafIds();

        //search by supervisionId only
        Long supervisionId = supIds.get(4);
        HousingChangeRequestSearchType search = new HousingChangeRequestSearchType();
        search.setSupervisionId(supervisionId);
        HousingChangeRequestsReturnType ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by supervisionId only
        supervisionId = supIds.get(5);
        search = new HousingChangeRequestSearchType();
        search.setSupervisionId(supervisionId);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //Long facilityId = 1L;
        search = new HousingChangeRequestSearchType();
        search.setFacilityId(facilityId);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 10L);

        //search by ActivityState only
        String activityState = ACTIVITY_STATE_WAITL;
        search = new HousingChangeRequestSearchType();
        search.setActivityState(activityState);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 5L);

        //search by movementReason only
        String movementReason = MOVEMENT_REASON_OFFREQ;
        search = new HousingChangeRequestSearchType();
        search.setMovementReason(movementReason);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 5L);

        //search by locationRequested only
        Long locationRequested = locIds.get(3);
        search = new HousingChangeRequestSearchType();
        search.setLocationRequested(locationRequested);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 2L);

        //search by crPriority only
        String crPriority = CR_PRIORITY_HIGH;
        search = new HousingChangeRequestSearchType();
        search.setCRPriority(crPriority);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 10L);

        //search by crType only
        String crType = CR_TYPE_INTRA;
        search = new HousingChangeRequestSearchType();
        search.setCRType(crType);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 10L);

        //search by crType only
        crType = "INTRA2";
        search = new HousingChangeRequestSearchType();
        search.setCRType(crType);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by issuedBy only
        //Long issuedBy = 11L;
        search = new HousingChangeRequestSearchType();
        search.setIssuedBy(issuedBy);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 10L);

        //search by issuedDateFrom only - future day.
        Date issuedDateFrom = BeanHelper.nextNthDays(new Date(), 1);
        search = new HousingChangeRequestSearchType();
        search.setIssuedDateFrom(issuedDateFrom);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by issuedDateFrom only - today.
        issuedDateFrom = BeanHelper.createDate();
        search = new HousingChangeRequestSearchType();
        search.setIssuedDateFrom(issuedDateFrom);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by issuedDateFrom only - past date.
        issuedDateFrom = BeanHelper.getPastDate(new Date(), 1);
        search = new HousingChangeRequestSearchType();
        search.setIssuedDateFrom(issuedDateFrom);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 10L);

        //search by issuedDateTo only - past day.
        Date issuedDateTo = BeanHelper.getPastDate(new Date(), 1);
        search = new HousingChangeRequestSearchType();
        search.setIssuedDateTo(issuedDateTo);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by issuedDateTo only - today.
        issuedDateTo = BeanHelper.createDate();
        search = new HousingChangeRequestSearchType();
        search.setIssuedDateTo(issuedDateTo);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 10L);

        //search by approvedBy only
        //Long approvedBy = 16L;
        search = new HousingChangeRequestSearchType();
        search.setApprovedBy(issuedBy);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 5L);

        //search by approvedDateFrom only - future day.
        Date approvedDateFrom = BeanHelper.nextNthDays(new Date(), 1);
        search = new HousingChangeRequestSearchType();
        search.setApprovedDateFrom(approvedDateFrom);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by approvedDateFrom only - today.
        approvedDateFrom = BeanHelper.createDate();
        search = new HousingChangeRequestSearchType();
        search.setApprovedDateFrom(approvedDateFrom);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by approvedDateFrom only - past date.
        approvedDateFrom = BeanHelper.getPastDate(new Date(), 1);
        search = new HousingChangeRequestSearchType();
        search.setApprovedDateFrom(approvedDateFrom);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 5L);

        //search by approvedDateTo only - past day.
        Date approvedDateTo = BeanHelper.getPastDate(new Date(), 1);
        search = new HousingChangeRequestSearchType();
        search.setApprovedDateTo(approvedDateTo);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by approvedDateTo only - today.
        approvedDateTo = BeanHelper.createDate();
        search = new HousingChangeRequestSearchType();
        search.setApprovedDateTo(approvedDateTo);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 5L);

        //search by closedBy only
        //Long closedBy = 16L;
        search = new HousingChangeRequestSearchType();
        search.setClosedBy(issuedBy);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 5L);

        //search by closedDateFrom only - future day.
        Date closedDateFrom = BeanHelper.nextNthDays(new Date(), 1);
        search = new HousingChangeRequestSearchType();
        search.setClosedDateFrom(closedDateFrom);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by closedDateFrom only - today.
        closedDateFrom = BeanHelper.createDate();
        search = new HousingChangeRequestSearchType();
        search.setClosedDateFrom(closedDateFrom);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by closedDateFrom only - past date.
        closedDateFrom = BeanHelper.getPastDate(new Date(), 1);
        search = new HousingChangeRequestSearchType();
        search.setClosedDateFrom(closedDateFrom);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 5L);

        //search by closedDateTo only - past day.
        Date closedDateTo = BeanHelper.getPastDate(new Date(), 1);
        search = new HousingChangeRequestSearchType();
        search.setClosedDateTo(closedDateTo);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by closedDateTo only - today.
        closedDateTo = BeanHelper.createDate();
        search = new HousingChangeRequestSearchType();
        search.setClosedDateTo(closedDateTo);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 5L);

        //search by movementComment only
        String movementComment = "ChangeRquest-Waitlist comments11";
        search = new HousingChangeRequestSearchType();
        search.setMovementComment(movementComment);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by movementComment only
        movementComment = "ChangeRquest";
        search = new HousingChangeRequestSearchType();
        search.setMovementComment(movementComment);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by movementComment only - partial
        movementComment = "ChangeRquest*";
        search = new HousingChangeRequestSearchType();
        search.setMovementComment(movementComment);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 10L);

        //search by approvedComment only
        String approvedComment = "approveChangeRequest comment16";
        search = new HousingChangeRequestSearchType();
        search.setApprovedComment(approvedComment);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by approvedComment only
        approvedComment = "approveChangeRequest";
        search = new HousingChangeRequestSearchType();
        search.setApprovedComment(approvedComment);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by approvedComment only - partial
        approvedComment = "approveChangeRequest*";
        search = new HousingChangeRequestSearchType();
        search.setApprovedComment(approvedComment);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 5L);

        //search by closedComment only
        String closedComment = "cancelChangeRequest comment16";
        search = new HousingChangeRequestSearchType();
        search.setClosedComment(closedComment);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by closedComment only
        closedComment = "cancelChangeRequest";
        search = new HousingChangeRequestSearchType();
        search.setClosedComment(closedComment);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by closedComment only - partial
        closedComment = "cancelChangeRequest*";
        search = new HousingChangeRequestSearchType();
        search.setClosedComment(closedComment);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 5L);

        //search by reservationFlag only
        search = new HousingChangeRequestSearchType();
        search.setReservationFlag(true);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 5L);

        //search by reservationFlag only
        search = new HousingChangeRequestSearchType();
        search.setReservationFlag(false);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 5L);
        /*
        //search by validationFlag only
		search = new HousingChangeRequestSearchType();
		search.setValidationFlag(true);
		ret = service.searchChangeRequest(uc, search, 0L, 200L, null);
		
		assert(ret.getChangeRequestWaitlists().size() == 0L);
		
		//search by validationFlag only
		search = new HousingChangeRequestSearchType();
		search.setValidationFlag(false);
		ret = service.searchChangeRequest(uc, search, 0L, 200L, null);
		
		assert(ret.getChangeRequestWaitlists().size() == 10L);
		*/
        /////////////////////////////////////////////////////////////////
        //Multiple condition search 
        /////////////////////////////////////////////////////////////////

        //search by supervisionId and activityState 
        supervisionId = supIds.get(5);
        activityState = ACTIVITY_STATE_ASSGN;
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, null, null, null, null, null, null, null, null, null, null, null, null, null,
                null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by supervisionId and activityState 
        activityState = ACTIVITY_STATE_WAITL;
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, null, null, null, null, null, null, null, null, null, null, null, null, null,
                null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, facilityId and activityState 
        activityState = ACTIVITY_STATE_WAITL;
        search = new HousingChangeRequestSearchType(supervisionId, facilityId, activityState, null, null, null, null, null, null, null, null, null, null, null, null,
                null, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason
        movementReason = MOVEMENT_REASON_MED;
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, null, null, null, null, null, null, null, null, null, null, null,
                null, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        movementComment = "ChangeRquest-Waitlist comments11";
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, null, null, null, null, null, null, null, null,
                null, null, null, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested
        locationRequested = locIds.get(3);
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, null, null, null, null, null,
                null, null, null, null, null, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority
        crPriority = CR_PRIORITY_HIGH;
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, null, null, null,
                null, null, null, null, null, null, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType
        crType = CR_TYPE_INTRA;
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType, null,
                null, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom
        issuedDateFrom = BeanHelper.createDate();  //today
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, null, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom
        issuedDateFrom = BeanHelper.getPastDate(new Date(), 1);  //past date
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, null, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom, issuedDateTo
        issuedDateTo = BeanHelper.createDate();  //today
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, issuedDateTo, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom, issuedDateTo
        //issuedBy		
        //issuedBy = 11L;
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, issuedDateTo, issuedBy, null, null, null, null, null, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom, issuedDateTo
        //issuedBy, approvedDateFrom
        approvedDateFrom = BeanHelper.createDate();        //today
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, issuedDateTo, issuedBy, approvedDateFrom, null, null, null, null, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom, issuedDateTo
        //issuedBy, approvedDateFrom
        supervisionId = supIds.get(10);
        activityState = ACTIVITY_STATE_CANCL;
        movementReason = MOVEMENT_REASON_OFFREQ;
        movementComment = "ChangeRquest-Approval Required comments16";
        locationRequested = locIds.get(2);
        //issuedBy = 16L;
        approvedDateFrom = BeanHelper.getPastDate(new Date(), 1);        //past date
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, issuedDateTo, issuedBy, approvedDateFrom, null, null, null, null, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom, issuedDateTo
        //issuedBy, approvedDateFrom, approvedDateTo
        approvedDateTo = BeanHelper.createDate();        //today
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, issuedDateTo, issuedBy, approvedDateFrom, approvedDateTo, null, null, null, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom, issuedDateTo
        //issuedBy, approvedDateFrom, approvedDateTo, approvedBy
        Long approvedBy = issuedBy;
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, issuedDateTo, issuedBy, approvedDateFrom, approvedDateTo, approvedBy, null, null, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom, issuedDateTo
        //issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment
        approvedComment = "approveChangeRequest comment16";
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, issuedDateTo, issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment, null, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom, issuedDateTo
        //issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment
        //closedDateFrom
        closedDateFrom = BeanHelper.getPastDate(new Date(), 1);        //past date
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, issuedDateTo, issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment, closedDateFrom, null, null, null, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom, issuedDateTo
        //issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment
        //closedDateFrom, closedDateTo
        closedDateTo = BeanHelper.createDate();        //today
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, issuedDateTo, issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment, closedDateFrom, closedDateTo, null, null, null,
                null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom, issuedDateTo
        //issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment
        //closedDateFrom, closedDateTo, closedBy
        Long closedBy = issuedBy;
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, issuedDateTo, issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment, closedDateFrom, closedDateTo, closedBy, null, null,
                null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom, issuedDateTo
        //issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment
        //closedDateFrom, closedDateTo, closedBy, closedComment
        closedComment = "cancelChangeRequest comment16";
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, issuedDateTo, issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment, closedDateFrom, closedDateTo, closedBy,
                closedComment, null, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom, issuedDateTo
        //issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment
        //closedDateFrom, closedDateTo, closedBy, closedComment, reservationFlag
        Boolean reservationFlag = true;
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, issuedDateTo, issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment, closedDateFrom, closedDateTo, closedBy,
                closedComment, reservationFlag, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom, issuedDateTo
        //issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment
        //closedDateFrom, closedDateTo, closedBy, closedComment, reservationFlag
        reservationFlag = false;
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, issuedDateTo, issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment, closedDateFrom, closedDateTo, closedBy,
                closedComment, reservationFlag, null);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom, issuedDateTo
        //issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment
        //closedDateFrom, closedDateTo, closedBy, closedComment, reservationFlag and validationFlag
        Boolean validationFlag = false;
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, issuedDateTo, issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment, closedDateFrom, closedDateTo, closedBy,
                closedComment, reservationFlag, validationFlag);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 1L);

        //search by supervisionId, null, activityState, movementReason, movementComment
        //locationRequested, crPriority, crType, issuedDateFrom, issuedDateTo
        //issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment
        //closedDateFrom, closedDateTo, closedBy, closedComment, reservationFlag and validationFlag
        validationFlag = true;
        search = new HousingChangeRequestSearchType(supervisionId, null, activityState, movementReason, movementComment, locationRequested, crPriority, crType,
                issuedDateFrom, issuedDateTo, issuedBy, approvedDateFrom, approvedDateTo, approvedBy, approvedComment, closedDateFrom, closedDateTo, closedBy,
                closedComment, reservationFlag, validationFlag);
        ret = service.searchChangeRequest(uc, search, 0L, 200L, null);

        assert (ret.getChangeRequestWaitlists().size() == 0L);

    }

    public List<Long> retrieveCreation() {

        //clean db and reload meta data
        reset();

        List<Long> supervisionIds = new ArrayList<Long>();

        List<Long> leafLoations = filTest.getLeafIds();

        //set for configuration data.
        setHousingPeriodicReviewConfig();

        Date assignedDate = new Date();

        // Create housing assigned activity for same offender
        Long sup1 = createSupervision();
        supervisionIds.add(sup1);
        for (int i = 1; i <= 5; i++) {

            // build housing bed mgmt activity properties	
            String movementReason = MOVEMENT_REASON_BEH;

            // build housing assignment properties
            Long locationTo = leafLoations.get(i);

            // build HousingAttributeMismatch for assignment
            HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, SECURITY_LEVEL_MIN, getSecurityLevelCodes(),
                    OUTCOME_SUTBL, activityId);
            HousingAttributeMismatchType mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_GENDER, OFFENDER_GENDER_F, getGenderCodes(), OUTCOME_SUTBL,
                    activityId);

            Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
            assignmentMismatches.add(mismatch);
            assignmentMismatches.add(mismatch2);

            /////////////////////////////////////////////////////////////////

            String movementComment = "Housing Assinged comments" + sup1;
            HousingLocationAssignType housingAssign = new HousingLocationAssignType(activityId, sup1, movementReason, locationTo, assignedBy, assignedDate,
                    assignmentMismatches, OUTCOME_SUTBL, movementComment, facilityId);

            HousingBedMgmtActivityType ret = service.assignHousingLocation(uc, housingAssign);

            assert (ret != null);

        }

        // Create housing assigned activity with assign to same location
        for (int i = 1; i <= 5; i++) {

            // build housing bed mgmt activity properties		
            sup1 = createSupervision();
            supervisionIds.add(sup1);
            String movementReason = MOVEMENT_REASON_BEH;

            // build housing assignment properties
            Long locationTo = leafLoations.get(5);

            // build HousingAttributeMismatch for assignment
            HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, SECURITY_LEVEL_MIN, getSecurityLevelCodes(),
                    OUTCOME_SUTBL, activityId);
            HousingAttributeMismatchType mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_GENDER, OFFENDER_GENDER_F, getGenderCodes(), OUTCOME_SUTBL,
                    activityId);

            Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
            assignmentMismatches.add(mismatch);
            assignmentMismatches.add(mismatch2);

            /////////////////////////////////////////////////////////////////

            //Long facilityId = 1L;

            String movementComment = "Housing Assinged comments" + sup1;
            HousingLocationAssignType housingAssign = new HousingLocationAssignType(activityId, sup1, movementReason, locationTo, assignedBy, assignedDate,
                    assignmentMismatches, OUTCOME_SUTBL, movementComment, facilityId);

            HousingBedMgmtActivityType ret = service.assignHousingLocation(uc, housingAssign);

            assert (ret != null);

        }

        return supervisionIds;

    }

    @Test
    public void retrieveAssignmentsByOffenderNegative() {

        //supervisionId is empty, will fail.
        Long supervisionId = null;
        verifyRetrieveAssignmentsByOffenderNegative(supervisionId, null, InvalidInputException.class);

        //historyDays is negative, will fail.
        supervisionId = 1L;
        verifyRetrieveAssignmentsByOffenderNegative(supervisionId, -1L, InvalidInputException.class);

    }

    @Test
    public void retrieveAssignmentsByOffenderPositive() {

        List<Long> supIds = retrieveCreation();

        //Success
		/*
		Long supervisionId = 1L;
		OffenderHousingAssignmentsReturnType ret = service.retrieveAssignmentsByOffender(uc, supervisionId, null);
		
		assert(ret.getOffenderHousingAssignments().size() == 1L);
		*/

        Long supervisionId = supIds.get(0);
        OffenderHousingAssignmentsReturnType ret = service.retrieveAssignmentsByOffender(uc, supervisionId, null);

        assert (ret.getOffenderHousingAssignments().size() == 1L);

        //supervisionId = 100L;
        ret = service.retrieveAssignmentsByOffender(uc, supervisionId, 10L);

        assert (ret.getOffenderHousingAssignments().size() == 5L);

        supervisionId = supIds.get(1);
        ret = service.retrieveAssignmentsByOffender(uc, supervisionId, null);

        assert (ret.getOffenderHousingAssignments().size() == 1L);

        //historyDays
        ret = service.retrieveAssignmentsByOffender(uc, supervisionId, 0L);

        assert (ret.getOffenderHousingAssignments().size() == 1L);

        //historyDays
        ret = service.retrieveAssignmentsByOffender(uc, supervisionId, 10L);

        assert (ret.getOffenderHousingAssignments().size() == 1L);

        //Invoke other retrieve functions
        retrieveAssignmentsByLocationPositive();
        retrieveMishousedAssignmentsPositive();
        retrieveDueChangeAssignmentsPositive();

    }

    @Test
    public void retrieveAssignmentsByLocationNegative() {

        //locationTo is null, will fail.
        verifyRetrieveAssignmentsByLocationNegative(null, null, InvalidInputException.class);

        //historyDays is negative, will fail.
        locationTo = 1L;
        verifyRetrieveAssignmentsByLocationNegative(locationTo, -1L, InvalidInputException.class);

    }

    //@Test
    public void retrieveAssignmentsByLocationPositive() {
		/*
		Long locationTo = 3L;
		OffenderHousingAssignmentsReturnType ret = service.retrieveAssignmentsByLocation(uc, locationTo, null);
		
		assert(ret.getOffenderHousingAssignments().size() == 2L);
		*/

        //retrieveCreation();

        List<Long> leafLocations = filTest.getLeafIds();

        //Success
        Long locationTo = leafLocations.get(8);
        OffenderHousingAssignmentsReturnType ret = service.retrieveAssignmentsByLocation(uc, locationTo, null);

        assert (ret.getOffenderHousingAssignments().size() == 0L);

        locationTo = leafLocations.get(5);
        ret = service.retrieveAssignmentsByLocation(uc, locationTo, null);

        assert (ret.getOffenderHousingAssignments().size() == 6L);

        ret = service.retrieveAssignmentsByLocation(uc, locationTo, 0L);

        assert (ret.getOffenderHousingAssignments().size() == 6L);

        ret = service.retrieveAssignmentsByLocation(uc, locationTo, 10L);

        assert (ret.getOffenderHousingAssignments().size() == 6L);

    }

    @Test
    public void retrieveMishousedAssignmentsNegative() {

        //requestedLocations is null, will fail.
        verifyRetrieveMishousedAssignmentsNegative(null, null, InvalidInputException.class);

        //requestedLocations is empty, will fail.
        Set<Long> requestedLocations = new HashSet<Long>();
        verifyRetrieveMishousedAssignmentsNegative(requestedLocations, null, InvalidInputException.class);

        //newMishoused is null, will fail.
        requestedLocations = new HashSet<Long>();
        requestedLocations.add(1L);
        requestedLocations.add(2L);
        verifyRetrieveMishousedAssignmentsNegative(requestedLocations, null, InvalidInputException.class);

    }

    //@Test
    public void retrieveMishousedAssignmentsPositive() {

        //retrieveCreation();
        List<Long> leafLocations = filTest.getLeafIds();

        //Success
        Set<Long> requestedLocations = new HashSet<Long>();
        requestedLocations.add(leafLocations.get(0));
        requestedLocations.add(leafLocations.get(1));
        requestedLocations.add(leafLocations.get(5));

        OffenderHousingAssignmentsReturnType ret = service.retrieveMishousedAssignments(uc, requestedLocations, false);

        assert (ret.getOffenderHousingAssignments().size() == 6L);

        ret = service.retrieveMishousedAssignments(uc, requestedLocations, true);

        assert (ret.getOffenderHousingAssignments().size() == 0L);

    }

    @Test
    public void retrieveDueChangeAssignmentsNegative() {

        //requestedLocations is null, will fail.
        verifyRetrieveDueChangeAssignmentsNegative(null, null, InvalidInputException.class);

        //requestedLocations is empty, will fail.
        Set<Long> requestedLocations = new HashSet<Long>();
        verifyRetrieveDueChangeAssignmentsNegative(requestedLocations, null, InvalidInputException.class);

        //requestedLocations DTO validation failure
        requestedLocations = new HashSet<Long>();
        requestedLocations.add(null);
        verifyRetrieveDueChangeAssignmentsNegative(requestedLocations, null, InvalidInputException.class);

        //daysUpcomingWithin is negative, will fail.
        requestedLocations = new HashSet<Long>();
        requestedLocations.add(1L);
        requestedLocations.add(2L);
        verifyRetrieveDueChangeAssignmentsNegative(requestedLocations, -1L, InvalidInputException.class);

    }

    //@Test
    public void retrieveDueChangeAssignmentsPositive() {

        //retrieveCreation();		

        //success
        //default DaysAheadReviewRequired is 7 days
        List<Long> leafLocations = filTest.getLeafIds();

        Set<Long> requestedLocations = new HashSet<Long>();
        requestedLocations.add(leafLocations.get(0));
        requestedLocations.add(leafLocations.get(1));
        requestedLocations.add(leafLocations.get(5));

        OffenderHousingAssignmentsReturnType ret = service.retrieveDueChangeAssignments(uc, requestedLocations, null);

        assert (ret.getOffenderHousingAssignments().size() == 0L);

        //MaxDaysInOneCell is 180 days.
        //default DaysAheadReviewRequired is 7 days
        //daysUpcomingWithin is 180 days.
        ret = service.retrieveDueChangeAssignments(uc, requestedLocations, 180L);

        assert (ret.getOffenderHousingAssignments().size() == 6L);

        //daysUpcomingWithin is 180 days.
        requestedLocations = new HashSet<Long>();
        requestedLocations.add(leafLocations.get(0));
        ret = service.retrieveDueChangeAssignments(uc, requestedLocations, 180L);

        assert (ret.getOffenderHousingAssignments().size() == 0L);

        //daysUpcomingWithin is 150 days.
        requestedLocations = new HashSet<Long>();
        requestedLocations.add(leafLocations.get(1));
        ret = service.retrieveDueChangeAssignments(uc, requestedLocations, 150L);

        assert (ret.getOffenderHousingAssignments().size() == 0L);

        //daysUpcomingWithin is 180 days.
        requestedLocations = new HashSet<Long>();
        requestedLocations.add(leafLocations.get(1));
        requestedLocations.add(leafLocations.get(5));
        ret = service.retrieveDueChangeAssignments(uc, requestedLocations, 180L);

        assert (ret.getOffenderHousingAssignments().size() == 6L);

    }

    //@Test
    public List<Long> retrieveMultipleCreation() {

        //clean db and reload meta data
        reset();

        //Long toIdentifer = null;

        List<Long> supIds = new ArrayList<Long>();
        List<Long> locIds = filTest.getLeafIds();

        //set for configuration data.
        setHousingPeriodicReviewConfig();

        // assign offender 1-5 to location 1
        for (int i = 1; i <= 5; i++) {
            //toIdentifer = Long.valueOf(i);	
            Long supId = createSupervision();
            assignHousingLocation(supId, locIds.get(0));
            supIds.add(supId);
        }

        // assign offender 1-5 to location 4
        for (int i = 1; i <= 5; i++) {
            //toIdentifer = Long.valueOf(i);		
            Long supId = supIds.get(i - 1);
            assignHousingLocation(supId, locIds.get(3));
        }

        // assign offender 6-10 to location 2
        for (int i = 6; i <= 10; i++) {
            //toIdentifer = Long.valueOf(i);		
            Long supId = createSupervision();
            assignHousingLocation(supId, locIds.get(1));
            supIds.add(supId);
        }

        // assign offender 11-15 to location 3
        for (int i = 11; i <= 15; i++) {
            //toIdentifer = Long.valueOf(i);	
            Long supId = createSupervision();
            assignHousingLocation(supId, locIds.get(2));
            supIds.add(supId);
        }

        return supIds;
    }

    @Test
    public void retrieveCurrentAssignmentsByOffendersNegative() {

        // offenderReferences is null
        verifyRetrieveDueChangeAssignmentsNegative(null, InvalidInputException.class);

        // offenderReferences is empty
        Set<Long> offenderReferences = new HashSet<Long>();
        verifyRetrieveDueChangeAssignmentsNegative(offenderReferences, InvalidInputException.class);

        // supervisionId is null
        offenderReferences = new HashSet<Long>();
        offenderReferences.add(null);
        verifyRetrieveDueChangeAssignmentsNegative(offenderReferences, InvalidInputException.class);

    }

    //@Test
    public void retrieveCurrentAssignmentsByOffendersPositive2() {

        Set<Long> offenderReferences = null;
        Long toIdentifer = null;
        int intStartNum = 254;
        // create for offender 1-5
        offenderReferences = new HashSet<Long>();
        for (int i = intStartNum; i <= intStartNum + 1; i++) {
            toIdentifer = Long.valueOf(i);

            Long supervisionId = toIdentifer;
            offenderReferences.add(supervisionId);

        }

        OffenderHousingAssignmentsReturnType ret = service.retrieveCurrentAssignmentsByOffenders(uc, offenderReferences);

        assert (ret.getOffenderHousingAssignments().size() == 2L);

    }

    @Test
    public void retrieveCurrentAssignmentsByOffendersPositive() {

        List<Long> supIds = retrieveMultipleCreation();

        Set<Long> offenderReferences = null;
        //Long toIdentifer = null;

        // create for offender 1-5
        offenderReferences = new HashSet<Long>();
        for (int i = 1; i <= 5; i++) {
            //toIdentifer = Long.valueOf(i);

            //Long supervisionId = locIds.get(1);
            Long supervisionId = supIds.get(i - 1);
            offenderReferences.add(supervisionId);

        }

        OffenderHousingAssignmentsReturnType ret = service.retrieveCurrentAssignmentsByOffenders(uc, offenderReferences);

        assert (ret.getOffenderHousingAssignments().size() == 5L);

        // create for offender 1-10
        offenderReferences = new HashSet<Long>();
        for (int i = 1; i <= 10; i++) {
            //toIdentifer = Long.valueOf(i);

            //Long supervisionId = toIdentifer;
            Long supervisionId = supIds.get(i - 1);
            offenderReferences.add(supervisionId);
        }

        // only have 15 offenders in housing, so only return 15 offenders
        ret = service.retrieveCurrentAssignmentsByOffenders(uc, offenderReferences);

        assert (ret.getOffenderHousingAssignments().size() == 10L);

        // invoke other multiple records retrieve
        retrieveCurrentAssignmentsByLocationsPositive();
    }

    @Test
    public void retrieveCurrentAssignmentsByLocationsNegative() {

        // locationReferences is null
        verifyRetrieveCurrentAssignmentsByLocationsNegative(null, InvalidInputException.class);

        // locationReferences is empty
        Set<Long> locationReferences = new HashSet<Long>();
        verifyRetrieveCurrentAssignmentsByLocationsNegative(locationReferences, InvalidInputException.class);

        // locationReference is null
        locationReferences = new HashSet<Long>();
        locationReferences.add(null);
        verifyRetrieveCurrentAssignmentsByLocationsNegative(locationReferences, InvalidInputException.class);

    }

    //@Test
    public void retrieveCurrentAssignmentsByLocationsPositive() {

        //retrieveMultipleCreation();
        List<Long> locIds = filTest.getLeafIds();

        Set<Long> locationReferences = null;
        //Long toIdentifer = null;

        // create for location 1
        locationReferences = new HashSet<Long>();
        for (int i = 1; i <= 1; i++) {
            //toIdentifer = Long.valueOf(i);	
            //Long locationReference = toIdentifer;
            locationReferences.add(locIds.get(i - 1));
        }

        LocationHousingAssignmentsReturnType ret = service.retrieveCurrentAssignmentsByLocations(uc, locationReferences);

        assert (ret.getLocationHousingAssignments().size() == 0L);

        // create for location 1-5
        locationReferences = new HashSet<Long>();
        for (int i = 1; i <= 5; i++) {
            //toIdentifer = Long.valueOf(i);	
            //Long locationReference = toIdentifer;
            locationReferences.add(locIds.get(i - 1));
        }

        ret = service.retrieveCurrentAssignmentsByLocations(uc, locationReferences);

        assert (ret.getLocationHousingAssignments().size() == 3L);

    }

    //@Test
    public List<Long> retrieveMultipleChangeRequestsCreation() {

        //clean db and reload meta data
        reset();

        //Long toIdentifer = null;

        //Create a change request housing activity instance	

        List<Long> supIds = new ArrayList<Long>();
        List<Long> locIds = filTest.getLeafIds();
        // offender 1-5 to location 1, isReserved = true
        for (int i = 1; i <= 5; i++) {
            //toIdentifer = Long.valueOf(i);	
            Long supId = createSupervision();
            createChangeRequest(supId, locIds.get(0), MOVEMENT_REASON_MED, true); //Approval Not Required
            supIds.add(supId);
        }

        // offender 1-5 to location 2, isReserved = true
        for (int i = 1; i <= 5; i++) {
            //toIdentifer = Long.valueOf(i);	
            Long supId = supIds.get(i - 1);
            createChangeRequest(supId, locIds.get(1), MOVEMENT_REASON_OFFREQ, true); //Approval Required
        }

        // offender 1-5 to location 3, isReserved = false
        for (int i = 1; i <= 5; i++) {
            //toIdentifer = Long.valueOf(i);
            Long supId = supIds.get(i - 1);
            createChangeRequest(supId, locIds.get(2), MOVEMENT_REASON_MED, false); //Approval Not Required
        }

        // offender 6-10 to location 4
        for (int i = 6; i <= 10; i++) {
            //toIdentifer = Long.valueOf(i);
            Long supId = createSupervision();
            supIds.add(supId);
            createChangeRequest(supId, locIds.get(3), MOVEMENT_REASON_OFFREQ, true);    //Approval Required		
        }

        // offender 6-10 to location 5
        for (int i = 6; i <= 10; i++) {
            //toIdentifer = Long.valueOf(i);	
            Long supId = supIds.get(i - 1);
            createChangeRequest(supId, locIds.get(4), MOVEMENT_REASON_MED, true); //Approval Not Required
        }

        // offender 11-15 to location 6
        for (int i = 11; i <= 15; i++) {
            //toIdentifer = Long.valueOf(i);	
            Long supId = createSupervision();
            supIds.add(supId);
            createChangeRequest(supId, locIds.get(5), MOVEMENT_REASON_MED, true); //Approval Not Required	
        }

        return supIds;

    }

    @Test
    public void retrieveReservationsByOffendersNegative() {

        // offenderReferences is null
        verifyRetrieveReservationsByOffendersNegative(null, InvalidInputException.class);

        // offenderReferences is empty
        Set<Long> offenderReferences = new HashSet<Long>();
        verifyRetrieveReservationsByOffendersNegative(offenderReferences, InvalidInputException.class);

        // supervisionId is null
        offenderReferences = new HashSet<Long>();
        offenderReferences.add(null);
        verifyRetrieveReservationsByOffendersNegative(offenderReferences, InvalidInputException.class);

    }

    @Test
    public void retrieveReservationsByOffendersPositive() {

        List<Long> supIds = retrieveMultipleChangeRequestsCreation();

        Set<Long> offenderReferences = null;
        //Long toIdentifer = null;

        // create for offender 1-5
        offenderReferences = new HashSet<Long>();
        for (int i = 1; i <= 5; i++) {
            //toIdentifer = Long.valueOf(i);

            Long supervisionId = supIds.get(i - 0);
            offenderReferences.add(supervisionId);

        }

        HousingChangeRequestsReturnType ret = service.retrieveReservationsByOffenders(uc, offenderReferences);

        assert (ret.getChangeRequestWaitlists().size() == 5L);

        // create for offender 1-10
        offenderReferences = new HashSet<Long>();
        for (int i = 1; i <= 10; i++) {
            //toIdentifer = Long.valueOf(i);

            Long supervisionId = supIds.get(i - 0);
            offenderReferences.add(supervisionId);
        }

        // only have 15 offenders in housing, so only return 15 offenders
        ret = service.retrieveReservationsByOffenders(uc, offenderReferences);

        assert (ret.getChangeRequestWaitlists().size() == 10L);

        // invoke other multiple records retrieve
        retrieveReservationsByLocationsPositive();
    }

    @Test
    public void retrieveReservationsByLocationsNegative() {

        // locationReferences is null
        verifyRetrieveReservationsByLocationsNegative(null, InvalidInputException.class);

        // locationReferences is empty
        Set<Long> locationReferences = new HashSet<Long>();
        verifyRetrieveReservationsByLocationsNegative(locationReferences, InvalidInputException.class);

        // locationReference is null
        locationReferences = new HashSet<Long>();
        locationReferences.add(null);
        verifyRetrieveReservationsByLocationsNegative(locationReferences, InvalidInputException.class);

        // Success
        locationReferences = new HashSet<Long>();
        locationReferences.add(1L);
        LocationHousingChangeRequestsReturnType ret = service.retrieveReservationsByLocations(uc, locationReferences);
        assert (ret != null);

    }

    //@Test
    public void retrieveReservationsByLocationsPositive() {

        //retrieveMultipleChangeRequestsCreation();

        Set<Long> locationReferences = null;
        List<Long> locIds = filTest.getLeafIds();
        //Long toIdentifer = null;

        // create for location 1
        locationReferences = new HashSet<Long>();
        for (int i = 1; i <= 1; i++) {
            //toIdentifer = Long.valueOf(i);	
            Long locationReference = locIds.get(0);
            locationReferences.add(locationReference);
        }

        LocationHousingChangeRequestsReturnType ret = service.retrieveReservationsByLocations(uc, locationReferences);

        assert (ret.getLocationHousingChangeRequests().size() == 1L);

        // create for location 1-5
        locationReferences = new HashSet<Long>();
        for (int i = 1; i <= 5; i++) {
            //toIdentifer = Long.valueOf(i);	
            Long locationReference = locIds.get(i - 1);
            locationReferences.add(locationReference);
        }

        ret = service.retrieveReservationsByLocations(uc, locationReferences);

        assert (ret.getLocationHousingChangeRequests().size() == 2L);

    }

    @Test
    public void setHousingPeriodicReviewConfig() {

        //HousingPeriodicReviewType is null, will fail.
        verifySetHousingPeriodicReviewConfigNegative(null, InvalidInputException.class);

        //HousingPeriodicReviewType is empty, will fail.
        HousingPeriodicReviewType housingPeriodicReview = new HousingPeriodicReviewType();
        verifySetHousingPeriodicReviewConfigNegative(housingPeriodicReview, InvalidInputException.class);

        //HousingPeriodicReviewType DTO validation failure
        housingPeriodicReview = new HousingPeriodicReviewType(null, 7L);
        verifySetHousingPeriodicReviewConfigNegative(housingPeriodicReview, InvalidInputException.class);

        //Success
        housingPeriodicReview = new HousingPeriodicReviewType(180L, 7L);
        HousingPeriodicReviewType ret = service.setHousingPeriodicReviewConfig(uc, housingPeriodicReview);
        assert (ret != null);

    }

    @Test
    public void retrieveHousingPeriodicReviewConfig() {

        // create configuration data first.
        setHousingPeriodicReviewConfig();

        HousingPeriodicReviewType ret = service.retrieveHousingPeriodicReviewConfig(uc);
        assert (ret != null);

    }

    @Test
    public void setHousingCRPriorities() {

        //housingCRPriorities is null, will fail.
        verifySetHousingCRPrioritiesNegative(null, InvalidInputException.class);

        //housingCRPriorities is empty, will fail.
        Set<HousingCRPriorityType> housingCRPriorities = new HashSet<HousingCRPriorityType>();
        verifySetHousingCRPrioritiesNegative(housingCRPriorities, InvalidInputException.class);

        //housingCRPriorities DTO validation failure
        housingCRPriorities = new HashSet<HousingCRPriorityType>();
        housingCRPriorities.add(new HousingCRPriorityType());
        verifySetHousingCRPrioritiesNegative(housingCRPriorities, InvalidInputException.class);

        //Only one code created
        housingCRPriorities = new HashSet<HousingCRPriorityType>();
        housingCRPriorities.add(new HousingCRPriorityType(CR_PRIORITY_LOW, 1L));
        housingCRPriorities.add(new HousingCRPriorityType(CR_PRIORITY_LOW, 1L));
        housingCRPriorities.add(new HousingCRPriorityType(CR_PRIORITY_LOW, 1L));
        HousingCRPrioritiesReturnType ret = service.setHousingCRPriorities(uc, housingCRPriorities);
        assert (ret.getHousingCRPriorities().size() == 1L);

        //Create 3 codes
        housingCRPriorities = new HashSet<HousingCRPriorityType>();
        housingCRPriorities.add(new HousingCRPriorityType(CR_PRIORITY_HIGH, 1L));
        housingCRPriorities.add(new HousingCRPriorityType(CR_PRIORITY_MED, 2L));
        housingCRPriorities.add(new HousingCRPriorityType(CR_PRIORITY_LOW, 3L));
        ret = service.setHousingCRPriorities(uc, housingCRPriorities);

        assert (ret.getHousingCRPriorities().size() == 3L);

    }

    @Test
    public void retrieveHousingCRPriorities() {

        //set HousingCRPriorities
        setHousingCRPriorities();

        //get one CR Priority
        String crPriority = CR_PRIORITY_HIGH;
        HousingCRPrioritiesReturnType ret = service.retrieveHousingCRPriorities(uc, crPriority);
        assert (ret.getHousingCRPriorities().size() == 1L);

        //get all CRPriorities
        ret = service.retrieveHousingCRPriorities(uc, null);
        assert (ret.getHousingCRPriorities().size() == 3L);

    }

    @Test
    public void deletePositive() {

        //Create housing assignment activity
        Long supId = createSupervision();
        HousingBedMgmtActivityType retCreate = assignHousingLocation(supId);

        Long ret = service.delete(uc, retCreate);
        assert (ret.equals(ReturnCode.Success.returnCode()));

    }

    @Test
    public void deleteAll() {

        Long ret = service.deleteAll(uc);
        assert (ret.equals(ReturnCode.Success.returnCode()));

        //reload default meta data.
        reset();

    }

    @Test
    public void getNegative() {

        //housingActivityId is null
        Long housingActivityId = null;
        verifyGetNegative(housingActivityId, InvalidInputException.class);

        //housingActivityId is wrong
        housingActivityId = -1L;
        verifyGetNegative(housingActivityId, DataNotExistException.class);

    }

    @Test
    public void getPositive() {

        //Create housing assignment activity
        Long supId = createSupervision();
        HousingBedMgmtActivityType retCreate = assignHousingLocation(supId);

        //Success
        Long housingActivityId = retCreate.getHousingActivityId();
        HousingBedMgmtActivityType ret = service.get(uc, housingActivityId);

        assert (ret != null);

    }

    @Test
    public void getAll() {

        //reload default meta data.
        reset();

        Long locId = filTest.getCell4();
        // Create housing assigned activity 
        for (int i = 1; i <= 5; i++) {
            Long supId = createSupervision();
            assignHousingLocation(supId, locId);
        }

        Set<Long> ret = service.getAll(uc);
        assert (ret.size() == 5);

    }

    @Test
    public void getCount() {

        //reload default meta data.
        reset();

        // Create housing assigned activity 
        for (int i = 1; i <= 5; i++) {
            Long supId = createSupervision();
            assignHousingLocation(supId);
        }

        Long ret = service.getCount(uc);
        assert (ret == 5L);

    }

    @Test
    public void getStampPositive() {

        //Create housing assignment activity
        Long supId = createSupervision();
        HousingBedMgmtActivityType retCreate = assignHousingLocation(supId);

        //Success
        Long housingActivityId = retCreate.getHousingActivityId();
        StampType ret = service.getStamp(uc, housingActivityId);
        assert (ret != null);

    }

    @Test
    public void getNiem() {

        //housingActivityId is null
        String ret = service.getNiem(uc, null);
        assert (ret != null);

    }

    //@Test
    public void testCRDefect966() {

        reset();

        //Create housing assignment activity

        // Test to check if persist the activityReference of mismatch, also check the offenderValue of mismatch is null when mismatch attribute value is AGE.
        Long supId = createSupervision();
        HousingBedMgmtActivityType retCreate = assignHousingLocation(supId);

        //Success
        Long housingActivityId = retCreate.getHousingActivityId();
        HousingBedMgmtActivityType ret = service.get(uc, housingActivityId);

        assert (ret != null);

    }

    //@Test
    public void testCR1962() {

        //retrieve current assignment by offender
        Long supervisionId = 1150L;
        OffenderHousingAssignmentsReturnType ret = service.retrieveAssignmentsByOffender(uc, supervisionId, null);

        assert (ret.getOffenderHousingAssignments().size() == 1L);
    }

    //@Test
    public void testDefect1005() {

        reset();

        Long offenderId = 100L;
        Long locationId = 100L;

        String movementReason = MOVEMENT_REASON_MED;

        String movementComment = "assignHousingLocation comments" + offenderId;

        Long supervisionId = offenderId;

        Long locationTo = locationId;

        // build HousingAttributeMismatch for assignment
        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(100001L, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, getSecurityLevelCodes(),
                OUTCOME_SUTBL, activityId);
        HousingAttributeMismatchType mismatch2 = new HousingAttributeMismatchType(100002L, SUITABILITY_GENDER, CODE_SEX_F, getGenderCodes(), OUTCOME_SUTBL, activityId);
        HousingAttributeMismatchType mismatch3 = new HousingAttributeMismatchType(100003L, SUITABILITY_AGE, AGE_RANGE_ADULT, getAgeRangeCodes(), OUTCOME_SUTBL,
                activityId);

        Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        assignmentMismatches.add(mismatch);
        assignmentMismatches.add(mismatch2);
        assignmentMismatches.add(mismatch3);

        // build HousingLocationAssignType
        Date assignedDate = new Date();
        HousingLocationAssignType housingAssign = new HousingLocationAssignType(activityId, supervisionId, movementReason, locationTo, assignedBy, assignedDate,
                assignmentMismatches, OUTCOME_SUTBL, movementComment, facilityId);

        // call assignHousingLocation
        HousingBedMgmtActivityType ret = service.assignHousingLocation(uc, housingAssign);

        assert (ret != null);

        // The test result is that main instance was created, but the child table assignmentMismatch was ignored by hibernate without any exception, because this input data was wrong for creation. 
    }

    //@Test
    public void initData() {

        service.deleteAll(uc);

        //To be removed if default meta no link between these two reference codes.
        //Delete reference code link for movementReason=OFFREQ

        loadReferenceLinks();

        //set for configuration data.
        setHousingPeriodicReviewConfig();

        /////////////////////////////////////////
    }

    private void loadReferenceLinks() {

        LinkCodeType link = new LinkCodeType();
        link.setReferenceSet(ReferenceSet.HOUSING_MOVEMENT_REASON.value());
        link.setReferenceCode("OFFREQ");
        link.setLinkedReferenceSet(ReferenceSet.APPROVAL_REQUIRED.value());
        link.setLinkedReferenceCode("NAPP");

        try {
            refService.deleteLink(uc, link);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //Create new reference code link with Approval Required for movementReason=OFFREQ
        link = new LinkCodeType();
        link.setReferenceSet(ReferenceSet.HOUSING_MOVEMENT_REASON.value());
        link.setReferenceCode("OFFREQ");
        link.setLinkedReferenceSet(ReferenceSet.APPROVAL_REQUIRED.value());
        link.setLinkedReferenceCode("APP");

        try {
            refService.createLink(uc, link);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private Long createSupervision() {
        return supTest.createSupervision(facilityId, true);
    }

    /**
     * Use to create housing assignment activity record
     *
     * @param offenderId
     * @return
     */
    private HousingBedMgmtActivityType assignHousingLocation(Long offenderId, Long locationId, String movementReasonCode) {

        String movementReason = movementReasonCode;

        String movementComment = "Housing Assinged comments" + offenderId;

        Long supervisionId = offenderId;
        Long locationTo = locationId;
        //Long assignedBy = offenderId;		
        //Long facilityId = 1L;

        // build HousingAttributeMismatch for assignment
        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, getSecurityLevelCodes(),
                OUTCOME_SUTBL, activityId);

        Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        assignmentMismatches.add(mismatch);

        // build HousingLocationAssignType
        Date assignedDate = new Date();
        HousingLocationAssignType housingAssign = new HousingLocationAssignType(activityId, supervisionId, movementReason, locationTo, assignedBy, assignedDate,
                assignmentMismatches, OUTCOME_SUTBL, movementComment, facilityId);

        // call assignHousingLocation
        HousingBedMgmtActivityType ret = service.assignHousingLocation(uc, housingAssign);

        return ret;

    }

    /**
     * Use to create housing assignment activity record
     *
     * @param offenderId
     * @return
     */
    private HousingBedMgmtActivityType assignHousingLocation(Long offenderId, Long locationId) {

        String movementReason = MOVEMENT_REASON_MED;

        // call assignHousingLocation
        HousingBedMgmtActivityType ret = assignHousingLocation(offenderId, locationId, movementReason);

        return ret;

    }

    /**
     * Use to create housing assignment activity record
     *
     * @param offenderId
     * @return
     */
    private HousingBedMgmtActivityType assignHousingLocation(Long offenderId) {

        String movementReason = MOVEMENT_REASON_MED;

        String movementComment = "Housing Assinged comments" + offenderId;

        Long supervisionId = offenderId;

        // build HousingAttributeMismatch for assignment
        Set<HousingAttributeMismatchType> assignmentMismatches = getAllMismatches();

        // build HousingLocationAssignType
        Date assignedDate = new Date();
        Long locId = filTest.getCell2();
        HousingLocationAssignType housingAssign = new HousingLocationAssignType(activityId, supervisionId, movementReason, locId, assignedBy, assignedDate,
                assignmentMismatches, OUTCOME_SUTBL, movementComment, facilityId);

        // call assignHousingLocation
        HousingBedMgmtActivityType ret = service.assignHousingLocation(uc, housingAssign);

        return ret;

    }

    /**
     * Use to create the ChangeRequest housing activity only.
     *
     * @param offenderId
     * @param locationId
     * @param movementReason - movementReason will determine if this change request are approval required or not.
     * @return
     */
    private HousingBedMgmtActivityType createChangeRequest(Long offenderId, Long locationId, String movementReason, boolean isReservationFlag) {

        assignHousingLocation(offenderId, locationId + 1);

        // build housing bed mgmt activity properties
        //String movementReason = MOVEMENT_REASON_OFFREQ;	//Approval Required
        //String movementReason = MOVEMENT_REASON_MED; //Approval Not Required
        Long supervisionId = offenderId;

        // build static association properties
        Long locationRequested = locationId;
        //Long issuedBy = offenderId;

        // build code type properties
        String crPriority = "HIGH";
        String crType = "INTRA";

        // build OffenderHousingAttribute
        String securityLevel = ASSESSMENT_RESULT_MIN;
        String gender = SEX_F;

        String sentenceStatuses = CASE_SENTENCE_STATUS_SENT;

        OffenderHousingAttributeType housingAttributesRequired = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatuses, null, null);

        String movementComment = "createChangeRequest comments" + offenderId;

        // build HousingLocationCRType        
        HousingLocationCRType housingLocationCR = new HousingLocationCRType(activityId, supervisionId, movementReason, locationRequested, issuedBy, crPriority, crType,
                isReservationFlag, housingAttributesRequired, null, null, movementComment);

        HousingBedMgmtActivityType retCreate = service.createChangeRequest(uc, housingLocationCR);

        return retCreate;

    }

    /**
     * Use to create the ChangeRequest housing activity only.
     *
     * @param offenderId
     * @param movementReason - movementReason will determine if this change request are approval required or not.
     * @return
     */
    private HousingBedMgmtActivityType createChangeRequest(Long offenderId, String movementReason, boolean isReservationFlag) {

        assignHousingLocation(offenderId, filTest.getCell4());

        // build housing bed mgmt activity properties
        //String movementReason = MOVEMENT_REASON_OFFREQ;	//Approval Required
        //String movementReason = MOVEMENT_REASON_MED; //Approval Not Required
        Long supervisionId = offenderId;

        // build static association properties
        //Long locationRequested = 1L;
        //Long issuedBy = 1L;

        // build code type properties
        String crPriority = "HIGH";
        String crType = "INTRA";

        // build OffenderHousingAttribute
        String securityLevel = ASSESSMENT_RESULT_MIN;
        String gender = SEX_M;

        String sentenceStatuses = CASE_SENTENCE_STATUS_SENT;

        Set<String> locationProperties = new HashSet<String>();
        locationProperties.add(HEALTH_ISSUES_D);
        locationProperties.add(HEALTH_ISSUES_DI);

        Set<String> categories = new HashSet<String>();
        categories.add(CAUTION_CODE_S);
        categories.add(CAUTION_CODE_V);

        OffenderHousingAttributeType housingAttributesRequired = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatuses, locationProperties,
                categories);

        /////////////////////////////

        String movementComment = "createChangeRequest comments" + offenderId;

        // build HousingLocationCRType        
        Long locId = filTest.getCell3();
        HousingLocationCRType housingLocationCR = new HousingLocationCRType(activityId, supervisionId, movementReason, locId, issuedBy, crPriority, crType,
                isReservationFlag, housingAttributesRequired, null, null, movementComment);

        HousingBedMgmtActivityType retCreate = service.createChangeRequest(uc, housingLocationCR);

        return retCreate;

    }

    private Set<CodeType> getSecurityLevelCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(SECURITY_LEVEL_MIN);
        ret.add(SECURITY_LEVEL_MAX);
        return ret;
    }

    private Set<CodeType> getGenderCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(OFFENDER_GENDER_F);
        ret.add(OFFENDER_GENDER_M);
        return ret;
    }

    private Set<CodeType> getAgeRangeCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(AGE_RANGE_ADULT);
        ret.add(AGE_RANGE_JUVENILE);
        return ret;
    }

    private Set<CodeType> getSentenceStatusCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(SENTENCE_STATUS_SENTC);
        ret.add(SENTENCE_STATUS_UNSTC);
        return ret;
    }

    private Set<CodeType> getOffenderCategoryCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(OFFENDER_CATEGORY_SUCD);
        ret.add(OFFENDER_CATEGORY_PRCUS);
        return ret;
    }

    private Set<CodeType> getLocationPropertyCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(LOCATION_PROPERTY_HSENS);
        ret.add(LOCATION_PROPERTY_SHOWER);
        ret.add(LOCATION_PROPERTY_WHEELCHAIR);
        return ret;
    }

    private Set<HousingAttributeMismatchType> getAllMismatches() {

        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, getSecurityLevelCodes(),
                OUTCOME_SUTBL, activityId);
        HousingAttributeMismatchType mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_GENDER, CODE_SEX_F, getGenderCodes(), OUTCOME_SUTBL, activityId);
        HousingAttributeMismatchType mismatch3 = new HousingAttributeMismatchType(null, SUITABILITY_AGE, AGE_RANGE_JUVENILE, getAgeRangeCodes(), OUTCOME_WRNG,
                activityId);
        HousingAttributeMismatchType mismatch4 = new HousingAttributeMismatchType(null, SUITABILITY_SENTENCE, CODE_CASE_SENTENCE_STATUS_SENT, getSentenceStatusCodes(),
                OUTCOME_WRNG, activityId);
        HousingAttributeMismatchType mismatch5 = new HousingAttributeMismatchType(null, SUITABILITY_OFFCAT, CODE_CAUTION_CODE_S, getOffenderCategoryCodes(), OUTCOME_APPR,
                activityId);
        HousingAttributeMismatchType mismatch6 = new HousingAttributeMismatchType(null, SUITABILITY_LOCPROP, CODE_HEALTH_ISSUES_D, getLocationPropertyCodes(),
                OUTCOME_NOTPD, activityId);

        Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        assignmentMismatches.add(mismatch);
        assignmentMismatches.add(mismatch2);
        assignmentMismatches.add(mismatch3);
        assignmentMismatches.add(mismatch4);
        assignmentMismatches.add(mismatch5);
        assignmentMismatches.add(mismatch6);

        return assignmentMismatches;

    }

    private Set<HousingAttributeMismatchType> getMismatches() {

        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, getSecurityLevelCodes(),
                OUTCOME_SUTBL, activityId);

        HousingAttributeMismatchType mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_GENDER, CODE_SEX_F, getGenderCodes(), OUTCOME_SUTBL, activityId);

        HousingAttributeMismatchType mismatch3 = new HousingAttributeMismatchType(null, SUITABILITY_AGE, AGE_RANGE_ADULT, getAgeRangeCodes(), OUTCOME_SUTBL, activityId);
        Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        assignmentMismatches.add(mismatch);
        assignmentMismatches.add(mismatch2);
        assignmentMismatches.add(mismatch3);

        return assignmentMismatches;

    }

    private void verifySetHousingPeriodicReviewConfigNegative(HousingPeriodicReviewType housingPeriodicReview, Class<?> expectionClazz) {
        try {
            HousingPeriodicReviewType ret = service.setHousingPeriodicReviewConfig(uc, housingPeriodicReview);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifySetHousingCRPrioritiesNegative(Set<HousingCRPriorityType> housingCRPriorities, Class<?> expectionClazz) {
        try {
            HousingCRPrioritiesReturnType ret = service.setHousingCRPriorities(uc, housingCRPriorities);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveReservationsByLocationsNegative(Set<Long> locationTos, Class<?> expectionClazz) {
        try {
            LocationHousingChangeRequestsReturnType ret = service.retrieveReservationsByLocations(uc, locationTos);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveReservationsByOffendersNegative(Set<Long> supervisionIds, Class<?> expectionClazz) {
        try {
            HousingChangeRequestsReturnType ret = service.retrieveReservationsByOffenders(uc, supervisionIds);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveCurrentAssignmentsByLocationsNegative(Set<Long> locationTos, Class<?> expectionClazz) {
        try {
            LocationHousingAssignmentsReturnType ret = service.retrieveCurrentAssignmentsByLocations(uc, locationTos);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveDueChangeAssignmentsNegative(Set<Long> supervisionIds, Class<?> expectionClazz) {
        try {
            OffenderHousingAssignmentsReturnType ret = service.retrieveCurrentAssignmentsByOffenders(uc, supervisionIds);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveDueChangeAssignmentsNegative(Set<Long> requestedLocations, Long daysUpcomingWithin, Class<?> expectionClazz) {
        try {
            OffenderHousingAssignmentsReturnType ret = service.retrieveDueChangeAssignments(uc, requestedLocations, daysUpcomingWithin);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveMishousedAssignmentsNegative(Set<Long> requestedLocations, Boolean newMishoused, Class<?> expectionClazz) {
        try {
            OffenderHousingAssignmentsReturnType ret = service.retrieveMishousedAssignments(uc, requestedLocations, newMishoused);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveAssignmentsByLocationNegative(Long locationTo, Long historyDays, Class<?> expectionClazz) {
        try {
            OffenderHousingAssignmentsReturnType ret = service.retrieveAssignmentsByLocation(uc, locationTo, historyDays);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveAssignmentsByOffenderNegative(Long supervisionId, Long historyDays, Class<?> expectionClazz) {
        try {
            OffenderHousingAssignmentsReturnType ret = service.retrieveAssignmentsByOffender(uc, supervisionId, historyDays);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyGetNegative(Long housingActivityId, Class<?> expectionClazz) {
        try {
            HousingBedMgmtActivityType ret = service.get(uc, housingActivityId);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifySearchChangeRequestNegative(HousingChangeRequestSearchType search, Class<?> expectionClazz) {
        try {
            HousingChangeRequestsReturnType ret = service.searchChangeRequest(uc, search, 0L, 200L, null);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifySearchNegative(Set<Long> subsetSearch, HousingBedMgmtActivitySearchType search, Class<?> expectionClazz) {
        try {
            HousingBedMgmtActivitiesReturnType ret = service.search(uc, subsetSearch, search, 0L, 200L, null);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyApplyChangeRequestNegative(Long housingActivityId, Long assignedBy, Long locationTo, Set<HousingAttributeMismatchType> assignmentMismatches,
            String overallOutcome, Date assignedDate, Class<?> expectionClazz) {
        try {
            HousingBedMgmtActivityType ret = service.applyChangeRequest(uc, housingActivityId, assignedBy, locationTo, assignmentMismatches, overallOutcome, assignedDate,
                    null);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyValidateMismatchNegative(HousingUpdateType dto, Class<?> expectionClazz) {
        try {
            HousingBedMgmtActivityType ret = service.validateMismatch(uc, dto);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyCancelChangeRequestNegative(CancelChangeRequestType dto, Class<?> expectionClazz) {
        try {
            HousingBedMgmtActivityType ret = service.cancelChangeRequest(uc, dto);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyApproveChangeRequestNegative(ApproveChangeRequestType dto, Class<?> expectionClazz) {
        try {
            HousingBedMgmtActivityType ret = service.approveChangeRequest(uc, dto);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyUpdateHousingMismatchesNegative(Long housingActivityId, Set<HousingAttributeMismatchType> housingAttributeMismatches, String overallOutcome,
            Class<?> expectionClazz) {
        try {
            HousingBedMgmtActivityType ret = service.updateHousingMismatches(uc, housingActivityId, housingAttributeMismatches, overallOutcome);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyUpdateNegative(HousingUpdateType dto, Class<?> expectionClazz) {
        try {
            HousingBedMgmtActivityType ret = service.update(uc, dto);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyAssignHousingLocationNegative(HousingLocationAssignType dto, Class<?> expectionClazz) {
        try {
            HousingBedMgmtActivityType ret = service.assignHousingLocation(uc, dto);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyUnassignHousingLocationNegative(HousingLocationUnassignType dto, Class<?> expectionClazz) {
        try {
            HousingBedMgmtActivityType ret = service.unassignHousingLocation(uc, dto);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyCreateChangeRequestNegative(HousingLocationCRType dto, Class<?> expectionClazz) {
        try {
            HousingBedMgmtActivityType ret = service.createChangeRequest(uc, dto);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private HousingUpdateType getHousingUpdateType(Long housingActivityId, String crPriority, Long locationTo, String movementComment) {

        HousingUpdateType ret = new HousingUpdateType();
        ret.setHousingActivityId(housingActivityId);
        ret.setCrPriority(crPriority);
        ret.setLocationTo(locationTo);
        ret.setMovementComment(movementComment);
        return ret;

    }

    private HousingUpdateType getHousingUpdateType(Long housingActivityId, String movementComment) {
        HousingUpdateType ret = new HousingUpdateType();
        ret.setHousingActivityId(housingActivityId);
        ret.setMovementComment(movementComment);
        return ret;
    }

    private CancelChangeRequestType getCancelChangeRequestType(Long housingActivityId, HousingActivityClosureType housingActivityClosure) {
        CancelChangeRequestType ret = new CancelChangeRequestType();
        ret.setHousingActivityId(housingActivityId);
        ret.setHousingActivityClosure(housingActivityClosure);
        return ret;
    }

    private ApproveChangeRequestType getApproveChangeRequestType(Long housingActivityId, HousingChangeRequestApprovalType housingCRApproval) {
        ApproveChangeRequestType ret = new ApproveChangeRequestType();
        ret.setHousingActivityId(housingActivityId);
        ret.setHousingCRApproval(housingCRApproval);
        return ret;
    }
}
