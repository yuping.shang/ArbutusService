package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.OffenderHousingAttributeType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class OffenderHousingAttributeTypeTest {

    @Test
    public void isValid() {

        OffenderHousingAttributeType data = new OffenderHousingAttributeType();
        assert (validate(data) == false);

        data = new OffenderHousingAttributeType(null, null, null, null, null, null);
        assert (validate(data) == false);

        String securityLevel = "TestCode1";
        data = new OffenderHousingAttributeType(securityLevel, null, null, null, null, null);
        assert (validate(data) == false);

        String gender = "TestCode2";
        data = new OffenderHousingAttributeType(securityLevel, gender, null, null, null, null);
        assert (validate(data) == false);

        data = new OffenderHousingAttributeType(securityLevel, gender, 20L, null, null, null);
        assert (validate(data) == false);

        String sentenceStatus = null;
        data = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatus, null, null);
        assert (validate(data) == false);

        sentenceStatus = "TestCode3";
        data = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatus, null, null);
        assert (validate(data) == true);

        Set<String> locationProperty = new HashSet<String>();
        locationProperty.add("TestCode3");
        data = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatus, locationProperty, null);
        assert (validate(data) == true);

        Set<String> category = new HashSet<String>();
        category.add("TestCode3");
        data = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatus, locationProperty, category);
        assert (validate(data) == true);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
