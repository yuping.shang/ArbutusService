package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import java.util.Date;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingChangeRequestSearchType;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingChangeRequestSearchTypeTest {

    private static Long approvedBy = 2L;

    @Test
    public void isValid() {

        HousingChangeRequestSearchType data = new HousingChangeRequestSearchType();
        assert (validate(data) == false);

        data = new HousingChangeRequestSearchType();
        data.setApprovedBy(approvedBy);
        assert (validate(data) == true);

        data = new HousingChangeRequestSearchType();
        data.setCRPriority(null);
        assert (validate(data) == false);

        data = new HousingChangeRequestSearchType();
        data.setCRPriority("TestCode1");
        assert (validate(data) == true);

        //toDate cannot before the fromDate
        Date dateFrom = BeanHelper.createDate();
        Date dateTo = BeanHelper.getPastDate(dateFrom, 1);
        data = new HousingChangeRequestSearchType();
        data.setClosedDateFrom(dateFrom);
        data.setClosedDateTo(dateTo);
        assert (validate(data) == false);

        data = new HousingChangeRequestSearchType();
        data.setApprovedDateFrom(dateFrom);
        data.setApprovedDateTo(dateTo);
        assert (validate(data) == false);

        data = new HousingChangeRequestSearchType();
        data.setIssuedDateFrom(dateFrom);
        data.setIssuedDateTo(dateTo);
        assert (validate(data) == false);

        data = new HousingChangeRequestSearchType();
        data.setValidationFlag(true);
        assert (validate(data) == true);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validateSearchType(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
