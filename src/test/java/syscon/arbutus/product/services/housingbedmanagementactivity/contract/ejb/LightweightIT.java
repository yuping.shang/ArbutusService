package syscon.arbutus.product.services.housingbedmanagementactivity.contract.ejb;

import java.util.List;

import org.junit.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseTestJMockIt;
import syscon.arbutus.product.services.common.HibernateUtil;

/**
 * Created by os on 25/06/15.
 */
public class LightweightIT extends BaseTestJMockIt {

    private IT superIT;

    @BeforeClass
    public void beforeClass() {
        super.beforeClass();

        superIT = new IT(housingService, referenceDataService, staffTest, activityTest, facilityTest, supervisionTest, facilityInternalLocationTest, personIdentityTest,
                personTest);

        superIT.initOtherServiceData();
        HibernateUtil.commitAndClose();
    }

    @AfterClass
    public void afterClass() {

        super.afterClass();

        housingService.deleteAll(userContext);

        facilityInternalLocationTest.deleteAll();
        supervisionTest.deleteAll();
        activityTest.deleteAll();
        staffTest.deleteAll();
        facilityTest.deleteAll();
        personIdentityTest.deleteAll();
        personTest.deleteAll();
        HibernateUtil.commitAndClose();
    }

    @Test
    public void assignHousingLocationNegative() {
        superIT.assignHousingLocationNegative();
    }

    @Test
    public void assignHousingLocationPositive() {
        superIT.assignHousingLocationPositive();
    }

    @Test
    public void unassignHousingLocationNegative() {
        superIT.unassignHousingLocationNegative();
    }

    @Test
    public void unassignHousingLocationPositive() {
        superIT.unassignHousingLocationPositive();
    }

    @Test
    public void createChangeRequestNegative() {
        superIT.createChangeRequestNegative();
    }

    @Test
    public void createChangeRequestPositive() {
        superIT.createChangeRequestPositive();
    }

    @Test
    public void updateNegative() {
        superIT.updateNegative();
    }

    @Test
    public void updatePositive() {
        superIT.updatePositive();
    }

    @Test
    public void updateHousingMismatchesNegative() {
        superIT.updateHousingMismatchesNegative();
    }

    @Test
    public void updateHousingMismatchesPositive() {
        superIT.updateHousingMismatchesPositive();
    }

    @Test
    public void approveChangeRequestNegative() {
        superIT.approveChangeRequestNegative();
    }

    @Test
    public void approveChangeRequestPositive() {
        superIT.approveChangeRequestPositive();
    }

    @Test
    public void cancelChangeRequestNegative() {
        superIT.cancelChangeRequestNegative();
    }

    @Test
    public void cancelChangeRequestPositive() {
        superIT.cancelChangeRequestPositive();
    }

    @Test
    public void validteMismatchNegative() {
        superIT.validteMismatchNegative();
    }

    @Test
    public void validateMismatchPositive() {
        superIT.validateMismatchPositive();
    }

    @Test
    public void applyChangeRequestNegative() {
        superIT.applyChangeRequestNegative();
    }

    @Test
    public void applyChangeRequestPositive() {
        superIT.applyChangeRequestPositive();
    }

    @Test(enabled = false)
    public List<Long> searchCreation() {
        return superIT.searchCreation();
    }

    @Test
    public void searchNegative() {
        superIT.searchNegative();
    }

    @Test
    public void searchPositive() {
        superIT.searchPositive();
    }

    @Test(enabled = false)
    public void searchByWildCard() {
        superIT.searchByWildCard();
    }

    @Test(enabled = false)
    public void searchSubSet() {
        superIT.searchSubSet();
    }

    @Test
    public void searchChangeRequestNegative() {
        superIT.searchChangeRequestNegative();
    }

    @Test(enabled = false)
    public void searchChangeRequestPositive(List<Long> supIds) {
        superIT.searchChangeRequestPositive(supIds);
    }

    @Test
    public void retrieveAssignmentsByOffenderNegative() {
        superIT.retrieveAssignmentsByOffenderNegative();
    }

    @Test
    public void retrieveAssignmentsByOffenderPositive() {
        superIT.retrieveAssignmentsByOffenderPositive();
    }

    @Test
    public void retrieveAssignmentsByLocationNegative() {
        superIT.retrieveAssignmentsByLocationNegative();
    }

    @Test(enabled = false)
    public void retrieveAssignmentsByLocationPositive() {
        superIT.retrieveAssignmentsByLocationPositive();
    }

    @Test
    public void retrieveMishousedAssignmentsNegative() {
        superIT.retrieveMishousedAssignmentsNegative();
    }

    @Test(enabled = false)
    public void retrieveMishousedAssignmentsPositive() {
        superIT.retrieveMishousedAssignmentsPositive();
    }

    @Test
    public void retrieveDueChangeAssignmentsNegative() {
        superIT.retrieveDueChangeAssignmentsNegative();
    }

    @Test(enabled = false)
    public void retrieveDueChangeAssignmentsPositive() {
        superIT.retrieveDueChangeAssignmentsPositive();
    }

    @Test(enabled = false)
    public List<Long> retrieveMultipleCreation() {
        return superIT.retrieveMultipleCreation();
    }

    @Test
    public void retrieveCurrentAssignmentsByOffendersNegative() {
        superIT.retrieveCurrentAssignmentsByOffendersNegative();
    }

    @Test(enabled = false)
    public void retrieveCurrentAssignmentsByOffendersPositive2() {
        superIT.retrieveCurrentAssignmentsByOffendersPositive2();
    }

    @Test
    public void retrieveCurrentAssignmentsByOffendersPositive() {
        superIT.retrieveCurrentAssignmentsByOffendersPositive();
    }

    @Test
    public void retrieveCurrentAssignmentsByLocationsNegative() {
        superIT.retrieveCurrentAssignmentsByLocationsNegative();
    }

    @Test(enabled = false)
    public void retrieveCurrentAssignmentsByLocationsPositive() {
        superIT.retrieveCurrentAssignmentsByLocationsPositive();
    }

    @Test(enabled = false)
    public List<Long> retrieveMultipleChangeRequestsCreation() {
        return superIT.retrieveMultipleChangeRequestsCreation();
    }

    @Test
    public void retrieveReservationsByOffendersNegative() {
        superIT.retrieveReservationsByOffendersNegative();
    }

    @Test
    public void retrieveReservationsByOffendersPositive() {
        superIT.retrieveReservationsByOffendersPositive();
    }

    @Test
    public void retrieveReservationsByLocationsNegative() {
        superIT.retrieveReservationsByLocationsNegative();
    }

    @Test(enabled = false)
    public void retrieveReservationsByLocationsPositive() {
        superIT.retrieveReservationsByLocationsPositive();
    }

    @Test
    public void setHousingPeriodicReviewConfig() {
        superIT.setHousingPeriodicReviewConfig();
    }

    @Test
    public void retrieveHousingPeriodicReviewConfig() {
        superIT.retrieveHousingPeriodicReviewConfig();
    }

    @Test
    public void setHousingCRPriorities() {
        superIT.setHousingCRPriorities();
    }

    @Test
    public void retrieveHousingCRPriorities() {
        superIT.retrieveHousingCRPriorities();
    }

    @Test
    public void deletePositive() {
        superIT.deletePositive();
    }

    @Test
    public void deleteAll() {
        superIT.deleteAll();
    }

    @Test
    public void getNegative() {
        superIT.getNegative();
    }

    @Test
    public void getPositive() {
        superIT.getPositive();
    }

    @Test
    public void getAll() {
        superIT.getAll();
    }

    @Test
    public void getCount() {
        superIT.getCount();
    }

    @Test
    public void getStampPositive() {
        superIT.getStampPositive();
    }

    @Test
    public void getNiem() {
        superIT.getNiem();
    }

    @Test
    public void testCRDefect966() {
        superIT.testCRDefect966();
    }

    @Test(enabled = false)
    public void testCR1962() {
        superIT.testCR1962();
    }

    @Test(enabled = false)
    public void testDefect1005() {
        superIT.testDefect1005();
    }

    @Test
    public void initData() {
        superIT.initData();
    }
}