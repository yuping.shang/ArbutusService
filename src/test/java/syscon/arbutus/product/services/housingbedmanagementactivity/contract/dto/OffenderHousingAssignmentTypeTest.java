package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import java.util.Date;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingAssignmentType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.OffenderHousingAssignmentType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class OffenderHousingAssignmentTypeTest {

    private static Long offender = 1L;
    private static Long facility = 1L;
    private static Long locationFrom = 1L;
    private static Long locationTo = 1L;
    private static Long assignedBy = 1L;
    private static String overallOutcome = "Test";

    @Test
    public void isValid() {

        OffenderHousingAssignmentType data = new OffenderHousingAssignmentType();
        assert (validate(data) == false);

        data = new OffenderHousingAssignmentType(null, null, null, null, null, null);
        assert (validate(data) == false);

        data = new OffenderHousingAssignmentType(1L, null, null, null, null, null);
        assert (validate(data) == false);

        data = new OffenderHousingAssignmentType(1L, offender, null, null, null, null);
        assert (validate(data) == false);

        String code1 = "TestCode1";
        data = new OffenderHousingAssignmentType(1L, offender, null, code1, null, null);
        assert (validate(data) == false);

        HousingAssignmentType housingAssignment = new HousingAssignmentType();
        data = new OffenderHousingAssignmentType(1L, offender, null, code1, housingAssignment, null);
        assert (validate(data) == false);

        // failed because facilty is null.
        housingAssignment = new HousingAssignmentType(-1L, locationFrom, locationTo, assignedBy, new Date(), new Date(), null, overallOutcome);
        data = new OffenderHousingAssignmentType(1L, offender, null, code1, housingAssignment, null);
        assert (validate(data) == false);

        housingAssignment = new HousingAssignmentType(-1L, locationFrom, locationTo, assignedBy, new Date(), new Date(), null, overallOutcome);
        data = new OffenderHousingAssignmentType(1L, offender, facility, code1, housingAssignment, null);
        assert (validate(data) == true);

        //Wrong Code
        code1 = "";
        data = new OffenderHousingAssignmentType(1L, offender, null, code1, housingAssignment, null);
        assert (validate(data) == false);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
