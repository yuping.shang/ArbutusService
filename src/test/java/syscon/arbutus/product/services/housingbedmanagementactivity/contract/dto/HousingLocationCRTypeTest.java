package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.contract.dto.AssociationType;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingAttributeMismatchType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingLocationCRType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.OffenderHousingAttributeType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingLocationCRTypeTest {

    private static Long offender = 1L;
    private static Long issuedBy = 2L;
    private static Long locationRequested = 2L;

    @Test
    public void isValid() {

        HousingLocationCRType data = new HousingLocationCRType();
        assert (validate(data) == false);

        data = new HousingLocationCRType(null, null, null, null, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        data = new HousingLocationCRType(1L, offender, null, null, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        String movementReason = "TestCode";
        data = new HousingLocationCRType(1L, offender, movementReason, null, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        data = new HousingLocationCRType(1L, offender, movementReason, locationRequested, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        data = new HousingLocationCRType(1L, offender, movementReason, locationRequested, issuedBy, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        String crPriority = "TestCode";
        data = new HousingLocationCRType(1L, offender, movementReason, locationRequested, issuedBy, crPriority, null, null, null, null, null, null);
        assert (validate(data) == false);

        //crType
        String crType = "TestCode";
        data = new HousingLocationCRType(1L, offender, movementReason, locationRequested, issuedBy, crPriority, crType, null, null, null, null, null);
        assert (validate(data) == false);

        data = new HousingLocationCRType(1L, offender, movementReason, locationRequested, issuedBy, crPriority, crType, false, null, null, null, null);
        assert (validate(data) == false);

        //housingAttributesRequired
        OffenderHousingAttributeType housingAttributesRequired = new OffenderHousingAttributeType();
        data = new HousingLocationCRType(1L, offender, movementReason, locationRequested, issuedBy, crPriority, crType, false, housingAttributesRequired, null, null,
                null);
        assert (validate(data) == false);

        Set<HousingAttributeMismatchType> mismatchesWithOffender = new HashSet<HousingAttributeMismatchType>();
        mismatchesWithOffender.add(null);
        data = new HousingLocationCRType(1L, offender, movementReason, locationRequested, issuedBy, crPriority, crType, false, housingAttributesRequired,
                mismatchesWithOffender, null, null);
        assert (validate(data) == false);

        //housingAttributesRequired
        String securityLevel = "TestCode1";
        String gender = "TestCode2";
        String sentenceStatus = "TestCode3";
        housingAttributesRequired = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatus, null, null);

        mismatchesWithOffender = new HashSet<HousingAttributeMismatchType>();
        data = new HousingLocationCRType(1L, offender, movementReason, locationRequested, issuedBy, crPriority, crType, false, housingAttributesRequired,
                mismatchesWithOffender, null, null);
        assert (validate(data) == true);
    }

    @Test
    public void create() {

        String movementReason = "MED"; //Approval Not Required
        Long offenderReference = 15L;

        // build static association properties
        Long locationRequested = 1L;
        Long issuedBy = 1L;

        // build code type properties
        String crPriority = "HIGH";
        String crType = "INTRA";

        // build OffenderHousingAttribute
        String securityLevel = "MIN";
        String gender = "F";

        String sentenceStatuses = "SENT";

        Set<String> locationProperties = new HashSet<String>();
        locationProperties.add("WHEELCHAIR");
        locationProperties.add("HSENS");
        locationProperties.add("SHOWER");

        Set<String> categories = new HashSet<String>();
        categories.add("SUCD");

        OffenderHousingAttributeType housingAttributesRequired = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatuses, locationProperties,
                categories);
        /////////////////////////////

        Set<AssociationType> associations = new HashSet<AssociationType>();
        associations.add(new AssociationType("ACTIVITY", 1L));

        HousingLocationCRType housingLocationCR = new HousingLocationCRType(1L, offenderReference, movementReason, locationRequested, issuedBy, crPriority, crType, true,
                housingAttributesRequired, null, null, "Movement comments");

        assert (validate(housingLocationCR) == true);
    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
