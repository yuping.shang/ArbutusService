package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingCRPriorityType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingCRPriorityTypeTest {

    @Test
    public void isValid() {

        HousingCRPriorityType data = new HousingCRPriorityType();
        assert (validate(data) == false);

        data = new HousingCRPriorityType(null, null);
        assert (validate(data) == false);

        data = new HousingCRPriorityType("", null);
        assert (validate(data) == false);

        data = new HousingCRPriorityType("TestCode", null);
        assert (validate(data) == false);

        data = new HousingCRPriorityType("TestCode", 1L);
        assert (validate(data) == true);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
