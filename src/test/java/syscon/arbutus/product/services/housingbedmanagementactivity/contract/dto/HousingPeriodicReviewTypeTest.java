package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingPeriodicReviewType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingPeriodicReviewTypeTest {

    @Test
    public void isValid() {

        HousingPeriodicReviewType data = new HousingPeriodicReviewType();
        assert (validate(data) == false);

        data = new HousingPeriodicReviewType(null, null);
        assert (validate(data) == false);

        data = new HousingPeriodicReviewType(1L, null);
        assert (validate(data) == false);

        data = new HousingPeriodicReviewType(1L, 2L);
        assert (validate(data) == false);

        data = new HousingPeriodicReviewType(3L, 1L);
        assert (validate(data) == true);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }

}
