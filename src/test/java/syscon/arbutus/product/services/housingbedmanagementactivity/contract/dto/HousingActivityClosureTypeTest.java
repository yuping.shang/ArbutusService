package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import java.util.Date;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingActivityClosureType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingActivityClosureTypeTest {

    private static Long staff1 = 1L;

    @Test
    public void isValid() {

        HousingActivityClosureType data = new HousingActivityClosureType();
        assert (validate(data) == false);

        data = new HousingActivityClosureType(null, null, null);
        assert (validate(data) == false);

        data = new HousingActivityClosureType(staff1, null, null);
        assert (validate(data) == true);

        data = new HousingActivityClosureType(staff1, new Date(), null);
        assert (validate(data) == true);

        data = new HousingActivityClosureType(staff1, new Date(), "Test Comment");
        assert (validate(data) == true);

        data = new HousingActivityClosureType(staff1, new Date(),
                "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
        assert (validate(data) == false);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
