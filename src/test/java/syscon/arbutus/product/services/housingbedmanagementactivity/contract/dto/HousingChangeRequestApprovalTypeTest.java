package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import java.util.Date;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingChangeRequestApprovalType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingChangeRequestApprovalTypeTest {

    @Test
    public void isValid() {

        HousingChangeRequestApprovalType data = new HousingChangeRequestApprovalType();
        assert (validate(data) == false);

        data = new HousingChangeRequestApprovalType(null, null, null);
        assert (validate(data) == false);

        data = new HousingChangeRequestApprovalType(1L, null, null);
        assert (validate(data) == true);

        data = new HousingChangeRequestApprovalType(1L, new Date(), null);
        assert (validate(data) == true);

        data = new HousingChangeRequestApprovalType(1L, new Date(), "Test Comment");
        assert (validate(data) == true);

        data = new HousingChangeRequestApprovalType(1L, new Date(),
                "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
        assert (validate(data) == false);
    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }

}
