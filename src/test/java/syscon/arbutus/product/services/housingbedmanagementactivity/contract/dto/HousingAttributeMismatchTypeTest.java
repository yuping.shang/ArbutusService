package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingAttributeMismatchType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingAttributeMismatchTypeTest {

    @Test
    public void isValid() {

        HousingAttributeMismatchType data = new HousingAttributeMismatchType();
        assert (validate(data) == false);

        data = new HousingAttributeMismatchType(-1L, null, null, null, null, null);
        assert (validate(data) == false);

        String code1 = "TestCode1";
        data = new HousingAttributeMismatchType(-1L, code1, null, null, null, null);
        assert (validate(data) == false);

        CodeType code2 = new CodeType("TestSet2", "TestCode2");
        data = new HousingAttributeMismatchType(-1L, code1, code2, null, null, null);
        assert (validate(data) == false);

        Set<CodeType> locCodes = new HashSet<CodeType>();
        locCodes.add(new CodeType("TestSet3", "TestCode3"));
        data = new HousingAttributeMismatchType(-1L, code1, code2, locCodes, null, null);
        assert (validate(data) == false);

        String code4 = "TestCode4";
        //test for CR966 .
        data = new HousingAttributeMismatchType(-1L, code1, null, locCodes, code4, null);
        assert (validate(data) == false);

        data = new HousingAttributeMismatchType(-1L, code1, code2, locCodes, code4, null);
        assert (validate(data) == true);

        //TODO: will add validation on code string length check.
        code4 = "012345678901234567890123456789012345678901234567890123456789012345";
        data = new HousingAttributeMismatchType(-1L, code1, code2, locCodes, code4, null);
        assert (validate(data) == true);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
