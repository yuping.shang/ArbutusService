package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingAssignmentType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingAttributeMismatchType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingAssignmentTypeTest {

    private static Long locationTo = 1L;
    private static Long assignedBy = 1L;
    private static String overallOutcome = "Test";

    @Test
    public void isValid() {

        HousingAssignmentType data = new HousingAssignmentType();
        assert (validate(data) == false);

        data = new HousingAssignmentType(-1L, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        data = new HousingAssignmentType(-1L, null, locationTo, null, null, null, null, null);
        assert (validate(data) == false);

        //failed because the overallOutcome is null
        data = new HousingAssignmentType(-1L, null, locationTo, assignedBy, null, null, null, null);
        assert (validate(data) == false);

        Date assignedDate = new Date();
        data = new HousingAssignmentType(-1L, null, locationTo, assignedBy, assignedDate, null, null, overallOutcome);
        assert (validate(data) == true);

        Set<HousingAttributeMismatchType> mismatchesWithOffender = new HashSet<HousingAttributeMismatchType>();
        data = new HousingAssignmentType(-1L, null, locationTo, assignedBy, assignedDate, null, mismatchesWithOffender, overallOutcome);
        assert (validate(data) == true);

        //Wrong mismatch
        mismatchesWithOffender = new HashSet<HousingAttributeMismatchType>();
        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(-1L, null, null, null, null, null);
        mismatchesWithOffender.add(mismatch);
        data = new HousingAssignmentType(-1L, null, locationTo, assignedBy, assignedDate, null, mismatchesWithOffender, overallOutcome);
        assert (validate(data) == false);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
