package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import java.util.Date;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingLocationUnassignType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingLocationUnassignTypeTest {

    private static Long offender = 1L;
    private static Long unassignedBy = 1L;

    @Test
    public void isValid() {

        HousingLocationUnassignType data = new HousingLocationUnassignType();
        assert (validate(data) == false);

        data = new HousingLocationUnassignType(null, null, null, null, null, null);
        assert (validate(data) == false);

        data = new HousingLocationUnassignType(1L, offender, null, null, null, null);
        assert (validate(data) == false);

        String movementReason = "TestCode";
        data = new HousingLocationUnassignType(1L, offender, movementReason, null, null, null);
        assert (validate(data) == false);

        //failed bacause the unassignedDate is null
        data = new HousingLocationUnassignType(1L, offender, movementReason, unassignedBy, null, null);
        assert (validate(data) == false);

        data = new HousingLocationUnassignType(1L, offender, movementReason, unassignedBy, new Date(), null);
        assert (validate(data) == true);
    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
