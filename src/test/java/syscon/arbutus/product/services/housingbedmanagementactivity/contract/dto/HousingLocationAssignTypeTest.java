package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingAttributeMismatchType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingLocationAssignType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingLocationAssignTypeTest {

    private static Long offender = 1L;
    private static Long locationTo = 1L;
    private static Long assignedBy = 1L;
    private static Long facility = 1L;
    private static String overallOutcome = "Test";

    @Test
    public void isValid() {

        HousingLocationAssignType data = new HousingLocationAssignType();
        assert (validate(data) == false);

        data = new HousingLocationAssignType(null, null, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        data = new HousingLocationAssignType(1L, offender, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        String movementReason = "TestCode";
        data = new HousingLocationAssignType(1L, offender, movementReason, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        data = new HousingLocationAssignType(1L, offender, movementReason, locationTo, null, null, null, null, null, null);
        assert (validate(data) == false);

        data = new HousingLocationAssignType(1L, offender, movementReason, locationTo, null, null, null, null, null, facility);
        assert (validate(data) == false);

        data = new HousingLocationAssignType(1L, offender, movementReason, locationTo, assignedBy, null, null, null, null, facility);
        assert (validate(data) == false);

        //failed bacause the assignedDate is null
        data = new HousingLocationAssignType(1L, offender, movementReason, locationTo, assignedBy, null, null, overallOutcome, null, facility);
        assert (validate(data) == false);

        Date assignedDate = new Date();
        data = new HousingLocationAssignType(1L, offender, movementReason, locationTo, assignedBy, assignedDate, null, overallOutcome, null, facility);
        assert (validate(data) == true);

        Set<HousingAttributeMismatchType> mismatchesWithOffender = new HashSet<HousingAttributeMismatchType>();
        data = new HousingLocationAssignType(1L, offender, movementReason, locationTo, assignedBy, assignedDate, mismatchesWithOffender, overallOutcome, null, facility);
        assert (validate(data) == true);

        //Wrong mismatch
        mismatchesWithOffender = new HashSet<HousingAttributeMismatchType>();
        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(-1L, null, null, null, null, null);
        mismatchesWithOffender.add(mismatch);
        data = new HousingLocationAssignType(1L, offender, movementReason, locationTo, assignedBy, assignedDate, mismatchesWithOffender, overallOutcome, null, facility);
        assert (validate(data) == false);
    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
