package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import java.util.Date;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingBedMgmtActivitySearchType;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingBedMgmtActivitySearchTypeTest {

    @Test
    public void isValid() {

        HousingBedMgmtActivitySearchType data = new HousingBedMgmtActivitySearchType();
        assert (validate(data) == false);

        data = new HousingBedMgmtActivitySearchType();
        data.setActiveFlag(true);
        assert (validate(data) == true);

        data = new HousingBedMgmtActivitySearchType();
        data.setActivityState(null);
        assert (validate(data) == false);

        data = new HousingBedMgmtActivitySearchType();
        data.setActivityState("TestCode");
        assert (validate(data) == true);

        //toDate cannot before the fromDate
        Date dateFrom = BeanHelper.createDate();
        Date dateTo = BeanHelper.getPastDate(dateFrom, 1);
        data = new HousingBedMgmtActivitySearchType();
        data.setAssignedDateFrom(dateFrom);
        data.setAssignedDateTo(dateTo);
        assert (validate(data) == false);

        data = new HousingBedMgmtActivitySearchType();
        data.setUnassignedDateFrom(dateFrom);
        data.setUnassignedDateTo(dateTo);
        assert (validate(data) == false);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validateSearchType(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
