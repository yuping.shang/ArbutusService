package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import java.util.Date;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.ChangeRequestWaitlistType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingChangeRequestType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.OffenderHousingAttributeType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class ChangeRequestWaitlistTypeTest {

    private static Long offender = 1L;
    private static Long facility = 1L;
    private static Long issuedBy = 2L;
    private static Long locationRequested = 2L;

    @Test
    public void isValid() {

        ChangeRequestWaitlistType data = new ChangeRequestWaitlistType();
        assert (validate(data) == false);

        data = new ChangeRequestWaitlistType(null, null, null, null, null, null);
        assert (validate(data) == false);

        //housingActivityId
        data = new ChangeRequestWaitlistType(1L, null, null, null, null, null);
        assert (validate(data) == false);

        //offenderReference
        data = new ChangeRequestWaitlistType(1L, offender, null, null, null, null);
        assert (validate(data) == false);

        //movementReason
        String movementReason = "TestCode";
        data = new ChangeRequestWaitlistType(1L, offender, null, movementReason, null, null);
        assert (validate(data) == false);

        //housingChangeRequest
        HousingChangeRequestType housingChangeRequest = new HousingChangeRequestType();
        data = new ChangeRequestWaitlistType(1L, offender, null, movementReason, housingChangeRequest, null);
        assert (validate(data) == false);

        //create OffenderHousingAttributeType
        OffenderHousingAttributeType housingAttributesRequired = new OffenderHousingAttributeType();
        String securityLevel = "TestCode1";
        String gender = "TestCode2";
        String sentenceStatus = "TestCode3";
        housingAttributesRequired = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatus, null, null);

        //create HousingChangeRequestType
        String crPriority = "TestCode";
        String crType = "TestCode";

        //facility is null, failed.
        housingChangeRequest = new HousingChangeRequestType(-1L, locationRequested, issuedBy, new Date(), crPriority, crType, null, true, housingAttributesRequired, null,
                null, null, null);
        data = new ChangeRequestWaitlistType(1L, offender, null, movementReason, housingChangeRequest, null);
        assert (validate(data) == false);

        housingChangeRequest = new HousingChangeRequestType(-1L, locationRequested, issuedBy, new Date(), crPriority, crType, null, true, housingAttributesRequired, null,
                null, null, null);
        data = new ChangeRequestWaitlistType(1L, offender, facility, movementReason, housingChangeRequest, null);
        assert (validate(data) == true);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
