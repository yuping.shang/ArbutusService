package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingBedMgmtActivityType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingBedMgmtActivityTypeTest {

    private static Long offender = 1L;
    private static Long facility = 1L;

    @Test
    public void isValid() {

        HousingBedMgmtActivityType data = new HousingBedMgmtActivityType();
        assert (validate(data) == false);

        data = new HousingBedMgmtActivityType(null, null, null, null, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        //ActivityId
        data = new HousingBedMgmtActivityType(null, 1L, null, null, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        //ActivityState
        String activityState = "TestCode";
        data = new HousingBedMgmtActivityType(null, 1L, activityState, null, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        //offender reference
        data = new HousingBedMgmtActivityType(null, 1L, activityState, offender, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        //facility reference
        data = new HousingBedMgmtActivityType(null, 1L, activityState, offender, null, null, null, null, null, null, facility, null);
        assert (validate(data) == false);

        //movementReason
        String movementReason = "TestCode";
        data = new HousingBedMgmtActivityType(null, 1L, activityState, offender, movementReason, null, null, null, null, null, facility, null);
        assert (validate(data) == true);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
