package syscon.arbutus.product.services.housingbedmanagementactivity.contract.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.*;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingChangeRequestTypeTest {

    private static Long staff1 = 1L;
    private static Long issuedBy = 2L;
    private static Long locationRequested = 2L;

    @Test
    public void isValid() {

        HousingChangeRequestType data = new HousingChangeRequestType();
        assert (validate(data) == false);

        data = new HousingChangeRequestType(-1L, null, null, null, null, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        //locationRequested
        data = new HousingChangeRequestType(-1L, locationRequested, null, null, null, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        //issuedBy
        data = new HousingChangeRequestType(-1L, locationRequested, issuedBy, null, null, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        //issuedDate
        data = new HousingChangeRequestType(-1L, locationRequested, issuedBy, new Date(), null, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        //crPriority
        String crPriority = "TestCode";
        data = new HousingChangeRequestType(-1L, locationRequested, issuedBy, new Date(), crPriority, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        //crType
        String crType = "TestCode";
        data = new HousingChangeRequestType(-1L, locationRequested, issuedBy, new Date(), crPriority, crType, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        //reservationFlag
        data = new HousingChangeRequestType(-1L, locationRequested, issuedBy, new Date(), crPriority, crType, null, true, null, null, null, null, null);
        assert (validate(data) == false);

        //housingAttributesRequired
        OffenderHousingAttributeType housingAttributesRequired = new OffenderHousingAttributeType();
        data = new HousingChangeRequestType(-1L, locationRequested, issuedBy, new Date(), crPriority, crType, null, true, housingAttributesRequired, null, null, null,
                null);
        assert (validate(data) == false);

        //housingAttributesRequired
        String securityLevel = "TestCode1";
        String gender = "TestCode2";
        String sentenceStatus = "TestCode3";
        housingAttributesRequired = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatus, null, null);
        data = new HousingChangeRequestType(-1L, locationRequested, issuedBy, new Date(), crPriority, crType, null, true, housingAttributesRequired, null, null, null,
                null);
        assert (validate(data) == true);

        //housingCRApproval
        HousingChangeRequestApprovalType housingCRApproval = new HousingChangeRequestApprovalType();
        data = new HousingChangeRequestType(-1L, locationRequested, issuedBy, new Date(), crPriority, crType, housingCRApproval, true, housingAttributesRequired, null,
                null, null, null);
        assert (validate(data) == false);

        //housingCRApproval
        housingCRApproval = new HousingChangeRequestApprovalType(staff1, new Date(), null);
        data = new HousingChangeRequestType(-1L, locationRequested, issuedBy, new Date(), crPriority, crType, housingCRApproval, true, housingAttributesRequired, null,
                null, null, null);
        assert (validate(data) == true);

        //housingActivityClosure
        HousingActivityClosureType housingActivityClosure = new HousingActivityClosureType(staff1, new Date(), null);
        data = new HousingChangeRequestType(-1L, locationRequested, issuedBy, new Date(), crPriority, crType, housingCRApproval, true, housingAttributesRequired, null,
                null, housingActivityClosure, null);
        assert (validate(data) == true);

        //attMismatchForValidation
        Set<HousingAttributeMismatchType> attMismatchForValidation = new HashSet<HousingAttributeMismatchType>();
        data = new HousingChangeRequestType(-1L, locationRequested, issuedBy, new Date(), crPriority, crType, housingCRApproval, true, housingAttributesRequired,
                attMismatchForValidation, null, housingActivityClosure, null);
        assert (validate(data) == true);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
