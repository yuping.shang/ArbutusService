package syscon.arbutus.product.services.approval.contract.ejb;

import org.apache.log4j.Logger;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.approval.contract.dto.*;
import syscon.arbutus.product.services.approval.contract.dto.EntityType;
import syscon.arbutus.product.services.approval.contract.interfaces.ApprovalService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.core.exception.ArbutusOptimisticLockException;
import syscon.arbutus.product.services.vine.gov.niem.niem.niem_core._2.*;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ModuleConfig
public class IT extends BaseIT {

    private static Logger log = Logger.getLogger(IT.class);

    private ApprovalService approvalService;

    @AfterMethod
    public void afterMethod() {
    }

    @BeforeClass
    public void beforeClass() {

        log.info("beforeClass Begin - JNDI lookup");
        approvalService = (ApprovalService) JNDILookUp(this.getClass(), ApprovalService.class);
        uc = super.initUserContext();

        SecurityClient client;
        try {
            client = SecurityClientFactory.getSecurityClient();
            client.setSimple("devtest", "devtest"); // nice regular service user
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //init();
    }

    @AfterClass
    public void afterTest() {

    }

    public void init() {
        approvalService.deleteAllConfiguration(uc);
        approvalService.deleteAllEntityInstances(uc);
    }

    @Test
    public void createEntity() {
        EntityType entityType = new EntityType("A", "Desc", "B", "C", "D", "E");
        entityType = approvalService.createEntity(uc, entityType);
        assert (entityType != null);
        assert (entityType.getVersion() == 0L);
    }

    @Test
    void createProcess() {
        ProcessType processType = new ProcessType("A", "AND");
        processType = approvalService.createProcess(uc, processType);
        assert (processType != null);
        assert (processType.getVersion() != null);
    }

    @Test
    void createReviewTask() {

        ReviewTaskType reviewTaskType = new ReviewTaskType("A", 1, "HOUR", 1000, "HOUR", 1000);

        reviewTaskType = approvalService.createReviewTask(uc, reviewTaskType);
        assert (reviewTaskType != null);
        assert (reviewTaskType.getVersion() != null);
    }

    @Test
    public void createEntityInstance() {
        EntityInstanceType entityInstanceType = new EntityInstanceType("A", 1L);
        entityInstanceType = approvalService.createEntityInstance(uc, entityInstanceType);
        assert (entityInstanceType != null);
        assert (entityInstanceType.getVersion() == 0L);
    }

    @Test
    public void createReview() {
        ReviewType reviewType = new ReviewType("A", 1L);
        reviewType = approvalService.createReview(uc, reviewType);
        assert (reviewType != null);
        assert (reviewType.getReviewId() != null);
        assert (reviewType.getVersion() == 0L);
    }

    @Test
    public void updateEntity() {
        EntityType entityType = new EntityType("A", "Desc", "B", "C", "D", "E");
        entityType = approvalService.createEntity(uc, entityType);

        //
        entityType.setApprovalRefSet("F");
        EntityType updated1 = approvalService.updateEntity(uc, entityType);
        assert (entityType.getVersion() != updated1.getVersion());

        //
        boolean locked = false;
        try {
            entityType.setApprovalRefSet("G");
            EntityType updated2 = approvalService.updateEntity(uc, entityType);
        } catch (ArbutusOptimisticLockException ex) {
            locked = true;
        }
        assert (locked);
    }

    @Test
    public void updateEntityWithoutLocking() {
        EntityType entityType = new EntityType("A", "Desc", "B", "C", "D", "E");
        entityType = approvalService.createEntity(uc, entityType);

        //
        entityType.setApprovalRefSet("F");
        EntityType updated1 = approvalService.forceUpdateEntity(uc, entityType);
        assert (entityType.getVersion() != updated1.getVersion());

        entityType.setApprovalRefSet("G");
        EntityType updated2 = approvalService.forceUpdateEntity(uc, entityType);
        assert (updated2.getVersion() != updated1.getVersion());

    }

    @Test
    void updateProcess() {
        ProcessType processType = new ProcessType("A", "AND");
        processType = approvalService.createProcess(uc, processType);

        //
        processType.setProcessOrchestration("OR");
        ProcessType updated1 = approvalService.updateProcess(uc, processType);
        assert (processType.getVersion() != updated1.getVersion());

        //
        boolean locked = false;
        try {
            processType.setProcessOrchestration("AND");
            ProcessType updated2 = approvalService.updateProcess(uc, processType);
            assert (updated2.getVersion() != updated1.getVersion());
        } catch (ArbutusOptimisticLockException ex) {
            locked = true;
        }
        assert (locked);
    }

    @Test
    void updateProcessWithoutLocking() {
        ProcessType processType = new ProcessType("A", "AND");
        processType = approvalService.createProcess(uc, processType);

        //
        processType.setProcessOrchestration("OR");
        ProcessType updated1 = approvalService.forceUpdateProcess(uc, processType);
        assert (processType.getVersion() != updated1.getVersion());

        //
        processType.setProcessOrchestration("AND");
        ProcessType updated2 = approvalService.forceUpdateProcess(uc, processType);
        assert (updated2.getVersion() != updated1.getVersion());
    }

    @Test
    public void updateEntityInstance() {

        EntityInstanceType entityInstanceType = new EntityInstanceType("A", 1L);
        entityInstanceType = approvalService.createEntityInstance(uc, entityInstanceType);

        //
        entityInstanceType.setDecisionCode("R");
        EntityInstanceType updated1 = approvalService.updateEntityInstance(uc, entityInstanceType);
        assert (entityInstanceType.getVersion() != updated1.getVersion());

        //
        boolean locked = false;
        try {
            entityInstanceType.setDecisionCode("A");
            EntityInstanceType updated2 = approvalService.updateEntityInstance(uc, entityInstanceType);
            assert (updated2.getVersion() != updated1.getVersion());
        } catch (ArbutusOptimisticLockException ex) {
            locked = true;
        }
        assert (locked);
    }

    @Test
    public void updateEntityInstanceWithoutLocking() {

        EntityInstanceType entityInstanceType = new EntityInstanceType("A", 1L);
        entityInstanceType = approvalService.createEntityInstance(uc, entityInstanceType);

        //
        entityInstanceType.setDecisionCode("R");
        EntityInstanceType updated1 = approvalService.forceUpdateEntityInstance(uc, entityInstanceType);
        assert (entityInstanceType.getVersion() != updated1.getVersion());

        //
        entityInstanceType.setDecisionCode("A");
        EntityInstanceType updated2 = approvalService.forceUpdateEntityInstance(uc, entityInstanceType);
        assert (updated2.getVersion() != updated1.getVersion());

    }

    @Test
    public void getAllEntities() {

        EntityType entityType = new EntityType("A", "Desc", "B", "C", "D", "E");
        entityType = approvalService.createEntity(uc, entityType);

        entityType = new EntityType("B", "Desc", "B", "C", "D", "E");
        entityType = approvalService.createEntity(uc, entityType);

        List<EntityType> entityTypeList = approvalService.getAllEntities(uc);
        assert(entityTypeList != null && entityTypeList.size() == 2);
    }

    @Test
    public void getAllProcessesAndRoles() {

        ProcessType processType = new ProcessType("A", "AND");
        processType = approvalService.createProcess(uc, processType);
        processType = new ProcessType("B", "AND");
        processType = approvalService.createProcess(uc, processType);

        List<Long> roleIdList = new ArrayList<Long>();
        roleIdList.add(1L);
        approvalService.createProcessRoles(uc, "A", roleIdList);
        roleIdList.add(2L);
        approvalService.createProcessRoles(uc, "B", roleIdList);

        List<ProcessType> processTypeList = approvalService.getAllProcessesAndRoles(uc);
        assert(processTypeList != null && processTypeList.size() == 2);
        assert(processTypeList.get(1).getRoleTypeList().size() == 2);

    }

    @Test
    public void getAll() {

        ProcessType processType = new ProcessType("A", "AND");
        processType = approvalService.createProcess(uc, processType);
        processType = new ProcessType("B", "AND");
        processType = approvalService.createProcess(uc, processType);

        List<Long> roleIdList = new ArrayList<Long>();
        roleIdList.add(1L);
        approvalService.createProcessRoles(uc, "A", roleIdList);
        roleIdList.add(2L);
        approvalService.createProcessRoles(uc, "B", roleIdList);

        List<ProcessType> processTypeList = approvalService.getAllProcessesAndRoles(uc);
        assert(processTypeList != null && processTypeList.size() == 2);
        assert(processTypeList.get(1).getRoleTypeList().size() == 2);

    }

    @Test
    public void getProcessAndRoles() {

        ProcessType processType = new ProcessType("A", "AND");
        processType = approvalService.createProcess(uc, processType);
        processType = new ProcessType("B", "AND");
        processType = approvalService.createProcess(uc, processType);

        List<Long> roleIdList = new ArrayList<Long>();
        roleIdList.add(1L);
        approvalService.createProcessRoles(uc, "A", roleIdList);
        roleIdList.add(2L);
        approvalService.createProcessRoles(uc, "B", roleIdList);
        roleIdList.add(3L);
        approvalService.createProcessRoles(uc, "B", roleIdList);

        processType = approvalService.getProcessAndRoles(uc, "B");
        assert(processType.getRoleTypeList().size() == 3);
    }

    @Test
    public void getAllReviewTasks() {

        ReviewTaskType reviewTaskType = new ReviewTaskType("A", 1, "HOUR", 1000, "HOUR", 1000);

        reviewTaskType = approvalService.createReviewTask(uc, reviewTaskType);
        reviewTaskType = new ReviewTaskType("A", 2, "HOUR", 1000, "HOUR", 1000);
        reviewTaskType = approvalService.createReviewTask(uc, reviewTaskType);
        List<ReviewTaskType> reviewTaskTypeList = approvalService.getAllReviewTasksAndRoles(uc, "A");
        assert(reviewTaskTypeList != null && reviewTaskTypeList.size() == 2);

    }

    @Test
    public void updateProcessAndRoles() {

        ProcessType processType = new ProcessType("A", "AND");
        processType = approvalService.createProcess(uc, processType);

        List<Long> roleIdList = new ArrayList<Long>();
        roleIdList.add(1L);
        roleIdList.add(2L);
        approvalService.createProcessRoles(uc, "A", roleIdList);
        roleIdList = new ArrayList<Long>();
        roleIdList.add(3L);
        processType.setProcessOrchestration("OR");
        processType = approvalService.updateProcessAndRoles(uc, processType, roleIdList);
        processType = approvalService.getProcessAndRoles(uc, "A");
        assert("OR".equalsIgnoreCase(processType.getProcessOrchestration()));
        assert(processType.getRoleTypeList().size() == 1);
    }


    @Test
    public void updateReviewTaskAndRoles() {

        ReviewTaskType reviewTaskType = new ReviewTaskType("A", 1, "HOUR", 1000, "HOUR", 1000);
        reviewTaskType = approvalService.createReviewTask(uc, reviewTaskType);
        List<Long> roleIdList = new ArrayList<Long>();
        roleIdList.add(1L);
        roleIdList.add(2L);
        approvalService.createReviewTaskRoles(uc, reviewTaskType.getReviewTaskId(), roleIdList);
        roleIdList.add(3L);
        reviewTaskType.setReviewReminder(2000);
        reviewTaskType = approvalService.updateReviewTaskAndRoles(uc, reviewTaskType, roleIdList);
        assert(reviewTaskType.getReviewReminder() == 2000);
        assert(reviewTaskType.getRoleTypeList().size() == 3);
    }

    @Test
    public void deleteProcess() {
        ProcessType processType = new ProcessType("A", "AND");
        processType = approvalService.createProcess(uc, processType);

        List<Long> roleIdList = new ArrayList<Long>();
        roleIdList.add(1L);
        roleIdList.add(2L);
        approvalService.createProcessRoles(uc, "A", roleIdList);
        approvalService.deleteProcess(uc, "A");
        processType = approvalService.getProcessAndRoles(uc, "A");
        assert(processType == null);
    }

    @Test
    public  void deleteReviewTask() {
        ReviewTaskType reviewTaskType = new ReviewTaskType("A", 1, "HOUR", 1000, "HOUR", 1000);
        reviewTaskType = approvalService.createReviewTask(uc, reviewTaskType);
        List<Long> roleIdList = new ArrayList<Long>();
        roleIdList.add(1L);
        roleIdList.add(2L);
        approvalService.createReviewTaskRoles(uc, reviewTaskType.getReviewTaskId(), roleIdList);
        approvalService.deleteReviewTask(uc, reviewTaskType.getReviewTaskId());

        reviewTaskType = approvalService.getReviewTaskAndRoles(uc, reviewTaskType.getReviewTaskId());
        assert(reviewTaskType == null);
    }

    @Test
    public void getAllReviews() {

        ReviewType reviewType = new ReviewType("A", 1L, null, null, null, null, 1L);
        reviewType = approvalService.createReview(uc, reviewType);

        reviewType = new ReviewType("A", 1L, null, null, null, null, 2L);
        reviewType = approvalService.createReview(uc, reviewType);

        reviewType = new ReviewType("A", 1L, null, null, null, null, -1L);
        reviewType = approvalService.createReview(uc, reviewType);

        List<ReviewType> reviewTypeList = approvalService.getAllReviews(uc, "A", 1L);
        assert (reviewTypeList != null);
        assert (reviewTypeList.size() == 2);

    }
}