package syscon.arbutus.product.services.sentencecalculation.contract.ejb;

import javax.naming.NamingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.sentencecalculation.contract.dto.*;
import syscon.arbutus.product.services.sentencecalculation.contract.interfaces.SentenceCalculation;
import syscon.arbutus.product.services.sentencecalculation.contract.util.DateUtil;

import static org.testng.Assert.*;

@Test
public class SentenceCalculationIT extends BaseIT {

    private static final String CUSTODY = "CUS";
    private static final String MAX_TERM = "MAX";
    private static final String MIN_TERM = "MIN";
    private static final String COM_TERM = "COM";
    private static final String SINGLE = "SINGLE";
    private static final String SENTENCE_TYPE_DEF = "DEF";
    private static final String SENTENCE_TYPE_INT = "INT";
    private static final String SENTENCE_TYPE_FIN = "FINEDEF";
    private static final Long NO_GOOD_TIME = 0L;
    private static final String SENTENCE_TYPE_INDETERMINANT = "INDETER";
    private static final String SENTENCE_TYPE_DETERMINANT = "DETER";
    private static final String SENTENCE_TYPE_SPLIT = "SPLIT";

    private static Logger log = LoggerFactory.getLogger(SentenceCalculationIT.class);
    DateUtil du = new DateUtil();
    private SentenceCalculation service;

    @BeforeMethod
    public void beforeMethod() {

    }

    @BeforeClass(enabled = true)
    public void beforeClass() throws NamingException {
        log.info("beforeClass Begin - JNDI lookup");
        service = (SentenceCalculation) JNDILookUp(this.getClass(), SentenceCalculation.class);
        uc = super.initUserContext();
    }

    @AfterClass
    public void afterClass() {
    }

    @BeforeTest
    public void beforeTest() {
    }

    @AfterTest
    public void afterTest() {
    }

    @Test(enabled = true)
    public void jndiLookUp() {
        assertNotNull(service);
    }

    /**
     * Scenario 1 - Sentence Calc with Single sentence
     */
    @Test(enabled = true)
    public void test1() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms.add(maxTerm);

        sentences.add(new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/03/2013"), 0L, terms));
        sv.setSentences(sentences);
        SupervisionReturn ret = service.keyDatesCalculate(sv);

        //Supervision Key Dates
        assertEquals(simpleDate("15/05/2013"), ret.getSupervision().getParoleEligibilityDate());
        assertEquals(simpleDate("15/07/2013"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("14/09/2013"), ret.getSupervision().getSentenceExpiryDate());

        //Controlling Sentence
        assertTrue(ret.getSupervision().getSentences().get(0).getControllingSentence());

        //Aggregated Controlling Sentence
        assertTrue(ret.getSupervision().getAggregates().get(0).getControllingSentence());
    }

    private Supervision getSupervision(Supervision sv) {
        sv.setSupervisionId(1L);

        List<KeyDate> keyDates = new ArrayList<KeyDate>();
        keyDates.add(new KeyDate(sv.getSupervisionId(), "SSD"));
        keyDates.add(new KeyDate(sv.getSupervisionId(), "PRD"));
        keyDates.add(new KeyDate(sv.getSupervisionId(), "PDD"));
        keyDates.add(new KeyDate(sv.getSupervisionId(), "SED"));
        keyDates.add(new KeyDate(sv.getSupervisionId(), "DUM"));

        sv.setKeyDates(keyDates);
        return sv;
    }

    /**
     * Scenario 2 - Sentence Calc with two concurrent sentences
     */
    @Test(enabled = true)
    public void test2() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms1.add(maxTerm1);
        sentences.add(new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/03/2013"), 0L, terms1));

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms2.add(maxTerm2);
        sentences.add(new Sentence(2L, 2L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/04/2013"), 0L, terms2));

        sv.setSentences(sentences);
        SupervisionReturn ret = service.keyDatesCalculate(sv);

        //Supervision Key Date
        assertEquals(simpleDate("25/05/2013"), ret.getSupervision().getParoleEligibilityDate());
        assertEquals(simpleDate("04/08/2013"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("14/10/2013"), ret.getSupervision().getSentenceExpiryDate());

        //Controlling Sentence
        assertTrue(ret.getSupervision().getSentences().get(0).getControllingSentence());
        assertFalse(ret.getSupervision().getSentences().get(1).getControllingSentence());

        //Aggregated Controlling Sentence
        assertTrue(ret.getSupervision().getAggregates().get(0).getControllingSentence());

    }

    /**
     * Scenario 3 - Sentence Calc with two sentences having gap
     */
    @Test(enabled = true)
    public void test3() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms1.add(maxTerm1);
        sentences.add(new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/03/2013"), 0L, terms1));

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms2.add(maxTerm2);
        sentences.add(new Sentence(2L, 2L, 1L, SENTENCE_TYPE_DEF, simpleDate("01/08/2013"), 0L, terms2));

        sv.setSentences(sentences);
        SupervisionReturn ret = service.keyDatesCalculate(sv);

        // Supervision key dates
        assertEquals(simpleDate("01/12/2013"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("31/01/2014"), ret.getSupervision().getSentenceExpiryDate());

        // Aggregate count
        assertEquals(2, ret.getAggregates().size());

        // first aggregate key dates
        assertEquals(simpleDate("15/07/2013"), ret.getAggregates().get(0).getPrdDate());
        assertEquals(simpleDate("14/09/2013"), ret.getAggregates().get(0).getEndDate());

        //second aggregate key dates
        assertEquals(simpleDate("01/12/2013"), ret.getAggregates().get(1).getPrdDate());
        assertEquals(simpleDate("31/01/2014"), ret.getAggregates().get(1).getEndDate());

        //controlling sentence
        assertTrue(ret.getSupervision().getSentences().get(1).getControllingSentence());
        assertFalse(ret.getSupervision().getSentences().get(0).getControllingSentence());

        //Aggregated Controlling Sentence
        assertFalse(ret.getSupervision().getAggregates().get(0).getControllingSentence());
        assertTrue(ret.getSupervision().getAggregates().get(1).getControllingSentence());
    }

    /**
     * Scenario 4 - Sentence Calc with two consecutive sentences
     */
    @Test(enabled = true)
    public void test4() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms1.add(maxTerm1);
        sentences.add(new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/03/2013"), 0L, terms1));

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms2.add(maxTerm2);
        sentences.add(new Sentence(2L, 2L, 1L, SENTENCE_TYPE_DEF, simpleDate("01/08/2013"), 1L, terms2));

        sv.setSentences(sentences);
        SupervisionReturn ret = service.keyDatesCalculate(sv);

        // Supervision key dates
        assertEquals(simpleDate("13/11/2013"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("14/03/2014"), ret.getSupervision().getSentenceExpiryDate());

        // Aggregate count
        assertEquals(1, ret.getAggregates().size());

        // first aggregate key dates
        assertEquals(simpleDate("13/11/2013"), ret.getAggregates().get(0).getPrdDate());
        assertEquals(simpleDate("14/03/2014"), ret.getAggregates().get(0).getEndDate());

        //controlling sentence
        assertFalse(ret.getSupervision().getSentences().get(0).getControllingSentence());
        assertTrue(ret.getSupervision().getSentences().get(1).getControllingSentence());

        //Aggregated Controlling Sentence
        assertTrue(ret.getSupervision().getAggregates().get(0).getControllingSentence());

    }

    /**
     * Scenario 5 - Single Civil Sentence
     */
    @Test(enabled = true)
    public void test5() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();

        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms1.add(maxTerm1);
        sentences.add(new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/03/2013"), 0L, terms1));

        sv.setSentences(sentences);
        SupervisionReturn ret = service.keyDatesCalculate(sv);

        // Supervision key dates
        assertEquals(simpleDate("15/07/2013"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("14/09/2013"), ret.getSupervision().getSentenceExpiryDate());

        // Aggregate count
        assertEquals(1, ret.getAggregates().size());

        // first aggregate key dates
        assertEquals(simpleDate("15/07/2013"), ret.getAggregates().get(0).getPrdDate());
        assertEquals(simpleDate("14/09/2013"), ret.getAggregates().get(0).getEndDate());

        //controlling sentence
        assertTrue(ret.getSupervision().getSentences().get(0).getControllingSentence());

        //Aggregated Controlling Sentence
        assertTrue(ret.getSupervision().getAggregates().get(0).getControllingSentence());

    }

    /**
     * Scenario 6 - Multiple Civil Sentences
     * assumption: Sentence Type INT is aggregatBy NA
     */
    @Test(enabled = true)
    public void test6() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms1.add(maxTerm1);
        sentences.add(new Sentence(1L, 1L, 1L, SENTENCE_TYPE_INT, simpleDate("15/03/2013"), 0L, terms1));

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms2.add(maxTerm2);
        sentences.add(new Sentence(2L, 2L, 1L, SENTENCE_TYPE_INT, simpleDate("15/04/2013"), 0L, terms2));

        sv.setSentences(sentences);
        SupervisionReturn ret = service.keyDatesCalculate(sv);

        // Supervision key dates
        assertEquals(simpleDate("14/08/2013"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("14/10/2013"), ret.getSupervision().getSentenceExpiryDate());

        // Aggregate count
        assertEquals(2, ret.getAggregates().size());

        // first aggregate key dates
        assertEquals(simpleDate("15/07/2013"), ret.getAggregates().get(0).getPrdDate());
        assertEquals(simpleDate("14/09/2013"), ret.getAggregates().get(0).getEndDate());

        // second aggregate key dates
        assertEquals(simpleDate("14/08/2013"), ret.getAggregates().get(1).getPrdDate());
        assertEquals(simpleDate("14/10/2013"), ret.getAggregates().get(1).getEndDate());

        //controlling sentence
        assertTrue(ret.getSupervision().getSentences().get(0).getControllingSentence());
        assertFalse(ret.getSupervision().getSentences().get(1).getControllingSentence());

        //Aggregated Controlling Sentence
        assertTrue(ret.getSupervision().getAggregates().get(0).getControllingSentence());
    }

    /**
     * Scenario 7 - Sentence Calc of two consecutive sentences aggregate by PRD
     */
    @Test(enabled = true)
    public void test7() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms1.add(maxTerm1);
        sentences.add(new Sentence(1L, 1L, 1L, SENTENCE_TYPE_INT, simpleDate("15/03/2013"), 0L, terms1));

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms2.add(maxTerm2);
        sentences.add(new Sentence(2L, 2L, 1L, SENTENCE_TYPE_INT, simpleDate("01/08/2013"), 1L, terms2));

        sv.setSentences(sentences);
        SupervisionReturn ret = service.keyDatesCalculate(sv);

        // Supervision key dates
        assertEquals(simpleDate("13/11/2013"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("14/03/2014"), ret.getSupervision().getSentenceExpiryDate());

        // Aggregate count
        assertEquals(1, ret.getAggregates().size());

        // first aggregate key dates
        assertEquals(simpleDate("13/11/2013"), ret.getAggregates().get(0).getPrdDate());
        assertEquals(simpleDate("14/03/2014"), ret.getAggregates().get(0).getEndDate());

        //controlling sentence
        assertTrue(ret.getSupervision().getSentences().get(1).getControllingSentence());
        assertFalse(ret.getSupervision().getSentences().get(0).getControllingSentence());

        //Aggregated Controlling Sentence
        assertTrue(ret.getSupervision().getAggregates().get(0).getControllingSentence());
    }

    /**
     * No good time after cut-off date
     * Scenario 1
     */
    @Test(enabled = true)
    public void noGoodTime1() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms.add(maxTerm);

        sentences.add(new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/03/1982"), 0L, terms));
        sv.setSentences(sentences);
        SupervisionReturn ret = service.keyDatesCalculate(sv);

        //System.out.println("PDD date : " + ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("15/05/1982"), ret.getSupervision().getParoleEligibilityDate());
        assertEquals(simpleDate("14/09/1982"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("14/09/1982"), ret.getSupervision().getSentenceExpiryDate());

        assertEquals(NO_GOOD_TIME, ret.getSupervision().getGoodTime());

        //controlling sentence
        assertTrue(ret.getSupervision().getSentences().get(0).getControllingSentence());

        //Aggregated Controlling Sentence
        assertTrue(ret.getSupervision().getAggregates().get(0).getControllingSentence());

    }

    /**
     * No good time after cut-off date
     * Scenario 2
     */
    @Test(enabled = true)
    public void noGoodTime2() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 5L, 0L, 0L, 0L);
        terms1.add(maxTerm1);

        sentences.add(new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/11/1982"), 0L, terms1));

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 5L, 0L, 0L, 0L);
        terms2.add(maxTerm2);

        sentences.add(new Sentence(2L, 2L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/12/1982"), 0L, terms2));
        sv.setSentences(sentences);

        SupervisionReturn ret = service.keyDatesCalculate(sv);

        //System.out.println("PDD date : " + ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("14/01/1983"), ret.getSupervision().getParoleEligibilityDate());
        assertEquals(simpleDate("14/05/1983"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("14/05/1983"), ret.getSupervision().getSentenceExpiryDate());

        assertEquals(NO_GOOD_TIME, ret.getSupervision().getGoodTime());

        //controlling sentence
        assertTrue(ret.getSupervision().getSentences().get(1).getControllingSentence());
        assertFalse(ret.getSupervision().getSentences().get(0).getControllingSentence());

        //Aggregated Controlling Sentence
        assertTrue(ret.getSupervision().getAggregates().get(0).getControllingSentence());

    }

    //Bugs Reported test cases

    /**
     * Scenario 50 - Sentence Calc using three concurrent sentences
     */
    @Test(enabled = true)
    public void test50() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms1.add(maxTerm1);
        sentences.add(new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/03/2013"), 0L, terms1));

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 1L, 0L, 0L, 0L, 0L);
        terms2.add(maxTerm2);
        sentences.add(new Sentence(2L, 2L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/08/2013"), 0L, terms2));

        Set<SentenceTerm> terms3 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm3 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 5L, 0L, 0L, 0L, 0L);
        terms3.add(maxTerm3);
        sentences.add(new Sentence(3L, 3L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/05/2013"), 0L, terms3));

        sv.setSentences(sentences);
        SupervisionReturn ret = service.keyDatesCalculate(sv);

        // Aggregate count
        assertEquals(1, ret.getAggregates().size());

        // Supervision key dates
        assertEquals(simpleDate("03/12/2014"), ret.getSupervision().getParoleEligibilityDate());
        assertEquals(simpleDate("23/08/2016"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("14/05/2018"), ret.getSupervision().getSentenceExpiryDate());

        // first aggregate key dates
        assertEquals(simpleDate("23/08/2016"), ret.getAggregates().get(0).getPrdDate());
        assertEquals(simpleDate("14/05/2018"), ret.getAggregates().get(0).getEndDate());

        //controlling sentence
        assertTrue(ret.getSupervision().getSentences().get(1).getControllingSentence());
        assertFalse(ret.getSupervision().getSentences().get(0).getControllingSentence());
        assertFalse(ret.getSupervision().getSentences().get(2).getControllingSentence());

        //Aggregated Controlling Sentence
        assertTrue(ret.getSupervision().getAggregates().get(0).getControllingSentence());

    }

    @Test(enabled = true)
    public void wor8174() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms1.add(maxTerm1);
        sentences.add(new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/03/2013"), 0L, terms1));

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 3L, 6L, 0L, 0L, 0L);
        terms2.add(maxTerm2);
        sentences.add(new Sentence(2L, 2L, 1L, SENTENCE_TYPE_DEF, simpleDate("01/08/2013"), 0L, terms2));

        Set<SentenceTerm> terms3 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm3 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms3.add(maxTerm3);
        sentences.add(new Sentence(3L, 3L, 1L, SENTENCE_TYPE_DEF, simpleDate("13/08/2013"), 1L, terms3));

        Set<SentenceTerm> terms4 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm4 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 1L, 6L, 0L, 0L, 0L);
        terms4.add(maxTerm4);
        sentences.add(new Sentence(4L, 4L, 1L, SENTENCE_TYPE_DEF, simpleDate("13/10/2013"), 2L, terms4));

        sv.setSentences(sentences);
        SupervisionReturn ret = service.keyDatesCalculate(sv);

        // Aggregate count
        assertEquals(ret.getAggregates().size(), 1);

        // Supervision key dates
        assertEquals(simpleDate("29/12/2014"), ret.getSupervision().getParoleEligibilityDate());
        assertEquals(simpleDate("14/10/2016"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("31/07/2018"), ret.getSupervision().getSentenceExpiryDate());

        // first aggregate key dates
        assertEquals(simpleDate("14/10/2016"), ret.getAggregates().get(0).getPrdDate());
        assertEquals(simpleDate("31/07/2018"), ret.getAggregates().get(0).getEndDate());

    }

    /**
     * Do not aggregate fine sentence
     */

    @Test(enabled = true)
    public void wor8143() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms1.add(maxTerm1);
        sentences.add(new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/03/2013"), 0L, terms1));

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 3L, 6L, 0L, 0L, 0L);
        terms2.add(maxTerm2);
        sentences.add(new Sentence(2L, 2L, 1L, SENTENCE_TYPE_FIN, simpleDate("15/03/2013"), 0L, terms2));

        sv.setSentences(sentences);
        SupervisionReturn ret = service.keyDatesCalculate(sv);

        // Aggregate count
        assertEquals(2, ret.getAggregates().size());

        // Supervision key dates
        assertEquals(simpleDate("15/05/2014"), ret.getSupervision().getParoleEligibilityDate());
        assertEquals(simpleDate("16/07/2015"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("14/09/2016"), ret.getSupervision().getSentenceExpiryDate());

/*   		

   		// first aggregate key dates 
   		assertEquals(simpleDate("14/10/2016"), ret.getAggregates().get(0).getPrdDate());
   		assertEquals(simpleDate("31/07/2018"), ret.getAggregates().get(0).getEndDate());
   		
*/

    }

    /**
     * Adjustments
     * Adjustment with simple sentence
     */

    @Test(enabled = true)
    public void test101() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Adjustment> adjustments = new ArrayList<>();
        adjustments.add(new Adjustment(simpleDate("01/04/2013"), "CRE", -4L));
        sv.setAdjustments(adjustments);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);

        terms1.add(maxTerm1);
        Sentence sentence = new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/03/2013"), 0L, terms1);
        sentence.setAdjustments(adjustments);

        sentences.add(sentence);

        sv.setSentences(sentences);
        sv.setAdjustments(adjustments);
        SupervisionReturn ret = service.keyDatesCalculate(sv);

        // Supervision key dates
        assertEquals(simpleDate("12/05/2013"), ret.getSupervision().getParoleEligibilityDate());
        assertEquals(simpleDate("11/07/2013"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("14/09/2013"), ret.getSupervision().getSentenceExpiryDate());

        // Aggregate count
        assertEquals(1, ret.getAggregates().size());

    }

    /**
     * Adjustments
     * WOR-6991
     */
    @Test(enabled = true)
    public void testadj1() {

        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Adjustment> adjustments = new ArrayList<>();
        Adjustment adjustment = new Adjustment(simpleDate("01/04/2013"), "CRE", -10L);
        adjustments.add(adjustment);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms1.add(maxTerm1);

        Sentence sentence1 = new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/03/2013"), 0L, terms1);
        sentence1.setAdjustments(adjustments);
        sentences.add(sentence1);

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms2.add(maxTerm2);

        Sentence sentence2 = new Sentence(2L, 2L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/04/2013"), 0L, terms2);
        sentence2.setAdjustments(adjustments);
        sentences.add(sentence2);

        sv.setSentences(sentences);
        sv.setAdjustments(adjustments);

        SupervisionReturn ret = service.keyDatesCalculate(sv);

        //assertEquals(simpleDate("17/05/2013"),ret.getSupervision().getParoleEligibilityDate());

        assertEquals(simpleDate("25/07/2013"), ret.getSupervision().getProbableDischargeDate()); //without adjustment
        assertEquals(simpleDate("14/10/2013"), ret.getSupervision().getSentenceExpiryDate());

    }

    /**
     * Adjustments  Demo scenario 2
     */
    @Test(enabled = true)
    public void demo2WithOutAdjustment() {

        Supervision sv = new Supervision();
        sv = getSupervision(sv);

	/*	List<Adjustment> adjustments = new ArrayList<>();
		Adjustment adjustment = new Adjustment(simpleDate("01/07/2006"), "JT", -10L);
		adjustments.add(adjustment);*/

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 0L, 0L, 60L, 0L);
        terms1.add(maxTerm1);

        Sentence sentence1 = new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("11/07/2006"), 0L, terms1);
        //sentence1.setAdjustments(adjustments);
        sentences.add(sentence1);

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 0L, 0L, 60L, 0L);
        terms2.add(maxTerm2);

        Sentence sentence2 = new Sentence(2L, 2L, 1L, SENTENCE_TYPE_DEF, simpleDate("28/07/2006"), 0L, terms2);
        //sentence2.setAdjustments(adjustments);
        sentences.add(sentence2);

        Set<SentenceTerm> terms3 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm3 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 0L, 0L, 60L, 0L);
        terms3.add(maxTerm3);

        Sentence sentence3 = new Sentence(3L, 3L, 1L, SENTENCE_TYPE_DEF, simpleDate("21/08/2006"), 0L, terms3);
        //sentence2.setAdjustments(adjustments);
        sentences.add(sentence3);

        sv.setSentences(sentences);
        //sv.setAdjustments(adjustments);

        SupervisionReturn ret = service.keyDatesCalculate(sv);

        //Key Dates without Adjustment
        assertEquals(simpleDate("13/08/2006"), ret.getSupervision().getParoleEligibilityDate());
        assertEquals(simpleDate("16/09/2006"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("19/10/2006"), ret.getSupervision().getSentenceExpiryDate());

    }

    /**
     * Adjustments  Demo scenario 2
     */
    @Test(enabled = true)
    public void demo2WithAdjustment() {

        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Adjustment> adjustments = new ArrayList<>();
        Adjustment adjustment = new Adjustment(simpleDate("01/07/2006"), "JT", -10L);
        adjustments.add(adjustment);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 0L, 0L, 60L, 0L);
        terms1.add(maxTerm1);

        Sentence sentence1 = new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("11/07/2006"), 0L, terms1);
        sentence1.setAdjustments(adjustments);
        sentences.add(sentence1);

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 0L, 0L, 60L, 0L);
        terms2.add(maxTerm2);

        Sentence sentence2 = new Sentence(2L, 2L, 1L, SENTENCE_TYPE_DEF, simpleDate("28/07/2006"), 0L, terms2);
        sentence2.setAdjustments(adjustments);
        sentences.add(sentence2);

        Set<SentenceTerm> terms3 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm3 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 0L, 0L, 60L, 0L);
        terms3.add(maxTerm3);

        Sentence sentence3 = new Sentence(3L, 3L, 1L, SENTENCE_TYPE_DEF, simpleDate("21/08/2006"), 0L, terms3);
        sentence3.setAdjustments(adjustments);
        sentences.add(sentence3);

        sv.setSentences(sentences);
        sv.setAdjustments(adjustments);

        SupervisionReturn ret = service.keyDatesCalculate(sv);

        //Key Dates with Adjustment
        assertEquals(simpleDate("10/08/2006"), ret.getSupervision().getParoleEligibilityDate());
        assertEquals(simpleDate("09/09/2006"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("09/10/2006"), ret.getSupervision().getSentenceExpiryDate());

    }

    /**
     * Demo scenario 3 -  Without Adjustment
     */
    @Test(enabled = true)
    public void testDemo3WithOutAdjustment() {

        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 0L, 0L, 60L, 0L);
        terms1.add(maxTerm1);

        Sentence sentence1 = new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("11/07/2006"), 0L, terms1);
        sentences.add(sentence1);

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 0L, 0L, 60L, 0L);
        terms2.add(maxTerm2);

        Sentence sentence2 = new Sentence(2L, 2L, 1L, SENTENCE_TYPE_DEF, simpleDate("11/07/2006"), 0L, terms2);
        sentences.add(sentence2);

        Set<SentenceTerm> terms3 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm3 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 0L, 0L, 60L, 0L);
        terms3.add(maxTerm3);

        Sentence sentence3 = new Sentence(3L, 3L, 1L, SENTENCE_TYPE_DEF, simpleDate("25/08/2006"), 0L, terms3);
        sentences.add(sentence3);

        sv.setSentences(sentences);

        SupervisionReturn ret = service.keyDatesCalculate(sv);

        //Key Dates without Adjustment

        assertEquals(simpleDate("14/08/2006"), ret.getSupervision().getParoleEligibilityDate());
        assertEquals(simpleDate("03/10/2006"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("23/10/2006"), ret.getSupervision().getSentenceExpiryDate());
        assertEquals(ret.getSupervision().getSentenceDays(), new Long(105));

    }

    /**
     * Demo scenario 3 -  With Adjustment
     */
    @Test(enabled = true)
    public void demo3WithAdjustment() {

        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Adjustment> adjustments = new ArrayList<>();
        Adjustment adjustment = new Adjustment(simpleDate("01/07/2006"), "JT", -10L);
        adjustments.add(adjustment);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 0L, 0L, 60L, 0L);
        terms1.add(maxTerm1);

        Sentence sentence1 = new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("11/07/2006"), 0L, terms1);
        sentence1.setAdjustments(adjustments);
        sentences.add(sentence1);

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 0L, 0L, 60L, 0L);
        terms2.add(maxTerm2);

        Sentence sentence2 = new Sentence(2L, 2L, 1L, SENTENCE_TYPE_DEF, simpleDate("11/07/2006"), 0L, terms2);
        sentence2.setAdjustments(adjustments);
        sentences.add(sentence2);

        Set<SentenceTerm> terms3 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm3 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 0L, 0L, 60L, 0L);
        terms3.add(maxTerm3);

        Sentence sentence3 = new Sentence(3L, 3L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/08/2006"), 0L, terms3);
        sentence3.setAdjustments(adjustments);
        sentences.add(sentence3);

        sv.setSentences(sentences);
        sv.setAdjustments(adjustments);

        SupervisionReturn ret = service.keyDatesCalculate(sv);

        //Key Dates with Adjustment
        assertEquals(simpleDate("08/08/2006"), ret.getSupervision().getParoleEligibilityDate());
        assertEquals(simpleDate("17/09/2006"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("03/10/2006"), ret.getSupervision().getSentenceExpiryDate());
        assertEquals(ret.getSupervision().getSentenceDays(), new Long(85));

    }

    /**
     * WOR-8631  Adjustment
     */
    @Test(enabled = true)
    public void testWOR8631() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Adjustment> adjustments = new ArrayList<>();
        Adjustment adjustment = new Adjustment(simpleDate("01/06/2013"), "CRE", 10L);
        adjustments.add(adjustment);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms1.add(maxTerm1);

        Sentence sentence1 = new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/03/2013"), 0L, terms1);
        sentence1.setAdjustments(adjustments);
        sentences.add(sentence1);

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms2.add(maxTerm2);

        Sentence sentence2 = new Sentence(2L, 2L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/04/2013"), 0L, terms2);
        sentence2.setAdjustments(adjustments);
        sentences.add(sentence2);
   		
/*   		Set<SentenceTerm> terms3 = new HashSet<SentenceTerm>();
   		SentenceTerm maxTerm3 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 0L, 0L, 60L, 0L);
		terms3.add(maxTerm3);
		
		Sentence sentence3 = new Sentence(3L, 3L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/08/2006"), 0L, terms3);
		sentence3.setAdjustments(adjustments);
   		sentences.add(sentence3);*/

        sv.setSentences(sentences);
        sv.setAdjustments(adjustments);

        SupervisionReturn ret = service.keyDatesCalculate(sv);

        //Supervision Key Date

        assertEquals(simpleDate("25/05/2013"), ret.getSupervision().getParoleEligibilityDate());
        assertEquals(simpleDate("14/08/2013"), ret.getSupervision().getProbableDischargeDate());
        assertEquals(simpleDate("24/10/2013"), ret.getSupervision().getSentenceExpiryDate());

        //Controlling Sentence
        assertTrue(ret.getSupervision().getSentences().get(0).getControllingSentence());
        assertFalse(ret.getSupervision().getSentences().get(1).getControllingSentence());

        //Aggregated Controlling Sentence
        assertTrue(ret.getSupervision().getAggregates().get(0).getControllingSentence());

    }

    /**
     * WOR-8631  Adjustment
     */
    @Test(enabled = true)
    public void WOR8143() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms1.add(maxTerm1);

        Sentence sentence1 = new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/03/2013"), 0L, terms1);
        sentences.add(sentence1);

        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms2.add(maxTerm2);

        Sentence sentence2 = new Sentence(2L, 2L, 1L, SENTENCE_TYPE_FIN, simpleDate("15/04/2013"), 0L, terms2);
        sentences.add(sentence2);

        Set<SentenceTerm> terms3 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm3 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms3.add(maxTerm3);

        Sentence sentence3 = new Sentence(3L, 3L, 1L, SENTENCE_TYPE_FIN, simpleDate("15/05/2013"), 1L, terms3);
        sentences.add(sentence3);

        sv.setSentences(sentences);

        SupervisionReturn ret = service.keyDatesCalculate(sv);

        assertEquals(ret.getSupervision().getParoleEligibilityDate(), simpleDate("14/07/2013"));
        assertEquals(ret.getSupervision().getProbableDischargeDate(), simpleDate("13/11/2013"));
        assertEquals(ret.getSupervision().getSentenceExpiryDate(), simpleDate("14/03/2014"));

        //Controlling Sentence
        assertFalse(ret.getSupervision().getSentences().get(0).getControllingSentence());
        assertFalse(ret.getSupervision().getSentences().get(1).getControllingSentence());
        assertTrue(ret.getSupervision().getSentences().get(2).getControllingSentence());

        //Aggregated Controlling Sentence
        assertFalse(ret.getSupervision().getAggregates().get(1).getControllingSentence());
        assertTrue(ret.getSupervision().getAggregates().get(0).getControllingSentence());

    }

    /**
     * WOR-9485  dis-aggregated sentence view not showing correct number of sentences
     */
    @Test(enabled = true)
    public void WOR9485() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        //**************  Sentence 1 ************************
        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 1L, 1L, 0L, 1L, 0L);
        terms1.add(maxTerm1);

        Sentence sentence1 = new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("25/02/2014"), 0L, terms1);
        sentences.add(sentence1);

        //************** Sentence 2 *************************
        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 1L, 5L, 0L, 0L, 0L);
        terms2.add(maxTerm2);

        SentenceTerm minTerm2 = new SentenceTerm(CUSTODY, MIN_TERM, SINGLE, 0L, 5L, 0L, 0L, 0L);
        terms2.add(minTerm2);

        Sentence sentence2 = new Sentence(2L, 2L, 1L, SENTENCE_TYPE_INDETERMINANT, simpleDate("25/02/2014"), 0L, terms2);
        sentences.add(sentence2);

        //************** Sentence 3 *******************************
        Set<SentenceTerm> terms3 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm31 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 2L, 0L, 0L, 0L, 0L);
        terms3.add(maxTerm31);

        SentenceTerm maxTerm32 = new SentenceTerm(CUSTODY, MIN_TERM, SINGLE, 0L, 2L, 0L, 0L, 0L);
        terms3.add(maxTerm32);

        SentenceTerm maxTerm33 = new SentenceTerm(CUSTODY, COM_TERM, SINGLE, 0L, 0L, 0L, 0L, 12L);
        terms3.add(maxTerm33);

        Sentence sentence3 = new Sentence(3L, 3L, 1L, SENTENCE_TYPE_DETERMINANT, simpleDate("24/02/2014"), 0L, terms3);
        sentences.add(sentence3);

        //************** Sentence 4 *******************************
        Set<SentenceTerm> terms4 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm3 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 1L, 1L, 0L, 1L, 0L);
        terms4.add(maxTerm3);

        Sentence sentence4 = new Sentence(4L, 4L, 1L, SENTENCE_TYPE_SPLIT, simpleDate("27/02/2014"), 0L, terms4);
        sentences.add(sentence4);

        sv.setSentences(sentences);

        SupervisionReturn ret = service.keyDatesCalculate(sv);

        assertEquals(ret.getSupervision().getAggregates().size(), 4);
        for (Aggregate aggregate : ret.getSupervision().getAggregates()) {
            List<Sentence> sentencesInAggregate = aggregate.getSentences();
            assertEquals(sentencesInAggregate.size(), 1);
            Sentence sentence = sentencesInAggregate.get(0);
            if (sentence.getSentenceId().equals(1l)) {
                assertEquals(sentence.getSentenceType(), SENTENCE_TYPE_DEF);
            } else if (sentence.getSentenceId().equals(2l)) {
                assertEquals(sentence.getSentenceType(), SENTENCE_TYPE_INDETERMINANT);
            } else if (sentence.getSentenceId().equals(3l)) {
                assertEquals(sentence.getSentenceType(), SENTENCE_TYPE_DETERMINANT);
            } else if (sentence.getSentenceId().equals(4l)) {
                assertEquals(sentence.getSentenceType(), SENTENCE_TYPE_SPLIT);
            }
        }

    }

    /**
     * WOR-9486  View dis-aggregate sentences: different types of sentence aggregate to one
     */
    @Test(enabled = true)
    public void WOR9486() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        //**************  Sentence 1 ************************
        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 2L, 2L, 0L, 2L, 0L);
        terms1.add(maxTerm1);

        Sentence sentence1 = new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("27/02/2014"), 0L, terms1);
        sentences.add(sentence1);

        //************** Sentence 2 *************************
        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 3L, 3L, 0L, 3L, 0L);
        terms2.add(maxTerm2);

        SentenceTerm minTerm2 = new SentenceTerm(CUSTODY, MIN_TERM, SINGLE, 2L, 2L, 0L, 2L, 0L);
        terms2.add(minTerm2);

        Sentence sentence2 = new Sentence(2L, 2L, 1L, SENTENCE_TYPE_INDETERMINANT, simpleDate("27/02/2014"), 0L, terms2);
        sentences.add(sentence2);

        //************** Sentence 3 *******************************
        Set<SentenceTerm> terms3 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm31 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 4L, 4L, 0L, 4L, 0L);
        terms3.add(maxTerm31);

        SentenceTerm maxTerm32 = new SentenceTerm(CUSTODY, MIN_TERM, SINGLE, 2L, 2L, 0L, 2L, 0L);
        terms3.add(maxTerm32);

        SentenceTerm maxTerm33 = new SentenceTerm(CUSTODY, COM_TERM, SINGLE, 0L, 0L, 0L, 0L, 12L);
        terms3.add(maxTerm33);

        Sentence sentence3 = new Sentence(3L, 3L, 1L, SENTENCE_TYPE_DETERMINANT, simpleDate("25/02/2014"), 0L, terms3);
        sentences.add(sentence3);

        //************** Sentence 4 *******************************
        Set<SentenceTerm> terms4 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm4 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 2L, 1L, 0L, 1L, 0L);
        terms4.add(maxTerm4);

        Sentence sentence4 = new Sentence(4L, 4L, 1L, SENTENCE_TYPE_SPLIT, simpleDate("27/02/2014"), 0L, terms4);
        sentences.add(sentence4);

        sv.setSentences(sentences);

        SupervisionReturn ret = service.keyDatesCalculate(sv);

        assertEquals(ret.getSupervision().getAggregates().size(), 4);

        for (Aggregate aggregate : ret.getSupervision().getAggregates()) {
            List<Sentence> sentencesInAggregate = aggregate.getSentences();
            assertEquals(sentencesInAggregate.size(), 1);
            Sentence sentence = sentencesInAggregate.get(0);
            if (sentence.getSentenceId().equals(1l)) {
                assertEquals(sentence.getSentenceType(), SENTENCE_TYPE_DEF);
            } else if (sentence.getSentenceId().equals(2l)) {
                assertEquals(sentence.getSentenceType(), SENTENCE_TYPE_INDETERMINANT);
            } else if (sentence.getSentenceId().equals(3l)) {
                assertEquals(sentence.getSentenceType(), SENTENCE_TYPE_DETERMINANT);
            } else if (sentence.getSentenceId().equals(4l)) {
                assertEquals(sentence.getSentenceType(), SENTENCE_TYPE_SPLIT);
            }
        }

    }

    /**
     * WOR-10104  GOOD TIME' credit not correct
     */
    @Test(enabled = true)
    public void WOR10104() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        //**************  Sentence 1 ************************
        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms1.add(maxTerm1);

        Sentence sentence1 = new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/03/2013"), 0L, terms1);
        sentences.add(sentence1);

        //************** Sentence 2 *************************
        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms2.add(maxTerm2);

        Sentence sentence2 = new Sentence(2L, 2L, 1L, SENTENCE_TYPE_DEF, simpleDate("18/06/2013"), 1L, terms2);
        sentences.add(sentence2);

        sv.setSentences(sentences);

        SupervisionReturn ret = service.keyDatesCalculate(sv);

        assertEquals(ret.getSupervision().getGoodTime(), Long.valueOf(121L));

    }

    /**
     * WOR-10102  GOOD TIME' calculation not correct
     */
    @Test(enabled = true)
    public void WOR10102() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        //**************  Sentence 1 ************************
        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms1.add(maxTerm1);

        Sentence sentence1 = new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("15/03/2013"), 0L, terms1);
        sentences.add(sentence1);

        //************** Sentence 2 *************************
        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 0L, 6L, 0L, 0L, 0L);
        terms2.add(maxTerm2);

        Sentence sentence2 = new Sentence(2L, 2L, 1L, SENTENCE_TYPE_DEF, simpleDate("01/08/2013"), 0L, terms2);
        sentences.add(sentence2);

        sv.setSentences(sentences);

        SupervisionReturn ret = service.keyDatesCalculate(sv);
        assertEquals(ret.getSupervision().getAggregates().size(), 2);
        assertEquals(ret.getSupervision().getGoodTime(), Long.valueOf(122L));

    }

    /**
     * WOR-9485  dis-aggregated sentence view not showing correct number of sentences when there are 2 definite sentences
     */
    @Test(enabled = true)
    public void WOR9485b() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        //**************  Sentence 1 ************************
        Set<SentenceTerm> terms1 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm1 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 1L, 0L, 0L, 0L, 0L);
        terms1.add(maxTerm1);

        Sentence sentence1 = new Sentence(1L, 1L, 1L, SENTENCE_TYPE_DEF, simpleDate("07/03/2014"), 0L, terms1);
        sentences.add(sentence1);

        //************** Sentence 2 *************************
        Set<SentenceTerm> terms2 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm2 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 1L, 0L, 0L, 0L, 0L);
        terms2.add(maxTerm2);

        Sentence sentence2 = new Sentence(2L, 2L, 1L, SENTENCE_TYPE_DEF, simpleDate("07/03/2014"), 0L, terms2);
        sentences.add(sentence2);

        //************** Sentence 3 *******************************
        Set<SentenceTerm> terms3 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm31 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 1L, 1L, 0L, 0L, 0L);
        terms3.add(maxTerm31);

        SentenceTerm maxTerm32 = new SentenceTerm(CUSTODY, MIN_TERM, SINGLE, 0L, 1L, 0L, 0L, 0L);
        terms3.add(maxTerm32);

        Sentence sentence3 = new Sentence(3L, 3L, 1L, SENTENCE_TYPE_INDETERMINANT, simpleDate("07/03/2014"), 0L, terms3);
        sentences.add(sentence3);

        sv.setSentences(sentences);

        SupervisionReturn ret = service.keyDatesCalculate(sv);

        assertEquals(ret.getSupervision().getAggregates().size(), 2);
        for (Aggregate aggregate : ret.getSupervision().getAggregates()) {
            List<Sentence> sentencesInAggregate = aggregate.getSentences();
            if (sentencesInAggregate.size() == 1) {
                Sentence sentence = sentencesInAggregate.get(0);
                assertEquals(sentence.getSentenceType(), SENTENCE_TYPE_INDETERMINANT);
            }

            if (sentencesInAggregate.size() == 2) {
                for (Sentence sentence : sentencesInAggregate) {
                    assertEquals(sentence.getSentenceType(), SENTENCE_TYPE_DEF);
                }
            }
        }
    }

    /**
     * WOR-8225  Sentence Calculation not correct if sentence type is associated with multiple terms
     */
    @Test(enabled = true)
    public void WOR8225() {
        Supervision sv = new Supervision();
        sv = getSupervision(sv);

        List<Sentence> sentences = new ArrayList<Sentence>();

        Set<SentenceTerm> terms3 = new HashSet<SentenceTerm>();
        SentenceTerm maxTerm31 = new SentenceTerm(CUSTODY, MAX_TERM, SINGLE, 4L, 6L, 0L, 0L, 0L);
        terms3.add(maxTerm31);

        SentenceTerm maxTerm32 = new SentenceTerm(CUSTODY, MIN_TERM, SINGLE, 2L, 1L, 0L, 0L, 0L);
        terms3.add(maxTerm32);

        Sentence sentence3 = new Sentence(3L, 3L, 1L, SENTENCE_TYPE_INDETERMINANT, simpleDate("15/03/2013"), 0L, terms3);
        sentences.add(sentence3);

        sv.setSentences(sentences);

        SupervisionReturn ret = service.keyDatesCalculate(sv);
        assertFalse(ret.getSupervision().getSentenceDays() == 1);

    }
    /*
     	Private Methods
     */

    private Date simpleDate(String datestr) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        try {

            date.setTime(format.parse(datestr).getTime());
        } catch (ParseException e) {
            throw new IllegalArgumentException("Could not parse date (" + datestr + ").", e);
        }
        return date;
    }

}
