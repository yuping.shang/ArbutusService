package syscon.arbutus.product.services.common;

import syscon.arbutus.product.services.housing.contract.interfaces.HousingService;

public class HousingBedActivityTest extends BaseIT {

    private HousingService service;

    public HousingBedActivityTest() {
        lookupJNDILogin();
    }

    public HousingBedActivityTest(HousingService service) {
        this.service = service;
    }

    private void lookupJNDILogin() {
        service = (HousingService) super.JNDILookUp(this.getClass(), HousingService.class);
        uc = super.initUserContext();
    }

    public void deleteAll() {
        service.deleteAll(uc);
    }

}
