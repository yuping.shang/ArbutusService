package syscon.arbutus.product.services.common;

import java.util.Date;

import syscon.arbutus.product.services.activity.contract.dto.ActivityCategory;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;

public class ActivityTest extends BaseIT {

    private ActivityService service;

    public ActivityTest() {
        lookupJNDILogin();
    }

    private void lookupJNDILogin() {
        service = (ActivityService) super.JNDILookUp(this.getClass(), ActivityService.class);
        uc = super.initUserContext();
    }

    public ActivityTest(ActivityService service) {
        this.service = service;
    }

    public Long createActivity(ActivityCategory category) {

        ActivityType ret = null;

        ActivityType dto = buildActivityType(category);

        ret = service.create(uc, dto, null, true);
        assert (ret != null);

        return ret.getActivityIdentification();

    }

    public Long createActivityForHousing() {
        Long ret = createActivity(ActivityCategory.HBDMGMT);
        return ret;
    }

    private ActivityType buildActivityType(ActivityCategory category) {

        Date currentDate = new Date();
        ActivityType activityType = new ActivityType();
        activityType.setActiveStatusFlag(false);
        activityType.setPlannedStartDate(currentDate);
        activityType.setPlannedEndDate(currentDate);
        activityType.setActivityCategory(category.value().toUpperCase());

        return activityType;
    }

    public Long createActivityFromHousing(Long supervisionId) {

        Date currentDate = new Date();
        ActivityType activityType = new ActivityType();
        activityType.setSupervisionId(supervisionId);
        activityType.setActiveStatusFlag(true);
        activityType.setPlannedStartDate(currentDate);
        activityType.setPlannedEndDate(currentDate);
        activityType.setActivityCategory(ActivityCategory.HBDMGMT.value());

        ActivityType ret = service.create(uc, activityType, null, true);

        return ret.getActivityIdentification();
    }

    public Long createActivity(Long supervisionId, Long parentActivityId) {

        Date currentDate = new Date();
        ActivityType activityType = new ActivityType();
        activityType.setActivityAntecedentId(parentActivityId);
        activityType.setSupervisionId(supervisionId);
        activityType.setActiveStatusFlag(false);
        activityType.setPlannedStartDate(currentDate);
        activityType.setPlannedEndDate(currentDate);
        activityType.setActivityCategory(ActivityCategory.MOVEMENT.value().toUpperCase());

        ActivityType ret = service.create(uc, activityType, null, true);

        return ret.getActivityIdentification();
    }

    public void deleteAll() {
        service.deleteAll(uc);
    }
}
