package syscon.arbutus.product.services.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines the module name to be used on JNDI Lookups.
 * Default is package name for a full build.
 * With the possibility of packaging per modules, it is necessary to change the module name
 * whenever it is packaged differently.
 * This annotations is meant to be used by unit test cases.
 *
 * @author William Madruga
 */

@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface ModuleConfig {

    String name() default "ArbutusService-1.0-SNAPSHOT";
}
