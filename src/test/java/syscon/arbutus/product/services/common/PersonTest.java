package syscon.arbutus.product.services.common;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.image.contract.dto.ImageFormat;
import syscon.arbutus.product.services.person.contract.dto.*;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.realization.util.BeanHelper;

public class PersonTest extends BaseIT {

    private PersonService service;

    public PersonTest() {
        lookupJNDILogin();
    }

    public PersonTest(PersonService service) {
        this.service = service;
    }

    private static PersonalInformationType createPersonInformation(String city) {

        PersonalInformationType ret = new PersonalInformationType();
        ret.setCitizenship("DEU");        //new CodeType(ReferenceSet.CITIZENSHIP.name(),
        ret.setNationality("USA");
        ret.setBirthCountry("USA");        //new CodeType(ReferenceSet.COUNTRY.name(),
        ret.setBirthState("WA");
        ret.setBirthCity(city);
        ret.setEthnicity("H");
        ret.setMaritalStatus("1");        //new CodeType(ReferenceSet.MARITALSTATUS.name(),
        ret.setNoOfDependents(3l);
        ret.setReligion("JEWIS");
        ret.setSexualOrientation("HETERO");

        return ret;
    }

    private static PhysicalIdentifierType createPhysicalIdentifier() {

        PhysicalIdentifierType ret = new PhysicalIdentifierType();
        ret.setBuild("HEAVY");                //new CodeType(ReferenceSet.BUILD.name(),
        ret.setEyeColorCode("BLK");
        ret.setEyewearDescription("GLASS");
        ret.setFacialHair("BEARD");
        ret.setHairAppearance("SHAVED");    //new CodeType(ReferenceSet.HAIRAPPEARANCE.name(),
        ret.setHairColor("BLK");
        ret.setHandDexterity("AMBI");
        ret.setHeight(180L);
        ret.setWeight(120L);
        ret.setRace("A");
        ret.setSkinTone("YEL");

        return ret;
    }

    private static EducationHistoryType createEducationHistory(String schoolName) {
        Date present = new Date();
        EducationHistoryType eduHist = new EducationHistoryType();
        eduHist.setAreaOfStudy("ENGLA");        //new CodeType(ReferenceSet.EDUCATIONSTUDYAREA.name(),
        eduHist.setSchool(schoolName);
        eduHist.setEducationLevel("ELEMENTARY");    //new CodeType(ReferenceSet.EDUCATIONLEVEL.name(),
        eduHist.setSchool("EMMA");
        eduHist.setEducationSchedule("FULL");
        eduHist.setSchoolStartDate(BeanHelper.getPastDate(present, 1000));
        eduHist.setSchoolEndDate(BeanHelper.getPastDate(present, 500));
        eduHist.setSchoolComment(schoolName + " School Comment");
        return eduHist;
    }

    private static EmploymentHistoryType createEmploymentHistory(String employerName) {
        Date present = new Date();
        EmploymentHistoryType emp = new EmploymentHistoryType();
        emp.setOccupation("23");            //new CodeType(ReferenceSet.EMPLOYEEOCCUPATION.name(),
        emp.setEmployerReference(employerName);
        emp.setCompensationRate(new BigDecimal("50.00"));
        emp.setHoursPerWeek(new BigDecimal("30.50"));
        emp.setSupervisorName("SupervisionName");
        emp.setTerminationReason("TerminationReason contract end");

        emp.setWorkSchedule("PART");
        emp.setEmployerAwared(true);
        emp.setEmployerContactable(false);
        emp.setEmploymentStatus("PT");
        emp.setEmploymentStartDate(BeanHelper.getPastDate(present, 1000));
        emp.setEmploymentEndDate(BeanHelper.getPastDate(present, 100));

        emp.setEmploymentComment(employerName + " Employment Comment");
        emp.setPayPeriod("HOUR");
        emp.setWage(new BigDecimal("50.00"));
        return emp;
    }

    private static HealthRecordHistoryType createHealthRecordHistory(String healthIssue) {
        Date present = new Date();
        HealthRecordHistoryType ret = new HealthRecordHistoryType();
        ret.setHealthCategory("PHYSICALNEED");        //new CodeType(ReferenceSet.HEALTHRECORDCATEGORY.name(),
        ret.setHealthIssue(healthIssue);            //new CodeType(ReferenceSet.HEALTHISSUE.name(),
        ret.setHealthStatus("CHRONIC");    //new CodeType(ReferenceSet.HEALTHISSUESTATUS.name(),
        ret.setHealthDescription(healthIssue + " HealthDescription");
        ret.setHealthStartDate(BeanHelper.getPastDate(present, 1000));
        ret.setHealthEndDate(BeanHelper.getPastDate(present, 500));

        Set<HealthRecordTreatmentType> treatments = new HashSet<HealthRecordTreatmentType>();
        HealthRecordTreatmentType treatment1 = createHealthRecordTreatment("HOSP");
        treatments.add(treatment1);
        HealthRecordTreatmentType treatment2 = createHealthRecordTreatment("PD");
        treatments.add(treatment2);
        ret.setTreatments(treatments);

        return ret;

    }

    private static HealthRecordTreatmentType createHealthRecordTreatment(String healthTreatment) {
        Date present = new Date();
        HealthRecordTreatmentType ret = new HealthRecordTreatmentType();
        ret.setTreatmentProvider("HOS");
        ret.setHealthTreatment(healthTreatment);
        ret.setTreatmentDescription(healthTreatment + "Description");
        ret.setTreatmentComment(healthTreatment + "Comment");
        ret.setTreatmentStartDate(BeanHelper.getPastDate(present, 1000));
        ret.setTreatmentEndDate(BeanHelper.getPastDate(present, 1000));

        return ret;
    }

    private static LanguageType createLanguage(String language, boolean isPrimary) {

        LanguageType ret = new LanguageType();
        ret.setLanguage(language);            //new CodeType(ReferenceSet.LANGUAGE.name(),
        ret.setSpeechDescription(language + "Description");
        ret.setPrimary(isPrimary);
        ret.setReading("LVL3");
        ret.setSpeaking("LVL3");
        ret.setWriting("LVL3");
        ret.setListening("LVL3");

        return ret;
    }

    private static MilitaryHistoryType createMilitaryHistory(String branch) {
        Date present = new Date();
        MilitaryHistoryType ret = new MilitaryHistoryType();
        ret.setBranch(branch);            //new CodeType(ReferenceSet.MILITARYBRANCH.name(),
        ret.setMilitaryComment(branch + "comment");
        ret.setDischargeCategory("B");
        ret.setDischargeDate(BeanHelper.getPastDate(present, 1000));
        ret.setDischargeLocation(branch + "DischargeLocation");
        ret.setDischargeRank("AB");
        ret.setEnlistDate(BeanHelper.getPastDate(present, 1000));
        ret.setEnlistLocation(branch + "EnlistLocation");
        ret.setExemptionDescription(branch + "ExemptionDescription");
        ret.setHighestRank("CAPT");
        ret.setServiceActive(false);
        ret.setServiceNumber("ServiceNumber");
        ret.setUnitNumber("UnitNumber");
        ret.getDisciplinaryActions().add("CM");
        ret.getDisciplinaryActions().add("DCM");
        ret.getWarZones().add("IRQ");            //new CodeType(ReferenceSet.MILITARYWARZONE.name(),
        ret.getWarZones().add("SVK");
        ret.getSpecializations().add("AVA");    //new CodeType(ReferenceSet.MILITARYSPECIALIZATION.name(),
        ret.getSpecializations().add("CLE");

        return ret;
    }

    private static PhysicalMarkType createPhysicalMark(String category) {

        Date present = new Date();
        PhysicalMarkType ret = new PhysicalMarkType();
        ret.setMarkCategory(category);            //new CodeType(ReferenceSet.PHYSICALMARK.name(),
        ret.setMarkDescription("flower");
        ret.setMarkLocation("ARM");            //new CodeType(ReferenceSet.PHYSICALMARKLOCATION.name(),
        ret.setBodySide("F");                //new CodeType(ReferenceSet.PHYSICALMARKSIDE.name(),
        ret.setMarkOrientation("CENTR");
        ret.setDeactivationDate(BeanHelper.getPastDate(present, 1000));

        ret.getMarkImages().add(createImage(true));
        ret.getMarkImages().add(createImage(false));

        ret.getDeactivationReasons().add(createComment("Reason1"));
        ret.getDeactivationReasons().add(createComment("Reason2"));

        return ret;
    }

    private static PhotoType createImage(boolean isDefault) {
        byte[] imageData;
        if (isDefault) {
            imageData = new byte[] { 1, 1, 1, 1 };
        } else {
            imageData = new byte[] { 2, 2, 2, 2 };
        }

        Date present = new Date();
        PhotoType ret = new PhotoType(imageData, ImageFormat.PNG);
        ret.setCaptureDate(present);
        ret.setDefaultImage(isDefault);
        ret.setPersonId(1L);
        return ret;
    }

    private static CommentType createComment(String comment) {
        Date present = new Date();
        CommentType ret = new CommentType();
        ret.setComment(comment);
        ret.setCommentDate(present);
        ret.setUserId("UserId");

        return ret;
    }

    private static SkillType createSkill(String skill, boolean isActive) {

        Date present = new Date();
        SkillType ret = new SkillType();
        ret.setSkill(skill);
        ret.setSubType("CCTV");
        ret.setActive(isActive);
        ret.setAcquisitionDate(BeanHelper.getPastDate(present, 1000));
        ret.setExpiryDate(BeanHelper.getPastDate(present, 50));
        ret.setSkillComment(skill + "Comment");

        return ret;

    }

    private static SubstanceAbuseType createSubstanceAbuse(String drug) {

        SubstanceAbuseType ret = new SubstanceAbuseType();
        ret.setDrugType(drug);                //new CodeType(ReferenceSet.DRUG.name(),
        ret.setAgeFirstUsed(30L);

        SubstanceAbuseTreatmentType tr = createSubstanceAbuseTreatment("MED");
        ret.getTreatments().add(tr);
        SubstanceAbuseTreatmentType tr2 = createSubstanceAbuseTreatment("COU");
        ret.getTreatments().add(tr2);

        SubstanceAbuseUseType use1 = createSubstanceAbuseUse("ADD");
        ret.getSubstanceUseLevels().add(use1);
        SubstanceAbuseUseType use2 = createSubstanceAbuseUse("FRE");
        ret.getSubstanceUseLevels().add(use2);

        return ret;

    }

    private static SubstanceAbuseTreatmentType createSubstanceAbuseTreatment(String treatment) {
        Date present = new Date();
        SubstanceAbuseTreatmentType ret = new SubstanceAbuseTreatmentType();
        ret.setTreatmentReceived(treatment);    //new CodeType(ReferenceSet.DRUGTREATMENT.name(),
        ret.setTreatmentProvider(treatment + "Provider");
        ret.setTreatmentStartDate(BeanHelper.getPastDate(present, 1000));
        ret.setTreatmentEndDate(BeanHelper.getPastDate(present, 100));
        ret.setTreatmentComment(treatment + "Comment");

        return ret;

    }

    private static SubstanceAbuseUseType createSubstanceAbuseUse(String useLevel) {

        SubstanceAbuseUseType ret = new SubstanceAbuseUseType();
        ret.setSubstanceUseLevel(useLevel);
        ret.setSubstanceInformationSource("INF");
        ret.setSubstanceUseComment(useLevel + "Comment");
        ret.setSubstanceUsePeriod(useLevel + "Period");

        return ret;

    }

    @SuppressWarnings("unused")
    private static Set<Long> createLocations(int startNum, int numOfLocations) {

        Set<Long> ret = new HashSet<Long>();

        for (int i = startNum; i < startNum + numOfLocations; i++) {
            ret.add(new Long(i + 1));
        }
        return ret;
    }

    private void lookupJNDILogin() {
        service = (PersonService) super.JNDILookUp(this.getClass(), PersonService.class);
        uc = super.initUserContext();
    }

    public Long createPerson() {
        PersonType dto = createSimplePerson();
        PersonType ret = service.createPersonType(uc, dto);
        return ret.getPersonIdentification();
    }

    public void deleteAll() {
        service.deleteAllPersonType(uc);
    }

    private PersonType createSimplePerson() {

        Date present = new Date();
        PersonType person = new PersonType();

        person.setActiveStatus("ACTIVE");        //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        person.setNumeracyProficiency("AVERAGE");    //new CodeType(ReferenceSet.NUMERACYSKILL.name(),
        person.setDeathDate(present);
        person.setAccent("Canadian Accent");

        EducationHistoryType eduHist = createEducationHistory("SFU");
        person.getEducationHistories().add(eduHist);

        EmploymentHistoryType emp = createEmploymentHistory("Google");
        person.getEmploymentHistories().add(emp);

        HealthRecordHistoryType hrh = createHealthRecordHistory("D");
        person.getHealthRecordHistories().add(hrh);

        LanguageType lan = createLanguage("EN", true);
        person.getLanguages().add(lan);

        MilitaryHistoryType mi = createMilitaryHistory("ARMY");
        person.getMilitaryHistories().add(mi);

        PersonalInformationType pi = createPersonInformation("VANCOUVER");
        person.setPersonalInformation(pi);

        PhysicalIdentifierType phi = createPhysicalIdentifier();
        person.setPhysicalIdentifier(phi);

        SkillType sk = createSkill("HOSNEG", true);
        person.getSkills().add(sk);

        SubstanceAbuseType sa = createSubstanceAbuse("D3");
        person.getSubstanceAbuses().add(sa);

        return person;
    }

    @SuppressWarnings("unused")
    private PersonType createWellFormedPerson() {

        Date present = new Date();
        PersonType person = new PersonType();

        person.setActiveStatus("ACTIVE");        //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        person.setNumeracyProficiency("AVERAGE");    //new CodeType(ReferenceSet.NUMERACYSKILL.name(),
        person.setDeathDate(present);
        person.setAccent("Canadian Accent");

        EducationHistoryType eduHist = createEducationHistory("SFU");
        person.getEducationHistories().add(eduHist);
        EducationHistoryType edu2 = createEducationHistory("UBC");
        person.getEducationHistories().add(edu2);

        EmploymentHistoryType emp = createEmploymentHistory("Google");
        person.getEmploymentHistories().add(emp);
        EmploymentHistoryType emp2 = createEmploymentHistory("Oracle");
        person.getEmploymentHistories().add(emp2);

        HealthRecordHistoryType hrh = createHealthRecordHistory("D");
        person.getHealthRecordHistories().add(hrh);
        HealthRecordHistoryType hrh2 = createHealthRecordHistory("DI");
        person.getHealthRecordHistories().add(hrh2);

        LanguageType lan = createLanguage("EN", true);
        person.getLanguages().add(lan);
        LanguageType fr = createLanguage("FR", false);
        person.getLanguages().add(fr);

        MilitaryHistoryType mi = createMilitaryHistory("ARMY");
        person.getMilitaryHistories().add(mi);
        MilitaryHistoryType mi2 = createMilitaryHistory("AIR");
        person.getMilitaryHistories().add(mi2);

        PersonalInformationType pi = createPersonInformation("VANCOUVER");
        person.setPersonalInformation(pi);

        PhysicalIdentifierType phi = createPhysicalIdentifier();
        person.setPhysicalIdentifier(phi);

        PhysicalMarkType pm = createPhysicalMark("TAT");
        person.getPhysicalMarks().add(pm);
        PhysicalMarkType pm2 = createPhysicalMark("MARK");
        person.getPhysicalMarks().add(pm2);

        SkillType sk = createSkill("HOSNEG", true);
        person.getSkills().add(sk);
        SkillType sk2 = createSkill("OFMANGT", true);
        person.getSkills().add(sk2);

        SubstanceAbuseType sa = createSubstanceAbuse("D3");
        person.getSubstanceAbuses().add(sa);
        SubstanceAbuseType sa2 = createSubstanceAbuse("B2");
        person.getSubstanceAbuses().add(sa2);

        return person;
    }

}
