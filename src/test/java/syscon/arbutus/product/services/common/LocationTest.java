package syscon.arbutus.product.services.common;

import syscon.arbutus.product.services.location.contract.dto.*;
import syscon.arbutus.product.services.location.contract.interfaces.LocationService;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class LocationTest extends BaseIT {

    private LocationService service;

    public LocationTest() {
        lookupJNDILogin();
    }

    private void lookupJNDILogin() {
        service = (LocationService) super.JNDILookUp(this.getClass(), LocationService.class);
        uc = super.initUserContext();
    }

    public Location createLocation(boolean primary, Long personId, Long facilityId) {
        Location loc = createLocationType(primary, personId, facilityId);
        Location ret = service.create(uc, loc);
        return ret;
    }

    public Location createLocationType(boolean primary, Long personId, Long facilityId) {

        Location l = new Location();

        if (facilityId != null) {
            l.setAddressCategory(AddressCategory.FACILITY);
            l.setInstanceId(facilityId);
        }

        if (personId != null) {
            l.setAddressCategory(AddressCategory.PERSON);
            l.setInstanceId(personId);
        }
        l.setLocationName("Location Name " + personId + facilityId);
        l.setLocationDescriptionText("The locationDescriptionText");
        l.setLocationCrossStreet("the location cross street");
        l.setLocationActiveFlag(null);
        l.setLocationStartDate(getDate(2011, 1, 28));
        l.setLocationEndDate(getDate(2012, 1, 28));
        l.setLocationPrimaryFlag(primary);

        Set<String> typeSet = new HashSet<>();
        typeSet.add(new String("MAILING"));
        l.setTypeOfLocationType(typeSet);

        Set<String> lcs = new HashSet<>();
        lcs.add(new String("0" + personId + facilityId));

        l.setLocationCategory(lcs);

        // set LocationAddressType
        LocationAddressType la = new LocationAddressType();
        //		la.setInternationalAddressLine("The internationalAddressLine"); // must be set alone only
        la.setLocationCountryName(new String("CAN"));
        la.setAddressBuildingText("the addressBuildingText");
        la.setLocationStreetNumSuffix("The locationStreetNumSuffix");
        la.setLocationStreetNumberText("12");
        la.setLocationStreetName("The locationStreetName");
        la.setLocationStreetCategory(new String("AVE"));
        la.setLocationStreetPredirectionalText(new String("E"));
        la.setLocationStreetPostdirectionalText(new String("E"));
        la.setLocationUnitNumber("13");
        la.setLocationFloorNum(12l);
        la.setLocationCityName("vancouver " + personId + facilityId);
        la.setLocationStateCode(new String("AB"));
        la.setLocationPostalCode("V" + personId + facilityId + "X 1S2");
        la.setLocationPostalExtensionCode("12-1");
        la.setAddressDeliveryMode(new String("MS"));
        la.setAddressDeliveryModeNumber(12l);
        la.setLocationCountyName(new String("037-06"));
        la.setLocationLocalityName("the locality name");

        l.setLocationAddress(la);

        Set<LocationPhoneFaxType> lpfns = new HashSet<>();

        LocationPhoneFaxType lpft = new LocationPhoneFaxType();
        lpft.setLocationPhoneFaxNumberCategory(LocationPhoneFaxType.HOME_PHONE);
        lpft.setLocationPhoneFaxNumber("the locationPhoneFaxNumber " + personId + facilityId);
        lpft.setLocationPhoneExtension("the locationPhoneExtension");

        lpfns.add(lpft);

        l.setLocationPhoneFaxNumber(lpfns);

        //
        Set<String> les = new HashSet<>();
        les.add("cw@abm.com");
        les.add("email2@hotm.com");

        l.setLocationEmail(les);

        // locationLocale;
        Set<LocationLocaleType> lls = new HashSet<>();

        LocationLocaleType llt = new LocationLocaleType();
        llt.setLocaleCommunityName("the localeCommunityName ");
        llt.setLocaleDescriptionText("the localeDescriptionText ");
        llt.setLocaleDistrictName("the localeDistrictName");
        llt.setLocaleEmergencyServiceCityName(" the localeEmergencyServiceCityName");
        llt.setLocaleFireJurisdictionID("the localeFireJurisdictionID");
        llt.setLocaleJudicialDistrictName(" the localeJudicialDistrictName");
        llt.setLocaleJudicialDistrictCode("the localeJudicialDistrictCode");
        llt.setLocaleNeighborhoodName("the localeNeighborhoodName");
        llt.setLocalePoliceBeatText("the localePoliceBeatText");
        llt.setLocalePoliceGridText("the localePoliceGridText");
        llt.setLocalePoliceJurisdictionID("the localePoliceJurisdictionID");
        llt.setLocaleRegionName("the localeRegionName " + personId + facilityId);
        llt.setLocaleSubdivisionName("the localeSubdivisionName");
        llt.setLocaleZoneName("the localeZoneName");
        llt.setLocaleEmergencyServiceJurisdictionID("the localeEmergencyServiceJurisdictionID");

        lls.add(llt);

        LocationLocaleType llt2 = new LocationLocaleType();
        llt2.setLocaleCommunityName("the localeCommunityName ");
        llt2.setLocaleDescriptionText("the localeDescriptionText ");
        llt2.setLocaleDistrictName("the localeDistrictName");
        llt2.setLocaleEmergencyServiceCityName(" the localeEmergencyServiceCityName");
        llt2.setLocaleFireJurisdictionID("the localeFireJurisdictionID");
        llt2.setLocaleJudicialDistrictName(" the localeJudicialDistrictName");
        llt2.setLocaleJudicialDistrictCode("the localeJudicialDistrictCode");
        llt2.setLocaleNeighborhoodName("the localeNeighborhoodName");
        llt2.setLocalePoliceBeatText("the localePoliceBeatText");
        llt2.setLocalePoliceGridText("the localePoliceGridText");
        llt2.setLocalePoliceJurisdictionID("the localePoliceJurisdictionID");
        llt2.setLocaleRegionName("the localeRegionName " + personId + facilityId);
        llt2.setLocaleSubdivisionName("the localeSubdivisionName");
        llt2.setLocaleZoneName("the localeZoneName");
        llt2.setLocaleEmergencyServiceJurisdictionID("the localeEmergencyServiceJurisdictionID");

        lls.add(llt2);

        l.setLocationLocale(lls);

        // Set<String> locationSurroundingAreaDescriptionText;
        Set<String> lsadts = new HashSet<>();
        lsadts.add("left corrner");

        l.setLocationSurroundingAreaDescriptionText(lsadts);

        // LocationTwoDimentionalGeographicalCoordinateType
        LocationTwoDimentionalGeographicalCoordinateType ltdgct = new LocationTwoDimentionalGeographicalCoordinateType();
        ltdgct.setGeographicDatumCode(new String("ADI-A"));
        ltdgct.setLatitudeDegreeValue(null);
        ltdgct.setLatitudeMinuteValue(33L);
        ltdgct.setLatitudeSecondValue(new BigDecimal(3.123));
        ltdgct.setLongtitudeDegreeValue(44L);
        ltdgct.setLongtitudeMinuteValue(55L);
        ltdgct.setLongtitudeSecondValue(new BigDecimal(6.89788));
        ltdgct.setGeographicDatumCode(new String("ADI-B"));

        l.setLocationTwoDimentionalGeographicalCoordinate(ltdgct);

        Set<String> surrSet = new HashSet<>();
        surrSet.add("A");
        surrSet.add("B");

        l.setLocationSurroundingAreaDescriptionText(surrSet);

        return l;
    }

    public void deleteAll() {
        service.deleteAll(uc);
    }

    private Date getDate(int year, int month, int date) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, date, 0, 0, 0);
        return cal.getTime();
    }

}
