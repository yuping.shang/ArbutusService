package syscon.arbutus.product.services.common;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by os on 24/06/15.
 */
public class HibernateUtil {
    private final static Logger LOGGER = LoggerFactory.getLogger(HibernateUtil.class);

    private final static Configuration configuration = new Configuration();
    private final static ServiceRegistry serviceRegistry;
    private final static SessionFactory sessionFactory;
    private final static ThreadLocal<Session> SESSION_THREAD_LOCAL = new ThreadLocal<>();

    static {
        try {
            configuration.configure(HibernateUtil.class.getResource("/hibernate.cfg.xml"));
            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Exception exp) {
            LOGGER.error(exp.getMessage(), exp);
            throw exp;
        }
    }

    public static Session getSession() {
        if (SESSION_THREAD_LOCAL.get() != null) {
            return SESSION_THREAD_LOCAL.get();
        }
        final Session session = sessionFactory.openSession();
        session.beginTransaction();

        SESSION_THREAD_LOCAL.set(session);
        return session;
    }

    public static void close() {
        if (SESSION_THREAD_LOCAL.get() != null) {
            final Session session = SESSION_THREAD_LOCAL.get();
            session.getTransaction().rollback();
            session.close();
            SESSION_THREAD_LOCAL.set(null);
        }
    }

    public static void commitAndClose() {
        if (SESSION_THREAD_LOCAL.get() != null) {
            final Session session = SESSION_THREAD_LOCAL.get();
            session.getTransaction().commit();
            session.close();
            SESSION_THREAD_LOCAL.set(null);
        }
    }

    public static void clearSession() {
        if (SESSION_THREAD_LOCAL.get() != null) {
            final Session session = SESSION_THREAD_LOCAL.get();
            session.flush();
            session.clear();
        }
    }
}