package syscon.arbutus.product.services.common;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static mockit.Deencapsulation.setField;

/**
 * Created by os on 26/06/15.
 */
public class ServiceProxy implements java.lang.reflect.InvocationHandler {
    private final static Logger LOGGER = LoggerFactory.getLogger(ServiceProxy.class);

    private final Object obj;
    private Boolean cleanSession = Boolean.TRUE;

    public static <T> T newInstance(final T obj, final Boolean cleanSession) {
        final ServiceProxy serviceProxy = new ServiceProxy(obj);
        serviceProxy.cleanSession = cleanSession;
        return (T) java.lang.reflect.Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(), serviceProxy);
    }

    public static <T> T newInstance(final T obj) {
        return (T) java.lang.reflect.Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(), new ServiceProxy(obj));
    }

    private ServiceProxy(final Object obj) {
        this.obj = obj;
    }

    public Object getUnwrappedObject() {
        return this.obj;
    }

    public Object invoke(final Object proxy, final Method m, final Object[] args) throws Throwable {
        final Object result;
        try {
            LOGGER.debug("before method " + m.getName());
            final Session session = HibernateUtil.getSession();
            setField(obj, "session", session);
            result = m.invoke(obj, args);
        } catch (final InvocationTargetException e) {
            throw e.getTargetException();
        } finally {
            if (cleanSession) {
                HibernateUtil.clearSession();
            }
            LOGGER.debug("after method " + m.getName());
        }
        return result;
    }
}