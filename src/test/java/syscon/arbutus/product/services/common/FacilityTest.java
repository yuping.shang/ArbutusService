package syscon.arbutus.product.services.common;

import java.util.Date;
import java.util.Random;

import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;

public class FacilityTest extends BaseIT {

    private FacilityService service;
    private OrganizationTest orgService;
    private Long personId = 0L;

    public FacilityTest() {
        lookupJNDILogin();
    }

    public FacilityTest(FacilityService service) {
        this.service = service;
    }

    private void lookupJNDILogin() {
        service = (FacilityService) super.JNDILookUp(this.getClass(), FacilityService.class);
        uc = super.initUserContext();

    }

    public Long createFacility() {

        Facility ret = null;

        Facility dto = buildFacilityType(null);

        orgService = new OrganizationTest();
        Long orgId = orgService.createOrganization();
        dto.setParentOrganizationId(orgId);
        ret = service.create(uc, dto);
        assert (ret != null);

        return ret.getFacilityIdentification();

    }

    private Facility buildFacilityType(Long personId) {

        Facility facility = new Facility(-1L, "FacilityCodeCommon" + new Random().nextLong(), "FacilityName", new Date(), "COMMU", null, null, null, personId, false);    //To be updated for ContactId

        return facility;
    }

    public Long deleteFacility(Long facilityId) {
        return service.delete(uc, facilityId, personId);
    }

    public void deleteAll() {
        service.deleteAll(uc);
    }
}
