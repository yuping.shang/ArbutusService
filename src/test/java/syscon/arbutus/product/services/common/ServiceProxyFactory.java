package syscon.arbutus.product.services.common;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static mockit.Deencapsulation.setField;

/**
 * Created by os on 9/30/15.
 */
public class ServiceProxyFactory {
    private final static Logger LOGGER = LoggerFactory.getLogger(ServiceProxyFactory.class);

    private final Class service;
    private Boolean cleanSession = Boolean.TRUE;

    public ServiceProxyFactory(final Class service) {
        this.service = service;
    }

    public ServiceProxyFactory(final Class service, final Boolean cleanSession) {
        this.service = service;
        this.cleanSession = cleanSession;
    }

    public Object create(final Class[] paramTypes, final Object[] args) {
        final ProxyFactory factory = new ProxyFactory();
        factory.setSuperclass(service);
        final MethodHandler handler = new MethodHandler() {
            @Override
            public Object invoke(final Object obj, final Method m, final Method proceed, final Object[] args) throws Throwable {
                try {
                    final Session session = HibernateUtil.getSession();
                    setField(obj, "session", session);
                } catch (final IllegalArgumentException e) {
                    //do nothing
                    LOGGER.info(e.getMessage(), e);
                } finally {
                    if (cleanSession) {
                        HibernateUtil.clearSession();
                    }
                }
                return proceed.invoke(obj, args);
            }
        };

        try {
            return factory.create(paramTypes, args, handler);
        } catch (final NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalAccessException e) {
            LOGGER.error(e.getMessage(), e);
        }

        throw new RuntimeException(String.format("ServiceProxyFactory failed for %s", service));
    }

}
