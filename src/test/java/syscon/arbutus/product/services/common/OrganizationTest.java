package syscon.arbutus.product.services.common;

import java.util.HashSet;
import java.util.Set;

import syscon.arbutus.product.services.organization.contract.dto.Organization;
import syscon.arbutus.product.services.organization.contract.interfaces.OrganizationService;

public class OrganizationTest extends BaseIT {

    private OrganizationService service;

    public OrganizationTest() {
        lookupJNDILogin();
    }

    private void lookupJNDILogin() {
        service = (OrganizationService) super.JNDILookUp(this.getClass(), OrganizationService.class);
        uc = super.initUserContext();
    }

    public Long createOrganization() {
        Set<String> categories = new HashSet<String>();
        categories.add("CJ");
        Organization org = new Organization();
        org.setOrganizationName("New York Department of Correction");
        org.setOrganizationCategories(categories);
        org.setOrganizationLocalId(null);
        org.setOrganizationAbbreviation("NYDOC");
        org.setOrganizationORIIdentification(null);
        Organization createdOrg = service.create(uc, org);
        return createdOrg.getOrganizationIdentification();
    }

    public Long deleteOrganization(Long organizationId) {
        return service.delete(uc, organizationId);
    }
}
