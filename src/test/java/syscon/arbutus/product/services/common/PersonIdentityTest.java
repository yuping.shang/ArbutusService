package syscon.arbutus.product.services.common;

import java.util.*;

import syscon.arbutus.product.services.person.contract.dto.personidentity.IdentifierType;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;

/**
 * This is for creating the PersonIdentity data for unit test only.
 * (Not use for IT test).
 *
 * @author byu
 * @since Aug 12, 2013
 */
public class PersonIdentityTest extends BaseIT {

    private PersonService service;
    private PersonTest psnTest;

    public PersonIdentityTest() {
        lookupJNDILogin();
        psnTest = new PersonTest();
    }

    public PersonIdentityTest(PersonService service, PersonTest psnTest) {
        this.service = service;
        this.psnTest = psnTest;
    }

    private void lookupJNDILogin() {
        service = (PersonService) super.JNDILookUp(this.getClass(), PersonService.class);
        uc = super.initUserContext();
    }

    public Long createPersonIdentity() {
        return createPersonIdentityType().getPersonIdentityId();
    }

    public PersonIdentityType createPersonIdentityType() {

        //Create Person
        Long personId = psnTest.createPerson();

        //Create Person Identity
        PersonIdentityType personIdentity = null;
        Date dob = getDate(1985, 3, 10);
        Set<IdentifierType> identifiers = null;

        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", getDate(1980, 3, 10), getDate(1975, 3, 10), "G", "XXXX-XX", "Active"));

        personIdentity = new PersonIdentityType(null, personId, null, "MR", "Jason" + personId, "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, "OffenderNumber1",
                null, null);

        PersonIdentityType ret = service.createPersonIdentityType(uc, personIdentity, true, false);

        return ret;
    }

    public List<Long> createPersonIdentity(List<Long> personIds) {

        List<Long> ret = new ArrayList<Long>();

        for (Long personId : personIds) {
            Long personIdentityId = createPersonIdentity(personId);
            ret.add(personIdentityId);
        }

        return ret;

    }

    public Long createPersonIdentity(Long personId) {

        PersonIdentityType personIdentity = null;
        Date dob = getDate(1985, 3, 10);
        Set<IdentifierType> identifiers = null;

        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", getDate(1980, 3, 10), getDate(1975, 3, 10), "G", "XXXX-XX", "Active"));

        personIdentity = new PersonIdentityType(null, personId, null, "MR", "Jason" + personId, "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, null, null, null);

        PersonIdentityType ret = service.createPersonIdentityType(uc, personIdentity, true, false);

        return ret.getPersonIdentityId();

    }

    public void deleteAll() {
        service.deleteAllPersonIdentityType(uc);
    }

    private Date getDate(int year, int month, int date) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, date, 0, 0, 0);
        return cal.getTime();
    }
}
