package syscon.arbutus.product.services.common;

import java.util.*;

import syscon.arbutus.product.services.facilityinternallocation.contract.dto.*;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class FacilityInternalLocationTest extends BaseIT {

    protected Long facilityId;
    protected FacilityInternalLocation faclity2root;
    protected FacilityInternalLocation child211;
    protected FacilityInternalLocation child212;
    protected FacilityInternalLocation child221;
    protected FacilityInternalLocation child222;
    protected FacilityInternalLocation child223;
    protected FacilityInternalLocation child224;
    protected FacilityInternalLocation child231;
    protected FacilityInternalLocation child232;
    protected FacilityInternalLocation child233;
    protected FacilityInternalLocation child234;
    protected FacilityInternalLocation child235;
    protected FacilityInternalLocation child236;
    protected FacilityInternalLocation child237;
    protected FacilityInternalLocation child238;
    protected FacilityInternalLocation child239;

    private FacilityInternalLocationService service;

    public FacilityInternalLocationTest() {
        lookupJNDILogin();
    }

    public FacilityInternalLocationTest(FacilityInternalLocationService service) {
        this.service = service;
    }

    /**
     * Check if a set is null or empty
     *
     * @param set
     * @return true for null or empty
     */
    public static boolean isEmpty(Set<?> set) {
        return set == null || set.size() == 0;
    }

    /**
     * check if a set is not null or empty
     *
     * @param set
     * @return true for not null or empty
     */
    public static boolean isNotEmpty(Set<?> set) {
        return !isEmpty(set);
    }

    private void lookupJNDILogin() {
        service = (FacilityInternalLocationService) super.JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        uc = super.initUserContext();
    }

    public void createFILTree(Long facilityId) {
        testCreateAllHierchyLevelsl(facilityId);
        createFacilityInternalLocationTree(facilityId);
        this.facilityId = facilityId;

    }

    public Long getFacilityId() {
        return facilityId;
    }

    public Long getRootId() {
        return faclity2root.getId();
    }

    public List<Long> getLeafIds() {
        List<Long> ret = new ArrayList<Long>();
        ret.add(child231.getId());
        ret.add(child232.getId());
        ret.add(child233.getId());
        ret.add(child234.getId());
        ret.add(child235.getId());
        ret.add(child236.getId());
        ret.add(child237.getId());
        ret.add(child238.getId());
        ret.add(child239.getId());
        return ret;
    }

    public Long getCell1() {
        return child231.getId();
    }

    public Long getCell2() {
        return child232.getId();
    }

    public Long getCell3() {
        return child233.getId();
    }

    public Long getCell4() {
        return child234.getId();
    }

    public List<Long> getAllChildrenAndRoot() {
        List<Long> ret = new ArrayList<>();
        ret.add(child239.getId());
        ret.add(child238.getId());
        ret.add(child237.getId());
        ret.add(child236.getId());
        ret.add(child235.getId());
        ret.add(child234.getId());
        ret.add(child233.getId());
        ret.add(child232.getId());
        ret.add(child231.getId());
        ret.add(child224.getId());
        ret.add(child223.getId());
        ret.add(child222.getId());
        ret.add(child221.getId());
        ret.add(child212.getId());
        ret.add(child211.getId());
        ret.add(faclity2root.getId());
        return ret;
    }

    public void deleteAll() {
        service.deleteAll(uc);
    }

    @SuppressWarnings("unused")
    private void deleteNonAssociation(Long facilityInternalLocationId, String code) {

        FacilityInternalLocation ret = service.get(uc, facilityInternalLocationId);
        assertNotNull(ret);
        FacilityInternalLocation fil = ret;

        boolean isExist = false;
        for (String dto : fil.getNonAssociations()) {
            if (dto != null && dto.equalsIgnoreCase(code)) {
                isExist = true;
            }
        }

        if (isExist) {
            fil.setNonAssociations(null);
        }

        ret = service.update(uc, fil);
        assertNotNull(ret);
    }

    @SuppressWarnings("unused")
    private void updateNonAssociation(Long facilityInternalLocationId, String code) {

        FacilityInternalLocation ret = service.get(uc, facilityInternalLocationId);
        assertNotNull(ret);
        FacilityInternalLocation fil = ret;
        addProperties(fil, new String[] { FacilityInternalLocationMetaSet.INTERNAL_LOCATION_NONASSOCIATION, code });
        ret = service.update(uc, fil);
        assertNotNull(ret);
    }

    //@Test
    public void testLocationConflict() {

        Set<Long> checkIds = new HashSet<Long>();
        checkIds.add(33L);
        Set<NonAssociationConflictType> nonAssocSet = new HashSet<NonAssociationConflictType>();
        NonAssociationConflictType nonAssoc = new NonAssociationConflictType();
        nonAssoc.setNaInternalLocationId(29L);
        nonAssoc.setNaOffenderSupervisionId(3678L);
        nonAssoc.setNonAssociation(new String("STG"));
        nonAssocSet.add(nonAssoc);

        List<HousingNonAssociationConflictType> ret = service.getHousingNonAssociationConflicts(uc, checkIds, nonAssocSet);
        assertNotNull(ret);
        assertEquals(ret.size(), 1l);

    }

    private FacilityInternalLocation createFacilityInternalLocation(Long facilityId, Long parentId, String level, String code, String description,
            Set<String[]> props, UsageCategory usageCategory) {
        FacilityInternalLocation pt = new FacilityInternalLocation();
        pt.setFacilityId(facilityId);
        pt.setHierarchyLevel(level);
        pt.setLocationCode(code);
        pt.setDescription(description);
        pt.setParentId(parentId);
        if (isNotEmpty(props)) {
            for (String[] ct : props) {
                addProperties(pt, ct);
            }
        }

        FacilityInternalLocation ret = service.create(uc, pt, usageCategory);
        assertNotNull(ret);
        FacilityInternalLocation created = ret;
        assertNotNull(created);
        return created;
    }

    private void createFacilityInternalLocationTree(Long facilityId) {

        Set<String[]> prop1 = new java.util.HashSet<String[]>();
        prop1.add(new String[] { FacilityInternalLocationMetaSet.INTERNAL_LOCATION_NONASSOCIATION, "STG" });
        prop1.add(new String[] { FacilityInternalLocationMetaSet.INTERNAL_LOCATION_NONASSOCIATION, "TOTAL" });

        faclity2root = createFacilityInternalLocation(facilityId, null, "FACILITY", "Root2", "physical location root facility 2", prop1, UsageCategory.HOU);
        child211 = createFacilityInternalLocation(facilityId, faclity2root.getId(), "WING", "Wing 2-1", "first Level 1 child in facility 2", null,
                UsageCategory.HOU); //Swing
        child212 = createFacilityInternalLocation(facilityId, faclity2root.getId(), "WING", "Wing 2-2", "second Level 1 child in facility 2", null, UsageCategory.HOU);

        child221 = createFacilityInternalLocation(facilityId, child211.getId(), "UNIT", "Unit 2-1", "first Level 2 child in facility 2", prop1,
                UsageCategory.HOU);  //Floor
        child222 = createFacilityInternalLocation(facilityId, child211.getId(), "UNIT", "Unit 2-2", "second Level 2 child in facility 2", prop1, UsageCategory.HOU);

        Set<String[]> prop2 = new java.util.HashSet<String[]>();
        prop2.add(new String[] { FacilityInternalLocationMetaSet.AGE_RANGE, "JUVENILE" });
        prop2.add(new String[] { FacilityInternalLocationMetaSet.LOCATION_PROPERTY, "WHEELCHAIR" });
        prop2.add(new String[] { FacilityInternalLocationMetaSet.LOCATION_PROPERTY, "SHOWER" });
        prop2.add(new String[] { FacilityInternalLocationMetaSet.SECURITY_LEVEL, "MAX" });
        prop2.add(new String[] { FacilityInternalLocationMetaSet.SENTENCE_STATUS, "SENTENCED" });
        prop2.add(new String[] { FacilityInternalLocationMetaSet.OFFENDER_CATEGORY, "SUIC" });
        prop2.add(new String[] { FacilityInternalLocationMetaSet.OFFENDER_GENDER, "M" });
        child231 = createFacilityInternalLocation(facilityId, child221.getId(), "ROOM", "ROOM 2-1", "first Level 3 child in facility 2", prop2, UsageCategory.HOU); //Cell
        String subType = new String("PC");
        createUsage(UsageCategory.HOU, subType, child231.getId(), "desc Room231", 8l, 2l, "Room231");

        Set<String[]> prop3 = new java.util.HashSet<String[]>();
        prop3.add(new String[] { FacilityInternalLocationMetaSet.AGE_RANGE, "ADULT" });
        prop3.add(new String[] { FacilityInternalLocationMetaSet.SECURITY_LEVEL, "MIN" });
        prop3.add(new String[] { FacilityInternalLocationMetaSet.SENTENCE_STATUS, "UNSENTENCED" });
        prop3.add(new String[] { FacilityInternalLocationMetaSet.OFFENDER_GENDER, "F" });
        child232 = createFacilityInternalLocation(facilityId, child221.getId(), "ROOM", "ROOM 2-2", "second Level 3 child in facility 2", prop3,
                UsageCategory.HOU);  //Cell
        createUsage(UsageCategory.HOU, subType, child232.getId(), "desc Room232", 8l, 2l, "Room232");

        //Create another floor for swing 2
        child223 = createFacilityInternalLocation(facilityId, child212.getId(), "UNIT", "Unit 2-1", "first Level 2 child in facility 2", prop1,
                UsageCategory.HOU);  //Floor
        child224 = createFacilityInternalLocation(facilityId, child212.getId(), "UNIT", "Unit 2-2", "second Level 2 child in facility 2", null, UsageCategory.HOU);

        //Create leaf room under unit 1
        child233 = createFacilityInternalLocation(facilityId, child223.getId(), "ROOM", "ROOM 2-1", "Wing2-Unit1-Room1", prop2, UsageCategory.HOU); //Cell
        createUsage(UsageCategory.HOU, subType, child233.getId(), "desc Room233", 8l, 2l, "Room233");

        child234 = createFacilityInternalLocation(facilityId, child223.getId(), "ROOM", "ROOM 2-2", "Wing2-Unit1-Room2", prop3, UsageCategory.HOU);  //Cell
        createUsage(UsageCategory.HOU, subType, child234.getId(), "desc Room234", 8l, 2l, "Room234");

        child235 = createFacilityInternalLocation(facilityId, child223.getId(), "ROOM", "ROOM 2-3", "Wing2-Unit1-Room3", prop3, UsageCategory.HOU);  //Cell
        createUsage(UsageCategory.HOU, subType, child235.getId(), "desc Room235", 8l, 2l, "Room235");

        child236 = createFacilityInternalLocation(facilityId, child223.getId(), "ROOM", "ROOM 2-4", "Wing2-Unit1-Room4", prop3, UsageCategory.HOU);  //Cell
        createUsage(UsageCategory.HOU, subType, child236.getId(), "desc Room236", 8l, 2l, "Room236");

        child237 = createFacilityInternalLocation(facilityId, child223.getId(), "ROOM", "ROOM 2-5", "Wing2-Unit1-Room5", prop3, UsageCategory.HOU);  //Cell
        createUsage(UsageCategory.HOU, subType, child237.getId(), "desc Room237", 8l, 2l, "Room237");

        child238 = createFacilityInternalLocation(facilityId, child223.getId(), "ROOM", "ROOM 2-6", "Wing2-Unit1-Room6", prop3, UsageCategory.HOU);  //Cell
        createUsage(UsageCategory.HOU, subType, child238.getId(), "desc Room238", 8l, 2l, "Room238");

        child239 = createFacilityInternalLocation(facilityId, child223.getId(), "ROOM", "ROOM 2-7", "Wing2-Unit1-Room7", prop3, UsageCategory.HOU);  //Cell
        createUsage(UsageCategory.HOU, subType, child239.getId(), "desc Room239", 8l, 2l, "Room239");

    }

    private void addProperties(FacilityInternalLocation fil, String[] ct) {
        if (fil.getLocationAttribute() == null) {
            LocationAttributeType attr = new LocationAttributeType();
            fil.setLocationAttribute(attr);
        }
        String propName = ct[0];
        switch (propName) {
            case FacilityInternalLocationMetaSet.AGE_RANGE:
                fil.getLocationAttribute().getAgeRanges().add(ct[1]);
                break;
            case FacilityInternalLocationMetaSet.INTERNAL_LOCATION_NONASSOCIATION:
                fil.getNonAssociations().add(ct[1]);
                break;
            case FacilityInternalLocationMetaSet.OFFENDER_CATEGORY:
                fil.getLocationAttribute().getOffenderCategories().add(ct[1]);
                break;
            case FacilityInternalLocationMetaSet.OFFENDER_GENDER:
                fil.getLocationAttribute().getGenders().add(ct[1]);
                break;
            case FacilityInternalLocationMetaSet.LOCATION_PROPERTY:
                fil.getLocationAttribute().getLocationProperties().add(ct[1]);
                break;
            case FacilityInternalLocationMetaSet.SECURITY_LEVEL:
                fil.getLocationAttribute().getSecurityLevels().add(ct[1]);
                break;
            case FacilityInternalLocationMetaSet.SENTENCE_STATUS:
                fil.getLocationAttribute().getSentenceStatuses().add(ct[1]);
                break;
        }

    }

    private UsageType createUsage(UsageCategory category, String subtype, Long locationId, String usageDesc, Long optimalCapacity, Long overflowcapacity,
            String usageCode) {
        UsageType h = new UsageType();
        h.setUsageCategory(category);
        h.setUsageSubtype(subtype);
        h.setInternalLocationId(locationId);
        h.setDescription(usageDesc);
        h.setUsageCode(usageCode);
        CapacityType cp = new CapacityType();
        cp.setUsageCategory(h.getUsageCategory());
        cp.setUsageType(h.getUsageSubtype());
        cp.setOptimalCapacity(optimalCapacity);
        cp.setOverflowCapacity(overflowcapacity);
        cp.setInternalLocationId(locationId);
        h.setCapacity(cp);
        h.setUsageAllowed(true);

        UsageType ret = service.addUsage(uc, h);
        assertNotNull(ret);
        UsageType created = ret;
        assertNotNull(created);
        return created;
    }

    private void testCreateAllHierchyLevelsl(Long facilityID) {
        List<HierarchyType> list = new ArrayList<HierarchyType>();
        list.add(new HierarchyType("FACILITY", "WING", UsageCategory.HOU, facilityID));
        list.add(new HierarchyType("WING", "UNIT", UsageCategory.HOU, facilityID));
        list.add(new HierarchyType("UNIT", "ROOM", UsageCategory.HOU, facilityID));
        Map<String, HierarchyType> ret = service.createUpdateLocationHierarchies(uc, list);
    }

    public class FacilityInternalLocationMetaSet {
        /**
         * Internal location security level
         */
        public static final String SECURITY_LEVEL = "SECURITYLEVEL";

        /**
         * Internal location offender category
         */
        public static final String OFFENDER_CATEGORY = "OFFENDERCATEGORY";
        /**
         * Internal location offender gender
         */
        public static final String OFFENDER_GENDER = "OFFENDERGENDER";
        /**
         * Internal location age range
         */
        public static final String AGE_RANGE = "AGERANGE";
        /**
         * Internal location hierarchy level
         */
        public static final String HIERARCHY_LEVEL = "HIERARCHYLEVEL";
        /**
         * Internal location non association
         */
        public static final String INTERNAL_LOCATION_NONASSOCIATION = "INTERNALLOCATIONNONASSOCIATION";
        /**
         * Internal location physical properties
         */
        public static final String LOCATION_PROPERTY = "LOCATIONPROPERTY";
        /**
         * Usage category: HOU, ACT, STO. Not configurable
         */
        public static final String USAGE_CATEGORY = "USAGECATEGORY";
        /**
         * Housing usage sub type
         */
        public static final String HOUSING_USAGETYPE = "HOUSINGUSAGETYPE";
        /**
         * Activity usage sub type
         */
        public static final String ACTIVITY_USAGETYPE = "ACTIVITYUSAGETYPE";
        /**
         * Storage usage sub type
         */
        public static final String STORAGE_USAGETYPE = "STORAGEUSAGETYPE";
        /**
         * Deactivate reason
         */
        public static final String DEACTIVATEREASON = "DEACTIVATEREASON";
        /**
         * Storage usage allowed property type
         */
        public static final String PROPERTYITEMTYPE = "PROPERTYITEMTYPE";
        /**
         * Internal location category
         */
        public static final String Int_Loc_Category = "IntLocCategory";

        public static final String SENTENCE_STATUS = "SentenceStatus";
    }
}
