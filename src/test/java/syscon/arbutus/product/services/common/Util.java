package syscon.arbutus.product.services.common;

import java.util.Random;

public class Util {

    public static Long randLong(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return Long.valueOf((long) randomNum);
    }

}
