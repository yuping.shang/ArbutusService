package syscon.arbutus.product.services.common;

import java.util.Date;

import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;

public class LegalTest extends BaseIT {

    private static final String CASE_CATEGORY_IJ = "IJ";
    private static final String CASE_TYPE_ADULT = "ADULT";
    private LegalService service;

    public LegalTest() {
        lookupJNDILogin();
    }

    private void lookupJNDILogin() {
        service = (LegalService) super.JNDILookUp(this.getClass(), LegalService.class);
        uc = super.initUserContext();
    }

    public CaseInfoType createCase(Long supervisionId) {
        CaseInfoType caseInfo = generateDefaultCaseInfo(supervisionId);
        CaseInfoType ret = service.createCaseInfo(uc, caseInfo, false);
        return ret;
    }

    private CaseInfoType generateDefaultCaseInfo(Long supervisionId) {
        CaseInfoType caseInfo = new CaseInfoType();
        Date currentDate = new Date();

        //Mandatory fields:
        caseInfo.setCaseCategory(CASE_CATEGORY_IJ);
        caseInfo.setCaseType(CASE_TYPE_ADULT);
        caseInfo.setCreatedDate(currentDate);
        caseInfo.setDiverted(true);
        caseInfo.setDivertedSuccess(true);
        caseInfo.setActive(true);
        caseInfo.setSupervisionId(supervisionId);

        return caseInfo;
    }
}
