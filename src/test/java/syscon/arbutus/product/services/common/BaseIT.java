package syscon.arbutus.product.services.common;

import org.apache.log4j.BasicConfigurator;
import org.jboss.ejb.client.ContextSelector;
import org.jboss.ejb.client.EJBClientConfiguration;
import org.jboss.ejb.client.EJBClientContext;
import org.jboss.ejb.client.PropertiesBasedEJBClientConfiguration;
import org.jboss.ejb.client.remoting.ConfigBasedEJBClientContextSelector;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Properties;

public class BaseIT {

    protected static final String PROPERTIES_FILE = "service_local_build.properties";
    private static final String MODULE_NAME = "ArbutusService-1.0-SNAPSHOT";
    private static final String REMOTE_SSL_ENABLED = "remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED";
    private static final String REMOTE_CONNECTIONS = "remote.connections";
    private static final String REMOTE_DEFAULT_PORT = "remote.connection.default.port";
    private static final String REMOTE_DEFAULT_HOST = "remote.connection.default.host";
    private static final String REMOTE_NOANONYMOUS = "remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS";
    //Security related
    private static final String REMOTE_USERNAME = "remote.connection.default.username";
    private static final String REMOTE_PASSWORD = "remote.connection.default.password";
    private static final String REMOTE_ENDPOINT = "endpoint.name";
    private static final String REMOTE_SASL_POLICY = "remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT";
    protected static String USER_NAME = "mbrown";
    protected static String PASSW = "mbrown";
    protected UserContext uc = new UserContext();
    private Logger log = LoggerFactory.getLogger(BaseIT.class);

    protected static void login() throws Exception {
        SecurityClient client = SecurityClientFactory.getSecurityClient();
        client.setSimple(USER_NAME, PASSW);
        client.login();
    }

    /**
     * Performs JNDI Lookup
     *
     * @param testClazz
     * @param serviceClazz
     */
    @SuppressWarnings("rawtypes")
    public Object JNDILookUp(Class testClazz, Class serviceClazz) {

        Object ctObject = null;

        try {

            BasicConfigurator.configure();
            //read host name and port number from .properties file.
            Properties locProps = getServiceProperties();

            Properties clientProps = new Properties();
            clientProps.put(REMOTE_DEFAULT_HOST, locProps.getProperty(REMOTE_DEFAULT_HOST));
            clientProps.put(REMOTE_DEFAULT_PORT, locProps.getProperty(REMOTE_DEFAULT_PORT));
            clientProps.put(REMOTE_SSL_ENABLED, locProps.getProperty(REMOTE_SSL_ENABLED));
            clientProps.put(REMOTE_CONNECTIONS, locProps.getProperty(REMOTE_CONNECTIONS));
            clientProps.put(REMOTE_NOANONYMOUS, locProps.getProperty(REMOTE_NOANONYMOUS));
            //Security related
            clientProps.put(REMOTE_USERNAME, locProps.getProperty(REMOTE_USERNAME));
            clientProps.put(REMOTE_PASSWORD, locProps.getProperty(REMOTE_PASSWORD));
            clientProps.put(REMOTE_ENDPOINT, locProps.getProperty(REMOTE_ENDPOINT));
            clientProps.put(REMOTE_SASL_POLICY, locProps.getProperty(REMOTE_SASL_POLICY));

            clientProps.put("remote.cluster.ejb.connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS", "false");
            clientProps.put("remote.cluster.ejb.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
            clientProps.put("remote.clusters", "ejb");
            clientProps.put("remote.cluster.ejb.username", "devtest");
            clientProps.put("remote.cluster.ejb.password", "devtest");

            USER_NAME = locProps.getProperty(REMOTE_USERNAME);
            PASSW = locProps.getProperty(REMOTE_PASSWORD);

            EJBClientConfiguration cc = new PropertiesBasedEJBClientConfiguration(clientProps);
            ContextSelector<EJBClientContext> selector = new ConfigBasedEJBClientContextSelector(cc);
            EJBClientContext.setSelector(selector);

            Properties jndiProps = new Properties();
            jndiProps.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

            Context context = new InitialContext(jndiProps);
            String jndiName = getJNDIServiceName(serviceClazz, getModuleName(testClazz));

            ctObject = context.lookup(jndiName);

        } catch (Exception e) {
            log.error(serviceClazz.getSimpleName() + " service not found, " + e.getMessage());
        }

        return ctObject;
    }

    /**
     * Performs JNDI Lookup with defined user and password
     *
     * @param testClazz
     * @param serviceClazz
     */
    @SuppressWarnings("rawtypes")
    public Object JNDILookUp(Class testClazz, Class serviceClazz, String username, String password) {

        Object ctObject = null;

        try {

            BasicConfigurator.configure();
            //read host name and port number from .properties file.
            Properties locProps = getServiceProperties();

            Properties clientProps = new Properties();
            clientProps.put(REMOTE_DEFAULT_HOST, locProps.getProperty(REMOTE_DEFAULT_HOST));
            clientProps.put(REMOTE_DEFAULT_PORT, locProps.getProperty(REMOTE_DEFAULT_PORT));
            clientProps.put(REMOTE_SSL_ENABLED, locProps.getProperty(REMOTE_SSL_ENABLED));
            clientProps.put(REMOTE_CONNECTIONS, locProps.getProperty(REMOTE_CONNECTIONS));
            clientProps.put(REMOTE_NOANONYMOUS, locProps.getProperty(REMOTE_NOANONYMOUS));
            //Security related
            clientProps.put(REMOTE_USERNAME, username);
            clientProps.put(REMOTE_PASSWORD, password);
            clientProps.put(REMOTE_ENDPOINT, locProps.getProperty(REMOTE_ENDPOINT));
            clientProps.put(REMOTE_SASL_POLICY, locProps.getProperty(REMOTE_SASL_POLICY));

            EJBClientConfiguration cc = new PropertiesBasedEJBClientConfiguration(clientProps);
            ContextSelector<EJBClientContext> selector = new ConfigBasedEJBClientContextSelector(cc);
            EJBClientContext.setSelector(selector);

            Properties jndiProps = new Properties();
            jndiProps.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

            Context context = new InitialContext(jndiProps);
            String jndiName = getJNDIServiceName(serviceClazz, getModuleName(testClazz));

            ctObject = context.lookup(jndiName);

        } catch (Exception e) {
            log.error(serviceClazz.getSimpleName() + " service not found, " + e.getMessage());
        }

        return ctObject;
    }

    /**
     * Get host address and port from configuration file
     *
     * @return host address and port
     */
    private Properties getServiceProperties() {

        Properties lbProps = null;
        try {
            lbProps = new Properties();
            final String propertiesPath = Paths.get("Services").toAbsolutePath().toString() + File.separator + PROPERTIES_FILE;
            FileInputStream in = new FileInputStream(propertiesPath);
            lbProps.load(in);
            in.close();

            if (!lbProps.containsKey(REMOTE_DEFAULT_HOST) || !lbProps.containsKey(REMOTE_DEFAULT_PORT) || !lbProps.containsKey(REMOTE_CONNECTIONS)
                    || !lbProps.containsKey(REMOTE_SSL_ENABLED) || !lbProps.containsKey(REMOTE_NOANONYMOUS) || !lbProps.containsKey(REMOTE_USERNAME)
                    || !lbProps.containsKey(REMOTE_PASSWORD) || !lbProps.containsKey(REMOTE_ENDPOINT) || !lbProps.containsKey(REMOTE_SASL_POLICY)) {
                throw new Exception("Unable to retrieve JBoss EJB Client JNDI configurations");
            }

        } catch (Exception e) {
            log.error("Unable to retrieve JBoss EJB Client JNDI configurations");
            e.printStackTrace();
        }
        return lbProps;
    }

    /**
     * Initializes UserContext
     *
     * @return UserContext
     */
    public UserContext initUserContext() {
        UserContext uc = new UserContext();
        uc.setConsumingApplicationId("devtest");
        //	    uc.setFacilityId(1l);
        //	    uc.setDeviceId(121l);
        //	    uc.setProcess("orgadm");
        //	    uc.setTask("orgsrch");
        uc.setTimestamp(new Date());
        try {
            login();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return uc;
    }

    /**
     * Builds JNDI name
     *
     * @param serviceClazz
     * @param moduleName
     * @return String
     */
    @SuppressWarnings("rawtypes")
    private String getJNDIServiceName(Class serviceClazz, String moduleName) {

        String appName = "WorkflowJail";
        String distinctName = "";
        String beanName = serviceClazz.getSimpleName() + "Bean";
        String viewClassName = serviceClazz.getName();

        String jndiName = "ejb:" + appName + "/" + moduleName + "/" + distinctName + "/" + beanName + "!" + viewClassName;
        System.out.println("jndiName=" + jndiName);

        return jndiName;
    }

    /**
     * Gets ModuleName from annotated method.
     * If not found, using default: ArbutusService-1.0
     *
     * @param clazz
     * @return String, Module Name
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    private String getModuleName(Class clazz) {

        ModuleConfig module = (ModuleConfig) clazz.getAnnotation(ModuleConfig.class);
        if (module != null) {
            return module.name();
        } else {
            //log.info("Could not find ModuleConfig annotation in class "+ clazz.getName() +". This is mandatory for JNDI lookups.");
            //log.info("Using default package name: " + MODULE_NAME);
        }
        return MODULE_NAME;

    }
}