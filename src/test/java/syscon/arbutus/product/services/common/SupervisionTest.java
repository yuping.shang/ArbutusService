package syscon.arbutus.product.services.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import syscon.arbutus.product.services.supervision.contract.dto.SupervisionConfigVariablesType;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

/**
 * This is for creating the SupervisionService data for unit test only.
 * (Not use for IT test).
 *
 * @author byu
 * @since Aug 12, 2013
 */
public class SupervisionTest extends BaseIT {

    private SupervisionService service;

    private PersonIdentityTest piTest;

    public SupervisionTest() {
        lookupJNDILogin();
        piTest = new PersonIdentityTest();
    }

    private void lookupJNDILogin() {
        service = (SupervisionService) super.JNDILookUp(this.getClass(), SupervisionService.class);
        uc = super.initUserContext();
    }

    public SupervisionTest(SupervisionService service, PersonIdentityTest piTest) {
        this.service = service;
        this.piTest = piTest;
    }

    public String getSupervisionDisplayFormat() {

        SupervisionConfigVariablesType returnType = service.getConfigVars(uc);

        if (returnType != null) {
            return returnType.getSupervisionDisplayIDFormat();
        }

        return null;
    }

    public String getSupervisionDisplayNumber(String format) {

        if (format == null || format.trim().isEmpty()) {
            format = getSupervisionDisplayFormat();
        }
        return service.generateDisplayId(uc, format);

    }

    public Long createSupervision() {

        //Create Person Identity
        Long personIdentityId = piTest.createPersonIdentity();

        Long supervisionId = createSupervision(personIdentityId, 1L, true);
        return supervisionId;

    }

    public Long createSupervision(Long facilityId, boolean isActive) {

        //Create Person Identity
        Long personIdentityId = piTest.createPersonIdentity();

        Long supervisionId = createSupervision(personIdentityId, facilityId, isActive);
        return supervisionId;

    }

    public Long createSupervision(Long personIdentityId, Long facilityId, boolean isActive) {

        SupervisionType supervision = new SupervisionType();

        Date startDate = new Date();
        Date endDate = null;

        if (!isActive) {
			endDate = new Date();
		}

        supervision.setSupervisionDisplayID("ID" + personIdentityId);
        supervision.setSupervisionStartDate(startDate);
        supervision.setSupervisionEndDate(endDate);
        supervision.setPersonIdentityId(personIdentityId);
        supervision.setFacilityId(facilityId);

        SupervisionType ret = service.create(uc, supervision);
        assert (ret != null);

        return ret.getSupervisionIdentification();
    }

    public List<Long> createSupervsionByPersonIdentity(List<Long> personIdentityIds, Long facilityId) {

        //Map<personIdentityId, supervisionId>
        List<Long> ret = new ArrayList<Long>();

        for (Long personIdentityId : personIdentityIds) {
            Long supervisionId = createSupervision(personIdentityId, facilityId, true);
            ret.add(supervisionId);
        }

        return ret;
    }

    public List<Long> createSupervisionForAssignment(Long facilityId) {

        //Create 20 offenders
        List<Long> ret = new ArrayList<Long>();

        //Long toIdentifer = null;
        for (int i = 1; i <= 20; i++) {
            //toIdentifer = Long.valueOf(i);
            Long supervisionId = createSupervision(facilityId, true);
            ret.add(supervisionId);
        }

        return ret;

    }

    public Long deleteSupervision(Long supervisionId) {
        return service.delete(uc, supervisionId);
    }

    public void deleteAll() {
        service.deleteAll(uc);
    }

}
