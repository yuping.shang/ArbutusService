package syscon.arbutus.product.services.common;

import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;

public class MovementActivityTest extends BaseIT {

    private MovementService service;

    public MovementActivityTest() {
        lookupJNDILogin();
    }

    private void lookupJNDILogin() {
        service = (MovementService) super.JNDILookUp(this.getClass(), MovementService.class);
        uc = super.initUserContext();
    }

    public void deleteAll() {
        service.deleteAll(uc);
    }

}
