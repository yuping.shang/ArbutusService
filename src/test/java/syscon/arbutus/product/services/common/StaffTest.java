package syscon.arbutus.product.services.common;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import syscon.arbutus.product.services.person.contract.dto.StaffPosition;
import syscon.arbutus.product.services.person.contract.dto.StaffType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;

public class StaffTest extends BaseIT {

    private PersonService service;

    private PersonIdentityTest piTest;

    public StaffTest() {
        lookupJNDILogin();
        piTest = new PersonIdentityTest();
    }

    public StaffTest(PersonService service, PersonIdentityTest piTest) {
        this.service = service;
        this.piTest = piTest;
    }

    private void lookupJNDILogin() {
        service = (PersonService) super.JNDILookUp(this.getClass(), PersonService.class);
        uc = super.initUserContext();
    }

    public Long createStaff() {

        StaffType ret = null;

        Long personIdentityId = piTest.createPersonIdentity();
        StaffType staff = buildStaffType();
        staff.setPersonIdentityID(personIdentityId);
        //
        ret = service.getStaff(uc, service.createStaff(uc, staff));
        assert (ret != null);

        return ret.getStaffID();

    }

    public Long createStaff(Long personIdentityId) {

        StaffType ret = null;

        StaffType staff = buildStaffType();
        staff.setPersonIdentityID(personIdentityId);
        //
        ret = service.getStaff(uc, service.createStaff(uc, staff));
        assert (ret != null);

        return ret.getStaffID();

    }

    public void deleteAll() {

        service.deleteAllStaff(uc);

    }

    @SuppressWarnings("deprecation")
    private StaffType buildStaffType() {

        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CPT"));
        staffPositions.add(new StaffPosition("DEP"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 30);
        String category = new String("CON");
        String reason = new String("SICK");

        StaffType staff = new StaffType(null, null, category, staffPositions, reason, startDate, endDate, null);

        return staff;
    }
}
