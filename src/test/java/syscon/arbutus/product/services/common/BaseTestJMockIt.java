package syscon.arbutus.product.services.common;

import javax.ejb.SessionContext;
import java.lang.reflect.Proxy;

import mockit.Mock;
import mockit.MockUp;
import org.hibernate.Session;
import org.junit.AfterClass;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.ejb.ActivityServiceBean;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.appointment.contract.ejb.AppointmentServiceBean;
import syscon.arbutus.product.services.appointment.contract.interfaces.AppointmentService;
import syscon.arbutus.product.services.audit.contract.dto.AuditModule;
import syscon.arbutus.product.services.audit.realization.AuditServiceAdapter;
import syscon.arbutus.product.services.core.common.adapters.LocationServiceAdapter;
import syscon.arbutus.product.services.core.common.adapters.MovementServiceAdapter;
import syscon.arbutus.product.services.facility.contract.ejb.FacilityServiceBean;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilitycountactivity.contract.ejb.ConfigureFacilityCountHelper;
import syscon.arbutus.product.services.facilitycountactivity.contract.ejb.FacilityCountActivityDAO;
import syscon.arbutus.product.services.facilitycountactivity.contract.ejb.FacilityCountActivityServiceBean;
import syscon.arbutus.product.services.facilitycountactivity.contract.interfaces.FacilityCountActivityService;
import syscon.arbutus.product.services.facilityinternallocation.contract.ejb.FacilityInternalLocationDAO;
import syscon.arbutus.product.services.facilityinternallocation.contract.ejb.FacilityInternalLocationHelper;
import syscon.arbutus.product.services.facilityinternallocation.contract.ejb.FacilityInternalLocationServiceBean;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.housing.contract.ejb.HousingBedManagementActivityServiceAdapter;
import syscon.arbutus.product.services.housing.contract.ejb.HousingServiceBean;
import syscon.arbutus.product.services.housing.contract.ejb.MovementDetailsServiceAdapter;
import syscon.arbutus.product.services.housing.contract.interfaces.HousingService;
import syscon.arbutus.product.services.housingsuitability.contract.ejb.MovementActivityBeanTest;
import syscon.arbutus.product.services.housingsuitability.contract.ejb.PersonPersonBeanTest;
import syscon.arbutus.product.services.location.contract.ejb.LocationServiceBean;
import syscon.arbutus.product.services.location.contract.interfaces.LocationService;
import syscon.arbutus.product.services.movement.contract.ejb.MovementServiceBean;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;
import syscon.arbutus.product.services.organization.contract.ejb.OrganizationServiceBean;
import syscon.arbutus.product.services.organization.contract.interfaces.OrganizationService;
import syscon.arbutus.product.services.person.contract.ejb.PersonServiceBean;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.person.contract.interfaces.PersonServiceCommon;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.ReferenceDataHelper;
import syscon.arbutus.product.services.referencedata.contract.ejb.ReferenceDataServiceBean;
import syscon.arbutus.product.services.referencedata.contract.interfaces.ReferenceDataService;
import syscon.arbutus.product.services.supervision.contract.ejb.SupervisionServiceBean;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;
import syscon.arbutus.product.services.userpreference.contract.ejb.UserPreferenceServiceBean;
import syscon.arbutus.product.services.userpreference.contract.interfaces.UserPreferenceService;
import syscon.arbutus.product.services.visitation.contract.ejb.VisitationServiceBean;
import syscon.arbutus.product.services.visitation.contract.interfaces.VisitationService;

import static mockit.Deencapsulation.setField;

public class BaseTestJMockIt {
    protected SessionContext sessionContext;

    protected StaffTest staffTest;
    protected ActivityTest activityTest;
    protected FacilityTest facilityTest;
    protected SupervisionTest supervisionTest;
    protected FacilityInternalLocationTest facilityInternalLocationTest;
    protected PersonIdentityTest personIdentityTest;
    protected PersonTest personTest;
    protected HousingBedActivityTest housingBedActivityTest;
    protected PersonPersonBeanTest personPersonBeanTest;
    protected MovementActivityBeanTest movementActivityBeanTest;

    protected final HousingService housingService = ServiceProxy.newInstance(new HousingServiceBean());
    protected final PersonServiceCommon personService = ServiceProxy.newInstance(new PersonServiceBean());
    protected final FacilityInternalLocationService facilityInternalLocationService = ServiceProxy.newInstance(new FacilityInternalLocationServiceBean());
    protected final SupervisionService supervisionService = ServiceProxy.newInstance(new SupervisionServiceBean());
    protected final FacilityService facilityService = ServiceProxy.newInstance(new FacilityServiceBean());
    protected final ActivityService activityService = ServiceProxy.newInstance(new ActivityServiceBean());
    protected final ReferenceDataService referenceDataService = ServiceProxy.newInstance(new ReferenceDataServiceBean(), Boolean.FALSE);
    protected final LocationService locationService = ServiceProxy.newInstance(new LocationServiceBean(), Boolean.FALSE);
    protected final MovementService movementService = ServiceProxy.newInstance(new MovementServiceBean());
    protected final AppointmentService appointmentService = ServiceProxy.newInstance(new AppointmentServiceBean());
    protected final UserPreferenceService userPreferenceService = ServiceProxy.newInstance(new UserPreferenceServiceBean());
    protected final OrganizationService organizationService = ServiceProxy.newInstance(new OrganizationServiceBean());
    protected final FacilityCountActivityService facilityCountActivityService = ServiceProxy.newInstance(new FacilityCountActivityServiceBean());
    protected final VisitationService visitationService = ServiceProxy.newInstance(new VisitationServiceBean());

    protected UserContext userContext;

    @BeforeClass
    public void beforeClass() {

        //        LogManager.getRootLogger().setLevel(Level.INFO);
        //        LogManager.getRootLogger().setAdditivity(Boolean.FALSE);
        //        LogManager.getLogger("org.h2.jdbc.JdbcSQLException").setLevel(Level.DEBUG);
        //        LogManager.getLogger("org.hibernate").setLevel(Level.DEBUG);
        //        LogManager.getLogger("syscon.arbutus").setLevel(Level.DEBUG);

        userContext = new UserContext();

        new MockUp<AuditServiceAdapter>() {
            @Mock
            public boolean isAuditOn(AuditModule auditModule) {
                return false;
            }
        };

        new MockUp<MovementDetailsServiceAdapter>() {
            @Mock
            private MovementService getService() {
                return movementService;
            }
        };

        new MockUp<MovementServiceAdapter>() {
            @Mock
            private MovementService getService() {
                return movementService;
            }
        };

        new MockUp<ReferenceDataHelper>() {
            @Mock
            public ReferenceDataService getRefDataService() {
                return referenceDataService;
            }
        };

        new MockUp<LocationServiceAdapter>() {
            @Mock
            private LocationService getService() {
                return locationService;
            }
        };

        //        new MockUp<SupervisionHandler>() {
        //            @Mock
        //            private void updateSearchIndex(Class<SupervisionEntity> className, Long objectId) {
        //
        //            }
        //        };

        new MockUp<FacilityInternalLocationHelper>() {
            @Mock
            public String getUserId(UserContext uc, SessionContext context) {
                return "devtest";
            }

            @Mock
            public String getInvocationContext(UserContext uc, SessionContext context) {
                return "";
            }
            //
            //            @Mock
            //            public Set<String> getUserPrivileges(UserContext uc, SessionContext context) {
            //                return new HashSet<>();
            //            }
        };

        new MockUp<BeanHelper>() {
            @Mock
            private ReferenceDataService getRefDataService() {
                return referenceDataService;
            }
        };

        new MockUp<HousingBedManagementActivityServiceAdapter>() {
            @Mock
            private HousingService getService() {
                return housingService;
            }
        };

        this.personTest = new PersonTest((PersonService) personService);
        this.personIdentityTest = new PersonIdentityTest((PersonService) personService, personTest);
        this.facilityInternalLocationTest = new FacilityInternalLocationTest(facilityInternalLocationService);
        this.supervisionTest = new SupervisionTest(supervisionService, personIdentityTest);
        this.facilityTest = new FacilityTest(facilityService);
        this.activityTest = new ActivityTest(activityService);
        this.staffTest = new StaffTest((PersonService) personService, personIdentityTest);
        this.housingBedActivityTest = new HousingBedActivityTest(housingService);
        this.personPersonBeanTest = new PersonPersonBeanTest((PersonService) personService, personTest);
        this.movementActivityBeanTest = new MovementActivityBeanTest(movementService);

        final KieServices ks = KieServices.Factory.get();
        final KieContainer kContainer = ks.getKieClasspathContainer();

        setServiceField(housingService, "ksession", kContainer.newKieSession("housingKS"));
        setServiceField(appointmentService, "facilityInternalLocationService", facilityInternalLocationService);

        final FacilityInternalLocationDAO facilityInternalLocationDAO = (FacilityInternalLocationDAO) new ServiceProxyFactory(FacilityInternalLocationDAO.class).create(
                new Class<?>[] { SessionContext.class, Session.class }, new Object[] { null, null });

        final FacilityCountActivityDAO facilityCountActivityDAO = new FacilityCountActivityDAO(activityService, facilityInternalLocationService);

        final ConfigureFacilityCountHelper configureFacilityCountHelper = new ConfigureFacilityCountHelper(activityService, facilityService, facilityCountActivityDAO);

        setServiceField(facilityInternalLocationService, "facilityInternalLocationDAO", facilityInternalLocationDAO);

        setServiceField(facilityCountActivityService, "activityService", activityService);
        setServiceField(facilityCountActivityService, "facilityService", facilityService);
        setServiceField(facilityCountActivityService, "facilityInternalLocationService", facilityInternalLocationService);
        setServiceField(facilityCountActivityService, "personService", personService);
        setServiceField(facilityCountActivityService, "supervisionService", supervisionService);
        setServiceField(facilityCountActivityService, "referenceDataService", referenceDataService);
        setServiceField(facilityCountActivityService, "locationService", locationService);
        setServiceField(facilityCountActivityService, "handler", facilityCountActivityDAO);
        setServiceField(facilityCountActivityService, "configureFacilityCountHelper", configureFacilityCountHelper);

        setServiceField(personService, "visitationService", visitationService);
    }

    private void setServiceField(final Object service, final String filedName, final Object obj) {
        setField(((ServiceProxy) Proxy.getInvocationHandler(service)).getUnwrappedObject(), filedName, obj);
    }

    @AfterClass
    public void afterClass() {

    }

    @BeforeMethod
    public void BeforeMethod() {
    }

    @AfterMethod
    public void AfterMethod() {
        HibernateUtil.close();
    }
}