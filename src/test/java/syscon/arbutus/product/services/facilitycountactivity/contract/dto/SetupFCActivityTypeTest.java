//package syscon.arbutus.product.services.facilitycountactivity.contract.dto;
//
//import java.util.Date;
//import java.util.HashSet;
//import java.util.Set;
//
//import org.testng.annotations.Test;
//
//import syscon.arbutus.product.services.core.contract.dto.AssociationType;
//import syscon.arbutus.product.services.core.contract.dto.CodeType;
//import syscon.arbutus.product.services.realization.util.BeanHelper;
//
//public class SetupFCActivityTypeTest {
//   @Test
//	public void isWellFormed() {
//		SetupFCActivityType data = new SetupFCActivityType();
//		assert (!data.isWellFormed());
//		data = new SetupFCActivityType(null, null, null, null, null, null, null, null);
//		assert (!data.isWellFormed());
//
//		CodeType code = new CodeType(ReferenceSet.COUNT_TYPE.value(), "V");
//		// internal location set is null
//		Set<AssociationType> references = new HashSet<AssociationType>();
//		AssociationType facilityRef = new AssociationType(
//				AssociationToClass.FACILITY.value(), 1L);
//		
//		AssociationType activityRef = new AssociationType(
//				AssociationToClass.ACTIVITY.value(), 1L);
//		references.add(activityRef);
//		Set<String> privileges = new HashSet<String>();
//		privileges = new HashSet<String>();
//		privileges.add("HP");
//		Date countDate = BeanHelper.createDate();
//		data = new SetupFCActivityType(null, code, countDate, references,
//				privileges, null, null, facilityRef);
//		assert (!data.isWellFormed());
//
//		// null object in internal location set
//		Set<AssociationType> internalLocations = new HashSet<AssociationType>();
//		internalLocations.add(null);
//		data = new SetupFCActivityType(null, code, countDate, references,
//				privileges, internalLocations, null, facilityRef);
//		assert (!data.isWellFormed());
//
//		internalLocations = new HashSet<AssociationType>();
//		internalLocations.add(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(),1L));
//		internalLocations.add(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(),2L));
//		data = new SetupFCActivityType(null, code, countDate, references,
//				privileges, internalLocations, null, facilityRef);
//		assert (data.isWellFormed());
//
//	}
//
//	@Test
//	public void isValid() {
//		SetupFCActivityType data = new SetupFCActivityType();
//		assert (!data.isValid());
//
//		data = new SetupFCActivityType(null, null, null, null, null, null, null, null);
//		assert (!data.isValid());
//
//		CodeType code = new CodeType(ReferenceSet.COUNT_TYPE.value(), "V");
//		// internal location set is null
//		Set<AssociationType> references = new HashSet<AssociationType>();
//		AssociationType facilityRef = new AssociationType(
//				AssociationToClass.FACILITY.value(), 1L);
//		
//		AssociationType activityRef = new AssociationType(
//				AssociationToClass.ACTIVITY.value(), 1L);
//		references.add(activityRef);
//		Set<String> privileges = new HashSet<String>();
//		privileges = new HashSet<String>();
//		privileges.add("HP");
//		Date countDate = BeanHelper.createDate();
//		data = new SetupFCActivityType(null, code, countDate, references,
//				privileges, null, null, facilityRef);
//		assert (!data.isValid());
//
//		// null object in internal location set
//		Set<AssociationType> internalLocations = new HashSet<AssociationType>();
//		internalLocations.add(null);
//		data = new SetupFCActivityType(null, code, countDate, references,
//				privileges, internalLocations, null, facilityRef);
//		assert (!data.isValid());
//
//		internalLocations = new HashSet<AssociationType>();
//		internalLocations.add(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(),1L));
//		internalLocations.add(new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(),2L));
//		data = new SetupFCActivityType(null, code, countDate, references,
//				privileges, internalLocations, null, facilityRef);
//		assert (data.isValid());
//
//	}
//}
