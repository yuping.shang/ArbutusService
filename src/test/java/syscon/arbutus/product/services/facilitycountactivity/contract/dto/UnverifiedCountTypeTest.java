//package syscon.arbutus.product.services.facilitycountactivity.contract.dto;
//
//import java.util.Date;
//
//import org.testng.annotations.Test;
//
//import syscon.arbutus.product.services.core.contract.dto.AssociationType;
//import syscon.arbutus.product.services.core.contract.dto.CodeType;
//import syscon.arbutus.product.services.realization.util.BeanHelper;
//
//public class UnverifiedCountTypeTest {
//
//	@Test
//	public void isWellFormed() {
//
//		UnverifiedCountType data = new UnverifiedCountType();
//		assert (!data.isWellFormed());
//		
//		AssociationType type= new AssociationType();
//		type.setToClass(AssociationToClass.FACILITY_INTERNAL_LOCATION.value());
//		
//		
//
//		// all properties are null
//		data = new UnverifiedCountType(null, null, null, null, null, null,
//				null, null);
//		assert (!data.isWellFormed());
//
//		// countType is null
//		data = new UnverifiedCountType(1L, null, null, null, null, null, null,
//				null);
//		assert (!data.isWellFormed());
//
//		CodeType countType = new CodeType(ReferenceSet.COUNT_TYPE.value(), "V");
//
//		// countOutcome is null
//		data = new UnverifiedCountType(1L, countType, null, null, null, null,
//				null, null);
//		assert (!data.isWellFormed());
//
//		CodeType countOutCome = new CodeType(
//				ReferenceSet.COUNT_OUTCOME.value(), "V");
//
//		// internalLocation is null
//		data = new UnverifiedCountType(1L, countType, countOutCome, null, null,
//				null, null, null);
//		assert (!data.isWellFormed());
//
//		// submittedInCount is null
//		type.setToIdentifier(1L);
//		data = new UnverifiedCountType(1L, countType, countOutCome, type, null,
//				null, null, null);
//		assert (!data.isWellFormed());
//
//		// verifiedFlag is null
//		data = new UnverifiedCountType(1L, countType, countOutCome, type, 2L,
//				null, null, null);
//		assert (!data.isWellFormed());
//
//		// countStartDate is null
//		data = new UnverifiedCountType(1L, countType, countOutCome, type, 2L,
//				null, true, null);
//		assert (!data.isWellFormed());
//
//		// countStartDate is null
//		Date countStartDate = BeanHelper.createDate();
//		data = new UnverifiedCountType(1L, countType, countOutCome, type, 2L,
//				null, true, countStartDate);
//		assert (data.isWellFormed());
//
//		data = new UnverifiedCountType(1L, countType, countOutCome, type, -2L,
//				null, true, countStartDate);
//		assert (data.isWellFormed());
//	}
//
//	@Test
//	public void isValid() {
//
//		UnverifiedCountType data = new UnverifiedCountType();
//		assert (!data.isValid());
//		AssociationType type= new AssociationType();
//		type.setToClass(AssociationToClass.FACILITY_INTERNAL_LOCATION.value());
//
//		// all properties are null
//		data = new UnverifiedCountType(null, null, null, null, null, null,
//				null, null);
//		assert (!data.isValid());
//
//		// countType is null
//		data = new UnverifiedCountType(1L, null, null, null, null, null, null,
//				null);
//		assert (!data.isValid());     
//
//		CodeType countType = new CodeType(ReferenceSet.COUNT_TYPE.value(), "V");
//
//		// countOutcome is null
//		data = new UnverifiedCountType(1L, countType, null, null, null, null,
//				null, null);
//		assert (!data.isValid());
//
//		CodeType countOutCome = new CodeType(
//				ReferenceSet.COUNT_OUTCOME.value(), "V");
//
//		// internalLocation is null
//		data = new UnverifiedCountType(1L, countType, countOutCome, null, null,
//				null, null, null);
//		assert (!data.isValid());
//
//		// submittedInCount is null
//		type.setToIdentifier(1L);
//		data = new UnverifiedCountType(1L, countType, countOutCome, type, null,
//				null, null, null);
//		assert (!data.isValid());
//
//		// verifiedFlag is null
//		data = new UnverifiedCountType(1L, countType, countOutCome, type, 2L,
//				null, null, null);
//		assert (!data.isValid());
//
//		// countStartDate is null
//		data = new UnverifiedCountType(1L, countType, countOutCome, type, 2L,
//				null, true, null);
//		assert (!data.isValid());
//
//		// countStartDate is null
//		Date countStartDate = BeanHelper.createDate();
//		data = new UnverifiedCountType(1L, countType, countOutCome, type, 2L,
//				null, true, countStartDate);
//		assert (data.isValid());
//
//		// Negative submitted in-count
//		data = new UnverifiedCountType(1L, countType, countOutCome, type, -2L,
//				null, true, countStartDate);
//		assert (!data.isValid());
//	}
//}
