package syscon.arbutus.product.services.facilitycountactivity.contract.ejb;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseTestJMockIt;
import syscon.arbutus.product.services.facilitycountactivity.contract.dto.FCActivity;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.UsageCategory;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionPersonIdentityType;

/**
 * Created by os on 9/30/15.
 */
public class LightweightIT extends BaseTestJMockIt {

    @BeforeClass
    public void beforeClass() {
        super.beforeClass();
    }

    @Test
    public void getHistoricalCountSupervisionPersonIdentityTypes() {
        List<Long> supervisionIds = new ArrayList<>();
        supervisionIds.add(20L);
        List<SupervisionPersonIdentityType> ret = supervisionService.getSupervisionPersonIdentityType(userContext, supervisionIds);
        System.out.println(ret);
//        List<SupervisionPersonIdentityType> ret = facilityCountActivityService.getHistoricalCountSupervisionPersonIdentityTypes(userContext, 114L,46L);
//        System.out.println(ret);
        UsageCategory usageCategory = facilityInternalLocationService.getUsageCategory(userContext, 50l);
        System.out.println(usageCategory);
        usageCategory = facilityInternalLocationService.getUsageCategory(userContext,46l);
        System.out.println(usageCategory);
    }
}
