//package syscon.arbutus.product.services.facilitycountactivity.contract.dto;
//
//import org.testng.annotations.Test;
//
//import syscon.arbutus.product.services.core.contract.dto.AssociationType;
//
//public class SubmittedLocationCountTypeTest {
//
//	@Test
//	public void isWellFormed() {
//
//		SubmittedLocationCountType data = new SubmittedLocationCountType();
//	 AssociationType type = new AssociationType();
//	 type.setToClass(AssociationToClass.FACILITY_INTERNAL_LOCATION.value());
//	 type.setToIdentifier(1L);
//		assert (!data.isWellFormed());
//
//		// All properties are null
//		data = new SubmittedLocationCountType(null, null, null, null, null,
//				null, null, null, null);
//		assert (!data.isWellFormed());
//
//		// submittedInCount is null
//		data = new SubmittedLocationCountType(type, null, null, null, null, null,
//				null, null, null);
//		assert (!data.isWellFormed());
//
//		// submissionStaff is null
//		data = new SubmittedLocationCountType(type, 5L, null, null, null, null,
//				null, null, null);
//		assert (!data.isWellFormed());
//
//		// conductingStaff is null
//		data = new SubmittedLocationCountType(type, 5L, null, null, null, 1L,
//				null, null, null);
//		assert (!data.isWellFormed());
//
//		data = new SubmittedLocationCountType(type, 5L, null, null, null, 1L, 2L,
//				null, null);
//		assert (data.isWellFormed());
//
//		// submittedInCount is negative
//		data = new SubmittedLocationCountType(type, -5L, null, null, null, 1L,
//				2L, null, null);
//		assert (data.isWellFormed());
//
//	}
//
//	@Test
//	public void isValid() {
//
//		SubmittedLocationCountType data = new SubmittedLocationCountType();
//		AssociationType type = new AssociationType();
//		 type.setToClass(AssociationToClass.FACILITY_INTERNAL_LOCATION.value());
//		
//		assert (!data.isValid());
//
//		// All properties are null
//		data = new SubmittedLocationCountType(null, null, null, null, null,
//				null, null, null, null);
//		assert (!data.isValid());
//
//		// submittedInCount is null
//		 type.setToIdentifier(1L);
//		data = new SubmittedLocationCountType(type, null, null, null, null, null,
//				null, null, null);
//		assert (!data.isValid());
//
//		// submissionStaff is null
//		data = new SubmittedLocationCountType(type, 5L, null, null, null, null,
//				null, null, null);
//		assert (!data.isValid());
//
//		// conductingStaff is null
//		data = new SubmittedLocationCountType(type, 5L, null, null, null, 1L,
//				null, null, null);
//		assert (!data.isValid());
//
//		data = new SubmittedLocationCountType(type, 5L, null, null, null, 1L, 2L,
//				null, null);
//		assert (data.isValid());
//
//		// submittedInCount is negative
//		data = new SubmittedLocationCountType(type, -5L, null, null, null, 1L,
//				2L, null, null);
//		assert (!data.isValid());
//
//	}
//}
