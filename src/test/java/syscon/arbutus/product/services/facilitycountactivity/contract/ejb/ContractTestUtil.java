package syscon.arbutus.product.services.facilitycountactivity.contract.ejb;

import java.util.Date;

import syscon.arbutus.product.security.UserContext;

//TODO May be need to change in accordance to FacilityCount Activity
public class ContractTestUtil {
    public static UserContext initializeUserContext() {
        UserContext uc = new UserContext();
        uc.setConsumingApplicationId("clientAppId");
        //		uc.setFacilityId(10l);
        //		uc.setDeviceId(102l);
        //		uc.setProcess("FacilityManagement");
        //		uc.setTask("CreateFacility");
        uc.setTimestamp(new Date());
        return uc;
    }

}
