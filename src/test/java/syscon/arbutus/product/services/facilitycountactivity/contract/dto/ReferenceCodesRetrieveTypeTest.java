package syscon.arbutus.product.services.facilitycountactivity.contract.dto;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.contract.dto.ReferenceCodesRetrieveType;

public class ReferenceCodesRetrieveTypeTest {

    @Test
    public void isValid() {
        ReferenceCodesRetrieveType data = new ReferenceCodesRetrieveType("CountStatus", null, "cn", null);
        assert (!data.isValid());

        data = new ReferenceCodesRetrieveType("CountStatus_", null, "cn", null);
        assert (!data.isValid());

        data = new ReferenceCodesRetrieveType("01234567890123456789012345678901234567890123456789012345678901234", null, "en", null);
        assert (!data.isValid());

        data = new ReferenceCodesRetrieveType("CountStatus", null, "01234567890123456789012345678901234567890123456789012345678901234", null);
        assert (!data.isValid());

        data = new ReferenceCodesRetrieveType("CountStatus", null, "en", true);
        assert (data.isValid());

        data = new ReferenceCodesRetrieveType("CountStatus", null, "en", false);
        assert (data.isValid());

        data = new ReferenceCodesRetrieveType(" NEW-SET", "NEW-CODE", "en", false);
        assert (!data.isValid());

        data = new ReferenceCodesRetrieveType("NEW-SET", " NEW-CODE", "en", false);
        assert (!data.isValid());

        data = new ReferenceCodesRetrieveType("NEW-SET", "", "en", false);
        assert (!data.isValid());
    }

    @Test
    public void isWellFormed() {
        ReferenceCodesRetrieveType data = new ReferenceCodesRetrieveType();
        assert (!data.isWellFormed());

        data = new ReferenceCodesRetrieveType(null, null, null, null);
        assert (!data.isWellFormed());

        data = new ReferenceCodesRetrieveType("", null, null, null);
        assert (!data.isWellFormed());

        data = new ReferenceCodesRetrieveType("TypeOfCount", null, null, null);
        assert (!data.isWellFormed());

        data = new ReferenceCodesRetrieveType("TypeOfCount", "", null, null);
        assert (!data.isWellFormed());

        data = new ReferenceCodesRetrieveType("TypeOfCount", null, "en", null);
        assert (data.isWellFormed());

    }
}
