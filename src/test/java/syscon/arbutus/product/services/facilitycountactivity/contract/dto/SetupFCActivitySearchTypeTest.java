//package syscon.arbutus.product.services.facilitycountactivity.contract.dto;
//
//import java.util.Date;
//import java.util.HashSet;
//import java.util.Set;
//
//import org.testng.annotations.Test;
//
//import syscon.arbutus.product.services.core.contract.dto.AssociationType;
//import syscon.arbutus.product.services.core.contract.dto.CodeType;
//import syscon.arbutus.product.services.realization.util.BeanHelper;
//
//public class SetupFCActivitySearchTypeTest {
//
//	@Test
//	public void isWellFormed() {
//		SetupFCActivitySearchType data = new SetupFCActivitySearchType();
//		assert (!data.isWellFormed());
//
//		data = new SetupFCActivitySearchType(null, null, null, null, null,
//				null, null);
//		assert (!data.isWellFormed());
//		// Test Blank COUNT_TYPE
//		CodeType code = new CodeType();
//		data = new SetupFCActivitySearchType(code, null, null, null, null,
//				null, null);
//		assert (!data.isWellFormed());
//
//		// Test Valid codeType
//		code = new CodeType(ReferenceSet.COUNT_TYPE.value(), "V");
//		data = new SetupFCActivitySearchType(code, null, null, null, null,
//				null, null);
//		assert (data.isWellFormed());
//
//		// Test null internal location
//		Set<AssociationType> internalLocations = new HashSet<AssociationType>();
//		internalLocations.add(null);
//		data = new SetupFCActivitySearchType(null, internalLocations, null,
//				null, null, null, null);
//		assert (!data.isWellFormed());
//
//		// Test non null internal location
//		internalLocations = new HashSet<AssociationType>();
//		AssociationType type = new AssociationType();
//		type.setToClass(AssociationToClass.FACILITY_INTERNAL_LOCATION.value());
//		type.setToIdentifier(1L);
//		internalLocations.add(type);
//		data = new SetupFCActivitySearchType(null, internalLocations, null,
//				null, null, null, null);
//		assert (data.isWellFormed());
//	}
//
//	@Test
//	public void isValid() {
//		SetupFCActivitySearchType data = new SetupFCActivitySearchType();
//		assert (!data.isValid());
//
//		data = new SetupFCActivitySearchType(null, null, null, null, null,
//				null, null);
//		assert (!data.isValid());
//		// Test Blank COUNT_TYPE
//		CodeType code = new CodeType();
//		data = new SetupFCActivitySearchType(code, null, null, null, null,
//				null, null);
//		assert (!data.isValid());
//
//		// Test Valid codeType
//		code = new CodeType(ReferenceSet.COUNT_TYPE.value(), "V");
//		data = new SetupFCActivitySearchType(code, null, null, null, null,
//				null, null);
//		assert (data.isValid());
//
//		// Test null internal location
//		Set<AssociationType> internalLocations = new HashSet<AssociationType>();
//		internalLocations.add(null);
//		data = new SetupFCActivitySearchType(null, internalLocations, null,
//				null, null, null, null);
//		assert (!data.isValid());
//
//		// Test non null internal location
//		internalLocations = new HashSet<AssociationType>();
//		AssociationType type = new AssociationType();
//		type.setToClass(AssociationToClass.FACILITY_INTERNAL_LOCATION.value());
//		type.setToIdentifier(1L);
//		internalLocations.add(type);
//		data = new SetupFCActivitySearchType(null, internalLocations, null,
//				null, null, null, null);
//		assert (data.isValid());
//
//		AssociationType facType = new AssociationType();
//		facType.setToClass(AssociationToClass.FACILITY.value());
//		facType.setToIdentifier(1L);
//		Date presentDate = BeanHelper.createDate();
//		Date pastDate = BeanHelper.getPastDate(presentDate, 10);
//		// Passing presentDate in all the date fields should be success
//		data = new SetupFCActivitySearchType(null, internalLocations, facType,
//				null, null, presentDate, presentDate);
//		assert (data.isValid());
//
//		// Passing pastDate in all the date fields should be success
//		data = new SetupFCActivitySearchType(null, internalLocations, facType,
//				null, null, pastDate, pastDate);
//		assert (data.isValid());
//
//		// Passing presentDate in from date and pastDate in to dates in all the
//		// date fields should be failed
//		data = new SetupFCActivitySearchType(null, internalLocations, facType,
//				null, null, presentDate, pastDate);
//		assert (!data.isValid());
//	}
//}
