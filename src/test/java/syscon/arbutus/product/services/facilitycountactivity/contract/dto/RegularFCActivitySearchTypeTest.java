//package syscon.arbutus.product.services.facilitycountactivity.contract.dto;
//
//import java.util.Date;
//import java.util.HashSet;
//import java.util.Set;
//
//import org.testng.annotations.Test;
//
//import syscon.arbutus.product.services.core.contract.dto.AssociationType;
//import syscon.arbutus.product.services.core.contract.dto.CodeType;
//import syscon.arbutus.product.services.realization.util.BeanHelper;
//
//public class RegularFCActivitySearchTypeTest {
//
//	@Test
//	public void isWellFormed() {
//
//		RegularFCActivitySearchType data = new RegularFCActivitySearchType();
//		assert (!data.isWellFormed());
//
//		data = new RegularFCActivitySearchType(null, null, null, null, null,
//				null, null, null, null, null, null, null, null, null, null,
//				null, null, null, null, null, null);
//		assert (!data.isWellFormed());
//
//		// Test Blank COUNT_TYPE
//		CodeType code = new CodeType();
//		data = new RegularFCActivitySearchType(null, null, null, null, null,
//				null, null, code, null, null, null, null, null, null, null,
//				null, null, null, null, null, null);
//		assert (!data.isWellFormed());
//
//		// Test Valid codeType
//		code = new CodeType(ReferenceSet.COUNT_TYPE.value(), "V");
//		data = new RegularFCActivitySearchType(null, null, null, null, null,
//				null, null, code, null, null, null, null, null, null, null,
//				null, null, null, null, null, null);
//		assert (data.isWellFormed());
//
//		Set<AssociationType> internalLocations = new HashSet<AssociationType>();
//		internalLocations.add(null);
//		data = new RegularFCActivitySearchType(internalLocations, null, null,
//				null, null, null, null, null, null, null, null, null, null,
//				null, null, null, null, null, null, null, null);
//		assert (data.isWellFormed());
//
//		internalLocations = new HashSet<AssociationType>();
//		AssociationType type= new AssociationType();
//		type.setToClass(AssociationToClass.FACILITY_INTERNAL_LOCATION.value());
//		type.setToIdentifier(1L);
//		internalLocations.add(type);
//		data = new RegularFCActivitySearchType(internalLocations, null, null,
//				null, null, null, null, null, null, null, null, null, null,
//				null, null, null, null, null, null, null, null);
//		assert (data.isWellFormed());
//
//		Date presentDate = BeanHelper.createDate();
//		@SuppressWarnings("deprecation")
//		Date pastDate = BeanHelper.getPastDate(presentDate, 10);
//		// Passing presentDate in all the date fields should be success
//		data = new RegularFCActivitySearchType(null, null, null, null, null,
//				null, null, code, null, null, null, null, null, null,
//				presentDate, presentDate, presentDate, presentDate, null, null,
//				null);
//		assert (data.isWellFormed());
//
//		// Passing pastDate in all the date fields should be success
//		data = new RegularFCActivitySearchType(null, null, null, null, null,
//				null, null, code, null, null, null, null, null, null, pastDate,
//				pastDate, pastDate, pastDate, null, null, null);
//		assert (data.isWellFormed());
//
//		// Passing presentdate in from dates and pastDate in to dates in all the
//		// date fields should be success
//		data = new RegularFCActivitySearchType(null, null, null, null, null,
//				null, null, code, null, null, null, null, null, null,
//				presentDate, pastDate, presentDate, pastDate, null, null, null);
//		assert (data.isWellFormed());
//
//		// Passing presentdate in to dates and pastDate in from dates in all the
//		// date fields should be success
//		data = new RegularFCActivitySearchType(null, null, null, null, null,
//				null, null, code, null, null, null, null, null, null, pastDate,
//				presentDate, pastDate, presentDate, null, null, null);
//		assert (data.isWellFormed());
//	}
//
//	@Test
//	public void isValid() {
//
//		RegularFCActivitySearchType data = new RegularFCActivitySearchType();
//		assert (!data.isValid());
//
//		data = new RegularFCActivitySearchType(null, null, null, null, null,
//				null, null, null, null, null, null, null, null, null, null,
//				null, null, null, null, null, null);
//		assert (!data.isValid());
//
//		// Test Blank COUNT_TYPE
//		CodeType code = new CodeType();
//		data = new RegularFCActivitySearchType(null, null, null, null, null,
//				null, null, code, null, null, null, null, null, null, null,
//				null, null, null, null, null, null);
//		assert (!data.isValid());
//
//		// Test Valid codeType
//		code = new CodeType(ReferenceSet.COUNT_TYPE.value(), "V");
//		data = new RegularFCActivitySearchType(null, null, null, null, null,
//				null, null, code, null, null, null, null, null, null, null,
//				null, null, null, null, null, null);
//		assert (data.isValid());
//
//		Set<AssociationType> internalLocations = new HashSet<AssociationType>();
//		internalLocations.add(null);
//		data = new RegularFCActivitySearchType(internalLocations, null, null,
//				null, null, null, null, null, null, null, null, null, null,
//				null, null, null, null, null, null, null, null);
//		assert (!data.isValid());
//
//		internalLocations = new HashSet<AssociationType>();
//		AssociationType type= new AssociationType();
//		type.setToClass(AssociationToClass.FACILITY_INTERNAL_LOCATION.value());
//		type.setToIdentifier(1L);
//		internalLocations.add(type);
//		data = new RegularFCActivitySearchType(internalLocations, null, null,
//				null, null, null, null, null, null, null, null, null, null,
//				null, null, null, null, null, null, null, null);
//		assert (data.isValid());
//
//		Date presentDate = BeanHelper.createDate();
//		Date pastDate = BeanHelper.getPastDate(presentDate, 10);
//		// Passing presentDate in all the date fields should be success
//		data = new RegularFCActivitySearchType(null, null, null, null, null,
//				null, null, code, null, null, null, null, null, null,
//				presentDate, presentDate, presentDate, presentDate, null, null,
//				null);
//		assert (data.isValid());
//
//		// Passing pastDate in all the date fields should be success
//		data = new RegularFCActivitySearchType(null, null, null, null, null,
//				null, null, code, null, null, null, null, null, null, pastDate,
//				pastDate, pastDate, pastDate, null, null, null);
//		assert (data.isValid());
//
//		// Passing presentdate in from dates and pastDate in to dates in all the
//		// date fields should be failed
//		data = new RegularFCActivitySearchType(null, null, null, null, null,
//				null, null, code, null, null, null, null, null, null,
//				presentDate, pastDate, presentDate, pastDate, null, null, null);
//		assert (!data.isValid());
//
//		// Passing presentdate in to dates and pastDate in from dates in all the
//		// date fields should be success
//		data = new RegularFCActivitySearchType(null, null, null, null, null,
//				null, null, code, null, null, null, null, null, null, pastDate,
//				presentDate, pastDate, presentDate, null, null, null);
//		assert (data.isValid());
//	}
//}
