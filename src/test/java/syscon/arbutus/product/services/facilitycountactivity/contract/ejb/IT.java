package syscon.arbutus.product.services.facilitycountactivity.contract.ejb;

import java.math.BigInteger;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.contract.dto.*;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilitycountactivity.contract.dto.*;
import syscon.arbutus.product.services.facilitycountactivity.contract.interfaces.FacilityCountActivityService;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.FacilityInternalLocation;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.HierarchyType;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.LocationAttributeType;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.UsageCategory;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.person.contract.dto.PersonType;
import syscon.arbutus.product.services.person.contract.dto.StaffPosition;
import syscon.arbutus.product.services.person.contract.dto.StaffType;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.person.contract.test.ContractTestUtil;
import syscon.arbutus.product.services.realization.util.BeanHelper;

import static org.testng.Assert.*;

public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);

    private static String COUNT_TYPE = new String("SCH");
    private static String COUNT_TYPE_USCH = new String("USCH");
    private static String COUNT_STATUS_READY = new String("Ready");
    private static String COUNT_STATUS_COMPLETED = new String("Completed");
    private static String COUNT_STATUS_IN_PROGRESS = new String("InProgress");

    private static String COUNT_OUTCOME_CLEARCOUNT = new String("CLEARCOUNT");
    private static String COUNT_OUTCOME_RECOUNT = new String("Recount");
    private static String RECOUNT_REASON = new String("STF");
    private static String CLEAR_COUNT_REASON_ESCAPE = new String("ACC");
    private static String RESUBMISSION_REASON = new String("MSC");


    private static String ACTIVITY_CATEGORY_FACILITY_COUNT = new String("FACOUNT");

    Long staffId;
    // shared data
    Object tip;
    Long unverifiedFacilityId;
    Long regularFacilityId;
    Long regularActivityId;
    Long unverifiedStaffId;
    Long facId;
    Long actId;
    Long filId;
    SearchReturnDTO<UnverifiedCount> unverifiedCountsReturn;
    Date presentDate = BeanHelper.createDate();

    private FacilityCountActivityService service;
    private FacilityService facService;
    private ActivityService actService;
    private FacilityInternalLocationService facInService;
    private PersonService personService;

    private FacilityInternalLocation root;
    private FacilityInternalLocation childL1;
    private FacilityInternalLocation childL2;

    @AfterClass
    public void afterClass(){
		//service.deleteAll(uc);
	}


    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        uc = super.initUserContext();
        service = (FacilityCountActivityService) JNDILookUp(this.getClass(), FacilityCountActivityService.class);

        /*
        facService = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        facInService = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        actService = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        personService = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        facId = createFacility(null);
        actId = createActivity();
        testCreateAllHierchyLevelsl(facId);
        filId = createFacilityInternalLocation();
        createFacilityInternalLocationChild1();
        createFacilityInternalLocationChild2();
        staffId = createStaff();
        */
    }

    @Test
    public void getHousingLocationCount1() {
        Long facilityId = 1l;
        InternalLocationCountResult ret = service.getHousingLocationCount(uc, facilityId, null);
        assert (ret != null);
    }

    @Test
    public void getHousingLocationCount2() {
        Long internalLocationId = 25l;
        InternalLocationCountResult ret = service.getHousingLocationCount(uc, null, internalLocationId);
        assert (ret != null);
    }


    @Test
    public void getActivityLocationCount1() {
        Long facilityId = 1l;
        InternalLocationCountResult ret = service.getActivityLocationCount(uc, facilityId, null);
        assert (ret != null);
    }

    @Test
    public void getActivityLocationCount2() {
        Long internalLocationId = 23l;
        InternalLocationCountResult ret = service.getActivityLocationCount(uc, null, internalLocationId);
        assert (ret != null);

    }

    //
//    @Test
//    public void testJNDILookup() {
//        assertNotNull(service);
//    }
//
//    private void createData() {
//        // staffId = 420L;
//
//        try {
//
//            SetupFCActivity ret = new SetupFCActivity();
//            // Create Scheduled Setup FCA
//            SetupFCActivity setupFCActivity = getSetupFCAInstance();
//            ret = service.create(uc, setupFCActivity);
//
//            assert (ret != null);
//            assert (ret != null && ret.getFacilityCountId() > 0);
//
//            Long setupFCAId = ret.getFacilityCountId();
//
//            // Create Scheduled Regular FCA
//            // long staffId = 420;
//            RegularFCActivity retCreate = service.create(uc, setupFCAId, staffId);
//            assert (retCreate != null);
//            assert (retCreate != null && retCreate.getFacilityCountId() > 0);
//
//            // Create Non scheduled Setup FCA
//            setupFCActivity.setCountType(COUNT_TYPE_USCH);
//            setupFCActivity.setActivityId(null);
//
//            ret = service.create(uc, setupFCActivity);
//
//            assert (ret != null);
//            assert (ret != null && ret.getFacilityCountId() > 0);
//
//            setupFCAId = ret.getFacilityCountId();
//
//            // Create Non Scheduled Regular FCA
//            retCreate = service.create(uc, setupFCAId, staffId);
//            assert (retCreate != null);
//            assert (retCreate != null && retCreate.getFacilityCountId() > 0);
//            // System.out.println("facility count id generated for Regular FCA is =======================>>>>"+this.facilityCountId_Regular);
//
//            regularFacilityId = setupFCActivity.getFacilityId();
//            regularActivityId = setupFCActivity.getActivityId();
//
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testGet() {
//
//        SetupFCActivity ret = null;
//
//        SetupFCActivity setUpFCA = getSetupFCAInstance();
//        setUpFCA.setFacilityId(facId);
//        ret = service.create(uc, setUpFCA);
//        assert (ret != null);
//
//        Long setupFCAId = ret.getFacilityCountId();
//
//        RegularFCActivity regRet = service.create(uc, setupFCAId, staffId);
//
//        Long regFCAId = regRet.getFacilityCountId();
//
//        assert (regFCAId > 0);
//
//        FCActivity retFCA = service.get(uc, regFCAId);
//
//        assert (retFCA != null);
//    }
//
//    @Test
//    public void testGetCount() {
//
//        SetupFCActivity ret = null;
//
//        SetupFCActivity setUpFCA = getSetupFCAInstance();
//
//        ret = service.create(uc, setUpFCA);
//        assert (ret != null);
//
//        Long setupFCAId = ret.getFacilityCountId();
//
//        assert (setupFCAId > 0);
//
//        Long retLng = service.getCount(uc);
//
//        assert (retLng >= 1);
//    }
//
//    @Test
//    public void testCreateSetupFCA() {
//
//        try {
//
//            SetupFCActivity ret = new SetupFCActivity();
//            // System.out.println("going to create a Setup FCA........====================================");
//            // Create Scheduled Setup FCA
//            SetupFCActivity setupFCActivity = getSetupFCAInstance();
//            ret = service.create(uc, setupFCActivity);
//
//            assert (ret != null);
//            assert (ret != null && ret.getFacilityCountId() > 0);
//
//            // Create Non scheduled Setup FCA
//            setupFCActivity.setCountType(COUNT_TYPE_USCH);
//            setupFCActivity.setActivityId(null);
//            setupFCActivity.setParentId(null);
//            ret = service.create(uc, setupFCActivity);
//
//            assert (ret != null);
//            assert (ret != null && ret.getFacilityCountId() > 0);
//
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testCreateRegularFCA() {
//
//        // staffId = 420L;
//        try {
//
//            SetupFCActivity ret = new SetupFCActivity();
//            // Create Scheduled Setup FCA
//            SetupFCActivity setupFCActivity = getSetupFCAInstance();
//            ret = service.create(uc, setupFCActivity);
//
//            assert (ret != null);
//            assert (ret != null && ret.getFacilityCountId() > 0);
//
//            Long setupFCAId = ret.getFacilityCountId();
//
//            // Create Scheduled Regular FCA
//            // long staffId = 420;
//            RegularFCActivity retCreate = service.create(uc, setupFCAId, staffId);
//            assert (retCreate != null);
//            assert (retCreate != null && retCreate.getFacilityCountId() > 0);
//
//            // Create Non scheduled Setup FCA
//            setupFCActivity.setCountType(COUNT_TYPE_USCH);
//            setupFCActivity.setActivityId(null);
//
//            ret = service.create(uc, setupFCActivity);
//
//            assert (ret != null);
//            assert (ret != null && ret.getFacilityCountId() > 0);
//
//            setupFCAId = ret.getFacilityCountId();
//            // Create Non Scheduled Regular FCA
//            retCreate = service.create(uc, setupFCAId, staffId);
//            assert (retCreate != null);
//            assert (retCreate != null && retCreate.getFacilityCountId() > 0);
//            // System.out.println("facility count id generated for Regular FCA is =======================>>>>"+this.facilityCountId_Regular);
//
//            regularFacilityId = setupFCActivity.getFacilityId();
//            regularActivityId = setupFCActivity.getActivityId();
//
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testUpdateSetupFCA() {
//
//        try {
//            // Create Scheduled Setup FCA
//            SetupFCActivity setupFCActivity = getSetupFCAInstance();
//            SetupFCActivity retCreate = service.create(uc, setupFCActivity);
//            assert (retCreate != null);
//            assert (retCreate != null && retCreate.getFacilityCountId() > 0);
//
//            // Update Scheduled Setup FCA
//            retCreate.getInternalLocationTypes().add(new InternalLocation(filId, false));
//            SetupFCActivity retUpdate = service.update(uc, retCreate);
//
//            assert (retUpdate != null);
//            assert (retUpdate != null && retUpdate.getFacilityCountId() > 0);
//
//            // Create Non scheduled Setup FCA
//            setupFCActivity.setCountType(COUNT_TYPE_USCH);
//            setupFCActivity.setActivityId(null);
//            setupFCActivity.setParentId(null);
//            retCreate = service.create(uc, setupFCActivity);
//
//            assert (retCreate != null);
//            assert (retCreate != null && retCreate.getFacilityCountId() > 0);
//
//            // Update Non Scheduled Setup FCA
//            retCreate.getInternalLocationTypes().add(new InternalLocation(filId, false));
//            retUpdate = service.update(uc, retCreate);
//            assert (retUpdate != null);
//            assert (retUpdate != null && retUpdate.getFacilityCountId() > 0);
//
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testUpdateRegularSetupFCA() {
//
//        try {
//            SetupFCActivity ret = new SetupFCActivity();
//            // Create Scheduled Setup FCA
//            SetupFCActivity setupFCActivity = getSetupFCAInstance();
//            ret = service.create(uc, setupFCActivity);
//
//            assert (ret != null);
//            assert (ret != null && ret.getFacilityCountId() > 0);
//
//            Long setupFCAId = ret.getFacilityCountId();
//
//            // Create Scheduled Regular FCA
//            // long staffId = 420;
//            RegularFCActivity retCreate = service.create(uc, setupFCAId, staffId);
//            assert (retCreate != null);
//            assert (retCreate.getFacilityCountId() != null && retCreate.getFacilityCountId() > 0);
//
//            RegularFCActivity regularFCActivity = retCreate;
//            CalculatedFacilityCount calfacCount = new CalculatedFacilityCount(100L, 5L, 95L);
//            ConsolidatedLocationCount conLocCount = new ConsolidatedLocationCount(filId, null, null, null, null, null, null, RESUBMISSION_REASON, null, null, null, false,
//                    null, null, null, null);
//
//            regularFCActivity.setCalculatedFacilityCount(calfacCount);
//            regularFCActivity.setConsolidatedLocationCounts(new HashSet<ConsolidatedLocationCount>());
//            regularFCActivity.getConsolidatedLocationCounts().add(conLocCount);
//            regularFCActivity.setRecountReason(null);
//            regularFCActivity.setClearCountReason(null);
//            regularFCActivity.setComments(null);
//            regularFCActivity.setCountOutcome(null);
//            regularFCActivity.setCountStatus(COUNT_STATUS_COMPLETED);
//            regularFCActivity.setConsolidationStaff(12345L);
//            regularFCActivity.setCountAttempt(5L);
//            regularFCActivity.setCountEndDate(null);
//
//             * //Update Scheduled Regular FCA RegularFCActivityType
//			 * regularFCActivity = retCreate;
//			 *
//			 * regularFCActivity.setCountOutcome(COUNT_OUTCOME_RECOUNT);
//			 * regularFCActivity.setRecountReason(RECOUNT_REASON);
//			 * regularFCActivity.setCountStatus(COUNT_STATUS_IN_PROGRESS);
//			 * regularFCActivity.setConsolidationStaff(420L);
//			 * regularFCActivity.setCountEndDate(new Date());
//
//
//
//            RegularFCActivity retupadte = service.update(uc, regularFCActivity);
//            assert (retupadte != null);
//            assert (retupadte.getFacilityCountId() != null && retupadte.getCalculatedFacilityCount() != null);
//
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testSearchSetupFCA() {
//
//        createData();
//
//        try {
//            SetupFCActivity setupFCA = getSetupFCAInstance();
//            SetupFCActivitySearch sFCASrchType = new SetupFCActivitySearch();
//
//            // Search for FACILITY ASSOCIATION
//            // System.out.println("============SEARCH DEPENDs ON FACILITY ASSO====================");
//            sFCASrchType.setFacilityId(regularFacilityId);
//            sFCASrchType.setActivityId(regularActivityId);
//            SearchReturnDTO<SetupFCActivity> sFCARts = service.search(uc, null, sFCASrchType, 0L, 100L, null);
//            assertNotNull(sFCARts);
//            assert (sFCARts.getTotalSize() > 0);
//
//            // Search on basis of FCA created Date
//            sFCASrchType = new SetupFCActivitySearch();
//            sFCASrchType.setCountDateFrom(setupFCA.getCountDate());
//            sFCARts = service.search(uc, null, sFCASrchType, 0L, 100L, null);
//            assertNotNull(sFCARts);
//
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//
//    }
//
//    @Test
//    public void testSearchRegularFCA() {
//        createData();
//
//        // Search on ACTIVITY REFERNECE
//        RegularFCActivitySearch rFCASrchType = new RegularFCActivitySearch();
//        rFCASrchType.setFacilityId(regularFacilityId);
//        rFCASrchType.setCountType(COUNT_TYPE);
//        rFCASrchType.setCountStartDateFrom(getDate(2013, 1, 1, 1, 1, 1));
//        rFCASrchType.setCountStartDateTo(new Date());
//        try {
//            SearchReturnDTO<RegularFCActivity> rFCARts = service.search(uc, null, rFCASrchType, 0L, 100L, null);
//            assertNotNull(rFCARts);
//            if (rFCARts.getResultSet() != null) {
//                for (RegularFCActivity type : rFCARts.getResultSet()) {
//                    assertNotNull(type);
//                    assertNotNull(type.getFacilityCountId());
//                }
//            }
//
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//    }
//
//    private Date getDate(int year, int month, int day, int hour, int minute, int second) {
//        Calendar c = Calendar.getInstance();
//        c.set(Calendar.YEAR, year);
//        c.set(Calendar.MONTH, month - 1);
//        c.set(Calendar.DAY_OF_MONTH, day);
//
//        c.set(Calendar.HOUR_OF_DAY, hour);
//        c.set(Calendar.MINUTE, minute);
//        c.set(Calendar.SECOND, second);
//        c.set(Calendar.MILLISECOND, 0);
//
//        return c.getTime();
//    }
//
//    public void testDelete() {
//
//        SetupFCActivity setupFCActivity = getSetupFCAInstance();
//        SetupFCActivity ret = service.create(uc, setupFCActivity);
//
//        // System.out.println("Return Code===>"+ret.getReturnCode());
//
//        Long fCAId = ret.getFacilityCountId();
//        // System.out.println("===============Facility count Id of created record is....=="+fCAId);
//        service.delete(uc, fCAId);
//
//        try {
//            FCActivity ret2 = service.get(uc, fCAId);
//            assertEquals(ret2, null);
//        } catch (Exception ex) {
//            assert (ex.getClass().equals(DataNotExistException.class));
//        }
//
//    }
//
//    @Test
//    public void testSubmitReportingLocCount() {
//
//        SetupFCActivity retSetup = new SetupFCActivity();
//        // Create Scheduled Setup FCA
//        SetupFCActivity setupFCActivity = getSetupFCAInstance();
//        retSetup = service.create(uc, setupFCActivity);
//
//        assert (retSetup != null);
//        assert (retSetup.getFacilityCountId() != null && retSetup.getFacilityCountId() > 0);
//
//        Long setupFCAId = retSetup.getFacilityCountId();
//
//        // Create Scheduled Regular FCA
//        // long staffId = 420;
//        RegularFCActivity retCreate = service.create(uc, setupFCAId, staffId);
//        assert (retCreate != null);
//        assert (retCreate.getFacilityCountId() != null && retCreate.getFacilityCountId() > 0);
//
//        long regFCACountId = retCreate.getFacilityCountId();
//
//        //Long conductingStaff = 420L;
//        //Long submissionStaff = 400L;
//
//        SubmittedLocationCount ret = new SubmittedLocationCount();
//        SubmittedLocationCount type = new SubmittedLocationCount();
//
//        for (InternalLocation locId : retSetup.getInternalLocationTypes()) {
//            type.setConductingStaff(staffId);
//            type.setInternalLocation(locId.getInternalLocation());
//            type.setResubmissionComments("ABC");
//            type.setReSubmissionDiscrepancy(100L);
//            type.setResubmissionReason(RESUBMISSION_REASON);
//            type.setResubmittedInCount(200L);
//            type.setSubmissionDiscrepancy(200L);
//            type.setSubmissionStaff(staffId);
//            type.setSubmittedInCount(230L);
//
//            try {
//                ret = service.submitHeadCount(uc, regFCACountId, type);
//                assertNotNull(ret);
//                assertNotNull(ret.getInternalLocation());
//
//            } catch (Exception re) {
//                re.printStackTrace();
//            }
//        }
//
//    }
//
//    @Test
//    public void testRecordCalculatedCount() {
//
//        SetupFCActivity retSetup = new SetupFCActivity();
//        // Create Scheduled Setup FCA
//        SetupFCActivity setupFCActivity = getSetupFCAInstance();
//        retSetup = service.create(uc, setupFCActivity);
//
//        assert (retSetup != null);
//        assert (retSetup.getFacilityCountId() != null && retSetup.getFacilityCountId() > 0);
//
//        Long setupFCAId = retSetup.getFacilityCountId();
//
//        // Create Scheduled Regular FCA
//        // long staffId = 420;
//        // long conductingStaff = 420;
//        // long submissionStaff = 410L;
//        long conductingStaff = staffId;
//        long submissionStaff = staffId;
//
//        RegularFCActivity retCreate = service.create(uc, setupFCAId, staffId);
//        assert (retCreate != null);
//        assert (retCreate.getFacilityCountId() != null && retCreate.getFacilityCountId() > 0);
//
//        long regFCACountId = retCreate.getFacilityCountId();
//        RegularFCActivity regFCAType = retCreate;
//
//        for (ConsolidatedLocationCount conCountType : regFCAType.getConsolidatedLocationCounts()) {
//            SubmittedLocationCount submittedLocationCount = new SubmittedLocationCount();
//            submittedLocationCount.setConductingStaff(conductingStaff);
//            submittedLocationCount.setInternalLocation(conCountType.getInternalLocation());
//
//            submittedLocationCount.setResubmissionComments(null);
//            submittedLocationCount.setResubmissionReason(null);
//            submittedLocationCount.setReSubmissionDiscrepancy(null);
//            submittedLocationCount.setResubmittedInCount(null);
//            submittedLocationCount.setSubmissionStaff(submissionStaff);
//            submittedLocationCount.setSubmittedInCount(150L);
//
//            log.info(String.format("submittedLocationCountType = ", submittedLocationCount));
//            service.submitHeadCount(uc, regFCAType.getFacilityCountId(), submittedLocationCount);
//        }
//
//        regFCAType = (RegularFCActivity) service.get(uc, regFCAType.getFacilityCountId());
//
//        for (ConsolidatedLocationCount conCountType : regFCAType.getConsolidatedLocationCounts()) {
//            conCountType.setCalculatedInCount(10L);
//            conCountType.setSubmittedInCount(10L);
//            conCountType.setSubmittedDate(new Date());
//
//        }
//
//        regFCAType.setClearCountReason(CLEAR_COUNT_REASON_ESCAPE);
//        regFCAType.setCountOutcome(COUNT_OUTCOME_CLEARCOUNT);
//        regFCAType.setConsolidationStaff(submissionStaff);
//        regFCAType.setCountEndDate(new Date());
//
//        retCreate = service.update(uc, regFCAType);
//        assert (retCreate != null);
//
//        Set<CalculatedLocationCount> locationCounts = new HashSet<CalculatedLocationCount>();
//
//        for (ConsolidatedLocationCount conCountType : regFCAType.getConsolidatedLocationCounts()) {
//            Long internal = conCountType.getInternalLocation();
//            locationCounts.add(new CalculatedLocationCount(internal, 100L, 101L));
//        }
//
//        CalculatedFacilityCount facilityCount = new CalculatedFacilityCount(600L, 50L, 20L);
//
//        retCreate = service.update(uc, regFCAType);
//        assert (retCreate != null);
//
//        unverifiedFacilityId = regFCAType.getFacilityId();
//        unverifiedStaffId = submissionStaff;
//
//        try {
//            RegularFCActivity ret = service.recordCalculatedCount(uc, regFCACountId, locationCounts, facilityCount);
//            assertNotNull(ret);
//            System.out.println(ret);
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//    }
//
//    private void createRecordCalculatedCount() {
//
//        SetupFCActivity retSetup = new SetupFCActivity();
//        // Create Scheduled Setup FCA
//        SetupFCActivity setupFCActivity = getSetupFCAInstance();
//        retSetup = service.create(uc, setupFCActivity);
//
//        assert (retSetup != null);
//        assert (retSetup.getFacilityCountId() != null && retSetup.getFacilityCountId() > 0);
//
//        Long setupFCAId = retSetup.getFacilityCountId();
//
//        // Create Scheduled Regular FCA
//        // long staffId = 420;
//        // long conductingStaff = 420;
//        // long submissionStaff = 410L;
//        long conductingStaff = staffId;
//        long submissionStaff = staffId;
//
//        RegularFCActivity retCreate = service.create(uc, setupFCAId, staffId);
//        assert (retCreate != null);
//        assert (retCreate.getFacilityCountId() != null && retCreate.getFacilityCountId() > 0);
//
//        long regFCACountId = retCreate.getFacilityCountId();
//        RegularFCActivity regFCAType = retCreate;
//
//        for (ConsolidatedLocationCount conCountType : regFCAType.getConsolidatedLocationCounts()) {
//            SubmittedLocationCount submittedLocationCount = new SubmittedLocationCount();
//            submittedLocationCount.setConductingStaff(conductingStaff);
//            submittedLocationCount.setInternalLocation(conCountType.getInternalLocation());
//
//            submittedLocationCount.setResubmissionComments(null);
//            submittedLocationCount.setResubmissionReason(null);
//            submittedLocationCount.setReSubmissionDiscrepancy(null);
//            submittedLocationCount.setResubmittedInCount(null);
//            submittedLocationCount.setSubmissionStaff(submissionStaff);
//            submittedLocationCount.setSubmittedInCount(150L);
//
//            log.info(String.format("submittedLocationCountType = ", submittedLocationCount));
//            service.submitHeadCount(uc, regFCAType.getFacilityCountId(), submittedLocationCount);
//        }
//
//        regFCAType = (RegularFCActivity) service.get(uc, regFCAType.getFacilityCountId());
//
//        for (ConsolidatedLocationCount conCountType : regFCAType.getConsolidatedLocationCounts()) {
//            conCountType.setCalculatedInCount(10L);
//            conCountType.setSubmittedInCount(10L);
//            conCountType.setSubmittedDate(new Date());
//
//        }
//
//        regFCAType.setClearCountReason(CLEAR_COUNT_REASON_ESCAPE);
//        regFCAType.setCountOutcome(COUNT_OUTCOME_CLEARCOUNT);
//        regFCAType.setConsolidationStaff(submissionStaff);
//        regFCAType.setCountEndDate(new Date());
//
//        retCreate = service.update(uc, regFCAType);
//        assert (retCreate != null);
//
//        Set<CalculatedLocationCount> locationCounts = new HashSet<CalculatedLocationCount>();
//
//        for (ConsolidatedLocationCount conCountType : regFCAType.getConsolidatedLocationCounts()) {
//            Long internal = conCountType.getInternalLocation();
//            locationCounts.add(new CalculatedLocationCount(internal, 100L, 101L));
//        }
//
//        CalculatedFacilityCount facilityCount = new CalculatedFacilityCount(600L, 50L, 20L);
//
//        retCreate = service.update(uc, regFCAType);
//        assert (retCreate != null);
//
//        unverifiedFacilityId = regFCAType.getFacilityId();
//        unverifiedStaffId = submissionStaff;
//
//        try {
//            RegularFCActivity ret = service.recordCalculatedCount(uc, regFCACountId, locationCounts, facilityCount);
//            assertNotNull(ret);
//            System.out.println(ret);
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//
//    }
//
//    @Test
//    public void testRetrieveUnverifiedFacilityCount() {
//        createRecordCalculatedCount();
//        // usijng staff and facility from the testRecordCalculatedCount
//        unverifiedCountsReturn = null;
//        try {
//            unverifiedCountsReturn = service.retrieveUnverifiedFacilityCount(uc, facId, null, null, staffId);
//            assert (unverifiedCountsReturn.getTotalSize() > 0);
//			 * assert(unverifiedCountsReturnType.getReturnCode().equals(
//			 * FacilityCountActivityService.SUCCESS));
//			 * assertNotNull(unverifiedCountsReturnType.getCount());
//			 * assertNotNull(unverifiedCountsReturnType.getUnverifiedCounts());
//			 * for( UnverifiedCountType countType :
//			 * unverifiedCountsReturnType.getUnverifiedCounts() ){
//			 * assertNotNull(countType); }
//
//
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testVerifyFacilityCount() {
//        testRetrieveUnverifiedFacilityCount();
//        Set<UnverifiedCount> countTypes = new HashSet<UnverifiedCount>();
//        System.out.println("unverifiedCountsReturnType===>" + unverifiedCountsReturn);
//        if (unverifiedCountsReturn.getResultSet() != null && !unverifiedCountsReturn.getResultSet().isEmpty()) {
//
//            for (int index = 0; index < unverifiedCountsReturn.getResultSet().size(); index++) {
//                countTypes.add((UnverifiedCount) (unverifiedCountsReturn.getResultSet().toArray()[index]));
//            }
//        }
//        try {
//            SearchReturnDTO<UnverifiedCount> ret = service.verifyFacilityCount(uc, countTypes);
//            assertNotNull(ret);
//            assertNotNull(ret.getResultSet());
//            assertNotNull(ret.getTotalSize());
//            for (UnverifiedCount countType : ret.getResultSet()) {
//                assertNotNull(countType);
//                System.out.println("verfied Flag is------>>>>>>>" + countType.isVerifiedFlag());
//            }
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testCreateRecountActivity() {
//
//        try {
//            SetupFCActivity retSetup = new SetupFCActivity();
//            // Create Scheduled Setup FCA
//            SetupFCActivity setupFCActivity = getSetupFCAInstance();
//            retSetup = service.create(uc, setupFCActivity);
//
//            assert (retSetup != null);
//
//            Long setupFCAId = retSetup.getFacilityCountId();
//
//            // Create Scheduled Regular FCA
//            // long staffId = 420;
//            RegularFCActivity retCreate = service.create(uc, setupFCAId, staffId);
//            assert (retCreate != null);
//
//            long facilityCountId = retCreate.getFacilityCountId();
//            RegularFCActivity ret = service.createRecountActivity(uc, facilityCountId);
//            assert (ret != null);
//            assert (ret.getCountStatus() != null && ret.getCalculatedFacilityCount() != null);
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testDeleteAll() {
//
//        service.deleteAll(uc);
//    }
//
//    @Test
//    public void testGetAll() {
//
//        try {
//            SetupFCActivity setupFCActivity = new SetupFCActivity();
//            setupFCActivity.setCountDate(presentDate);
//            setupFCActivity.setCountType(COUNT_TYPE);
//
//            Set<InternalLocation> internalLocations = new HashSet<InternalLocation>();
//            internalLocations.add(new InternalLocation(filId, false));
//
//            setupFCActivity.setInternalLocations(internalLocations);
//
//            setupFCActivity.setActivityId(actId);
//            setupFCActivity.setFacilityId(facId);
//            // setupFCActivity.setParentId(createFacilityCountActivity());
//
//            SetupFCActivity sRt = service.create(uc, setupFCActivity);
//            // long staffId = 400L;
//            @SuppressWarnings("unused") RegularFCActivity rRt = service.create(uc, sRt.getFacilityCountId(), staffId);
//
//            Set<Long> ret = service.getAll(uc);
//            assertNotNull(ret);
//            assertEquals(ret.size(), 4);
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testGetStamp() {
//
//        try {
//
//            SetupFCActivity ret = null;
//
//            SetupFCActivity setUpFCA = getSetupFCAInstance();
//
//            ret = service.create(uc, setUpFCA);
//
//            assert (ret != null);
//
//            Long setupFCAId = ret.getFacilityCountId();
//            assert (setupFCAId > 0);
//
//            StampType stamp = service.getStamp(uc, setupFCAId);
//            assert (stamp != null);
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//    }
//
//*************************
//
//*
//     * *From FCA Composite Test Cases***
//
//
//
//    public void testDefectNO1453and1454and1463() {
//
//        Set<RegularFCActivity> regularFCAIdSet = new HashSet<RegularFCActivity>();
//
//        for (int index = 1; index <= 1; index++) {
//            RegularFCActivity regFCARet = new RegularFCActivity();
//
//            SetupFCActivity setupFCActivity = getSetupFCAInstance();
//
//            SetupFCActivity setupFCARet = service.create(uc, setupFCActivity);
//
//            Long setupFCAId = setupFCARet.getFacilityCountId();
//
//            System.out.println("Setup Facility count activity created successfully.....with Id....." + setupFCAId);
//
//            // Long staffId = 30L;
//            regFCARet = service.create(uc, setupFCAId, staffId);
//            Long regFCAId = regFCARet.getFacilityCountId();
//            regularFCAIdSet.add(regFCARet);
//            System.out.println("Regular Facility count activity created successfully.....with Id....." + regFCAId);
//        }
//
//        Iterator<RegularFCActivity> itr = regularFCAIdSet.iterator();
//        // Long submissionStaff = 31L;
//        // Long conductingStaff = 32L;
//
//        while (itr.hasNext()) {
//            RegularFCActivity tempReg = itr.next();
//            System.out.println("Updating location counts for Regular FCA......with Id......." + tempReg.getFacilityCountId());
//            for (ConsolidatedLocationCount conCountType : tempReg.getConsolidatedLocationCounts()) {
//                SubmittedLocationCount submittedLocationCount = new SubmittedLocationCount();
//                submittedLocationCount.setConductingStaff(staffId);
//                submittedLocationCount.setInternalLocation(conCountType.getInternalLocation());
//                submittedLocationCount.setResubmissionComments(null);
//                submittedLocationCount.setResubmissionReason(null);
//                submittedLocationCount.setReSubmissionDiscrepancy(null);
//                submittedLocationCount.setResubmittedInCount(null);
//                submittedLocationCount.setSubmissionStaff(staffId);
//                submittedLocationCount.setSubmittedInCount(150L);
//
//                service.submitHeadCount(uc, tempReg.getFacilityCountId(), submittedLocationCount);
//                System.out.println("Location Counts has been submitted successfully......for internal location....." + conCountType.getInternalLocation());
//            }
//            FCActivity fcaTypeRet = service.get(uc, tempReg.getFacilityCountId());
//            if (fcaTypeRet != null && fcaTypeRet instanceof RegularFCActivity) {
//                RegularFCActivity regFCAType = (RegularFCActivity) fcaTypeRet;
//
//                for (ConsolidatedLocationCount locType : regFCAType.getConsolidatedLocationCounts()) {
//                    // locType.setCalculatedInCount(121L);
//                    locType.setCalculatedInCount(0L);
//                    locType.setCalculatedOutCount(110L);
//                    // locType.setConductingStaff(11L);
//                    locType.setConductingStaff(staffId);
//                    locType.setResubmissionComments("Abc");
//                    locType.setResubmissionReason(RESUBMISSION_REASON);
//                    locType.setReSubmittedDate(getDate(1, 1, 2, 1, 1, 1));
//                    locType.setResubmittedInCount(111L);
//                    locType.setSubmissionStaff(staffId);
//                    locType.setSubmittedDate(getDate(1, 1, 1, 1, 1, 1));
//                    locType.setSubmittedInCount(120L);
//                    locType.setVerifiedFlag(Boolean.FALSE);
//                    locType.setSubmissionDiscrepancy(0L);
//                    locType.setReSubmissionDiscrepancy(0L);
//                }
//
//                regFCAType.getCalculatedFacilityCount().setNumActiveSupervision(100L);
//                regFCAType.getCalculatedFacilityCount().setNumOffenderOutside(101L);
//                regFCAType.getCalculatedFacilityCount().setSumCalculatedInCount(102L);
//
//                regFCAType.setCountStatus(COUNT_STATUS_IN_PROGRESS);
//                regFCAType.setCountOutcome(COUNT_OUTCOME_RECOUNT);
//                regFCAType.setRecountReason(RECOUNT_REASON);
//                regFCAType.setCountType(COUNT_TYPE_USCH);
//
//                regFCAType.setParentId(createFacilityCountActivity());
//
//                regFCAType.setCountOutcome(COUNT_OUTCOME_CLEARCOUNT);
//                regFCAType.setClearCountReason(CLEAR_COUNT_REASON_ESCAPE);
//
//                unverifiedFacilityId = regFCAType.getFacilityId();
//                unverifiedStaffId = staffId;
//
//                regFCAType.setCountEndDate(new Date());
//                regFCAType.setComments("Hello");
//                RegularFCActivity regFCARet = service.update(uc, regFCAType);
//                System.out.println("Regular FCA has been updated successfully and the Count Status become InProgress............");
//
//                regFCAType = regFCARet;
//
//                for (ConsolidatedLocationCount locType : regFCAType.getConsolidatedLocationCounts()) {
//                    locType.setCalculatedInCount(121L);
//                    // locType.setCalculatedInCount(0L);
//                    locType.setCalculatedOutCount(110L);
//                    // locType.setConductingStaff(12L);
//                    locType.setConductingStaff(staffId);
//                    locType.setResubmissionComments("XYZ");
//                    locType.setResubmissionReason(RESUBMISSION_REASON);
//                    locType.setReSubmittedDate(getDate(1, 1, 2, 1, 1, 1));
//                    locType.setResubmittedInCount(111L);
//                    // locType.setSubmissionStaff(value)
//                    locType.setSubmittedDate(getDate(1, 1, 1, 1, 1, 1));
//                    locType.setSubmittedInCount(120L);
//                    locType.setVerifiedFlag(Boolean.FALSE);
//                    locType.setSubmissionDiscrepancy(0L);
//                    locType.setReSubmissionDiscrepancy(0L);
//                }
//
//                regFCAType.getCalculatedFacilityCount().setNumActiveSupervision(200L);
//                regFCAType.getCalculatedFacilityCount().setNumOffenderOutside(201L);
//                regFCAType.getCalculatedFacilityCount().setSumCalculatedInCount(202L);
//
//                regFCAType.setCountStatus(COUNT_STATUS_IN_PROGRESS);
//                regFCAType.setCountOutcome(COUNT_OUTCOME_RECOUNT);
//                regFCAType.setRecountReason(RECOUNT_REASON);
//                regFCAType.setCountType(COUNT_TYPE);
//                regFCAType.setActivityId(actId);
//                regFCAType.setCountOutcome(COUNT_OUTCOME_CLEARCOUNT);
//                regFCAType.setClearCountReason(CLEAR_COUNT_REASON_ESCAPE);
//
//                regFCAType.setCountEndDate(new Date());
//                regFCAType.setComments("Hello");
//                regFCARet = service.update(uc, regFCAType);
//
//            }
//        }
//        // staffId = 11111L;
//        Long facId = unverifiedFacilityId;
//
//        unverifiedCountsReturn = service.retrieveUnverifiedFacilityCount(uc, facId, null, null, 11111L);
//        assertNotNull(unverifiedCountsReturn);
//
//        assert (unverifiedCountsReturn.getTotalSize() == 0);
//
//        //staffId = unverifiedStaffId;
//        unverifiedCountsReturn = service.retrieveUnverifiedFacilityCount(uc, facId, null, null, unverifiedStaffId);
//        assertNotNull(unverifiedCountsReturn);
//        assert (unverifiedCountsReturn.getTotalSize() > 0);// ERROR
//
//        for (UnverifiedCount unverifiedCount : unverifiedCountsReturn.getResultSet()) {
//            assert (unverifiedCount.isVerifiedFlag().equals(Boolean.FALSE));
//            assert (unverifiedCount.getCountOutcome() != null);
//        }
//
//    }
//
//    @Test
//    public void testDefectNO1461() {
//
//        try {
//            SetupFCActivity ret = new SetupFCActivity();
//            // Create Scheduled Setup FCA
//            SetupFCActivity setupFCActivity = getSetupFCAInstance();
//            ret = service.create(uc, setupFCActivity);
//
//            assert (ret != null);
//            assert (ret != null && ret.getFacilityCountId() > 0);
//
//            Long setupFCAId = ret.getFacilityCountId();
//
//            // Create Scheduled Regular FCA
//            // long staffId = 420;
//            RegularFCActivity retCreate = service.create(uc, setupFCAId, staffId);
//            assert (retCreate != null);
//            assert (retCreate.getFacilityCountId() != null && retCreate.getFacilityCountId() > 0);
//
//            RegularFCActivity regularFCActivity = retCreate;
//            CalculatedFacilityCount calfacCount = new CalculatedFacilityCount(100L, 5L, 95L);
//            ConsolidatedLocationCount conLocCount = new ConsolidatedLocationCount(filId, null, null, null, null, null, null, RESUBMISSION_REASON, null, null, null, false,
//                    null, null, null, null);
//
//            regularFCActivity.setCalculatedFacilityCount(calfacCount);
//            regularFCActivity.setConsolidatedLocationCounts(new HashSet<ConsolidatedLocationCount>());
//            regularFCActivity.getConsolidatedLocationCounts().add(conLocCount);
//            regularFCActivity.setRecountReason(null);
//            regularFCActivity.setClearCountReason(null);
//            regularFCActivity.setComments(null);
//            regularFCActivity.setCountOutcome(null);
//            regularFCActivity.setCountStatus(COUNT_STATUS_COMPLETED);
//            // regularFCActivity.setConsolidationStaff(12345L);
//            regularFCActivity.setConsolidationStaff(staffId);
//            regularFCActivity.setCountAttempt(5L);
//            regularFCActivity.setCountEndDate(null);
//
//            RegularFCActivity retupadte = service.update(uc, regularFCActivity);
//            assert (retupadte != null);
//            assert (retupadte.getFacilityCountId() != null && retupadte.getCalculatedFacilityCount() != null);
//
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//    }
//
//    @Test
//    public void testDefectNo1469and1470() {
//
//        SetupFCActivity setupFCActivity = getSetupFCAInstance();
//        SetupFCActivity ret = service.create(uc, setupFCActivity);
//
//        assert (ret != null);
//        assert (ret != null && ret.getFacilityCountId() > 0);
//
//        SetupFCActivitySearch searchType = new SetupFCActivitySearch();
//
//        searchType.setCountType(null);
//        searchType.setInternalLocations(null);
//        searchType.setFacilityId(null);
//        searchType.setActivityId(actId);
//        searchType.setParentId(null);
//        searchType.setCountDateFrom(null);
//        searchType.setCountDateTo(null);
//
//        SearchReturnDTO<SetupFCActivity> searchSetupRet = service.search(uc, null, searchType, 0L, 100L, null);
//
//        assert (searchSetupRet != null);
//
//        searchType = new SetupFCActivitySearch();
//
//        searchType.setCountType(null);
//        searchType.setInternalLocations(null);
//        searchType.setFacilityId(null);
//        searchType.setActivityId(null);
//        searchType.setParentId(null);
//        searchType.setCountDateFrom(null);
//        searchType.setCountDateTo(null);
//
//        try {
//            searchSetupRet = service.search(uc, null, searchType, 0L, 100L, null);
//            assertEquals(searchSetupRet, null);
//        } catch (Exception ex) {
//            assert (ex.getClass().equals(InvalidInputException.class));
//        }
//
//        searchType.setCountDateTo(new Date());
//        searchType.setInternalLocations(new HashSet<Long>());
//        searchType.getInternalLocations().add(filId);
//        searchSetupRet = service.search(uc, null, searchType, 0L, 100L, null);
//        assert (searchSetupRet != null);
//
//        long setupFCAId = ret.getFacilityCountId();
//        // Create Scheduled Regular FCA
//        // long staffId = 420;
//        RegularFCActivity retCreate = service.create(uc, setupFCAId, staffId);
//        assert (retCreate != null);
//        assert (retCreate.getFacilityCountId() != null && retCreate.getFacilityCountId() > 0);
//
//        RegularFCActivitySearch regSearchType = new RegularFCActivitySearch();
//
//        regSearchType.setSubmissionStaff(staffId);
//        regSearchType.setConductingStaff(staffId);
//        regSearchType.setConsolidationStaff(staffId);
//        regSearchType.setInternalLocations(new HashSet<Long>());
//        searchType.getInternalLocations().add(filId);
//        SearchReturnDTO<RegularFCActivity> searchRegRet = service.search(uc, null, regSearchType, 0L, 100L, null);
//        assert (searchRegRet != null);
//
//    }
//
//    @Test
//    public void testDefectNo1505() {
//
//        SetupFCActivity setupFCActivity = getSetupFCAInstance();
//        SetupFCActivity ret = service.create(uc, setupFCActivity);
//
//        assert (ret != null);
//        assert (ret != null && ret.getFacilityCountId() > 0);
//
//        long setupFCAId = ret.getFacilityCountId();
//        // Create Scheduled Regular FCA
//        // long staffId = 420;
//        RegularFCActivity retCreate = service.create(uc, setupFCAId, staffId);
//        assert (retCreate != null);
//        assert (retCreate.getFacilityCountId() != null && retCreate.getFacilityCountId() > 0);
//
//        RegularFCActivitySearch regSearchType = new RegularFCActivitySearch();
//
//        regSearchType.setConsolidationStaff(staffId);
//        SearchReturnDTO<RegularFCActivity> searchRegRet = service.search(uc, null, regSearchType, 0L, 100L, null);
//        assert (searchRegRet != null);
//
//    }
//
//    @Test
//    public void testDefectNo1474and1476and1477() {
//
//        SetupFCActivity ret = new SetupFCActivity();
//        // Create Scheduled Setup FCA
//        SetupFCActivity setupFCActivity = getSetupFCAInstance();
//        ret = service.create(uc, setupFCActivity);
//
//        assert (ret != null);
//        assert (ret != null && ret.getFacilityCountId() > 0);
//
//        Long setupFCAId = ret.getFacilityCountId();
//
//        // Create Scheduled Regular FCA
//        // long staffId = 420;
//        RegularFCActivity retCreate = service.create(uc, setupFCAId, staffId);
//        assert (retCreate != null);
//        assert (retCreate.getFacilityCountId() != null && retCreate.getFacilityCountId() > 0);
//
//        // Pass the any dummy data, should return success code and no data
//        RegularFCActivitySearch regSearchType = new RegularFCActivitySearch();
//        regSearchType.setInternalLocations(new HashSet<Long>());
//        regSearchType.getInternalLocations().add(filId);
//        regSearchType.setResubmissionReason(new String("MSC"));
//        regSearchType.setResubmissionComments(
//                "abcdefghi0123456789012345678901234567890123456789012345678901234abcdefghi0123456789012345678901234567890123456789012345678901234");
//        // regSearchType.setSubmissionStaff(789L);
//        // regSearchType.setConductingStaff(987L);
//        regSearchType.setSubmissionStaff(staffId);
//        regSearchType.setConductingStaff(staffId);
//        regSearchType.setVerifiedFlag(true);
//        regSearchType.setDiscrepancyFlag(false);
//        regSearchType.setCountType(new String("SCH"));
//        regSearchType.setRecountReason(RECOUNT_REASON);
//        regSearchType.setClearCountReason(CLEAR_COUNT_REASON_ESCAPE);
//        regSearchType.setOutcomeComments(
//                "abcdefghi0123456789012345678901234567890123456789012345678901234abcdefghi0123456789012345678901234567890123456789012345678901234");
//        regSearchType.setCountOutcome(COUNT_OUTCOME_RECOUNT);
//        regSearchType.setCountStatus(COUNT_STATUS_READY);
//        // regSearchType.setConsolidationStaff(456L);
//        regSearchType.setConsolidationStaff(staffId);
//        regSearchType.setCountStartDateFrom(null);
//        regSearchType.setCountStartDateTo(null);
//        regSearchType.setCountEndDateFrom(null);
//        regSearchType.setCountEndDateTo(null);
//        regSearchType.setFacilityId(null);
//        regSearchType.setActivityId(null);
//        regSearchType.setParentId(null);
//        SearchReturnDTO<RegularFCActivity> searchRegRet = service.search(uc, null, regSearchType, 0L, 100L, null);
//        assert (searchRegRet != null);
//
//        regSearchType = new RegularFCActivitySearch();
//
//        regSearchType.setInternalLocations(null);
//        regSearchType.setResubmissionReason(null);
//        regSearchType.setResubmissionComments(null);
//        regSearchType.setConductingStaff(null);
//        regSearchType.setVerifiedFlag(null);
//        regSearchType.setDiscrepancyFlag(null);
//        regSearchType.setCountType(null);
//        regSearchType.setRecountReason(null);
//        regSearchType.setClearCountReason(null);
//        regSearchType.setOutcomeComments(null);
//        regSearchType.setCountOutcome(null);
//        regSearchType.setCountStatus(null);
//        regSearchType.setConsolidationStaff(null);
//        regSearchType.setCountStartDateFrom(null);
//        regSearchType.setCountStartDateTo(null);
//        regSearchType.setCountEndDateFrom(null);
//        regSearchType.setCountEndDateTo(null);
//        regSearchType.setFacilityId(null);
//        regSearchType.setActivityId(null);
//        regSearchType.setParentId(null);
//        regSearchType.setSubmissionStaff(staffId);// Set Existing Submission
//        // staff
//        searchRegRet = service.search(uc, null, regSearchType, 0L, 100L, null);
//        assert (searchRegRet != null);
//        assert (searchRegRet.getTotalSize() > 0);
//
//        regSearchType = new RegularFCActivitySearch();
//
//        regSearchType.setInternalLocations(null);
//        regSearchType.setResubmissionReason(null);
//        regSearchType.setResubmissionComments(null);
//        regSearchType.setConductingStaff(null);
//        regSearchType.setVerifiedFlag(false);// Set Verified Flag Only to FALSE
//        regSearchType.setDiscrepancyFlag(null);
//        regSearchType.setCountType(null);
//        regSearchType.setRecountReason(null);
//        regSearchType.setClearCountReason(null);
//        regSearchType.setOutcomeComments(null);
//        regSearchType.setCountOutcome(null);
//        regSearchType.setCountStatus(null);
//        regSearchType.setConsolidationStaff(null);
//        regSearchType.setCountStartDateFrom(null);
//        regSearchType.setCountStartDateTo(null);
//        regSearchType.setCountEndDateFrom(null);
//        regSearchType.setCountEndDateTo(null);
//        regSearchType.setFacilityId(null);
//        regSearchType.setActivityId(null);
//        regSearchType.setParentId(null);
//        regSearchType.setSubmissionStaff(null);
//        searchRegRet = service.search(uc, null, regSearchType, 0L, 100L, null);
//        assert (searchRegRet != null);
//        assert (searchRegRet.getTotalSize() > 0);
//
//        regSearchType = new RegularFCActivitySearch();
//
//        regSearchType.setInternalLocations(null);
//        regSearchType.setResubmissionReason(null);
//        regSearchType.setResubmissionComments(null);
//        regSearchType.setConductingStaff(null);
//        regSearchType.setVerifiedFlag(true);// Set Verified Flag Only to true
//        regSearchType.setDiscrepancyFlag(null);
//        regSearchType.setCountType(null);
//        regSearchType.setRecountReason(null);
//        regSearchType.setClearCountReason(null);
//        regSearchType.setOutcomeComments(null);
//        regSearchType.setCountOutcome(null);
//        regSearchType.setCountStatus(null);
//        regSearchType.setConsolidationStaff(null);
//        regSearchType.setCountStartDateFrom(null);
//        regSearchType.setCountStartDateTo(null);
//        regSearchType.setCountEndDateFrom(null);
//        regSearchType.setCountEndDateTo(null);
//        regSearchType.setFacilityId(null);
//        regSearchType.setActivityId(null);
//        regSearchType.setParentId(null);
//        regSearchType.setSubmissionStaff(null);
//        searchRegRet = service.search(uc, null, regSearchType, 0L, 100L, null);
//        assert (searchRegRet != null);
//        assert (searchRegRet.getTotalSize() == 0);
//    }
//
//    private SetupFCActivity getSetupFCAInstance(String countType, Date countDate) {
//
//        SetupFCActivity setupFCActivity = new SetupFCActivity();
//        setupFCActivity.setCountDate(countDate);
//        setupFCActivity.setCountType(countType);
//
//        Set<InternalLocation> internalLocations = new HashSet<InternalLocation>();
//
//        internalLocations.add(new InternalLocation(childL1.getId(), false));
//        internalLocations.add(new InternalLocation(childL2.getId(), false));
//
//        setupFCActivity.setInternalLocations(internalLocations);
//
//        setupFCActivity.setFacilityId(facId);
//        setupFCActivity.setActivityId(facId);
//        setupFCActivity.setParentId(null);
//
//        return setupFCActivity;
//    }
//
//    private ScheduleTimeslotType getScheduleTimeslotInstance(Long scheduleId) {
//        ScheduleTimeslotType scheduleTimeslotType = new ScheduleTimeslotType();
//
//        scheduleTimeslotType.setScheduleID(scheduleId);
//        scheduleTimeslotType.setTimeslotStartDateTime(getDate(2012, 12, 30, 15, 10, 10));
//        scheduleTimeslotType.setTimeslotEndDateTime(getDate(2012, 12, 30, 17, 10, 10));
//
//        return scheduleTimeslotType;
//    }
//
//    @Test(dependsOnMethods = { "testJNDILookup" })
//    public void testFCAInmateDetails() {
//
//        SearchFCActivityInmateDetails searchFCActivityInmateDetails = new SearchFCActivityInmateDetails();
//        searchFCActivityInmateDetails.setLocationId(112L);
//
//        FCAInmateDetailsGridReturn ret = service.searchInmateDetailsgrid(uc, searchFCActivityInmateDetails, 0L, 20L, null, null);
//        assert (ret != null);
//    }
//
//    public void testAcceptFCAEntry() {
//
//    }
//
//    public void testVerifyFCAInProgress() {
//
//    }
//
//    @Test
//    public void testRetrieveUnverifiedFacilityCountFCAComposite() {
//        try {
//            SearchReturnDTO<UnverifiedCount> unverifiedCountsReturn = service.retrieveUnverifiedFacilityCount(uc, facId, staffId);
//            assertNotNull(unverifiedCountsReturn);
//
//        } catch (Exception re) {
//            re.printStackTrace();
//        }
//    }
//
//    @Test(dependsOnMethods = { "testJNDILookup" })
//    public void testSearchHistoricalCounts() {
//        FacilityCountSearchCriteria fcaSearchCriteria = new FacilityCountSearchCriteria();
//
//        try {
//            service.searchHistoricalCounts(uc, fcaSearchCriteria, null);
//        } catch (Exception ex) {
//            assert (ex.getClass().equals(InvalidInputException.class));
//        }
//
//        List<FacilityCountSearchResult> searchRet = service.searchHistoricalCounts(uc, fcaSearchCriteria, null);
//        System.out.println("Search Ret====>" + searchRet.toString());
//        System.out.println("getTotalSize====>" + searchRet.size());
//
//        assert (searchRet != null);
//    }
//
//    @Test(dependsOnMethods = { "testJNDILookup" })
//    public void testRetrievePreConfiguredScheduledTime() {
//        SchedulesReturnType ret = service.retrievePreConfiguredScheduledTime(uc, facId, 0L, 100L, null);
//
//        assert (ret != null);
//    }
//
//    @SuppressWarnings("unused")
//    private void makeUpdatesInSchedule(Schedule schedule) {
//        schedule.setTerminationDate(getEffectiveDate());
//
//    }
//
//    private void makeUpdatesInActivity(ActivityType activityType) {
//        // Long newAss = new Long(
//        // syscon.arbutus.product.services.activity.contract.dto.AssociationToClass.FACOUNT
//        // .value(), 100L);
//        // Long newAss = new Long(100L);
//        // activityType.getAssociations().add(newAss);
//
//        // instead of chaning association, I am changing the planned date change
//        Calendar c = Calendar.getInstance();
//        c.add(Calendar.DAY_OF_MONTH, 10);
//        Date TenAhead = c.getTime();
//        activityType.setPlannedEndDate(TenAhead);
//    }
//
//    private void makeUpdatesInSetupFCA(SetupFCActivity setupFCActivity) {
//        setupFCActivity.setCountType(COUNT_TYPE);
//        setupFCActivity.setActivityId(actId);
//        setupFCActivity.setCountDate(getDateWithoutTime(new Date()));
//    }
//
//    private SetupFCActivity getSetupFCAInstance() {
//        Date presentDate = getDate(2012, 10, 10, 10, 10, 10);
//
//        SetupFCActivity setupFCActivity = new SetupFCActivity();
//        setupFCActivity.setCountDate(presentDate);
//        setupFCActivity.setCountType(COUNT_TYPE);
//
//        Set<InternalLocation> internalLocations = new HashSet<InternalLocation>();
//
//        internalLocations.add(new InternalLocation(childL1.getId(), false));
//        internalLocations.add(new InternalLocation(childL2.getId(), false));
//
//        setupFCActivity.setInternalLocations(internalLocations);
//
//        setupFCActivity.setFacilityId(facId);
//        // FacilityCountActivity has activityId
//        setupFCActivity.setActivityId(actId);
//        setupFCActivity.setParentId(null);// must have null parent
//        return setupFCActivity;
//    }
//
//    private ActivityType getActivityInstance() {
//
//        ActivityType activityType = new ActivityType();
//        Boolean activeStatusFlag = Boolean.TRUE;
//        Calendar c = Calendar.getInstance();
//        Date today = Calendar.getInstance().getTime();
//        c.add(Calendar.DAY_OF_MONTH, 1);
//        Date tomorrow = c.getTime();
//        String activityCategory = new String("FACOUNT");
//        Long activityAntecedentId = new Long(createActivity());
//
//        activityType = new ActivityType(null, null, activeStatusFlag, today, tomorrow, activityCategory, null, null, null);
//
//        activityType.setActivityAntecedentId(activityAntecedentId);
//
//        return activityType;
//    }
//
//    private Schedule getScheduleInstance() {
//
//        Long forInternalLocationId = root.getId();
//
//        Schedule schedule = new Schedule();
//
//        schedule.setForInternalLocationId(forInternalLocationId);
//
//        schedule.setCapacity(100L);
//        schedule.setEffectiveDate(getDateWithoutTime(2012, Calendar.MAY, 1));
//        schedule.setFacilityId(facId);
//        schedule.setFacilityScheduleReason(null);
//        schedule.setScheduleCategory(ACTIVITY_CATEGORY_FACILITY_COUNT);
//        schedule.setScheduleEndTime("17:00");
//        // scheduleType.setScheduleIdentification()
//        schedule.setScheduleStartTime("15:00");
//        schedule.setTerminationDate(getDateWithoutTime(2015, Calendar.DECEMBER, 31));
//
//        Schedule ret = actService.createSchedule(uc, schedule);
//
//        return ret;
//    }
//
//    @SuppressWarnings("unused")
//    private Date createFutureDate(int day) {
//        Date now = new Date();
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(now);
//        cal.add(Calendar.DAY_OF_YEAR, day);
//        Date futureDate = cal.getTime();
//        return futureDate;
//    }
//
//    @SuppressWarnings("unused")
//    private Date createPastDate(int day) {
//        Date now = new Date();
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(now);
//        cal.add(Calendar.DAY_OF_YEAR, -day);
//        Date pastDate = cal.getTime();
//        return pastDate;
//    }
//
//    private Long createFacilityCountActivity() {
//
//        SetupFCActivity setupFCActivity = getSetupFCAInstance();
//
//        // Create Non scheduled Setup FCA
//        setupFCActivity.setCountType(COUNT_TYPE_USCH);
//        setupFCActivity.setActivityId(null);
//        setupFCActivity.setParentId(null);
//        SetupFCActivity ret = service.create(uc, setupFCActivity);
//
//        assert (ret != null);
//
//        return ret.getFacilityCountId();
//    }
//
//    private Long createActivity() {
//        Schedule sch = new Schedule();
//        sch.setScheduleCategory(ActivityCategory.MOVEMENT.value());
//        sch.setFacilityId(facId);
//        sch.setCapacity(10L);
//        sch.setEffectiveDate(getEffectiveDate());
//        sch.setScheduleStartTime("07:00");
//        sch.setScheduleEndTime("17:00");
//
//        Schedule schRet = actService.createSchedule(uc, sch);
//
//        // ActivityType
//        ActivityType activityType = new ActivityType();
//
//        // Required - ActiveStatusFlag & ActivityCategory
//        activityType.setActiveStatusFlag(Boolean.TRUE);
//        activityType.setActivityCategory("MOVEMENT");
//
//        // Planned end date is required if planned start date is populated
//        Calendar cal = Calendar.getInstance();
//        activityType.setPlannedStartDate(cal.getTime());
//
//        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1);
//        activityType.setPlannedEndDate(cal.getTime());
//
//        // scheduleID
//        activityType.setScheduleID(schRet.getScheduleIdentification());
//
//        // ScheduleTimeslotType
//        ScheduleTimeslotType scheduleTimeslot = new ScheduleTimeslotType();
//        scheduleTimeslot.setScheduleID(schRet.getScheduleIdentification());
//
//        scheduleTimeslot.setTimeslotStartDateTime(Calendar.getInstance().getTime());
//
//        ActivityType ret = actService.create(uc, activityType, null, Boolean.TRUE);
//        assertNotNull(ret);
//
//        assertNotNull(ret);
//        return ret.getActivityIdentification();
//    }
//
//    private Long createFacilityInternalLocation() {
//        String phyCode = "PC01";
//        String description = "physical location root";
//        String level = "FACILITY";
//        FacilityInternalLocationType pl = createFacilityInternalLocation(level, phyCode, description);
//        pl.getComments().add(createComment("root comment"));
//
//        LocationAttributeType prop = new LocationAttributeType();
//        prop.getOffenderCategories().add("SUIC");
//        pl.setLocationAttribute(prop);
//        FacilityInternalLocationType ret = facInService.create(uc, pl, UsageCategory.HOU);
//        assertNotNull(ret);
//        log.info(String.format("ret = %s", ret));
//        root = ret;
//        return ret.getId();
//    }
//
//    private void createFacilityInternalLocationChild1() {
//        String phyCode = "Wing 2-1";
//        String description = "first Level 1 child in facility 2";
//        String level = "WING";
//        childL1 = createFacilityInternalLocation(level, phyCode, description);
//        childL1.setParentId(root.getId());
//
//        LocationAttributeType prop = new LocationAttributeType();
//        childL1.setLocationAttribute(prop);
//        prop.getOffenderCategories().add("PRCUS");
//        prop.getGenders().add("F");
//        prop.getAgeRanges().add("JUVENILE");
//        FacilityInternalLocationType ret = facInService.create(uc, childL1, UsageCategory.HOU);
//        childL1 = ret;
//    }
//
//    private void createFacilityInternalLocationChild2() {
//        childL2 = createFacilityInternalLocation("WING", "Wing 2-2", "second Level 1 child in facility 2");
//        childL2.setParentId(root.getId());
//
//        LocationAttributeType prop = new LocationAttributeType();
//        childL2.setLocationAttribute(prop);
//        prop.getOffenderCategories().add("PRCUS");
//        prop.getGenders().add("F");
//        prop.getAgeRanges().add("JUVENILE");
//        FacilityInternalLocationType ret = facInService.create(uc, childL2, UsageCategory.HOU);
//        childL2 = ret;
//    }
//
//    private Long createFacility(Long orgId) {
//
//        Set<Long> orgs = new HashSet<Long>();
//        if (!BeanHelper.isEmpty(orgId)) {
//            orgs.add(orgId);
//        }
//
//        FacilityType ret = null;
//        String category = new String("COMMU");
//        Long rand = new Random().nextLong();
//        FacilityType facility = new FacilityType(-1L, "FacilityCodeIT" + rand, "FacilityName" + rand, new Date(), category, null, null, orgs, null, false);
//        log.info(String.format("org createFacility  11 %s", facility));
//        ret = facService.create(uc, facility);
//        log.info(String.format("org createFacility  12 %s", ret));
//        assertNotEquals(ret, null);
//        return ret.getFacilityIdentification();
//    }
//
//    public FacilityInternalLocationType createFacilityInternalLocation(String level, String code, String description) {
//        FacilityInternalLocationType pt = new FacilityInternalLocationType();
//        pt.setFacilityId(facId);
//        pt.setHierarchyLevel(level);
//        pt.setLocationCode(code);
//        pt.setDescription(description);
//
//        pt.getNonAssociations().add("TOTAL");
//        pt.getNonAssociations().add("STG");
//
//        return pt;
//    }
//
//    public Long createStaff() {
//        StaffType ret = null;
//        Set<StaffPosition> staffPositions = new HashSet<>();
//        staffPositions.add(new StaffPosition("CPT"));
//        staffPositions.add(new StaffPosition("DEP"));
//        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
//        Date endDate = new Date(2912 - 1900, 11, 30);
//        String category = new String("CON");
//        String reason = new String("SICK");
//
//        PersonIdentityType pi = createPersonalIdenity();
//
//        StaffType staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, null);
//        staff.setPersonId(pi.getPersonId());
//        //
//        ret = personService.getStaff(uc, personService.createStaff(uc, staff));
//        assert (ret != null);
//        log.info(String.format("ret = %s", ret));
//        return ret.getStaffID();
//
//    }
//
//    // Helper Method
//    private PersonIdentityType createPersonalIdenity() {
//
//        Date dob = new Date(1985 - 1900, 3, 10);
//        PersonIdentityType peter = new PersonIdentityType(null, createPerson(), null, null, "peter", "S", "S", "Pan", "i", dob, "M", null, null, "offenderNumTest", null,
//                null);
//
//        // PersonIdentityType peter = createIndentity("Peter", "Pan", new
//        // Date(84, 3, 10), "M");
//        peter = personService.createPersonIdentityType(uc, peter, Boolean.FALSE, Boolean.FALSE);
//        return peter;
//    }
//
//    private Long createPerson() {
//
//        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
//        instanceForTest.setActiveStatus("ACTIVE");
//        instanceForTest = personService.createPersonType(uc, instanceForTest);
//        return instanceForTest.getPersonIdentification();
//    }
//
//    private void testCreateAllHierchyLevelsl(Long facilityID) {
//        List<HierarchyType> list = new ArrayList<HierarchyType>();
//        list.add(new HierarchyType("FACILITY", "WING", UsageCategory.HOU, facilityID));
//        list.add(new HierarchyType("WING", "UNIT", UsageCategory.HOU, facilityID));
//        list.add(new HierarchyType("UNIT", "ROOM", UsageCategory.HOU, facilityID));
//        Map<String, HierarchyType> ret = facInService.createUpdateLocationHierarchies(uc, list);
//    }


    private static Long randLong(int i) {
        return BigInteger.valueOf(new Random().nextInt(i)).abs().longValue();
    }

    private static Date getEffectiveDate() {
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.HOUR_OF_DAY, randLong(23).intValue());
        cal.set(Calendar.MINUTE, randLong(59).intValue());
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, randLong(2012).intValue());

        return cal.getTime();
    }

    private void cleanData() {
        service.deleteAll(uc);
        facService.deleteAll(uc);
        facInService.deleteAll(uc);
        actService.deleteAll(uc);
        personService.deleteAllPersonType(uc);
    }


    public static CommentType createComment(String text) {
        CommentType ct = new CommentType();
        ct.setComment(text);
        ct.setCommentDate(new Date());
        return ct;
    }

    private static Date getDateWithoutTime(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.DAY_OF_MONTH, day);

        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    private static Date getDateWithoutTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Date newDate = cal.getTime();
        return newDate;
    }

}
