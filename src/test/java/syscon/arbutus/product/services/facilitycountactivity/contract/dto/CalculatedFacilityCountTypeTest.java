//package syscon.arbutus.product.services.facilitycountactivity.contract.dto;
//
//import org.testng.annotations.Test;
//
//public class CalculatedFacilityCountTypeTest {
//
//	@Test
//	public void isWellFormed() {
//
//		CalculatedFacilityCountType data = new CalculatedFacilityCountType();
//		assert (!data.isWellFormed());
//
//		data = new CalculatedFacilityCountType(null, null, null);
//		assert (!data.isWellFormed());
//
//		data = new CalculatedFacilityCountType(1L, null, null);
//		assert (!data.isWellFormed());
//
//		data = new CalculatedFacilityCountType(1L, 2L, null);
//		assert (!data.isWellFormed());
//
//		data = new CalculatedFacilityCountType(1L, 2L, 3L);
//		assert (data.isWellFormed());
//
//		data = new CalculatedFacilityCountType(-1L, 2L, 3L);
//		assert (data.isWellFormed());
//
//		data = new CalculatedFacilityCountType(1L, -2L, 3L);
//		assert (data.isWellFormed());
//
//		data = new CalculatedFacilityCountType(1L, 2L, -3L);
//		assert (data.isWellFormed());
//	}
//
//	@Test
//	public void isValid() {
//
//		CalculatedFacilityCountType data = new CalculatedFacilityCountType();
//		assert (!data.isValid());
//
//		data = new CalculatedFacilityCountType(null, null, null);
//		assert (!data.isValid());
//
//		data = new CalculatedFacilityCountType(1L, null, null);
//		assert (!data.isValid());
//
//		data = new CalculatedFacilityCountType(1L, 2L, null);
//		assert (!data.isValid());
//
//		data = new CalculatedFacilityCountType(1L, 2L, 3L);
//		assert (data.isValid());
//
//		data = new CalculatedFacilityCountType(-1L, 2L, 3L);
//		assert (!data.isValid());
//
//		data = new CalculatedFacilityCountType(1L, -2L, 3L);
//		assert (!data.isValid());
//
//		data = new CalculatedFacilityCountType(1L, 2L, -3L);
//		assert (!data.isValid());
//	}
//}
