//package syscon.arbutus.product.services.facilitycountactivity.contract.dto;
//
//import org.testng.annotations.Test;
//
//import syscon.arbutus.product.services.core.contract.dto.AssociationType;
//
//public class ConsolidatedLocationCountTypeTest {
//
//	@Test
//	public void isWellFormed() {
//
//		ConsolidatedLocationCountType data = new ConsolidatedLocationCountType();
//		assert (!data.isWellFormed());
//		AssociationType internalLoc = new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 1L);
//		// internalLocation is null
//		data = new ConsolidatedLocationCountType(null, null, null, null, null,
//				null, null, null, null, null, null, null, null, null);
//		assert (!data.isWellFormed());
//
//		// verifiedFlag is null
//		data = new ConsolidatedLocationCountType(internalLoc, null, null, null, null,
//				null, null, null, null, null, null, null, null, null);
//		assert (!data.isWellFormed());
//
//		data = new ConsolidatedLocationCountType(internalLoc, null, null, null, null,
//				null, null, null, null, null, null, true, 1L, null);
//		assert (data.isWellFormed());
//
//	}
//
//	@Test
//	public void isValid() {
//
//		ConsolidatedLocationCountType data = new ConsolidatedLocationCountType();
//		assert (!data.isValid());
//		AssociationType internalLoc = new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(), 1L);
//		// internalLocation is null
//		data = new ConsolidatedLocationCountType(null, null, null, null, null,
//				null, null, null, null, null, null, null, null, null);
//		assert (!data.isValid());
//
//		// verifiedFlag is null
//		data = new ConsolidatedLocationCountType(internalLoc, null, null, null, null,
//				null, null, null, null, null, null, null, null, null);
//		assert (!data.isValid());
//
//		data = new ConsolidatedLocationCountType(internalLoc, null, null, null, null,
//				null, null, null, null, null, null, true, 1L, null);
//		assert (data.isValid());
//
//	}
//}
