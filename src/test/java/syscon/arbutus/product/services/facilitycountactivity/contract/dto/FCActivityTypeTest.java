//package syscon.arbutus.product.services.facilitycountactivity.contract.dto;
//
//import java.util.Date;
//import java.util.HashSet;
//import java.util.Set;
//
//import org.testng.annotations.Test;
//
//import syscon.arbutus.product.services.core.contract.dto.AssociationType;
//import syscon.arbutus.product.services.core.contract.dto.CodeType;
//
//public class FCActivityTypeTest {
//
//	@Test
//	public void isWellFormed() {
//		FCActivityType data = new FCActivityType();
//		assert (!data.isWellFormed());
//
//		data = new FCActivityType(null, null, null, null, null, null, null);
//		assert (!data.isWellFormed());
//
//		CodeType code = new CodeType(ReferenceSet.COUNT_TYPE.value(), "V");
//
//		data = new FCActivityType(null, code, null, null, null, null, null);
//		assert (!data.isWellFormed());
//
//		Set<AssociationType> references = new HashSet<AssociationType>();
//		data = new FCActivityType(null, code, null, references, null, null, null);
//		assert (!data.isWellFormed());
//
//		references = new HashSet<AssociationType>();
//		AssociationType facilityRef = new AssociationType();
//		references.add(facilityRef);
//		data = new FCActivityType(null, code, new Date(), references, null,
//				null, null);
//		assert (!data.isWellFormed());
//
//		references = new HashSet<AssociationType>();
//		facilityRef = new AssociationType(AssociationToClass.FACILITY.value(),
//				1L);
//		AssociationType activityRef = new AssociationType(
//				AssociationToClass.ACTIVITY.value(), 2L);
//		references.add(activityRef);
//		
//		data = new FCActivityType(null, code, new Date(), references, null,
//				null,facilityRef);
//		assert (data.isWellFormed());
//
//		references = new HashSet<AssociationType>();
//		activityRef = new AssociationType(AssociationToClass.ACTIVITY.value(),
//				1L);
//		references.add(activityRef);
//		data = new FCActivityType(null, code, new Date(), references, null,
//				null,facilityRef);
//		assert (data.isWellFormed());
//
//		Set<String> privileges = new HashSet<String>();
//		data = new FCActivityType(null, code, new Date(), references,
//				privileges, null, facilityRef);
//		assert (data.isWellFormed());
//
//		privileges = new HashSet<String>();
//		privileges.add(new String());
//		data = new FCActivityType(null, code, new Date(), references,
//				privileges, null, facilityRef);
//		assert (data.isWellFormed());
//
//		privileges = new HashSet<String>();
//		privileges.add(null);
//		data = new FCActivityType(null, code, new Date(), references,
//				privileges, null, facilityRef);
//		assert (data.isWellFormed());
//
//		privileges = new HashSet<String>();
//		privileges.add("HP");
//		data = new FCActivityType(null, code, new Date(), references,
//				privileges, null, facilityRef);
//		assert (data.isWellFormed());
//
//		Set<AssociationType> facilitySet = new HashSet<AssociationType>();
//		facilitySet.add(facilityRef);
//
//		data = new FCActivityType(null, code, new Date(), references,
//				privileges, facilitySet, facilityRef);
//		assert (data.isWellFormed());
//
//		facilitySet.add(facilityRef);
//		facilitySet
//				.add(new AssociationType(AssociationToClass.SELF.value(), 1L));
//
//		data = new FCActivityType(null, code, new Date(), references,
//				privileges, facilitySet, facilityRef);
//		assert (data.isWellFormed());
//	}
//
//	@Test
//	public void isValid() {
//		FCActivityType data = new FCActivityType();
//		assert (!data.isValid());
//
//		data = new FCActivityType(null, null, null, null, null, null,null);
//		assert (!data.isValid());
//
//		CodeType code = new CodeType(ReferenceSet.COUNT_TYPE.value(), "V");
//
//		data = new FCActivityType(null, code, null, null, null, null,null);
//		assert (!data.isValid());
//
//		Set<AssociationType> references = new HashSet<AssociationType>();
//		data = new FCActivityType(null, code, null, references, null, null,null);
//		assert (!data.isValid());
//
//		references = new HashSet<AssociationType>();
//		AssociationType facilityRef = new AssociationType();
//		references.add(facilityRef);
//		data = new FCActivityType(null, code, new Date(), references, null,
//				null, facilityRef);
//		assert (!data.isValid());
//
//		references = new HashSet<AssociationType>();
//		facilityRef = new AssociationType(AssociationToClass.FACILITY.value(),
//				1L);
//		AssociationType activityRef = new AssociationType(
//				AssociationToClass.ACTIVITY.value(), 2L);
//		
//		references.add(activityRef);
//		data = new FCActivityType(null, code, new Date(), references, null,
//				null, facilityRef);
//		assert (data.isValid());
//
//		references = new HashSet<AssociationType>();
//		activityRef= new AssociationType(AssociationToClass.FACILITY.value(),
//				1L);
//		references.add(activityRef);
//		data = new FCActivityType(null, code, new Date(), references, null,
//				null, facilityRef);
//		assert (data.isValid());
//
//		Set<String> privileges = new HashSet<String>();
//		data = new FCActivityType(null, code, new Date(), references,
//				privileges, null, facilityRef);
//		assert (data.isValid());
//
//		privileges = new HashSet<String>();
//		privileges.add(new String());
//		data = new FCActivityType(null, code, new Date(), references,
//				privileges, null, facilityRef);
//		assert (!data.isValid());
//
//		privileges = new HashSet<String>();
//		privileges.add(null);
//		data = new FCActivityType(null, code, new Date(), references,
//				privileges, null, facilityRef);
//		assert (!data.isValid());
//
//		privileges = new HashSet<String>();
//		privileges.add("HP");
//		data = new FCActivityType(null, code, new Date(), references,
//				privileges, null, facilityRef);
//		assert (data.isValid());
//
//		Set<AssociationType> facilitySet = new HashSet<AssociationType>();
//		facilitySet.add(facilityRef);
//
//		data = new FCActivityType(null, code, new Date(), references,
//				privileges, facilitySet, facilityRef);
//		assert (data.isValid());
//
//		facilitySet.add(facilityRef);
//		facilitySet
//				.add(new AssociationType(AssociationToClass.SELF.value(), 1L));
//
//		data = new FCActivityType(null, code, new Date(), references,
//				privileges, facilitySet, facilityRef);
//		assert (data.isValid());
//	}
//}
