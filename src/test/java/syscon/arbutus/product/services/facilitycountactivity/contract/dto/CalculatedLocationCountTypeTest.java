//package syscon.arbutus.product.services.facilitycountactivity.contract.dto;
//
//import org.testng.annotations.Test;
//
//import syscon.arbutus.product.services.core.contract.dto.AssociationType;
//
//public class CalculatedLocationCountTypeTest {
//
//	@Test
//	public void isWellFormed() {
//
//		CalculatedLocationCountType data = new CalculatedLocationCountType();
//		assert (!data.isWellFormed());
//
//		data = new CalculatedLocationCountType(null, null, null);
//		assert (!data.isWellFormed());
//		
//		AssociationType type = new AssociationType();
//		type.setToClass(AssociationToClass.FACILITY_INTERNAL_LOCATION.value());
//		type.setToIdentifier(1L);
//		data = new CalculatedLocationCountType(type, null, null);
//		assert (!data.isWellFormed());
//		
//		type.setToIdentifier(1L);
//		data = new CalculatedLocationCountType(type, 5L, null);
//		assert (!data.isWellFormed());
//
//		type.setToIdentifier(1L);
//		data = new CalculatedLocationCountType(type, 5L, 5L);
//		assert (data.isWellFormed());
//
//		type.setToIdentifier(1L);
//		data = new CalculatedLocationCountType(type, -5L, 5L);
//		assert (data.isWellFormed());
//
//		type.setToIdentifier(1L);
//		data = new CalculatedLocationCountType(type, 5L, -5L);
//		assert (data.isWellFormed());
//	}
//
//	@Test
//	public void isValid() {
//
//		CalculatedLocationCountType data = new CalculatedLocationCountType();
//		assert (!data.isValid());
//
//		data = new CalculatedLocationCountType(null, null, null);
//		assert (!data.isValid());
//		AssociationType type1 = new AssociationType();
//		type1.setToClass(AssociationToClass.FACILITY_INTERNAL_LOCATION.value());
//		type1.setToIdentifier(1L);
//		
//		data = new CalculatedLocationCountType(type1, null, null);
//		assert (!data.isValid());
//		
//		type1.setToIdentifier(1L);
//		data = new CalculatedLocationCountType(type1, 5L, null);
//		assert (!data.isValid());
//
//		type1.setToIdentifier(1L);
//		data = new CalculatedLocationCountType(type1, 5L, 5L);
//		assert (data.isValid());
//		
//		type1.setToIdentifier(1L);
//		data = new CalculatedLocationCountType(type1, -5L, 5L);
//		assert (!data.isValid());
//		
//		type1.setToIdentifier(1L);
//		data = new CalculatedLocationCountType(type1, 5L, -5L);
//		assert (!data.isValid());
//	}
//}
