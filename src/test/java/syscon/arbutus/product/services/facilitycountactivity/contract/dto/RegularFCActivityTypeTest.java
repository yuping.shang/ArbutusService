//package syscon.arbutus.product.services.facilitycountactivity.contract.dto;
//
//import java.util.Date;
//import java.util.HashSet;
//import java.util.Set;
//
//import org.testng.annotations.Test;
//
//import syscon.arbutus.product.services.core.contract.dto.AssociationType;
//import syscon.arbutus.product.services.core.contract.dto.CodeType;
//import syscon.arbutus.product.services.realization.util.BeanHelper;
//
//public class RegularFCActivityTypeTest {
//
//	@Test
//	public void isWellFormed() {
//		RegularFCActivityType data = new RegularFCActivityType();
//		assert (!data.isWellFormed());
//
//		data = new RegularFCActivityType(null, null, null, null, null, null,
//				null, null, null, null, null, null, null, null, null, null,null);
//		assert (!data.isWellFormed());
//
//		CodeType code = new CodeType(ReferenceSet.COUNT_TYPE.value(), "V");
//		Date countDate = BeanHelper.createDate();
//
//		// ConsolidatedLocationCountType set is null
//		Set<AssociationType> references = new HashSet<AssociationType>();
//		AssociationType facilityRef = new AssociationType(
//				AssociationToClass.FACILITY.value(), 1L);
//		AssociationType activityRef = new AssociationType(
//				AssociationToClass.ACTIVITY.value(), 1L);
//		references.add(activityRef);
//		Set<String> privileges = new HashSet<String>();
//		privileges = new HashSet<String>();
//		privileges.add("HP");
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null, facilityRef,null, null, null, null, null, null, null,
//				null, null, null);
//		assert (!data.isWellFormed());
//		AssociationType internalLoc = new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(),1L);
//		// countStatus is null
//		Set<ConsolidatedLocationCountType> consolidatedLocationCounts = new HashSet<ConsolidatedLocationCountType>();
//		ConsolidatedLocationCountType locationCount = new ConsolidatedLocationCountType(
//				internalLoc, null, null, null, null, null, null, null, null, null, null,
//				true, 1L, null);
//		consolidatedLocationCounts.add(locationCount);
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, null, consolidatedLocationCounts, null, null,
//				null, null, null, null, null, null);
//		assert (!data.isWellFormed());
//
//		CodeType status = new CodeType(ReferenceSet.COUNT_STATUS.value(), "V");
//
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, null, consolidatedLocationCounts, null, null,
//				null, null, status, null, null, null);
//		assert (!data.isWellFormed());
//
//		// countAttempt is null
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, null, consolidatedLocationCounts, null, null,
//				null, null, status, 2L, null, null);
//		assert (!data.isWellFormed());
//
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, null, consolidatedLocationCounts, null, null,
//				null, null, status, 2L, 1L, null);
//		assert (data.isWellFormed());
//
//		// consolidatedLocationCounts contains null, should return true
//		consolidatedLocationCounts = new HashSet<ConsolidatedLocationCountType>();
//		locationCount = new ConsolidatedLocationCountType(internalLoc, null, null, null,
//				null, null, null, null, null, null, null, true, 1L, null);
//		consolidatedLocationCounts.add(locationCount);
//		consolidatedLocationCounts.add(null);
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, null, consolidatedLocationCounts, null, null,
//				null, null, status, 2L, 1L, null);
//		assert (data.isWellFormed());
//
//		consolidatedLocationCounts = new HashSet<ConsolidatedLocationCountType>();
//		locationCount = new ConsolidatedLocationCountType(internalLoc, null, null, null,
//				null, null, null, null, null, null, null, true, 1L, null);
//		consolidatedLocationCounts.add(locationCount);
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, null, consolidatedLocationCounts, null, null,
//				null, null, status, 2L, 1L, null);
//		assert (data.isWellFormed());
//
//		// Check for CalculatedFacilityCountType
//		CalculatedFacilityCountType facilityCount = new CalculatedFacilityCountType();
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, facilityCount, consolidatedLocationCounts,
//				null, null, null, null, status, 2L, 1L, null);
//		assert (!data.isWellFormed());
//
//		facilityCount = new CalculatedFacilityCountType(1L, 2L, 3L);
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, facilityCount, consolidatedLocationCounts,
//				null, null, null, null, status, 2L, 1L, null);
//		assert (data.isWellFormed());
//
//	}
//
//	@Test
//	public void isValid() {
//		RegularFCActivityType data = new RegularFCActivityType();
//		assert (!data.isValid());
//
//		data = new RegularFCActivityType(null, null, null, null, null, null,
//				null, null, null,null, null, null, null, null, null, null, null);
//		assert (!data.isValid());
//
//		CodeType code = new CodeType(ReferenceSet.COUNT_TYPE.value(), "V");
//		Date countDate = BeanHelper.createDate();
//
//		// ConsolidatedLocationCountType set is null
//		Set<AssociationType> references = new HashSet<AssociationType>();
//		AssociationType facilityRef = new AssociationType(
//				AssociationToClass.FACILITY.value(), 1L);
//		AssociationType activityRef = new AssociationType(
//				AssociationToClass.ACTIVITY.value(), 1L);
//		references.add(activityRef);
//		Set<String> privileges = new HashSet<String>();
//		privileges = new HashSet<String>();
//		privileges.add("HP");
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, null, null, null, null, null, null, null,
//				null, null, null);
//		assert (!data.isValid());
//		AssociationType internalLoc = new AssociationType(AssociationToClass.FACILITY_INTERNAL_LOCATION.value(),1L);
//		// countStatus is null
//		Set<ConsolidatedLocationCountType> consolidatedLocationCounts = new HashSet<ConsolidatedLocationCountType>();
//		ConsolidatedLocationCountType locationCount = new ConsolidatedLocationCountType(
//				internalLoc, null, null, null, null, null, null, null, null, null, null,
//				true, 1L, null);
//		consolidatedLocationCounts.add(locationCount);
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, null, consolidatedLocationCounts, null, null,
//				null, null, null, null, null, null);
//		assert (!data.isValid());
//
//		CodeType status = new CodeType(ReferenceSet.COUNT_STATUS.value(), "V");
//
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, null, consolidatedLocationCounts, null, null,
//				null, null, status, null, null, null);
//		assert (!data.isValid());
//
//		// countAttempt is null
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, null, consolidatedLocationCounts, null, null,
//				null, null, status, 2L, null, null);
//		assert (!data.isValid());
//
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, null, consolidatedLocationCounts, null, null,
//				null, null, status, 2L, 1L, null);
//		assert (data.isValid());
//
//		// consolidatedLocationCounts contains null, should return true
//		consolidatedLocationCounts = new HashSet<ConsolidatedLocationCountType>();
//		locationCount = new ConsolidatedLocationCountType(internalLoc, null, null, null,
//				null, null, null, null, null, null, null, true, 1L, null);
//		consolidatedLocationCounts.add(locationCount);
//		consolidatedLocationCounts.add(null);
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, null, consolidatedLocationCounts, null, null,
//				null, null, status, 2L, 1L, null);
//		assert (!data.isValid());
//
//		consolidatedLocationCounts = new HashSet<ConsolidatedLocationCountType>();
//		locationCount = new ConsolidatedLocationCountType(internalLoc, null, null, null,
//				null, null, null, null, null, null, null, true, 1L, null);
//		consolidatedLocationCounts.add(locationCount);
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, null, consolidatedLocationCounts, null, null,
//				null, null, status, 2L, 1L, null);
//		assert (data.isValid());
//
//		// Check for CalculatedFacilityCountType
//		CalculatedFacilityCountType facilityCount = new CalculatedFacilityCountType();
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, facilityCount, consolidatedLocationCounts,
//				null, null, null, null, status, 2L, 1L, null);
//		assert (!data.isValid());
//
//		facilityCount = new CalculatedFacilityCountType(1L, 2L, 3L);
//		data = new RegularFCActivityType(null, code, countDate, references,
//				privileges, null,facilityRef, facilityCount, consolidatedLocationCounts,
//				null, null, null, null, status, 2L, 1L, null);
//		assert (data.isValid());
//
//	}
//}
