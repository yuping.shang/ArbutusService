package syscon.arbutus.product.services.facilitycountactivity.contract.dto;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.contract.dto.DescriptionType;

public class DescriptionTypeTest {

    @Test
    public void isValid() {
        // Wrong languageCode
        DescriptionType data = new DescriptionType("cn", "Desc");
        assert (!data.isValid());

        // Wrong languageCode size
        data = new DescriptionType("01234567890123456789012345678901234567890123456789012345678901234", "Desc");
        assert (!data.isValid());

        // Wrong description size
        data = new DescriptionType("en",
                "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
        assert (!data.isValid());

        // Ignore case for languageCode
        data = new DescriptionType("EN", "Desc");
        assert (data.isValid());

        data = new DescriptionType("en", "Desc");
        assert (data.isValid());
    }

    @Test
    public void isWellFormed() {
        DescriptionType data = new DescriptionType();
        assert (!data.isWellFormed());

        data = new DescriptionType(null, null);
        assert (!data.isWellFormed());

        data = new DescriptionType("", null);
        assert (!data.isWellFormed());

        data = new DescriptionType("en", null);
        assert (!data.isWellFormed());

        data = new DescriptionType("en", "");
        assert (!data.isWellFormed());

        data = new DescriptionType("en", "Desc");
        assert (data.isWellFormed());

    }
}
