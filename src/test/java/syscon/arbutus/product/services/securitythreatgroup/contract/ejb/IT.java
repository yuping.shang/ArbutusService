/**
 * 2013 by Syscon Justice Systems. All rights reserved.
 * Copying and distribution, any modifications or adaptation of the contents, or creation of derivative
 * works are not allowed without the express and prior written consent of Syscon Justice Systems.
 */
package syscon.arbutus.product.services.securitythreatgroup.contract.ejb;

import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.naming.NamingException;

import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.person.contract.dto.PersonType;
import syscon.arbutus.product.services.person.contract.dto.StaffPosition;
import syscon.arbutus.product.services.person.contract.dto.StaffType;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.person.contract.test.ContractTestUtil;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.Affiliation;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.AffiliationSearch;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.STGIdentifier;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.STGIdentifierImage;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.STGIdentifierSearch;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.STGIdentifyingWords;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.STGRelationship;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.STGRelationshipSearch;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.STGRelationshipTypeEnum;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.STGSearchTypeEnum;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.SecurityThreatGroup;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.SecurityThreatGroupHierarchy;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.ValidateAffiliation;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.ValidateSecurityThreatGroup;
import syscon.arbutus.product.services.securitythreatgroup.contract.interfaces.SecurityThreatGroupService;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

public class IT extends BaseIT {
    private static Logger log = LoggerFactory.getLogger(IT.class);
    protected UserContext uc = null;
    // shared data
    Object tip;
    private SecurityThreatGroupService service;
    private PersonService personService;
    private FacilityService facService;
    private SupervisionService supService;
    private Long facilityId;
    private Long staffId;
    private Long personId;
    private Long personIdentityId;
    private SecurityThreatGroup securityThreatGroupType;
    private SecurityThreatGroup securityThreatGroupTypeChild;
    private STGRelationship stgRelationshipType;
    private STGIdentifier stgIdentifierType;
    private STGIdentifyingWords stgIdentifyingWordsType;

    @BeforeClass
    public void beforeTest() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");
        uc = super.initUserContext();
        facService = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        service = (SecurityThreatGroupService) JNDILookUp(this.getClass(), SecurityThreatGroupService.class);
        personService = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        supService = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        facilityId = createFacility();
        personId = createPerson();
        personIdentityId = createPersonIdenity();
        staffId = createStaff();

    }

    private void clientlogin(String user, String password) {
        try {
            SecurityClient client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password);
            client.login();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    @Test
    public void testCreateSTG() {
        //        clientlogin("devtest", "devtest");
        //        SecurityThreatGroup retParent = new SecurityThreatGroup();
        //        SecurityThreatGroup retChild = new SecurityThreatGroup();
        //        SecurityThreatGroup parentSecurityThreatGroupType = getSecurityThreatGroupTypeInstance();
        //        retParent = service.createSecurityThreatGroup(uc, parentSecurityThreatGroupType);
        //        assert (retParent != null);
        //        SecurityThreatGroup childSecurityThreatGroupType = getSecurityThreatGroupTypeInstance();
        //        childSecurityThreatGroupType.setParentId(retParent.getId());
        //        retChild = service.createOrUpdateSTG(uc, childSecurityThreatGroupType);
        //        assert (retChild != null);
        //        retParent.setCode("code has been edited");
        //        SecurityThreatGroup retUpdated = service.createOrUpdateSTG(uc, retParent);
        //        assert (retUpdated != null);
        //        securityThreatGroupType = retParent;
        //        securityThreatGroupTypeChild = retChild;

    }

    @Test
    public void testSearchSTG() {
        //        clientlogin("devtest", "devtest");
        //        SecurityThreatGroup ret = new SecurityThreatGroup();
        //        SecurityThreatGroup securityThreatGroupType = getSecurityThreatGroupTypeInstance();
        //        ret = service.createOrUpdateSTG(uc, securityThreatGroupType);
        //
        //        assert (ret != null);
        //        assert (ret.getId() > 0);
        //        List<SecurityThreatGroup> list = new ArrayList<SecurityThreatGroup>();
        //
        //        SecurityThreatGroupSearch search = new SecurityThreatGroupSearch();
        //        search.setIsActive(Boolean.TRUE);
        //        search.setOnlyParent(Boolean.FALSE);
        //        list = service.searchSTG(uc, search, null, null, null);
        //        assert (list != null);
    }

    @Test
    public void testCreateOrUpdateAffiliation() {
        //        clientlogin("devtest", "devtest");
        //        Affiliation retAffiliationType = new Affiliation();
        //        SecurityThreatGroup retSTG = new SecurityThreatGroup();
        //        SecurityThreatGroup securityThreatGroupType = getSecurityThreatGroupTypeInstance();
        //        retSTG = service.createOrUpdateSTG(uc, securityThreatGroupType);
        //        assert (retSTG != null);
        //        Affiliation affiliationType = getAffiliationTypeInstance();
        //        affiliationType.setSTGId(retSTG.getId());
        //        retAffiliationType = service.createOrUpdateAffiliation(uc, affiliationType);
        //        assert (retAffiliationType != null);
    }

    @Test
    public void testSearchAffiliation() {
        clientlogin("devtest", "devtest");

        Affiliation retAffiliationType = new Affiliation();
        SecurityThreatGroup retSTG = new SecurityThreatGroup();
        SecurityThreatGroup securityThreatGroupType = getSecurityThreatGroupTypeInstance();
        //retSTG = service.createOrUpdateSTG(uc, securityThreatGroupType);  -- TODO
        assert (retSTG != null);
        Affiliation affiliationType = getAffiliationTypeInstance();
        affiliationType.setSTGId(retSTG.getId());

        Long affiliationId = service.createAffiliation(uc, affiliationType);
        assert (affiliationId != null);
        assert (retSTG != null);
        assert (retSTG.getId() > 0);
        List<Affiliation> list = new ArrayList<Affiliation>();

        AffiliationSearch search = new AffiliationSearch();
        search.setSTGId(retSTG.getId());
        list = service.searchAffiliation(uc, search, null, null, null);
        assert (list != null);
    }

    @Test
    public void testEditAffiliation() {
        clientlogin("devtest", "devtest");
        Affiliation retAffiliationType = new Affiliation();
        Affiliation retUpdatedAffiliationType = new Affiliation();
        SecurityThreatGroup retSTG = new SecurityThreatGroup();
        SecurityThreatGroup securityThreatGroupType = getSecurityThreatGroupTypeInstance();
        //retSTG = service.createOrUpdateSTG(uc, securityThreatGroupType); --TODO
        assert (retSTG != null);
        Affiliation affiliationType = getAffiliationTypeInstance();
        affiliationType.setSTGId(retSTG.getId());
        Long affiliationId = service.createAffiliation(uc, affiliationType);
        assert (affiliationId != null);
        retAffiliationType.setAffiliationComment("Updated affiliatin Comment");
        retAffiliationType.setNotificationComment("Updated Notification Comment");
        service.updateAffiliation(uc, retAffiliationType);
    }

    private SecurityThreatGroup getSecurityThreatGroupTypeInstance() {
        SecurityThreatGroup securityThreatGroupType = new SecurityThreatGroup();
        Date effectiveDate = new Date();
        Date expiryDate = getNextDay(new Date());
        securityThreatGroupType.setCode("code");
        securityThreatGroupType.setDescription("description");
        securityThreatGroupType.setEffectiveDate(effectiveDate);
        securityThreatGroupType.setExpiryDate(expiryDate);
        securityThreatGroupType.setMembers("5");
        securityThreatGroupType.setStatus(Boolean.TRUE);
        securityThreatGroupType.setValidationStatus("Not-Validated");
        securityThreatGroupType.setStaff(staffId);
        securityThreatGroupType.setParentId(null);
        securityThreatGroupType.setDeactivationReasonCode("reasoncode");
        return securityThreatGroupType;
    }

    private Date getNextDay(Date date) {

        if (null == date) {
            return date;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, 1);
        return cal.getTime();
    }

    private Long createStaff() {
        StaffType ret = null;
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CPT"));
        staffPositions.add(new StaffPosition("DEP"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 30);
        String category = new String("CON");
        String reason = new String("SICK");

        StaffType staff = new StaffType(null, personIdentityId, category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(personId);
        //
        ret = personService.getStaff(uc, personService.createStaff(uc, staff));
        assert (ret != null);
        log.info(String.format("ret = %s", ret));
        return ret.getStaffID();

    }

    private Long createPerson() {

        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        instanceForTest.setActiveStatus("ACTIVE"); // new
        // CodeType(ReferenceSet.PERSONSTATUS.name(),
        instanceForTest = personService.createPersonType(uc, instanceForTest);
        return instanceForTest.getPersonIdentification();
    }

    // Helper Method
    private Long createPersonIdenity() {
        Long lng = new Random().nextLong();
        Date dob = new Date(1985 - 1900, 3, 10);
        PersonIdentityType peter = new PersonIdentityType(null, personId, null, null, "peter" + lng, "S" + lng, "S" + lng, "Pan" + lng, "i", dob, "M", null, null,
                "offenderNumTest" + lng, null, null);
        // PersonIdentityType peter = createIndentity("Peter", "Pan", new
        // Date(84, 3, 10), "M");
        peter = personService.createPersonIdentityType(uc, peter, Boolean.FALSE, Boolean.FALSE);
        return peter.getPersonIdentityId();
    }

    private Long createFacility() {
        Facility ret = null;
        String category = new String("CUSTODY");

        Facility facility = new Facility(-1L, "FacilityCodeIT" + new Random().nextLong(), "FacilityName", new Date(), category, null, null, null, null, false);
        ret = facService.create(uc, facility);
        assertNotEquals(ret, null);
        return ret.getFacilityIdentification();
    }

    private Affiliation getAffiliationTypeInstance() {
        Affiliation affiliationType = new Affiliation();
        Date affiliationDate = new Date();
        Date notificationDate = getNextDay(affiliationDate);
        Date appealDate = getNextDay(notificationDate);
        Date expiryDate = getNextDay(affiliationDate);
        affiliationType.setAffiliationDate(affiliationDate);
        affiliationType.setAffiliationReason("affiliationReason");
        affiliationType.setAffiliationComment("affiliationComment");
        affiliationType.setAffiliatedBystaff(staffId);
        affiliationType.setPersonId(personId);
        affiliationType.setNotificationDate(notificationDate);
        affiliationType.setNotificationComment("notificationComment");
        affiliationType.setNotifiedBystaff(staffId);
        affiliationType.setAppealDate(appealDate);
        affiliationType.setStatus(Boolean.TRUE);
        affiliationType.setExpiryDate(expiryDate);
        affiliationType.setExpiryReason("expiryReason");
        affiliationType.setExpiredBy(staffId);
        return affiliationType;
    }

    @Test
    public void testCreateOrUpdateValidateSTG() {
        clientlogin("devtest", "devtest");
        ValidateSecurityThreatGroup threatGroup = new ValidateSecurityThreatGroup();
        threatGroup = getValidateSecurityThreatGroupTypeInstance();
        threatGroup = service.createValidateSTG(uc, threatGroup);
        assert (threatGroup != null);

    }

    private ValidateSecurityThreatGroup getValidateSecurityThreatGroupTypeInstance() {
        ValidateSecurityThreatGroup validateSecurityThreatGroupType = new ValidateSecurityThreatGroup();
        validateSecurityThreatGroupType.setsTGId(2L);
        validateSecurityThreatGroupType.setDesignation("DISRUPTIVEGROUP");
        validateSecurityThreatGroupType.setReason("TOBEDETERMINED");
        validateSecurityThreatGroupType.setValidatedBy(1L);
        validateSecurityThreatGroupType.setValidationStatus("MONITORING");
        validateSecurityThreatGroupType.setCreatedDate(new Date().toString());
        validateSecurityThreatGroupType.setCreatedUserId("1");
        validateSecurityThreatGroupType.setCreatedUserName("Marco Brown");
        return validateSecurityThreatGroupType;
    }

    @Test
    public void testGetGangValidationDetail() {
        clientlogin("devtest", "devtest");
        List<ValidateSecurityThreatGroup> threatGroup = new ArrayList<ValidateSecurityThreatGroup>();
        threatGroup = service.getGangValidationDetail(uc, 2L);
        assert (threatGroup != null);
        assert (!threatGroup.isEmpty());
        assert (threatGroup.size() > 0);

    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testGetAllCCFMetaForGangs() {
        //        clientlogin("devtest", "devtest");
        //        List<ClientCustomFieldMetaType> ret = new ArrayList<ClientCustomFieldMetaType>();
        //        String eventType = "GANGS";
        //        ret = service.getAllCCFMetaForGangs(uc, eventType);
        //        assert (ret != null);
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testSaveCCFMetaValueForGangs() {
        //        clientlogin("devtest", "devtest");
        //        String type = "GANGS";
        //        List<ClientCustomFieldMetaType> metaTypes = service.getAllCCFMetaForGangs(uc, type);
        //        assert (metaTypes != null);
        //        if (metaTypes != null && !metaTypes.isEmpty()) {
        //            for (ClientCustomFieldMetaType meta : metaTypes) {
        //                ClientCustomFieldValueType valueType = new ClientCustomFieldValueType();
        //                valueType.setCcfId(meta.getCcfId());
        //                valueType.setDtoBinding(meta.getDtoBinding());
        //                valueType.setIdBinding(1L);
        //                valueType.setName(meta.getName());
        //                valueType.setMetaType(meta);
        //                valueType.setValue("ABC");
        //                ClientCustomFieldValueType retValue = service.saveCCFMetaValueForGangs(uc, valueType);
        //                assert (retValue != null);
        //            }
        //        }
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testSaveCCFMetaValuesForGangs() {
        //        clientlogin("devtest", "devtest");
        //        String type = "GANGS";
        //        List<ClientCustomFieldMetaType> metaTypes = service.getAllCCFMetaForGangs(uc, type);
        //        List<ClientCustomFieldValueType> values = new ArrayList<ClientCustomFieldValueType>();
        //        assert (metaTypes != null);
        //        if (metaTypes != null && !metaTypes.isEmpty()) {
        //            for (ClientCustomFieldMetaType meta : metaTypes) {
        //                ClientCustomFieldValueType valueType = new ClientCustomFieldValueType();
        //                valueType.setCcfId(meta.getCcfId());
        //                valueType.setDtoBinding(meta.getDtoBinding());
        //                valueType.setIdBinding(1L);
        //                valueType.setName(meta.getName());
        //                valueType.setMetaType(meta);
        //                valueType.setValue("ABC");
        //                values.add(valueType);
        //                List<ClientCustomFieldValueType> retValues = service.saveCCFMetaValuesForGangs(uc, values);
        //                assert (retValues != null && retValues.size() > 0);
        //            }
        //        }
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testUpdateCCFMetaValues() {
        //        clientlogin("devtest", "devtest");
        //        String type = "GANGS";
        //        List<ClientCustomFieldMetaType> metaTypes = service.getAllCCFMetaForGangs(uc, type);
        //        List<ClientCustomFieldValueType> values = new ArrayList<ClientCustomFieldValueType>();
        //        List<ClientCustomFieldValueType> retValues = new ArrayList<ClientCustomFieldValueType>();
        //        assert (metaTypes != null);
        //        if (metaTypes != null && !metaTypes.isEmpty()) {
        //            for (ClientCustomFieldMetaType meta : metaTypes) {
        //                ClientCustomFieldValueType valueType = new ClientCustomFieldValueType();
        //                valueType.setCcfId(meta.getCcfId());
        //                valueType.setDtoBinding(meta.getDtoBinding());
        //                valueType.setIdBinding(1L);
        //                valueType.setName(meta.getName());
        //                valueType.setMetaType(meta);
        //                valueType.setValue("ABC");
        //                values.add(valueType);
        //
        //            }
        //            retValues = service.saveCCFMetaValuesForGangs(uc, values);
        //            assert (retValues != null && retValues.size() > 0);
        //
        //            retValues = service.updateCCFMetaValues(uc, retValues);
        //            assert (retValues != null);
        //        }

    }

    @Test(dependsOnMethods = { "testSaveCCFMetaValuesForGangs" })
    public void testGetCCFMetaValuesForGangs() {
        //        clientlogin("devtest", "devtest");
        //        String type = "GANGS";
        //
        //        ClientCustomFieldValueSearchType searchType = new ClientCustomFieldValueSearchType();
        //        List<ClientCustomFieldMetaType> metaTypes = service.getAllCCFMetaForGangs(uc, type);
        //        for (ClientCustomFieldMetaType meta : metaTypes) {
        //            searchType.setCCFMetaName(meta.getName());
        //            break;
        //        }
        //
        //        searchType.setValue("ABC");
        //
        //        List<ClientCustomFieldValueType> ccfValues = service.getCCFMetaValuesForGangs(uc, searchType);
        //        assert (ccfValues != null);

    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testValidateAffiliation() {
        clientlogin("devtest", "devtest");
        Affiliation retAffiliationType = new Affiliation();
        SecurityThreatGroup retSTG = new SecurityThreatGroup();
        SecurityThreatGroup securityThreatGroupType = getSecurityThreatGroupTypeInstance();
        // retSTG = service.createOrUpdateSTG(uc, securityThreatGroupType); -- TODO
        assert (retSTG != null);
        Affiliation affiliationType = getAffiliationTypeInstance();
        affiliationType.setSTGId(retSTG.getId());

        Long affiliationId = service.createAffiliation(uc, affiliationType);
        assert (affiliationId != null);
        ValidateAffiliation validateAffiliationType = getValidateAffiTypeInstance();
        validateAffiliationType.setAffiliation(retAffiliationType);
        Long id = service.saveValidateAffiliation(uc, validateAffiliationType);
        assert (id != null);
    }

    private ValidateAffiliation getValidateAffiTypeInstance() {
        ValidateAffiliation validateAffiliationType = new ValidateAffiliation();
        Date affiliationDate = new Date();
        Date notificationDate = getNextDay(affiliationDate);
        Date appealDate = getNextDay(notificationDate);
        Date validationDate = getNextDay(appealDate);
        validateAffiliationType.setNotificationDate(notificationDate);
        validateAffiliationType.setAppealDate(appealDate);
        validateAffiliationType.setAction("Validated");
        validateAffiliationType.setValidationReason("Assessment Score");
        validateAffiliationType.setValidationDate(validationDate);
        validateAffiliationType.setValidationComment("Validation Comment");
        validateAffiliationType.setNotifiedBy(staffId);
        validateAffiliationType.setValidatedBystaff(staffId);
        return validateAffiliationType;
    }

    @Test
    public void testGetValidationHistyOfAffiation() {
        clientlogin("devtest", "devtest");

        Affiliation retAffiliationType = new Affiliation();
        SecurityThreatGroup retSTG = new SecurityThreatGroup();
        SecurityThreatGroup securityThreatGroupType = getSecurityThreatGroupTypeInstance();
        // retSTG = service.createOrUpdateSTG(uc, securityThreatGroupType); /// --- TODO
        assert (retSTG != null);
        Affiliation affiliationType = getAffiliationTypeInstance();
        affiliationType.setSTGId(retSTG.getId());
        Long affiliationId = service.createAffiliation(uc, affiliationType);
        assert (affiliationId != null);

        ValidateAffiliation validateAffiliationType = getValidateAffiTypeInstance();
        validateAffiliationType.setAffiliation(retAffiliationType);
        Long id = service.saveValidateAffiliation(uc, validateAffiliationType);
        assert (id != null);

        assert (retSTG != null);
        assert (retSTG.getId() > 0);
        List<ValidateAffiliation> list = new ArrayList<ValidateAffiliation>();

        AffiliationSearch search = new AffiliationSearch();
        search.setAffiliationId(retAffiliationType.getId());
        list = service.getValidationHistyOfAffiation(uc, search, null, null, null);
        assert (list != null);
    }

    @Test(dependsOnMethods = { "testCreateSTG" })
    public void testGetSecurityThreatGroup() {
        SecurityThreatGroup stg = service.getSecurityThreatGroup(uc, securityThreatGroupType.getId());
        assert (null != stg);
    }

    @Test(dependsOnMethods = { "testCreateSTG" })
    public void testIsDuplicateSTG() {
        SecurityThreatGroup stg = service.getSecurityThreatGroup(uc, securityThreatGroupType.getId());
        boolean isDuplicate = service.isDuplicateSTG(uc, stg);
        assert (isDuplicate);
    }

    @Test
    public void testCreateOrUpdateSTGHierarchies() {
        //        List<SecurityThreatGroupHierarchy> stgHierarchies = new ArrayList<SecurityThreatGroupHierarchy>();
        //
        //        SecurityThreatGroupHierarchy stgHt = new SecurityThreatGroupHierarchy();
        //        stgHt.setStgHierarchyName("Group");
        //        stgHt.setActive(true);
        //        stgHierarchies.add(stgHt);
        //
        //        stgHt = new SecurityThreatGroupHierarchy();
        //        stgHt.setStgHierarchyName("Gang");
        //        stgHt.setActive(true);
        //        stgHierarchies.add(stgHt);
        //
        //        List<SecurityThreatGroupHierarchy> stgHTList = service.createOsrUpdateSTGHierarchies(uc, stgHierarchies);
        //        assert (null != stgHTList && stgHTList.size() > 0);
    }

    @Test(dependsOnMethods = { "testCreateOrUpdateSTGHierarchies" })
    public void testGetSTGHierarchies() {
        List<SecurityThreatGroupHierarchy> stgHTList = service.getSTGHierarchies(uc);
        assert (null != stgHTList && stgHTList.size() > 0);
    }

    @Test(dependsOnMethods = { "testCreateSTG" })
    public void testCreateRelationship() {
        STGRelationship type = new STGRelationship();

        type.setRelationshipType(STGRelationshipTypeEnum.ENEMY);
        type.setEnteredByStaff(staffId);
        type.setStgId(securityThreatGroupType.getId());
        type.setAssociateSTGId(securityThreatGroupTypeChild.getId());
        type.setReason("Test Reason");
        type.setEffectiveDate(new Date());
        type.setStatus(true);

        stgRelationshipType = service.createRelationship(uc, type);

        assert (null != stgRelationshipType && null != stgRelationshipType.getRelationshipId());

    }

    @Test(dependsOnMethods = { "testCreateRelationship" })
    public void testUpdateRelationship() {
        stgRelationshipType.setReason("It is update reson");
        stgRelationshipType = service.updateRelationship(uc, stgRelationshipType);
        assert (null != stgRelationshipType && null != stgRelationshipType.getRelationshipId());
    }

    @Test(dependsOnMethods = { "testCreateRelationship" })
    public void testGetRelationship() {
        STGRelationship stgRT = service.getRelationship(uc, stgRelationshipType.getRelationshipId());
        assert (null != stgRT && null != stgRT.getRelationshipId());
    }

    @Test(dependsOnMethods = { "testCreateRelationship" })
    public void testSearchRelationship() {
        STGRelationshipSearch relationshipSearchType = new STGRelationshipSearch();
        relationshipSearchType.setRelationshipType(STGRelationshipTypeEnum.ENEMY);
        List<STGRelationship> stgRTList = service.searchRelationship(uc, relationshipSearchType);
        assert (null != stgRTList && stgRTList.size() > 0);
    }

    @Test(dependsOnMethods = { "testCreateRelationship" })
    public void testGetEligibleSTGForRelationshipType() {
        List<SecurityThreatGroup> stgRTList = service.getEligibleSTGForRelationshipType(uc, securityThreatGroupType.getId(), STGRelationshipTypeEnum.ENEMY, null, null,
                null);
        assert (null != stgRTList && stgRTList.size() > 0);

    }

    @Test(dependsOnMethods = { "testCreateSTG" })
    public void testCreateSTGIdentifier() {
        STGIdentifier type = new STGIdentifier();

        type.setStgId(securityThreatGroupType.getId());
        type.setCharacteristicType("TAT");
        type.setEnteredByStaff(staffId);
        type.setDescription("This is description");
        type.setIdentifierDate(new Date());

        Long stgIdentifierId = service.createSTGIdentifier(uc, type);
        assert (null != stgIdentifierId);

    }

    @Test(dependsOnMethods = { "testCreateSTGIdentifier" })
    public void testUpdateSTGIdentifier() {
        stgIdentifierType.setCharacteristicType("LGO");
        service.updateSTGIdentifier(uc, stgIdentifierType);
        assert (null != stgIdentifierType && null != stgIdentifierType.getStgIdentifierId());

    }

    @Test(dependsOnMethods = { "testCreateSTGIdentifier" })
    public void testGetSTGIdentifierById() {
        STGIdentifier type = service.getSTGIdentifierById(uc, stgIdentifierType.getStgIdentifierId());
        assert (null != type && null != type.getStgIdentifierId());

    }

    @Test(dependsOnMethods = { "testCreateSTGIdentifier" })
    public void testGetSTGIdentifiers() {
        List<STGIdentifier> typeList = service.getSTGIdentifiers(uc, stgIdentifierType.getStgId(), null, null, null);
        assert (null != typeList && typeList.size() > 0);

    }

    @Test(dependsOnMethods = { "testCreateSTGIdentifier" })
    public void testdeleteSTGIdentifier() {
        Long stdIdentifierID = service.deleteSTGIdentifier(uc, stgIdentifierType.getStgIdentifierId());
        assert (null != stdIdentifierID);
    }

    @Test(dependsOnMethods = { "testCreateSTG" })
    public void testCreateSTGIdentifyingWord() {
        STGIdentifyingWords type = new STGIdentifyingWords();
        type.setStgId(securityThreatGroupType.getId());
        type.setCode("ABC");
        type.setIdentifyingWordsDate(new Date());
        type.setEnteredByStaff(staffId);
        type.setDescription("TEST");

        stgIdentifyingWordsType = service.createSTGIdentifyingWord(uc, type);

        assert (null != stgIdentifyingWordsType && null != stgIdentifyingWordsType.getStgIdentifyingWordsId());
    }

    @Test(dependsOnMethods = { "testCreateSTGIdentifyingWord" })
    public void testUpdateSTGIdentifyingWord() {
        stgIdentifyingWordsType.setDescription("Test 123");
        stgIdentifyingWordsType = service.updateSTGIdentifyingWord(uc, stgIdentifyingWordsType);

        assert (null != stgIdentifyingWordsType && null != stgIdentifyingWordsType.getStgIdentifyingWordsId());

    }

    @Test(dependsOnMethods = { "testCreateSTGIdentifyingWord" })
    public void testGetSTGIdentifyingWordById() {
        STGIdentifyingWords type = service.getSTGIdentifyingWordById(uc, stgIdentifyingWordsType.getStgIdentifyingWordsId());
        assert (null != type && null != type.getStgIdentifyingWordsId());
    }

    @Test(dependsOnMethods = { "testCreateSTGIdentifyingWord" })
    public void testGetSTGIdentifyingWords() {
        List<STGIdentifyingWords> typeList = service.getSTGIdentifyingWords(uc, securityThreatGroupType.getId(), null, null, null);
        assert (null != typeList);

    }

    @Test(dependsOnMethods = { "testCreateSTGIdentifyingWord" })
    public void testDeleteSTGIdentifyingWord() {
        Long stgIentifierWordId = service.deleteSTGIdentifyingWord(uc, stgIdentifyingWordsType.getStgIdentifyingWordsId());
        assert (null != stgIentifierWordId);
    }

    @Test(dependsOnMethods = { "testCreateSTGIdentifier" })
    public void testSearchSTGIdentifiers() {
        STGIdentifierSearch searchType = new STGIdentifierSearch();
        searchType.setStgSearchType(STGSearchTypeEnum.GROUPCHARACTERISTICS);

        List<STGIdentifier> typeList = service.searchSTGIdentifiers(uc, searchType, null, null, null);
        assert (null != typeList && typeList.size() > 0);
    }

    @Test(dependsOnMethods = { "testCreateSTGIdentifyingWord" })
    public void testSearchSTGIdentifyingWords() {
        STGIdentifierSearch searchType = new STGIdentifierSearch();
        searchType.setStgSearchType(STGSearchTypeEnum.IDENTIFYINGWORDS);
        List<STGIdentifyingWords> typeList = service.searchSTGIdentifyingWords(uc, searchType, null, null, null);
        assert (null != typeList && typeList.size() > 0);

    }

    @Test(dependsOnMethods = { "testCreateSTGIdentifier" })
    public void testSearchSTGIdentifyingWordsAndIdentifiers() {
       /* STGIdentifierSearch searchType = new STGIdentifierSearch();
        searchType.setStgSearchType(STGSearchTypeEnum.GROUPCHARACTERISTICS);

        STGIdentifierSearchReturn type = service.searchSTGIdentifyingWordsAndIdentifiers(uc, searchType, null, null, null);
        assert (null != type && type.getStgIdentifierTypeList().size() > 0);*/

		/*searchType = new STGIdentifierSearchType();
        searchType.setStgSearchType(STGSearchTypeEnum.IDENTIFYINGWORDS);
		type = service.searchSTGIdentifyingWordsAndIdentifiers(uc, searchType, null, null, null);
		assert(null != type && type.getStgIdentifyingWordsTypeList().size()>0);*/

    }

    @Test(dependsOnMethods = { "testCreateSTGIdentifier" })
    public void testCreateIdentifierImage() {
        STGIdentifierImage type = new STGIdentifierImage();

        byte[] ba = { 1, 2, 3, 4, 5 };

        type.setData(ba);
        type.setDefaultImage(true);
        type.setImageDescription("Test Image");
        type.setStaff(staffId);
        type.setThumbnail(null);
        type.setStgIdentifierId(1L);
        type.setUploadedDate(new Date());

        type = service.createIdentifierImage(uc, type);

        assert (null != type && type.getImageId() > 0);
    }

    @Test(dependsOnMethods = { "testCreateSTGIdentifier" })
    public void testCreateIdentifierImages() {
        List<STGIdentifierImage> typeList = new ArrayList<STGIdentifierImage>();
        STGIdentifierImage type = new STGIdentifierImage();

        byte[] ba1 = { 1, 2, 3, 4, 5 };

        type.setData(ba1);
        type.setDefaultImage(true);
        type.setImageDescription("Test Image 1");
        type.setStaff(staffId);
        type.setThumbnail(null);
        type.setStgIdentifierId(1L);
        type.setUploadedDate(new Date());

        typeList.add(type);

        type = new STGIdentifierImage();

        byte[] ba2 = { 6, 7, 8, 9 };

        type.setData(ba2);
        type.setDefaultImage(true);
        type.setImageDescription("Test Image 2");
        type.setStaff(staffId);
        type.setThumbnail(null);
        type.setStgIdentifierId(1L);
        type.setUploadedDate(new Date());

        typeList.add(type);

        List<STGIdentifierImage> returnTypeList = service.createIdentifierImages(uc, typeList);

        assert (null != returnTypeList && returnTypeList.size() > 0);
    }

}
