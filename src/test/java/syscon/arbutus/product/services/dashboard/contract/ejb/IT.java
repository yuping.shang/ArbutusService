package syscon.arbutus.product.services.dashboard.contract.ejb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.dashboard.contract.interfaces.DashboardService;

public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);
    private DashboardService service;
    private Long facilityId;

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (DashboardService) JNDILookUp(this.getClass(), DashboardService.class);
        uc = super.initUserContext();
        facilityId = 1L;

    }

    @BeforeMethod
    public void beforeMethod() {
        SecurityClient client;
        try {
            client = SecurityClientFactory.getSecurityClient();
            client.setSimple("devtest", "devtest"); // nice regular service user
            client.login();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void getPopulationData() {

        //
        String ret = service.getPopulationData(uc, facilityId);
        log.info("getPopulationData : " + ret);
        assert (ret != null);
    }

    @Test
    public void getTotalPopulation() {

        //
        String ret = service.getTotalPopulation(uc, facilityId);
        log.info("getTotalPopulation : " + ret);
        assert (ret != null);
    }

    @Test
    public void getOccupancyDistributionData() {

        //
        String ret = service.getOccupancyDistributionData(uc, facilityId);
        log.info("getOccupancyDistributionData : " + ret);
        assert (ret != null);
    }

}
