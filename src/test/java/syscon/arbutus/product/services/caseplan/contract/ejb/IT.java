/**
 * © 2013 by Syscon Justice Systems. All rights reserved.
 * Copying and distribution, any modifications or adaptation of the contents, or creation of derivative
 * works are not allowed without the express and prior written consent of Syscon Justice Systems.
 */
package syscon.arbutus.product.services.caseplan.contract.ejb;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.kie.api.KieServices;
import org.kie.api.builder.ReleaseId;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.assessment.contract.dto.AssessedNeeds;
import syscon.arbutus.product.services.caseplan.contract.dto.*;
import syscon.arbutus.product.services.caseplan.contract.interfaces.CasePlanService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.common.SupervisionTest;
import syscon.arbutus.product.services.legal.contract.dto.condition.ConditionType;
import syscon.arbutus.product.services.legal.contract.ejb.ConditionIT;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * Test Cases to test CasePlanService
 *
 * @author Amlendu Kumar
 * @version 1.0
 * @since 06-February-2014
 */
public class IT extends BaseIT {
    private static Logger log = LoggerFactory.getLogger(IT.class);
    ConditionType conditionType;
    private CasePlanService service;
    private SupervisionTest supervisionTest;
    private FacilityTest facilityTest;
    private ConditionIT conditionTest;
    private Long supervisionId = 1l;
    private Long facilityId;
    private Long casePlanId = 1l;
    private Long actionPlanId = 1l;

    /**
     * @throws Exception
     */
    @BeforeClass
    public void beforeTest() throws Exception {
        log.info("beforeClass Begin - JNDI lookup");
        service = (CasePlanService) JNDILookUp(this.getClass(), CasePlanService.class);
        uc = super.initUserContext();
        supervisionTest = new SupervisionTest();
        facilityTest = new FacilityTest();
        facilityId = facilityTest.createFacility();
        //facilityId = 1l;
        supervisionId = supervisionTest.createSupervision(facilityId, true);
        //supervisionId = 1l;
        conditionTest = new ConditionIT();
        conditionType = conditionTest.createBAILCondition(uc);
    }

    /**
     *
     */
    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    /**
     * This test case creates Case Plan
     */
    @Test
    public void testCreateCasePlan() {
        CasePlanType casePlanType = new CasePlanType();
        casePlanType.setCasePlanType("CSPL");
        casePlanType.setCaseStatus("ACT");
        //casePlanType.setCommunityCaseOwnerId(1l);
        //casePlanType.setCommunityLocationId(facilityId);
        casePlanType.setCreateTimeStamp(new Date());
        casePlanType.setCustodyLocationId(facilityId);
        casePlanType.setNextReviewDate(new Date());
        casePlanType.setReviewDate(new Date());
        casePlanType.setReviewDuration(30l);
        casePlanType.setReviewPersonId(1l);
        casePlanType.setStaffId(1l);
        casePlanType.setSupervisionId(supervisionId);
        CasePlanType casePlanTypeRet = service.createCasePlan(uc, casePlanType);
        assertNotNull(casePlanTypeRet);
        casePlanId = casePlanTypeRet.getCasePlanId();
    }

    /**
     * This test case creates Case Plan along with a child (Criminogenic Need)
     */
    @Test
    public void testCreateCasePlanWithChild() {
        CasePlanType casePlanType = new CasePlanType();
        casePlanType.setCasePlanType("CSPL");
        casePlanType.setCaseStatus("INACT");
        casePlanType.setCommunityCaseOwnerId(1l);
        casePlanType.setCommunityLocationId(facilityId);
        casePlanType.setCreateTimeStamp(new Date());
        casePlanType.setCustodyLocationId(facilityId);
        casePlanType.setNextReviewDate(new Date());
        casePlanType.setReviewDate(new Date());
        casePlanType.setReviewDuration(30l);
        casePlanType.setReviewPersonId(1l);
        casePlanType.setStaffId(1l);
        casePlanType.setSupervisionId(supervisionId);
        CriminogenicNeedsType criminogenicNeedsType = new CriminogenicNeedsType();
        criminogenicNeedsType.setAssessedRisk("assessedRisk");
        criminogenicNeedsType.setDeactivationDate(new Date());
        criminogenicNeedsType.setEndDate(new Date());
        criminogenicNeedsType.setObjective("CAN");
        criminogenicNeedsType.setStatus("INACT"); //ACT, INACT, REV

        criminogenicNeedsType.setTargetDate(new Date());
        List<CriminogenicNeedsType> children = new ArrayList<CriminogenicNeedsType>();
        children.add(criminogenicNeedsType);
        casePlanType.setCriminogenicNeeds(children);
        service.createCasePlan(uc, casePlanType);
    }

    /**
     *
     */
    @Test
    public void testSearch() {
        CasePlanSearchType criteria = new CasePlanSearchType();
        criteria.setSupervisionId(supervisionId);
        Set<Long> facilitySet = new HashSet<Long>();
        facilitySet.add(facilityId);
        criteria.setFacilitySet(facilitySet);
        CasePlanResultType result = service.search(uc, null, criteria, null, null, null);
        assertEquals(result.getTotalSize().intValue(), 2);
    }

    /**
     * This test case retrieves case plan by case plan ID
     */
    @Test
    public void testGetCasePlan() {
        CasePlanType casePlanType = service.getCasePlan(uc, casePlanId);
        assertNotNull(casePlanType);
    }

    /**
     * This test case retrieves Criminogenic Need by Criminogenic Need ID
     */
    @Test
    public void testGetCriminogenicNeed() {
        CriminogenicNeedsType criminogenicNeedsType = service.getCriminogenicNeed(uc, 1l);
        assertNotNull(criminogenicNeedsType);
    }

    @Test
    public void testCreateActionPlan() {
        ActionPlanType actionPlanType = new ActionPlanType();
        actionPlanType.setAssessedRiskId(2l);
        actionPlanType.setCaseworkSteps("AA");
        actionPlanType.setComments("comments");
        actionPlanType.setEndDate(new Date());
        actionPlanType.setProgram("AAPG");
        actionPlanType.setSequenceNumber(1l);
        actionPlanType.setStartDate(new Date());
        actionPlanType.setStepsOutcome("COM");
        actionPlanType.setStepStatus("ACT");
        ActionPlanType actionPlanTypeRet = service.createActionPlan(uc, actionPlanType);
        assertNotNull(actionPlanTypeRet);
        actionPlanId = actionPlanTypeRet.getActionPlanId();
    }

    @Test
    public void testCreateActionPlan2() {
        ActionPlanType actionPlanType = new ActionPlanType();
        actionPlanType.setConditionId(conditionType.getConditionId());
        actionPlanType.setCaseworkSteps("AA");
        actionPlanType.setComments("comments");
        actionPlanType.setEndDate(new Date());
        actionPlanType.setProgram("AAPG");
        actionPlanType.setSequenceNumber(1l);
        actionPlanType.setStartDate(new Date());
        actionPlanType.setStepsOutcome("COM");
        actionPlanType.setStepStatus("ACT");
        ActionPlanType actionPlanTypeRet = service.createActionPlan(uc, actionPlanType);
        assertNotNull(actionPlanTypeRet);
        actionPlanId = actionPlanTypeRet.getActionPlanId();
    }

    @Test
    public void testReviewCasePlan() {
        CasePlanType casePlanType = service.getCasePlan(uc, casePlanId);
        casePlanType.setReviewDate(new Date());
        casePlanType.setReviewPersonId(2l);
        service.reviewCasePlan(uc, casePlanType);

    }

    @Test
    public void testUpdateCasePlan() {
        CasePlanType casePlanType = service.getCasePlan(uc, casePlanId);
        casePlanType.setCaseStatus("INACT");
        casePlanType.setReviewDate(new Date());
        casePlanType.setReviewPersonId(2l);
        service.updateCasePlan(uc, casePlanType);
    }

    /**
     * This test case creates Case Plan along with a child (Criminogenic Need)
     */
    @Test
    public void testCreateCriminogenicNeeds() {
        CriminogenicNeedsType criminogenicNeedsType = new CriminogenicNeedsType();
        criminogenicNeedsType.setAssessedRisk("assessedRisk");
        criminogenicNeedsType.setDeactivationDate(new Date());
        criminogenicNeedsType.setEndDate(new Date());
        criminogenicNeedsType.setObjective("CAN");
        criminogenicNeedsType.setStatus("INACT"); //ACT, INACT, REV
        criminogenicNeedsType.setTargetDate(new Date());
        criminogenicNeedsType.setCasePlanId(casePlanId);
        service.createCriminogenicNeeds(uc, criminogenicNeedsType);
    }

    /**
     * This test case updates Criminogenic Need
     */
    @Test
    public void testUpdateCriminogenicNeed() {
        CriminogenicNeedsType criminogenicNeedsType = service.getCriminogenicNeed(uc, 2l);
        assertNotNull(criminogenicNeedsType);
        criminogenicNeedsType.setAssessedRisk("Modified");
        CriminogenicNeedsType criminogenicNeedsType1 = service.updateCriminogenicNeeds(uc, criminogenicNeedsType);
        assertEquals("Modified", criminogenicNeedsType1.getAssessedRisk());
    }

    /**
     * This test case update Action Plan
     */
    @Test
    public void testUpdateActionPlan() {
        ActionPlanType actionPlanType = service.getActionPlan(uc, actionPlanId);
        assertNotNull(actionPlanType);
        actionPlanType.setComments("new comments");
        ActionPlanType actionPlanType1 = service.updateActionPlan(uc, actionPlanType);
        assertEquals("new comments", actionPlanType1.getComments());
    }

    /**
     * This test case verify case plan
     */
    @Test
    public void testVerifyCasePlan() {
        CasePlanType casePlanType = service.getCasePlan(uc, casePlanId);
        casePlanType.setCaseStatus("INACT");
        casePlanType.setReviewDate(new Date());
        casePlanType.setReviewPersonId(2l);
        CasePlanType casePlanTypeRet = service.verifyCasePlan(uc, casePlanType);
        assertNotNull(casePlanTypeRet);
    }

    @Test
    public void testSearchIncludingSnapshot() {
        CasePlanSearchType criteria = new CasePlanSearchType();
        criteria.setSupervisionId(supervisionId);
        CasePlanResultType result = service.search(uc, null, criteria, null, null, null);
        assertEquals(result.getTotalSize().intValue(), 3);
    }

    /**
     * This test case verify review period
     */
    @Test
    public void testFindReviewPeriod() {
        CasePlanRuleDrools casePlanRuleDrools = new CasePlanRuleDrools(getKieSession());
        CasePlanRuleType casePlanRuleType = casePlanRuleDrools.findReviewPeriod("CSPL", "UNCLASS", "CLASS", 0l);
        assertNotNull(casePlanRuleType);
    }

    private KieSession getKieSession() {
        final ReleaseId releaseId = KieServices.Factory.get().newReleaseId("syscon.arbutus.product", "caseplan", "LATEST");
        final KieServices ks = KieServices.Factory.get();
        final KieContainer kContainer = ks.newKieContainer(releaseId);
        return kContainer.newKieSession("caseplanKS");
    }

    /**
     * This case test search API with all the criteria.
     */
    @Test
    public void testSearchAllCriteria() {
        CasePlanSearchType criteria = new CasePlanSearchType();
        criteria.setSupervisionId(supervisionId);
        criteria.setCasePlanVersion(1l);
        criteria.setCustodyLocationId(facilityId);
        criteria.setStaffId(1l);
        criteria.setCommunityLocationId(facilityId);
        criteria.setCommunityCaseOwnerId(1l);
        criteria.setCasePlanType("CSPL");
        criteria.setReviewFromDate(new Date());
        criteria.setReviewToDate(new Date());
        criteria.setCreateFromDate(new Date());
        criteria.setCreateToDate(new Date());
        criteria.setProgressStatus("IP");
        criteria.setInmateFirstName("B");
        criteria.setInmateLastName("B");
        criteria.setCaseStatus("IP");
        CasePlanResultType result = service.search(uc, null, criteria, null, null, null);
        assertEquals(result.getTotalSize().intValue(), 0);
    }

    /**
     * This test case will fetch assessed needs
     */
    @Test
    public void testFetchAssessedNeeds() {
        AssessedNeedsDrools assessedNeedsDrools = new AssessedNeedsDrools(getKieSession());
        AssessedNeeds assessedNeeds = new AssessedNeeds();

        assessedNeeds.setAssessmentCategory("SECURITY");
        assessedNeeds.setAssessmentType("CLASS");
        assessedNeeds.setCalculatedScore(10L);
        assessedNeeds.setCasePlanType("CSPL");
        assessedNeeds.setSecurityLevel("GOOD");
        assessedNeeds.setActivationDate(new Date());
        assessedNeeds.setDeactivationDate(new Date());
        assessedNeeds = assessedNeedsDrools.fetchAssessedNeeds(assessedNeeds);
        assertNotNull(assessedNeeds);
    }

    @Test
    public void testCloseCasePlan() {
        CasePlanType casePlanType = service.getCasePlan(uc, 2l);
        assertNotNull(casePlanType);
        CasePlanType casePlanType1 = service.closeCasePlan(uc, casePlanType);
        assertEquals("CLS", casePlanType1.getProgressStatus());
    }

    /**
     * This case test search API with supervision name.
     */
    @Test
    public void testSearchByName() {
        CasePlanSearchType criteria = new CasePlanSearchType();
        criteria.setInmateFirstName("Jason");
        //criteria.setInmateLastName("B");
        CasePlanResultType result = service.search(uc, null, criteria, null, null, null);
        assertEquals(result.getTotalSize().intValue(), 2);
    }
}
