package syscon.arbutus.product.services.conflict.contact.ejb;

import javax.naming.NamingException;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.conflict.contract.dto.NonAssociationType;
import syscon.arbutus.product.services.conflict.contract.interfaces.ConflictService;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class ConflictServiceIT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(ConflictServiceIT.class);

    private ConflictService service;

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (ConflictService) JNDILookUp(this.getClass(), ConflictService.class);
        uc = super.initUserContext();
    }

    @BeforeTest
    public void beforeTest() throws NamingException, Exception {
        BasicConfigurator.configure();
    }

    @AfterClass
    public void afterClass() {
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    /*@Test(enabled = false)
    public void checkNonAssociationConflictForProgramForDateSession() {
        List<NonAssociationType> nonAssociations;
        nonAssociations = service.checkNonAssociationConflictByPattern(uc, 1L, // primarySupervisionId
                1L, // facilityId
                69L, // facilityInternalLocationId
                new LocalDate().withYear(2015).withMonthOfYear(3).withDayOfMonth(18), // sessionDate
                new LocalTime().withHourOfDay(10).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0),  // startTime
                new LocalTime().withHourOfDay(12).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0), // endTime
                NonAssociationType.EventType.PROGRAM,
                new DailyRecurrencePatternType(1L, false, new HashSet<String>(Arrays.asList(new String[] { "WED" }))) // RecurrencePatternType
        );
        assertNotNull(nonAssociations);
        assertEquals(nonAssociations.size(), 0);

        nonAssociations = service.checkNonAssociationConflictByPattern(uc, 2L, // primarySupervisionId
                1L, // facilityId
                69L, // facilityInternalLocationId
                new LocalDate().withYear(2015).withMonthOfYear(3).withDayOfMonth(18), // sessionDate
                new LocalTime().withHourOfDay(11).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0),  // startTime
                new LocalTime().withHourOfDay(13).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0), // endTime
                NonAssociationType.EventType.PROGRAM,
                new DailyRecurrencePatternType(1L, false, new HashSet<String>(Arrays.asList(new String[] { "WED" }))) // RecurrencePatternType
        );
        assertNotNull(nonAssociations);
        assertEquals(nonAssociations.size(), 1);
    }*/

    @Test(enabled = true)
    public void checkNonAssociationConflictForOnceAndDailyProgram() {
        List<NonAssociationType> nonAssociations;
        nonAssociations = service.checkNonAssociationConflict(uc, 1L, // primarySupervisionId
                1L, // facilityId
                69L, // facilityInternalLocationId
                new LocalDate().withYear(2015).withMonthOfYear(3).withDayOfMonth(18), // sessionDate
                new LocalTime().withHourOfDay(10).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0),  // startTime
                new LocalTime().withHourOfDay(12).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0), // endTime
                NonAssociationType.EventType.PROGRAM,
                null // RecurrencePatternType
        );
        assertNotNull(nonAssociations);
        assertEquals(nonAssociations.size(), 0);
    }


    @Test(enabled = true)
    public void checkNonAssociationConflictForHousingMovement() {
        List<NonAssociationType> nonAssociations;
        nonAssociations = service.checkNonAssociationConflictForHousingMovement(uc, 1L, 1L, null, null);
        assertNotNull(nonAssociations);
        assertEquals(nonAssociations.size(), 1);
    }
}
