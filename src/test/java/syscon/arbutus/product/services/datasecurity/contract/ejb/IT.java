package syscon.arbutus.product.services.datasecurity.contract.ejb;

import java.math.BigDecimal;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.PersonIdentityTest;
import syscon.arbutus.product.services.common.StaffTest;
import syscon.arbutus.product.services.common.SupervisionTest;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.datasecurity.contract.dto.*;
import syscon.arbutus.product.services.datasecurity.contract.dto.ReferenceSet.DataSecurityRecordTypeEnum;
import syscon.arbutus.product.services.datasecurity.contract.dto.ReferenceSet.EntityTypeEnum;
import syscon.arbutus.product.services.datasecurity.contract.interfaces.DataSecurityService;
import syscon.arbutus.product.services.facility.contract.util.DateUtil;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType;
import syscon.arbutus.product.services.legal.contract.dto.charge.*;
import syscon.arbutus.product.services.legal.contract.dto.ordersentence.*;
import syscon.arbutus.product.services.legal.contract.ejb.OrderSentenceIT.*;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;
import syscon.arbutus.product.services.person.contract.dto.caution.CautionType;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

import static org.testng.Assert.*;

public class IT extends BaseIT {

    private static final Long DOJL = 1L;
    private static Logger log = LoggerFactory.getLogger(IT.class);
    private static String CODE_V = "90";
    private static String CODE_EMPTY = "MEDICAL";
    Set<Long> caseInfoIds = new HashSet<Long>();
    Set<Long> orderIds = new HashSet<Long>();
    Set<Long> chargeIds = new HashSet<Long>();
    private DataSecurityService dService;
    private PersonIdentityTest piTest;
    private PersonService personService;
    private SupervisionService supervisionService;
    private SupervisionTest supervisionTest;
    private StaffTest staffTest;
    private LegalService lService; // for local test

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        dService = (DataSecurityService) JNDILookUp(this.getClass(), DataSecurityService.class);
        piTest = new PersonIdentityTest();
        supervisionTest = new SupervisionTest();
        uc = super.initUserContext();
        staffTest = new StaffTest();
        personService = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        supervisionService = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        lService = (LegalService) JNDILookUp(this.getClass(), LegalService.class);

        // service.deleteAll(uc);

    }

    @BeforeMethod
    public void beforeMethod() {
        // service.deleteAll(uc);

    }

    @Test
    public void testMergeConflict() {

        Long piId = piTest.createPersonIdentity();
        supervisionTest.createSupervision(piId, DOJL, Boolean.TRUE);
        Long toPersonIdentity = piTest.createPersonIdentity();
        supervisionTest.createSupervision(toPersonIdentity, DOJL, Boolean.TRUE);

        List<MergeConflictType> mergeConflictList = dService.getMergeConflict(uc, piId, toPersonIdentity);

        assertFalse(mergeConflictList.isEmpty());

    }

    @Test
    public void testNoMergeConflict() {

        Long piId = piTest.createPersonIdentity();
        Long toPersonIdentity = piTest.createPersonIdentity();
        supervisionTest.createSupervision(toPersonIdentity, DOJL, Boolean.TRUE);

        List<MergeConflictType> mergeConflictList = dService.getMergeConflict(uc, toPersonIdentity, piId);

        assertTrue(mergeConflictList.isEmpty());

    }

    @Test
    public void testMerge() {
        Long fromPersonIdentity = piTest.createPersonIdentity();
        PersonIdentityType fromPersonIdentityType = personService.getPersonIdentityType(uc, fromPersonIdentity, null);

        piTest.createPersonIdentity(fromPersonIdentityType.getPersonId());
        Date presentDate = BeanHelper.createDate();
        CautionType caution = new CautionType(null, CODE_V, CODE_EMPTY, presentDate, null, null, fromPersonIdentityType.getPersonId(), null);
        personService.createCautionType(uc, caution);

        Long toPersonIdentity = piTest.createPersonIdentity();
        supervisionTest.createSupervision(toPersonIdentity, DOJL, Boolean.TRUE);

        Boolean merged = dService.merge(uc, toPersonIdentity, fromPersonIdentity);
        assertTrue(merged);
    }

    @Test
    public void testMergeWithClosedSupervision() {
        Long fromPersonIdentity = piTest.createPersonIdentity();
        PersonIdentityType fromPersonIdentityType = personService.getPersonIdentityType(uc, fromPersonIdentity, null);
        piTest.createPersonIdentity(fromPersonIdentityType.getPersonId());
        Date presentDate = BeanHelper.createDate();
        CautionType caution = new CautionType(null, CODE_V, CODE_EMPTY, presentDate, null, null, fromPersonIdentityType.getPersonId(), null);
        personService.createCautionType(uc, caution);
        Long fromSupervisionId = supervisionTest.createSupervision(fromPersonIdentity, DOJL, Boolean.TRUE);
        supervisionService.close(uc, fromSupervisionId, new Date());

        Long toPersonIdentity = piTest.createPersonIdentity();
        supervisionTest.createSupervision(toPersonIdentity, DOJL, Boolean.TRUE);

        Boolean merged = dService.merge(uc, toPersonIdentity, fromPersonIdentity);
        assertTrue(merged);
    }

    @Test
    public void testMergeWithStaffRecord() {
        Long fromPersonIdentity = piTest.createPersonIdentity();
        PersonIdentityType fromPersonIdentityType = personService.getPersonIdentityType(uc, fromPersonIdentity, null);
        piTest.createPersonIdentity(fromPersonIdentityType.getPersonId());
        staffTest.createStaff(fromPersonIdentity);

        Long toPersonIdentity = piTest.createPersonIdentity();
        supervisionTest.createSupervision(toPersonIdentity, DOJL, Boolean.TRUE);

        Boolean merged = dService.merge(uc, toPersonIdentity, fromPersonIdentity);
        assertTrue(merged);
    }

    @Test
    public void testMergeLog() {
        Long piId = piTest.createPersonIdentity();
        Long toPersonIdentity = piTest.createPersonIdentity();
        supervisionTest.createSupervision(toPersonIdentity, DOJL, Boolean.TRUE);

        Boolean merged = dService.merge(uc, toPersonIdentity, piId);

        assertTrue(merged);
        List<MergeInfoType> mergeInfoTypeList = dService.getMergeLog(uc, toPersonIdentity);

        assertFalse(mergeInfoTypeList.isEmpty());
    }

    @Test
    public void CRUDRecord() {

        Long piId = piTest.createPersonIdentity();
        PersonIdentityType pi = personService.getPersonIdentityType(uc, piId, null);

        Long entityId = 2L;
        String entityType = ReferenceSet.EntityTypeEnum.CHARGE.code();
        Long parentEntityId = 1L;
        String parentEntityType = ReferenceSet.EntityTypeEnum.CASE.code();
        String recordType = ReferenceSet.DataSecurityRecordTypeEnum.SEALCHARGE.code();
        Long personId = pi.getPersonId();
        Date recordDate = new Date();
        String comments = "New Record Items";

        DataSecurityRecordType dsr = new DataSecurityRecordType(null, entityId, entityType, parentEntityId, parentEntityType, recordType, personId, recordDate, comments,
                null);

        dsr = dService.createRecord(uc, dsr);

        assert (dsr.getEntityId().equals(entityId));

        dsr = dService.get(uc, dsr.getDataSecurityRecordId());

        assert (dsr.getEntityId().equals(entityId));

        entityId = 5L;
        dsr.setEntityId(entityId);

        dsr = dService.updateRecord(uc, dsr);

        assert (dsr.getEntityId().equals(entityId));

        dService.deleteRecord(uc, dsr.getDataSecurityRecordId());

        try {
            dsr = dService.get(uc, dsr.getDataSecurityRecordId());
        } catch (Exception ex) {
            assert (dsr == null);
        }

    }

    // @Test
    public void searchRecord() {
        lService.deleteAll(uc);
        // Create Charage
        CaseInfoType caseInfo = generateDefaultCaseInfo(true);

        CaseInfoType ret = lService.createCaseInfo(uc, caseInfo, null);

        ret = lService.getCaseInfo(uc, ret.getCaseInfoId());
        ;
        Long piId = piTest.createPersonIdentity();
        PersonIdentityType pi = personService.getPersonIdentityType(uc, piId, null);
        Long personId = pi.getPersonId();

        String comments = "New Recod";

        lService.updateCaseDataSecurityStatus(uc, ret.getCaseInfoId(), DataFlag.SEAL.value(), comments, personId, EntityTypeEnum.CASE.code(), ret.getCaseInfoId(), null,
                Boolean.FALSE);

        // Saerch its record

        Long caseIdToSearch = ret.getCaseInfoId();
        DataSecurityRecordSearchType dataSecurityRecordSearchType = new DataSecurityRecordSearchType();
        dataSecurityRecordSearchType.setRecordType(DataSecurityRecordTypeEnum.SEALCASE.code());
        dataSecurityRecordSearchType.setEntityType(EntityTypeEnum.CASE.code());
        dataSecurityRecordSearchType.setEntityId(caseIdToSearch);
        DataSecurityRecordsReturnType returnType = dService.search(uc, null, dataSecurityRecordSearchType, null, null, null);

        assert (returnType != null);
        assert (returnType.getTotalSize().intValue() > 0);

        DataSecurityRecordType sdsr = returnType.getDataSecurityRecord().get(0);
        assert (sdsr.getEntityId().equals(caseIdToSearch));

    }

    // @Test
    public void sealCharge() {

        lService.deleteAll(uc);
        dService.deleteAll(uc);
        // ChargeType charge = createCharge();

        // Long caseId = charge.getCaseInfoId();

        createAndGet();
        ChargeType chgRet = null;

        Long caseInfoId = caseInfoIds.iterator().next();
        // Long caseInfoId = 11L;
        CaseInfoType caseInfo = lService.getCaseInfo(uc, caseInfoId);
        Set<Long> orderIds2 = caseInfo.getOrderSentenceIds();

        log.error("chargeIds size = " + chargeIds.size());

        // Long sealMe = chargeIds.iterator().next();

        Long piId = piTest.createPersonIdentity();
        PersonIdentityType pi = personService.getPersonIdentityType(uc, piId, null);
        Long personId = pi.getPersonId();

        String comments = "New Recod";

        Long chargeId = chargeIds.iterator().next();
        // Long chargeId = 11L;

        try {
            chgRet = lService.getCharge(uc, chargeId);

            assert (chgRet != null);

            // Long ordId = orderIds.iterator().next();

            lService.updateChargeDataSecurityStatus(uc, chargeId, DataFlag.SEAL.value(), comments, personId, EntityTypeEnum.CASE.code(), caseInfoId, null, Boolean.TRUE);

            CaseInfoType caseInfo2 = lService.getCaseInfo(uc, caseInfoId);

            log.error("chargeIds2 size = " + caseInfo2);
        } catch (Exception e) {
            log.error("e = " + e);
        }

        ChargeType chgRet2 = null;
        try {

            chgRet2 = lService.getCharge(uc, chargeId);

        } catch (DataNotExistException dne) {

            assert (chgRet2 == null);
        }

        Long ret = lService.updateChargeDataSecurityStatus(uc, chargeId, DataFlag.ACTIVE.value(), comments, personId, EntityTypeEnum.CASE.code(), caseInfoId, null,
                Boolean.TRUE);

        log.error("unseal  ret = " + ret);

        ChargeType chgRetunseald = lService.getCharge(uc, chargeId);

        log.error("unsealed  chgRetunseald = " + chgRetunseald);

        assert (chgRetunseald != null);

    }

    // @Test
    public void sealCase() {

        lService.deleteAll(uc);

        // Create only mandatory fields
        CaseInfoType caseInfo = generateDefaultCaseInfo(true);

        CaseInfoType ret = lService.createCaseInfo(uc, caseInfo, null);

        ret = lService.getCaseInfo(uc, ret.getCaseInfoId());

        CaseInfoType caseInfo2 = lService.getCaseInfo(uc, ret.getCaseInfoId());

        log.error("chargeIds2 size = " + ret);

        Long piId = piTest.createPersonIdentity();
        PersonIdentityType pi = personService.getPersonIdentityType(uc, piId, null);
        Long personId = pi.getPersonId();

        String comments = "New Recod";

        lService.updateCaseDataSecurityStatus(uc, ret.getCaseInfoId(), DataFlag.SEAL.value(), comments, personId, EntityTypeEnum.CASE.code(), ret.getCaseInfoId(), null,
                Boolean.FALSE);

        CaseInfoType caseinfo = null;
        try {

            caseinfo = lService.getCaseInfo(uc, ret.getCaseInfoId());

        } catch (DataNotExistException dne) {

            assert (caseinfo == null);
        }

    }

    private DataSecurityRecordType createDataSecRecord() {
        Long piId = piTest.createPersonIdentity();
        PersonIdentityType pi = personService.getPersonIdentityType(uc, piId, null);

        Long entityId = 2L;
        String entityType = ReferenceSet.EntityTypeEnum.CHARGE.code();
        Long parentEntityId = 1L;
        String parentEntityType = ReferenceSet.EntityTypeEnum.CASE.code();
        String recordType = ReferenceSet.DataSecurityRecordTypeEnum.SEALCHARGE.code();
        Long personId = pi.getPersonId();
        Date recordDate = new Date();
        String comments = "New Record Items";

        DataSecurityRecordType dsr = new DataSecurityRecordType(null, entityId, entityType, parentEntityId, parentEntityType, recordType, personId, recordDate, comments,
                null);

        dsr = dService.createRecord(uc, dsr);
        return dsr;
    }

    // //////////////////////////////////////////////////////////////////////
    // Create CaseInfo for Charge
    private List<Long> createCaseInfo() {

        List<CaseInfoType> createdCaseInfos = new ArrayList<CaseInfoType>();
        List<Long> caseInfoIds = new ArrayList<Long>();

        // Create only mandatory fields
        CaseInfoType caseInfo = generateDefaultCaseInfo(true);

        CaseInfoType ret = lService.createCaseInfo(uc, caseInfo, null);

        ret = lService.getCaseInfo(uc, ret.getCaseInfoId());

        createdCaseInfos.add(ret);
        caseInfoIds.add(ret.getCaseInfoId());

        return caseInfoIds;

    }

    private CaseInfoType generateDefaultCaseInfo(boolean onlyMandatory) {

        // createUpdateSearchCaseIdentifierConfigPositive();

        CaseInfoType caseInfo = new CaseInfoType();
        Date currentDate = new Date();

        // Mandatory fields:
        caseInfo.setCaseCategory("IJ");
        caseInfo.setCaseType("ADULT");
        caseInfo.setCreatedDate(currentDate);
        caseInfo.setDiverted(true);
        caseInfo.setDivertedSuccess(true);
        caseInfo.setActive(true);
        caseInfo.setSupervisionId(1L);

        if (onlyMandatory) {
            return caseInfo;
        }

        // Add optional fields:
        caseInfo.setIssuedDate(currentDate);
        caseInfo.setSentenceStatus("SENTENCED");
        caseInfo.setFacilityId(1L);

        CommentType comment = new CommentType();
        comment.setUserId("1");
        comment.setCommentDate(currentDate);
        comment.setComment("createCaseInfoPositive");
        caseInfo.setComment(comment);

        caseInfo.getCaseActivityIds().add(1L);

        // Add case identifiers
        CaseIdentifierType caseIdentifier1 = new CaseIdentifierType(null, "DOCKET", "0001A", false, 1L, 1L, "createCaseInfoPositive comment1", true);

        CaseIdentifierType caseIdentifier2 = new CaseIdentifierType(null, "INDICTMENT", "0002", false, 1L, 1L, "createCaseInfoPositive comment2", false);

        caseInfo.getCaseIdentifiers().add(caseIdentifier1);
        caseInfo.getCaseIdentifiers().add(caseIdentifier2);

        return caseInfo;
    }

    public ChargeType createCharge() {

        // Create single charge instance
        ChargeType dto = getChargeType(1);

        ChargeType ret = lService.createCharge(uc, dto);
        assert (ret != null);

        ret = lService.getCharge(uc, ret.getChargeId());

        return ret;
    }

    private ChargeType getChargeType(long chargeCount) {

        List<Long> caseInfoIds = createCaseInfo();
        StatuteChargeConfigType csc = createStatuteChargePositive();

        ChargeType dto = new ChargeType();

        // get StatuteChargeConfigs
        StatuteChargeConfigSearchType search = new StatuteChargeConfigSearchType();
        search.setChargeCode("15-10");
        StatuteChargeConfigsReturnType rtn = lService.searchStatuteCharge(uc, search, null, null, null);

        // Mandatory properties
        dto.setStatuteChargeId(rtn.getStatuteChargeConfigs().get(0).getStatuteChargeId());

        Date now = new Date();
        ChargeDispositionType chargeDisposition = new ChargeDispositionType("SENT", "ACTIVE", now);
        dto.setChargeDisposition(chargeDisposition);
        dto.setPrimary(true);
        Long caseInfoId = caseInfoIds.get(0);
        dto.setCaseInfoId(caseInfoId);

        // Optional properties
        Set<ChargeIdentifierType> chargeIdentifiers = new HashSet<ChargeIdentifierType>();

        ChargeIdentifierType aChargeIdentifier = new ChargeIdentifierType(null, "CJIS", "1-23456", "0-00000", 1L, 1L, "Comment getChargeIdentifierType");
        chargeIdentifiers.add(aChargeIdentifier);

        dto.setChargeIdentifiers(chargeIdentifiers);

        Set<ChargeIndicatorType> chargeIndicators = new HashSet<ChargeIndicatorType>();

        ChargeIndicatorType aChargeIndicator = new ChargeIndicatorType(null, "ACC", true, "1-23456");
        chargeIndicators.add(aChargeIndicator);

        dto.setChargeIndicators(chargeIndicators);

        dto.setLegalText("Legal Text");
        dto.setChargeCount(chargeCount);
        dto.setFacilityId(1L);
        dto.setOrganizationId(1L);
        dto.setPersonIdentityId(1L);
        dto.setOffenceStartDate(now);
        dto.setOffenceEndDate(DateUtil.createFutureDate(10));
        dto.setFilingDate(now);

        ChargePleaType chargePlea = new ChargePleaType("G", "CHARGE_PLEA_G creation");
        dto.setChargePlea(chargePlea);

        ChargeAppealType chargeAppeal = new ChargeAppealType("INPROGRESS", "APPEAL_STATUS_INPROGRESS creation");
        dto.setChargeAppeal(chargeAppeal);

        // dto.setConditionIds(getModuleIdSet());
        // dto.setOrderSentenceIds(getModuleIdSet());

        CommentType comment = new CommentType();
        comment.setComment("Create Charge");
        comment.setCommentDate(now);
        comment.setUserId("2");
        dto.setComment(comment);

        return dto;
    }

    public StatuteChargeConfigType createStatuteChargePositive() {

        StatuteChargeConfigType dto = new StatuteChargeConfigType();

        // Set mandatory fields
        dto.setStatuteId(1L);
        dto.setChargeCode("15-10");

        StatuteChargeTextType ct = new StatuteChargeTextType("EN", "StatuteChargeTextType desc", "StatuteChargeTextType legal text");

        dto.setChargeText(ct);
        dto.setBailAllowed(true);
        dto.setBondAllowed(false);

        dto.setStartDate(DateUtil.createPastDate(10));
        dto.setEndDate(DateUtil.createFutureDate(1000));
        dto.setChargeType("F");

        // Set optional fields
        dto.setBailAmount(10000L);

        Set<Long> externalChargeCodeIds = new HashSet<Long>();
        externalChargeCodeIds.add(1L);
        externalChargeCodeIds.add(2L);
        dto.setExternalChargeCodeIds(externalChargeCodeIds);

        dto.setChargeDegree("I");
        dto.setChargeCategory("T");
        dto.setChargeSeverity("H");

        ChargeIndicatorType cit = new ChargeIndicatorType(null, "ACC", true, "1-23456");

        Set<ChargeIndicatorType> chargeIndicators = new HashSet<ChargeIndicatorType>();
        chargeIndicators.add(cit);
        dto.setChargeIndicators(chargeIndicators);

        Set<String> cef = new HashSet<String>();
        cef.add("RACE");
        cef.add("SEX");

        dto.setEnhancingFactors(cef);

        Set<String> crf = new HashSet<String>();
        cef.add("MI");
        cef.add("MIN");
        dto.setReducingFactors(crf);
        Date endDate = new Date();
        dto.setEndDate(endDate);

        StatuteChargeConfigType ret = lService.createStatuteCharge(uc, dto);

        assert (ret != null);

        Long statuteChargeConfigId = ret.getStatuteChargeId();

        // Test to create second recored with same charge code and text
        // Set mandatory fields
        dto.setStatuteId(1L);
        dto.setChargeCode("15-10");
        dto.setChargeText(ct);
        dto.setBailAllowed(true);
        dto.setBondAllowed(false);

        dto.setStartDate(DateUtil.createPastDate(10));
        dto.setEndDate(DateUtil.createFutureDate(1000));
        dto.setChargeType("F");

        dto.setChargeDegree("II");

        ret = lService.createStatuteCharge(uc, dto);
        assert (ret != null);

        return ret;
    }

    public void createOrderConfiguration() {

        // Bail
        OrderConfigurationType ret;
        OrderConfigurationType config;
        OrderConfigurationType ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.BAIL.name());
        ordConfig.setOrderType(OrdType.TOO.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(false);
        ordConfig.setIsSchedulingNeeded(true);
        ordConfig.setHasCharges(true);
        ordConfig.setStartDate(new Date());

        ordConfig.setIssuingAgencies(instancesOfNotification());
        ret = lService.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);
        config = ret;
        assertEquals(config.getOrderClassification(), OrdClassification.BAIL.name());
        assertEquals(config.getOrderType(), OrdType.TOO.name());
        assertEquals(config.getOrderCategory(), OrdCategory.IJ.name());
        assertEquals(config.getIsHoldingOrder(), ordConfig.getIsHoldingOrder());
        assertEquals(config.getIsSchedulingNeeded(), ordConfig.getIsSchedulingNeeded());
        assertEquals(config.getHasCharges(), ordConfig.getHasCharges());
        assertEquals(config.getIssuingAgencies(), instancesOfNotification());

        // SentenceOrder
        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.SENT.name());
        ordConfig.setOrderType(OrdType.SENTORD.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(true);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(true);
        ordConfig.setStartDate(new Date());

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = lService.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);
        config = ret;
        assertEquals(config.getOrderClassification(), OrdClassification.SENT.name());
        assertEquals(config.getOrderType(), OrdType.SENTORD.name());
        assertEquals(config.getOrderCategory(), OrdCategory.IJ.name());
        assertEquals(config.getIsHoldingOrder(), Boolean.FALSE, "SDD: If Order Classification = SENT (Sentence Order) then isHoldingOrder = true");
        assertEquals(config.getIsSchedulingNeeded(), ordConfig.getIsSchedulingNeeded());
        assertEquals(config.getHasCharges(), ordConfig.getHasCharges());
        assertEquals(config.getIssuingAgencies(), instancesOfNotification2());

        // LegalOrder
        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.LO.name());
        ordConfig.setOrderType(OrdType.SENTORD.name());
        ordConfig.setOrderCategory(OrdCategory.OJ.name());
        ordConfig.setIsHoldingOrder(true);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(true);
        ordConfig.setStartDate(new Date());

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = lService.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);

        // WarrantDetainer
        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.WD.name());
        ordConfig.setOrderType(OrdType.ICE.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(true);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(true);
        ordConfig.setStartDate(new Date());

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = lService.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);

        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.WD.name());
        ordConfig.setOrderType(OrdType.WRMD.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(false);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(false);
        ordConfig.setStartDate(new Date());

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = lService.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);
    }

    private Set<NotificationType> instancesOfNotification2() {
        Set<NotificationType> notifications = new HashSet<NotificationType>();
        NotificationType notification = new NotificationType();
        notification.setNotificationOrganizationAssociation(2L);
        notification.setNotificationFacilityAssociation(205L);
        notification.setIsNotificationNeeded(true);
        notification.setIsNotificationConfirmed(true);
        notification.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6500L, 6600L })));
        ContactType contact1 = getContact("Syscon Justice");
        ContactType contact2 = getContact("Richmond Court");
        ContactType contact3 = getContact("BC Govt");
        Set<ContactType> contacts = new HashSet<>();
        contacts.add(contact1);
        contacts.add(contact2);
        contacts.add(contact3);
        notification.setNotificationAgencyContacts(contacts);
        notifications.add(notification);

        NotificationType notification2 = new NotificationType();
        notification2.setNotificationOrganizationAssociation(3L);
        notification2.setNotificationFacilityAssociation(206L);
        notification2.setIsNotificationNeeded(true);
        notification2.setIsNotificationConfirmed(true);
        notification2.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6701L, 6801L })));
        ContactType contact4 = getContact("Canada High Court");
        contacts.add(contact4);
        notification2.setNotificationAgencyContacts(contacts);
        notification2.setNotificationAgencyContacts(contacts);
        notifications.add(notification2);
        return notifications;
    }

    private ContactType getContact(String str) {
        if (str != null && !str.trim().isEmpty()) {
			str = " " + str;
		}
        ContactType contact = new ContactType();
        contact.setFirstName("First Name" + str);
        contact.setLastName("Last Name" + str);
        contact.setAddress("Address" + str);
        contact.setCity("City" + str);
        contact.setCounty("County" + str);
        contact.setCountry("Country" + str);
        contact.setZipCode("Zip Code" + str);
        contact.setPhoneNumber("Phone Number" + str);
        contact.setFaxNumber("Fax Number" + str);
        contact.setEmail("Email" + str);
        contact.setIdentityNumber("Identity Number" + str);
        contact.setAgentContact("Agent Contact" + str);
        return contact;
    }

    private Set<NotificationType> instancesOfNotification() {
        Set<NotificationType> notifications = new HashSet<NotificationType>();
        NotificationType notification = new NotificationType();
        notification.setNotificationOrganizationAssociation(1L);
        notification.setNotificationFacilityAssociation(204L);
        notification.setIsNotificationNeeded(true);
        notification.setIsNotificationConfirmed(true);
        notification.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6500L, 6600L })));
        ContactType contact1 = getContact("Syscon Justice");
        ContactType contact2 = getContact("Richmond Court");
        ContactType contact3 = getContact("BC Govt");
        Set<ContactType> contacts = new HashSet<>();
        contacts.add(contact1);
        contacts.add(contact2);
        contacts.add(contact3);
        notification.setNotificationAgencyContacts(contacts);
        notifications.add(notification);

        NotificationType notification2 = new NotificationType();
        notification2.setNotificationOrganizationAssociation(2L);
        notification2.setNotificationFacilityAssociation(205L);
        notification2.setIsNotificationNeeded(true);
        notification2.setIsNotificationConfirmed(true);
        notification2.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6700L, 6800L })));
        ContactType contact4 = getContact("Canada High Court");
        contacts.add(contact4);
        notification2.setNotificationAgencyContacts(contacts);
        notifications.add(notification2);
        return notifications;
    }

    public <T extends OrderType> void createAndGet() {

        createOrderConfiguration();
        ChargeType charge = createCharge();

        chargeIds.add(charge.getChargeId());
        caseInfoIds.add(charge.getCaseInfoId());

        Long[] arrayChargeIds = new Long[] { (Long) chargeIds.toArray()[0] };
        List<T> orders;

        // Warrant
        WarrantDetainerType retWarrant;
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        cal.add(Calendar.YEAR, 1);
        Date expirationDate = cal.getTime();
        Date termEndDate = cal.getTime();
        Date sentenceEndDate = cal.getTime();

        WarrantDetainerType warrant = new WarrantDetainerType();
        warrant.setOrderClassification(OrdClassification.WD.name());
        warrant.setOrderType(OrdType.ICE.name());
        warrant.setOrderCategory(OrdCategory.IJ.name());
        warrant.setOrderSubType(OrdSubType.WANT.name());
        warrant.setOrderNumber("Warrant Order No.12345");
        warrant.setOrderDisposition(instanceOfDisposition());
        warrant.setOrderIssuanceDate(today);
        warrant.setOrderReceivedDate(today);
        warrant.setOrderStartDate(today);
        warrant.setOrderExpirationDate(today);

        warrant.setIsHoldingOrder(true);
        warrant.setIsSchedulingNeeded(false);
        warrant.setHasCharges(true);
        warrant.setIssuingAgency(instancesOfNotification().iterator().next());

        // SETTING ASSOCIATIONS
        warrant.setCaseInfoIds(caseInfoIds);
        warrant.setChargeIds(chargeIds);
        Set<Long> conditionIds = new HashSet<Long>();
        warrant.setConditionIds(conditionIds);

        warrant.setAgencyToBeNotified(instancesOfNotification());

        retWarrant = lService.createWarrantDetainer(uc, warrant);
        Long orderId = retWarrant.getOrderIdentification();
        warrant.setOrderIdentification(orderId);

        warrant.setNotificationLog(new HashSet<NotificationLogType>());
        WarrantDetainerType warrant2 = lService.getOrder(uc, warrant.getOrderIdentification());

        orderIds.add(orderId);

        // Bail
        BailType bailRet;
        BailType bail = new BailType();
        bail.setOrderClassification(OrdClassification.BAIL.name());
        bail.setOrderType(OrdType.TOO.name());
        bail.setOrderCategory(OrdCategory.IJ.name());
        bail.setOrderSubType(OrdSubType.DETAINER.name());
        bail.setOrderNumber("Bail No.12345");
        bail.setOrderDisposition(instanceOfDisposition());
        bail.setComments(instanceOfComment());
        bail.setOrderIssuanceDate(today);
        bail.setOrderReceivedDate(today);
        bail.setOrderStartDate(today);
        bail.setOrderExpirationDate(expirationDate);
        bail.setIsSchedulingNeeded(true);
        bail.setHasCharges(true);
        bail.setIssuingAgency(instancesOfNotification().iterator().next());

        // SETTING ASSOCIATIONS
        bail.setCaseInfoIds(caseInfoIds);
        bail.setChargeIds(chargeIds);
        bail.setConditionIds(new HashSet<Long>());

        bail.setBailAmounts(new HashSet<BailAmountType>(Arrays.asList(
                new BailAmountType[] { new BailAmountType(BlType.CASH.name(), BigDecimal.valueOf(400.00), new HashSet<Long>(Arrays.asList(new Long[] { 400L }))),
                        new BailAmountType(BlType.PROP.name(), BigDecimal.valueOf(5000.00), new HashSet<Long>(Arrays.asList(new Long[] { 500L }))) })));
        bail.setBailRelationship(BlRelationship.AG.name());
        bail.setBailPostedAmounts(new HashSet<BailAmountType>(Arrays.asList(
                new BailAmountType[] { new BailAmountType(BlType.CASH.name(), BigDecimal.valueOf(2000.00), new HashSet<Long>(Arrays.asList(new Long[] { 400L }))),
                        new BailAmountType(BlType.PROP.name(), BigDecimal.valueOf(3000.00), new HashSet<Long>(Arrays.asList(new Long[] { 500L }))) })));
        bail.setBailPaymentReceiptNo("Bail paid no. 123456");
        bail.setBailRequirement("Bail Requirement");
        bail.setBailerPersonIdentityAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 111L, 222L, 333L })));
        bail.setBondPostedAmount(new BailAmountType(BlType.CASH.name(), BigDecimal.valueOf(2000.00), new HashSet<Long>(Arrays.asList(new Long[] { 400L }))));
        bail.setBondPaymentDescription("Bond($2000.00) has been paid.");
        bail.setBondOrganizationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 1111L, 2222L, 3333L })));
        bail.setIsBailAllowed(true);

        bailRet = lService.createBail(uc, bail);
        bail.setOrderIdentification(bailRet.getOrderIdentification());
        BailType bail2 = lService.getOrder(uc, bail.getOrderIdentification());

        orderIds.add(bail.getOrderIdentification());

        // Sentence
        assertTrue(lService.setSentenceNumberConfiguration(uc, true));
        SentenceType sentenceRet;
        SentenceType sentence = new SentenceType();
        sentence.setOrderClassification(OrdClassification.SENT.name());
        sentence.setOrderType(OrdType.SENTORD.name());
        sentence.setOrderCategory(OrdCategory.IJ.name());
        sentence.setOrderSubType(OrdSubType.DETAINER.name());
        sentence.setOrderNumber("Sentence No.12345");
        sentence.setOrderDisposition(instanceOfDisposition());
        sentence.setComments(instanceOfComment());
        sentence.setOrderIssuanceDate(today);
        sentence.setOrderReceivedDate(today);
        sentence.setOrderStartDate(today);
        sentence.setOrderExpirationDate(expirationDate);
        sentence.setIsHoldingOrder(true);
        sentence.setIsSchedulingNeeded(false);
        sentence.setHasCharges(true);
        sentence.setIssuingAgency(instancesOfNotification().iterator().next());

        // SETTING ASSOCIATIONS
        sentence.setCaseInfoIds(caseInfoIds);
        sentence.setChargeIds(new HashSet<Long>(Arrays.asList(new Long[] { arrayChargeIds[0] })));
        sentence.setConditionIds(new HashSet<Long>());

        sentence.setSentenceType(SentType.FINEDEF.name());
        sentence.setSentenceNumber(12345L);
        TermType term = new TermType();
        term.setTermCategory(TmCategory.CUS.name());
        term.setTermName(TmName.MAX.name());
        term.setTermType(TmType.INT.name());
        term.setTermStatus(TmStatus.EXC.name());
        term.setTermSequenceNo(12345L);
        term.setTermStartDate(today);
        term.setTermEndDate(termEndDate);
        Set<TermType> sentenceTerms = new HashSet<TermType>();
        sentenceTerms.add(term);
        sentence.setSentenceTerms(sentenceTerms);
        sentence.setSentenceStatus(SentStatus.EXC.name());
        sentence.setSentenceStartDate(today);
        sentence.setSentenceEndDate(sentenceEndDate);
        sentence.setSentenceAppeal(new SentenceAppealType(AppealStatus.ACCEPTED.name(), "Accepted"));
        // sentence.setConcecutiveFrom(warrant.getOrderIdentification());
        sentence.setFineAmount(BigDecimal.valueOf(10000.00));
        sentence.setFineAmountPaid(true);
        sentence.setSentenceAggravateFlag(true);
        IntermittentScheduleType sch = new IntermittentScheduleType();
        sch.setDayOfWeek(Day.MON.name());
        sch.setDayOutOfWeek(Day.MON.name());
        sch.setStartTime(sch.new TimeValue(8L, 30L, 0L));
        sch.setEndTime(sch.new TimeValue(16L, 30L, 0L));
        sch.setLocationId(6500L);

        Set<IntermittentScheduleType> intermittentSchedules = new HashSet<IntermittentScheduleType>();
        intermittentSchedules.add(sch);
        sentence.setIntermittentSchedule(intermittentSchedules);

        sentenceRet = lService.createSentence(uc, sentence);

        Long sentenceId1 = sentenceRet.getOrderIdentification();
        sentence.setOrderIdentification(sentenceRet.getOrderIdentification());

        SentenceType sentence2 = lService.getOrder(uc, sentence.getOrderIdentification());

        orderIds.add(sentence.getOrderIdentification());

        orderIds.add(sentence2.getOrderIdentification());

        SentenceType sentenceDef = sentence2;

        // Legal
        LegalOrderType legalRet;
        LegalOrderType legal = new LegalOrderType();
        legal.setOrderClassification(OrdClassification.LO.name());
        legal.setOrderType(OrdType.SENTORD.name());
        legal.setOrderCategory(OrdCategory.OJ.name());
        legal.setOjSupervisionId(1L);
        // legal.setOrderSubType(new
        // CodeType(ReferenceSet.ORDER_SUB_TYPE.value(),
        // OrdSubType.DETAINER.name()));
        legal.setOrderNumber("Legal Order No.12345");
        legal.setOrderDisposition(instanceOfDisposition());
        legal.setComments(instanceOfComment());
        legal.setOrderIssuanceDate(today);
        legal.setOrderReceivedDate(today);
        legal.setOrderStartDate(today);
        legal.setOrderExpirationDate(expirationDate);
        legal.setIsHoldingOrder(true);
        legal.setIsSchedulingNeeded(false);
        legal.setHasCharges(true);
        legal.setIssuingAgency(instancesOfNotification().iterator().next());

        // SETTING ASSOCIATIONS
        legal.setCaseInfoIds(caseInfoIds);
        legal.setChargeIds(new HashSet<Long>());
        legal.setConditionIds(new HashSet<Long>());
        legalRet = lService.createLegalOrder(uc, legal);
        legal.setOrderIdentification(legalRet.getOrderIdentification());
        LegalOrderType legal2 = lService.getOrder(uc, legal.getOrderIdentification());

        orderIds.add(legal.getOrderIdentification());
    }

    private DispositionType instanceOfDisposition() {
        DispositionType disposition = new DispositionType();
        disposition.setDispositionOutcome(OrdOutcome.SENT.name());
        disposition.setOrderStatus(OrdStatus.ACTIVE.name());
        disposition.setDispositionDate(new Date());

        return disposition;
    }

    private CommentType instanceOfComment() {
        CommentType comment = new CommentType();
        comment.setCommentDate(new Date());
        comment.setComment("Order Comment");
        comment.setUserId("devtest");

        return comment;
    }

    private void gettabale() {

    }

}
