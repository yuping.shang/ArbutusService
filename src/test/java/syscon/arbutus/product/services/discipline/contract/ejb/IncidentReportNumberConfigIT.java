package syscon.arbutus.product.services.discipline.contract.ejb;

import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.discipline.contract.dto.incidentreport.IncidentReportNumConfigType;
import syscon.arbutus.product.services.discipline.contract.interfaces.DisciplineService;

import static org.testng.Assert.assertNotNull;

/**
 * IncidentReportNumberConfigIT to test the APIs which are used to configure incident report number.
 * This test is not included in IT test and can be run separately.
 *
 * @author lhan
 */
public class IncidentReportNumberConfigIT extends BaseIT {
    private UserContext uc = null;
    private Logger log = LoggerFactory.getLogger(IncidentReportNumberConfigIT.class);

    private DisciplineService service;

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();
        assertNotNull(service);
    }

    @Test
    public void getVersion() {
        String rtn = service.getVersion(uc);
        Assert.assertEquals(rtn, "1.0");
    }

    @DataProvider(name = "incidentReportNumberFormat")
    public Object[][] incidentReportNumberFormat() {

        Object[][] retObjArr = { { "IR999999-YYYY" }, { "ABC999-/999-YY" }, { "999-YY-999999-YY" }, { "/\"999-IR-999ABC999_-@#$%^&*()/DD/MM/YY" },
                //...
        };
        return (retObjArr);
    }

    @Test(enabled = true, dataProvider = "incidentReportNumberFormat")
    public void configIncidentReportNumber(String format) {

        IncidentReportNumConfigType configType = new IncidentReportNumConfigType();
        configType.setFormat(format);
        configType.setGeneration(true);

        IncidentReportNumConfigType rtn = service.updateIncidentReportNumConfig(uc, configType);
        Assert.assertNotNull(rtn);
        Assert.assertEquals(format, rtn.getFormat());

        String incidentReportNumber = service.generateIncidentReportNumber(uc);
        Assert.assertNotNull(incidentReportNumber);

    }

    //for release
    private void lookupJNDILogin() {

        log.info("Before JNDI lookup");
        service = (DisciplineService) JNDILookUp(this.getClass(), DisciplineService.class);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();
        clientlogin("devtest", "devtest");
    }

    private void clientlogin(String user, String password) {

        SecurityClient client;

        try {
            client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password); //"devtest", "devtest" nice regular service user
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
