package syscon.arbutus.product.services.discipline.contract.ejb;

import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.discipline.contract.dto.incidentreport.IncidentReportSearchType;
import syscon.arbutus.product.services.discipline.contract.dto.incidentreport.IncidentReportsReturnType;
import syscon.arbutus.product.services.discipline.contract.interfaces.DisciplineService;

import static org.testng.Assert.assertNotNull;

/**
 * IncidentReportSearchbyReportedByStaffBadgeIdIT to test search incident report by reported by staff badge id.
 * The Pre-condition is manually created through UI, so this test won't be included in IT test and can only be run manually.
 *
 * @author lhan
 */
public class IncidentReportSearchbyReportedByStaffBadgeIdIT extends BaseIT {
    private UserContext uc = null;
    private Logger log = LoggerFactory.getLogger(IncidentReportSearchbyReportedByStaffBadgeIdIT.class);

    private DisciplineService service;

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();
        assertNotNull(service);
    }

    @Test
    public void getVersion() {
        String rtn = service.getVersion(uc);
        Assert.assertEquals(rtn, "1.0");
    }

    @Test(enabled = false)
    public void searchIncidentReport() {
        //test search
        IncidentReportSearchType search = null;
        IncidentReportsReturnType ret = null;

        //search by report by reported by staff badge id only
        search = new IncidentReportSearchType();
        search.setReportedByStaffBadgeId("12345");

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertNotNull(ret);
        Assert.assertEquals(new Long(1L), ret.getTotalSize());

        //wildcard search
        search.setReportedByStaffBadgeId("*123*");

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertNotNull(ret);
        Assert.assertEquals(new Long(1L), ret.getTotalSize());

        //search by report by reported by staff badge id and other field
        search.setReportedByFirstName("LEI");

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertNotNull(ret);
        Assert.assertEquals(new Long(1L), ret.getTotalSize());

        search.setReportedByLastName("ABC");

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertNotNull(ret);
        Assert.assertEquals(new Long(0L), ret.getTotalSize());

    }

    @AfterClass
    public void afterClass() {

    }

    //for release
    private void lookupJNDILogin() {

        log.info("Before JNDI lookup");
        service = (DisciplineService) JNDILookUp(this.getClass(), DisciplineService.class);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();
        clientlogin("devtest", "devtest");
    }

    private void clientlogin(String user, String password) {

        SecurityClient client;

        try {
            client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password); //"devtest", "devtest" nice regular service user
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
