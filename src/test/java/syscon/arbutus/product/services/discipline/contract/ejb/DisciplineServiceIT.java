package syscon.arbutus.product.services.discipline.contract.ejb;

import java.math.BigDecimal;
import java.util.*;

import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.*;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.discipline.contract.dto.*;
import syscon.arbutus.product.services.discipline.contract.dto.incidentreport.*;
import syscon.arbutus.product.services.discipline.contract.interfaces.DisciplineService;
import syscon.arbutus.product.services.movement.contract.dto.CourtMovementType;

import static org.testng.Assert.assertNotNull;

/**
 * DisciplineServiceIT to test APIs of IncidentReport service.
 *
 * @author lhan
 */
public class DisciplineServiceIT extends BaseIT {
    private static final String INCIDENT_SUB_TYPE = "WPN";
    private static final String INCIDENT_TYPE = "INJ";
    private static final String MEDICAL_ATTN = "SINJ";
    private static final String NARRATIVE = "Injury incident.";
    private static final boolean INDICATOR_OF_STG = false;
    private static final boolean USE_OF_FORCE = false;
    private static final String ACTION_TAKEN = "RTO";
    private static final String ROLE = "ROLE1";
    private static final String NEW_INCIDENT_NUMBER = "NUMBER2";
    private static final String NEW_INCIDENT_SUB_TYPE = "ACCT";
    private static final String NEW_INCIDENT_TYPE = "RIOT";
    private static final String NEW_ACTION_TAKEN = "RFH";
    private static final String NEW_NARRATIVE = "Injury incident 2.";
    private static final String NEW_MEDICAL_ATTN = "MINJ";
    private static final String NEW_COMMENT = "NEW COMMENT";
    private static final String NEW_ROLE = "new role";
    private UserContext uc = null;
    private Logger log = LoggerFactory.getLogger(DisciplineServiceIT.class);

    private DisciplineService service;
    private Long shiftLogId;
    private Long facilityId;
    private Long facilityInternalLocationId;
    private Long staffId;
    private Date occurredDateTime;
    private Date reportedDateTime;
    private List<Long> incidentReportIds = new ArrayList<Long>();
    private Long personIdentityId;
    private Long staffReportId;
    private Long newOccurredFacilityId;
    private Long newOccurredFacilityInternalLocationId;
    private Long newReportedBy;
    private Long newStaffId;
    private Long newStaffReportId;
    private Long newPersonIdenetityId;
    private String incidentReportNumber;
    private JurisdictionType jurisdiction;
    private RegulationType regulation;
    private RegulationRuleType regulationRule;
    private RegulationRuleType regulationRule1;
    private Long supervisionId = 1l;
    private OffenseInCustodyType offenseInCustodyType;

    private StaffTest staffTest = new StaffTest();
    private PersonIdentityTest personIdentityTest = new PersonIdentityTest();
    private PersonTest psnTest = new PersonTest();
    private SupervisionTest supervisionTest = new SupervisionTest();
    private OffenseInCustodyChargeType charge;
    ;
    private OffenseInCustodyHearingType hearing;
    private OffenseInCustodyInvestigatorType investigator;
    private OffenseInCustodyPenaltyType penalty;
    private InvestigationNoteType note;
    private InvestigationResultType investigationResult;

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();
        assertNotNull(service);

        //TODO: need to generate real Ids once FK constraint added.
        shiftLogId = 1L;
        facilityId = 1L;
        facilityInternalLocationId = 1L;
        staffReportId = 1L;
        newOccurredFacilityId = 2L;
        newOccurredFacilityInternalLocationId = 2L;
        newStaffReportId = 2L;

        staffId = staffTest.createStaff();
        newReportedBy = staffTest.createStaff();
        newStaffId = staffTest.createStaff();

        personIdentityId = personIdentityTest.createPersonIdentity();
        newPersonIdenetityId = personIdentityTest.createPersonIdentity();

        occurredDateTime = new Date();
        reportedDateTime = new Date();

        IncidentReportNumConfigType incidentReportNumConfig = new IncidentReportNumConfigType();
        supervisionId = supervisionTest.createSupervision(facilityId, true);
        try {
            incidentReportNumConfig.setFormat("IR99999-YYYY");
            incidentReportNumConfig.setGeneration(true);
            service.createIncidentReportNumConfig(uc, incidentReportNumConfig);
        } catch (Exception e) {
            service.updateIncidentReportNumConfig(uc, incidentReportNumConfig);
        }
    }

    @Test
    public void getVersion() {
        String rtn = service.getVersion(uc);
        Assert.assertEquals(rtn, "1.0");
    }

    public String getIncidentReportNumber() {
        incidentReportNumber = service.generateIncidentReportNumber(uc);
        Assert.assertNotNull(incidentReportNumber);
        return incidentReportNumber;

    }

    @Test
    public void createIncidentReport() {
        //create with only mandatory
        IncidentReportType incidentReport = generateDefaultIncidentReport(true);

        IncidentReportType rtn1 = service.createIncidentReport(uc, incidentReport);

        verifyIncidentReport(rtn1, incidentReport);

        incidentReportIds.add(rtn1.getIncidentReportId());

        //create with all fields
        incidentReport = generateDefaultIncidentReport(false);

        IncidentReportType rtn2 = service.createIncidentReport(uc, incidentReport);

        verifyIncidentReport(rtn2, incidentReport);

        incidentReportIds.add(rtn2.getIncidentReportId());
    }

    @Test(dependsOnMethods = "createIncidentReport", enabled = true)
    public void getIncidentReport() {
        //create with only mandatory
        IncidentReportType incidentReport = generateDefaultIncidentReport(false);

        IncidentReportType rtn = service.createIncidentReport(uc, incidentReport);

        verifyIncidentReport(rtn, incidentReport);

        Long incidentReportId = rtn.getIncidentReportId();
        incidentReportIds.add(incidentReportId);

        //test get
        IncidentReportType ret = service.getIncidentReport(uc, incidentReportId);
        verifyIncidentReport(ret, rtn);

        Assert.assertEquals("Liang", ret.getReportedByLastName());
        Assert.assertTrue(ret.getReportedByFirstName().contains("Jason"));
    }

    @Test(dependsOnMethods = { "getIncidentReport" }, enabled = true)
    public void deleteIncidentReport() {
        //create with only mandatory
        IncidentReportType incidentReport = generateDefaultIncidentReport(true);

        IncidentReportType rtn = service.createIncidentReport(uc, incidentReport);

        verifyIncidentReport(rtn, incidentReport);

        Long id = rtn.getIncidentReportId();

        Long deleteRtn = service.deleteIncidentReport(uc, id);
        assert (deleteRtn.equals(1L));

        try {
            service.getIncidentReport(uc, id);
        } catch (Exception e) {
            assert (e instanceof DataNotExistException);
        }

    }

    @Test(dependsOnMethods = { "deleteIncidentReport" }, enabled = true)
    public void updateIncidentReport() {
        //create with only mandatory
        IncidentReportType incidentReport = generateDefaultIncidentReport(false);

        IncidentReportType rtn = service.createIncidentReport(uc, incidentReport);

        verifyIncidentReport(rtn, incidentReport);

        incidentReportIds.add(rtn.getIncidentReportId());

        //test update
        rtn.setAssociatedShiftLogId(2L);
        rtn.setIncidentNumber(NEW_INCIDENT_NUMBER);
        rtn.setIncidentSubType(NEW_INCIDENT_SUB_TYPE);
        rtn.setIncidentType(NEW_INCIDENT_TYPE);
        rtn.setIndicatorOfSTG(false);
        rtn.setNarrative(NEW_NARRATIVE);
        rtn.setOccurredDateTime(new Date());
        rtn.setOccurredFacilityId(newOccurredFacilityId);
        rtn.setOccurredFacilityInternalLocationId(newOccurredFacilityInternalLocationId);
        rtn.setReportedBy(newReportedBy);
        rtn.setUseOfForce(false);
        rtn.setReportedBy(newReportedBy);
        rtn.setReportedDateTime(new Date());

        IncidentReportType rtnUpdate = service.updateIncidentReport(uc, rtn);

        verifyIncidentReport(rtn, rtnUpdate);
        Assert.assertTrue(rtn.getReportedDateTime().getTime() > rtnUpdate.getReportedDateTime().getTime());

    }

    @Test(dependsOnMethods = "updateIncidentReport")
    public void CRUDIncidentReportInmate() {
        //create with only mandatory
        IncidentReportType incidentReport = generateDefaultIncidentReport(true);

        IncidentReportType rtn = service.createIncidentReport(uc, incidentReport);

        verifyIncidentReport(rtn, incidentReport);

        //create IncidentReportInmate
        IncidentReportInmateType inmate = generateDefaultIncidentReportInmate(rtn.getIncidentReportId());

        IncidentReportInmateType ret = service.createIncidentReportInmate(uc, inmate);

        verifyInmate(ret, inmate);

        //get IncidentReportInmate
        Long id = ret.getIncidentReportInmateId();

        IncidentReportInmateType retGet = service.getIncidentReportInmate(uc, id);

        verifyInmate(retGet, ret);

        //update
        retGet.setComment(NEW_COMMENT);
        retGet.setRole(NEW_ROLE);
        retGet.setActionTaken(NEW_ACTION_TAKEN);
        retGet.setHearingDateTime(new Date());
        retGet.setPersonIdentityId(newPersonIdenetityId);
        retGet.setMedicalAttentionRequired(NEW_MEDICAL_ATTN);
        retGet.setUseOfForce(true);

        IncidentReportInmateType retUpdate = service.updateIncidentReportInmate(uc, retGet);

        verifyInmate(retUpdate, retGet);

        incidentReport = service.getIncidentReport(uc, rtn.getIncidentReportId());
        Assert.assertEquals(true, incidentReport.isUseOfForce());

        //delete IncidentReportInmate
        Long deleteRtn = service.deleteIncidentReportInmate(uc, id);

        assert (deleteRtn.equals(1L));

        try {
            service.getIncidentReportInmate(uc, id);
        } catch (Exception e) {
            assert (e instanceof DataNotExistException);
        }

        incidentReport = service.getIncidentReport(uc, rtn.getIncidentReportId());
        Assert.assertEquals(false, incidentReport.isUseOfForce());

        //get list by incident report id
        inmate = generateDefaultIncidentReportInmate(rtn.getIncidentReportId());

        ret = service.createIncidentReportInmate(uc, inmate);

        verifyInmate(ret, inmate);

        List<IncidentReportInmateType> rtnList = service.getIncidentReportInmatesByIncidentReportId(uc, rtn.getIncidentReportId());

        Assert.assertEquals(1, rtnList.size());

        incidentReportIds.add(rtn.getIncidentReportId());
    }

    @Test(dependsOnMethods = "CRUDIncidentReportInmate")
    public void CRUDIncidentReportStaff() {
        //create with only mandatory
        IncidentReportType incidentReport = generateDefaultIncidentReport(true);

        IncidentReportType rtn = service.createIncidentReport(uc, incidentReport);

        verifyIncidentReport(rtn, incidentReport);

        //create IncidentReportStaff
        IncidentReportStaffType staff = generateDefaultIncidentReportStaff(rtn.getIncidentReportId());

        IncidentReportStaffType ret = service.createIncidentReportStaff(uc, staff);

        verifyStaff(ret, staff);

        //get IncidentReportStaff
        Long id = ret.getIncidentReportStaffId();

        IncidentReportStaffType retGet = service.getIncidentReportStaff(uc, id);

        verifyStaff(retGet, ret);

        //update
        retGet.setComment(NEW_COMMENT);
        retGet.setRole(NEW_ROLE);
        retGet.setStaffId(newStaffId);
        retGet.setStaffReportId(newStaffReportId);
        retGet.setMedicalAttentionRequired(NEW_MEDICAL_ATTN);

        IncidentReportStaffType retUpdate = service.updateIncidentReportStaff(uc, retGet);

        verifyStaff(retUpdate, retGet);

        //delete IncidentReportStaff
        Long deleteRtn = service.deleteIncidentReportStaff(uc, id);

        assert (deleteRtn.equals(1L));

        try {
            service.getIncidentReportStaff(uc, id);
        } catch (Exception e) {
            assert (e instanceof DataNotExistException);
        }

        //get list by incident report id
        staff = generateDefaultIncidentReportStaff(rtn.getIncidentReportId());

        ret = service.createIncidentReportStaff(uc, staff);

        verifyStaff(ret, staff);

        List<IncidentReportStaffType> rtnList = service.getIncidentReportStaffsByIncidentReportId(uc, rtn.getIncidentReportId());

        Assert.assertEquals(1, rtnList.size());

        incidentReportIds.add(rtn.getIncidentReportId());
    }

    @Test(dependsOnMethods = "CRUDIncidentReportStaff")
    public void CRUDIncidentReportPerson() {
        //create with only mandatory
        IncidentReportType incidentReport = generateDefaultIncidentReport(true);

        IncidentReportType rtn = service.createIncidentReport(uc, incidentReport);

        verifyIncidentReport(rtn, incidentReport);

        //create IncidentReportPerson
        IncidentReportPersonType person = generateDefaultIncidentReportPerson(rtn.getIncidentReportId());

        IncidentReportPersonType ret = service.createIncidentReportPerson(uc, person);

        verifyPerson(ret, person);

        //get IncidentReportPerson
        Long id = ret.getIncidentReportPersonId();

        IncidentReportPersonType retGet = service.getIncidentReportPerson(uc, id);

        verifyPerson(retGet, ret);

        //update
        retGet.setComment(NEW_COMMENT);
        retGet.setRole(NEW_ROLE);
        retGet.setPersonIdentityId(newPersonIdenetityId);
        retGet.setMedicalAttentionRequired(NEW_MEDICAL_ATTN);

        IncidentReportPersonType retUpdate = service.updateIncidentReportPerson(uc, retGet);

        verifyPerson(retUpdate, retGet);

        //delete IncidentReportPerson
        Long deleteRtn = service.deleteIncidentReportPerson(uc, id);

        assert (deleteRtn.equals(1L));

        try {
            service.getIncidentReportPerson(uc, id);
        } catch (Exception e) {
            assert (e instanceof DataNotExistException);
        }

        //get list by incident report id
        person = generateDefaultIncidentReportPerson(rtn.getIncidentReportId());

        ret = service.createIncidentReportPerson(uc, person);

        verifyPerson(ret, person);

        List<IncidentReportPersonType> rtnList = service.getIncidentReportPeopleByIncidentReportId(uc, rtn.getIncidentReportId());

        Assert.assertEquals(1, rtnList.size());

        incidentReportIds.add(rtn.getIncidentReportId());
    }

    //@Test
    public void createStaffReport() {

        StaffReportType staffReportType = generateDefaultStaffReport("1", 4L);
        staffReportType.setStaffStatement("staement1");

        StaffReportType rtn = service.createStaffReport(uc, staffReportType);
        System.out.println("");
    }

    //@Test
    public void updateStaffReport() {
        StaffReportType staffReportType = generateDefaultStaffReport("4", 4L);
        staffReportType.setStaffStatement("statement2");
        staffReportType.setStaffReportId(1L);
        StaffReportType rtn = service.updateStaffReport(uc, staffReportType);
        System.out.println("");
    }

    //@Test
    public void deleteStaffReport() {

        long flag = service.deleteStaffReport(uc, 1L);
        System.out.println(flag + "flag Value");
    }

    private StaffReportType generateDefaultStaffReport(String reportIncidentId, Long incidentReportStaffId) {
        StaffReportType staffReportType = new StaffReportType();
        staffReportType.setIncidentReportId(Long.parseLong(reportIncidentId));
        staffReportType.setIncidentReportStaffId(incidentReportStaffId);
        staffReportType.setStaffReportId(1L);
        return staffReportType;
    }

    @Test(dependsOnMethods = "CRUDIncidentReportPerson")
    public void CRUDJuridiction() {
        //test create
        jurisdiction = new JurisdictionType();
        jurisdiction.setCountry("USA");
        jurisdiction.setStateProvince("MD");
        jurisdiction.setDescription("Maryland");

        jurisdiction = service.createJurisdiction(uc, jurisdiction);

        Assert.assertNotNull(jurisdiction);
        Assert.assertNotNull(jurisdiction.getJurisdictionId());

        //test retrieve
        List<JurisdictionType> rtn = service.retrieveAllJurisdictions(uc);
        Assert.assertEquals(rtn.size(), 1);

        JurisdictionType jurisdictionType1 = new JurisdictionType();
        jurisdictionType1.setCountry("USA");
        jurisdictionType1.setStateProvince("WA");
        jurisdictionType1.setDescription("Washington");
        jurisdictionType1 = service.createJurisdiction(uc, jurisdictionType1);

        rtn = service.retrieveAllJurisdictions(uc);
        Assert.assertEquals(rtn.size(), 2);

        //test update
        jurisdictionType1.setCity("Vancouver");
        jurisdictionType1.setCountry("CAN");
        jurisdictionType1.setStateProvince("BC");
        jurisdictionType1.setCounty("131");
        jurisdictionType1.setDescription("vancouver jurisdiction");
        JurisdictionType updateRtn = service.updateJurisdiction(uc, jurisdictionType1);

        Assert.assertNotNull(updateRtn);
        Assert.assertEquals(jurisdictionType1.getJurisdictionId(), updateRtn.getJurisdictionId());

        verifyJurisdiction(updateRtn, jurisdictionType1);

        //test get single one
        Long jurisdictionId = updateRtn.getJurisdictionId();

        JurisdictionType getRtn = service.getJurisdiction(uc, jurisdictionId);

        Assert.assertNotNull(getRtn);
        Assert.assertEquals(updateRtn.getJurisdictionId(), getRtn.getJurisdictionId());
        verifyJurisdiction(getRtn, updateRtn);
    }

    private void verifyJurisdiction(JurisdictionType rtn, JurisdictionType expected) {
        Assert.assertEquals(expected.getCity(), rtn.getCity());
        Assert.assertEquals(expected.getCountry(), rtn.getCountry());
        Assert.assertEquals(expected.getStateProvince(), rtn.getStateProvince());
        Assert.assertEquals(expected.getDescription(), rtn.getDescription());
        Assert.assertEquals(expected.getCounty(), rtn.getCounty());
    }

    @Test //(dependsOnMethods = "CRUDJuridiction" ) //WOR-13655
    public void CRUDRegulation() {
        regulation = new RegulationType();
        //regulation.setDefaultJurisdiction(true); //WOR-13655
        regulation.setRegulationCode("PRIR");
        regulation.setRegulationDescription("Prison Rule And Regulation");
        regulation.setStartDate(getDate(1900, 1, 1));
        regulation.setEndDate(getDate(2500, 1, 1));
        //regulation.setJurisdictionId(jurisdiction.getJurisdictionId()); //WOR-13655

        regulation = service.createRegulation(uc, regulation);
        Assert.assertNotNull(regulation);
        Assert.assertNotNull(regulation.getRegulationId());

        //get
        RegulationType getRtn = service.getRegulation(uc, regulation.getRegulationId());
        Assert.assertNotNull(getRtn);
        Assert.assertEquals(getRtn.getRegulationId(), regulation.getRegulationId());

        //update
        getRtn.setStartDate(getDate(1922, 2, 2));
        getRtn.setEndDate(getDate(2014, 1, 1));
        RegulationType updateRtn = service.updateRegulation(uc, getRtn);
        Assert.assertNotNull(updateRtn);
        Assert.assertEquals(updateRtn.getRegulationId(), regulation.getRegulationId());
        Assert.assertEquals(updateRtn.getStartDate().getTime(), getRtn.getStartDate().getTime());

        //retrieve by jurisdiction id
        //		List<RegulationType> retrieveRtn = service.retrieveRegulationsByJurisdictionId(uc, jurisdiction.getJurisdictionId()); //WOR-13655
        //		Assert.assertNotNull(retrieveRtn);
        //		Assert.assertEquals(retrieveRtn.size(), 1);

        //retrieve by jurisdiction id
        List<RegulationType> retrieveRtn1 = service.retrieveRegulations(uc);
        Assert.assertNotNull(retrieveRtn1);
        //Assert.assertEquals(retrieveRtn1.size(), 1);
    }

    @Test(dependsOnMethods = "CRUDRegulation")
    public void CRUDRegulationRule() {
        regulationRule = new RegulationRuleType();
        regulationRule.setRuleCategory("CATA");
        regulationRule.setRuleType("BRCH");
        regulationRule.setRuleDescription("Murder");
        regulationRule.setRuleCode("C100");
        regulationRule.setStartDate(getDate(1900, 1, 1));
        regulationRule.setEndDate(getDate(2500, 1, 1));
        regulationRule.setRegulationId(regulation.getRegulationId());

        Set<SanctionType> sanctions = new HashSet<SanctionType>();
        SanctionType sanction1 = new SanctionType();
        sanction1.setMaxDays(365L);
        sanction1.setSanctionTypeCode("LP");

        SanctionType sanction2 = new SanctionType();
        sanction2.setMaxDays(365L);
        sanction2.setSanctionTypeCode("PS");

        SanctionType sanction3 = new SanctionType();
        sanction3.setMaxDays(200L);
        sanction3.setSanctionTypeCode("LGT");

        sanctions.add(sanction1);
        sanctions.add(sanction2);
        sanctions.add(sanction3);

        regulationRule.setRuleSanctions(sanctions);

        regulationRule1 = service.createRegulationRule(uc, regulationRule);
        Assert.assertNotNull(regulationRule1);
        Assert.assertNotNull(regulationRule1.getRegulationRuleId());

        regulationRule = service.createRegulationRule(uc, regulationRule);

        Assert.assertNotNull(regulationRule);
        Assert.assertNotNull(regulationRule.getRegulationRuleId());

        //get
        RegulationRuleType getRtn = service.getRegulationRule(uc, regulationRule.getRegulationRuleId());
        Assert.assertNotNull(getRtn);
        Assert.assertEquals(getRtn.getRegulationRuleId(), regulationRule.getRegulationRuleId());

        //update
        getRtn.setStartDate(getDate(1922, 2, 2));
        RegulationRuleType updateRtn = service.updateRegulationRule(uc, getRtn);
        Assert.assertNotNull(updateRtn);
        Assert.assertEquals(updateRtn.getRegulationRuleId(), regulationRule.getRegulationRuleId());
        Assert.assertEquals(updateRtn.getStartDate().getTime(), getRtn.getStartDate().getTime());

        //retrieve by regulation id
        List<RegulationRuleType> retrieveRtn = service.retrieveRegulationRulesByRegulationId(uc, regulation.getRegulationId());
        Assert.assertNotNull(retrieveRtn);
        Assert.assertEquals(retrieveRtn.size(), 2);
    }

    @Test(dependsOnMethods = "CRUDRegulationRule")
    public void CRUDInmateCharge() {
        //create with only mandatory
        IncidentReportType incidentReport = generateDefaultIncidentReport(true);

        IncidentReportType rtn = service.createIncidentReport(uc, incidentReport);

        verifyIncidentReport(rtn, incidentReport);

        //create IncidentReportInmate
        IncidentReportInmateType inmate = generateDefaultIncidentReportInmate(rtn.getIncidentReportId());

        IncidentReportInmateType ret = service.createIncidentReportInmate(uc, inmate);

        verifyInmate(ret, inmate);

        //Get regulations by incident report inmate id
        Long incidentReportInmateId = ret.getIncidentReportInmateId();

        List<RegulationType> regulations = service.getRegulationsByIncidentReportInmateId(uc, incidentReportInmateId);
        Assert.assertNotNull(regulations);
        Assert.assertEquals(1, regulations.size());

        Long regulationId = regulations.get(0).getRegulationId();

        //Get regulation rules by incident report inmate id and regulation id.
        List<RegulationRuleType> rules = service.getRegulationRulesByIncidentReportInmateIdAndRegulationId(uc, incidentReportInmateId, regulationId);
        Assert.assertNotNull(rules);
        Assert.assertTrue(rules.size() > 0);

        //Create inmate charge
        IncidentReportInmateChargeType inmateCharge = new IncidentReportInmateChargeType();
        inmateCharge.setEvidence("evidence");
        inmateCharge.setIncidentReportInmateId(ret.getIncidentReportInmateId());
        inmateCharge.setRegulationRuleId(regulationRule.getRegulationRuleId());

        inmateCharge = service.createIncidentReportInmateCharge(uc, inmateCharge);
        Assert.assertNotNull(inmateCharge);
        Assert.assertNotNull(inmateCharge.getIncidentReportInmateChargeId());
        Assert.assertEquals(ret.getIncidentReportInmateId(), inmateCharge.getIncidentReportInmateId());
        Assert.assertEquals("evidence", inmateCharge.getEvidence());
        Assert.assertEquals(regulationRule.getRegulationRuleId(), inmateCharge.getRegulationRuleId());

        //Get inmate charge by id
        Long incidentReportInmateChargeId = inmateCharge.getIncidentReportInmateChargeId();
        inmateCharge = service.getIncidentReportInmateCharge(uc, incidentReportInmateChargeId);
        Assert.assertNotNull(inmateCharge);

        //update inmate charge
        inmateCharge.setComment("comment1");
        inmateCharge.setEvidence("evidentce1");
        inmateCharge.setRegulationRuleId(regulationRule1.getRegulationRuleId());
        IncidentReportInmateChargeType updatedInmateCharge = service.updateIncidentReportInmateCharge(uc, inmateCharge);

        Assert.assertNotNull(updatedInmateCharge);
        Assert.assertNotNull(updatedInmateCharge.getIncidentReportInmateChargeId());
        Assert.assertEquals(inmateCharge.getIncidentReportInmateId(), updatedInmateCharge.getIncidentReportInmateId());
        Assert.assertEquals(inmateCharge.getEvidence(), updatedInmateCharge.getEvidence());
        Assert.assertEquals(inmateCharge.getComment(), updatedInmateCharge.getComment());
        Assert.assertEquals(regulationRule1.getRegulationRuleId(), updatedInmateCharge.getRegulationRuleId());

        //Get inmate charges by inmate id
        List<IncidentReportInmateChargeType> inmateCharges = service.getIncidentReportInmateChargesByInmateId(uc, incidentReportInmateId);
        Assert.assertNotNull(inmateCharges);
        Assert.assertTrue(inmateCharges.size() > 0);

        //Delete inmate charge by id
        Long deleted = service.deleteIncidentReportInmateCharge(uc, incidentReportInmateChargeId);
        Assert.assertNotNull(deleted);
        Assert.assertTrue(deleted.equals(1L));

        try {
            inmateCharge = service.getIncidentReportInmateCharge(uc, incidentReportInmateChargeId);
        } catch (Exception e) {
            Assert.assertTrue(e instanceof DataNotExistException);
        }
        incidentReportIds.add(rtn.getIncidentReportId());
    }

    @Test(dependsOnMethods = { "CRUDInmateCharge" }, enabled = true)
    public void searchIncidentReport() {
        for (Long id : incidentReportIds) {
            service.deleteIncidentReport(uc, id);
        }

        incidentReportIds.clear();

        //create with only mandatory
        IncidentReportType incidentReport = generateDefaultIncidentReport(false);

        IncidentReportType rtn1 = service.createIncidentReport(uc, incidentReport);

        verifyIncidentReport(rtn1, incidentReport);

        Long incidentReportId1 = rtn1.getIncidentReportId();
        incidentReportIds.add(incidentReportId1);
        //create with all fields
        incidentReport = generateDefaultIncidentReport(false);

        IncidentReportType rtn2 = service.createIncidentReport(uc, incidentReport);

        verifyIncidentReport(rtn2, incidentReport);

        Long incidentReportId2 = rtn2.getIncidentReportId();
        incidentReportIds.add(incidentReportId2);

        //test search
        IncidentReportSearchType search = null;
        IncidentReportsReturnType ret = null;

        //search by incident number exact
        search = new IncidentReportSearchType();
        search.setIncidentNumber(rtn1.getIncidentNumber());

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertNotNull(ret);
        Assert.assertEquals(new Long(1L), ret.getTotalSize());
        Assert.assertEquals(rtn1.getIncidentNumber(), ret.getIncidentReports().get(0).getIncidentNumber());

        //search by incident number wildcard
        search = new IncidentReportSearchType();
        search.setIncidentNumber("IR*");

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertNotNull(ret);
        Assert.assertEquals(new Long(2L), ret.getTotalSize());

        //search by from and to date
        search = new IncidentReportSearchType();
        search.setIncidentFromDateTime(getDate(2012, 01, 01));
        search.setIncidentToDateTime(getDate(2020, 01, 01));

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertNotNull(ret);
        Assert.assertEquals(new Long(2L), ret.getTotalSize());

        //search Reportedby
        search = new IncidentReportSearchType();
        search.setReportedByFirstName("Jason*");

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertNotNull(ret);
        Assert.assertEquals(new Long(2L), ret.getTotalSize());

        //search by Reportedby and incident number
        search = new IncidentReportSearchType();
        search.setReportedByFirstName("Jason*");
        search.setIncidentNumber(rtn1.getIncidentNumber());

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertNotNull(ret);
        Assert.assertEquals(new Long(1L), ret.getTotalSize());

        //search by inmate first name
        //create IncidentReportInmate
        IncidentReportInmateType inmate = generateDefaultIncidentReportInmate(incidentReportId2);
        IncidentReportInmateType inmateRtn = service.createIncidentReportInmate(uc, inmate);
        verifyInmate(inmateRtn, inmate);

        search = new IncidentReportSearchType();
        search.setInmateInvolvedFirstName("Jason*");

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertNotNull(ret);
        Assert.assertEquals(new Long(1L), ret.getTotalSize());
        Assert.assertEquals(incidentReportId2, ret.getIncidentReports().get(0).getIncidentReportId());

        //search by inmate first name and incident number
        search = new IncidentReportSearchType();
        search.setInmateInvolvedFirstName("Jason*");
        search.setIncidentNumber(rtn2.getIncidentNumber());

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertNotNull(ret);
        Assert.assertEquals(new Long(1L), ret.getTotalSize());
        Assert.assertEquals(incidentReportId2, ret.getIncidentReports().get(0).getIncidentReportId());

        //search by inmate first name and incident number with no data can be found.
        search = new IncidentReportSearchType();
        search.setInmateInvolvedFirstName("Jason*");
        search.setIncidentNumber(rtn1.getIncidentNumber());

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertNotNull(ret);
        Assert.assertEquals(new Long(0), ret.getTotalSize());

        //search by inmate first name and reported by first name.
        search = new IncidentReportSearchType();
        search.setInmateInvolvedFirstName("Jason*");
        search.setReportedByFirstName("Jason*");

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertEquals(new Long(1L), ret.getTotalSize());
        Assert.assertEquals(incidentReportId2, ret.getIncidentReports().get(0).getIncidentReportId());

        //search by facilityId
        search = new IncidentReportSearchType();
        search.setIncidentFacilityId(facilityId);

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertEquals(new Long(2L), ret.getTotalSize());

        //search by facilityInternalLocationId
        search = new IncidentReportSearchType();
        search.setIncidentFacilityInternalLocationId(facilityInternalLocationId);

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertEquals(new Long(2L), ret.getTotalSize());

        //search by facility set
        search = new IncidentReportSearchType();
        List<Long> facilitySetIds = new ArrayList<Long>();
        facilitySetIds.add(facilityInternalLocationId);
        search.setFacilitySetIds(facilitySetIds);

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertEquals(new Long(2L), ret.getTotalSize());
    }

    @Test
    public void testAddPhoto() {
        Long incidentReportId = 1L;

        // Set/get photo
        byte[] img1 = new byte[] { 1, 1, 1, 1 };

        IncidentReportImageType incidentReportImageType = new IncidentReportImageType();
        incidentReportImageType.setIncidentReportId(incidentReportId);
        incidentReportImageType.setDefaultImage(true);
        incidentReportImageType.setStaff(staffId);
        incidentReportImageType.setImageNumber("1234");
        incidentReportImageType.setImageDescription("eventLogImageDescription");
        incidentReportImageType.setUploadedDate(new Date());
        incidentReportImageType.setData(img1);
        incidentReportImageType = service.addPhoto(uc, incidentReportImageType);
        assert (incidentReportImageType != null);
    }

    @Test
    public void testUpdatePhoto() {
        Long incidentReportId = 1L;

        byte[] img1 = new byte[] { 1, 1, 1, 1 };
        byte[] img2 = new byte[] { 1, 1, 1, 1 };

        IncidentReportImageType incidentReportImageType = new IncidentReportImageType();
        incidentReportImageType.setIncidentReportId(incidentReportId);
        incidentReportImageType.setDefaultImage(true);
        incidentReportImageType.setStaff(staffId);
        incidentReportImageType.setImageNumber("1234");
        incidentReportImageType.setImageDescription("eventLogImageDescription");
        incidentReportImageType.setUploadedDate(new Date());
        incidentReportImageType.setData(img1);
        incidentReportImageType = service.addPhoto(uc, incidentReportImageType);
        assert (incidentReportImageType != null);

        incidentReportImageType.setData(img2);
        IncidentReportImageType retEdit = service.updatePhoto(uc, incidentReportImageType);
        assert (retEdit != null);
    }

    @Test
    public void DeletePhoto() {
        Long incidentReportId = 1L;

        // Set/get photo
        byte[] img1 = new byte[] { 1, 1, 1, 1 };

        IncidentReportImageType incidentReportImageType = new IncidentReportImageType();
        incidentReportImageType.setIncidentReportId(incidentReportId);
        incidentReportImageType.setDefaultImage(true);
        incidentReportImageType.setStaff(staffId);
        incidentReportImageType.setImageNumber("1234");
        incidentReportImageType.setImageDescription("eventLogImageDescription");
        incidentReportImageType.setUploadedDate(new Date());
        incidentReportImageType.setData(img1);
        incidentReportImageType = service.addPhoto(uc, incidentReportImageType);
        assert (incidentReportImageType != null);

        Long retCode = service.deletePhoto(uc, incidentReportImageType.getImageId());
        assert (retCode > 0);
    }

    @Test
    public void testGetPhotos() {
        Long incidentReportId = 1L;

        // Set/get photo
        byte[] img1 = new byte[] { 1, 1, 1, 1 };

        IncidentReportImageType incidentReportImageType = new IncidentReportImageType();
        incidentReportImageType.setIncidentReportId(incidentReportId);
        incidentReportImageType.setDefaultImage(true);
        incidentReportImageType.setStaff(staffId);
        incidentReportImageType.setImageNumber("1234");
        incidentReportImageType.setImageDescription("eventLogImageDescription");
        incidentReportImageType.setUploadedDate(new Date());
        incidentReportImageType.setData(img1);
        incidentReportImageType = service.addPhoto(uc, incidentReportImageType);
        incidentReportImageType = service.addPhoto(uc, incidentReportImageType);
        incidentReportImageType = service.addPhoto(uc, incidentReportImageType);
        List<IncidentReportImageType> list = new ArrayList<IncidentReportImageType>();

        list = service.getPhotos(uc, incidentReportId);
        assert (list != null);
    }

    @Test
    public void testGetPhoto() {
        Long incidentReportId = 1L;
        // Set/get photo
        byte[] img1 = new byte[] { 1, 1, 1, 1 };

        IncidentReportImageType incidentReportImageType = new IncidentReportImageType();
        incidentReportImageType.setIncidentReportId(incidentReportId);
        incidentReportImageType.setDefaultImage(true);
        incidentReportImageType.setStaff(staffId);
        incidentReportImageType.setImageNumber("1234");
        incidentReportImageType.setImageDescription("eventLogImageDescription");
        incidentReportImageType.setUploadedDate(new Date());
        incidentReportImageType.setData(img1);
        incidentReportImageType = service.addPhoto(uc, incidentReportImageType);

        IncidentReportImageType photo = service.getPhoto(uc, incidentReportImageType.getImageId());
        assert (photo != null);
    }

    @Test(dependsOnMethods = "searchIncidentReport", enabled = true)
    public void testAppendIncidentReport() {
        //create with only mandatory
        IncidentReportType incidentReport = generateDefaultIncidentReport(true);

        IncidentReportType rtn1 = service.createIncidentReport(uc, incidentReport);

        verifyIncidentReport(rtn1, incidentReport);

        Long incidentReportId = rtn1.getIncidentReportId();
        incidentReportIds.add(incidentReportId);

        //append incident report
        Long personIdentityId = personIdentityTest.createPersonIdentity();
        IncidentReportAppendantType appendant = new IncidentReportAppendantType();
        appendant.setAppendNarrative("bbb");
        appendant.setAppendPersonIdentityId(personIdentityId);
        appendant.setIncidentReportId(incidentReportId);

        Long rtn = service.appendIncidentReport(uc, appendant);
        Assert.assertEquals(new Long(1L), rtn);

        //get appendants
        List<IncidentReportAppendantType> listRtn = service.getAppendantListForIncidentReport(uc, incidentReportId);
        Assert.assertNotNull(listRtn);
        Assert.assertEquals(1, listRtn.size());

        //append incident report
        IncidentReportAppendantType appendant1 = new IncidentReportAppendantType();
        appendant1.setAppendNarrative("ccc");
        appendant1.setAppendPersonIdentityId(personIdentityId);
        appendant1.setIncidentReportId(incidentReportId);

        Long rtn2 = service.appendIncidentReport(uc, appendant1);
        Assert.assertEquals(new Long(1L), rtn2);

        //get appendants
        List<IncidentReportAppendantType> listRtn1 = service.getAppendantListForIncidentReport(uc, incidentReportId);
        Assert.assertNotNull(listRtn1);
        Assert.assertEquals(2, listRtn1.size());

    }

    @AfterClass
    public void afterClass() {
        service.deleteAll(uc);
        supervisionTest.deleteAll();
        staffTest.deleteAll();
        personIdentityTest.deleteAll();
        psnTest.deleteAll();
    }

    @Test
    public void testCreateOffenseInCustody() {
        OffenseInCustodyType type = new OffenseInCustodyType();
        type.setCreatedById(staffId);
        type.setCustodyDate(new Date());
        type.setFacilityId(facilityId);
        type.setLocationId(facilityInternalLocationId);
        type.setOffenseType("OT");
        type.setOicNumber("202");
        //type.setOutcome(entity.getOicOutcome());
        //type.setRelatedIncidentId(entity.getRelatedIncidentId());
        type.setStatus(OICStatus.ACT.name());
        type.setSupervisionId(supervisionId);
        offenseInCustodyType = service.createOffenseInCustody(uc, type);
        assertNotNull(offenseInCustodyType);

    }

    @Test(dependsOnMethods = "testCreateOffenseInCustody", enabled = true)
    public void testGetOffenseInCustody() {
        OffenseInCustodyType ret = service.getOffenseInCustody(uc, offenseInCustodyType.getId());
        assertNotNull(ret);
    }

    @Test(dependsOnMethods = "testCreateOffenseInCustody", enabled = true)
    public void testSearchOffenseInCustody() {
        OffenseInCustodySearchType searchCriteria = new OffenseInCustodySearchType();
        searchCriteria.setSupervisionId(supervisionId);
        OffenseInCustodyReturnType ret = service.searchOffenseInCustody(uc, searchCriteria, null, null, null);
        assertNotNull(ret);
        Assert.assertEquals(ret.getTotalSize(), (Long) 1l);
    }

    @Test
    public void updateGetOffenseInCustody() {
        OffenseInCustodyType type = service.getOffenseInCustody(uc, offenseInCustodyType.getId());
        assertNotNull(type);
        type.setOicNumber("1001");
        type.setStatus(OICStatus.INACT.name());
        type.setComments("My Comments");
        OffenseInCustodyType ret = service.updateOffenseInCustody(uc, type);
        assertNotNull(ret);
        Assert.assertEquals(ret.getOicNumber(), (Long) 1001l);
    }

    @Test(dependsOnMethods = "CRUDRegulation", enabled = true)
    public void testDeleteOffenseInCustody() {
        OffenseInCustodyType type = new OffenseInCustodyType();
        type.setCreatedById(staffId);
        type.setCustodyDate(new Date());
        type.setFacilityId(facilityId);
        type.setLocationId(277l);
        type.setOffenseType("OT");
        type.setStatus(OICStatus.INACT.name());
        type.setOicNumber("202");
        type.setSupervisionId(supervisionId);
        OffenseInCustodyType ret = service.createOffenseInCustody(uc, type);
        assertNotNull(ret);
        service.deleteOffenseInCustody(uc, ret.getId());
    }

    @Test(dependsOnMethods = { "CRUDRegulation", "testCreateOffenseInCustody" }, enabled = true)
    public void testCreateOffenseInCustodyCharge() {
        OffenseInCustodyChargeType type = new OffenseInCustodyChargeType();
        type.setOffenseDate(new Date());
        type.setOicCaseId(offenseInCustodyType.getId());
        type.setOicChargeCategory("Category");
        type.setOicChargeCode("code");
        type.setOicChargeDescription("description");
        type.setOicChargeOutcome("outcome");
        type.setOicChargeStatus("ACT");
        type.setOicChargeType("type");
        type.setRegulationId(regulation.getRegulationId());
        charge = service.createOffenseInCustodyCharge(uc, type);
        assertNotNull(charge);
    }

    @Test(dependsOnMethods = "testCreateOffenseInCustody", enabled = true)
    public void testCreateOffenseInCustodyHearing() {
        OffenseInCustodyHearingType type = new OffenseInCustodyHearingType();
        //type.setHearingComments("my comments");
        type.setHearingDate(new Date());
        type.setHearingOutcome("outcome");
        type.setHearingStatus("status");
        type.setHearingType("type");
        type.setOicId(offenseInCustodyType.getId());
        //type.setPresidingOfficer("Filst Last Name");
        type.setFacilityId(facilityId);
        type.setLocationId(facilityInternalLocationId);
        CourtMovementType courtMovementType = new CourtMovementType();
        courtMovementType.setCourtPartRoom(facilityInternalLocationId);
        courtMovementType.setBypassConflicts(true);
        courtMovementType.setFromFacilityId(2l);
        courtMovementType.setToFacilityId(newOccurredFacilityInternalLocationId);
        courtMovementType.setMoveDate(new Date());
        courtMovementType.setReturnDate(new Date());
        courtMovementType.setMovementReason("COURT");
        courtMovementType.setMovementInStatus("PENDING");
        courtMovementType.setMovementOutStatus("PENDING");
        courtMovementType.setMovementType("VISIT");
        courtMovementType.setSupervisionId(supervisionId);
        type.setCourtMovementType(courtMovementType);
        hearing = service.createOffenseInCustodyHearing(uc, type);
        assertNotNull(hearing);
    }

    @Test(dependsOnMethods = "testCreateOffenseInCustodyCharge", enabled = true)
    public void testUpdateOffenseInCustodyCharge() {
        charge.setOicChargeCategory("Category1");
        charge.setOicChargeCode("code1");
        charge.setOicChargeDescription("description 1");
        charge.setOicChargeOutcome("outcome1");
        charge.setOicChargeStatus("INACT");
        charge.setOicChargeType("type1");
        OffenseInCustodyChargeType ret = service.createOffenseInCustodyCharge(uc, charge);
        assertNotNull(ret);
    }

    @Test(dependsOnMethods = "testCreateOffenseInCustodyHearing", enabled = true)
    public void testUpdateOffenseInCustodyHearing() {
        hearing.setHearingComments("new hearingComments.");
        Date newDate = new Date(new Date().getTime() + 100000l);
        hearing.setHearingDate(newDate);
        CourtMovementType courtMovementType = new CourtMovementType();
        courtMovementType.setMoveDate(newDate);
        courtMovementType.setReturnDate(newDate);
        hearing.setCourtMovementType(courtMovementType);
        OffenseInCustodyHearingType ret = service.updateOffenseInCustodyHearing(uc, hearing);
        assertNotNull(ret);
    }

    @Test(dependsOnMethods = "testCreateOffenseInCustody", enabled = true)
    public void testCreateOffenseInCustodyInvestigator() {
        OffenseInCustodyInvestigatorType type = new OffenseInCustodyInvestigatorType();
        type.setAssignedDate(new Date());
        type.setComments("my comments");
        type.setInvestigatorId(1l);
        type.setOicId(offenseInCustodyType.getId());
        investigator = service.createOffenseInCustodyInvestigator(uc, type);
        assertNotNull(investigator);
    }

    @Test(dependsOnMethods = "testCreateOffenseInCustodyInvestigator", enabled = true)
    public void testGetOffenseInCustodyInvestigator() {
        investigator = service.getOffenseInCustodyInvestigator(uc, investigator.getId());
        assertNotNull(investigator);
    }

    @Test(dependsOnMethods = { "testCreateOffenseInCustody", "testCreateOffenseInCustodyCharge", "testCreateOffenseInCustodyHearing" }, enabled = true)
    public void testCreateOffenseInCustodyPenalty() {
        OffenseInCustodyPenaltyType type = new OffenseInCustodyPenaltyType();
        type.setAmount(new BigDecimal(100));
        type.setDetails("details");
        type.setImposedDate(new Date());
        type.setOutcome("outcome");
        type.setPenaltyEndDate(new Date());
        type.setPenaltyStartDate(new Date());
        type.setPenaltyType("penaltyType");
        type.setStatus("status");
        type.setTerm(5l);
        type.setUnit("Years");
        type.setOicCharge(charge.getId());
        //type.setOicCharge(6l);
        List<Long> hearings = new ArrayList<Long>();
        hearings.add(hearing.getId());
        //hearings.add(1l);
        type.setOicHearings(hearings);
        type.setOicId(offenseInCustodyType.getId());
        //type.setOicId(4l);
        type.setPenaltyNumber(service.getLastPenaltyNumberBySupervisionAndOic(uc, offenseInCustodyType.getSupervisionId(), offenseInCustodyType.getId()));
        //type.setPenaltyNumber(service.getLastPenaltyNumberBySupervisionAndOic(uc, 5l, 4l) + 1l);
        penalty = service.createOffenseInCustodyPenalty(uc, type);
        assertNotNull(penalty);
    }

    @Test(dependsOnMethods = "testCreateOffenseInCustodyPenalty", enabled = true)
    public void testCreateOffenseInCustodyPenaltyWithParent() {
        charge.setOicChargeCategory("Category1");
        charge.setOicChargeCode("code1");
        charge.setOicChargeDescription("description 1");
        charge.setOicChargeOutcome("outcome1");
        charge.setOicChargeStatus("INACT");
        charge.setOicChargeType("type1");
        OffenseInCustodyChargeType tempCharge = service.createOffenseInCustodyCharge(uc, charge);

        OffenseInCustodyPenaltyType type = new OffenseInCustodyPenaltyType();
        type.setAmount(new BigDecimal(100));
        type.setDetails("child: details");
        type.setImposedDate(new Date());
        type.setOutcome("outcome");
        type.setPenaltyEndDate(new Date());
        type.setPenaltyStartDate(new Date());
        type.setPenaltyType("penaltyType");
        type.setStatus("status");
        type.setTerm(5l);
        type.setUnit("Years");
        type.setOicCharge(tempCharge.getId());
        List<Long> hearings = new ArrayList<Long>();
        hearings.add(hearing.getId());
        type.setOicHearings(hearings);
        type.setOicId(offenseInCustodyType.getId());
        //type.setParentId(penalty.getId());
        type.setParentId(penalty.getId());
        type.setPenaltyNumber(service.getLastPenaltyNumberBySupervision(uc, offenseInCustodyType.getSupervisionId()));
        OffenseInCustodyPenaltyType ret = service.createOffenseInCustodyPenalty(uc, type);
        assertNotNull(ret);
    }

    @Test(dependsOnMethods = "testCreateOffenseInCustodyPenalty", enabled = true)
    public void testGetOffenseInCustodyPenalty() {
        OffenseInCustodyPenaltyType type = service.getOffenseInCustodyPenalty(uc, penalty.getId());
        assertNotNull(type);
    }

    @Test(dependsOnMethods = "testCreateOffenseInCustody", enabled = true)
    public void testCreateInvestigationNote() {
        InvestigationNoteType type = new InvestigationNoteType();
        type.setInvestigatorId(1l);
        type.setNoteDate(new Date());
        type.setNotes("Note B");
        type.setOicId(offenseInCustodyType.getId());
        note = service.createOICInvestigationNote(uc, type);
        assertNotNull(note);
    }

    @Test(dependsOnMethods = "testCreateInvestigationNote", enabled = true)
    public void testAmendInvestigationNote() {
        note.setNotes("Modified Note");
        InvestigationNoteType ret = service.amendOICInvestigationNote(uc, note);
        assertNotNull(ret);
    }

    @Test //(dependsOnMethods = "testCreateInvestigationNote", enabled = true)
    public void testGetInvestigationNote() {
        InvestigationNoteType ret = service.getOICInvestigationNote(uc, 1l);
        assertNotNull(ret);
    }

    @Test(dependsOnMethods = "testCreateOffenseInCustodyPenalty", enabled = true)
    public void testUpdateOffenseInCustodyPenalty() {
        penalty.setAmount(new BigDecimal(101));
        penalty.setDetails("child: details updated");
        penalty.setImposedDate(new Date());
        penalty.setOutcome("outcome 1");
        penalty.setPenaltyEndDate(new Date());
        penalty.setPenaltyStartDate(new Date());
        penalty.setStatus("status 1");
        penalty.setTerm(5l);
        penalty.setUnit("Month");
        penalty.setOicCharge(penalty.getId());
        //penalty.setParentId(45l);
        List<Long> hearings = new ArrayList<Long>();
        hearings.add(hearing.getId());
        penalty.setOicHearings(hearings);
        OffenseInCustodyPenaltyType ret = service.updateOffenseInCustodyPenalty(uc, penalty);
        assertNotNull(ret);
    }

    @Test(dependsOnMethods = "testCreateOffenseInCustodyInvestigator", enabled = true)
    public void testCreateInvestigationResult() {
        InvestigationResultType type = new InvestigationResultType();
        type.setDamageDescription("Description");
        type.setCreatedById(1l);
        type.setInvestigatorId(investigator.getId());
        type.setOutcome("outcome");
        type.setEstimatedDamages(BigDecimal.TEN);
        investigationResult = service.createOICInvestigationResult(uc, type);
        assertNotNull(investigationResult);
    }

    @Test(dependsOnMethods = "testCreateInvestigationResult", enabled = true)
    public void testGetInvestigationResultsByInvestigatorId() {
        InvestigationResultReturnType ret = service.getInvestigationResultsByInvestigatorId(uc, investigator.getId());
        assertNotNull(ret);
    }

    @Test(dependsOnMethods = "testCreateInvestigationResult", enabled = true)
    public void testGetOICInvestigationResult() {
        InvestigationResultType ret = service.getOICInvestigationResult(uc, investigationResult.getId());
        assertNotNull(ret);
    }

    //////////////////////////////////////////////Private methods////////////////////////////////////
    private void verifyStaff(IncidentReportStaffType rtn, IncidentReportStaffType staff) {
        Assert.assertNotNull(rtn);
        Assert.assertNotNull(rtn.getIncidentReportStaffId());
        Assert.assertEquals(rtn.getComment(), staff.getComment());
        Assert.assertEquals(rtn.getRole(), staff.getRole());
        Assert.assertEquals(rtn.getIncidentReportId(), staff.getIncidentReportId());
        Assert.assertEquals(rtn.getStaffId(), staff.getStaffId());
        Assert.assertEquals(rtn.getStaffReportId(), staff.getStaffReportId());
        Assert.assertNotNull(rtn.getStaff());
        Assert.assertEquals(rtn.getMedicalAttentionRequired(), staff.getMedicalAttentionRequired());

    }

    private IncidentReportStaffType generateDefaultIncidentReportStaff(Long incidentReportId) {
        IncidentReportStaffType staff = new IncidentReportStaffType();
        staff.setIncidentReportId(incidentReportId);
        staff.setComment("comment1");
        staff.setRole(ROLE);
        staff.setStaffId(staffId);
        staff.setStaffReportId(staffReportId);
        staff.setMedicalAttentionRequired(MEDICAL_ATTN);

        return staff;
    }

    private void verifyPerson(IncidentReportPersonType rtn, IncidentReportPersonType person) {
        Assert.assertNotNull(rtn);
        Assert.assertNotNull(rtn.getIncidentReportPersonId());
        Assert.assertEquals(rtn.getComment(), person.getComment());
        Assert.assertEquals(rtn.getRole(), person.getRole());
        Assert.assertEquals(rtn.getIncidentReportId(), person.getIncidentReportId());
        Assert.assertEquals(rtn.getPersonIdentityId(), person.getPersonIdentityId());
        Assert.assertNotNull(rtn.getIndividual());
        Assert.assertEquals(rtn.getMedicalAttentionRequired(), person.getMedicalAttentionRequired());
    }

    private IncidentReportPersonType generateDefaultIncidentReportPerson(Long incidentReportId) {
        IncidentReportPersonType person = new IncidentReportPersonType();
        person.setIncidentReportId(incidentReportId);
        person.setComment("comment1");
        person.setPersonIdentityId(personIdentityId);
        person.setRole(ROLE);
        person.setMedicalAttentionRequired(MEDICAL_ATTN);

        return person;
    }

    private void verifyInmate(IncidentReportInmateType rtn, IncidentReportInmateType inmate) {
        Assert.assertNotNull(rtn);
        Assert.assertNotNull(rtn.getIncidentReportInmateId());
        Assert.assertEquals(rtn.getActionTaken(), inmate.getActionTaken());
        Assert.assertEquals(rtn.getComment(), inmate.getComment());
        Assert.assertEquals(rtn.getRole(), inmate.getRole());
        Assert.assertEquals(rtn.getHearingDateTime().getTime(), inmate.getHearingDateTime().getTime());
        Assert.assertEquals(rtn.getIncidentReportId(), inmate.getIncidentReportId());
        Assert.assertEquals(rtn.getPersonIdentityId(), inmate.getPersonIdentityId());
        Assert.assertNotNull(rtn.getIndividual());
        Assert.assertEquals(rtn.getMedicalAttentionRequired(), inmate.getMedicalAttentionRequired());
        Assert.assertEquals(inmate.isUseOfForce(), rtn.isUseOfForce());

    }

    private IncidentReportInmateType generateDefaultIncidentReportInmate(Long incidentReportId) {
        IncidentReportInmateType inmate = new IncidentReportInmateType();
        inmate.setIncidentReportId(incidentReportId);
        inmate.setActionTaken(ACTION_TAKEN);
        inmate.setComment("comment1");
        inmate.setHearingDateTime(new Date());
        inmate.setPersonIdentityId(personIdentityId);
        inmate.setRole(ROLE);
        inmate.setMedicalAttentionRequired(MEDICAL_ATTN);
        inmate.setUseOfForce(USE_OF_FORCE);

        return inmate;
    }

    private void verifyIncidentReport(IncidentReportType rtn, IncidentReportType incidentReport) {
        Assert.assertNotNull(rtn);
        Assert.assertNotNull(rtn.getIncidentReportId());
        Assert.assertEquals(rtn.getAssociatedShiftLogId(), incidentReport.getAssociatedShiftLogId());
        Assert.assertEquals(rtn.getIncidentNumber(), incidentReport.getIncidentNumber());
        Assert.assertEquals(rtn.getIncidentSubType(), incidentReport.getIncidentSubType());
        Assert.assertEquals(rtn.getIncidentType(), incidentReport.getIncidentType());
        Assert.assertEquals(rtn.getNarrative(), incidentReport.getNarrative());
        Assert.assertEquals(rtn.getOccurredDateTime().getTime(), incidentReport.getOccurredDateTime().getTime());
        Assert.assertEquals(rtn.getOccurredFacilityId(), incidentReport.getOccurredFacilityId());
        Assert.assertEquals(rtn.getOccurredFacilityInternalLocationId(), incidentReport.getOccurredFacilityInternalLocationId());
        Assert.assertEquals(rtn.getReportedBy(), incidentReport.getReportedBy());
        Assert.assertNotNull(rtn.getReportedDateTime());
        Assert.assertEquals(rtn.isIndicatorOfSTG(), incidentReport.isIndicatorOfSTG());
        Assert.assertEquals(rtn.getLocationDescription(), incidentReport.getLocationDescription());
        //Assert.assertEquals(rtn.isUseOfForce() , incidentReport.isUseOfForce());
    }

    private IncidentReportType generateDefaultIncidentReport(boolean onlyMadatory) {
        IncidentReportType incidentReport = new IncidentReportType();

        incidentReport.setIncidentType(INCIDENT_TYPE);
        incidentReport.setNarrative(NARRATIVE);
        incidentReport.setOccurredDateTime(occurredDateTime);
        incidentReport.setOccurredFacilityId(facilityId);
        incidentReport.setOccurredFacilityInternalLocationId(facilityInternalLocationId);
        incidentReport.setReportedBy(staffId);
        incidentReport.setLocationDescription("default location");

        if (onlyMadatory) {
            return incidentReport;
        }

        incidentReport.setIncidentSubType(INCIDENT_SUB_TYPE);
        incidentReport.setIndicatorOfSTG(INDICATOR_OF_STG);
        incidentReport.setUseOfForce(USE_OF_FORCE);
        incidentReport.setAssociatedShiftLogId(shiftLogId);
        incidentReport.setIncidentNumber(getIncidentReportNumber());

        //ignored: incidentReport.setReportedDateTime(reportedDateTime);

        return incidentReport;
    }

    //for release
    private void lookupJNDILogin() {

        log.info("Before JNDI lookup");
        service = (DisciplineService) JNDILookUp(this.getClass(), DisciplineService.class);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();
        clientlogin("devtest", "devtest");
    }

    private void clientlogin(String user, String password) {

        SecurityClient client;

        try {
            client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password); //"devtest", "devtest" nice regular service user
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Date getDate(int year, int month, int date) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, date);
        Date rtn = cal.getTime();
        return rtn;
    }

    @Test
    public void testSearchIncidentReport() {
        IncidentReportSearchType search = new IncidentReportSearchType();
        search.setInmateInvolvedFirstName("Jason*");

        IncidentReportsReturnType ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertNotNull(ret);
        Assert.assertEquals(new Long(1L), ret.getTotalSize());
    }

    @Test(dependsOnMethods = "testCreateOffenseInCustody", enabled = true)
    public void testSearchOffenseInCustody2() {
        OffenseInCustodySearchType searchCriteria = new OffenseInCustodySearchType();
        searchCriteria.setFacilityId(offenseInCustodyType.getFacilityId());
        searchCriteria.setCreatedById(offenseInCustodyType.getCreatedById());
        searchCriteria.setInmateFirstName("Jason*");
        searchCriteria.setLocationId(offenseInCustodyType.getLocationId());
        searchCriteria.setOffenseType(offenseInCustodyType.getOffenseType());
        searchCriteria.setOicNumber(offenseInCustodyType.getOicNumber());
        OffenseInCustodyReturnType ret = service.searchOffenseInCustody(uc, searchCriteria, null, null, null);
        assertNotNull(ret);
        Assert.assertEquals(ret.getTotalSize(), (Long) 1l);
    }

    @Test(dependsOnMethods = "testCreateOffenseInCustody", enabled = true)
    public void testSearchOffenseInCustody3() {
        OffenseInCustodySearchType searchCriteria = new OffenseInCustodySearchType();
        searchCriteria.setCreatedById(offenseInCustodyType.getCreatedById());
        searchCriteria.setFromDate(new Date());
        searchCriteria.setToDate(new Date());
        searchCriteria.setInmateFirstName("Jason*");
        searchCriteria.setInmateLastName("Jason*");
        searchCriteria.setLocationId(offenseInCustodyType.getLocationId());
        searchCriteria.setOffenseType(offenseInCustodyType.getOffenseType());
        searchCriteria.setOicNumber(offenseInCustodyType.getOicNumber());
        Set<Long> facilities = new HashSet<Long>();
        facilities.add(1L);
        facilities.add(2L);
        facilities.add(3L);
        searchCriteria.setFacilitySets(facilities);
        OffenseInCustodyReturnType ret = service.searchOffenseInCustody(uc, searchCriteria, null, null, null);
        assertNotNull(ret);
        Assert.assertEquals(ret.getTotalSize(), (Long) 0l);
    }

    private enum OICStatus {ACT, INACT}
}
