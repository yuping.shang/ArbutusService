package syscon.arbutus.product.services.discipline.contract.ejb;

import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.discipline.contract.dto.incidentreport.ReportableIncidentType;
import syscon.arbutus.product.services.discipline.contract.interfaces.DisciplineService;

import static org.testng.Assert.assertNotNull;

public class ReportableIncidentRuleIT extends BaseIT {

    private UserContext uc = null;
    private Logger log = LoggerFactory.getLogger(ReportableIncidentRuleIT.class);

    private Date startDate;
    private DisciplineService service;

    @BeforeClass
    public void beforeClass() {
        lookupJNDILogin();
        assertNotNull(service);
        startDate = new Date();

    }

    @Test
    public void createReportableIncidentRule() {
        ReportableIncidentType reportableIncidentType = new ReportableIncidentType();
        reportableIncidentType.setIncidentType("Death");
        reportableIncidentType.setIncidentSubType("Suicide");
        reportableIncidentType.setQuestionaireType("Reportable Incident");
        reportableIncidentType.setQuestionaireSubType("Dealth Inquiry");
        reportableIncidentType.setStartDate(new Date());
        reportableIncidentType.setEndDate(new Date());
        reportableIncidentType.setReportableDescription("asdfhaksljdhfjk");
        ReportableIncidentType rtn1 = service.createReportableIncident(uc, reportableIncidentType);
        System.out.println("rtn1:::" + rtn1.toString());

    }

    private Date getDate(int year, int month, int date) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, date);
        Date rtn = cal.getTime();
        return rtn;
    }

    //for release
    private void lookupJNDILogin() {

        log.info("Before JNDI lookup");
        service = (DisciplineService) JNDILookUp(this.getClass(), DisciplineService.class);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();
        clientlogin("devtest", "devtest");
    }

    private void clientlogin(String string, String string2) {

        SecurityClient client;
        try {
            client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(string, string2);
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
