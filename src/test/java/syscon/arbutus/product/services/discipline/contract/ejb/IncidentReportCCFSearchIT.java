package syscon.arbutus.product.services.discipline.contract.ejb;

import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldMetaType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.DTOBindingTypeEnum;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.discipline.contract.dto.incidentreport.IncidentReportSearchType;
import syscon.arbutus.product.services.discipline.contract.dto.incidentreport.IncidentReportsReturnType;
import syscon.arbutus.product.services.discipline.contract.interfaces.DisciplineService;

import static org.testng.Assert.assertNotNull;

/**
 * IncidentReportCCFSearchIT to test search incident report by CCF.
 * The Pre-condition is manually created through UI, so this test won't be included in IT test and can only be run manually.
 *
 * @author lhan
 */
public class IncidentReportCCFSearchIT extends BaseIT {
    private UserContext uc = null;
    private Logger log = LoggerFactory.getLogger(IncidentReportCCFSearchIT.class);

    private DisciplineService service;

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();
        assertNotNull(service);
    }

    @Test
    public void getVersion() {
        String rtn = service.getVersion(uc);
        Assert.assertEquals(rtn, "1.0");
    }

    @Test(enabled = false)
    public void searchIncidentReport() {
        //test search
        IncidentReportSearchType search = null;
        IncidentReportsReturnType ret = null;

        //search by ccf fields
        search = new IncidentReportSearchType();
        Set<ClientCustomFieldValueType> cCFSearchCriteria = new HashSet<ClientCustomFieldValueType>();
        ClientCustomFieldValueType ccfValueType = new ClientCustomFieldValueType();
        ccfValueType.setDtoBinding(DTOBindingTypeEnum.INCIDENTREPORT.getCode());
        ccfValueType.setValue("tt1");
        ccfValueType.setCcfId(1L);
        ClientCustomFieldMetaType metaType = new ClientCustomFieldMetaType();
        metaType.setFieldType("TEXT");
        ccfValueType.setMetaType(metaType);
        cCFSearchCriteria.add(ccfValueType);
        search.setCCFSearchCriteria(cCFSearchCriteria);

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertNotNull(ret);
        Assert.assertEquals(new Long(1L), ret.getTotalSize());

        //search by ccf fields and incident report field
        search = new IncidentReportSearchType();

        search.setCCFSearchCriteria(cCFSearchCriteria);
        search.setIncidentType("DEATH");

        ret = service.searchIncidentReport(uc, search, null, null, null);

        Assert.assertNotNull(ret);
        Assert.assertEquals(new Long(1L), ret.getTotalSize());
    }

    @AfterClass
    public void afterClass() {

    }

    //for release
    private void lookupJNDILogin() {

        log.info("Before JNDI lookup");
        service = (DisciplineService) JNDILookUp(this.getClass(), DisciplineService.class);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();
        clientlogin("devtest", "devtest");
    }

    private void clientlogin(String user, String password) {

        SecurityClient client;

        try {
            client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password); //"devtest", "devtest" nice regular service user
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
