package syscon.arbutus.product.services.personrelationship.contract.ejb;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.PersonIdentityTest;
import syscon.arbutus.product.services.core.exception.*;
import syscon.arbutus.product.services.person.contract.dto.personrelationship.*;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;

import static org.testng.Assert.*;

/**
 * Created by os on 8/14/15.
 */
public class IT extends BaseIT {
    private static Logger log = LoggerFactory.getLogger(IT.class);

    private static String FAM = "FAM";
    private static String FIN = "FIN";
    private static String LEG = "LEG";

    private static String CELL = "CELL";
    private static String TOTAL = "TOTAL";

    private static String FATHER = "FATHER";
    private static String MOTHER = "MOTHER";

    private static String WITNESS = "WITNESS";
    private static String VICTIM = "VICTIM";

    private static String OTHER = "OTHER";

    private PersonService service;
    private PersonIdentityTest identityTest;

    private final Date pastDate = new Date(1, 1, 1);

    private class SearchCreationReturn {
        final Long mainPersonId1;
        final Long mainPersonId2;
        final Map<Long, PersonAssociation> associationIds;
        final Map<Long, PersonNonAssociation> nonAssociationIds;
        final List<Long> personIds;

        public SearchCreationReturn(Long mainPersonId1, Long mainPersonId2, Map<Long, PersonAssociation> associationIds,
                Map<Long, PersonNonAssociation> nonAssociationIds, List<Long> personIds) {
            this.mainPersonId1 = mainPersonId1;
            this.mainPersonId2 = mainPersonId2;
            this.associationIds = associationIds;
            this.nonAssociationIds = nonAssociationIds;
            this.personIds = personIds;
        }
    }

    public IT() {
    }

    public IT(PersonService service, PersonIdentityTest psnTest) {
        this.service = service;
        this.identityTest = psnTest;
    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        assertNotNull(service);
        uc = super.initUserContext();

        identityTest = new PersonIdentityTest();

    }

    @AfterClass
    public void afterClass() {
        service.deleteAllPersonRelationship(uc);
    }

    private PersonAssociation newEmptyPersonAssociation() {
        return new PersonAssociation(null, null, null, null, null, null, null, null, null, null);
    }

    private PersonAssociation newPersonAssociation(Long personMain, Long personAssociated, String type, String mainRole, String associatedRole, String comment) {
        return new PersonAssociation(null, personMain, personAssociated, type, mainRole, associatedRole, null, comment, null, pastDate);
    }

    private PersonAssociation updatePersonAssociation(PersonAssociation instance, String type, String mainRole, String associatedRole, String comment, Date effectiveDate,
            Date expiryDate) {
        instance.setAssociationType(type);
        instance.setMainPersonRole(mainRole);
        instance.setAssociatedPersonRole(associatedRole);
        instance.setComment(comment);
        instance.setExpiryDate(expiryDate);
        instance.setEffectiveDate(effectiveDate);

        return instance;
    }

    private PersonNonAssociation newPersonNonAssociation(Long personMain, Long personAssociated, String type, String mainRole, String associatedRole, String comment) {
        return new PersonNonAssociation(null, personMain, personAssociated, type, mainRole, associatedRole, null, comment, null, pastDate);
    }

    private PersonNonAssociation updatePersonNonAssociation(PersonNonAssociation instance, String type, String mainRole, String associatedRole, String comment,
            Date effectiveDate, Date expiryDate) {
        instance.setNonAssociationType(type);
        instance.setMainPersonRole(mainRole);
        instance.setAssociatedPersonRole(associatedRole);
        instance.setComment(comment);
        instance.setExpiryDate(expiryDate);
        instance.setEffectiveDate(effectiveDate);
        return instance;
    }

    private void deletePersonRelationship(Long instanceId, Long mainPersonId) {
        try {
            service.deletePersonRelationship(uc, service.getPersonAssociation(uc, instanceId, mainPersonId));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void deletePerson(Long instanceId) {
        try {
            service.deletePersonType(uc, instanceId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Test
    public void create() {

        Long personMain = null;
        Long person2 = null;
        Long person3 = null;

        Long rel1 = null;
        Long rel2 = null;

        try {
            PersonRelationship personPerson = newEmptyPersonAssociation();

            //personPersonType is null, this will fail
            verifyCreateNegative(personPerson, InvalidInputException.class, null);

            //PersonAssociation and OffenderNonAssociation are null, this will fail
            personMain = identityTest.createPersonIdentityType().getPersonId();
            person2 = identityTest.createPersonIdentityType().getPersonId();
            person3 = identityTest.createPersonIdentityType().getPersonId();

            personPerson = new PersonAssociation(null, personMain, person2, null, null, null, null, null, null, null);
            verifyCreateNegative(personPerson, InvalidInputException.class, null);

            PersonAssociation personAssociation1 = newPersonAssociation(personMain, person2, FAM, FATHER, FATHER, "Family");

            PersonNonAssociation offenderNonAssociation1 = newPersonNonAssociation(personMain, person3, CELL, WITNESS, VICTIM, "Cell Desc");
            PersonNonAssociation offenderNonAssociation2 = newPersonNonAssociation(personMain, person2, TOTAL, WITNESS, VICTIM, "TOTAL Desc");

            //Success
            rel1 = service.createPersonRelationship(uc, personAssociation1);
            assertNotNull(rel1);

            verifyCreateNegative(personAssociation1, ValidationException.class, ErrorCodes.PSN_DUPLICATE_RELATIONSHIP);

            //Success
            rel2 = service.createPersonRelationship(uc, offenderNonAssociation1);
            assertNotNull(rel2);

            verifyCreateNegative(offenderNonAssociation2, ValidationException.class, ErrorCodes.PSN_ASSOCIATED_RELATIONSHIP_EXIST);
            verifyCreateNegative(offenderNonAssociation1, ValidationException.class, ErrorCodes.PSN_DUPLICATE_RELATIONSHIP);

            verifyCreateNegative(newPersonAssociation(personMain, personMain, FAM, FATHER, FATHER, "Family"), ValidationException.class,
                    ErrorCodes.PSN_RELATIONSHIP_SAME_PERSON);
            verifyCreateNegative(newPersonAssociation(personMain, person2, null, FATHER, FATHER, "Family"), InvalidInputException.class, null);
            verifyCreateNegative(newPersonAssociation(personMain, person2, FAM, null, FATHER, "Family"), InvalidInputException.class, null);
            verifyCreateNegative(newPersonAssociation(personMain, person2, FAM, FATHER, null, "Family"), InvalidInputException.class, null);

            verifyCreateNegative(newPersonNonAssociation(personMain, personMain, CELL, WITNESS, VICTIM, "TOTAL Desc"), ValidationException.class,
                    ErrorCodes.PSN_RELATIONSHIP_SAME_PERSON);
            verifyCreateNegative(newPersonNonAssociation(personMain, person2, null, WITNESS, VICTIM, "TOTAL Desc"), InvalidInputException.class, null);
            verifyCreateNegative(newPersonNonAssociation(personMain, person2, CELL, null, VICTIM, "TOTAL Desc"), InvalidInputException.class, null);
            verifyCreateNegative(newPersonNonAssociation(personMain, person2, CELL, WITNESS, null, "TOTAL Desc"), InvalidInputException.class, null);
        } finally {
            deletePersonRelationship(rel1, personMain);
            deletePersonRelationship(rel2, personMain);

            deletePerson(personMain);
            deletePerson(person2);
            deletePerson(person3);
        }

    }

    @Test
    public void update() {
        Long personMain = null;
        Long person2 = null;
        Long person3 = null;

        Long rel1 = null;
        Long rel2 = null;

        try {
            personMain = identityTest.createPersonIdentityType().getPersonId();
            person2 = identityTest.createPersonIdentityType().getPersonId();
            person3 = identityTest.createPersonIdentityType().getPersonId();

            PersonAssociation personAssociation1 = newPersonAssociation(personMain, person2, FAM, FATHER, FATHER, "Family");

            PersonNonAssociation offenderNonAssociation1 = newPersonNonAssociation(personMain, person3, CELL, WITNESS, VICTIM, "Cell Desc");

            //Success
            rel1 = service.createPersonRelationship(uc, personAssociation1);
            assertNotNull(rel1);
            //Success
            rel2 = service.createPersonRelationship(uc, offenderNonAssociation1);
            assertNotNull(rel2);

            personAssociation1 = service.getPersonAssociation(uc, rel1, personMain);
            assertNotNull(personAssociation1);

            offenderNonAssociation1 = service.getPersonNonAssociation(uc, rel2, personMain);
            assertNotNull(offenderNonAssociation1);

            verifyUpdateNegative(updatePersonAssociation(personAssociation1, null, FATHER, FATHER, "Family", pastDate, null), InvalidInputException.class, null);
            verifyUpdateNegative(updatePersonAssociation(personAssociation1, FAM, null, FATHER, "Family", pastDate, null), InvalidInputException.class, null);
            verifyUpdateNegative(updatePersonAssociation(personAssociation1, FAM, FATHER, null, "Family", pastDate, null), InvalidInputException.class, null);
            verifyUpdateNegative(updatePersonAssociation(personAssociation1, FAM, FATHER, FATHER, "Family", null, null), InvalidInputException.class, null);
            verifyUpdateNegative(updatePersonAssociation(personAssociation1, FAM, FATHER, FATHER, "Family", pastDate, new Date(new Date().getTime() + 1000)),
                    ValidationException.class, ErrorCodes.PSN_RELATIONSHIP_EXP_DATE_LOWER_THAN_EFFECTIVE_DATE);

            verifyUpdateNegative(updatePersonNonAssociation(offenderNonAssociation1, null, WITNESS, VICTIM, "Cell Desc", pastDate, null), InvalidInputException.class,
                    null);
            verifyUpdateNegative(updatePersonNonAssociation(offenderNonAssociation1, CELL, null, VICTIM, "Cell Desc", pastDate, null), InvalidInputException.class, null);
            verifyUpdateNegative(updatePersonNonAssociation(offenderNonAssociation1, CELL, WITNESS, null, "Cell Desc", pastDate, null), InvalidInputException.class,
                    null);
            verifyUpdateNegative(updatePersonNonAssociation(offenderNonAssociation1, CELL, WITNESS, VICTIM, "Cell Desc", null, null), InvalidInputException.class, null);
            verifyUpdateNegative(updatePersonNonAssociation(offenderNonAssociation1, CELL, WITNESS, VICTIM, "Cell Desc", pastDate, new Date(new Date().getTime() + 1000)),
                    ValidationException.class, ErrorCodes.PSN_RELATIONSHIP_EXP_DATE_LOWER_THAN_EFFECTIVE_DATE);

            personAssociation1 = service.getPersonAssociation(uc, rel1, personMain);
            offenderNonAssociation1 = service.getPersonNonAssociation(uc, rel2, personMain);
            service.updatePersonRelationship(uc, updatePersonAssociation(personAssociation1, FAM, FATHER, FATHER, "Family", pastDate, new Date()));
            service.updatePersonRelationship(uc, updatePersonNonAssociation(offenderNonAssociation1, CELL, WITNESS, VICTIM, "Cell Desc", pastDate, new Date()));

            personAssociation1 = service.getPersonAssociation(uc, rel1, personMain);
            assertNotNull(personAssociation1.getEffectiveDate());
            offenderNonAssociation1 = service.getPersonNonAssociation(uc, rel2, personMain);
            assertNotNull(offenderNonAssociation1.getEffectiveDate());

            service.updatePersonRelationship(uc, updatePersonAssociation(personAssociation1, FAM, FATHER, FATHER, "Family2", pastDate, new Date()));
            service.updatePersonRelationship(uc, updatePersonNonAssociation(offenderNonAssociation1, CELL, WITNESS, VICTIM, "Cell Desc2", pastDate, new Date()));
            personAssociation1 = service.getPersonAssociation(uc, rel1, personMain);
            assertEquals(personAssociation1.getComment(), "Family2");
            offenderNonAssociation1 = service.getPersonNonAssociation(uc, rel2, personMain);
            assertEquals(offenderNonAssociation1.getComment(), "Cell Desc2");

            service.updatePersonRelationship(uc, updatePersonAssociation(personAssociation1, FAM, MOTHER, OTHER, "Family2", pastDate, new Date()));
            service.updatePersonRelationship(uc, updatePersonNonAssociation(offenderNonAssociation1, CELL, OTHER, WITNESS, "Cell Desc2", pastDate, new Date()));
            personAssociation1 = service.getPersonAssociation(uc, rel1, personMain);
            assertEquals(personAssociation1.getMainPersonRole(), MOTHER);
            assertEquals(personAssociation1.getAssociatedPersonRole(), OTHER);
            offenderNonAssociation1 = service.getPersonNonAssociation(uc, rel2, personMain);
            assertEquals(offenderNonAssociation1.getMainPersonRole(), OTHER);
            assertEquals(offenderNonAssociation1.getAssociatedPersonRole(), WITNESS);

        } finally {
            deletePersonRelationship(rel1, personMain);
            deletePersonRelationship(rel2, personMain);

            deletePerson(personMain);
            deletePerson(person2);
            deletePerson(person3);
        }
    }

    @Test
    public void get() {

        Long personMain = null;
        Long person2 = null;
        Long person3 = null;

        Long rel1 = null;
        Long rel2 = null;

        try {

            verifyGetPersonAssociationNegative(null, null, InvalidInputException.class);
            verifyGetPersonAssociationNegative(1l, null, InvalidInputException.class);
            verifyGetPersonAssociationNegative(null, 1l, InvalidInputException.class);
            verifyGetPersonAssociationNegative(-1l, -1l, DataNotExistException.class);

            verifyGetPersonNonAssociationNegative(null, null, InvalidInputException.class);
            verifyGetPersonNonAssociationNegative(1l, null, InvalidInputException.class);
            verifyGetPersonNonAssociationNegative(null, 1l, InvalidInputException.class);
            verifyGetPersonNonAssociationNegative(-1l, -1l, DataNotExistException.class);

            personMain = identityTest.createPersonIdentityType().getPersonId();
            person2 = identityTest.createPersonIdentityType().getPersonId();
            person3 = identityTest.createPersonIdentityType().getPersonId();

            PersonAssociation personAssociation1 = newPersonAssociation(personMain, person2, FAM, FATHER, MOTHER, "Family");

            PersonNonAssociation offenderNonAssociation1 = newPersonNonAssociation(personMain, person3, CELL, WITNESS, VICTIM, "Cell Desc");

            //Success
            rel1 = service.createPersonRelationship(uc, personAssociation1);
            assertNotNull(rel1);
            //Success
            rel2 = service.createPersonRelationship(uc, offenderNonAssociation1);
            assertNotNull(rel2);

            personAssociation1 = service.getPersonAssociation(uc, rel1, personMain);
            assertNotNull(personAssociation1);
            assertEquals(personAssociation1.getMainPersonId(), personMain);
            assertEquals(personAssociation1.getAssociatedPerson().getPersonId(), person2);
            assertEquals(personAssociation1.getMainPersonRole(), FATHER);
            assertEquals(personAssociation1.getAssociatedPersonRole(), MOTHER);

            offenderNonAssociation1 = service.getPersonNonAssociation(uc, rel2, personMain);
            assertNotNull(offenderNonAssociation1);
            assertEquals(offenderNonAssociation1.getMainPersonId(), personMain);
            assertEquals(offenderNonAssociation1.getAssociatedPerson().getPersonId(), person3);
            assertEquals(offenderNonAssociation1.getMainPersonRole(), WITNESS);
            assertEquals(offenderNonAssociation1.getAssociatedPersonRole(), VICTIM);

            personAssociation1 = service.getPersonAssociation(uc, rel1, person2);
            assertNotNull(personAssociation1);
            assertEquals(personAssociation1.getMainPersonId(), person2);
            assertEquals(personAssociation1.getAssociatedPerson().getPersonId(), personMain);
            assertEquals(personAssociation1.getMainPersonRole(), MOTHER);
            assertEquals(personAssociation1.getAssociatedPersonRole(), FATHER);

            offenderNonAssociation1 = service.getPersonNonAssociation(uc, rel2, person3);
            assertNotNull(offenderNonAssociation1);
            assertEquals(offenderNonAssociation1.getMainPersonId(), person3);
            assertEquals(offenderNonAssociation1.getAssociatedPerson().getPersonId(), personMain);
            assertEquals(offenderNonAssociation1.getMainPersonRole(), VICTIM);
            assertEquals(offenderNonAssociation1.getAssociatedPersonRole(), WITNESS);

        } finally {
            deletePersonRelationship(rel1, personMain);
            deletePersonRelationship(rel2, personMain);

            deletePerson(personMain);
            deletePerson(person2);
            deletePerson(person3);
        }

    }

    @Test
    public void delete() {
        //personPersonAssociationID is null, this will fail.
        verifyDeleteNegative(null, InvalidInputException.class);
    }

    @Test
    public SearchCreationReturn searchCreation() {
        Map<Long, PersonAssociation> associationIds = new HashMap<>(10);
        Map<Long, PersonNonAssociation> nonAssociationIds = new HashMap<>(10);
        List<Long> personIds = new ArrayList<Long>();
        Long personMain = identityTest.createPersonIdentityType().getPersonId();

        for (int i = 1; i <= 5; i++) {
            Long person2 = identityTest.createPersonIdentityType().getPersonId();
            Long person3 = identityTest.createPersonIdentityType().getPersonId();

            PersonAssociation personAssociation1 = newPersonAssociation(personMain, person2, FAM, FATHER, FATHER, "Family");
            PersonNonAssociation offenderNonAssociation1 = newPersonNonAssociation(personMain, person3, CELL, WITNESS, VICTIM, "Cell Desc");

            //Success
            Long rel1 = service.createPersonRelationship(uc, personAssociation1);
            assertNotNull(rel1);
            //Success
            Long rel2 = service.createPersonRelationship(uc, offenderNonAssociation1);
            assertNotNull(rel2);

            associationIds.put(rel1, personAssociation1);
            nonAssociationIds.put(rel2, offenderNonAssociation1);
            personIds.add(person2);
            personIds.add(person3);
        }

        for (int i = 1; i <= 5; i++) {
            Long person2 = identityTest.createPersonIdentityType().getPersonId();
            Long person3 = identityTest.createPersonIdentityType().getPersonId();

            PersonAssociation personAssociation1 = newPersonAssociation(personMain, person2, FAM, FATHER, MOTHER, "Family");
            PersonNonAssociation offenderNonAssociation1 = newPersonNonAssociation(personMain, person3, TOTAL, VICTIM, WITNESS, "Cell Desc");

            //Success
            Long rel1 = service.createPersonRelationship(uc, personAssociation1);
            assertNotNull(rel1);
            //Success
            Long rel2 = service.createPersonRelationship(uc, offenderNonAssociation1);
            assertNotNull(rel2);

            associationIds.put(rel1, personAssociation1);
            nonAssociationIds.put(rel2, offenderNonAssociation1);
            personIds.add(person2);
            personIds.add(person3);
        }

        Long personMain2 = identityTest.createPersonIdentityType().getPersonId();

        for (int i = 1; i <= 5; i++) {
            Long person2 = identityTest.createPersonIdentityType().getPersonId();
            Long person3 = identityTest.createPersonIdentityType().getPersonId();

            PersonAssociation personAssociation1 = newPersonAssociation(personMain2, person2, FAM, FATHER, FATHER, "Family");
            PersonNonAssociation offenderNonAssociation1 = newPersonNonAssociation(personMain2, person3, CELL, WITNESS, VICTIM, "Cell Desc");

            //Success
            Long rel1 = service.createPersonRelationship(uc, personAssociation1);
            assertNotNull(rel1);
            //Success
            Long rel2 = service.createPersonRelationship(uc, offenderNonAssociation1);
            assertNotNull(rel2);

            associationIds.put(rel1, personAssociation1);
            nonAssociationIds.put(rel2, offenderNonAssociation1);
            personIds.add(person2);
            personIds.add(person3);
        }

        return new SearchCreationReturn(personMain, personMain2, associationIds, nonAssociationIds, personIds);
    }

    @Test
    public void search() {
        SearchCreationReturn creationReturn = null;
        try {
            //Create search data
            //searchCreation();
            creationReturn = searchCreation();

            //the PersonPersonSearchType is null, this will fail.
            verifySearchNegative(null, null, InvalidInputException.class);
            verifySearchNegative(1l, null, InvalidInputException.class);
            verifySearchNegative(null, new PersonAssociationSearchType(), InvalidInputException.class);

            PersonAssociationSearchType associationSearchType = new PersonAssociationSearchType();
            PersonNonAssociationSearchType nonAssociationSearchType = new PersonNonAssociationSearchType();

            PersonRelationshipReturnType ret = service.searchPersonRelationship(uc, creationReturn.mainPersonId1, associationSearchType, 0l, 200l, null);
            assertNotNull(ret);
            assertEquals(ret.getTotalSize(), new Long(10));
            assertEquals(new Long(ret.getPersonRelationships().size()), new Long(10));

            ret = service.searchPersonRelationship(uc, creationReturn.mainPersonId1, nonAssociationSearchType, 0l, 200l, null);
            assertNotNull(ret);
            assertEquals(ret.getTotalSize(), new Long(10));
            assertEquals(new Long(ret.getPersonRelationships().size()), new Long(10));

            ret = service.searchPersonRelationship(uc, creationReturn.mainPersonId2, associationSearchType, 0l, 200l, null);
            assertNotNull(ret);
            assertEquals(ret.getTotalSize(), new Long(5));
            assertEquals(new Long(ret.getPersonRelationships().size()), new Long(5));

            ret = service.searchPersonRelationship(uc, creationReturn.mainPersonId2, nonAssociationSearchType, 0l, 200l, null);
            assertNotNull(ret);
            assertEquals(ret.getTotalSize(), new Long(5));
            assertEquals(new Long(ret.getPersonRelationships().size()), new Long(5));

            ret = service.searchPersonRelationship(uc, creationReturn.associationIds.values().iterator().next().getAssociatedPerson().getPersonId(),
                    associationSearchType, 0l, 200l, null);
            assertNotNull(ret);
            assertEquals(ret.getTotalSize(), new Long(1));
            assertEquals(new Long(ret.getPersonRelationships().size()), new Long(1));

            ret = service.searchPersonRelationship(uc, creationReturn.nonAssociationIds.values().iterator().next().getAssociatedPerson().getPersonId(),
                    nonAssociationSearchType, 0l, 200l, null);
            assertNotNull(ret);
            assertEquals(ret.getTotalSize(), new Long(1));
            assertEquals(new Long(ret.getPersonRelationships().size()), new Long(1));

            associationSearchType.setMainPersonRole(FATHER);
            associationSearchType.setAssociatedPersonRole(FATHER);
            nonAssociationSearchType.setActiveFlag(Boolean.TRUE);
            ret = service.searchPersonRelationship(uc, creationReturn.mainPersonId1, associationSearchType, 0l, 200l, null);
            assertNotNull(ret);
            assertEquals(ret.getTotalSize(), new Long(5));
            assertEquals(new Long(ret.getPersonRelationships().size()), new Long(5));

            associationSearchType.setMainPersonRole(FATHER);
            associationSearchType.setAssociatedPersonRole(MOTHER);
            nonAssociationSearchType.setActiveFlag(Boolean.TRUE);
            ret = service.searchPersonRelationship(uc, creationReturn.mainPersonId1, associationSearchType, 0l, 200l, null);
            assertNotNull(ret);
            assertEquals(ret.getTotalSize(), new Long(5));
            assertEquals(new Long(ret.getPersonRelationships().size()), new Long(5));

            nonAssociationSearchType.setMainPersonRole(WITNESS);
            nonAssociationSearchType.setAssociatedPersonRole(VICTIM);
            nonAssociationSearchType.setActiveFlag(Boolean.TRUE);
            ret = service.searchPersonRelationship(uc, creationReturn.mainPersonId1, nonAssociationSearchType, 0l, 200l, null);
            assertNotNull(ret);
            assertEquals(ret.getTotalSize(), new Long(5));
            assertEquals(new Long(ret.getPersonRelationships().size()), new Long(5));

            nonAssociationSearchType.setMainPersonRole(VICTIM);
            nonAssociationSearchType.setAssociatedPersonRole(WITNESS);
            nonAssociationSearchType.setActiveFlag(Boolean.TRUE);
            ret = service.searchPersonRelationship(uc, creationReturn.mainPersonId1, associationSearchType, 0l, 200l, null);
            assertNotNull(ret);
            assertEquals(ret.getTotalSize(), new Long(5));
            assertEquals(new Long(ret.getPersonRelationships().size()), new Long(5));

            associationSearchType.setActiveFlag(Boolean.FALSE);
            ret = service.searchPersonRelationship(uc, creationReturn.mainPersonId1, associationSearchType, 0l, 200l, null);
            assertNotNull(ret);
            assertEquals(ret.getTotalSize(), new Long(0));

        } finally {
            for (Map.Entry<Long, PersonAssociation> entry : creationReturn.associationIds.entrySet()) {
                deletePersonRelationship(entry.getKey(), entry.getValue().getMainPersonId());
            }

            for (Map.Entry<Long, PersonNonAssociation> entry : creationReturn.nonAssociationIds.entrySet()) {
                deletePersonRelationship(entry.getKey(), entry.getValue().getMainPersonId());
            }

            for (Long id : creationReturn.personIds) {
                deletePerson(id);
            }
        }

        log.info("search End");
    }

    private void verifySearchNegative(final Long mainPersonId, final PersonRelationshipSearchType search, Class<?> expectionClazz) {
        try {
            PersonRelationshipReturnType ret = service.searchPersonRelationship(uc, mainPersonId, search, 0L, 200L, null);
            assertNull(ret);
        } catch (Exception ex) {
            assertEquals(ex.getClass(), expectionClazz);
        }
    }

    private void verifyGetPersonNonAssociationNegative(final Long instanceId, final Long mainPersonId, Class<?> expectionClazz) {
        try {
            PersonNonAssociation ret = service.getPersonNonAssociation(uc, instanceId, mainPersonId);
            assertNull(ret);
        } catch (Exception ex) {
            assertEquals(ex.getClass(), expectionClazz);
        }
    }

    private void verifyGetPersonAssociationNegative(final Long instanceId, final Long mainPersonId, Class<?> expectionClazz) {
        try {
            PersonAssociation ret = service.getPersonAssociation(uc, instanceId, mainPersonId);
            assertNull(ret);
        } catch (Exception ex) {
            assertEquals(ex.getClass(), expectionClazz);
        }
    }

    private void verifyDeleteNegative(PersonRelationship instance, Class<?> expectionClazz) {
        try {
            service.deletePersonRelationship(uc, instance);
        } catch (Exception ex) {
            assertEquals(ex.getClass(), expectionClazz);
        }
    }

    private void verifyCreateNegative(PersonRelationship instance, Class<?> expectionClazz, ErrorCodes errorCodes) {
        try {
            Long ret = service.createPersonRelationship(uc, instance);
            assertNull(ret);
        } catch (Exception ex) {
            assertEquals(ex.getClass(), expectionClazz);
            if (errorCodes != null) {
                assertEquals(((ArbutusRuntimeException) ex).getCode(), errorCodes);
            }
        }
    }

    private void verifyUpdateNegative(PersonRelationship instance, Class<?> expectionClazz, ErrorCodes errorCodes) {
        try {
            service.updatePersonRelationship(uc, instance);
        } catch (Exception ex) {
            assertEquals(ex.getClass(), expectionClazz);
            if (errorCodes != null) {
                assertEquals(((ArbutusRuntimeException) ex).getCode(), errorCodes);
            }
        }
    }
}

