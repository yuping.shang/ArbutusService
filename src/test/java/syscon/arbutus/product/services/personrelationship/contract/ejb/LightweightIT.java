package syscon.arbutus.product.services.personrelationship.contract.ejb;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseTestJMockIt;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;

/**
 * Created by os on 8/14/15.
 */
public class LightweightIT extends BaseTestJMockIt {

    private IT superIT;

    @BeforeClass
    public void beforeClass() {
        super.beforeClass();
        superIT = new IT((PersonService) personService, personIdentityTest);
    }

    @Test
    public void create() {
        superIT.create();
    }

    @Test
    public void update() {
        superIT.update();
    }

    @Test
    public void get() {
        superIT.get();
    }

    @Test
    public void delete() {
        superIT.delete();
    }

    @Test
    public void search() {
        superIT.search();
    }
}
