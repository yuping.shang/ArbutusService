/**
 * © 2013 by Syscon Justice Systems. All rights reserved.
 * Copying and distribution, any modifications or adaptation of the contents, or creation of derivative
 * works are not allowed without the express and prior written consent of Syscon Justice Systems.
 */
package syscon.arbutus.product.services.clientcustomfield.contract.ejb;

import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.*;
import syscon.arbutus.product.services.clientcustomfield.contract.interfaces.ClientCustomFieldService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.ReferenceCodeType;
import syscon.arbutus.product.services.eventlog.contract.dto.ReferenceSets;

import javax.naming.NamingException;
import java.util.*;

import static org.testng.Assert.assertNotNull;

public class IT extends BaseIT {
    private static Logger log = LoggerFactory.getLogger(IT.class);
    protected UserContext uc = null;
    // shared data
    Object tip;
    private ClientCustomFieldService service;

    @BeforeClass
    public void beforeTest() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");
        service = (ClientCustomFieldService) JNDILookUp(this.getClass(), ClientCustomFieldService.class);
        uc = super.initUserContext();
        // service.reset(uc);
    }

    private void clientlogin(String user, String password) {
        try {
            SecurityClient client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password);
            client.login();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    @Test
    public void testSetCCFMeta() {
        GroupType groupType = new GroupType();
        groupType.setEntity(DTOBindingTypeEnum.CASENOTE.getCode());
        Set<GroupLabelType> grpLbls = new HashSet<GroupLabelType>();
        grpLbls.add(new GroupLabelType(null, "Group Label 2", "en"));
        groupType.setGroupLables(grpLbls);
        groupType.setName("Group 2");
        groupType.setSequence(1L);

        GroupType retGroup = service.createGroup(uc, groupType);
        ClientCustomFieldMetaType metaType = getCCFMetaTextBoxInstance();
        metaType.setGroup(retGroup.getName());
        metaType.setGroupId(retGroup.getGroupId());
        metaType.setGroupLabel(retGroup.getGroupLables());
        metaType.setGroupSequence(retGroup.getSequence().toString());

        service.save(uc, metaType);

        metaType = getCCFMetaLOVInstance();
        metaType.setGroup(retGroup.getName());
        metaType.setGroupId(retGroup.getGroupId());
        metaType.setGroupLabel(retGroup.getGroupLables());
        metaType.setGroupSequence(retGroup.getSequence().toString());

        service.save(uc, metaType);

        metaType = getCCFMetaEventDateInstance();
        metaType.setGroup(retGroup.getName());
        metaType.setGroupId(retGroup.getGroupId());
        metaType.setGroupLabel(retGroup.getGroupLables());
        metaType.setGroupSequence(retGroup.getSequence().toString());

        service.save(uc, metaType);
    }

    // @Test
    public void testGetCCFMeta() {
        String json = service.getCCFMeta(uc, "EVENTLOG", "en");
        //System.out.println("Meta Data Json ===>" + json);
    }

    // TODO commented because its failing @Test
    public void testGetCCFMetaDTO() {
        ClientCustomFieldMetaType ccf = service.getClientCustomFieldMeta(uc, DTOBindingTypeEnum.CASENOTE.getCode(), "NoteType");
        //System.out.println("Meta Data DTO ===>" + ccf.toString());
    }

    //@Test
    public void testSetCCFValueInBulk() {
        GroupType groupType = new GroupType();
        groupType.setEntity(DTOBindingTypeEnum.CASENOTE.getCode());
        Set<GroupLabelType> grpLbls = new HashSet<GroupLabelType>();
        grpLbls.add(new GroupLabelType(null, "Group Label 1", "en"));
        groupType.setGroupLables(grpLbls);
        groupType.setName("Group 1");
        groupType.setSequence(1L);

        GroupType retGroup = service.createGroup(uc, groupType);

        ClientCustomFieldMetaType metaType = getCCFMetaTextBoxInstance();
        metaType.setGroup(retGroup.getName());
        metaType.setGroupId(retGroup.getGroupId());
        metaType.setGroupLabel(retGroup.getGroupLables());
        metaType.setGroupSequence(retGroup.getSequence().toString());

        ClientCustomFieldMetaType retMeta = service.save(uc, metaType);

        ClientCustomFieldValueType ccfValue = new ClientCustomFieldValueType();
        ccfValue.setCcfId(retMeta.getCcfId());
        ccfValue.setCodeValue(null);
        ccfValue.setDtoBinding(retMeta.getDtoBinding());
        ccfValue.setIdBinding(1L);
        ccfValue.setName(retMeta.getName());
        ccfValue.setValue("Working with Syscon.");

        ClientCustomFieldValueType retValue = service.setClientCustomFieldValue(uc, ccfValue);
        //System.out.println("Meta Value created ===>" + retValue.toString());
        retValue.setValue("No I am working with Polaris.");
        ClientCustomFieldValueType retUpdateValue = service.updateClientCustomFieldValue(uc, retValue);

        //System.out.println("Meta Value Updated ===>"
        //+ retUpdateValue.toString());
        ClientCustomFieldValueSearchType searchType = new ClientCustomFieldValueSearchType();
        searchType.setCcfId(retUpdateValue.getCcfId());
        List<ClientCustomFieldValueType> retGetValue = service.search(uc, searchType, null, null, null);
        assert (retGetValue != null);

        //System.out.println("Fetched Value===>" + retGetValue.toString());

        searchType.setCcfValueId(retUpdateValue.getCcfValueId());
        searchType.setDtoBinding(retUpdateValue.getDtoBinding());
        searchType.setIdBinding(1L);

        retGetValue = service.search(uc, searchType, null, null, null);

        //System.out.println("Fetched Value===>" + retGetValue.toString());
    }

    @Test
    public void testUpdateCCFMetaValue() {
        GroupType groupType = new GroupType();
        groupType.setEntity(DTOBindingTypeEnum.SHIFTLOG.getCode());
        Set<GroupLabelType> grpLbls = new HashSet<GroupLabelType>();
        grpLbls.add(new GroupLabelType(null, "Group Label 3", "en"));
        groupType.setGroupLables(grpLbls);
        groupType.setName("Group 3");
        groupType.setSequence(1L);

        GroupType retGroup = service.createGroup(uc, groupType);

        ClientCustomFieldMetaType metaType = getCCFMetaTextBoxInstance();
        metaType.setGroup(retGroup.getName());
        metaType.setGroupId(retGroup.getGroupId());
        metaType.setGroupLabel(retGroup.getGroupLables());
        metaType.setGroupSequence(retGroup.getSequence().toString());

        ClientCustomFieldMetaType retMeta = service.save(uc, metaType);

        metaType = getCCFMetaLOVInstance();
        metaType.setGroup(retGroup.getName());
        metaType.setGroupId(retGroup.getGroupId());
        metaType.setGroupLabel(retGroup.getGroupLables());
        metaType.setGroupSequence(retGroup.getSequence().toString());

        retMeta = service.save(uc, metaType);

        metaType = getCCFMetaEventDateInstance();
        metaType.setGroup(retGroup.getName());
        metaType.setGroupId(retGroup.getGroupId());
        metaType.setGroupLabel(retGroup.getGroupLables());
        metaType.setGroupSequence(retGroup.getSequence().toString());

        retMeta = service.save(uc, metaType);
        Set<ClientCustomFieldMetaType> metaTypes = service.getAllClientCustomFieldMeta(uc, DTOBindingTypeEnum.CASENOTE.getCode());
        List<ClientCustomFieldValueType> values = new ArrayList<ClientCustomFieldValueType>();
        if (metaTypes == null || metaTypes.isEmpty()) {
			System.out.println("NO Meta type defined for Event Log...");
		} else {
            for (ClientCustomFieldMetaType meta : metaTypes) {
                ClientCustomFieldValueType valueType = new ClientCustomFieldValueType();
                valueType.setCcfId(meta.getCcfId());
                valueType.setDtoBinding(meta.getDtoBinding());
                valueType.setIdBinding(1L);
                valueType.setName(meta.getName());
                valueType.setValue(null);
                values.add(service.setClientCustomFieldValue(uc, valueType));
            }
            //System.out.println("CCF Values Created is ====>"+ values.toString());
            List<ClientCustomFieldValueType> updateValues = new ArrayList<ClientCustomFieldValueType>();
            for (ClientCustomFieldValueType value : values) {
                value.setValue("This is updated Value....");
                updateValues.add(value);
            }
            List<ClientCustomFieldValueType> retValues = service.updateClientCustomFieldValues(uc, values);
            //System.out.println("CCF Values Updated Count is ====>" + retValues.size());
        }
    }

    @Test(dependsOnMethods = { "testSetCCFMeta" })
    public void testGetAllCCFMeta() {
    }

    private ClientCustomFieldMetaType getCCFMetaTextBoxInstance() {
        ClientCustomFieldMetaType metaType = new ClientCustomFieldMetaType();
        metaType.setActiveDate(createPastDate(2));
        metaType.setClassification(Constants.Classification.PRIMARY.code());
        metaType.setDeactiveDate(createPastDate(1));
        metaType.setDefaultValue("XYZ");
        // metaType.setDefaultValueReferenceSet();
        metaType.setDtoBinding(DTOBindingTypeEnum.CASENOTE.getCode());
        metaType.setFieldType(FieldTypeEnum.TEXT.getId());
        // metaType.setGroup();
        metaType.setGroupId(1L);
        // metaType.setGroupLabel();
        // metaType.setGroupLabel();
        // metaType.setGroupSequence();
        metaType.setLength(3000L);
        metaType.setName("Narrative");
        metaType.setReadOnly(Boolean.FALSE);
        metaType.setReferenceSet(null);
        metaType.setRequired(null);
        metaType.setSequence(1L);
        metaType.setTranslation(getTranslationSetForTextBox());
        metaType.setValidationRule("ABC-TextBox");
        metaType.setVisible(Boolean.TRUE);

        return metaType;
    }

    private ClientCustomFieldMetaType getCCFMetaLOVInstance() {
        ClientCustomFieldMetaType metaType = new ClientCustomFieldMetaType();
        metaType.setActiveDate(createPastDate(3));
        metaType.setClassification(Constants.Classification.PRIMARY.code());
        metaType.setDeactiveDate(createFutureDate(2));
        // metaType.setDefaultValue();
        // metaType.setDefaultValueReferenceSet();
        metaType.setDtoBinding(DTOBindingTypeEnum.CASENOTE.getCode());
        metaType.setFieldType(FieldTypeEnum.LOV.getId());
        // metaType.setGroup();
        metaType.setGroupId(2L);
        metaType.setReferenceSet(ReferenceSets.NoteType.value());
        // metaType.setGroupLabel();
        // metaType.setGroupLabel();
        // metaType.setGroupSequence();
        // metaType.setLength(3000L);
        metaType.setName("NoteType");
        metaType.setReadOnly(Boolean.FALSE);
        // metaType.setRequired(null);
        metaType.setSequence(2L);
        metaType.setTranslation(getTranslationSetForLOV());
        metaType.setValidationRule("ABC-LOV");
        metaType.setVisible(Boolean.TRUE);

        return metaType;
    }

    private ClientCustomFieldMetaType getCCFMetaEventDateInstance() {
        ClientCustomFieldMetaType metaType = new ClientCustomFieldMetaType();
        metaType.setActiveDate(createPastDate(4));
        metaType.setClassification(Constants.Classification.PRIMARY.code());
        metaType.setDeactiveDate(createFutureDate(3));
        // metaType.setDefaultValue();
        // metaType.setDefaultValueReferenceSet();
        metaType.setDtoBinding(DTOBindingTypeEnum.CASENOTE.getCode());
        metaType.setFieldType(FieldTypeEnum.DATE.getId());
        // metaType.setGroup();
        metaType.setGroupId(3L);
        // metaType.setReferenceSet(ReferenceSets.NoteType.value());
        // metaType.setGroupLabel();
        // metaType.setGroupLabel();
        // metaType.setGroupSequence();
        // metaType.setLength(3000L);
        metaType.setName("EventDate");
        metaType.setReadOnly(Boolean.FALSE);
        // metaType.setRequired(null);
        metaType.setSequence(3L);
        metaType.setTranslation(getTranslationSetForDATE());
        metaType.setValidationRule("ABC-Date");
        metaType.setVisible(Boolean.TRUE);

        return metaType;
    }

    private Set<TranslationType> getTranslationSetForTextBox() {
        Set<TranslationType> translations = new HashSet<TranslationType>();
        TranslationType trans = new TranslationType();
        trans.setError("This is Error Message");
        trans.setHelp("This is Help Message");
        trans.setLabel("Narrative Text");
        trans.setLanguage("en");
        translations.add(trans);
        return translations;
    }

    private Set<TranslationType> getTranslationSetForLOV() {
        Set<TranslationType> translations = new HashSet<TranslationType>();
        TranslationType trans = new TranslationType();
        trans.setError("This is Error Message");
        trans.setHelp("This is Help Message");
        trans.setLabel("Note Type");
        trans.setLanguage("en");
        translations.add(trans);
        return translations;
    }

    private Set<TranslationType> getTranslationSetForDATE() {
        Set<TranslationType> translations = new HashSet<TranslationType>();
        TranslationType trans = new TranslationType();
        trans.setError("This is Error Message");
        trans.setHelp("This is Help Message");
        trans.setLabel("Event Date");
        trans.setLanguage("en");
        translations.add(trans);
        return translations;
    }

    @SuppressWarnings("unused")
    private Date createFutureDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, day);
        Date futureDate = cal.getTime();
        return futureDate;
    }

    @SuppressWarnings("unused")
    private Date createPastDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, -day);
        Date pastDate = cal.getTime();
        return pastDate;
    }

    @Test
    public void testCreateGroup() {
        GroupType gt = service.createGroup(uc, getGroupTypeForCreate());
        //System.out.println("GroupType : " + gt.toString());
    }

    // @Test
    public void testUpdateGroup() {
        GroupType groupType = getGroupTypeForCreate();
        groupType.setGroupId(21L);
        groupType.setName("Test Name");
        groupType.setEntity("TestEntity");

        GroupType gt = service.updateGroup(uc, groupType);
        //System.out.println("GroupType : " + gt.toString());
    }

    @Test
    public void testSearchAllGroupLabel() {
        Set<GroupType> groupTypes = service.searchAllGroupLabel(uc);
        //System.out.println("GroupType : " + groupTypes);
    }

    private GroupType getGroupTypeForCreate() {
        Long lng = new Random().nextLong();
        GroupType groupType = new GroupType();
        groupType.setEntity("TestEntity" + lng);
        groupType.setName("Test Name" + lng);
        groupType.setSequence(1L);
        GroupLabelType groupLabelType = new GroupLabelType();
        groupLabelType.setLabel("Test Entity Label");
        groupLabelType.setLanguage("EN");
        Set<GroupLabelType> groupLabelTypes = new HashSet<GroupLabelType>();
        groupLabelTypes.add(groupLabelType);
        groupType.setGroupLables(groupLabelTypes);
        return groupType;
    }

    @Test
    public void testSave() {
        ClientCustomFieldMetaType t = service.save(uc, getClientCustomFieldMetaType());
        //System.out.println("ClientCustomFieldMetaType : " + t.toString());
    }

    // @Test
    public void testUpdate() {
        ClientCustomFieldMetaType clientCustomFieldMetaType = getClientCustomFieldMetaType();
        clientCustomFieldMetaType.setCcfId(4L);
        clientCustomFieldMetaType.setName("New Name 1");
        ClientCustomFieldMetaType t = service.update(uc, clientCustomFieldMetaType);
        //System.out.println("ClientCustomFieldMetaType : " + t.toString());
    }

    private ClientCustomFieldMetaType getClientCustomFieldMetaType() {
        ClientCustomFieldMetaType clientCustomFieldMetaType = new ClientCustomFieldMetaType();
        clientCustomFieldMetaType.setActiveDate(new Date());
        clientCustomFieldMetaType.setClassification("Classification");
        clientCustomFieldMetaType.setDeactiveDate(new Date());
        clientCustomFieldMetaType.setDefaultValue("DafaultValue");
        clientCustomFieldMetaType.setDefaultValueReferenceSet("defaultValueReferenceSet");
        clientCustomFieldMetaType.setDtoBinding("dtoBinding");
        clientCustomFieldMetaType.setFieldType("Test");
        // clientCustomFieldMetaType.setGroupId(42L);
        // clientCustomFieldMetaType.setGroup();
        clientCustomFieldMetaType.setSequence(1L);
        clientCustomFieldMetaType.setLength(1L);
        clientCustomFieldMetaType.setName("name3");
        clientCustomFieldMetaType.setReadOnly(false);
        clientCustomFieldMetaType.setReferenceSet("referenceSet");
        clientCustomFieldMetaType.setRequired(true);
        clientCustomFieldMetaType.setValidationRule("validation=true");
        clientCustomFieldMetaType.setVisible(true);
        Set<TranslationType> translationTypes = new HashSet<TranslationType>();
        TranslationType translationType = new TranslationType();
        translationType.setError("error");
        translationType.setHelp("help");
        translationType.setLabel("label");
        translationType.setLanguage("EN");
        translationTypes.add(translationType);
        clientCustomFieldMetaType.setTranslation(translationTypes);
        return clientCustomFieldMetaType;
    }

    @Test
    public void testGetFieldTypes() {
        for (ReferenceCodeType ct : service.getFieldType(uc)) {
            //System.out.println(ct.getCode());
            //System.out.println(ct.getDescriptions());
            //System.out.println("getCodeForId : " + FieldTypeEnum.getCodeForId(ct.getCode()));
        }
    }

    @Test
    public void testGetDTOBindings() {
        for (CodeType ct : service.getDTOBindings(uc)) {
            //System.out.println(ct.getCode());
            //System.out.println(ct.getSet());
        }
    }

    @Test
    public void testGetAllClientCustomFieldMeta() {
        List<ClientCustomFieldMetaType> result = service.getAllClientCustomFieldMeta(uc, "EventLog", "TEXT", "Narrat%", "");
        //System.out.println(result.size());
    }

    // @Test
    public void testGetClientCustomFieldMeta() {
        ClientCustomFieldMetaType clientCustomFieldMetaType = service.getClientCustomFieldMeta(uc, 28L);
        assertNotNull(clientCustomFieldMetaType);
        //System.out.println(clientCustomFieldMetaType.toString());
    }

    @Test
    public void testSearchAllGroup() {
        List<GroupType> groupTypes = service.searchAllGroup(uc, "EventLog1");
        assert (groupTypes.isEmpty());
        //System.out.println("GroupType : " + groupTypes);
    }

    @Test
    public void testIsGroupExist() {

        assert (!service.isGroupExist(uc, "Test Name", "TestEntity", false));
    }

    @Test
    public void testIsMaximumDTOBiningExceed() {

        assert (service.isMaximumDTOBiningExceeded(uc, "CASENOTE"));
        //System.out.println("GroupType : " + service.isMaximumDTOBiningExceeded(uc,"CASENOTE"));
    }

    @Test
    public void testGetCCFFields() {

        String entity = "CASENOTE";
        List<String> ccfNames = service.getClientCustomFields(entity, "EN");
        for (String name : ccfNames) {
            log.info(name);
        }

    }

    @Test //(dependsOnMethods = "testUpdateCCFMetaValue", enabled = true)
    public void testDeleteCCFMetaData() {
        Boolean ret = service.deleteCCFMetaData(uc, 3l);
        log.info(ret.toString());
    }

}
