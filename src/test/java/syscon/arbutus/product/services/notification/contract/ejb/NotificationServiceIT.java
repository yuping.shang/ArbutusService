package syscon.arbutus.product.services.notification.contract.ejb;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.PersonIdentityTest;
import syscon.arbutus.product.services.common.PersonTest;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.notification.contract.dto.BroadcastMessageSearchType;
import syscon.arbutus.product.services.notification.contract.dto.BroadcastMessageType;
import syscon.arbutus.product.services.notification.contract.dto.BroadcastMessagesReturnType;
import syscon.arbutus.product.services.notification.contract.interfaces.NotificationService;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;

import static org.testng.Assert.assertNotNull;

/**
 * NotificationServiceIT to test APIs of Notification service.
 *
 * @author lhan
 */
public class NotificationServiceIT extends BaseIT {
    private UserContext uc = null;
    private Logger log = LoggerFactory.getLogger(NotificationServiceIT.class);

    private NotificationService service;
    private PersonService personService;
    private Long facilityId;
    private List<Long> broadcastMessageIds = new ArrayList<Long>();
    private Date endDateTime;
    private String messageName;
    private String messageNarrative;
    private Date startDateTime;
    private Long facilitySetId;
    private Long personId;
    private Long personIdentityId;

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();

        personId = (new PersonTest()).createPerson();
        personIdentityId = (new PersonIdentityTest()).createPersonIdentity();

        assertNotNull(personId);
        assertNotNull(personIdentityId);

        facilityId = 1L;
        facilitySetId = 1L;

        messageName = "name1";
        messageNarrative = "narrative";
        startDateTime = getDate(2012, 01, 01);
        endDateTime = getDate(2020, 01, 01);
    }

    @AfterClass
    public void afterClass() {
        for (Long id : broadcastMessageIds) {
            service.deleteBroadcastMessage(uc, id);
        }

        if (personId != null) {
            personService.deletePersonTypeInternally(uc, personId);
        }
    }

    @Test
    public void getVersion() {
        String rtn = service.getVersion(uc);
        Assert.assertEquals(rtn, "1.0");
    }

    @Test
    public void createBroadcastMessage() {
        //create with only mandatory
        BroadcastMessageType broadcastMessage = generateDefaultBroadCastMessage(true);

        BroadcastMessageType rtn1 = service.createBroadcastMessage(uc, broadcastMessage);

        verifyBroadcastMessage(rtn1, broadcastMessage);

        broadcastMessageIds.add(rtn1.getBroadCastMessageId());

        //create with all fields
        broadcastMessage = generateDefaultBroadCastMessage(true);

        BroadcastMessageType rtn2 = service.createBroadcastMessage(uc, broadcastMessage);

        verifyBroadcastMessage(rtn2, broadcastMessage);

        broadcastMessageIds.add(rtn2.getBroadCastMessageId());
    }

    @Test(dependsOnMethods = "createBroadcastMessage", enabled = true)
    public void getBroadcastMessage() {
        //create with only mandatory
        BroadcastMessageType broadcastMessage = generateDefaultBroadCastMessage(true);

        BroadcastMessageType rtn = service.createBroadcastMessage(uc, broadcastMessage);

        verifyBroadcastMessage(rtn, broadcastMessage);

        Long broadcastMessageId = rtn.getBroadCastMessageId();
        broadcastMessageIds.add(broadcastMessageId);

        //test get
        BroadcastMessageType ret = service.getBroadcastMessage(uc, broadcastMessageId);
        verifyBroadcastMessage(ret, rtn);

        Assert.assertEquals("Liang".toUpperCase(), ret.getCreatedByLastName());
        Assert.assertTrue(ret.getCreatedByFirstName().contains("Jason".toUpperCase()));
    }

    @Test(dependsOnMethods = { "getBroadcastMessage" }, enabled = true)
    public void deleteBroadcastMessage() {
        //create with only mandatory
        BroadcastMessageType broadcastMessage = generateDefaultBroadCastMessage(true);

        BroadcastMessageType rtn = service.createBroadcastMessage(uc, broadcastMessage);

        verifyBroadcastMessage(rtn, broadcastMessage);

        Long broadcastMessageId = rtn.getBroadCastMessageId();

        Long deleteRtn = service.deleteBroadcastMessage(uc, broadcastMessageId);
        assert (deleteRtn.equals(1L));

        try {
            service.getBroadcastMessage(uc, broadcastMessageId);
        } catch (Exception e) {
            assert (e instanceof DataNotExistException);
        }

    }

    @Test(dependsOnMethods = { "deleteBroadcastMessage" }, enabled = true)
    public void updateBroadcastMessage() {
        //create with only mandatory
        BroadcastMessageType broadcastMessage = generateDefaultBroadCastMessage(true);

        BroadcastMessageType rtn = service.createBroadcastMessage(uc, broadcastMessage);

        verifyBroadcastMessage(rtn, broadcastMessage);

        broadcastMessageIds.add(rtn.getBroadCastMessageId());

        //test update
        rtn.setMessageName("new name");
        rtn.setMessageNarrative("new narrative");
        rtn.setCreatedByPersonIdentityId(personIdentityId);
        rtn.setStartDateTime(getDate(2012, 02, 02));
        rtn.setEndDateTime(getDate(2020, 02, 02));

        BroadcastMessageType rtnUpdate = service.updateBroadcastMessage(uc, rtn);

        verifyBroadcastMessage(rtn, rtnUpdate);
        Assert.assertTrue(rtn.getModifiedDateTime().getTime() < rtnUpdate.getModifiedDateTime().getTime());

    }

    @Test(dependsOnMethods = { "updateBroadcastMessage" }, enabled = true)
    public void searchBroadcastMessage() {
        for (Long id : broadcastMessageIds) {
            service.deleteBroadcastMessage(uc, id);
        }

        broadcastMessageIds.clear();

        //create with only mandatory
        BroadcastMessageType broadcastMessage = generateDefaultBroadCastMessage(true);

        BroadcastMessageType rtn = service.createBroadcastMessage(uc, broadcastMessage);

        verifyBroadcastMessage(rtn, broadcastMessage);

        broadcastMessageIds.add(rtn.getBroadCastMessageId());

        BroadcastMessageSearchType search = null;

        //search by create to data time
        search = new BroadcastMessageSearchType();
        search.setCreatedToDate(new Date());

        BroadcastMessagesReturnType rtnSearch = service.searchBroadcastMessage(uc, search, null, null, null);

        Assert.assertNotNull(rtnSearch);
        Assert.assertEquals(1, rtnSearch.getTotalSize().intValue());
        Assert.assertEquals(1, rtnSearch.getBroadcastMessages().size());

        //get effective messages
        List<BroadcastMessageType> rtnEffective = service.getEffectiveBroadcaseMessages(uc);

        Assert.assertNotNull(rtnEffective);
        Assert.assertEquals(1, rtnEffective.size());

        //create with only mandatory
        broadcastMessage = generateDefaultBroadCastMessage(true);
        broadcastMessage.setStartDateTime(getDate(2020, 03, 03));
        broadcastMessage.setEndDateTime(getDate(2020, 03, 04));

        rtn = service.createBroadcastMessage(uc, broadcastMessage);

        verifyBroadcastMessage(rtn, broadcastMessage);

        broadcastMessageIds.add(rtn.getBroadCastMessageId());

        //search by create to data time again
        search = new BroadcastMessageSearchType();
        search.setCreatedToDate(new Date());

        rtnSearch = service.searchBroadcastMessage(uc, search, null, null, null);

        Assert.assertNotNull(rtnSearch);
        Assert.assertEquals(2, rtnSearch.getTotalSize().intValue());
        Assert.assertEquals(2, rtnSearch.getBroadcastMessages().size());

        //get effective messages
        rtnEffective = service.getEffectiveBroadcaseMessages(uc);

        Assert.assertNotNull(rtnEffective);
        Assert.assertEquals(1, rtnEffective.size());
    }

    //////////////////////////////////////////////Private methods////////////////////////////////////
    private void lookupJNDILogin() {

        log.info("Before JNDI lookup");
        service = (NotificationService) JNDILookUp(this.getClass(), NotificationService.class);
        personService = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        assertNotNull(service);
        assertNotNull(personService);

        //init user context and login
        uc = super.initUserContext();
        clientlogin("devtest", "devtest");
    }

    private void clientlogin(String user, String password) {

        SecurityClient client;

        try {
            client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password); //"devtest", "devtest" nice regular service user
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void verifyBroadcastMessage(BroadcastMessageType rtn, BroadcastMessageType broadcastMessage) {
        Assert.assertNotNull(rtn);
        Assert.assertNotNull(rtn.getBroadCastMessageId());
        Assert.assertEquals(rtn.getMessageName(), broadcastMessage.getMessageName());
        Assert.assertEquals(rtn.getMessageNarrative(), broadcastMessage.getMessageNarrative());
        Assert.assertEquals(rtn.getCreatedByPersonIdentityId(), broadcastMessage.getCreatedByPersonIdentityId());
        Assert.assertNotNull(rtn.getCreatedDateTime());
        Assert.assertEquals(rtn.getEndDateTime().getTime(), broadcastMessage.getEndDateTime().getTime());
        Assert.assertEquals(rtn.getFacilityId(), broadcastMessage.getFacilityId());
        Assert.assertEquals(rtn.getFacilitySetId(), broadcastMessage.getFacilitySetId());
        Assert.assertNotNull(rtn.getModifiedDateTime());
        Assert.assertEquals(rtn.getStartDateTime().getTime(), broadcastMessage.getStartDateTime().getTime());

    }

    private BroadcastMessageType generateDefaultBroadCastMessage(boolean onlyMadatory) {
        BroadcastMessageType broadcastMessage = new BroadcastMessageType();

        broadcastMessage.setCreatedByPersonIdentityId(personIdentityId);
        broadcastMessage.setEndDateTime(endDateTime);
        broadcastMessage.setMessageName(messageName);
        broadcastMessage.setMessageNarrative(messageNarrative);
        broadcastMessage.setStartDateTime(startDateTime);

        if (onlyMadatory) {
            return broadcastMessage;
        }

        broadcastMessage.setFacilityId(facilityId);
        broadcastMessage.setFacilitySetId(facilitySetId);

        return broadcastMessage;
    }

    private Date getDate(int year, int month, int date) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, date);
        Date rtn = cal.getTime();
        return rtn;
    }

}
