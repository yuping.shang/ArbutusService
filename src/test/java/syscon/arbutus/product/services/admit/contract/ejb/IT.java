package syscon.arbutus.product.services.admit.contract.ejb;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.PersonIdentityTest;
import syscon.arbutus.product.services.common.SupervisionTest;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.supervision.contract.dto.AdmitOffenderType;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

import static org.testng.Assert.assertNotNull;

public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);

    private SupervisionService service;

    private PersonIdentityTest testPI;
    private SupervisionTest testSUP;

    @BeforeClass
    public void beforeClass() {

        log.info("beforeClass Begin - JNDI lookup");
        service = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        uc = super.initUserContext();

        init();

    }

    private void init() {
        testPI = new PersonIdentityTest();
        testSUP = new SupervisionTest();
    }

    @AfterClass
    public void afterClass() {
    }

    @Test(enabled = false)
    public void lookupJNDI() {
        log.info("lookupJNDI Begin");
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "lookupJNDI" }, enabled = false)
    public void reset() {
    }

    //@Test
    public void admitOffenderPositive() {

        //toggleSupervision="NONE"
        Long personIdentityId = testPI.createPersonIdentity();
        String toggleSupervision = AdmitOffenderType.SupervisionAction.NONE.value();

        AdmitOffenderType admitOffender = createAdmitOffenderType(personIdentityId, toggleSupervision, "NEW");


        SupervisionType supervisionId = service.admitOffender(uc, admitOffender);
        assert (supervisionId != null);

		/*
        //Need release inmate to test reopen scenario
		
		//toggleSupervision="REOPEN"
		personIdentityId = testPI.createPersonIdentity();
		toggleSupervision= AdmitOffenderType.SupervisionAction.CREATE.value();
		admitOffender = createAdmitOffenderType(personIdentityId, toggleSupervision);				
		supervisionService.admitOffender(uc, admitOffender);
		
		//toggleSupervision="CREATE"
		personIdentityId = testPI.createPersonIdentity();
		toggleSupervision= AdmitOffenderType.SupervisionAction.CREATE.value();
		admitOffender = createAdmitOffenderType(personIdentityId, toggleSupervision);				
		supervisionService.admitOffender(uc, admitOffender);
		*/

    }

    @Test
    public void admitOffenderNegative() {

        //toggleSupervision="NONE"
        Long personIdentityId = testPI.createPersonIdentity();
        String toggleSupervision = AdmitOffenderType.SupervisionAction.NONE.value();

        //Not passing the movement reason, fail on movementType validation, it will roll back the transaction.
        AdmitOffenderType admitOffender = createAdmitOffenderType(personIdentityId, toggleSupervision, null);

        verifyAdmitOffenderNegative(admitOffender, InvalidInputException.class);

    }

    private void verifyAdmitOffenderNegative(AdmitOffenderType admitOffender, Class<?> expectionClazz) {
        try {
            SupervisionType ret = service.admitOffender(uc, admitOffender);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private AdmitOffenderType createAdmitOffenderType(Long personIdentityId, String toggleSupervision, String movementReason) {

        AdmitOffenderType admitOffender = new AdmitOffenderType();

        admitOffender.setMovementDate(new Date());
        admitOffender.setMovementTime(new Date());
        admitOffender.setFromFacility(179L);        //107th Precinct (Queens)
        admitOffender.setToFacility(1L);            //Denver County Jail
        admitOffender.setMovementReason(movementReason);        //New Admission
        admitOffender.setCommentText("New Suppervision");

        admitOffender.setPersonIdentityId(personIdentityId);
        admitOffender.setStaffId(9L);
        admitOffender.setToggleSupervision(toggleSupervision);

        SupervisionType supervisionType = new SupervisionType();
        String supervisionDisplayId = testSUP.getSupervisionDisplayNumber(null);
        supervisionType.setSupervisionDisplayID(supervisionDisplayId);
        admitOffender.setSupervisionType(supervisionType);

        //To added the CCF data

        return admitOffender;

    }
}
