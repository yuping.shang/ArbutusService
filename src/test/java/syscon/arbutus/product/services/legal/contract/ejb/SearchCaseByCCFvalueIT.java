package syscon.arbutus.product.services.legal.contract.ejb;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueType;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType;
import syscon.arbutus.product.services.legal.contract.dto.composite.CaseOffendersReturnType;
import syscon.arbutus.product.services.legal.contract.dto.composite.InquiryCaseSearchType;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * This test is only run by developer manually.
 * The pre-condition is done by creating cases with CCF from web UI manually.
 * create 3 cases with DOCKET number and with CCF value null, text1 & text2.
 *
 * @author LHan
 */
public class SearchCaseByCCFvalueIT extends BaseIT {

    private static final String CASE_IDENTIFIER_TYPE_DOCKET = "DOCKET";
    //private CaseManagementServiceBean service;   //for local test
    private UserContext uc = null; //new UserContext();
    private Logger log = LoggerFactory.getLogger(SearchCaseByCCFvalueIT.class);

    private LegalService service;

    /**
     * <p>Cut the time portion of a given Date object.</p>
     *
     * @param dateTime A Date object to trim.
     * @return A Date object.
     */
    private static Date getDatePartial(Date dateTime) {

        if (dateTime == null) {
			return null;
		}

        Calendar cal = Calendar.getInstance();
        cal.setTime(dateTime);

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();
    }

    @BeforeMethod
    public void beforeMethod() {
    }

    //for release
    private void lookupJNDILogin() {

        log.info("Before JNDI lookup");
        service = (LegalService) JNDILookUp(this.getClass(), LegalService.class);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();
        clientlogin("devtest", "devtest");
    }

    @Test
    public void testLookupJNDI() {
        assertNotNull(service);
    }

    //////////////////////////////////////////private methods/////////////////////////////////////////

    @Test(dependsOnMethods = { "testLookupJNDI" })
    public void testInquireCases() {

        //inquire by CaseIdentifierType and CCF
        InquiryCaseSearchType caseSearchType = new InquiryCaseSearchType();
        caseSearchType.setCaseIdentifierType(CASE_IDENTIFIER_TYPE_DOCKET);

        ClientCustomFieldValueType ccfValueType = new ClientCustomFieldValueType();
        ccfValueType.setValue("text1");
        ccfValueType.setCcfId(1L);

        Set<ClientCustomFieldValueType> ccfValueTypes = new HashSet<ClientCustomFieldValueType>();
        ccfValueTypes.add(ccfValueType);

        caseSearchType.setCCFSearchCriteria(ccfValueTypes);

        CaseOffendersReturnType ret = service.inquireCaseOffenders(uc, caseSearchType, 0l, 100l, null);

        assert (ret != null);
        assert (ret.getCaseOffenders().size() == 1);

        //inquire by CCF only
        caseSearchType = new InquiryCaseSearchType();

        ccfValueType = new ClientCustomFieldValueType();
        ccfValueType.setValue("text1");
        ccfValueType.setCcfId(1L);

        ccfValueTypes = new HashSet<ClientCustomFieldValueType>();

        caseSearchType.setCCFSearchCriteria(ccfValueTypes);
        ccfValueTypes.add(ccfValueType);

        ret = service.inquireCaseOffenders(uc, caseSearchType, 0l, 100l, null);

        assert (ret != null);
        assert (ret.getCaseOffenders().size() == 1);

        //inquire by CaseIdentifierType and CCF with wild card search
        /*caseSearchType = new InquiryCaseSearchType();
        caseSearchType.setCaseIdentifierType(CASE_IDENTIFIER_TYPE_DOCKET);

		ccfValueType = new ClientCustomFieldValueType();
		ccfValueType.setValue("text*");
		ccfValueType.setCcfId(1L);

		ccfValueTypes = new HashSet<ClientCustomFieldValueType>();
		ccfValueTypes.add(ccfValueType);

		caseSearchType.setCCFSearchCriteria(ccfValueTypes);

		ret = service.inquireCaseOffenders(uc, caseSearchType, 0l, 100l, null);

		assert(ret != null);
		assert(ret.getCaseOffenders().size() == 2);		*/
    }

    private void clientlogin(String user, String password) {

        SecurityClient client;

        try {
            client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password); //"devtest", "devtest" nice regular service user
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    private void verfiyCreatedCaseInfo(CaseInfoType actual, CaseInfoType expected) {
        assertNotNull(actual.getCaseInfoId());
        //TODO: assertEquals(actual.getAssociations(), expected.getAssociations());
        assertEquals(actual.getCaseCategory(), expected.getCaseCategory());

        verifyCreatedCaseIdentifiers(actual.getCaseIdentifiers(), expected.getCaseIdentifiers());

        assertEquals(actual.getCaseInfoIds(), expected.getCaseInfoIds());
        assertEquals(actual.getStatusChangeReason(), expected.getStatusChangeReason());
        assertEquals(actual.getCaseType(), expected.getCaseType());
        assertEquals(actual.getChargeIds(), expected.getChargeIds());

        verifyCreatedComment(actual.getComment(), expected.getComment());

        assertEquals(actual.getConditionIds(), expected.getConditionIds());
        assertEquals(getDatePartial(actual.getCreatedDate()), getDatePartial(expected.getCreatedDate()));
        assertEquals(actual.getFacilityId(), expected.getFacilityId());
        assertEquals(getDatePartial(actual.getIssuedDate()), getDatePartial(expected.getIssuedDate()));
        assertEquals(actual.getOrderSentenceIds(), expected.getOrderSentenceIds());
        assertEquals(actual.getSentenceStatus(), expected.getSentenceStatus());
        assertEquals(actual.getSupervisionId(), expected.getSupervisionId());
        assertEquals(actual.isActive(), expected.isActive());
        assertEquals(actual.isDiverted(), expected.isDiverted());
        assertEquals(actual.isDivertedSuccess(), expected.isDivertedSuccess());
    }

    private void verifyCreatedComment(CommentType actual, CommentType expected) {
        if ((actual == null && expected == null) || (actual != null && expected == null) || (actual == null && expected != null)) {
            Assert.assertEquals(actual, expected);
            return;
        }

        assertEquals(actual.getComment(), expected.getComment());
        assertEquals(actual.getCommentDate(), expected.getCommentDate());
        assertEquals(actual.getUserId(), expected.getUserId());
    }

    private void verifyCreatedCaseIdentifiers(List<CaseIdentifierType> actual, List<CaseIdentifierType> expected) {
        if ((actual == null && expected == null) || (actual != null && expected == null) || (actual == null && expected != null)) {
            Assert.assertEquals(actual, expected);
            return;
        }

        Set<CaseIdentifierType> actual1 = new HashSet<CaseIdentifierType>(actual);
        Set<CaseIdentifierType> expected1 = new HashSet<CaseIdentifierType>(expected);
        Assert.assertEquals(actual1, expected1);
    }
}
