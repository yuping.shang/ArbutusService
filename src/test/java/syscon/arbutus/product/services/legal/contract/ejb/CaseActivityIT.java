package syscon.arbutus.product.services.legal.contract.ejb;

import java.util.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.legal.contract.dto.LegalModule;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CaseActivitySearchType;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivitiesReturnType;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivityType;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.SearchSetMode;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class CaseActivityIT extends BaseIT {
    final static Long success = 1L;
    private static final String CASE_CATEGORY_IJ = "IJ";
    private static final String CASE_TYPE_ADULT = "ADULT";
    private static final String SENTENCE_STATUS_SENTENCED = "SENTENCED";
    private static final String CASE_STATUS_REASON_DISMISSED = "CD";
    private static final String CASE_IDENTIFIER_TYPE_DOCKET = "DOCKET";
    private static final String CASE_IDENTIFIER_TYPE_INDICTMENT = "INDICTMENT";
    private static Logger log = LoggerFactory.getLogger(CaseActivityIT.class);
    Long ID;
    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").serializeNulls().create();
    private List<CourtActivityType> createdCourtActivities = new ArrayList<CourtActivityType>();
    private List<Long> orderIds;
    private List<Long> historySetCourt = new ArrayList<Long>();
    private LegalService service;
    private Long caseId;

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        lookupJNDILogin();

        uc = super.initUserContext();
    }

    //for release
    private void lookupJNDILogin() {

        log.info("Before JNDI lookup");
        service = (LegalService) JNDILookUp(this.getClass(), LegalService.class);
        assertNotNull(service);

    }

    @Test
    public void testLookupJNDI() {
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "testLookupJNDI" })
    public void testGetVersion() {
        String version = service.getVersion(null);
        assert ("1.0".equals(version));
    }

    @Test(dependsOnMethods = { "testGetVersion" })
    public void testDeleteAll() {
        Long ret = service.deleteAll(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());
    }

    @Test(dependsOnMethods = { "testDeleteAll" })
    public void testReset() {
        Long ret = service.deleteAll(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());
        CaseInfoType caseInfo = generateDefaultCaseInfo(true);
        CaseInfoType rtn = service.createCaseInfo(uc, caseInfo, null);
        assert (ret != null);
        caseId = rtn.getCaseInfoId();

        CreateOrderTest orderTest = new CreateOrderTest();
        orderTest.beforeClass();
        orderTest.createOrderConfiguration();
        orderTest.createAndGet();
        orderIds = orderTest.getOrderIds();
    }

    @Test(dependsOnMethods = { "testReset" })
    public void testCreate() {

        //courtActivity1: madatory fields
        CourtActivityType courtActivity = new CourtActivityType();

        //test an empty object;
        verfiyCreateNegative(courtActivity);
        //assertEquals(service.createCaseActivity(uc, courtActivity).getReturnCode().longValue(), 2002l);

        //test when ActivityCategory has value.
        courtActivity.setActivityCategory("COURT");
        verfiyCreateNegative(courtActivity);
        //assertEquals(service.createCaseActivity(uc, courtActivity).getReturnCode().longValue(), 2002l);

        //test when ActivityType has value.
        courtActivity.setActivityType("INITCRT");
        verfiyCreateNegative(courtActivity);
        //assertEquals(service.createCaseActivity(uc, courtActivity).getReturnCode().longValue(), 2002l);

        //test when ActivityStatus has value.
        courtActivity.setActivityStatus("ACTIVE");
        verfiyCreateNegative(courtActivity);
        //assertEquals(service.createCaseActivity(uc, courtActivity).getReturnCode().longValue(), 2002l);

        //test when ActivityOccurredDate has value.
        courtActivity.setActivityOccurredDate(new Date());
        verfiyCreateNegative(courtActivity);
        //assertEquals(service.createCaseActivity(uc, courtActivity).getReturnCode().longValue(), 2002l);

        //test all mandatory fields have values, create should pass now.
        courtActivity.getCaseIds().add(caseId);

        CourtActivityType rtn = service.createCaseActivity(uc, courtActivity);

        assertNotNull(rtn, null);

        CourtActivityType createdCourtActivity = rtn;
        assertNotNull(createdCourtActivity);

        assertNotNull(createdCourtActivity.getCaseActivityIdentification());
        assertEquals(createdCourtActivity.getActivityCategory(), courtActivity.getActivityCategory());
        assertEquals(createdCourtActivity.getActivityType(), courtActivity.getActivityType());
        assertEquals(createdCourtActivity.getActivityOccurredDate(), courtActivity.getActivityOccurredDate());
        assertEquals(createdCourtActivity.getActivityStatus(), courtActivity.getActivityStatus());
        assertEquals(createdCourtActivity.getCaseIds().size(), courtActivity.getCaseIds().size());

        createdCourtActivities.add(createdCourtActivity);

        //test cretae activity with all fields have values
        courtActivity = generateDefaultCourtActivity(false);

        assertEquals(courtActivity.getOutcomeDate(), null);

        rtn = service.createCaseActivity(uc, courtActivity);

        Date afterCreate = new Date();

        assertNotNull(rtn, null);

        createdCourtActivity = rtn;
        assertNotNull(createdCourtActivity);

        //test getStamp for create API:
        StampType stamp = service.getStamp(uc, LegalModule.CASE_ACTIVITY, createdCourtActivity.getCaseActivityIdentification());
        Assert.assertTrue(stamp.getCreateDateTime().getTime() < afterCreate.getTime());
        //TODO: Assert.assertTrue(stamp.getCreateDateTime().getTime() > beforeCreate.getTime());
        Assert.assertTrue(stamp.getModifyDateTime().before(afterCreate));
        //TODO Assert.assertTrue(stamp.getModifyDateTime().after(beforeCreate));
        Assert.assertTrue("devtest".equals(stamp.getCreateUserId()));
        Assert.assertTrue("devtest".equals(stamp.getModifyUserId()));
        Assert.assertTrue(stamp.getInvocationContext() instanceof String);
        assertNotNull(createdCourtActivity.getOutcomeDate());

        assertNotNull(createdCourtActivity.getCaseActivityIdentification());
        assertEquals(createdCourtActivity.getActivityCategory(), courtActivity.getActivityCategory());
        assertEquals(createdCourtActivity.getActivityType(), courtActivity.getActivityType());
        assertEquals(createdCourtActivity.getActivityOccurredDate(), courtActivity.getActivityOccurredDate());
        assertEquals(createdCourtActivity.getActivityStatus(), courtActivity.getActivityStatus());
        assertEquals(createdCourtActivity.getChargeIds().size(), courtActivity.getChargeIds().size());
        assertEquals(createdCourtActivity.getOrderInititatedIds().size(), courtActivity.getOrderInititatedIds().size());
        assertEquals(createdCourtActivity.getOrderResultedIds().size(), courtActivity.getOrderResultedIds().size());
        assertEquals(createdCourtActivity.getFacilityId(), courtActivity.getFacilityId());
        assertEquals(createdCourtActivity.getFacilityInternalLocationId(), courtActivity.getFacilityInternalLocationId());

        createdCourtActivities.add(createdCourtActivity);

        //test create activity with same values again since SDD has not limit to create duplicated activities.
        courtActivity = generateDefaultCourtActivity(false);

        rtn = service.createCaseActivity(uc, courtActivity);

        assertNotNull(rtn, null);

        createdCourtActivity = rtn;
        assertNotNull(createdCourtActivity);

        assertNotNull(createdCourtActivity.getCaseActivityIdentification());
        assertEquals(createdCourtActivity.getActivityCategory(), courtActivity.getActivityCategory());
        assertEquals(createdCourtActivity.getActivityType(), courtActivity.getActivityType());
        assertEquals(createdCourtActivity.getActivityOccurredDate(), courtActivity.getActivityOccurredDate());
        assertEquals(createdCourtActivity.getActivityStatus(), courtActivity.getActivityStatus());
        assertEquals(createdCourtActivity.getChargeIds().size(), courtActivity.getChargeIds().size());
        assertEquals(createdCourtActivity.getOrderInititatedIds().size(), courtActivity.getOrderInititatedIds().size());
        assertEquals(createdCourtActivity.getOrderResultedIds().size(), courtActivity.getOrderResultedIds().size());

        createdCourtActivities.add(createdCourtActivity);

    }

    @Test(dependsOnMethods = { "testCreate" })
    public void testGet() {
        //test get by valid ids.
        for (CourtActivityType activity : createdCourtActivities) {
            Long id = activity.getCaseActivityIdentification();
            CourtActivityType activityRet = service.getCaseActivity(uc, id);
            assertNotNull(activityRet);
            assertEquals(activityRet.getCaseActivityIdentification(), id);

        }

        //test get by invalid id
        verfiyGetNegative(9999999L);

    }

    @Test(dependsOnMethods = { "testGet" })
    public void testGetAllandGetCount() {
        Set<Long> ret = service.getAll(uc, LegalModule.CASE_ACTIVITY);
        Assert.assertTrue(ret.size() == createdCourtActivities.size());

        for (Long id : ret) {
            CourtActivityType activityRet = service.getCaseActivity(uc, id);
            Assert.assertNotEquals(activityRet, null);
            assertEquals(activityRet.getCaseActivityIdentification(), id);

        }
        assertEquals(ret.size(), service.getCount(uc, LegalModule.CASE_ACTIVITY).intValue());
    }

    @Test(dependsOnMethods = { "testGetAllandGetCount" }, enabled = true)
    public void testUpdateCaseActivity() {

        //Negative return test.
        verfiyUpdateNegative(null);
        verfiyUpdateNegative(new CourtActivityType());

        //no id.
        CourtActivityType courtActivity = generateDefaultCourtActivity(true);
        verfiyUpdateNegative(courtActivity);

        Date fromDate = new Date();
        Date toDate = new Date();

        //positive test.
        for (CourtActivityType activity : createdCourtActivities) {
            int i = 0;
            List<CourtActivityType> historyRet;

            CourtActivityType activityRet = service.getCaseActivity(uc, activity.getCaseActivityIdentification());
            courtActivity = activityRet;

            //TODO: courtActivity.setActivityCategory(new CodeType(ReferenceSet.ACTIVITY_CATEGORY.value(),"PROB"));
            //verfiyUpdateNegative(courtActivity);

            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), null, null);
            assertEquals(historyRet.size(), i + 1);

            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), fromDate, toDate);
            assertEquals(historyRet.size(), 0);

            courtActivity.setActivityCategory("COURT");

            //test when ActivityType has updated value.
            courtActivity.setActivityType("PRELHEAR");
            fromDate = new Date();

            Date beforeUpdate = new Date();
            activityRet = service.updateCaseActivity(uc, courtActivity);
            Date afterUpdate = new Date();

            assertNotNull(activityRet);

            //test getStamp for update API:
            StampType stamp = service.getStamp(uc, LegalModule.CASE_ACTIVITY, activityRet.getCaseActivityIdentification());
            Assert.assertTrue(stamp.getCreateDateTime().before(afterUpdate));
            Assert.assertTrue(stamp.getCreateDateTime().before(beforeUpdate));
            Assert.assertTrue(stamp.getModifyDateTime().before(afterUpdate));
            //TODO Assert.assertTrue(stamp.getModifyDateTime().after(beforeUpdate));
            Assert.assertTrue("devtest".equals(stamp.getCreateUserId()));
            Assert.assertTrue("devtest".equals(stamp.getModifyUserId()));
            Assert.assertTrue(stamp.getInvocationContext() instanceof String);

            i++;
            assertEquals(activityRet.getActivityType(), "PRELHEAR");
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), null, null);
            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), fromDate, toDate);
            assertEquals(historyRet.size(), 1);

            //test when ActivityStatus has updated value.
            courtActivity.setActivityStatus("INACTIVE");
            fromDate = new Date();
            activityRet = service.updateCaseActivity(uc, courtActivity);
            assertNotNull(activityRet);
            i++;
            assertEquals(activityRet.getActivityStatus(), "INACTIVE");
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), null, null);
            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), fromDate, toDate);
            assertEquals(historyRet.size(), 1);

            //test ActivityOccurredDate has update value.
            Date newDate = new Date();
            courtActivity.setActivityOccurredDate(newDate);
            fromDate = new Date();
            activityRet = service.updateCaseActivity(uc, courtActivity);
            assertNotNull(activityRet);
            i++;
            assertEquals(activityRet.getActivityOccurredDate(), newDate);
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), null, null);
            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), fromDate, toDate);
            assertEquals(historyRet.size(), 1);

            //test ActivityOutcome has updated value.
            Date oldOutComeDate = courtActivity.getOutcomeDate();
            courtActivity.setActivityOutcome("PROBATION");
            fromDate = new Date();
            activityRet = service.updateCaseActivity(uc, courtActivity);
            assertNotNull(activityRet);
            i++;
            assertEquals(activityRet.getActivityOutcome(), "PROBATION");
            Date outcomeDate = activityRet.getOutcomeDate();
            assertNotNull(outcomeDate); //outcome date should be set value.
            Assert.assertNotEquals(outcomeDate, oldOutComeDate); //outcome date should not equal to old value.

            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), null, null);
            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), fromDate, toDate);
            assertEquals(historyRet.size(), 1);

            //test comment
            CommentType comment = new CommentType();
            comment.setComment("comment2");
            comment.setCommentDate(new Date());
            courtActivity.setActivityComment(comment);
            fromDate = new Date();

            activityRet = service.updateCaseActivity(uc, courtActivity);
            assertNotNull(activityRet);
            assertEquals(outcomeDate.getTime(), activityRet.getOutcomeDate().getTime()); //outcome not change, outcome date should not be changed.
            i++;
            assertEquals(activityRet.getActivityComment().getComment(), comment.getComment());
            assertEquals(activityRet.getActivityComment().getCommentDate(), comment.getCommentDate());
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), fromDate, toDate);

            assertEquals(historyRet.size(), 1);

            //test judge
            courtActivity.setJudge("judge2");
            fromDate = new Date();

            activityRet = service.updateCaseActivity(uc, courtActivity);
            assertNotNull(activityRet);
            i++;
            assertEquals(activityRet.getJudge(), "judge2");
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), fromDate, toDate);

            assertEquals(historyRet.size(), 1);

            //test facility association
            courtActivity.setFacilityId(9991L);
            activityRet = service.updateCaseActivity(uc, courtActivity);
            assertNotNull(activityRet);
            i++;
            assertEquals(activityRet.getFacilityId(), courtActivity.getFacilityId());
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), fromDate, toDate);

            assertEquals(historyRet.size(), 2);

            //test organization association
            courtActivity.setOrganizationId(9991L);
            activityRet = service.updateCaseActivity(uc, courtActivity);
            assertNotNull(activityRet);
            i++;
            assertEquals(activityRet.getOrganizationId(), courtActivity.getOrganizationId());
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), fromDate, toDate);

            assertEquals(historyRet.size(), 3);

            //test static associations positive
            Set<Long> chargeAssociations = new HashSet<Long>();
            Set<Long> orderInitialAssociations = new HashSet<Long>();
            Set<Long> orderResultAssociations = new HashSet<Long>();

            Long asso1 = 3011L;
            chargeAssociations.add(asso1);
            asso1 = 3021L;
            chargeAssociations.add(asso1);
            asso1 = 3031L;
            chargeAssociations.add(asso1);

            orderInitialAssociations.add(orderIds.get(2));
            orderInitialAssociations.add(orderIds.get(3));

            orderResultAssociations.add(orderIds.get(2));

            courtActivity.setChargeIds(chargeAssociations);
            activityRet = service.updateCaseActivity(uc, courtActivity);
            assertNotNull(activityRet);
            i++;
            assertEquals(activityRet.getChargeIds(), courtActivity.getChargeIds());
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), fromDate, toDate);

            assertEquals(historyRet.size(), 4);

            courtActivity.setOrderInititatedIds(orderInitialAssociations);
            activityRet = service.updateCaseActivity(uc, courtActivity);
            assertNotNull(activityRet);
            i++;
            assertEquals(activityRet.getOrderInititatedIds(), courtActivity.getOrderInititatedIds());
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), fromDate, toDate);

            assertEquals(historyRet.size(), 5);

            courtActivity.setOrderResultedIds(orderResultAssociations);
            activityRet = service.updateCaseActivity(uc, courtActivity);
            assertNotNull(activityRet);
            i++;
            assertEquals(activityRet.getOrderResultedIds(), courtActivity.getOrderResultedIds());
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseActivityHistory(uc, activity.getCaseActivityIdentification(), fromDate, toDate);

            assertEquals(historyRet.size(), 6);

            historySetCourt.add(new Long(i));
        }

    }

    @Test(dependsOnMethods = { "testUpdateCaseActivity" }, enabled = true)
    public void testRetrieveCaseActivityHistory() {

        Set<Long> retAll = service.getAll(uc, LegalModule.CASE_ACTIVITY);
        assertEquals(retAll.size(), createdCourtActivities.size());
        for (int index = 0; index < createdCourtActivities.size(); index++) {
            Long id = createdCourtActivities.get(index).getCaseActivityIdentification();
            List<CourtActivityType> ret = service.retrieveCaseActivityHistory(uc, id, null, null);
            assertEquals(ret.size(), historySetCourt.get(index) + 1);
        }

        //test invalid id.
        //TODO: Long id = 9999999L;
        List<CourtActivityType> ret; // = service.retrieveCaseActivityHistory(uc, id, null, null);
        //assertEquals(ret.getReturnCode().longValue(), 1003l);

        //test start date and end date
        ret = service.retrieveCaseActivityHistory(uc, createdCourtActivities.get(0).getCaseActivityIdentification(), new Date(), new Date());
        assertEquals(ret.size(), 0);
    }

    @Test(dependsOnMethods = { "testRetrieveCaseActivityHistory" }, enabled = true)
    public void testPagingSearch() {

        //test ActivityCategory
        CaseActivitySearchType searchType = new CaseActivitySearchType();
        searchType.setActivityCategory("COURT");
        CourtActivitiesReturnType rtn = service.searchCourtActivity(uc, searchType, 0L, 10L, null);
        Assert.assertEquals(rtn.getTotalSize().intValue(), createdCourtActivities.size());
        Assert.assertEquals(rtn.getCourtActivities().size(), createdCourtActivities.size());

        rtn = service.searchCourtActivity(uc, searchType, 0L, 1L, null);
        Assert.assertEquals(rtn.getTotalSize().intValue(), createdCourtActivities.size());
        Assert.assertEquals(rtn.getCourtActivities().size(), 1);

        rtn = service.searchCourtActivity(uc, searchType, 1L, 1L, null);
        Assert.assertEquals(rtn.getTotalSize().intValue(), createdCourtActivities.size());
        Assert.assertEquals(rtn.getCourtActivities().size(), 1);

        rtn = service.searchCourtActivity(uc, searchType, 2L, 1L, null);
        Assert.assertEquals(rtn.getTotalSize().intValue(), createdCourtActivities.size());
        Assert.assertEquals(rtn.getCourtActivities().size(), 1);

        rtn = service.searchCourtActivity(uc, searchType, 3L, 1L, null);
        Assert.assertEquals(rtn.getTotalSize().intValue(), createdCourtActivities.size());
        Assert.assertEquals(rtn.getCourtActivities().size(), 0);

        searchType.setActivityCategory("POLICE");
        rtn = service.searchCourtActivity(uc, searchType, 0L, 10L, null);
        Assert.assertEquals(rtn.getTotalSize(), new Long(0L));
        Assert.assertEquals(rtn.getCourtActivities().size(), 0);

        //test ActivityOutcome
/*		searchType = new CaseActivitySearchType();
        searchType.setActivityOutcome("PROBATION");
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);	
		
		searchType.setActivityOutcome("DISMISS");
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 0);
		
		//test ActivityStatus
		searchType = new CaseActivitySearchType();
		searchType.setActivityStatus("INACTIVE");
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);	
		
		searchType.setActivityStatus("ACTIVE");
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 0);	
		
		//test ActivityType
		searchType = new CaseActivitySearchType();
		searchType.setActivityType("PRELHEAR");
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);	
		
		searchType.setActivityType("INITCRT");
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 0);
		
		//test CaseAssociation
		searchType = new CaseActivitySearchType();
		searchType.setCaseId(201L);
		
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 1);	
		
		searchType = new CaseActivitySearchType();
		searchType.setCaseId(202L);
		
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 2);	
		
		
		//test ChargeAssociation
		searchType = new CaseActivitySearchType();
		Set<Long> chargeAssocations = new HashSet<Long>();
		chargeAssocations.add(301L);
		searchType.setChargeIds(chargeAssocations);
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 0);
		
		//search history
		rtn = service.searchCaseActivity(uc, null, searchType, Boolean.TRUE);
		
		Assert.assertEquals(rtn.size(), 2);
		
		//set mode = ALL
		chargeAssocations = new HashSet<Long>();
		chargeAssocations.add(3011L);
		searchType.setChargeIds(chargeAssocations);
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		//set mode = ANY
		searchType.setChargeIdsSetMode(SearchSetMode.ANY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		//set mode = ONLY
		searchType.setChargeIdsSetMode(SearchSetMode.ONLY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 0);
		
		chargeAssocations.add(3021L);
		chargeAssocations.add(3031L);
		searchType.setChargeIdsSetMode(SearchSetMode.ONLY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		//set mode = ANY
		searchType.setChargeIdsSetMode(SearchSetMode.ANY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		//set mode = ALL
		searchType.setChargeIdsSetMode(SearchSetMode.ALL.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		chargeAssocations.add(3041L);
		searchType.setChargeIdsSetMode(SearchSetMode.ALL.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 0);
		
		//set mode  = ONLY
		searchType.setChargeIdsSetMode(SearchSetMode.ONLY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 0);
		
		//set mode = ANY
		searchType.setChargeIdsSetMode(SearchSetMode.ANY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		
		//test orderInititatedAssociations
		searchType = new CaseActivitySearchType();
		Set<Long> orderInititatedAssociations = new HashSet<Long>();
		orderInititatedAssociations.add(401L);
		searchType.setOrderInititatedIds(orderInititatedAssociations);
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 0);
		
		//set mode = ALL
		orderInititatedAssociations = new HashSet<Long>();
		orderInititatedAssociations.add(4011L);
		searchType.setOrderInititatedIds(orderInititatedAssociations);
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		//set mode = ANY
		searchType.setOrderInititatedIdsSetMode(SearchSetMode.ANY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		//set mode = ONLY
		searchType.setOrderInititatedIdsSetMode(SearchSetMode.ONLY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 0);
		
		orderInititatedAssociations.add(4021L);
		searchType.setOrderInititatedIdsSetMode(SearchSetMode.ONLY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		
		//test combine CharegeAssociations and orderInititatedAssociations
		//set mode = ALL
		chargeAssocations = new HashSet<Long>();
		chargeAssocations.add(3011L);
		searchType.setChargeIds(chargeAssocations);
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		//set mode = ANY
		searchType.setChargeIdsSetMode(SearchSetMode.ANY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		//set mode = ONLY
		searchType.setChargeIdsSetMode(SearchSetMode.ONLY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 0);*/

        //TODO: add more test cases to verify when have time.

    }

    @Test(dependsOnMethods = { "testPagingSearch" }, enabled = true)
    public void testSearchCaseActivity() {
        //test isWellFormed and isValid
        CaseActivitySearchType searchType = new CaseActivitySearchType();
        //Assert.assertEquals(searchType.isWellFormed(), Boolean.FALSE);
        //Assert.assertEquals(searchType.isValid(), Boolean.FALSE);

        searchType.setActivityCategory("COURT");
        //Assert.assertEquals(searchType.isWellFormed(), Boolean.TRUE);
        //Assert.assertEquals(searchType.isValid(), Boolean.TRUE);

        searchType.setChargeIdsSetMode("ABC");
        //Assert.assertEquals(searchType.isWellFormed(), Boolean.TRUE);
        //Assert.assertEquals(searchType.isValid(), Boolean.FALSE);

        //test ActivityCategory
        searchType = new CaseActivitySearchType();
        searchType.setActivityCategory("COURT");
        List<CourtActivityType> rtn = service.searchCaseActivity(uc, null, searchType, null);
        //
        //Assert.assertEquals(rtn.size(), createdCourtActivities.size());

        searchType.setActivityCategory("POLICE");
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 0);

        //test ActivityOutcome
        searchType = new CaseActivitySearchType();
        searchType.setActivityOutcome("PROBATION");
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 3);

        searchType.setActivityOutcome("DISMISS");
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 0);

        //test ActivityStatus
        searchType = new CaseActivitySearchType();
        searchType.setActivityStatus("INACTIVE");
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 3);

        searchType.setActivityStatus("ACTIVE");
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 0);

        //test ActivityType
        searchType = new CaseActivitySearchType();
        searchType.setActivityType("PRELHEAR");
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 3);

        searchType.setActivityType("INITCRT");
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 0);

        //test CaseAssociation
        searchType = new CaseActivitySearchType();
        searchType.setCaseId(caseId);

        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 3);

        //searchType = new CaseActivitySearchType();
        //searchType.setCaseId(202L);

        //rtn = service.searchCaseActivity(uc, null, searchType, null);
        //
        //Assert.assertEquals(rtn.size(), 2);

        //test ChargeAssociation
        searchType = new CaseActivitySearchType();
        Set<Long> chargeAssocations = new HashSet<Long>();
        chargeAssocations.add(301L);
        searchType.setChargeIds(chargeAssocations);
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 0);

        //search history
        rtn = service.searchCaseActivity(uc, null, searchType, Boolean.TRUE);

        Assert.assertEquals(rtn.size(), 2);

        //set mode = ALL
        chargeAssocations = new HashSet<Long>();
        chargeAssocations.add(3011L);
        searchType.setChargeIds(chargeAssocations);
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 3);

        //set mode = ANY
        searchType.setChargeIdsSetMode(SearchSetMode.ANY.value());
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 3);

        //set mode = ONLY
        searchType.setChargeIdsSetMode(SearchSetMode.ONLY.value());
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 0);

        chargeAssocations.add(3021L);
        chargeAssocations.add(3031L);
        searchType.setChargeIdsSetMode(SearchSetMode.ONLY.value());
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 3);

        //set mode = ANY
        searchType.setChargeIdsSetMode(SearchSetMode.ANY.value());
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 3);

        //set mode = ALL
        searchType.setChargeIdsSetMode(SearchSetMode.ALL.value());
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 3);

        chargeAssocations.add(3041L);
        searchType.setChargeIdsSetMode(SearchSetMode.ALL.value());
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 0);

        //set mode  = ONLY
        searchType.setChargeIdsSetMode(SearchSetMode.ONLY.value());
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 0);

        //set mode = ANY
        searchType.setChargeIdsSetMode(SearchSetMode.ANY.value());
        rtn = service.searchCaseActivity(uc, null, searchType, null);

        Assert.assertEquals(rtn.size(), 3);

		
/*TODO:		//test orderInititatedAssociations
		searchType = new CaseActivitySearchType();
		Set<Long> orderInititatedAssociations = new HashSet<Long>();
		orderInititatedAssociations.add(401L);
		searchType.setOrderInititatedIds(orderInititatedAssociations);
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 0);
		
		//set mode = ALL
		orderInititatedAssociations = new HashSet<Long>();
		orderInititatedAssociations.add(orderIds.get(2));
		searchType.setOrderInititatedIds(orderInititatedAssociations);
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		//set mode = ANY
		searchType.setOrderInititatedIdsSetMode(SearchSetMode.ANY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		//set mode = ONLY
		searchType.setOrderInititatedIdsSetMode(SearchSetMode.ONLY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 0);
		
		orderInititatedAssociations.add(orderIds.get(3));
		searchType.setOrderInititatedIdsSetMode(SearchSetMode.ONLY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		
		//test combine CharegeAssociations and orderInititatedAssociations
		//set mode = ALL
		chargeAssocations = new HashSet<Long>();
		chargeAssocations.add(3011L);
		searchType.setChargeIds(chargeAssocations);
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		//set mode = ANY
		searchType.setChargeIdsSetMode(SearchSetMode.ANY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 3);
		
		//set mode = ONLY
		searchType.setChargeIdsSetMode(SearchSetMode.ONLY.value());
		rtn = service.searchCaseActivity(uc, null, searchType, null);
		
		Assert.assertEquals(rtn.size(), 0);*/

        //TODO: add more test cases to verify when have time.

    }

    @Test(dependsOnMethods = { "testSearchCaseActivity" }, enabled = true)
    public void testRetrieveCaseActivities() {

        //test without ACTIVITY_STATUS

        List<CourtActivityType> rtn = service.retrieveCaseActivities(uc, caseId, null);
        Assert.assertEquals(rtn.size(), 3);

        //test with ACTIVITY_STATUS
        rtn = service.retrieveCaseActivities(uc, caseId, "INACTIVE");
        Assert.assertEquals(rtn.size(), 3);

        rtn = service.retrieveCaseActivities(uc, caseId, "ACTIVE");
        Assert.assertEquals(rtn.size(), 0);

        //Test invalid input

        //all null
        //TODO:rtn = service.retrieveCaseActivities(uc, null, null);
        //Assert.assertEquals(rtn.getReturnCode().longValue(), 2002L);

        //wrong reference code
        try {
            rtn = service.retrieveCaseActivities(uc, caseId, "ABC");
        } catch (Exception e) {
            Assert.assertTrue(e instanceof InvalidInputException);
        }

    }

    @Test(dependsOnMethods = { "testRetrieveCaseActivities" }, enabled = true)
    public void testCancel() {
        CourtActivityType caseActivity = createdCourtActivities.get(0);

        //cancel first
        CourtActivityType cancelRtn = service.cancelCourtActivity(uc, caseActivity.getCaseActivityIdentification());
        Assert.assertNotEquals(cancelRtn, null);
        assertEquals(cancelRtn.getActivityStatus(), "INACTIVE");

        //cancel second time, same result.
        cancelRtn = service.cancelCourtActivity(uc, caseActivity.getCaseActivityIdentification());
        Assert.assertNotEquals(cancelRtn, null);
        assertEquals(cancelRtn.getActivityStatus(), "INACTIVE");

        //null id
        //TODO:cancelRtn = service.cancelCourtActivity(uc, null);
        //assertEquals(cancelRtn.getReturnCode(), ReturnCode.CInvalidInput2002.returnCode());

        //no exisitng id
        //TODO: cancelRtn = service.cancelCourtActivity(uc, 99999999L);
        //assertEquals(cancelRtn.getReturnCode(), ReturnCode.RInvalidInput1003.returnCode());

    }

    @Test(dependsOnMethods = "testCancel")
    public void testUpdateFacilityInternalId() {
        //to test update FacilityInternalLocationId
        CourtActivityType act = createdCourtActivities.get(0);
        Long newId = 888L;
        act.setFacilityInternalLocationId(newId);
        CourtActivityType actRet = service.updateCaseActivity(uc, act);
        assertNotNull(actRet);
        Assert.assertEquals(actRet.getCaseActivityIdentification(), act.getCaseActivityIdentification());
        Assert.assertEquals(actRet.getFacilityInternalLocationId(), newId);

        actRet = service.getCaseActivity(uc, act.getCaseActivityIdentification());
        assertNotNull(actRet);
        Assert.assertEquals(actRet.getCaseActivityIdentification(), act.getCaseActivityIdentification());
        Assert.assertEquals(actRet.getFacilityInternalLocationId(), newId);
    }

    @Test(dependsOnMethods = { "testUpdateFacilityInternalId" }, enabled = true)
    public void testDelete() {

        //test delete null
        Long deleteRtn; //TODO: = service.delete(uc, null);
        //assertEquals(deleteRtn.longValue(), 2002l);
        //test delete invalid id.
        //TODO: deleteRtn = service.delete(uc, 9999999L);
        //assertEquals(deleteRtn.longValue(), 1003l);

        //test normal delete case.
        Set<Long> retAll = service.getAll(uc, LegalModule.CASE_ACTIVITY);
        assertEquals(retAll.size(), createdCourtActivities.size());
        for (Long id : retAll) {
            deleteRtn = service.deleteCaseActivity(uc, id);
            assertEquals(deleteRtn.longValue(), success.longValue());

            //can not get current one.
            try {
                service.getCaseActivity(uc, id);
            } catch (Exception e) {
                Assert.assertTrue(e instanceof DataNotExistException);
            }

            //can not get history too.
            try {
                List<CourtActivityType> rtn = service.retrieveCaseActivityHistory(uc, id, null, null);
                Assert.assertEquals(rtn.size(), 0);
            } catch (Exception e) {
                Assert.assertTrue(e instanceof DataNotExistException);
            }

        }
    }

    @AfterClass
    public void afterTest() {
        //service.reset(uc);
    }

    ///////////////////////////////////private methods//////////////////////////////////////////////////
	
	
/*	private Date getEffectiveDate() {
		Calendar cal = Calendar.getInstance();

		cal.set(Calendar.HOUR_OF_DAY, randLong(23).intValue());
		cal.set(Calendar.MINUTE, randLong(59).intValue());
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.YEAR, randLong(2012).intValue());

		return cal.getTime();
	}
	
	private Long randLong(int i) {
		return BigInteger.valueOf(new Random().nextInt(i)).abs().longValue();
	}
	
	private Long randLong(int from, int to) {
		Long rand = BigInteger.valueOf(new Random().nextInt(to)).abs().longValue();
		while (rand < from) {
			rand = BigInteger.valueOf(new Random().nextInt(to)).abs().longValue();
		}
		return rand;
	}
	
	private Date getDateWithoutTime(int year, int month, int day) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, month-1);
		c.set(Calendar.DAY_OF_MONTH, day);
		
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		
		return c.getTime();
	}
	
	private Date getDateWithoutTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		// Set time fields to zero
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		Date newDate = cal.getTime();
		return newDate;
	}
	
	private Date getDate(int year, int month, int day, int hour, int minute, int second) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, month-1);
		c.set(Calendar.DAY_OF_MONTH, day);
		
		c.set(Calendar.HOUR_OF_DAY, hour);
		c.set(Calendar.MINUTE, minute);
		c.set(Calendar.SECOND, second);
		c.set(Calendar.MILLISECOND, 0);
		
		return c.getTime();
	}*/

    /**
     * @param b
     * @return
     */
    private CaseInfoType generateDefaultCaseInfo(boolean onlyMandatory) {
        CaseInfoType caseInfo = new CaseInfoType();
        Date currentDate = new Date();

        //Mandatory fields:
        caseInfo.setCaseCategory(CASE_CATEGORY_IJ);
        caseInfo.setCaseType(CASE_TYPE_ADULT);
        caseInfo.setCreatedDate(currentDate);
        caseInfo.setDiverted(true);
        caseInfo.setDivertedSuccess(true);
        caseInfo.setActive(true);
        caseInfo.setSupervisionId(1L);

        if (onlyMandatory) {
            return caseInfo;
        }

        //Add optional fields:
        caseInfo.setIssuedDate(currentDate);
        caseInfo.setStatusChangeReason(CASE_STATUS_REASON_DISMISSED);
        caseInfo.setSentenceStatus(SENTENCE_STATUS_SENTENCED);
        caseInfo.setFacilityId(1L);

        CommentType comment = new CommentType();
        comment.setUserId("1");
        comment.setCommentDate(currentDate);
        comment.setComment("createCaseInfoPositive");
        caseInfo.setComment(comment);

        //caseInfo.getAssociations().add(new AssociationType(CASE_ACTIVITY, 1L));

        //Add case identifiers
        CaseIdentifierType caseIdentifier1 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_DOCKET, "0001A", false, 1L, 1L, "createCaseInfoPositive comment1", true);

        CaseIdentifierType caseIdentifier2 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_INDICTMENT, "0002", false, 1L, 1L, "createCaseInfoPositive comment2",
                false);

        caseInfo.getCaseIdentifiers().add(caseIdentifier1);
        caseInfo.getCaseIdentifiers().add(caseIdentifier2);

        return caseInfo;
    }

    private CourtActivityType generateDefaultCourtActivity(boolean onlyMandatoryFields) {

        //courtActivity1: madatory fields
        CourtActivityType courtActivity = new CourtActivityType();

        //test an empty object;
        //assertEquals(service.createCaseActivity(uc, courtActivity).getReturnCode().longValue(), 2002l);

        //test ActivityCategory has value.
        courtActivity.setActivityCategory("COURT");
        //assertEquals(service.createCaseActivity(uc, courtActivity).getReturnCode().longValue(), 2002l);

        //test ActivityType has value.
        courtActivity.setActivityType("INITCRT");
        //assertEquals(service.createCaseActivity(uc, courtActivity).getReturnCode().longValue(), 2002l);

        //test ActivityStatus has value.
        courtActivity.setActivityStatus("ACTIVE");
        //assertEquals(service.createCaseActivity(uc, courtActivity).getReturnCode().longValue(), 2002l);

        //test ActivityOccurredDate has value.
        courtActivity.setActivityOccurredDate(new Date());
        //assertEquals(service.createCaseActivity(uc, courtActivity).getReturnCode().longValue(), 2002l);

        //all mandatory fields have values, create should pass now.
        courtActivity.getCaseIds().add(caseId);

        if (onlyMandatoryFields) {
			return courtActivity;
		}

        //test ActivityCategory has value.
        courtActivity.setActivityCategory("COURT");

        //test ActivityType has value.
        courtActivity.setActivityType("INITCRT");

        //test ActivityStatus has value.
        courtActivity.setActivityStatus("ACTIVE");

        //test ActivityOccurredDate has value.
        courtActivity.setActivityOccurredDate(new Date());

        //test associations positive.
        //courtActivity.getActivityIds().add(101L);
        //courtActivity.getActivityIds().add(102L);

        //add optional elements:

        //test ActivityOutcome has value.
        courtActivity.setActivityOutcome("DISMISS");

        //test comment
        CommentType comment = new CommentType();
        comment.setComment("comment1");
        comment.setCommentDate(new Date());
        courtActivity.setActivityComment(comment);

        //test judge
        courtActivity.setJudge("judge1");

        //test facility association
        courtActivity.setFacilityId(999L);

        //test facility association
        courtActivity.setFacilityInternalLocationId(998L);

        //test facility association
        courtActivity.setOrganizationId(999L);

        //test static associations positive
        Set<Long> chargeAssociations = new HashSet<Long>();
        Set<Long> orderInitialAssociations = new HashSet<Long>();
        Set<Long> orderResultAssociations = new HashSet<Long>();

        Long asso11 = 301L;
        chargeAssociations.add(asso11);
        asso11 = 302L;
        chargeAssociations.add(asso11);
        asso11 = 303L;
        chargeAssociations.add(asso11);

        orderInitialAssociations.add(orderIds.get(0));
        orderInitialAssociations.add(orderIds.get(1));

        orderResultAssociations.add(orderIds.get(0));

        courtActivity.setChargeIds(chargeAssociations);
        courtActivity.setOrderInititatedIds(orderInitialAssociations);
        courtActivity.setOrderResultedIds(orderResultAssociations);

        return courtActivity;
    }

    private void verfiyCreateNegative(CourtActivityType courtActivity) {
        try {
            CourtActivityType rtn = service.createCaseActivity(uc, courtActivity);

            Assert.assertEquals(rtn, null);

        } catch (Exception e) {
            Assert.assertNotEquals(e, null);
            //Assert.assertTrue(e instanceof ArbutusRuntimeException);
        }
    }

    private void verfiyUpdateNegative(CourtActivityType courtActivity) {
        try {
            CourtActivityType rtn = service.updateCaseActivity(uc, courtActivity);
            Assert.assertEquals(rtn, null);
        } catch (Exception e) {
            Assert.assertNotEquals(e, null);
            // Assert.assertTrue(e instanceof ArbutusRuntimeException);
        }
    }

    private void verfiyGetNegative(Long id) {
        try {

            CourtActivityType rtn = service.getCaseActivity(uc, id);
            Assert.assertEquals(rtn, null);
        } catch (Exception e) {
            Assert.assertNotEquals(e, null);
            // Assert.assertTrue(e instanceof ArbutusRuntimeException);
        }
    }

}
