package syscon.arbutus.product.services.legal.contract.ejb;

import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.legal.contract.dto.SearchSentenceTermType;
import syscon.arbutus.product.services.legal.contract.dto.SentenceTermNameType;
import syscon.arbutus.product.services.legal.contract.dto.SentenceTermType;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;

import static org.testng.Assert.assertNotNull;

/**
 * ConfiureSentenceTermIT to test APIs of Configure Sentence Term.
 *
 * @author Ashish
 * @version 1.0
 * @December 10, 2013
 */
public class ConfiureSentenceTermIT extends BaseIT {
    private static Logger log = LoggerFactory.getLogger(ConfiureSentenceTermIT.class);
    protected UserContext uc = null;
    private LegalService service;

    // shared data
    private SentenceTermType sentenceTermTypeObj1;
    private SentenceTermType sentenceTermTypeObj2;

    @BeforeClass
    public void beforeTest() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");
        uc = super.initUserContext();
        service = (LegalService) JNDILookUp(this.getClass(), LegalService.class);

    }

    private void clientlogin(String user, String password) {
        try {
            SecurityClient client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password);
            client.login();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testCreateSentenceTerm() {
        clientlogin("devtest", "devtest");
        SentenceTermType sentenceTermType = new SentenceTermType();
        SentenceTermNameType sentenceTermNameType = new SentenceTermNameType();
        List<SentenceTermNameType> sentenceTermNameTypeList = new ArrayList<SentenceTermNameType>();
        sentenceTermType.setSentenceType("DEF");
        sentenceTermType.setTermType("SINGLE");

        sentenceTermNameType.setTermName("MAX");
        sentenceTermNameTypeList.add(sentenceTermNameType);

        sentenceTermType.setSentenceTermNameList(sentenceTermNameTypeList);

        sentenceTermTypeObj1 = service.createSentenceTerm(uc, sentenceTermType);

        assert (sentenceTermTypeObj1 != null);

        sentenceTermNameTypeList = new ArrayList<SentenceTermNameType>();

        sentenceTermType = new SentenceTermType();
        sentenceTermType.setSentenceType("INDETER");
        sentenceTermType.setTermType("DUAL");

        sentenceTermNameType = new SentenceTermNameType();
        sentenceTermNameType.setTermName("MIN");
        sentenceTermNameTypeList.add(sentenceTermNameType);

        sentenceTermNameType = new SentenceTermNameType();
        sentenceTermNameType.setTermName("MAX");
        sentenceTermNameTypeList.add(sentenceTermNameType);

        sentenceTermType.setSentenceTermNameList(sentenceTermNameTypeList);

        sentenceTermTypeObj2 = service.createSentenceTerm(uc, sentenceTermType);

        assert (sentenceTermTypeObj2 != null);

    }

    @Test(dependsOnMethods = { "testCreateSentenceTerm" })
    public void testUpdateSentenceTerm() {
        SentenceTermType sentenceTermType = service.getSentenceTerm(uc, sentenceTermTypeObj1.getSentenceTermId());

        assert (sentenceTermType != null);

        sentenceTermType.setDeactivationDate(new Date());

        sentenceTermType = service.updateSentenceTerm(uc, sentenceTermType);

        assert (sentenceTermType != null);
        assert (sentenceTermType.getDeactivationDate() != null);

    }

    @Test(dependsOnMethods = { "testCreateSentenceTerm" })
    public void testGetSentenceTerm() {
        SentenceTermType sentenceTermType = service.getSentenceTerm(uc, sentenceTermTypeObj1.getSentenceTermId());
        assert (sentenceTermType != null);

    }

    @Test(dependsOnMethods = { "testCreateSentenceTerm" })
    public void testGetAllSentenceTerm() {
        clientlogin("devtest", "devtest");
        List<SentenceTermType> sentenceTermTypeList = service.getAllSentenceTerm(uc, null, null, null);
        assert (sentenceTermTypeList != null && sentenceTermTypeList.size() > 1);

    }

    @Test(dependsOnMethods = { "testCreateSentenceTerm" })
    public void testSearchSentenceTerm() {
        clientlogin("devtest", "devtest");
        SearchSentenceTermType searchType = new SearchSentenceTermType();
        searchType.setSentenceType("INDETER");

        List<SentenceTermType> sentenceTermTypeList = service.searchSentenceTerm(uc, searchType, null, null, null);
        assert (sentenceTermTypeList != null && sentenceTermTypeList.size() > 0);

    }

}
