package syscon.arbutus.product.services.legal.contract.ejb;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.exception.DataExistException;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.*;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class CaseInfoIT extends BaseIT {

    private static final String CASE_CATEGORY_IJ = "IJ";
    private static final String CASE_CATEGORY_OJ = "OJ";

    private static final String CASE_TYPE_ADULT = "ADULT";
    private static final String CASE_TYPE_JUVENILE = "JUVENILE";
    private static final String CASE_TYPE_TRAFFIC = "TRAFFIC";

    private static final String SENTENCE_STATUS_SENTENCED = "SENTENCED";
    private static final String SENTENCE_STATUS_UNSENTENCED = "UNSENTENCED";

    private static final String CASE_STATUS_REASON_DISMISSED = "CD";
    private static final String CASE_STATUS_REASON_RE_OPENED = "RO";

    private static final String CASE_IDENTIFIER_TYPE_DOCKET = "DOCKET";
    private static final String CASE_IDENTIFIER_TYPE_INDICTMENT = "INDICTMENT";

    private static final String CATEGORY_PERSON = "PI";
    private static final String TYPE_LEGAL = "LEG";
    private static final String ROLE_WITNESS = "WITNESS";

    private static final Long success = ReturnCode.Success.returnCode();

    //private CaseManagementServiceBean service;   //for local test
    private UserContext uc = null; //new UserContext();
    private Logger log = LoggerFactory.getLogger(CaseInfoIT.class);

    private List<CaseInfoType> createdCaseInfos = new ArrayList<CaseInfoType>();
    private List<CaseIdentifierConfigType> createdConfigs = new ArrayList<CaseIdentifierConfigType>();
    private List<Long> historySetCourt = new ArrayList<Long>();

    private LegalService service;

    /**
     * <p>Cut the time portion of a given Date object.</p>
     *
     * @param dateTime A Date object to trim.
     * @return A Date object.
     */
    private static Date getDatePartial(Date dateTime) {

        if (dateTime == null) {
			return null;
		}

        Calendar cal = Calendar.getInstance();
        cal.setTime(dateTime);

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();

        //localLogin();
    }

	/*private Session getSession() {history = historyRet.getCaseInfos().iterator().next();
            assertEquals(history.getSentenceStatus(), SENTENCE_STATUS_SENTENCED);
		
		BasicConfigurator.configure();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("arbutus-casemanagement-test-unit");
	    EntityManager em = emf.createEntityManager();
	    return (Session)em.getDelegate();
	    
	}*/

    //for local debug use only
	/*private void localLogin() {
		
		service = new CaseManagementServiceBean();
		//init user context and login
		uc = super.getUserContext();
		super.login();
		
		service.setSession(getSession());
		
	}*/

    @BeforeMethod
    public void beforeMethod() {
    }

    //for release
    private void lookupJNDILogin() {

        log.info("Before JNDI lookup");
        service = (LegalService) JNDILookUp(this.getClass(), LegalService.class);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();
        clientlogin("devtest", "devtest");
    }

    @Test
    public void testLookupJNDI() {
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "testLookupJNDI" })
    public void reset() {

        Long ret = service.deleteAll(uc);
        assert (ret.equals(ReturnCode.Success.returnCode()));
    }

    @Test(dependsOnMethods = { "reset" })
    public void createUpdateSearchCaseIdentifierConfigPositive() {

        //Create
        CaseIdentifierConfigType dto = new CaseIdentifierConfigType(null, CASE_IDENTIFIER_TYPE_DOCKET, "000000A", true, true, true, true);

        CaseIdentifierConfigType ret = service.createCaseIdentifierConfig(uc, dto);
        assert (ret != null);

        createdConfigs.add(ret);

        log.info(ret.toString());

        dto = new CaseIdentifierConfigType(null, CASE_IDENTIFIER_TYPE_DOCKET, "000000A", false, false, false, false);

        ret = service.createCaseIdentifierConfig(uc, dto);
        assert (ret != null);

        createdConfigs.add(ret);

        //Search
        CaseIdentifierConfigSearchType caseIdentifierConfigSearch = new CaseIdentifierConfigSearchType();
        caseIdentifierConfigSearch.setIdentifierType(CASE_IDENTIFIER_TYPE_DOCKET);
        List<CaseIdentifierConfigType> searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);
        Assert.assertNotNull(searchRtn);
        Assert.assertEquals(searchRtn.size(), 2);

        //Update
        dto = createdConfigs.get(0);
        dto.setIdentifierType(CASE_IDENTIFIER_TYPE_INDICTMENT);

        ret = service.updateCaseIdentifierConfig(uc, dto);
        assert (ret != null);
        Assert.assertEquals(ret.getIdentifierType(), CASE_IDENTIFIER_TYPE_INDICTMENT);

        //Search
        caseIdentifierConfigSearch = new CaseIdentifierConfigSearchType();
        caseIdentifierConfigSearch.setIdentifierType(CASE_IDENTIFIER_TYPE_INDICTMENT);
        searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);
        Assert.assertNotNull(searchRtn);
        Assert.assertEquals(searchRtn.size(), 1);

        //Update
        dto.setPrimary(false);

        ret = service.updateCaseIdentifierConfig(uc, dto);

        assert (ret != null);
        Assert.assertEquals(ret.isPrimary(), Boolean.FALSE);

        //Search
        caseIdentifierConfigSearch.setPrimary(false);
        searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);

        Assert.assertNotNull(searchRtn);
        Assert.assertEquals(searchRtn.size(), 1);

        //Update
        dto.setActive(false);

        ret = service.updateCaseIdentifierConfig(uc, dto);

        assert (ret != null);
        Assert.assertEquals(ret.isActive(), Boolean.FALSE);

        //Search
        caseIdentifierConfigSearch.setActive(false);
        searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);

        Assert.assertNotNull(searchRtn);
        Assert.assertEquals(searchRtn.size(), 1);

        caseIdentifierConfigSearch = new CaseIdentifierConfigSearchType();
        caseIdentifierConfigSearch.setPrimary(false);
        searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);

        Assert.assertNotNull(searchRtn);
        Assert.assertEquals(searchRtn.size(), 2);

        //Update
        dto.setAutoGeneration(false);

        ret = service.updateCaseIdentifierConfig(uc, dto);

        assert (ret != null);
        //Assert.assertEquals(ret.isAutoGeneration(), Boolean.FALSE);

/*		//Search
		caseIdentifierConfigSearch.setAutoGeneration(false);
		searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);

		Assert.assertNotNull(searchRtn);
		Assert.assertEquals(searchRtn.size(), 0);*/

        //Update
        dto.setDuplicateCheck(false);

        ret = service.updateCaseIdentifierConfig(uc, dto);

        assert (ret != null);
        //Assert.assertEquals(ret.isDuplicateCheck(), Boolean.FALSE);


/*		//Search
		caseIdentifierConfigSearch.setIdentifierFormat("000000A");
		searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);

		Assert.assertNotNull(searchRtn);
		Assert.assertEquals(searchRtn.size(), 0);*/

        //Search
        caseIdentifierConfigSearch.setIdentifierType(CASE_IDENTIFIER_TYPE_DOCKET);
        searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);

        Assert.assertNotNull(searchRtn);
        Assert.assertEquals(searchRtn.size(), 1);

        //Search
        caseIdentifierConfigSearch.setActive(true);
        searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);

        Assert.assertNotNull(searchRtn);
        Assert.assertEquals(searchRtn.size(), 0);

    }

    @Test(dependsOnMethods = { "createUpdateSearchCaseIdentifierConfigPositive" }, enabled = true)
    public void createUpdateCaseIdentifierConfigNegative() {

        CaseIdentifierConfigType ret; // = service.createCaseIdentifierConfig(null, null);

        //		assert(ret == null);
        //
        //		//Empty DTO
        CaseIdentifierConfigType dto; //= new CaseIdentifierConfigType();
        //		ret = service.createCaseIdentifierConfig(uc, dto);
        //
        //		assert(ret == null);
        //
        //		//Wrong DTO
        //		dto = new CaseIdentifierConfigType();
        //		dto.setAutoGeneration(true);
        //		ret = service.createCaseIdentifierConfig(uc, dto);
        //
        //		assert(ret == null);

        //For an identifier type, can only create one active config type.
        try {
            dto = new CaseIdentifierConfigType(null, CASE_IDENTIFIER_TYPE_DOCKET, "000000A", true, true, null, true);
            ret = service.createCaseIdentifierConfig(uc, dto);
        } catch (Exception e) {
            log.info(e.getMessage());
        }

        //		assert(ret != null);

        //		dto = new CaseIdentifierConfigType(null,
        //				CASE_IDENTIFIER_TYPE_DOCKET,
        //				"000000A",
        //				false, true, false, false);
        //		ret = service.createCaseIdentifierConfig(uc, dto);
        //
        //		Assert.assertEquals(ret, null);
        //
        //		// Only 2 primary
        //		dto = new CaseIdentifierConfigType(null,
        //				CASE_IDENTIFIER_TYPE_DOCKET,
        //				"000000A",
        //				false, false, true, false);
        //		ret = service.createCaseIdentifierConfig(uc, dto);
        //
        //		Assert.assertNotNull(ret);
        //
        //		dto = new CaseIdentifierConfigType(null,
        //				CASE_IDENTIFIER_TYPE_DOCKET,
        //				"000000A",
        //				false, false, true, false);
        //		ret = service.createCaseIdentifierConfig(uc, dto);
        //
        //		Assert.assertEquals(ret, null);

    }

    @Test(dependsOnMethods = { "createUpdateSearchCaseIdentifierConfigPositive" })
    public void createCaseInfoPositive() {

        // Create only mandatory fields
        CaseInfoType caseInfo = generateDefaultCaseInfo(true);

        CaseInfoType ret = service.createCaseInfo(uc, caseInfo, null);

        assert (ret != null);

        verfiyCreatedCaseInfo(ret, caseInfo);

        ret = service.getCaseInfo(uc, ret.getCaseInfoId());

        verfiyCreatedCaseInfo(ret, caseInfo);

        createdCaseInfos.add(ret);

        log.info(ret.toString());

        // Create full fields
        caseInfo = generateDefaultCaseInfo(false);
        ret = service.createCaseInfo(uc, caseInfo, null);

        assert (ret != null);

        verfiyCreatedCaseInfo(ret, caseInfo);

        ret = service.getCaseInfo(uc, ret.getCaseInfoId());

        verfiyCreatedCaseInfo(ret, caseInfo);

        createdCaseInfos.add(ret);

        log.info(ret.toString());

        //Create full fields with duplicated check true
        //test 1, duplicated check with DB
        //caseInfo = generateDefaultCaseInfo(false);
        //ret = service.createCaseInfo(uc, caseInfo, true);

        caseInfo.setCaseIdentifiers(new ArrayList<CaseIdentifierType>());
        //Add case identifiers
        CaseIdentifierType caseIdentifier1 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_DOCKET, "0001A", false, null, null, null, true);
        caseInfo.getCaseIdentifiers().add(caseIdentifier1);
        //ret = service.createCaseInfo(uc, caseInfo, true);

        //test 2, just duplicated check in caseInfo.getCaseIdentifiers.
        caseInfo = generateDefaultCaseInfo(false);
        //Add case identifiers
        caseIdentifier1 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_DOCKET, "0001A", false, 2L, 2L, "createCaseInfoPositive comment2", true);

        CaseIdentifierType caseIdentifier2 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_DOCKET, "0001A", false, 1L, 1L, "createCaseInfoPositive comment1", false);
        caseInfo.setCaseIdentifiers(new ArrayList<CaseIdentifierType>());
        caseInfo.getCaseIdentifiers().add(caseIdentifier1);
        caseInfo.getCaseIdentifiers().add(caseIdentifier2);

        //ret = service.createCaseInfo(uc, caseInfo, true);

        //Test auto-generated case identifier value(only passed in auto flag is true).
        caseInfo = generateDefaultCaseInfo(true);
        caseInfo.setCaseIdentifiers(new ArrayList<CaseIdentifierType>());
        //Add case identifiers
        caseIdentifier1 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_INDICTMENT, "bbb", true, null, null, null, true);
        caseInfo.getCaseIdentifiers().add(caseIdentifier1);
        caseInfo.setFacilityId(99998L);
        //ret = service.createCaseInfo(uc, caseInfo, true);

        //Assert.assertNotNull(ret);

        //verfiyCreatedCaseInfo(ret, caseInfo);

        //Test auto-generated case identifier value.(both auto flag are true).
        caseInfo = generateDefaultCaseInfo(true);
        caseInfo.setCaseIdentifiers(new ArrayList<CaseIdentifierType>());
        //Add case identifiers
        caseIdentifier1 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_DOCKET, "bbb", true, null, null, null, true);
        caseInfo.getCaseIdentifiers().add(caseIdentifier1);
        //ret = service.createCaseInfo(uc, caseInfo, true);

        //Assert.assertNotNull(ret);

        //verfiyCreatedCaseInfo(ret, caseInfo);

    }

    @Test(dependsOnMethods = { "createCaseInfoPositive" }, enabled = false)
    public void createCaseInfoNegative() {

        //NULL CaseInfoType
        CaseInfoType ret = service.createCaseInfo(null, null, null);

        assert (ret == null);

        //Empty CaseInfoType
        CaseInfoType caseInfo = new CaseInfoType();
        ret = service.createCaseInfo(uc, caseInfo, null);

        assert (ret == null);

        //Missing mandatory fields in CaseInfoType
        caseInfo = new CaseInfoType();

        //Has case category
        caseInfo.setCaseCategory(CASE_CATEGORY_IJ);
        ret = service.createCaseInfo(uc, caseInfo, null);

        assert (ret == null);

        //Has case type
        caseInfo.setCaseType(CASE_TYPE_ADULT);
        ret = service.createCaseInfo(uc, caseInfo, null);

        assert (ret == null);

        //Has case created date
        Date currentDate = Calendar.getInstance().getTime();
        caseInfo.setCreatedDate(currentDate);
        ret = service.createCaseInfo(uc, caseInfo, null);

        assert (ret == null);

        //Has case diverted flag
        caseInfo.setDiverted(true);
        ret = service.createCaseInfo(uc, caseInfo, null);

        assert (ret == null);

        //Has case diverted success flag.
        caseInfo.setDivertedSuccess(true);
        ret = service.createCaseInfo(uc, caseInfo, null);

        assert (ret == null);

        //Has active flag.
        caseInfo.setActive(true);
        ret = service.createCaseInfo(uc, caseInfo, null);

        assert (ret == null);

    }

    @Test(dependsOnMethods = { "createCaseInfoPositive" })
    public void getCaseInfoPositive() {
        for (CaseInfoType caseInfo : createdCaseInfos) {
            Long id = caseInfo.getCaseInfoId();
            CaseInfoType ret = service.getCaseInfo(uc, id);

            assert (ret != null);

            log.info(ret.toString());
        }
    }

    @Test(dependsOnMethods = { "getCaseInfoPositive" }, enabled = false)
    public void getCaseInfoNegative() {
        //null id
        Long id = null;
        CaseInfoType ret = service.getCaseInfo(uc, id);

        assert (ret == null);

        //invalid id.
        id = 99999L;
        ret = service.getCaseInfo(uc, id);

        assert (ret == null);

    }

    @Test(dependsOnMethods = { "getCaseInfoPositive" })
    public void testUpdateCaseInfo() {

        //Negative return test.
        //assertEquals(service.updateCaseInfo(uc, null, null).getReturnCode(), ReturnCode.CInvalidInput2002.returnCode());
        //assertEquals(service.updateCaseInfo(uc, new CaseInfoType(), null).getReturnCode(), ReturnCode.CInvalidInput2002.returnCode());

        //no id.
        CaseInfoType caseInfo = generateDefaultCaseInfo(true);
        //assertEquals(service.updateCaseInfo(uc, caseInfo, null).getReturnCode(), ReturnCode.CInvalidInput2002.returnCode());

        Date fromDate = new Date();
        Date toDate = new Date();

        //positive test.
        for (CaseInfoType createdCaseInfo : createdCaseInfos) {
            int i = 0;
            List<CaseInfoType> historyRet;

            CaseInfoType caseInfoRet = service.getCaseInfo(uc, createdCaseInfo.getCaseInfoId());
            caseInfo = caseInfoRet;

            //test when category has new value.
            caseInfo.setCaseCategory(CASE_CATEGORY_OJ);

            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;

            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            //assertEquals(historyRet.size(), 0);

            //test when Type has updated value.
            caseInfo.setCaseType(CASE_TYPE_TRAFFIC);
            fromDate = new Date();

            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            assertEquals(caseInfoRet.getCaseType(), CASE_TYPE_TRAFFIC);
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 1);

            //test when status has updated value.
            caseInfo.setSentenceStatus(SENTENCE_STATUS_UNSENTENCED);
            fromDate = new Date();
            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            assertEquals(caseInfoRet.getSentenceStatus(), SENTENCE_STATUS_UNSENTENCED);
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 1);

            //test IssuedDate has update value.
            Date newDate = new Date();
            caseInfo.setIssuedDate(newDate);
            fromDate = new Date();
            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            assertEquals(caseInfoRet.getIssuedDate(), getDatePartial(newDate));
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 1);

            //test CaseStatusReason has updated value.
            caseInfo.setStatusChangeReason(CASE_STATUS_REASON_RE_OPENED);
            fromDate = new Date();
            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            assertEquals(caseInfoRet.getStatusChangeReason(), CASE_STATUS_REASON_RE_OPENED);
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 1);

            //test comment
            CommentType comment = new CommentType();
            comment.setComment("comment2");
            comment.setCommentDate(new Date());
            caseInfo.setComment(comment);
            fromDate = new Date();

            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            assertEquals(caseInfoRet.getComment().getComment(), comment.getComment());
            assertEquals(caseInfoRet.getComment().getCommentDate(), comment.getCommentDate());
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 1);

            //test active
            caseInfo.setActive(false);
            fromDate = new Date();

            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            assertEquals(caseInfoRet.isActive(), Boolean.FALSE);
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 1);

            //test Diverted
            caseInfo.setDiverted(false);
            fromDate = new Date();

            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            assertEquals(caseInfoRet.isDiverted(), Boolean.FALSE);
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 1);

            //test DivertedSuccess
            caseInfo.setDivertedSuccess(false);
            fromDate = new Date();

            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            assertEquals(caseInfoRet.isDivertedSuccess(), Boolean.FALSE);
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 1);

            //test facility association
            caseInfo.setFacilityId(9991L);
            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            assertEquals(caseInfoRet.getFacilityId(), caseInfo.getFacilityId());
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 2);

            //test Supervision association
            caseInfo.setSupervisionId(9991L);
            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            assertEquals(caseInfoRet.getSupervisionId(), caseInfo.getSupervisionId());
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 3);

            //test static associations positive
            Set<Long> chargeIds = new HashSet<Long>();
            Set<Long> orderIds = new HashSet<Long>();
            Set<Long> conditionIds = new HashSet<Long>();

            Long id1 = new Long(3011L);
            chargeIds.add(id1);
            id1 = new Long(3021L);
            chargeIds.add(id1);
            id1 = new Long(3031L);
            chargeIds.add(id1);

            id1 = new Long(4011L);
            orderIds.add(id1);
            id1 = new Long(4021L);
            orderIds.add(id1);

            id1 = new Long(5011L);
            conditionIds.add(id1);

            caseInfo.setChargeIds(chargeIds);
            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            assertEquals(caseInfoRet.getChargeIds(), caseInfo.getChargeIds());
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 4);

            caseInfo.setOrderSentenceIds(orderIds);
            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            assertEquals(caseInfoRet.getOrderSentenceIds(), caseInfo.getOrderSentenceIds());
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 5);

            caseInfo.setConditionIds(conditionIds);
            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            assertEquals(caseInfoRet.getConditionIds(), caseInfo.getConditionIds());
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 6);

            // test privileges negative
            //			privileges.add("F");
            //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
            //
            //			assertEquals(caseInfoRet, null);

/*			login("QAUser3", "QAUser3");
			privileges.clear();
			privileges.add("A");

			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

			assertEquals(caseInfoRet, null);

			login("devtest", "devtest");*/

            //test update primary case identifier to new value with save as secondary identifier.
            String oldNumber = null;
            String newNumber = "456";
            for (CaseIdentifierType identifier : caseInfo.getCaseIdentifiers()) {
                if (identifier.isPrimary()) {
                    oldNumber = identifier.getIdentifierValue();
                    identifier.setIdentifierValue(newNumber);
                }
            }
            caseInfo.setSaveAsSecondaryIdentifier(true);

            fromDate = new Date();
            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 1);

            historySetCourt.add(new Long(i));

            boolean oldInSecondList = false;
            for (CaseIdentifierType identifier : caseInfoRet.getCaseIdentifiers()) {
                if (identifier.isPrimary()) {
                    Assert.assertEquals(identifier.getIdentifierValue(), newNumber);
                } else {
                    if (identifier.getIdentifierValue().equals(oldNumber)) {
						oldInSecondList = true;
					}
                }
            }
            if (caseInfoRet.getCaseIdentifiers() != null && caseInfoRet.getCaseIdentifiers().size() > 0) {
				Assert.assertEquals(oldInSecondList, true);
			}

            //update primary identifier without save as secondary identifier.
            caseInfo = caseInfoRet;
            newNumber = oldNumber;
            for (CaseIdentifierType identifier : caseInfo.getCaseIdentifiers()) {
                if (identifier.isPrimary()) {
                    oldNumber = identifier.getIdentifierValue();
                    identifier.setIdentifierValue(newNumber);
                }
            }
            caseInfo.setSaveAsSecondaryIdentifier(false);

            fromDate = new Date();
            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 1);

            historySetCourt.add(new Long(i));

            oldInSecondList = false;
            boolean newInSecondList = false;
            for (CaseIdentifierType identifier : caseInfoRet.getCaseIdentifiers()) {
                if (identifier.isPrimary()) {
                    Assert.assertEquals(identifier.getIdentifierValue(), newNumber);
                } else {
                    if (identifier.getIdentifierValue().equals(oldNumber)) {
						oldInSecondList = true;
					}
                    if (identifier.getIdentifierValue().equals(newNumber)) {
						newInSecondList = true;
					}
                }
            }
            if (caseInfoRet.getCaseIdentifiers() != null && caseInfoRet.getCaseIdentifiers().size() > 0) {
                Assert.assertEquals(oldInSecondList, false);
                Assert.assertEquals(newInSecondList, false);
            }

            //test add new identifiers
            CaseIdentifierType newIdentifier = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_INDICTMENT, "0003", false, 1L, 1L, "createCaseInfoPositive comment3",
                    false);

            caseInfo = caseInfoRet;
            caseInfo.getCaseIdentifiers().add(newIdentifier);

            fromDate = new Date();
            caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);

            i++;
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);

            assertEquals(historyRet.size(), i + 1);

            toDate = new Date();
            historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);

            assertEquals(historyRet.size(), 1);

            historySetCourt.add(new Long(i));

            //caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
            newInSecondList = false;
            for (CaseIdentifierType identifier : caseInfoRet.getCaseIdentifiers()) {
                if (!identifier.isPrimary()) {
                    if (identifier.getIdentifierValue().equals("0003")) {
						newInSecondList = true;
					}
                }
            }
            if (caseInfoRet.getCaseIdentifiers() != null && caseInfoRet.getCaseIdentifiers().size() > 0) {
                Assert.assertEquals(newInSecondList, true);
            }
        }
    }

    @Test(dependsOnMethods = { "testUpdateCaseInfo" }, enabled = true)
    public void testRetrieveCaseInfoHistory() {

        //Set<Long> retAll = service.getAll(uc);
        //assertEquals(retAll.size(), createdCaseInfos.size());
        for (int index = 0; index < createdCaseInfos.size(); index++) {
            Long id = createdCaseInfos.get(index).getCaseInfoId();
            List<CaseInfoType> ret = service.retrieveCaseInfoHistory(uc, id, null, null);
            //assertEquals(ret.size(), historySetCourt.get(index) + 1);
        }

        List<CaseInfoType> ret;

        //test invalid id.
        //Long id = 9999999L;
        //ret = service.retrieveCaseInfoHistory(uc, id, null, null);

        //test start date and end date
        ret = service.retrieveCaseInfoHistory(uc, createdCaseInfos.get(0).getCaseInfoId(), new Date(), new Date());
        assertEquals(ret.size(), 0);

/*		//test privileges negative.
		login("QAUser3", "QAUser3");

		//ret = service.retrieveCaseInfoHistory(uc, createdCaseInfos.get(1).getCaseInfoId(), new Date(), new Date());

		login("devtest", "devtest");*/
    }

    @Test(dependsOnMethods = { "testRetrieveCaseInfoHistory" }, enabled = true)
    public void testSearchCaseInfo() {

        //TODO: ADD test back later.
        //test CaseCategory
        //		searchType = new CaseInfoSearchType();
        //		searchType.setCaseCategory(CASE_CATEGORY_IJ);
        //		List<CaseInfoType> rtn = service.searchCaseInfo(uc, null, searchType, null);
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		searchType.setCaseType(CASE_TYPE_JUVENILE);
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 0);
        //
        //		searchType.setCaseType(CASE_TYPE_ADULT);
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		searchType.setCaseType(CASE_TYPE_TRAFFIC);
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 0);
        //
        //		//test CASE_STATUS_REASON
        //		searchType = new CaseInfoSearchType();
        //		searchType.setReason(CASE_STATUS_REASON_RE_OPENED);
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		searchType.setReason(CASE_STATUS_REASON_DISMISSED);
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 0);
        //
        //		//test SENTENCE_STATUS
        //		searchType = new CaseInfoSearchType();
        //		searchType.setSentenceStatus(SENTENCE_STATUS_UNSENTENCED);
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		searchType.setSentenceStatus(SENTENCE_STATUS_SENTENCED);
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 0);
        //
        //		//test SUPERVISION Association
        //		searchType = new CaseInfoSearchType();
        //		AssociationType association = new AssociationType();
        //		association.setToClass(AssociationToClass.SUPERVISION.value());
        //		association.setToIdentifier(9991L);
        //		searchType.setSupervision(association);
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		searchType = new CaseInfoSearchType();
        //		association = new AssociationType();
        //		association.setToClass(AssociationToClass.SUPERVISION.value());
        //		association.setToIdentifier(1L);
        //		searchType.setSupervision(association);
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		//from/to start date
        //		searchType = new CaseInfoSearchType();
        //		Calendar fromDate = Calendar.getInstance();
        //		fromDate.set(2011,01,01);
        //		searchType.setCreatedDateFrom(fromDate.getTime());
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 4);
        //
        //		fromDate.set(2021,01,01);
        //		searchType.setCreatedDateFrom(fromDate.getTime());
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 0);
        //
        //		Calendar toDate = Calendar.getInstance();
        //		toDate.set(2020,01,01);
        //		searchType.setCreatedDateTo(toDate.getTime());
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 0);
        //
        //		//from/to issued date
        //		searchType = new CaseInfoSearchType();
        //		fromDate = Calendar.getInstance();
        //		fromDate.set(2011,01,01);
        //		searchType.setIssuedDateFrom(fromDate.getTime());
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		toDate = Calendar.getInstance();
        //		toDate.set(2021,01,01);
        //		searchType.setIssuedDateTo(toDate.getTime());
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		//isDevertedflag
        //		searchType = new CaseInfoSearchType();
        //		searchType.setDiverted(true);
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		searchType.setDiverted(false);
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		//isDevertedSuccessfullyFlag
        //		searchType = new CaseInfoSearchType();
        //		searchType.setDivertedSuccess(true);
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		searchType.setDivertedSuccess(false);
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		//isActive
        //		searchType = new CaseInfoSearchType();
        //		searchType.setActive(true);
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		searchType.setActive(false);
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		//Case Identifier Search Type
        //		searchType = new CaseInfoSearchType();
        //		CaseIdentifierSearchType identifierSearchType = new CaseIdentifierSearchType();
        //		identifierSearchType.setIdentifierValue("****");
        //		searchType.setCaseIdentifierSearch(identifierSearchType);
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn, null);
        //
        //		searchType = new CaseInfoSearchType();
        //		identifierSearchType = new CaseIdentifierSearchType();
        //		identifierSearchType.setIdentifierValue("0002");
        //		searchType.setCaseIdentifierSearch(identifierSearchType);
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 1);
        //
        //		identifierSearchType.setIdentifierValue("000");
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 0);
        //
        //		identifierSearchType.setIdentifierValue("000*");
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		identifierSearchType.setIdentifierValue("000*");
        //		identifierSearchType.setIdentifierType(CASE_IDENTIFIER_TYPE_DOCKET);
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		identifierSearchType.setIdentifierValue("000*");
        //		identifierSearchType.setIdentifierType(CASE_IDENTIFIER_TYPE_INDICTMENT);
        //		rtn = service.searchCaseInfo(uc, null, searchType, null);
        //
        //		Assert.assertEquals(rtn.size(), 1);
        //
        //
        //		//search history
        //		rtn = service.searchCaseInfo(uc, null, searchType, Boolean.TRUE);
        //
        //		Assert.assertEquals(rtn.size(), 1);
        //
        //		searchType = new CaseInfoSearchType();
        //		searchType.setActive(true);
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, Boolean.FALSE);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, Boolean.TRUE);
        //
        //		Assert.assertEquals(rtn.size(), 4);
        //
        //		searchType = new CaseInfoSearchType();
        //		searchType.setCaseCategory(CASE_CATEGORY_IJ);
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, Boolean.FALSE);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, Boolean.TRUE);
        //
        //		Assert.assertEquals(rtn.size(), 4);
        //
        //		//Test privileges
        //		searchType = new CaseInfoSearchType();
        //		searchType.setCaseCategory(CASE_CATEGORY_OJ);
        //
        ///*		login("QAUser3", "QAUser3");
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, Boolean.FALSE);
        //
        //		Assert.assertEquals(rtn.size(), 0);
        //
        //		login("devtest", "devtest");	*/
        //
        //		rtn = service.searchCaseInfo(uc, null, searchType, Boolean.FALSE);
        //
        //		Assert.assertEquals(rtn.size(), 2);

        //TODO: add more test cases to verify when have time.

    }

    @Test(dependsOnMethods = { "testSearchCaseInfo" }, enabled = true)
    public void testRetrieveCaseInfos() {
        //		List<CaseInfoType> rtn;
        //
        //		//test without active status
        //		Long supervisionId = new Long(1L);
        //		rtn = service.retrieveCaseInfos(uc, supervisionId, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		//test with ACTIVITY_STATUS
        //		rtn = service.retrieveCaseInfos(uc, supervisionId, true);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		rtn = service.retrieveCaseInfos(uc, supervisionId, false);
        //
        //		Assert.assertEquals(rtn.size(), 0);
        //
        ///*		//Test dataPrivileges
        //		login("QAUser3", "QAUser3");
        //
        //		rtn = service.retrieveCaseInfos(uc, 9991L, null);
        //
        //		Assert.assertEquals(rtn.size(), 0);
        //
        //		login("devtest", "devtest");*/
        //
        //		rtn = service.retrieveCaseInfos(uc, 9991L, null);
        //
        //		Assert.assertEquals(rtn.size(), 2);
        //
        //		//Test invalid input
        //		//supervsion id is null
        //		//rtn = service.retrieveCaseInfos(uc, null, true);

    }

    @Test(dependsOnMethods = { "testRetrieveCaseInfos" }, enabled = true)
    public void testRetrieveCaseStatusChangeHistory() {

        CaseInfoType caseInfo = generateDefaultCaseInfo(true);

        caseInfo = service.createCaseInfo(uc, caseInfo, null);

        Long caseInfoId = caseInfo.getCaseInfoId();

        List<CaseStatusChangeType> list = service.retrieveCaseStatusChangeHistory(uc, caseInfoId, null, null);
        Assert.assertTrue(list.size() == 1);

        caseInfo.setActive(false);
        caseInfo.setStatusChangeReason(CASE_STATUS_REASON_DISMISSED);
        caseInfo = service.updateCaseInfo(uc, caseInfo, null);
        list = service.retrieveCaseStatusChangeHistory(uc, caseInfoId, null, null);
        Assert.assertTrue(list.size() == 2);

        caseInfo.setActive(true);
        caseInfo.setStatusChangeReason(CASE_STATUS_REASON_RE_OPENED);
        caseInfo = service.updateCaseInfo(uc, caseInfo, null);
        list = service.retrieveCaseStatusChangeHistory(uc, caseInfoId, null, null);
        Assert.assertTrue(list.size() == 3);

        for (int i = 0; i < list.size() - 1; i++) {
            CaseStatusChangeType caseStatusChageType1 = list.get(i);
            CaseStatusChangeType caseStatusChageType2 = list.get(i + 1);
            Assert.assertTrue(caseStatusChageType1.getStatusChangeDate().getTime() > caseStatusChageType2.getStatusChangeDate().getTime());
        }

    }

    ///////////////////////Private methods////////////////////////////////////////////////////////////

    @Test(dependsOnMethods = { "testRetrieveCaseStatusChangeHistory" }, enabled = true)
    public void testDelete() {

        Long deleteRtn;

        //Negative delete.
        for (CaseInfoType caseInfo : createdCaseInfos) {
            Long id = caseInfo.getCaseInfoId();
            try {
                deleteRtn = service.deleteCaseInfo(uc, id);
                Assert.assertNotEquals(deleteRtn.longValue(), success.longValue());
            } catch (Exception e) {
                Assert.assertTrue(e instanceof DataExistException);
            }
        }

        //Positive delete.
        CaseInfoType caseInfo = generateDefaultCaseInfo(true);

        CaseInfoType ret = service.createCaseInfo(uc, caseInfo, null);
        Long id = ret.getCaseInfoId();

        CaseAffiliationType caseAffiliation = new CaseAffiliationType();

        caseAffiliation.setCaseAffiliationCategory(CATEGORY_PERSON);
        caseAffiliation.setCaseAffiliationType(TYPE_LEGAL);
        caseAffiliation.setCaseAffiliationRole(ROLE_WITNESS);
        caseAffiliation.setStartDate(new Date());
        caseAffiliation.setAffiliatedPersonIdentityId(999L);
        caseAffiliation.setAffiliatedCaseId(99999l);

        caseAffiliation.setAffiliatedCaseId(id);
        CaseAffiliationType rtn = service.createCaseAffiliation(uc, caseAffiliation);
        Assert.assertNotNull(rtn);

        rtn = service.getCaseAffiliation(uc, rtn.getCaseAffiliationId());
        Assert.assertNotNull(rtn);

        deleteRtn = service.deleteCaseInfo(uc, id);
        assertEquals(deleteRtn.longValue(), success.longValue());
        try {
            service.getCase(uc, id);
        } catch (Exception e) {
            Assert.assertTrue(e instanceof DataNotExistException);
        }
        try {
            service.getCaseAffiliation(uc, rtn.getCaseAffiliationId());
        } catch (Exception e) {
            Assert.assertTrue(e instanceof DataNotExistException);
        }

    }

    private void clientlogin(String user, String password) {

        SecurityClient client;

        try {
            client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password); //"devtest", "devtest" nice regular service user
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private CaseInfoType generateDefaultCaseInfo(boolean onlyMandatory) {
        CaseInfoType caseInfo = new CaseInfoType();
        Date currentDate = new Date();

        //Mandatory fields:
        caseInfo.setCaseCategory(CASE_CATEGORY_IJ);
        caseInfo.setCaseType(CASE_TYPE_ADULT);
        caseInfo.setCreatedDate(currentDate);
        caseInfo.setDiverted(true);
        caseInfo.setDivertedSuccess(true);
        caseInfo.setActive(true);
        caseInfo.setSupervisionId(1L);

        if (onlyMandatory) {
            return caseInfo;
        }

        //Add optional fields:
        caseInfo.setIssuedDate(currentDate);
        caseInfo.setStatusChangeReason(CASE_STATUS_REASON_DISMISSED);
        caseInfo.setSentenceStatus(SENTENCE_STATUS_SENTENCED);
        caseInfo.setFacilityId(1L);

        CommentType comment = new CommentType();
        comment.setUserId("1");
        comment.setCommentDate(currentDate);
        comment.setComment("createCaseInfoPositive");
        caseInfo.setComment(comment);

        caseInfo.getCaseActivityIds().add(1L);

        //Add case identifiers
        CaseIdentifierType caseIdentifier1 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_DOCKET, "0001A", false, 1L, 1L, "createCaseInfoPositive comment1", true);

        CaseIdentifierType caseIdentifier2 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_INDICTMENT, "0002", false, 1L, 1L, "createCaseInfoPositive comment2",
                false);

        caseInfo.getCaseIdentifiers().add(caseIdentifier1);
        caseInfo.getCaseIdentifiers().add(caseIdentifier2);

        return caseInfo;
    }

    private void verfiyCreatedCaseInfo(CaseInfoType actual, CaseInfoType expected) {
        assertNotNull(actual.getCaseInfoId());
        //TODO: assertEquals(actual.getAssociations(), expected.getAssociations());
        assertEquals(actual.getCaseCategory(), expected.getCaseCategory());

        verifyCreatedCaseIdentifiers(actual.getCaseIdentifiers(), expected.getCaseIdentifiers());

        assertEquals(actual.getCaseInfoIds(), expected.getCaseInfoIds());
        assertEquals(actual.getStatusChangeReason(), expected.getStatusChangeReason());
        assertEquals(actual.getCaseType(), expected.getCaseType());
        assertEquals(actual.getChargeIds(), expected.getChargeIds());

        verifyCreatedComment(actual.getComment(), expected.getComment());

        assertEquals(actual.getConditionIds(), expected.getConditionIds());
        assertEquals(getDatePartial(actual.getCreatedDate()), getDatePartial(expected.getCreatedDate()));
        assertEquals(actual.getFacilityId(), expected.getFacilityId());
        assertEquals(getDatePartial(actual.getIssuedDate()), getDatePartial(expected.getIssuedDate()));
        assertEquals(actual.getOrderSentenceIds(), expected.getOrderSentenceIds());
        assertEquals(actual.getSentenceStatus(), expected.getSentenceStatus());
        assertEquals(actual.getSupervisionId(), expected.getSupervisionId());
        assertEquals(actual.isActive(), expected.isActive());
        assertEquals(actual.isDiverted(), expected.isDiverted());
        assertEquals(actual.isDivertedSuccess(), expected.isDivertedSuccess());
    }

    private void verifyCreatedComment(CommentType actual, CommentType expected) {
        if ((actual == null && expected == null) || (actual != null && expected == null) || (actual == null && expected != null)) {
            Assert.assertEquals(actual, expected);
            return;
        }

        assertEquals(actual.getComment(), expected.getComment());
        assertEquals(actual.getCommentDate(), expected.getCommentDate());
        assertEquals(actual.getUserId(), expected.getUserId());
    }

    private void verifyCreatedCaseIdentifiers(List<CaseIdentifierType> actual, List<CaseIdentifierType> expected) {
        if ((actual == null && expected == null) || (actual != null && expected == null) || (actual == null && expected != null)) {
            Assert.assertEquals(actual, expected);
            return;
        }

        Set<CaseIdentifierType> actual1 = new HashSet<CaseIdentifierType>(actual);
        Set<CaseIdentifierType> expected1 = new HashSet<CaseIdentifierType>(expected);
        Assert.assertEquals(actual1, expected1);
    }
}
