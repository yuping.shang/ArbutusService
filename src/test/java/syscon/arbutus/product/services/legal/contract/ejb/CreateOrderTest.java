/**
 *
 */
package syscon.arbutus.product.services.legal.contract.ejb;

import java.math.BigDecimal;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType;
import syscon.arbutus.product.services.legal.contract.dto.charge.*;
import syscon.arbutus.product.services.legal.contract.dto.ordersentence.*;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;

import static org.testng.Assert.*;

/**
 * @author lhan
 */
public class CreateOrderTest extends BaseIT {

    private static final String CASE_CATEGORY_IJ = "IJ";
    private static final String CASE_TYPE_ADULT = "ADULT";
    private static final String SENTENCE_STATUS_SENTENCED = "SENTENCED";
    private static final String CASE_IDENTIFIER_TYPE_DOCKET = "DOCKET";
    private static final String CASE_IDENTIFIER_TYPE_INDICTMENT = "INDICTMENT";
    //=================================================================
    private static final String CHARGE_OUTCOME_SENT = "SENT";
    private static final String CHARGE_STATUS_ACTIVE = "ACTIVE";
    private static final String CHARGE_IDENTIFIER_CJIS = "CJIS";
    private static final String CHARGE_IDENTIFIER_FORMAT_0_00000 = "0-00000";
    private static final String CHARGE_INDICATOR_ACC = "ACC";
    private static final String CHARGE_PLEA_G = "G";
    private static final String APPEAL_STATUS_INPROGRESS = "INPROGRESS";
    private static final String CHARGE_CATEGORY_T = "T";
    private static final String CHARGE_CODE_1510 = "15-10";
    private static final String CHARGE_TYPE_F = "F";
    private static final String CHARGE_DEGREE_I = "I";
    private static final String CHARGE_SEVERITY_H = "H";
    private static final String CHARGE_LANGUAGE_EN = "EN";
    private static final String CHARGE_ENHANCING_FACTOR_RACE = "RACE";
    private static final String CHARGE_ENHANCING_FACTOR_SEX = "SEX";
    private static final String CHARGE_REDUCING_FACTOR_MI = "MI";
    private static final String CHARGE_REDUCING_FACTOR_MIN = "MIN";
    //private CaseManagementService service;  //for Integration test
    //private CaseManagementServiceBean service;   //for local test
    private UserContext uc = null; //new UserContext();
    private Logger log = LoggerFactory.getLogger(OrderSentenceIT.class);
    private List<CaseInfoType> createdCaseInfos = new ArrayList<CaseInfoType>();
    private Set<Long> caseInfoIds = new HashSet<Long>();
    private List<Long> orderIds = new ArrayList<Long>();
    private Set<Long> chargeIds = new HashSet<Long>();
    private LegalService service;
    private String NULL_ERROR_MESSAGE = "Validation Error: syscon.arbutus.product.services: argument must not be null.";

    /**
     * @return the orderIds
     */
    public List<Long> getOrderIds() {
        return orderIds;
    }

    @BeforeClass
    public void beforeClass() {
        lookupJNDILogin();
    }

    //for release
    private void lookupJNDILogin() {
        log.info("Before JNDI lookup");
        service = (LegalService) JNDILookUp(this.getClass(), LegalService.class);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();
    }

    @Test(enabled = true)
    public void testLookupJNDI() {
        assertNotNull(service);
    }

    // @Test(dependsOnMethods = { "testLookupJNDI" })
    public void reset() {
        service.deleteAll(uc);
    }

    @Test(enabled = true)
    public void createOrderConfiguration() {
        //reset();

        // Bail
        OrderConfigurationType ret;
        OrderConfigurationType config;
        OrderConfigurationType ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.BAIL.name());
        ordConfig.setOrderType(OrdType.TOO.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(false);
        ordConfig.setIsSchedulingNeeded(true);
        ordConfig.setHasCharges(true);

        ordConfig.setIssuingAgencies(instancesOfNotification());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);
        config = ret;
        assertEquals(config.getOrderClassification(), OrdClassification.BAIL.name());
        assertEquals(config.getOrderType(), OrdType.TOO.name());
        assertEquals(config.getOrderCategory(), OrdCategory.IJ.name());
        assertEquals(config.getIsHoldingOrder(), ordConfig.getIsHoldingOrder());
        assertEquals(config.getIsSchedulingNeeded(), ordConfig.getIsSchedulingNeeded());
        assertEquals(config.getHasCharges(), ordConfig.getHasCharges());
        assertEquals(config.getIssuingAgencies(), instancesOfNotification());

        // SentenceOrder
        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.SENT.name());
        ordConfig.setOrderType(OrdType.SENTORD.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(true);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(true);

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);
        config = ret;
        assertEquals(config.getOrderClassification(), OrdClassification.SENT.name());
        assertEquals(config.getOrderType(), OrdType.SENTORD.name());
        assertEquals(config.getOrderCategory(), OrdCategory.IJ.name());
        assertEquals(config.getIsHoldingOrder(), Boolean.FALSE, "SDD: If Order Classification = SENT (Sentence Order) then isHoldingOrder = true");
        assertEquals(config.getIsSchedulingNeeded(), ordConfig.getIsSchedulingNeeded());
        assertEquals(config.getHasCharges(), ordConfig.getHasCharges());
        assertEquals(config.getIssuingAgencies(), instancesOfNotification2());

        // LegalOrder
        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.LO.name());
        ordConfig.setOrderType(OrdType.SENTORD.name());
        ordConfig.setOrderCategory(OrdCategory.OJ.name());
        ordConfig.setIsHoldingOrder(true);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(true);

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);

        // WarrantDetainer
        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.WD.name());
        ordConfig.setOrderType(OrdType.ICE.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(true);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(true);

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);

        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.WD.name());
        ordConfig.setOrderType(OrdType.WRMD.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(false);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(false);

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);
    }

    @Test(dependsOnMethods = { "createOrderConfiguration" }, enabled = true)
    public <T extends OrderType> void createAndGet() {
        createCaseInfo();
        createSignleChargePositive();
        createSignleChargePositive();
        Long[] arrayChargeIds = new Long[] { (Long) chargeIds.toArray()[0], (Long) chargeIds.toArray()[1] };

        List<T> orders;
        try {
            assertNull(service.createBail(uc, null));
        } catch (InvalidInputException e) {
            assertEquals(e.getMessage(), NULL_ERROR_MESSAGE);
        }

        try {
            assertNull(service.createWarrantDetainer(uc, null));
        } catch (InvalidInputException e) {
            assertEquals(e.getMessage(), NULL_ERROR_MESSAGE);
        }

        try {
            assertNull(service.createSentence(uc, null));
        } catch (InvalidInputException e) {
            assertEquals(e.getMessage(), NULL_ERROR_MESSAGE);
        }

        try {
            assertNull(service.createLegalOrder(uc, null));
        } catch (InvalidInputException e) {
            assertEquals(e.getMessage(), NULL_ERROR_MESSAGE);
        }

        // Warrant
        WarrantDetainerType retWarrant;
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        cal.add(Calendar.YEAR, 1);
        Date expirationDate = cal.getTime();
        Date termEndDate = cal.getTime();
        Date sentenceEndDate = cal.getTime();

        WarrantDetainerType warrant = new WarrantDetainerType();
        warrant.setOrderClassification(OrdClassification.WD.name());
        warrant.setOrderType(OrdType.ICE.name());
        warrant.setOrderCategory(OrdCategory.IJ.name());
        warrant.setOrderSubType(OrdSubType.WANT.name());
        warrant.setOrderNumber("Warrant Order No.12345");
        warrant.setOrderDisposition(instanceOfDisposition());
        warrant.setComments(instanceOfComment());
        warrant.setOrderIssuanceDate(today);
        warrant.setOrderReceivedDate(today);
        warrant.setOrderStartDate(today);
        warrant.setOrderExpirationDate(today);

        warrant.setIsHoldingOrder(true);
        warrant.setIsSchedulingNeeded(false);
        warrant.setHasCharges(true);
        warrant.setIssuingAgency(instanceOfNotification());

        warrant.setCaseInfoIds(caseInfoIds);

        // Set<Long> chargeIds = new HashSet<Long>();
        //		chargeIds.add(100L);
        //		chargeIds.add(200L);
        //		chargeIds.add(300L);
        warrant.setChargeIds(chargeIds);

        Set<Long> conditionIds = new HashSet<Long>();
        //		conditionIds.add(1000L);
        //		conditionIds.add(2000L);
        //		conditionIds.add(3000L);
        warrant.setConditionIds(conditionIds);

        warrant.setAgencyToBeNotified(instancesOfNotification());

        retWarrant = service.createWarrantDetainer(uc, warrant);
        assertNotNull(retWarrant);
        Long orderId = retWarrant.getOrderIdentification();
        assertNotNull(orderId);
        warrant.setOrderIdentification(orderId);
        assertEquals(retWarrant.getOrderClassification(), OrdClassification.WD.name());
        assertEquals(retWarrant.getOrderType(), OrdType.ICE.name());

        assertEquals(retWarrant.getOrderCategory(), OrdCategory.IJ.name());
        assertEquals(retWarrant.getOrderSubType(), OrdSubType.WANT.name());
        assertEquals(retWarrant.getOrderNumber(), "Warrant Order No.12345");
        assertEquals(retWarrant.getOrderDisposition(), warrant.getOrderDisposition());
        assertNotNull(retWarrant.getComments());
        assertEquals(retWarrant.getOrderIssuanceDate(), today);
        assertEquals(retWarrant.getOrderReceivedDate(), today);
        assertEquals(retWarrant.getOrderStartDate(), today);
        assertEquals(retWarrant.getOrderExpirationDate(), today);

        assertEquals(retWarrant.getIsHoldingOrder(), warrant.getIsHoldingOrder());
        assertEquals(retWarrant.getIsSchedulingNeeded(), warrant.getIsSchedulingNeeded());
        assertEquals(retWarrant.getHasCharges(), warrant.getHasCharges());
        assertEquals(retWarrant.getIssuingAgency(), warrant.getIssuingAgency());
        assertEquals(retWarrant.getCaseInfoIds(), warrant.getCaseInfoIds());
        assertEquals(retWarrant.getChargeIds(), warrant.getChargeIds());
        assertEquals(retWarrant.getConditionIds(), warrant.getConditionIds());
        assertEquals(retWarrant.getAgencyToBeNotified(), warrant.getAgencyToBeNotified());

        warrant.setNotificationLog(new HashSet<NotificationLogType>());
        WarrantDetainerType warrant2 = service.getOrder(uc, warrant.getOrderIdentification());
        assertNotNull(warrant2);
        assertEquals(warrant2, retWarrant);

        for (Long caseId : this.caseInfoIds) {
            orders = service.retrieveOrdersForCase(uc, caseId, null, true);
            assertNotNull(orders);
            assertTrue(orders.size() > 0);
            assertTrue(orders.contains(service.getOrder(uc, warrant.getOrderIdentification())));
        }

		/*assertNotNull(warrant.getCaseActivityInitiatedOrderAssociations());

		assertNotNull(warrant.getOrderInitiatedCaseActivityAssociations());

		for (Long orderInitiatedCaseActivityId: warrant.getOrderInitiatedCaseActivityAssociations()) {
			CourtActivityType ca = service.getCaseActivity(uc, orderInitiatedCaseActivityId);
			assertNotNull(ca);
			assertNotNull(ca.getOrderInititatedIds());
		}

		for (Long caseActivityInitiatedOrderId: warrant.getCaseActivityInitiatedOrderAssociations()) {
			CourtActivityType ca = service.getCaseActivity(uc, caseActivityInitiatedOrderId);
			assertNotNull(ca);
		}*/

        orderIds.add(orderId);

        // Bail
        BailType bailRet;
        BailType bail = new BailType();
        bail.setOrderClassification(OrdClassification.BAIL.name());
        bail.setOrderType(OrdType.TOO.name());
        bail.setOrderCategory(OrdCategory.IJ.name());
        bail.setOrderSubType(OrdSubType.DETAINER.name());
        bail.setOrderNumber("Bail No.12345");
        bail.setOrderDisposition(instanceOfDisposition());
        bail.setComments(instanceOfComment());
        bail.setOrderIssuanceDate(today);
        bail.setOrderReceivedDate(today);
        bail.setOrderStartDate(today);
        bail.setOrderExpirationDate(expirationDate);
        bail.setIsHoldingOrder(false);
        bail.setIsSchedulingNeeded(true);
        bail.setHasCharges(true);
        bail.setIssuingAgency(instanceOfNotification());

        bail.setCaseInfoIds(caseInfoIds);
        // bail.setChargeIds(new HashSet<Long>(Arrays.asList(new Long[] {400L, 500L, 600L})));
        // bail.setChargeIds(new HashSet<Long>());
        bail.setChargeIds(chargeIds);
        // bail.setConditionIds(new HashSet<Long>(Arrays.asList(new Long[] {4000L, 5000L, 6000L})));
        bail.setConditionIds(new HashSet<Long>());

        bail.setBailAmounts(new HashSet<BailAmountType>(Arrays.asList(
                new BailAmountType[] { new BailAmountType(BlType.CASH.name(), BigDecimal.valueOf(400.00), new HashSet<Long>(Arrays.asList(new Long[] { 400L }))),
                        new BailAmountType(BlType.PROP.name(), BigDecimal.valueOf(5000.00), new HashSet<Long>(Arrays.asList(new Long[] { 500L }))) })));
        bail.setBailRelationship(BlRelationship.AG.name());
        bail.setBailPostedAmounts(new HashSet<BailAmountType>(Arrays.asList(
                new BailAmountType[] { new BailAmountType(BlType.CASH.name(), BigDecimal.valueOf(2000.00), new HashSet<Long>(Arrays.asList(new Long[] { 400L }))),
                        new BailAmountType(BlType.PROP.name(), BigDecimal.valueOf(3000.00), new HashSet<Long>(Arrays.asList(new Long[] { 500L }))) })));
        bail.setBailPaymentReceiptNo("Bail paid no. 123456");
        bail.setBailRequirement("Bail Requirement");
        bail.setBailerPersonIdentityAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 111L, 222L, 333L })));
        bail.setBondPostedAmount(new BailAmountType(BlType.CASH.name(), BigDecimal.valueOf(2000.00), new HashSet<Long>(Arrays.asList(new Long[] { 400L }))));
        bail.setBondPaymentDescription("Bond($2000.00) has been paid.");
        bail.setBondOrganizationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 1111L, 2222L, 3333L })));
        bail.setIsBailAllowed(true);

        bailRet = service.createBail(uc, bail);
        assertNotNull(bailRet);
        bail.setOrderIdentification(bailRet.getOrderIdentification());
        orderIds.add(bail.getOrderIdentification());

        // Sentence
        assertTrue(service.setSentenceNumberConfiguration(uc, true));
        SentenceType sentenceRet;
        SentenceType sentence = new SentenceType();
        sentence.setOrderClassification(OrdClassification.SENT.name());
        sentence.setOrderType(OrdType.SENTORD.name());
        sentence.setOrderCategory(OrdCategory.IJ.name());
        sentence.setOrderSubType(OrdSubType.DETAINER.name());
        sentence.setOrderNumber("Sentence No.12345");
        sentence.setOrderDisposition(instanceOfDisposition());
        sentence.setComments(instanceOfComment());
        sentence.setOrderIssuanceDate(today);
        sentence.setOrderReceivedDate(today);
        sentence.setOrderStartDate(today);
        sentence.setOrderExpirationDate(expirationDate);

        sentence.setIsHoldingOrder(true);
        sentence.setIsSchedulingNeeded(false);
        sentence.setHasCharges(true);
        sentence.setIssuingAgency(instanceOfNotification());

        sentence.setCaseInfoIds(caseInfoIds);
        sentence.setChargeIds(new HashSet<Long>(Arrays.asList(new Long[] { arrayChargeIds[0] })));

        // sentence.setConditionIds(new HashSet<Long>(Arrays.asList(new Long[] {40000L, 50000L, 60000L})));
        sentence.setConditionIds(new HashSet<Long>());

        sentence.setSentenceType(SentType.FINEDEF.name());
        sentence.setSentenceNumber(12345L);
        TermType term = new TermType();
        term.setTermCategory(TmCategory.CUS.name());
        term.setTermName(TmName.MAX.name());
        term.setTermType(TmType.INT.name());
        term.setTermStatus(TmStatus.EXC.name());
        term.setTermSequenceNo(12345L);
        term.setTermStartDate(today);
        term.setTermEndDate(termEndDate);
        Set<TermType> sentenceTerms = new HashSet<TermType>();
        sentenceTerms.add(term);
        sentence.setSentenceTerms(sentenceTerms);
        sentence.setSentenceStatus(SentStatus.EXC.name());
        sentence.setSentenceStartDate(today);
        sentence.setSentenceEndDate(sentenceEndDate);
        sentence.setSentenceAppeal(new SentenceAppealType(AppealStatus.ACCEPTED.name(), "Accepted"));
        sentence.setConcecutiveFrom(warrant.getOrderIdentification());
        sentence.setFineAmount(BigDecimal.valueOf(10000.00));
        sentence.setFineAmountPaid(true);
        sentence.setSentenceAggravateFlag(true);
        IntermittentScheduleType sch = new IntermittentScheduleType();
        sch.setDayOfWeek(Day.MON.name());
        sch.setStartTime(sch.new TimeValue(8L, 30L, 0L));
        sch.setEndTime(sch.new TimeValue(16L, 30L, 0L));
        Set<IntermittentScheduleType> intermittentSchedules = new HashSet<IntermittentScheduleType>();
        intermittentSchedules.add(sch);
        sentence.setIntermittentSchedule(intermittentSchedules);

        sentenceRet = service.createSentence(uc, sentence);
        assertNotNull(sentenceRet);
        assertNotNull(sentenceRet.getOrderIdentification());
        sentence.setOrderIdentification(sentenceRet.getOrderIdentification());

        orderIds.add(sentence.getOrderIdentification());

        // Legal
        LegalOrderType legalRet;
        LegalOrderType legal = new LegalOrderType();
        legal.setOrderClassification(OrdClassification.LO.name());
        legal.setOrderType(OrdType.SENTORD.name());
        legal.setOrderCategory(OrdCategory.OJ.name());
        legal.setOjSupervisionId(1111L);
        // legal.setOrderSubType(new CodeType(ReferenceSet.ORDER_SUB_TYPE.value(), OrdSubType.DETAINER.name()));
        legal.setOrderNumber("Legal Order No.12345");
        legal.setOrderDisposition(instanceOfDisposition());
        legal.setComments(instanceOfComment());
        legal.setOrderIssuanceDate(today);
        legal.setOrderReceivedDate(today);
        legal.setOrderStartDate(today);
        legal.setOrderExpirationDate(expirationDate);

        legal.setIsHoldingOrder(true);
        legal.setIsSchedulingNeeded(false);
        legal.setHasCharges(true);
        legal.setIssuingAgency(instanceOfNotification());

        legal.setCaseInfoIds(caseInfoIds);
        // legal.setChargeIds(new HashSet<Long>(Arrays.asList(new Long[] {40000L, 50000L, 60000L})));
        legal.setChargeIds(new HashSet<Long>());
        // legal.setConditionIds(new HashSet<Long>(Arrays.asList(new Long[] {400000L, 500000L, 600000L})));
        legal.setConditionIds(new HashSet<Long>());
        legalRet = service.createLegalOrder(uc, legal);
        assertNotNull(legalRet);
        assertNotNull(legalRet.getOrderIdentification());
        legal.setOrderIdentification(legalRet.getOrderIdentification());

        orderIds.add(legal.getOrderIdentification());
    }

    public void createSignleChargePositive() {

        createStatuteChargePositive();

        //Create single charge instance
        ChargeType dto = getChargeType(1);

        ChargeType ret = service.createCharge(uc, dto);
        assert (ret != null);

        // Test new added display fields.
        Assert.assertTrue(ret.getChargeCode() != null);
        Assert.assertTrue(ret.getChargeDegree() != null);
        Assert.assertTrue(ret.getChargeDescription() != null);
        Assert.assertTrue(ret.getChargeSeverity() != null);
        Assert.assertTrue(ret.getChargeType() != null);

        chargeIds.add(ret.getChargeId());
    }

    private void createStatuteChargePositive() {

        StatuteChargeConfigType dto = new StatuteChargeConfigType();

        //Set mandatory fields
        dto.setStatuteId(1L);
        dto.setChargeCode(CHARGE_CODE_1510);
        dto.setChargeText(getStatuteChargeTextType());
        dto.setBailAllowed(true);
        dto.setBondAllowed(false);

        dto.setStartDate(createPastDate(10));
        dto.setEndDate(createFutureDate(1000));
        dto.setChargeType(CHARGE_TYPE_F);

        //Set optional fields
        dto.setBailAmount(10000L);

        Set<Long> externalChargeCodeIds = new HashSet<Long>();
        externalChargeCodeIds.add(1L);
        externalChargeCodeIds.add(2L);
        dto.setExternalChargeCodeIds(externalChargeCodeIds);

        dto.setChargeDegree(CHARGE_DEGREE_I);
        dto.setChargeCategory(CHARGE_CATEGORY_T);
        dto.setChargeSeverity(CHARGE_SEVERITY_H);

        Set<ChargeIndicatorType> chargeIndicators = new HashSet<ChargeIndicatorType>();
        chargeIndicators.add(getChargeIndicatorType());
        dto.setChargeIndicators(chargeIndicators);

        dto.setEnhancingFactors(getChargeEnhancingFactors());
        dto.setReducingFactors(getChargeReducingFactors());

        StatuteChargeConfigType ret = service.createStatuteCharge(uc, dto);

        assert (ret != null);

        verifyStatuteChargeConfigType(ret, dto);
        ret.getStatuteChargeId();
    }

    private ChargeType getChargeType(long chargeCount) {
        // createCaseInfo();

        ChargeType dto = new ChargeType();

        //get StatuteChargeConfigs
        StatuteChargeConfigSearchType search = new StatuteChargeConfigSearchType();
        search.setChargeCode(CHARGE_CODE_1510);
        StatuteChargeConfigsReturnType rtn = service.searchStatuteCharge(uc, search, null, null, null);

        //Mandatory properties
        dto.setStatuteChargeId(rtn.getStatuteChargeConfigs().get(0).getStatuteChargeId());

        Date now = new Date();
        ChargeDispositionType chargeDisposition = new ChargeDispositionType(CHARGE_OUTCOME_SENT, CHARGE_STATUS_ACTIVE, now);
        dto.setChargeDisposition(chargeDisposition);
        dto.setPrimary(true);
        Long caseInfoId = (Long) this.caseInfoIds.toArray()[0];
        dto.setCaseInfoId(caseInfoId);

        //Optional properties
        Set<ChargeIdentifierType> chargeIdentifiers = new HashSet<ChargeIdentifierType>();
        chargeIdentifiers.add(getChargeIdentifierType());
        dto.setChargeIdentifiers(chargeIdentifiers);

        Set<ChargeIndicatorType> chargeIndicators = new HashSet<ChargeIndicatorType>();
        chargeIndicators.add(getChargeIndicatorType());
        dto.setChargeIndicators(chargeIndicators);

        dto.setLegalText("Legal Text");
        dto.setChargeCount(chargeCount);
        dto.setFacilityId(1L);
        dto.setOrganizationId(1L);
        dto.setPersonIdentityId(1L);
        dto.setOffenceStartDate(now);
        dto.setOffenceEndDate(createFutureDate(10));
        dto.setFilingDate(now);

        ChargePleaType chargePlea = new ChargePleaType(CHARGE_PLEA_G, "CHARGE_PLEA_G creation");
        dto.setChargePlea(chargePlea);

        ChargeAppealType chargeAppeal = new ChargeAppealType(APPEAL_STATUS_INPROGRESS, "APPEAL_STATUS_INPROGRESS creation");
        dto.setChargeAppeal(chargeAppeal);

        // dto.setConditionIds(getModuleIdSet());
        // dto.setOrderSentenceIds(getModuleIdSet());

        CommentType comment = new CommentType();
        comment.setComment("Create Charge");
        comment.setCommentDate(now);
        comment.setUserId("2");
        dto.setComment(comment);

        return dto;
    }

    private ChargeIdentifierType getChargeIdentifierType() {

        ChargeIdentifierType ret = new ChargeIdentifierType(null, CHARGE_IDENTIFIER_CJIS, "1-23456", CHARGE_IDENTIFIER_FORMAT_0_00000, 1L, 1L,
                "Comment getChargeIdentifierType");

        return ret;

    }

    private void verifyStatuteChargeConfigType(StatuteChargeConfigType actual, StatuteChargeConfigType expected) {

        assertNotNull(actual.getStatuteChargeId());
        assertEquals(actual.getStatuteId(), expected.getStatuteId());
        assertEquals(actual.getChargeCode(), expected.getChargeCode());

        assertEquals(actual.getChargeText(), expected.getChargeText());

        assertEquals(actual.getBailAmount(), expected.getBailAmount());
        assertEquals(actual.isBailAllowed(), expected.isBailAllowed());
        assertEquals(actual.isBondAllowed(), expected.isBondAllowed());
        assertEquals(actual.getExternalChargeCodeIds(), expected.getExternalChargeCodeIds());
        assertEquals(actual.getChargeType(), expected.getChargeType());
        assertEquals(actual.getChargeDegree(), expected.getChargeDegree());
        assertEquals(actual.getChargeCategory(), expected.getChargeCategory());
        assertEquals(actual.getChargeSeverity(), expected.getChargeSeverity());

        assertEquals(actual.getChargeIndicators(), expected.getChargeIndicators());

        assertEquals(actual.getEnhancingFactors(), expected.getEnhancingFactors());
        assertEquals(actual.getReducingFactors(), expected.getReducingFactors());

    }

    private Set<String> getChargeEnhancingFactors() {

        Set<String> ret = new HashSet<String>();
        ret.add(CHARGE_ENHANCING_FACTOR_RACE);
        ret.add(CHARGE_ENHANCING_FACTOR_SEX);
        return ret;

    }

    private Set<String> getChargeReducingFactors() {

        Set<String> ret = new HashSet<String>();
        ret.add(CHARGE_REDUCING_FACTOR_MI);
        ret.add(CHARGE_REDUCING_FACTOR_MIN);
        return ret;
    }

    private StatuteChargeTextType getStatuteChargeTextType() {

        StatuteChargeTextType ret = new StatuteChargeTextType(CHARGE_LANGUAGE_EN, "StatuteChargeTextType desc", "StatuteChargeTextType legal text");

        return ret;
    }

    private ChargeIndicatorType getChargeIndicatorType() {

        ChargeIndicatorType ret = new ChargeIndicatorType(null, CHARGE_INDICATOR_ACC, true, "1-23456");

        return ret;

    }

    private Date createPastDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, -day);
        Date pastDate = cal.getTime();
        return pastDate;
    }

    private Date createFutureDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, day);
        Date futureDate = cal.getTime();
        return futureDate;
    }

    private CommentType instanceOfComment() {
        CommentType comment = new CommentType();
        comment.setCommentDate(new Date());
        comment.setComment("Order Comment");
        comment.setUserId("devtest");

        return comment;
    }

    private DispositionType instanceOfDisposition() {
        DispositionType disposition = new DispositionType();
        disposition.setDispositionOutcome(OrdOutcome.SENT.name());
        disposition.setOrderStatus(OrdStatus.ACTIVE.name());
        disposition.setDispositionDate(new Date());

        return disposition;
    }

    private NotificationType instanceOfNotification() {
        NotificationType notification = new NotificationType();
        notification.setNotificationOrganizationAssociation(1L);
        notification.setNotificationFacilityAssociation(204L);
        notification.setIsNotificationNeeded(true);
        notification.setIsNotificationConfirmed(true);
        notification.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6500L, 6600L })));
        return notification;
    }

    private Set<NotificationType> instancesOfNotification() {
        Set<NotificationType> notifications = new HashSet<NotificationType>();
        NotificationType notification = new NotificationType();
        notification.setNotificationOrganizationAssociation(1L);
        notification.setNotificationFacilityAssociation(204L);
        notification.setIsNotificationNeeded(true);
        notification.setIsNotificationConfirmed(true);
        notification.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6500L, 6600L })));
        notifications.add(notification);

        NotificationType notification2 = new NotificationType();
        notification2.setNotificationOrganizationAssociation(2L);
        notification2.setNotificationFacilityAssociation(205L);
        notification2.setIsNotificationNeeded(true);
        notification2.setIsNotificationConfirmed(true);
        notification2.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6700L, 6800L })));
        notifications.add(notification2);
        return notifications;
    }

    private Set<NotificationType> instancesOfNotification2() {
        Set<NotificationType> notifications = new HashSet<NotificationType>();
        NotificationType notification = new NotificationType();
        notification.setNotificationOrganizationAssociation(2L);
        notification.setNotificationFacilityAssociation(205L);
        notification.setIsNotificationNeeded(true);
        notification.setIsNotificationConfirmed(true);
        notification.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6500L, 6600L })));
        notifications.add(notification);

        NotificationType notification2 = new NotificationType();
        notification2.setNotificationOrganizationAssociation(3L);
        notification2.setNotificationFacilityAssociation(206L);
        notification2.setIsNotificationNeeded(true);
        notification2.setIsNotificationConfirmed(true);
        notification2.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6701L, 6801L })));
        notifications.add(notification2);
        return notifications;
    }

    private void createCaseInfo() {

        // Create only mandatory fields
        CaseInfoType caseInfo = generateDefaultCaseInfo(true);

        CaseInfoType ret = service.createCaseInfo(uc, caseInfo, null);

        ret = service.getCaseInfo(uc, ret.getCaseInfoId());

        createdCaseInfos.add(ret);
        caseInfoIds.add(ret.getCaseInfoId());

    }

    private CaseInfoType generateDefaultCaseInfo(boolean onlyMandatory) {

        //createUpdateSearchCaseIdentifierConfigPositive();

        CaseInfoType caseInfo = new CaseInfoType();
        Date currentDate = new Date();

        //Mandatory fields:
        caseInfo.setCaseCategory(CASE_CATEGORY_IJ);
        caseInfo.setCaseType(CASE_TYPE_ADULT);
        caseInfo.setCreatedDate(currentDate);
        caseInfo.setDiverted(true);
        caseInfo.setDivertedSuccess(true);
        caseInfo.setActive(true);
        caseInfo.setSupervisionId(1L);

        if (onlyMandatory) {
            return caseInfo;
        }

        //Add optional fields:
        caseInfo.setIssuedDate(currentDate);
        caseInfo.setSentenceStatus(SENTENCE_STATUS_SENTENCED);
        caseInfo.setFacilityId(1L);

        CommentType comment = new CommentType();
        comment.setUserId("devtest");
        comment.setCommentDate(currentDate);
        comment.setComment("createCaseInfoPositive");
        caseInfo.setComment(comment);

        //caseInfo.getCaseActivityIds().add(1L);

        //Add case identifiers
        CaseIdentifierType caseIdentifier1 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_DOCKET, "0001A", false, 1L, 1L, "createCaseInfoPositive comment1", true);

        CaseIdentifierType caseIdentifier2 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_INDICTMENT, "0002", false, 1L, 1L, "createCaseInfoPositive comment2",
                false);

        caseInfo.getCaseIdentifiers().add(caseIdentifier1);
        caseInfo.getCaseIdentifiers().add(caseIdentifier2);

        return caseInfo;
    }

    public enum OrdClassification {WD, LO, BAIL, SENT}

    public enum OrdType {TOO, SENTORD, ICE, SW, WRMD}

    public enum OrdSubType {WANT, DETAINER, HOLD}

    public enum OrdStatus {ACTIVE, INACTIVE}

    public enum OrdOutcome {WC, WE, SENT, BLSET, BLPOST, BLREV, BLBREACH, BOPOST, BOREV, BOBREACH}

    public enum OrdCategory {IJ, OJ}

    public enum BlType {CASH, SURETY, PROP}

    public enum BlRelationship {AG, DISAG}

    public enum TmType {INGGLE, DUAL, THREE, DUR, INT, TS, SUS}

    public enum TmName {MIN, MAX}

    public enum TmCategory {CUS, COM}

    public enum TmStatus {PEND, INC, EXC}

    public enum Day {MON, TUE, WED, THU, FRI, SAT, SUN}

    public enum Duration {HR, DAY}

    public enum SentType {DEF, INDETER, DETER, SPLIT, INTER, LIFE, DEATH, FINEDEF, PROB, PAR, FIX, FINEDSENT}

    public enum SentStatus {PEND, INC, EXC}

    public enum AppealStatus {INPROGRESS, REJECTED, ACCEPTED}

    public enum NotifType {NOT1, NOT2}

    public enum KeyDates {PDD, PDDWF, HDCED, HDCAD, ARD, PED, SED, CRD, PDDWA, PDDSTAT, PDDWSTAT, PDDWCRE}

    public enum CaseActivityCategory {COURT, PROB, POL, PAROLE}

    public enum CaseActivityType {INITCRT, PRELHEAR, BAILHEAR, GRDJURY, ARRAIGN, TRIAL, SENTENCE, APPEAL, PAROLEHR, PROBHR}

    public enum CaseActivityOutcome {DISMISS, PROBATION, CAPITAL, SENTC, TRL, APPL, BAILAPP, APPLFL, NBL, RMD, PARAPP, PARDNY}

    public enum CaseActivityStatus {ACTIVE, INACTIVE}
}
