package syscon.arbutus.product.services.legal.contract.ejb;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.legal.contract.dto.CaseIdentifierType;
import syscon.arbutus.product.services.legal.contract.dto.CaseInfoType;
import syscon.arbutus.product.services.legal.contract.dto.CaseSearchType;
import syscon.arbutus.product.services.legal.contract.dto.CaseType;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;

import static org.testng.Assert.assertNotNull;

/**
 * LegalServiceIT to test APIs of Legal service.
 *
 * @author lhan
 */
public class LegalServiceIT extends BaseIT {

    protected static final String SERVICE_NAME = "LegalService";
    protected static final String SERVICE_MODULE = "Legal-1.0";
    protected static final String FACILITY_SERVICE_NAME = "FacilityService";
    protected static final String FACILITY_SERVICE_MODULE = "Facility-2.1";
    private static final String FacilityName = "Surrey Court";
    private static final String CASE_CATEGORY_IJ = "IJ";
    private static final String CASE_CATEGORY_OJ = "OJ";
    private static final String CASE_TYPE_ADULT = "ADULT";
    private static final String CASE_TYPE_JUVENILE = "JUVENILE";
    private static final String CASE_TYPE_TRAFFIC = "TRAFFIC";
    private static final String SENTENCE_STATUS_SENTENCED = "SENTENCED";
    private static final String SENTENCE_STATUS_UNSENTENCED = "UNSENTENCED";
    private static final String CASE_STATUS_REASON_DISMISSED = "CD";
    private static final String CASE_STATUS_REASON_RE_OPENED = "RO";
    private static final String CASE_IDENTIFIER_TYPE_DOCKET = "DOCKET";
    private static final String CASE_IDENTIFIER_TYPE_INDICTMENT = "INDICTMENT";
    private static final String CASE_IDENTIFIER_VALUE1 = "0001A";
    private static final String COMMENT = "new comment";
    private LegalService service;
    private FacilityService facilityService;
    private UserContext uc = null; //new UserContext();
    private Logger log = LoggerFactory.getLogger(LegalServiceIT.class);
    private Long personId = 0L;

    private List<CaseInfoType> createdCaseInfos = new ArrayList<CaseInfoType>();
    private List<Facility> issuedFacilities = new ArrayList<Facility>();
    //private List<CaseIdentifierConfigType> createdConfigs = new ArrayList<CaseIdentifierConfigType>();
    //private List<Long> historySetCourt = new ArrayList<Long>();

    /**
     * <p>Cut the time portion of a given Date object.</p>
     *
     * @param dateTime A Date object to trim.
     * @return A Date object.
     */
    private static Date getDatePartial(Date dateTime) {

        if (dateTime == null) {
			return null;
		}

        Calendar cal = Calendar.getInstance();
        cal.setTime(dateTime);

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();
        createFacility("name1", "code1");

        //localLogin();
    }

    @AfterClass
    public void afterClass() {
        for (CaseInfoType caseInfo : createdCaseInfos) {
            service.deleteCaseInfo(uc, caseInfo.getCaseId());
        }

        for (Facility facility : issuedFacilities) {
            facilityService.delete(uc, facility.getFacilityIdentification(), personId);
        }
    }

	/*private Session getSession() {history = historyRet.getCaseInfos().iterator().next();
            assertEquals(history.getSentenceStatus(), SENTENCE_STATUS_SENTENCED);
		
		BasicConfigurator.configure();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("arbutus-casemanagement-test-unit");
	    EntityManager em = emf.createEntityManager();
	    return (Session)em.getDelegate();
	    
	}*/

    //for local debug use only
	/*private void localLogin() {
		
		service = new CaseManagementServiceBean();
		//init user context and login
		uc = super.getUserContext();
		super.login();
		
		service.setSession(getSession());
		
	}*/

    @BeforeMethod
    public void beforeMethod() {
    }

    //for release
    private void lookupJNDILogin() {

        log.info("Before JNDI lookup");
        service = (LegalService) JNDILookUp(this.getClass(), LegalService.class);
        assertNotNull(service);

        facilityService = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        assertNotNull(facilityService);

        //init user context and login
        uc = super.initUserContext();
    }

    @Test
    public void testLookupJNDI() {
        assertNotNull(service);
    }

    //	@Test(dependsOnMethods = { "testLookupJNDI" })
    //	public void createUpdateSearchCaseIdentifierConfigPositive() {
    //
    //		//Create
    //		CaseIdentifierConfigType dto = new CaseIdentifierConfigType(null,
    //				CASE_IDENTIFIER_TYPE_DOCKET,
    //				"000000A",
    //				true, true, true, true);
    //
    //		CaseIdentifierConfigType ret = service.createCaseIdentifierConfig(uc, dto);
    //		assert(ret != null);
    //
    //		createdConfigs.add(ret);
    //
    //		dto = new CaseIdentifierConfigType(null,
    //				CASE_IDENTIFIER_TYPE_DOCKET,
    //				"000000A",
    //				false, false, false, false);
    //
    //		ret = service.createCaseIdentifierConfig(uc, dto);
    //		assert(ret != null);
    //
    //		createdConfigs.add(ret);
    //
    //		//Search
    //		CaseIdentifierConfigSearchType caseIdentifierConfigSearch = new CaseIdentifierConfigSearchType();
    //		caseIdentifierConfigSearch.setIdentifierType(CASE_IDENTIFIER_TYPE_DOCKET);
    //		List<CaseIdentifierConfigType> searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);
    //		Assert.assertNotNull(searchRtn);
    //		Assert.assertEquals(searchRtn.size(), 2);
    //
    //		//Update
    //		dto = createdConfigs.get(0);
    //		dto.setIdentifierType(CASE_IDENTIFIER_TYPE_INDICTMENT);
    //
    //		ret = service.updateCaseIdentifierConfig(uc, dto);
    //		assert(ret != null);
    //		Assert.assertEquals(ret.getIdentifierType(), CASE_IDENTIFIER_TYPE_INDICTMENT);
    //
    //		//Search
    //		caseIdentifierConfigSearch = new CaseIdentifierConfigSearchType();
    //		caseIdentifierConfigSearch.setIdentifierType(CASE_IDENTIFIER_TYPE_INDICTMENT);
    //		searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);
    //		Assert.assertNotNull(searchRtn);
    //		Assert.assertEquals(searchRtn.size(), 1);
    //
    //		//Update
    //		dto.setPrimary(false);
    //
    //		ret = service.updateCaseIdentifierConfig(uc, dto);
    //
    //		assert(ret != null);
    //		Assert.assertEquals(ret.isPrimary(), Boolean.FALSE);
    //
    //		//Search
    //		caseIdentifierConfigSearch.setPrimary(false);
    //		searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);
    //
    //		Assert.assertNotNull(searchRtn);
    //		Assert.assertEquals(searchRtn.size(), 1);
    //
    //		//Update
    //		dto.setActive(false);
    //
    //		ret = service.updateCaseIdentifierConfig(uc, dto);
    //
    //		assert(ret != null);
    //		Assert.assertEquals(ret.isActive(), Boolean.FALSE);
    //
    //		//Search
    //		caseIdentifierConfigSearch.setActive(false);
    //		searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);
    //
    //		Assert.assertNotNull(searchRtn);
    //		Assert.assertEquals(searchRtn.size(), 1);
    //
    //		//Update
    //		dto.setAutoGeneration(false);
    //
    //		ret = service.updateCaseIdentifierConfig(uc, dto);
    //
    //		assert(ret != null);
    //		Assert.assertEquals(ret.isAutoGeneration(), Boolean.FALSE);
    //
    //		//Search
    //		caseIdentifierConfigSearch.setAutoGeneration(false);
    //		searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);
    //
    //		Assert.assertNotNull(searchRtn);
    //		Assert.assertEquals(searchRtn.size(), 1);
    //
    //		//Update
    //		dto.setDuplicateCheck(false);
    //
    //		ret = service.updateCaseIdentifierConfig(uc, dto);
    //
    //		assert(ret != null);
    //		Assert.assertEquals(ret.isDuplicateCheck(), Boolean.FALSE);
    //
    //
    //		//Search
    //		caseIdentifierConfigSearch.setIdentifierFormat("000000A");
    //		searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);
    //
    //		Assert.assertNotNull(searchRtn);
    //		Assert.assertEquals(searchRtn.size(), 1);
    //
    //		//Search
    //		caseIdentifierConfigSearch.setIdentifierType(CASE_IDENTIFIER_TYPE_DOCKET);
    //		searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);
    //
    //		Assert.assertNotNull(searchRtn);
    //		Assert.assertEquals(searchRtn.size(), 1);
    //
    //		//Search
    //		caseIdentifierConfigSearch.setActive(true);
    //		searchRtn = service.searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);
    //
    //		Assert.assertNotNull(searchRtn);
    //		Assert.assertEquals(searchRtn.size(), 0);
    //
    //	}
    //
    //	@Test(dependsOnMethods = { "createUpdateSearchCaseIdentifierConfigPositive" }, enabled = false)
    //	public void createUpdateCaseIdentifierConfigNegative() {
    //
    //		CaseIdentifierConfigType ret = service.createCaseIdentifierConfig(null, null);
    //
    //		assert(ret == null);
    //
    //		//Empty DTO
    //		CaseIdentifierConfigType dto = new CaseIdentifierConfigType();
    //		ret = service.createCaseIdentifierConfig(uc, dto);
    //
    //		assert(ret == null);
    //
    //		//Wrong DTO
    //		dto = new CaseIdentifierConfigType();
    //		dto.setAutoGeneration(true);
    //		ret = service.createCaseIdentifierConfig(uc, dto);
    //
    //		assert(ret == null);
    //
    //		//For an identifier type, can only create one active config type.
    //		dto = new CaseIdentifierConfigType(null,
    //				CASE_IDENTIFIER_TYPE_DOCKET,
    //				"000000A",
    //				true, true, true, true);
    //		ret = service.createCaseIdentifierConfig(uc, dto);
    //
    //		assert(ret != null);
    //
    //		dto = new CaseIdentifierConfigType(null,
    //				CASE_IDENTIFIER_TYPE_DOCKET,
    //				"000000A",
    //				false, true, false, false);
    //		ret = service.createCaseIdentifierConfig(uc, dto);
    //
    //		Assert.assertEquals(ret, null);
    //
    //		// Only 2 primary
    //		dto = new CaseIdentifierConfigType(null,
    //				CASE_IDENTIFIER_TYPE_DOCKET,
    //				"000000A",
    //				false, false, true, false);
    //		ret = service.createCaseIdentifierConfig(uc, dto);
    //
    //		Assert.assertNotNull(ret);
    //
    //		dto = new CaseIdentifierConfigType(null,
    //				CASE_IDENTIFIER_TYPE_DOCKET,
    //				"000000A",
    //				false, false, true, false);
    //		ret = service.createCaseIdentifierConfig(uc, dto);
    //
    //		Assert.assertEquals(ret, null);
    //
    //	}
    //
    @Test(dependsOnMethods = { "testLookupJNDI" })
    public void createCasePositive() {

        // Create only mandatory fields
        CaseType caseInfo = generateDefaultCaseInfo(true);

        CaseType ret = service.saveCase(uc, caseInfo);

        assert (ret != null);

        verfiyCreatedCaseInfo(ret, caseInfo);

        ret = service.getCase(uc, ret.getCaseId());

        verfiyCreatedCaseInfo(ret, caseInfo);

        createdCaseInfos.add(ret);

        // Create full fields
        caseInfo = generateDefaultCaseInfo(false);
        ret = service.saveCase(uc, caseInfo);

        assert (ret != null);

        verfiyCreatedCaseInfo(ret, caseInfo);

        ret = service.getCase(uc, ret.getCaseId());

        verfiyCreatedCaseInfo(ret, caseInfo);

        createdCaseInfos.add(ret);
    }

    //
    //	@Test(dependsOnMethods = { "createCaseInfoPositive" }, enabled = false)
    //	public void createCaseInfoNegative() {
    //
    //		//NULL CaseInfoType
    //		CaseInfoType ret = service.createCaseInfo(null, null, null);
    //
    //		assert(ret == null);
    //
    //		//Empty CaseInfoType
    //		CaseInfoType caseInfo = new CaseInfoType();
    //		ret = service.createCaseInfo(uc, caseInfo, null);
    //
    //		assert(ret == null);
    //
    //		//Missing mandatory fields in CaseInfoType
    //		caseInfo = new CaseInfoType();
    //
    //		//Has case category
    //    	caseInfo.setCaseCategory(CASE_CATEGORY_IJ);
    //		ret = service.createCaseInfo(uc, caseInfo, null);
    //
    //		assert(ret == null);
    //
    //		//Has case type
    //    	caseInfo.setCaseType(CASE_TYPE_ADULT);
    //		ret = service.createCaseInfo(uc, caseInfo, null);
    //
    //		assert(ret == null);
    //
    //		//Has case created date
    //    	Date currentDate = Calendar.getInstance().getTime();
    //		caseInfo.setCreatedDate(currentDate);
    //		ret = service.createCaseInfo(uc, caseInfo, null);
    //
    //		assert(ret == null);
    //
    //		//Has case diverted flag
    //    	caseInfo.setDiverted(true);
    //		ret = service.createCaseInfo(uc, caseInfo, null);
    //
    //		assert(ret == null);
    //
    //		//Has case diverted success flag.
    //		caseInfo.setDivertedSuccess(true);
    //		ret = service.createCaseInfo(uc, caseInfo, null);
    //
    //		assert(ret == null);
    //
    //		//Has active flag.
    //		caseInfo.setActive(true);
    //		ret = service.createCaseInfo(uc, caseInfo, null);
    //
    //		assert(ret == null);
    //
    //		// Test privileges
    //		caseInfo = generateDefaultCaseInfo(false);
    //		caseInfo.getDataPrivileges().add("F");
    //
    //		ret = service.createCaseInfo(uc, caseInfo, null);
    //
    //		assert(ret == null);
    //
    //		// Test association privileges
    //		Set<String> privileges = new HashSet<String>();
    //		privileges.add("D");
    //
    //		AssociationType association = createdCaseInfos.get(1).getAssociations().iterator().next();
    //
    //		Long rtn = service.updateAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.Success.returnCode());
    //		Set<String> privilegesReturned = service.getAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association);
    //		Assert.assertEquals(privilegesReturned, privileges);
    //
    //		caseInfo = generateDefaultCaseInfo(true);
    //		caseInfo.getAssociations().add(association);
    //
    //		login("QAUser3", "QAUser3");
    //		ret = service.createCaseInfo(uc, caseInfo, null);
    //
    //		assert(ret == null);
    //
    //		login("devtest", "devtest");
    //	}
    //
    @Test(dependsOnMethods = { "createCasePositive" })
    public void getCasePositive() {
        for (CaseInfoType caseInfo : createdCaseInfos) {
            Long id = caseInfo.getCaseId();
            CaseType ret = service.getCase(uc, id);

            assert (ret != null);
            Assert.assertEquals(ret.getPrimaryCaseIdentifier(), caseInfo.getPrimaryCaseIdentifier());
        }

/*		Long id = 99999L;
		CaseType ret = service.getCase(uc, id);*/

    }

    @Test(dependsOnMethods = { "getCasePositive" })
    public void updateCasePositive() {

        // Create only mandatory fields
        CaseInfoType caseInfo = createdCaseInfos.get(1);

        // add new second case identifier
        CaseIdentifierType identifier = new CaseIdentifierType();
        identifier.setIdentifierType(CASE_IDENTIFIER_TYPE_INDICTMENT);
        identifier.setIdentifierValue("new added");

        caseInfo.getCaseIdentifiers().add(identifier);

        CaseType caseType = new CaseType(caseInfo);

        CaseType ret = service.saveCase(uc, caseType);

        assert (ret != null);

        verfiyCreatedCaseInfo(ret, caseType);

        ret = service.getCase(uc, ret.getCaseId());

        verfiyCreatedCaseInfo(ret, caseType);

        //Update primary case identifier.
        caseType = ret;

        CaseIdentifierType oldPrimary = new CaseIdentifierType();
        oldPrimary.setIdentifierType(caseType.getPrimaryCaseIdentifier().getIdentifierType());
        oldPrimary.setIdentifierValue(caseType.getPrimaryCaseIdentifier().getIdentifierValue());
        oldPrimary.setSourceFacilityId(caseType.getPrimaryCaseIdentifier().getSourceFacilityId());
        oldPrimary.setSourceOrganizationId(caseType.getPrimaryCaseIdentifier().getSourceOrganizationId());

        caseType.getPrimaryCaseIdentifier().setIdentifierValue("new primary");
        caseType.setSaveAsSecondaryIdentifier(true);

        ret = service.saveCase(uc, caseType);

        assert (ret != null);

        assert (ret.getCaseIdentifiers().size() == caseType.getCaseIdentifiers().size() + 1);

        assert (ret.getCaseIdentifiers().contains(oldPrimary));

        ret = service.getCase(uc, ret.getCaseId());

        assert (ret != null);

        assert (ret.getCaseIdentifiers().size() == caseType.getCaseIdentifiers().size() + 1);

        assert (ret.getCaseIdentifiers().contains(oldPrimary));

        //Update primary case identifier back.
        caseType = ret;

        CaseIdentifierType secondOldPrimary = new CaseIdentifierType();
        secondOldPrimary.setIdentifierType(caseType.getPrimaryCaseIdentifier().getIdentifierType());
        secondOldPrimary.setIdentifierValue(caseType.getPrimaryCaseIdentifier().getIdentifierValue());
        secondOldPrimary.setSourceFacilityId(caseType.getPrimaryCaseIdentifier().getSourceFacilityId());
        secondOldPrimary.setSourceOrganizationId(caseType.getPrimaryCaseIdentifier().getSourceOrganizationId());

        caseType.getPrimaryCaseIdentifier().setIdentifierValue(oldPrimary.getIdentifierValue());
        caseType.setSaveAsSecondaryIdentifier(true);

        ret = service.saveCase(uc, caseType);

        assert (ret != null);
        assert (ret.getCaseIdentifiers().size() == caseType.getCaseIdentifiers().size());
        assert (!ret.getCaseIdentifiers().contains(oldPrimary));
        assert (ret.getCaseIdentifiers().contains(secondOldPrimary));

        ret = service.getCase(uc, ret.getCaseId());

        assert (ret != null);
        assert (ret.getCaseIdentifiers().size() == caseType.getCaseIdentifiers().size());
        assert (!ret.getCaseIdentifiers().contains(oldPrimary));
        assert (ret.getCaseIdentifiers().contains(secondOldPrimary));

        //Update primary case identifier without save as sencondary identifier.
        caseType = ret;

        oldPrimary = new CaseIdentifierType();
        oldPrimary.setIdentifierType(caseType.getPrimaryCaseIdentifier().getIdentifierType());
        oldPrimary.setIdentifierValue(caseType.getPrimaryCaseIdentifier().getIdentifierValue());
        oldPrimary.setSourceFacilityId(caseType.getPrimaryCaseIdentifier().getSourceFacilityId());
        oldPrimary.setSourceOrganizationId(caseType.getPrimaryCaseIdentifier().getSourceOrganizationId());

        caseType.getPrimaryCaseIdentifier().setIdentifierValue("new primary again");
        caseType.setSaveAsSecondaryIdentifier(false);

        ret = service.saveCase(uc, caseType);

        assert (ret != null);

        assert (ret.getCaseIdentifiers().size() == caseType.getCaseIdentifiers().size());

        assert (!ret.getCaseIdentifiers().contains(oldPrimary));

        ret = service.getCase(uc, ret.getCaseId());

        assert (ret != null);

        assert (ret.getCaseIdentifiers().size() == caseType.getCaseIdentifiers().size());

        assert (!ret.getCaseIdentifiers().contains(oldPrimary));

        //Update other fields:
        caseType = ret;

        CommentType newComment = new CommentType();
        newComment.setComment("updated comment");
        newComment.setCommentDate(new Date());
        caseType.setComment(newComment);

        caseType.setActiveFlag(false);
        caseType.setCaseCategory(CASE_CATEGORY_OJ);
        caseType.setCaseType(CASE_TYPE_JUVENILE);
        //caseType.setFacilityId(998L);
        caseType.setIssuedDate(new Date());
        caseType.setStatusChangeReason(CASE_STATUS_REASON_RE_OPENED);
        caseType.setSentenceStatus(SENTENCE_STATUS_UNSENTENCED);
        caseType.setSupervisionId(998L);

        ret = service.saveCase(uc, caseType);

        assert (ret != null);
        assert (ret.getCaseIdentifiers().size() == caseType.getCaseIdentifiers().size());
        assert (!ret.getCaseIdentifiers().contains(oldPrimary));
        assert (ret.getCaseIdentifiers().contains(secondOldPrimary));

        verfiyCreatedCaseInfo(ret, caseType);

        ret = service.getCase(uc, ret.getCaseId());

        assert (ret != null);
        assert (ret.getCaseIdentifiers().size() == caseType.getCaseIdentifiers().size());
        assert (!ret.getCaseIdentifiers().contains(oldPrimary));
        assert (ret.getCaseIdentifiers().contains(secondOldPrimary));

        verfiyCreatedCaseInfo(ret, caseType);

        //Special test, update null comment.
        caseInfo = createdCaseInfos.get(0);
        caseType = new CaseType(caseInfo);

        newComment = new CommentType();
        newComment.setComment("updated comment");
        newComment.setCommentDate(new Date());
        caseType.setComment(newComment);
        ret = service.saveCase(uc, caseType);

        assert (ret != null);

        verfiyCreatedCaseInfo(ret, caseType);
    }
    //
    //	@Test(dependsOnMethods = { "getCaseInfoPositive" }, enabled = false)
    //	public void getCaseInfoNegative(){
    //		//null id
    //		Long id = null;
    //		CaseInfoType ret = service.getCaseInfo(uc, id);
    //
    //		assert(ret == null);
    //
    //		//invalid id.
    //		id = 99999L;
    //		ret = service.getCaseInfo(uc, id);
    //
    //		assert(ret == null);
    //
    //		//Test dataPrivileges
    //		login("QAUser3", "QAUser3");
    //
    //		ret = service.getCaseInfo(uc, createdCaseInfos.get(1).getCaseInfoId());
    //
    //		assert(ret == null);
    //
    //		login("devtest", "devtest");
    //
    //		// Test association privileges
    //		Set<String> privileges = new HashSet<String>();
    //		privileges.add("A");
    //		privileges.add("B");
    //		privileges.add("C");
    //
    //		Long rtn = service.updateInstanceDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, createdCaseInfos.get(1).getCaseInfoId(), privileges);
    //		Assert.assertEquals(rtn, ReturnCode.Success.returnCode());
    //
    //		privileges = new HashSet<String>();
    //		privileges.add("D");
    //
    //		AssociationType association = createdCaseInfos.get(1).getAssociations().iterator().next();
    //
    //		rtn = service.updateAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.Success.returnCode());
    //		Set<String> privilegesReturned = service.getAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association);
    //		Assert.assertEquals(privilegesReturned, privileges);
    //
    //		login("QAUser3", "QAUser3");
    //		ret = service.getCaseInfo(uc, createdCaseInfos.get(1).getCaseInfoId());
    //
    //		assert(ret != null);
    //
    //		Assert.assertTrue(ret.getAssociations().contains(association) == false);
    //
    //		login("devtest", "devtest");
    //
    //		ret = service.getCaseInfo(uc, createdCaseInfos.get(1).getCaseInfoId());
    //
    //		assert(ret!= null);
    //
    //		Assert.assertTrue(ret.getAssociations().contains(association) == true);
    //
    //	}
    //
    //	@Test(dependsOnMethods = { "getCaseInfoPositive" })
    //	public void testUpdate() {
    //
    //		//Negative return test.
    //		//assertEquals(service.updateCaseInfo(uc, null, null).getReturnCode(), ReturnCode.CInvalidInput2002.returnCode());
    //		//assertEquals(service.updateCaseInfo(uc, new CaseInfoType(), null).getReturnCode(), ReturnCode.CInvalidInput2002.returnCode());
    //
    //		//no id.
    //		CaseInfoType caseInfo = generateDefaultCaseInfo(true);
    //		//assertEquals(service.updateCaseInfo(uc, caseInfo, null).getReturnCode(), ReturnCode.CInvalidInput2002.returnCode());
    //
    //		Date fromDate = new Date();
    //		Date toDate = new Date();
    //
    //		//positive test.
    //		for (CaseInfoType createdCaseInfo: createdCaseInfos) {
    //			int i = 0;
    //			List<CaseInfoType> historyRet;
    //
    //			CaseInfoType caseInfoRet = service.getCaseInfo(uc, createdCaseInfo.getCaseInfoId());
    //			caseInfo = caseInfoRet;
    //
    //			//test when category has new value.
    //			caseInfo.setCaseCategory(CASE_CATEGORY_OJ);
    //
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			i++;
    //
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 0);
    //
    //			//test when Type has updated value.
    //			caseInfo.setCaseType(CASE_TYPE_TRAFFIC);
    //			fromDate = new Date();
    //
    //			Date beforeUpdate = new Date();
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //			Date afterUpdate = new Date();
    //
    //
    //
    //			//test getStamp for update API:
    //			/*StampType stamp = service.getStamp(uc, caseInfoRet.getCaseInfoId());
    //			Assert.assertTrue(stamp.getCreateDateTime().before(afterUpdate));
    //			Assert.assertTrue(stamp.getCreateDateTime().before(beforeUpdate));
    //			Assert.assertTrue(stamp.getModifyDateTime().before(afterUpdate));
    //			//TODO Assert.assertTrue(stamp.getModifyDateTime().after(beforeUpdate));
    //			Assert.assertTrue("devtest".equals(stamp.getCreateUserId()));
    //			Assert.assertTrue("devtest".equals(stamp.getModifyUserId()));
    //			Assert.assertTrue(stamp.getInvocationContext() instanceof String);*/
    //
    //			i++;
    //			assertEquals(caseInfoRet.getCaseType(), CASE_TYPE_TRAFFIC);
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			toDate = new Date();
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 1);
    //
    //			//test when status has updated value.
    //			caseInfo.setSentenceStatus(SENTENCE_STATUS_UNSENTENCED);
    //			fromDate = new Date();
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			i++;
    //			assertEquals(caseInfoRet.getSentenceStatus(), SENTENCE_STATUS_UNSENTENCED);
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			toDate = new Date();
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 1);
    //
    //			//test IssuedDate has update value.
    //			Date newDate = new Date();
    //			caseInfo.setIssuedDate(newDate);
    //			fromDate = new Date();
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			i++;
    //			assertEquals(caseInfoRet.getIssuedDate(), getDatePartial(newDate));
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			toDate = new Date();
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 1);
    //
    //			//test CaseStatusReason has updated value.
    //			caseInfo.setReactivationReason(CASE_STATUS_REASON_RE_OPENED);
    //			fromDate = new Date();
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			i++;
    //			assertEquals(caseInfoRet.getReactivationReason(), CASE_STATUS_REASON_RE_OPENED);
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			toDate = new Date();
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 1);
    //
    //			//test comment
    //			CommentType comment = new CommentType();
    //			comment.setComment("comment2");
    //			comment.setCommentDate(new Date());
    //			caseInfo.setComment(comment);
    //			fromDate = new Date();
    //
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			i++;
    //			assertEquals(caseInfoRet.getComment().getComment(), comment.getComment());
    //			assertEquals(caseInfoRet.getComment().getCommentDate(), comment.getCommentDate());
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			toDate = new Date();
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 1);
    //
    //			//test active
    //			caseInfo.setActive(false);
    //			fromDate = new Date();
    //
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			i++;
    //			assertEquals(caseInfoRet.isActive(), Boolean.FALSE);
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			toDate = new Date();
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 1);
    //
    //			//test Diverted
    //			caseInfo.setDiverted(false);
    //			fromDate = new Date();
    //
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			i++;
    //			assertEquals(caseInfoRet.isDiverted(), Boolean.FALSE);
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			toDate = new Date();
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 1);
    //
    //			//test DivertedSuccess
    //			caseInfo.setDivertedSuccess(false);
    //			fromDate = new Date();
    //
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			i++;
    //			assertEquals(caseInfoRet.isDivertedSuccess(), Boolean.FALSE);
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			toDate = new Date();
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 1);
    //
    //			//test facility association
    //			caseInfo.setFacility(new AssociationType(AssociationToClass.FACILITY.value(),9991L));
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			i++;
    //			assertEquals(caseInfoRet.getFacility(), caseInfo.getFacility());
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			toDate = new Date();
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 2);
    //
    //			//test Supervision association
    //			caseInfo.setSupervision(new AssociationType(AssociationToClass.SUPERVISION.value(),9991L));
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			i++;
    //			assertEquals(caseInfoRet.getSupervision(), caseInfo.getSupervision());
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			toDate = new Date();
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 3);
    //
    //			//test static associations positive
    //			Set<Long> chargeIds = new HashSet<Long>();
    //			Set<Long> orderIds = new HashSet<Long>();
    //			Set<Long> conditionIds = new HashSet<Long>();
    //
    //			Long id1 = new Long(3011L);
    //			chargeIds.add(id1);
    //			id1 = new Long(3021L);
    //			chargeIds.add(id1);
    //			id1 = new Long(3031L);
    //			chargeIds.add(id1);
    //
    //			id1 = new Long(4011L);
    //			orderIds.add(id1);
    //			id1 = new Long(4021L);
    //			orderIds.add(id1);
    //
    //			id1 = new Long(5011L);
    //			conditionIds.add(id1);
    //
    //			caseInfo.setChargeIds(chargeIds);
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			i++;
    //			assertEquals(caseInfoRet.getChargeIds(), caseInfo.getChargeIds());
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			toDate = new Date();
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 4);
    //
    //			caseInfo.setOrderSentenceIds(orderIds);
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			i++;
    //			assertEquals(caseInfoRet.getOrderSentenceIds(), caseInfo.getOrderSentenceIds());
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			toDate = new Date();
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 5);
    //
    //			caseInfo.setConditionIds(conditionIds);
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			i++;
    //			assertEquals(caseInfoRet.getConditionIds(), caseInfo.getConditionIds());
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			toDate = new Date();
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 6);
    //
    //			//testing dataPrivileges positive
    //			Set<String> privileges = new HashSet<String>();
    //			privileges.add("D");
    //
    //			fromDate = new Date();
    //
    //			caseInfo.setDataPrivileges(privileges);
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			i++;
    //			assertEquals(caseInfoRet.getDataPrivileges(), caseInfo.getDataPrivileges());
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			toDate = new Date();
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 1);
    //
    //			//test associations has update value.
    //			Set<AssociationType> associations = new HashSet<AssociationType>();
    //			AssociationType asso1 = new AssociationType(AssociationToClass.CASE_ACTIVITY.value(), 105L);
    //			associations.add(asso1);
    //			caseInfo.setAssociations(associations);
    //			asso1 = new AssociationType(AssociationToClass.CASE_ACTIVITY.value(), 205L);
    //			associations.add(asso1);
    //
    //			fromDate = new Date();
    //
    //
    //			i++;
    //			assertEquals(caseInfoRet.getDataPrivileges(), caseInfo.getDataPrivileges());
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), null, null);
    //
    //			assertEquals(historyRet.size(), i + 1);
    //
    //			toDate = new Date();
    //			historyRet = service.retrieveCaseInfoHistory(uc, caseInfo.getCaseInfoId(), fromDate, toDate);
    //
    //			assertEquals(historyRet.size(), 1);
    //
    //			historySetCourt.add(new Long(i));
    //
    //			// test privileges negative
    //			privileges.add("F");
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			assertEquals(caseInfoRet, null);
    //
    //			login("QAUser3", "QAUser3");
    //			privileges.clear();
    //			privileges.add("A");
    //
    //			caseInfoRet = service.updateCaseInfo(uc, caseInfo, null);
    //
    //			assertEquals(caseInfoRet, null);
    //
    //			login("devtest", "devtest");
    //
    //		}
    //	}
    //
    //
    //	@Test(dependsOnMethods = { "testUpdate" }, enabled = true)
    //	public void testRetrieveCaseInfoHistory() {
    //
    //		//Set<Long> retAll = service.getAll(uc);
    //		//assertEquals(retAll.size(), createdCaseInfos.size());
    //		for (int index =0 ; index < createdCaseInfos.size(); index++) {
    //			Long id = createdCaseInfos.get(index).getCaseInfoId();
    //			List<CaseInfoType> ret = service.retrieveCaseInfoHistory(uc, id, null, null);
    //			assertEquals(ret.size(), historySetCourt.get(index) + 1);
    //		}
    //
    //		List<CaseInfoType> ret;
    //
    //		//test invalid id.
    //		//Long id = 9999999L;
    //		//ret = service.retrieveCaseInfoHistory(uc, id, null, null);
    //
    //		//test start date and end date
    //		ret = service.retrieveCaseInfoHistory(uc, createdCaseInfos.get(0).getCaseInfoId(), new Date(), new Date());
    //		assertEquals(ret.size(), 0);
    //
    //		//test privileges negative.
    //		login("QAUser3", "QAUser3");
    //
    //		//ret = service.retrieveCaseInfoHistory(uc, createdCaseInfos.get(1).getCaseInfoId(), new Date(), new Date());
    //
    //		login("devtest", "devtest");
    //	}
    //
    //	@Test(dependsOnMethods = { "testRetrieveCaseInfoHistory" }, enabled = true)
    //	public void testSearch() {
    //		//test isWellFormed and isValid
    //		CaseInfoSearchType searchType = new CaseInfoSearchType();
    //		Assert.assertEquals(searchType.isWellFormed(), Boolean.FALSE);
    //		Assert.assertEquals(searchType.isValid(), Boolean.FALSE);
    //
    //		searchType.setCaseCategory(new CodeType(ReferenceSet.CASE_CATEGORY.value(), "IJ"));
    //		Assert.assertEquals(searchType.isWellFormed(), Boolean.TRUE);
    //		Assert.assertEquals(searchType.isValid(), Boolean.TRUE);
    //
    //		//test CaseCategory
    //		searchType = new CaseInfoSearchType();
    //		searchType.setCaseCategory(CASE_CATEGORY_IJ);
    //		List<CaseInfoType> rtn = service.searchCaseInfo(uc, null, searchType, null);
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		searchType.setCaseType(CASE_TYPE_JUVENILE);
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 0);
    //
    //		searchType.setCaseType(CASE_TYPE_ADULT);
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		searchType.setCaseType(CASE_TYPE_TRAFFIC);
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 0);
    //
    //		//test CASE_STATUS_REASON
    //		searchType = new CaseInfoSearchType();
    //		searchType.setReason(CASE_STATUS_REASON_RE_OPENED);
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		searchType.setReason(CASE_STATUS_REASON_DISMISSED);
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 0);
    //
    //		//test SENTENCE_STATUS
    //		searchType = new CaseInfoSearchType();
    //		searchType.setSentenceStatus(SENTENCE_STATUS_UNSENTENCED);
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		searchType.setSentenceStatus(SENTENCE_STATUS_SENTENCED);
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 0);
    //
    //		//test SUPERVISION Association
    //		searchType = new CaseInfoSearchType();
    //		AssociationType association = new AssociationType();
    //		association.setToClass(AssociationToClass.SUPERVISION.value());
    //		association.setToIdentifier(9991L);
    //		searchType.setSupervision(association);
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		searchType = new CaseInfoSearchType();
    //		association = new AssociationType();
    //		association.setToClass(AssociationToClass.SUPERVISION.value());
    //		association.setToIdentifier(1L);
    //		searchType.setSupervision(association);
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		//from/to start date
    //		searchType = new CaseInfoSearchType();
    //		Calendar fromDate = Calendar.getInstance();
    //		fromDate.set(2011,01,01);
    //		searchType.setCreatedDateFrom(fromDate.getTime());
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 4);
    //
    //		fromDate.set(2021,01,01);
    //		searchType.setCreatedDateFrom(fromDate.getTime());
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 0);
    //
    //		Calendar toDate = Calendar.getInstance();
    //		toDate.set(2020,01,01);
    //		searchType.setCreatedDateTo(toDate.getTime());
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 0);
    //
    //		//from/to issued date
    //		searchType = new CaseInfoSearchType();
    //		fromDate = Calendar.getInstance();
    //		fromDate.set(2011,01,01);
    //		searchType.setIssuedDateFrom(fromDate.getTime());
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		toDate = Calendar.getInstance();
    //		toDate.set(2021,01,01);
    //		searchType.setIssuedDateTo(toDate.getTime());
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		//isDevertedflag
    //		searchType = new CaseInfoSearchType();
    //		searchType.setDiverted(true);
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		searchType.setDiverted(false);
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		//isDevertedSuccessfullyFlag
    //		searchType = new CaseInfoSearchType();
    //		searchType.setDivertedSuccess(true);
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		searchType.setDivertedSuccess(false);
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		//isActive
    //		searchType = new CaseInfoSearchType();
    //		searchType.setActive(true);
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		searchType.setActive(false);
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		//Case Identifier Search Type
    //		searchType = new CaseInfoSearchType();
    //		CaseIdentifierSearchType identifierSearchType = new CaseIdentifierSearchType();
    //		identifierSearchType.setIdentifierValue("****");
    //		searchType.setCaseIdentifierSearch(identifierSearchType);
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn, null);
    //
    //		searchType = new CaseInfoSearchType();
    //		identifierSearchType = new CaseIdentifierSearchType();
    //		identifierSearchType.setIdentifierValue("0002");
    //		searchType.setCaseIdentifierSearch(identifierSearchType);
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 1);
    //
    //		identifierSearchType.setIdentifierValue("000");
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 0);
    //
    //		identifierSearchType.setIdentifierValue("000*");
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		identifierSearchType.setIdentifierValue("000*");
    //		identifierSearchType.setIdentifierType(CASE_IDENTIFIER_TYPE_DOCKET);
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		identifierSearchType.setIdentifierValue("000*");
    //		identifierSearchType.setIdentifierType(CASE_IDENTIFIER_TYPE_INDICTMENT);
    //		rtn = service.searchCaseInfo(uc, null, searchType, null);
    //
    //		Assert.assertEquals(rtn.size(), 1);
    //
    //
    //		//search history
    //		rtn = service.searchCaseInfo(uc, null, searchType, Boolean.TRUE);
    //
    //		Assert.assertEquals(rtn.size(), 1);
    //
    //		searchType = new CaseInfoSearchType();
    //		searchType.setActive(true);
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, Boolean.FALSE);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, Boolean.TRUE);
    //
    //		Assert.assertEquals(rtn.size(), 4);
    //
    //		searchType = new CaseInfoSearchType();
    //		searchType.setCaseCategory(CASE_CATEGORY_IJ);
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, Boolean.FALSE);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, Boolean.TRUE);
    //
    //		Assert.assertEquals(rtn.size(), 4);
    //
    //		//Test privileges
    //		searchType = new CaseInfoSearchType();
    //		searchType.setCaseCategory(CASE_CATEGORY_OJ);
    //
    //		login("QAUser3", "QAUser3");
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, Boolean.FALSE);
    //
    //		Assert.assertEquals(rtn.size(), 0);
    //
    //		login("devtest", "devtest");
    //
    //		rtn = service.searchCaseInfo(uc, null, searchType, Boolean.FALSE);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //
    //		//TODO: add more test cases to verify when have time.
    //
    //	}
    //
    //	@Test(dependsOnMethods = { "testSearch" }, enabled = true)
    //	public void testRetrieveCaseInfos() {
    //		List<CaseInfoType> rtn;
    //
    //		//test without active status
    //		Long supervisionId = new Long(1L);
    //		rtn = service.retrieveCaseInfos(uc, supervisionId, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		//test with ACTIVITY_STATUS
    //		rtn = service.retrieveCaseInfos(uc, supervisionId, true);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		rtn = service.retrieveCaseInfos(uc, supervisionId, false);
    //
    //		Assert.assertEquals(rtn.size(), 0);
    //
    //		//Test dataPrivileges
    //		login("QAUser3", "QAUser3");
    //
    //		rtn = service.retrieveCaseInfos(uc, 9991L, null);
    //
    //		Assert.assertEquals(rtn.size(), 0);
    //
    //		login("devtest", "devtest");
    //
    //		rtn = service.retrieveCaseInfos(uc, 9991L, null);
    //
    //		Assert.assertEquals(rtn.size(), 2);
    //
    //		//Test invalid input
    //		//supervsion id is null
    //		//rtn = service.retrieveCaseInfos(uc, null, true);
    //
    //
    //	}
    //
    //	@Test(dependsOnMethods = { "testRetrieveCaseInfos" }, enabled = true)
    //	public void testCreateAssociation() {
    //		Long caseInfoId = createdCaseInfos.get(0).getCaseInfoId();
    //		AssociationType association = new AssociationType(AssociationToClass.CASE_ACTIVITY.value(), 2002L);
    //
    //		AssociationReturnType rtn = service.createAssociation(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, association);
    //
    //		Assert.assertNotNull(rtn.getAssociation());
    //
    //		association = new AssociationType(AssociationToClass.CASE_ACTIVITY.value(), 2002L);
    //		rtn = service.createAssociation(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, association);
    //		Assert.assertEquals(rtn.getReturnCode(), ReturnCode.RInvalidInput1003.returnCode());
    //		Assert.assertEquals(rtn.getAssociation(), null);
    //
    //		association = new AssociationType(AssociationToClass.CASE_ACTIVITY.value(), 2003L);
    //		rtn = service.createAssociation(uc, CaseMgmtModule.CASE_INFORMATION, null, association);
    //
    //		Assert.assertEquals(rtn.getAssociation(), null);
    //
    //		association = new AssociationType(AssociationToClass.FACILITY.value(), 2003L);
    //		rtn = service.createAssociation(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, association);
    //		Assert.assertEquals(rtn.getReturnCode(), ReturnCode.CInvalidInput2003.returnCode());
    //		Assert.assertEquals(rtn.getAssociation(), null);
    //
    //		//test dataprivileges
    //		login("QAUser3", "QAUser3");
    //
    //		association = new AssociationType(AssociationToClass.CASE_ACTIVITY.value(), 999L);
    //		rtn = service.createAssociation(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, association);
    //		Assert.assertEquals(rtn.getReturnCode(), ReturnCode.RInvalidInput1003.returnCode());
    //		Assert.assertEquals(rtn.getAssociation(), null);
    //
    //		login("devtest", "devtest");
    //
    //		association = new AssociationType(AssociationToClass.CASE_ACTIVITY.value(), 999L);
    //		rtn = service.createAssociation(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, association);
    //
    //		Assert.assertNotNull(rtn.getAssociation());
    //
    //	}
    //
    //	@Test(dependsOnMethods = { "testCreateAssociation" }, enabled = true)
    //	public void testGetAssociations() {
    //		Long ID = createdCaseInfos.get(0).getCaseInfoId();
    //
    //		AssociationsReturnType ret = service.getAllAssociations(uc, CaseMgmtModule.CASE_INFORMATION, ID);
    //		assertEquals(ret.getReturnCode(), success);
    //		Assert.assertTrue(ret.getCount() > 0);
    //		assertNotNull(ret.getAssociations());
    //
    //		ret = service.getAllAssociations(uc, CaseMgmtModule.CASE_INFORMATION, 1111111L);
    //		assertEquals(ret.getReturnCode().longValue(), 1003L);
    //		Assert.assertNull(ret.getCount());
    //		Assert.assertNull(ret.getAssociations());
    //
    //		// getAssociationsOfClass
    //		ret = service.getAssociationsOfClass(uc, CaseMgmtModule.CASE_INFORMATION, ID, "StaffService");
    //		assertEquals(ret.getReturnCode().intValue(), 2002);
    //		Assert.assertNull(ret.getCount());
    //		Assert.assertNull(ret.getAssociations());
    //
    //		ret = service.getAssociationsOfClass(uc, CaseMgmtModule.CASE_INFORMATION, ID, AssociationToClass.CASE_ACTIVITY.value());
    //		assertEquals(ret.getReturnCode(), success);
    //		Assert.assertTrue(ret.getCount() > 0);
    //		assertNotNull(ret.getAssociations());
    //	}
    //
    //	@Test(dependsOnMethods = { "testGetAssociations" }, enabled = true)
    //	public void testDeleteAssociation() {
    //		Long caseInfoId = createdCaseInfos.get(0).getCaseInfoId();
    //		AssociationType association = new AssociationType(AssociationToClass.CASE_ACTIVITY.value(), 2002L);
    //
    //		Long rtn = service.deleteAssociation(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, association);
    //		Assert.assertEquals(rtn, success);
    //
    //		association = new AssociationType(AssociationToClass.CASE_ACTIVITY.value(), 99999L);
    //
    //		rtn = service.deleteAssociation(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, association);
    //		Assert.assertEquals(rtn, ReturnCode.RInvalidInput1003.returnCode());
    //
    //		//Test privileges
    //		//test dataprivileges
    //		login("QAUser3", "QAUser3");
    //
    //		association = new AssociationType(AssociationToClass.CASE_ACTIVITY.value(), 999L);
    //		rtn = service.deleteAssociation(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, association);
    //		Assert.assertEquals(rtn, ReturnCode.RInvalidInput1003.returnCode());
    //
    //		login("devtest", "devtest");
    //
    //		association = new AssociationType(AssociationToClass.CASE_ACTIVITY.value(), 999L);
    //		rtn = service.deleteAssociation(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, association);
    //		Assert.assertEquals(rtn, success);
    //
    //	}
    //
    //	@Test(dependsOnMethods = { "testDeleteAssociation" }, enabled = true)
    //	public void testUpdateInstanceDataPrivileges() {
    //		Long caseInfoId = createdCaseInfos.get(0).getCaseInfoId();
    //
    //		Set<String> privileges = new HashSet<String>();
    //
    //		privileges.add("A");
    //
    //		Long rtn = service.updateInstanceDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.Success.returnCode());
    //
    //		CaseInfoType caseInfoNew = service.getCaseInfo(uc, caseInfoId);
    //
    //		Assert.assertEquals(caseInfoNew.getCaseInfoId(), caseInfoId);
    //		Assert.assertEquals(caseInfoNew.getDataPrivileges(), privileges);
    //
    //		privileges.add("B");
    //
    //		rtn = service.updateInstanceDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.Success.returnCode());
    //
    //		caseInfoNew = service.getCaseInfo(uc, caseInfoId);
    //
    //		Assert.assertEquals(caseInfoNew.getCaseInfoId(), caseInfoId);
    //		Assert.assertEquals(caseInfoNew.getDataPrivileges(), privileges);
    //
    //		privileges.add("C");
    //
    //		rtn = service.updateInstanceDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.Success.returnCode());
    //
    //		caseInfoNew = service.getCaseInfo(uc, caseInfoId);
    //
    //		Assert.assertEquals(caseInfoNew.getCaseInfoId(), caseInfoId);
    //		Assert.assertEquals(caseInfoNew.getDataPrivileges(), privileges);
    //
    //		privileges.add("F");
    //
    //		rtn = service.updateInstanceDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.RInvalidInput1003.returnCode());
    //
    //
    //		privileges = new HashSet<String>();
    //		privileges.add("HP");
    //
    //		rtn = service.updateInstanceDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.Success.returnCode());
    //
    //		caseInfoNew = service.getCaseInfo(uc, caseInfoId);
    //
    //		Assert.assertEquals(caseInfoNew.getCaseInfoId(), caseInfoId);
    //		Assert.assertEquals(caseInfoNew.getDataPrivileges(), privileges);
    //
    //		// test privileges negative
    //		login("QAUser3", "QAUser3");
    //		privileges = new HashSet<String>();
    //
    //		rtn = service.updateInstanceDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.RInvalidInput1003.returnCode());
    //
    //		login("devtest", "devtest");
    //		privileges = new HashSet<String>();
    //		privileges.add("ABC");
    //
    //		rtn = service.updateInstanceDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, caseInfoId, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.RInvalidInput1003.returnCode());
    //	}
    //
    //	@Test(dependsOnMethods = { "testUpdateInstanceDataPrivileges" }, enabled = true)
    //	public void testGetandUpdateAssociationDataPrivileges() {
    //
    //		AssociationType association = createdCaseInfos.get(1).getAssociations().iterator().next();
    //
    //		Set<String> privileges = new HashSet<String>();
    //
    //		privileges.add("A");
    //
    //		Long rtn = service.updateAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.Success.returnCode());
    //		Set<String> privilegesReturned = service.getAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association);
    //		Assert.assertEquals(privilegesReturned, privileges);
    //
    //		privileges.add("B");
    //
    //		rtn = service.updateAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.Success.returnCode());
    //		 privilegesReturned = service.getAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association);
    //		Assert.assertEquals(privilegesReturned, privileges);
    //
    //		privileges.add("C");
    //
    //		rtn = service.updateAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.Success.returnCode());
    //		 privilegesReturned = service.getAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association);
    //		Assert.assertEquals(privilegesReturned, privileges);
    //
    //		privileges.add("F");
    //
    //		rtn = service.updateAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.RInvalidInput1003.returnCode());
    //		privilegesReturned = service.getAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association);
    //		privileges.remove("F");
    //		Assert.assertEquals(privilegesReturned, privileges);
    //
    //
    //		privileges = new HashSet<String>();
    //		privileges.add("D");
    //
    //		rtn = service.updateAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.Success.returnCode());
    //		privilegesReturned = service.getAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association);
    //		Assert.assertEquals(privilegesReturned, privileges);
    //
    //		// test privileges negative
    //		login("QAUser3", "QAUser3");
    //		privileges = new HashSet<String>();
    //
    //		rtn = service.updateAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.RInvalidInput1003.returnCode());
    //
    //		login("devtest", "devtest");
    //		privileges = new HashSet<String>();
    //		privileges.add("ABC");
    //
    //		rtn = service.updateAssociationDataPrivileges(uc, CaseMgmtModule.CASE_INFORMATION, association, privileges);
    //		Assert.assertEquals(rtn, ReturnCode.RInvalidInput1003.returnCode());
    //
    //	}
    //
    //	@Test(dependsOnMethods = { "testGetandUpdateAssociationDataPrivileges" }, enabled = true)
    //	public void testDelete() {
    //
    //		//test delete null
    //		Long deleteRtn = service.deleteCaseInfo(uc, null);
    //		assertEquals(deleteRtn.longValue(), 2002l);
    //
    //		//test delete invalid id.
    //		 deleteRtn = service.deleteCaseInfo(uc, 9999999L);
    //		assertEquals(deleteRtn.longValue(), 1003l);
    //
    //		//test privileges negative.
    //		login("QAUser3", "QAUser3");
    //
    //		Long id = createdCaseInfos.get(1).getCaseInfoId();
    //		deleteRtn = service.deleteCaseInfo(uc, id);
    //		assertEquals(deleteRtn, ReturnCode.RInvalidInput1003.returnCode());
    //
    //		login("devtest", "devtest");
    //
    //		//positive delete.
    //		for (CaseInfoType caseInfo : createdCaseInfos) {
    //			id = caseInfo.getCaseInfoId();
    //			deleteRtn = service.deleteCaseInfo(uc, id);
    //			assertEquals(deleteRtn.longValue(), success.longValue());
    //
    //			//can not get current one.
    //			//CaseInfoType getRtn = service.getCaseInfo(uc, id);
    //
    //			//can not get history too.
    //			//List<CaseInfoType> ret = service.retrieveCaseInfoHistory(uc, id, null, null);
    //
    //
    //			//can not get associations by id
    //			AssociationsReturnType assocRtn = service.getAllAssociations(uc,CaseMgmtModule.CASE_INFORMATION, id);
    //			assertEquals(assocRtn.getReturnCode().longValue(), 1003l);
    //		}
    //	}

    ///////////////////////Private methods////////////////////////////////////////////////////////////

    @Test(dependsOnMethods = { "getCasePositive" })
    public void getCasesPositive() {
        CaseSearchType searchType = new CaseSearchType();
        searchType.setSupervisionId(1L);
        List<CaseInfoType> ret = service.getCases(uc, searchType);

        assert (ret != null);
        assert (ret.size() >= 1);

        searchType = new CaseSearchType();
        searchType.setFacility(issuedFacilities.get(0).getFacilityIdentification());
        searchType.setSupervisionId(1L);
        searchType.setCaseIdentifierValue(CASE_IDENTIFIER_VALUE1);
        searchType.setCaseIdentifierType(CASE_IDENTIFIER_TYPE_DOCKET);

        ret = service.getCases(uc, searchType);

        assert (ret != null);
        Assert.assertNotSame(ret.size(), 0);

        searchType = new CaseSearchType();
        searchType.setFacility(2L);
        searchType.setSupervisionId(1L);
        searchType.setCaseIdentifierValue(CASE_IDENTIFIER_VALUE1);
        searchType.setCaseIdentifierType(CASE_IDENTIFIER_TYPE_DOCKET);

        ret = service.getCases(uc, searchType);

        assert (ret == null);

    }

    private CaseType generateDefaultCaseInfo(boolean onlyMandatory) {
        CaseType caseInfo = new CaseType();
        Date currentDate = new Date();

        //Mandatory fields:
        caseInfo.setCaseCategory(CASE_CATEGORY_IJ);
        caseInfo.setCaseType(CASE_TYPE_ADULT);
        caseInfo.setCreatedDate(currentDate);
        caseInfo.setActiveFlag(true);
        caseInfo.setSupervisionId(1L);
        caseInfo.setFacilityId(issuedFacilities.get(0).getFacilityIdentification());

        //Add primary case identifier
        CaseIdentifierType caseIdentifier = new CaseIdentifierType();
        caseIdentifier.setIdentifierType(CASE_IDENTIFIER_TYPE_DOCKET);
        caseIdentifier.setIdentifierValue("0001A");
        caseInfo.setPrimaryCaseIdentifier(caseIdentifier);

        if (onlyMandatory) {
            return caseInfo;
        }

        //add comment
        CommentType comment = new CommentType();
        comment.setComment(COMMENT);
        comment.setCommentDate(currentDate);
        caseInfo.setComment(comment);

        //Add optional fields:
        caseInfo.setIssuedDate(currentDate);
        caseInfo.setStatusChangeReason(CASE_STATUS_REASON_DISMISSED);
        caseInfo.setSentenceStatus(SENTENCE_STATUS_SENTENCED);

        //caseInfo.getAssociations().add(new AssociationType(CASE_ACTIVITY, 1L));

        //Add case identifiers
        CaseIdentifierType caseIdentifier1 = new CaseIdentifierType();
        caseIdentifier1.setIdentifierType(CASE_IDENTIFIER_TYPE_DOCKET);
        caseIdentifier1.setIdentifierValue("0002A");

        CaseIdentifierType caseIdentifier2 = new CaseIdentifierType();
        caseIdentifier2.setIdentifierType(CASE_IDENTIFIER_TYPE_INDICTMENT);
        caseIdentifier2.setIdentifierValue("0002");
        caseIdentifier2.setSourceFacilityId(1L);
        caseIdentifier2.setSourceOrganizationId(1L);

        caseInfo.setCaseIdentifiers(new ArrayList<CaseIdentifierType>());

        caseInfo.getCaseIdentifiers().add(caseIdentifier1);
        caseInfo.getCaseIdentifiers().add(caseIdentifier2);

        // Add dataPrivileges
        Set<String> privileges = new HashSet<String>();
        privileges.add("A");
        privileges.add("B");
        privileges.add("C");
        privileges.add("D");
        caseInfo.setDataPrivileges(privileges);

        return caseInfo;
    }

    private void createFacility(String name, String code) {
        Facility facility = new Facility();
        facility.setFacilityCategoryText("COURT");
        facility.setFacilityCode(code);
        facility.setFacilityName(name);

        Facility ret = facilityService.create(uc, facility);

        issuedFacilities.add(ret);
    }

    private void verfiyCreatedCaseInfo(CaseType actual, CaseType expected) {
        Assert.assertNotNull(actual.getCaseId());
        Assert.assertEquals(actual.getCaseCategory(), expected.getCaseCategory());

        //verifyCreatedCaseIdentifiers(actual.getCaseIdentifiers(), expected.getCaseIdentifiers());

        //assertEquals(actual.getCaseInfoIds(), expected.getCaseInfoIds());
        Assert.assertEquals(actual.getStatusChangeReason(), expected.getStatusChangeReason());
        Assert.assertEquals(actual.getCaseType(), expected.getCaseType());
        //assertEquals(actual.getChargeIds(), expected.getChargeIds());

        //assertEquals(actual.getConditionIds(), expected.getConditionIds());
        Assert.assertEquals(getDatePartial(actual.getCreatedDate()), getDatePartial(expected.getCreatedDate()));
        //Assert.assertEquals(actual.getDataPrivileges(), expected.getDataPrivileges());
        Assert.assertEquals(actual.getFacilityId(), expected.getFacilityId());
        Assert.assertEquals(getDatePartial(actual.getIssuedDate()), getDatePartial(expected.getIssuedDate()));
        //assertEquals(actual.getOrderSentenceIds(), expected.getOrderSentenceIds());
        Assert.assertEquals(actual.getSentenceStatus(), expected.getSentenceStatus());
        Assert.assertEquals(actual.getSupervisionId(), expected.getSupervisionId());
        Assert.assertEquals(actual.isActiveFlag(), expected.isActiveFlag());
        Assert.assertEquals(actual.getIssuingFacilityName(), issuedFacilities.get(0).getFacilityName());

        Assert.assertEquals(actual.getPrimaryCaseIdentifier(), expected.getPrimaryCaseIdentifier());

        if (expected.getComment() != null) {
            Assert.assertEquals(actual.getComment().getComment(), expected.getComment().getComment());
        }

    }

	/*private void verifyCreatedCaseIdentifiers(
			List<CaseIdentifierType> actual,
			List<CaseIdentifierType> expected) {
		if((actual == null && expected == null) || (actual != null && expected == null) || (actual == null && expected != null)){
			Assert.assertEquals(actual, expected);
			return;
		}
		
		Assert.assertEquals(actual, expected);		
	}*/
}
