package syscon.arbutus.product.services.legal.contract.ejb;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigDecimal;
import java.util.*;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Session;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.exception.DataExistException;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierConfigType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType;
import syscon.arbutus.product.services.legal.contract.dto.charge.*;
import syscon.arbutus.product.services.legal.contract.dto.ordersentence.*;
import syscon.arbutus.product.services.legal.contract.ejb.OrderSentenceIT.*;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;
import syscon.arbutus.product.services.realization.util.BeanHelper;

import static org.testng.Assert.*;

public class ChargeIT extends BaseIT {

    private static final String EXTERNAL_CHARGE_CODE_CATEGORY_I = "I";

    private static final String EXTERNAL_CHARGE_CODE_36A = "36A";

    private static final String EXTERNAL_CHARGE_CODE_GROUP_UCR = "UCR";
    private static final String EXTERNAL_CHARGE_CODE_GROUP_NCIC = "NCIC";

    private static final String STATUTE_COUNTRY_USA = "USA";
    private static final String STATUTE_COUNTRY_CAN = "CAN";

    private static final String STATUTE_PROVINCE_BC = "BC";
    private static final String STATUTE_PROVINCE_OR = "OR";

    private static final String STATUTE_COUNTY_13 = "13";
    private static final String STATUTE_COUNTY_1 = "1";

    private static final String STATUTE_CODE_CC = "CC";
    private static final String STATUTE_CODE_CS = "CS";

    private static final String CHARGE_OUTCOME_SENT = "SENT";
    private static final String CHARGE_OUTCOME_RELBAIL = "RELBAIL";

    private static final String CHARGE_STATUS_ACTIVE = "ACTIVE";
    private static final String CHARGE_STATUS_INACTIVE = "INACTIVE";

    private static final String CHARGE_IDENTIFIER_CJIS = "CJIS";

    private static final String CHARGE_IDENTIFIER_FORMAT_0_00000 = "0-00000";

    private static final String CHARGE_INDICATOR_ACC = "ACC";
    private static final String CHARGE_INDICATOR_ARRT = "ARRT";
    private static final String CHARGE_INDICATOR_EMP = "EMP";

    private static final String CHARGE_PLEA_G = "G";
    private static final String CHARGE_PLEA_N = "N";

    private static final String APPEAL_STATUS_INPROGRESS = "INPROGRESS";
    private static final String APPEAL_STATUS_ACCEPTED = "ACCEPTED";

    private static final String CHARGE_CATEGORY_T = "T";
    private static final String CHARGE_CATEGORY_A = "A";

    private static final String CHARGE_CODE_1510 = "15-10";

    private static final String CHARGE_TYPE_F = "F";

    private static final String CHARGE_DEGREE_I = "I";
    private static final String CHARGE_DEGREE_II = "II";

    private static final String CHARGE_SEVERITY_H = "H";
    private static final String CHARGE_SEVERITY_L = "L";

    private static final String CHARGE_LANGUAGE_EN = "EN";

    private static final String CHARGE_ENHANCING_FACTOR_RACE = "RACE";
    private static final String CHARGE_ENHANCING_FACTOR_SEX = "SEX";

    private static final String CHARGE_REDUCING_FACTOR_MI = "MI";
    private static final String CHARGE_REDUCING_FACTOR_MIN = "MIN";

    // ===CaseInfo Related ========================================================

    private static final String CASE_CATEGORY_IJ = "IJ";
    private static final String CASE_CATEGORY_OJ = "OJ";

    private static final String CASE_TYPE_ADULT = "ADULT";
    private static final String CASE_TYPE_JUVENILE = "JUVENILE";
    private static final String CASE_TYPE_TRAFFIC = "TRAFFIC";

    private static final String SENTENCE_STATUS_SENTENCED = "SENTENCED";
    private static final String SENTENCE_STATUS_UNSENTENCED = "UNSENTENCED";

    private static final String CASE_STATUS_REASON_DISMISSED = "DISMISSED";
    private static final String CASE_STATUS_REASON_RE_OPENED = "RE-OPENED";

    private static final String CASE_IDENTIFIER_TYPE_DOCKET = "DOCKET";
    private static final String CASE_IDENTIFIER_TYPE_INDICTMENT = "INDICTMENT";

    private static final String CASE_IDENTIFIER_FORMAT_000000A = "000000A";

    private List<CaseInfoType> createdCaseInfos = new ArrayList<CaseInfoType>();
    private List<CaseIdentifierConfigType> createdConfigs = new ArrayList<CaseIdentifierConfigType>();
    private List<Long> historySetCourt = new ArrayList<Long>();
    private List<Long> caseInfoIds = new ArrayList<Long>();
    private Long statuteChargeConfigId = 1L;
    //===========================================================

    private List<Long> orderIds = new ArrayList<Long>();

    private LegalService service;   //for local test

    private UserContext uc = null; //new UserContext();
    private Logger log = LoggerFactory.getLogger(ChargeIT.class);

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();

        //localLogin();

        reset();
    }

    @BeforeMethod
    public void beforeMethod() {
    }

    //for local debug use only
    @SuppressWarnings("unused")
    private void localLogin() {

        BasicConfigurator.configure();
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("arbutus-legal-test-unit");
        EntityManager em = emf.createEntityManager();
        Session session = (Session) em.getDelegate();

        //service = new LegalServiceBean(session, context);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();

    }

    //for release
    private void lookupJNDILogin() {

        log.info("Before JNDI lookup");

        service = (LegalService) JNDILookUp(this.getClass(), LegalService.class);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();

    }

    @Test
    public void testLookupJNDI() {
        assertNotNull(service);
    }

    //@Test(dependsOnMethods = { "testLookupJNDI" })
    public void reset() {

        Long ret = service.deleteAll(uc);
        assert (ret.equals(ReturnCode.Success.returnCode()));
    }

    @Test
    public void setOffenceDateConfig() {

        Long ret = service.setOffenceDateConfig(uc, false);
        assert (ret.equals(ReturnCode.Success.returnCode()));

        ret = service.setOffenceDateConfig(uc, true);
        assert (ret.equals(ReturnCode.Success.returnCode()));

    }

    @Test(dependsOnMethods = { "setOffenceDateConfig" })
    public void getOffenceStartDateConfig() {

        Boolean ret = service.getOffenceStartDateConfig(uc);
        assert (ret != null);
    }

    @Test(dependsOnMethods = { "getOffenceStartDateConfig" }, enabled = false)
    public void deleteOffenceDateConfig() {

        Long ret = service.deleteOffenceDateConfig(uc);
        assert (ret.equals(ReturnCode.Success.returnCode()));

    }

    //@Test
    public void createExternalChargeCodeNegative() {

        //Empty parameter
        ExternalChargeCodeConfigType ret = service.createExternalChargeCode(null, null);
        assert (ret == null);
        //Empty dto
        ExternalChargeCodeConfigType dto = new ExternalChargeCodeConfigType();
        ret = service.createExternalChargeCode(uc, dto);

        //Not set all mandatory fields
        dto = new ExternalChargeCodeConfigType();
        dto.setCodeCategory("Test");
        ret = service.createExternalChargeCode(uc, dto);

        //Empty parameter
        ret = service.updateExternalChargeCode(null, null);

        //Empty dto
        dto = new ExternalChargeCodeConfigType();
        ret = service.updateExternalChargeCode(uc, dto);

        //Not set all mandatory fields
        dto = new ExternalChargeCodeConfigType();
        dto.setCodeCategory("Test");
        ret = service.updateExternalChargeCode(uc, dto);

    }

    @Test
    public void createExternalChargeCodePositive() {
        reset();

        // Need to clear the exist records first
        ExternalChargeCodeConfigType dto = new ExternalChargeCodeConfigType();
        dto.setCodeCategory(EXTERNAL_CHARGE_CODE_CATEGORY_I);
        dto.setChargeCode(EXTERNAL_CHARGE_CODE_36A);
        dto.setCodeGroup(EXTERNAL_CHARGE_CODE_GROUP_UCR);
        dto.setDescription("Test createExternalChargeCodePositive");
        ExternalChargeCodeConfigType ret = service.createExternalChargeCode(uc, dto);

        assert (ret != null);

        verifyExternalChargeCodeConfigType(ret, dto);
        /*
        //Check for unique duplicated, will return 1003
		dto = new ExternalChargeCodeConfigType();
		dto.setCodeCategory(EXTERNAL_CHARGE_CODE_CATEGORY_I);
		dto.setChargeCode(EXTERNAL_CHARGE_CODE_36A);
		dto.setCodeGroup(EXTERNAL_CHARGE_CODE_GROUP_UCR);
		dto.setDescription("Test createExternalChargeCodePositive");
		ret = service.createExternalChargeCode(uc, dto);
		
		assert(ret == null);
		*/
    }

    @Test(dependsOnMethods = { "createExternalChargeCodePositive" })
    public void updateExternalChargeCodePositive() {

        Long extChargeCodeId = createExternalChargeCode("I", "240", "UCR");

        ExternalChargeCodeConfigType dto = new ExternalChargeCodeConfigType();
        dto.setChargeCodeId(extChargeCodeId);
        dto.setCodeCategory(EXTERNAL_CHARGE_CODE_CATEGORY_I);
        dto.setChargeCode(EXTERNAL_CHARGE_CODE_36A);
        dto.setCodeGroup(EXTERNAL_CHARGE_CODE_GROUP_NCIC);
        dto.setDescription("Test updateExternalChargeCodePositive");
        ExternalChargeCodeConfigType ret = service.updateExternalChargeCode(uc, dto);

        assert (ret != null);

        verifyExternalChargeCodeConfigType(ret, dto);

    }

    @Test(dependsOnMethods = { "updateExternalChargeCodePositive" })
    public void deleteExternalChargeCode() {
		/*
		Long ret = service.deleteExternalChargeCode(null, null);
		assert(ret.equals(ReturnCode.CInvalidInput2002.returnCode()));
		*/
        Long extChargeCodeId = createExternalChargeCode("I", "510", "UCR");

        Long ret = service.deleteExternalChargeCode(uc, extChargeCodeId);
        assert (ret.equals(ReturnCode.Success.returnCode()));

    }

    @Test(dependsOnMethods = { "deleteExternalChargeCode" })
    public void createMultiExternalChargeCode() {

        //Load data for testing pagination
        createExternalChargeCode("I", "26E", "UCR");
        createExternalChargeCode("I", "11C", "UCR");
        createExternalChargeCode("I", "90H", "NCIC");
        createExternalChargeCode("I", "220", "NCIC");
        createExternalChargeCode("I", "23F", "NCIC");
        createExternalChargeCode("I", "13A", "NCIC");
        createExternalChargeCode("I", "23C", "NCIC");
        //createExternalChargeCode("I", "26B", "NCIC"); //26B is getting deactivated by other testcase.
        createExternalChargeCode("I", "200", "NCIC");
        createExternalChargeCode("I", "100", "NCIC");

    }

    @Test(dependsOnMethods = { "createMultiExternalChargeCode" })
    public void searchExternalChargeCode() {

        ExternalChargeCodeConfigSearchType search = new ExternalChargeCodeConfigSearchType();
        search.setCodeCategory(EXTERNAL_CHARGE_CODE_CATEGORY_I);

        List<ExternalChargeCodeConfigType> ret = service.searchExternalChargeCode(uc, search);
        assert (ret != null);

        //Test wild card search on charge code
        search = new ExternalChargeCodeConfigSearchType();
        search.setChargeCode("1*");

        ret = service.searchExternalChargeCode(uc, search);
        assert (ret != null);
        assert (ret.size() == 3);

        //Test exact search on charge code
        search = new ExternalChargeCodeConfigSearchType();
        search.setChargeCode("100");

        ret = service.searchExternalChargeCode(uc, search);
        assert (ret != null);
        assert (ret.size() == 1);

    }

    //@Test
    public void searchAllExternalChargeCode() {

        List<ExternalChargeCodeConfigType> ret = service.searchAllExternalChargeCode(uc);
        assert (ret != null);

    }

    //@Test
    public void createUpdateJurisdictionNegative() {

        //Empty parameter
        JurisdictionConfigType ret = service.createJurisdiction(null, null);
        assert (ret == null);
        //Empty dto
        JurisdictionConfigType dto = new JurisdictionConfigType();
        ret = service.createJurisdiction(uc, dto);
        assert (ret == null);
        //Not set all mandatory fields
        dto = new JurisdictionConfigType();
        dto.setCountry(STATUTE_COUNTRY_USA);
        ret = service.createJurisdiction(uc, dto);
        assert (ret == null);
        //Empty parameter
        ret = service.updateJurisdiction(null, null);
        assert (ret == null);
        //Empty dto
        dto = new JurisdictionConfigType();
        ret = service.updateJurisdiction(uc, dto);
        assert (ret == null);
        //Not set all mandatory fields
        dto = new JurisdictionConfigType();
        dto.setCountry(STATUTE_COUNTRY_USA);
        ret = service.updateJurisdiction(uc, dto);
        assert (ret == null);

    }

    @Test
    public void createJurisdictionPositive() {

        JurisdictionConfigType dto = new JurisdictionConfigType();
        dto.setCountry(STATUTE_COUNTRY_CAN);
        dto.setStateProvince(STATUTE_PROVINCE_BC);
        dto.setCity("Vancouver");
        dto.setCounty(STATUTE_COUNTY_1);
        dto.setDescription("Test createJurisdictionPositive");

        JurisdictionConfigType ret = service.createJurisdiction(uc, dto);

        assert (ret != null);
        verifyJurisdictionConfigType(ret, dto);

    }

    @Test
    public void createMultiJurisdiction() {

        createJurisdiction("CAN", "AB", "AB City", "1");
        createJurisdiction("CAN", "BC", "BC City", "1");
        createJurisdiction("CAN", "MB", "MB City", "1");
        createJurisdiction("CAN", "NB", "NB City", "1");
        createJurisdiction("CAN", "NL", "NL City", "1");
        createJurisdiction("CAN", "NS", "NS City", "1");
        createJurisdiction("CAN", "NU", "NU City", "1");
        createJurisdiction("CAN", "ON", "ON City", "1");
        createJurisdiction("CAN", "QC", "QC City", "1");
        createJurisdiction("CAN", "SK", "SK City", "1");
        createJurisdiction("CAN", "YT", "YT City", "1");
    }

    @Test
    public void updateJurisdictionPositive() {

        //Create record first
        Long jurisdictionId = createJurisdiction("USA", "OH", "City", "13");

        JurisdictionConfigType dto = new JurisdictionConfigType();
        dto.setJurisdictionId(jurisdictionId);
        dto.setCountry(STATUTE_COUNTRY_USA);
        dto.setStateProvince(STATUTE_PROVINCE_OR);
        dto.setCity("Vancouver");
        dto.setCounty(STATUTE_COUNTY_13);
        dto.setDescription("Test updateJurisdictionPositive");

        JurisdictionConfigType ret = service.updateJurisdiction(uc, dto);

        assert (ret != null);

        verifyJurisdictionConfigType(ret, dto);

    }

    @Test
    public void deleteJurisdiction() {

        Long jurisdictionId = createJurisdiction("USA", "MO", "Vancouver", "13");

        Long ret = service.deleteJurisdiction(uc, jurisdictionId);
        assert (ret.equals(ReturnCode.Success.returnCode()));

    }

    @Test
    public void searchJurisdiction() {

        String orderStr = "country.ASC;stateProvince.DESC";
        JurisdictionConfigSearchType search = new JurisdictionConfigSearchType();
        search.setCountry(STATUTE_COUNTRY_CAN);
        //search.setStateProvince(STATUTE_PROVINCE_OR);
        //search.setCounty(STATUTE_COUNTY_13);
        //search.setCity("Vanco*");
        //search.setDescription("est*");
        JurisdictionConfigsReturnType ret = service.searchJurisdiction(uc, search, 0l, 8l, orderStr);
        assert (ret != null);

        ret = service.searchJurisdiction(uc, search, 8l, 8l, orderStr);
        assert (ret != null);

    }

    //@Test
    public void createUpdateStatuteNegative() {

        //Empty parameter
        StatuteConfigType ret = service.createStatute(null, null);
        assert (ret == null);

        //Empty DTO
        StatuteConfigType dto = new StatuteConfigType();
        ret = service.createStatute(uc, dto);
        assert (ret == null);

        //Not set all mandatory fields
        dto = new StatuteConfigType();
        dto.setStatuteCode(STATUTE_CODE_CC);
        ret = service.createStatute(uc, dto);
        assert (ret == null);

        //Empty parameter
        ret = service.updateStatute(null, null);
        assert (ret == null);

        //Empty DTO
        dto = new StatuteConfigType();
        ret = service.updateStatute(uc, dto);
        assert (ret == null);

        //Not set all mandatory fields
        dto = new StatuteConfigType();
        dto.setStatuteCode(STATUTE_CODE_CC);
        ret = service.updateStatute(uc, dto);

        assert (ret == null);

    }

    @Test
    public void createStatutePositive() {

        Date currentDate = new Date();

        Long jurisdictionId = createJurisdiction("CAN", "AB", "AB City", "1");

        StatuteConfigType dto = new StatuteConfigType();
        dto.setStatuteCode(STATUTE_CODE_CS);
        dto.setActive(true);
        dto.setDefaultJurisdiction(true);
        dto.setEnactmentDate(currentDate);
        dto.setRepealDate(currentDate);
        dto.setOutOfJurisdiction(false);
        dto.setJurisdictionId(jurisdictionId);
        dto.setStatuteSection("StatuteSection createStatutePositive");
        dto.setDescription("Description createStatutePositive");

        StatuteConfigType ret = service.createStatute(uc, dto);

        assert (ret != null);

        verifyStatuteConfigType(ret, dto);

    }

    @Test
    public void updateStatutePositive() {

        StatuteConfigType dto = createStatute("CC");

        Date currentDate = new Date();
        Long jurisdictionId = createJurisdiction("CAN", "ON", "ON City", "1");

        dto.setStatuteCode(STATUTE_CODE_CS);
        dto.setActive(true);
        dto.setDefaultJurisdiction(true);
        dto.setEnactmentDate(currentDate);
        dto.setRepealDate(currentDate);
        dto.setOutOfJurisdiction(false);
        dto.setJurisdictionId(jurisdictionId);
        dto.setStatuteSection("StatuteSection updateStatutePositive");
        dto.setDescription("Description updateStatutePositive");

        StatuteConfigType ret = service.updateStatute(uc, dto);

        assert (ret != null);

        verifyStatuteConfigType(ret, dto);

    }

    @Test
    public void deleteStatute() {
		/*
		Long ret = service.deleteStatute(null, null);
		assert(ret.equals(ReturnCode.CInvalidInput2002.returnCode()));
		*/
        StatuteConfigType dto = createStatute("CC");
        Long statuteId = dto.getStatuteId();

        Long ret = service.deleteStatute(uc, statuteId);
        assert (ret.equals(ReturnCode.Success.returnCode()));

    }

    @Test
    public void searchStatute() {

        StatuteConfigSearchType search = new StatuteConfigSearchType();
        search.setStatuteCode(STATUTE_CODE_CS);
        search.setEnactmentDateFrom(getPastDate(1));
        search.setEnactmentDateTo(getNextDate(1));
        search.setRepealDateFrom(getPastDate(1));
        search.setRepealDateTo(getNextDate(1));
        search.setSection("Section*");
        search.setDefaultJurisdiction(true);
        search.setOutOfJurisdiction(false);
        search.setDescription("Desc*");
        search.setActive(false);

        StatuteConfigsReturnType ret = service.searchStatute(uc, search, null, null, null);
        assert (ret != null);

    }

    //@Test
    public void createUpdateStatuteChargeNegative() {

        //Empty parameter
        StatuteChargeConfigType ret = service.createStatuteCharge(uc, null);
        assert (ret.equals(ReturnCode.CInvalidInput2002.returnCode()));
        assert (ret == null);

        //Empty DTO
        StatuteChargeConfigType dto = new StatuteChargeConfigType();
        ret = service.createStatuteCharge(uc, dto);
        assert (ret.equals(ReturnCode.CInvalidInput2002.returnCode()));
        assert (ret == null);

        //Not set all mandatory fields
        dto = new StatuteChargeConfigType();
        dto.setChargeCategory(CHARGE_CATEGORY_T);
        ret = service.createStatuteCharge(uc, dto);
        assert (ret.equals(ReturnCode.CInvalidInput2002.returnCode()));
        assert (ret == null);

        //Empty parameter
        ret = service.updateStatuteCharge(uc, null);
        assert (ret.equals(ReturnCode.CInvalidInput2002.returnCode()));
        assert (ret == null);

        //Empty DTO
        dto = new StatuteChargeConfigType();
        ret = service.updateStatuteCharge(uc, dto);
        assert (ret.equals(ReturnCode.CInvalidInput2002.returnCode()));
        assert (ret == null);

        //Not set all mandatory fields
        dto = new StatuteChargeConfigType();
        dto.setChargeCategory(CHARGE_CATEGORY_T);
        ret = service.updateStatuteCharge(uc, dto);
        assert (ret.equals(ReturnCode.CInvalidInput2002.returnCode()));
        assert (ret == null);

    }

    @Test
    public void createStatuteChargePositive() {

        StatuteChargeConfigType dto = new StatuteChargeConfigType();

        //Set mandatory fields
        dto.setStatuteId(1L);
        dto.setChargeCode(CHARGE_CODE_1510);
        dto.setChargeText(getStatuteChargeTextType());
        dto.setBailAllowed(true);
        dto.setBondAllowed(false);

        dto.setStartDate(createPastDate(10));
        dto.setEndDate(createFutureDate(1000));
        dto.setChargeType(CHARGE_TYPE_F);

        //Set optional fields
        dto.setBailAmount(10000L);

        Set<Long> externalChargeCodeIds = new HashSet<Long>();
        externalChargeCodeIds.add(1L);
        externalChargeCodeIds.add(2L);
        dto.setExternalChargeCodeIds(externalChargeCodeIds);

        dto.setChargeDegree(CHARGE_DEGREE_I);
        dto.setChargeCategory(CHARGE_CATEGORY_T);
        dto.setChargeSeverity(CHARGE_SEVERITY_H);

        Set<ChargeIndicatorType> chargeIndicators = new HashSet<ChargeIndicatorType>();
        chargeIndicators.add(getChargeIndicatorType());
        dto.setChargeIndicators(chargeIndicators);

        dto.setEnhancingFactors(getChargeEnhancingFactors());
        dto.setReducingFactors(getChargeReducingFactors());

        StatuteChargeConfigType ret = service.createStatuteCharge(uc, dto);

        assert (ret != null);

        verifyStatuteChargeConfigType(ret, dto);
        statuteChargeConfigId = ret.getStatuteChargeId();

        //Test to create second recored with same charge code and text
        //Set mandatory fields
        dto.setStatuteId(1L);
        dto.setChargeCode(CHARGE_CODE_1510);
        dto.setChargeText(getStatuteChargeTextType());
        dto.setBailAllowed(true);
        dto.setBondAllowed(false);

        dto.setStartDate(createPastDate(10));
        dto.setEndDate(createFutureDate(1000));
        dto.setChargeType(CHARGE_TYPE_F);

        dto.setChargeDegree(CHARGE_DEGREE_II);

        ret = service.createStatuteCharge(uc, dto);
        assert (ret != null);
        verifyStatuteChargeConfigType(ret, dto);
    }

    @Test(dependsOnMethods = "createStatuteChargePositive")
    public void testRetrieveChargeCodeDescprition() {

        //Test retrieveChargeCodeDescriptions
        List<ChargeCodeDescriptionType> rtn = service.retrieveChargeCodeDescriptions(uc, 1L, createPastDate(10));

        assert (rtn != null);
        assert (rtn.size() == 1);

        //Test retrieveChargeCodeDescriptions
        ChargeCodeDescriptionType chargeCodeDescriptionType = rtn.get(0);

        List<StatuteChargeConfigType> rtn2 = service.retrieveStatuteChargeConfigsByCodeDescription(uc, chargeCodeDescriptionType);

        assert (rtn2 != null);
        assert (rtn2.size() == 2);
    }

    @Test
    public void updateStatuteChargePositive() {

        StatuteChargeConfigType dto = createStatuteCharge();

        //Set mandatory fields
        dto.setStatuteId(2L);
        dto.setChargeCode(CHARGE_CODE_1510);
        dto.setChargeText(getStatuteChargeTextType());
        dto.setBailAllowed(true);
        dto.setBondAllowed(false);

        dto.setStartDate(createPastDate(20));
        dto.setEndDate(createFutureDate(1000));
        dto.setChargeType(CHARGE_TYPE_F);

        //Set optional fields
        dto.setBailAmount(10000L);

        Set<Long> externalChargeCodeIds = new HashSet<Long>();
        externalChargeCodeIds.add(1L);
        externalChargeCodeIds.add(2L);
        dto.setExternalChargeCodeIds(externalChargeCodeIds);

        dto.setChargeDegree(CHARGE_DEGREE_II);
        dto.setChargeCategory(CHARGE_CATEGORY_A);
        dto.setChargeSeverity(CHARGE_SEVERITY_L);

        Set<ChargeIndicatorType> chargeIndicators = new HashSet<ChargeIndicatorType>();
        chargeIndicators.add(getChargeIndicatorType());
        dto.setChargeIndicators(chargeIndicators);

        dto.setEnhancingFactors(getChargeEnhancingFactors());
        dto.setReducingFactors(getChargeReducingFactors());

        StatuteChargeConfigType ret = service.updateStatuteCharge(uc, dto);

        assert (ret != null);

        verifyStatuteChargeConfigType(ret, dto);
        statuteChargeConfigId = ret.getStatuteChargeId();
    }

    @Test(enabled = false)
    public void deteleStatuteCharge() {
		
		/*
		Long ret = service.deleteStatuteCharge(null, null);
		assert(ret.equals(ReturnCode.CInvalidInput2002.returnCode()));
		*/
        StatuteChargeConfigType dto = createStatuteCharge();
        Long statuteChargeId = dto.getStatuteChargeId();

        Long ret = service.deleteStatuteCharge(uc, statuteChargeId);
        assert (ret.equals(ReturnCode.Success.returnCode()));

    }

    @Test(dependsOnMethods = { "testRetrieveChargeCodeDescprition" })
    public void searchStatuteCharge() {

        StatuteChargeConfigSearchType search = new StatuteChargeConfigSearchType();

        search.setStatuteId(1L);
        //search.setChargeCode(CHARGE_CODE_1510);
        //search.setChargeType(CHARGE_TYPE_F);
        //search.setChargeCategory(CHARGE_CATEGORY_A);
        //search.setChargeDegree(CHARGE_DEGREE_II);
        //search.setChargeSeverity(CHARGE_SEVERITY_H);
        //search.setStartDateFrom(createPastDate(11));

        StatuteChargeConfigsReturnType ret = service.searchStatuteCharge(uc, search, 0l, 3l, null);
        assert (ret != null);
        assertEquals(ret.getTotalSize().intValue(), 2);
        assertNotNull(ret.getStatuteChargeConfigs());
    }

    //@Test
    public void createChargeNegative() {

        //Empty parameter
        ChargeType ret = service.createCharge(null, null);
        assert (ret == null);

        //Empty DTO
        ChargeType dto = new ChargeType();
        ret = service.createCharge(uc, dto);
        assert (ret == null);

        //Not set all mandatory fields
        dto = new ChargeType();
        dto.setCaseInfoId(1L);
        ret = service.createCharge(uc, dto);
        assert (ret == null);

    }

    @Test(dependsOnMethods = { "createStatuteChargePositive" })
    public void createSignleChargePositive() {

        //Create single charge instance
        ChargeType dto = getChargeType(1);

        ChargeType ret = service.createCharge(uc, dto);
        assert (ret != null);

        // Test new added display fields.
        Assert.assertTrue(ret.getChargeCode() != null);
        Assert.assertTrue(ret.getChargeDegree() != null);
        Assert.assertTrue(ret.getChargeDescription() != null);
        Assert.assertTrue(ret.getChargeSeverity() != null);
        Assert.assertTrue(ret.getChargeType() != null);

        ret = service.getCharge(uc, ret.getChargeId());

        //Test disposition history is created correctly.
        Assert.assertTrue(ret.getChargeDisposition() != null);
        ChargeDispositionType disposition = ret.getChargeDisposition();
        Assert.assertTrue(ret.getDispositionHistories() != null);
        Assert.assertEquals(ret.getDispositionHistories().size(), 1);
        ChargeDispositionType dispositionHistory = ret.getDispositionHistories().get(0);
        Assert.assertEquals(dispositionHistory.getChargeStatus(), disposition.getChargeStatus());
        Assert.assertEquals(dispositionHistory.getDispositionOutcome(), disposition.getDispositionOutcome());
        Assert.assertEquals(dispositionHistory.getDispositionDate(), disposition.getDispositionDate());
    }

    @Test(dependsOnMethods = { "createSignleChargePositive" })
    public void testOJCharge() {
        //Create OJ charge
        ChargeType dto = getChargeType(1);

        dto.setOJOrderId(1L);
        dto.setCaseInfoId(null);
        dto.setOJChargeCode("OJ charge code");
        dto.setOJChargeDescription("OJ charge description");
        dto.setOJStatuteIdName("OJ statue id and name");
        dto.setChargeJurisdiction("OJ");
        dto.setStatuteChargeId(null);

        ChargeType ret = service.createCharge(uc, dto);
        assert (ret != null);
        Assert.assertTrue(ret.getOJChargeCode().equals(dto.getOJChargeCode()));
        Assert.assertTrue(ret.getOJChargeDescription().equals(dto.getOJChargeDescription()));
        Assert.assertTrue(ret.getOJOrderId().equals(dto.getOJOrderId()));
        Assert.assertTrue(ret.getOJStatuteIdName().equals(dto.getOJStatuteIdName()));
        Assert.assertTrue(ret.getCaseInfoId() == null);
        Assert.assertTrue(ret.getStatuteChargeId() == null);
        Assert.assertTrue(ret.getChargeJurisdiction().equals(dto.getChargeJurisdiction()));

        //Get OJ charge
        Long chargeId = ret.getChargeId();
        ret = service.getCharge(uc, chargeId);
        assert (ret != null);
        Assert.assertTrue(ret.getOJChargeCode().equals(dto.getOJChargeCode()));
        Assert.assertTrue(ret.getOJChargeDescription().equals(dto.getOJChargeDescription()));
        Assert.assertTrue(ret.getOJOrderId().equals(dto.getOJOrderId()));
        Assert.assertTrue(ret.getOJStatuteIdName().equals(dto.getOJStatuteIdName()));
        Assert.assertTrue(ret.getCaseInfoId() == null);
        Assert.assertTrue(ret.getStatuteChargeId() == null);
        Assert.assertTrue(ret.getChargeJurisdiction().equals(dto.getChargeJurisdiction()));

        //Get OJ charge by OJ-order id.
        List<ChargeType> list = service.getChargesByOJOrderId(uc, 1L);
        assert (list != null);
        assert (list.size() == 1);

        list = service.getChargesByOJOrderId(uc, 2L);
        assert (list != null);
        assert (list.size() == 0);

        //Update OJ charge
        dto = ret;
        dto.setOJOrderId(1L);
        dto.setCaseInfoId(null);
        dto.setOJChargeCode("OJ charge code new");
        dto.setOJChargeDescription("OJ charge description new");
        dto.setOJStatuteIdName("OJ statue id and name new");
        dto.setChargeJurisdiction("OJ");
        dto.setStatuteChargeId(null);

        ret = service.updateCharge(uc, dto);
        assert (ret != null);
        Assert.assertTrue(ret.getOJChargeCode().equals(dto.getOJChargeCode()));
        Assert.assertTrue(ret.getOJChargeDescription().equals(dto.getOJChargeDescription()));
        Assert.assertTrue(ret.getOJOrderId().equals(dto.getOJOrderId()));
        Assert.assertTrue(ret.getOJStatuteIdName().equals(dto.getOJStatuteIdName()));
        Assert.assertTrue(ret.getCaseInfoId() == null);
        Assert.assertTrue(ret.getStatuteChargeId() == null);
        Assert.assertTrue(ret.getChargeJurisdiction().equals(dto.getChargeJurisdiction()));

        //Delete OJ charge
        Long deleteRtn = service.deleteCharge(uc, chargeId);
        assert (deleteRtn.equals(new Long(1l)));
        try {
            ret = service.getCharge(uc, chargeId);
        } catch (Exception e) {
            assert (e instanceof DataNotExistException);
        }
    }

    @Test(dependsOnMethods = { "createStatuteChargePositive" })
    public void deleteSignleChargePositive() {
        createOrderConfiguration();
        //Create single charge instance
        ChargeType dto = getChargeType(1);

        ChargeType ret = service.createCharge(uc, dto);
        assert (ret != null);

        // Test new added display fields.
        Assert.assertTrue(ret.getChargeCode() != null);
        Assert.assertTrue(ret.getChargeDegree() != null);
        Assert.assertTrue(ret.getChargeDescription() != null);
        Assert.assertTrue(ret.getChargeSeverity() != null);
        Assert.assertTrue(ret.getChargeType() != null);

        ret = service.getCharge(uc, ret.getChargeId());

        //Test disposition history is created correctly.
        Assert.assertTrue(ret.getChargeDisposition() != null);
        ChargeDispositionType disposition = ret.getChargeDisposition();
        Assert.assertTrue(ret.getDispositionHistories() != null);
        Assert.assertEquals(ret.getDispositionHistories().size(), 1);
        ChargeDispositionType dispositionHistory = ret.getDispositionHistories().get(0);
        Assert.assertEquals(dispositionHistory.getChargeStatus(), disposition.getChargeStatus());
        Assert.assertEquals(dispositionHistory.getDispositionOutcome(), disposition.getDispositionOutcome());
        Assert.assertEquals(dispositionHistory.getDispositionDate(), disposition.getDispositionDate());

        //Delete test
        Long id = ret.getChargeId();
        Long deleteRtn = service.deleteCharge(uc, id);

        assertEquals(deleteRtn, ReturnCode.Success.returnCode());

        //can not get deleted one.
        try {
            service.getCharge(uc, id);
        } catch (Exception e) {
            Assert.assertTrue(e instanceof DataNotExistException);
        }

        //Can not delete charge when sentence exist
        ChargeType charge2 = getChargeType(1);
        ChargeType ret1 = service.createCharge(uc, charge2);
        assert (ret1 != null);
        Long chargeId = ret1.getChargeId();
        Long sentenceId = createSentence(chargeId);

        charge2 = service.getCharge(uc, chargeId);
        assertEquals(charge2.getSentenceId(), sentenceId);

        try {
            Long rtn = service.deleteCharge(uc, chargeId);
            Assert.assertNotEquals(rtn, ReturnCode.Success.returnCode());
        } catch (Exception e) {
            Assert.assertTrue(e instanceof DataExistException);
        }

        //Can delete charge after sentence is deleted. WOR-6030 testing
        Long rtn = service.deleteOrder(uc, sentenceId);
        Assert.assertEquals(rtn, ReturnCode.Success.returnCode());

        rtn = service.deleteCharge(uc, chargeId);
        Assert.assertEquals(rtn, ReturnCode.Success.returnCode());
        //can not get the deleted one.
        try {
            service.getCharge(uc, chargeId);
        } catch (Exception e) {
            Assert.assertTrue(e instanceof DataNotExistException);
        }

        //Negative test: case has bail. WOR-5324
        Long caseInfoId = caseInfoIds.get(0);
        createBail(caseInfoId);

        ChargeType charge3 = getChargeType(1);

        ret1 = service.createCharge(uc, charge3);
        assert (ret1 != null);
        //can not delete a charge when the case has active bail.
        try {
            rtn = service.deleteCharge(uc, ret1.getChargeId());
            Assert.assertNotEquals(rtn, ReturnCode.Success.returnCode());
        } catch (Exception e) {
            Assert.assertTrue(e instanceof DataExistException);
        }

        //POSITIVE: delete inactive charge.
        ret1.getChargeDisposition().setChargeStatus("INACTIVE");
        ret1 = service.updateCharge(uc, ret1);
        assert (ret1 != null);
        deleteRtn = service.deleteCharge(uc, ret1.getChargeId());

        assertEquals(deleteRtn, ReturnCode.Success.returnCode());

        //can not get current one.
        try {
            service.getCharge(uc, id);
        } catch (Exception e) {
            Assert.assertTrue(e instanceof DataNotExistException);
        }

    }

    @Test
    public void updateChargePositive() {

        //Create single charge instancecharge
        ChargeType dto = getChargeType(1);

        ChargeType ret = service.createCharge(uc, dto);
        assert (ret != null);

        //Update offence date
        Date newDate = new Date();
        ret.setOffenceStartDate(newDate);

        //update statute charge id
        StatuteChargeConfigSearchType search = new StatuteChargeConfigSearchType();
        search.setChargeCode(CHARGE_CODE_1510);
        StatuteChargeConfigsReturnType rtn1 = service.searchStatuteCharge(uc, search, null, null, null);

        Long newStatuteChargeId = rtn1.getStatuteChargeConfigs().get(0).getStatuteChargeId();

        ret.setStatuteChargeId(newStatuteChargeId);

        //update primary flag
        ret.setPrimary(false);

        //update comment
        CommentType newComment = new CommentType();
        newComment.setComment("new");
        newComment.setCommentDate(newDate);
        newComment.setUserId("devtest");
        ret.setComment(newComment);

        //update outcome
        ret.getChargeDisposition().setDispositionOutcome(CHARGE_OUTCOME_RELBAIL);

        ChargeType rtn = service.updateCharge(uc, ret);
        assert (rtn != null);

        //Verify primary
        Assert.assertEquals(rtn.isPrimary(), Boolean.FALSE);

        //Verify statute charge id
        Assert.assertEquals(rtn.getStatuteChargeId(), newStatuteChargeId);

        //Verify comment
        Assert.assertEquals(rtn.getComment().getComment(), newComment.getComment());
        Assert.assertEquals(rtn.getComment().getUserId(), newComment.getUserId());
        Assert.assertEquals(rtn.getComment().getCommentDate(), newComment.getCommentDate());

        //Verify outcome
        Assert.assertEquals(rtn.getChargeDisposition().getDispositionOutcome(), CHARGE_OUTCOME_RELBAIL);

        ret = service.getCharge(uc, ret.getChargeId());

        //Verify disposition histories
        Assert.assertEquals(ret.getDispositionHistories().size(), 2);
        List<ChargeDispositionType> dispositionHistories = ret.getDispositionHistories();
        Assert.assertEquals(dispositionHistories.get(0).getDispositionOutcome(), CHARGE_OUTCOME_RELBAIL);
        Assert.assertEquals(dispositionHistories.get(1).getDispositionOutcome(), CHARGE_OUTCOME_SENT);

        //Verify offense date
        Assert.assertEquals(rtn.getOffenceStartDate(), newDate);

        //verify WOR-6624
        ret.getChargeDisposition().setDispositionOutcome(null);
        ret = service.updateCharge(uc, ret);

        assert (ret != null);
        assert (ret.getChargeDisposition().getDispositionOutcome() == null);

        newComment = new CommentType();
        newComment.setComment("new new");
        newComment.setCommentDate(newDate);
        newComment.setUserId("devtest");
        ret.setComment(newComment);

        ret = service.updateCharge(uc, ret);
        assert (ret != null);
        assert (ret.getComment().getComment().equals("new new"));
        assert (ret.getChargeDisposition().getDispositionOutcome() == null);

    }

    @Test(dependsOnMethods = { "createSignleChargePositive" })
    public void createMultipleChargePositive() {

        //Create single charge instance
        ChargeType dto = getChargeType(3);

        List<ChargeType> ret = service.createCharges(uc, dto);

        assert (ret.size() == 3);

    }

    @Test(dependsOnMethods = { "createMultipleChargePositive" })
    public void getChargesByCaseInfoId() {

        List<ChargeType> charges = service.getChargesByCaseInfo(uc, caseInfoIds.get(0));
        assertNotNull(charges);
        for (ChargeType charge : charges) {
            ChargeType chg = service.getCharge(uc, charge.getChargeId());
            assertNotNull(chg);

        }
    }

    @Test
    public void testUpdateChargesStatus() {
        ChargesStatusType chargesStatusType = new ChargesStatusType();

        Date outcomeDate = new Date();

        List<Long> chargeIds = new ArrayList<Long>();

        for (int i = 0; i < 10; i++) {
            ChargeType charge = getChargeType(1);
            charge = service.createCharge(uc, charge);
            chargeIds.add(charge.getChargeId());
        }

        chargesStatusType.setSelectedActiveChargeIds(chargeIds);
        chargesStatusType.setChargeOutcome(CHARGE_OUTCOME_RELBAIL);
        chargesStatusType.setChargeStatus(CHARGE_STATUS_ACTIVE);
        chargesStatusType.setChargeOutcomeDate(outcomeDate);

        String comment = "update charge outcome to relbail";
        chargesStatusType.setComment(comment);

        service.updateChargesStatus(uc, chargesStatusType);

        //Verify update results.
        for (Long id : chargeIds) {
            ChargeType charge = service.getCharge(uc, id);
            Assert.assertEquals(charge.getComment().getComment(), comment);
            ChargeDispositionType disposition = charge.getChargeDisposition();
            Assert.assertEquals(disposition.getDispositionOutcome(), CHARGE_OUTCOME_RELBAIL);
            Assert.assertEquals(disposition.getChargeStatus(), CHARGE_STATUS_ACTIVE);
            Assert.assertEquals(disposition.getDispositionDate(), outcomeDate);

            //Verify disposition histories
            Assert.assertEquals(charge.getDispositionHistories().size(), 2);
            List<ChargeDispositionType> dispositionHistories = charge.getDispositionHistories();
            Assert.assertEquals(dispositionHistories.get(0).getDispositionOutcome(), CHARGE_OUTCOME_RELBAIL);
            Assert.assertEquals(dispositionHistories.get(1).getDispositionOutcome(), CHARGE_OUTCOME_SENT);
        }

        //Negative
        //DTO validation
        chargesStatusType = new ChargesStatusType();
        chargesStatusType.setComment(comment);
        try {
            service.updateChargesStatus(uc, chargesStatusType);
        } catch (Exception e) {
            Assert.assertTrue(e instanceof InvalidInputException);
        }

        //MetaData validation
        chargesStatusType = new ChargesStatusType();
        chargesStatusType.setSelectedActiveChargeIds(chargeIds);
        chargesStatusType.setChargeOutcome(CHARGE_OUTCOME_RELBAIL);
        chargesStatusType.setChargeStatus("Open");
        chargesStatusType.setChargeOutcomeDate(outcomeDate);
        try {
            service.updateChargesStatus(uc, chargesStatusType);
        } catch (Exception e) {
            Assert.assertTrue(e instanceof DataNotExistException);
        }
    }

    /////////////////////////////////private methods////////////////////////////////////////

    private Long createExternalChargeCode(String category, String chargeCode, String codeGroup) {

        Long externalChargeCodeId = null;

        String desc = "Test Create" + category + "-" + chargeCode + "-" + codeGroup;
        ExternalChargeCodeConfigType dto = new ExternalChargeCodeConfigType();
        dto.setCodeCategory(category);
        dto.setChargeCode(chargeCode);
        dto.setCodeGroup(codeGroup);
        dto.setDescription(desc);
        ExternalChargeCodeConfigType ret = service.createExternalChargeCode(uc, dto);

        assert (ret != null);

        externalChargeCodeId = ret.getChargeCodeId();

        return externalChargeCodeId;
    }

    private Long createJurisdiction(String country, String province, String city, String county) {

        Long jurisdictionId = null;

        String desc = "Test Create-" + country + "-" + province + "-" + city + "-" + county;
        JurisdictionConfigType dto = new JurisdictionConfigType();
        dto.setCountry(country);
        dto.setStateProvince(province);
        dto.setCounty(county);
        dto.setCity(city);
        dto.setDescription(desc);

        JurisdictionConfigType ret = service.createJurisdiction(uc, dto);

        assert (ret != null);

        jurisdictionId = ret.getJurisdictionId();
        return jurisdictionId;

    }

    private StatuteConfigType createStatute(String statuteCode) {

        Date currentDate = new Date();

        Long jurisdictionId = createJurisdiction("CAN", "AB", "AB City", "1");

        StatuteConfigType dto = new StatuteConfigType();
        dto.setStatuteCode(statuteCode);
        dto.setActive(true);
        dto.setDefaultJurisdiction(true);
        dto.setEnactmentDate(currentDate);
        dto.setRepealDate(currentDate);
        dto.setOutOfJurisdiction(false);
        dto.setJurisdictionId(jurisdictionId);
        dto.setStatuteSection("StatuteSection createStatutePositive");
        dto.setDescription("Description createStatutePositive");

        StatuteConfigType ret = service.createStatute(uc, dto);
        assert (ret != null);

        return ret;

    }

    private StatuteChargeConfigType createStatuteCharge() {
        StatuteChargeConfigType dto = new StatuteChargeConfigType();

        //Set mandatory fields
        dto.setStatuteId(1L);
        dto.setChargeCode(CHARGE_CODE_1510);
        dto.setChargeText(getStatuteChargeTextType());
        dto.setBailAllowed(true);
        dto.setBondAllowed(false);

        dto.setStartDate(createPastDate(10));
        dto.setEndDate(createFutureDate(1000));
        dto.setChargeType(CHARGE_TYPE_F);

        //Set optional fields
        dto.setBailAmount(10000L);

        Set<Long> externalChargeCodeIds = new HashSet<Long>();
        externalChargeCodeIds.add(1L);
        externalChargeCodeIds.add(2L);
        dto.setExternalChargeCodeIds(externalChargeCodeIds);

        dto.setChargeDegree(CHARGE_DEGREE_I);
        dto.setChargeCategory(CHARGE_CATEGORY_T);
        dto.setChargeSeverity(CHARGE_SEVERITY_H);

        Set<ChargeIndicatorType> chargeIndicators = new HashSet<ChargeIndicatorType>();
        chargeIndicators.add(getChargeIndicatorType());
        dto.setChargeIndicators(chargeIndicators);

        dto.setEnhancingFactors(getChargeEnhancingFactors());
        dto.setReducingFactors(getChargeReducingFactors());

        StatuteChargeConfigType ret = service.createStatuteCharge(uc, dto);
        assert (ret != null);

        verifyStatuteChargeConfigType(ret, dto);

        return ret;
    }

    private ChargeType getChargeType(long chargeCount) {
        createCaseInfo();

        ChargeType dto = new ChargeType();

        //get StatuteChargeConfigs
        StatuteChargeConfigSearchType search = new StatuteChargeConfigSearchType();
        search.setChargeCode(CHARGE_CODE_1510);
        StatuteChargeConfigsReturnType rtn = service.searchStatuteCharge(uc, search, null, null, null);

        //Mandatory properties
        dto.setStatuteChargeId(rtn.getStatuteChargeConfigs().get(0).getStatuteChargeId());

        Date now = new Date();
        ChargeDispositionType chargeDisposition = new ChargeDispositionType(CHARGE_OUTCOME_SENT, CHARGE_STATUS_ACTIVE, now);
        dto.setChargeDisposition(chargeDisposition);
        dto.setPrimary(true);
        Long caseInfoId = caseInfoIds.get(0);
        dto.setCaseInfoId(caseInfoId);

        //Optional properties
        Set<ChargeIdentifierType> chargeIdentifiers = new HashSet<ChargeIdentifierType>();
        chargeIdentifiers.add(getChargeIdentifierType());
        dto.setChargeIdentifiers(chargeIdentifiers);

        Set<ChargeIndicatorType> chargeIndicators = new HashSet<ChargeIndicatorType>();
        chargeIndicators.add(getChargeIndicatorType());
        dto.setChargeIndicators(chargeIndicators);

        dto.setLegalText("Legal Text");
        dto.setChargeCount(chargeCount);
        dto.setFacilityId(1L);
        dto.setOrganizationId(1L);
        dto.setPersonIdentityId(1L);
        dto.setOffenceStartDate(now);
        dto.setOffenceEndDate(createFutureDate(10));
        dto.setFilingDate(now);

        ChargePleaType chargePlea = new ChargePleaType(CHARGE_PLEA_G, "CHARGE_PLEA_G creation");
        dto.setChargePlea(chargePlea);

        ChargeAppealType chargeAppeal = new ChargeAppealType(APPEAL_STATUS_INPROGRESS, "APPEAL_STATUS_INPROGRESS creation");
        dto.setChargeAppeal(chargeAppeal);

        // dto.setConditionIds(getModuleIdSet());
        // dto.setOrderSentenceIds(getModuleIdSet());

        CommentType comment = new CommentType();
        comment.setComment("Create Charge");
        comment.setCommentDate(now);
        comment.setUserId("2");
        dto.setComment(comment);

        return dto;
    }

    private StatuteChargeTextType getStatuteChargeTextType() {

        StatuteChargeTextType ret = new StatuteChargeTextType(CHARGE_LANGUAGE_EN, "StatuteChargeTextType desc", "StatuteChargeTextType legal text");

        return ret;
    }

    private ChargeIdentifierType getChargeIdentifierType() {

        ChargeIdentifierType ret = new ChargeIdentifierType(null, CHARGE_IDENTIFIER_CJIS, "1-23456", CHARGE_IDENTIFIER_FORMAT_0_00000, 1L, 1L,
                "Comment getChargeIdentifierType");

        return ret;

    }

    private ChargeIndicatorType getChargeIndicatorType() {

        ChargeIndicatorType ret = new ChargeIndicatorType(null, CHARGE_INDICATOR_ACC, true, "1-23456");

        return ret;

    }

    private Set<String> getChargeEnhancingFactors() {

        Set<String> ret = new HashSet<String>();
        ret.add(CHARGE_ENHANCING_FACTOR_RACE);
        ret.add(CHARGE_ENHANCING_FACTOR_SEX);
        return ret;

    }

    private Set<String> getChargeReducingFactors() {

        Set<String> ret = new HashSet<String>();
        ret.add(CHARGE_REDUCING_FACTOR_MI);
        ret.add(CHARGE_REDUCING_FACTOR_MIN);
        return ret;
    }

    private Date createPastDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, -day);
        Date pastDate = cal.getTime();
        return pastDate;
    }

    private Date createFutureDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, day);
        Date futureDate = cal.getTime();
        return futureDate;
    }

    private void verifyExternalChargeCodeConfigType(ExternalChargeCodeConfigType actual, ExternalChargeCodeConfigType expected) {

        assertNotNull(actual.getChargeCodeId());
        assertEquals(actual.getChargeCode(), expected.getChargeCode());
        assertEquals(actual.getCodeCategory(), expected.getCodeCategory());
        assertEquals(actual.getCodeGroup(), expected.getCodeGroup());
        assertEquals(actual.getDescription(), expected.getDescription());

    }

    private void verifyJurisdictionConfigType(JurisdictionConfigType actual, JurisdictionConfigType expected) {

        assertNotNull(actual.getJurisdictionId());
        assertEquals(actual.getCountry(), expected.getCountry());
        assertEquals(actual.getStateProvince(), expected.getStateProvince());
        assertEquals(actual.getCity(), expected.getCity());
        assertEquals(actual.getCounty(), expected.getCounty());
        assertEquals(actual.getDescription(), expected.getDescription());
    }

    private void verifyStatuteConfigType(StatuteConfigType actual, StatuteConfigType expected) {

        assertNotNull(actual.getStatuteId());
        assertEquals(actual.getStatuteCode(), expected.getStatuteCode());
        assertEquals(actual.getDescription(), expected.getDescription());
        assertEquals(actual.getEnactmentDate(), expected.getEnactmentDate());
        assertEquals(actual.getRepealDate(), expected.getRepealDate());
        assertEquals(actual.getStatuteSection(), expected.getStatuteSection());
        assertEquals(actual.getJurisdictionId(), expected.getJurisdictionId());
        assert (actual.isActive() != null);
        assertEquals(actual.isDefaultJurisdiction(), expected.isDefaultJurisdiction());
        assertEquals(actual.isOutOfJurisdiction(), expected.isOutOfJurisdiction());

    }

    private void verifyStatuteChargeConfigType(StatuteChargeConfigType actual, StatuteChargeConfigType expected) {

        assertNotNull(actual.getStatuteChargeId());
        assertEquals(actual.getStatuteId(), expected.getStatuteId());
        assertEquals(actual.getChargeCode(), expected.getChargeCode());

        assertEquals(actual.getChargeText(), expected.getChargeText());

        assertEquals(actual.getBailAmount(), expected.getBailAmount());
        assertEquals(actual.isBailAllowed(), expected.isBailAllowed());
        assertEquals(actual.isBondAllowed(), expected.isBondAllowed());
        assertEquals(actual.getExternalChargeCodeIds(), expected.getExternalChargeCodeIds());
        assertEquals(actual.getStartDate().getDate(), expected.getStartDate().getDate());
        assertEquals(actual.getEndDate().getDate(), expected.getEndDate().getDate());
        assertEquals(actual.getChargeType(), expected.getChargeType());
        assertEquals(actual.getChargeDegree(), expected.getChargeDegree());
        assertEquals(actual.getChargeCategory(), expected.getChargeCategory());
        assertEquals(actual.getChargeSeverity(), expected.getChargeSeverity());

        assertEquals(actual.getChargeIndicators(), expected.getChargeIndicators());

        assertEquals(actual.getEnhancingFactors(), expected.getEnhancingFactors());
        assertEquals(actual.getReducingFactors(), expected.getReducingFactors());

    }

    private Date getPastDate(int days) {
        Date current = new Date();
        return BeanHelper.getPastDate(current, days);
    }

    private Date getNextDate(int days) {
        Date current = new Date();
        return BeanHelper.nextNthDays(current, days);
    }

    ////////////////////////////////////////////////////////////////////////
    // Create CaseInfo for Charge
    private void createCaseInfo() {

        // Create only mandatory fields
        CaseInfoType caseInfo = generateDefaultCaseInfo(true);

        CaseInfoType ret = service.createCaseInfo(uc, caseInfo, null);

        ret = service.getCaseInfo(uc, ret.getCaseInfoId());

        createdCaseInfos.add(ret);
        caseInfoIds.add(ret.getCaseInfoId());

    }

    private CaseInfoType generateDefaultCaseInfo(boolean onlyMandatory) {

        // createUpdateSearchCaseIdentifierConfigPositive();

        CaseInfoType caseInfo = new CaseInfoType();
        Date currentDate = new Date();

        //Mandatory fields:
        caseInfo.setCaseCategory(CASE_CATEGORY_IJ);
        caseInfo.setCaseType(CASE_TYPE_ADULT);
        caseInfo.setCreatedDate(currentDate);
        caseInfo.setDiverted(true);
        caseInfo.setDivertedSuccess(true);
        caseInfo.setActive(true);
        caseInfo.setSupervisionId(1L);

        if (onlyMandatory) {
            return caseInfo;
        }

        //Add optional fields:
        caseInfo.setIssuedDate(currentDate);
        caseInfo.setSentenceStatus(SENTENCE_STATUS_SENTENCED);
        caseInfo.setFacilityId(1L);

        CommentType comment = new CommentType();
        comment.setUserId("1");
        comment.setCommentDate(currentDate);
        comment.setComment("createCaseInfoPositive");
        caseInfo.setComment(comment);

        caseInfo.getCaseActivityIds().add(1L);

        //Add case identifiers
        CaseIdentifierType caseIdentifier1 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_DOCKET, "0001A", false, 1L, 1L, "createCaseInfoPositive comment1", true);

        CaseIdentifierType caseIdentifier2 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_INDICTMENT, "0002", false, 1L, 1L, "createCaseInfoPositive comment2",
                false);

        caseInfo.getCaseIdentifiers().add(caseIdentifier1);
        caseInfo.getCaseIdentifiers().add(caseIdentifier2);

        return caseInfo;
    }

    public void createUpdateSearchCaseIdentifierConfigPositive() {

        //Create
        CaseIdentifierConfigType dto = new CaseIdentifierConfigType(null, CASE_IDENTIFIER_TYPE_DOCKET, CASE_IDENTIFIER_FORMAT_000000A, true, true, true, true);

        CaseIdentifierConfigType ret = service.createCaseIdentifierConfig(uc, dto);

        createdConfigs.add(ret);

        dto = new CaseIdentifierConfigType(null, CASE_IDENTIFIER_TYPE_DOCKET, CASE_IDENTIFIER_FORMAT_000000A, false, false, false, false);

        ret = service.createCaseIdentifierConfig(uc, dto);

        createdConfigs.add(ret);
    }

    private Long createSentence(Long chargeId) {
        // Sentence
        assertTrue(service.setSentenceNumberConfiguration(uc, true));
        SentenceType sentenceRet;
        SentenceType sentence = new SentenceType();
        sentence.setOrderClassification(OrdClassification.SENT.name());
        sentence.setOrderType(OrdType.SENTORD.name());
        sentence.setOrderCategory(OrdCategory.IJ.name());
        sentence.setOrderSubType(OrdSubType.DETAINER.name());
        sentence.setOrderNumber("Sentence No.12345");
        sentence.setOrderDisposition(instanceOfDisposition());
        sentence.setOrderIssuanceDate(new Date());
        sentence.setOrderReceivedDate(new Date());
        sentence.setOrderStartDate(new Date());
        sentence.setOrderExpirationDate(new Date());

        sentence.setIsHoldingOrder(true);
        sentence.setIsSchedulingNeeded(false);
        sentence.setHasCharges(true);

        sentence.setCaseInfoIds(new HashSet<Long>(caseInfoIds));
        sentence.setChargeIds(new HashSet<Long>(Arrays.asList(new Long[] { chargeId })));

        sentence.setSentenceType(SentType.FINEDEF.name());
        sentence.setSentenceNumber(12345L);
        TermType term = new TermType();
        term.setTermCategory(TmCategory.CUS.name());
        term.setTermName(TmName.MAX.name());
        term.setTermType(TmType.INT.name());
        term.setTermStatus(TmStatus.EXC.name());
        term.setTermSequenceNo(12345L);
        term.setTermStartDate(new Date());
        term.setTermEndDate(new Date());
        Set<TermType> sentenceTerms = new HashSet<TermType>();
        sentenceTerms.add(term);
        sentence.setSentenceTerms(sentenceTerms);
        sentence.setSentenceStatus(SentStatus.EXC.name());
        sentence.setSentenceStartDate(new Date());
        sentence.setSentenceAppeal(new SentenceAppealType(AppealStatus.ACCEPTED.name(), "Accepted"));
        sentence.setFineAmount(BigDecimal.valueOf(10000.00));
        sentence.setFineAmountPaid(true);
        sentence.setSentenceAggravateFlag(true);
        IntermittentScheduleType sch = new IntermittentScheduleType();
        sch.setDayOfWeek(Day.MON.name());
        sch.setStartTime(sch.new TimeValue(8L, 30L, 0L));
        sch.setEndTime(sch.new TimeValue(16L, 30L, 0L));
        Set<IntermittentScheduleType> intermittentSchedules = new HashSet<IntermittentScheduleType>();
        intermittentSchedules.add(sch);
        sentence.setIntermittentSchedule(intermittentSchedules);

        sentenceRet = service.createSentence(uc, sentence);
        assertNotNull(sentenceRet);
        assertNotNull(sentenceRet.getOrderIdentification());

        return sentenceRet.getOrderIdentification();

    }

    private Set<NotificationType> instancesOfNotification() {
        Set<NotificationType> notifications = new HashSet<NotificationType>();
        NotificationType notification = new NotificationType();
        notification.setNotificationOrganizationAssociation(1L);
        notification.setNotificationFacilityAssociation(204L);
        notification.setIsNotificationNeeded(true);
        notification.setIsNotificationConfirmed(true);
        notification.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6500L, 6600L })));

        notifications.add(notification);

        NotificationType notification2 = new NotificationType();
        notification2.setNotificationOrganizationAssociation(2L);
        notification2.setNotificationFacilityAssociation(205L);
        notification2.setIsNotificationNeeded(true);
        notification2.setIsNotificationConfirmed(true);
        notification2.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6700L, 6800L })));

        notifications.add(notification2);
        return notifications;
    }

    private void createOrderConfiguration() {

        // Bail
        OrderConfigurationType ret;
        OrderConfigurationType config;
        OrderConfigurationType ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.BAIL.name());
        ordConfig.setOrderType(OrdType.TOO.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(false);
        ordConfig.setIsSchedulingNeeded(true);
        ordConfig.setHasCharges(true);

        ordConfig.setIssuingAgencies(instancesOfNotification());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);
        config = ret;
        assertEquals(config.getOrderClassification(), OrdClassification.BAIL.name());
        assertEquals(config.getOrderType(), OrdType.TOO.name());
        assertEquals(config.getOrderCategory(), OrdCategory.IJ.name());
        assertEquals(config.getIsHoldingOrder(), ordConfig.getIsHoldingOrder());
        assertEquals(config.getIsSchedulingNeeded(), ordConfig.getIsSchedulingNeeded());
        assertEquals(config.getHasCharges(), ordConfig.getHasCharges());
        assertEquals(config.getIssuingAgencies(), instancesOfNotification());

        // SentenceOrder
        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.SENT.name());
        ordConfig.setOrderType(OrdType.SENTORD.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(true);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(true);

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);
        config = ret;
        assertEquals(config.getOrderClassification(), OrdClassification.SENT.name());
        assertEquals(config.getOrderType(), OrdType.SENTORD.name());
        assertEquals(config.getOrderCategory(), OrdCategory.IJ.name());
        assertEquals(config.getIsHoldingOrder(), Boolean.FALSE, "SDD: If Order Classification = SENT (Sentence Order) then isHoldingOrder = true");
        assertEquals(config.getIsSchedulingNeeded(), ordConfig.getIsSchedulingNeeded());
        assertEquals(config.getHasCharges(), ordConfig.getHasCharges());
        assertEquals(config.getIssuingAgencies(), instancesOfNotification2());

        // LegalOrder
        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.LO.name());
        ordConfig.setOrderType(OrdType.SENTORD.name());
        ordConfig.setOrderCategory(OrdCategory.OJ.name());
        ordConfig.setIsHoldingOrder(true);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(true);

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);

        // WarrantDetainer
        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.WD.name());
        ordConfig.setOrderType(OrdType.ICE.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(true);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(true);

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);

        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.WD.name());
        ordConfig.setOrderType(OrdType.WRMD.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(false);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(false);

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);
    }

    private Set<NotificationType> instancesOfNotification2() {
        Set<NotificationType> notifications = new HashSet<NotificationType>();
        NotificationType notification = new NotificationType();
        notification.setNotificationOrganizationAssociation(2L);
        notification.setNotificationFacilityAssociation(205L);
        notification.setIsNotificationNeeded(true);
        notification.setIsNotificationConfirmed(true);
        notification.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6500L, 6600L })));
        notifications.add(notification);

        NotificationType notification2 = new NotificationType();
        notification2.setNotificationOrganizationAssociation(3L);
        notification2.setNotificationFacilityAssociation(206L);
        notification2.setIsNotificationNeeded(true);
        notification2.setIsNotificationConfirmed(true);
        notification2.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6701L, 6801L })));
        notifications.add(notification2);
        return notifications;
    }

    private void createBail(Long caseInfoId) {
        BailType bailRet;
        BailType bail = new BailType();
        bail.setOrderClassification(OrdClassification.BAIL.name());
        bail.setOrderType(OrdType.TOO.name());
        bail.setOrderCategory(OrdCategory.IJ.name());
        bail.setOrderSubType(OrdSubType.DETAINER.name());
        bail.setOrderNumber("Bail No.12345");
        bail.setOrderDisposition(instanceOfDisposition());
        bail.setOrderIssuanceDate(new Date());
        bail.setOrderReceivedDate(new Date());
        bail.setOrderStartDate(new Date());
        bail.setOrderExpirationDate(new Date());
        bail.setIsHoldingOrder(false);
        bail.setIsSchedulingNeeded(true);
        bail.setHasCharges(true);

        bail.setCaseInfoIds(new HashSet<Long>(caseInfoIds));

        bail.setBailAmounts(new HashSet<BailAmountType>(Arrays.asList(
                new BailAmountType[] { new BailAmountType(BlType.CASH.name(), BigDecimal.valueOf(400.00), new HashSet<Long>(Arrays.asList(new Long[] { 400L }))),
                        new BailAmountType(BlType.PROP.name(), BigDecimal.valueOf(5000.00), new HashSet<Long>(Arrays.asList(new Long[] { 500L }))) })));
        bail.setBailRelationship(BlRelationship.AG.name());
        bail.setBailPostedAmounts(new HashSet<BailAmountType>(Arrays.asList(
                new BailAmountType[] { new BailAmountType(BlType.CASH.name(), BigDecimal.valueOf(2000.00), new HashSet<Long>(Arrays.asList(new Long[] { 400L }))),
                        new BailAmountType(BlType.PROP.name(), BigDecimal.valueOf(3000.00), new HashSet<Long>(Arrays.asList(new Long[] { 500L }))) })));
        bail.setBailPaymentReceiptNo("Bail paid no. 123456");
        bail.setBailRequirement("Bail Requirement");
        bail.setBailerPersonIdentityAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 111L, 222L, 333L })));
        bail.setBondPostedAmount(new BailAmountType(BlType.CASH.name(), BigDecimal.valueOf(2000.00), new HashSet<Long>(Arrays.asList(new Long[] { 400L }))));
        bail.setBondPaymentDescription("Bond($2000.00) has been paid.");
        bail.setBondOrganizationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 1111L, 2222L, 3333L })));
        bail.setIsBailAllowed(true);

        bailRet = service.createBail(uc, bail);
        assertNotNull(bailRet);

    }

    private DispositionType instanceOfDisposition() {
        DispositionType disposition = new DispositionType();
        disposition.setDispositionOutcome(OrdOutcome.SENT.name());
        disposition.setOrderStatus(OrdStatus.ACTIVE.name());
        disposition.setDispositionDate(new Date());

        return disposition;
    }
}