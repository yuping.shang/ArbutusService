package syscon.arbutus.product.services.legal.contract.ejb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.legal.contract.dto.ReferenceSet;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;

import static org.testng.Assert.*;

public class ConfigurationIT extends BaseIT {
    private static final Long success = ReturnCode.Success.returnCode();

    //private CaseManagementServiceBean service;   //for local test
    private UserContext uc = null; //new UserContext();
    private Logger log = LoggerFactory.getLogger(ConfigurationIT.class);

    private LegalService service;

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();

        //localLogin();
    }

    @BeforeMethod
    public void beforeMethod() {
        service.deleteAll(uc);
    }

	/*private Session getSession() {history = historyRet.getCaseInfos().iterator().next();
            assertEquals(history.getSentenceStatus(), SENTENCE_STATUS_SENTENCED);
		
		BasicConfigurator.configure();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("arbutus-legal-test-unit");
	    EntityManager em = emf.createEntityManager();
	    return (Session)em.getDelegate();
	    
	}*/

    //for local debug use only
	/*private void localLogin() {
		
		service = new CaseManagementServiceBean();
		//init user context and login
		uc = super.getUserContext();
		super.login();
		
		service.setSession(getSession());
		
	}*/

    //for release
    private void lookupJNDILogin() {

        log.info("Before JNDI lookup");
        service = (LegalService) JNDILookUp(this.getClass(), LegalService.class);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();
    }

    @Test
    public void testLookupJNDI() {
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "testLookupJNDI" })
    public void currencyConfiguration() {
        // Set
        CodeType cad = service.setCurrencyConfig(uc, "CAD");
        assertNotNull(cad);
        assertEquals(cad, new CodeType(ReferenceSet.CURRENCY.value().toUpperCase(), "CAD"));
        // Get
        assertEquals(service.getCurrencyConfig(uc), new CodeType(ReferenceSet.CURRENCY.value().toUpperCase(), "CAD"));
        // Set
        CodeType usd = service.setCurrencyConfig(uc, "USD");
        assertNotNull(usd);
        assertEquals(usd, new CodeType(ReferenceSet.CURRENCY.value().toUpperCase(), "USD"));
        // Get
        assertEquals(service.getCurrencyConfig(uc), new CodeType(ReferenceSet.CURRENCY.value().toUpperCase(), "USD"));
        // Remove
        CodeType nullType = service.setCurrencyConfig(uc, null);
        assertNull(nullType);
        assertNull(service.getCurrencyConfig(uc));
    }

    private void login(String user, String password) {

        SecurityClient client;

        try {
            client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password); //"devtest", "devtest" nice regular service user
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
