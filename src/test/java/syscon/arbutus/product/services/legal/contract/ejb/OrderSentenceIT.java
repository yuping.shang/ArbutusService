package syscon.arbutus.product.services.legal.contract.ejb;

import java.math.BigDecimal;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.legal.contract.dto.LegalModule;
import syscon.arbutus.product.services.legal.contract.dto.ReferenceSet;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CaseActivitySearchType;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivityType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierConfigType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType;
import syscon.arbutus.product.services.legal.contract.dto.charge.*;
import syscon.arbutus.product.services.legal.contract.dto.ordersentence.*;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.AdjustmentType;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.SentenceAdjustmentType;
import syscon.arbutus.product.services.legal.contract.ejb.SentenceAdjustmentIT.ADJUSTMENT_CLASSIFICATION;
import syscon.arbutus.product.services.legal.contract.ejb.SentenceAdjustmentIT.ADJUSTMENT_FUNCTION;
import syscon.arbutus.product.services.legal.contract.ejb.SentenceAdjustmentIT.ADJUSTMENT_STATUS;
import syscon.arbutus.product.services.legal.contract.ejb.SentenceAdjustmentIT.ADJUSTMENT_TYPE;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;
import syscon.arbutus.product.services.sentencecalculation.contract.dto.Sentence;
import syscon.arbutus.product.services.sentencecalculation.contract.dto.Supervision;
import syscon.arbutus.product.services.sentencecalculation.contract.interfaces.SentenceCalculation;

import static org.testng.Assert.*;

public class OrderSentenceIT extends BaseIT {

    private static final Long success = ReturnCode.Success.returnCode();
    private static final String CASE_CATEGORY_IJ = "IJ";
    private static final String CASE_CATEGORY_OJ = "OJ";
    private static final String CASE_TYPE_ADULT = "ADULT";
    private static final String CASE_TYPE_JUVENILE = "JUVENILE";
    private static final String CASE_TYPE_TRAFFIC = "TRAFFIC";
    private static final String SENTENCE_STATUS_SENTENCED = "SENTENCED";
    private static final String SENTENCE_STATUS_UNSENTENCED = "UNSENTENCED";
    private static final String CASE_STATUS_REASON_DISMISSED = "DISMISSED";
    private static final String CASE_STATUS_REASON_RE_OPENED = "RE-OPENED";
    private static final String CASE_IDENTIFIER_TYPE_DOCKET = "DOCKET";
    private static final String CASE_IDENTIFIER_TYPE_INDICTMENT = "INDICTMENT";
    private static final String CASE_IDENTIFIER_FORMAT_000000A = "000000A";
    //=================================================================
    private static final String CHARGE_OUTCOME_SENT = "SENT";
    private static final String CHARGE_OUTCOME_RELBAIL = "RELBAIL";
    private static final String CHARGE_STATUS_ACTIVE = "ACTIVE";
    private static final String CHARGE_STATUS_INACTIVE = "INACTIVE";
    private static final String CHARGE_IDENTIFIER_CJIS = "CJIS";
    private static final String CHARGE_IDENTIFIER_FORMAT_0_00000 = "0-00000";
    private static final String CHARGE_INDICATOR_ACC = "ACC";
    private static final String CHARGE_INDICATOR_ARRT = "ARRT";
    private static final String CHARGE_INDICATOR_EMP = "EMP";
    private static final String CHARGE_PLEA_G = "G";
    private static final String CHARGE_PLEA_N = "N";
    private static final String APPEAL_STATUS_INPROGRESS = "INPROGRESS";
    private static final String APPEAL_STATUS_ACCEPTED = "ACCEPTED";
    private static final String CHARGE_CATEGORY_T = "T";
    private static final String CHARGE_CATEGORY_A = "A";
    private static final String CHARGE_CODE_1510 = "15-10";
    private static final String CHARGE_TYPE_F = "F";
    private static final String CHARGE_DEGREE_I = "I";
    private static final String CHARGE_DEGREE_II = "II";
    private static final String CHARGE_SEVERITY_H = "H";
    private static final String CHARGE_SEVERITY_L = "L";
    private static final String CHARGE_LANGUAGE_EN = "EN";
    private static final String CHARGE_ENHANCING_FACTOR_RACE = "RACE";
    private static final String CHARGE_ENHANCING_FACTOR_SEX = "SEX";
    private static final String CHARGE_REDUCING_FACTOR_MI = "MI";
    private static final String CHARGE_REDUCING_FACTOR_MIN = "MIN";
    //private CaseManagementService service;  //for Integration test
    //private CaseManagementServiceBean service;   //for local test
    private UserContext uc = null; //new UserContext();
    private Logger log = LoggerFactory.getLogger(OrderSentenceIT.class);
    private Long statuteChargeConfigId = 1L;
    private List<CaseInfoType> createdCaseInfos = new ArrayList<CaseInfoType>();
    private List<CaseIdentifierConfigType> createdConfigs = new ArrayList<CaseIdentifierConfigType>();
    private List<Long> historySetCourt = new ArrayList<Long>();
    private Set<Long> caseInfoIds = new HashSet<Long>();
    private Set<Long> orderIds = new HashSet<Long>();
    private Long sentenceId1, sentenceId2;
    private Set<Long> chargeIds = new HashSet<Long>();
    private Set<Long> subChargeIds = new HashSet<Long>();
    private List<CourtActivityType> createdCourtActivities = new ArrayList<CourtActivityType>();
    private List<Long> courtActivityIds = new ArrayList<Long>();
    private LegalService service;
    private SentenceCalculation sentenceCalculation;
    private String NULL_ERROR_MESSAGE = "Validation Error: syscon.arbutus.product.services: argument must not be null.";
    private String SEARCH_ERROR_MESSAGE = "Validation: At least one argument must be provided for search.";
    private Long supervisionId = 1L;
    private Long supervisionId2 = 2L;
    private Long staffId = 1L;
    private Long facilityId = 1L;

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();

        // Facility
        // 		FacilityTest fac = new FacilityTest();
        // 		facilityId = fac.createFacility();
        //
        //        //localLogin();
        //        StaffTest staffTest = new StaffTest();
        //        staffId = staffTest.createStaff();
        //
        //        SupervisionTest sup = new SupervisionTest();
        //        supervisionId = sup.createSupervision();
        //        supervisionId2 = sup.createSupervision();
    }

    @AfterClass
    public void afterClass() {
        //    	StaffTest staffTest = new StaffTest();
        //    	staffTest.delete(staffId);
        //
        //    	SupervisionTest sup = new SupervisionTest();
        //    	sup.deleteSupervision(supervisionId);
        //    	sup.deleteSupervision(supervisionId2);
        //
        //    	FacilityTest fac = new FacilityTest();
        //    	fac.deleteFacility(facilityId);
    }

    @BeforeMethod
    public void beforeMethod() {

    }

    //for release
    private void lookupJNDILogin() {
        log.info("Before JNDI lookup");
        service = (LegalService) JNDILookUp(this.getClass(), LegalService.class);
        assertNotNull(service);

        sentenceCalculation = (SentenceCalculation) JNDILookUp(this.getClass(), SentenceCalculation.class);

        //init user context and login
        uc = super.initUserContext();
    }

    @Test(enabled = true)
    public void testLookupJNDI() {
        assertNotNull(service);
    }
    //====================================================================

    // @Test(dependsOnMethods = { "testLookupJNDI" })
    public void reset() {
        Long ret = service.deleteAll(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());
    }

    @Test
    public void setKeyDateViewByAggregated() {
        service.setKeyDateViewByAggregated(uc, Boolean.TRUE);
        assertTrue(service.isKeyDateViewByAggregated(uc));
        service.setKeyDateViewByAggregated(uc, Boolean.FALSE);
        assertFalse(service.isKeyDateViewByAggregated(uc));
        assertEquals(service.deleteKeyDateViewByAggregatedConfiguration(uc).intValue(), 1);
        assertNull(service.isKeyDateViewByAggregated(uc));
    }

    @Test(enabled = true)
    public void createOrderConfiguration() {
        reset();

        // Bail
        OrderConfigurationType ret;
        OrderConfigurationType config;
        OrderConfigurationType ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.BAIL.name());
        ordConfig.setOrderType(OrdType.TOO.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(false);
        ordConfig.setIsSchedulingNeeded(true);
        ordConfig.setHasCharges(true);

        ordConfig.setIssuingAgencies(instancesOfNotification());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);
        config = ret;
        assertEquals(config.getOrderClassification(), OrdClassification.BAIL.name());
        assertEquals(config.getOrderType(), OrdType.TOO.name());
        assertEquals(config.getOrderCategory(), OrdCategory.IJ.name());
        assertEquals(config.getIsHoldingOrder(), ordConfig.getIsHoldingOrder());
        assertEquals(config.getIsSchedulingNeeded(), ordConfig.getIsSchedulingNeeded());
        assertEquals(config.getHasCharges(), ordConfig.getHasCharges());
        assertEquals(config.getIssuingAgencies(), instancesOfNotification());

        // SentenceOrder
        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.SENT.name());
        ordConfig.setOrderType(OrdType.SENTORD.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(true);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(true);

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);
        config = ret;
        assertEquals(config.getOrderClassification(), OrdClassification.SENT.name());
        assertEquals(config.getOrderType(), OrdType.SENTORD.name());
        assertEquals(config.getOrderCategory(), OrdCategory.IJ.name());
        assertEquals(config.getIsHoldingOrder(), Boolean.FALSE, "SDD: If Order Classification = SENT (Sentence Order) then isHoldingOrder = true");
        assertEquals(config.getIsSchedulingNeeded(), ordConfig.getIsSchedulingNeeded());
        assertEquals(config.getHasCharges(), ordConfig.getHasCharges());
        assertEquals(config.getIssuingAgencies(), instancesOfNotification2());

        // LegalOrder
        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.LO.name());
        ordConfig.setOrderType(OrdType.SENTORD.name());
        ordConfig.setOrderCategory(OrdCategory.OJ.name());
        ordConfig.setIsHoldingOrder(true);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(true);

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);

        // WarrantDetainer
        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.WD.name());
        ordConfig.setOrderType(OrdType.ICE.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(true);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(true);

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);

        ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.WD.name());
        ordConfig.setOrderType(OrdType.WRMD.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(false);
        ordConfig.setIsSchedulingNeeded(false);
        ordConfig.setHasCharges(false);

        ordConfig.setIssuingAgencies(instancesOfNotification2());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);
    }

    @Test(dependsOnMethods = { "createOrderConfiguration" }, enabled = true)
    public void retrieveOrderConfiguration() {
        Set<OrderConfigurationType> ret;
        OrderConfigurationReturnType retType;

        Long startIndex = 0L;
        Long resultSize = 300L;
        String resultOrder = "orderClassification;orderType";

        OrderConfigurationType config = new OrderConfigurationType();
        Set<NotificationType> notifications = new HashSet<NotificationType>();
        NotificationType notification;

        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 5);
        // OrderClassification
        config = new OrderConfigurationType();
        config.setOrderClassification(AppealStatus.ACCEPTED.name());
        try {
            ret = service.retrieveOrderConfiguration(uc, config);
        } catch (Exception e) {
            Assert.assertTrue(e instanceof DataNotExistException);
        }

        config.setOrderClassification(OrdClassification.BAIL.name());
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 1);

        retType = service.retrieveOrderConfiguration(uc, config, startIndex, resultSize, resultOrder);
        assertNotNull(retType.getOrderConfigurations());
        assertEquals(retType.getTotalSize().intValue(), 1);
        assertEquals(retType.getOrderConfigurations().size(), 1);
        assertEquals(ret.toArray()[0], retType.getOrderConfigurations().toArray()[0]);

        // OrderType
        config = new OrderConfigurationType();
        config.setOrderType(OrdType.TOO.name());
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 1);

        retType = service.retrieveOrderConfiguration(uc, config, startIndex, resultSize, resultOrder);
        assertNotNull(retType.getOrderConfigurations());
        assertEquals(retType.getTotalSize().intValue(), 1);
        assertEquals(retType.getOrderConfigurations().size(), 1);
        assertEquals(ret.toArray()[0], retType.getOrderConfigurations().toArray()[0]);

        config = new OrderConfigurationType();
        config.setOrderType(OrdType.ICE.name());
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 1);

        retType = service.retrieveOrderConfiguration(uc, config, startIndex, resultSize, resultOrder);
        assertNotNull(retType.getOrderConfigurations());
        assertEquals(retType.getTotalSize().intValue(), 1);
        assertEquals(retType.getOrderConfigurations().size(), 1);
        assertEquals(ret.toArray()[0], retType.getOrderConfigurations().toArray()[0]);

        // OrderCategory
        config = new OrderConfigurationType();
        config.setOrderCategory(OrdCategory.IJ.name());
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 4);

        retType = service.retrieveOrderConfiguration(uc, config, startIndex, resultSize, resultOrder);
        assertNotNull(retType.getOrderConfigurations());
        assertEquals(retType.getTotalSize().intValue(), 4);
        assertEquals(retType.getOrderConfigurations().size(), 4);
        assertTrue(ret.containsAll(retType.getOrderConfigurations()));
        assertTrue(retType.getOrderConfigurations().containsAll(ret));

        //		for (OrderConfigurationType conf: retType.getOrderConfigurations()) {
        //			log.info(conf);
        //		}

        config = new OrderConfigurationType();
        config.setOrderCategory(OrdCategory.OJ.name());
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 1);
        // isHoldingOrder
        config = new OrderConfigurationType();
        config.setIsHoldingOrder(true);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 2);

        config = new OrderConfigurationType();
        config.setIsHoldingOrder(false);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 3);
        // isSchedulingNeeded
        config = new OrderConfigurationType();
        config.setIsSchedulingNeeded(true);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 1);

        config = new OrderConfigurationType();
        config.setIsSchedulingNeeded(false);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 4);
        // hasCharges
        config = new OrderConfigurationType();
        config.setHasCharges(true);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 4);

        config = new OrderConfigurationType();
        config.setHasCharges(false);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 1);
        // issuingAgencies
        config = new OrderConfigurationType();
        notifications = new HashSet<NotificationType>();
        notification = new NotificationType();
        // issuingAgencies -- Notification Organization Association
        notification.setNotificationOrganizationAssociation(1L);
        notifications.add(notification);
        config.setIssuingAgencies(notifications);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 1);

        config = new OrderConfigurationType();
        notifications = new HashSet<NotificationType>();
        notification = new NotificationType();
        notification.setNotificationOrganizationAssociation(2L);
        notifications.add(notification);
        config.setIssuingAgencies(notifications);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 5);
        // issuingAgencies -- Notification Facility Association
        config = new OrderConfigurationType();
        notifications = new HashSet<NotificationType>();
        notification = new NotificationType();
        notification.setNotificationFacilityAssociation(204L);
        notifications.add(notification);
        config.setIssuingAgencies(notifications);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 1);

        config = new OrderConfigurationType();
        notifications = new HashSet<NotificationType>();
        notification = new NotificationType();
        notification.setNotificationFacilityAssociation(205L);
        notifications.add(notification);
        config.setIssuingAgencies(notifications);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 5);
        // issuingAgencies -- Is Notification Confirmed
        config = new OrderConfigurationType();
        notifications = new HashSet<NotificationType>();
        notification = new NotificationType();
        notification.setIsNotificationConfirmed(true);
        notifications.add(notification);
        config.setIssuingAgencies(notifications);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 5);
        // issuingAgencies -- IsNotificationNeeded
        config = new OrderConfigurationType();
        notifications = new HashSet<NotificationType>();
        notification = new NotificationType();
        notification.setIsNotificationNeeded(true);
        notifications.add(notification);
        config.setIssuingAgencies(notifications);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 5);
        // issuingAgencies -- Notification Agency Location Associations
        config = new OrderConfigurationType();
        notifications = new HashSet<NotificationType>();
        notification = new NotificationType();
        notification.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6500L, 6600L })));
        notifications.add(notification);
        config.setIssuingAgencies(notifications);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 5);
        // issuingAgencies -- Notification Agency Contacts
        config = new OrderConfigurationType();
        notifications = new HashSet<NotificationType>();
        notification = new NotificationType();
        ContactType contact1 = getContact("1");
        ContactType contact2 = getContact("2");
        ContactType contact3 = getContact("3");
        Set<ContactType> contacts = new HashSet<>();
        contacts.add(contact1);
        contacts.add(contact2);
        contacts.add(contact3);
        notification.setNotificationAgencyContacts(contacts);

        notifications.add(notification);
        // config.setIssuingAgencies(notifications);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 5);

        config = new OrderConfigurationType();
        notifications = new HashSet<NotificationType>();
        notification = new NotificationType();
        ContactType contactBC = getContact("BC");
        Set<ContactType> contactsBC = new HashSet<>();
        contactsBC.add(contactBC);
        notification.setNotificationAgencyContacts(contactsBC); // wildcard
        notifications.add(notification);
        // config.setIssuingAgencies(notifications);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 5);

        config = new OrderConfigurationType();
        notifications = new HashSet<NotificationType>();
        notification = new NotificationType();
        notification.setNotificationAgencyContacts(contactsBC);
        notifications.add(notification);
        config.setIssuingAgencies(notifications);
        ret = service.retrieveOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertEquals(ret.size(), 0);
    }

    @Test(dependsOnMethods = { "retrieveOrderConfiguration" }, enabled = false)
    public void updateOrderConfiguration() {
        reset();

        OrderConfigurationType ret;
        OrderConfigurationType config;
        OrderConfigurationType ordConfig = new OrderConfigurationType();
        ordConfig.setOrderClassification(OrdClassification.BAIL.name());
        ordConfig.setOrderType(OrdType.TOO.name());
        ordConfig.setOrderCategory(OrdCategory.IJ.name());
        ordConfig.setIsHoldingOrder(false);
        ordConfig.setIsSchedulingNeeded(true);
        ordConfig.setHasCharges(true);

        ordConfig.setIssuingAgencies(instancesOfNotification());
        ret = service.setOrderConfiguration(uc, ordConfig);
        assertNotNull(ret);
        config = ret;
        assertEquals(config.getOrderClassification(), OrdClassification.BAIL.name());
        assertEquals(config.getOrderType(), OrdType.TOO.name());
        assertEquals(config.getOrderCategory(), OrdCategory.IJ.name());
        assertEquals(config.getIsHoldingOrder(), ordConfig.getIsHoldingOrder());
        assertEquals(config.getIsSchedulingNeeded(), ordConfig.getIsSchedulingNeeded());
        assertEquals(config.getHasCharges(), ordConfig.getHasCharges());
        assertEquals(config.getIssuingAgencies(), instancesOfNotification());
        // update ORDER_CLASSIFICATION to SENT from BAIL
        config = ret;
        // config.setOrderClassification(new CodeType(ReferenceSet.ORDER_CLASSIFICATION.value(), OrdClassification.SENT.name()));
        config.setIsSchedulingNeeded(true);
        ret = service.setOrderConfiguration(uc, config);
        assertNotNull(ret);
        assertTrue(ret.getIsSchedulingNeeded());
    }

    @Test(dependsOnMethods = { "createOrderConfiguration" }, enabled = true)
    public void getDispositionStatus() {
        assertNotNull(service.getOrderDispositionStatus(uc, new CodeType(ReferenceSet.ORDER_OUTCOME.value(), OrdOutcome.WE.name())));
        assertEquals(service.getOrderDispositionStatus(uc, new CodeType(ReferenceSet.ORDER_OUTCOME.value(), OrdOutcome.SENT.name())),
                new CodeType(ReferenceSet.ORDER_STATUS.value().toUpperCase(), OrdStatus.ACTIVE.name()));

        assertNotNull(service.getOrderDispositionStatus(uc, new CodeType(ReferenceSet.ORDER_OUTCOME.value(), OrdOutcome.WC.name())));
        assertEquals(service.getOrderDispositionStatus(uc, new CodeType(ReferenceSet.ORDER_OUTCOME.value(), OrdOutcome.WC.name())),
                new CodeType(ReferenceSet.ORDER_STATUS.value().toUpperCase(), OrdStatus.INACTIVE.name()));
    }

    @Test(enabled = true)
    public void createSentenceKeyDate() {
        SentenceKeyDateType ret;
        SentenceKeyDateType sentenceKeyDate = new SentenceKeyDateType();

        List<KeyDateType> keyDateTypeList = service.getSentenceKeyDateHistory(uc, supervisionId, KeyDates.PDD.name());
        assertEquals(keyDateTypeList.size(), 0);
        keyDateTypeList = service.getSentenceKeyDateHistory(uc, supervisionId, KeyDates.SED.name());
        assertEquals(keyDateTypeList.size(), 0);

        sentenceKeyDate.setSupervisionId(supervisionId);
        sentenceKeyDate.setStaffId(staffId);
        CommentType comment = new CommentType();
        comment.setComment("Comment of SentenceKeyDate");
        comment.setCommentDate(new Date());
        comment.setUserId(this.uc.getConsumingApplicationId());
        sentenceKeyDate.setComment(comment);
        sentenceKeyDate.setReviewRequired(true);
        Set<KeyDateType> keydates = new HashSet<KeyDateType>();
        Date date = new Date();
        KeyDateType keydate1 = new KeyDateType(KeyDates.PDD.name(), date, date, date, staffId, "CALCERR");
        KeyDateType keydate2 = new KeyDateType(KeyDates.SED.name(), date, date, date, staffId, "SENTADJ");
        keydates.add(keydate2);
        keydates.add(keydate1);
        sentenceKeyDate.setKeydates(keydates);
        ret = service.createSentenceKeyDate(uc, sentenceKeyDate);
        assertNotNull(ret);
        assertEquals(ret, service.getSentenceKeyDate(uc, supervisionId));

        keyDateTypeList = service.getSentenceKeyDateHistory(uc, supervisionId, KeyDates.PDD.name());
        assertEquals(keyDateTypeList.size(), 1);
        /*System.out.println("createSentenceKeyDate: KeyDates.PDD: " + keyDateTypeList);
        System.out.println();*/

        keyDateTypeList = service.getSentenceKeyDateHistory(uc, supervisionId, KeyDates.SED.name());
        assertEquals(keyDateTypeList.size(), 1);
        /*System.out.println("createSentenceKeyDate: KeyDates.SED: " + keyDateTypeList);
        System.out.println();*/
    }

    @Test(dependsOnMethods = { "createSentenceKeyDate" }, enabled = true)
    public void updateSentenceKeyDate() {
        SentenceKeyDateType ret;
        SentenceKeyDateType sentenceKeyDate = new SentenceKeyDateType();
        // Long supervisionId = 1L;

        sentenceKeyDate.setSupervisionId(supervisionId);
        sentenceKeyDate.setStaffId(staffId);
        CommentType comment = new CommentType();
        comment.setComment("Comment of SentenceKeyDate");
        comment.setCommentDate(new Date());
        comment.setUserId(this.uc.getConsumingApplicationId());
        sentenceKeyDate.setComment(comment);
        sentenceKeyDate.setReviewRequired(true);
        Set<KeyDateType> keydates = new HashSet<KeyDateType>();
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        KeyDateType keydate1 = new KeyDateType(KeyDates.PDD.name(), date, date, date, staffId, "CALCERR");
        KeyDateType keydate2 = new KeyDateType(KeyDates.SED.name(), date, date, date, staffId, "SENTADJ");
        KeyDateType keydate3 = new KeyDateType(KeyDates.PED.name(), date, date, date, staffId, "RECLASS");
        keydates.add(keydate3);
        keydates.add(keydate2);
        keydates.add(keydate1);
        sentenceKeyDate.setKeydates(keydates);

        ret = service.updateSentenceKeyDate(uc, sentenceKeyDate);
        assertNotNull(ret);
        assertEquals(ret, service.getSentenceKeyDate(uc, supervisionId));

        List<SentenceKeyDateType> histories = service.getSentenceKeyDateHistory(uc, supervisionId);
        assertTrue(histories.size() > 0);
        for (SentenceKeyDateType kd : histories) {
            Set<KeyDateType> kds = kd.getKeydates();
            for (KeyDateType k : kds) {
                assertNotNull(k.getKeyDate());
                assertNotNull(k.getKeyDateAdjust());
                assertNotNull(k.getKeyDateType());
                assertNotNull(k.getKeyDateOverride());
                assertNotNull(k.getOverrideByStaffId());
                assertNotNull(k.getOverrideReason());
            }
        }

        sentenceKeyDate.setStaffId(staffId);
        CommentType comment2 = new CommentType();
        comment.setComment("Comment of SentenceKeyDate 2");
        comment.setCommentDate(new Date());
        comment.setUserId(this.uc.getConsumingApplicationId());
        sentenceKeyDate.setComment(comment2);
        sentenceKeyDate.setReviewRequired(true);
        keydates = new HashSet<KeyDateType>();
        cal.add(Calendar.DAY_OF_MONTH, 1);
        date = cal.getTime();
        KeyDateType keydate6 = new KeyDateType(KeyDates.PDD.name(), date, date, date, staffId, "CALCERR");
        KeyDateType keydate7 = new KeyDateType(KeyDates.SED.name(), date, date, date, staffId, "SENTADJ");
        KeyDateType keydate8 = new KeyDateType(KeyDates.PED.name(), date, date, date, staffId, "RECLASS");
        keydates.add(keydate6);
        keydates.add(keydate7);
        keydates.add(keydate8);
        sentenceKeyDate.setKeydates(keydates);

        ret = service.updateSentenceKeyDate(uc, sentenceKeyDate);
        assertNotNull(ret);
        assertEquals(ret, service.getSentenceKeyDate(uc, supervisionId));
        histories = service.getSentenceKeyDateHistory(uc, supervisionId);
        assertTrue(histories.size() > 0);
        for (SentenceKeyDateType kd : histories) {
            Set<KeyDateType> kds = kd.getKeydates();
            for (KeyDateType k : kds) {
                assertNotNull(k.getKeyDate());
                assertNotNull(k.getKeyDateAdjust());
                assertNotNull(k.getKeyDateType());
                assertNotNull(k.getKeyDateOverride());
                assertNotNull(k.getOverrideByStaffId());
                assertNotNull(k.getOverrideReason());
            }
        }

        List<KeyDateType> keyDateTypeList = service.getSentenceKeyDateHistory(uc, supervisionId, KeyDates.PDD.name());
        assertEquals(keyDateTypeList.size(), 3);
        /*for (KeyDateType keyDateType: keyDateTypeList)
            System.out.println(keyDateType);
        System.out.println();*/

        keyDateTypeList = service.getSentenceKeyDateHistory(uc, supervisionId, KeyDates.SED.name());
        assertEquals(keyDateTypeList.size(), 3);
        /*for (KeyDateType keyDateType: keyDateTypeList)
            System.out.println(keyDateType);
        System.out.println();*/

        keyDateTypeList = service.getSentenceKeyDateHistory(uc, supervisionId, KeyDates.PED.name());
        assertEquals(keyDateTypeList.size(), 2);
        /*for (KeyDateType keyDateType: keyDateTypeList)
            System.out.println(keyDateType);
        System.out.println();*/

    }

    @Test(dependsOnMethods = { "updateSentenceKeyDate", "createAndGet" }, enabled = true)
    public void calculateSentenceKeyDate() {
        createSentenceAdjustment();

        //    	SentenceAdjustmentType sentenceAdjustment1 = service.getSentenceAdjustment(uc, supervisionId);
        //    	for (AdjustmentType adjustment: sentenceAdjustment1.getAdjustments()) {
        //			assertNotNull(adjustment.getAdjustment());
        //			System.out.println("AdjustmentType: " + adjustment.getAdjustmentType() + ": " + adjustment.getAdjustment());
        //			System.out.println("Sentence ID: " + adjustment.getSentenceIds());
        //    	}
        //    	System.out.println("\n");
        //    	System.out.println(service.getSentenceKeyDate(uc, supervisionId));
        //    	System.out.println("\n");

        String comment = "Calculation comment";

        // Boolean reviewRequired = sentenceKeyDate.getReviewRequired();
        Boolean reviewRequired = true;
        SentenceKeyDateType ret = service.calculateSentenceKeyDates(uc, supervisionId, staffId, comment, reviewRequired);
        assertNotNull(ret);
        assertEquals(ret.getSupervisionId(), supervisionId);
        assertEquals(ret.getStaffId(), staffId);
        assertEquals(ret.getComment().getComment(), comment);

        SentenceKeyDateType ret2 = service.getSentenceKeyDate(uc, supervisionId);
        assertEquals(ret, ret2);
        //    	System.out.println(service.getSentenceKeyDate(uc, supervisionId));
        //    	System.out.println("\n");

        for (KeyDateType kd : ret2.getKeydates()) {
            if (KeyDates.PDD.name().equalsIgnoreCase(kd.getKeyDateType())) {
                assertEquals(kd.getKeyDate(), getDate(2013, 7, 25));
            } else if (KeyDates.SED.name().equalsIgnoreCase(kd.getKeyDateType())) {
                assertEquals(kd.getKeyDate(), getDate(2013, 10, 14));
            }
        }
        // assertFalse(ret.equals(sentenceKeyDate));

        SentenceAdjustmentType sentenceAdjustment = service.getSentenceAdjustment(uc, supervisionId);
        assertNotNull(sentenceAdjustment.getAdjustments());
        for (AdjustmentType adjustment : sentenceAdjustment.getAdjustments()) {
            assertNotNull(adjustment.getAdjustment());
            System.out.println("AdjustmentType: " + adjustment.getAdjustmentType() + ": " + adjustment.getAdjustment());
            System.out.println("Sentence ID: " + adjustment.getSentenceIds());
        }
    }

    @Test(dependsOnMethods = { "calculateSentenceKeyDate" }, enabled = true)
    public void retrieveAggregateSentenceBySupervisionId() {
        List<AggregateSentenceGroupType> aggregateSentences = service.retrieveAggregateSentencesBySupervisionId(uc, supervisionId);
        assertNotNull(aggregateSentences);

        //    	for (AggregateSentenceGroupType agg: aggregateSentences) {
        //    		System.out.println(agg);
        //    		System.out.println();
        //    	}
    }

    @Test(dependsOnMethods = { "updateSentenceKeyDate" }, enabled = true)
    public void deleteSentenceKeyDate() {
        SentenceKeyDateType ret;
        //Long supervisionId = 1L;

        ret = service.getSentenceKeyDate(uc, supervisionId);
        assertNotNull(ret);
        assertEquals(service.deleteSentenceKeyDate(uc, supervisionId).intValue(), 1);
        assertNull(service.findSentenceKeyDateBySupervisionId(uc, supervisionId));
    }

    @Test(dependsOnMethods = { "createOrderConfiguration" }, enabled = false)
    public <T extends OrderType> void createWarrant() {
        createCaseInfo();

        List<T> orders;

        // Warrant
        WarrantDetainerType retWarrant;
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        cal.add(Calendar.YEAR, 1);
        Date expirationDate = cal.getTime();
        Date termEndDate = cal.getTime();
        Date sentenceEndDate = cal.getTime();

        WarrantDetainerType warrant = new WarrantDetainerType();
        warrant.setOrderClassification(OrdClassification.WD.name());
        warrant.setOrderType(OrdType.WRMD.name());
        warrant.setOrderCategory(OrdCategory.IJ.name());
        //warrant.setOrderSubType(new CodeType(ReferenceSet.ORDER_SUB_TYPE.value(), OrdSubType.HOLD.name()));
        warrant.setOrderNumber("Warrant Order No.12345");
        //warrant.setOrderDisposition(instanceOfDisposition());
        DispositionType disposition = new DispositionType();
        CodeType dispositionOutcome = new CodeType(ReferenceSet.ORDER_OUTCOME.value(), OrdOutcome.WC.name());
        disposition.setDispositionOutcome(OrdOutcome.WC.name());
        CodeType dispositionStatus = service.getOrderDispositionStatus(uc, dispositionOutcome);
        disposition.setOrderStatus(dispositionStatus == null ? null : dispositionStatus.getCode());
        disposition.setDispositionDate(new Date());
        warrant.setOrderDisposition(disposition);

        CommentType comment = new CommentType();
        comment.setCommentDate(new Date());
        comment.setComment("Warrant ABC");
        comment.setUserId("devtest");
        warrant.setComments(comment);

        warrant.setOrderIssuanceDate(today);
        warrant.setOrderReceivedDate(today);
        warrant.setOrderStartDate(today);
        warrant.setOrderExpirationDate(expirationDate);

        Set<Long> caseActivityInitiatedOrderAssociations = new HashSet<Long>();
        caseActivityInitiatedOrderAssociations.add(1L);
        caseActivityInitiatedOrderAssociations.add(2L);
        caseActivityInitiatedOrderAssociations.add(3L);
        warrant.setCaseActivityInitiatedOrderAssociations(caseActivityInitiatedOrderAssociations);

        Set<Long> orderInitiatedCaseActivityAssociations = new HashSet<Long>();
        orderInitiatedCaseActivityAssociations.add(4L);
        orderInitiatedCaseActivityAssociations.add(5L);
        orderInitiatedCaseActivityAssociations.add(6L);
        warrant.setOrderInitiatedCaseActivityAssociations(orderInitiatedCaseActivityAssociations);

        warrant.setIsHoldingOrder(true);
        warrant.setIsSchedulingNeeded(false);
        warrant.setHasCharges(true);
        warrant.setIssuingAgency(instanceOfNotification());

        warrant.setCaseInfoIds(caseInfoIds);

        Set<Long> chargeIds = new HashSet<Long>();
        //		chargeIds.add(100L);
        //		chargeIds.add(200L);
        //		chargeIds.add(300L);
        warrant.setChargeIds(chargeIds);

        Set<Long> conditionIds = new HashSet<Long>();
        //		conditionIds.add(1000L);
        //		conditionIds.add(2000L);
        //		conditionIds.add(3000L);
        warrant.setConditionIds(conditionIds);

        warrant.setAgencyToBeNotified(instancesOfNotification());

        retWarrant = service.createWarrantDetainer(uc, warrant);
        assertNotNull(retWarrant);
        Long orderId = retWarrant.getOrderIdentification();
        assertNotNull(orderId);
        warrant.setOrderIdentification(orderId);

        assertEquals(retWarrant, warrant);
        WarrantDetainerType warrant2 = service.getOrder(uc, warrant.getOrderIdentification());
        assertNotNull(warrant2);
        warrant2.setNotificationLog(null);
        assertEquals(warrant2, retWarrant);

        for (Long caseId : this.caseInfoIds) {
            orders = service.retrieveOrdersForCase(uc, caseId, null, true);
            assertNotNull(orders);
            assertTrue(orders.size() > 0);
            assertTrue(orders.contains(service.getOrder(uc, warrant.getOrderIdentification())));
        }
    }

    @Test(dependsOnMethods = { "createOrderConfiguration" }, enabled = true)
    public <T extends OrderType> void createAndGet() {
        // createOrderConfiguration();
        //createCaseActivities();
        createCaseInfo();
        createSignleChargePositive();
        createSignleChargePositive();
        Long[] arrayChargeIds = new Long[] { (Long) chargeIds.toArray()[0], (Long) chargeIds.toArray()[1] };

        createCaseActivities();

        List<T> orders;
        try {
            assertNull(service.createBail(uc, null));
        } catch (InvalidInputException e) {
            assertEquals(e.getMessage(), NULL_ERROR_MESSAGE);
        }

        try {
            assertNull(service.createWarrantDetainer(uc, null));
        } catch (InvalidInputException e) {
            assertEquals(e.getMessage(), NULL_ERROR_MESSAGE);
        }

        try {
            assertNull(service.createSentence(uc, null));
        } catch (InvalidInputException e) {
            assertEquals(e.getMessage(), NULL_ERROR_MESSAGE);
        }

        try {
            assertNull(service.createLegalOrder(uc, null));
        } catch (InvalidInputException e) {
            assertEquals(e.getMessage(), NULL_ERROR_MESSAGE);
        }

        // Warrant
        WarrantDetainerType retWarrant;
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        cal.add(Calendar.YEAR, 1);
        Date expirationDate = cal.getTime();
        Date termEndDate = cal.getTime();
        Date sentenceEndDate = cal.getTime();

        WarrantDetainerType warrant = new WarrantDetainerType();
        warrant.setOrderClassification(OrdClassification.WD.name());
        warrant.setOrderType(OrdType.ICE.name());
        warrant.setOrderCategory(OrdCategory.IJ.name());
        warrant.setOrderSubType(OrdSubType.WANT.name());
        warrant.setOrderNumber("Warrant Order No.12345");
        warrant.setOrderDisposition(instanceOfDisposition(today));
        warrant.setComments(instanceOfComment());
        warrant.setOrderIssuanceDate(today);
        warrant.setOrderReceivedDate(today);
        warrant.setOrderStartDate(today);
        warrant.setOrderExpirationDate(today);

        Set<Long> caseActivityInitiatedOrderAssociations = new HashSet<Long>();
        caseActivityInitiatedOrderAssociations.add(courtActivityIds.get(0));
        warrant.setCaseActivityInitiatedOrderAssociations(caseActivityInitiatedOrderAssociations);

        Set<Long> orderInitiatedCaseActivityAssociations = new HashSet<Long>();
        orderInitiatedCaseActivityAssociations.add(courtActivityIds.get(1));
        orderInitiatedCaseActivityAssociations.add(courtActivityIds.get(2));
        warrant.setOrderInitiatedCaseActivityAssociations(orderInitiatedCaseActivityAssociations);

        warrant.setIsHoldingOrder(true);
        warrant.setIsSchedulingNeeded(false);
        warrant.setHasCharges(true);
        warrant.setIssuingAgency(instanceOfNotification());

        warrant.setCaseInfoIds(caseInfoIds);

        // Set<Long> chargeIds = new HashSet<Long>();
        //		chargeIds.add(100L);
        //		chargeIds.add(200L);
        //		chargeIds.add(300L);
        warrant.setChargeIds(chargeIds);

        Set<Long> conditionIds = new HashSet<Long>();
        //		conditionIds.add(1000L);
        //		conditionIds.add(2000L);
        //		conditionIds.add(3000L);
        warrant.setConditionIds(conditionIds);

        warrant.setAgencyToBeNotified(instancesOfNotification());

        retWarrant = service.createWarrantDetainer(uc, warrant);
        assertNotNull(retWarrant);
        Long orderId = retWarrant.getOrderIdentification();
        assertNotNull(orderId);
        warrant.setOrderIdentification(orderId);
        assertEquals(retWarrant.getOrderClassification(), OrdClassification.WD.name());
        assertEquals(retWarrant.getOrderType(), OrdType.ICE.name());

        assertEquals(retWarrant.getOrderCategory(), OrdCategory.IJ.name());
        assertEquals(retWarrant.getOrderSubType(), OrdSubType.WANT.name());
        assertEquals(retWarrant.getOrderNumber(), "Warrant Order No.12345");
        assertEquals(retWarrant.getOrderDisposition(), warrant.getOrderDisposition());
        assertNotNull(retWarrant.getComments());
        assertEquals(retWarrant.getOrderIssuanceDate(), today);
        assertEquals(retWarrant.getOrderReceivedDate(), today);
        assertEquals(retWarrant.getOrderStartDate(), today);
        assertEquals(retWarrant.getOrderExpirationDate(), today);
        assertEquals(retWarrant.getCaseActivityInitiatedOrderAssociations(), caseActivityInitiatedOrderAssociations);
        assertEquals(retWarrant.getOrderInitiatedCaseActivityAssociations(), orderInitiatedCaseActivityAssociations);

        assertEquals(retWarrant.getIsHoldingOrder(), warrant.getIsHoldingOrder());
        assertEquals(retWarrant.getIsSchedulingNeeded(), warrant.getIsSchedulingNeeded());
        assertEquals(retWarrant.getHasCharges(), warrant.getHasCharges());
        assertEquals(retWarrant.getIssuingAgency(), warrant.getIssuingAgency());
        assertEquals(retWarrant.getCaseInfoIds(), warrant.getCaseInfoIds());
        assertEquals(retWarrant.getChargeIds(), warrant.getChargeIds());
        assertEquals(retWarrant.getConditionIds(), warrant.getConditionIds());
        assertEquals(retWarrant.getAgencyToBeNotified(), warrant.getAgencyToBeNotified());

        warrant.setNotificationLog(new HashSet<NotificationLogType>());
        assertEquals(retWarrant, warrant);
        WarrantDetainerType warrant2 = service.getOrder(uc, warrant.getOrderIdentification());
        assertNotNull(warrant2);
        assertEquals(warrant2, retWarrant);

        for (Long caseId : this.caseInfoIds) {
            orders = service.retrieveOrdersForCase(uc, caseId, null, true);
            assertNotNull(orders);
            assertTrue(orders.size() > 0);
            assertTrue(orders.contains(service.getOrder(uc, warrant.getOrderIdentification())));
        }

        assertNotNull(warrant.getCaseActivityInitiatedOrderAssociations());
        assertTrue(warrant.getCaseActivityInitiatedOrderAssociations().size() > 0);
        assertEquals(warrant.getCaseActivityInitiatedOrderAssociations().size(), 1);
        assertTrue(courtActivityIds.containsAll(warrant.getCaseActivityInitiatedOrderAssociations()));

        assertNotNull(warrant.getOrderInitiatedCaseActivityAssociations());
        assertTrue(warrant.getOrderInitiatedCaseActivityAssociations().size() > 0);
        assertEquals(warrant.getOrderInitiatedCaseActivityAssociations().size(), 2);
        assertTrue(courtActivityIds.containsAll(warrant.getOrderInitiatedCaseActivityAssociations()));

        for (Long orderInitiatedCaseActivityId : warrant.getOrderInitiatedCaseActivityAssociations()) {
            CourtActivityType ca = service.getCaseActivity(uc, orderInitiatedCaseActivityId);
            assertNotNull(ca);
            assertNotNull(ca.getOrderInititatedIds());
            assertTrue(ca.getOrderInititatedIds().size() > 0);
            assertEquals(ca.getOrderInititatedIds().size(), 1);
            assertEquals((Long) ca.getOrderInititatedIds().toArray()[0], warrant.getOrderIdentification());
        }

        for (Long caseActivityInitiatedOrderId : warrant.getCaseActivityInitiatedOrderAssociations()) {
            CourtActivityType ca = service.getCaseActivity(uc, caseActivityInitiatedOrderId);
            assertNotNull(ca);
            assertNotNull(ca.getOrderResultedIds());
            assertEquals(ca.getOrderResultedIds().size(), 1);
            assertTrue(ca.getOrderResultedIds().contains(warrant.getOrderIdentification()));
        }

        orderIds.add(orderId);

        // Bail
        BailType bailRet;
        BailType bail = new BailType();
        bail.setOrderClassification(OrdClassification.BAIL.name());
        bail.setOrderType(OrdType.TOO.name());
        bail.setOrderCategory(OrdCategory.IJ.name());
        bail.setOrderSubType(OrdSubType.DETAINER.name());
        bail.setOrderNumber("Bail No.12345");
        bail.setOrderDisposition(instanceOfDisposition(today));
        bail.setComments(instanceOfComment());
        bail.setOrderIssuanceDate(today);
        bail.setOrderReceivedDate(today);
        bail.setOrderStartDate(today);
        bail.setOrderExpirationDate(expirationDate);
        bail.setCaseActivityInitiatedOrderAssociations(new HashSet<Long>(Arrays.asList(new Long[] { courtActivityIds.get(0) })));
        bail.setOrderInitiatedCaseActivityAssociations(new HashSet<Long>(Arrays.asList(new Long[] { courtActivityIds.get(1), courtActivityIds.get(2) })));
        bail.setIsHoldingOrder(false);
        bail.setIsSchedulingNeeded(true);
        bail.setHasCharges(true);
        bail.setIssuingAgency(instanceOfNotification());

        bail.setCaseInfoIds(caseInfoIds);
        // bail.setChargeIds(new HashSet<Long>(Arrays.asList(new Long[] {400L, 500L, 600L})));
        // bail.setChargeIds(new HashSet<Long>());
        bail.setChargeIds(chargeIds);
        // bail.setConditionIds(new HashSet<Long>(Arrays.asList(new Long[] {4000L, 5000L, 6000L})));
        bail.setConditionIds(new HashSet<Long>());

        bail.setBailAmounts(new HashSet<BailAmountType>(Arrays.asList(
                new BailAmountType[] { new BailAmountType(BlType.CASH.name(), BigDecimal.valueOf(400.00), new HashSet<Long>(Arrays.asList(new Long[] { 400L }))),
                        new BailAmountType(BlType.PROP.name(), BigDecimal.valueOf(5000.00), new HashSet<Long>(Arrays.asList(new Long[] { 500L }))) })));
        bail.setBailRelationship(BlRelationship.AG.name());
        bail.setBailPostedAmounts(new HashSet<BailAmountType>(Arrays.asList(
                new BailAmountType[] { new BailAmountType(BlType.CASH.name(), BigDecimal.valueOf(2000.00), new HashSet<Long>(Arrays.asList(new Long[] { 400L }))),
                        new BailAmountType(BlType.PROP.name(), BigDecimal.valueOf(3000.00), new HashSet<Long>(Arrays.asList(new Long[] { 500L }))) })));
        bail.setBailPaymentReceiptNo("Bail paid no. 123456");
        bail.setBailRequirement("Bail Requirement");
        bail.setBailerPersonIdentityAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 111L, 222L, 333L })));
        bail.setBondPostedAmount(new BailAmountType(BlType.CASH.name(), BigDecimal.valueOf(2000.00), new HashSet<Long>(Arrays.asList(new Long[] { 400L }))));
        bail.setBondPaymentDescription("Bond($2000.00) has been paid.");
        bail.setBondOrganizationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 1111L, 2222L, 3333L })));
        bail.setIsBailAllowed(true);

        bailRet = service.createBail(uc, bail);
        assertNotNull(bailRet);
        bail.setOrderIdentification(bailRet.getOrderIdentification());
        assertEquals(bailRet, bail);
        BailType bail2 = service.getOrder(uc, bail.getOrderIdentification());
        assertNotNull(bail2);
        assertEquals(bail2, bail);

        for (Long caseId : this.caseInfoIds) {
            orders = service.retrieveOrdersForCase(uc, caseId, null, true);
            assertNotNull(orders);
            assertTrue(orders.size() > 0);
            assertTrue(orders.contains(bail2));
        }

        orderIds.add(bail.getOrderIdentification());

        // Sentence
        assertTrue(service.setSentenceNumberConfiguration(uc, true));
        SentenceType sentenceRet;
        SentenceType sentence = new SentenceType();
        sentence.setOrderClassification(OrdClassification.SENT.name());
        sentence.setOrderType(OrdType.SENTORD.name());
        sentence.setOrderCategory(OrdCategory.IJ.name());
        sentence.setOrderSubType(OrdSubType.DETAINER.name());
        sentence.setOrderNumber("Sentence No.12345");
        sentence.setOrderDisposition(instanceOfDisposition(getDate(2013, 3, 15)));
        sentence.setComments(instanceOfComment());
        sentence.setOrderIssuanceDate(getDate(2013, 3, 15));
        sentence.setOrderReceivedDate(getDate(2013, 3, 15));
        sentence.setOrderStartDate(getDate(2013, 3, 15));
        sentence.setOrderExpirationDate(null);
        sentence.setCaseActivityInitiatedOrderAssociations(new HashSet<Long>(Arrays.asList(new Long[] { courtActivityIds.get(0) })));
        sentence.setOrderInitiatedCaseActivityAssociations(new HashSet<Long>(Arrays.asList(new Long[] { courtActivityIds.get(1), courtActivityIds.get(2) })));
        sentence.setIsHoldingOrder(true);
        sentence.setIsSchedulingNeeded(false);
        sentence.setHasCharges(true);
        sentence.setIssuingAgency(instanceOfNotification());

        sentence.setCaseInfoIds(caseInfoIds);
        sentence.setChargeIds(new HashSet<Long>(Arrays.asList(new Long[] { arrayChargeIds[0] })));

        // sentence.setConditionIds(new HashSet<Long>(Arrays.asList(new Long[] {40000L, 50000L, 60000L})));
        sentence.setConditionIds(new HashSet<Long>());

        sentence.setSentenceType(SentType.DEF.name());
        sentence.setSentenceNumber(1L);
        TermType term = new TermType();
        term.setTermCategory(TmCategory.CUS.name());
        term.setTermName(TmName.MAX.name());
        term.setTermType(TmType.SINGLE.name());
        term.setTermStatus(TmStatus.INC.name());
        term.setMonths(6L);
        term.setTermSequenceNo(12345L);
        Set<TermType> sentenceTerms = new HashSet<TermType>();
        sentenceTerms.add(term);
        sentence.setSentenceTerms(sentenceTerms);
        sentence.setSentenceStatus(SentStatus.INC.name());
        sentence.setSentenceStartDate(getDate(2013, 3, 15));
        // sentence.setSentenceEndDate(endDate());
        sentence.setSentenceAppeal(new SentenceAppealType(AppealStatus.ACCEPTED.name(), "Accepted"));
        // sentence.setConcecutiveFrom(warrant.getOrderIdentification());
        sentence.setFineAmount(BigDecimal.valueOf(10000.00));
        sentence.setFineAmountPaid(true);
        sentence.setSentenceAggravateFlag(true);
        IntermittentScheduleType sch = new IntermittentScheduleType();
        sch.setDayOfWeek(Day.MON.name());
        sch.setStartTime(sch.new TimeValue(8L, 30L, 0L));
        sch.setEndTime(sch.new TimeValue(16L, 30L, 0L));
        Set<IntermittentScheduleType> intermittentSchedules = new HashSet<IntermittentScheduleType>();
        intermittentSchedules.add(sch);
        sentence.setIntermittentSchedule(intermittentSchedules);

        sentenceRet = service.createSentence(uc, sentence);
        assertNotNull(sentenceRet);
        assertNotNull(sentenceRet.getOrderIdentification());
        sentenceId1 = sentenceRet.getOrderIdentification();
        sentence.setOrderIdentification(sentenceRet.getOrderIdentification());
        // sentenceRet.setAssociations(null);
        // assertEquals(sentenceRet, sentence);
        SentenceType sentence2 = service.getOrder(uc, sentence.getOrderIdentification());
        assertNotNull(sentence2);
        //assertEquals(sentence2, sentence);
        assertEquals(sentence2, sentenceRet);
        assertEquals(sentence2.getSentenceNumber(), Long.valueOf(1));

        assertEquals(sentence2.getIntermittentSchedule(), intermittentSchedules);

        orderIds.add(sentence.getOrderIdentification());

        sentence2.setChargeIds(new HashSet<Long>(Arrays.asList(new Long[] { arrayChargeIds[1] })));
        // SentenceNumber
        // sentence2.setConcecutiveFrom(sentenceRet.getSentenceNumber());

        sentence2.setOrderIssuanceDate(getDate(2013, 4, 15));
        sentence2.setOrderStartDate(getDate(2013, 4, 15));
        sentence2.setOrderReceivedDate(getDate(2013, 4, 15));
        sentence2.setSentenceStartDate(getDate(2013, 4, 15));
        sentence2.setSentenceEndDate(null);
        sentence2.setOrderDisposition(instanceOfDisposition(getDate(2013, 4, 15)));

        sentence2 = service.createSentence(uc, sentence2);
        assertEquals(sentence2.getSentenceNumber(), Long.valueOf(2));
        sentenceId2 = sentence2.getOrderIdentification();

        for (Long caseId : this.caseInfoIds) {
            orders = service.retrieveOrdersForCase(uc, caseId, null, true);
            assertNotNull(orders);
            assertTrue(orders.size() > 0);
            assertTrue(orders.contains(service.getOrder(uc, sentence.getOrderIdentification())));
        }
        orderIds.add(sentence2.getOrderIdentification());

        SentenceType sentenceDef = sentence2;

        // Legal
        LegalOrderType legalRet;
        LegalOrderType legal = new LegalOrderType();
        legal.setOrderClassification(OrdClassification.LO.name());
        legal.setOrderType(OrdType.SENTORD.name());
        legal.setOrderCategory(OrdCategory.OJ.name());
        legal.setOjSupervisionId(supervisionId);
        // legal.setOrderSubType(new CodeType(ReferenceSet.ORDER_SUB_TYPE.value(), OrdSubType.DETAINER.name()));
        legal.setOrderNumber("Legal Order No.12345");
        legal.setOrderDisposition(instanceOfDisposition(today));
        legal.setComments(instanceOfComment());
        legal.setOrderIssuanceDate(today);
        legal.setOrderReceivedDate(today);
        legal.setOrderStartDate(today);
        legal.setOrderExpirationDate(expirationDate);
        legal.setCaseActivityInitiatedOrderAssociations(new HashSet<Long>(Arrays.asList(new Long[] { courtActivityIds.get(1) })));
        legal.setOrderInitiatedCaseActivityAssociations(new HashSet<Long>(Arrays.asList(new Long[] { courtActivityIds.get(1), courtActivityIds.get(2) })));
        legal.setIsHoldingOrder(true);
        legal.setIsSchedulingNeeded(false);
        legal.setHasCharges(true);
        legal.setIssuingAgency(instanceOfNotification());

        legal.setCaseInfoIds(caseInfoIds);
        // legal.setChargeIds(new HashSet<Long>(Arrays.asList(new Long[] {40000L, 50000L, 60000L})));
        legal.setChargeIds(new HashSet<Long>());
        // legal.setConditionIds(new HashSet<Long>(Arrays.asList(new Long[] {400000L, 500000L, 600000L})));
        legal.setConditionIds(new HashSet<Long>());
        legalRet = service.createLegalOrder(uc, legal);
        assertNotNull(legalRet);
        assertNotNull(legalRet.getOrderIdentification());
        legal.setOrderIdentification(legalRet.getOrderIdentification());
        assertEquals(legalRet, legal);
        LegalOrderType legal2 = service.getOrder(uc, legal.getOrderIdentification());
        assertNotNull(legal2);
        assertEquals(legal2, legal);

        for (Long caseId : this.caseInfoIds) {
            orders = service.retrieveOrdersForCase(uc, caseId, null, true);
            assertNotNull(orders);
            assertTrue(orders.size() > 0);
            assertTrue(orders.contains(service.getOrder(uc, legal.getOrderIdentification())));
        }

        orderIds.add(legal.getOrderIdentification());
    }

    @Test(dependsOnMethods = { "createAndGet" }, enabled = true)
    public void getMaxSentenceNumberBySupervisionId() {
        Long caseInfoId = caseInfoIds.iterator().next();
        Long supervisionId = service.getCaseInfo(uc, caseInfoId).getSupervisionId();
        Long maxSentenceNumber = service.getMaxSentenceNumberBySupervisionId(uc, supervisionId);
        assertNotNull(maxSentenceNumber);
        assertTrue(maxSentenceNumber.intValue() > 1);
        assertEquals(maxSentenceNumber.intValue(), 2, "Because 2 Sentences have just been created.");
    }

    @Test(dependsOnMethods = { "createAndGet" }, enabled = true)
    public void getMaxSentenceNumberByCaseInfo() {
        Long maxSentenceNumber = service.getMaxSentenceNumberByCaseInfo(uc, caseInfoIds);
        assertNotNull(maxSentenceNumber);
        assertTrue(maxSentenceNumber.intValue() > 1);
        assertEquals(maxSentenceNumber.intValue(), 2, "Because 2 Sentences have just been created.");
    }

    @Test(dependsOnMethods = { "createAndGet" }, enabled = true)
    public void retrieveSentenceIssuanceDate() {
        Long aId = 1234567L;
        Date sentenceIssuanceDate = null;
        try {
            sentenceIssuanceDate = service.retrieveSentenceIssuanceDateByChargeId(uc, 1234567L);
        } catch (DataNotExistException e) {
            // assertEquals(e.getMessage(), "Data not existed: Instance of syscon.arbutus.product.services.legal.realization.persistence.charge.ChargeEntity with id " + aId);
        }
        assertNull(sentenceIssuanceDate);

        assertTrue(chargeIds.size() > 0);
        sentenceIssuanceDate = service.retrieveSentenceIssuanceDateByChargeId(uc, (Long) chargeIds.toArray()[0]);
        assertNotNull(sentenceIssuanceDate);

        CaseActivitySearchType search = new CaseActivitySearchType();
        search.setActivityOutcome(CaseActivityOutcome.SENTC.name());
        search.setChargeIds(chargeIds);
        search.setChargeIdsSetMode("ALL");
        List<CourtActivityType> ret = service.searchCaseActivity(uc, null, search, false);
        assertTrue(ret.size() > 0);
        assertEquals(ret.size(), 1);
        for (CourtActivityType c : ret) {
            assertEquals(c.getActivityOccurredDate(), sentenceIssuanceDate);
        }
    }

    @Test(dependsOnMethods = { "createAndGet" }, enabled = true)
    public void retrieveSentencesBySupervisionId() {
        Long caseInfoId = caseInfoIds.iterator().next();
        Long supervisionId = service.getCaseInfo(uc, caseInfoId).getSupervisionId();
        List<SentenceType> ret = service.retrieveSentencesBySupervisionId(uc, supervisionId);
        assertNotNull(ret);
        assertFalse(ret.isEmpty());
        assertTrue(ret.size() > 0);
    }

    @Test(dependsOnMethods = { "createAndGet" }, enabled = true)
    public void getOrderBySupervisionIdForOutJurisdiction() {
        List<OrderType> ret = service.getOrderBySupervisionIdForOutJurisdiction(uc, supervisionId, null);
        assertNotNull(ret);
        assertFalse(ret.isEmpty());
        assertTrue(ret.size() > 0);
        OrderType order = (OrderType) ret.toArray()[0];
        assertEquals(order.getOjSupervisionId(), supervisionId);
    }

    @Test(dependsOnMethods = { "createAndGet" }, enabled = true)
    public void updateOrder() {
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        cal.add(Calendar.YEAR, 1);
        Date expirationDate = cal.getTime();
        Date termEndDate = cal.getTime();
        Date sentenceEndDate = cal.getTime();

        try {
            assertNull(service.updateBail(uc, null));
        } catch (InvalidInputException e) {
            assertEquals(e.getMessage(), "Invalid argument: null order");
        }
        Long warrantId = null;
        for (Long orderId : orderIds) {
            Object obj = service.getOrder(uc, orderId);

            // WarrantDetainer
            if (obj instanceof WarrantDetainerType) {
                WarrantDetainerType warrant = (WarrantDetainerType) obj;
                warrantId = warrant.getOrderIdentification();
                assertNotNull(warrant);
                WarrantDetainerType updated = service.updateWarrantDetainer(uc, warrant);
                assertEquals(updated, warrant);
                assertNotEquals(updated.getOrderExpirationDate(), expirationDate);
                warrant.setOrderExpirationDate(expirationDate);
                updated = service.updateWarrantDetainer(uc, warrant);
                assertEquals(updated, warrant);

                warrant.setIssuingAgency(instanceOfUpdatedNotification());
                updated = service.updateWarrantDetainer(uc, warrant);
                assertNotNull(updated);
                assertEquals(updated, warrant);

                warrant.setIssuingAgency(null);
                updated = service.updateWarrantDetainer(uc, warrant);
                assertNotNull(updated);
                assertEquals(updated, warrant);

                warrant.setIssuingAgency(instanceOfUpdatedNotification());
                updated = service.updateWarrantDetainer(uc, warrant);
                assertNotNull(updated);
                assertEquals(updated, warrant);

                Set<Long> caseActivityInitiatedOrderAssociations = new HashSet<Long>();
                caseActivityInitiatedOrderAssociations.add(courtActivityIds.get(1));
                warrant.setCaseActivityInitiatedOrderAssociations(caseActivityInitiatedOrderAssociations);

                Set<Long> orderInitiatedCaseActivityAssociations = new HashSet<Long>();
                orderInitiatedCaseActivityAssociations.add(courtActivityIds.get(0));
                orderInitiatedCaseActivityAssociations.add(courtActivityIds.get(2));
                warrant.setOrderInitiatedCaseActivityAssociations(orderInitiatedCaseActivityAssociations);
                updated = service.updateWarrantDetainer(uc, warrant);
                assertNotNull(updated);
                assertEquals(updated, warrant);

                caseActivityInitiatedOrderAssociations = new HashSet<Long>();
                warrant.setCaseActivityInitiatedOrderAssociations(caseActivityInitiatedOrderAssociations);
                orderInitiatedCaseActivityAssociations = new HashSet<Long>();
                warrant.setOrderInitiatedCaseActivityAssociations(orderInitiatedCaseActivityAssociations);
                updated = service.updateWarrantDetainer(uc, warrant);
                assertNotNull(updated);
                assertEquals(updated, warrant);

                warrant.setCaseActivityInitiatedOrderAssociations(null);
                warrant.setOrderInitiatedCaseActivityAssociations(null);
                updated = service.updateWarrantDetainer(uc, warrant);
                assertNotNull(updated);
                warrant.setCaseActivityInitiatedOrderAssociations(new HashSet<Long>());
                warrant.setOrderInitiatedCaseActivityAssociations(new HashSet<Long>());
                assertEquals(updated, warrant);

                warrant.setIssuingAgency(null);
                updated = service.updateWarrantDetainer(uc, warrant);
                assertNotNull(updated);

                warrant.setAgencyToBeNotified(null);
                updated = service.updateWarrantDetainer(uc, warrant);
                assertNotNull(updated);

                warrant.setAgencyToBeNotified(new HashSet<NotificationType>());
                updated = service.updateWarrantDetainer(uc, warrant);
                assertNotNull(updated);

                // create 3 charges
                createSignleChargePositive();
                createSignleChargePositive();
                createSignleChargePositive();
                assertNotNull(chargeIds);
                assertEquals(chargeIds.size(), 5);
                // set 3 charges to warrant
                warrant.setChargeIds(chargeIds);
                // to update with 3 associated charges
                updated = service.updateWarrantDetainer(uc, warrant);
                assertNotNull(updated);
                assertEquals(updated, warrant);
                assertNotNull(updated.getChargeIds());
                // the updated warrant has 3 associated charges
                assertEquals(updated.getChargeIds().size(), 5);
                assertTrue(updated.getChargeIds().containsAll(chargeIds));
                assertTrue(chargeIds.containsAll(updated.getChargeIds()));
                WarrantDetainerType ret = service.getOrder(uc, orderId);
                assertNotNull(ret);
                assertNotNull(ret.getChargeIds());
                assertEquals(ret.getChargeIds().size(), 5);
                assertTrue(ret.getChargeIds().containsAll(chargeIds));
                assertTrue(chargeIds.containsAll(ret.getChargeIds()));

                Long aChargeId = (Long) (chargeIds.toArray()[0]);
                chargeIds.remove(aChargeId);
                assertNotNull(chargeIds);
                // 2 charges
                assertEquals(chargeIds.size(), 4);
                // set 2 charges to warrant
                warrant.setChargeIds(chargeIds);
                // to update with 2 associated charges
                updated = service.updateWarrantDetainer(uc, warrant);
                assertNotNull(updated);
                assertEquals(updated, warrant);
                assertNotNull(updated.getChargeIds());
                // the updated warrant has 2 associated charges
                assertEquals(updated.getChargeIds().size(), 4);
                assertTrue(updated.getChargeIds().containsAll(chargeIds));
                assertTrue(chargeIds.containsAll(updated.getChargeIds()));
                ret = service.getOrder(uc, orderId);
                assertNotNull(ret);
                assertNotNull(ret.getChargeIds());
                assertEquals(ret.getChargeIds().size(), 4);
                assertTrue(ret.getChargeIds().containsAll(chargeIds));
                assertTrue(chargeIds.containsAll(ret.getChargeIds()));
            }

            // Bail
            if (obj instanceof BailType) {
                BailType bail = (BailType) obj;
                assertNotNull(bail);
                BailType updated = service.updateBail(uc, bail);
                assertEquals(updated, bail);
                updated = service.updateBail(uc, updated);
                assertEquals(updated, bail);

                bail.setBailAmounts(new HashSet<BailAmountType>(Arrays.asList(
                        new BailAmountType[] { new BailAmountType(BlType.CASH.name(), BigDecimal.valueOf(20.00), new HashSet<Long>(Arrays.asList(new Long[] { 400L }))),
                                new BailAmountType(BlType.PROP.name(), BigDecimal.valueOf(30.00), new HashSet<Long>(Arrays.asList(new Long[] { 500L }))) })));
                bail.setBailRelationship(BlRelationship.AG.name());
                bail.setBailPostedAmounts(new HashSet<BailAmountType>(Arrays.asList(
                        new BailAmountType[] { new BailAmountType(BlType.CASH.name(), BigDecimal.valueOf(2.00), new HashSet<Long>(Arrays.asList(new Long[] { 400L }))),
                                new BailAmountType(BlType.PROP.name(), BigDecimal.valueOf(3.00), new HashSet<Long>(Arrays.asList(new Long[] { 500L }))) })));

                updated = service.updateBail(uc, bail);
                assertEquals(updated, bail);
            }

            // Sentence
            if (obj instanceof SentenceType) {
                SentenceType sentence = (SentenceType) obj;
                assertNotNull(sentence);
                SentenceType updated = service.updateSentence(uc, sentence);
                assertEquals(updated, sentence);
                updated = service.updateSentence(uc, sentence);
                assertEquals(updated, sentence);

                //				Set<TermType> terms = sentence.getSentenceTerms();
                //				for (TermType t: terms) {
                //					t.setDays(10L);
                //				}
                // sentence.setSentenceTerms(null);
                updated = service.updateSentence(uc, sentence);
            }

            // Legal
            if (obj instanceof LegalOrderType) {
                LegalOrderType legal = (LegalOrderType) obj;
                assertNotNull(legal);
                LegalOrderType updated = service.updateLegalOrder(uc, legal);
                assertEquals(updated, legal);
                updated = service.updateLegalOrder(uc, updated);
                assertEquals(updated, legal);
            }
        }
        /*List<WarrantDetainerType> warrantDetainerTypeList = service.retrieveOrderHistory(uc, warrantId, null, null);
        int i = 0;
        for (WarrantDetainerType warrantDetainerType: warrantDetainerTypeList) {
            System.out.println("No.: " + ++i + ": " + warrantDetainerType);
            System.out.println();
        }*/
    }

    @Test(dependsOnMethods = { "updateOrder" }, enabled = true)
    public void searchOrder() {
        OrderSearchType search = new OrderSearchType();
        Set<Long> caseInfoIds = this.caseInfoIds;
        if (caseInfoIds != null) {
            for (Long id : caseInfoIds) {
                search.setOrderClassification(OrdClassification.WD.name());
                @SuppressWarnings("rawtypes") List ret = service.retrieveOrdersForCase(uc, id, search, true);
                log.info("###########################################");
                log.info("service.retrieveOrdersForCase(" + id + ", " + search + ") = \n\n" + ret);
                log.info("###########################################");
                assertNotNull(ret);
                assertEquals(ret.size(), 1);

                search.setOrderClassification(OrdClassification.BAIL.name());
                ret = service.retrieveOrdersForCase(uc, id, search, true);
                log.info("###########################################");
                log.info("service.retrieveOrdersForCase(" + id + ", " + search + ") = \n\n" + ret);
                log.info("###########################################");
                assertNotNull(ret);
                assertEquals(ret.size(), 1);
            }
        }

        search = new OrderSearchType();
        try {
            service.searchOrders(uc, null, null, null, null, null, null, null, null, null);
        } catch (InvalidInputException e) {
            assertEquals(e.getMessage(), NULL_ERROR_MESSAGE);
        }

        OrdersReturnType ret;
        // Sentence
        search.setOrderClassification("");
        try {
            ret = service.searchOrders(uc, null, null, null, null, search, null, null, null, null);
        } catch (InvalidInputException e) {
            assertEquals(e.getMessage(), SEARCH_ERROR_MESSAGE);
        }

        search.setOrderClassification(OrdClassification.SENT.name());
        ret = service.searchOrders(uc, null, null, null, null, search, null, null, null, null);
        assertTrue(ret.getTotalSize().intValue() > 1);
        assertNotNull(ret.getOrders());
        assertTrue(ret.getOrders().size() > 0);

        search = new OrderSearchType();
        TermSearchType term = new TermSearchType();
        term.setTermName("MAX");
        search.setSentenceTerm(term);
        ret = service.searchOrders(uc, null, null, null, null, search, null, null, null, null);
        assertTrue(ret.getTotalSize().intValue() > 0);
        assertNotNull(ret.getOrders());
        assertTrue(ret.getOrders().size() > 0);
    }

    @Test(dependsOnMethods = { "updateOrder" }, enabled = true)
    public void retrieveOrderHistory() {
        Set<Long> ids = service.getAll(uc, LegalModule.ORDER_SENTENCE);
        assertNotNull(ids);
        assertTrue(ids.size() > 0);
        int i = 0;
        for (Long id : ids) {
            List<OrderType> orderTypeList = service.retrieveOrderHistory(uc, id, null, null);
            assertNotNull(orderTypeList);
            for (OrderType orderType : orderTypeList) {
                log.info("Order History: " + orderType);
                /*System.out.println("No.: " + ++i + " Order Disposition History: " + orderType.getOrderDisposition());
                System.out.println();*/
            }
        }
    }

	/*private Session getSession() {history = historyRet.getCaseInfos().iterator().next();
            assertEquals(history.getSentenceStatus(), SENTENCE_STATUS_SENTENCED);
		
		BasicConfigurator.configure();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("arbutus-legal-test-unit");
	    EntityManager em = emf.createEntityManager();
	    return (Session)em.getDelegate();
	    
	}*/

    //for local debug use only
	/*private void localLogin() {
		
		service = new CaseManagementServiceBean();
		//init user context and login
		uc = super.getUserContext();
		super.login();
		
		service.setSession(getSession());
		
	}*/

    @Test(dependsOnMethods = { "updateOrder" }, enabled = true)
    public void retrieveDispositionHistoryByOrderId() {
        Set<Long> ids = service.getAll(uc, LegalModule.ORDER_SENTENCE);
        assertNotNull(ids);
        assertTrue(ids.size() > 0);
        for (Long id : ids) {
            List<DispositionType> dispositionTypeList = service.retrieveDispositionHistoryByOrderId(uc, id, null, null);
            for (DispositionType dispositionType : dispositionTypeList) {
                log.info("Disposition History: " + dispositionType);
            }
        }
    }

    @Test(dependsOnMethods = { "searchOrder" }, enabled = true)
    public void deleteOrder() {
        List<Long> ordIDs = new ArrayList(orderIds);
        Collections.sort(ordIDs);
        Collections.reverse(ordIDs);
        for (Long id : ordIDs) {
            OrderType order = service.getOrder(uc, id);
            assertNotNull(order);
            assertEquals(service.deleteOrder(uc, id), success);
        }
        @SuppressWarnings("rawtypes") OrdersReturnType ret = service.searchOrders(uc, orderIds, null, null, null, null, false, null, null, null);
        assertEquals(ret.getTotalSize().intValue(), 0);
        assertNotNull(ret.getOrders());
        assertEquals(ret.getOrders().size(), 0);
        OrderSearchType search = new OrderSearchType();
        search.setOrderClassification(OrdClassification.SENT.name());
        OrdersReturnType searchRet = service.searchOrders(uc, null, null, null, null, search, null, null, null, null);
        assertEquals(searchRet.getTotalSize().intValue(), 0);
        assertTrue(searchRet.getOrders().isEmpty());
    }

    private CommentType instanceOfComment() {
        CommentType comment = new CommentType();
        comment.setCommentDate(new Date());
        comment.setComment("Order Comment");
        comment.setUserId("devtest");

        return comment;
    }

    private DispositionType instanceOfDisposition(Date dispositionDate) {
        DispositionType disposition = new DispositionType();
        disposition.setDispositionOutcome(OrdOutcome.SENT.name());
        disposition.setOrderStatus(OrdStatus.ACTIVE.name());
        disposition.setDispositionDate(dispositionDate);

        return disposition;
    }

    private NotificationType instanceOfNotification() {
        NotificationType notification = new NotificationType();
        notification.setNotificationOrganizationAssociation(1L);
        notification.setNotificationFacilityAssociation(204L);
        notification.setIsNotificationNeeded(true);
        notification.setIsNotificationConfirmed(true);
        notification.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6500L, 6600L })));
        ContactType contact1 = getContact("Syscon Justice");
        ContactType contact2 = getContact("Richmond Court");
        ContactType contact3 = getContact("BC Govt");
        Set<ContactType> contacts = new HashSet<>();
        contacts.add(contact1);
        contacts.add(contact2);
        contacts.add(contact3);
        notification.setNotificationAgencyContacts(contacts);
        return notification;
    }

    private NotificationType instanceOfUpdatedNotification() {
        NotificationType notification = new NotificationType();
        notification.setNotificationOrganizationAssociation(100L);
        notification.setNotificationFacilityAssociation(20400L);
        notification.setIsNotificationNeeded(true);
        notification.setIsNotificationConfirmed(true);
        notification.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6501L, 6601L })));
        ContactType contact1 = getContact("Syscon Justice");
        ContactType contact2 = getContact("Richmond Court");
        ContactType contact3 = getContact("BC Govt");
        ContactType contact4 = getContact("Canada High Court");
        Set<ContactType> contacts = new HashSet<>();
        contacts.add(contact1);
        contacts.add(contact2);
        contacts.add(contact3);
        contacts.add(contact4);
        notification.setNotificationAgencyContacts(contacts);
        return notification;
    }

    private Set<NotificationType> instancesOfNotification() {
        Set<NotificationType> notifications = new HashSet<NotificationType>();
        NotificationType notification = new NotificationType();
        notification.setNotificationOrganizationAssociation(1L);
        notification.setNotificationFacilityAssociation(204L);
        notification.setIsNotificationNeeded(true);
        notification.setIsNotificationConfirmed(true);
        notification.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6500L, 6600L })));
        ContactType contact1 = getContact("Syscon Justice");
        ContactType contact2 = getContact("Richmond Court");
        ContactType contact3 = getContact("BC Govt");
        Set<ContactType> contacts = new HashSet<>();
        contacts.add(contact1);
        contacts.add(contact2);
        contacts.add(contact3);
        notification.setNotificationAgencyContacts(contacts);
        notifications.add(notification);

        NotificationType notification2 = new NotificationType();
        notification2.setNotificationOrganizationAssociation(2L);
        notification2.setNotificationFacilityAssociation(205L);
        notification2.setIsNotificationNeeded(true);
        notification2.setIsNotificationConfirmed(true);
        notification2.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6700L, 6800L })));
        ContactType contact4 = getContact("Canada High Court");
        contacts.add(contact4);
        notification2.setNotificationAgencyContacts(contacts);
        notifications.add(notification2);
        return notifications;
    }

    private Set<NotificationType> instancesOfNotification2() {
        Set<NotificationType> notifications = new HashSet<NotificationType>();
        NotificationType notification = new NotificationType();
        notification.setNotificationOrganizationAssociation(2L);
        notification.setNotificationFacilityAssociation(205L);
        notification.setIsNotificationNeeded(true);
        notification.setIsNotificationConfirmed(true);
        notification.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6500L, 6600L })));
        ContactType contact1 = getContact("Syscon Justice");
        ContactType contact2 = getContact("Richmond Court");
        ContactType contact3 = getContact("BC Govt");
        Set<ContactType> contacts = new HashSet<>();
        contacts.add(contact1);
        contacts.add(contact2);
        contacts.add(contact3);
        notification.setNotificationAgencyContacts(contacts);
        notifications.add(notification);

        NotificationType notification2 = new NotificationType();
        notification2.setNotificationOrganizationAssociation(3L);
        notification2.setNotificationFacilityAssociation(206L);
        notification2.setIsNotificationNeeded(true);
        notification2.setIsNotificationConfirmed(true);
        notification2.setNotificationAgencyLocationAssociations(new HashSet<Long>(Arrays.asList(new Long[] { 6701L, 6801L })));
        ContactType contact4 = getContact("Canada High Court");
        contacts.add(contact4);
        notification2.setNotificationAgencyContacts(contacts);
        notification2.setNotificationAgencyContacts(contacts);
        notifications.add(notification2);
        return notifications;
    }

    private Set<NotificationLogType> instancesOfNotificationLog() {
        Set<NotificationLogType> notificationLogs = new HashSet<NotificationLogType>();
        Calendar cal = Calendar.getInstance();
        Date notificationDate = cal.getTime();

        NotificationLogType nLog1 = new NotificationLogType();
        nLog1.setNotificationDate(notificationDate);
        // nLog1.setNotificationType(notificationType)

        return notificationLogs;
    }

    private void createCaseInfo() {

        // Create only mandatory fields
        CaseInfoType caseInfo = generateDefaultCaseInfo(true);

        CaseInfoType ret = service.createCaseInfo(uc, caseInfo, null);

        ret = service.getCaseInfo(uc, ret.getCaseInfoId());

        createdCaseInfos.add(ret);
        caseInfoIds.add(ret.getCaseInfoId());

    }

    private CaseInfoType generateDefaultCaseInfo(boolean onlyMandatory) {

        //createUpdateSearchCaseIdentifierConfigPositive();

        CaseInfoType caseInfo = new CaseInfoType();
        Date currentDate = new Date();

        //Mandatory fields:
        caseInfo.setCaseCategory(CASE_CATEGORY_IJ);
        caseInfo.setCaseType(CASE_TYPE_ADULT);
        caseInfo.setCreatedDate(currentDate);
        caseInfo.setDiverted(true);
        caseInfo.setDivertedSuccess(true);
        caseInfo.setActive(true);
        caseInfo.setSupervisionId(supervisionId);

        if (onlyMandatory) {
            return caseInfo;
        }

        //Add optional fields:
        caseInfo.setIssuedDate(currentDate);
        caseInfo.setSentenceStatus(SENTENCE_STATUS_SENTENCED);
        caseInfo.setFacilityId(1L);

        CommentType comment = new CommentType();
        comment.setUserId("devtest");
        comment.setCommentDate(currentDate);
        comment.setComment("createCaseInfoPositive");
        caseInfo.setComment(comment);

        //caseInfo.getCaseActivityIds().add(1L);

        //Add case identifiers
        CaseIdentifierType caseIdentifier1 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_DOCKET, "0001A", false, 1L, 1L, "createCaseInfoPositive comment1", true);

        CaseIdentifierType caseIdentifier2 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_INDICTMENT, "0002", false, 1L, 1L, "createCaseInfoPositive comment2",
                false);

        caseInfo.getCaseIdentifiers().add(caseIdentifier1);
        caseInfo.getCaseIdentifiers().add(caseIdentifier2);

        return caseInfo;
    }

    public void createUpdateSearchCaseIdentifierConfigPositive() {

        //Create
        CaseIdentifierConfigType dto = new CaseIdentifierConfigType(null, CASE_IDENTIFIER_TYPE_DOCKET, CASE_IDENTIFIER_FORMAT_000000A, true, true, true, true);

        CaseIdentifierConfigType ret = service.createCaseIdentifierConfig(uc, dto);

        createdConfigs.add(ret);

        dto = new CaseIdentifierConfigType(null, CASE_IDENTIFIER_TYPE_DOCKET, CASE_IDENTIFIER_FORMAT_000000A, false, false, false, false);

        ret = service.createCaseIdentifierConfig(uc, dto);

        createdConfigs.add(ret);
    }

    public void createSignleChargePositive() {

        createStatuteChargePositive();

        //Create single charge instance
        ChargeType dto = getChargeType(1);

        ChargeType ret = service.createCharge(uc, dto);
        assert (ret != null);

        // Test new added display fields.
        Assert.assertTrue(ret.getChargeCode() != null);
        Assert.assertTrue(ret.getChargeDegree() != null);
        Assert.assertTrue(ret.getChargeDescription() != null);
        Assert.assertTrue(ret.getChargeSeverity() != null);
        Assert.assertTrue(ret.getChargeType() != null);

        chargeIds.add(ret.getChargeId());
    }

    private void createStatuteChargePositive() {

        StatuteChargeConfigType dto = new StatuteChargeConfigType();

        //Set mandatory fields
        dto.setStatuteId(1L);
        dto.setChargeCode(CHARGE_CODE_1510);
        dto.setChargeText(getStatuteChargeTextType());
        dto.setBailAllowed(true);
        dto.setBondAllowed(false);

        dto.setStartDate(createPastDate(10));
        dto.setEndDate(createFutureDate(1000));
        dto.setChargeType(CHARGE_TYPE_F);

        //Set optional fields
        dto.setBailAmount(10000L);

        Set<Long> externalChargeCodeIds = new HashSet<Long>();
        externalChargeCodeIds.add(1L);
        externalChargeCodeIds.add(2L);
        dto.setExternalChargeCodeIds(externalChargeCodeIds);

        dto.setChargeDegree(CHARGE_DEGREE_I);
        dto.setChargeCategory(CHARGE_CATEGORY_T);
        dto.setChargeSeverity(CHARGE_SEVERITY_H);

        Set<ChargeIndicatorType> chargeIndicators = new HashSet<ChargeIndicatorType>();
        chargeIndicators.add(getChargeIndicatorType());
        dto.setChargeIndicators(chargeIndicators);

        dto.setEnhancingFactors(getChargeEnhancingFactors());
        dto.setReducingFactors(getChargeReducingFactors());

        StatuteChargeConfigType ret = service.createStatuteCharge(uc, dto);

        assert (ret != null);

        // verifyStatuteChargeConfigType(ret, dto);
        statuteChargeConfigId = ret.getStatuteChargeId();
    }

    private ChargeType getChargeType(long chargeCount) {
        // createCaseInfo();

        ChargeType dto = new ChargeType();

        //get StatuteChargeConfigs
        StatuteChargeConfigSearchType search = new StatuteChargeConfigSearchType();
        search.setChargeCode(CHARGE_CODE_1510);
        StatuteChargeConfigsReturnType rtn = service.searchStatuteCharge(uc, search, null, null, null);

        //Mandatory properties
        dto.setStatuteChargeId(rtn.getStatuteChargeConfigs().get(0).getStatuteChargeId());

        Date now = new Date();
        ChargeDispositionType chargeDisposition = new ChargeDispositionType(CHARGE_OUTCOME_SENT, CHARGE_STATUS_ACTIVE, now);
        dto.setChargeDisposition(chargeDisposition);
        dto.setPrimary(true);
        Long caseInfoId = (Long) this.caseInfoIds.toArray()[0];
        dto.setCaseInfoId(caseInfoId);

        //Optional properties
        Set<ChargeIdentifierType> chargeIdentifiers = new HashSet<ChargeIdentifierType>();
        chargeIdentifiers.add(getChargeIdentifierType());
        dto.setChargeIdentifiers(chargeIdentifiers);

        Set<ChargeIndicatorType> chargeIndicators = new HashSet<ChargeIndicatorType>();
        chargeIndicators.add(getChargeIndicatorType());
        dto.setChargeIndicators(chargeIndicators);

        dto.setLegalText("Legal Text");
        dto.setChargeCount(chargeCount);
        dto.setFacilityId(1L);
        dto.setOrganizationId(1L);
        dto.setPersonIdentityId(1L);
        dto.setOffenceStartDate(now);
        dto.setOffenceEndDate(createFutureDate(10));
        dto.setFilingDate(now);

        ChargePleaType chargePlea = new ChargePleaType(CHARGE_PLEA_G, "CHARGE_PLEA_G creation");
        dto.setChargePlea(chargePlea);

        ChargeAppealType chargeAppeal = new ChargeAppealType(APPEAL_STATUS_INPROGRESS, "APPEAL_STATUS_INPROGRESS creation");
        dto.setChargeAppeal(chargeAppeal);

        // dto.setConditionIds(getModuleIdSet());
        // dto.setOrderSentenceIds(getModuleIdSet());

        CommentType comment = new CommentType();
        comment.setComment("Create Charge");
        comment.setCommentDate(now);
        comment.setUserId("2");
        dto.setComment(comment);

        return dto;
    }

    private ChargeIdentifierType getChargeIdentifierType() {

        ChargeIdentifierType ret = new ChargeIdentifierType(null, CHARGE_IDENTIFIER_CJIS, "1-23456", CHARGE_IDENTIFIER_FORMAT_0_00000, 1L, 1L,
                "Comment getChargeIdentifierType");

        return ret;

    }

    private void verifyStatuteChargeConfigType(StatuteChargeConfigType actual, StatuteChargeConfigType expected) {

        assertNotNull(actual.getStatuteChargeId());
        assertEquals(actual.getStatuteId(), expected.getStatuteId());
        assertEquals(actual.getChargeCode(), expected.getChargeCode());

        assertEquals(actual.getChargeText(), expected.getChargeText());

        assertEquals(actual.getBailAmount(), expected.getBailAmount());
        assertEquals(actual.isBailAllowed(), expected.isBailAllowed());
        assertEquals(actual.isBondAllowed(), expected.isBondAllowed());
        assertEquals(actual.getExternalChargeCodeIds(), expected.getExternalChargeCodeIds());
        assertEquals(actual.getStartDate(), expected.getStartDate());
        assertEquals(actual.getEndDate(), expected.getEndDate());
        assertEquals(actual.getChargeType(), expected.getChargeType());
        assertEquals(actual.getChargeDegree(), expected.getChargeDegree());
        assertEquals(actual.getChargeCategory(), expected.getChargeCategory());
        assertEquals(actual.getChargeSeverity(), expected.getChargeSeverity());

        assertEquals(actual.getChargeIndicators(), expected.getChargeIndicators());

        assertEquals(actual.getEnhancingFactors(), expected.getEnhancingFactors());
        assertEquals(actual.getReducingFactors(), expected.getReducingFactors());

    }

    private Set<String> getChargeEnhancingFactors() {

        Set<String> ret = new HashSet<String>();
        ret.add(CHARGE_ENHANCING_FACTOR_RACE);
        ret.add(CHARGE_ENHANCING_FACTOR_SEX);
        return ret;

    }

    private Set<String> getChargeReducingFactors() {

        Set<String> ret = new HashSet<String>();
        ret.add(CHARGE_REDUCING_FACTOR_MI);
        ret.add(CHARGE_REDUCING_FACTOR_MIN);
        return ret;
    }

    private StatuteChargeTextType getStatuteChargeTextType() {

        StatuteChargeTextType ret = new StatuteChargeTextType(CHARGE_LANGUAGE_EN, "StatuteChargeTextType desc", "StatuteChargeTextType legal text");

        return ret;
    }

    private ChargeIndicatorType getChargeIndicatorType() {

        ChargeIndicatorType ret = new ChargeIndicatorType(null, CHARGE_INDICATOR_ACC, true, "1-23456");

        return ret;

    }

    private void createCaseActivities() {
        //courtActivity1: madatory fields
        CourtActivityType courtActivity = new CourtActivityType();

        //test ActivityCategory has value.
        courtActivity.setActivityCategory(CaseActivityCategory.COURT.name());

        //test ActivityType has value.
        courtActivity.setActivityType(CaseActivityType.INITCRT.name());

        //test when ActivityStatus has value.
        courtActivity.setActivityStatus(CaseActivityStatus.ACTIVE.name());
        // courtActivity.setActivityOutcome(CaseActivityOutcome.SENTC.name());

        //test when ActivityOccurredDate has value.
        courtActivity.setActivityOccurredDate(new Date());

        courtActivity.getCaseIds().add(createdCaseInfos.get(0).getCaseInfoId());

        //test all mandatory fields have values, create should pass now.
        CourtActivityType rtn = service.createCaseActivity(uc, courtActivity);

        assertNotNull(rtn, null);

        CourtActivityType createdCourtActivity = rtn;
        assertNotNull(createdCourtActivity);

        assertNotNull(createdCourtActivity.getCaseActivityIdentification());
        assertEquals(createdCourtActivity.getActivityCategory(), courtActivity.getActivityCategory());
        assertEquals(createdCourtActivity.getActivityType(), courtActivity.getActivityType());
        assertEquals(createdCourtActivity.getActivityOccurredDate(), courtActivity.getActivityOccurredDate());
        assertEquals(createdCourtActivity.getActivityStatus(), courtActivity.getActivityStatus());
        //TODO: assertEquals(createdCourtActivity.getAssociations().size(), courtActivity.getAssociations().size());

        createdCourtActivities.add(createdCourtActivity);
        courtActivityIds.add(createdCourtActivity.getCaseActivityIdentification());

        //test cretae activity with all fields have values
        courtActivity = generateDefaultCourtActivity(false);

        rtn = service.createCaseActivity(uc, courtActivity);

        Date afterCreate = new Date();

        assertNotNull(rtn, null);

        createdCourtActivity = rtn;
        assertNotNull(createdCourtActivity);

        //test getStamp for create API:
        StampType stamp = service.getStamp(uc, LegalModule.CASE_ACTIVITY, createdCourtActivity.getCaseActivityIdentification());
        Assert.assertTrue(stamp.getCreateDateTime().getTime() < afterCreate.getTime());
        //TODO: Assert.assertTrue(stamp.getCreateDateTime().getTime() > beforeCreate.getTime());
        Assert.assertTrue(stamp.getModifyDateTime().before(afterCreate));
        //TODO Assert.assertTrue(stamp.getModifyDateTime().after(beforeCreate));
        Assert.assertTrue("devtest".equals(stamp.getCreateUserId()));
        Assert.assertTrue("devtest".equals(stamp.getModifyUserId()));
        Assert.assertTrue(stamp.getInvocationContext() instanceof String);

        assertNotNull(createdCourtActivity.getCaseActivityIdentification());
        assertEquals(createdCourtActivity.getActivityCategory(), courtActivity.getActivityCategory());
        assertEquals(createdCourtActivity.getActivityType(), courtActivity.getActivityType());
        assertEquals(createdCourtActivity.getActivityOccurredDate(), courtActivity.getActivityOccurredDate());
        assertEquals(createdCourtActivity.getActivityStatus(), courtActivity.getActivityStatus());
        //TODO:assertEquals(createdCourtActivity.getAssociations().size(), courtActivity.getAssociations().size());
        // assertEquals(createdCourtActivity.getChargeIds().size(), courtActivity.getChargeIds().size());
        // assertEquals(createdCourtActivity.getOrderInititatedIds().size(), courtActivity.getOrderInititatedIds().size());
        // assertEquals(createdCourtActivity.getOrderResultedIds().size(), courtActivity.getOrderResultedIds().size());
        // assertEquals(createdCourtActivity.getFacilityId(), courtActivity.getFacilityId());
        // assertEquals(createdCourtActivity.getFacilityInternalLocationId(), courtActivity.getFacilityInternalLocationId());

        createdCourtActivities.add(createdCourtActivity);
        courtActivityIds.add(createdCourtActivity.getCaseActivityIdentification());

        //test create activity with same values again since SDD has not limit to create duplicated activities.
        courtActivity = generateDefaultCourtActivity(false);

        rtn = service.createCaseActivity(uc, courtActivity);

        assertNotNull(rtn);

        createdCourtActivity = rtn;
        assertNotNull(createdCourtActivity);

        assertNotNull(createdCourtActivity.getCaseActivityIdentification());
        assertEquals(createdCourtActivity.getActivityCategory(), courtActivity.getActivityCategory());
        assertEquals(createdCourtActivity.getActivityType(), courtActivity.getActivityType());
        assertEquals(createdCourtActivity.getActivityOccurredDate(), courtActivity.getActivityOccurredDate());
        assertEquals(createdCourtActivity.getActivityStatus(), courtActivity.getActivityStatus());
        //TODO:assertEquals(createdCourtActivity.getAssociations().size(), courtActivity.getAssociations().size());
        //		assertEquals(createdCourtActivity.getChargeIds().size(), courtActivity.getChargeIds().size());
        //		assertEquals(createdCourtActivity.getOrderInititatedIds().size(), courtActivity.getOrderInititatedIds().size());
        //		assertEquals(createdCourtActivity.getOrderResultedIds().size(), courtActivity.getOrderResultedIds().size());

        createdCourtActivities.add(createdCourtActivity);
        courtActivityIds.add(createdCourtActivity.getCaseActivityIdentification());

        // Case Activity on Court for Sentence
        courtActivity = generateDefaultCourtActivity(false);
        courtActivity.setActivityCategory(CaseActivityCategory.COURT.name());
        courtActivity.setActivityType(CaseActivityType.SENTENCE.name());
        courtActivity.setActivityOutcome(CaseActivityOutcome.SENTC.name());
        courtActivity.setActivityStatus(CaseActivityStatus.INACTIVE.name());

        courtActivity.setChargeIds(chargeIds);

        rtn = service.createCaseActivity(uc, courtActivity);
        assertNotNull(rtn);
        assertTrue(rtn.getChargeIds().containsAll(chargeIds));
        courtActivityIds.add(rtn.getCaseActivityIdentification());
    }

    private CourtActivityType generateDefaultCourtActivity(boolean onlyMandatoryFields) {

        //courtActivity1: madatory fields
        CourtActivityType courtActivity = new CourtActivityType();

        //test ActivityCategory has value.
        courtActivity.setActivityCategory(CaseActivityCategory.COURT.name());

        //test ActivityType has value.
        courtActivity.setActivityType(CaseActivityType.INITCRT.name());

        //test ActivityStatus has value.
        courtActivity.setActivityStatus(CaseActivityStatus.ACTIVE.name());
        // courtActivity.setActivityOutcome(CaseActivityOutcome.SENTC.name());

        //test ActivityOccurredDate has value.
        courtActivity.setActivityOccurredDate(new Date());

        courtActivity.getCaseIds().add(createdCaseInfos.get(0).getCaseInfoId());

        if (onlyMandatoryFields) {
			return courtActivity;
		}

        //test ActivityCategory has value.
        courtActivity.setActivityCategory(CaseActivityCategory.COURT.name());

        //test ActivityType has value.
        courtActivity.setActivityType(CaseActivityType.INITCRT.name());

        //test ActivityOccurredDate has value.
        courtActivity.setActivityOccurredDate(new Date());

        //test associations positive.
        //        associations = new HashSet<AssociationType>();
        //        asso1 = new AssociationType(syscon.arbutus.product.services.legal.contract.dto.caseactivity.AssociationToClass.ACTIVITY.value(), 102L);
        //        associations.add(asso1);

        //TODO:courtActivity.setAssociations(associations);

        //all mandatory fields have values.
        //      asso2 = new AssociationType(syscon.arbutus.product.services.legal.contract.dto.caseactivity.AssociationToClass.CASE_INFORMATION.value(), 202L);
        //      associations.add(asso2);

        //add optional elements:

        //test ActivityStatus has value.
        courtActivity.setActivityOutcome("DISMISS");

        //test comment
        CommentType comment = new CommentType();
        comment.setComment("comment1");
        comment.setCommentDate(new Date());
        courtActivity.setActivityComment(comment);

        //test judge
        courtActivity.setJudge("judge1");

        //test facility association
        courtActivity.setFacilityId(999L);

        //test facility association
        courtActivity.setFacilityInternalLocationId(998L);

        //test facility association
        courtActivity.setOrganizationId(999L);

        //test static associations positive
        Set<Long> chargeAssociations = new HashSet<Long>();
        Set<Long> orderInitialAssociations = new HashSet<Long>();
        Set<Long> orderResultAssociations = new HashSet<Long>();

        Long asso11 = 301L;
        chargeAssociations.add(asso11);
        asso11 = 302L;
        chargeAssociations.add(asso11);
        asso11 = 303L;
        chargeAssociations.add(asso11);

        asso11 = 401L;
        Long asso22 = 402L;
        orderInitialAssociations.add(asso11);
        orderInitialAssociations.add(asso22);

        asso11 = 501L;
        orderResultAssociations.add(asso11);

        //		courtActivity.setChargeIds(chargeAssociations);
        //		courtActivity.setOrderInititatedIds(orderInitialAssociations);
        //		courtActivity.setOrderResultedIds(orderResultAssociations);

        return courtActivity;
    }

    public void createSentenceAdjustment() {
        assertNull(service.getSentenceAdjustment(uc, supervisionId));

        SentenceAdjustmentType sentenceAdjustment = new SentenceAdjustmentType();
        sentenceAdjustment.setSupervisionId(supervisionId);
        SentenceAdjustmentType ret; // = service.createSentenceAdjustment(uc, sentenceAdjustment);

        //        AdjustmentType goodTime = new AdjustmentType();
        //        goodTime.setAdjustmentType(ADJUSTMENT_TYPE.GT.name());
        //        goodTime.setAdjustmentClassification(ADJUSTMENT_CLASSIFICATION.SYSG.name());
        //        goodTime.setAdjustmentFunction(ADJUSTMENT_FUNCTION.CRE.name());
        //        goodTime.setAdjustmentStatus(ADJUSTMENT_STATUS.INCL.name());
        //        goodTime.setApplicationType(APPLICATION_TYPE.AGGSENT.name());
        //        goodTime.setAdjustment(60L);
        //        goodTime.setStartDate(startDate());
        //        goodTime.setEndDate(endDate());
        //        goodTime.setPostedDate(postedDate());
        //        goodTime.getSentenceIds().add(sentenceId1);
        //        goodTime.getSentenceIds().add(sentenceId2);
        //        goodTime.setByStaffId(staffId);
        //        goodTime.setComment("Good time");
        //        sentenceAdjustment.getAdjustments().add(goodTime);
        //        ret = service.createSentenceAdjustment(uc, sentenceAdjustment);
        //
        //        AdjustmentType lgt = new AdjustmentType();
        //        lgt.setAdjustmentType(ADJUSTMENT_TYPE.LGT.name());
        //        lgt.setAdjustmentClassification(ADJUSTMENT_CLASSIFICATION.MANUAL.name());
        //        lgt.setAdjustmentFunction(ADJUSTMENT_FUNCTION.DEB.name());
        //        lgt.setAdjustmentStatus(ADJUSTMENT_STATUS.INCL.name());
        //        lgt.setApplicationType(APPLICATION_TYPE.AGGSENT.name());
        //        lgt.setAdjustment(0L);
        //        lgt.setStartDate(startDate());
        //        lgt.setEndDate(endDate());
        //        lgt.setPostedDate(postedDate());
        //        lgt.getSentenceIds().add(sentenceId1);
        //        lgt.getSentenceIds().add(sentenceId2);
        //        lgt.setByStaffId(staffId);
        //        lgt.setComment("Loss Good time");
        //        sentenceAdjustment.getAdjustments().add(lgt);

        //        ret = service.createSentenceAdjustment(uc, sentenceAdjustment);
        //
        //        assertNotNull(ret);
        //        assertEquals(ret.getSupervisionId(), supervisionId);
        //        assertEquals(ret.getAdjustments().size(), 1);
        //
        //        ret.getAdjustments().add(goodTime);
        //        ret = service.createSentenceAdjustment(uc, ret);
        //        assertNotNull(ret);
        //        assertEquals(ret.getSupervisionId(), supervisionId2);
        //        assertEquals(ret.getAdjustments().size(), 2);

        AdjustmentType jailTime = new AdjustmentType();
        jailTime.setAdjustmentType(ADJUSTMENT_TYPE.WORKCDT.name());
        jailTime.setAdjustmentClassification(ADJUSTMENT_CLASSIFICATION.MANUAL.name());
        jailTime.setAdjustmentFunction(ADJUSTMENT_FUNCTION.CRE.name());
        jailTime.setAdjustmentStatus(ADJUSTMENT_STATUS.INCL.name());
        jailTime.setAdjustment(10L);
        jailTime.setStartDate(getDate(2013, 4, 1));
        jailTime.setEndDate(null);
        jailTime.setPostedDate(null);
        jailTime.setByStaffId(staffId);
        jailTime.getSentenceIds().add(sentenceId1);
        jailTime.getSentenceIds().add(sentenceId2);
        jailTime.setComment("Jail Time");
        sentenceAdjustment.getAdjustments().add(jailTime);
        ret = service.createSentenceAdjustment(uc, sentenceAdjustment);
        assertNotNull(ret);
        assertEquals(ret.getSupervisionId(), supervisionId);
        // assertEquals(ret.getAdjustments().size(), 1);

    }

    private Supervision getSupervision1() {
        Supervision supervision = new Supervision();
        supervision.setSupervisionId(supervisionId2);
        List<Sentence> sentences = new ArrayList<Sentence>();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2014);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        sentences.add(new Sentence(1L, 1L, supervisionId2, "DEF", cal.getTime(), 0, 0, 0, 30, null));

        supervision.setSentences(sentences);
        return supervision;
    }

    private Date createPastDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, -day);
        Date pastDate = cal.getTime();
        return pastDate;
    }

    private Date createFutureDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, day);
        Date futureDate = cal.getTime();
        return futureDate;
    }

    private ContactType getContact(String str) {
        if (str != null && !str.trim().isEmpty()) {
			str = " " + str;
		}
        ContactType contact = new ContactType();
        contact.setFirstName("First Name" + str);
        contact.setLastName("Last Name" + str);
        contact.setAddress("Address" + str);
        contact.setCity("City" + str);
        contact.setCounty("County" + str);
        contact.setCountry("Country" + str);
        contact.setZipCode("Zip Code" + str);
        contact.setPhoneNumber("Phone Number" + str);
        contact.setFaxNumber("Fax Number" + str);
        contact.setEmail("Email" + str);
        contact.setIdentityNumber("Identity Number" + str);
        contact.setAgentContact("Agent Contact" + str);
        return contact;
    }

    private Date startDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -6);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    private Date endDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 6);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    private Date postedDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    private Date getDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1, day);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public enum OrdClassification {WD, LO, BAIL, SENT}

    public enum OrdType {TOO, SENTORD, ICE, SW, WRMD}

    public enum OrdSubType {WANT, DETAINER, HOLD}

    public enum OrdStatus {ACTIVE, INACTIVE}

    public enum OrdOutcome {WC, WE, SENT, BLSET, BLPOST, BLREV, BLBREACH, BOPOST, BOREV, BOBREACH}

    public enum OrdCategory {IJ, OJ}

    public enum BlType {CASH, SURETY, PROP}

    public enum BlRelationship {AG, DISAG}

    public enum TmType {SINGLE, DUAL, THREE, INT}

    public enum TmName {MIN, MAX}

    public enum TmCategory {CUS, COM}

    public enum TmStatus {PEND, INC, EXC}

    public enum Day {MON, TUE, WED, THU, FRI, SAT, SUN}

    public enum Duration {HR, DAY}

    public enum SentType {DEF, INDETER, DETER, SPLIT, INTER, LIFE, DEATH, FINEDEF, PROB, PAR, FIX, FINEDSENT}

    public enum SentStatus {PEND, INC, EXC}

    public enum AppealStatus {INPROGRESS, REJECTED, ACCEPTED}

    public enum NotifType {NOT1, NOT2}

    public enum KeyDates {PDD, PDDWF, HDCED, HDCAD, ARD, PED, SED, CRD, PDDWA, PDDSTAT, PDDWSTAT, PDDWCRE}

    public enum CaseActivityCategory {COURT, PROB, POL, PAROLE}

    public enum CaseActivityType {INITCRT, PRELHEAR, BAILHEAR, GRDJURY, ARRAIGN, TRIAL, SENTENCE, APPEAL, PAROLEHR, PROBHR}

    public enum CaseActivityOutcome {DISMISS, PROBATION, CAPITAL, SENTC, TRL, APPL, BAILAPP, APPLFL, NBL, RMD, PARAPP, PARDNY}

    public enum CaseActivityStatus {ACTIVE, INACTIVE}
}
