package syscon.arbutus.product.services.legal.contract.ejb;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class CaseAffiliationIT extends BaseIT {
    final static Long success = 1L;

    private static final String CATEGORY_PERSON = "PI";
    private static final String TYPE_LEGAL = "LEG";
    private static final String ROLE_WITNESS = "WITNESS";

    private static final String CASE_CATEGORY_IJ = "IJ";
    private static final String CASE_TYPE_ADULT = "ADULT";
    private static final String SENTENCE_STATUS_SENTENCED = "SENTENCED";
    private static final String CASE_STATUS_REASON_DISMISSED = "CD";
    private static final String CASE_IDENTIFIER_TYPE_DOCKET = "DOCKET";
    private static final String CASE_IDENTIFIER_TYPE_INDICTMENT = "INDICTMENT";

    private static final String ROLE_VICTIM = "VICTIM";
    private static final String TYPE_PROB = "PROB";

    private static Logger log = LoggerFactory.getLogger(CaseAffiliationIT.class);

    private List<CaseAffiliationType> createdCaseAffiliations = new ArrayList<CaseAffiliationType>();

    private LegalService service;

    private Long caseId;

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        lookupJNDILogin();

        uc = super.initUserContext();
    }

    //for release
    private void lookupJNDILogin() {

        log.info("Before JNDI lookup");
        service = (LegalService) JNDILookUp(this.getClass(), LegalService.class);
        assertNotNull(service);

    }

    @Test
    public void testLookupJNDI() {
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "testLookupJNDI" })
    public void testGetVersion() {
        String version = service.getVersion(null);
        assert ("1.0".equals(version));
    }

    @Test(dependsOnMethods = { "testLookupJNDI" })
    public void testCreate() {

        CaseAffiliationType caseAffiliation = new CaseAffiliationType();

        verfiyCreateNegative(caseAffiliation, InvalidInputException.class);

        caseAffiliation.setCaseAffiliationType(TYPE_LEGAL);
        verfiyCreateNegative(caseAffiliation, InvalidInputException.class);

        caseAffiliation.setCaseAffiliationRole(ROLE_WITNESS);
        verfiyCreateNegative(caseAffiliation, InvalidInputException.class);

        caseAffiliation.setStartDate(new Date());
        verfiyCreateNegative(caseAffiliation, InvalidInputException.class);

        caseAffiliation.setAffiliatedPersonIdentityId(999L);
        verfiyCreateNegative(caseAffiliation, InvalidInputException.class);

        caseAffiliation.setAffiliatedCaseId(99999l);
        verfiyCreateNegative(caseAffiliation, DataNotExistException.class);

        CaseInfoType caseInfo = generateDefaultCaseInfo(true);
        CaseInfoType ret = service.createCaseInfo(uc, caseInfo, null);
        assert (ret != null);
        caseId = ret.getCaseInfoId();

        caseAffiliation.setAffiliatedCaseId(caseId);
        CaseAffiliationType rtn = service.createCaseAffiliation(uc, caseAffiliation);
        Assert.assertNotNull(rtn);

        Assert.assertEquals(rtn.getCaseAffiliationCategory(), CATEGORY_PERSON);

        createdCaseAffiliations.add(rtn);

    }

    /**
     * @param b
     * @return
     */
    private CaseInfoType generateDefaultCaseInfo(boolean onlyMandatory) {
        CaseInfoType caseInfo = new CaseInfoType();
        Date currentDate = new Date();

        //Mandatory fields:
        caseInfo.setCaseCategory(CASE_CATEGORY_IJ);
        caseInfo.setCaseType(CASE_TYPE_ADULT);
        caseInfo.setCreatedDate(currentDate);
        caseInfo.setDiverted(true);
        caseInfo.setDivertedSuccess(true);
        caseInfo.setActive(true);
        caseInfo.setSupervisionId(1L);

        if (onlyMandatory) {
            return caseInfo;
        }

        //Add optional fields:
        caseInfo.setIssuedDate(currentDate);
        caseInfo.setStatusChangeReason(CASE_STATUS_REASON_DISMISSED);
        caseInfo.setSentenceStatus(SENTENCE_STATUS_SENTENCED);
        caseInfo.setFacilityId(1L);

        CommentType comment = new CommentType();
        comment.setUserId("1");
        comment.setCommentDate(currentDate);
        comment.setComment("createCaseInfoPositive");
        caseInfo.setComment(comment);

        //caseInfo.getAssociations().add(new AssociationType(CASE_ACTIVITY, 1L));

        //Add case identifiers
        CaseIdentifierType caseIdentifier1 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_DOCKET, "0001A", false, 1L, 1L, "createCaseInfoPositive comment1", true);

        CaseIdentifierType caseIdentifier2 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_INDICTMENT, "0002", false, 1L, 1L, "createCaseInfoPositive comment2",
                false);

        caseInfo.getCaseIdentifiers().add(caseIdentifier1);
        caseInfo.getCaseIdentifiers().add(caseIdentifier2);

        return caseInfo;
    }

    @Test(dependsOnMethods = { "testCreate" })
    public void testGet() {
        //test get by valid ids.
        for (CaseAffiliationType caseAffiliation : createdCaseAffiliations) {
            Long id = caseAffiliation.getCaseAffiliationId();
            CaseAffiliationType ret = service.getCaseAffiliation(uc, id);
            assertNotNull(ret);
            assertEquals(ret.getCaseAffiliationId(), id);

        }

        //test get by invalid id
        verfiyGetNegative(9999999L);
    }

    @Test(dependsOnMethods = { "testGet" })
    public void testUpdate() {

        CaseAffiliationType caseAffiliation = createdCaseAffiliations.get(0);
        caseAffiliation.setAffiliatedPersonIdentityId(998L);
        caseAffiliation.setCaseAffiliationType(TYPE_PROB);
        caseAffiliation.setCaseAffiliationRole(ROLE_VICTIM);
        caseAffiliation.setStartDate(getDateWithoutTime(2013, 01, 01));
        caseAffiliation.setEndDate(getDateWithoutTime(2013, 02, 02));
        caseAffiliation.setOrgnizationPersonJobPosition("new position");

        CaseAffiliationType ret = service.updateCaseAffiliation(uc, caseAffiliation);
        assertNotNull(ret);
        assertEquals(ret.getCaseAffiliationId(), caseAffiliation.getCaseAffiliationId());
        assertEquals(ret.getAffiliatedPersonIdentityId(), new Long(998L));
        assertEquals(ret.getCaseAffiliationType(), TYPE_PROB);
        assertEquals(ret.getCaseAffiliationRole(), ROLE_VICTIM);
        assertEquals(ret.getStartDate(), getDateWithoutTime(2013, 01, 01));
        assertEquals(ret.getEndDate(), getDateWithoutTime(2013, 02, 02));
        assertEquals(ret.getOrgnizationPersonJobPosition(), "new position");

    }

    @Test(dependsOnMethods = { "testUpdate" })
    public void testDelete() {

        for (CaseAffiliationType caseAffiliation : createdCaseAffiliations) {
            Long id = caseAffiliation.getCaseAffiliationId();
            Long deleteRtn = service.deleteCaseAffiliation(uc, id);

            assertEquals(deleteRtn.longValue(), success.longValue());

            //can not get current one.
            try {
                service.getCaseAffiliation(uc, id);
            } catch (Exception e) {
                Assert.assertTrue(e instanceof DataNotExistException);
            }
        }
    }

    @AfterClass
    public void afterTest() {

    }

    ///////////////////////////////////private methods//////////////////////////////////////////////////

    private Date getDateWithoutTime(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.DAY_OF_MONTH, day);

        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    private Date getDateWithoutTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Date newDate = cal.getTime();
        return newDate;
    }

    private Date getDate(int year, int month, int day, int hour, int minute, int second) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.DAY_OF_MONTH, day);

        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, second);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    /**
     * @param caseAffiliation
     * @param class1
     */
    private <T extends Exception> void verfiyCreateNegative(CaseAffiliationType caseAffiliation, Class<T> class1) {
        try {
            CaseAffiliationType rtn = service.createCaseAffiliation(uc, caseAffiliation);

            Assert.assertEquals(rtn, null);

        } catch (Exception e) {
            if (class1 != null) {
				Assert.assertEquals(e.getClass(), class1);
			}
        }
    }

    private void verfiyUpdateNegative(CaseAffiliationType caseAffiliation) {
        try {
            CaseAffiliationType rtn = service.updateCaseAffiliation(uc, caseAffiliation);
            Assert.assertEquals(rtn, null);
        } catch (Exception e) {
            Assert.assertNotEquals(e, null);
            // Assert.assertTrue(e instanceof ArbutusRuntimeException);
        }
    }

    private void verfiyGetNegative(Long id) {
        try {

            CaseAffiliationType rtn = service.getCaseAffiliation(uc, id);
            Assert.assertEquals(rtn, null);
        } catch (Exception e) {
            Assert.assertNotEquals(e, null);
            // Assert.assertTrue(e instanceof ArbutusRuntimeException);
        }
    }

}
