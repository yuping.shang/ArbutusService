package syscon.arbutus.product.services.legal.contract.ejb;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.AdjustmentConfigType;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.AdjustmentType;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.SentenceAdjustmentSearchType;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.SentenceAdjustmentType;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;
import syscon.arbutus.product.services.sentencecalculation.contract.dto.SentenceCalculatorReturnType;
import syscon.arbutus.product.services.sentencecalculation.contract.dto.SentenceCalculatorType;

import static org.testng.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: yshang
 * Date: 10/10/13
 * Time: 2:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class SentenceAdjustmentIT extends BaseIT {

    private static final Long success = ReturnCode.Success.returnCode();
    Long supervisionID = 100L;
    Long supervisionID2 = 200L;
    Long sentenceID1 = 1L;
    Long sentenceID2 = 2L;
    Long staffID = 1L;
    private UserContext uc = null; //new UserContext();
    private Logger log = LoggerFactory.getLogger(SentenceAdjustmentIT.class);
    private LegalService service;

    @BeforeClass
    public void beforeClass() {
        lookupJNDILogin();
    }

    @BeforeMethod
    public void beforeMethod() {

    }

    private void lookupJNDILogin() {
        log.info("Before JNDI lookup");
        service = (LegalService) JNDILookUp(this.getClass(), LegalService.class);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();
    }

    @Test(enabled = true)
    public void testLookupJNDI() {
        assertNotNull(service);
    }

    public void reset() {
        Long ret = service.deleteAllSentenceAdjustmentConfiguration(uc);
        ret = service.deleteAllSentenceAdjustments(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());
    }

    @Test
    public void setSentenceAdjustmentConfiguration() {
        AdjustmentConfigType config = new AdjustmentConfigType();
        config.setAdjustmentClassification(ADJUSTMENT_CLASSIFICATION.SYSG.name());
        config.setAdjustmentType(ADJUSTMENT_TYPE.GT.name());
        config.setAdjustmentFunction(ADJUSTMENT_FUNCTION.CRE.name());
        config.setApplicationType(APPLICATION_TYPE.AGGSENT.name());
        config.setAdjustmentStatus(ADJUSTMENT_STATUS.INCL.name());
        config.setSpecialAdjustment(10L);
        config.setByStaffId(staffID);
        config.setComment("Good Time");

        service.setSentenceAdjustmentConfiguration(uc, config);
        AdjustmentConfigType ret = service.getSentenceAdjustmentConfiguration(uc, ADJUSTMENT_TYPE.GT.name());
        assertEquals(ret, config);
        assertNotNull(ret.getStamp());

        config = new AdjustmentConfigType();

        String adjustmentType = ADJUSTMENT_TYPE.values()[1].name();
        config.setAdjustmentType(adjustmentType);

        config.setAdjustmentClassification(ADJUSTMENT_CLASSIFICATION.values()[0].name());
        config.setAdjustmentFunction(ADJUSTMENT_FUNCTION.values()[0].name());
        config.setApplicationType(APPLICATION_TYPE.values()[2].name());
        config.setAdjustmentStatus(ADJUSTMENT_STATUS.values()[0].name());
        config.setSpecialAdjustment(Long.valueOf(6));
        config.setByStaffId(staffID);
        config.setComment("Comment " + APPLICATION_TYPE.values()[2].name());
        service.setSentenceAdjustmentConfiguration(uc, config);
        ret = service.getSentenceAdjustmentConfiguration(uc, adjustmentType);
        assertEquals(ret, config);

    }

    @Test(dependsOnMethods = { "setSentenceAdjustmentConfiguration" })
    public void getSentenceAdjustmentConfiguration() {

        String adjustmentType = ADJUSTMENT_TYPE.values()[0].name();
        AdjustmentConfigType cfg = service.getSentenceAdjustmentConfiguration(uc, adjustmentType);
        assertNotNull(cfg);
        assertNotNull(cfg.getAdjustmentType());
        assertEquals(cfg.getAdjustmentType(), adjustmentType);

    }

    @Test(dependsOnMethods = { "setSentenceAdjustmentConfiguration" })
    public void getAllSentenceAdjustmentConfiguration() {
        List<AdjustmentConfigType> ret = service.getAllSentenceAdjustmentConfiguration(uc);
        assertNotNull(ret);
        assertFalse(ret.isEmpty());
        // assertEquals(ret.size(), ADJUSTMENT_TYPE.values().length);
        List<String> typeList = new ArrayList();
        for (ADJUSTMENT_TYPE t : ADJUSTMENT_TYPE.values()) {
            typeList.add(t.name());
        }
        Collections.sort(typeList);
        // test the returned result is sorted
        /*for (int i = 0; i < ret.size(); i++) {
            assertEquals(ret.get(i).getAdjustmentType(), typeList.get(i).toString(),
                    "The returned result should be sorted");
        }*/
    }

    @Test(dependsOnMethods = { "getSentenceAdjustmentConfiguration", "getAllSentenceAdjustmentConfiguration" })
    public void deleteSentenceAdjustmentConfiguration() {
        List<AdjustmentConfigType> ret = service.getAllSentenceAdjustmentConfiguration(uc);
        for (AdjustmentConfigType config : ret) {
            Long rt = service.deleteSentenceAdjustmentConfiguration(uc, config.getAdjustmentType());
            assertEquals(rt, success);
        }
        assertEquals(service.getAllSentenceAdjustmentConfiguration(uc).size(), 0);
    }

    @Test(dependsOnMethods = { "deleteSentenceAdjustmentConfiguration" })
    public void deleteAllSentenceAdjustmentConfiguration() {
        setSentenceAdjustmentConfiguration();
        List<AdjustmentConfigType> ret = service.getAllSentenceAdjustmentConfiguration(uc);
        assertTrue(ret.size() > 0);
        assertEquals(service.deleteAllSentenceAdjustmentConfiguration(uc), success);
        ret = service.getAllSentenceAdjustmentConfiguration(uc);
        assertEquals(ret.size(), 0);
    }

    @Test(dependsOnMethods = { "deleteAllSentenceAdjustmentConfiguration" })
    public void createSentenceAdjustment() {
        reset();
        assertNull(service.getSentenceAdjustment(uc, supervisionID));
        setSentenceAdjustmentConfiguration();

        SentenceAdjustmentType sentenceAdjustment = new SentenceAdjustmentType();
        sentenceAdjustment.setSupervisionId(supervisionID);
        SentenceAdjustmentType ret; // = service.createSentenceAdjustment(uc, sentenceAdjustment);
        //        assertNotNull(ret);
        //        assertEquals(ret.getSupervisionId(), supervisionID);
        //        assertEquals(ret.getAdjustments().size(), 0);

        AdjustmentType goodTime = new AdjustmentType();
        goodTime.setAdjustmentType(ADJUSTMENT_TYPE.GT.name());
        goodTime.setAdjustmentClassification(ADJUSTMENT_CLASSIFICATION.SYSG.name());
        goodTime.setAdjustmentFunction(ADJUSTMENT_FUNCTION.CRE.name());
        goodTime.setAdjustmentStatus(ADJUSTMENT_STATUS.INCL.name());
        goodTime.setApplicationType(APPLICATION_TYPE.AGGSENT.name());
        goodTime.setAdjustment(60L);
        goodTime.setStartDate(startDate());
        goodTime.setEndDate(endDate());
        goodTime.setPostedDate(postedDate());
        goodTime.getSentenceIds().add(sentenceID1);
        goodTime.getSentenceIds().add(sentenceID2);
        goodTime.setByStaffId(staffID);
        goodTime.setComment("Good time");
        sentenceAdjustment.getAdjustments().add(goodTime);

        AdjustmentType lgt = new AdjustmentType();
        lgt.setAdjustmentType(ADJUSTMENT_TYPE.LGT.name());
        lgt.setAdjustmentClassification(ADJUSTMENT_CLASSIFICATION.MANUAL.name());
        lgt.setAdjustmentFunction(ADJUSTMENT_FUNCTION.DEB.name());
        lgt.setAdjustmentStatus(ADJUSTMENT_STATUS.INCL.name());
        lgt.setApplicationType(APPLICATION_TYPE.AGGSENT.name());
        lgt.setAdjustment(40L);
        lgt.setStartDate(startDate());
        lgt.setEndDate(endDate());
        lgt.setPostedDate(postedDate());
        lgt.getSentenceIds().add(sentenceID1);
        lgt.getSentenceIds().add(sentenceID2);
        lgt.setByStaffId(staffID);
        lgt.setComment("Loss Good time");
        sentenceAdjustment.getAdjustments().add(lgt);

        ret = service.createSentenceAdjustment(uc, sentenceAdjustment);

        assertNotNull(ret);
        assertEquals(ret.getSupervisionId(), supervisionID);
        assertEquals(ret.getAdjustments().size(), 2);

        ret.getAdjustments().add(goodTime);
        ret = service.createSentenceAdjustment(uc, ret);
        assertNotNull(ret);
        assertEquals(ret.getSupervisionId(), supervisionID);
        assertEquals(ret.getAdjustments().size(), 3);

        AdjustmentType jailTime = new AdjustmentType();
        jailTime.setAdjustmentType(ADJUSTMENT_TYPE.JT.name());
        jailTime.setAdjustmentClassification(ADJUSTMENT_CLASSIFICATION.MANUAL.name());
        jailTime.setAdjustmentFunction(ADJUSTMENT_FUNCTION.CRE.name());
        jailTime.setAdjustmentStatus(ADJUSTMENT_STATUS.INCL.name());
        jailTime.setAdjustment(15L);
        jailTime.setStartDate(startDate());
        jailTime.setEndDate(endDate());
        jailTime.setPostedDate(postedDate());
        jailTime.setByStaffId(staffID);
        jailTime.getSentenceIds().add(sentenceID1);
        jailTime.setComment("Jail Time");
        ret.getAdjustments().add(jailTime);
        ret = service.createSentenceAdjustment(uc, ret);
        assertNotNull(ret);
        assertEquals(ret.getSupervisionId(), supervisionID);
        assertEquals(ret.getAdjustments().size(), 4);

        sentenceAdjustment = new SentenceAdjustmentType();
        sentenceAdjustment.setSupervisionId(supervisionID2);
        sentenceAdjustment.getAdjustments().add(goodTime);
        jailTime.setAdjustmentType(ADJUSTMENT_TYPE.WORKCDT.name());
        jailTime.setComment("Work time");
        jailTime.setAdjustment(45L);
        sentenceAdjustment.getAdjustments().add(jailTime);
        ret = service.createSentenceAdjustment(uc, sentenceAdjustment);
        assertNotNull(ret);
        assertEquals(ret.getSupervisionId(), supervisionID2);
        assertEquals(ret.getAdjustments().size(), 2);
    }

    @Test(dependsOnMethods = { "createSentenceAdjustment" }, enabled = true)
    public void updateSentenceAdjustment() {
        SentenceAdjustmentType sentenceAdjustment = service.getSentenceAdjustment(uc, supervisionID);
        for (AdjustmentType adjustment : sentenceAdjustment.getAdjustments()) {
            adjustment.setAdjustment(adjustment.getAdjustment() + 1000L);
        }
        SentenceAdjustmentType ret = service.updateSentenceAdjustment(uc, sentenceAdjustment);
        for (AdjustmentType adjustment : ret.getAdjustments()) {
            assertTrue(adjustment.getAdjustment() > 1000L);
        }

        AdjustmentType lgt = new AdjustmentType();
        lgt.setAdjustmentId(null);
        lgt.setAdjustmentType(ADJUSTMENT_TYPE.LGT.name());
        // lgt.setAdjustmentType(ADJUSTMENT_TYPE.PRGCDT.name());
        lgt.setAdjustmentClassification(ADJUSTMENT_CLASSIFICATION.MANUAL.name());
        lgt.setAdjustmentFunction(ADJUSTMENT_FUNCTION.DEB.name());
        lgt.setAdjustmentStatus(ADJUSTMENT_STATUS.INCL.name());
        lgt.setAdjustment(15L);
        lgt.setStartDate(startDate());
        lgt.setEndDate(endDate());
        lgt.setPostedDate(postedDate());
        lgt.getSentenceIds().add(sentenceID2);
        lgt.setByStaffId(staffID);
        lgt.setComment("Jail Time");
        ret.getAdjustments().add(lgt);
        ret = service.updateSentenceAdjustment(uc, ret);
        assertNotNull(ret);
        assertEquals(ret.getAdjustments().size(), 5);
        boolean flag = false;
        for (AdjustmentType adj : ret.getAdjustments()) {
            if (adj.getAdjustmentType().equalsIgnoreCase(ADJUSTMENT_TYPE.LGT.name())) {
                ret.getAdjustments().remove(adj);
                service.updateSentenceAdjustment(uc, ret);
                flag = true;
                break;
            }
        }
        assertTrue(flag);
    }

    @Test(dependsOnMethods = { "updateSentenceAdjustment" }, enabled = true)
    public void addAdjustment() {
        assertEquals(service.getSentenceAdjustment(uc, supervisionID).getAdjustments().size(), 4);

        AdjustmentType program = new AdjustmentType();
        program.setAdjustmentType(ADJUSTMENT_TYPE.PRGCDT.name());
        program.setAdjustmentClassification(ADJUSTMENT_CLASSIFICATION.MANUAL.name());
        program.setAdjustmentFunction(ADJUSTMENT_FUNCTION.CRE.name());
        program.setAdjustmentStatus(ADJUSTMENT_STATUS.INCL.name());
        program.setAdjustment(15L);
        program.setStartDate(startDate());
        program.setEndDate(endDate());
        program.setPostedDate(postedDate());
        program.setByStaffId(staffID);
        program.setComment("Program Time");
        SentenceAdjustmentType ret = service.addAdjustment(uc, supervisionID, program);
        assertEquals(ret.getAdjustments().size(), 5);

        assertEquals(service.getSentenceAdjustment(uc, supervisionID).getAdjustments().size(), 5);
    }

    @Test(dependsOnMethods = { "addAdjustment" }, enabled = true)
    public void addAdjustments() {
        assertEquals(service.getSentenceAdjustment(uc, supervisionID2).getAdjustments().size(), 2);

        Set<AdjustmentType> adjustments = new HashSet<>();
        AdjustmentType program = new AdjustmentType();
        program.setAdjustmentType(ADJUSTMENT_TYPE.PRGCDT.name());
        program.setAdjustmentClassification(ADJUSTMENT_CLASSIFICATION.MANUAL.name());
        program.setAdjustmentFunction(ADJUSTMENT_FUNCTION.CRE.name());
        program.setAdjustmentStatus(ADJUSTMENT_STATUS.INCL.name());
        program.setAdjustment(15L);
        program.setStartDate(startDate());
        program.setEndDate(endDate());
        program.setPostedDate(postedDate());
        program.setByStaffId(staffID);
        program.setComment("Program Time");
        adjustments.add(program);

        AdjustmentType goodTime = new AdjustmentType();
        goodTime.setAdjustmentType(ADJUSTMENT_TYPE.GT.name());
        goodTime.setAdjustmentClassification(ADJUSTMENT_CLASSIFICATION.MANUAL.name());
        goodTime.setAdjustmentFunction(ADJUSTMENT_FUNCTION.CRE.name());
        goodTime.setAdjustmentStatus(ADJUSTMENT_STATUS.INCL.name());
        goodTime.setAdjustment(90L);
        goodTime.setStartDate(startDate());
        goodTime.setEndDate(endDate());
        goodTime.setPostedDate(postedDate());
        goodTime.setByStaffId(staffID);
        goodTime.setComment("Good time 90");
        adjustments.add(goodTime);

        SentenceAdjustmentType ret = service.addAdjustments(uc, supervisionID2, adjustments);
        assertEquals(ret.getAdjustments().size(), 4);
        for (AdjustmentType rt : ret.getAdjustments()) {
            if (rt.getAdjustmentType().equalsIgnoreCase(ADJUSTMENT_TYPE.PRGCDT.name())) {
                ret = service.removeAdjustment(uc, supervisionID2, rt);
                assertEquals(ret.getAdjustments().size(), 3);
                break;
            }
        }
        assertEquals(service.getSentenceAdjustment(uc, supervisionID2).getAdjustments().size(), 3);

        service.removeAdjustments(uc, supervisionID2, ret.getAdjustments());
        // assertEquals(service.getSentenceAdjustment(uc, supervisionID2).getAdjustments().size(), 0);
    }

    @Test(dependsOnMethods = { "addAdjustments" }, enabled = true)
    public void getSentenceAdjustment() {
        SentenceAdjustmentType ret = service.getSentenceAdjustment(uc, supervisionID);
        assertNotNull(ret);
        assertNotNull(ret.getSupervisionId());
        assertNotNull(ret.getAdjustments());
        assertTrue(ret.getAdjustments().size() > 0);
        assertEquals(ret.getAdjustments().size(), 5);

        ret = service.getSentenceAdjustment(uc, supervisionID2);
        assertNull(ret);
    }

    @Test(dependsOnMethods = { "getSentenceAdjustment" }, enabled = true)
    public void getSentenceAdjustmentsWithHistory() {
        List<SentenceAdjustmentType> ret = service.getSentenceAdjustmentsWithHistory(uc, supervisionID);
        assertNotNull(ret);
        assertTrue(ret.size() > 0);
        assertEquals(ret.size(), 7);
        int count = 0;
        for (SentenceAdjustmentType sa : ret) {
            // System.out.println("Supervision: " + sa.getSupervisionId());
            assertTrue(sa.getAdjustments().size() > 0);
            for (AdjustmentType adj : sa.getAdjustments()) {
                // System.out.println(adj.getAdjustmentId());
                count++;
            }
        }
        assertEquals(count, 7 * 23);

        ret = service.getSentenceAdjustmentsWithHistory(uc, supervisionID2);
        assertNotNull(ret);
        assertTrue(ret.size() > 0);
        assertEquals(ret.size(), 2);
        count = 0;
        for (SentenceAdjustmentType sa : ret) {
            // System.out.println("Supervision: " + sa.getSupervisionId());
            assertTrue(sa.getAdjustments().size() > 0);
            for (AdjustmentType adj : sa.getAdjustments()) {
                // System.out.println(adj.getAdjustmentId());
                count++;
            }
        }
    }

    @Test(dependsOnMethods = { "getSentenceAdjustmentsWithHistory" }, enabled = true)
    public void search() {
        SentenceAdjustmentSearchType search = new SentenceAdjustmentSearchType();
        search.setSentenceId(sentenceID1);
        List<SentenceAdjustmentType> ret = service.searchSentenceAdjustment(uc, supervisionID, search, null, null, null);
        assertNotNull(ret);
        assertTrue(ret.size() > 0);
        assertEquals(ret.size(), 1);
    }

    @Test(dependsOnMethods = { "search" }, enabled = true)
    public void removeAdjustment() {
        SentenceAdjustmentType ret = service.getSentenceAdjustment(uc, supervisionID);
        if (ret != null) {
            Set<AdjustmentType> adjustments = ret.getAdjustments();
            assertNotNull(adjustments);
            for (AdjustmentType adjustment : adjustments) {
                if (!adjustment.getAdjustmentType().equalsIgnoreCase(ADJUSTMENT_TYPE.GT.name())) {
                    service.removeAdjustment(uc, supervisionID, adjustment);
                }
            }
            ret = service.getSentenceAdjustment(uc, supervisionID);
            assertNotNull(ret);
            assertEquals(ret.getAdjustments().size(), 1);
        }
    }

    @Test(dependsOnMethods = { "search" }, enabled = true)
    public void deleteSentenceAdjustment() {
        if (service.getSentenceAdjustment(uc, supervisionID2) != null) {
            Long ret = service.deleteSentenceAdjustment(uc, supervisionID2);
            assertEquals(ret, success);
            assertNull(service.getSentenceAdjustment(uc, supervisionID2));
        }
    }

    @Test(dependsOnMethods = { "search" }, enabled = true)
    public void deleteAllSentenceAdjustments() {
        Long ret = service.deleteAllSentenceAdjustments(uc);
        assertEquals(ret, success);
        assertNull(service.getSentenceAdjustment(uc, supervisionID));
    }

    private Date startDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -6);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    private Date endDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 6);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    private Date postedDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    @Test
    public void testCaculateFine() {
        SentenceCalculatorType sentenceCalculator = new SentenceCalculatorType();
        sentenceCalculator.setFineAmount(new BigDecimal(340.00));
        sentenceCalculator.setInputPDD(new Date());
        sentenceCalculator.setPayAmount(new BigDecimal(170));
        //sentenceCalculator.setPayAmount(new BigDecimal(150));
        //sentenceCalculator.setAlreadyPaidAmount(new BigDecimal(20));
        sentenceCalculator.setStartDate(new Date());
        sentenceCalculator.setDaysAlreadyServed(0l);
        sentenceCalculator.setTermInHrs(10l);
        sentenceCalculator.setTermInDays(1l);
        sentenceCalculator.setHoursToCalculatePerDiem(24l);
        SentenceCalculatorReturnType ret = service.calculateFine(uc, sentenceCalculator);
        assertNotNull(ret);
        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
    }

    @Test
    public void testCaculateFineCase2() {
        SentenceCalculatorType sentenceCalculator = new SentenceCalculatorType();
        sentenceCalculator.setFineAmount(new BigDecimal(480.00));
        sentenceCalculator.setInputPDD(new Date());
        sentenceCalculator.setServeDays(1l);
        sentenceCalculator.setStartDate(new Date());
        sentenceCalculator.setDaysAlreadyServed(0l);
        sentenceCalculator.setTermInDays(2l);
        sentenceCalculator.setHoursToCalculatePerDiem(24l);
        SentenceCalculatorReturnType ret = service.calculateFine(uc, sentenceCalculator);
        assertNotNull(ret);
        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
    }

    enum ADJUSTMENT_CLASSIFICATION {MANUAL, SYSG}

    enum ADJUSTMENT_TYPE {GT, JT, LGT, WORKCDT, LWRKCRD, PRGCDT, LPRGCDT, ESCP, OICDBT, WKNCDT, STATCDT}

    enum ADJUSTMENT_FUNCTION {CRE, DEB}

    enum ADJUSTMENT_STATUS {INCL, EXCL, PEND}

    enum APPLICATION_TYPE {ASENT, AGGSENT, BOOKLEV}
}
