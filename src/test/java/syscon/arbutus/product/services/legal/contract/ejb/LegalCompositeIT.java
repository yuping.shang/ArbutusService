package syscon.arbutus.product.services.legal.contract.ejb;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.*;
import syscon.arbutus.product.services.core.common.Individual;
import syscon.arbutus.product.services.core.common.Supervision;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingLocationAssignType;
import syscon.arbutus.product.services.housing.contract.interfaces.HousingService;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivityType;
import syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType;
import syscon.arbutus.product.services.legal.contract.dto.composite.CaseOffenderType;
import syscon.arbutus.product.services.legal.contract.dto.composite.CaseOffendersReturnType;
import syscon.arbutus.product.services.legal.contract.dto.composite.CourtActivityMovementType;
import syscon.arbutus.product.services.legal.contract.dto.composite.InquiryCaseSearchType;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;
import syscon.arbutus.product.services.movement.contract.dto.CourtMovementReturnTimeConfigurationType;
import syscon.arbutus.product.services.movement.contract.dto.ExternalMovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivitiesReturnType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivitySearchType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;
import syscon.arbutus.product.services.organization.contract.dto.Organization;
import syscon.arbutus.product.services.organization.contract.interfaces.OrganizationService;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class LegalCompositeIT extends BaseIT {

    private static final String CASE_CATEGORY_IJ = "IJ";
    private static final String CASE_TYPE_ADULT = "ADULT";
    private static final String SENTENCE_STATUS_SENTENCED = "SENTENCED";
    private static final String CASE_STATUS_REASON_DISMISSED = "CD";
    private static final String CASE_IDENTIFIER_TYPE_DOCKET = "DOCKET";
    private static final String CASE_IDENTIFIER_TYPE_ARREST = "ARREST";

    private static final String MOVEMENT_REASON1 = "COURT";
    private static final String MOVEMENT_REASON2 = "TRIAL";
    private static final String MOVEMENT_COMMENT1 = "commment1";
    private static final String MOVEMENT_COMMENT2 = "commment2";
    private static final String MOVEMENT_STATUS1 = "PENDING";
    private static final String MOVEMENT_STATUS2 = "PENDING";

    private static final String SEX = "M";
    private static final String FIRST_NAME = "FIRST NAME";
    private static final String LAST_NAME = "LAST NAME";
    private static final String SUPERVISION_DISPLAY_ID = "display id";
    private static final String CATEGORY_PERSON = "PI";
    private static final String TYPE_LEGAL = "LEG";
    private static final String ROLE_WITNESS = "WITNESS";
    private static final String ROLE_VICTIM = "VICTIM";
    private static final String CATEGORY_PERSON_ORG = "ORGPI";
    private static String expectedFacilityName = "";
    private boolean expectedSupervisionStatus;
    private List<CaseAffiliationType> createdCaseAffiliations = new ArrayList<CaseAffiliationType>();

    //private CaseManagementServiceBean service;   //for local test
    private UserContext uc = null; //new UserContext();
    private Logger log = LoggerFactory.getLogger(LegalCompositeIT.class);

    private LegalService service;
    private FacilityService facilityService;
    private PersonService personIdentityService;
    private SupervisionService supervisionService;
    private HousingService housingBedService;
    private ActivityService activityService;
    private MovementService movementService;
    private OrganizationService organizationService;

    private List<PersonIdentityType> createdPersonIdentityType = new ArrayList<PersonIdentityType>();
    private SupervisionType createdSupervision = null;
    private Facility createdFacility = null;
    private CourtActivityType createdCourtActivity = null;
    private CourtActivityMovementType createdCourtActivityMovementType = null;
    private Organization createdOrganization = null;
    private List<CaseInfoType> createdCaseInfos = new ArrayList<CaseInfoType>();

    /**
     * <p>Cut the time portion of a given Date object.</p>
     *
     * @param dateTime A Date object to trim.
     * @return A Date object.
     */
    private static Date getDatePartial(Date dateTime) {

        if (dateTime == null) {
            return null;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(dateTime);

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();

        //localLogin();
    }

    @BeforeMethod
    public void beforeMethod() {
    }

    //for release
    private void lookupJNDILogin() {

        log.info("Before JNDI lookup");
        service = (LegalService) JNDILookUp(this.getClass(), LegalService.class);
        assertNotNull(service);

        facilityService = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        assertNotNull(facilityService);

        supervisionService = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        assertNotNull(supervisionService);

        personIdentityService = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        assertNotNull(personIdentityService);

        housingBedService = (HousingService) JNDILookUp(this.getClass(), HousingService.class);
        assertNotNull(housingBedService);

        activityService = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        assertNotNull(activityService);

        movementService = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
        assertNotNull(movementService);

        organizationService = (OrganizationService) JNDILookUp(this.getClass(), OrganizationService.class);
        assertNotNull(organizationService);

        //init user context and login
        uc = super.initUserContext();
        clientlogin("devtest", "devtest");
    }

    @Test
    public void testLookupJNDI() {
        assertNotNull(service);
        assertNotNull(facilityService);
        assertNotNull(supervisionService);
        assertNotNull(personIdentityService);
        assertNotNull(housingBedService);
        assertNotNull(activityService);
        assertNotNull(movementService);
    }

    @Test(dependsOnMethods = { "testLookupJNDI" })
    public void reset() {
        service.deleteAll(uc);

        //create persion identity.
        PersonIdentityType personIdentity = new PersonIdentityType();
        personIdentity.setFirstName(FIRST_NAME);
        personIdentity.setLastName(LAST_NAME);
        personIdentity.setSex(SEX);
        personIdentity.setDateOfBirth(new Date(11));
        personIdentity.setOffenderNumber("offender number1");

        PersonTest personTest = new PersonTest();
        Long personId = personTest.createPerson();

        personIdentity.setPersonId(personId);

        PersonIdentityType piRtn = personIdentityService.createPersonIdentityType(uc, personIdentity, true, false);

        createdPersonIdentityType.add(piRtn);

        personIdentity = new PersonIdentityType();
        personIdentity.setFirstName(FIRST_NAME);
        personIdentity.setLastName(LAST_NAME);
        personIdentity.setSex(SEX);
        personIdentity.setDateOfBirth(new Date(22));
        personIdentity.setOffenderNumber("offender number2");

        personId = personTest.createPerson();
        personIdentity.setPersonId(personId);

        piRtn = personIdentityService.createPersonIdentityType(uc, personIdentity, true, false);

        createdPersonIdentityType.add(piRtn);

        //create facility
        FacilityTest facilityTest = new FacilityTest();
        Long facilityId = facilityTest.createFacility();

        createdFacility = facilityService.get(uc, facilityId);
        expectedFacilityName = createdFacility.getFacilityName();

        //create supervision
        SupervisionType supervision = new SupervisionType();
        supervision.setPersonIdentityId(createdPersonIdentityType.get(0).getPersonIdentityId());
        supervision.setFacilityId(createdFacility.getFacilityIdentification());

        supervision.setSupervisionDisplayID(SUPERVISION_DISPLAY_ID);
        supervision.setSupervisionStartDate(new Date(9999));

        createdSupervision = supervisionService.create(uc, supervision);

        expectedSupervisionStatus = createdSupervision.getSupervisionStatusFlag();

        //create housing assignment.
        StaffTest staffTest = new StaffTest();
        Long staffId = staffTest.createStaff();

        FacilityInternalLocationTest filTest = new FacilityInternalLocationTest();
        filTest.createFILTree(createdFacility.getFacilityIdentification());
        Long filId = filTest.getCell1();

        ActivityTest activityTest = new ActivityTest();
        Long activityId = activityTest.createActivityForHousing();

        HousingLocationAssignType housingLocationAssignType = new HousingLocationAssignType();
        housingLocationAssignType.setAssignedBy(staffId);
        housingLocationAssignType.setMovementReason("OFFREQ");
        housingLocationAssignType.setLocationTo(filId);
        housingLocationAssignType.setFacilityId(createdFacility.getFacilityIdentification());
        housingLocationAssignType.setSupervisionId(createdSupervision.getSupervisionIdentification());
        housingLocationAssignType.setActivityId(activityId);
        housingLocationAssignType.setAssignedDate(new Date());
        housingLocationAssignType.setOverallOutcome("SUTBL");

        housingBedService.assignHousingLocation(uc, housingLocationAssignType);

        //create case info 1.
        CaseInfoType caseInfo = generateDefaultCaseInfo(false);
        caseInfo.setSupervisionId(createdSupervision.getSupervisionIdentification());

        caseInfo = service.createCaseInfo(uc, caseInfo, null);

        createdCaseInfos.add(caseInfo);

        //create case info 2
        caseInfo = generateDefaultCaseInfo(false);
        caseInfo.setSupervisionId(createdSupervision.getSupervisionIdentification());

        caseInfo = service.createCaseInfo(uc, caseInfo, null);

        createdCaseInfos.add(caseInfo);

        //create Organization
        Organization organization = new Organization();
        organization.setOrganizationName("AAA company");
        organization.setOrganizationAbbreviation("AAA");

        createdOrganization = organizationService.create(uc, organization);

    }

    @Test(dependsOnMethods = { "reset" })
    public void testInquireCases() {

        //inquire by CaseIdentifierType
        InquiryCaseSearchType caseSearchType = new InquiryCaseSearchType();
        caseSearchType.setCaseIdentifierType(CASE_IDENTIFIER_TYPE_ARREST);

        CaseOffendersReturnType ret = service.inquireCaseOffenders(uc, caseSearchType, 0l, 100l, null);

        verifyCaseOffendersReturn(ret, caseSearchType);

        //inquire by CaseIdentifierNumber
        caseSearchType = new InquiryCaseSearchType();
        caseSearchType.setCaseIdentifierNumber("ARREST1");

        ret = service.inquireCaseOffenders(uc, caseSearchType, 0l, 100l, null);

        verifyCaseOffendersReturn(ret, caseSearchType);

        //inquire by CaseIssuedDateFrom
        caseSearchType = new InquiryCaseSearchType();
        caseSearchType.setCaseIssuedDateFrom(new Date());

        ret = service.inquireCaseOffenders(uc, caseSearchType, 0l, 10l, null);

        verifyCaseOffendersReturn(ret, caseSearchType);

        //inquire by CaseIssuedDateFrom & CaseIssuedDateTO
        caseSearchType = new InquiryCaseSearchType();
        caseSearchType.setCaseIssuedDateFrom(new Date());
        caseSearchType.setCaseIssuedDateTo(new Date());

        ret = service.inquireCaseOffenders(uc, caseSearchType, 0l, 100l, null);

        verifyCaseOffendersReturn(ret, caseSearchType);

        //inquire by CaseIdentifierType & CaseIdentifierNumber
        caseSearchType = new InquiryCaseSearchType();
        caseSearchType.setCaseIdentifierType(CASE_IDENTIFIER_TYPE_ARREST);
        caseSearchType.setCaseIdentifierNumber("ARREST1");

        ret = service.inquireCaseOffenders(uc, caseSearchType, 0l, 100l, null);

        verifyCaseOffendersReturn(ret, caseSearchType);

        //can not find data: inquire by CaseIdentifierType & CaseIdentifierNumber
        caseSearchType = new InquiryCaseSearchType();
        caseSearchType.setCaseIdentifierType(CASE_IDENTIFIER_TYPE_DOCKET);
        caseSearchType.setCaseIdentifierNumber("ARREST1");

        ret = service.inquireCaseOffenders(uc, caseSearchType, 0l, 100l, null);
        Assert.assertEquals(ret.getTotalSize().intValue(), 0);

        //negative: only To date is set
        caseSearchType = new InquiryCaseSearchType();
        caseSearchType.setCaseIssuedDateTo(new Date());

        try {
            ret = service.inquireCaseOffenders(uc, caseSearchType, 0l, 10l, null);
            Assert.assertEquals(ret, null);
        } catch (Exception e) {
            Assert.assertTrue(e instanceof InvalidInputException);
        }

    }

    @Test(dependsOnMethods = { "reset" })
    public void testCreateCourtActivityWithMovement() {
        CourtMovementReturnTimeConfigurationType returnTimeConfig = new CourtMovementReturnTimeConfigurationType();
        returnTimeConfig.setReturnTime("16:00");
        CourtMovementReturnTimeConfigurationType rtn = movementService.setCourtMovementReturnTimeConfiguration(uc, returnTimeConfig);
        assert (rtn != null);

        CaseInfoType caseInfo = createdCaseInfos.get(0);
        Long caseId = caseInfo.getCaseInfoId();
        Long supervisionId = caseInfo.getSupervisionId();

        CourtActivityType courtActivity = generateDefaultCourtActivity(false);
        courtActivity.getCaseIds().add(caseId);

        CourtActivityMovementType movementDetailType = new CourtActivityMovementType();
        //movementDetailType.setCommment(MOVEMENT_COMMENT1);
        movementDetailType.setMovementReason(MOVEMENT_REASON1);
        movementDetailType.setStatus(MOVEMENT_STATUS1);
        movementDetailType.setSupervisionId(supervisionId);
        movementDetailType.setCourtActivityType(courtActivity);

        createdCourtActivityMovementType = service.createCourtActivityWithMovement(uc, movementDetailType);

        Assert.assertNotEquals(createdCourtActivityMovementType, null);
        verifyCourtActivityMovementType(createdCourtActivityMovementType, movementDetailType);

        createdCourtActivity = createdCourtActivityMovementType.getCourtActivityType();
        Assert.assertNotEquals(createdCourtActivity, null);

    }

    @Test(dependsOnMethods = { "testCreateCourtActivityWithMovement" })
    public void testRetrieveCourtActivityWithMovement() {
        Long id = createdCourtActivity.getCaseActivityIdentification();

        CourtActivityMovementType rtn = service.retrieveCourtActivityWithMovement(uc, id);
        verifyCourtActivityMovementType(rtn, createdCourtActivityMovementType);
    }

    @Test(dependsOnMethods = { "testRetrieveCourtActivityWithMovement" })
    public void testUpdateCourtActivityWithMovement() {
        createdCourtActivityMovementType.setCommment(MOVEMENT_COMMENT2);
        createdCourtActivityMovementType.setMovementReason(MOVEMENT_REASON2);
        createdCourtActivityMovementType.setStatus(MOVEMENT_STATUS2);
        createdCourtActivity.setActivityOccurredDate(new Date(2020, 01, 01));

        CourtActivityMovementType rtn = service.updateCourtActivityWithMovement(uc, createdCourtActivityMovementType);
        verifyCourtActivityMovementType(rtn, createdCourtActivityMovementType);

        createdCourtActivityMovementType = rtn;

        //WOR-5609
        CourtActivityType courtActivity = createdCourtActivityMovementType.getCourtActivityType();
        Set<Long> activityIds = courtActivity.getActivityIds();
        if (activityIds != null && !activityIds.isEmpty()) {
            MovementActivitySearchType search = new MovementActivitySearchType();
            search.setActivityIds(activityIds);
            MovementActivitiesReturnType externalMovement = movementService.search(uc, search, null, null, null);

            for (ExternalMovementActivityType externalMove : externalMovement.getExternalMovementActivity()) {
                externalMove.setMovementStatus("COMPLETED");
                externalMove.setMovementDate(new Date());
                externalMove.setToFacilityInternalLocationId(1L);
                movementService.update(uc, externalMove);
            }
        }

        createdCourtActivityMovementType = service.retrieveCourtActivityWithMovement(uc,
                createdCourtActivityMovementType.getCourtActivityType().getCaseActivityIdentification());
        courtActivity = createdCourtActivityMovementType.getCourtActivityType();
        courtActivity.setActivityOutcome("SENTC");
        courtActivity.setActivityComment(new CommentType(null, null, new Date(0), "update outcome and judge name"));
        courtActivity.setJudge("new judge name");
        rtn = service.updateCourtActivityWithMovement(uc, createdCourtActivityMovementType);
        verifyCourtActivityMovementType(rtn, createdCourtActivityMovementType);
        createdCourtActivityMovementType = rtn;
        //end of testing WOR-5069
    }

    @Test(dependsOnMethods = { "testUpdateCourtActivityWithMovement" })
    public void testRetrieveCourtActivityWithMovements() {
        CaseInfoType caseInfo = createdCaseInfos.get(0);
        Long caseId = caseInfo.getCaseInfoId();

        List<CourtActivityMovementType> rtn = service.retrieveCourtActivityWithMovements(uc, caseId, null);

        for (CourtActivityMovementType courtActivityMovementType : rtn) {
            verifyCourtActivityMovementType(courtActivityMovementType, createdCourtActivityMovementType);
        }
    }

    //////////////////////////////////////////private methods/////////////////////////////////////////

    @Test(dependsOnMethods = "reset")
    public void testRetriveCaseAffiliations() {
        Long affiliatedPersonIdentityId = createdPersonIdentityType.get(1).getPersonIdentityId();
        Long affiliatedOrganizationId = createdOrganization.getOrganizationIdentification();

        //create case affiliation1
        CaseAffiliationType affiliation1 = generateDefaultCaseAffiliation();
        affiliation1.setCaseAffiliationCategory(CATEGORY_PERSON);
        affiliation1.setCaseAffiliationRole(ROLE_VICTIM);
        affiliation1.setAffiliatedPersonIdentityId(affiliatedPersonIdentityId);
        createCaseAffilication(affiliation1);

        //create case affiliation2
        CaseAffiliationType affiliation2 = generateDefaultCaseAffiliation();
        Date start = getDateWithoutTime(2012, 01, 01);
        Date end = getDateWithoutTime(2012, 01, 02);
        affiliation2.setStartDate(start);
        affiliation2.setEndDate(end);
        affiliation2.setCaseAffiliationCategory(CATEGORY_PERSON_ORG);
        affiliation2.setCaseAffiliationRole(ROLE_WITNESS);
        affiliation2.setAffiliatedOrganizationId(affiliatedOrganizationId);
        affiliation2.setAffiliatedPersonIdentityId(affiliatedPersonIdentityId);
        createCaseAffilication(affiliation2);

        Long caseId = createdCaseInfos.get(0).getCaseInfoId();

        //test get both
        List<CaseAffiliationType> ret = service.retrieveCaseAffiliations(uc, caseId, null);
        Assert.assertNotNull(ret);
        Assert.assertEquals(ret.size(), 2);

        //test get active one
        ret = service.retrieveCaseAffiliations(uc, caseId, true);
        Assert.assertNotNull(ret);
        Assert.assertEquals(ret.size(), 1);
        CaseAffiliationType rtnType = ret.get(0);
        Assert.assertEquals(rtnType.getAffiliatedPersonIdentityId(), affiliatedPersonIdentityId);
        Assert.assertEquals(rtnType.getCaseAffiliationCategory(), CATEGORY_PERSON);
        Individual individual = rtnType.getAffiliatedPersonInfo();
        Assert.assertEquals(individual.getFirstName(), createdPersonIdentityType.get(1).getFirstName());
        Assert.assertEquals(individual.getLastName(), createdPersonIdentityType.get(1).getLastName());
        Assert.assertEquals(rtnType.getCaseAffiliationRole(), ROLE_VICTIM);
        Assert.assertEquals(rtnType.getStartDate(), getDatePartial(new Date()));
        Assert.assertNull(rtnType.getEndDate());

        //test get inactive one
        ret = service.retrieveCaseAffiliations(uc, caseId, false);
        Assert.assertNotNull(ret);
        Assert.assertEquals(ret.size(), 1);
        rtnType = ret.get(0);
        Assert.assertEquals(rtnType.getAffiliatedPersonIdentityId(), affiliatedPersonIdentityId);
        individual = rtnType.getAffiliatedPersonInfo();
        Assert.assertEquals(individual.getFirstName(), createdPersonIdentityType.get(1).getFirstName());
        Assert.assertEquals(individual.getLastName(), createdPersonIdentityType.get(1).getLastName());
        Assert.assertEquals(rtnType.getAffiliatedOrganizationId(), affiliatedOrganizationId);
        Assert.assertEquals(rtnType.getCaseAffiliationRole(), ROLE_WITNESS);
        Assert.assertEquals(rtnType.getAffiliatedOrganizationName(), createdOrganization.getOrganizationName());
        Assert.assertEquals(rtnType.getCaseAffiliationCategory(), CATEGORY_PERSON_ORG);
        Assert.assertEquals(rtnType.getStartDate(), start);
        Assert.assertEquals(rtnType.getEndDate(), end);

        //test can not get data
        ret = service.retrieveCaseAffiliations(uc, 999L, null);
        Assert.assertNull(ret);

    }

    /**
     * @param ret
     */
    private void verifyCaseOffendersReturn(CaseOffendersReturnType ret, InquiryCaseSearchType caseSearchType) {
        Assert.assertNotEquals(ret, null);

        List<CaseOffenderType> caseOffenders = ret.getCaseOffenders();
        Assert.assertNotEquals(caseOffenders, null);
        Assert.assertTrue(caseOffenders.size() <= ret.getTotalSize().intValue());

        for (CaseOffenderType caseOffender : ret.getCaseOffenders()) {
            if (caseSearchType.getCaseIdentifierType() != null) {
                Assert.assertEquals(caseOffender.getCaseIdentifierType(), caseSearchType.getCaseIdentifierType());
            }
            if (caseSearchType.getCaseIdentifierNumber() != null) {
                Assert.assertEquals(caseOffender.getCaseIdentifierNumber(), caseSearchType.getCaseIdentifierNumber());
            }
            if (caseSearchType.getCaseIssuedDateFrom() != null && caseSearchType.getCaseIssuedDateTo() != null) {
                Assert.assertTrue(caseOffender.getCaseIssuedDate().getTime() <= getDatePartial(caseSearchType.getCaseIssuedDateTo()).getTime());
                Assert.assertTrue(caseOffender.getCaseIssuedDate().getTime() >= getDatePartial(caseSearchType.getCaseIssuedDateFrom()).getTime());
            } else if (caseSearchType.getCaseIssuedDateFrom() != null) {
                Assert.assertEquals(caseOffender.getCaseIssuedDate(), getDatePartial(caseSearchType.getCaseIssuedDateFrom()));
            }

            Assert.assertNotNull(caseOffender.getCaseId());

            Supervision supervision = caseOffender.getSupervision();
            Assert.assertEquals(supervision.getSupervisionDisplayId(), SUPERVISION_DISPLAY_ID);
            Assert.assertEquals(supervision.getFirstName(), FIRST_NAME);
            Assert.assertEquals(supervision.getLastName(), LAST_NAME);
            Assert.assertEquals(supervision.getHousingFacility(), expectedFacilityName);
            Assert.assertEquals(supervision.getSupervisionStatus(), expectedSupervisionStatus);
        }

    }
    ///////////////////////Private methods////////////////////////////////////////////////////////////

    private CourtActivityType generateDefaultCourtActivity(boolean onlyMandatoryFields) {

        //courtActivity1: madatory fields
        CourtActivityType courtActivity = new CourtActivityType();

        //test an empty object;
        //assertEquals(service.createCaseActivity(uc, courtActivity).getReturnCode().longValue(), 2002l);

        //test ActivityCategory has value.
        courtActivity.setActivityCategory("COURT");
        //assertEquals(service.createCaseActivity(uc, courtActivity).getReturnCode().longValue(), 2002l);

        //test ActivityType has value.
        courtActivity.setActivityType("INITCRT");
        //assertEquals(service.createCaseActivity(uc, courtActivity).getReturnCode().longValue(), 2002l);

        //test ActivityStatus has value.
        courtActivity.setActivityStatus("ACTIVE");
        //assertEquals(service.createCaseActivity(uc, courtActivity).getReturnCode().longValue(), 2002l);

        //test ActivityOccurredDate has value.
        courtActivity.setActivityOccurredDate(new Date());
        //assertEquals(service.createCaseActivity(uc, courtActivity).getReturnCode().longValue(), 2002l);

        if (onlyMandatoryFields) {
            return courtActivity;
        }

        //test ActivityCategory has value.
        courtActivity.setActivityCategory("COURT");

        //test ActivityType has value.
        courtActivity.setActivityType("INITCRT");

        //test ActivityStatus has value.
        courtActivity.setActivityStatus("ACTIVE");

        //test ActivityOccurredDate has value.
        courtActivity.setActivityOccurredDate(new Date());

        //test associations positive.
        //		associations = new HashSet<AssociationType>();
        //		asso1 = new AssociationType(AssociationToClass.ACTIVITY.value(), 102L);
        //		associations.add(asso1);

        //		courtActivity.setAssociations(associations);

        //all mandatory fields have values.
        //		asso2 = new AssociationType(AssociationToClass.CASE_INFORMATION.value(), 202L);
        //		associations.add(asso2);

        //add optional elements:

        //test ActivityStatus has value.
        courtActivity.setActivityOutcome("DISMISS");

        //test comment
        CommentType comment = new CommentType();
        comment.setComment("comment1");
        comment.setCommentDate(new Date());
        courtActivity.setActivityComment(comment);

        //test judge
        courtActivity.setJudge("judge1");

        //test facility association
        courtActivity.setFacilityId(999L);

        //test facility association
        courtActivity.setFacilityInternalLocationId(998L);

        //test facility association
        courtActivity.setOrganizationId(999L);

        //test static associations positive
        Set<Long> chargeAssociations = new HashSet<Long>();
        Set<Long> orderInitialAssociations = new HashSet<Long>();
        Set<Long> orderResultAssociations = new HashSet<Long>();

        courtActivity.setChargeIds(chargeAssociations);
        courtActivity.setOrderInititatedIds(orderInitialAssociations);
        courtActivity.setOrderResultedIds(orderResultAssociations);

        return courtActivity;
    }

    private void clientlogin(String user, String password) {

        SecurityClient client;

        try {
            client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password); //"devtest", "devtest" nice regular service user
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private CaseInfoType generateDefaultCaseInfo(boolean onlyMandatory) {
        CaseInfoType caseInfo = new CaseInfoType();
        Date currentDate = new Date();

        //Mandatory fields:
        caseInfo.setCaseCategory(CASE_CATEGORY_IJ);
        caseInfo.setCaseType(CASE_TYPE_ADULT);
        caseInfo.setCreatedDate(currentDate);
        caseInfo.setDiverted(true);
        caseInfo.setDivertedSuccess(true);
        caseInfo.setActive(true);
        caseInfo.setSupervisionId(3L);

        if (onlyMandatory) {
            return caseInfo;
        }

        //Add optional fields:
        caseInfo.setIssuedDate(currentDate);
        caseInfo.setStatusChangeReason(CASE_STATUS_REASON_DISMISSED);
        caseInfo.setSentenceStatus(SENTENCE_STATUS_SENTENCED);
        caseInfo.setFacilityId(1L);

        CommentType comment = new CommentType();
        comment.setUserId("1");
        comment.setCommentDate(currentDate);
        comment.setComment("createCaseInfoPositive");
        caseInfo.setComment(comment);

        //TODO: caseInfo.getAssociations().add(new AssociationType(CASE_ACTIVITY, 1L));

        //Add case identifiers
        CaseIdentifierType caseIdentifier1 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_DOCKET, "docket1", false, 1L, 1L, "createCaseInfoPositive comment1", true);

        CaseIdentifierType caseIdentifier2 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_ARREST, "ARREST1", false, 1L, 1L, "createCaseInfoPositive comment2",
                false);

        CaseIdentifierType caseIdentifier3 = new CaseIdentifierType(null, CASE_IDENTIFIER_TYPE_ARREST, "ARREST2", false, 1L, 1L, "createCaseInfoPositive comment3",
                false);

        caseInfo.getCaseIdentifiers().add(caseIdentifier1);
        caseInfo.getCaseIdentifiers().add(caseIdentifier2);
        caseInfo.getCaseIdentifiers().add(caseIdentifier3);

        return caseInfo;
    }

    private Date getDateWithoutTime(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.DAY_OF_MONTH, day);

        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    @SuppressWarnings("unused")
    private void verfiyCreatedCaseInfo(CaseInfoType actual, CaseInfoType expected) {
        assertNotNull(actual.getCaseInfoId());
        //TODO: assertEquals(actual.getAssociations(), expected.getAssociations());
        assertEquals(actual.getCaseCategory(), expected.getCaseCategory());

        verifyCreatedCaseIdentifiers(actual.getCaseIdentifiers(), expected.getCaseIdentifiers());

        assertEquals(actual.getCaseInfoIds(), expected.getCaseInfoIds());
        assertEquals(actual.getStatusChangeReason(), expected.getStatusChangeReason());
        assertEquals(actual.getCaseType(), expected.getCaseType());
        assertEquals(actual.getChargeIds(), expected.getChargeIds());

        verifyCreatedComment(actual.getComment(), expected.getComment());

        assertEquals(actual.getConditionIds(), expected.getConditionIds());
        assertEquals(getDatePartial(actual.getCreatedDate()), getDatePartial(expected.getCreatedDate()));
        assertEquals(actual.getFacilityId(), expected.getFacilityId());
        assertEquals(getDatePartial(actual.getIssuedDate()), getDatePartial(expected.getIssuedDate()));
        assertEquals(actual.getOrderSentenceIds(), expected.getOrderSentenceIds());
        assertEquals(actual.getSentenceStatus(), expected.getSentenceStatus());
        assertEquals(actual.getSupervisionId(), expected.getSupervisionId());
        assertEquals(actual.isActive(), expected.isActive());
        assertEquals(actual.isDiverted(), expected.isDiverted());
        assertEquals(actual.isDivertedSuccess(), expected.isDivertedSuccess());
    }

    /**
     * @param createdCourtActivity2
     * @param courtActivity
     */
    private void verifyCreatedCourtActivity(CourtActivityType createdCourtActivity, CourtActivityType courtActivity) {
        assertNotNull(createdCourtActivity.getCaseActivityIdentification());
        assertEquals(createdCourtActivity.getActivityCategory(), courtActivity.getActivityCategory());
        assertEquals(createdCourtActivity.getActivityType(), courtActivity.getActivityType());
        assertEquals(createdCourtActivity.getActivityOccurredDate(), courtActivity.getActivityOccurredDate());
        assertEquals(createdCourtActivity.getActivityStatus(), courtActivity.getActivityStatus());
        //assertEquals(createdCourtActivity.getAssociations().size(), courtActivity.getAssociations().size());
        assertEquals(createdCourtActivity.getChargeIds().size(), courtActivity.getChargeIds().size());
        assertEquals(createdCourtActivity.getOrderInititatedIds().size(), courtActivity.getOrderInititatedIds().size());
        assertEquals(createdCourtActivity.getOrderResultedIds().size(), courtActivity.getOrderResultedIds().size());
        assertEquals(createdCourtActivity.getFacilityId(), courtActivity.getFacilityId());
        assertEquals(createdCourtActivity.getFacilityInternalLocationId(), courtActivity.getFacilityInternalLocationId());
        assertEquals(createdCourtActivity.getActivityOutcome(), courtActivity.getActivityOutcome());

    }

    /**
     * @param rtn
     * @param createdCourtActivityMovementType
     */
    private void verifyCourtActivityMovementType(CourtActivityMovementType rtn, CourtActivityMovementType courtActivityMovementType) {

        CourtActivityType courtActivityType1 = rtn.getCourtActivityType();
        CourtActivityType courtActivityType2 = courtActivityMovementType.getCourtActivityType();
        verifyCreatedCourtActivity(courtActivityType1, courtActivityType2);

        assertEquals(rtn.getStatus(), courtActivityMovementType.getStatus());
        assertEquals(rtn.getCommment(), courtActivityMovementType.getCommment());
        assertEquals(rtn.getMovementReason(), courtActivityMovementType.getMovementReason());
        assertEquals(rtn.getSupervisionId(), courtActivityMovementType.getSupervisionId());

    }

    private void verifyCreatedComment(CommentType actual, CommentType expected) {
        if ((actual == null && expected == null) || (actual != null && expected == null) ||
                (actual == null && expected != null)) {
            Assert.assertEquals(actual, expected);
            return;
        }

        assertEquals(actual.getComment(), expected.getComment());
        assertEquals(actual.getCommentDate(), expected.getCommentDate());
        assertEquals(actual.getUserId(), expected.getUserId());
    }

    private void verifyCreatedCaseIdentifiers(List<CaseIdentifierType> actual, List<CaseIdentifierType> expected) {
        if ((actual == null && expected == null) || (actual != null && expected == null) ||
                (actual == null && expected != null)) {
            Assert.assertEquals(actual, expected);
            return;
        }

        Set<CaseIdentifierType> actual1 = new HashSet<CaseIdentifierType>(actual);
        Set<CaseIdentifierType> expected1 = new HashSet<CaseIdentifierType>(expected);
        Assert.assertEquals(actual1, expected1);
    }

    private CaseAffiliationType generateDefaultCaseAffiliation() {
        Long caseId = createdCaseInfos.get(0).getCaseInfoId();

        CaseAffiliationType caseAffiliation = new CaseAffiliationType();

        caseAffiliation.setCaseAffiliationType(TYPE_LEGAL);

        caseAffiliation.setStartDate(new Date());

        caseAffiliation.setAffiliatedCaseId(caseId);

        return caseAffiliation;
    }

    private void createCaseAffilication(CaseAffiliationType caseAffiliation) {
        CaseAffiliationType rtn = service.createCaseAffiliation(uc, caseAffiliation);
        Assert.assertNotNull(rtn);

        createdCaseAffiliations.add(rtn);
    }
}
