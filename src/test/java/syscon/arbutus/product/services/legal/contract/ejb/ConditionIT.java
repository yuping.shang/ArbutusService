package syscon.arbutus.product.services.legal.contract.ejb;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.legal.contract.dto.condition.*;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * @author wmadruga, lhan
 */
public class ConditionIT extends BaseIT {

    //BAIL Condition
    private static final String TYPE3 = "BAIL"; // Condition type of Charge
    private static final String CATEGORY3 = "LEGL"; // Financial
    private static final String SUB_CATEGORY3 = "BAIL"; // Restitution
    private static final String OBJECTIVES = "CAN";
    private UserContext uc = null;
    private Logger log = LoggerFactory.getLogger(ConditionIT.class);
    private ConditionType ret;
    private LegalService service;
    public ConditionIT() {
        lookupJNDILogin();
    }

    @BeforeClass
    public void beforeClass() {
        lookupJNDILogin();
        //localLogin();
    }

    @BeforeMethod
    public void beforeMethod() {

    }

    @Test
    public void testLookupJNDI() {
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "testLookupJNDI" })
    public void deleteAll() {

        Long ret = service.deleteAll(uc);
        assert (ret.equals(ReturnCode.Success.returnCode()));
    }

    @Test(dependsOnMethods = "deleteAll", enabled = true)
    public void createBAILConditionTest() {
        //Creating a bail condition.
        ConditionType condition = new ConditionType();
        condition.setConditionType(TYPE3);
        condition.setConditionCategory(CATEGORY3);
        condition.setConditionSubCategory(SUB_CATEGORY3);
        condition.setIsConditionStatus(true);
        Date currentDate = getCurrentDate();
        condition.setConditionStartDate(currentDate);
        condition.setConditionEndDate(currentDate);
        condition.setOrderSentenceId(1L);
        condition.setConditionDetails("details.");
        condition.setShortName("short name");

        ret = service.createCondition(uc, condition);
        assertNotNull(ret);

        assert (ret.getOrderSentenceId().equals(1L));
        assert (ret.getConditionType().equals(TYPE3));
        assert (ret.getConditionCategory().equals(CATEGORY3));
        assert (ret.getConditionSubCategory().equals(SUB_CATEGORY3));
        assert (ret.getIsConditionStatus().equals(true));
        assert (ret.getConditionDetails().equals("details."));
        Assert.assertEquals(ret.getConditionStartDate(), currentDate);
        Assert.assertEquals(ret.getConditionEndDate(), currentDate);
        assert (ret.getConditionCreateDate() != null);
    }

    @Test(dependsOnMethods = "createBAILConditionTest", enabled = true)
    public void getCondition() {
        //Creating a bail condition.
        ConditionType condition = new ConditionType();
        condition.setConditionType(TYPE3);
        condition.setConditionCategory(CATEGORY3);
        condition.setConditionSubCategory(SUB_CATEGORY3);
        condition.setIsConditionStatus(true);
        Date currentDate = getCurrentDate();
        condition.setConditionStartDate(currentDate);
        condition.setConditionEndDate(currentDate);
        condition.setOrderSentenceId(1L);
        condition.setConditionDetails("details.");
        condition.setShortName("short name");
        ret = service.createCondition(uc, condition);
        assertNotNull(ret);

        ret = service.getCondition(uc, ret.getConditionId());
        assertNotNull(ret);

        assert (ret.getOrderSentenceId().equals(1L));
        assert (ret.getConditionType().equals(TYPE3));
        assert (ret.getConditionCategory().equals(CATEGORY3));
        assert (ret.getConditionSubCategory().equals(SUB_CATEGORY3));
        assert (ret.getIsConditionStatus().equals(true));
        assert (ret.getConditionDetails().equals("details."));
        Assert.assertEquals(ret.getConditionStartDate(), currentDate);
        Assert.assertEquals(ret.getConditionEndDate(), currentDate);
        assert (ret.getConditionCreateDate() != null);
    }

    @Test(dependsOnMethods = { "getCondition" }, enabled = true)
    public void updateBailConditionTest() {

        // Perform changes and updates
        ConditionType newCondition = ret;

        //Condition start date, end date, status and details
        newCondition.setConditionStartDate(getCurrentDate());
        newCondition.setConditionEndDate(getCurrentDate());
        newCondition.setIsConditionStatus(null);
        newCondition.setConditionDetails("new condition detail.");

        ret = service.updateCondition(uc, newCondition);

        Assert.assertEquals(ret.getConditionStartDate(), newCondition.getConditionStartDate());
        Assert.assertEquals(ret.getConditionEndDate(), newCondition.getConditionEndDate());
        Assert.assertEquals(ret.getIsConditionStatus(), newCondition.getIsConditionStatus());
        Assert.assertEquals(ret.getConditionDetails(), newCondition.getConditionDetails());

        //only change detail:
        newCondition = ret;
        newCondition.setConditionDetails("only change detail");
        ret = service.updateCondition(uc, newCondition);

        Assert.assertEquals(ret.getConditionDetails(), newCondition.getConditionDetails());

    }

    @Test(dependsOnMethods = { "updateBailConditionTest" }, enabled = true)
    public void deleteBailConditionTest() {
        //Creating a bail condition.
        ConditionType condition = new ConditionType();
        condition.setConditionType(TYPE3);
        condition.setConditionCategory(CATEGORY3);
        condition.setConditionSubCategory(SUB_CATEGORY3);
        condition.setIsConditionStatus(true);
        Date currentDate = getCurrentDate();
        condition.setConditionStartDate(currentDate);
        condition.setConditionEndDate(currentDate);
        condition.setOrderSentenceId(1L);
        condition.setConditionDetails("details.");
        condition.setShortName("short name");
        ConditionType newCondition = service.createCondition(uc, condition);

        Long id = newCondition.getConditionId();
        Long deleteRtn = service.deleteCondition(uc, id);

        assertEquals(deleteRtn, ReturnCode.Success.returnCode());

        //can not get current one.
        try {
            service.getCondition(uc, id);
        } catch (Exception e) {
            Assert.assertTrue(e instanceof DataNotExistException);
        }

    }

    @Test(dependsOnMethods = { "updateBailConditionTest" }, enabled = true)
    public void retrieveBailCondition() {
        Long bailId = 994L;
        Boolean activeFlag = true;

        for (int i = 0; i < 10; i++) {
            //Creating a bail condition.
            ConditionType condition = new ConditionType();
            condition.setConditionType(TYPE3);
            condition.setConditionCategory(CATEGORY3);
            condition.setConditionSubCategory(SUB_CATEGORY3);
            condition.setIsConditionStatus(activeFlag);
            Date currentDate = getCurrentDate();
            condition.setConditionStartDate(currentDate);
            condition.setConditionEndDate(currentDate);
            condition.setOrderSentenceId(bailId);
            condition.setConditionDetails("details.");
            condition.setShortName("short name");
            ret = service.createCondition(uc, condition);
            assertNotNull(ret);
            activeFlag = false;
        }

        //Retrieve By Bail Id
        List<ConditionType> conditions = service.retrieveConditionBy(uc, null, null, bailId, null, null);
        Assert.assertTrue(conditions != null);

        Assert.assertTrue(conditions.size() == 10);

        for (ConditionType condition : conditions) {
            Assert.assertEquals(condition.getOrderSentenceId(), bailId);
        }

        conditions = service.retrieveConditionBy(uc, null, null, bailId, null, true);
        Assert.assertTrue(conditions != null);
        Assert.assertTrue(conditions.size() == 1);
        for (ConditionType condition : conditions) {
            Assert.assertEquals(condition.getOrderSentenceId(), bailId);
        }
    }

    /**
     * To verify the issue of Update Bail: Cannot change condition status resolved.
     */
    @Test(dependsOnMethods = { "retrieveBailCondition" })
    public void testWOR_4587() {
        //to verify the issue resolved: Update Bail: Cannot change condition status
        ConditionType newCondition = ret;

        //Condition status can be changed to null
        newCondition = ret;
        newCondition.setIsConditionStatus(null);
        ret = service.updateCondition(uc, newCondition);
        Assert.assertEquals(ret.getIsConditionStatus(), newCondition.getIsConditionStatus());

        //Condition status can be changed to false
        newCondition = ret;
        newCondition.setIsConditionStatus(false);
        ret = service.updateCondition(uc, newCondition);
        Assert.assertEquals(ret.getIsConditionStatus(), newCondition.getIsConditionStatus());

        //Condition status can be changed to true
        newCondition = ret;
        newCondition.setIsConditionStatus(true);
        ret = service.updateCondition(uc, newCondition);
        Assert.assertEquals(ret.getIsConditionStatus(), newCondition.getIsConditionStatus());

    }

    //for release
    private void lookupJNDILogin() {

        log.info("Before JNDI lookup");

        service = (LegalService) JNDILookUp(this.getClass(), LegalService.class);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();

    }

	/*
     * MISC
	 */

    private Date getCurrentDate() {
        Date currentDate = new Date();
        return currentDate;
    }

    @Test
    public void testCreateConfigureCondition() {
        ConfigureConditionType type = new ConfigureConditionType();
        type.setActivationDate(new Date());
        type.setAttribute("attribute");
        type.setCategory(CATEGORY3);
        type.setCode("code");
        type.setShortName("shortName1");
        type.setType(TYPE3);
        type.setFullDetail("fullDetail");
        ConfigureConditionType retType = service.createConfigureCondition(uc, type);
        assertNotNull(retType);
    }

    @Test
    public void testUpdateConfigureCondition() {
        ConfigureConditionType type = new ConfigureConditionType();
        type.setId(1l);
        type.setActivationDate(new Date());
        type.setAttribute("attribute");
        type.setCategory(CATEGORY3);
        type.setCode("code");
        type.setShortName("shortName");
        type.setType(TYPE3);
        type.setFullDetail("fullDetail modified");
        type.setAutoPopulateToCasePlan(Boolean.TRUE);
        type.setObjective(OBJECTIVES);
        type.setCasePlanType(CasePlanType.CSPL.name());
        ConfigureConditionType retType = service.updateConfigureCondition(uc, type);
        assertNotNull(retType);
    }

    @Test
    public void testGetConfigureCondition() {
        ConfigureConditionType retType = service.getConfigureCondition(uc, 1l);
        assertNotNull(retType);
    }

    @Test
    public void testSearch() {
        ConfigureConditionSearchType searchCriteria = new ConfigureConditionSearchType();
        searchCriteria.setAttribute("attribute");
        searchCriteria.setCategory(CATEGORY3);
        searchCriteria.setCode("code");
        searchCriteria.setType(TYPE3);
        ConfigureConditionsReturnType retType = service.searchConfigureCondition(uc, searchCriteria, null, null, null, null);
        assertNotNull(retType);
        Long ll = 1l;
        assertEquals(retType.getTotalSize(), ll);
    }

    @Test
    public void testSearchAll() {
        ConfigureConditionsReturnType retType = service.searchConfigureCondition(uc, null, null, null, null, null);
        assertNotNull(retType);
        Long ll = 1l;
        assertEquals(retType.getTotalSize(), ll);
    }

    //@Test
    public void testDeleteConfigureCondition() {
        Long retType = service.deleteConfigureCondition(uc, 1l);
        assertNotNull(retType);
    }

    public ConditionType createBAILCondition(UserContext userContext) {
        //Creating a bail condition.
        ConditionType condition = new ConditionType();
        condition.setConditionType(TYPE3);
        condition.setConditionCategory(CATEGORY3);
        condition.setConditionSubCategory(SUB_CATEGORY3);
        condition.setIsConditionStatus(true);
        Date currentDate = getCurrentDate();
        condition.setConditionStartDate(currentDate);
        condition.setConditionEndDate(currentDate);
        condition.setOrderSentenceId(1L);
        condition.setConditionDetails("details.");
        condition.setShortName("short name");
        ret = service.createCondition(userContext, condition);
        return ret;
    }

    @Test
    public void createConditionCasePlanTest() {
        //Creating a bail condition.
        ConditionType condition = new ConditionType();
        condition.setConditionType(TYPE3);
        condition.setConditionCategory(CATEGORY3);
        condition.setConditionSubCategory(SUB_CATEGORY3);
        condition.setIsConditionStatus(true);
        Date currentDate = getCurrentDate();
        condition.setConditionStartDate(currentDate);
        condition.setConditionEndDate(currentDate);
        condition.setOrderSentenceId(1L);
        condition.setConditionDetails("details.");
        condition.setShortName("short name");
        condition.setCasePlanId(1l);

        ret = service.createCondition(uc, condition);
        assertNotNull(ret);

        assert (ret.getConditionType().equals(TYPE3));
        assert (ret.getConditionCategory().equals(CATEGORY3));
        assert (ret.getConditionSubCategory().equals(SUB_CATEGORY3));
        assert (ret.getIsConditionStatus().equals(true));
        assert (ret.getConditionDetails().equals("details."));
        Assert.assertEquals(ret.getConditionStartDate(), currentDate);
        Assert.assertEquals(ret.getConditionEndDate(), currentDate);
        assert (ret.getConditionCreateDate() != null);
    }

    @Test
    public void testSearchCondition() {
        ConditionSearchType search = new ConditionSearchType();
        search.setSupervisionId(1l);
        Set<Long> casePlanIds = new HashSet<Long>();
        casePlanIds.add(9l);
        search.setCasePlanIds(casePlanIds);
        ConditionsReturnType result = service.searchCondition(uc, search, null, null, null, null, null, null);
        assertNotNull(result);
        System.out.println(result.getConditions().size());
    }

    private enum CasePlanType {
        CSPL, RLPL, RETR;
    }
}
