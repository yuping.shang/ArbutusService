package syscon.arbutus.product.services.supervisiondetails.contract.ejb;

import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.exception.ArbutusException;
import syscon.arbutus.product.services.person.contract.dto.*;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.person.contract.test.ContractTestUtil;

import static org.testng.Assert.assertNotNull;

public class CreatePersonPositiveTest extends BaseIT {

    protected static final String SERVICE_MODULE = "Person-2.1";
    protected static final String SERVICE_NAME = "PersonService";
    private static Logger log = LoggerFactory.getLogger(CreatePersonPositiveTest.class);
    protected PersonService service;

    protected Long personId = null;

    public CreatePersonPositiveTest() {

        if (service == null) {
			service = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
		}
    }

    @SuppressWarnings("unused")
    private static PhotoType createImage(byte[] data) {
        PhotoType image = new PhotoType();
        image.setCaptureDate(DateTimeUtil.getCurrentUTCTime());
        image.setDefaultImage(true);
        image.getImage().setPhoto(data);
        image.setPersonId(1L);
        return image;
    }

    /**
     * @return the personId
     */
    public Long getPersonId() {
        return personId;
    }

    @BeforeTest
    public void beforeTest() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");

        service = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        assertNotNull(service);
        uc = super.initUserContext();
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
        assertNotNull(uc);
    }

    @Test(dependsOnMethods = "testJNDILookup")
    public void createPerson() {
        try {
            //assertEquals(service.deleteAll(uc), new Long(1));
            PersonType person = person();
            PersonType ret = service.createPersonType(uc, person);
            personId = ret.getPersonIdentification();

        } catch (ArbutusException e) {
            log.error("Error in Create person method." + e.getMessage());
        }
        assertNotNull(personId);
    }

    @Test(dependsOnMethods = { "createPerson" })
    public void getPerson() {

        PersonType ret = new PersonType();
        try {
            if (personId == null) {
				throw new ArbutusException("Invalid PersonId.");
			}
            ret = service.getPersonType(uc, personId);

        } catch (ArbutusException e) {
            log.error(e.getMessage());
        }
        assertNotNull(ret);
    }

    private PersonType person() throws ArbutusException {

        PersonType person = new PersonType();
        person.setActiveStatus("ACTIVE");        //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        person.setDeathDate(ContractTestUtil.getDateFromString("1997-12-21"));
        person.setAccent("India");
        EducationHistoryType eduHist = new EducationHistoryType();
        eduHist.setAreaOfStudy("HEA");            //new CodeType(ReferenceSet.EDUCATIONSTUDYAREA.name(),
        eduHist.setSchool("Uvic");
        eduHist.setEducationLevel("UNIVERSITY");    //new CodeType(ReferenceSet.EDUCATIONLEVEL.name(),
        person.getEducationHistories().add(eduHist);
        EducationHistoryType edu2 = new EducationHistoryType();
        edu2.setAreaOfStudy("his");            //new CodeType(ReferenceSet.EDUCATIONSTUDYAREA.name(),
        edu2.setSchool("Oxford");
        edu2.setEducationLevel("ELEMENTARY");    //new CodeType(ReferenceSet.EDUCATIONLEVEL.name(),
        person.getEducationHistories().add(edu2);

        //		person.getPrivilegeFilters().add("HP");

        EmploymentHistoryType emp = new EmploymentHistoryType();
        emp.setOccupation("564");            //new CodeType(ReferenceSet.EMPLOYEEOCCUPATION.name(),
        emp.setEmployerReference("apple");
        person.getEmploymentHistories().add(emp);

        HealthRecordHistoryType hrh = new HealthRecordHistoryType();
        hrh.setHealthCategory("DISAB");    //new CodeType(ReferenceSet.HEALTHRECORDCATEGORY.name(),
        hrh.setHealthIssue("d");        //new CodeType(ReferenceSet.HEALTHISSUE.name(),
        hrh.setHealthStatus("CHRONIC");    //new CodeType(ReferenceSet.HEALTHISSUESTATUS.name(),
        HealthRecordTreatmentType ht = new HealthRecordTreatmentType();
        ht.setTreatmentStartDate(ContractTestUtil.getDateFromString("1998-02-21"));
        ht.setHealthTreatment("pd");    //new CodeType(ReferenceSet.HEALTHRECORDTREATMENT.name(),
        ht.setTreatmentProvider("dr");        //new CodeType(ReferenceSet.HEALTHTREATMENTPROVIDER.name(),
        ht.setTreatmentComment("health record treatment comment");
        hrh.getTreatments().add(ht);

        HealthRecordTreatmentType ht2 = new HealthRecordTreatmentType();
        ht2.setHealthTreatment("tre");        //new CodeType(ReferenceSet.HEALTHRECORDTREATMENT.name(),
        ht2.setTreatmentEndDate(ContractTestUtil.getDateFromString("2001-02-21"));
        hrh.getTreatments().add(ht2);

        person.getHealthRecordHistories().add(hrh);

        LanguageType lan = new LanguageType();
        lan.setLanguage("en");        //new CodeType(ReferenceSet.LANGUAGE.name(),
        lan.setPrimary(true);
        person.getLanguages().add(lan);
        LanguageType fr = new LanguageType();
        fr.setLanguage("fr");        //new CodeType(ReferenceSet.LANGUAGE.name(),
        fr.setPrimary(false);
        person.getLanguages().add(fr);

        MilitaryHistoryType mi = new MilitaryHistoryType();
        mi.setBranch("Army");                //new CodeType(ReferenceSet.MILITARYBRANCH.name(),
        mi.getWarZones().add("IRQ");        //new CodeType(ReferenceSet.MILITARYWARZONE.name(),
        mi.getSpecializations().add("ava");    //new CodeType(ReferenceSet.MILITARYSPECIALIZATION.name(),
        person.getMilitaryHistories().add(mi);

        PersonalInformationType pi = new PersonalInformationType();
        pi.setBirthCity("Burnaby");
        pi.setCitizenship("DJI");            //new CodeType(ReferenceSet.CITIZENSHIP.name(),
        pi.setMaritalStatus("2");            //new CodeType(ReferenceSet.MARITALSTATUS.name(),
        pi.setNoOfDependents(1l);

        PhysicalIdentifierType phi = new PhysicalIdentifierType();
        phi.setBuild("MEDIUM");            //new CodeType(ReferenceSet.BUILD.name(),
        phi.setHairAppearance("SHLDR");    //new CodeType(ReferenceSet.HAIRAPPEARANCE.name(),

        person.setPhysicalIdentifier(phi);

        PhysicalMarkType pm = new PhysicalMarkType();
        pm.setMarkCategory("TAT");        //new CodeType(ReferenceSet.PHYSICALMARK.name(),
        pm.setMarkDescription("flower");
        pm.setMarkLocation("ARM");        //new CodeType(ReferenceSet.PHYSICALMARKLOCATION.name(),
        pm.setBodySide("F");            //new CodeType(ReferenceSet.PHYSICALMARKSIDE.name(),
        person.getPhysicalMarks().add(pm);

        SkillType sk = new SkillType();
        sk.setSkill("PERFIN");            //new CodeType(ReferenceSet.SKILL.name(),
        sk.setSubType("BAILINF");        //new CodeType(ReferenceSet.SKILLSUBTYPE.name(),
        person.getSkills().add(sk);
        SkillType sk2 = new SkillType();
        sk2.setSkill("LANG");            //new CodeType(ReferenceSet.SKILL.name(),
        sk2.setSubType("CCTV");            //new CodeType(ReferenceSet.SKILLSUBTYPE.name(),
        person.getSkills().add(sk2);

        SubstanceAbuseType sa = new SubstanceAbuseType();
        sa.setDrugType("E1");            //new CodeType(ReferenceSet.DRUG.name(),
        SubstanceAbuseTreatmentType tr = new SubstanceAbuseTreatmentType();
        tr.setTreatmentReceived("COU");    //new CodeType(ReferenceSet.DRUGTREATMENT.name(),
        tr.setTreatmentProvider("doc");
        sa.getTreatments().add(tr);

        SubstanceAbuseUseType use = new SubstanceAbuseUseType();
        use.setSubstanceUseLevel("FRE");    //new CodeType(ReferenceSet.DRUGUSELEVEL.name(),
        use.setSubstanceUsePeriod("one year");
        sa.getSubstanceUseLevels().add(use);
        SubstanceAbuseUseType use2 = new SubstanceAbuseUseType();
        use2.setSubstanceUseLevel("add");    //new CodeType(ReferenceSet.DRUGUSELEVEL.name(),
        use2.setSubstanceUseComment("second time");
        use2.setSubstanceUsePeriod("two year");
        sa.getSubstanceUseLevels().add(use2);

        person.getSubstanceAbuses().add(sa);
        return person;
    }

}

