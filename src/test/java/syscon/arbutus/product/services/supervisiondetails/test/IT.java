package syscon.arbutus.product.services.supervisiondetails.test;

import javax.naming.NamingException;
import java.util.Set;

import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.*;
import syscon.arbutus.product.services.registry.contract.dto.SystemProfileCategoryEnum;
import syscon.arbutus.product.services.registry.contract.dto.SystemProfileType;
import syscon.arbutus.product.services.registry.contract.interfaces.RegistryService;
import syscon.arbutus.product.services.supervision.contract.dto.supervisiondetails.SupervisionDetailType;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;
import syscon.arbutus.product.services.supervisiondetails.contract.ejb.CreateCautionPositiveTest;
import syscon.arbutus.product.services.supervisiondetails.contract.ejb.CreatePersonPositiveTest;
import syscon.arbutus.product.services.supervisiondetails.contract.ejb.CreateSupervisionPositiveTest;

import static org.testng.Assert.assertNotNull;

public class IT extends BaseIT {

    protected static final String SERVICE_MODULE = "SupervisionDetails-1.0";
    protected static final String SERVICE_NAME = "SupervisionDetailService";
    protected static Long supervisionId = null;
    protected static Long personId = null;
    protected static Long personIdentityId = null;
    protected static Set<Long> cautionIds = null;
    private static Logger log = LoggerFactory.getLogger(IT.class);
    protected SupervisionService supervisionService;
    private RegistryService registryService;

    @BeforeClass
    public void beforeTest() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");
        supervisionService = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        registryService = (RegistryService) JNDILookUp(this.getClass(), RegistryService.class);
        assertNotNull(supervisionService);

        uc = super.initUserContext();

        if (registryService.getPreferredSystemProfile(uc, SystemProfileCategoryEnum.HEIGHT_UNIT.value()) == null) {
            SystemProfileType systemProfile = new SystemProfileType(SystemProfileCategoryEnum.HEIGHT_UNIT.value(), "Metric (cm)", "cm", true);
            SystemProfileType systemProfileType = registryService.createSystemProfile(uc, systemProfile);
            assert (systemProfileType != null);
        }

        if (registryService.getPreferredSystemProfile(uc, SystemProfileCategoryEnum.DATE_FORMAT.value()) == null) {
            SystemProfileType systemProfile = new SystemProfileType(SystemProfileCategoryEnum.DATE_FORMAT.value(), "YYYY/MM/DD", "yyyy/MM/dd", true);
            SystemProfileType systemProfileType = registryService.createSystemProfile(uc, systemProfile);
            assert (systemProfileType != null);
        }

        if (registryService.getPreferredSystemProfile(uc, SystemProfileCategoryEnum.WEIGHT_UNIT.value()) == null) {
            SystemProfileType systemProfile = new SystemProfileType(SystemProfileCategoryEnum.WEIGHT_UNIT.value(), "Metric (cm)", "cm", true);
            SystemProfileType systemProfileType = registryService.createSystemProfile(uc, systemProfile);
            assert (systemProfileType != null);
        }
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(supervisionService);
        assertNotNull(uc);
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void create() {
        CreatePersonPositiveTest personTest = new CreatePersonPositiveTest();
        personTest.createPerson();
        personId = personTest.getPersonId();
        assertNotNull(personId);

        StaffTest stfTest = new StaffTest();
        Long staffId = stfTest.createStaff();

        CreateCautionPositiveTest cautionTest = new CreateCautionPositiveTest(personId, staffId);
        cautionTest.createCaution();
        cautionIds = cautionTest.getCautionIds();
        assertNotNull(cautionIds);

        PersonIdentityTest piTest = new PersonIdentityTest();
        personIdentityId = piTest.createPersonIdentity(personId);
        assertNotNull(personIdentityId);

        CreateSupervisionPositiveTest supervisionTest = new CreateSupervisionPositiveTest(personIdentityId);
        supervisionTest.create();
        supervisionId = supervisionTest.getSupervisionId();
        assertNotNull(supervisionId);
    }

    @AfterClass
    public void afterClass() {
        SupervisionTest supTest = new SupervisionTest();
        supTest.deleteAll();

        CreateCautionPositiveTest cautionTest = new CreateCautionPositiveTest();
        cautionTest.deleteAll();

        StaffTest stfTest = new StaffTest();
        stfTest.deleteAll();

        PersonIdentityTest piTest = new PersonIdentityTest();
        piTest.deleteAll();

        PersonTest psnTest = new PersonTest();
        psnTest.deleteAll();

        FacilityTest facTest = new FacilityTest();
        facTest.deleteAll();
    }

    @Test(dependsOnMethods = { "create" })
    public void validateSupervisionDetails() {
        supervisionService.getSupervisionDetails(uc, supervisionId);
        //    	supervisionService.getSupervisionDetails(uc, null);

        SupervisionDetailType ret = supervisionService.getSupervisionDetails(uc, supervisionId);
        Assert.assertNotNull(ret);

    }
}

