package syscon.arbutus.product.services.supervisiondetails.contract.ejb;

import javax.naming.NamingException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.exception.ArbutusException;
import syscon.arbutus.product.services.person.contract.dto.caution.CautionSearchType;
import syscon.arbutus.product.services.person.contract.dto.caution.CautionType;
import syscon.arbutus.product.services.person.contract.dto.caution.CautionsReturnType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.realization.util.BeanHelper;

import static org.testng.Assert.assertNotNull;

public class CreateCautionPositiveTest extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(CreateCautionPositiveTest.class);
    private static String CODE_SECURITY = "SECURITY";
    private static String CODE_V = "90";
    private static String CODE_K = "10";
    private static String CODE_S = "30";
    private static String CODE_I = "80";
    private static String CODE_P = "20";
    protected PersonService service;
    protected Long personId = 1L;
    protected Set<Long> cautionIds = new HashSet<Long>();

    private Long staffId;

    public CreateCautionPositiveTest(Long personId, Long staffId) {
        this.personId = personId;
        this.staffId = staffId;
        if (service == null) {
            service = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
            assertNotNull(service);
        }
    }

    public CreateCautionPositiveTest() {
        if (service == null) {
            service = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
            assertNotNull(service);
        }
    }

    /**
     * @return the cautionIds
     */
    public Set<Long> getCautionIds() {
        return cautionIds;
    }

    @BeforeTest
    public void beforeTest() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");
        service = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        assertNotNull(service);

        uc = super.initUserContext();
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
        assertNotNull(uc);
    }

    @Test(dependsOnMethods = "testJNDILookup")
    public void createCaution() {

        try {
            //assertEquals(service.deleteAll(uc), l(1));
            assertNotNull(personId);

            Date currentDate = BeanHelper.createDate();
            CautionType retCaution = service.createCautionType(uc, caution(currentDate, BeanHelper.nextNthDays(currentDate, 10), CODE_S, staffId));  //Present date
            cautionIds.add(retCaution.getCautionId());

            retCaution = service.createCautionType(uc,
                    caution(BeanHelper.getPastDate(currentDate, 10), BeanHelper.nextNthDays(currentDate, 10), CODE_V, staffId)); //past date
            cautionIds.add(retCaution.getCautionId());

            retCaution = service.createCautionType(uc, caution(BeanHelper.nextNthDays(currentDate, 1), BeanHelper.nextNthDays(currentDate, 20), CODE_I, staffId));
            cautionIds.add(retCaution.getCautionId());

            retCaution = service.createCautionType(uc, caution(BeanHelper.getPastDate(currentDate, 10), BeanHelper.getPastDate(currentDate, 1), CODE_K, staffId));
            cautionIds.add(retCaution.getCautionId());

            retCaution = service.createCautionType(uc, caution(BeanHelper.getPastDate(currentDate, 10), BeanHelper.createDate(), CODE_P, staffId));
            cautionIds.add(retCaution.getCautionId());

        } catch (ArbutusException e) {
            log.error(e.getMessage());
        }

        assertNotNull(cautionIds);

    }

    @Test(dependsOnMethods = { "createCaution" })
    public void getCaution() {
        /*CautionType ret = new CautionType();
        try{
		if (cautionId == null) throw new ArbutusException("Invalid PersonId.");
			ret = service.get(uc, cautionId);
		}catch (ArbutusException e)
    	{
			log.error(e.getMessage());
    	}    	
    	assertNotNull(ret);*/

    }

    @Test(dependsOnMethods = { "getCaution" })
    public void searcByPersonId() throws ArbutusException {
        log.info("searchCautions by PersionId Begin");

        Set<CautionType> cautions = searchByPersonId(uc, personId);

        assertNotNull(cautions);

    }

    public void deleteAll() {
        service.deleteAllCautionType(uc);
    }

    private Set<CautionType> searchByPersonId(UserContext uc, Long personId) throws ArbutusException {
        CautionSearchType searchCriteria = new CautionSearchType();
        searchCriteria.setPersonId(personId);
        Date presentDate = BeanHelper.createDate();
        searchCriteria.setEffectiveDateTo(presentDate);
        searchCriteria.setExpiryDateFrom(presentDate);
        CautionsReturnType returnType = service.searchCautionType(uc, null, searchCriteria, 0L, 200L, null, null);

        Set<CautionType> result = new HashSet<CautionType>();
        if (returnType.getCaution() != null) {
            for (CautionType ct : returnType.getCaution()) {
                result.add(ct);
            }
        }
        return result;
    }

    private CautionType caution(Date effectiveDt, Date expiryDt, String ct, Long staffId) throws ArbutusException {

        CautionType caution = new CautionType();
        caution.setCode(ct);
        caution.setCategory(CODE_SECURITY);
        caution.setEffectiveDate(effectiveDt);
        caution.setExpiryDate(expiryDt);
        caution.setAuthorizedBy(staffId);
        caution.setPersonId(personId);

        //Set<CommentType> comments = new HashSet<CommentType>();
        //comments.add(new CommentType(null, presentDate, "Comment 1"));
        //comments.add(new CommentType(null, presentDate, "Comment 2"));

        //caution.setComments(comments);      

        return caution;
    }

    public Long l(int val) {
        return new Long(val);
    }
}
