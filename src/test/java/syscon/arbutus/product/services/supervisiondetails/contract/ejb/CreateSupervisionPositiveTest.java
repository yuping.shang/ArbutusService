package syscon.arbutus.product.services.supervisiondetails.contract.ejb;

import javax.naming.NamingException;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.supervision.contract.dto.ImprisonmentStatusType;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

import static org.testng.Assert.assertNotNull;

public class CreateSupervisionPositiveTest extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(CreateSupervisionPositiveTest.class);
    protected SupervisionService service;
    protected Long supervisionId = null;
    protected Long personIdentityId = null;

    public CreateSupervisionPositiveTest(Long personIdentityId) {
        this.personIdentityId = personIdentityId;
        if (service == null) {
            service = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
            assertNotNull(service);
        }
    }

    /**
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    @BeforeTest
    public void beforeTest() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");

        service = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        assertNotNull(service);
        uc = super.initUserContext();
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
        assertNotNull(uc);
    }

    @Test
    public void create() {
        try {

            if (personIdentityId == null) {
                personIdentityId = 1L;
            }
            //assertEquals(service.deleteAll(uc), new Long(1));
            SupervisionType supType = addSupervision("dId", 5, personIdentityId);
            SupervisionType ret = service.create(uc, supType);
            supervisionId = ret.getSupervisionIdentification();
            if (supervisionId == null) {
				throw new ArbutusRuntimeException("Supervision can not be null.");
			}

        } catch (ArbutusRuntimeException e) {
            log.error("Error in Create Supervison method." + e.getMessage());
        }

        assertNotNull(supervisionId);
    }

    private SupervisionType addSupervision(String displayId, int n, Long personIdentityId) {
        SupervisionType supervision = new SupervisionType();

        supervision.setSupervisionDisplayID(displayId);
        supervision.setSupervisionStartDate(getDate(2012, 6, n));
        //supervision.setSupervisionEndDate(getDate(2014, 6, n*2));
        supervision.setDateOrientationProvided(getDate(2012, 7, n * 3));

        supervision.setPersonIdentityId(personIdentityId);
        Long facilityId = new FacilityTest().createFacility();
        supervision.setFacilityId(facilityId);

        // create ImprisonmentStatusSet
        String statusSet = "BOP";
        String statusSet2 = "DOC";
        String subStatusSet = "FPV";
        String subStatusSet2 = "SPV";

        // create ImprisonmentStatus
        ImprisonmentStatusType imprisonmentStatus = new ImprisonmentStatusType();
        imprisonmentStatus.setImprisonmentStatus(statusSet);
        imprisonmentStatus.setImprisonmentSubStatus(subStatusSet);
        imprisonmentStatus.setImprisonmentStatusStartDate(getDate(2012, 6, n * 2));

        // create ImprisonmentStatus2
        ImprisonmentStatusType imprisonmentStatus2 = new ImprisonmentStatusType();
        imprisonmentStatus2.setImprisonmentStatus(statusSet2);
        imprisonmentStatus2.setImprisonmentSubStatus(subStatusSet2);
        imprisonmentStatus2.setImprisonmentStatusStartDate(getDate(2012, 6, n));
        //		imprisonmentStatus2.setImprisonmentStatusEndDate(getDate(2012, 6, n*2));

        // create ImprisonmentStatus3
        ImprisonmentStatusType imprisonmentStatus3 = new ImprisonmentStatusType();
        imprisonmentStatus3.setImprisonmentStatus(statusSet2);
        imprisonmentStatus3.setImprisonmentSubStatus(subStatusSet2);
        imprisonmentStatus3.setImprisonmentStatusStartDate(getDate(2012, 6, n * 2));
        //		imprisonmentStatus3.setImprisonmentStatusEndDate(getDate(2012, 6, n*2));

        // set ImprisonmentStatusEntity
        supervision.getImprisonmentStatus().add(imprisonmentStatus);
        supervision.getImprisonmentStatus().add(imprisonmentStatus2);
        supervision.getImprisonmentStatus().add(imprisonmentStatus3);

        return supervision;
    }

    private Date getDate(int year, int month, int date) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, date);
        return cal.getTime();
    }

}
