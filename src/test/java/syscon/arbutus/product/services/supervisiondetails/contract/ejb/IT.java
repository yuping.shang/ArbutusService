package syscon.arbutus.product.services.supervisiondetails.contract.ejb;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.supervision.contract.dto.supervisiondetails.Location;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

@ModuleConfig
public class IT extends BaseIT {

    private static Logger log = Logger.getLogger(IT.class);

    SupervisionService supervisionService;

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        supervisionService = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        uc = super.initUserContext();
    }

    @AfterClass
    public void afterClass() {
        // psnTest.deleteAll();

    }

    @Test
    public void getOffenderStatusBySupervisionId() {
        // Just check if query is right
        String offenderStatus = supervisionService.getOffenderStatus(uc, 1L);
    }

    @Test
    public void getOffenderCurrentLocationBySupervisionId() {

        // Just check if query is right
        Location locationType = supervisionService.getOffenderCurrentLocation(uc, 1L);
    }
}