package syscon.arbutus.product.services.supervisiondetails.contract.ejb;

import javax.naming.NamingException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.exception.ArbutusException;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.person.contract.dto.personidentity.IdentifierType;
import syscon.arbutus.product.services.person.contract.dto.personidentity.OffNumConfigType;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.realization.util.ExceptionHelper;

import static org.testng.Assert.assertNotNull;

public class CreatePersonIdentifierPositiveTest extends BaseIT {

    protected static final String SERVICE_MODULE = "PersonIdentity-1.2";
    protected static final String SERVICE_NAME = "PersonIdentityService";
    private static Logger log = LoggerFactory.getLogger(CreatePersonIdentifierPositiveTest.class);
    protected PersonService service;
    protected Long personId = 1L;
    protected Long personIdentityId = null;

    public CreatePersonIdentifierPositiveTest(Long personId) {
        this.personId = personId;
        if (service == null) {
            service = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
            assertNotNull(service);
        }
    }

    /**
     * @return the personIdentityId
     */
    public Long getPersonIdentityId() {
        return personIdentityId;
    }

    @BeforeTest
    public void beforeTest() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");

        service = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        assertNotNull(service);

        uc = super.initUserContext();
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
        assertNotNull(uc);
    }

    @Test(dependsOnMethods = "testJNDILookup")
    public void createPersonIdentity() {
        try {
            //assertEquals(service.deleteAll(uc),l(1));
            if (personId == null) {
                throw new ArbutusRuntimeException("personId is null when creating person identity");
            }
            PersonIdentityType identity = identity(personId);
            PersonIdentityType ret = service.createPersonIdentityType(uc, identity, true, false);

            personIdentityId = ret.getPersonIdentityId();

        } catch (Exception e) {
            ExceptionHelper.handleException(log, e);
        }

        assertNotNull(personIdentityId);
    }

    public Long l(int val) {
        return new Long(val);
    }

    @Test(dependsOnMethods = "createPersonIdentity")
    public void getPersonIdentity() {
        try {
            PersonIdentityType piRet = service.getPersonIdentityType(uc, personIdentityId, null);
            assertNotNull(piRet);
            personId = piRet.getPersonId();
        } catch (Exception e) {
            log.error(e.getMessage());
        }

    }

    private PersonIdentityType identity(Long personIdentityId) throws ArbutusException {

        OffNumConfigType offNumConfig = new OffNumConfigType("NY 0000000000", true);
        service.createOffNumConfig(uc, offNumConfig);
        //

        PersonIdentityType personIdentity = null;
        Date dob = getDate(1985, 3, 10);
        Set<IdentifierType> identifiers = null;

        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", getDate(1980, 3, 10), getDate(1975, 3, 10), "G", "XXXX-XX", "Active"));

        personIdentity = new PersonIdentityType(null, personIdentityId, null, "MR", "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, null, null, null);

        return personIdentity;

    }

    private Date getDate(int year, int month, int date) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, date, 0, 0, 0);
        return cal.getTime();
    }
}

