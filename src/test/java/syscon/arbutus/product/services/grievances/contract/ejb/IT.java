/**
 * © 2013 by Syscon Justice Systems. All rights reserved.
 * Copying and distribution, any modifications or adaptation of the contents, or creation of derivative
 * works are not allowed without the express and prior written consent of Syscon Justice Systems.
 */
package syscon.arbutus.product.services.grievances.contract.ejb;

import javax.naming.NamingException;
import java.util.*;

import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.LegalTest;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.eventlog.contract.dto.TransactLogEntityTypes;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.FacilityInternalLocation;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.LocationAttributeType;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.UsageCategory;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.grievances.contract.dto.GrievancesSearchType;
import syscon.arbutus.product.services.grievances.contract.dto.GrievancesType;
import syscon.arbutus.product.services.grievances.contract.dto.TransactLogType;
import syscon.arbutus.product.services.grievances.contract.interfaces.GrievancesService;
import syscon.arbutus.product.services.person.contract.dto.PersonType;
import syscon.arbutus.product.services.person.contract.dto.StaffPosition;
import syscon.arbutus.product.services.person.contract.dto.StaffType;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.person.contract.test.ContractTestUtil;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;

public class IT extends BaseIT {
    private static Logger log = LoggerFactory.getLogger(IT.class);
    protected UserContext uc = null;
    // shared data
    Object tip;
    private GrievancesService service;
    private FacilityService facService;
    private FacilityInternalLocationService facilityInternalLocationService;
    private SupervisionService supService;
    private PersonService personService;
    private Long facilityId;
    private Long facilityInternalLocationId;
    private Long staffId;
    private Long supervisionId;
    private Long personId;
    private Long personIdentityId;
    private LegalTest legalTest = new LegalTest();

    public static CommentType createComment(String text) {
        CommentType ct = new CommentType();
        ct.setComment(text);
        ct.setCommentDate(new Date());
        return ct;
    }

    @BeforeClass
    public void beforeClass() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");
        service = (GrievancesService) JNDILookUp(this.getClass(), GrievancesService.class);
        uc = super.initUserContext();

        facService = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        facilityInternalLocationService = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        supService = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        personService = (PersonService) JNDILookUp(this.getClass(), PersonService.class);

        facilityId = createFacility();
        facilityInternalLocationId = createFacilityInternalLocation();

        personId = createPerson();
        personIdentityId = createPersonalIdenity();
        staffId = createStaff();
        supervisionId = createSupervision();

    }

    private void clientlogin(String user, String password) {
        try {
            SecurityClient client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password);
            client.login();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    @AfterClass
    public void afterClass() {
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testCreateGrievances() {

        clientlogin("devtest", "devtest");
        GrievancesType ret = new GrievancesType();
        GrievancesType type = getGrievancesTypeInstance();
        // CodeType eventType = new CodeType(ReferenceSets.EventType.value(),
        // EventType.CASENOTE.code());
        // caseNoteType.setCaseNoteNumber(service.getNextEventLogNumber(uc,
        // eventType));
        ret = service.create(uc, type);

        assert (ret != null);
        assert (ret.getGrievancesId() > 0);

		/*try {
            ret = service.create(uc, type);
			assert (ret == null);
		} catch (Exception e) {
			assert (e instanceof InvalidInputException);
		}*/
    }

    @Test
    public void testSearchGrievances() {
        clientlogin("devtest", "devtest");
        GrievancesType type = getGrievancesTypeInstance();
        GrievancesType retGR = service.create(uc, type);

        assert (retGR != null);
        assert (retGR.getGrievancesId() > 0);
        List<GrievancesType> ret = new ArrayList<GrievancesType>();

        GrievancesSearchType search = new GrievancesSearchType();

        search.setReportedBy(staffId);
        ret = service.search(uc, search, null, null, null);

        assert (ret != null);
    }

    @Test
    public void testupdateGrievance() {
        clientlogin("devtest", "devtest");
        GrievancesType ret = new GrievancesType();
        GrievancesType type = getGrievancesTypeInstance();
        ret = service.create(uc, type);
        assert (ret != null);
        ret.setOfficialResponse("Official Response updated");
        ret.setAssignedTo(null);
        GrievancesType retEdit = service.update(uc, ret, staffId);
        assert (retEdit != null);

        ret.setAssignedTo(staffId);
        retEdit = service.update(uc, ret, staffId);

        List<Long> misconductStaffs = new ArrayList<Long>();
        misconductStaffs.add(3l);

        type.setMisconductStaffIds(misconductStaffs);
        retEdit = service.update(uc, ret, staffId);

        assert (retEdit != null);
    }

    @Test
    public void testGetTransactLogs() {
        clientlogin("devtest", "devtest");

        GrievancesType ret = new GrievancesType();
        GrievancesType type = getGrievancesTypeInstance();
        ret = service.create(uc, type);
        assert (ret != null);
        List<TransactLogType> grTransactlogs = service.getTransactLogs(uc, ret.getGrievancesId(), TransactLogEntityTypes.Grievance);
        assert (grTransactlogs != null);
        assert (!grTransactlogs.isEmpty());
    }

    private GrievancesType getGrievancesTypeInstance() {
        GrievancesType type = new GrievancesType();
        // caseNoteType.setCaseNoteNumber(service.getNextEventLogNumber(uc, new
        // CodeType(Refer)));
        Date eventDate = new Date();
        Date eventTime = new Date();
        eventTime.setHours(1);
        eventTime.setMinutes(1);
        eventTime.setSeconds(1);
        type.setClaimAmount("100");
        type.setGrievanceDateTime(eventDate);
        type.setIncidentFlag(Boolean.FALSE);
        type.setIssueReason("Issue Reason");
        type.setIssueSubType(Constants.IssueSubType.MEDICAL.code());
        type.setIssueType(Constants.IssueTypes.REQUEST.code());
        type.setLocationId(facilityInternalLocationId);
        type.setFacilityId(facilityId);
        type.setSupervisionId(supervisionId);
        type.setReportedBy(staffId);
        type.setOfficialResponse("officialResponse");
        type.setInFormalResponse("ifFormalResponse");
        type.setAssignedTo(staffId);

        List<Long> misconductStaffs = new ArrayList<Long>();
        misconductStaffs.add(1l);
        misconductStaffs.add(2l);

        type.setMisconductStaffIds(misconductStaffs);

        return type;
    }

    @SuppressWarnings("unused")
    private Date createFutureDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, day);
        Date futureDate = cal.getTime();
        return futureDate;
    }

    @SuppressWarnings("unused")
    private Date createPastDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, -day);
        Date pastDate = cal.getTime();
        return pastDate;
    }

    private Long createFacility() {
        Long lng = new Random().nextLong();

        Facility ret = null;
        String category = new String("CUSTODY");

        Facility facility = new Facility(-1L, "FacilityCodeIT" + lng, "FacilityName", new Date(), category, null, null, null, null, false);
        ret = facService.create(uc, facility);
        assertNotEquals(ret, null);
        return ret.getFacilityIdentification();
    }

    private Long createFacilityInternalLocation() {
        String phyCode = "PC01";
        String description = "physical location root";
        String level = "FACILITY";
        FacilityInternalLocation pl = createFacilityInternalLocation(level, phyCode, description);
        pl.getComments().add(createComment("root comment"));

        LocationAttributeType prop = new LocationAttributeType();
        prop.getOffenderCategories().add("SUIC");
        pl.setLocationAttribute(prop);
        FacilityInternalLocation ret = facilityInternalLocationService.create(uc, pl, UsageCategory.HOU);
        assertNotNull(ret);
        log.info(String.format("ret = %s", ret));
        return ret.getId();
    }

    private FacilityInternalLocation createFacilityInternalLocation(String level, String code, String description) {
        FacilityInternalLocation pt = new FacilityInternalLocation();
        pt.setFacilityId(createFacility());
        pt.setHierarchyLevel(level);
        pt.setLocationCode(code);
        pt.setDescription(description);

        pt.getNonAssociations().add("TOTAL");
        pt.getNonAssociations().add("STG");

        return pt;
    }

    public Long createStaff() {
        StaffType ret = null;
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CPT"));
        staffPositions.add(new StaffPosition("DEP"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 30);
        String category = new String("CON");
        String reason = new String("SICK");

        StaffType staff = new StaffType(null, personIdentityId, category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(personId);
        //
        ret = personService.getStaff(uc, personService.createStaff(uc, staff));
        assert (ret != null);
        log.info(String.format("ret = %s", ret));
        return ret.getStaffID();

    }

    // Helper Method
    private Long createPersonalIdenity() {
        Long lng = new Random().nextLong();

        Date dob = new Date(1985 - 1900, 3, 10);
        PersonIdentityType peter = new PersonIdentityType(null, personId, null, null, "peter" + lng, "S" + lng, "S" + lng, "Pan" + lng, "i", dob, "M", null, null,
                "offenderNumTest" + lng, null, null);

        // PersonIdentityType peter = createIndentity("Peter", "Pan", new
        // Date(84, 3, 10), "M");
        peter = personService.createPersonIdentityType(uc, peter, Boolean.FALSE, Boolean.FALSE);
        return peter.getPersonIdentityId();
    }

    private Long createPerson() {

        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        instanceForTest.setActiveStatus("ACTIVE");    //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        instanceForTest = personService.createPersonType(uc, instanceForTest);
        return instanceForTest.getPersonIdentification();
    }

    private Long createSupervision() {

        // Create Person Identity

        Long supervisionId = createSupervision(personIdentityId, facilityId, true);
        return supervisionId;

    }

    public Long createSupervision(Long personIdentityId, Long facilityId, boolean isActive) {

        SupervisionType supervision = new SupervisionType();

        Date startDate = new Date();
        Date endDate = null;

        if (!isActive) {
            endDate = new Date();
        }

        supervision.setSupervisionDisplayID("ID" + personIdentityId);
        supervision.setSupervisionStartDate(startDate);
        supervision.setSupervisionEndDate(endDate);
        supervision.setPersonIdentityId(personIdentityId);
        supervision.setFacilityId(facilityId);

        SupervisionType ret = supService.create(uc, supervision);
        assert (ret != null);

        return ret.getSupervisionIdentification();
    }

    @Test
    public void testAppealGrievances() {
        clientlogin("devtest", "devtest");
        GrievancesType type = getGrievancesTypeInstance();
        GrievancesType retGR = service.create(uc, type);

        assert (retGR != null);
        assert (retGR.getGrievancesId() > 0);

        retGR.setStatus("RESOLVED");
        GrievancesType retResolve = service.resolve(uc, retGR, staffId);
        assert (retResolve != null);

        GrievancesType retAppeal = service.appealGrievance(uc, retGR, staffId);
        assert (retAppeal != null);
    }

}
