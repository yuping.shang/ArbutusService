package syscon.arbutus.product.services.individualassociationsearch.contract.ejb;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.person.contract.dto.personidentity.IdentifierType;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;

public class PersonIdentityBeanTest extends BaseIT {

    private PersonService service;

    /**
     * @param service
     */
    public PersonIdentityBeanTest(PersonService service) {
        super();
        this.service = service;
        uc = super.initUserContext();
    }

    public Long createPersonIdentity(Long personId) {

        PersonIdentityType personIdentity = null;
        Date dob = getDate(1985, 3, 10);
        Set<IdentifierType> identifiers = null;

        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", getDate(1980, 3, 10), getDate(1975, 3, 10), "G", "XXXX-XX", "Active"));

        personIdentity = new PersonIdentityType(null, personId, null, "MR", "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, null, null, null);

        PersonIdentityType ret = service.createPersonIdentityType(uc, personIdentity, true, false);

        return ret.getPersonIdentityId();

    }

    private Date getDate(int year, int month, int date) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, date, 0, 0, 0);
        return cal.getTime();
    }
}
