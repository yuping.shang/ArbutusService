package syscon.arbutus.product.services.individualassociationsearch.contract.ejb;

import java.util.Date;

import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

public class SupervisionBeanTest extends BaseIT {

    private SupervisionService service;

    /**
     * @param service
     */
    public SupervisionBeanTest(SupervisionService service) {
        super();
        this.service = service;
        uc = super.initUserContext();
    }

    public Long createSupervision(Long personIdentityId, Long facilityId, boolean isActive) {

        SupervisionType supervision = new SupervisionType();

        Date startDate = new Date();
        Date endDate = null;

        if (!isActive) {
			endDate = new Date();
		}

        supervision.setSupervisionDisplayID(String.valueOf(personIdentityId));
        supervision.setSupervisionStartDate(startDate);
        supervision.setSupervisionEndDate(endDate);
        supervision.setPersonIdentityId(personIdentityId);
        supervision.setFacilityId(facilityId);
        SupervisionType ret = service.create(uc, supervision);
        assert (ret != null);

        return ret.getSupervisionIdentification();

    }

}
