package syscon.arbutus.product.services.individualassociationsearch.contract.ejb;

import java.util.Date;

import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.person.contract.dto.personrelationship.PersonNonAssociation;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;

public class PersonPersonBeanTest extends BaseIT {

    protected static String PERSON = "PersonService";

    //private static String PERSON = AssociationToClass.PERSON.value();
    protected static String CELL = "CELL";
    protected static String STG = "STG";
    protected static String TOTAL = "TOTAL";

    private PersonService service;

    /**
     * @param service
     */
    public PersonPersonBeanTest(PersonService service) {
        super();
        this.service = service;
        uc = super.initUserContext();
    }

    public void createNonAssociation() {

        //init reference code etc.
        service.deleteAllPersonRelationship(uc);

        //Create data for Person1
        createNonAssociation(1L, 2L, CELL, true);
        createNonAssociation(1L, 3L, STG, true);

        createNonAssociation(1L, 12L, CELL, true);
        createNonAssociation(1L, 13L, CELL, false);
        createNonAssociation(1L, 14L, CELL, false);
        createNonAssociation(1L, 15L, STG, true);
        createNonAssociation(1L, 16L, STG, true);
        createNonAssociation(1L, 17L, STG, false);
        createNonAssociation(1L, 18L, TOTAL, true);
        createNonAssociation(1L, 19L, TOTAL, false);

    }

    public Long createNonAssociation(Long personOne, Long personTwo, String nonCode, boolean isActive) {

        Date effectiveDate = new Date();
        Date expiryDate = null;

        //Active
        if (!isActive) {
            expiryDate = new Date();
        }

        String code = nonCode;

        PersonNonAssociation personNonAssociation = new PersonNonAssociation(null, personOne, personTwo, code, "OTHER", "OTHER", null, code + " Desc", expiryDate,
                effectiveDate);

        return service.createPersonRelationship(uc, personNonAssociation);
    }

}
