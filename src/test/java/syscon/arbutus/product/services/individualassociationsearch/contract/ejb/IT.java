package syscon.arbutus.product.services.individualassociationsearch.contract.ejb;

import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.common.PersonTest;
import syscon.arbutus.product.services.common.SupervisionTest;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.individualassociationsearch.contract.dto.SupervisionNonAssociationType;
import syscon.arbutus.product.services.individualassociationsearch.contract.interfaces.IndividualAssociationSearchService;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

import static org.testng.Assert.*;

public class IT extends BaseIT {

    private static final Long SUCCESS = ReturnCode.Success.returnCode();
    private static Logger log = LoggerFactory.getLogger(IT.class);
    List<Long> pi_ids = new ArrayList<Long>();
    List<Long> sup_ids = new ArrayList<Long>();
    List<Long> pp_ids = new ArrayList<Long>();
    List<Long> personIds = new ArrayList<Long>();
    // services name
    private ActivityService activityService;
    private IndividualAssociationSearchService service;
    private PersonService personService;
    private SupervisionService sup_service;
    private PersonTest personTest = new PersonTest();
    private FacilityTest facilityTest = new FacilityTest();

    @BeforeClass
    public void beforeTest() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");
        activityService = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        service = (IndividualAssociationSearchService) JNDILookUp(this.getClass(), IndividualAssociationSearchService.class);
        personService = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        sup_service = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        uc = super.initUserContext();
        login();
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
        assertNotNull(personService);
        assertNotNull(personService);
        assertNotNull(sup_service);
        assertNotNull(uc);
    }

    @Test(dependsOnMethods = "testJNDILookup")
    public void testReset() {
        assertEquals(activityService.deleteAll(uc), SUCCESS);
        assertEquals(personService.deleteAllPersonIdentityType(uc), SUCCESS);
        personService.deleteAllPersonRelationship(uc);
        assertEquals(sup_service.deleteAll(uc), SUCCESS);
    }

    @Test(dependsOnMethods = "testReset")
    public void testCreatePersonIdentity() {
        PersonIdentityBeanTest pi_bean = new PersonIdentityBeanTest(personService);
        pi_ids.clear();
        for (int i = 1; i < 8; i++) {
            Long personId = personTest.createPerson();
            personIds.add(personId);
            pi_ids.add(pi_bean.createPersonIdentity(personId));
        }
    }

    @Test(dependsOnMethods = "testCreatePersonIdentity")
    public void testCreateSupervision() {
        SupervisionBeanTest sup_bean = new SupervisionBeanTest(sup_service);
        sup_ids.clear();
        Long facilityId = facilityTest.createFacility();
        for (int i = 1; i < 8; i++) {
            sup_ids.add(sup_bean.createSupervision(pi_ids.get(i - 1), facilityId, true));
        }

    }

    @Test(dependsOnMethods = "testCreateSupervision")
    public void testCreatePersonPerson() {
        PersonPersonBeanTest pp_bean = new PersonPersonBeanTest(personService);

        pp_ids.clear();

        pp_ids.add(pp_bean.createNonAssociation(personIds.get(0), personIds.get(1), PersonPersonBeanTest.CELL, true));
        pp_ids.add(pp_bean.createNonAssociation(personIds.get(0), personIds.get(3), PersonPersonBeanTest.TOTAL, true));

        pp_ids.add(pp_bean.createNonAssociation(personIds.get(0), personIds.get(4), PersonPersonBeanTest.CELL, false));
        pp_ids.add(pp_bean.createNonAssociation(personIds.get(0), personIds.get(6), PersonPersonBeanTest.TOTAL, false));
    }

    @AfterClass
    public void afterClass() {
        SupervisionTest supervisionTest = new SupervisionTest();
        supervisionTest.deleteAll();
        assertEquals(personService.deleteAllPersonIdentityType(uc), SUCCESS);
        personService.deleteAllPersonRelationship(uc);
        //assertEquals(sup_service.deleteAll(uc), SUCCESS);
        personTest.deleteAll();
        facilityTest.deleteAll();

    }


}
