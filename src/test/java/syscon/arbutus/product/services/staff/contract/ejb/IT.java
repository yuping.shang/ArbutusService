package syscon.arbutus.product.services.staff.contract.ejb;

import org.hibernate.exception.ConstraintViolationException;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.PersonTest;
import syscon.arbutus.product.services.core.contract.dto.AssociationType;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.SearchSetMode;
import syscon.arbutus.product.services.core.exception.ArbutusOptimisticLockException;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilitycountactivity.contract.interfaces.FacilityCountActivityService;
import syscon.arbutus.product.services.housing.contract.interfaces.HousingService;
import syscon.arbutus.product.services.location.contract.dto.*;
import syscon.arbutus.product.services.location.contract.interfaces.LocationService;
import syscon.arbutus.product.services.organization.contract.dto.Organization;
import syscon.arbutus.product.services.organization.contract.interfaces.OrganizationService;
import syscon.arbutus.product.services.person.contract.dto.StaffPosition;
import syscon.arbutus.product.services.person.contract.dto.StaffSearchType;
import syscon.arbutus.product.services.person.contract.dto.StaffType;
import syscon.arbutus.product.services.person.contract.dto.StaffsReturnType;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;
import syscon.arbutus.product.services.userpreference.contract.interfaces.UserPreferenceService;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static org.testng.Assert.*;

public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);
    Long parentId = null;
    Set<Long> locations = new HashSet<>();
    Set<Long> organizations = new HashSet<>();
    private PersonService service;
    private HousingService housingService;
    private FacilityService facservice;
    private ActivityService actService;
    protected Long personIdenttityId;
    protected Long facilityId;
    private UserPreferenceService upService;
    private OrganizationService orgService;
    private LocationService locService;
    private SupervisionService supService;
    private FacilityCountActivityService fcaService;
    protected PersonTest pTest;

    public IT() {
    }

    public IT(PersonService service, FacilityService facservice, ActivityService actService, UserPreferenceService upService, OrganizationService orgService,
            LocationService locService, SupervisionService supService, FacilityCountActivityService fcaService, PersonTest pTest, HousingService housingService) {
        this.service = service;
        this.facservice = facservice;
        this.actService = actService;
        this.upService = upService;
        this.orgService = orgService;
        this.locService = locService;
        this.supService = supService;
        this.fcaService = fcaService;
        this.pTest = pTest;
        this.housingService = housingService;
    }

    public static Long randLong(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return Long.valueOf((long) randomNum);
    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        uc = super.initUserContext();
        facservice = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        orgService = (OrganizationService) JNDILookUp(this.getClass(), OrganizationService.class);
        actService = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        locService = (LocationService) JNDILookUp(this.getClass(), LocationService.class);
        upService = (UserPreferenceService) JNDILookUp(this.getClass(), UserPreferenceService.class);
        fcaService = (FacilityCountActivityService) JNDILookUp(this.getClass(), FacilityCountActivityService.class);
        supService = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);

        pTest = new PersonTest();

/*		actService.deleteAll(uc);
        supService.deleteAll(uc);
		upService.deleteAll(uc);
	    locService.deleteAll(uc);
	    fcaService.deleteAll(uc);
		facservice.deleteAll(uc);
		orgService.deleteAll(uc);
        pService.deleteAll(uc);
		piService.deleteAll(uc);
		service.deleteAll(uc);*/

        PersonIdentityType pi = createPersonalIdenity();
        personIdenttityId = pi.getPersonIdentityId();
        facilityId = createFacility();

        parentId = createFacilty();
        locations.add(createLocation());
        organizations.add(createOrganization());
    }

    @Test(dataProvider = "dp")
    public void f(Integer n, String s) {
    }

    @BeforeMethod
    public void beforeMethod() {
        SecurityClient client;
        try {
            client = SecurityClientFactory.getSecurityClient();
            client.setSimple("devtest", "devtest"); // nice regular service user
            client.login();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @DataProvider
    public Object[][] dp() {
        return new Object[][] { new Object[] { 1, "a" }, new Object[] { 2, "b" }, };
    }

    @Test
    public void getCount() {

        //
        StaffType ret = null;

        //
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CPT"));
        staffPositions.add(new StaffPosition("DEP"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 30);
        String category = new String("CON");
        String reason = new String("SICK");

        PersonIdentityType pi = createPersonalIdenity();

        StaffType staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(pi.getPersonId());

        //
        ret = service.getStaff(uc, service.createStaff(uc, staff));
        assert (ret != null);

        Long cou = service.getStaffCount(null);

        assert (cou >= 0L);
    }

    // Helper Method
    protected PersonIdentityType createPersonalIdenity() {

        Date dob = new Date(1985 - 1900, 3, 10);
        PersonIdentityType peter = new PersonIdentityType(null, pTest.createPerson(), null, null, "peter" + randLong(1, 1000000), "S", "S", "Pan" + randLong(1, 1000000),
                "i", dob, "M", null, null, "offenderNumTest", null, null);

        // PersonIdentityType peter = createIndentity("Peter", "Pan", new
        // Date(84, 3, 10), "M");
        peter = service.createPersonIdentityType(uc, peter, Boolean.FALSE, Boolean.FALSE);
        personIdenttityId = peter.getPersonIdentityId();

        return peter;
    }

    protected Long createFacility() {
        //facservice.deleteAll(uc);
        Long lng = 1L;

        Facility ret = null;
        String category = new String("COMMU");

        Facility facility = new Facility(-1L, "FacilityCodeIT" + randLong(1, 1000000), "FacilityName", new Date(), category, null, null, null, null, false);
        ret = facservice.create(uc, facility);
        assertNotEquals(ret, null);
        return ret.getFacilityIdentification();
    }

    // Helper Method
    private void createSearchStaff(int count, Set<StaffPosition> staffPositions, String category, String reason, Date startDate, Date endDate) {
        for (int i = 0; i < count; i++) {
            StaffType ret = null;
            //
            PersonIdentityType pi = createPersonalIdenity();
            StaffType staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, null);
            staff.setPersonId(pi.getPersonId());

            //
            ret = service.getStaff(uc, service.createStaff(uc, staff));
            assert (ret != null);
        }
    }

    @SuppressWarnings("deprecation")
    @Test
    public void create() {
        //
        StaffType ret = null;

        //
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CPT"));
        staffPositions.add(new StaffPosition("DEP"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 30);
        String category = new String("CON");
        String reason = new String("SICK");

        PersonIdentityType pi = createPersonalIdenity();

        StaffType staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(pi.getPersonId());
        //
        ret = service.getStaff(uc, service.createStaff(uc, staff));
        assert (ret != null);

    }

    @SuppressWarnings("deprecation")
    @Test
    public void metaCode() {
        //
        StaffType ret = null;

        //
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CPT"));
        staffPositions.add(new StaffPosition("DEP"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 30);
        String category = new String("CON");
        String reason = new String("TRM");

        PersonIdentityType pi = createPersonalIdenity();

        StaffType staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(pi.getPersonId());

        //
        ret = service.getStaff(uc, service.createStaff(uc, staff));
        assert (ret != null);

    }

    @SuppressWarnings("deprecation")
    @Test
    public void metaCodePosition() {
        //
        StaffType ret = null;

        //
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("DEP"));
        staffPositions.add(new StaffPosition("DEP0-0"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 30);
        String category = new String("CON");
        String reason = new String("SICK");

        PersonIdentityType pi = createPersonalIdenity();

        StaffType staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(pi.getPersonId());

        try {
            ret = service.getStaff(uc, service.createStaff(uc, staff));
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(DataNotExistException.class));
        }

    }

    @SuppressWarnings("deprecation")
    @Test
    public void facilities() {
        housingService.deleteAll(uc);
        service.deleteAllStaff(uc);

        StaffType ret = null;

        Set<Long> fac = new HashSet<>();
        fac.add(createFacility());

        //
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("cgy"));
        staffPositions.add(new StaffPosition("md"));
        staffPositions.add(new StaffPosition("SHR"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 30);
        String category = new String("vol");
        String reason = new String("TRM");

        PersonIdentityType pi = createPersonalIdenity();
        StaffType staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, fac);
        staff.setPersonId(pi.getPersonId());

        //
        ret = service.getStaff(uc, service.createStaff(uc, staff));
        assert (ret != null);

        fac = new HashSet<>();
        fac.add(8348L);
        staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, fac);
        //
        try {
            ret = service.getStaff(uc, service.createStaff(uc, staff));
        } catch (Exception ex) {
        }

        assert (ret != null);

        ret = null;

        fac = new HashSet<>();
        fac.add(createFacility());

        pi = createPersonalIdenity();
        //
        staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("cgy"));
        staffPositions.add(new StaffPosition("md"));
        staffPositions.add(new StaffPosition("SHR"));
        startDate = new Date(2012 - 1900, 2, 1); // March 1st
        endDate = new Date(2912 - 1900, 11, 30);
        category = new String("vol");
        reason = new String("TRM");

        staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, fac);
        staff.setPersonId(pi.getPersonId());

        //
        ret = service.getStaff(uc, service.createStaff(uc, staff));
        assert (ret != null);

    }

    @SuppressWarnings("deprecation")
    @Test
    public void searchMode() {

        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 30);

        // Create 10 Staff of Vol
        StaffSearchType search = new StaffSearchType();
        search.setSetMode(SearchSetMode.ANY);

        Set<StaffPosition> staffPositions = new HashSet<>();
        String category = "VOL";

        createSearchStaff(10, staffPositions, category, null, startDate, endDate);

        // Get all Staff positions
        search.setStaffPosition(staffPositions.stream().map(StaffPosition::getStaffPosition).collect(Collectors.toSet()));
        search.setStaffCategory(category);

        Set<Long> subIds = new HashSet<>();

        StaffsReturnType sret = service.searchStaff(uc, subIds, search, 0L, 10L, null);
        assert (sret.getStaff().size() == 10);
        // Test total size
        assert (sret.getTotalSize() >= 10);

        //Create 5 Staff of MED and RN & CPT
        search = new StaffSearchType();
        staffPositions.add(new StaffPosition("RN"));
        staffPositions.add(new StaffPosition("CPT"));
        category = "MED";
        search.setStaffCategory(category);
        search.setStaffPosition(staffPositions.stream().map(StaffPosition::getStaffPosition).collect(Collectors.toSet()));
        search.setSetMode(SearchSetMode.ANY);

        createSearchStaff(5, staffPositions, category, null, startDate, endDate);

        // Get ANY of RN , CPT
        search.setStaffPosition(staffPositions.stream().map(StaffPosition::getStaffPosition).collect(Collectors.toSet()));
        sret = service.searchStaff(uc, subIds, search, 0L, 5L, null);

        assert (sret.getStaff().size() == 5);

        //Create 10 Staff of MD
        search = new StaffSearchType();
        search.setStaffCategory(category);
        staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("MD"));
        createSearchStaff(10, staffPositions, category, null, startDate, endDate);

        // Get any of  RN , CPT , MD
        // 10 of MD, 5 of RN/CPT, getting 7 of those
        search.setSetMode(SearchSetMode.ANY);
        sret = service.searchStaff(uc, subIds, search, 0L, 7L, null);
        assert (sret.getStaff().size() == 7);

        //Get ALL of RN, CPT, MD
        // 10 of MD, 5 of RN/CPT , ALL = 15
        search.setSetMode(SearchSetMode.ALL);
        sret = service.searchStaff(uc, subIds, search, 0L, 15L, null);
        assert (sret.getStaff().size() == 15);

    }

    @SuppressWarnings("deprecation")
    @Test
    public void createNegative() {

        housingService.deleteAll(uc);
        service.deleteAllStaff(uc);

        StaffType ret = null;

        //
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CPT"));
        staffPositions.add(new StaffPosition("DEP"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 30);
        String category = new String("CON");
        String reason = new String("SICK");

        PersonIdentityType pi = createPersonalIdenity();
        Set<Long> fac = new HashSet<>();
        fac.add(createFacility());

        StaffType staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, fac);
        staff.setPersonId(pi.getPersonId());

        // Invalid category. this will fail
        staff.setStaffCategory("NONE ");
        try {
            service.createStaff(uc, staff);
        } catch (Exception ex) {
            assert (ex.getClass().equals(DataNotExistException.class));
        }
        staff.setStaffCategory(category);

        // Non-exist category. this will fail
        staff.setStaffCategory("NONE_");
        try {
            service.createStaff(uc, staff);
        } catch (Exception ex) {
            assert (ex.getClass().equals(DataNotExistException.class));
        }
        staff.setStaffCategory(category);

        // Invalid position. This will fail
        Set<StaffPosition> nonstaffPositions = new HashSet<>();
        nonstaffPositions.add(new StaffPosition("NONE "));
        staff.setStaffPosition(nonstaffPositions);
        try {
            service.createStaff(uc, staff);
        } catch (Exception ex) {
            log.info(String.format("***********ex %s", ex));
            assert (ex.getClass().equals(DataNotExistException.class));
        }
        staff.setStaffPosition(staffPositions);
        assert (ret == null);

        // None exist position. This will fail
        nonstaffPositions = new HashSet<>();
        nonstaffPositions.add(new StaffPosition("NONE_"));
        staff.setStaffPosition(nonstaffPositions);
        try {
            service.createStaff(uc, staff);
        } catch (Exception ex) {
            assertEquals(ex.getClass(), DataNotExistException.class);
        }
        staff.setStaffPosition(staffPositions);
        assert (ret == null);

        // Invalid reason. this will fail
        staff.setStaffDeactivationReason("NONE ");
        try {
            service.createStaff(uc, staff);
        } catch (Exception ex) {
            log.info(String.format("***********ex %s", ex));
            assert (ex.getClass().equals(DataNotExistException.class));
        }
        staff.setStaffDeactivationReason(reason);

        // Non-exist reason. this will fail
        staff.setStaffDeactivationReason("NONE_");
        try {
            service.createStaff(uc, staff);
        } catch (Exception ex) {
            assert (ex.getClass().equals(DataNotExistException.class));
        }
        staff.setStaffDeactivationReason(reason);

        // No person. this will fail
        staff.setPersonIdentityID(5555L);
        try {
            service.createStaff(uc, staff);
        } catch (Exception ex) {
            assert (ex.getClass().equals(DataNotExistException.class));
        }
        staff.setPersonIdentityID(pi.getPersonIdentityId());

        // Invlaid facility. this will fail
        fac = new HashSet<>();
        fac.add(723L);
        staff.setFacilities(fac);
        try {
            service.createStaff(uc, staff);
        } catch (Exception ex) {
            assert (ex.getClass().equals(DataNotExistException.class));
        }
        fac = new HashSet<>();
        fac.add(createFacility());
        staff.setFacilities(fac);

        // No category
        staff.setStaffCategory(null);
        try {
            service.createStaff(uc, staff);
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        staff.setStaffCategory(category);

        // No start date
        staff.setStaffStartDate(null);
        try {
            service.createStaff(uc, staff);
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        staff.setStaffStartDate(startDate);

        // This will be OK
        //
        try {
            ret = service.getStaff(uc, service.createStaff(uc, staff));
        } catch (Exception ex) {
        }

        assert (ret != null);
        assert (ret.getStaffCategory().equals("CON"));
        assert (ret.getStaffDeactivationReason().endsWith("SICK"));
        assert (startDate.equals(ret.getStaffStartDate()));
        assert (endDate.equals(ret.getStaffEndDate()));
        assert (ret.getStaffPosition().size() == 2);

        //
        Long staffId = ret.getStaffID();
        ret = service.getStaff(uc, staffId);
        assert (ret != null);
        assert (ret.getStaffCategory().equals(category));
        assert (ret.getStaffDeactivationReason().equals(reason));
        assert (startDate.equals(ret.getStaffStartDate()));
        assert (endDate.equals(ret.getStaffEndDate()));
        assert (ret.getStaffPosition().size() == 2);
        assert (ret.isIsActiveFlag().equals(Boolean.TRUE));
        // Same person. this will fail
        try {
            service.createStaff(uc, staff);
        } catch (Exception ex) {
            assertEquals(ex.getClass(), ConstraintViolationException.class);
        }

    }

    @SuppressWarnings("deprecation")
    @Test
    public void update() {
        housingService.deleteAll(uc);
        service.deleteAllStaff(uc);

        Long pid = personIdenttityId;

        //
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("cgy"));
        staffPositions.add(new StaffPosition("md"));
        staffPositions.add(new StaffPosition("SHR"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 30);
        String category = new String("vol");
        String reason = new String("TRM");

        PersonIdentityType pi = createPersonalIdenity();

        StaffType staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(pi.getPersonId());

        //
        staff = service.getStaff(uc, service.createStaff(uc, staff));
        assert (staff != null);
        Long staffId = staff.getStaffID();

        //
        category = new String("CON");
        staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("PA"));
        staffPositions.add(new StaffPosition("RN"));
        reason = new String("SICK");
        startDate = new Date(2012 - 1900, 0, 1); // March 1st
        endDate = new Date(2035 - 1900, 11, 31);
        staff.setStaffID(staffId);
        staff.setStaffCategory(category);
        staff.setStaffPosition(staffPositions);
        staff.setStaffDeactivationReason(reason);
        staff.setStaffStartDate(startDate);
        staff.setStaffEndDate(endDate);
        staff.setPersonIdentityID(staff.getPersonIdentityID());

        service.updateStaff(uc, staff);
        assert (staff != null);

        Set<Long> fac = new HashSet<>();
        fac.add(createFacility());

        staff.setStaffID(staffId);
        staff.setFacilities(fac);

        try {
            service.updateStaff(uc, staff);
            fail("Expecting: Exception ArbutusOptimisticLockException  ");
        } catch (Exception ex) {
            assertEquals(ex.getClass(), ArbutusOptimisticLockException.class);
        }

        staff = service.getStaff(uc, staffId);

        staff.setStaffID(staffId);
        staff.setFacilities(fac);

        service.updateStaff(uc, staff);
        assert (staff != null);

        staff = service.getStaff(uc, staffId);
        staff.setPersonIdentityID(staff.getPersonIdentityID());
        service.updateStaff(uc, staff);
        assert (staff != null);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void deactivate() {
        //
        //service.deleteAll(uc);

        //
        StaffType ret = null;

        //
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CPT"));
        staffPositions.add(new StaffPosition("DEP"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2019 - 1900, 11, 30);
        String category = new String("CON");
        String reason = new String("SICK");

        PersonIdentityType pi = createPersonalIdenity();

        StaffType staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(pi.getPersonId());

        //
        ret = service.getStaff(uc, service.createStaff(uc, staff));
        assert (ret != null);

        assert (ret != null);
        Long staffId = ret.getStaffID();

        // No start date
        try {
            // Delete a none-existing staff
            service.deactivateStaff(uc, staffId, "", null, endDate, ret.getVersion());
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }

        try {
            // End date is earlier than start date
            ret = service.deactivateStaff(uc, staffId, "", startDate, new Date(2012 - 1900, 0, 1), ret.getVersion());
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }

        try {
            // Wrong reason
            ret = service.deactivateStaff(uc, staffId, "NONE_", startDate, endDate, ret.getVersion());
            // assert(ret.getReturnCode().equals(ReturnCode.RInvalidData1007.returnCode()));
        } catch (Exception ex) {
            assert (ex.getClass().equals(DataNotExistException.class));
        }

        try {
            // Invalid reason
            ret = service.deactivateStaff(uc, staffId, "NONE ", startDate, endDate, ret.getVersion());
        } catch (Exception ex) {
            assert (ex.getClass().equals(DataNotExistException.class));
        }

        //
        Date startDate2 = new Date(2012 - 1900, 2, 2); // March 2nd
        Date endDate2 = new Date(2012 - 1900, 11, 30);// Dec 30
        StaffType ret2 = service.deactivateStaff(uc, staffId, "TRM", startDate2, endDate2, ret.getVersion());

        ret = service.getStaff(uc, staffId);
        assert (ret != null);

        assert (startDate2.equals(ret.getStaffStartDate()));
        assert (endDate2.equals(ret.getStaffEndDate()));
        assert (ret.getStaffDeactivationReason().equals("TRM"));

    }

    @SuppressWarnings("deprecation")
    @Test
    public void activate() {

        //
        //service.deleteAll(uc);

        //
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("RN"));
        staffPositions.add(new StaffPosition("CPT"));

        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 29);
        String category = "CON";
        String reason = "SICK";

        PersonIdentityType pi = createPersonalIdenity();
        StaffType staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(pi.getPersonId());

        // Create a staff instance
        StaffType ret = service.getStaff(uc, service.createStaff(uc, staff));
        assert (ret != null);
        Long staffId = ret.getStaffID();

        // No start date
        try {
            // Delete a none-existing staff
            service.activateStaff(uc, staffId, null, endDate, ret.getVersion());
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }

        try {
            // End date is earlier than start date
            ret = service.activateStaff(uc, staffId, startDate, new Date(2012 - 1900, 0, 1), ret.getVersion());
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }

        //
        Date startDate2 = new Date(2012 - 1900, 2, 2); // March 1st
        Date endDate2 = new Date(2912 - 1900, 11, 30);// Dec 30
        ret = service.activateStaff(uc, staffId, startDate2, endDate2, ret.getVersion());

        assert (ret != null);

        ret = service.getStaff(uc, staffId);
        assert (ret != null);
        assert (startDate2.getYear() == (ret.getStaffStartDate().getYear()));
        assert (endDate2.getYear() == ret.getStaffEndDate().getYear());
        assert (ret.getStaffDeactivationReason() == null);

    }

    @SuppressWarnings("deprecation")
    @Test
    public void search() {
        housingService.deleteAll(uc);
        service.deleteAllStaff(uc);

        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("RN"));
        staffPositions.add(new StaffPosition("CPT"));
        String category = "CON";
        String reason = "SICK";
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 29);

        createSearchStaff(50, staffPositions, category, reason, startDate, endDate);

        startDate = new Date(2012 - 1900, 2, 2); // March 1st
        endDate = null;
        staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CGY"));
        staffPositions.add(new StaffPosition("DEP"));
        category = "VOL";
        reason = "SUS";
        createSearchStaff(10, staffPositions, category, reason, startDate, endDate);

        startDate = new Date(2012 - 1900, 2, 1); // March 1st
        endDate = new Date(2512 - 1900, 2, 1);
        staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CGY"));
        staffPositions.add(new StaffPosition("DEP"));
        category = "VOL";
        reason = "SUS";
        createSearchStaff(10, staffPositions, category, reason, startDate, endDate);

        startDate = new Date(2012 - 1900, 2, 1); // March 1st
        endDate = new Date(2012 - 1900, 2, 30);
        staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CGY"));
        staffPositions.add(new StaffPosition("DEP"));
        category = "VOL";
        reason = "SUS";
        createSearchStaff(10, staffPositions, category, reason, startDate, endDate);

        //
        StaffSearchType staffSearch = new StaffSearchType("VOL", null, SearchSetMode.ANY, null, null, null, null, null, null);

        StaffsReturnType sret = service.searchStaff(uc, null, staffSearch, 0L, 30L, null);
        assert (sret != null);
        assert (sret.getStaff().size() == 30);

        staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CGY"));
        staffPositions.add(new StaffPosition("DEP"));

        staffSearch = new StaffSearchType(null, staffPositions.stream().map(StaffPosition::getStaffPosition).collect(Collectors.toSet()), SearchSetMode.ANY, null, null,
                null, null, null, null);
        sret = service.searchStaff(uc, null, staffSearch, 0L, 30L, null);
        assert (sret != null);
        assertEquals(sret.getStaff().size(), 30);

        staffSearch = new StaffSearchType(null, null, SearchSetMode.ANY, null, "SUS", null, null, null, null);
        sret = service.searchStaff(uc, null, staffSearch, 0L, 30L, null);
        assert (sret != null);
        assertEquals(sret.getStaff().size(), 30);

        staffSearch = new StaffSearchType(null, null, SearchSetMode.ANY, Boolean.TRUE, null, null, null, null, null);
        sret = service.searchStaff(uc, null, staffSearch, 0L, 50L, null);
        assert (sret != null);
        assertEquals(sret.getStaff().size(), 50);

        staffSearch = new StaffSearchType(null, null, null, Boolean.FALSE, null, null, null, null, null);
        sret = service.searchStaff(uc, null, staffSearch, 0L, 15L, null);
        assert (sret != null);
        assertEquals(sret.getStaff().size(), 10);

        staffSearch = new StaffSearchType(null, null, null, null, null, new Date(2012 - 1900, 2, 1), new Date(2012 - 1900, 2, 1), null, null);
        sret = service.searchStaff(uc, null, staffSearch, 0L, 100L, null);
        assert (sret != null);
        assertEquals(sret.getStaff().size(), 70);

        staffSearch = new StaffSearchType(null, null, null, null, null, new Date(2012 - 1900, 2, 1), new Date(2012 - 1900, 2, 2), null, null);
        sret = service.searchStaff(uc, null, staffSearch, 0L, 100L, null);
        assert (sret != null);
        assertEquals(sret.getStaff().size(), 80);

        staffSearch = new StaffSearchType(null, null, null, null, null, null, null, new Date(2512 - 1900, 2, 1), new Date(2512 - 1900, 2, 1));
        sret = service.searchStaff(uc, null, staffSearch, 0L, 15L, null);
        assert (sret != null);
        assertEquals(sret.getStaff().size(), 10);

        staffSearch = new StaffSearchType(null, null, null, null, null, null, null, new Date(2012 - 1900, 2, 30), new Date(2512 - 1900, 2, 1));
        sret = service.searchStaff(uc, null, staffSearch, 0L, 100L, null);
        assert (sret != null);
        assertEquals(sret.getStaff().size(), 20);

        staffSearch = new StaffSearchType(null, null, null, Boolean.FALSE, null, null, null, new Date(2012 - 1900, 2, 30), new Date(2512 - 1900, 2, 1));
        sret = service.searchStaff(uc, null, staffSearch, 0L, 100L, null);
        assert (sret != null);
        assertEquals(sret.getStaff().size(), 10);

        staffSearch = new StaffSearchType(null, null, null, Boolean.TRUE, null, null, null, new Date(2012 - 1900, 2, 30), new Date(2512 - 1900, 2, 1));
        sret = service.searchStaff(uc, null, staffSearch, 0L, 15L, null);
        assert (sret != null);
        assertEquals(sret.getStaff().size(), 10);

        startDate = new Date(2012 - 1900, 2, 2); // March 1st
        endDate = null;
        staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CGY"));
        staffPositions.add(new StaffPosition("DEP"));
        category = "VOL";
        reason = "SUS";
        createSearchStaff(10, staffPositions, category, reason, startDate, endDate);

        //
        Set<Long> subSet = new HashSet<>();
        subSet.add(-1L);
        staffSearch = new StaffSearchType(null, staffPositions.stream().map(StaffPosition::getStaffPosition).collect(Collectors.toSet()), SearchSetMode.ALL, null, null,
                null, null, null, null);
        try {
            sret = service.searchStaff(uc, subSet, staffSearch, 0L, 15L, null);
        } catch (Exception ex) {
        }

        assert (sret != null);
        assert (sret.getStaff().size() == 0);

    }

    @SuppressWarnings("deprecation")
    @Test
    public void delete() {

        housingService.deleteAll(uc);
        service.deleteAllStaff(uc);

        //
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("RN"));
        staffPositions.add(new StaffPosition("CPT"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 29);
        String category = "CON";
        String reason = "SICK";

        PersonIdentityType pi = createPersonalIdenity();

        StaffType staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(pi.getPersonId());

        // Create a staff instance
        StaffType ret = service.getStaff(uc, service.createStaff(uc, staff));
        assert (ret != null);
        Long staffId = ret.getStaffID();

        service.deleteStaff(null, staffId, ret.getVersion());

        try {
            // Delete a none-existing staff
            service.deleteStaff(null, 0L, 0l);
            fail("Should not be able to delete!");
        } catch (Exception ex) {
            assertEquals(ex.getClass(), ArbutusOptimisticLockException.class);
        }

    }

    @SuppressWarnings("deprecation")
    @Test
    public void getAll() {
        //
        //service.deleteAll(uc);
        Set<Long> aret = service.getAllStaff(null);
        assert (aret != null);
        int size1 = aret.size();

        //
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("RN"));
        staffPositions.add(new StaffPosition("CPT"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 29);
        String category = "CON";
        String reason = "SICK";

        PersonIdentityType pi = createPersonalIdenity();

        StaffType staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(pi.getPersonId());

        // Create a staff instance
        StaffType sret = service.getStaff(uc, service.createStaff(uc, staff));
        assert (sret != null);

        //
        aret = service.getAllStaff(null);
        assert (aret != null);
        assert (aret.size() == size1 + 1);

    }

    @SuppressWarnings("deprecation")
    @Test
    public void get() {
        //
        //service.deleteAll(uc);
        //
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("RN"));
        staffPositions.add(new StaffPosition("CPT"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 29);
        String category = "CON";
        String reason = "SICK";

        PersonIdentityType pi = createPersonalIdenity();

        StaffType staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(pi.getPersonId());

        // Create a staff instance
        staff = service.getStaff(uc, service.createStaff(uc, staff));
        assert (staff != null);

        //
        StaffType aret = service.getStaff(null, staff.getStaffID());
        assert (aret != null);

        try {
            aret = service.getStaff(null, -staff.getStaffID());
        } catch (Exception ex) {
            assert (ex.getClass().equals(DataNotExistException.class));
        }

    }

    @SuppressWarnings("deprecation")
    @Test
    public void getNiem() {
        //
        //service.deleteAll(uc);
        //
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("RN"));
        staffPositions.add(new StaffPosition("CPT"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 29);
        String category = "CON";
        String reason = "SICK";

        PersonIdentityType pi = createPersonalIdenity();

        StaffType staff = new StaffType(null, pi.getPersonIdentityId(), category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(pi.getPersonId());

        // Create a staff instance
        staff = service.getStaff(uc, service.createStaff(uc, staff));
        assert (staff != null);

        //
        String niem = service.getStaffNiem(null, staff.getStaffID());
        assert (niem != null);

    }

    protected Long createOrganization() {

        Organization org = new Organization();
        Organization ret;
        Organization orgRet = null;

        org.setOrganizationName("New York Department of Correction");
        org.setOrganizationLocalId(null);
        org.setOrganizationAbbreviation("NYDOC");
        org.setOrganizationORIIdentification(null);
        ret = orgService.create(uc, org);
        assertNotNull(ret);
        assertNotNull(ret.getOrganizationIdentification());
        assertEquals(ret.getOrganizationName(), org.getOrganizationName());
        assertEquals(ret.getOrganizationAbbreviation(), org.getOrganizationAbbreviation());

        org.setOrganizationName("Los Angeles Department of Correction");
        org.setOrganizationLocalId("LA456-05-356");
        org.setOrganizationAbbreviation("LADOC");
        org.setOrganizationORIIdentification("LA001" + randLong(1000, 9999));
        Set<String> categories = new HashSet<>();
        categories.add("NONCJ");
        categories.add("CJ");
        categories.add("CORP");
        org.setOrganizationCategories(categories);
        ret = orgService.create(uc, org);
        assertNotNull(ret);
        assertNotNull(ret.getOrganizationIdentification());
        assertEquals(ret.getOrganizationName(), org.getOrganizationName());
        assertEquals(ret.getOrganizationAbbreviation(), org.getOrganizationAbbreviation());
        assertEquals(ret.getOrganizationORIIdentification(), org.getOrganizationORIIdentification());
        assertEquals(ret.getOrganizationCategories(), org.getOrganizationCategories());

        org = new Organization();

        Set<String> privileges = new HashSet<>();
        privileges.add("S");
        privileges.add("HP");
        Set<CodeType> categoriesType = new HashSet<>();
        categoriesType.add(new CodeType("ORGANIZATIONCATEGORY", "CJ"));
        // categoriesType.add(new CodeType("ORGANIZATIONCATEGORY", "NONCJ"));
        categoriesType.add(new CodeType("ORGANIZATIONCATEGORY", "CORP"));
        categoriesType.add(new CodeType("ORGANIZATIONCATEGORY", "PROGRAM"));
        categoriesType.add(new CodeType("ORGANIZATIONCATEGORY", "GOVT"));
        Set<AssociationType> associationsType = new HashSet<>();
        associationsType.add(new AssociationType("FacilityService", 1L));
        associationsType.add(new AssociationType("FacilityService", 2L));
        associationsType.add(new AssociationType("FacilityService", 333L));

        Set<String> userPrivileges = new HashSet<>();
        userPrivileges.add("HP");
        userPrivileges.add("S");
        userPrivileges.add("RED");

        org.setOrganizationIdentification(111L);
        org.setOrganizationAbbreviation("Richmond Govt");
        org.setOrganizationLocalId("6500 River Road, Richomnd, BC");
        org.setOrganizationName("Richmond Court");
        org.setOrganizationORIIdentification("RCMP12345");
        org.setOrganizationORIIdentification("RCMP1" + randLong(1000, 9999));

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Date nextYear = cal.getTime();
        org.setOrganizationTerminationDate(nextYear);

        org.setOrganizationParent(null);

        log.debug(org.toString());

        ret = orgService.create(uc, org);

        orgRet = ret;
        assertNotNull(orgRet);

        org.setOrganizationIdentification(orgRet.getOrganizationIdentification());

        Long orgId = orgRet.getOrganizationIdentification();
        organizations.add(orgId);
        return orgId;
    }

    protected Long createLocation() {
        Location lrt = null;
        Long locId = null;

        try {
            Location loc1 = createLocation("1", false, 1l);

            loc1.setTypeOfLocationType(null);

            loc1.setTypeOfLocationType(new HashSet<>());

            lrt = locService.create(uc, createLocation("1", false, 1l));
            locService.create(uc, createLocation("2", false, 1l));
            locService.create(uc, createLocation("3", false, 1l));
            locService.create(uc, createLocation("4", false, 2l));

            Location in = createLocation("5", false, 1l);
            lrt = locService.create(uc, in);

            Location ret = lrt;

            assertNotNull(ret);
            assertEquals("ADI-B", ret.getLocationTwoDimentionalGeographicalCoordinate().getGeographicDatumCode());

            BigDecimal bd = new BigDecimal(6.89788);
            bd = bd.setScale(6, BigDecimal.ROUND_HALF_UP);
            assertEquals(0, (bd).compareTo(ret.getLocationTwoDimentionalGeographicalCoordinate().getLongtitudeSecondValue()));

            locId = lrt.getLocationIdentification();
            locations.add(locId);
        } catch (Exception re) {
            re.printStackTrace();
        }

        return locId;
    }

    private Location createLocation(String name, boolean primary, Long refId) throws Exception {
        Location l = new Location();

        l.setLocationName("Location Name " + name);
        l.setLocationDescriptionText("The locationDescriptionText");
        l.setLocationCrossStreet("the location cross street");
        l.setLocationActiveFlag(null);
        l.setLocationStartDate(new Date());
        if (name != null) {
            Calendar cal = Calendar.getInstance();
            cal.set(2035, Integer.parseInt(name), 28);
            l.setLocationEndDate(cal.getTime());
        }
        l.setLocationPrimaryFlag(primary);

        Set<String> typeSet = new HashSet<>();
        typeSet.add(new String("MAILING"));
        l.setTypeOfLocationType(typeSet);

        //
        Set<String> lcs = new HashSet<>();
        lcs.add(new String("0" + name));
        // lcs.add(new String("1" + name));
        // lcs.add(new String("18"));

        l.setLocationCategory(lcs);

        // set LocationAddressType
        LocationAddressType la = new LocationAddressType();
        // la.setInternationalAddressLine("The internationalAddressLine"); //
        // must be set alone only
        la.setLocationCountryName(new String("CAN"));
        la.setAddressBuildingText("the addressBuildingText");
        la.setLocationStreetNumSuffix("The locationStreetNumSuffix");
        la.setLocationStreetNumberText("12");
        la.setLocationStreetName("The locationStreetName");
        la.setLocationStreetCategory(new String("AVE"));
        la.setLocationStreetPredirectionalText(new String("E"));
        la.setLocationStreetPostdirectionalText(new String("E"));
        la.setLocationUnitNumber("13");
        la.setLocationFloorNum(12l);
        la.setLocationCityName("vancouver " + name);
        la.setLocationStateCode(new String("AB"));
        la.setLocationPostalCode("V" + name + "X 1S2");
        la.setLocationPostalExtensionCode("12-1");
        la.setAddressDeliveryMode(new String("MS"));
        la.setAddressDeliveryModeNumber(12l);
        la.setLocationCountyName(new String("037-06"));
        la.setLocationLocalityName("the locality name");

        l.setLocationAddress(la);

        //
        Set<LocationPhoneFaxType> lpfns = new HashSet<>();

        LocationPhoneFaxType lpft = new LocationPhoneFaxType();
        lpft.setLocationPhoneFaxNumberCategory(LocationPhoneFaxType.HOME_PHONE);
        lpft.setLocationPhoneFaxNumber("the locationPhoneFaxNumber " + name);
        lpft.setLocationPhoneExtension("the locationPhoneExtension");

        lpfns.add(lpft);

        l.setLocationPhoneFaxNumber(lpfns);

        //
        Set<String> les = new HashSet<>();
        les.add("cw@abm.com");
        les.add("email2@hotm.com");

        l.setLocationEmail(les);

        // locationLocale;
        Set<LocationLocaleType> lls = new HashSet<>();

        LocationLocaleType llt = new LocationLocaleType();
        llt.setLocaleCommunityName("the localeCommunityName ");
        llt.setLocaleDescriptionText("the localeDescriptionText ");
        llt.setLocaleDistrictName("the localeDistrictName");
        llt.setLocaleEmergencyServiceCityName(" the localeEmergencyServiceCityName");
        llt.setLocaleFireJurisdictionID("the localeFireJurisdictionID");
        llt.setLocaleJudicialDistrictName(" the localeJudicialDistrictName");
        llt.setLocaleJudicialDistrictCode("the localeJudicialDistrictCode");
        llt.setLocaleNeighborhoodName("the localeNeighborhoodName");
        llt.setLocalePoliceBeatText("the localePoliceBeatText");
        llt.setLocalePoliceGridText("the localePoliceGridText");
        llt.setLocalePoliceJurisdictionID("the localePoliceJurisdictionID");
        llt.setLocaleRegionName("the localeRegionName " + name);
        llt.setLocaleSubdivisionName("the localeSubdivisionName");
        llt.setLocaleZoneName("the localeZoneName");
        llt.setLocaleEmergencyServiceJurisdictionID("the localeEmergencyServiceJurisdictionID");

        lls.add(llt);

        LocationLocaleType llt2 = new LocationLocaleType();
        llt2.setLocaleCommunityName("the localeCommunityName ");
        llt2.setLocaleDescriptionText("the localeDescriptionText ");
        llt2.setLocaleDistrictName("the localeDistrictName");
        llt2.setLocaleEmergencyServiceCityName(" the localeEmergencyServiceCityName");
        llt2.setLocaleFireJurisdictionID("the localeFireJurisdictionID");
        llt2.setLocaleJudicialDistrictName(" the localeJudicialDistrictName");
        llt2.setLocaleJudicialDistrictCode("the localeJudicialDistrictCode");
        llt2.setLocaleNeighborhoodName("the localeNeighborhoodName");
        llt2.setLocalePoliceBeatText("the localePoliceBeatText");
        llt2.setLocalePoliceGridText("the localePoliceGridText");
        llt2.setLocalePoliceJurisdictionID("the localePoliceJurisdictionID");
        llt2.setLocaleRegionName("the localeRegionName " + name + name);
        llt2.setLocaleSubdivisionName("the localeSubdivisionName");
        llt2.setLocaleZoneName("the localeZoneName");
        llt2.setLocaleEmergencyServiceJurisdictionID("the localeEmergencyServiceJurisdictionID");

        lls.add(llt2);

        l.setLocationLocale(lls);

        // locationReferences 1-to-1
        l.setAddressCategory(AddressCategory.FACILITY);
        l.setInstanceId(refId);

        // Set<String> locationSurroundingAreaDescriptionText;
        Set<String> lsadts = new HashSet<>();
        lsadts.add("left corrner");

        l.setLocationSurroundingAreaDescriptionText(lsadts);

        // LocationTwoDimentionalGeographicalCoordinateType
        LocationTwoDimentionalGeographicalCoordinateType ltdgct = new LocationTwoDimentionalGeographicalCoordinateType();
        ltdgct.setGeographicDatumCode(new String("ADI-A"));
        ltdgct.setLatitudeDegreeValue(null);
        ltdgct.setLatitudeMinuteValue(33L);
        ltdgct.setLatitudeSecondValue(new BigDecimal(3.123));
        ltdgct.setLongtitudeDegreeValue(44L);
        ltdgct.setLongtitudeMinuteValue(55L);
        ltdgct.setLongtitudeSecondValue(new BigDecimal(6.89788));
        ltdgct.setGeographicDatumCode(new String("ADI-B"));

        l.setLocationTwoDimentionalGeographicalCoordinate(ltdgct);

        // Set<String> locationSurroundingAreaDescriptionText;
        Set<String> surrSet = new HashSet<>();
        surrSet.add("A");
        surrSet.add("B");
        surrSet.add("A" + name);
        surrSet.add("A" + name + name);
        surrSet.add("B" + name);

        l.setLocationSurroundingAreaDescriptionText(surrSet);

        return l;
    }

    protected Long createFacilty() {
        Facility ret = null;
        String category = new String("COMMU");
        Facility facility = new Facility(-1L, "FacilityCodeIT" + randLong(1, 1000000), "FacilityName1" + randLong(1, 1000000), new Date(), category, null,
                parentId, organizations, null, false);
        ret = facservice.create(uc, facility);
        assertNotEquals(ret, null);
        return ret.getFacilityIdentification();
    }

}
