package syscon.arbutus.product.services.staff.contract.ejb;

import org.junit.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseTestJMockIt;
import syscon.arbutus.product.services.common.HibernateUtil;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;

/**
 * Created by os on 22/07/15.
 */
public class LightweightIT extends BaseTestJMockIt {
    private IT superIT = new IT((PersonService) personService, facilityService, activityService, userPreferenceService, organizationService, locationService,
            supervisionService, facilityCountActivityService, personTest, housingService);

    @BeforeClass
    public void beforeClass() {
        super.beforeClass();
        superIT.pTest = personTest;
        PersonIdentityType pi = superIT.createPersonalIdenity();
        superIT.personIdenttityId = pi.getPersonIdentityId();
        superIT.facilityId = superIT.createFacility();

        superIT.parentId = superIT.createFacilty();
        superIT.locations.add(superIT.createLocation());
        superIT.organizations.add(superIT.createOrganization());

        HibernateUtil.commitAndClose();
    }

    @AfterClass
    public void afterClass() {
        super.afterClass();
        HibernateUtil.commitAndClose();
    }

    @Test
    public void getCount() {
        superIT.getCount();
    }

    @Test
    public void create() {
        superIT.create();
    }

    @Test
    public void metaCode() {
        superIT.metaCode();
    }

    @Test
    public void metaCodePosition() {
        superIT.metaCodePosition();
    }

    @Test
    public void facilities() {
        superIT.facilities();
    }

    @Test
    public void searchMode() {
        superIT.searchMode();
    }

    @Test
    public void createNegative() {
        superIT.createNegative();
    }

    @Test
    public void update() {
        superIT.update();
    }

    @Test
    public void deactivate() {
        superIT.deactivate();
    }

    @Test
    public void activate() {
        superIT.activate();
    }

    @Test
    public void search() {
        superIT.search();
    }

    @Test
    public void delete() {
        superIT.delete();
    }

    @Test
    public void getAll() {
        superIT.getAll();
    }

    @Test
    public void get() {
        superIT.get();
    }

    @Test
    public void getNiem() {
        superIT.getNiem();
    }
}
