package syscon.arbutus.product.services.bam.contract.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.bam.contract.dto.BAMCCFType;
import syscon.arbutus.product.services.bam.contract.dto.BAMCaseNoteType;
import syscon.arbutus.product.services.bam.contract.dto.BAMGrievanceType;
import syscon.arbutus.product.services.bam.contract.interfaces.BAMService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.grievances.contract.ejb.Constants;

import static org.testng.Assert.assertNotNull;

public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);

    private BAMService service;

    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").serializeNulls().create();

    @BeforeClass
    public void beforeClass() {

        log.info("beforeClass Begin - JNDI lookup");
        service = (BAMService) JNDILookUp(this.getClass(), BAMService.class);
        uc = super.initUserContext();
    }

    @AfterClass
    public void afterClass() {
    }

    @Test
    public void lookupJNDI() {
        log.info("lookupJNDI Begin");
        assertNotNull(service);
    }

    @Test
    public void getGrievanceActionByJson() {

        String strGrievance = "{'eventAction':null,'grievancesId':null,'grievancesNumber':null," +
                "'grievanceDateTime':'2014-06-02 23:00:31','issueType':'COMPLAINT','issueSubType':'MEDICAL'," +
                "'facilityId':1,'facilityName':null,'supervisionId':1,'activityId':null,'locationId':1," +
                "'locationDescription':null,'issueReason':'Issue Reason','reportedBy':1,'reportedByName':null," +
                "'assignedTo':1,'assignedToName':null,'resolution':null,'officialResponse':'officialResponse'," +
                "'inFormalResponse':'ifFormalResponse','claimAmount':'100','incidentFlag':false,'status':null," +
                "'stampType':null,'inmateName':null,'inmateId':null,'noOfAppeals':null}";

        String ret = service.getGrievanceActionByJson(uc, strGrievance);
        assert (ret != null);
        //Need to update the assert if rule changed.
        assert (ret.contains("email"));

        //Not passing the eventAction attribute
        strGrievance = "{'grievancesId':null,'grievancesNumber':null," +
                "'grievanceDateTime':'2014-06-02 23:00:31','issueType':'COMPLAINT','issueSubType':'MEDICAL'," +
                "'facilityId':1,'facilityName':null,'supervisionId':1,'activityId':null,'locationId':1," +
                "'locationDescription':null,'issueReason':'Issue Reason','reportedBy':1,'reportedByName':null," +
                "'assignedTo':1,'assignedToName':null,'resolution':null,'officialResponse':'officialResponse'," +
                "'inFormalResponse':'ifFormalResponse','claimAmount':'100','incidentFlag':false,'status':null," +
                "'stampType':null,'inmateName':null,'inmateId':null,'noOfAppeals':null}";

        ret = service.getGrievanceActionByJson(uc, strGrievance);
        assert (ret != null);
        //Need to update the assert if rule changed.
        assert (ret.contains("email"));

        //Change issueType to request
        strGrievance = "{'grievancesId':null,'grievancesNumber':null," +
                "'grievanceDateTime':'2014-06-02 23:00:31','issueType':'REQUEST','issueSubType':'MEDICAL'," +
                "'facilityId':1,'facilityName':null,'supervisionId':1,'activityId':null,'locationId':1," +
                "'locationDescription':null,'issueReason':'Issue Reason','reportedBy':1,'reportedByName':null," +
                "'assignedTo':1,'assignedToName':null,'resolution':null,'officialResponse':'officialResponse'," +
                "'inFormalResponse':'ifFormalResponse','claimAmount':'100','incidentFlag':false,'status':null," +
                "'stampType':null,'inmateName':null,'inmateId':null,'noOfAppeals':null}";

        ret = service.getGrievanceActionByJson(uc, strGrievance);
        assert (ret != null);
        //Need to update the assert if rule changed.
        assert (ret.contains("eventAction"));

    }

    @Test
    public void getGrievanceAction() {

        log.info("getGrievanceAction Begin");
        BAMGrievanceType grievance = getBAMGrievancesTypeInstance();
        BAMGrievanceType ret = service.getGrievanceAction(uc, grievance);
        String retString = gson.toJson(ret);
        log.info("retString=" + retString);
        assert (ret != null);
        assert (ret.getEventAction().equals("email"));
        log.info("getGrievanceAction End");
    }

    @Test
    public void getCaseNoteAction() {

        log.info("getCaseNoteAction Begin");
        BAMCaseNoteType caseNote = getBAMCaseNoteTypeInstance();
        BAMCaseNoteType ret = service.getCaseNoteAction(uc, caseNote);
        String retString = gson.toJson(ret);
        log.info("retString=" + retString);
        assert (ret != null);
        assert (ret.getEventAction().equals("EMAIL"));
        log.info("getCaseNoteAction End");

    }

    @SuppressWarnings("deprecation")
    private BAMGrievanceType getBAMGrievancesTypeInstance() {
        BAMGrievanceType type = new BAMGrievanceType();
        // caseNoteType.setCaseNoteNumber(service.getNextEventLogNumber(uc, new
        // CodeType(Refer)));
        Date eventDate = new Date();
        Date eventTime = new Date();
        eventTime.setHours(1);
        eventTime.setMinutes(1);
        eventTime.setSeconds(1);
        type.setClaimAmount("100");
        type.setGrievanceDateTime(eventDate);
        type.setIncidentFlag(Boolean.FALSE);
        type.setIssueReason("Issue Reason");
        type.setIssueSubType(Constants.IssueSubType.MEDICAL.code());
        type.setIssueType(Constants.IssueTypes.COMPLAINT.code());
        type.setLocationId(1L);
        type.setFacilityId(1L);
        type.setSupervisionId(1L);
        type.setReportedBy(1L);
        type.setOfficialResponse("officialResponse");
        type.setInFormalResponse("ifFormalResponse");
        type.setAssignedTo(1L);

        return type;
    }

    private BAMCaseNoteType getBAMCaseNoteTypeInstance() {

        BAMCaseNoteType caseNoteType = new BAMCaseNoteType();
        Date eventDate = new Date();
        Date eventTime = new Date();
        eventTime.setHours(1);
        eventTime.setMinutes(1);
        eventTime.setSeconds(1);
        caseNoteType.setEventDate(eventDate);
        caseNoteType.setEventTime(eventTime);
        caseNoteType.setNarrativeDesc("This is Test Case Note");
        caseNoteType.setNoteType("CAU");
        caseNoteType.setNoteSubType("ACTIVE");
        caseNoteType.setReportingStaff(1L);
        caseNoteType.setSupervisionId(1L);

        //Add CCF columns
        List<BAMCCFType> ccfs = new ArrayList<BAMCCFType>();
        BAMCCFType ccf1 = new BAMCCFType();
        ccf1.setColumnName("ForReviewColumn");
        ccf1.setColumnValue("FORREVIEW2");
        ccfs.add(ccf1);

        BAMCCFType ccf2 = new BAMCCFType();
        ccf2.setColumnName("CCFColumn2");
        ccf2.setColumnValue("Others");
        ccfs.add(ccf2);
        caseNoteType.setBamCCFTypes(ccfs);
        return caseNoteType;
    }

}
