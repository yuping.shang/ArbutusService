package syscon.arbutus.product.services.bpmextension.contract.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.bpmextension.contract.dto.TaskDataSearchType;
import syscon.arbutus.product.services.bpmextension.contract.dto.TaskDataViewReturnType;
import syscon.arbutus.product.services.bpmextension.contract.dto.TaskType;
import syscon.arbutus.product.services.bpmextension.contract.interfaces.BPMExtensionService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.ModuleConfig;

@ModuleConfig
public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);
    private BPMExtensionService service;

    @AfterMethod
    public void afterMethod() {
    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (BPMExtensionService) JNDILookUp(this.getClass(), BPMExtensionService.class);
        uc = super.initUserContext();
    }

    @BeforeMethod
    public void beforeMethod() {
        service.deleteAll(null);
        //        SecurityClient client;
        //        try {
        //            client = SecurityClientFactory.getSecurityClient();
        //            client.setSimple("devtest", "devtest"); // nice regular service user
        //            client.login();
        //        } catch (Exception e) {
        //            // TODO Auto-generated catch block
        //            e.printStackTrace();
        //        }
    }

    @SuppressWarnings("deprecation")
    @Test
    public void getCount() {

        //

        Long lret = service.getCount(uc);
        assert (lret.equals(0L));
    }

    @Test
    public void create() {

        TaskType task = new TaskType("admitoffender", "OFFENDER", new Date(), new Date());
        task.setExternalId("1");
        TaskType ret = service.create(uc, task);
        ret = service.create(uc, task);
        ret = service.create(uc, task);
        ret = service.create(uc, task);
        ret = service.create(uc, task);
        assert (ret != null);
    }

    @Test
    public void getTasksByEntity() {

        List<TaskType> taskList = service.getTasksByEntity(uc, 62L, false);
        assert (taskList != null);
    }

    @Test
    public void getUncompletedTasksByEntity() {

        List<TaskType> taskList = service.getUncompletedTasksByEntity(uc, 62L, false);
        assert (taskList != null);
    }

    @Test
    public void getExpiredTasks() {
        List<Long> taskList = service.getExpiredTasks(uc, false);
        assert (taskList != null);
    }

    @Test
    public void updateEntity() {
        service.updateEntity(uc, String.valueOf(4L), 71L, null);
    }

    @Test
    public void addTaskMeta() {
        TaskType task = new TaskType("admitoffender", "OFFENDER", new Date(), new Date());
        task.setExternalId(String.valueOf(9998L));
        TaskType ret = service.create(uc, task);

        service.addTaskMeta(uc, String.valueOf(9998L), "locId", "2");

        ret = service.get(uc, String.valueOf(9998L), true);
        assert (ret != null);
    }

    @Test
    public void updateTaskMeta() {
        TaskType task = new TaskType("admitoffender", "OFFENDER", new Date(), new Date());
        task.setExternalId(String.valueOf(9999L));
        TaskType ret = service.create(uc, task);

        service.updateTaskMeta(uc, String.valueOf(9999L), "locId", "1");

        ret = service.get(uc, String.valueOf(9999L), true);
        assert (ret != null);
    }

    @Test
    public void searchMyTask() {

        List<String> groupOwnerList = new ArrayList<String>();
        List<String> statusList = new ArrayList<String>();
        groupOwnerList.add("ArbutusWorkflowUser");
        statusList.add("InProgress");
        statusList.add("Reserved");
        List<Long> facilityIdList = new ArrayList<Long>();
        facilityIdList.add(3l);
        TaskDataSearchType taskDataSearchType = new TaskDataSearchType();
        taskDataSearchType.setTaskStatusList(statusList);
        taskDataSearchType.setFacilityIdList(facilityIdList);
        taskDataSearchType.setCreatedByUser("devtest");
        taskDataSearchType.setGroupOwnerIdList(groupOwnerList);
        TaskDataViewReturnType ret = service.searchTaskData(uc, taskDataSearchType);
    }

}