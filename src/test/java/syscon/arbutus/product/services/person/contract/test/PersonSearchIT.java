package syscon.arbutus.product.services.person.contract.test;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.LocationTest;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.exception.ArbutusOptimisticLockException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.location.contract.dto.Location;
import syscon.arbutus.product.services.person.contract.dto.*;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.realization.util.BeanHelper;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

@ModuleConfig
public class PersonSearchIT extends BaseIT {

    private UserContext uc = null;

    private List<Long> testPersonIdList = new ArrayList<Long>();

    private Date time1;
    private Date time3;

    private PersonService service;
    private FacilityService facilityService;
    private ActivityService activityService;

    private LocationTest locTest;

    @BeforeClass
    public void BeforeClass() {

        service = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        facilityService = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        activityService = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        uc = super.initUserContext();

        locTest = new LocationTest();

        //        activityService.deleteAll(uc);
        //        facilityService.deleteAll(uc);
    }

    @AfterClass
    public void afterClass() {

        //        locTest.deleteAll();
        //        service.deleteAllPersonType(uc);
    }

    @Test
    public void getVersion() {
        String version = service.getVersion(uc);
        assert ("2.1".equals(version));
    }

    @Test(enabled = true)
    public void reset() {
        Long ret = service.deleteAllPersonType(uc);
        assertEquals(ret.longValue(), 1L);
    }

    @Test
    public void get() {

        //PersonId is null
        verifyGetNegative(null, InvalidInputException.class);

        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        instanceForTest.setActiveStatus("ACTIVE");    //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);
        Long id = rt.getPersonIdentification();
        testPersonIdList.add(id);

        //Long id = 13L;
        PersonType ret = service.getPersonType(uc, id);
        assertNotNull(ret);

    }

    @Test
    public void getCount() {

        Long ret = service.getPersonTypeCount(uc);
        assert (ret >= 0L);
    }

    @Test
    public void getAll() {
        //Call getAll method
        Set<Long> ret = service.getAllPersonType(uc);
        assert (ret.size() >= 0L);
    }

    @Test
    public void create() {

        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        instanceForTest.setActiveStatus("ACTIVE");    //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);
        Long id = rt.getPersonIdentification();
        testPersonIdList.add(id);

        //Long id = 13L;
        PersonType ret = service.getPersonType(uc, id);
        assertNotNull(ret);

    }

    @Test
    public void update() {

        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        instanceForTest.setActiveStatus("ACTIVE");    //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);

        //UpdatedU
        rt.setActiveStatus("Inactive");
        rt.setAccent("English Accent");
        rt.getPersonalInformation().setBirthCity("Richmond");
        PersonType rtUpdate = service.updatePersonType(uc, rt);
        assertNotNull(rtUpdate);

    }

    @Test
    public void delete() {

        //PersonId is null
        verifyDeleteNegative(null, InvalidInputException.class);

        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        instanceForTest.setActiveStatus("ACTIVE");    //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);

        Long ret = service.deletePersonType(uc, rt.getPersonIdentification());
        assert (ret.equals(ReturnCode.Success.returnCode()));

    }

    @Test
    public void deleteAll() {
        Long ret = service.deleteAllPersonType(uc);
        assert (ret.equals(ReturnCode.Success.returnCode()));

    }

    @Test(enabled = true)
    public void getNium() {

        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);

        String ret = service.getNIEM(uc, rt.getPersonIdentification());
        assertNotNull(ret);
    }

    @Test(enabled = false, groups = "group_history")
    public void getHistory() {

        Date startTime = new Date();
        time1 = new Date(); //DateTimeUtil.getCurrentUTCTime();
        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        instanceForTest.setActiveStatus("ACTIVE");    //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);
        PersonType created = rt;
        testPersonIdList.add(created.getPersonIdentification());

        PersonType person2 = ContractTestUtil.createWellFormedPerson();
        PersonType rt2 = service.createPersonType(uc, person2);
        assertNotNull(rt2);
        PersonType created2 = rt2;
        testPersonIdList.add(created2.getPersonIdentification());

        PhysicalIdentifierType phi = created.getPhysicalIdentifier();
        phi.setBuild("HEAVY");        //new CodeType(ReferenceSet.BUILD.name(),
        phi.setHairAppearance("SHLDR");    //new CodeType(ReferenceSet.HAIRAPPEARANCE.name(),
        phi.setEyeColorCode("BLU");    //new CodeType(ReferenceSet.EYECOLOR.name(),

        //images
        PhysicalMarkType mark = created.getPhysicalMarks().iterator().next();
        mark.setBodySide("b");        //new CodeType(ReferenceSet.PHYSICALMARKSIDE.name(),
        mark.setMarkLocation("face");    ///new CodeType(ReferenceSet.PHYSICALMARKLOCATION.name(),
        PhotoType image = new PhotoType();
        image.setCaptureDate(new Date());
        image.setDefaultImage(true);
        image.setPersonId(instanceForTest.getPersonIdentification());
        mark.getMarkImages().add(image);

        //new image data
        byte[] data2 = new byte[5];
        for (int i = 0; i < 5; i++) {
            data2[i] = (byte) (i);
        }
        image.getImage().setPhoto(data2);
        created.getPhysicalMarks().add(mark);

        PhysicalMarkType pm2 = new PhysicalMarkType();
        pm2.setMarkCategory("Tat");        //new CodeType(ReferenceSet.PHYSICALMARK.name(),
        pm2.setMarkDescription("snake");
        pm2.setMarkLocation("hand");    //new CodeType(ReferenceSet.PHYSICALMARKLOCATION.name(),
        pm2.setBodySide("l");            //new CodeType(ReferenceSet.PHYSICALMARKSIDE.name(),
        created.getPhysicalMarks().add(pm2);

        PersonType ud = service.updatePersonType(uc, created);
        assertNotNull(ud);

        time3 = new Date();
        DateTimeUtil.getCurrentUTCTime();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Date endTime = new Date();
        PersonsReturnType hs = service.getHistory(uc, created.getPersonIdentification(), time1, time3);
        assertNotNull(hs);
        assertNotNull(hs.getPersons());

        PersonSearchType historySearchCriteria = new PersonSearchType();
        historySearchCriteria.setSearchHistory(true);
        historySearchCriteria.setHistorySearchStartTime(startTime);
        historySearchCriteria.setHistorySearchEndTime(endTime);

        PhysicalIdentifierSearchType pc = new PhysicalIdentifierSearchType();
        pc.setBuild("HEAVY");        //new CodeType(ReferenceSet.BUILD.name(),
        //	pc.setHairAppearance("long");
        historySearchCriteria.setPhysicalIdentifierSearch(pc);

        PhysicalMarkSearchType pm = new PhysicalMarkSearchType();
        pm.setBodySide("f");        //new CodeType(ReferenceSet.PHYSICALMARKSIDE.name(),
        pm.setMarkLocation("Arm");    //new CodeType(ReferenceSet.PHYSICALMARKLOCATION.name(),
        historySearchCriteria.setPhysicalMarkSearch(pm);

        //Search history
        PersonsReturnType hps = service.searchPersonType(uc, null, historySearchCriteria, 0L, 200L, null);
        assertNotNull(hps);

        assertEquals(hps.getTotalSize().longValue(), 2l);

        //Search current data
        PersonSearchType personSearchCriteria = new PersonSearchType();
        personSearchCriteria.setSearchHistory(false);

        PhysicalMarkSearchType pmc2 = new PhysicalMarkSearchType();
        pmc2.setMarkCategory("Tat");    //new CodeType(ReferenceSet.PHYSICALMARK.name(),
        pmc2.setMarkDescription("snake");
        personSearchCriteria.setPhysicalMarkSearch(pmc2);

        PersonsReturnType ps = service.searchPersonType(uc, null, personSearchCriteria, 0L, 200L, null);
        assertNotNull(ps);

        assertEquals(ps.getTotalSize().longValue(), 1l);
    }

    //    @Test(enabled = true)
    //    public void getPhysicalMarkImages() {
    //
    //        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
    //        PersonType rt = service.createPersonType(uc, instanceForTest);
    //        assertNotNull(rt);
    //
    //        Set<Long> imageIdSet = getImageIdSet(rt.getPhysicalMarks());
    //
    //        ImagesReturnType ret = service.getPhysicalMarkImages(uc, imageIdSet);
    //        assertNotNull(ret);
    //
    //    }

    @Test
    public void getThumbnailPhoto() {

        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);

        // set/get person photo
        Date captureDate = new Date();
        byte[] img1 = new byte[] { 1, 1, 1, 1 };
        byte[] img2 = new byte[] { 2, 2, 2, 2 };
        byte[] img3 = new byte[] { 3, 3, 3, 3 };

        Long id1 = rt.getPersonIdentification();

        PhotoType image = new PhotoType();
        image.setCaptureDate(captureDate);
        image.getImage().setPhoto(img1);
        image.setPersonId(id1);
        image.setDefaultImage(true);

        PhotoType imgType = service.addPhoto(uc, id1, image);
        assertEquals(imgType.getImage(), image.getImage());

        image.getImage().setPhoto(img2);
        imgType = service.addPhoto(uc, id1, image);
        assertEquals(imgType.getImage(), imgType.getImage());

        image.getImage().setPhoto(img3);
        imgType = service.addPhoto(uc, id1, image);
        assertEquals(imgType.getImage(), imgType.getImage());

        ImageType ret = service.getThumbnailPhoto(uc, id1, imgType.getId());
        assertNotNull(ret.getImage());

    }

    //Comment this test case because it depends client's image file, can do this test manual.
    //@Test
    //    public void getResizedPhotos() {
    //
    //        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
    //        PersonType rt = service.createPersonType(uc, instanceForTest);
    //        assertNotNull(rt);
    //
    //        Iterator readers = ImageIO.getImageReadersByFormatName("PNG");
    //        ImageReader reader = (ImageReader) readers.next();
    //        try {
    //            // set/get person photo
    //            Date captureDate = new Date();
    //            // byte[] img1 = new byte[]{1, 1, 1, 1};
    //            File imageSrc = new File("/home/dev/Downloads/png2.png");
    //            File imageSrc2 = new File("/home/dev/Downloads/png2.png");
    //            ImageInputStream iis = ImageIO.createImageInputStream(imageSrc);
    //            ImageInputStream iis2 = ImageIO.createImageInputStream(imageSrc);
    //
    //            reader.setInput(iis, true);
    //            ImageReadParam param = reader.getDefaultReadParam();
    //            BufferedImage image = reader.read(0, param);
    //
    //            reader.setInput(iis2, true);
    //            ImageReadParam param2 = reader.getDefaultReadParam();
    //            BufferedImage image2 = reader.read(0, param2);
    //
    //            ByteArrayOutputStream baos = new ByteArrayOutputStream();
    //            ImageIO.write(image, "png", baos);
    //            baos.flush();
    //            byte[] imageInByte = baos.toByteArray();
    //            baos.close();
    //
    //            ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
    //            ImageIO.write(image2, "png", baos2);
    //            baos2.flush();
    //            byte[] imageInByte2 = baos2.toByteArray();
    //            baos2.close();
    //
    //            Long id1 = rt.getPersonIdentification();
    //
    //            PhotoType imageOut = new PhotoType();
    //            imageOut.setCaptureDate(captureDate);
    //            imageOut.getImage().setPhoto(imageInByte);
    //            imageOut.setPersonId(id1);
    //            imageOut.setDefaultImage(true);
    //
    //            PhotoType imageOut2 = new PhotoType();
    //            imageOut2.setCaptureDate(captureDate);
    //            imageOut2.getImage().setPhoto(imageInByte2);
    //            imageOut2.setPersonId(id1);
    //            imageOut2.setDefaultImage(true);
    //
    //            PhotoType imgType = service.addPhoto(uc, id1, imageOut);
    //            assertEquals(imgType.getImage(), imageOut.getImage());
    //
    //            PhotoType imgType2 = service.addPhoto(uc, id1, imageOut2);
    //            assertEquals(imgType2.getImage(), imageOut2.getImage());
    //
    //            List<Long> imageIds = new ArrayList<Long>();
    //            imageIds.add(imgType.getId());
    //            imageIds.add(imgType2.getId());
    //
    //            List<PhotoType> ret = service.getResizedPhotos(uc, id1, imageIds, 300, 400);
    //
    //            assert (ret.size() == 2);
    //
    //            for (PhotoType img : ret) {
    //                assert (img.getDefaultImage().equals(Boolean.TRUE));
    //            }
    //
    //        } catch (IOException e) {
    //            e.printStackTrace();
    //        }
    //
    //    }

    //@Test
    public void getResizedPhoto() {

        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);

        Iterator readers = ImageIO.getImageReadersByFormatName("PNG");
        ImageReader reader = (ImageReader) readers.next();
        try {
            // set/get person photo
            Date captureDate = new Date();
            // byte[] img1 = new byte[]{1, 1, 1, 1};
            File imageSrc = new File("/home/dev/Downloads/png2.png");
            ImageInputStream iis = ImageIO.createImageInputStream(imageSrc);

            reader.setInput(iis, true);

            ImageReadParam param = reader.getDefaultReadParam();
            BufferedImage image2 = reader.read(0, param);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image2, "png", baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            baos.close();

            Long id1 = rt.getPersonIdentification();

            PhotoType image = new PhotoType();
            image.setCaptureDate(captureDate);
            image.getImage().setPhoto(imageInByte);
            image.setPersonId(id1);
            image.setDefaultImage(true);

            PhotoType imgType = service.addPhoto(uc, id1, image);
            assertEquals(imgType.getImage(), image.getImage());

            PhotoType ret = service.getResizedPhoto(uc, id1, imgType.getId(), 300, 400);
            assertNotNull(ret.getImage());

            InputStream in = null;
            in = new ByteArrayInputStream(ret.getImage().getPhoto());

            iis = ImageIO.createImageInputStream(in);

            reader.setInput(iis, true);

            BufferedImage image3 = reader.read(0, param);
            if (image3 != null) {
                int imageWidth = image3.getWidth(null);
                int imageHeight = image3.getHeight(null);
                assert (imageWidth == 300);
                assert (imageHeight == 400);
            }

            File imageFile = new File("/home/dev/Downloads/png2_out.png");
            ImageIO.write(image3, "png", imageFile);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //    @Test
    //    public void setPhoto() {
    //
    //        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
    //        PersonType rt = service.createPersonType(uc, instanceForTest);
    //        assertNotNull(rt);
    //
    //        // set/get person photo
    //        Date captureDate = new Date();
    //        byte[] img1 = new byte[] { 1, 1, 1, 1 };
    //
    //        PhotoType image = new PhotoType();
    //        image.setCaptureDate(captureDate);
    //        image.setImage(img1);
    //        image.setEyeColor("BRN");
    //        image.setDefaultImage(true);
    //
    //        Long id1 = rt.getPersonIdentification();
    //        image.setPersonId(id1);
    //        ImageType imgType = service.addPhoto(uc, id1, image);
    //        assertEquals(imgType.getImage(), img1);
    //
    //        List<PhotoType> imgRet = service.getPhotos(uc, id1, ImageKind.ORIGINAL);
    //        assertNotNull(imgRet);
    //        assertEquals(imgRet.get(0).getImage(), img1);
    //
    //    }

    //    @Test
    //    public void getPhoto() {
    //
    //        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
    //        PersonType rt = service.createPersonType(uc, instanceForTest);
    //        assertNotNull(rt);
    //
    //        // set/get person photo
    //        Date captureDate = new Date();
    //        byte[] img1 = new byte[] { 1, 1, 1, 1 };
    //        byte[] img2 = new byte[] { 2, 2, 2, 2 };
    //        byte[] img3 = new byte[] { 3, 3, 3, 3 };
    //
    //        Long id1 = rt.getPersonIdentification();
    //
    //        PhotoType image = new PhotoType();
    //        image.setCaptureDate(captureDate);
    //        image.setImage(img1);
    //        image.setEyeColor("BRN");
    //        image.setPersonId(id1);
    //        image.setDefaultImage(true);
    //
    //        PhotoType imgType = service.addPhoto(uc, id1, image);
    //        assertEquals(imgType.getImage(), image.getImage());
    //
    //        image.setImage(img2);
    //        imgType = service.addPhoto(uc, id1, image);
    //        assertEquals(imgType.getImage(), imgType.getImage());
    //
    //        image.setImage(img3);
    //        imgType = service.addPhoto(uc, id1, image);
    //        assertEquals(imgType.getImage(), imgType.getImage());
    //
    //        //Will get latest image
    //        List<PhotoType> imgRet = service.getPhotos(uc, id1, ImageKind.ORIGINAL);
    //        assertNotNull(imgRet);
    //        boolean found = false;
    //        for (ImageType img : imgRet) {
    //            if (img.getImage()[1] == (img1[1])) {
    //                found = true;
    //            }
    //        }
    //        assertEquals(found, true);
    //
    //        //InvalidInput
    //        verifyPersonPhotoNegative(null, null, InvalidInputException.class);
    //
    //        //InvalidInput
    //        verifyPersonPhotoNegative(id1, null, InvalidInputException.class);
    //
    //    }

    //    @Test
    //    public void retrieveHistoryPhotos() {
    //
    //        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
    //        PersonType rt = service.createPersonType(uc, instanceForTest);
    //        assertNotNull(rt);
    //
    //        // set/get person photo
    //        Date captureDate = new Date();
    //        byte[] img1 = new byte[] { 1, 1, 1, 1 };
    //        byte[] img2 = new byte[] { 2, 2, 2, 2 };
    //        byte[] img3 = new byte[] { 3, 3, 3, 3 };
    //
    //        Long id1 = rt.getPersonIdentification();
    //
    //        PhotoType image = new PhotoType();
    //        image.setCaptureDate(captureDate);
    //        image.setImage(img1);
    //        image.setPersonId(id1);
    //        image.setDefaultImage(true);
    //
    //        ImageType imgType = service.addPhoto(uc, id1, image);
    //        assertEquals(imgType.getImage(), image.getImage());
    //
    //        image.setImage(img2);
    //        imgType = service.addPhoto(uc, id1, image);
    //        assertEquals(imgType.getImage(), imgType.getImage());
    //
    //        image.setImage(img3);
    //        imgType = service.addPhoto(uc, id1, image);
    //        assertEquals(imgType.getImage(), imgType.getImage());
    //
    //        ImagesReturnType imgRet = service.retrieveHistoryPhotos(uc, id1, null, null, null);
    //        assertNotNull(imgRet);
    //
    //        imgRet = service.retrieveHistoryPhotos(uc, id1, ImageKind.ORIGINAL, null, null);
    //        assertNotNull(imgRet);
    //
    //        imgRet = service.retrieveHistoryPhotos(uc, id1, ImageKind.THUMBNAIL, null, null);
    //        assertNotNull(imgRet);
    //
    //        imgRet = service.retrieveHistoryPhotos(uc, id1, ImageKind.THUMBNAIL, BeanHelper.getPastDate(captureDate, 1), null);
    //        assertNotNull(imgRet);
    //
    //        imgRet = service.retrieveHistoryPhotos(uc, id1, ImageKind.THUMBNAIL, BeanHelper.nextNthDays(captureDate, 1), null);
    //        assertNotNull(imgRet);
    //
    //    }

    @Test
    public void deletePhoto() {

        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);

        // set/get person photo
        Date captureDate = new Date();
        byte[] img1 = new byte[] { 1, 1, 1, 1 };
        byte[] img2 = new byte[] { 2, 2, 2, 2 };
        byte[] img3 = new byte[] { 3, 3, 3, 3 };

        Long id1 = rt.getPersonIdentification();

        PhotoType image = new PhotoType();
        image.setCaptureDate(captureDate);
        image.getImage().setPhoto(img1);
        image.setPersonId(id1);
        image.setDefaultImage(true);

        PhotoType imgType = service.addPhoto(uc, id1, image);
        assertEquals(imgType.getImage(), image.getImage());

        image.getImage().setPhoto(img2);
        imgType = service.addPhoto(uc, id1, image);
        assertEquals(imgType.getImage(), imgType.getImage());

        image.getImage().setPhoto(img3);
        imgType = service.addPhoto(uc, id1, image);
        assertEquals(imgType.getImage(), imgType.getImage());

        Long ret = service.deletePersonPhotos(uc, id1);
        assert (ret == 1L);

    }

    @Test
    public void deleteAllPhotos() {

        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);

        // set/get person photo
        Date captureDate = new Date();
        byte[] img1 = new byte[] { 1, 1, 1, 1 };
        byte[] img2 = new byte[] { 2, 2, 2, 2 };
        byte[] img3 = new byte[] { 3, 3, 3, 3 };

        Long id1 = rt.getPersonIdentification();

        PhotoType image = new PhotoType();
        image.setCaptureDate(captureDate);
        image.getImage().setPhoto(img1);
        image.setPersonId(id1);
        image.setDefaultImage(true);

        ImageType imgType = service.addPhoto(uc, id1, image);
        assertEquals(imgType.getImage(), image.getImage());

        image.getImage().setPhoto(img2);
        imgType = service.addPhoto(uc, id1, image);
        assertEquals(imgType.getImage(), imgType.getImage());

        image.getImage().setPhoto(img3);
        imgType = service.addPhoto(uc, id1, image);
        assertEquals(imgType.getImage(), imgType.getImage());

        Long ret = service.deleteAllPhotos(uc);
        assert (ret == 1L);
    }

    @Test
    public void addPhysicalMark() {
        PersonType rt = createPerson();
        Long personId = rt.getPersonIdentification();

        PhysicalMarkType mark = new PhysicalMarkType();
        mark.setPersonId(personId);
        mark.setMarkCategory("MARK");
        String markDesc = "hook on left arm";
        mark.setMarkDescription(markDesc);
        mark.setMarkLocation("ARM");

        PhysicalMarkType ret = service.addPhysicalMark(uc, mark);
        assertNotNull(ret);
        assertEquals(ret.getMarkDescription(), markDesc);

    }

    @Test
    public void updatePhysicalMark() {

        PersonType rt = createPerson();

        PhysicalMarkType mark = rt.getPhysicalMarks().iterator().next();
        String newDesc = "new mark description";
        mark.setMarkDescription(newDesc);
        service.updatePhysicalMark(uc, mark);
        PhysicalMarkType mret = service.getPhysicalMark(uc, mark.getPhysicalMarkIdentification());
        assertNotNull(mret);
        assertEquals(mret.getMarkDescription(), newDesc);

    }

    @Test
    public void deletePhysicalMark() {

        PersonType rt = createPerson();

        PhysicalMarkType mark = rt.getPhysicalMarks().iterator().next();

        Long rc = service.deletePhysicalMark(uc, mark);
        assertEquals(rc, ReturnCode.Success.returnCode());

    }

    @Test
    public void addEducationHistory() {
        PersonType rt = createPerson();
        Long personId = rt.getPersonIdentification();

        EducationHistoryType edu = new EducationHistoryType();
        edu.setPersonId(personId);
        edu.setSchool("SFU");
        edu.setAreaOfStudy("ACCOU");

        EducationHistoryType ret = service.addEducationHistory(uc, edu);
        assertNotNull(ret);
        assertEquals(ret.getSchool(), "SFU");

        edu = new EducationHistoryType();
        edu.setPersonId(personId);
        edu.setSchool("A School");
        edu.setAreaOfStudy("ACCUP");
        edu.setEducationLevel("ELEMENTARY");
        edu.setEducationSchedule("FULL");
        edu.setSchoolComment("Comments");
        Date date = BeanHelper.createDate();
        edu.setSchoolStartDate(date);
        edu.setSchoolEndDate(date);

        ret = service.addEducationHistory(uc, edu);
        assertNotNull(ret);
        assertEquals(ret.getSchool(), "A School");

    }

    @Test
    public void updateEducationHistory() {

        PersonType rt = createPerson();

        EducationHistoryType edu = rt.getEducationHistories().iterator().next();
        String newSchool = "UBC";
        edu.setSchool(newSchool);
        service.updateEducationHistory(uc, edu);
        EducationHistoryType mret = service.getEducationHistory(uc, edu.getEducationHistoryIdentification());
        assertNotNull(mret);
        assertEquals(mret.getSchool(), newSchool);

    }

    @Test
    public void deleteEducationHistory() {

        PersonType rt = createPerson();

        EducationHistoryType edu = rt.getEducationHistories().iterator().next();

        Long rc = service.deleteEducationHistory(uc, edu);
        assertEquals(rc, ReturnCode.Success.returnCode());

    }

    @Test
    public void addEmploymentHistory() {
        PersonType rt = createPerson();
        Long personId = rt.getPersonIdentification();

        EmploymentHistoryType emp = new EmploymentHistoryType();
        emp.setPersonId(personId);
        String company = "Microsoft";
        emp.setEmployerReference(company);
        emp.setOccupation("187");

        EmploymentHistoryType ret = service.addEmploymentHistory(uc, emp);
        assertNotNull(ret);
        assertEquals(ret.getEmployerReference(), company);

    }

    @Test
    public void updateEmploymentHistory() {

        PersonType rt = createPerson();

        EmploymentHistoryType edu = rt.getEmploymentHistories().iterator().next();
        String newcom = "IBM";
        edu.setEmployerReference(newcom);
        service.updateEmploymentHistory(uc, edu);
        EmploymentHistoryType mret = service.getEmploymentHistory(uc, edu.getEmploymentHistoryIdentification());
        assertNotNull(mret);
        assertEquals(mret.getEmployerReference(), newcom);

    }

    @Test
    public void deleteEmploymentHistory() {

        PersonType rt = createPerson();

        EmploymentHistoryType edu = rt.getEmploymentHistories().iterator().next();

        Long rc = service.deleteEmploymentHistory(uc, edu);
        assertEquals(rc, ReturnCode.Success.returnCode());

    }

    @Test
    public void addHealthRecordHistory() {
        PersonType rt = createPerson();
        Long personId = rt.getPersonIdentification();

        HealthRecordHistoryType healthRecord = new HealthRecordHistoryType();
        healthRecord.setPersonId(personId);
        healthRecord.setHealthCategory("DISAB");
        healthRecord.setHealthIssue("ARTH");
        healthRecord.setHealthStatus("ONGOING");

        HealthRecordHistoryType ret = service.addHealthRecordHistory(uc, healthRecord);
        assertNotNull(ret);
        assertEquals(ret.getHealthStatus(), "ONGOING");

    }

    @Test
    public void updateHealthRecordHistory() {

        PersonType rt = createPerson();

        HealthRecordHistoryType HealthRecord = rt.getHealthRecordHistories().iterator().next();
        HealthRecord.setHealthStatus("RECOVERED");
        service.updateHealthRecordHistory(uc, HealthRecord);
        HealthRecordHistoryType mret = service.getHealthRecordHistory(uc, HealthRecord.getHealthRecordIdentification());
        assertNotNull(mret);
        assertEquals(mret.getHealthStatus(), "RECOVERED");

    }

    @Test
    public void deleteHealthRecordHistory() {

        PersonType rt = createPerson();

        HealthRecordHistoryType dto = rt.getHealthRecordHistories().iterator().next();

        Long rc = service.deleteHealthRecordHistory(uc, dto);
        assertEquals(rc, ReturnCode.Success.returnCode());

    }

    @Test
    public void addLanguage() {
        PersonType rt = createPerson();
        Long personId = rt.getPersonIdentification();

        LanguageType lang = new LanguageType();
        lang.setPersonId(personId);
        lang.setLanguage("ZU");
        lang.setPrimary(false);

        Long id = service.addLanguage(uc, lang);
        LanguageType ret = service.getLanguage(uc, id);
        assertNotNull(ret);
        assertEquals(ret.getLanguage(), "ZU");

    }

    @Test
    public void updateLanguage() {
        PersonType rt = createPerson();

        LanguageType ab = rt.getLanguages().iterator().next();

        ab.setReading("LVL3");
        service.updateLanguage(uc, ab);

        ab.setWriting("LVL3");
        try {
            service.updateLanguage(uc, ab);
        } catch (Exception ex) {
            assert (ex.getClass().equals(ArbutusOptimisticLockException.class));
        }

        LanguageType mret = service.getLanguage(uc, ab.getLanguageIdentification());

        mret.setWriting("LVL3");
        service.updateLanguage(uc, mret);

        mret = service.getLanguage(uc, ab.getLanguageIdentification());

        assertNotNull(mret);
        assertEquals(mret.getReading(), "LVL3");
        assertEquals(mret.getWriting(), "LVL3");
    }

    @Test
    public void deleteLanguage() {

        PersonType rt = createPerson();

        LanguageType dto = rt.getLanguages().iterator().next();

        Long rc = service.deleteLanguage(uc, dto);
        assertEquals(rc, ReturnCode.Success.returnCode());

    }

    @Test
    public void addMilitaryHistory() {
        PersonType rt = createPerson();
        Long personId = rt.getPersonIdentification();

        MilitaryHistoryType military = new MilitaryHistoryType();
        military.setPersonId(personId);
        military.setBranch("ARMY");

        MilitaryHistoryType ret = service.addMilitaryHistory(uc, military);
        assertNotNull(ret);
        assertEquals(ret.getBranch(), "ARMY");

    }

    @Test
    public void updateMilitaryHistory() {

        PersonType rt = createPerson();

        MilitaryHistoryType military = rt.getMilitaryHistories().iterator().next();
        String newcom = "one a solider";
        military.setMilitaryComment(newcom);
        service.updateMilitaryHistory(uc, military);
        MilitaryHistoryType mret = service.getMilitaryHistory(uc, military.getMilitaryHistoryIdentification());
        assertNotNull(mret);
        assertEquals(mret.getMilitaryComment(), newcom);

    }

    @Test
    public void deleteMilitaryHistory() {

        PersonType rt = createPerson();

        MilitaryHistoryType dto = rt.getMilitaryHistories().iterator().next();

        Long rc = service.deleteMilitaryHistory(uc, dto);
        assertEquals(rc, ReturnCode.Success.returnCode());

    }

    @Test
    public void updatePersonalInformation() {

        PersonType rt = createPerson();

        PersonalInformationType personalInfo = rt.getPersonalInformation();

        if (personalInfo == null) {
            personalInfo = new PersonalInformationType();
        }
        String city = "Ocean";
        personalInfo.setBirthCity(city);
        service.updatePersonalInformation(uc, personalInfo);
        PersonalInformationType pret = service.getPersonalInformation(uc, personalInfo.getPersonalInformationIdentification());
        assertNotNull(pret);
        assertEquals(pret.getBirthCity(), city);

    }

    @Test
    public void updatePhysicalIdentifier() {

        PersonType rt = createPerson();

        PhysicalIdentifierType physicalIdentifier = rt.getPhysicalIdentifier();
        if (physicalIdentifier == null) {
            physicalIdentifier = new PhysicalIdentifierType();
        }
        Long h = 100000l;
        physicalIdentifier.setHeight(h);
        service.updatePhysicalIdentifier(uc, physicalIdentifier);
        PhysicalIdentifierType pret = service.getPhysicalIdentifier(uc, physicalIdentifier.getPhysicalIdentifierIdentification());
        assertNotNull(pret);
        assertEquals(pret.getHeight(), h);

    }

    @Test
    public void addSkill() {
        PersonType rt = createPerson();
        Long personId = rt.getPersonIdentification();

        SkillType skill = new SkillType();
        skill.setPersonId(personId);
        skill.setSkill("HOSNEG");
        skill.setSubType("CHAP");

        SkillType ret = service.addSkill(uc, skill);
        assertNotNull(ret);
        assertEquals(ret.getSkill(), "HOSNEG");

    }

    @Test
    public void updateSkill() {

        PersonType rt = createPerson();

        SkillType ab = rt.getSkills().iterator().next();
        String skillComment = "can talk";
        ab.setSkillComment(skillComment);
        service.updateSkill(uc, ab);
        SkillType mret = service.getSkill(uc, ab.getSkillIdentification());
        assertNotNull(mret);
        assertEquals(mret.getSkillComment(), skillComment);

    }

    @Test
    public void deleteSkill() {

        PersonType rt = createPerson();
        SkillType s = rt.getSkills().iterator().next();

        Long rc = service.deleteSkill(uc, s);
        assertEquals(rc, ReturnCode.Success.returnCode());

    }

    @Test
    public void addSubstanceAbuse() {
        PersonType rt = createPerson();
        Long personId = rt.getPersonIdentification();

        SubstanceAbuseType abuse = new SubstanceAbuseType();
        abuse.setPersonId(personId);
        abuse.setDrugType("A1");

        SubstanceAbuseType ret = service.addSubstanceAbuse(uc, abuse);
        assertNotNull(ret);
        assertEquals(ret.getDrugType(), "A1");

    }

    @Test
    public void updateSubstanceAbuse() {

        PersonType rt = createPerson();
        SubstanceAbuseType ab = rt.getSubstanceAbuses().iterator().next();
        ab.setAgeFirstUsed(30l);
        service.updateSubstanceAbuse(uc, ab);
        SubstanceAbuseType mret = service.getSubstanceAbuse(uc, ab.getSubstanceAbuseIdentification());
        assertNotNull(mret);
        assertEquals(mret.getAgeFirstUsed().longValue(), 30l);

    }

    @Test
    public void deleteSubstanceAbuse() {

        PersonType rt = createPerson();

        SubstanceAbuseType drug = rt.getSubstanceAbuses().iterator().next();

        Long rc = service.deleteSubstanceAbuse(uc, drug);
        assertEquals(rc, ReturnCode.Success.returnCode());
    }

    @Test
    public void addLocation() {

        PersonType rt = createPerson();
        Long personId = rt.getPersonIdentification();

        LocationTest locTest = new LocationTest();
        Location loc = locTest.createLocationType(true, personId, null);

        Location ret = service.addLocation(uc, loc);
        assertNotNull(ret);

        locTest.deleteAll();
    }

    @Test
    public void updateLocation() {

        Location loc = createLocation();
        loc.setLocationCrossStreet("UpdateLocation");
        loc.setLocationName("UpdateLocationName");

        Location ret = service.updateLocation(uc, loc);
        assertNotNull(ret);

        locTest.deleteAll();
    }

    @Test
    public void deleteLocation() {

        Location loc = createLocation();

        Long ret = service.deleteLocation(uc, loc.getInstanceId(), loc.getLocationIdentification());
        assertEquals(ret, ReturnCode.Success.returnCode());

        locTest.deleteAll();
    }

    @Test
    public void getLocation() {

        Location loc = createLocation();
        Location ret = service.getLocation(uc, loc.getLocationIdentification());
        assertNotNull(ret);

        locTest.deleteAll();
    }

    @Test
    public void getLocations() {

        locTest.deleteAll();

        PersonType rt = createPerson();
        Long personId = rt.getPersonIdentification();

        LocationTest locTest = new LocationTest();
        Location loc = locTest.createLocationType(true, personId, null);
        Location loc2 = locTest.createLocationType(true, personId, null);
        Location loc3 = locTest.createLocationType(true, personId, null);

        Location ret = service.addLocation(uc, loc);
        assertNotNull(ret);

        Location ret2 = service.addLocation(uc, loc2);
        assertNotNull(ret2);

        Location ret3 = service.addLocation(uc, loc3);
        assertNotNull(ret3);

        List<Location> locsRet = service.getLocations(uc, personId);
        assertNotNull(locsRet);

        assertEquals(locsRet.size(), 3L);

        locTest.deleteAll();
    }

    @SuppressWarnings("deprecation")
    @Test(enabled = true)
    public void testUpdate_Images_sameid() {

        PersonType instanceForTest = new PersonType();
        instanceForTest.setActiveStatus("ACTIVE");        //new CodeType(ReferenceSet.PERSONSTATUS.name(),

        //images
        PhysicalMarkType mark = new PhysicalMarkType();
        mark.setMarkCategory("Tat");    //new CodeType(ReferenceSet.PHYSICALMARK.name(),
        mark.setMarkDescription("flower");
        mark.setBodySide("b");        //new CodeType(ReferenceSet.PHYSICALMARKSIDE.name(),
        mark.setMarkLocation("face");    //new CodeType(ReferenceSet.PHYSICALMARKLOCATION.name(),
        ImageType image1 = new ImageType();
        image1.setCaptureDate(new Date());
        image1.setDefaultImage(true);

        // image data
        byte[] data1 = new byte[5];
        for (int i = 0; i < 5; i++) {
            data1[i] = (byte) (i);
        }
        image1.getImage().setPhoto(data1);
        mark.getMarkImages().add(image1);

        ImageType image2 = new ImageType();
        byte[] data2 = new byte[5];
        for (int i = 0; i < 5; i++) {
            data2[i] = (byte) (i + 10);
        }

        image2.getImage().setPhoto(data2);
        image2.setCaptureDate(new Date("2012/3/12"));
        image2.setDefaultImage(false);
        mark.getMarkImages().add(image2);

        instanceForTest.getPhysicalMarks().add(mark);

        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);
        PersonType created = rt;
        testPersonIdList.add(created.getPersonIdentification());

        PhysicalMarkType mt = created.getPhysicalMarks().iterator().next();
        Set<ImageType> images = mt.getMarkImages();
        int i = 0;
        Long id = null;
        for (ImageType img : images) {
            if (i == 0) {
                id = img.getId();
            } else {
                img.setId(id);
            }
            i++;
        }

        //Invalid Input because duplicated ImageId
        verifyUpdateNegative(created, InvalidInputException.class);

    }

    @Test(enabled = false, groups = "group_history")
    public void testSearchAllProperties() {

        time1 = DateTimeUtil.getCurrentUTCTime();
        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        instanceForTest.setActiveStatus("ACTIVE");        //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);
        PersonType created = rt;
        testPersonIdList.add(created.getPersonIdentification());

        PersonType person2 = ContractTestUtil.createWellFormedPerson2();
        PersonType rt2 = service.createPersonType(uc, person2);
        assertNotNull(rt2);
        PersonType created2 = rt2;
        testPersonIdList.add(created2.getPersonIdentification());

        PersonSearchType pc = new PersonSearchType();
        pc.setSearchHistory(false);
        pc.setActiveStatus("active");    //new CodeType(ReferenceSet.PERSONSTATUS.name(),

        HealthRecordHistorySearchType hrh = new HealthRecordHistorySearchType();
        //		hrh.setHealthCategory("Mental");
        //		hrh.setHealthIssue("brain");
        //		hrh.setHealthStatus("fixed");
        //		HealthRecordTreatmentSearchCriteria ht = new HealthRecordTreatmentSearchCriteria();
        //		ht.setFromTreatmentStartDate(ContractTestUtil.getDateFromString("1998-01-21"));
        //		ht.setToTreatmentStartDate(ContractTestUtil.getDateFromString("1998-02-21"));
        //		ht.setHealthTreatment("health treat");
        //		ht.setTreatmentProvider("provider");
        //		hrh.setTreatment(ht);

        HealthRecordTreatmentSearchType ht2 = new HealthRecordTreatmentSearchType();
        ht2.setFromTreatmentEndDate(ContractTestUtil.getDateFromString("2001-02-01"));
        ht2.setToTreatmentEndDate(ContractTestUtil.getDateFromString("2002-02-22"));
        hrh.setTreatment(ht2);

        pc.setHealthRecordHistorySearch(hrh);

        SubstanceAbuseSearchType sa = new SubstanceAbuseSearchType();
        sa.setDrugType("E1");    //new CodeType(ReferenceSet.DRUG.name(),
        SubstanceAbuseTreatmentSearchType tr = new SubstanceAbuseTreatmentSearchType();
        tr.setTreatmentReceived("COU");    //new CodeType(ReferenceSet.DRUGTREATMENT.name(),
        tr.setTreatmentProvider("DOC");
        sa.setTreatment(tr);

        SubstanceAbuseUseSearchType us = new SubstanceAbuseUseSearchType();
        us.setSubstanceUseLevel("add");    //new CodeType(ReferenceSet.DRUGUSELEVEL.name(),
        us.setSubstanceUsePeriod("two year");
        sa.setDrugUseLevel(us);

        pc.setSubstanceAbuseSearch(sa);

        //		LanguageSearchCriteria l1 = new LanguageSearchCriteria();
        //		l1.setLanguage("Chinese");
        //		pc.getLanguages().add(l1);
        //		LanguageSearchCriteria l2 = new LanguageSearchCriteria();
        //		l2.setLanguage("French");
        //		pc.getLanguages().add(l2);
        //		pc.setFromDeathDate(ContractTestUtil.getDateFromString("1997-12-21"));
        //		pc.setToDeathDate(ContractTestUtil.getDateFromString("1997-12-22"));
        //
        //		PersonalInformationSearchCriteria pinfo = new PersonalInformationSearchCriteria();
        //		pinfo.setBirthCity("Vancouver");
        //		pinfo.setNoOfDependents(3l);
        //		pc.getPersonalInformations().add(pinfo);

        //		PhysicalIdentifierSearchCriteria pc = new PhysicalIdentifierSearchCriteria();
        //		pc.setBuild("strong");
        //		personSearchCriteria.getPhysicalIdentifiers().add(pc);
        //
        //		PhysicalIdentifierSearchCriteria pc2 = new PhysicalIdentifierSearchCriteria();
        //		pc2.setBuild("weak");
        //		pc2.setHairAppearance("long");
        //		personSearchCriteria.getPhysicalIdentifiers().add(pc2);

        EmploymentHistorySearchType empHis = new EmploymentHistorySearchType();
        empHis.setEmployerReference("apple");

        PersonsReturnType ps = service.searchPersonType(uc, null, pc, 0L, 200L, null);
        assertNotNull(ps);

        assertEquals(ps.getTotalSize().longValue(), 1l);
    }

    @Test(enabled = true)
    public void testSearch_PhysicalIdentifier() {

        reset();

        time1 = DateTimeUtil.getCurrentUTCTime();
        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        instanceForTest.setActiveStatus("ACTIVE");
        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);
        PersonType created = rt;
        testPersonIdList.add(created.getPersonIdentification());

        PersonType person2 = ContractTestUtil.createWellFormedPerson();
        PersonType rt2 = service.createPersonType(uc, person2);
        assertNotNull(rt2);
        PersonType created2 = rt2;
        testPersonIdList.add(created2.getPersonIdentification());

        PhysicalIdentifierType phi = created.getPhysicalIdentifier();
        phi.setBuild("HEAVY");        //new CodeType(ReferenceSet.BUILD.name(),
        phi.setHairAppearance("SHLDR");    //new CodeType(ReferenceSet.HAIRAPPEARANCE.name(),
        phi.setEyeColorCode("BLU");    //new CodeType(ReferenceSet.EYECOLOR.name(),

        PersonType ud = service.updatePersonType(uc, created);
        assertNotNull(ud);

        //Search current data
        PersonSearchType search = new PersonSearchType();
        search.setSearchHistory(null);

        PhysicalIdentifierSearchType pc = new PhysicalIdentifierSearchType();
        pc.setBuild("HEAVY");    //new CodeType(ReferenceSet.BUILD.name(),
        //pc.setMaxHeight(200l);
        search.setPhysicalIdentifierSearch(pc);

        PersonsReturnType ps = service.searchPersonType(uc, null, search, 0L, 200L, null);
        assertNotNull(ps);

        assertEquals(ps.getTotalSize().longValue(), 2l);
    }

    @Test(enabled = false, groups = "group_history")
    public void testSearch_BooleanProperty() {

        time1 = DateTimeUtil.getCurrentUTCTime();
        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        instanceForTest.setActiveStatus("ACTIVE");        //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);
        PersonType created = rt;
        testPersonIdList.add(created.getPersonIdentification());

        MilitaryHistorySearchType mhsc = new MilitaryHistorySearchType();
        mhsc.setServiceActive(Boolean.TRUE);

        PersonSearchType pc = new PersonSearchType();
        pc.setSearchHistory(false);
        pc.setMilitaryHistorySearch(mhsc);

        PersonsReturnType ps = service.searchPersonType(uc, null, pc, 0L, 200L, null);
        assertNotNull(ps);

        assertEquals(ps.getTotalSize().longValue(), 1l);
    }

    @SuppressWarnings("deprecation")
    @Test(enabled = false, groups = "group_history")
    public void testSearch_MilitaryHistory() {

        time1 = DateTimeUtil.getCurrentUTCTime();
        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        instanceForTest.setActiveStatus("ACTIVE");        //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);
        PersonType created = rt;
        testPersonIdList.add(created.getPersonIdentification());

        PersonType person2 = ContractTestUtil.createWellFormedPerson2();
        PersonType rt2 = service.createPersonType(uc, person2);
        assertNotNull(rt2);
        PersonType created2 = rt2;
        testPersonIdList.add(created2.getPersonIdentification());

        MilitaryHistorySearchType ms = new MilitaryHistorySearchType();
        ms.setFromEnlistDate(new Date("2010/06/17"));
        ms.setToEnlistDate(new Date("2010/06/19"));

        PersonSearchType pc = new PersonSearchType();
        pc.setSearchHistory(false);
        pc.setMilitaryHistorySearch(ms);

        PersonsReturnType ps = service.searchPersonType(uc, null, pc, 0L, 200L, null);
        assertNotNull(ps);

        assertEquals(ps.getTotalSize().longValue(), 1l);
    }

    @Test(enabled = false)
    public void testSearchMode() {
        PersonType firstPerson = new PersonType();
        firstPerson.setActiveStatus("ACTIVE");            //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        PhysicalMarkType mark = createMark("Tat", "tiger", "face");
        firstPerson.getPhysicalMarks().add(mark);

        PersonType rt = service.createPersonType(uc, firstPerson);
        assertNotNull(rt);

        //	testPersonIdList.add(created.getPersonIdentification());

        PersonType secondPerson = new PersonType();
        secondPerson.setActiveStatus("ACTIVE");    //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        PhysicalMarkType mark1 = createMark("Tat", "tiger", "face");
        secondPerson.getPhysicalMarks().add(mark1);
        PhysicalMarkType mark2 = createMark("Tat", "smake", "arm");
        secondPerson.getPhysicalMarks().add(mark2);

        PersonType rt2 = service.createPersonType(uc, secondPerson);
        assertNotNull(rt2);
        //testPersonIdList.add(created2.getPersonIdentification());

        //search
        PhysicalMarkSearchType markSearch = createMarkSearch("Tat", "tiger", "face");
        PersonSearchType personSearchCriteria = new PersonSearchType();
        personSearchCriteria.setSearchHistory(false);
        personSearchCriteria.setPhysicalMarkSearch(markSearch);
        PersonsReturnType ps = service.searchPersonType(uc, null, personSearchCriteria, 0L, 200L, null);
        assertNotNull(ps);

        assertEquals(ps.getTotalSize().longValue(), 1l);
    }

    private PersonType createPerson() {
        PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        instanceForTest.setActiveStatus("ACTIVE");
        PersonType rt = service.createPersonType(uc, instanceForTest);
        assertNotNull(rt);
        return rt;
    }

    private Location createLocation() {
        PersonType rt = createPerson();
        Long personId = rt.getPersonIdentification();

        LocationTest locTest = new LocationTest();
        Location loc = locTest.createLocationType(true, personId, null);

        Location ret = service.addLocation(uc, loc);
        assertNotNull(ret);
        return ret;
    }

    private Set<Long> getImageIdSet(Set<PhysicalMarkType> dtos) {

        Set<Long> ret = new HashSet<Long>();

        if (dtos != null) {
            for (PhysicalMarkType dto : dtos) {
                Set<ImageType> images = dto.getMarkImages();
                if (images != null) {
                    for (ImageType image : images) {
                        ret.add(image.getId());
                    }
                }
            }
        }

        return ret;
    }

    private PhysicalMarkType createMark(String cat, String desc, String location) {
        PhysicalMarkType mark = new PhysicalMarkType();
        mark.setMarkCategory(cat);    //new CodeType(ReferenceSet.PHYSICALMARK.name(),
        mark.setMarkDescription(desc);
        mark.setMarkLocation(location);    //new CodeType(ReferenceSet.PHYSICALMARKLOCATION.name(),
        return mark;
    }

    private PhysicalMarkSearchType createMarkSearch(String cat, String desc, String location) {
        PhysicalMarkSearchType mark = new PhysicalMarkSearchType();
        mark.setMarkCategory(cat);        //new CodeType(ReferenceSet.PHYSICALMARK.name(),
        mark.setMarkDescription(desc);
        mark.setMarkLocation(location);    //new CodeType(ReferenceSet.PHYSICALMARKLOCATION.name(),
        return mark;
    }

    //    private void verifyPersonPhotoNegative(Long personId, ImageKind imageKind, Class<?> expectionClazz) {
    //        try {
    //            List<PhotoType> ret = service.getPhotos(uc, personId, imageKind);
    //            assert (ret == null);
    //        } catch (Exception ex) {
    //            assert (ex.getClass().equals(expectionClazz));
    //        }
    //    }

    private void verifyUpdateNegative(PersonType person, Class<?> expectionClazz) {
        try {
            PersonType ret = service.updatePersonType(uc, person);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyDeleteNegative(Long personId, Class<?> expectionClazz) {
        try {
            Long ret = service.deletePersonType(uc, personId);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyGetNegative(Long personId, Class<?> expectionClazz) {
        try {
            PersonType ret = service.getPersonType(uc, personId);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }
}
