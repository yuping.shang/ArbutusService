package syscon.arbutus.product.services.person.contract.test;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.person.contract.dto.*;
import syscon.arbutus.product.services.realization.util.BeanHelper;

public class ContractTestUtil {
    public static UserContext initializeUserContext() {
        UserContext uc = new UserContext();
        uc.setConsumingApplicationId("clientAppId");
        //		uc.setFacilityId(10l);
        //		uc.setDeviceId(102l);
        //		uc.setProcess("FacilityManagement");
        //		uc.setTask("CreateFacility");
        uc.setTimestamp(new Date());
        return uc;
    }

    public static PersonType createWellFormedPerson() {

        Date present = new Date();
        PersonType person = new PersonType();

        person.setActiveStatus("ACTIVE");        //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        person.setNumeracyProficiency("AVERAGE");    //new CodeType(ReferenceSet.NUMERACYSKILL.name(),
        person.setDeathDate(present);
        person.setAccent("Canadian Accent");

        EducationHistoryType eduHist = createEducationHistory("SFU");
        person.getEducationHistories().add(eduHist);
        EducationHistoryType edu2 = createEducationHistory("UBC");
        person.getEducationHistories().add(edu2);

        EmploymentHistoryType emp = createEmploymentHistory("Google");
        person.getEmploymentHistories().add(emp);
        EmploymentHistoryType emp2 = createEmploymentHistory("Oracle");
        person.getEmploymentHistories().add(emp2);

        HealthRecordHistoryType hrh = createHealthRecordHistory("D");
        person.getHealthRecordHistories().add(hrh);
        HealthRecordHistoryType hrh2 = createHealthRecordHistory("DI");
        person.getHealthRecordHistories().add(hrh2);

        LanguageType lan = createLanguage("EN", true);
        person.getLanguages().add(lan);
        LanguageType fr = createLanguage("FR", false);
        person.getLanguages().add(fr);

        MilitaryHistoryType mi = createMilitaryHistory("ARMY");
        person.getMilitaryHistories().add(mi);
        MilitaryHistoryType mi2 = createMilitaryHistory("AIR");
        person.getMilitaryHistories().add(mi2);

        PersonalInformationType pi = createPersonInformation("VANCOUVER");
        person.setPersonalInformation(pi);

        PhysicalIdentifierType phi = createPhysicalIdentifier();
        person.setPhysicalIdentifier(phi);

        PhysicalMarkType pm = createPhysicalMark("TAT");
        person.getPhysicalMarks().add(pm);
        PhysicalMarkType pm2 = createPhysicalMark("MARK");
        person.getPhysicalMarks().add(pm2);

        SkillType sk = createSkill("HOSNEG", true);
        person.getSkills().add(sk);
        SkillType sk2 = createSkill("OFMANGT", true);
        person.getSkills().add(sk2);

        SubstanceAbuseType sa = createSubstanceAbuse("D3");
        person.getSubstanceAbuses().add(sa);
        SubstanceAbuseType sa2 = createSubstanceAbuse("B2");
        person.getSubstanceAbuses().add(sa2);

        return person;
    }

    public static PersonType createWellFormedPerson2() {
        PersonType person = new PersonType();
        person.setActiveStatus("ACTIVE");        //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        person.setDeathDate(ContractTestUtil.getDateFromString("1997-12-21"));
        person.setAccent("India");
        EducationHistoryType eduHist = new EducationHistoryType();
        eduHist.setAreaOfStudy("HEA");            //new CodeType(ReferenceSet.EDUCATIONSTUDYAREA.name(),
        eduHist.setSchool("Uvic");
        eduHist.setEducationLevel("UNIVERSITY");    //new CodeType(ReferenceSet.EDUCATIONLEVEL.name(),
        person.getEducationHistories().add(eduHist);
        EducationHistoryType edu2 = new EducationHistoryType();
        edu2.setAreaOfStudy("his");            //new CodeType(ReferenceSet.EDUCATIONSTUDYAREA.name(),
        edu2.setSchool("Oxford");
        edu2.setEducationLevel("ELEMENTARY");    //new CodeType(ReferenceSet.EDUCATIONLEVEL.name(),
        person.getEducationHistories().add(edu2);

        //		person.getPrivilegeFilters().add("HP");

        EmploymentHistoryType emp = new EmploymentHistoryType();
        emp.setOccupation("564");            //new CodeType(ReferenceSet.EMPLOYEEOCCUPATION.name(),
        emp.setEmployerReference("apple");
        person.getEmploymentHistories().add(emp);

        HealthRecordHistoryType hrh = new HealthRecordHistoryType();
        hrh.setHealthCategory("DISAB");    //new CodeType(ReferenceSet.HEALTHRECORDCATEGORY.name(),
        hrh.setHealthIssue("d");        //new CodeType(ReferenceSet.HEALTHISSUE.name(),
        hrh.setHealthStatus("CHRONIC");    //new CodeType(ReferenceSet.HEALTHISSUESTATUS.name(),
        HealthRecordTreatmentType ht = new HealthRecordTreatmentType();
        ht.setTreatmentStartDate(ContractTestUtil.getDateFromString("1998-02-21"));
        ht.setHealthTreatment("pd");    //new CodeType(ReferenceSet.HEALTHRECORDTREATMENT.name(),
        ht.setTreatmentProvider("dr");        //new CodeType(ReferenceSet.HEALTHTREATMENTPROVIDER.name(),
        ht.setTreatmentComment("health record treatment comment");
        hrh.getTreatments().add(ht);

        HealthRecordTreatmentType ht2 = new HealthRecordTreatmentType();
        ht2.setHealthTreatment("tre");        //new CodeType(ReferenceSet.HEALTHRECORDTREATMENT.name(),
        ht2.setTreatmentEndDate(ContractTestUtil.getDateFromString("2001-02-21"));
        hrh.getTreatments().add(ht2);

        person.getHealthRecordHistories().add(hrh);

        LanguageType lan = new LanguageType();
        lan.setLanguage("en");        //new CodeType(ReferenceSet.LANGUAGE.name(),
        lan.setPrimary(true);
        person.getLanguages().add(lan);
        LanguageType fr = new LanguageType();
        fr.setLanguage("fr");        //new CodeType(ReferenceSet.LANGUAGE.name(),
        fr.setPrimary(false);
        person.getLanguages().add(fr);

        MilitaryHistoryType mi = new MilitaryHistoryType();
        mi.setBranch("Army");                //new CodeType(ReferenceSet.MILITARYBRANCH.name(),
        mi.getWarZones().add("IRQ");        //new CodeType(ReferenceSet.MILITARYWARZONE.name(),
        mi.getSpecializations().add("ava");    //new CodeType(ReferenceSet.MILITARYSPECIALIZATION.name(),
        person.getMilitaryHistories().add(mi);

        PersonalInformationType pi = new PersonalInformationType();
        pi.setBirthCity("Burnaby");
        pi.setCitizenship("DJI");            //new CodeType(ReferenceSet.CITIZENSHIP.name(),
        pi.setMaritalStatus("2");            //new CodeType(ReferenceSet.MARITALSTATUS.name(),
        pi.setNoOfDependents(1l);

        PhysicalIdentifierType phi = new PhysicalIdentifierType();
        phi.setBuild("MEDIUM");            //new CodeType(ReferenceSet.BUILD.name(),
        phi.setHairAppearance("SHLDR");    //new CodeType(ReferenceSet.HAIRAPPEARANCE.name(),

        person.setPhysicalIdentifier(phi);

        PhysicalMarkType pm = new PhysicalMarkType();
        pm.setMarkCategory("TAT");        //new CodeType(ReferenceSet.PHYSICALMARK.name(),
        pm.setMarkDescription("flower");
        pm.setMarkLocation("ARM");        //new CodeType(ReferenceSet.PHYSICALMARKLOCATION.name(),
        pm.setBodySide("F");            //new CodeType(ReferenceSet.PHYSICALMARKSIDE.name(),
        person.getPhysicalMarks().add(pm);

        SkillType sk = new SkillType();
        sk.setSkill("PERFIN");            //new CodeType(ReferenceSet.SKILL.name(),
        sk.setSubType("BAILINF");        //new CodeType(ReferenceSet.SKILLSUBTYPE.name(),
        person.getSkills().add(sk);
        SkillType sk2 = new SkillType();
        sk2.setSkill("LANG");            //new CodeType(ReferenceSet.SKILL.name(),
        sk2.setSubType("CCTV");            //new CodeType(ReferenceSet.SKILLSUBTYPE.name(),
        person.getSkills().add(sk2);

        SubstanceAbuseType sa = new SubstanceAbuseType();
        sa.setDrugType("E1");            //new CodeType(ReferenceSet.DRUG.name(),
        SubstanceAbuseTreatmentType tr = new SubstanceAbuseTreatmentType();
        tr.setTreatmentReceived("COU");    //new CodeType(ReferenceSet.DRUGTREATMENT.name(),
        tr.setTreatmentProvider("doc");
        sa.getTreatments().add(tr);

        SubstanceAbuseUseType use = new SubstanceAbuseUseType();
        use.setSubstanceUseLevel("FRE");    //new CodeType(ReferenceSet.DRUGUSELEVEL.name(),
        use.setSubstanceUsePeriod("one year");
        sa.getSubstanceUseLevels().add(use);
        SubstanceAbuseUseType use2 = new SubstanceAbuseUseType();
        use2.setSubstanceUseLevel("add");    //new CodeType(ReferenceSet.DRUGUSELEVEL.name(),
        use2.setSubstanceUseComment("second time");
        use2.setSubstanceUsePeriod("two year");
        sa.getSubstanceUseLevels().add(use2);

        person.getSubstanceAbuses().add(sa);
        return person;
    }

    public static PersonType createWellFormedPerson3() {
        PersonType person = new PersonType();
        person.setActiveStatus("ACTIVE");        //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        person.setNumeracyProficiency("AVERAGE");    //new CodeType(ReferenceSet.NUMERACYSKILL.name(),
        person.setAccent("rec0101");

        PhysicalMarkType pm = new PhysicalMarkType();
        pm.setMarkCategory("MARK");        //new CodeType(ReferenceSet.PHYSICALMARK.name(),
        pm.setMarkDescription("mark description");
        pm.setMarkLocation("ARM");        //new CodeType(ReferenceSet.PHYSICALMARKLOCATION.name(),
        person.getPhysicalMarks().add(pm);

        LanguageType lan = new LanguageType();
        lan.setLanguage("FR");            //new CodeType(ReferenceSet.LANGUAGE.name(),
        lan.setPrimary(true);
        person.getLanguages().add(lan);

        EducationHistoryType eduHist = new EducationHistoryType();
        eduHist.setSchool("A School");
        person.getEducationHistories().add(eduHist);

        EmploymentHistoryType emp = new EmploymentHistoryType();
        emp.setOccupation("187");        //new CodeType(ReferenceSet.EMPLOYEEOCCUPATION.name(),
        emp.setEmployerReference("apple");
        emp.setCompensationRate(new BigDecimal(1345.56));
        person.getEmploymentHistories().add(emp);

        SubstanceAbuseType sa = new SubstanceAbuseType();
        sa.setDrugType("N7");        //new CodeType(ReferenceSet.DRUG.name(),
        person.getSubstanceAbuses().add(sa);

        MilitaryHistoryType mi = new MilitaryHistoryType();
        mi.setBranch("MARINE");     //new CodeType(ReferenceSet.MILITARYBRANCH.name(),
        mi.getDisciplinaryActions().add("DCM");    //new CodeType(ReferenceSet.MILITARYDISCIPLINEACTION.name(),
        person.getMilitaryHistories().add(mi);

        HealthRecordHistoryType hrh = new HealthRecordHistoryType();
        hrh.setHealthCategory("DISAB");        //new CodeType(ReferenceSet.HEALTHRECORDCATEGORY.name(),
        hrh.setHealthIssue("EPI");            //new CodeType(ReferenceSet.HEALTHISSUE.name(),
        hrh.setHealthStatus("RECOVERED");    //new CodeType(ReferenceSet.HEALTHISSUESTATUS.name(),
        person.getHealthRecordHistories().add(hrh);

        SkillType sk = new SkillType();
        sk.setSkill("LANG");        //new CodeType(ReferenceSet.SKILL.name(),
        sk.setSubType("ALO");        //new CodeType(ReferenceSet.SKILLSUBTYPE.name(),
        person.getSkills().add(sk);

        return person;
    }

    private static PersonalInformationType createPersonInformation(String city) {

        PersonalInformationType ret = new PersonalInformationType();
        ret.setCitizenship("DEU");        //new CodeType(ReferenceSet.CITIZENSHIP.name(),
        ret.setNationality("USA");
        ret.setBirthCountry("USA");        //new CodeType(ReferenceSet.COUNTRY.name(),
        ret.setBirthState("WA");
        ret.setBirthCity(city);
        ret.setEthnicity("H");
        ret.setMaritalStatus("1");        //new CodeType(ReferenceSet.MARITALSTATUS.name(),
        ret.setNoOfDependents(3l);
        ret.setReligion("JEWIS");
        ret.setSexualOrientation("HETERO");

        return ret;
    }

    private static PhysicalIdentifierType createPhysicalIdentifier() {

        PhysicalIdentifierType ret = new PhysicalIdentifierType();
        ret.setBuild("HEAVY");                //new CodeType(ReferenceSet.BUILD.name(),
        ret.setEyeColorCode("BLK");
        ret.setEyewearDescription("GLASS");
        ret.setFacialHair("BEARD");
        ret.setHairAppearance("SHAVED");    //new CodeType(ReferenceSet.HAIRAPPEARANCE.name(),
        ret.setHairColor("BLK");
        ret.setHandDexterity("AMBI");
        ret.setHeight(180L);
        ret.setWeight(120L);
        ret.setRace("A");
        ret.setSkinTone("YEL");

        return ret;
    }

    private static EducationHistoryType createEducationHistory(String schoolName) {
        Date present = new Date();
        EducationHistoryType eduHist = new EducationHistoryType();
        eduHist.setAreaOfStudy("ENGLA");        //new CodeType(ReferenceSet.EDUCATIONSTUDYAREA.name(),
        eduHist.setSchool(schoolName);
        eduHist.setEducationLevel("ELEMENTARY");    //new CodeType(ReferenceSet.EDUCATIONLEVEL.name(),
        eduHist.setSchool("EMMA");
        eduHist.setEducationSchedule("FULL");
        eduHist.setSchoolStartDate(BeanHelper.getPastDate(present, 1000));
        eduHist.setSchoolEndDate(BeanHelper.getPastDate(present, 500));
        eduHist.setSchoolComment(schoolName + " School Comment");
        return eduHist;
    }

    private static EmploymentHistoryType createEmploymentHistory(String employerName) {
        Date present = new Date();
        EmploymentHistoryType emp = new EmploymentHistoryType();
        emp.setOccupation("23");            //new CodeType(ReferenceSet.EMPLOYEEOCCUPATION.name(),
        emp.setEmployerReference(employerName);
        emp.setCompensationRate(new BigDecimal("50.00"));
        emp.setHoursPerWeek(new BigDecimal("30.50"));
        emp.setSupervisorName("SupervisionName");
        emp.setTerminationReason("TerminationReason contract end");

        emp.setWorkSchedule("PART");
        emp.setEmployerAwared(true);
        emp.setEmployerContactable(false);
        emp.setEmploymentStatus("PT");
        emp.setEmploymentStartDate(BeanHelper.getPastDate(present, 1000));
        emp.setEmploymentEndDate(BeanHelper.getPastDate(present, 100));

        emp.setEmploymentComment(employerName + " Employment Comment");
        emp.setPayPeriod("HOUR");
        emp.setWage(new BigDecimal("50.00"));
        return emp;
    }

    private static HealthRecordHistoryType createHealthRecordHistory(String healthIssue) {
        Date present = new Date();
        HealthRecordHistoryType ret = new HealthRecordHistoryType();
        ret.setHealthCategory("PHYSICALNEED");        //new CodeType(ReferenceSet.HEALTHRECORDCATEGORY.name(),
        ret.setHealthIssue(healthIssue);            //new CodeType(ReferenceSet.HEALTHISSUE.name(),
        ret.setHealthStatus("CHRONIC");    //new CodeType(ReferenceSet.HEALTHISSUESTATUS.name(),
        ret.setHealthDescription(healthIssue + " HealthDescription");
        ret.setHealthStartDate(BeanHelper.getPastDate(present, 1000));
        ret.setHealthEndDate(BeanHelper.getPastDate(present, 500));

        Set<HealthRecordTreatmentType> treatments = new HashSet<HealthRecordTreatmentType>();
        HealthRecordTreatmentType treatment1 = createHealthRecordTreatment("HOSP");
        treatments.add(treatment1);
        HealthRecordTreatmentType treatment2 = createHealthRecordTreatment("PD");
        treatments.add(treatment2);
        ret.setTreatments(treatments);

        return ret;

    }

    private static HealthRecordTreatmentType createHealthRecordTreatment(String healthTreatment) {
        Date present = new Date();
        HealthRecordTreatmentType ret = new HealthRecordTreatmentType();
        ret.setTreatmentProvider("HOS");
        ret.setHealthTreatment(healthTreatment);
        ret.setTreatmentDescription(healthTreatment + "Description");
        ret.setTreatmentComment(healthTreatment + "Comment");
        ret.setTreatmentStartDate(BeanHelper.getPastDate(present, 1000));
        ret.setTreatmentEndDate(BeanHelper.getPastDate(present, 1000));

        return ret;
    }

    private static LanguageType createLanguage(String language, boolean isPrimary) {

        LanguageType ret = new LanguageType();
        ret.setLanguage(language);            //new CodeType(ReferenceSet.LANGUAGE.name(),
        ret.setSpeechDescription(language + "Description");
        ret.setPrimary(isPrimary);
        ret.setReading("LVL3");
        ret.setSpeaking("LVL3");
        ret.setWriting("LVL3");
        ret.setListening("LVL3");

        return ret;
    }

    private static MilitaryHistoryType createMilitaryHistory(String branch) {
        Date present = new Date();
        MilitaryHistoryType ret = new MilitaryHistoryType();
        ret.setBranch(branch);            //new CodeType(ReferenceSet.MILITARYBRANCH.name(),
        ret.setMilitaryComment(branch + "comment");
        ret.setDischargeCategory("B");
        ret.setDischargeDate(BeanHelper.getPastDate(present, 1000));
        ret.setDischargeLocation(branch + "DischargeLocation");
        ret.setDischargeRank("AB");
        ret.setEnlistDate(BeanHelper.getPastDate(present, 1000));
        ret.setEnlistLocation(branch + "EnlistLocation");
        ret.setExemptionDescription(branch + "ExemptionDescription");
        ret.setHighestRank("CAPT");
        ret.setServiceActive(false);
        ret.setServiceNumber("ServiceNumber");
        ret.setUnitNumber("UnitNumber");
        ret.getDisciplinaryActions().add("CM");
        ret.getDisciplinaryActions().add("DCM");
        ret.getWarZones().add("IRQ");            //new CodeType(ReferenceSet.MILITARYWARZONE.name(),
        ret.getWarZones().add("SVK");
        ret.getSpecializations().add("AVA");    //new CodeType(ReferenceSet.MILITARYSPECIALIZATION.name(),
        ret.getSpecializations().add("CLE");

        return ret;
    }

    public static PhysicalMarkType createPhysicalMark(String category) {

        Date present = new Date();
        PhysicalMarkType ret = new PhysicalMarkType();
        ret.setMarkCategory(category);            //new CodeType(ReferenceSet.PHYSICALMARK.name(),
        ret.setMarkDescription("flower");
        ret.setMarkLocation("ARM");            //new CodeType(ReferenceSet.PHYSICALMARKLOCATION.name(),
        ret.setBodySide("F");                //new CodeType(ReferenceSet.PHYSICALMARKSIDE.name(),
        ret.setMarkOrientation("CENTR");
        ret.setDeactivationDate(BeanHelper.getPastDate(present, 1000));

        ret.getMarkImages().add(createImage(true));
        ret.getMarkImages().add(createImage(false));

        ret.getDeactivationReasons().add(createComment("Reason1"));
        ret.getDeactivationReasons().add(createComment("Reason2"));

        return ret;
    }

    private static ImageType createImage(boolean isDefault) {
        Date present = new Date();
        ImageType ret = new ImageType();
        ret.setCaptureDate(present);
        ret.setDefaultImage(isDefault);
        byte[] imageData;
        if (isDefault) {
			imageData = new byte[] { 1, 1, 1, 1 };
		} else {
			imageData = new byte[] { 2, 2, 2, 2 };
		}
        ret.getImage().setPhoto(imageData);
        return ret;
    }

    private static CommentType createComment(String comment) {
        Date present = new Date();
        CommentType ret = new CommentType();
        ret.setComment(comment);
        ret.setCommentDate(present);
        ret.setUserId("UserId");

        return ret;
    }

    private static SkillType createSkill(String skill, boolean isActive) {

        Date present = new Date();
        SkillType ret = new SkillType();
        ret.setSkill(skill);
        ret.setSubType("CCTV");
        ret.setActive(isActive);
        ret.setAcquisitionDate(BeanHelper.getPastDate(present, 1000));
        ret.setExpiryDate(BeanHelper.getPastDate(present, 50));
        ret.setSkillComment(skill + "Comment");

        return ret;

    }

    private static SubstanceAbuseType createSubstanceAbuse(String drug) {

        SubstanceAbuseType ret = new SubstanceAbuseType();
        ret.setDrugType(drug);                //new CodeType(ReferenceSet.DRUG.name(),
        ret.setAgeFirstUsed(30L);

        SubstanceAbuseTreatmentType tr = createSubstanceAbuseTreatment("MED");
        ret.getTreatments().add(tr);
        SubstanceAbuseTreatmentType tr2 = createSubstanceAbuseTreatment("COU");
        ret.getTreatments().add(tr2);

        SubstanceAbuseUseType use1 = createSubstanceAbuseUse("ADD");
        ret.getSubstanceUseLevels().add(use1);
        SubstanceAbuseUseType use2 = createSubstanceAbuseUse("FRE");
        ret.getSubstanceUseLevels().add(use2);

        return ret;

    }

    private static SubstanceAbuseTreatmentType createSubstanceAbuseTreatment(String treatment) {
        Date present = new Date();
        SubstanceAbuseTreatmentType ret = new SubstanceAbuseTreatmentType();
        ret.setTreatmentReceived(treatment);    //new CodeType(ReferenceSet.DRUGTREATMENT.name(),
        ret.setTreatmentProvider(treatment + "Provider");
        ret.setTreatmentStartDate(BeanHelper.getPastDate(present, 1000));
        ret.setTreatmentEndDate(BeanHelper.getPastDate(present, 100));
        ret.setTreatmentComment(treatment + "Comment");

        return ret;

    }

    private static SubstanceAbuseUseType createSubstanceAbuseUse(String useLevel) {

        SubstanceAbuseUseType ret = new SubstanceAbuseUseType();
        ret.setSubstanceUseLevel(useLevel);
        ret.setSubstanceInformationSource("INF");
        ret.setSubstanceUseComment(useLevel + "Comment");
        ret.setSubstanceUsePeriod(useLevel + "Period");

        return ret;

    }

    public static Date getNDayDifferenceDate(int n) {
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        cal.add(Calendar.DATE, n);
        Date date = null;
        try {
            date = (Date) ft.parse(ft.format(cal.getTime()));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }

    public static Date getDateFromString(String dateString) {
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

        Date date = null;
        try {
            date = (Date) ft.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }
}
