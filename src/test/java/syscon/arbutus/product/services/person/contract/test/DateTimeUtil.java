package syscon.arbutus.product.services.person.contract.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Utility class for DataTime functions
 *
 * @author ptan
 */
public final class DateTimeUtil {

    private static final String DATETIME_FORMAT = "yyyy-MM-dd hh:mm:ss";
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    public static Date getCurrentUTCTime() {
        SimpleDateFormat ft = new SimpleDateFormat(DATETIME_FORMAT);
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        Date date = null;
        try {
            date = (Date) ft.parse(ft.format(cal.getTime()));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }

    public static Date getCurrentUTCDate() {
        SimpleDateFormat ft = new SimpleDateFormat(DATE_FORMAT);
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        Date date = null;
        try {
            date = (Date) ft.parse(ft.format(cal.getTime()));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }

    public static String getDateTimeString(Date date) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat ft = new SimpleDateFormat(DATETIME_FORMAT);
        return ft.format(date);
    }
}
