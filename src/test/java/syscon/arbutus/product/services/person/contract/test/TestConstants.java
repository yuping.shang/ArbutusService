package syscon.arbutus.product.services.person.contract.test;

public final class TestConstants {
    public static final String LENGTH_64 = "This is so looooooooooooooooooooooooooooooooooooooooooooooooooog";
    public static final String LENGTH_65 = "This is so looooooooooooooooooooooooooooooooooooooooooooooooooogG";
    public static final String LENGTH_128 = LENGTH_64 + LENGTH_64;
    public static final String LENGTH_129 = LENGTH_64 + LENGTH_65;
    public static final String LENGTH_512 = LENGTH_128 + LENGTH_128 + LENGTH_128 + LENGTH_128;
    public static final String LENGTH_513 = LENGTH_512 + "1";
}
