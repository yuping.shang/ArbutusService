package syscon.arbutus.product.services.oicautoconf.contract.ejb;

import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.oicautoconf.contract.dto.OICAutopopulateType;
import syscon.arbutus.product.services.oicautoconf.contract.interfaces.OICAutopopulateService;

import static org.testng.AssertJUnit.assertNotNull;

public class IT extends BaseIT {
    private static Logger log = LoggerFactory.getLogger(IT.class);
    protected UserContext uc = null;
    private OICAutopopulateService service;

    @BeforeClass
    public void beforeClass() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");
        service = (OICAutopopulateService) JNDILookUp(this.getClass(), OICAutopopulateService.class);
        uc = super.initUserContext();

    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void createSignature() {
        OICAutopopulateType type = getOICAutopopulateType();
        type = service.createConfiguration(uc, type);
        assertNotNull(type.getOicconfId());
    }

    private OICAutopopulateType getOICAutopopulateType() {
        OICAutopopulateType type = new OICAutopopulateType();
        type.setIncidentTypeCode("RIOT");
        type.setInmateActionCode("RFH");
        type.setCreateUserId("devtest");
        return type;
    }

}
