package syscon.arbutus.product.services.supervision.contract.ejb;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.common.PersonIdentityTest;
import syscon.arbutus.product.services.common.PersonTest;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.person.contract.dto.personidentity.OffNumConfigType;
import syscon.arbutus.product.services.supervision.contract.dto.*;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

import static org.testng.Assert.*;

public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);

    private SupervisionService service;

    private PersonIdentityTest piTest;
    private PersonTest personTest;
    private FacilityTest facilityTest;

    private List<Long> personIds = new ArrayList<Long>();
    private List<Long> personIdentityIds = new ArrayList<Long>();
    private List<Long> facilityIds = new ArrayList<Long>();

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        uc = super.initUserContext();

        service.deleteAll(uc);

        piTest = new PersonIdentityTest();

        personTest = new PersonTest();
        personIds.add(personTest.createPerson());

        personIdentityIds.add(piTest.createPersonIdentity(personIds.get(0)));

        facilityTest = new FacilityTest();
        facilityIds.add(facilityTest.createFacility());
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    @Test(dependsOnMethods = "testJNDILookup")
    public void reset() {
        service.deleteAll(uc);
    }

    @Test
    public void getVersion() {
        String version = service.getVersion(uc);
        assert ("1.1".equals(version));
    }

    @Test
    public void createConfigVar() {
        /*
        // set config var
		SupervisionConfigVariablesType configVariablesType = new SupervisionConfigVariablesType();
		configVariablesType.setSupervisionDisplayIDFormat("DD.MM.YY - 99999");
		configVariablesType.setSupervisionDisplayIDGenerationFlag(true);
		createConfigVars(uc, configVariablesType);
		*/

        SupervisionConfigVariablesType configVariablesType = new SupervisionConfigVariablesType();
        configVariablesType.setSupervisionDisplayIDFormat("DD.mm.YY - 999");
        configVariablesType.setSupervisionDisplayIDGenerationFlag(false);

        SupervisionConfigVariablesType returnType = service.createConfigVars(uc, configVariablesType);
        assertNotNull(returnType);
    }

    @Test(dependsOnMethods = "createConfigVar")
    public void updateConfigVar() {
        SupervisionConfigVariablesType configVariablesType = new SupervisionConfigVariablesType();
        configVariablesType.setSupervisionDisplayIDFormat("DD.MM.YYYY - 99999");
        configVariablesType.setSupervisionDisplayIDGenerationFlag(true);

        SupervisionConfigVariablesType returnType = service.updateConfigVars(uc, configVariablesType);
        assertNotNull(returnType);
    }

    @Test(dependsOnMethods = "updateConfigVar")
    public void getConfigVar() {
        SupervisionConfigVariablesType returnType = service.getConfigVars(uc);
        assertNotNull(returnType);

        assertEquals(returnType.getSupervisionDisplayIDFormat(), "DD.MM.YYYY - 99999");
        assertEquals(returnType.getSupervisionDisplayIDGenerationFlag(), Boolean.TRUE);
    }

    //@Test
    public void createSupervisionPersonOffNumConfig() {
        SupervisionConfigVariablesType configVariablesType = new SupervisionConfigVariablesType();
        configVariablesType.setSupervisionDisplayIDFormat("DD.mm.YY - 999");
        configVariablesType.setSupervisionDisplayIDGenerationFlag(false);
        OffNumConfigType offNumConfig = new OffNumConfigType("A", true);
        SupervisionPersonOffNumConfigType supervisionPersonOffNumConfigType = new SupervisionPersonOffNumConfigType();
        supervisionPersonOffNumConfigType.setPersonidentityOffNumConfigType(offNumConfig);
        supervisionPersonOffNumConfigType.setSupervisionConfigVariablesType(configVariablesType);
        SupervisionPersonOffNumConfigType returnType = service.createSupervisionPersonOffNumConfig(uc, supervisionPersonOffNumConfigType);// (uc,
        // configVariablesType);
        assertNotNull(returnType);
    }

    //@Test(dependsOnMethods = "createSupervisionPersonOffNumConfig")
    public void updateSupervisionPersonOffNumConfig() {
        SupervisionConfigVariablesType configVariablesType = new SupervisionConfigVariablesType();
        configVariablesType.setSupervisionDisplayIDFormat("DD.MM.YYYY - 99999");
        configVariablesType.setSupervisionDisplayIDGenerationFlag(true);
        OffNumConfigType configType = new OffNumConfigType("B", true);
        SupervisionPersonOffNumConfigType supervisionPersonOffNumConfigType = new SupervisionPersonOffNumConfigType();
        supervisionPersonOffNumConfigType.setPersonidentityOffNumConfigType(configType);
        supervisionPersonOffNumConfigType.setSupervisionConfigVariablesType(configVariablesType);

        SupervisionPersonOffNumConfigType returnType = service.updateSupervisionPersonOffNumConfig(uc, supervisionPersonOffNumConfigType);
        assertNotNull(returnType);
    }

    //@Test(dependsOnMethods = "updateSupervisionPersonOffNumConfig")
    public void getSupervisionPersonOffNumConfig() {
        SupervisionPersonOffNumConfigType returnType = service.getSupervisionPersonOffNumConfig(uc);
        assertNotNull(returnType);
        assertEquals(returnType.getSupervisionConfigVariablesType().getSupervisionDisplayIDFormat(), "DD.MM.YYYY - 99999");
        assertEquals(returnType.getSupervisionConfigVariablesType().getSupervisionDisplayIDGenerationFlag(), Boolean.TRUE);
        assert (returnType.getPersonidentityOffNumConfigType().getFormat().equals("A"));
        assert (returnType.getPersonidentityOffNumConfigType().isGeneration().equals(Boolean.TRUE));
    }

    @Test
    public void create() {

        //SupervisionType is null
        verifyCreateNegative(null, InvalidInputException.class);

        //PersonIdentity is null
        SupervisionType supType = new SupervisionType();
        supType.setFacilityId(facilityIds.get(0));
        supType.setSupervisionStartDate(new Date());
        verifyCreateNegative(supType, InvalidInputException.class);

        //Reference code not exist
        SupervisionType supervision = new SupervisionType();
        supervision.setSupervisionDisplayID("ABCD");
        supervision.setSupervisionStartDate(new Date());
        supervision.setPersonIdentityId(personIdentityIds.get(0));
        supervision.setFacilityId(facilityIds.get(0));
        // create ImprisonmentStatus
        ImprisonmentStatusType imprisonmentStatus = new ImprisonmentStatusType();
        imprisonmentStatus.setImprisonmentStatus("ABC");
        imprisonmentStatus.setImprisonmentStatusStartDate(new Date());
        supervision.getImprisonmentStatus().add(imprisonmentStatus);
        verifyCreateNegative(supervision, DataNotExistException.class);

        //Success
        Long personIdentity = piTest.createPersonIdentity();
        supType = getNewSupervisionType("dId", personIdentity);
        SupervisionType returnType = service.create(uc, supType);
        assertNotNull(returnType);

    }

    @Test
    public void isUniqueSupervisionDisplayId() {

        //SupervisionType is null
        verifyCreateNegative(null, InvalidInputException.class);

        //PersonIdentity is null
        SupervisionType supType = new SupervisionType();
        supType.setFacilityId(facilityIds.get(0));
        supType.setSupervisionStartDate(new Date());
        verifyCreateNegative(supType, InvalidInputException.class);

        //Reference code not exist
        SupervisionType supervision = new SupervisionType();
        supervision.setSupervisionDisplayID("ABCD");
        supervision.setSupervisionStartDate(new Date());
        supervision.setPersonIdentityId(personIdentityIds.get(0));
        supervision.setFacilityId(facilityIds.get(0));
        // create ImprisonmentStatus
        ImprisonmentStatusType imprisonmentStatus = new ImprisonmentStatusType();
        imprisonmentStatus.setImprisonmentStatus("ABC");
        imprisonmentStatus.setImprisonmentStatusStartDate(new Date());
        supervision.getImprisonmentStatus().add(imprisonmentStatus);
        verifyCreateNegative(supervision, DataNotExistException.class);

        //Success
        Long personIdentity = piTest.createPersonIdentity();
        supType = getNewSupervisionType("dId", personIdentity);
        SupervisionType returnType = service.create(uc, supType);
        assertNotNull(returnType);

        assertEquals(false, service.isUniqueSupervisionDisplayId(uc, supType.getSupervisionDisplayID()));

        supType.setSupervisionDisplayID("WXYZ");

        assertEquals(true, service.isUniqueSupervisionDisplayId(uc, supType.getSupervisionDisplayID()));

    }

    @Test
    public void update() {

        SupervisionType sup = createSupervision(personTest.createPerson());

        sup.setSupervisionDisplayID("updated_dId");
        //sup.setSupervisionEndDate(getDate(2019, 1, 1));
        sup.setFacilityId(facilityIds.get(0));

        SupervisionType supReturn = service.update(uc, sup);
        assertNotNull(supReturn);

        //Defect #2269
        SupervisionType supNew = supReturn;
        supNew.setFacilityId(facilityTest.createFacility());
        supReturn = service.update(uc, supNew);
        assertNotNull(supReturn);

    }

    @Test
    public void addImprisonmentStatus() {

        String statusSet = "ICE";
        String subStatusSet = "PS";

        SupervisionType sup = createSupervision(personTest.createPerson());
        Long supervisionId = sup.getSupervisionIdentification();

        // create ImprisonmentStatus
        ImprisonmentStatusType imprisonmentStatus = new ImprisonmentStatusType();
        imprisonmentStatus.setImprisonmentStatus(statusSet);
        imprisonmentStatus.setImprisonmentSubStatus(subStatusSet);
        imprisonmentStatus.setImprisonmentStatusStartDate(getDate(2019, 1, 1));

        ImprisonmentStatusType returnType = service.addImprisonmentStatus(uc, supervisionId, imprisonmentStatus);
        assertNotNull(returnType);
        assertNotNull(returnType.getImprisonmentStatus());

        imprisonmentStatus = returnType;
        Long imprisonmentStatusId = imprisonmentStatus.getImprisonmentStatusIdentification();
        assertNotNull(imprisonmentStatusId);

        // check supervision imprisonment status set size
        SupervisionType supReturn = service.get(uc, supervisionId);
        List<ImprisonmentStatusType> statusTypes = supReturn.getImprisonmentStatus();
        assertEquals(statusTypes.size(), 4);

        //test defect 1542
        statusSet = "BOP";
        imprisonmentStatus = new ImprisonmentStatusType();
        imprisonmentStatus.setImprisonmentStatus(statusSet);
        imprisonmentStatus.setImprisonmentSubStatus(null);
        imprisonmentStatus.setImprisonmentStatusStartDate(getDate(2019, 1, 1));

        returnType = service.addImprisonmentStatus(uc, supervisionId, imprisonmentStatus);
        assertNotNull(returnType);
        assertNotNull(returnType.getImprisonmentStatus());
        assertEquals(returnType.getImprisonmentSubStatus(), null);

        imprisonmentStatus = returnType;
        imprisonmentStatusId = imprisonmentStatus.getImprisonmentStatusIdentification();
        assertNotNull(imprisonmentStatusId);

        // check supervision imprisonment status set size
        supReturn = service.get(uc, supervisionId);
        statusTypes = supReturn.getImprisonmentStatus();
        assertEquals(statusTypes.size(), 5);

    }

    @Test
    public void removeImprisonmentStatus() {

        SupervisionType sup = createSupervision(personTest.createPerson());
        Long supervisionId = sup.getSupervisionIdentification();
        Long imprisonmentStatusId = sup.getImprisonmentStatus().get(0).getImprisonmentStatusIdentification();
        ImprisonmentStatusType returnType = service.removeImprisonmentStatus(uc, supervisionId, imprisonmentStatusId);

        assertNotNull(returnType);

        // check supervision imprisonment status set size
        SupervisionType ret = service.get(uc, supervisionId);
        assertEquals(ret.getImprisonmentStatus().size(), 2);

    }

    @Test
    public void updateImprisonmentStatus() {

        String statusSet = "USM";
        String subStatusSet = "SR";

        SupervisionType sup = createSupervision(personTest.createPerson());
        Long supervisionId = sup.getSupervisionIdentification();

        int size = sup.getImprisonmentStatus().size();
        ImprisonmentStatusType arr[] = new ImprisonmentStatusType[size];
        arr = sup.getImprisonmentStatus().toArray(arr);
        assertNotNull(arr);
        assertNotNull(arr[0]);
        ImprisonmentStatusType imprisonmentStatusType = arr[0];
        imprisonmentStatusType.setImprisonmentStatus(statusSet);
        imprisonmentStatusType.setImprisonmentSubStatus(subStatusSet);

        // set start/end date
        imprisonmentStatusType.setImprisonmentStatusEndDate(getDate(2013, 9, 9));

        ImprisonmentStatusType returnType = service.updateImprisonmentStatus(uc, supervisionId, imprisonmentStatusType);
        assertNotNull(returnType);
        assertNotNull(returnType.getImprisonmentStatus());

        sup = service.get(uc, supervisionId);
        assertNotNull(sup);
        List<ImprisonmentStatusType> isSet = sup.getImprisonmentStatus();
        for (ImprisonmentStatusType ist : isSet) {
            assertNotNull(ist);
        }

        //test defect 1542
        // create ImprisonmentStatus
        sup = service.get(uc, supervisionId);
        assertNotNull(sup);
        size = sup.getImprisonmentStatus().size();
        arr = sup.getImprisonmentStatus().toArray(arr);
        assertNotNull(arr);
        assertNotNull(arr[0]);
        imprisonmentStatusType = arr[0];
        statusSet = "CNTY";

        imprisonmentStatusType.setImprisonmentStatus(statusSet);
        imprisonmentStatusType.setImprisonmentSubStatus(null);

        // set start/end date
        imprisonmentStatusType.setImprisonmentStatusStartDate(getDate(2012, 9, 9));
        imprisonmentStatusType.setImprisonmentStatusEndDate(getDate(2013, 9, 9));

        returnType = service.updateImprisonmentStatus(uc, supervisionId, imprisonmentStatusType);
        assertNotNull(returnType);
        assertEquals(returnType.getImprisonmentStatus(), imprisonmentStatusType.getImprisonmentStatus());
        assertEquals(returnType.getImprisonmentSubStatus(), null);

        sup = service.get(uc, supervisionId);
        assertNotNull(sup);
        isSet = sup.getImprisonmentStatus();
        for (ImprisonmentStatusType ist : isSet) {
            assertNotNull(ist);

        }

    }

    @Test
    public void closeReopenSupervisions() {

        Long bbb = createSupervision(5L).getSupervisionIdentification();
        Long ccc = createSupervision(6L).getSupervisionIdentification();
        // close
        Date today = new Date();
        service.close(uc, bbb, today);
        SupervisionType returnType = service.close(uc, ccc, new Date());
        assertNotNull(returnType);

        assertEquals(returnType.getSupervisionStatusFlag(), Boolean.FALSE);
        assertNotNull(returnType.getSupervisionEndDate());
        // test ImprisonmentStatus
        List<ImprisonmentStatusType> imprisonmentStatusSet = returnType.getImprisonmentStatus();
        assertNotNull(imprisonmentStatusSet);
        assertEquals(returnType.getImprisonmentStatus().size(), 3);
        for (ImprisonmentStatusType is : imprisonmentStatusSet) {
            //assertTrue(compareDates(is.getImprisonmentStatusEndDate(), today));
        }

        // close again - should return exception
        verifyCloseNegative(ccc, new Date(), ArbutusRuntimeException.class);

        // reopen
        returnType = service.reopen(uc, ccc);
        assertNotNull(returnType);

        assertEquals(returnType.getSupervisionStatusFlag(), Boolean.TRUE);
        assertNull(returnType.getSupervisionEndDate());
        service.getSupervisionsForOffender(uc, 23L);
    }

    @Test
    public void get() {

        SupervisionType sup = createSupervision(personTest.createPerson());
        Long supervisionId = sup.getSupervisionIdentification();

        SupervisionType ret = service.get(uc, supervisionId);
        assertNotNull(ret);
    }

    @Test
    public void search() {

        SupervisionSearchType supervisionSearch = new SupervisionSearchType();
        supervisionSearch.setSupervisionDisplayID("*ID*");
        SupervisionsReturnType returnType = service.search(uc, null, supervisionSearch, 0L, 200L, null);
        assertNotNull(returnType);

        // imprisonment status
        supervisionSearch = new SupervisionSearchType();
        ImprisonmentStatusSearchType inStSearch = new ImprisonmentStatusSearchType();
        inStSearch.setImprisonmentStatus("BOP");
        supervisionSearch.setImprisonmentStatus(inStSearch);
        returnType = service.search(uc, null, supervisionSearch, 0L, 200L, null);
        assertNotNull(returnType);

        SupervisionSearchType searchType = new SupervisionSearchType();
        //searchType.setSupervisionStatusFlag(true);
        searchType.setSupervisionStatusFlag(false);
        searchType.setFacilityId(2L);

        SupervisionsReturnType type = service.search(uc, null, supervisionSearch, 0L, 200L, null);
        assertNotNull(type);

    }



    @Test
    public void testGetStamp() {

        SupervisionType sup = createSupervision(personTest.createPerson());
        Long supervisionId = sup.getSupervisionIdentification();

        StampType ret = service.getStamp(uc, supervisionId);
        assertNotNull(ret);
    }

    @Test
    public void getAll() {
        Set<Long> ret = service.getAll(null);
        assert (ret.size() >= 0);
    }

    @Test
    public void testGetCount() {

        Long ret = service.getCount(uc);
        assert (ret >= 0L);

    }

    @Test
    public void delete() {
        SupervisionType sup = createSupervision(personTest.createPerson());
        Long supervisionId = sup.getSupervisionIdentification();

        SupervisionType ret = service.get(uc, supervisionId);
        assertNotNull(ret);
        assertEquals(service.delete(uc, ret.getSupervisionIdentification()), Long.valueOf(1L));
    }

    @Test
    public void deleteAll() {
        Long l = service.deleteAll(uc);
        assertNotNull(l);
    }

    @SuppressWarnings("unused")
    @Test
    public void testWildCardBugs() {

        service.deleteAll(uc);

        //test bug 1430
        SupervisionType s1 = createSupervision("ABCD", 9999991L);
        SupervisionType s2 = createSupervision("AbCd", 9999992L);
        SupervisionType s3 = createSupervision("AbCcD", 9999993L);
        SupervisionType s4 = createSupervision("ABCD ", 9999994L);
        SupervisionType s5 = createSupervision("abcd", 9999995L);

        SupervisionSearchType searchType = new SupervisionSearchType();
        searchType.setSupervisionDisplayID("A*CD");

        SupervisionsReturnType rtn = service.search(uc, null, searchType, 0L, 200L, null);
        assertEquals(rtn.getTotalSize().longValue(), 5l);

        searchType = new SupervisionSearchType();
        searchType.setSupervisionDisplayID("A*CD*");

        rtn = service.search(uc, null, searchType, 0L, 200L, null);
        assertEquals(rtn.getTotalSize().longValue(), 5l);

        //test bug 1431
        Long personIdentityId = s1.getPersonIdentityId();
        searchType = new SupervisionSearchType();
        searchType.setPersonIdentityId(personIdentityId);

        rtn = service.search(uc, null, searchType, 0L, 200L, null);
        assertEquals(rtn.getTotalSize().longValue(), 1l);

    }

    private void verifyCloseNegative(Long supervisionId, Date endDate, Class<?> expectionClazz) {
        try {
            SupervisionType ret = service.close(uc, supervisionId, endDate);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyCreateNegative(SupervisionType dto, Class<?> expectionClazz) {
        try {
            SupervisionType ret = service.create(uc, dto);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private SupervisionType createSupervision(Long personId) {

        PersonIdentityTest piTest = new PersonIdentityTest();
        Long personIdentityId = piTest.createPersonIdentity();
        //Long personIdentityId = piTest.createPersonIdentity(personId);
        SupervisionType ret = createSupervisionByPersonIdentity(null, personIdentityId);
        assert (ret != null);
        return ret;
    }

    private SupervisionType createSupervision(String displayId, Long personId) {

        PersonIdentityTest piTest = new PersonIdentityTest();
        Long personIdentityId = piTest.createPersonIdentity();
        //Long personIdentityId = piTest.createPersonIdentity(personId);
        SupervisionType ret = createSupervisionByPersonIdentity(displayId, personIdentityId);
        assert (ret != null);
        return ret;
    }

    private SupervisionType createSupervisionByPersonIdentity(String displayId, Long personIdentityId) {
        if (displayId == null) {
            displayId = "ID" + personIdentityId;
        }
        SupervisionType ret = service.create(uc, getNewSupervisionType(displayId, personIdentityId));
        assert (ret != null);
        return ret;
    }

    private SupervisionType getNewSupervisionType(String displayId, Long personIdentityId) {

        int n = 1;

        SupervisionType supervision = new SupervisionType();

        supervision.setSupervisionDisplayID(displayId);
        supervision.setSupervisionStartDate(getDate(2012, 6, n));
        //supervision.setSupervisionEndDate(getDate(2014, 6, n*2));
        supervision.setDateOrientationProvided(getDate(2012, 7, n * 3));
        supervision.setPersonIdentityId(personIdentityId);
        supervision.setFacilityId(facilityIds.get(0));

        String statusSet = "BOP";
        String statusSet2 = "DOC";
        String subStatusSet = "FPV";
        String subStatusSet2 = "SPV";

        // create ImprisonmentStatus
        ImprisonmentStatusType imprisonmentStatus = new ImprisonmentStatusType();
        imprisonmentStatus.setImprisonmentStatus(statusSet);
        imprisonmentStatus.setImprisonmentStatusStartDate(getDate(2012, 6, n * 2));

        // create ImprisonmentStatus2
        ImprisonmentStatusType imprisonmentStatus2 = new ImprisonmentStatusType();
        imprisonmentStatus2.setImprisonmentStatus(statusSet2);
        imprisonmentStatus2.setImprisonmentSubStatus(subStatusSet);
        imprisonmentStatus2.setImprisonmentStatusStartDate(getDate(2012, 6, n));

        // create ImprisonmentStatus3
        ImprisonmentStatusType imprisonmentStatus3 = new ImprisonmentStatusType();
        imprisonmentStatus3.setImprisonmentStatus(statusSet2);
        imprisonmentStatus3.setImprisonmentSubStatus(subStatusSet2);
        imprisonmentStatus3.setImprisonmentStatusStartDate(getDate(2012, 6, n * 2));

        // set ImprisonmentStatusEntity
        supervision.getImprisonmentStatus().add(imprisonmentStatus);
        supervision.getImprisonmentStatus().add(imprisonmentStatus2);
        supervision.getImprisonmentStatus().add(imprisonmentStatus3);

        return supervision;
    }

    private boolean compareDates(Date imprisonmentStatusEndDate, Date today) {
        if (imprisonmentStatusEndDate.equals(today)) {
			return true;
		}

        Date todayPlus3Sec = new Date(today.getTime() + 3 * 1000);

        if (imprisonmentStatusEndDate.after(today) && imprisonmentStatusEndDate.before(todayPlus3Sec)) {
			return true;
		}

        return false;
    }

    private Date getDate(int year, int month, int date) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, date);
        return cal.getTime();
    }

    @AfterClass
    public void afterClass() {
        service.deleteAll(uc);

    }

}
