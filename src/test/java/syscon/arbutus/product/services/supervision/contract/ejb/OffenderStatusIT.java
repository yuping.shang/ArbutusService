package syscon.arbutus.product.services.supervision.contract.ejb;

import org.apache.log4j.Logger;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.supervision.contract.dto.OffenderStatus;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

/**
 * Created by jliang on 1/26/16.
 */
@ModuleConfig
public class OffenderStatusIT extends BaseIT {

    private static Logger log = Logger.getLogger(OffenderStatusIT.class);

    private SupervisionService service;

    @AfterMethod
    public void afterMethod() {
    }

    @BeforeClass
    public void beforeClass() {

        log.info("beforeClass Begin - JNDI lookup");
        service = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        uc = super.initUserContext();

        SecurityClient client;
        try {
            client = SecurityClientFactory.getSecurityClient();
            client.setSimple("devtest", "devtest"); // nice regular service user
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }

        init();
    }

    @AfterClass
    public void afterTest() {

    }

    public void init() {

    }

    @Test
    public void getOffenderStatus() {

        OffenderStatus offenderStatus = service.getOffenderStatusByPersonId(uc, 2L);
        assert (offenderStatus != null);

        offenderStatus = service.getOffenderStatusBySupervisionId(uc, 4L);
        assert (offenderStatus != null);
    }
}
