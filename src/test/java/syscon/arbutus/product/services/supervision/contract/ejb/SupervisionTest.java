package syscon.arbutus.product.services.supervision.contract.ejb;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.PersonIdentityTest;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.supervision.contract.dto.AdmitOffenderType;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionSearchType;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionsReturnType;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

import static org.testng.Assert.assertTrue;

/**
 * Created by chuan.pan on 27/07/2015.
 */
public class SupervisionTest extends BaseIT {
    static Logger logger = LoggerFactory.getLogger(SupervisionTest.class);
    private SupervisionService supervisionService;
    private PersonIdentityTest personIdentityTest;
    private syscon.arbutus.product.services.common.SupervisionTest supervisionTest;

    @BeforeClass
    public void setup() {
        supervisionService = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        personIdentityTest = new PersonIdentityTest();
        supervisionTest = new syscon.arbutus.product.services.common.SupervisionTest();
    }

    /**
     * Test get the last closed supervision
     * Steps:
     * 1. Create an offender
     * 2. Repeat doing admit then release the offender two times
     * 3. Admit the offender again
     * Verification:
     * The offender should have 3 supervision records (2 inactive, 1 active)
     * The last closed super vision should be the second one of the supervision records.
     */
    @Test
    public void testGetLastClosedSupervision() {
        PersonIdentityType personIdentityType = personIdentityTest.createPersonIdentityType();
        Long personIdentityId = personIdentityType.getPersonIdentityId();
        String toggleSupervision = AdmitOffenderType.SupervisionAction.NONE.value();

        AdmitOffenderType admitOffender = createAdmitOffenderType(personIdentityId, toggleSupervision, "NEW");



        // 1st admit and release offender
        SupervisionType supervisionType = supervisionService.admitOffender(uc, admitOffender);
        supervisionService.close(uc, supervisionType.getSupervisionIdentification(), new Date());

        // 2nd admit and release offender
        supervisionType = supervisionService.admitOffender(uc, admitOffender);
        supervisionService.close(uc, supervisionType.getSupervisionIdentification(), new Date());

        // 3rd admit but don't release this time
        supervisionType = supervisionService.admitOffender(uc, admitOffender);

        SupervisionSearchType supervisionSearch = new SupervisionSearchType();
        supervisionSearch.setSupervisionDisplayID(supervisionType.getSupervisionDisplayID());
        supervisionSearch.setSupervisionStatusFlag(false);

        SupervisionsReturnType supervisionsReturnType = supervisionService.search(uc, null, supervisionSearch, 0l, 1l, "supervisionEndDate.desc");

        SupervisionType the2ndSupervision = supervisionsReturnType.getSupervisions().iterator().next();
        SupervisionType theLastClosedSupervision = supervisionService.getLastClosedSupervision(uc, personIdentityId);
        // Verification: The offender should have 3 supervision records (2 inactive, 1 active)
        // The last closed super vision should be the second one of the supervision records.
        assertTrue(the2ndSupervision.getSupervisionIdentification().equals(theLastClosedSupervision.getSupervisionIdentification()), "The last closed supervision should be equal to the 2nd supervision");
    }

    private AdmitOffenderType createAdmitOffenderType(Long personIdentityId, String toggleSupervision, String movementReason) {

        AdmitOffenderType admitOffender = new AdmitOffenderType();
        admitOffender.setMovementDate(new Date());
        admitOffender.setMovementTime(new Date());
        admitOffender.setFromFacility(179L);
        admitOffender.setToFacility(1L);
        admitOffender.setMovementReason(movementReason);
        admitOffender.setCommentText("New Suppervision");
        admitOffender.setPersonIdentityId(personIdentityId);
        admitOffender.setStaffId(9L);
        admitOffender.setToggleSupervision(toggleSupervision);
        SupervisionType supervisionType = new SupervisionType();
        String supervisionDisplayId = supervisionTest.getSupervisionDisplayNumber(null);
        supervisionType.setSupervisionDisplayID(supervisionDisplayId);
        admitOffender.setSupervisionType(supervisionType);

        return admitOffender;
    }
}
