package syscon.arbutus.product.services.referencedata.contract.ejb.utils;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.DescriptionType;
import syscon.arbutus.product.services.core.contract.dto.ReferenceCodeType;

/**
 * Factory of ReferenceCodes
 *
 * @author wmadruga
 */
public class ReferenceCodeFactory {

    /**
     * Creates a Reference Code with given parameters.
     *
     * @param set
     * @param code
     * @param language
     * @param description
     * @param sequence
     * @param createDate  optional (today if null)
     * @param activeDate  optional (today if null)
     * @return ReferenceCodeType
     */
    public static ReferenceCodeType createReferenceCode(String set, String code, String language, String description, Long sequence, Date createDate, Date activeDate) {

        ReferenceCodeType referenceCode = new ReferenceCodeType();

        referenceCode.setSet(set);
        referenceCode.setCode(code);
        referenceCode.setDescriptions(createDescription(language, description));
        referenceCode.setSequence(sequence);
        referenceCode.setCreateDate((createDate == null) ? new Date() : createDate);
        referenceCode.setActiveDate((activeDate == null) ? new Date() : activeDate);

        return referenceCode;

    }

    /**
     * Creates a Description
     *
     * @param language
     * @param text
     * @return List of DescriptionType
     */
    private static Set<DescriptionType> createDescription(String language, String text) {
        Set<DescriptionType> descriptions = new HashSet<DescriptionType>();
        DescriptionType description = new DescriptionType(language, text);
        descriptions.add(description);
        return descriptions;
    }
}