package syscon.arbutus.product.services.referencedata.contract.ejb.utils;

import syscon.arbutus.product.services.core.contract.dto.LinkCodeType;

/**
 * Factory of Reference Code Links
 *
 * @author wmadruga
 */
public class ReferenceCodeLinkFactory {

    /**
     * Creates an active LinkType with given ReferenceCodeTypes
     *
     * @param code1 ReferenceCodeType
     * @param code2 ReferenceCodeType
     * @return LinkType
     */
    public static LinkCodeType createLink(String code1, String code2) {

        LinkCodeType link = new LinkCodeType();
        link.setReferenceCode(code1);
        link.setLinkedReferenceCode(code2);

        return link;
    }

}
