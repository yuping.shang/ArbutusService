package syscon.arbutus.product.services.referencedata.contract.ejb;

import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.LinkCodeType;
import syscon.arbutus.product.services.core.contract.dto.ReferenceCodeType;
import syscon.arbutus.product.services.referencedata.contract.ejb.utils.ReferenceCodeFactory;
import syscon.arbutus.product.services.referencedata.contract.ejb.utils.ReferenceCodeLinkFactory;
import syscon.arbutus.product.services.referencedata.contract.interfaces.ReferenceDataService;

import static org.testng.Assert.assertNotNull;

/**
 * Tests for ReferenceCode and Link Helpers
 *
 * @author wmadruga
 */
public class ReferenceCodeTest extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(ReferenceCodeIT.class);
    ReferenceCodeType referenceCodeType;
    private int count = 0;
    private Set<ReferenceCodeType> resReferenceCode;
    private Iterator<ReferenceCodeType> resultIterator;
    private ReferenceDataService service;

    @BeforeClass
    public void beforeClass() {
        lookupJNDILogin();
    }

    //for release
    private void lookupJNDILogin() {

        log.info("beforeClass Begin - JNDI lookup");
        service = (ReferenceDataService) JNDILookUp(this.getClass(), ReferenceDataService.class);
        uc = super.initUserContext();
    }

    @Test
    public void lookupJNDI() {
        assertNotNull(service);
    }

    /**
     * Creates ReferenceCodes and Links.
     */
    @Test(enabled = true)
    public void createRefCodesAndLinks() {

        // CODES

        ReferenceCodeType vancouver = shouldCreateGivenReferenceCode("CITY", "VANCOUVER", "EN", "Vancouver", 10L, null, null);

        ReferenceCodeType richmond = shouldCreateGivenReferenceCode("CITY", "RICHMOND", "EN", "Richmond", 10L, null, null);

        ReferenceCodeType coquitlam = shouldCreateGivenReferenceCode("CITY", "COQUITLAM", "EN", "Coquitlam", 10L, null, null);

        ReferenceCodeType canada = shouldCreateGivenReferenceCode("COUNTRY", "CANADA", "EN", "Canada", 10L, null, null);

        ReferenceCodeType bc = shouldCreateGivenReferenceCode("PROVINCE", "BC", "EN", "British Columbia", 10L, null, null);

        ReferenceCodeType ab = shouldCreateGivenReferenceCode("PROVINCE", "AB", "EN", "Alberta", 10L, null, null);

        // LINKS

        shouldCreateLink(ReferenceCodeLinkFactory.createLink(vancouver.getCode(), canada.getCode()));

        shouldCreateLink(ReferenceCodeLinkFactory.createLink(richmond.getCode(), canada.getCode()));

        shouldCreateLink(ReferenceCodeLinkFactory.createLink(coquitlam.getCode(), canada.getCode()));

        shouldCreateLink(ReferenceCodeLinkFactory.createLink(bc.getCode(), canada.getCode()));

        shouldCreateLink(ReferenceCodeLinkFactory.createLink(ab.getCode(), canada.getCode()));

        shouldCreateLink(ReferenceCodeLinkFactory.createLink(bc.getCode(), vancouver.getCode()));

        shouldCreateLink(ReferenceCodeLinkFactory.createLink(bc.getCode(), richmond.getCode()));

        shouldCreateLink(ReferenceCodeLinkFactory.createLink(bc.getCode(), coquitlam.getCode()));
    }

    /**
     * Checks if it is possible to retrieve reference codes filtered, given a set and a filter.
     * Checks if results are alphabetically sorted.
     * Checks if there is precedence in sorting by sequence prior to description.
     * More info on : http://wiki.syscon.ca:8080/display/arbutus/Reference+code+method+in+template
     */
    @Test(dependsOnMethods = { "createRefCodesAndLinks" }, enabled = true) //Depends on CoreLib interface update to List.
    public void getFilteredReferenceCodesSetForLink() {

        shouldRetrieveSortedCitiesGivenTheProvince();

        shouldRetrieveSortedProvincesGivenTheCountry();

        shouldRetrieveSortedCitiesGivenTheCountry();

    }

    /**
     * Should create a Reference Code with given parameters
     *
     * @param set         String
     * @param code        String
     * @param language    String
     * @param description String
     * @param sequence    Long
     * @param createDate  Date
     * @param activeDate  Date
     * @return ReferenceCodeReturnType
     */
    private ReferenceCodeType shouldCreateGivenReferenceCode(String set, String code, String language, String description, Long sequence, Date createDate,
            Date activeDate) {

        referenceCodeType = ReferenceCodeFactory.createReferenceCode(set, code, language, description, sequence, createDate, activeDate);
        ReferenceCodeType ret = service.createReferenceCode(uc, referenceCodeType);
        Assert.assertTrue(ret != null);
        return ret;
    }

    /**
     * Should create the specified Link.
     *
     * @param link LinkType
     */
    private void shouldCreateLink(LinkCodeType link) {
        LinkCodeType ret = service.createLink(uc, link);
        Assert.assertTrue(ret != null);
    }

    /**
     * Given the COUNTRY and a CITY filter, should retrieve all linked cities reference codes.
     */
    private void shouldRetrieveSortedCitiesGivenTheCountry() {
        count = 0;
        //resReferenceCode = LinkHandler.getFilteredReferenceCodesForLink(uc, session, new CodeType("COUNTRY", "CANADA"), "CITY", true, "EN");
        assert (resReferenceCode.size() > 0);

        resultIterator = resReferenceCode.iterator();
        while (resultIterator.hasNext()) {
            count = count++;
            ReferenceCodeType result = resultIterator.next();
            //Alphabetically
            if (count == 1) {
                assert (result.getCode().contentEquals("COQUITLAM"));
            } else if (count == 2) {
                assert (result.getCode().contentEquals("RICHMOND"));
            } else if (count == 3) {
                assert (result.getCode().contentEquals("VANCOUVER"));
            }
        }
    }

    /**
     * Given the COUNTRY and a PROVINCE filter, should retrieve all linked provinces reference codes.
     */
    private void shouldRetrieveSortedProvincesGivenTheCountry() {
        count = 0;
        //resReferenceCode = LinkHandler.getFilteredReferenceCodesForLink(uc, session, new CodeType("COUNTRY", "CANADA"), "PROVINCE", true, "EN");
        assert (resReferenceCode.size() > 0);

        resultIterator = resReferenceCode.iterator();
        while (resultIterator.hasNext()) {
            count = count++;
            ReferenceCodeType result = resultIterator.next();
            //Alphabetically
            if (count == 1) {
                assert (result.getCode().contentEquals("AB"));
            } else if (count == 2) {
                assert (result.getCode().contentEquals("BC"));
            }
        }
    }

    /**
     * Given the PROVINCE and a CITY filter, should retrieve all linked cities reference codes.
     */
    private void shouldRetrieveSortedCitiesGivenTheProvince() {
        //resReferenceCode = LinkHandler.getFilteredReferenceCodesForLink(uc, session, new CodeType("PROVINCE", "BC"), "CITY", true, "EN");
        assert (resReferenceCode.size() > 0);

        resultIterator = resReferenceCode.iterator();
        while (resultIterator.hasNext()) {
            count = count++;
            ReferenceCodeType result = resultIterator.next();
            //Alphabetically
            if (count == 1) {
                assert (result.getCode().contentEquals("COQUITLAM"));
            } else if (count == 2) {
                assert (result.getCode().contentEquals("RICHMOND"));
            } else if (count == 3) {
                assert (result.getCode().contentEquals("VANCOUVER"));
            }
        }
    }

}