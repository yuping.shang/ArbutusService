package syscon.arbutus.product.services.referencedata.contract.ejb;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.*;
import syscon.arbutus.product.services.core.exception.DataExistException;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.movement.contract.dto.ReferenceSet;
import syscon.arbutus.product.services.referencedata.contract.dto.LinkCodeDescriptionType;
import syscon.arbutus.product.services.referencedata.contract.interfaces.ReferenceDataService;

import static org.testng.Assert.assertNotNull;

public class ReferenceCodeIT extends BaseIT {

    private static final String EN_LANG = "EN";
    private static final String TRNIJ = "TRNIJ";
    private static Logger log = LoggerFactory.getLogger(ReferenceCodeIT.class);
    private ReferenceDataService service;

    @BeforeClass
    public void beforeClass() {
        lookupJNDILogin();
    }

    //for release
    private void lookupJNDILogin() {

        log.info("beforeClass Begin - JNDI lookup");
        service = (ReferenceDataService) JNDILookUp(this.getClass(), ReferenceDataService.class);
        uc = super.initUserContext();
    }

    @Test
    public void lookupJNDI() {
        assertNotNull(service);
    }

    @Test
    public void getReferenceCodesForSet() {
        ReferenceCodesRetrieveType referenceCodesRetrieve = new ReferenceCodesRetrieveType("MOVEMENTREASON", null, "EN", true);
        List<ReferenceCodeType> codes = service.getSortedReferenceCodes(uc, referenceCodesRetrieve);
        Assert.assertTrue(codes.size() > 0);
    }

    @Test
    public void getReasonsForTRNIJ() {

        CodeType type = new CodeType(ReferenceSet.MOVEMENT_TYPE.value(), TRNIJ);
        List<ReferenceCodeType> referenceCodeTypes = null;
        referenceCodeTypes = service.getReferenceCodesForLinkReferenceSet(uc, type, ReferenceSet.MOVEMENT_REASON.value(), true, "EN");
        assertNotNull(referenceCodeTypes);
        Assert.assertTrue(referenceCodeTypes.size() == 3L);
        for (ReferenceCodeType code : referenceCodeTypes) {
            Assert.assertTrue(code.getCode().contentEquals("ADMN") ||
                    code.getCode().contentEquals("MED") ||
                    code.getCode().contentEquals("TRNO"));
        }
    }

    @Test(enabled = true)
    public void LinkSetTest() {
        try {
            LinkCodeType link = new LinkCodeType("", "", "", "");
            service.createLink(uc, link);
        } catch (InvalidInputException iie) {
            Assert.assertTrue(true);
        }

    }

    @Test(enabled = true)
    public void createDuplicateLink() {
        try {
            LinkCodeType lnkCode = new LinkCodeType();
            lnkCode.setReferenceSet("FACILITYTYPE");
            lnkCode.setReferenceCode("CIT");
            lnkCode.setLinkedReferenceSet("FACILITYCATEGORY");
            lnkCode.setLinkedReferenceCode("COURT");

            service.createLink(uc, lnkCode);
        } catch (DataExistException dee) {
            Assert.assertTrue(true);
        }

    }

    @Test(enabled = true)
    public void linksWithCodeDescriptionTest() {
        List<LinkCodeDescriptionType> linksList = service.getAllLinksBy(uc, "CASEACTIVITYTYPE", EN_LANG);
        for (LinkCodeDescriptionType type : linksList) {
            assertNotNull(type.getCodeDescriptions());
            assertNotNull(type.getLinkedCodeDescriptions());
        }
    }

    @Test(enabled = true)
    public void sortedCodesTest() {

        List<ReferenceCodeType> codesList = service.getAllReferenceCodes(uc);

        for (int i = 1; i < codesList.size(); i++) {
            ReferenceCodeType previous = codesList.get(i - 1);
            ReferenceCodeType actual = codesList.get(i);

            if (actual.getSet().compareToIgnoreCase(previous.getSet()) == 0) {
                if (actual.getSequence() == previous.getSequence()) {
                    if (actual.getDescriptions() != null && previous.getDescriptions() != null) {
                        Assert.assertTrue(((DescriptionType) actual.getDescriptions().toArray()[0]).getDescription().compareToIgnoreCase(
                                ((DescriptionType) previous.getDescriptions().toArray()[0]).getDescription()) >= 0);
                    }
                } else {
                    Assert.assertTrue(actual.getSequence() > previous.getSequence());
                }
            } else {
                Assert.assertTrue(actual.getSet().compareToIgnoreCase(previous.getSet()) > 0);

            }
        }
    }

    @Test(enabled = true)
    public void sortedLinksTest() {

        List<LinkCodeDescriptionType> linksList = service.getAllLinksBy(uc, "CASEACTIVITYTYPE", EN_LANG);

        for (int i = 1; i < linksList.size(); i++) {
            LinkCodeDescriptionType previous = linksList.get(i - 1);
            LinkCodeDescriptionType actual = linksList.get(i);

            //			System.out.println(previous.getReferenceCode());
            //			System.out.println(actual.getReferenceCode());

            if (actual.getReferenceSet().compareToIgnoreCase(previous.getReferenceSet()) == 0) {
                // jump special cases that are sorted differently by database
                if (!actual.getReferenceCode().contains("_") && !previous.getReferenceCode().contains("_")) {
                    Assert.assertTrue(actual.getReferenceCode().compareToIgnoreCase(previous.getReferenceCode()) >= 0);
                }
            } else {
                Assert.assertTrue(actual.getReferenceSet().compareToIgnoreCase(previous.getReferenceSet()) > 0);
            }
        }
    }

    @Test(enabled = true)
    public void createCodeTest() {
        shouldCreateNewRandomReferenceCode(generateDescription(EN_LANG));
    }

    @Test(enabled = true)
    public void editableCodeTest() {

        shouldUpdateAllFields(generateDescription(EN_LANG));
        shouldUpdateSequenceAndDescriptionOnly(generateDescription(EN_LANG));
        shouldAddAnotherDescription(generateDescription("FR"));
        shouldDeactivate(generateDescription(EN_LANG));
        shouldActivate(generateDescription(EN_LANG));
    }

    @Test(enabled = true)
    public void handlingDefaultCodeTest() {

        ReferenceCodesRetrieveType retriever = new ReferenceCodesRetrieveType("ACTIVITYCATEGORY", "MOVEMENT", EN_LANG, false);
        List<ReferenceCodeType> rets = service.getReferenceCodes(uc, retriever);

        for (ReferenceCodeType codeType : rets) {

            ReferenceCodeType ret;
            ret = createOrUpdateReferenceCodeTest("UPDATE", codeType.getSet(), codeType.getCode(), codeType.getSequence(), codeType.getDescriptions(),
                    codeType.getActiveDate(), codeType.getDeactiveDate(), true, codeType.isEditable());

            Assert.assertTrue(ret != null);
            Assert.assertTrue(ret.isDefaultVal());

        }
    }

    @Test(enabled = true)
    public void verifyReferenceCodes() {
        Set<CodeType> codes = new HashSet<CodeType>();

        codes.add(new CodeType("SEX", "M"));
        codes.add(new CodeType("WEEKDAY", "WED"));
        codes.add(new CodeType("LANGUAGE", "FR"));
        codes.add(new CodeType("NAMESUFFIX", "I"));
        codes.add(new CodeType("PERSONROLE", "MOTHER"));
        codes.add(new CodeType("MONTH", "FEB"));
        codes.add(new CodeType("IDENTIFIER", "SID"));
        codes.add(new CodeType("STAFFCATEGORY", "CON"));
        codes.add(new CodeType("CAUTIONCODE", "90"));
        codes.add(new CodeType("FACILITYTYPE", "MAG"));

        codes.add(new CodeType("SEX", "M"));
        codes.add(new CodeType("WEEKDAY", "WED"));
        codes.add(new CodeType("LANGUAGE", "FR"));
        codes.add(new CodeType("NAMESUFFIX", "I"));
        codes.add(new CodeType("PERSONROLE", "MOTHER"));
        codes.add(new CodeType("MONTH", "FEB"));
        codes.add(new CodeType("IDENTIFIER", "SID"));
        codes.add(new CodeType("STAFFCATEGORY", "CON"));
        codes.add(new CodeType("CAUTIONCODE", "90"));
        codes.add(new CodeType("FACILITYTYPE", "MAG"));

        codes.add(new CodeType("SEX", "M"));
        codes.add(new CodeType("WEEKDAY", "WED"));
        codes.add(new CodeType("LANGUAGE", "FR"));
        codes.add(new CodeType("NAMESUFFIX", "I"));
        codes.add(new CodeType("PERSONROLE", "MOTHER"));
        codes.add(new CodeType("MONTH", "FEB"));
        codes.add(new CodeType("IDENTIFIER", "SID"));
        codes.add(new CodeType("STAFFCATEGORY", "CON"));
        codes.add(new CodeType("CAUTIONCODE", "90"));
        codes.add(new CodeType("FACILITYTYPE", "MAG"));

        codes.add(new CodeType("SEX", "M"));
        codes.add(new CodeType("WEEKDAY", "WED"));
        codes.add(new CodeType("LANGUAGE", "FR"));
        codes.add(new CodeType("NAMESUFFIX", "I"));
        codes.add(new CodeType("PERSONROLE", "MOTHER"));
        codes.add(new CodeType("MONTH", "FEB"));
        codes.add(new CodeType("IDENTIFIER", "SID"));
        codes.add(new CodeType("STAFFCATEGORY", "CON"));
        codes.add(new CodeType("CAUTIONCODE", "90"));
        codes.add(new CodeType("FACILITYTYPE", "MAG"));

        codes.add(new CodeType("SEX", "M"));
        codes.add(new CodeType("WEEKDAY", "WED"));
        codes.add(new CodeType("LANGUAGE", "FR"));
        codes.add(new CodeType("NAMESUFFIX", "I"));
        codes.add(new CodeType("PERSONROLE", "MOTHER"));
        codes.add(new CodeType("MONTH", "FEB"));
        codes.add(new CodeType("IDENTIFIER", "SID"));
        codes.add(new CodeType("STAFFCATEGORY", "CON"));
        codes.add(new CodeType("CAUTIONCODE", "90"));
        codes.add(new CodeType("FACILITYTYPE", "MAG"));
        service.isActiveReferenceCode(codes);
        service.doesExistReferenceCode(codes);
    }

    @Test(enabled = true)
    public void getReferenceCodes() {
        List<ReferenceCodeType> ret = null;
        //ReferenceCodeRetrieveType is null, this will fail
        try {
            ret = service.getReferenceCodes(uc, null);
        } catch (InvalidInputException e) {
            log.info(e.getMessage());
        }
        assert (ret == null);

        //language is null, this will fail
        ReferenceCodesRetrieveType referenceCodesRetrieve = new ReferenceCodesRetrieveType("LANGUAGE", null, null, null);
        try {
            ret = service.getReferenceCodes(uc, null);
        } catch (InvalidInputException e) {
            log.info(e.getMessage());
        }
        assert (ret == null);

        //no record found, this will success, and return empty set
        referenceCodesRetrieve = new ReferenceCodesRetrieveType("WrongSet_", null, "en", Boolean.FALSE);
        try {
            ret = service.getReferenceCodes(uc, referenceCodesRetrieve);
        } catch (DataNotExistException e) {
            log.info(e.getMessage());
        }

        assert (ret.size() == 0);

    }

    @Test(enabled = true)
    public void getAllReferenceCodes() {

        List<ReferenceCodeType> ret = service.getAllReferenceCodes(uc);
        assert (ret != null);
        assert (!ret.isEmpty());

    }

    private Date createPresentDate() {
        return Calendar.getInstance().getTime();
    }

    private Date createFutureDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, day);
        Date futureDate = cal.getTime();
        return futureDate;
    }

    private Date createPastDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, -day);
        Date pastDate = cal.getTime();
        return pastDate;
    }

    /**
     * Create or Update a Reference code based on given attributes
     */
    private ReferenceCodeType createOrUpdateReferenceCodeTest(String operation, String set, String code, Long sequenceNumber, Set<DescriptionType> descriptions,
            Date activate, Date deactivate, Boolean defaultVal, Boolean editable) {
        ReferenceCodeType ret;
        ReferenceCodeType referenceCode = generateReferenceCode(operation, set, code, sequenceNumber, descriptions, activate, deactivate, defaultVal, editable);

        if (operation.contentEquals("CREATE")) {
			ret = service.createReferenceCode(uc, referenceCode);
		} else {
			ret = service.updateReferenceCode(uc, referenceCode);
		}

        //		Assert.assertEquals(ret.getReturnCode(), ReturnCode.Success.returnCode(),
        //				String.format("[Return Code Error %s with referenceCode %s]", ret.getReturnCode(), referenceCode));

        return ret;
    }

    /**
     * Generates ReferenceCode with given values.
     */
    private ReferenceCodeType generateReferenceCode(String operation, String set, String code, Long sequenceNumber, Set<DescriptionType> descriptions, Date activate,
            Date deactivate, Boolean defaultVal, Boolean editable) {

        ReferenceCodeType referenceCode = new ReferenceCodeType();

        if (operation.contentEquals("CREATE")) {
            referenceCode.setCreateDate(createPresentDate());
            referenceCode.setEditable(true);
        } else {
            if (editable != null) {
				referenceCode.setEditable(editable);
			}
        }

        if (defaultVal != null) {
			referenceCode.setDefaultVal(defaultVal);
		}

        referenceCode.setSet(set);
        referenceCode.setCode(code);
        referenceCode.setDeactiveDate(deactivate);
        referenceCode.setSequence(sequenceNumber);
        referenceCode.setActiveDate(activate);
        referenceCode.setDescriptions(descriptions);

        return referenceCode;
    }

    /**
     * Returns a randomized Description from a list of descriptions.
     */
    private Set<DescriptionType> generateDescription(String language) {
        Set<DescriptionType> ret = new HashSet<DescriptionType>();
        String description = generateRandomString();
        ret.add(new DescriptionType(language, description));
        return ret;
    }

    /**
     * This Reference Code is loaded by MetadataTool
     * should deactivate because this is an editable ref set and code
     */
    private void shouldDeactivate(Set<DescriptionType> description) {
        Date deactivateDate = createFutureDate(10);

        //Since Weekday/MON is not editable, this test is not valid anymore, so change this test case with another code.
        //ReferenceCodesRetrieveType retriever = new ReferenceCodesRetrieveType("WEEKDAY", "MON", EN_LANG, true);
        ReferenceCodesRetrieveType retriever = new ReferenceCodesRetrieveType("CAUTIONCODE", "70", EN_LANG, true);
        List<ReferenceCodeType> rets = service.getReferenceCodes(uc, retriever);

        for (ReferenceCodeType codeType : rets) {
            ReferenceCodeType ret = createOrUpdateReferenceCodeTest("UPDATE", codeType.getSet(), codeType.getCode(), codeType.getSequence(), codeType.getDescriptions(),
                    codeType.getActiveDate(), deactivateDate, codeType.isDefaultVal(), codeType.isEditable());

            Assert.assertTrue(ret.getDeactiveDate() != null, String.format("[ERROR - Reference Code not deactivated - %s]", deactivateDate));
        }
    }

    /**
     * This Reference Code is loaded by MetadataTool
     * should deactivate because this is an editable ref set and code
     */
    private void shouldActivate(Set<DescriptionType> description) {
        Date activateDate = createPresentDate();
        Date deactivateDate = null;

        //Since Weekday/MON is not editable, this test is not valid anymore, so change this test case with another code.
        //ReferenceCodesRetrieveType retriever = new ReferenceCodesRetrieveType("WEEKDAY", "MON", EN_LANG, true);
        ReferenceCodesRetrieveType retriever = new ReferenceCodesRetrieveType("CAUTIONCODE", "70", EN_LANG, true);
        List<ReferenceCodeType> rets = service.getReferenceCodes(uc, retriever);

        for (ReferenceCodeType codeType : rets) {
            ReferenceCodeType ret = createOrUpdateReferenceCodeTest("UPDATE", codeType.getSet(), codeType.getCode(), codeType.getSequence(), codeType.getDescriptions(),
                    activateDate, deactivateDate, codeType.isDefaultVal(), codeType.isEditable());

            Assert.assertTrue(ret.getActiveDate() != null, String.format("[ERROR - Reference Code not activated - %s]", activateDate));
            Assert.assertTrue(ret.getDeactiveDate() == null, String.format("[ERROR - Reference Code not activated by nulling deactivate date - %s]", deactivateDate));
        }
    }

    /**
     * This Reference Code is loaded by MetadataTool
     * should update all fields because this is an editable reference set and code.
     */
    private void shouldUpdateAllFields(Set<DescriptionType> description) {

        ReferenceCodesRetrieveType retriever = new ReferenceCodesRetrieveType("WEEKDAY", "MON", EN_LANG, false);
        List<ReferenceCodeType> rets = service.getReferenceCodes(uc, retriever);
        Date activated = createFutureDate(2);
        Date deactivated = createFutureDate(5);

        for (ReferenceCodeType codeType : rets) {

            ReferenceCodeType ret = createOrUpdateReferenceCodeTest("UPDATE", codeType.getSet(), codeType.getCode(), 55L, description, activated, deactivated,
                    codeType.isDefaultVal(), codeType.isEditable());

            Assert.assertTrue(ret.getActiveDate().equals(activated), String.format("[ERROR - ActivateDate not updated - %s]", activated));

            Assert.assertTrue(ret.getDeactiveDate().equals(deactivated), String.format("[ERROR - DeactivateDate not updated - %s]", deactivated));

            Assert.assertTrue(ret.getSequence().equals(55L), String.format("[ERROR - Sequence not updated - 55]"));

            Assert.assertTrue(ret.getDescriptions().contains(description), String.format("[ERROR - Description not updated - %s]", description));

        }
    }

    /**
     * This Reference Code is loaded by MetadataTool
     * should change description and sequence number without editable access
     */
    private void shouldUpdateSequenceAndDescriptionOnly(Set<DescriptionType> description) {
        ReferenceCodesRetrieveType retriever = new ReferenceCodesRetrieveType("ACTIVITYCATEGORY", "MOVEMENT", EN_LANG, false);
        List<ReferenceCodeType> rets = service.getReferenceCodes(uc, retriever);

        for (ReferenceCodeType codeType : rets) {
            ReferenceCodeType ret = createOrUpdateReferenceCodeTest("UPDATE", codeType.getSet(), codeType.getCode(), 44L, description, null, null, null, null);

            Assert.assertTrue(ret.getSequence().equals(44L), String.format("[ERROR - Sequence not updated - 44]"));

            Assert.assertTrue(ret.getDescriptions().contains(description), String.format("[ERROR - Description not updated - %s]", description));

        }
    }

    /**
     * This Reference Code is loaded by MetadataTool
     * should add another description without editable access
     */
    private void shouldAddAnotherDescription(Set<DescriptionType> description) {

        ReferenceCodesRetrieveType retriever = new ReferenceCodesRetrieveType("ACTIVITYCATEGORY", "MOVEMENT", EN_LANG, false);
        List<ReferenceCodeType> rets = service.getReferenceCodes(uc, retriever);

        for (ReferenceCodeType codeType : rets) {
            ReferenceCodeType ret = createOrUpdateReferenceCodeTest("UPDATE", codeType.getSet(), codeType.getCode(), 99L, description, null, null, null, null);

            Assert.assertTrue(ret.getDescriptions().contains(description), String.format("[ERROR - Description not updated - %s]", description));
        }
    }

    /**
     * This will create a random reference code under NEWSET domain.
     */
    private void shouldCreateNewRandomReferenceCode(Set<DescriptionType> description) {
        Boolean found = false;
        String newCode = generateRandomString();

        ReferenceCodeType ret = createOrUpdateReferenceCodeTest("CREATE", "NEWSET", newCode, 99L, description, createPresentDate(), null, true, true);
        Assert.assertTrue(ret != null);

        ReferenceCodesRetrieveType retriever = new ReferenceCodesRetrieveType("NEWSET", newCode, EN_LANG, true);
        List<ReferenceCodeType> rets = service.getReferenceCodes(uc, retriever);

        for (ReferenceCodeType refCode : rets) {
            if (refCode.getCode().contentEquals(ret.getCode())) {
                found = true;
            }
        }
        Assert.assertTrue(found);
    }

    /**
     * Generates a random string
     */
    private String generateRandomString() {
        return Long.toHexString(Double.doubleToLongBits(Math.random()));
    }

}