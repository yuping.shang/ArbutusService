package syscon.arbutus.product.services.housingsuitability.contract.dto;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingsuitability.OffenderHousingAssignmentInOutType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class OffenderHousingAssignmentInOutTypeTest {

    private Long offenderReference = 1L;
    private Long assignedLocationReference = 1L;

    @Test
    public void isValid() {

        OffenderHousingAssignmentInOutType data = new OffenderHousingAssignmentInOutType();
        assert (validate(data) == false);

        data = new OffenderHousingAssignmentInOutType(null, null, null, null, null);
        assert (validate(data) == false);

        //offenderReference is null
        data = new OffenderHousingAssignmentInOutType(null, assignedLocationReference, null, null, null);
        assert (validate(data) == false);

        //assignedLocationReference is null
        data = new OffenderHousingAssignmentInOutType(offenderReference, null, null, null, null);
        assert (validate(data) == false);

        //success
        data = new OffenderHousingAssignmentInOutType(offenderReference, assignedLocationReference, null, null, null);
        assert (validate(data) == true);

        //inOutStatus exceed 64
        String inOutStatus = "1234567890123456789012345678901234567890123456789012345678901234567890";
        data = new OffenderHousingAssignmentInOutType(offenderReference, assignedLocationReference, null, null, inOutStatus);
        assert (validate(data) == false);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }

}
