package syscon.arbutus.product.services.housingsuitability.contract.ejb;

import java.util.Date;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.movement.contract.dto.ExternalMovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.InternalMovementActivityType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;

/**
 * This is for creating the MovementActivity data for unit test only.
 * (Not use for IT test).
 *
 * @author byu
 */
public class MovementActivityBeanTest extends BaseIT {

    private final static String DIRECTION_IN = "IN";
    private final static String DIRECTION_OUT = "OUT";

    private MovementService service;

    public MovementActivityBeanTest() {
        lookupJNDILogin();
        //Clean data and reload reference code
        //service.deleteAll(uc);
    }

    public MovementActivityBeanTest(MovementService service) {
        this.service = service;
    }

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();

    }

    //for release
    private void lookupJNDILogin() {

        //JNDI lookup
        service = (MovementService) super.JNDILookUp(this.getClass(), MovementService.class);

        //init user context and login
        uc = super.initUserContext();

    }

    public void deleteAll() {
        service.deleteAll(uc);
    }

    @Test
    public void testDataCreation() {

        //Clean data and reload reference code
        //service.deleteAll(uc);

        Long activityId = null;
        int startNum = 4255;  //need the first activity which just created
        Long supervisionId = 1061L;

        //Need to change start number which just create in activity test creation.
        for (int i = startNum; i <= startNum + 99; i++) {

            // Create internal movement -Out first.
            activityId = Long.valueOf(i);

            createInternalMovementActivity(supervisionId, activityId, activityId, DIRECTION_OUT);

            // Create internal movement -In.
            i = i + 1;
            activityId = Long.valueOf(i);
            createInternalMovementActivity(supervisionId, activityId, activityId, DIRECTION_IN);

            supervisionId = supervisionId + 1;
        }

    }

    public Long createInternalMovementOut(Long supervisionId, Long activityId, Long toLocationId) {
        return createInternalMovementActivity(supervisionId, activityId, toLocationId, DIRECTION_OUT);
    }

    public Long createInternalMovementIn(Long supervisionId, Long activityId, Long toLocationId) {
        return createInternalMovementActivity(supervisionId, activityId, toLocationId, DIRECTION_IN);
    }

    private Long createInternalMovementActivity(Long supervisionId, Long activityId, Long toLocationId, String direction) {

        InternalMovementActivityType ima = new InternalMovementActivityType();
        ima.setMovementCategory("INTERNAL");
        ima.setMovementType("CRT");
        ima.setMovementDirection(direction);
        ima.setMovementStatus("COMPLETED");
        ima.setMovementReason("LA");
        ima.setMovementOutcome("SICK");
        ima.setMovementDate(new Date());

        ima.setSupervisionId(supervisionId);

        ima.setActivityId(activityId);

        ima.setFromFacilityInternalLocationId(toLocationId);
        ima.setToFacilityInternalLocationId(toLocationId + 1);

        InternalMovementActivityType ret = (InternalMovementActivityType) service.create(uc, ima, false);
        assert (ret != null);

        return ret.getMovementId();
    }

    @SuppressWarnings("unused")
    private Long createExternalMovementActivity(Long supervisionId, Long activityId, Long toFacilityId, String direction) {

        ExternalMovementActivityType ima = new ExternalMovementActivityType();
        ima.setMovementCategory("EXTERNAL");
        ima.setMovementType("CRT");
        ima.setMovementDirection(direction);
        ima.setMovementStatus("COMPLETED");
        ima.setMovementReason("LA");
        ima.setMovementOutcome("SICK");
        ima.setMovementDate(new Date());

        ima.setSupervisionId(supervisionId);
        ima.setActivityId(activityId);

        ima.setToFacilityId(toFacilityId);
        ExternalMovementActivityType ret = (ExternalMovementActivityType) service.create(uc, ima, false);
        assert (ret != null);

        return ret.getMovementId();
    }
}
