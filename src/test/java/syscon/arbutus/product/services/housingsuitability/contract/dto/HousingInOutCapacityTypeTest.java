package syscon.arbutus.product.services.housingsuitability.contract.dto;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingsuitability.HousingInOutCapacityType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingInOutCapacityTypeTest {

    @Test
    public void isValid() {

        HousingInOutCapacityType data = new HousingInOutCapacityType();
        assert (validate(data) == true);

        //assignedIn is null
        data = new HousingInOutCapacityType(null, 1L, 1L, 1L);
        assert (validate(data) == false);

        //assignedOutInFacility is null
        data = new HousingInOutCapacityType(1L, null, 1L, 1L);
        assert (validate(data) == false);

        //assignedOutOfFacility is null
        data = new HousingInOutCapacityType(1L, 1L, null, 1L);
        assert (validate(data) == false);

        //assignedTotals is null
        data = new HousingInOutCapacityType(1L, 1L, 1L, null);
        assert (validate(data) == false);

        //success
        data = new HousingInOutCapacityType(1L, 1L, 1L, 1L);
        assert (validate(data) == true);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }

}
