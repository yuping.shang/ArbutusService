package syscon.arbutus.product.services.housingsuitability.contract.dto;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingsuitability.HousingCapacityType;
import syscon.arbutus.product.services.housing.contract.dto.housingsuitability.HousingLocationStatusType;
import syscon.arbutus.product.services.housing.contract.dto.housingsuitability.OffenderHousingAssignmentInOutType;
import syscon.arbutus.product.services.housing.contract.dto.housingsuitability.OffenderHousingReservationType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingLocationStatusTypeTest {

    private HousingCapacityType totalCapacity = new HousingCapacityType();
    private Long housingLocation = 1L;

    @Test
    public void isValid() {

        HousingLocationStatusType data = new HousingLocationStatusType();
        assert (validate(data) == false);

        data = new HousingLocationStatusType(null, null, null, null);
        assert (validate(data) == false);

        //housingLocation is null
        data = new HousingLocationStatusType(null, null, null, totalCapacity);
        assert (validate(data) == false);

        //totalCapacity is null
        data = new HousingLocationStatusType(housingLocation, null, null, null);
        assert (validate(data) == false);

        //success
        data = new HousingLocationStatusType(housingLocation, null, null, totalCapacity);
        assert (validate(data) == true);

        //OffenderHousingAssignmentInOutType is null
        Set<OffenderHousingAssignmentInOutType> offenderHousingAssignments = new HashSet<OffenderHousingAssignmentInOutType>();
        offenderHousingAssignments.add(null);
        data = new HousingLocationStatusType(housingLocation, offenderHousingAssignments, null, totalCapacity);
        assert (validate(data) == false);

        //OffenderHousingAssignmentInOutType is null
        Set<OffenderHousingReservationType> offenderWithReservations = new HashSet<OffenderHousingReservationType>();
        offenderWithReservations.add(null);
        data = new HousingLocationStatusType(housingLocation, null, offenderWithReservations, totalCapacity);
        assert (validate(data) == false);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }

}
