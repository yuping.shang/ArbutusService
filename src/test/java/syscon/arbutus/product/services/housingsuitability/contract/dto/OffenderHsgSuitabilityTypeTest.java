package syscon.arbutus.product.services.housingsuitability.contract.dto;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingAttributeMismatchType;
import syscon.arbutus.product.services.housing.contract.dto.housingsuitability.OffenderHsgSuitabilityType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class OffenderHsgSuitabilityTypeTest {

    private Long offenderReference = 1L;
    private Long housingLocation = 1L;
    private String overallOutcome = "Test";

    @Test
    public void isValid() {

        OffenderHsgSuitabilityType data = new OffenderHsgSuitabilityType();
        assert (validate(data) == false);

        //offenderReference is null, fail
        data = new OffenderHsgSuitabilityType(null, housingLocation, null, null, overallOutcome);
        assert (validate(data) == false);

        //housingLocation is null, fail
        data = new OffenderHsgSuitabilityType(offenderReference, null, null, null, overallOutcome);
        assert (validate(data) == false);

        //overallOutcome is null, fail
        data = new OffenderHsgSuitabilityType(offenderReference, housingLocation, null, null, null);
        assert (validate(data) == false);

        //success
        data = new OffenderHsgSuitabilityType(offenderReference, housingLocation, null, null, overallOutcome);
        assert (validate(data) == true);

        //Wrong HousingAttributeMismatchType
        Set<HousingAttributeMismatchType> housingMismatches = new HashSet<HousingAttributeMismatchType>();
        housingMismatches.add(new HousingAttributeMismatchType());
        data = new OffenderHsgSuitabilityType(offenderReference, housingLocation, housingMismatches, null, overallOutcome);
        assert (validate(data) == false);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }

}
