package syscon.arbutus.product.services.housingsuitability.contract.dto;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingsuitability.NonAssociationConflictType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class NonAssociationConflictTypeTest {

    private String calculatedOutcome = "Test";

    @Test
    public void isValid() {

        NonAssociationConflictType data = new NonAssociationConflictType();
        assert (validate(data) == false);

        //calculatedOutcome is null
        data = new NonAssociationConflictType(null, null, null, null);
        assert (validate(data) == false);

        //success
        data = new NonAssociationConflictType(null, null, null, calculatedOutcome);
        assert (validate(data) == true);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }

}
