package syscon.arbutus.product.services.housingsuitability.contract.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.testng.annotations.BeforeClass;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.PersonTest;
import syscon.arbutus.product.services.person.contract.dto.personrelationship.PersonNonAssociation;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;

import static org.testng.Assert.assertNotNull;

/**
 * This is for creating the PersonPerson data for unit test only.
 * (Not use for IT test).
 *
 * @author byu
 */
public class PersonPersonBeanTest extends BaseIT {

    private static String CELL = "CELL";
    private static String TOTAL = "TOTAL";

    private PersonService service;
    private PersonTest psnTest;

    private Long mainPersonId;
    private List<Long> associatedPersonIds;

    public PersonPersonBeanTest() {
        lookupJNDILogin();
        psnTest = new PersonTest();
        //init reference code etc.
        //service.deleteAll(uc);
    }

    public PersonPersonBeanTest(PersonService service, PersonTest psnTest) {
        this.service = service;
        this.psnTest = psnTest;
    }

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();

    }

    public void deleteAll() {
        service.deleteAllPersonRelationship(uc);
    }

    //for release
    private void lookupJNDILogin() {

        service = (PersonService) super.JNDILookUp(this.getClass(), PersonService.class);
        assertNotNull(service);

        //init user context and login
        uc = super.initUserContext();
    }

    public void createPersonPersonNonAssociation() {

        mainPersonId = psnTest.createPerson();

        associatedPersonIds = new ArrayList<Long>();
        for (int i = 0; i < 9; i++) {
            Long associatedPesonId = psnTest.createPerson();
            associatedPersonIds.add(associatedPesonId);
        }

        //Create data for Person1
        createNonAssociation(mainPersonId, associatedPersonIds.get(0), CELL, true);
        createNonAssociation(mainPersonId, associatedPersonIds.get(1), CELL, true);
        createNonAssociation(mainPersonId, associatedPersonIds.get(2), CELL, false);
        createNonAssociation(mainPersonId, associatedPersonIds.get(3), CELL, false);
        createNonAssociation(mainPersonId, associatedPersonIds.get(4), TOTAL, true);
        createNonAssociation(mainPersonId, associatedPersonIds.get(5), TOTAL, true);
        createNonAssociation(mainPersonId, associatedPersonIds.get(6), TOTAL, false);
        createNonAssociation(mainPersonId, associatedPersonIds.get(7), TOTAL, true);
        createNonAssociation(mainPersonId, associatedPersonIds.get(8), TOTAL, false);

    }

    public Long getMainPersonId() {
        return mainPersonId;
    }

    public List<Long> getAssociatedPersonIds() {

		/*Set<Long> ret = new HashSet<Long>();
        long startNum = 11l;
		for (long l= startNum; l<=startNum+8;l++) {
			ret.add(l);
		}
		*/
        return associatedPersonIds;
    }

    private void createNonAssociation(Long personOne, Long personTwo, String nonCode, boolean isActive) {

        Date effectiveDate = new Date();
        Date expiryDate = null;

        //Active
        if (!isActive) {
            expiryDate = new Date();
        }

        String code = nonCode;

        PersonNonAssociation personNonAssociation = new PersonNonAssociation(null, personOne, personTwo, code, "OTHER", "OTHER", null, code + " Desc", expiryDate,
                effectiveDate);

        service.createPersonRelationship(uc, personNonAssociation);

    }

}
