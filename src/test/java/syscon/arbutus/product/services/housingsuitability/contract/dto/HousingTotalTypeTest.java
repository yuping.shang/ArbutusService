package syscon.arbutus.product.services.housingsuitability.contract.dto;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingsuitability.HousingCapacityType;
import syscon.arbutus.product.services.housing.contract.dto.housingsuitability.HousingInOutCapacityType;
import syscon.arbutus.product.services.housing.contract.dto.housingsuitability.HousingTotalType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingTotalTypeTest {

    private Long housingLocation = 1L;
    private HousingCapacityType housingCapacity = new HousingCapacityType();
    private HousingInOutCapacityType housingInOutCapacity = new HousingInOutCapacityType();

    @Test
    public void isValid() {

        HousingTotalType data = new HousingTotalType();
        assert (validate(data) == false);

        data = new HousingTotalType(null, null, null);
        assert (validate(data) == false);

        //housingLocation is null
        data = new HousingTotalType(null, housingCapacity, null);
        assert (validate(data) == false);

        //housingCapacity is null
        data = new HousingTotalType(housingLocation, null, null);
        assert (validate(data) == false);

        //success
        data = new HousingTotalType(housingLocation, housingCapacity, null);
        assert (validate(data) == true);

        //success
        data = new HousingTotalType(housingLocation, housingCapacity, housingInOutCapacity);
        assert (validate(data) == true);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }

}
