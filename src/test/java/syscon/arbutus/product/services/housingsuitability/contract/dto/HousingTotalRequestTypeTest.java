package syscon.arbutus.product.services.housingsuitability.contract.dto;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingsuitability.HousingTotalRequestType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingTotalRequestTypeTest {

    private Long housingLocation = 1L;

    @Test
    public void isValid() {

        HousingTotalRequestType data = new HousingTotalRequestType();
        assert (validate(data) == false);

        //housingLocation is null
        data = new HousingTotalRequestType(null, null, null, null, null, null, null);
        assert (validate(data) == false);

        //success
        data = new HousingTotalRequestType(housingLocation, null, null, null, null, null, null);
        assert (validate(data) == true);

        //securityLevel is wrong
        Set<String> codes = new HashSet<String>();
        codes.add(new String());
        data = new HousingTotalRequestType(housingLocation, codes, null, null, null, null, null);
        assert (validate(data) == false);

        //sentenceStatus is wrong
        codes = new HashSet<String>();
        codes.add(new String());
        data = new HousingTotalRequestType(housingLocation, null, codes, null, null, null, null);
        assert (validate(data) == false);

        //offenderGender is wrong
        codes = new HashSet<String>();
        codes.add(new String());
        data = new HousingTotalRequestType(housingLocation, null, null, codes, null, null, null);
        assert (validate(data) == false);

        //ageRange is wrong
        codes = new HashSet<String>();
        codes.add(new String());
        data = new HousingTotalRequestType(housingLocation, null, null, null, codes, null, null);
        assert (validate(data) == false);

        //locationProperty is wrong
        codes = new HashSet<String>();
        codes.add(new String());
        data = new HousingTotalRequestType(housingLocation, null, null, null, null, codes, null);
        assert (validate(data) == false);

        //offenderCategory is wrong
        codes = new HashSet<String>();
        codes.add(new String());
        data = new HousingTotalRequestType(housingLocation, null, null, null, null, null, codes);
        assert (validate(data) == false);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
