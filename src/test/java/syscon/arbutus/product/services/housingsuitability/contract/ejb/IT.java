package syscon.arbutus.product.services.housingsuitability.contract.ejb;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.*;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingAttributeMismatchType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingBedMgmtActivityType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.OffenderHousingAttributeType;
import syscon.arbutus.product.services.housing.contract.dto.housingsuitability.*;
import syscon.arbutus.product.services.housing.contract.ejb.HousingBedManagementActivityServiceAdapter;
import syscon.arbutus.product.services.housing.contract.ejb.HousingServiceBean;
import syscon.arbutus.product.services.housing.contract.interfaces.HousingService;
import syscon.arbutus.product.services.housing.contract.dto.HousingAttributeMismatch;
import syscon.arbutus.product.services.referencedata.contract.interfaces.ReferenceDataService;

import static org.testng.Assert.*;

public class IT extends BaseIT {

    // reference set defined attributes

    private final static String SEX_M = "M";
    private final static String SEX_F = "F";

    private static final String SECURITY_LEVEL_MAX = "MAX";
    private static final String SECURITY_LEVEL_MIN = "MIN";

    private final static String ASSESSMENT_RESULT_MAX = "MAX";
    private final static String ASSESSMENT_RESULT_MIN = "MIN";

    private final static String CASE_SENTENCED = "SENTENCED";
    private final static String CASE_UNSENTENCED = "UNSENTENCED";

    private final static String CAUTION_CODE_70 = "70";
    private final static String CAUTION_CODE_75 = "75";
    private final static String CAUTION_CODE_90 = "90";

    private final static String HEALTH_ISSUES_WC = "WC";
    private final static String HEALTH_ISSUES_DI = "DI";
    private final static String HEALTH_ISSUES_HS = "HS";
    private final static String HEALTH_ISSUES_RS = "RS";

    private static Logger log = LoggerFactory.getLogger(IT.class);

    private Long facilityId = 3L;

    //updated based on FIL creation data (choose the root Id)
    private Long rootId = 37L;

    //find PersonIdentity Id which the fromIdentifier link to Person 1 from PI_Assc
    //find Supervision Id which link to PersonIdentity Id above from SUP_Assc
    private Long mainPersonSupervision = 86L;

    private Long leafLocation1 = 46L;

    private List<Long> leafHousingLocations = null;
    private List<Long> associatedPersonIds = null;
    private Set<Long> assignedOffenders = new HashSet<Long>();

    private HousingService service;
    private FacilityInternalLocationTest filTest;
    private FacilityTest facTest;
    private HousingBedManagementActivityBeanTest testHBMA;
    private PersonPersonBeanTest testPP;
    private PersonIdentityTest testPI;
    private SupervisionTest testSUP;

    private MovementActivityBeanTest testMA;
    private ActivityTest testACT;

    //for clean test data only
    private PersonTest psnTest;
    private StaffTest stfTest;
    private HousingBedActivityTest haTest;
    protected ReferenceDataService referenceDataService;

    public IT() {
    }

    public IT(HousingService service, FacilityInternalLocationTest filTest, FacilityTest facTest, PersonPersonBeanTest testPP, PersonIdentityTest testPI,
            SupervisionTest testSUP, MovementActivityBeanTest testMA, ActivityTest testACT, PersonTest psnTest, StaffTest stfTest, HousingBedActivityTest haTest) {
        this.service = service;
        this.filTest = filTest;
        this.facTest = facTest;
        this.testHBMA = testHBMA;
        this.testPP = testPP;
        this.testPI = testPI;
        this.testSUP = testSUP;
        this.testMA = testMA;
        this.testACT = testACT;
        this.psnTest = psnTest;
        this.stfTest = stfTest;
        this.haTest = haTest;
    }

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();

        init();
        cleanUp();

        testDateCreation();

    }

    @AfterClass
    public void afterClass() {

        cleanUp();

    }

    private void init() {

        filTest = new FacilityInternalLocationTest();
        facTest = new FacilityTest();
        testPP = new PersonPersonBeanTest();
        testPI = new PersonIdentityTest();
        testSUP = new SupervisionTest();
        //Create Activity Data
        testACT = new ActivityTest();
        //Create MovementActivity data
        testMA = new MovementActivityBeanTest();

        //for clean test data only
        haTest = new HousingBedActivityTest();
        psnTest = new PersonTest();
        stfTest = new StaffTest();
        testSUP = new SupervisionTest();
    }

    private void cleanUp() {

        testMA.deleteAll();
        haTest.deleteAll();
        testACT.deleteAll();
        stfTest.deleteAll();

        testPP.deleteAll();
        testSUP.deleteAll();

        filTest.deleteAll();
        facTest.deleteAll();

        testPI.deleteAll();
        psnTest.deleteAll();

    }

    // for local debug use only
    @SuppressWarnings("unused")
    private void localLogin() {

        service = new HousingServiceBean();
        // init user context and login
        uc = super.initUserContext();
    }

    //for release
    private void lookupJNDILogin() {

        service = (HousingService) super.JNDILookUp(this.getClass(), HousingService.class);
        assertNotNull(service);

        // init user context and login
        uc = super.initUserContext();

        referenceDataService = (ReferenceDataService) super.JNDILookUp(this.getClass(), ReferenceDataService.class);
        assertNotNull(referenceDataService);
    }

    //@Test
    public void loadCodeMapping() {

        log.info("loadCodeMapping");

        //service.loadBusinessRuleMappings();

    }

    //@Test
    @SuppressWarnings({ "unchecked", "rawtypes", "unused" })
    public void testDateCreation() {

        //localLogin();

        //create facility
        facilityId = facTest.createFacility();

        //Create FIL data tree
        filTest.createFILTree(facilityId);
        rootId = filTest.getRootId();
        leafHousingLocations = filTest.getLeafIds();
        leafLocation1 = filTest.getCell1();

        testHBMA = new HousingBedManagementActivityBeanTest(service, referenceDataService, stfTest, testACT, testSUP, facilityId, leafHousingLocations);

        //Create PersonPerson non association data
        testPP.createPersonPersonNonAssociation();
        associatedPersonIds = testPP.getAssociatedPersonIds();

        //Create PersonIdentity data for person.
        Long mainPersonId = testPP.getMainPersonId();
        Long mainPersonIdentityId = testPI.createPersonIdentity(mainPersonId);
        List<Long> associatedPersonIdentityIds = testPI.createPersonIdentity(associatedPersonIds);

        //Create Supervision data for PersonIdentity
        mainPersonSupervision = testSUP.createSupervision(mainPersonIdentityId, facilityId, true);

        //Map<personIdentityId, supervisionId>
        List<Long> associatedSupervisionIds = testSUP.createSupervsionByPersonIdentity(associatedPersonIdentityIds, facilityId);

        //Add supervision association for PersonIdentity
        //List<Long> associatedSupervisionIds = testSUP.getSupervisionIds(associationPersonIdentitySupervisionsMap);
        //List<Long> associatedSupervisionIds = testPI.addSupervisionAssociations(associationPersonIdentitySupervisionsMap);

        //Create supervision for housing assignment
        List supervisionForAssignmentList = testSUP.createSupervisionForAssignment(facilityId);

        //Create HBMA data

        //Assigned mainPerson to leafCell first
        testHBMA.assignHousingLocation(mainPersonSupervision, leafLocation1);
        assignedOffenders.add(mainPersonSupervision);    //to created the movement activity for this assignment later.

        // Data for Non-Association conflict check.
        Set<Long> associatedOffenders = testHBMA.assignHousingLocationForConflict(associatedSupervisionIds, leafHousingLocations);
        assignedOffenders.addAll(associatedOffenders);

        //Assign offender to leaf housing locations
        Set<Long> leafAssignedOffenders = testHBMA.assignHousingLocation(supervisionForAssignmentList, leafHousingLocations);
        assignedOffenders.addAll(leafAssignedOffenders);

        //Create change request to leaf housing locations
        List<Long> changeRequestOffenders = testHBMA.createChangeRequest(leafHousingLocations);
        assignedOffenders.addAll(changeRequestOffenders);

        //Create Activity Data
        //Create MovementActivity data
        //Set<Long> activityIds = testACT.createActivityByOffenders(assignedOffenders);
        for (Long offenderId : assignedOffenders) {

            Long activityId1 = testACT.createActivity(offenderId, null);
            Long movementActivityId1 = testMA.createInternalMovementOut(offenderId, activityId1, activityId1);

            Long activityId2 = testACT.createActivity(offenderId, activityId1);
            Long movementActivityId2 = testMA.createInternalMovementIn(offenderId, activityId2, activityId2);

        }

    }

    @Test
    public void retrieveAvailableHousingLocationsNegative() {

        //empty parameters, will fail.
        verifyRetrieveAvailableHousingLocationsNegative(null, null, null, null, InvalidInputException.class);

        Long housingLocationId = 1L;
        Long offenderSupervisionId = 1L;
        OffenderHousingAttributeType offenderHousingAttribute = new OffenderHousingAttributeType(ASSESSMENT_RESULT_MIN, SEX_M, 20L, CASE_UNSENTENCED, null, null);

        // empty target housing location
        verifyRetrieveAvailableHousingLocationsNegative(null, offenderSupervisionId, offenderHousingAttribute, true, InvalidInputException.class);

        // empty offender reference
        verifyRetrieveAvailableHousingLocationsNegative(housingLocationId, null, offenderHousingAttribute, true, InvalidInputException.class);

        // empty offenderHousingAttribute
        verifyRetrieveAvailableHousingLocationsNegative(housingLocationId, offenderSupervisionId, null, true, InvalidInputException.class);

        // Not valid offenderHousingAttribute
        offenderHousingAttribute = new OffenderHousingAttributeType(null, SEX_M, 20L, CASE_UNSENTENCED, null, null);
        verifyRetrieveAvailableHousingLocationsNegative(housingLocationId, offenderSupervisionId, null, true, InvalidInputException.class);

    }

    @Test
    public void retrieveAvailableHousingLocationsPositive() {

        Long housingLocationId = rootId;
        Long offenderSupervisionId = mainPersonSupervision;

        //Long housingLocationId = 1L;
        //Long offenderSupervisionId = 50L;

        Set<String> locationProperties = new HashSet<String>();
        locationProperties.add(HEALTH_ISSUES_WC);

        Set<String> offenderCategories = new HashSet<String>();
        offenderCategories.add(CAUTION_CODE_75);

        OffenderHousingAttributeType offenderHousingAttribute = new OffenderHousingAttributeType(ASSESSMENT_RESULT_MIN, SEX_F, 20L, CASE_UNSENTENCED, locationProperties,
                offenderCategories);

        OffenderHsgSuitabilityCapacitiesReturnType ret = service.retrieveAvailableHousingLocations(uc, housingLocationId, offenderSupervisionId, offenderHousingAttribute,
                true);

        assert (ret.getOffenderHsgSuitabilityCapacities() != null);
        log.info("ret.getOffenderHsgSuitabilityCapacities()=" + ret.getOffenderHsgSuitabilityCapacities());

		/*
        ret = service.retrieveAvailableHousingLocations(uc, housingLocationId,
				offenderSupervisionId, offenderHousingAttribute, false);
		assert(ret.getReturnCode().equals(ReturnCode.SUCCESS.returnCode()));
		assert(ret.getOffenderHsgSuitabilityCapacities() != null);
		*/

    }

    @Test
    public void retrieveMatchedHousingLocationNegative() {

        //empty parameters, will fail.
        verifyRetrieveMatchedHousingLocationsNegative(null, null, null, InvalidInputException.class);

        Long housingLocationId = 1L;
        Long offenderSupervisionId = 1L;

        OffenderHousingAttributeType offenderHousingAttribute = new OffenderHousingAttributeType(ASSESSMENT_RESULT_MIN, SEX_M, 20L, CASE_UNSENTENCED, null, null);

        // empty target housing location
        verifyRetrieveMatchedHousingLocationsNegative(null, offenderSupervisionId, offenderHousingAttribute, InvalidInputException.class);

        // empty offender reference
        verifyRetrieveMatchedHousingLocationsNegative(housingLocationId, null, offenderHousingAttribute, InvalidInputException.class);

        // empty offenderHousingAttribute
        verifyRetrieveMatchedHousingLocationsNegative(housingLocationId, offenderSupervisionId, null, InvalidInputException.class);

        // Not valid offenderHousingAttribute
        offenderHousingAttribute = new OffenderHousingAttributeType(null, SEX_M, 20L, CASE_UNSENTENCED, null, null);
        verifyRetrieveMatchedHousingLocationsNegative(housingLocationId, offenderSupervisionId, offenderHousingAttribute, InvalidInputException.class);

    }

    @Test
    public void retrieveMatchedHousingLocationPositive() {

        Long housingLocationId = rootId;
        Long offenderSupervisionId = mainPersonSupervision;

        Set<String> locationProperties = new HashSet<String>();
        locationProperties.add(HEALTH_ISSUES_WC);

        Set<String> offenderCategories = new HashSet<String>();
        offenderCategories.add(CAUTION_CODE_75);

        OffenderHousingAttributeType offenderHousingAttribute = new OffenderHousingAttributeType(ASSESSMENT_RESULT_MIN, SEX_F, 20L, CASE_UNSENTENCED, locationProperties,
                offenderCategories);

        OffenderHsgSuitabilityCapacitiesReturnType ret = service.retrieveMatchedHousingLocations(uc, facilityId, housingLocationId, offenderSupervisionId, offenderHousingAttribute);

        assert (ret.getOffenderHsgSuitabilityCapacities() != null);

    }

    @Test
    public void retrieveCurrentHousingLocationStatusNegative() {

        //empty parameters, will fail.
        verifyRetrieveCurrentHousingLocationStatusNegative(null, null, InvalidInputException.class);

        Long housingLocationId = -1L;
        verifyRetrieveCurrentHousingLocationStatusNegative(housingLocationId, null, ArbutusRuntimeException.class);

    }

    @Test
    public void retrieveCurrentHousingLocationStatusPositive() {

        Long housingLocationId = rootId;
        HousingLocationStatusType ret = service.retrieveCurrentHousingLocationStatus(uc, housingLocationId, null);
        assert (ret != null);

    }

    @Test
    public void retrieveCurrentHousingTotalNegative() {

        verifyRetrieveCurrentHousingTotalNegative(null, null, InvalidInputException.class);

        //Wrong DTO, will fail.
        HousingTotalRequestType request = new HousingTotalRequestType();
        verifyRetrieveCurrentHousingTotalNegative(request, null, InvalidInputException.class);

        //Wrong DTO, will fail.
        request.setAgeRange(new HashSet<String>());
        verifyRetrieveCurrentHousingTotalNegative(request, null, InvalidInputException.class);

    }

    @Test
    public void retrieveCurrentHousingTotalPositive() {

        HousingTotalRequestType request = new HousingTotalRequestType();

        request.setHousingLocation(rootId);
        request.getSecurityLevel().add(SECURITY_LEVEL_MIN);

        HousingTotalType ret = service.retrieveCurrentHousingTotal(uc, request, true);

        assert (ret != null);

		/*
		// Retrieve with In-Out status as well.
		ret = service.retrieveCurrentHousingTotal(uc, request, true);
		assert(ret.getReturnCode().equals(ReturnCode.SUCCESS.returnCode()));
		assert(ret != null);
		*/
    }

    @Test
    public void retrieveChildHousingTotalsNegative() {

        verifyRetrieveChildHousingTotalsNegative(null, null, InvalidInputException.class);

        //Wrong DTO, will fail.
        HousingTotalRequestType request = new HousingTotalRequestType();
        verifyRetrieveChildHousingTotalsNegative(request, null, InvalidInputException.class);

        //Wrong DTO, will fail.
        request.getSecurityLevel().add(SECURITY_LEVEL_MIN);
        verifyRetrieveChildHousingTotalsNegative(request, null, InvalidInputException.class);

        //Wrong location id, will fail.
        request.setHousingLocation(-1L);
        verifyRetrieveChildHousingTotalsNegative(request, null, ArbutusRuntimeException.class);

    }

    @Test
    public void retrieveChildHousingTotalsPositive() {

        HousingTotalRequestType request = new HousingTotalRequestType();

        request.setHousingLocation(rootId);
        request.getSecurityLevel().add(SECURITY_LEVEL_MAX);

        HousingTotalsReturnType ret = service.retrieveChildHousingTotals(uc, request, null);

        assert (ret.getHousingTotals() != null);

    }

    @Test
    public void retrieveHousingLocationWithAvailableCapacitiesNegative() {

        verifyRetrieveHousingLocationWithAvailableCapacitiesNegative(null, null, InvalidInputException.class);

        Long facilityId = -1L;
        verifyRetrieveHousingLocationWithAvailableCapacitiesNegative(facilityId, null, ArbutusRuntimeException.class);

    }

    @Test
    public void retrieveHousingLocationWithAvailableCapacitiesPositive() {

        HousingLocationStatusesReturnType ret = service.retrieveHousingLocationWithAvailableCapacities(uc, facilityId, true);

        assert (ret.getHousingLocationStatuses() != null);

        ret = service.retrieveHousingLocationWithAvailableCapacities(uc, facilityId, false);

        assert (ret.getHousingLocationStatuses() != null);

        // get with In-Out status
        ret = service.retrieveHousingLocationWithAvailableCapacities(uc, facilityId, true);

        assert (ret.getHousingLocationStatuses() != null);

    }

    @Test
    public void calculateHousingSuitabilityNegative() {

        //empty parameters, will fail.
        verifyCalculateHousingSuitabilityNegative(null, null, null, InvalidInputException.class);

        Long housingLocationId = 1L;
        Long offenderSupervisionId = 1L;

        OffenderHousingAttributeType offenderHousingAttribute = new OffenderHousingAttributeType(ASSESSMENT_RESULT_MIN, SEX_M, 20L, CASE_UNSENTENCED, null, null);

        // empty target housing location
        verifyCalculateHousingSuitabilityNegative(null, offenderSupervisionId, offenderHousingAttribute, InvalidInputException.class);

        // empty offender reference
        verifyCalculateHousingSuitabilityNegative(housingLocationId, null, offenderHousingAttribute, InvalidInputException.class);

        // empty offenderHousingAttribute
        verifyCalculateHousingSuitabilityNegative(housingLocationId, offenderSupervisionId, null, InvalidInputException.class);

        // Not valid offenderHousingAttribute
        offenderHousingAttribute = new OffenderHousingAttributeType(null, SEX_M, 20L, CASE_UNSENTENCED, null, null);
        verifyCalculateHousingSuitabilityNegative(housingLocationId, offenderSupervisionId, offenderHousingAttribute, InvalidInputException.class);

    }

    @Test
    public void calculateHousingSuitabilityPositive() {

        Long housingLocationId = rootId;
        Long offenderSupervisionId = mainPersonSupervision;
        //Long housingLocationId = 72L;
        //Long offenderSupervisionId = 322L;

        Set<String> locationProperties = getOffenderLocationProperties();
        Set<String> offenderCategories = getOffenderCategories();

        OffenderHousingAttributeType offenderHousingAttribute = new OffenderHousingAttributeType(ASSESSMENT_RESULT_MAX, SEX_F, 20L, CASE_SENTENCED, locationProperties,
                offenderCategories);

        OffenderHsgSuitabilitiesReturnType ret = service.calculateHousingSuitability(uc, housingLocationId, offenderSupervisionId, offenderHousingAttribute);

        assert (ret.getOffenderHsgSuitabilities() != null);

    }

    @Test
    public void validateHousingAttributesNegative() {

        //empty parameters, will fail.
        verifyValidateHousingAttributes(null, null, InvalidInputException.class);

        Long offenderSupervisionId = -1L;
        OffenderHousingAttributeType offenderHousingAttribute = new OffenderHousingAttributeType();

        //empty offenderSupervisionId, will fail.
        verifyValidateHousingAttributes(null, offenderHousingAttribute, InvalidInputException.class);

        //empty offenderHousingAttribute, will fail.
        verifyValidateHousingAttributes(offenderSupervisionId, offenderHousingAttribute, InvalidInputException.class);

        //Wrong offenderSupervisionId, will fail.
        offenderHousingAttribute = getOffenderAttributes();
        verifyValidateHousingAttributes(offenderSupervisionId, offenderHousingAttribute, ArbutusRuntimeException.class);

    }

    @Test
    public void validateHousingAttributesPositive() {

        Long offenderSupervisionId = mainPersonSupervision;

        OffenderHousingAttributeType offenderHousingAttribute = getOffenderAttributes();

        HousingBedMgmtActIdentifiersReturnType ret = service.validateHousingAttributes(uc, offenderSupervisionId, offenderHousingAttribute);

        assert (ret.getUpdatedHsgBedActivities() != null);

    }

    @Test
    public void retrieveHousingLocationCapacitiesNegative() {

        verifyRetrieveHousingLocationCapacities(null, null, InvalidInputException.class);

        HousingTotalRequestType request = new HousingTotalRequestType();
        verifyRetrieveHousingLocationCapacities(request, null, InvalidInputException.class);

        request = new HousingTotalRequestType();
        request.getSecurityLevel().add("Test");
        verifyRetrieveHousingLocationCapacities(request, null, InvalidInputException.class);

    }

    @Test
    public void retrieveHousingLocationCapacitiesPositive() {

        HousingTotalRequestType request = new HousingTotalRequestType();

        request.setHousingLocation(rootId);
        request.getSecurityLevel().add(SECURITY_LEVEL_MAX);

        HousingLocationCapacitiesReturnType ret = service.retrieveHousingLocationCapacities(uc, request, null);

        assert (ret.getHousingLocationCapacities() != null);

        ret = service.retrieveHousingLocationCapacities(uc, request, true);

        assert (ret.getHousingLocationCapacities() != null);

        ret = service.retrieveHousingLocationCapacities(uc, request, false);

        assert (ret.getHousingLocationCapacities() != null);

    }

    @Test
    public void calculateHousingSuitabilitiesNegative() {

        //Empty parameter
        verifyCalculateHousingSuitabilities(null, InvalidInputException.class);

        //Empty Set
        Set<OffenderHsgSuitabilityCheckType> dtos = new HashSet<OffenderHsgSuitabilityCheckType>();
        verifyCalculateHousingSuitabilities(dtos, InvalidInputException.class);

        //Empty DTO
        OffenderHsgSuitabilityCheckType dto = new OffenderHsgSuitabilityCheckType();
        dtos = new HashSet<OffenderHsgSuitabilityCheckType>();
        dtos.add(dto);
        verifyCalculateHousingSuitabilities(dtos, InvalidInputException.class);

        //Empty supervisionId
        dto = new OffenderHsgSuitabilityCheckType();
        dto.setHousingLocationId(1L);
        dtos = new HashSet<OffenderHsgSuitabilityCheckType>();
        dtos.add(dto);
        verifyCalculateHousingSuitabilities(dtos, InvalidInputException.class);

    }

    @Test
    public void calculateHousingSuitabilitiesPositive() {

        //To be updated the suppervision Ids
        OffenderHsgSuitabilityCheckType dto = new OffenderHsgSuitabilityCheckType(mainPersonSupervision, leafLocation1, getOffenderAttributes());
        OffenderHsgSuitabilityCheckType dto2 = new OffenderHsgSuitabilityCheckType(mainPersonSupervision + 2, leafLocation1, getOffenderAttributes());

        Set<OffenderHsgSuitabilityCheckType> dtos = new HashSet<OffenderHsgSuitabilityCheckType>();
        dtos.add(dto);
        dtos.add(dto2);

        OffenderHsgSuitabilitiesReturnType ret = service.calculateHousingSuitabilities(uc, dtos);

        assert (ret.getOffenderHsgSuitabilities() != null);

    }

    //@Test
    public void testCreateChangeRequest() {

        //Create HBMA data
        Long offenderId = 1650L;
        Long locationId = 358L;

        HousingBedMgmtActivityType ret = testHBMA.createChangeRequest(offenderId, locationId);
        assert (ret != null);

    }

    //@Test
    public void testDefect1960() {

        //If need to test the isHousingMismatchesChanged only, need to change to public method

        HousingBedManagementActivityServiceAdapter hbmaAdapter = new HousingBedManagementActivityServiceAdapter();

        Set<HousingAttributeMismatch> newMismatches = null; //getNewMismatches();
        Set<HousingAttributeMismatchType> oldmismatches = null; //getOldmismatches();
        boolean ret = hbmaAdapter.isHousingMismatchesChanged(newMismatches, oldmismatches);
        assertFalse(ret);

        newMismatches = new HashSet<HousingAttributeMismatch>();
        oldmismatches = new HashSet<HousingAttributeMismatchType>();
        ret = hbmaAdapter.isHousingMismatchesChanged(newMismatches, oldmismatches);
        assertFalse(ret);

        newMismatches = getNewMismatches();
        oldmismatches = HousingBedManagementActivityBeanTest.getMismatches();
        ret = hbmaAdapter.isHousingMismatchesChanged(newMismatches, oldmismatches);
        //assertFalse(ret);
        assertTrue(ret);

    }

    @Test
    public void testDefect2336() {

    }

    //@Test
    public void testRetrieveAvailableHousing() {

        Long housingLocationId = 22L;
        Long offenderSupervisionId = 106L;

        Set<String> locationProperties = new HashSet<String>();
        locationProperties.add(HEALTH_ISSUES_WC);

        Set<String> offenderCategories = new HashSet<String>();
        offenderCategories.add(CAUTION_CODE_75);

        OffenderHousingAttributeType offenderHousingAttribute = new OffenderHousingAttributeType(ASSESSMENT_RESULT_MIN, SEX_F, 20L, CASE_UNSENTENCED, locationProperties,
                offenderCategories);

        OffenderHsgSuitabilityCapacitiesReturnType ret = service.retrieveAvailableHousingLocations(uc, housingLocationId, offenderSupervisionId, offenderHousingAttribute,
                true);

        assert (ret.getOffenderHsgSuitabilityCapacities() != null);

    }

    @SuppressWarnings("unused")
    private HousingTotalRequestType getHousingTotalRequestType(Long locationId) {

        HousingTotalRequestType ret = new HousingTotalRequestType();

        ret.setHousingLocation(locationId);
        //ret.getSentenceStatus().add();
        ret.getSecurityLevel().add(SECURITY_LEVEL_MAX);

        return ret;

    }

    private Set<HousingAttributeMismatch> getNewMismatches() {

        Set<HousingAttributeMismatch> ret = new HashSet<HousingAttributeMismatch>();

        Set<String> locValue = new HashSet<String>();
        locValue.add("MED");
        locValue.add("MAX");
        HousingAttributeMismatch mismatch1 = new HousingAttributeMismatch("SECLEVEL", "MIN", locValue, "SUTBL", null);

        locValue = new HashSet<String>();
        locValue.add("M");
        locValue.add("F");
        HousingAttributeMismatch mismatch2 = new HousingAttributeMismatch("GENDER", "F", locValue, "WRNG", null);

        locValue = new HashSet<String>();
        locValue.add("ADULT");
        locValue.add("JUVENILE");
        HousingAttributeMismatch mismatch3 = new HousingAttributeMismatch("AGE", "ADULT", locValue, "SUTBL", null);

        ret.add(mismatch1);
        ret.add(mismatch2);
        ret.add(mismatch3);
        ret.add(null);

        return ret;
    }

    private Set<String> getOffenderCategories() {

        Set<String> offenderCategories = new HashSet<String>();
        offenderCategories.add(CAUTION_CODE_90);
        offenderCategories.add(CAUTION_CODE_75);
        offenderCategories.add(CAUTION_CODE_70);

        return offenderCategories;
    }

    private Set<String> getOffenderLocationProperties() {

        Set<String> locationProperties = new HashSet<String>();
        locationProperties.add(HEALTH_ISSUES_DI);
        locationProperties.add(HEALTH_ISSUES_WC);
        locationProperties.add(HEALTH_ISSUES_RS);
        locationProperties.add(HEALTH_ISSUES_HS);

        return locationProperties;
    }

    private OffenderHousingAttributeType getOffenderAttributes() {

        Set<String> locationProperties = new HashSet<String>();
        locationProperties.add(HEALTH_ISSUES_WC);

        Set<String> offenderCategories = new HashSet<String>();
        offenderCategories.add(CAUTION_CODE_75);

        OffenderHousingAttributeType offenderHousingAttribute = new OffenderHousingAttributeType(ASSESSMENT_RESULT_MIN, SEX_M, 20L, CASE_UNSENTENCED, locationProperties,
                offenderCategories);

        return offenderHousingAttribute;

    }

    private void verifyCalculateHousingSuitabilities(Set<OffenderHsgSuitabilityCheckType> offenderHousingCheckTypes, Class<?> expectionClazz) {
        try {
            OffenderHsgSuitabilitiesReturnType ret = service.calculateHousingSuitabilities(uc, offenderHousingCheckTypes);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveHousingLocationCapacities(HousingTotalRequestType housingTotalRequest, Boolean isAvailable, Class<?> expectionClazz) {
        try {
            HousingLocationCapacitiesReturnType ret = service.retrieveHousingLocationCapacities(uc, housingTotalRequest, isAvailable);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyValidateHousingAttributes(Long offenderSupervisionId, OffenderHousingAttributeType offenderHousingAttribute, Class<?> expectionClazz) {
        try {
            HousingBedMgmtActIdentifiersReturnType ret = service.validateHousingAttributes(uc, offenderSupervisionId, offenderHousingAttribute);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyCalculateHousingSuitabilityNegative(Long housingLocationId, Long offenderSupervisionId, OffenderHousingAttributeType offenderHousingAttribute,
            Class<?> expectionClazz) {
        try {
            OffenderHsgSuitabilitiesReturnType ret = service.calculateHousingSuitability(uc, housingLocationId, offenderSupervisionId, offenderHousingAttribute);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveHousingLocationWithAvailableCapacitiesNegative(Long facilityId, Boolean needInOutStatus, Class<?> expectionClazz) {
        try {
            HousingLocationStatusesReturnType ret = service.retrieveHousingLocationWithAvailableCapacities(uc, facilityId, needInOutStatus);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveChildHousingTotalsNegative(HousingTotalRequestType housingTotalRequest, Boolean needInOutStatus, Class<?> expectionClazz) {
        try {
            HousingTotalsReturnType ret = service.retrieveChildHousingTotals(uc, housingTotalRequest, needInOutStatus);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveCurrentHousingTotalNegative(HousingTotalRequestType housingTotalRequest, Boolean needInOutStatus, Class<?> expectionClazz) {
        try {
            HousingTotalType ret = service.retrieveCurrentHousingTotal(uc, housingTotalRequest, needInOutStatus);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveCurrentHousingLocationStatusNegative(Long housingLocationId, Boolean needInOutStatus, Class<?> expectionClazz) {
        try {
            HousingLocationStatusType ret = service.retrieveCurrentHousingLocationStatus(uc, housingLocationId, needInOutStatus);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveMatchedHousingLocationsNegative(Long housingLocationId, Long offenderSupervisionId, OffenderHousingAttributeType offenderHousingAttribute,
            Class<?> expectionClazz) {
        try {
            OffenderHsgSuitabilityCapacitiesReturnType ret = service.retrieveMatchedHousingLocations(uc, facilityId, housingLocationId, offenderSupervisionId,
                    offenderHousingAttribute);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveAvailableHousingLocationsNegative(Long housingLocationId, Long offenderSupervisionId,
            OffenderHousingAttributeType offenderHousingAttribute, Boolean needInOutStatus, Class<?> expectionClazz) {
        try {
            OffenderHsgSuitabilityCapacitiesReturnType ret = service.retrieveAvailableHousingLocations(uc, housingLocationId, offenderSupervisionId,
                    offenderHousingAttribute, needInOutStatus);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }
}
