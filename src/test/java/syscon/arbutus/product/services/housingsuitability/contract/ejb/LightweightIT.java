package syscon.arbutus.product.services.housingsuitability.contract.ejb;

import org.junit.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseTestJMockIt;
import syscon.arbutus.product.services.common.HibernateUtil;

/**
 * Created by os on 26/06/15.
 */
public class LightweightIT extends BaseTestJMockIt {
    private IT superIT;

    @BeforeClass
    public void beforeClass() {
        super.beforeClass();

        superIT = new IT(housingService, facilityInternalLocationTest, facilityTest, personPersonBeanTest, personIdentityTest, supervisionTest, movementActivityBeanTest,
                activityTest, personTest, staffTest, housingBedActivityTest);

        superIT.testDateCreation();
        HibernateUtil.commitAndClose();
    }

    @AfterClass
    public void afterClass() {

        super.afterClass();

        superIT.afterClass();
        HibernateUtil.commitAndClose();
    }

    @Test
    public void retrieveAvailableHousingLocationsNegative() {
        superIT.retrieveAvailableHousingLocationsNegative();
    }

    @Test
    public void retrieveAvailableHousingLocationsPositive() {
        superIT.retrieveAvailableHousingLocationsPositive();
    }

    @Test
    public void retrieveMatchedHousingLocationNegative() {
        superIT.retrieveMatchedHousingLocationNegative();
    }

    @Test
    public void retrieveMatchedHousingLocationPositive() {
        superIT.retrieveMatchedHousingLocationPositive();
    }

    @Test
    public void retrieveCurrentHousingLocationStatusNegative() {
        superIT.retrieveCurrentHousingLocationStatusNegative();
    }

    @Test
    public void retrieveCurrentHousingLocationStatusPositive() {
        superIT.retrieveCurrentHousingLocationStatusPositive();
    }

    @Test
    public void retrieveCurrentHousingTotalNegative() {
        superIT.retrieveCurrentHousingTotalNegative();
    }

    @Test
    public void retrieveCurrentHousingTotalPositive() {
        superIT.retrieveCurrentHousingTotalPositive();
    }

    @Test
    public void retrieveChildHousingTotalsNegative() {
        superIT.retrieveChildHousingTotalsNegative();
    }

    @Test
    public void retrieveChildHousingTotalsPositive() {
        superIT.retrieveChildHousingTotalsPositive();
    }

    @Test
    public void retrieveHousingLocationWithAvailableCapacitiesNegative() {
        superIT.retrieveHousingLocationWithAvailableCapacitiesNegative();
    }

    @Test
    public void retrieveHousingLocationWithAvailableCapacitiesPositive() {
        superIT.retrieveHousingLocationWithAvailableCapacitiesPositive();
    }

    @Test
    public void calculateHousingSuitabilityNegative() {
        superIT.calculateHousingSuitabilityNegative();
    }

    @Test
    public void calculateHousingSuitabilityPositive() {
        superIT.calculateHousingSuitabilityPositive();
    }

    @Test
    public void validateHousingAttributesNegative() {
        superIT.validateHousingAttributesNegative();
    }

    @Test
    public void validateHousingAttributesPositive() {
        superIT.validateHousingAttributesPositive();
    }

    @Test
    public void retrieveHousingLocationCapacitiesNegative() {
        superIT.retrieveHousingLocationCapacitiesNegative();
    }

    @Test
    public void retrieveHousingLocationCapacitiesPositive() {
        superIT.retrieveHousingLocationCapacitiesPositive();
    }

    @Test
    public void calculateHousingSuitabilitiesNegative() {
        superIT.calculateHousingSuitabilitiesNegative();
    }

    @Test
    public void calculateHousingSuitabilitiesPositive() {
        superIT.calculateHousingSuitabilitiesPositive();
    }

    @Test(enabled = false)
    public void testCreateChangeRequest() {
        superIT.testCreateChangeRequest();
    }

    @Test(enabled = false)
    public void testDefect1960() {
        superIT.testDefect1960();
    }

    @Test
    public void testDefect2336() {
        superIT.testDefect2336();
    }

    @Test(enabled = false)
    public void testRetrieveAvailableHousing() {
        superIT.testRetrieveAvailableHousing();
    }
}
