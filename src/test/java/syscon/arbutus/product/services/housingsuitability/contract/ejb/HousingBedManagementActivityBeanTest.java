package syscon.arbutus.product.services.housingsuitability.contract.ejb;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.ActivityTest;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.StaffTest;
import syscon.arbutus.product.services.common.SupervisionTest;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.LinkCodeType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.*;
import syscon.arbutus.product.services.housing.contract.interfaces.HousingService;
import syscon.arbutus.product.services.referencedata.contract.interfaces.ReferenceDataService;

import static org.testng.Assert.assertNotNull;

/**
 * This is for creating the HousingBedManagementActivity data for unit test only.
 * (Not use for IT test).
 *
 * @author byu
 */
public class HousingBedManagementActivityBeanTest extends BaseIT {

    private static final CodeType SECURITY_LEVEL_MIN = new CodeType(ReferenceSet.SECURITY_LEVEL.value(), "MIN");
    private static final CodeType SECURITY_LEVEL_MAX = new CodeType(ReferenceSet.SECURITY_LEVEL.value(), "MAX");
    private static final CodeType CODE_ASSESSMENT_RESULT_MIN = new CodeType(ReferenceSet.ASSESSMENT_RESULT.value(), "MIN");
    private static final String ASSESSMENT_RESULT_MIN = "MIN";
    private static final String SUITABILITY_SECLEVEL = "SECLEVEL";
    private static final String SUITABILITY_GENDER = "GENDER";
    private static final String SUITABILITY_AGE = "AGE";
    private static final CodeType CODE_SEX_F = new CodeType(ReferenceSet.SEX.value(), "F");
    private static final String SEX_F = "F";
    private static final CodeType OFFENDER_GENDER_F = new CodeType(ReferenceSet.OFFENDER_GENDER.value(), "F");
    private static final CodeType OFFENDER_GENDER_M = new CodeType(ReferenceSet.OFFENDER_GENDER.value(), "M");
    private static final CodeType CODE_AGE_RANGE_ADULT = new CodeType(ReferenceSet.AGE_RANGE_DESCRIPTION.value(), "ADULT");
    private static final CodeType CODE_AGE_RANGE_JUVENILE = new CodeType(ReferenceSet.AGE_RANGE_DESCRIPTION.value(), "JUVENILE");
    private static final String OUTCOME_SUTBL = "SUTBL";
    private static final String OUTCOME_WRNG = "WRNG";
    private static final String CASE_SENTENCE_STATUS_SENT = "SENTENCED";
    private static final String MOVEMENT_REASON_MED = "MED";
    @SuppressWarnings("unused")
    private static Logger log = LoggerFactory.getLogger(HousingBedManagementActivityBeanTest.class);
    private HousingService service;
    private ReferenceDataService refDataService = null;

    private StaffTest stfTest;
    private ActivityTest actTest;
    private SupervisionTest supTest;

    private Long assignedBy = 1L;
    private Long issuedBy = 1L;
    private Long facilityId;
    private Long activityId;
    private List<Long> locationIds;

    public HousingBedManagementActivityBeanTest(Long facilityId, List<Long> locIds) {
        lookupJNDILogin();
        refDataService = (ReferenceDataService) super.JNDILookUp(this.getClass(), ReferenceDataService.class);
        assertNotNull(refDataService);

        stfTest = new StaffTest();
        actTest = new ActivityTest();
        supTest = new SupervisionTest();

        //create staff
        Long staffId = stfTest.createStaff();
        assignedBy = staffId;
        issuedBy = staffId;

        //create facility
        this.facilityId = facilityId;

        this.locationIds = locIds;

        activityId = actTest.createActivityForHousing();

        initData();
    }

    public HousingBedManagementActivityBeanTest(HousingService service, ReferenceDataService refDataService, StaffTest stfTest, ActivityTest actTest,
            SupervisionTest supTest, Long facilityId, List<Long> locationIds) {
        this.service = service;
        this.refDataService = refDataService;
        this.stfTest = stfTest;
        this.actTest = actTest;
        this.supTest = supTest;
        this.facilityId = facilityId;
        this.locationIds = locationIds;
        //create staff
        Long staffId = stfTest.createStaff();
        assignedBy = staffId;
        issuedBy = staffId;

        activityId = actTest.createActivityForHousing();

        initData();
    }

    private static Set<CodeType> getSecurityLevelCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(SECURITY_LEVEL_MIN);
        ret.add(SECURITY_LEVEL_MAX);
        return ret;
    }

    private static Set<CodeType> getGenderCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(OFFENDER_GENDER_F);
        ret.add(OFFENDER_GENDER_M);
        return ret;
    }

    private static Set<CodeType> getAgeRangeCodes() {
        Set<CodeType> ret = new HashSet<CodeType>();
        ret.add(CODE_AGE_RANGE_ADULT);
        ret.add(CODE_AGE_RANGE_JUVENILE);
        return ret;
    }

    public static Set<HousingAttributeMismatchType> getMismatches() {

        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, getSecurityLevelCodes(),
                OUTCOME_SUTBL, null);

        HousingAttributeMismatchType mismatch2 = new HousingAttributeMismatchType(null, SUITABILITY_GENDER, CODE_SEX_F, getGenderCodes(), OUTCOME_WRNG, null);

        HousingAttributeMismatchType mismatch3 = new HousingAttributeMismatchType(null, SUITABILITY_AGE, CODE_AGE_RANGE_ADULT, getAgeRangeCodes(), OUTCOME_SUTBL, null);
        Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        assignmentMismatches.add(mismatch);
        assignmentMismatches.add(mismatch2);
        assignmentMismatches.add(mismatch3);

        return assignmentMismatches;

    }

    @BeforeClass
    public void beforeClass() {

        lookupJNDILogin();

    }

    //for release
    private void lookupJNDILogin() {

        //JNDI lookup
        service = (HousingService) super.JNDILookUp(this.getClass(), HousingService.class);

        //init user context and login
        uc = super.initUserContext();
    }

    public void deleteAll() {
        service.deleteAll(uc);
    }

    //@Test
    public void testDataCreation() {

        //clean db and reload meta data
        //initData();

        //Long toIdentifer = null;

        //set for configuration data.
        setHousingPeriodicReviewConfig();

        List<Long> supIds = new ArrayList<Long>();
        // Case 1 from housing suitability example 1

        //Need to update the location based on FIL data creation, use the cell Id only

        // assign offender 11-14 to location 1
        for (int i = 1; i <= 4; i++) {
            //toIdentifer = Long.valueOf(i+10);
            Long supId = createSupervision();
            supIds.add(supId);
            assignHousingLocation(supId, locationIds.get(0));
        }

        // create change request of offender 101-103 to location 2
        for (int i = 1; i <= 3; i++) {
            //toIdentifer = Long.valueOf(i+100);
            Long supId = createSupervision();
            supIds.add(supId);
            createChangeRequest(supId, locationIds.get(1), MOVEMENT_REASON_MED, true); //Approval Not Required
        }

        // Case 2 from housing suitability example 2
        // assign offender 21-28 to location 2
        for (int i = 1; i <= 8; i++) {
            //toIdentifer = Long.valueOf(i+20);
            Long supId = createSupervision();
            supIds.add(supId);
            assignHousingLocation(supId, locationIds.get(1));
        }

        // create change request of offender 101-104 to location 2
        for (int i = 1; i <= 4; i++) {
            //toIdentifer = Long.valueOf(i+100);
            Long supId = createSupervision();
            supIds.add(supId);
            createChangeRequest(supId, locationIds.get(1), MOVEMENT_REASON_MED, true); //Approval Not Required
        }

        // Case 3 from housing suitability example 3

        // assign offender 31-40 to location 4
        for (int i = 1; i <= 10; i++) {
            //toIdentifer = Long.valueOf(i+30);
            Long supId = createSupervision();
            supIds.add(supId);
            assignHousingLocation(supId, locationIds.get(3));
        }

        // create change request of offender 101-103 to location 4
        for (int i = 1; i <= 3; i++) {
            //toIdentifer = Long.valueOf(i+100);
            Long supId = createSupervision();
            supIds.add(supId);
            createChangeRequest(supId, locationIds.get(3), MOVEMENT_REASON_MED, true); //Approval Not Required
        }

    }

    private Long createSupervision() {
        return supTest.createSupervision(facilityId, true);
    }

    /**
     * Return assigned offender supervision Ids
     *
     * @param offenderIds
     * @param housingLocations
     * @return
     */
    public Set<Long> assignHousingLocationForConflict(List<Long> offenderIds, List<Long> housingList) {

        Collections.sort(offenderIds);

        Set<Long> ret = new HashSet<Long>();

        //List<Long> housingList = toSortedList(housingLocations);

        Long locationId = housingList.get(1);
        //Assigned each associated offender a location
        for (Long offenderId : offenderIds) {
            assignHousingLocation(offenderId, locationId);
            //assignHousingLocation(offenderId, 1000L);
            ret.add(offenderId);
        }

        //Each leaf location will assign one associated supervision.
        int i = 1;
        for (Long housingLocation : housingList) {
            if (locationId != housingLocation) {
                Long offenderId = offenderIds.get(i);
                assignHousingLocation(offenderId, housingLocation);
                ret.add(offenderId);
                i = i + 1;
            }
        }

        return ret;
    }

    /**
     * Return assigned offender supervision Ids
     *
     * @param offenderList
     * @param housingLocations
     * @return
     */
    public Set<Long> assignHousingLocation(List<Long> offenderList, List<Long> housingLocations) {

        Collections.sort(offenderList);

        int i = 1;

        Set<Long> ret = new HashSet<Long>();

        //Each location will assign 2 supervision.

        for (Long housingLocation : housingLocations) {

            for (int j = i; j <= i + 2; j++) {
                Long offenderId = offenderList.get(j);
                assignHousingLocation(offenderId, housingLocation);
                ret.add(offenderId);
            }
            i = i + 2;
        }

        return ret;

    }

    public void assignHousingLocation(Set<Long> offenderIds, Long housingLocation) {

        for (Long offenderId : offenderIds) {
            assignHousingLocation(offenderId, housingLocation);
        }

    }

    public List<Long> createChangeRequest(List<Long> housingLocations) {

        //List<Long> housingLocationList = toSortedList(housingLocations);

        //Long toIdentifer = null;
        List<Long> supIds = new ArrayList<Long>();
        // create change request of offender 101-103 to location 1
        for (int i = 1; i <= 3; i++) {
            //toIdentifer = Long.valueOf(i+100);
            Long supId = createSupervision();
            supIds.add(supId);
            createChangeRequest(supId, housingLocations.get(1), MOVEMENT_REASON_MED, true); //Approval Not Required
        }

        // create change request of offender 101-104 to location 2
        for (int i = 1; i <= 4; i++) {
            //toIdentifer = Long.valueOf(i+100);
            Long supId = createSupervision();
            supIds.add(supId);
            createChangeRequest(supId, housingLocations.get(2), MOVEMENT_REASON_MED, true); //Approval Not Required
        }

        // create change request of offender 101-103 to location 4
        for (int i = 1; i <= 3; i++) {
            //toIdentifer = Long.valueOf(i+100);
            Long supId = createSupervision();
            supIds.add(supId);
            createChangeRequest(supId, housingLocations.get(3), MOVEMENT_REASON_MED, true); //Approval Not Required
        }

        return supIds;

    }

    @Test
    public void testRetrieveAvailableHousingLocationsCreation() {

        //clean db and reload meta data
        initData();

        Long toIdentifer = null;

        //set for configuration data.
        setHousingPeriodicReviewConfig();

        // Case 1 from housing suitability example 1

        // assign offender 1161-1164 to location 1
        for (int i = 1611; i <= 261 + 3; i++) {
            toIdentifer = Long.valueOf(i);
            assignHousingLocation(toIdentifer, 68L);
        }

        // create change request of offender 101-103 to location 1
        for (int i = 1; i <= 3; i++) {
            toIdentifer = Long.valueOf(i + 100);
            createChangeRequest(toIdentifer, 68L, MOVEMENT_REASON_MED, true); //Approval Not Required
        }

        // Case 2 from housing suitability example 2
        // assign offender 1171-1178 to location 2
        for (int i = 1171; i <= 1171 + 7; i++) {
            toIdentifer = Long.valueOf(i);
            assignHousingLocation(toIdentifer, 69L);
        }

        // create change request of offender 101-104 to location 2
        for (int i = 1; i <= 4; i++) {
            toIdentifer = Long.valueOf(i + 100);
            createChangeRequest(toIdentifer, 69L, MOVEMENT_REASON_MED, true); //Approval Not Required
        }

        // Case 3 from housing suitability example 3

        // assign offender 1181-1190 to location 4
        for (int i = 1181; i <= 1181 + 9; i++) {
            toIdentifer = Long.valueOf(i);
            assignHousingLocation(toIdentifer, 73L);
        }

        // create change request of offender 101-103 to location 4
        for (int i = 1; i <= 3; i++) {
            toIdentifer = Long.valueOf(i + 100);
            createChangeRequest(toIdentifer, 73L, MOVEMENT_REASON_MED, true); //Approval Not Required
        }

        // Create this assignment for non-conflict offender assignment.
        int startNum = 1061;
        // assign offender 1061-1062 to location 20
        for (int i = startNum; i <= startNum + 2; i++) {
            toIdentifer = Long.valueOf(i);
            assignHousingLocation(toIdentifer, 20L);
        }

    }

    @Test
    public void testRetrieveCurrentHousingLocationStatusCreation() {

    }

    @Test
    public void testValidateHousingCreation() {

        // For housing suitability unit test data creation only

        //clean db and reload meta data
        initData();

        Long toIdentifer = null;

        //set for configuration data.
        setHousingPeriodicReviewConfig();

        // create change request of offender 1
        for (int i = 1; i <= 3; i++) {
            toIdentifer = Long.valueOf(i + 10);
            createChangeRequest(1L, toIdentifer, MOVEMENT_REASON_MED, true); //Approval Not Required
        }

    }

    @Test
    public void testCreation() {

        // Data for Non-Association conflict check.
        Long toIdentifer = null;
        int startNum = 1062;
        //set for configuration data.
        setHousingPeriodicReviewConfig();

        // assign offender to location 50
        for (int i = startNum; i <= startNum + 2; i++) {
            toIdentifer = Long.valueOf(i);
            assignHousingLocation(toIdentifer, 50L);
        }

        assignHousingLocation(1064L, 72L);
        assignHousingLocation(1065L, 73L);
    }

    @Test
    public void testCalculateHousingSuitabilityCreation() {

        //clean db and reload meta data
        initData();

        Long toIdentifer = null;

        //set for configuration data.
        setHousingPeriodicReviewConfig();

        // Case 1 from housing suitability example 1

        // assign offender 1-4 to location 2
        for (int i = 1; i <= 4; i++) {
            toIdentifer = Long.valueOf(i);
            assignHousingLocation(toIdentifer, 2L);
        }

        // create change request of offender 11-13 to location 2
        for (int i = 1; i <= 3; i++) {
            toIdentifer = Long.valueOf(i + 10);
            createChangeRequest(toIdentifer, 2L, MOVEMENT_REASON_MED, true); //Approval Not Required
        }

		/*
        // Case 2 from housing suitability example 2

		// assign offender 1-8 to location 2
		for (int i = 1; i <= 8; i++) {
			toIdentifer = Long.valueOf(i);
			assignHousingLocation(toIdentifer, 2L);
		}

		// create change request of offender 11-14 to location 2
		for (int i = 1; i <= 4; i++) {
			toIdentifer = Long.valueOf(i+10);
			createChangeRequest(toIdentifer, 2L, MOVEMENT_REASON_MED, true); //Approval Not Required
		}
		*/
        // Case 3 from housing suitability example 3
		/*
		// assign offender 1-10 to location 2
		for (int i = 1; i <= 10; i++) {
			toIdentifer = Long.valueOf(i);
			assignHousingLocation(toIdentifer, 2L);
		}

		// create change request of offender 11-13 to location 2
		for (int i = 1; i <= 3; i++) {
			toIdentifer = Long.valueOf(i+10);
			createChangeRequest(toIdentifer, 2L, MOVEMENT_REASON_MED, true); //Approval Not Required
		}
		*/
    }

    public void initData() {

        service.deleteAll(uc);

        //To be removed if default meta no link between these two reference codes.
        //Delete reference code link for movementReason=OFFREQ

        try {
            LinkCodeType link = new LinkCodeType();
            link.setReferenceSet(ReferenceSet.HOUSING_MOVEMENT_REASON.value());
            link.setReferenceCode("OFFREQ");
            link.setLinkedReferenceSet(ReferenceSet.APPROVAL_REQUIRED.value());
            link.setLinkedReferenceCode("NAPP");
            refDataService.deleteLink(uc, link);
            //Create new reference code link with Approval Required for movementReason=OFFREQ
            link = new LinkCodeType();
            link.setReferenceSet(ReferenceSet.HOUSING_MOVEMENT_REASON.value());
            link.setReferenceCode("OFFREQ");
            link.setLinkedReferenceSet(ReferenceSet.APPROVAL_REQUIRED.value());
            link.setLinkedReferenceCode("APP");
            refDataService.createLink(uc, link);
        } catch (Exception ex) {
            //log.error(ex.getMessage());
            // TODO: handle exception
        }

        /////////////////////////////////////////

        //set for configuration data.
        setHousingPeriodicReviewConfig();
    }

    private void setHousingPeriodicReviewConfig() {

        //Success
        HousingPeriodicReviewType housingPeriodicReview = new HousingPeriodicReviewType(180L, 7L);
        HousingPeriodicReviewType ret = service.setHousingPeriodicReviewConfig(uc, housingPeriodicReview);
        assert (ret != null);

    }

    public HousingBedMgmtActivityType assignHousingLocation(Long offenderId, Long locationId) {

        String movementReason = MOVEMENT_REASON_MED;

        // call assignHousingLocation
        HousingBedMgmtActivityType ret = assignHousingLocation(offenderId, locationId, movementReason);

        return ret;

    }

    private HousingBedMgmtActivityType assignHousingLocation(Long offenderId, Long locationId, String movementReasonCode) {

        String movementReason = movementReasonCode;

        String movementComment = "Housing Assinged comments" + offenderId;

        Long offenderReference = offenderId;
        Long locationTo = locationId;
        //Long assignedBy = offenderId;
        //Long facilityReference = 1L;

        // build HousingAttributeMismatch for assignment
        HousingAttributeMismatchType mismatch = new HousingAttributeMismatchType(null, SUITABILITY_SECLEVEL, CODE_ASSESSMENT_RESULT_MIN, getSecurityLevelCodes(),
                OUTCOME_SUTBL, activityId);

        Set<HousingAttributeMismatchType> assignmentMismatches = new HashSet<HousingAttributeMismatchType>();
        assignmentMismatches.add(mismatch);

        // build HousingLocationAssignType
        Date assignedDate = new Date();
        HousingLocationAssignType housingAssign = new HousingLocationAssignType(activityId, offenderReference, movementReason, locationTo, assignedBy, assignedDate,
                assignmentMismatches, OUTCOME_SUTBL, movementComment, facilityId);

        // call assignHousingLocation
        HousingBedMgmtActivityType ret = service.assignHousingLocation(uc, housingAssign);

        return ret;

    }

    /**
     * create change request on leaf only.
     *
     * @param offenderId
     * @param locationId
     * @return
     */
    public HousingBedMgmtActivityType createChangeRequest(Long offenderId, Long locationId) {

        Long offenderReference = offenderId;

        // build static association properties
        Long locationRequested = locationId;
        //Long issuedBy = offenderId;

        // build code type properties
        String crPriority = "HIGH";
        String crType = "INTRA";

        // build OffenderHousingAttribute
        String securityLevel = ASSESSMENT_RESULT_MIN;
        String gender = SEX_F;

        String sentenceStatuses = CASE_SENTENCE_STATUS_SENT;

        OffenderHousingAttributeType housingAttributesRequired = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatuses, null, null);

        String movementComment = "createChangeRequest comments" + offenderId;

        // build HousingLocationCRType
        HousingLocationCRType housingLocationCR = new HousingLocationCRType(activityId, offenderReference, MOVEMENT_REASON_MED, locationRequested, issuedBy, crPriority,
                crType, true, housingAttributesRequired, null, null, movementComment);

        HousingBedMgmtActivityType retCreate = service.createChangeRequest(uc, housingLocationCR);

        return retCreate;
    }

    private HousingBedMgmtActivityType createChangeRequest(Long offenderId, Long locationId, String movementReason, boolean isReservationFlag) {

        // Assign the offender to locationId + 1 first
        assignHousingLocation(offenderId, locationId + 1);

        // build housing bed mgmt activity properties
        //CodeType movementReason = MOVEMENT_REASON_OFFREQ;	//Approval Required
        //CodeType movementReason = MOVEMENT_REASON_MED; //Approval Not Required
        Long offenderReference = offenderId;

        // build static association properties
        Long locationRequested = locationId;
        //Long issuedBy = offenderId;

        // build code type properties
        String crPriority = "HIGH";
        String crType = "INTRA";

        // build OffenderHousingAttribute
        String securityLevel = ASSESSMENT_RESULT_MIN;
        String gender = SEX_F;

        String sentenceStatuses = CASE_SENTENCE_STATUS_SENT;

        OffenderHousingAttributeType housingAttributesRequired = new OffenderHousingAttributeType(securityLevel, gender, 20L, sentenceStatuses, null, null);

        String movementComment = "createChangeRequest comments" + offenderId;

        // build HousingLocationCRType
        HousingLocationCRType housingLocationCR = new HousingLocationCRType(activityId, offenderReference, movementReason, locationRequested, issuedBy, crPriority,
                crType, isReservationFlag, housingAttributesRequired, null, null, movementComment);

        HousingBedMgmtActivityType retCreate = service.createChangeRequest(uc, housingLocationCR);

        return retCreate;

    }

    @SuppressWarnings("unused")
    private List<Long> toSortedList(Set<Long> ids) {

        if (ids == null) {
			return null;
		}

        List<Long> ret = new ArrayList<Long>();

        for (Long id : ids) {
            ret.add(id);
        }

        Collections.sort(ret);

        return ret;
    }

}
