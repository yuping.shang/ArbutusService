package syscon.arbutus.product.services.housingsuitability.contract.dto;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.housing.contract.dto.housingsuitability.HousingCapacityType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class HousingCapacityTypeTest {

    @Test
    public void isValid() {

        HousingCapacityType data = new HousingCapacityType();
        assert (validate(data) == true);

        data = new HousingCapacityType(null, null, null, null, null, null, null, null, null, null);
        assert (validate(data) == false);

        //optimum is null
        data = new HousingCapacityType(null, 1L, 1L, 1L, 1L, 1L, 1L, 1L, 1L, 1L);
        assert (validate(data) == false);

        //overflow is null
        data = new HousingCapacityType(1L, null, 1L, 1L, 1L, 1L, 1L, 1L, 1L, 1L);
        assert (validate(data) == false);

        //occupied is null
        data = new HousingCapacityType(1L, 1L, null, 1L, 1L, 1L, 1L, 1L, 1L, 1L);
        assert (validate(data) == false);

        //reservation is null
        data = new HousingCapacityType(1L, 1L, 1L, null, 1L, 1L, 1L, 1L, 1L, 1L);
        assert (validate(data) == false);

        //available is null
        data = new HousingCapacityType(1L, 1L, 1L, 1L, null, 1L, 1L, 1L, 1L, 1L);
        assert (validate(data) == false);

        //reserved is null
        data = new HousingCapacityType(1L, 1L, 1L, 1L, 1L, null, 1L, 1L, 1L, 1L);
        assert (validate(data) == false);

        //availableOptimum is null
        data = new HousingCapacityType(1L, 1L, 1L, 1L, 1L, 1L, null, 1L, 1L, 1L);
        assert (validate(data) == false);

        //availableOverflow is null
        data = new HousingCapacityType(1L, 1L, 1L, 1L, 1L, 1L, 1L, null, 1L, 1L);
        assert (validate(data) == false);

        //occupiedOptimum is null
        data = new HousingCapacityType(1L, 1L, 1L, 1L, 1L, 1L, 1L, 1L, null, 1L);
        assert (validate(data) == false);

        //occupiedOverflow is null
        data = new HousingCapacityType(1L, 1L, 1L, 1L, 1L, 1L, 1L, 1L, 1L, null);
        assert (validate(data) == false);

        //success
        data = new HousingCapacityType(1L, 1L, 1L, 1L, 1L, 1L, 1L, 1L, 1L, 1L);
        assert (validate(data) == true);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }

}
