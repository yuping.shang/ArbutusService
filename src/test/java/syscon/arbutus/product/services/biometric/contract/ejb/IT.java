package syscon.arbutus.product.services.biometric.contract.ejb;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.biometric.contract.dto.*;
import syscon.arbutus.product.services.biometric.contract.ejb.ReferenceSet.BioDataFormatEnum;
import syscon.arbutus.product.services.biometric.contract.ejb.ReferenceSet.BioScanTypeEnum;
import syscon.arbutus.product.services.biometric.contract.interfaces.BiometricService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.PersonIdentityTest;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;

public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);
    Long parentId = null;
    private BiometricService service;
    private PersonIdentityTest piTest;

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (BiometricService) JNDILookUp(this.getClass(), BiometricService.class);
        uc = super.initUserContext();
        piTest = new PersonIdentityTest();

    }

    @BeforeMethod
    public void beforeMethod() {
        SecurityClient client;
        try {
            client = SecurityClientFactory.getSecurityClient();
            client.setSimple("devtest", "devtest"); // nice regular service user
            client.login();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void getCount() {

        Long cou = service.getCount(null);

        assert (cou.intValue() >= 0);
    }

    //@Test
    public void search() {

        PersonIdentityTest personIdentityTest = new PersonIdentityTest();
        PersonIdentityType personIdentity = personIdentityTest.createPersonIdentityType();

        Long personId = personIdentity.getPersonId();
        BiometricUserSearchType search = new BiometricUserSearchType(personId.toString(), null, null, null, null, null, null, null);
        BiometricsReturnType ret = service.search(uc, search, null, null, null);
        assert (ret.getBiometricTypes().size() > 1);
    }

    @Test
    public void createSubject() {

        PersonIdentityTest personIdentityTest = new PersonIdentityTest();
        PersonIdentityType personIdentity = personIdentityTest.createPersonIdentityType();

        Long personId = personIdentity.getPersonId();

        BioSubjectType bioSubject = new BioSubjectType();
        bioSubject.setAlertFlag("alert");
        bioSubject.setClusterHash(1L);
        bioSubject.setConsentGiven("Y");
        bioSubject.setPersonId(personId);
        bioSubject.setnTemplateUpdate(new Date());

        bioSubject = service.createSubject(uc, bioSubject);

        log.error("bioSubject  3 " + bioSubject);

        // BioSubject table will be populated by NServer 
        BioScanType scan = new BioScanType();
        scan.setScanType(BioScanTypeEnum.FINGERPRINT.code());
        scan.setOperatorLoginId("devtest");
        scan.setStationIP("127.0.0.1");

        Set<BioScanType> bioScans = new HashSet<BioScanType>();
        bioScans.add(scan);
        bioSubject.setBioScans(bioScans);

        // we dont need to create child, they are created by the paret
        //scan = service.createScan(uc, scan);

        bioSubject = service.updateSubject(uc, bioSubject);

        log.error("scan 1" + scan);

        bioSubject = service.getSubject(uc, bioSubject.getBioSubjectId());

        scan = bioSubject.getBioScans().iterator().next();

        scan = service.getScan(uc, scan.getBioScanId());

        log.error("bioSubject  4 " + bioSubject);
        log.error("scan 4 " + scan);

        List<BioFingerPrintType> fingerorintList = new ArrayList<BioFingerPrintType>(5);
        Set<BioFingerPrintType> outFingerorintList = new HashSet<BioFingerPrintType>(5);

        for (int i = 1; i < 6; i++) {

            BioFingerPrintType bioFingerPrint = new BioFingerPrintType();
            bioFingerPrint.setDataFormat(BioDataFormatEnum.FINGER_IMAGE.code());
            bioFingerPrint.setMasterDataRecord("1");
            bioFingerPrint.setFingerPosition(new Long(i));
            byte[] b = new byte[20];
            new Random().nextBytes(b);

            bioFingerPrint.setFingerPrintData(b);
            b = new byte[20];
            new Random().nextBytes(b);
            bioFingerPrint.setnRecord(b);

            log.error("bioFingerPrint " + bioFingerPrint);

            outFingerorintList.add(bioFingerPrint);
        }

        scan.setBioFingerPrints(outFingerorintList);

        scan = service.updateScan(uc, scan);
        log.error("scan 2" + scan);

        BioSubjectSearchType search = new BioSubjectSearchType();
        search.setPersonId(personId);
        BioSubjectsReturnType bioSubjects = service.searchSubject(uc, null, search, null, null, null);

        if (bioSubjects.getTotalSize().intValue() > 0) {
            BioSubjectType bioSubjectSearch = bioSubjects.getBioSubjectTypes().get(0);
            log.error("bioSubjectSearch 3" + bioSubjectSearch);

        }
    }

}
