package syscon.arbutus.product.services.security.contract.ejb;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.common.PersonIdentityTest;
import syscon.arbutus.product.services.core.common.Staff;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.registry.contract.dto.*;
import syscon.arbutus.product.services.registry.contract.interfaces.RegistryService;
import syscon.arbutus.product.services.security.contract.dto.*;
import syscon.arbutus.product.services.security.contract.interfaces.SecurityService;

import static org.testng.Assert.assertNotNull;

@ModuleConfig
public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);
    private SecurityService security;
    private RegistryService registry;
    private PersonService personService;

    private ComponentType testComponent;
    private ComponentType testProcess;
    private UserType testUser;
    private FacilitySetType testFacilitySet;
    private ComponentAliasType testComponentAlias;
    private ComponentAliasType testProcessAlias;
    private RoleType testRole;
    private Long personId;
    private Long personIdentityId;

    @AfterMethod
    public void afterMethod() {
    }

    @BeforeClass
    public void beforeClass() {

        log.info("beforeClass Begin - JNDI lookup");
        security = (SecurityService) JNDILookUp(this.getClass(), SecurityService.class);
        registry = (RegistryService) JNDILookUp(this.getClass(), RegistryService.class);
        personService = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        uc = super.initUserContext();

        SecurityClient client;
        try {
            client = SecurityClientFactory.getSecurityClient();
            client.setSimple("devtest", "devtest"); // nice regular service user
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }

        security.deleteAllUserData(uc);
        registry.deleteAllUserData(uc, true);

        init();
    }

    @AfterClass
    public void afterTest() {
        security.deleteAllUserData(uc);
        registry.deleteAllUserData(uc, true);
        if (personId != null) {
            personService.deletePersonTypeInternally(uc, personId);
        }
    }

    public void init() {
        PersonIdentityType personIdentity = (new PersonIdentityTest()).createPersonIdentityType();
        personId = personIdentity.getPersonId();
        personIdentityId = personIdentity.getPersonIdentityId();

        assertNotNull(personId);
        assertNotNull(personIdentityId);

        // Create component
        ComponentType inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.ACTIVITY.value(), "Activity"), "inquiryoffender",
                "Search Inmate Page", Long.valueOf(AttributeType.ATTRIBUTE_USER));
        testComponent = registry.createComponent(uc, inComponent);

        inComponent = new ComponentType(new ComponentCategoryType(ComponentCategoryEnum.PROCESS.value(), "Process"), "manageproperties", "Manage Properties",
                Long.valueOf(AttributeType.ATTRIBUTE_USER));
        testProcess = registry.createComponent(uc, inComponent);

        // Create component alias
        ComponentAliasType inComponentAlias = new ComponentAliasType(testComponent.getComponentId(), "inqueryperson", "Search Person Page", AttributeType.ATTRIBUTE_USER);
        testComponentAlias = registry.createComponentAlias(uc, inComponentAlias);

        inComponentAlias = new ComponentAliasType(testProcess.getComponentId(), "manageproperties", "Manage Properties", AttributeType.ATTRIBUTE_USER);
        testProcessAlias = registry.createComponentAlias(uc, inComponentAlias);

        // Create role
        RoleType inRole = new RoleType(RoleCategoryEnum.SERVICE.value(), "Arbutust", "Arbutus", AttributeType.ATTRIBUTE_USER);
        testRole = registry.createRole(uc, inRole);

        // Create facility set
        FacilitySetType inFacilitySet = new FacilitySetType("facilityset-a", "Facility Set A", AttributeType.ATTRIBUTE_USER);
        inFacilitySet.getFacilityList().add(1L);
        testFacilitySet = registry.createFacilitySet(uc, inFacilitySet);

        // Add component alias to role
        registry.addComponentAliasToRoleByName(uc, testRole.getRoleId(), testComponentAlias.getName());
        registry.addComponentAliasToRoleByName(uc, testRole.getRoleId(), testProcessAlias.getName());

        // Create user
        UserType inUser = new UserType(LoginModeType.LOGINMODE_DB, "jliang", AttributeType.ATTRIBUTE_USER);
        inUser.setPersonId(personId);
        inUser.setPersonIdentityId(personIdentityId);
        testUser = security.createUser(uc, inUser);

        // Create assignment
        AssignmentType inAssignment = new AssignmentType(testUser.getUserId(), PermissionType.PERMISSION_FULL);
        inAssignment.setFacilitySetId(testFacilitySet.getFacilitySetId());
        AssignmentRoleType inAssignmentRole = new AssignmentRoleType(testRole.getRoleId(), 1L);
        inAssignment.getRoleList().add(inAssignmentRole);
        AssignmentType outAssignment = security.createAssignment(uc, inAssignment);
    }

    @Test
    public void createUser() {
        UserType inUser = new UserType(LoginModeType.LOGINMODE_DB, "jliang3", AttributeType.ATTRIBUTE_USER);
        UserType outUser = security.createUser(uc, inUser);
        assert (outUser != null);
    }

    @Test
    public void createAssignment() {
        // Create user
        UserType inUser = new UserType(LoginModeType.LOGINMODE_DB, "jliang4", AttributeType.ATTRIBUTE_USER);
        UserType outUser = security.createUser(uc, inUser);
        assert (outUser != null);
        // Create assignment
        AssignmentType inAssignment = new AssignmentType(outUser.getUserId(), PermissionType.PERMISSION_FULL);
        AssignmentRoleType inAssignmentRole = new AssignmentRoleType(1L, 1L);
        inAssignment.getRoleList().add(inAssignmentRole);
        AssignmentType outAssignment = security.createAssignment(uc, inAssignment);
        assert (outAssignment != null);
        assert (outAssignment.getRoleList().size() == 1);
    }

    @Test
    public void isAllowedActivity() {
        boolean isAllowedActivity = security.isAllowedActivity(uc, testUser.getLoginId(), testComponent.getName());
        assert (isAllowedActivity);
        //
        isAllowedActivity = security.isAllowedActivity(uc, testUser.getLoginId(), "DoesNotExist");
        assert (!isAllowedActivity);
    }

    @Test
    public void getAllAllowedFacilityIds() {
        Set<Long> outFacilityIds = security.getAllAllowedFacilityIds(uc, testUser.getLoginId());
        assert (outFacilityIds != null);
        assert (outFacilityIds.size() == 1);
    }

    @Test
    public void getAllAllowedFacilityIdsOfFacilitySet() {
        List<Long> outFacilityIds = security.getAllAllowedFacilityIds(uc, testUser.getLoginId(), testFacilitySet.getFacilitySetId());
        assert (outFacilityIds != null);
        assert (outFacilityIds.size() == 1);
    }

    @Test
    public void getDefaultFacilityId() {
        Long outFacilityId = security.getDefaultFacilityId(uc, testUser.getLoginId());
        assert (outFacilityId != null);
    }

    @Test
    public void getDefaultFacilityIdOfFacilitySet() {
        Long outFacilityId = security.getDefaultFacilityId(uc, testUser.getLoginId(), testFacilitySet.getFacilitySetId());
        assert (outFacilityId != null);
    }

    @Test
    public void getAllAllowedRoleNames() {
        Set<String> outRoleNames = security.getAllowedRoleNames(uc, testUser.getLoginId());
        assert (outRoleNames != null);
        assert (outRoleNames.size() == 1);
    }

    @Test
    public void getAllowedRoleNamesOfFacilitySet() {
        List<String> outRoleNames = security.getAllowedRoleNames(uc, testUser.getLoginId(), testFacilitySet.getFacilitySetId());
        assert (outRoleNames != null);
        assert (outRoleNames.size() == 1);
    }

    @Test
    public void getAllAllowedFacilitySets() {
        List<FacilitySetType> outFacilitySetList = security.getAllowedFacilitySets(uc, testUser.getLoginId());
        assert (outFacilitySetList != null);
        assert (outFacilitySetList.size() == 1);
    }

    @Test
    public void getAllowedComponentAlias() {
        Map<String, Long> map = security.getAllowedComponentAlias(null, testUser.getLoginId(), testFacilitySet.getFacilitySetId());
        assert (map != null);
        assert (map.size() == 2);
    }

    @Test
    public void getAllowedRoleNames() {
        List<String> outRoles = security.getAllowedRoleNames(null, testUser.getLoginId(), testFacilitySet.getFacilitySetId());
        assert (outRoles != null);
        assert (outRoles.size() == 1);
    }

    @Test
    public void getTaskPermission() {
        Long outPermission = security.getTaskPermission(uc, testUser.getLoginId(), testFacilitySet.getFacilitySetId(), testComponentAlias.getName());
        assert (outPermission != null);
    }

    @Test
    public void getAllowedProcessNamesOfFacilitySet() {
        List<String> outProcessNameList = security.getAllowedProcessNames(uc, testUser.getLoginId(), testFacilitySet.getFacilitySetId());
        assert (outProcessNameList != null);
        assert (outProcessNameList.size() == 1);
    }

    @Test
    public void getAllowedActivityNamesOfFacilitySet() {
        List<String> allowedActivityNames = security.getAllowedActivityNames(uc, testUser.getLoginId(), testFacilitySet.getFacilitySetId());
        assert (allowedActivityNames != null);
        assert (allowedActivityNames.size() == 1);
    }

    @Test
    public void getAllowedProcessOfFacilitySet() {
        Map<String, Long> outProcessNameList = security.getAllowedComponentByCategory(uc, testUser.getLoginId(), testFacilitySet.getFacilitySetId(),
                ComponentCategoryEnum.PROCESS);
        assert (outProcessNameList != null);
        assert (outProcessNameList.size() == 1);
    }

    @Test
    public void getAllowedCautionCategoryCodesOfFacilitySet() {
        Map<String, Long> outProcessNameList = security.getAllowedComponentByCategory(uc, testUser.getLoginId(), testFacilitySet.getFacilitySetId(),
                ComponentCategoryEnum.CAUTION_CATEGORY);
        assert (outProcessNameList != null);
        assert (outProcessNameList.size() == 0);
    }

    @Test
    public void getAllowedShiftLogTypeCodesOfFacilitySet() {
        Map<String, Long> outProcessNameList = security.getAllowedComponentByCategory(uc, testUser.getLoginId(), testFacilitySet.getFacilitySetId(),
                ComponentCategoryEnum.SHIFTLOG_TYPE);
        assert (outProcessNameList != null);
        assert (outProcessNameList.size() == 0);
    }

    @Test
    public void getAllowedTaskRetrievalFlags() {
        Map<String, Long> allowedTaskRetrievalFlags = security.getAllowedTaskRetrievalFlags(uc, testUser.getLoginId(), testFacilitySet.getFacilitySetId());
        assert (allowedTaskRetrievalFlags != null);
        assert (allowedTaskRetrievalFlags.size() == 1);
    }

    @Test
    public void getAllUserDefaultRoles() {

        Map<String, List<String>> users = security.getAllUserDefaultRoles(uc);
        assert (users != null);
        assert (!users.isEmpty());
    }

    @Test
    public void getUserDefaultRoles() {
        List<String> roles = security.getUserDefaultRoles(uc, testUser.getLoginId());
        assert (roles != null);
        assert (!roles.isEmpty());
    }

    @Test
    public void getUserWildCard() {
        UserType inUser = new UserType(LoginModeType.LOGINMODE_DB, "UserWildCardTestUser", AttributeType.ATTRIBUTE_USER);
        inUser.setPersonId(personId);
        inUser.setPersonIdentityId(personIdentityId);
        inUser.setUserStatus(UserStatusType.USERSTATUS_ACTIVE);

        security.createUser(uc, inUser);

        List<UserType> users = security.getUserByWildCard(uc, "U*W*");
        assert (users != null);
    }

    @Test
    public void createJobTemplate() {

        JobTemplateType jobTemplate = new JobTemplateType();
        jobTemplate.setName("Jobtemplate-2");
        jobTemplate.setDescription("Jobtemplate-2-desc");

        JobTemplateRoleType rt = new JobTemplateRoleType();
        rt.setRoleId(5L);
        rt.setRWAFlag(2L);

        jobTemplate.getJobTemplateRoles().add(rt);

        JobTemplateType outJt = security.createJobTemplate(uc, jobTemplate);
        assert (outJt != null);
        assert (outJt.getJobTemplateRoles().size() == 1);

        JobTemplateType outJt2 = security.getJobTemplate(uc, outJt.getJobTemplateId());
        assert (outJt2.getJobTemplateRoles().size() == 1);

        JobTemplateRoleType jTRole = outJt2.getJobTemplateRoles().iterator().next();

        // Create user and add Jobtemplate to its AssignmentRole

        UserType inUser = new UserType(LoginModeType.LOGINMODE_DB, "JobTemplateTestUser", AttributeType.ATTRIBUTE_USER);
        UserType outUser = security.createUser(uc, inUser);
        assert (outUser != null);

        // Create assignment
        AssignmentType inAssignment = new AssignmentType(outUser.getUserId(), PermissionType.PERMISSION_FULL);
        AssignmentRoleType inAssignmentRole = new AssignmentRoleType(jTRole.getRoleId(), jTRole.getRWAFlag());
        inAssignment.getRoleList().add(inAssignmentRole);
        AssignmentType outAssignment = security.createAssignment(uc, inAssignment);

        assert (outAssignment != null);
        assert (outAssignment.getRoleList().size() == 1);
    }

    @Test
    public void getFacilitySetStaff() {
        List<Staff> staff = security.getFacilitySetStaff(uc, testFacilitySet.getFacilitySetId());
        assert (staff != null);
        assert (staff.size() == 1);
    }

    @Test
    public void validatePasswordPoliciesForRegularUser() {

        UserType userType = new UserType();
        userType.setUserId(2l);
        userType.setLoginId("mbr,ow.n-wor9_asw#pa@	with");
        userType.setPassword("pa@Asword18");
        boolean bool = security.validatePasswordByPolicies(uc, UserAccountType.REGULAR, userType, Boolean.TRUE);
        assert (!bool);
    }

    @Test
    public void validatePasswordPoliciesForRegularUser1() {

        UserType userType = new UserType();
        userType.setUserId(2l);
        userType.setLoginId("mbr#tttttTt9");
        userType.setPassword("pa@mbrrPd18");
        boolean bool = security.validatePasswordByPolicies(uc, UserAccountType.REGULAR, userType, Boolean.FALSE);
        assert (!bool);
    }

    @Test
    public void validatePasswordPoliciesForRegularUser2() {

        UserType userType = new UserType();
        userType.setUserId(2l);
        userType.setLoginId("mbrown");
        userType.setPassword("pa@Asword18");
        boolean bool = security.validatePasswordByPolicies(uc, UserAccountType.REGULAR, userType, Boolean.TRUE);
        assert (bool);
    }

    @Test
    public void validatePasswordPoliciesForAdminUser() {

        UserType userType = new UserType();
        userType.setLoginId("mbrown");
        userType.setUserId(1l);
        userType.setPassword("pa@mbrownord9");
        boolean bool = security.validatePasswordByPolicies(uc, UserAccountType.ADMINISTRATOR, userType, Boolean.FALSE);
        assert (!bool);
    }

}