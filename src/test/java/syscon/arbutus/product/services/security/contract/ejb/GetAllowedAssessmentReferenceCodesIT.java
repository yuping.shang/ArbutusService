package syscon.arbutus.product.services.security.contract.ejb;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.registry.contract.dto.ComponentCategoryEnum;
import syscon.arbutus.product.services.security.contract.interfaces.SecurityService;

/**
 * This IT test is supposed to be run manually after security data is loaded by running registry tool.
 *
 * @author LHan
 */
@ModuleConfig
public class GetAllowedAssessmentReferenceCodesIT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(GetAllowedAssessmentReferenceCodesIT.class);
    private SecurityService security;

    @AfterMethod
    public void afterMethod() {
    }

    @BeforeClass
    public void beforeClass() {

        log.info("beforeClass Begin - JNDI lookup");
        security = (SecurityService) JNDILookUp(this.getClass(), SecurityService.class);
        uc = super.initUserContext();

    }

    @Test(enabled = false)
    public void getAllowedAssessmentCategoryReferenceCodes() {
        Map<String, Long> outProcessNameList = security.getAllowedComponentByCategory(uc, "lisa", 109L, ComponentCategoryEnum.ASSESSMENT_CATEGORY);
        assert (outProcessNameList != null);
        assert (outProcessNameList.size() == 1);

        outProcessNameList = security.getAllowedComponentByCategory(uc, "lhan", 102L, ComponentCategoryEnum.ASSESSMENT_CATEGORY);
        assert (outProcessNameList != null);
        assert (outProcessNameList.size() == 3);

        outProcessNameList = security.getAllowedComponentByCategory(uc, "lisa", 102L, ComponentCategoryEnum.ASSESSMENT_CATEGORY);
        assert (outProcessNameList != null);
        assert (outProcessNameList.size() == 0);

    }

    @Test(enabled = false)
    public void getAllowedAssessmentStatusReferenceCodes() {
        Map<String, Long> outProcessNameList = security.getAllowedComponentByCategory(uc, "lisa", 109L, ComponentCategoryEnum.ASSESSMENT_STATUS);
        assert (outProcessNameList != null);
        assert (outProcessNameList.size() == 4);

        outProcessNameList = security.getAllowedComponentByCategory(uc, "lhan", 102L, ComponentCategoryEnum.ASSESSMENT_STATUS);
        assert (outProcessNameList != null);
        assert (outProcessNameList.size() == 5);

        outProcessNameList = security.getAllowedComponentByCategory(uc, "lisa", 102L, ComponentCategoryEnum.ASSESSMENT_STATUS);
        assert (outProcessNameList != null);
        assert (outProcessNameList.size() == 0);
    }

}