package syscon.arbutus.product.services.personidentity.contract.ejb;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;

public class StressTest {

    PersonService service;

    @BeforeMethod
    public void beforeMethod() {
        //
        //service.reset(null);
        //OffNumConfigType offNumConfig = new OffNumConfigType("NY 0000000000", true);
        //service.createOffNumConfig(null, offNumConfig);   
    }

    @AfterMethod
    public void afterMethod() {
    }

    @BeforeClass
    public void beforeClass() {
        BasicConfigurator.configure();

        Properties properties = new Properties();
        properties.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
        properties.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
        properties.put("java.naming.provider.url", "localhost");
        Context ctx;
        try {
            ctx = new InitialContext(properties);
            this.service = (PersonService) ctx.lookup("PersonServiceBean");
        } catch (NamingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //    @SuppressWarnings("deprecation")
    //    @Test
    //    public void create() {
    //
    //        PersonIdentityReturnType pret = null;
    //        PersonIdentityType personIdentity = null;
    //        Date dob = new Date(1985-1900, 3, 10);
    //        Set<AssociationType> references = null;
    //
    //        //
    //        references = new HashSet<AssociationType>();
    //        references.add(new AssociationType("Person", 1L));
    //        Set<String> privileges = new HashSet<String>();
    //        privileges.add("Sealed");
    //        privileges.add("Confidential");
    //        Set<IdentifierType> identifiers = null;
    //
    //        identifiers = new HashSet<IdentifierType>();
    //        String comment1 = "Comment 1";
    //        String comment2 = "Comment 2";
    //        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980-1900, 3, 10), new Date(1975 -1900, 3, 10), "G", "XXXX-XX", comment1));
    //
    //        for (int i = 1000000; i < 10000000; i ++) {
    //            references = new HashSet<AssociationType>();
    //            references.add(new AssociationType("Person", (long)i));
    //	        personIdentity = new PersonIdentityType(null, null, null, "Jason" + i, "Ji", "Xin", "Liang" + i, "I", dob, "M", identifiers, null, null, comment2, references, privileges);
    //	        pret = service.create(null, personIdentity, true, true);
    //        }
    //
    //    }

}
