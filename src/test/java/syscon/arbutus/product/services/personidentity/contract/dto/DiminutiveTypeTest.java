package syscon.arbutus.product.services.personidentity.contract.dto;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.person.contract.dto.personidentity.DiminutiveType;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class DiminutiveTypeTest {

    @Test
    public void isValid() {

        DiminutiveType data = new DiminutiveType();
        assert (validate(data) == false);

        //data = new DiminutiveType();
        data.setLanguageCode("en");
        assert (validate(data) == false);
        Set<String> names = new HashSet<String>();
        names.add("name2");
        //names.add("Name1123456789012345678901234567890123456789012345678901234567890");
        data.setNames(names);
        assert (validate(data) == true);

    }

    private boolean validate(Object data) {

        boolean ret = false;
        try {
            ValidationHelper.validate(data);
            ret = true;

        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        return ret;
    }
}
