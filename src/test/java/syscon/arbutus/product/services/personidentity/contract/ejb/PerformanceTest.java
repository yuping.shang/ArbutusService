package syscon.arbutus.product.services.personidentity.contract.ejb;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;

public class PerformanceTest {

    PersonService service;

    @BeforeMethod
    public void beforeMethod() {
    }

    @AfterMethod
    public void afterMethod() {
    }

    @BeforeClass
    public void beforeClass() {
        BasicConfigurator.configure();

        Properties properties = new Properties();
        properties.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
        properties.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
        properties.put("java.naming.provider.url", "localhost");
        Context ctx;
        try {
            ctx = new InitialContext(properties);
            this.service = (PersonService) ctx.lookup("PersonServiceBean");
        } catch (NamingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //    @Test
    //    public void partialSearch() {
    //
    //        IdentifierSearchType identifierSearch = null;
    //        PersonIdentitySearchType personIdentitySearch = null;
    //        PersonIdentitiesReturnType psret = null;
    //
    //        personIdentitySearch = new PersonIdentitySearchType(null, null, "Jason150000", null, null, "Liang150000", null, null, null, null, null, null, null, null);
    //        identifierSearch = new IdentifierSearchType("12345", null, null, null, null, null, null, null);
    //        personIdentitySearch.setIdentifier(identifierSearch);
    //        psret = service.search(null, null, personIdentitySearch, SearchMatchModeType.PARTIAL.value(), true, null);
    //        assert(psret.getReturnCode().equals(ReturnCode.SUCCESS.returnCode()));
    //        assert(psret.getCount() == 1);
    //    }
    //
    //    @Test
    //    public void wildcardSearch() {
    //
    //        IdentifierSearchType identifierSearch = null;
    //        PersonIdentitySearchType personIdentitySearch = null;
    //        PersonIdentitiesReturnType psret = null;
    //
    //        personIdentitySearch = new PersonIdentitySearchType(null, null, "Jason15*0000", null, null, "Liang*150000", null, null, null, null, null, null, null, null);
    //        identifierSearch = new IdentifierSearchType("12345", null, null, null, null, null, null, null);
    //        personIdentitySearch.setIdentifier(identifierSearch);
    //        psret = service.search(null, null, personIdentitySearch, SearchMatchModeType.WILDCARD.value(), true, null);
    //        assert(psret.getReturnCode().equals(ReturnCode.SUCCESS.returnCode()));
    //        assert(psret.getCount() == 1);
    //    }
    //
    //    @Test
    //    public void nameswitchSearch() {
    //
    //        IdentifierSearchType identifierSearch = null;
    //        PersonIdentitySearchType personIdentitySearch = null;
    //        PersonIdentitiesReturnType psret = null;
    //
    //        personIdentitySearch = new PersonIdentitySearchType(null, null, "Liang150000", null, null, "Jason150000", null, null, null, null, null, null, null, null);
    //        identifierSearch = new IdentifierSearchType("12345", null, null, null, null, null, null, null);
    //        personIdentitySearch.setIdentifier(identifierSearch);
    //        psret = service.search(null, null, personIdentitySearch, SearchMatchModeType.NAMESWITCHES.value(), true, null);
    //        assert(psret.getReturnCode().equals(ReturnCode.SUCCESS.returnCode()));
    //        assert(psret.getCount() == 1);
    //    }
    //
    //    @Test
    //    public void namesdiminutiveSearch() {
    //
    //        IdentifierSearchType identifierSearch = null;
    //        PersonIdentitySearchType personIdentitySearch = null;
    //        PersonIdentitiesReturnType psret = null;
    //
    //        personIdentitySearch = new PersonIdentitySearchType(null, null, "Jason150000", null, null, "Liang150000", null, null, null, null, null, null, null, null);
    //        identifierSearch = new IdentifierSearchType("12345", null, null, null, null, null, null, null);
    //        personIdentitySearch.setIdentifier(identifierSearch);
    //        psret = service.search(null, null, personIdentitySearch, SearchMatchModeType.NAMEDIMINUTIVE.value(), true, null);
    //        assert(psret.getReturnCode().equals(ReturnCode.SUCCESS.returnCode()));
    //        assert(psret.getCount() == 1);
    //    }
    //
    //    @Test
    //    public void soundexSearch() {
    //
    //        IdentifierSearchType identifierSearch = null;
    //        PersonIdentitySearchType personIdentitySearch = null;
    //        PersonIdentitiesReturnType psret = null;
    //
    //        personIdentitySearch = new PersonIdentitySearchType(null, null, "Jason150000", null, null, "Liang150000", null, null, null, null, null, null, null, null);
    //        identifierSearch = new IdentifierSearchType("12345", null, null, null, null, null, null, null);
    //        personIdentitySearch.setIdentifier(identifierSearch);
    //        psret = service.search(null, null, personIdentitySearch, SearchMatchModeType.SOUNDEX.value(), true, null);
    //        assert(psret.getReturnCode().equals(ReturnCode.SUCCESS.returnCode()));
    //        assert(psret.getCount() == 1);
    //    }
}
