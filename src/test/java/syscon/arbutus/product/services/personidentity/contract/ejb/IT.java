package syscon.arbutus.product.services.personidentity.contract.ejb;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.common.PersonTest;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.ArbutusOptimisticLockException;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.person.contract.dto.personidentity.*;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;

import static org.testng.Assert.assertEquals;

@ModuleConfig
public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);
    private PersonService service;
    private FacilityService facilityService;
    private ActivityService activityService;

    private PersonTest psnTest;

    private Long personId;

    @BeforeMethod
    public void beforeMethod() {

        //reset();
    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        facilityService = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        activityService = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        uc = super.initUserContext();

        psnTest = new PersonTest();

        personId = psnTest.createPerson();

        //activityService.deleteAll(uc);
        //facilityService.deleteAll(uc);
    }

    @AfterClass
    public void afterClass() {
       // psnTest.deleteAll();

    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void getCount() {

        //
        PersonIdentityType pret = null;
        PersonIdentityType personIdentity = null;
        Date dob = new Date(1985 - 1900, 3, 10);
        Set<IdentifierType> identifiers = null;

        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, personId, null, null, "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, null, null, null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, false);
        assert (pret != null);

        //
        Long lret = null;
        lret = service.getPersonIdentityTypeCount(uc);
        assert (lret.equals(1L));
    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void get() {

        //
        PersonIdentityType pret = null;
        PersonIdentityType personIdentity = null;
        Date dob = new Date(1985 - 1900, 3, 10);
        Set<IdentifierType> identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, personId, null, null, "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, null, null, null);

        pret = service.createPersonIdentityType(uc, personIdentity, true, false);
        assert (pret != null);
        Long personIdentityId = pret.getPersonIdentityId();

        //
        pret = service.getPersonIdentityType(null, personIdentityId, true);
        assert (pret != null);
    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void create() {

        //
        PersonIdentityType pret = null;
        PersonIdentityType personIdentity = null;
        Date dob = new Date(1985 - 1900, 3, 10);
        Set<IdentifierType> identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "sin", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, personId, null, null, "Jason", "Ji", "Xin", "Liang", "i", dob, "M", identifiers, null, "offenderNumTest", null,
                null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, false);
        assert (pret != null);
    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void primaryId() {

        //
        PersonIdentityType pret = null;
        PersonIdentityType personIdentity = null;
        Date dob = new Date(1985 - 1900, 3, 10);
        Set<IdentifierType> identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "sin", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, personId, null, null, "Imran", "M", "Rizvi", "irizvi", "i", dob, "M", identifiers, null, "offenderNumTest", null,
                true);

        PersonIdentityType pret1 = service.createPersonIdentityType(uc, personIdentity, true, false);
        assert (pret1 != null);
        assert (pret1.isPrimaryId() == true);

        pret1.setPrimaryId(false);
        service.updatePersonIdentityType(uc, pret1, true, false);
        pret1 = service.getPersonIdentityType(uc, pret1.getPersonIdentityId(), Boolean.FALSE);
        assert (pret1 != null);
        assert (pret1.isPrimaryId() == false);

        pret1.setComment("sdfsdf124ewasdffds");
        try {
            service.updatePersonIdentityType(uc, pret1, true, false);
        } catch (Exception exp) {
            assert (exp.getClass().equals(ArbutusOptimisticLockException.class));
        }

        pret = null;
        personIdentity = null;
        dob = new Date(1985 - 1900, 3, 10);
        identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "sin", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, personId, null, null, "Imran", "M", "Rizvi", "irizvi", "i", dob, "M", identifiers, null, "offenderNumTest", null,
                true);

        pret = service.createPersonIdentityType(uc, personIdentity, true, true);
        assert (pret != null);
        assert (pret.isPrimaryId() == true);

        PersonIdentityType pret2 = service.getPersonIdentityType(uc, pret1.getPersonIdentityId(), true);
        assert (pret2.isPrimaryId() == false);

        pret = null;
        personIdentity = null;
        dob = new Date(1985 - 1900, 3, 10);
        identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "123s45", "sin", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, personId, null, null, "Imran", "M", "Rizvsi", "iriddzvi", "i", dob, "M", identifiers, null, "offenderNumTest", null,
                null);

        pret = service.createPersonIdentityType(uc, personIdentity, true, true);
        assert (pret != null);
        assert (pret.isPrimaryId() == false);

        pret = null;
        personIdentity = null;
        dob = new Date(1985 - 1900, 3, 10);
        identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "123s45", "sin", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, personId, null, null, "Imran", "M", "Rizvsi", "iriddzvi", "i", dob, "M", identifiers, null, "offenderNumTest", null,
                null);

        pret = service.createPersonIdentityType(uc, personIdentity, true, true);
        assert (pret != null);
        assert (pret.isPrimaryId() == false);

    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void update() {

        //
        PersonIdentityType pret = null;
        PersonIdentityType personIdentity = null;
        Date dob = new Date(1985 - 1900, 3, 10);
        Set<IdentifierType> identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, personId, null, null, "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, null, null, null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, false);
        assert (pret != null);

        //
        pret = service.getPersonIdentityType(uc, pret.getPersonIdentityId(), Boolean.FALSE);
        pret.setComment("test1");
        service.updatePersonIdentityType(uc, pret, true, false);

        try {
            pret.setComment("test2");
            service.updatePersonIdentityType(uc, pret, true, false);
        } catch (Exception exp) {
            assert (exp.getClass().equals(ArbutusOptimisticLockException.class));
        }
    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void isUniqueOffenderNumber() {

        //
        PersonIdentityType pret = null;
        PersonIdentityType personIdentity = null;
        Date dob = new Date(1985 - 1900, 3, 10);
        Set<IdentifierType> identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, personId, null, null, "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, "XYZ", null, null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, false);
        assert (pret != null);

        assertEquals(false, service.isUniqueOffenderNumber(uc, personIdentity.getOffenderNumber()));

        assertEquals(true, service.isUniqueOffenderNumber(uc, "ABC"));
    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void search() {

        //

        Long person1 = psnTest.createPerson();
        Long person2 = psnTest.createPerson();
        PersonIdentityType pret = null;
        PersonIdentityType personIdentity = null;
        Date dob = new Date(1985 - 1900, 3, 10);
        Set<IdentifierType> identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, person1, null, null, "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, null, null, null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, false);
        assert (pret != null);

        //
        PersonIdentitiesReturnType psret = null;

        //
        PersonIdentitySearchType personIdentitySearch = new PersonIdentitySearchType();
        IdentifierSearchType identifierSearch = new IdentifierSearchType("12345", null, null, null, null, null, null, null);
        personIdentitySearch.setIdentifier(identifierSearch);
        psret = service.searchPersonIdentityType(uc, null, personIdentitySearch, SearchMatchModeType.EXACT.value(), null, null);
        assert (psret != null);
        assert (psret.getPersonIdentities().size() > 0);

        //search by personIds
        personIdentitySearch = new PersonIdentitySearchType();
        Set<Long> personIds = new HashSet<Long>();
        personIds.add(person1);
        personIds.add(person2);
        personIdentitySearch.setPersonIds(personIds);
        psret = service.searchPersonIdentityType(uc, null, personIdentitySearch, SearchMatchModeType.EXACT.value(), null, null);
        assert (psret.getPersonIdentities().size() > 0);

    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void searchWildcard() {

        //
        PersonIdentityType pret = null;
        PersonIdentityType personIdentity = null;
        Date dob = new Date(1985 - 1900, 3, 10);
        Set<IdentifierType> identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, personId, null, null, "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, null, null, null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, false);
        assert (pret != null);

        //
        PersonIdentitiesReturnType psret = null;

        //
        PersonIdentitySearchType personIdentitySearch = new PersonIdentitySearchType();
        IdentifierSearchType identifierSearch = new IdentifierSearchType("12*45", null, null, null, null, null, null, null);
        personIdentitySearch.setIdentifier(identifierSearch);
        psret = service.searchPersonIdentityType(uc, null, personIdentitySearch, SearchMatchModeType.EXACT.value(), null, null);
        assert (psret != null);
        assert (psret.getPersonIdentities().size() > 0);

        identifierSearch = new IdentifierSearchType("12****", null, null, null, null, null, null, null);
        personIdentitySearch.setIdentifier(identifierSearch);
        psret = service.searchPersonIdentityType(uc, null, personIdentitySearch, SearchMatchModeType.EXACT.value(), null, null);
        assert (psret.getPersonIdentities().size() > 0);
        assert (psret != null);

        identifierSearch = new IdentifierSearchType("R*", null, null, null, null, null, null, null);
        personIdentitySearch.setIdentifier(identifierSearch);
        psret = service.searchPersonIdentityType(uc, null, personIdentitySearch, SearchMatchModeType.EXACT.value(), null, null);
        assert (psret.getPersonIdentities().size() >= 0);
        assert (psret != null);

        identifierSearch = new IdentifierSearchType("*3*", null, null, null, null, null, null, null);
        personIdentitySearch.setIdentifier(identifierSearch);
        psret = service.searchPersonIdentityType(uc, null, personIdentitySearch, SearchMatchModeType.PATTERN.value(), null, null);
        assert (psret.getPersonIdentities().size() > 0);
        assert (psret != null);
    }

    @Test(dependsOnMethods = { "search" })
    public void searchDefect_504() {

        search();

        PersonIdentityType pret = null;

        PersonIdentityType personIdentity = createPersonIdentity(null, null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, false);
        assert (pret != null);

        //
        PersonIdentitySearchType personIdentitySearch = new PersonIdentitySearchType();
        personIdentitySearch.setNamePrefix("MR");

        PersonIdentitiesReturnType psret = null;
        psret = service.searchPersonIdentityType(uc, null, personIdentitySearch, SearchMatchModeType.PARTIAL.value(), null, null);
        assert (psret != null);

        IdentifierSearchType identifierSearchType = new IdentifierSearchType();
        identifierSearchType.setCategory("PASS");
        personIdentitySearch.setIdentifier(identifierSearchType);

        psret = service.searchPersonIdentityType(uc, null, personIdentitySearch, SearchMatchModeType.EXACT.value(), null, null);
        assert (psret != null);
    }

    @Test(dependsOnMethods = { "searchDefect_504" })
    public void searchDefect_501() {

        service.createPersonIdentityType(uc, createPersonIdentity(getDate(1966, 3, 22), "offenderNumTest"), true, false);
        service.createPersonIdentityType(uc, createPersonIdentity(getDate(1966, 3, 22), null), true, false);
        service.createPersonIdentityType(uc, createPersonIdentity(getDate(1989, 5, 21), null), true, false);
        service.createPersonIdentityType(uc, createPersonIdentity(getDate(1992, 10, 24), null), true, false);
        service.createPersonIdentityType(uc, createPersonIdentity(getDate(1992, 10, 24), null), true, false);
        service.createPersonIdentityType(uc, createPersonIdentity(getDate(1992, 10, 24), null), true, false);
        service.createPersonIdentityType(uc, createPersonIdentity(getDate(1992, 10, 24), null), true, false);
        service.createPersonIdentityType(uc, createPersonIdentity(getDate(2011, 8, 7), null), true, false);

        //
        PersonIdentitySearchType personIdentitySearch = new PersonIdentitySearchType();
        personIdentitySearch.setDateOfBirthFrom(getDate(1989, 1, 1));
        personIdentitySearch.setDateOfBirthTo(getDate(2012, 1, 1));

        PersonIdentitiesReturnType psret = null;
        psret = service.searchPersonIdentityType(uc, null, personIdentitySearch, SearchMatchModeType.EXACT.value(), null, null);
        assert (psret != null);

    }

    private PersonIdentityType createPersonIdentity(Date dob, String offenderNumber) {
        PersonIdentityType personIdentity = null;
        if (dob == null) {
            dob = getDate(1985, 3, 10);
        }
        Set<IdentifierType> identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", getDate(1980, 3, 10), getDate(1975, 3, 10), "G", "XXXX-XX", "Active"));

        personIdentity = new PersonIdentityType(null, psnTest.createPerson(), null, "MR", "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, offenderNumber,
                null, null);

        return personIdentity;
    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void retrievePersonIdentityHist() {

        //
        PersonIdentityType pret = null;
        PersonIdentityType personIdentity = null;
        Date dob = new Date(1985 - 1900, 3, 10);
        Set<IdentifierType> identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, personId, null, null, "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, null, null, null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, false);
        assert (pret != null);
        Long personIdentityId = pret.getPersonIdentityId();

        //
        PersonIdentitiesReturnType psret = null;

        //
        psret = service.retrievePersonIdentityType(uc, personIdentityId, null, new Date(2980 - 1900, 3, 10), null);
        assert (psret != null);
    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void retrievePersonIdentityOfPerson() {

        //
        PersonIdentityType pret = null;
        PersonIdentityType personIdentity = null;
        Date dob = new Date(1985 - 1900, 3, 10);
        Set<IdentifierType> identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, personId, null, null, "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, null, null, null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, false);
        assert (pret != null);

        PersonIdentitiesReturnType psret = null;
        psret = service.retrievePersonIdentityType(uc, 1L, null);
        assert (psret != null);
    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void delete() {

        //
        PersonIdentityType pret = null;
        PersonIdentityType personIdentity = null;
        Date dob = new Date(1985 - 1900, 3, 10);
        Set<IdentifierType> identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, personId, null, null, "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, null, null, null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, false);
        assert (pret != null);
        PersonIdentityType personIdentityToDelete = pret;

        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        PersonIdentityType personIdentity2 = new PersonIdentityType(null, personId, null, null, "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, null,
                null, null);
        pret = service.createPersonIdentityType(uc, personIdentity2, true, false);
        assert (pret != null);

        //
        Long lret = service.deletePersonIdentityType(uc, personIdentityToDelete);
        assert (lret.equals(ReturnCode.Success.returnCode()));

        verifyDeletePersonIdentityNegative(pret, InvalidInputException.class);
    }

    /**
     * CMB -- defectID_821 test
     */
    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void defectID_821() {

        //

        Long personId2 = psnTest.createPerson();
        PersonIdentityType pret = null;
        PersonIdentityType personIdentity = null;
        Date dob = new Date(1985 - 1900, 3, 10);
        Set<IdentifierType> identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, personId2, null, null, "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, null, null, null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, false);
        assert (pret != null);
        PersonIdentityType personIdentityToDelete = pret;

        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        PersonIdentityType personIdentity2 = new PersonIdentityType(null, personId2, null, null, "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, null,
                null, null);
        pret = service.createPersonIdentityType(uc, personIdentity2, true, false);
        assert (pret != null);

        //Success delete
        Long ret = service.deletePersonIdentityType(uc, personIdentityToDelete);
        assert (ret == ReturnCode.Success.returnCode());

        // Person must have one PersonIdentity associated to it.
        verifyDeletePersonIdentityNegative(pret, InvalidInputException.class);

        // Person should be deleted because method delete() is for administrator.
        //        Long lret = service.deletePersonIdentityType(uc, personIdentityId2, Boolean.FALSE);
        //        assert (lret.equals(ReturnCode.Success.returnCode()));

    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void delete2() {

        PersonIdentityType pret = null;
        Long lret = null;
        Date dob = new Date(1980 - 1900, 3, 10);

        Set<IdentifierType> identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1985 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        identifiers.add(new IdentifierType(null, "67890", "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        PersonIdentityType personIdentity = new PersonIdentityType(null, personId, "NICK", "MR", "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, false,
                "NO00001", "Comments", null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, true);

        //Data Not Exist
        PersonIdentityType personIdentityTypeToDelete = new PersonIdentityType();
        personIdentityTypeToDelete.setPersonIdentityId(-1L);
        verifyDeleteNegative(personIdentityTypeToDelete, DataNotExistException.class);

        personIdentity = new PersonIdentityType(null, personId, "NICK", "MR", "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, false, "NO00001", "Comments",
                null);
        service.createPersonIdentityType(uc, personIdentity, true, true);

        lret = service.deletePersonIdentityType(uc, pret);
        assert (lret.equals(ReturnCode.Success.returnCode()));

        //Data Not Exist, the get method was changed in revision#36429, this validation is not valid any more.
        //verifyGetNegative(personIdentityId, true, DataNotExistException.class);

    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void deleteAll() {

        PersonIdentityType pret = null;
        Long lret = null;
        Long personIdentityId = null;

        Date dob = new Date(1980 - 1900, 3, 10);
        Set<IdentifierType> identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1985 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        identifiers.add(new IdentifierType(null, "67890", "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        PersonIdentityType personIdentity = new PersonIdentityType(null, personId, "NICK", "MR", "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, false,
                "NO00001", "Comments", null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, true);

        //
        lret = service.deleteAllPersonIdentityType(uc);
        assert (lret.equals(ReturnCode.Success.returnCode()));
    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void getAll() {

        PersonIdentityType pret = null;
        Set<Long> lsret = null;
        Long personIdentityId = null;
        Date dob = new Date(1980 - 1900, 3, 10);
        Set<IdentifierType> identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1985 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        identifiers.add(new IdentifierType(null, "67890", "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        PersonIdentityType personIdentity = new PersonIdentityType(null, personId, "NICK", "MR", "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, false,
                "NO00001", "Comments", null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, true);
        personIdentityId = pret.getPersonIdentityId();
        personIdentity = service.getPersonIdentityType(uc, personIdentityId, Boolean.FALSE);
        personIdentity.setPersonIdentityId(personIdentityId);
        service.updatePersonIdentityType(uc, personIdentity, true, true);
        //
        lsret = service.getAllPersonIdentityType(uc);
        assert (lsret.size() == 1);
    }

    @Test
    public void reset() {

        Long lret = null;

        lret = service.deleteAllPersonIdentityType(null);
        assert (lret.equals(ReturnCode.Success.returnCode()));

        //
        OffNumConfigType offNumConfig = new OffNumConfigType("NY 0000000000", true);
        service.createOffNumConfig(uc, offNumConfig);
    }

    @SuppressWarnings("deprecation")
        //@Test(dependsOnMethods={"reset"})
    void getStamp() {

        PersonIdentityType pret = null;

        Date dob = new Date(1980 - 1900, 3, 10);
        Set<IdentifierType> identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1985 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        identifiers.add(new IdentifierType(null, "67890", "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        PersonIdentityType personIdentity = new PersonIdentityType(null, personId, "NICK", "MR", "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, false,
                "NO00001", "Comments", null);

        pret = service.createPersonIdentityType(uc, personIdentity, true, true);
        Long personIdentityId = pret.getPersonIdentityId();

        StampType stamp = service.getPersonIdentityTypeStamp(null, personIdentityId);
        assert (stamp != null);

    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void addIdentifier() {

        PersonIdentityType pret = null;
        IdentifierType iret = null;
        IdentifiersReturnType isret = null;
        IdentifierType identifier = null;

        Date dob = new Date(1980 - 1900, 3, 10);
        Set<IdentifierType> identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1985 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        identifiers.add(new IdentifierType(null, "67890", "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        PersonIdentityType personIdentity = new PersonIdentityType(null, personId, "NICK", "MR", "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, false,
                "NO00001", "Comments", null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, true);
        Long personIdentityId = pret.getPersonIdentityId();

        //Category is null
        identifier = new IdentifierType(1L, "12345", null, null, null, null, null, null);
        verifyAddIdenitiferNegative(personIdentityId, identifier, InvalidInputException.class);

        //Identifier is null
        identifier = new IdentifierType(1L, null, "FBI", null, null, null, null, null);
        verifyAddIdenitiferNegative(personIdentityId, identifier, InvalidInputException.class);

        //Category is not exist
        identifier = new IdentifierType(1L, "12345", "NONE_", null, null, null, null, null);
        verifyAddIdenitiferNegative(personIdentityId, identifier, DataNotExistException.class);

        //FORMAT is not exist
        identifier = new IdentifierType(1L, "12345", "FBI", null, null, null, "NONE_", null);
        verifyAddIdenitiferNegative(personIdentityId, identifier, DataNotExistException.class);

        //PersonIdentityId is not exist
        identifier = new IdentifierType(1L, "12345", "FBI", null, null, null, null, null);
        verifyAddIdenitiferNegative(-1L, identifier, DataNotExistException.class);

        identifier = new IdentifierType(1L, "12345", "FBI", null, null, null, null, null);
        iret = service.addIdentifier(null, personIdentityId, identifier);
        assert (iret != null);

        identifier = new IdentifierType(null, "12345", "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments");
        iret = service.addIdentifier(null, personIdentityId, identifier);
        assert (iret != null);

        isret = service.retrieveIdentifier(null, personIdentityId, null);
        assert (isret != null);
        assert (isret.getIdentifiers().size() == 4);
    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void updateIdentifier() {

        PersonIdentityType pret = null;
        IdentifierType iret = null;
        IdentifierType identifier = null;
        Long identifierId = null;

        Date dob = new Date(1980 - 1900, 3, 10);
        Set<IdentifierType> identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1985 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        identifiers.add(new IdentifierType(null, "67890", "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        PersonIdentityType personIdentity = new PersonIdentityType(null, personId, "NICK", "MR", "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, false,
                "NO00001", "Comments", null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, true);
        Long personIdentityId = pret.getPersonIdentityId();

        identifier = new IdentifierType(null, "12345", "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments");
        iret = service.addIdentifier(null, personIdentityId, identifier);
        assert (iret != null);
        identifierId = iret.getIdentifierId();

        //
        identifier = new IdentifierType(null, "12345", "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments");
        verifyUpdateIdenitiferNegative(identifier, InvalidInputException.class);

        //Empty Identifier
        identifier = new IdentifierType(identifierId, null, "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments");
        verifyUpdateIdenitiferNegative(identifier, InvalidInputException.class);

        //Empty Category
        identifier = new IdentifierType(identifierId, "12345", null, new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments");
        verifyUpdateIdenitiferNegative(identifier, InvalidInputException.class);

        //Category is not exist
        identifier = new IdentifierType(identifierId, "12345", "NONE_", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX",
                "Identifier Comments");
        verifyUpdateIdenitiferNegative(identifier, DataNotExistException.class);

        //Format is not exist
        identifier = new IdentifierType(identifierId, "12345", "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "NONE_", "Identifier Comments");
        verifyUpdateIdenitiferNegative(identifier, DataNotExistException.class);

        //IdentifierId is not exist
        identifier = new IdentifierType(-1L, "12345", "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments");
        verifyUpdateIdenitiferNegative(identifier, DataNotExistException.class);

        try {
            identifier = new IdentifierType(identifierId, "12345", "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX",
                    "Identifier Comments");
            service.updateIdentifier(null, identifier);
        } catch (Exception exp) {
            assert (exp.getClass().equals(ArbutusOptimisticLockException.class));
        }

        identifier = service.getPersonIdentityType(null, pret.getPersonIdentityId(), Boolean.FALSE).getIdentifiers().iterator().next();
        identifier.setIdentifier("123456");
        service.updateIdentifier(null, identifier);
    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void retrieveIdentifier() {

        PersonIdentityType pret = null;
        IdentifiersReturnType isret = null;
        IdentifierType identifierReturned = null;

        Date dob = new Date(1980 - 1900, 3, 10);
        Set<IdentifierType> identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1985 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        identifiers.add(new IdentifierType(null, "67890", "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        PersonIdentityType personIdentity = new PersonIdentityType(null, personId, "NICK", "MR", "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, false,
                "NO00001", "Comments", null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, true);
        Long personIdentityId = pret.getPersonIdentityId();

        //PersonIdentityId is null
        verifyRetrieveIdenitiferNegative(null, null, InvalidInputException.class);

        //Success
        isret = service.retrieveIdentifier(null, personIdentityId, null);
        assert (isret != null);
        assert (isret.getIdentifiers().size() == 2);

        //
        isret = service.retrieveIdentifier(null, personIdentityId, false);
        assert (isret != null);
        assert (isret.getIdentifiers().size() == 1);
        identifierReturned = (IdentifierType) isret.getIdentifiers().toArray()[0];
        assert (identifierReturned.getCategory().equals("SIN"));
        //
        isret = service.retrieveIdentifier(null, personIdentityId, true);
        assert (isret != null);
        assert (isret.getIdentifiers().size() == 1);
        identifierReturned = (IdentifierType) isret.getIdentifiers().toArray()[0];
        assert (identifierReturned.getCategory().equals("FBI"));

    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void removeIdentifier() {

        PersonIdentityType pret = null;
        IdentifiersReturnType isret = null;
        Long lret = null;

        Date dob = new Date(1980 - 1900, 3, 10);
        Set<IdentifierType> identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1985 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        identifiers.add(new IdentifierType(null, "67890", "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX", "Identifier Comments"));
        PersonIdentityType personIdentity = new PersonIdentityType(null, personId, "NICK", "MR", "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, false,
                "NO00001", "Comments", null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, true);
        Long personIdentityId = pret.getPersonIdentityId();

        IdentifierType identifier = new IdentifierType(null, "12345", "FBI", new Date(1980 - 1900, 3, 10), new Date(2980 - 1900, 3, 10), "G", "XXXX-XX",
                "Identifier Comments");
        IdentifierType iret = service.addIdentifier(null, personIdentityId, identifier);
        assert (iret != null);

        //InvalidInput
        verifyRemoveIdenitiferNegative(null, InvalidInputException.class);

        //Data not exist
        IdentifierType identifierTypeToDelete = new IdentifierType();
        identifierTypeToDelete.setIdentifierId(-1L);
        verifyRemoveIdenitiferNegative(identifierTypeToDelete, DataNotExistException.class);

        //Success
        lret = service.removeIdentifier(null, iret);
        assert (lret.equals(ReturnCode.Success.returnCode()));

        //
        isret = service.retrieveIdentifier(null, personIdentityId, null);
        assert (isret != null);
        assert (isret.getIdentifiers().size() == 2);

    }

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void generateOffenderNumber() {

        PersonIdentityType pret = null;
        PersonIdentityType personIdentity = null;
        Date dob = new Date(1985 - 1900, 3, 10);
        Long personIdentityId = null;
        String offenderNumber = null;

        Set<String> privileges = new HashSet<String>();
        privileges.add("Sealed");
        privileges.add("Confidential");

        personIdentity = new PersonIdentityType(null, personId, null, null, "Jason", null, null, "Liang", null, dob, "M", null, null, null, null, null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, false);
        assert (pret != null);
        assert (Boolean.TRUE.equals(pret.isActive()));
        assert (Boolean.TRUE.equals(pret.isCreatedWith()));
        assert (pret.getOffenderNumber() == null);
        personIdentityId = pret.getPersonIdentityId();

        //
        offenderNumber = service.generateOffenderNumber(null, personIdentityId);
        assert (offenderNumber != null);
        assert (offenderNumber.length() > 0);

    }

    @Test
    public void getOffNumConfig() {

        //Clean up the configuration
        service.deleteAllPersonIdentityType(uc);

        OffNumConfigType oret = null;
        OffNumConfigType offNumConfig = null;

        offNumConfig = new OffNumConfigType("A", true);
        oret = service.createOffNumConfig(uc, offNumConfig);

        //
        oret = service.getOffNumConfig(null, "A");
        assert (oret != null);
        assert (oret.getFormat().equals("A"));
        assert (oret.isGeneration().equals(Boolean.TRUE));

    }

    @Test
    public void updateOffNumConfig() {

        //Clean up the configuration
        service.deleteAllPersonIdentityType(uc);

        OffNumConfigType oret = null;
        OffNumConfigType offNumConfig = null;

        offNumConfig = new OffNumConfigType("B", true);
        oret = service.createOffNumConfig(uc, offNumConfig);

        offNumConfig = new OffNumConfigType("A", false);
        oret = service.updateOffNumConfig(uc, offNumConfig);
        assert (oret != null);
        oret = service.getOffNumConfig(null, "A");
        assert (oret != null);
        assert (oret.isGeneration().equals(Boolean.FALSE));

    }

    @Test
    public void getDiminutive() {

        DiminutiveType dret = null;
        DiminutiveType diminutive = null;
        Set<String> names = null;
        Long diminutiveId = null;

        diminutive = new DiminutiveType(-1L, "en", null);
        names = new HashSet<String>();
        names.add("Michael");
        names.add("Mike");
        diminutive.setNames(names);
        dret = service.createDiminutive(uc, diminutive);
        assert (dret != null);
        diminutiveId = dret.getDiminutiveId();

        dret = service.getDiminutive(null, diminutiveId);
        assert (dret != null);
        assert (dret.getNames().size() == 2);

    }

    @Test
    public void createDiminutive() {

        DiminutiveType dret = null;
        DiminutiveType diminutive = null;
        Set<String> names = null;

        diminutive = new DiminutiveType(-1L, "en", null);
        names = new HashSet<String>();
        names.add("Michael");
        names.add("Mike");
        diminutive.setNames(names);
        dret = service.createDiminutive(uc, diminutive);
        assert (dret != null);

    }

    @Test
    public void upadteDiminutive() {

        DiminutiveType dret = null;
        DiminutiveType diminutive = null;
        Set<String> names = null;
        Long diminutiveId = null;

        diminutive = new DiminutiveType(-1L, "en", null);
        names = new HashSet<String>();
        names.add("Michael");
        names.add("Mike");
        diminutive.setNames(names);
        dret = service.createDiminutive(uc, diminutive);
        assert (dret != null);
        diminutiveId = dret.getDiminutiveId();

        verifyUpadteDiminutiveNegative(null, InvalidInputException.class);

        diminutive = new DiminutiveType(-1L, "none", null);
        names = new HashSet<String>();
        names.add("Michael");
        names.add("Mike");
        diminutive.setNames(names);
        verifyUpadteDiminutiveNegative(diminutive, DataNotExistException.class);

        diminutive = new DiminutiveType(-1L, "en", null);
        verifyUpadteDiminutiveNegative(diminutive, InvalidInputException.class);

        names = new HashSet<String>();
        names.add("Michael");
        names.add("Mike");
        names.add("Jason");
        diminutive.setNames(names);
        diminutive = new DiminutiveType(null, "en", names);
        verifyUpadteDiminutiveNegative(diminutive, InvalidInputException.class);

        diminutive = new DiminutiveType(-1L, "en", null);
        names = new HashSet<String>();
        names.add("Michael");
        names.add("Mike");
        names.add("Jason");
        diminutive.setNames(names);
        verifyUpadteDiminutiveNegative(diminutive, DataNotExistException.class);

        diminutive = new DiminutiveType(diminutiveId, "en", null);
        names = new HashSet<String>();
        names.add("Michael");
        names.add("Mike");
        names.add("Jason");
        diminutive.setNames(names);
        dret = service.updateDiminutive(uc, diminutive);
        assert (dret != null);
        dret = service.getDiminutive(null, diminutiveId);
        assert (dret != null);
        assert (dret.getNames().size() == 3);

    }

    //    //@Test
    //    public void retrieveDiminutive() {
    //
    //        DiminutiveType dret = null;
    //        DiminutivesReturnType dsret = null;
    //        DiminutiveType diminutive = null;
    //        Set<String> names = null;
    //
    //        diminutive = new DiminutiveType(-1L, "en", null);
    //        names = new HashSet<String>();
    //        names.add("Michael");
    //        names.add("Mike");
    //        diminutive.setNames(names);
    //        dret = service.createDiminutive(uc, diminutive);
    //
    //        dsret = service.retrieveDiminutive(null, null);
    //        assert(dsret.getReturnCode().equals(ReturnCode.CInvalidInput2002.returnCode()));
    //
    //        dsret = service.retrieveDiminutive(null, "Mike");
    //        assert(dret != null);
    //        assert(dsret.getDiminutives().size() == 1);
    //
    //    }
    //
    //    //@Test
    //    public void deleteDiminutive() {
    //
    //        DiminutiveType dret = null;
    //        Long lret = null;
    //        Long diminutiveId = null;
    //
    //        DiminutiveType diminutive = new DiminutiveType(-1L, "en", null);
    //        Set<String> names = new HashSet<String>();
    //        names.add("Michael");
    //        names.add("Mike");
    //        diminutive.setNames(names);
    //        dret = service.createDiminutive(uc, diminutive);
    //        diminutiveId = dret.getDiminutiveId();
    //
    //        lret = service.deleteDiminutive(null, null);
    //        assert(lret.equals(ReturnCode.CInvalidInput2002.returnCode()));
    //
    //        lret = service.deleteDiminutive(null, -1L);
    //        assert(lret.equals(ReturnCode.RDataNotExist1008.returnCode()));
    //
    //        lret = service.deleteDiminutive(null, diminutiveId);
    //        assert(lret.equals(ReturnCode.Success.returnCode()));
    //    }
    //
    //
    //    //@Test
    //    public void addDiminutiveName() {
    //
    //        DiminutiveType dret = null;
    //        DiminutiveType diminutive = null;
    //        Long lret = null;
    //        Set<String> names = null;
    //        Long diminutiveId = null;
    //
    //        diminutive = new DiminutiveType(-1L, "en", null);
    //        names = new HashSet<String>();
    //        names.add("Michael");
    //        names.add("Mike");
    //        diminutive.setNames(names);
    //        dret = service.createDiminutive(uc, diminutive);
    //        diminutiveId = dret.getDiminutiveId();
    //
    //        lret = service.addDiminutiveName(null, null, null);
    //        assert(lret.equals(ReturnCode.CInvalidInput2002.returnCode()));
    //
    //        lret = service.addDiminutiveName(null, -1L, null);
    //        assert(lret.equals(ReturnCode.CInvalidInput2002.returnCode()));
    //
    //        lret = service.addDiminutiveName(null, null, "Jason");
    //        assert(lret.equals(ReturnCode.CInvalidInput2002.returnCode()));
    //
    //        lret = service.addDiminutiveName(null, -1L, "Jason");
    //        assert(lret.equals(ReturnCode.RDataNotExist1008.returnCode()));
    //
    //        lret = service.addDiminutiveName(null, diminutiveId, "Jason");
    //        assert(lret.equals(ReturnCode.Success.returnCode()));
    //
    //        dret = service.getDiminutive(null, diminutiveId);
    //        assert(dret != null);
    //        assert(dret.getDiminutive().getNames().size() == 3);
    //
    //    }
    //
    //    //@Test
    //    public void removeDiminutiveName() {
    //
    //        DiminutiveType dret = null;
    //        DiminutiveType diminutive = null;
    //        Long lret = null;
    //        Set<String> names = null;
    //        Long diminutiveId = null;
    //
    //        diminutive = new DiminutiveType(-1L, "en", null);
    //        names = new HashSet<String>();
    //        names.add("Michael");
    //        names.add("Mike");
    //        diminutive.setNames(names);
    //        dret = service.createDiminutive(uc, diminutive);
    //        diminutiveId = dret.getDiminutiveId();
    //
    //        lret = service.removeDiminutiveName(null, null, null);
    //        assert(lret.equals(ReturnCode.CInvalidInput2002.returnCode()));
    //
    //        lret = service.removeDiminutiveName(null, -1L, null);
    //        assert(lret.equals(ReturnCode.CInvalidInput2002.returnCode()));
    //
    //        lret = service.removeDiminutiveName(null, null, "Jason");
    //        assert(lret.equals(ReturnCode.CInvalidInput2002.returnCode()));
    //
    //        lret = service.removeDiminutiveName(null, -1L, "Jason");
    //        assert(lret.equals(ReturnCode.RDataNotExist1008.returnCode()));
    //
    //        lret = service.removeDiminutiveName(null, diminutiveId, "Jason");
    //        assert(lret.equals(ReturnCode.RDataNotExist1008.returnCode()));
    //
    //        lret = service.removeDiminutiveName(null, diminutiveId, "Mike");
    //        assert(lret.equals(ReturnCode.Success.returnCode()));
    //
    //        dret = service.getDiminutive(null, diminutiveId);
    //        assert(dret != null);
    //        assert(dret.getDiminutive().getNames().size() == 1);
    //
    //    }
    //

    @SuppressWarnings("deprecation")
    @Test(dependsOnMethods = { "reset" })
    public void getNiem() {

        PersonIdentityType pret = null;
        PersonIdentityType personIdentity = null;
        Date dob = new Date(1985 - 1900, 3, 10);
        Set<IdentifierType> identifiers = null;
        identifiers = new HashSet<IdentifierType>();
        identifiers.add(new IdentifierType(null, "12345", "SIN", new Date(1980 - 1900, 3, 10), new Date(1975 - 1900, 3, 10), "G", "XXXX-XX", "Active"));
        personIdentity = new PersonIdentityType(null, personId, null, null, "Jason", "Ji", "Xin", "Liang", "I", dob, "M", identifiers, null, null, null, null);
        pret = service.createPersonIdentityType(uc, personIdentity, true, false);
        assert (pret != null);

        Long personIdentityId = pret.getPersonIdentityId();
        String niem = service.getPersonIdentityTypeNiem(null, personIdentityId);
        assert (niem != null);

    }

    @Test(dependsOnMethods = { "reset" })
    public void verify596() {

        DiminutiveType dret = new DiminutiveType();

        Set<String> names = new HashSet<String>();
        names.add("Shurik");
        //names.add("Shurik123456789012345678901234567890123456789012345678901234567890");
        DiminutiveType diminutive = new DiminutiveType(-1L, "En", names);
        dret = service.createDiminutive(uc, diminutive);
        assert (dret != null);
        Long diminutiveId = dret.getDiminutiveId();

        //
        names = new HashSet<String>();
        names.add("Olex1");
        diminutive.setDiminutiveId(diminutiveId);
        diminutive.setNames(names);
        dret = service.updateDiminutive(uc, diminutive);
        assert (dret != null);
    }

    @Test
    public void verify597() {

        //Clean configuration first
        service.deleteAllPersonIdentityType(uc);

        OffNumConfigType oret = null;

        //
        OffNumConfigType offNumConfig = new OffNumConfigType("NY 0000000000", false);
        oret = service.createOffNumConfig(uc, offNumConfig);
        assert (oret != null);

        offNumConfig = new OffNumConfigType("NY 0000000000", true);
        oret = service.updateOffNumConfig(uc, offNumConfig);
        assert (oret != null);
    }

    private Date getDate(int year, int month, int date) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, date, 0, 0, 0);
        return cal.getTime();
    }
    //
    //    @Test
    //    public void testGetVersion() {
    //    	String version = service.getVersion(null);
    //    	assert("1.2".equals(version));
    //    }

    private void verifyUpadteDiminutiveNegative(DiminutiveType diminutive, Class<?> expectionClazz) {
        try {
            DiminutiveType ret = service.updateDiminutive(uc, diminutive);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyAddIdenitiferNegative(Long personIdentityId, IdentifierType identifier, Class<?> expectionClazz) {
        try {
            IdentifierType ret = service.addIdentifier(uc, personIdentityId, identifier);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyUpdateIdenitiferNegative(IdentifierType identifier, Class<?> expectionClazz) {
        try {
            service.updateIdentifier(uc, identifier);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRemoveIdenitiferNegative(IdentifierType identifierType, Class<?> expectionClazz) {
        try {
            Long ret = service.removeIdentifier(uc, identifierType);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyRetrieveIdenitiferNegative(Long instanceId, Boolean returnEffectiveIdentifier, Class<?> expectionClazz) {
        try {
            IdentifiersReturnType ret = service.retrieveIdentifier(uc, instanceId, returnEffectiveIdentifier);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyGetNegative(Long instanceId, Boolean returnEffectiveIdentifier, Class<?> expectionClazz) {
        try {
            PersonIdentityType ret = service.getPersonIdentityType(uc, instanceId, returnEffectiveIdentifier);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyDeleteNegative(PersonIdentityType personIdentityType, Class<?> expectionClazz) {
        try {
            Long ret = service.deletePersonIdentityType(uc, personIdentityType);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyDeletePersonIdentityNegative(PersonIdentityType personIdentityType, Class<?> expectionClazz) {
        try {
            Long ret = service.deletePersonIdentityType(uc, personIdentityType);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }
}
