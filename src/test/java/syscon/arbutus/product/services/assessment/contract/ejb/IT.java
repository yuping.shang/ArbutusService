package syscon.arbutus.product.services.assessment.contract.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.kie.api.KieServices;
import org.kie.api.builder.ReleaseId;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.assessment.contract.dto.*;
import syscon.arbutus.product.services.assessment.contract.dto.AssessmentSearchType.SearchTypeEnum;
import syscon.arbutus.product.services.assessment.contract.interfaces.AssessmentService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.common.SupervisionTest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class IT extends BaseIT {
    private static Logger log = LoggerFactory.getLogger(IT.class);
    private AssessmentService service;
    private SupervisionTest supervisionTest;
    private FacilityTest facilityTest;
    private AssessmentType assessmentTypeObj;

    @BeforeClass
    public void beforeTest() throws Exception {
        log.info("beforeClass Begin - JNDI lookup");
        service = (AssessmentService) JNDILookUp(this.getClass(), AssessmentService.class);
        uc = super.initUserContext();
        supervisionTest = new SupervisionTest();
        facilityTest = new FacilityTest();
    }

    @AfterClass
    public void afterClass() {
        //service.deleteAll(uc);
    }

        @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    /**
     * This method is used to test the functioning of the BRMS poc WOR-4934
     */
    //@Test
    public void testGetResult() {
        final ReleaseId releaseId = KieServices.Factory.get().newReleaseId("syscon.arbutus.product", "assessment", "LATEST");
        final KieServices ks = KieServices.Factory.get();
        final KieContainer kContainer = ks.newKieContainer(releaseId);
        final KieSession ksession = kContainer.newKieSession("assessmentKS");
        AssessmentHandler handler = new AssessmentHandlerDrools(null, null, ksession);
        String result = handler.getResult("RECLASS", 150l);
        assert (result.equals("MAE"));
    }

    @Test
    public void testConfigureAssessment() {
        AssessmentType assessmentType = getAssessmentTypeForCreate();
        service.configureAssessment(uc, assessmentType);
    }

    @Test(dependsOnMethods = { "testConfigureAssessment" })
    //@Test
    public void testStartAssessment() {
        Long supervisionId = supervisionTest.createSupervision(facilityTest.createFacility(), true);
        StartAssessmentType startAssessmentType = new StartAssessmentType("AssessmentCategory", "AssessmentType", supervisionId, "EN", 1L);
        startAssessmentType.setSupervisionId(supervisionId);
        AssessmentType assessmentType = service.startAssessment(uc, startAssessmentType, Boolean.FALSE);
        System.out.println(assessmentType);
        assert(assessmentType != null);
    }

    @Test
    public void testSearch() {
        List<AssessmentSearchResultType> result = service.search(uc, getSssessmentSearchType());
        System.out.println(result.size());
    }

    // @Test
    public void testSearchScheduledOrInProgressInmateAssessment() {
        AssessmentType assessmentType = new AssessmentType();
        assessmentType.setAssessmentCategory("AssessmentCategory");
        assessmentType.setAssessmentType("AssessmentType");
        assessmentType.setSupervisiondId(1L);
        assessmentType = service.searchScheduledOrInProgressInmateAssessment(uc, assessmentType);
        System.out.println(assessmentType.toString());
    }

    @Test(dependsOnMethods = { "testStartAssessment" })
    public void testGetInmateAssessment() {
        AssessmentType assessmentType = service.getInmateAssessment(uc, 1L);
        System.out.println(assessmentType.toString());
    }

    @Test
    public void testSaveScoreRange() {
        ScoringRangeType type = new ScoringRangeType();
        type.setRangeMaximum(100L);
        type.setRangeMinimum(1L);
        type.setRangeResult("Test");
        type.setAssessmentType("test");
        service.saveScoreRange(uc, type);
    }

    @Test(dependsOnMethods = { "testStartAssessment" })
    public void testGetInamteAssessmentResult() {
        service.getInamteAssessmentResult(uc, 1L);
    }

    @Test
    public void testGetInamteAssessmentAllResult() {
        service.getInamteAssessmentAllResult(uc, 1L);
    }

    @Test(dependsOnMethods = { "testApproveOrDeny" })
    public void testCancelAssessment() {
        AssessmentType assessmentType = service.getInmateAssessment(uc, 1L);
        service.cancelAssessment(uc, assessmentType);
    }

    @Test(dependsOnMethods = { "testStartAssessment" })
    public void testOverrideAssessment() {
        AssessmentType assessmentType = service.getInmateAssessment(uc, 1L);
        assessmentType.setOverrideResult("Test");
        assessmentType.setOverrideByStaffId(1L);
        service.overrideAssessment(uc, assessmentType);
    }

    @Test(dependsOnMethods = { "testOverrideAssessment" })
    public void testApproveOrDeny() {
        AssessmentType assessmentType = service.getInmateAssessment(uc, 1L);
        assessmentType.setApprovalResult("Test");
        assessmentType.setApprovedByStaffId(1L);
        service.approveOrDeny(uc, assessmentType);
    }

    @Test
    public void testGetSection() {
        AssessmentType assessmentType = new AssessmentType();
        assessmentType.setAssessmentId(1L);
        assessmentType.setSections(getSectionTypeForCreate());
        AssessmentType updatedAssessmentType = service.addSections(uc, assessmentType);
        Long sectionid = updatedAssessmentType.getSections().get(0).getSectionId();
        SectionType sectionType = service.getSection(uc, sectionid);
        assertNotNull(sectionType);
    }

    @Test(dependsOnMethods = { "testConfigureAssessment" })
    public void testGetQuestion() {
        QuestionAnswerType questionAnswerType = service.getQuestion(uc, 1L);
        System.out.println(questionAnswerType.toString());
    }

    private AssessmentType getAssessmentTypeForCreate() {
        AssessmentType assessmentType = new AssessmentType();
        assessmentType.setActivationDate(new Date());
        assessmentType.setApprovalRequired(true);
        assessmentType.setAssessmentCategory("AssessmentCategory");
        assessmentType.setAssessmentType("AssessmentType");
        assessmentType.setOverrideAllowed(true);
        assessmentType.setReviewDuration(30L);
        assessmentType.setSections(getSectionTypeForCreate());
        assessmentType.setAssessmentLanguageTypes(getAssessmentLanguageTypes());
        return assessmentType;
    }

    private List<SectionType> getSectionTypeForCreate() {
        List<SectionType> list = new ArrayList<SectionType>();
        SectionType sectionType = new SectionType();
        List<DescriptionType> descriptions = new ArrayList<DescriptionType>();
        DescriptionType description = new DescriptionType();
        description.setLabel("First Assessment 1");
        description.setHelpText("First Assessment 1 Help");
        description.setAssessmentLanguageType(getAssessmentLanguageTypeUpdate());
        descriptions.add(description);
        sectionType.setName(descriptions);
        sectionType.setQuestions(getQuestionnaireTypeForCreate());
        sectionType.setSumScore(true);
        // sectionType.setQuestions(new ArrayList<QuestionAnswerType>());
        sectionType.setSequence(1L);
        list.add(sectionType);
        return list;
    }

    private List<QuestionAnswerType> getQuestionnaireTypeForCreate() {
        List<QuestionAnswerType> list = new ArrayList<QuestionAnswerType>();
        QuestionAnswerType questionAnswerType = new QuestionAnswerType();
        questionAnswerType.setAnswerOptions(createAnswerOptionsTypeForCreate());
        // questionnaire.setAnswerOptions(new ArrayList<AnswerOptionsType>());
        List<DescriptionType> descriptions = new ArrayList<DescriptionType>();
        DescriptionType description = new DescriptionType();
        description.setLabel("First Question");
        description.setHelpText("First Question Help");
        description.setAssessmentLanguageType(getAssessmentLanguageType());
        descriptions.add(description);
        questionAnswerType.setLables(descriptions);
        questionAnswerType.setManditory(true);
        questionAnswerType.setSequence(1L);
        questionAnswerType.setType(AssessmentEnum.SINGLE_CODE.toString());
        list.add(questionAnswerType);
        return list;
    }

    private List<AnswerOptionsType> createAnswerOptionsTypeForCreate() {
        List<AnswerOptionsType> list = new ArrayList<AnswerOptionsType>();
        AnswerOptionsType answer = new AnswerOptionsType();
        List<DescriptionType> descriptions = new ArrayList<DescriptionType>();
        DescriptionType description = new DescriptionType();
        description.setLabel("Ans Option 1");
        description.setHelpText("Ans Option 1 Help");
        description.setAssessmentLanguageType(getAssessmentLanguageType());
        descriptions.add(description);
        answer.setAnswer(descriptions);
        answer.setScore(10L);
        list.add(answer);
        return list;
    }

    private AssessmentSearchType getSssessmentSearchType() {
        AssessmentSearchType assessmentSearchType = new AssessmentSearchType(SearchTypeEnum.OVERDUE, "AssessmentCategory", "AssessmentType");
        assessmentSearchType.setFromDate(new GregorianCalendar(2013, 6, 30).getTime());
        assessmentSearchType.setToDate(new GregorianCalendar(2013, 10, 30).getTime());
        return assessmentSearchType;
    }

    @Test //(dependsOnMethods = { "testConfigureAssessment" })
    public void testSearchQuestionnaire() {
        AssessmentSearchType assessmentSearchType = new AssessmentSearchType();
        assessmentSearchType.setFromDate(new GregorianCalendar(2013, 6, 30).getTime());
        assessmentSearchType.setToDate(new GregorianCalendar(2015, 8, 30).getTime());
        List<AssessmentType> list = service.searchQuestionnaire(uc, assessmentSearchType, 0L, 10L);
        System.out.println(list);
        System.out.println(list.size());
    }

    @Test
    public void testUpdateSection() {
        SectionType sectionType = new SectionType();
        sectionType.setSectionId(1L);
        List<DescriptionType> descriptions = new ArrayList<DescriptionType>();
        DescriptionType description = new DescriptionType();
        description.setLabel("2 Assessment (FR)");
        description.setHelpText("2 Assessment Help");
        description.setAssessmentLanguageType(getAssessmentLanguageTypeFRUpdate());
        descriptions.add(description);
        sectionType.setName(descriptions);
        sectionType.setSequence(2L);
        sectionType.setSumScore(true);
        service.updateSection(uc, sectionType);
    }

    @Test
    public void testUpdateAssessment() {
        AssessmentType assessmentType = new AssessmentType();
        assessmentType.setAssessmentId(1L);
        assessmentType.setActivationDate(new Date());
        assessmentType.setApprovalRequired(true);
        assessmentType.setAssessmentCategory("AssessmentCategory");
        assessmentType.setAssessmentType("AssessmentType");
        assessmentType.setOverrideAllowed(true);
        assessmentType.setReviewDuration(45L);
        assessmentType.setSections(getSectionTypeForCreate());
        service.updateAssessment(uc, assessmentType);
    }

    @Test(dependsOnMethods = { "testStartAssessment" })
    public void testAddSections() {
        AssessmentType assessmentType = new AssessmentType();
        assessmentType.setAssessmentId(1L);
        assessmentType.setSections(getSectionTypeForCreate());
        AssessmentType updatedAssessmentType = service.addSections(uc, assessmentType);
        System.out.println(updatedAssessmentType.toString());
    }

    @Test(dependsOnMethods = { "testStartAssessment" })
    public void testAddQuestionnaires() {
        SectionType sectionType = new SectionType();
        sectionType.setSectionId(1L);
        sectionType.setQuestions(new ArrayList<QuestionAnswerType>());
        QuestionAnswerType questionAnswerType = new QuestionAnswerType();
        List<DescriptionType> descriptions = new ArrayList<DescriptionType>();
        DescriptionType description = new DescriptionType();
        description.setLabel("Third Question Added");
        description.setHelpText("Third Question Added Help");
        description.setAssessmentLanguageType(getAssessmentLanguageTypeUpdate());
        descriptions.add(description);
        questionAnswerType.setLables(descriptions);
        questionAnswerType.setManditory(true);
        questionAnswerType.setSequence(1L);
        questionAnswerType.setType(AssessmentEnum.SINGLE_CODE.toString());
        sectionType.getQuestions().add(questionAnswerType);
        service.addQuestions(uc, sectionType);
    }

    @Test
    public void testUpdateQuestion() {
        QuestionAnswerType questionAnswerType = new QuestionAnswerType();
        questionAnswerType.setQuesitonId(1L);
        List<DescriptionType> descriptions = new ArrayList<DescriptionType>();
        DescriptionType description = new DescriptionType();
        description.setLabel("Third Question Added (FR)");
        description.setHelpText("Third Question Added Help");
        description.setAssessmentLanguageType(getAssessmentLanguageTypeFRUpdate());
        descriptions.add(description);
        questionAnswerType.setLables(descriptions);
        questionAnswerType.setManditory(true);
        questionAnswerType.setSequence(3L);
        questionAnswerType.setType(AssessmentEnum.SINGLE_CODE.toString());
        service.updateQuestion(uc, questionAnswerType);
    }

    @Test(dependsOnMethods = { "testConfigureAssessment" })
    public void testAddAnswers() {
        QuestionAnswerType questionAnswerType = new QuestionAnswerType();
        questionAnswerType.setQuesitonId(1L);
        List<AnswerOptionsType> answers = new ArrayList<AnswerOptionsType>();
        answers.add(createAnswerOptionsType());
        questionAnswerType.setAnswerOptions(answers);
        service.addAnswers(uc, questionAnswerType);
    }

    private AnswerOptionsType createAnswerOptionsType() {
        AnswerOptionsType answer = new AnswerOptionsType();
        List<DescriptionType> descriptions = new ArrayList<DescriptionType>();
        DescriptionType description = new DescriptionType();
        description.setLabel("Ans Option 2");
        description.setHelpText("Ans Option 2 Help");
        description.setAssessmentLanguageType(getAssessmentLanguageTypeUpdate());
        descriptions.add(description);
        answer.setAnswer(descriptions);
        answer.setScore(10L);
        answer.setNextQuestionId(1L);
        return answer;
    }

    @Test
    public void testGetAnswer() {
        AnswerOptionsType answerOptionsType = service.getAnswer(uc, 1L);
        System.out.println(answerOptionsType.toString());
    }

    //@Test
    public void testDeleteAnswers() {
        service.deleteAnswers(uc, 58L);
    }

    //@Test
    public void testDeleteAnswer() {
        service.deleteAnswer(uc, 58L);
    }

    //@Test
    public void testDeleteQuestion() {
        service.deleteQuestion(uc, 17L);
    }

    //@Test
    public void testDeleteAssessment() {
        service.deleteAssessment(uc, 2L);
    }

    @Test(dependsOnMethods = { "testStartAssessment" })
    public void testGetInmateQuestion() {
        QuestionAnswerType questionAnswerType = service.getInmateQuestion(uc, 1L);
        Assert.assertNotNull(questionAnswerType);
    }

    private AssessmentLanguageType getAssessmentLanguageType() {
        AssessmentLanguageType assessmentLanguageType = new AssessmentLanguageType();
        // assessmentLanguageType.setAssessmentLanguageId(1L);
        assessmentLanguageType.setLanguageCode("EN");
        assessmentLanguageType.setSequence(1L);
        return assessmentLanguageType;
    }

    private AssessmentLanguageType getAssessmentLanguageTypeUpdate() {
        AssessmentLanguageType assessmentLanguageType = new AssessmentLanguageType();
        assessmentLanguageType.setAssessmentLanguageId(1L);
        assessmentLanguageType.setLanguageCode("EN");
        assessmentLanguageType.setSequence(1L);
        return assessmentLanguageType;
    }

    private AssessmentLanguageType getAssessmentLanguageTypeFR() {
        AssessmentLanguageType assessmentLanguageType = new AssessmentLanguageType();
        // assessmentLanguageType.setAssessmentLanguageId(2L);
        assessmentLanguageType.setLanguageCode("FR");
        assessmentLanguageType.setSequence(1L);
        return assessmentLanguageType;
    }

    private AssessmentLanguageType getAssessmentLanguageTypeFRUpdate() {
        AssessmentLanguageType assessmentLanguageType = new AssessmentLanguageType();
        assessmentLanguageType.setAssessmentLanguageId(2L);
        assessmentLanguageType.setLanguageCode("FR");
        assessmentLanguageType.setSequence(1L);
        return assessmentLanguageType;
    }

    private List<AssessmentLanguageType> getAssessmentLanguageTypes() {
        List<AssessmentLanguageType> list = new ArrayList<AssessmentLanguageType>();
        list.add(getAssessmentLanguageType());
        list.add(getAssessmentLanguageTypeFR());
        return list;
    }

    //@Test(dependsOnMethods = { "testConfigureAssessment" })
    public void testStartNewScheduledAssessment() {
        AssessmentType assessmentType = new AssessmentType();
        assessmentType.setAssessmentCategory("AssessmentCategory");
        assessmentType.setAssessmentType("AssessmentType");
        assessmentType.setSupervisiondId(1L);
        assessmentType.setLanguage("EN");
        assessmentType.setInitiateByStaffId(1L);
        service.scheduleInmateInitialAssessement(uc, assessmentType.getSupervisiondId(), assessmentType.getInitiateByStaffId(), assessmentType.getLanguage());
    }

    // @Test
    public void testDeleteAssessmentLanguage() {
        service.deleteAssessmentLanguage(uc, 1L);
    }

    //@Test //(dependsOnMethods = { "testConfigureAssessment" })
    public void testCancelAssessmentForSupervision() {
        StartAssessmentType startAssessmentType = new StartAssessmentType("SECURITY", "CLASS", 1L, "EN", 1l);
        Long supervisionId = supervisionTest.createSupervision(facilityTest.createFacility(), true);
        startAssessmentType.setSupervisionId(supervisionId);
        AssessmentType assessmentType = service.startAssessment(uc, startAssessmentType, Boolean.FALSE);
        service.cancelAssessment(uc, assessmentType.getSupervisiondId());
        assessmentType = service.getInmateAssessment(uc, assessmentType.getAssessmentId());
        assertEquals(assessmentType.getStatus(), AssessmentStatusEnum.CANCELLED);
    }

    //@Test
    public void testDeleteSection() {
        service.deleteSection(uc, 4l);
    }

    //	@Test
    //	public void testSearchAssessment() {
    //		service.searchAssessment()
    //	}

    @Test(dependsOnMethods = { "testConfigureAssessment" })
    public void testGetAssessmentData() {
        Long supervisionId = supervisionTest.createSupervision(facilityTest.createFacility(), true);
        StartAssessmentType startAssessmentType = new StartAssessmentType("AssessmentCategory", "AssessmentType", 1L, "EN", 1L);
        assessmentTypeObj = service.getAssessmentData(uc, startAssessmentType, null, AssessmentActivityEnum.REPORT_INCIDENT_ACTIVITY);
        System.out.println(assessmentTypeObj);
    }

    @Test(dependsOnMethods = { "testConfigureAssessment" })
    public void testSaveAssessmentAnswer() {
        if (null != assessmentTypeObj) {
            List<AnswerOptionsType> answerOptionsList;
            List<AnswerOptionsType> answerOptionsListObj;
            for (SectionType sectionType : assessmentTypeObj.getSections()) {
                for (QuestionAnswerType qaType : sectionType.getQuestions()) {
                    if (qaType.getManditory()) {
                        if (qaType.getType().toString().equals(AssessmentEnum.FREE_TEXT.toString())) {
                            qaType.setAnswerFreeText("It is answer of question " + qaType.getQuestion());
                        }

                        if (qaType.getType().toString().equals("MULTIPLE")) {
                            answerOptionsList = qaType.getAnswers();
                            if (!answerOptionsList.isEmpty()) {
                                qaType.setAnswers(answerOptionsList);
                            }

                        }
                        if (qaType.getType().toString().equals("SINGLE")) {
                            answerOptionsList = qaType.getAnswers();
                            answerOptionsListObj = new ArrayList<AnswerOptionsType>();
                            if (!answerOptionsList.isEmpty()) {
                                answerOptionsListObj.add(answerOptionsList.get(0));
                                qaType.setAnswers(answerOptionsListObj);
                            }

                        }
                    }

                }
            }
            assessmentTypeObj = service.saveAssessmentAnswer(uc, assessmentTypeObj, 1L);
        }

    }

    @Test(dependsOnMethods = { "testConfigureAssessment" })
    public void testSaveAndCompleteAssessmentAnswer() {
        if (null != assessmentTypeObj) {
            List<AnswerOptionsType> answerOptionsList;
            List<AnswerOptionsType> answerOptionsListObj;
            for (SectionType sectionType : assessmentTypeObj.getSections()) {
                for (QuestionAnswerType qaType : sectionType.getQuestions()) {
                    if (qaType.getManditory()) {
                        if (qaType.getType().toString().equals(AssessmentEnum.FREE_TEXT.toString())) {
                            qaType.setAnswerFreeText("It is answer of question " + qaType.getQuestion());
                        }

                        if (qaType.getType().toString().equals("MULTIPLE")) {
                            answerOptionsList = qaType.getAnswers();
                            if (!answerOptionsList.isEmpty()) {
                                qaType.setAnswers(answerOptionsList);
                            }

                        }
                        if (qaType.getType().toString().equals("SINGLE")) {
                            answerOptionsList = qaType.getAnswers();
                            answerOptionsListObj = new ArrayList<AnswerOptionsType>();
                            if (!answerOptionsList.isEmpty()) {
                                answerOptionsListObj.add(answerOptionsList.get(0));
                                qaType.setAnswers(answerOptionsListObj);
                            }

                        }
                    }

                }
            }
            assessmentTypeObj = service.saveAndCompleteAssessmentAnswer(uc, assessmentTypeObj, 1L);
        }

    }

}
