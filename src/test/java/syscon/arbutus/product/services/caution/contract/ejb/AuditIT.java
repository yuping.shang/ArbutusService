package syscon.arbutus.product.services.caution.contract.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.PersonTest;
import syscon.arbutus.product.services.common.StaffTest;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.person.contract.dto.caution.CautionType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.realization.util.BeanHelper;

import static org.testng.Assert.assertNotNull;

public class AuditIT extends BaseIT {

    private static String CODE_V = "90";
    private static String CODE_E = "55";
    private static String CODE_SECURITY = "SECURITY";
    private static String CODE_MEDICAL = "MEDICAL";

    private PersonTest psnTest;
    private StaffTest stfTest;

    private PersonService service;

    private Long staffId;

    @BeforeClass
    public void beforeClass() {

        service = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        uc = super.initUserContext();

        psnTest = new PersonTest();

        stfTest = new StaffTest();
        staffId = stfTest.createStaff();

        service.deleteAllCautionType(uc);
    }

    @Test
    public void lookupJNDI() {

        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "lookupJNDI" })
    public void reset() {
        service.deleteAllCautionType(uc);
    }

    @Test
    public void create() {

        //clean db

        Long personId = psnTest.createPerson();
        Date presentDate = BeanHelper.createDate();
        CautionType caution = new CautionType(null, CODE_V, CODE_MEDICAL, presentDate, null, null, personId, null);
        CautionType ret = service.createCautionType(uc, caution);
        assert (ret != null);

        personId = psnTest.createPerson();
        caution = new CautionType();
        caution.setCode(CODE_E);
        caution.setCategory(CODE_SECURITY);
        caution.setEffectiveDate(presentDate);
        caution.setExpiryDate(presentDate);
        caution.setAuthorizedBy(staffId);
        caution.setPersonId(personId);

        List<CommentType> comments = new ArrayList<CommentType>();
        comments.add(getComment(null, presentDate, "Comment 1"));
        comments.add(getComment(null, presentDate, "Comment 2"));
        caution.setComments(comments);

        CautionType retCaution = service.createCautionType(uc, caution);

        assert (retCaution != null);

    }

    @Test
    public void update() {

        //Create new caution
        Long cautionId = createCaution();

        //Add new comments,
        CautionType caution = service.getCautionType(uc, cautionId);

        CommentType comment = getComment(null, new Date(), "Comment Update 3");
        caution.getComments().add(comment);
        Long staffIdNew = stfTest.createStaff();
        caution.setAuthorizedBy(staffIdNew);
        caution.setExpiryDate(new Date());

        service.updateCautionType(uc, caution);
        CautionType ret = service.getCautionType(uc, cautionId);

        assert (ret != null);
    }

    @Test
    public void delete() {

        //this will success
        Long cautionId = createCaution();
        Long ret = service.deleteCautionType(null, service.getCautionType(null, cautionId));
        assert (ret.equals(ReturnCode.Success.returnCode()));

    }

    private Long createCaution() {
        //Create new caution
        Date presentDate = BeanHelper.createDate();
        Date effectiveDate = BeanHelper.createDate();

        List<CommentType> comments = new ArrayList<CommentType>();
        comments.add(getComment(null, presentDate, "Comment 1"));
        comments.add(getComment(null, presentDate, "Comment 2"));

        Long personId = psnTest.createPerson();
        CautionType cautionNew = new CautionType(null, CODE_V, CODE_SECURITY, effectiveDate, null, comments, personId, null);
        CautionType retCreate = service.createCautionType(uc, cautionNew);

        assert (retCreate.getCautionId() != null);
        return retCreate.getCautionId();
    }

    private CommentType getComment(Long commentId, Date commentDate, String commentText) {
        CommentType ret = new CommentType();
        ret.setCommentIdentification(commentId);
        ret.setUserId("TestUserOne");
        ret.setCommentDate(commentDate);
        ret.setComment(commentText);
        return ret;
    }

    private Date createFutureDate(int days) {
        Date current = new Date();
        return BeanHelper.nextNthDays(current, days);
    }

}
