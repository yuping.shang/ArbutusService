package syscon.arbutus.product.services.caution.contract.ejb;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.*;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.ArbutusOptimisticLockException;
import syscon.arbutus.product.services.core.exception.DataExistException;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.person.contract.dto.caution.CautionSearchType;
import syscon.arbutus.product.services.person.contract.dto.caution.CautionType;
import syscon.arbutus.product.services.person.contract.dto.caution.CautionsReturnType;
import syscon.arbutus.product.services.person.contract.dto.caution.CommentSearchType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.realization.util.BeanHelper;

import static org.testng.Assert.assertNotNull;

@ModuleConfig
public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);

    private static String CODE_V = "90";
    private static String CODE_E = "55";
    private static String CODE_S = "30";
    private static String CODE_I = "80";
    private static String CODE_SECURITY = "SECURITY";
    private static String CODE_MEDICAL = "MEDICAL";
    private static String CODE_EMPTY = "MEDICAL";

    private PersonService service;

    private PersonTest psnTest;
    private StaffTest stfTest;
    private PersonIdentityTest piTest;

    private List<Long> personIds;
    private Long staffId;

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        uc = super.initUserContext();

        psnTest = new PersonTest();
        piTest = new PersonIdentityTest();

        stfTest = new StaffTest();
        staffId = stfTest.createStaff();
    }

    @AfterClass
    public void afterClass() {

        service.deleteAllCautionType(uc);
        stfTest.deleteAll();
        piTest.deleteAll();
        psnTest.deleteAll();

    }

    @Test
    public void testGetVersion() {
        String version = service.getCautionTypeVersion(null);
        assert ("1.1".equals(version));
    }

    @Test
    public void lookupJNDI() {
        log.info("lookupJNDI Begin");
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "lookupJNDI" })
    public void reset() {
        service.deleteAllCautionType(uc);
    }

    @Test
    public void createNegative() {

        //CautionType is null, this will fail
        verifyCreateNegative(null, InvalidInputException.class);

        //the personId is null, this will fail.
        Date presentDate = BeanHelper.createDate();
        CautionType caution = new CautionType(null, CODE_V, null, presentDate, null, null, null, null);
        verifyCreateNegative(caution, InvalidInputException.class);

        //Reference code is null, this will fail
        caution = new CautionType(null, null, "", presentDate, null, null, staffId, null);
        verifyCreateNegative(caution, InvalidInputException.class);

    }

    @Test
    public void create() {

        //clean db

        Long personId = psnTest.createPerson();
        Date presentDate = BeanHelper.createDate();
        CautionType caution = new CautionType(null, CODE_V, CODE_EMPTY, presentDate, null, null, personId, null);
        CautionType ret = service.createCautionType(uc, caution);
        assert (ret != null);

        personId = psnTest.createPerson();
        caution = new CautionType();
        caution.setCode(CODE_E);
        caution.setCategory(CODE_SECURITY);
        caution.setEffectiveDate(presentDate);
        caution.setExpiryDate(presentDate);
        caution.setAuthorizedBy(staffId);
        caution.setPersonId(personId);

        List<CommentType> comments = new ArrayList<CommentType>();
        comments.add(getComment(null, presentDate, "Comment 1"));
        comments.add(getComment(null, presentDate, "Comment 2"));
        caution.setComments(comments);

        CautionType retCaution = service.createCautionType(uc, caution);

        assert (retCaution != null);

    }

    @Test
    public void testCreate() {

        //Create new caution
        Date presentDate = BeanHelper.createDate();
        Date effectiveDate = BeanHelper.createDate();

        List<CommentType> comments = new ArrayList<CommentType>();
        comments.add(getComment(null, presentDate, "Comment 1"));
        comments.add(getComment(null, presentDate, "Comment 2"));

        Long personId = psnTest.createPerson();
        CautionType cautionNew = new CautionType(null, CODE_V, CODE_SECURITY, effectiveDate, null, comments, personId, null);
        CautionType retCreate = service.createCautionType(uc, cautionNew);
        assert (retCreate.getCautionId() != null);

        log.info("User 1 create the caution 1 -- Success.");

        verifyCreateNegative(cautionNew, DataExistException.class);
        log.info("User 2 is trying to create the same caution 1 -- Failed with Data Exist Exception, and transaction rollback.");

    }

    @Test
    public void testUpdate() {

        //Create new caution
        Long cautionId = createCaution();

        CautionType caution = service.getCautionType(uc, cautionId);

        caution.setEffectiveDate(createFutureDate(5)); //update for effectiveDate
        service.updateCautionType(uc, caution);
        log.info("User 1 update for effective date -- Success.");

        //Should occur the optimistic lock exception
        caution.setExpiryDate(createFutureDate(20));    //update for expiryDate
        verifyUpdateNegative(caution, ArbutusOptimisticLockException.class);
        log.info("User 2 update for expiry date -- Failed with optimistic lock exception.");

    }

    @Test
    public void update() {

        //clean db and reload meta data

        //Create new caution
        Long cautionId = createCaution();

        CautionType caution = service.getCautionType(uc, cautionId);
        Long currentPersonId = caution.getPersonId();
        //remove exist comments, it will be ignored, and no error will be thrown.
        caution.getComments().clear();
        service.updateCautionType(uc, caution);
        //update associations to another person
        caution.setPersonId(5L);
        verifyUpdateNegative(caution, InvalidInputException.class);
        //Add new comments,
        CommentType comment = getComment(null, new Date(), "Comment Update 3");
        caution.getComments().add(comment);
        caution.setAuthorizedBy(staffId); //update for authorization
        caution.setEffectiveDate(createFutureDate(5)); //update for effectiveDate
        caution.setExpiryDate(createFutureDate(20));    //update for expiryDate
        caution.setPersonId(currentPersonId);
        //	ret = service.update(uc, caution);
        verifyUpdateNegative(caution, ArbutusOptimisticLockException.class);
        //Should fail because same record was updated.
        caution.setExpiryDate(createFutureDate(30));    //update for expiryDate
        caution.setPersonId(currentPersonId);
        //ret = service.update(uc, caution);
        verifyUpdateNegative(caution, ArbutusOptimisticLockException.class);

    }

    @Test
    public void get() {

        //clean db and reload meta data

        //cautionId is null, this will fail.
        verifyGetNegative(null, InvalidInputException.class);

        //cautionId 1000000L doesn't exist, this will fail.
        verifyGetNegative(1000000L, DataNotExistException.class);

        //this will success
        Long cautionId = createCaution();
        CautionType ret = service.getCautionType(uc, cautionId);
        assert (ret.getCautionId().equals(cautionId));

    }

    @Test
    public void getAll() {
        //Call getAll method
        Set<Long> ret = service.getAllCautionType(uc);
        assert (ret.size() >= 0L);

    }

    //@Test (dependsOnMethods = { "getAll" })
    @Test
    public void getCount() {

        log.info("getCount Begin");

        Long ret = service.getCautionTypeCount(uc);
        assert (ret >= 0L);

    }

    @Test
    public void getStamp() {

        //this will success
        Long cautionId = createCaution();
        StampType stamp = service.getCautionTypeStamp(null, cautionId);
        assert (stamp != null);
    }

    @Test
    public void getNiem() {

        //cautionId is null, this will fail.
        String ret = service.getCautionTypeNiem(null, null);
        assert (ret != null);

        //this will success
        Long cautionId = createCaution();
        ret = service.getCautionTypeNiem(null, cautionId);
        assert (ret != null);
    }

    @Test
    public void delete() {

        //cautionId is null, this will fail.
        verifyDeleteNegative(null, InvalidInputException.class);

        //cautionId 1000000L doesn't exist, this will fail.
        verifyDeleteNegative(1000000L, DataNotExistException.class);

        //this will success
        Long cautionId = createCaution();
        Long ret = service.deleteCautionType(null, service.getCautionType(null, cautionId));
        assert (ret.equals(ReturnCode.Success.returnCode()));

    }

    //Enable to test the searchMaxLimit function.
    //@Test
    public void searchMaxLimit() {
        log.info("searchMaxLimit Begin");

        //Reset database and load default meta data

        //Create test data for search
        Date presentDate = BeanHelper.createDate();
        for (int i = 1; i <= 300; i++) {

            CautionType caution = new CautionType();
            caution.setCode(CODE_V);
            caution.setCategory(CODE_SECURITY);
            caution.setEffectiveDate(presentDate);    //today
            caution.setAuthorizedBy(staffId);

            List<CommentType> comments = new ArrayList<CommentType>();
            comments.add(getComment(null, presentDate, i + "SECURITY-V Comment 1"));
            comments.add(getComment(null, presentDate, i + "SECURITY-V Comment 2"));

            caution.setComments(comments);
            caution.setPersonId(Long.valueOf(i));

            service.createCautionType(uc, caution);

        }

        //search by code only, will get ArbutusRuntimeException because over searchMaxResult
        CautionSearchType cautionSearch = new CautionSearchType(null, CODE_V, null, null, null, null, null, null, null, null);

        CautionsReturnType ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret != null);
        assert (ret.getTotalSize() == 200L);

    }

    //@Test
    public void searchCreation() {

        //Create personId first
        personIds = new ArrayList<Long>();
        for (int i = 1; i <= 5; i++) {
            Long personId = psnTest.createPerson();
            personIds.add(personId);
        }

        //Create test data for search
        Date presentDate = BeanHelper.createDate();
        for (int i = 1; i <= 5; i++) {

            CautionType caution = new CautionType();
            caution.setCode(CODE_V);
            caution.setCategory(CODE_SECURITY);
            caution.setEffectiveDate(presentDate);    //today
            caution.setExpiryDate(createFutureDate(30));     //30 days later - Active
            caution.setAuthorizedBy(staffId);

            List<CommentType> comments = new ArrayList<CommentType>();
            comments.add(getComment(null, presentDate, i + "SECURITY-V Comment 1"));
            comments.add(getComment(null, presentDate, i + "SECURITY-V Comment 2"));

            caution.setComments(comments);
            caution.setPersonId(personIds.get(i - 1));

            service.createCautionType(uc, caution);

        }

        for (int i = 1; i <= 5; i++) {
            CautionType caution = new CautionType();
            caution.setCode(CODE_E);
            caution.setCategory(CODE_SECURITY);
            caution.setEffectiveDate(presentDate);     //Today - Active
            caution.setAuthorizedBy(staffId);

            List<CommentType> comments = new ArrayList<CommentType>();
            comments.add(getComment(null, presentDate, i + "SECURITY-E Comment 1"));
            comments.add(getComment(null, presentDate, i + "SECURITY-E Comment 2"));

            caution.setComments(comments);
            caution.setPersonId(personIds.get(i - 1));

            service.createCautionType(uc, caution);

        }

        Date effectiveDate = createPastDate(10);
        for (int i = 1; i <= 5; i++) {
            CautionType caution = new CautionType();
            caution.setCode(CODE_S);
            caution.setCategory(CODE_MEDICAL);
            caution.setEffectiveDate(effectiveDate);        //10 days ago
            caution.setExpiryDate(effectiveDate);            //10 days ago - inactive
            caution.setAuthorizedBy(staffId);

            List<CommentType> comments = new ArrayList<CommentType>();
            comments.add(getComment(null, effectiveDate, i + "MEDICAL-S Comment 1"));
            comments.add(getComment(null, effectiveDate, i + "MEDICAL-S Comment 2"));

            caution.setComments(comments);
            caution.setPersonId(personIds.get(i - 1));

            service.createCautionType(uc, caution);

        }

        effectiveDate = createFutureDate(10);
        for (int i = 1; i <= 5; i++) {
            CautionType caution = new CautionType();
            caution.setCode(CODE_I);
            caution.setCategory(CODE_EMPTY);
            caution.setEffectiveDate(effectiveDate);     //10 days later - inactive

            List<CommentType> comments = new ArrayList<CommentType>();
            comments.add(getComment(null, effectiveDate, i + "EMPTY-I Comment 1"));
            comments.add(getComment(null, effectiveDate, i + "EMPTY-I Comment 2"));

            caution.setComments(comments);
            caution.setPersonId(personIds.get(i - 1));

            service.createCautionType(uc, caution);

        }
    }

    @Test
    public void search() {

        reset();

        //create search data
        searchCreation();

        //the CautionSearchType is null, this will fail.
        verifySearchNegative(null, null, InvalidInputException.class);

        //TODO: to be add negative test for search

        //search by category only with empty string
        //CautionSearchType cautionSearch = new CautionSearchType(null, null, CODE_EMPTY, null, null, null, null, null, null, null);
        //verifySearchNegative(null, cautionSearch, InvalidInputException.class);

        //search by category only by one space string, no result return.
        CautionSearchType cautionSearch = new CautionSearchType(null, null, " ", null, null, null, null, null, null, null);
        //CautionsReturnType ret = service.search(uc, null, cautionSearch, 0L, 200L, null, null);
        //assert(ret == null);

        //search by association only
        Long personId = personIds.get(0); //1L;
        cautionSearch = new CautionSearchType(personId, null, null, null, null, null, null, null, null, null);
        CautionsReturnType ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 4L);

        //search by code only
        cautionSearch = new CautionSearchType(null, CODE_V, null, null, null, null, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //search by category only
        cautionSearch = new CautionSearchType(null, null, CODE_SECURITY, null, null, null, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 10L);

        //search by effectiveDateFrom only
        Date effectiveDateFrom = createPastDate(2);
        cautionSearch = new CautionSearchType(null, null, null, effectiveDateFrom, null, null, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 15L);

        //search by effectiveDateTo only
        Date effectiveDateTo = createFutureDate(2);
        cautionSearch = new CautionSearchType(null, null, null, null, effectiveDateTo, null, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 15L);

        //search by effectiveDateFrom and effectiveDateTo
        cautionSearch = new CautionSearchType(null, null, null, effectiveDateFrom, effectiveDateTo, null, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 10L);

        //search by expiryDateFrom only
        Date expiryDateFrom = createPastDate(2);
        cautionSearch = new CautionSearchType(null, null, null, null, null, expiryDateFrom, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //search by expiryDateTo only
        Date expiryDateTo = createFutureDate(2);
        cautionSearch = new CautionSearchType(null, null, null, null, null, null, expiryDateTo, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //search by expiryDateTo only
        expiryDateTo = createFutureDate(50);
        cautionSearch = new CautionSearchType(null, null, null, null, null, null, expiryDateTo, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 10L);

        //search by expiryDateFrom and expiryDateTo
        cautionSearch = new CautionSearchType(null, null, null, null, null, expiryDateFrom, expiryDateTo, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //Search by Comments only
        CommentSearchType comment = new CommentSearchType(null, null, "1SECURITY-V Comment 1");
        cautionSearch = new CautionSearchType(null, null, null, null, null, null, null, comment, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 1L);

        comment = new CommentSearchType(null, null, "A***");
        cautionSearch = new CautionSearchType(null, null, null, null, null, null, null, comment, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);

        //Search by Comments only - using wild-card search
        comment = new CommentSearchType(null, null, "1SECURITY-V Co*ent 1");
        cautionSearch = new CautionSearchType(null, null, null, null, null, null, null, comment, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 1L);

        //Search by Comments only - using wild-card search
        comment = new CommentSearchType(null, null, "*Y-V Comment 1");
        cautionSearch = new CautionSearchType(null, null, null, null, null, null, null, comment, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //Search by commentDateFrom only
        Date commentDateFrom = createPastDate(2);
        comment = new CommentSearchType(commentDateFrom, null, null);
        cautionSearch = new CautionSearchType(null, null, null, null, null, null, null, comment, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 15L);

        //Search by commentDateTo condition
        Date commentDateTo = createPastDate(2);
        comment = new CommentSearchType(null, commentDateTo, null);
        cautionSearch = new CautionSearchType(null, null, null, null, null, null, null, comment, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //Search by commentDateFrom and commentText
        comment = new CommentSearchType(commentDateFrom, null, "1SECURITY-V Comment 1");
        cautionSearch = new CautionSearchType(null, null, null, null, null, null, null, comment, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 1L);

        //Search by commentDateFrom, commentDateTo and commentText
        commentDateTo = createFutureDate(2);
        comment = new CommentSearchType(commentDateFrom, commentDateTo, "1SECURITY-V Comment 1");
        cautionSearch = new CautionSearchType(null, null, null, null, null, null, null, comment, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 1L);

        //Search by authorization only
        Long authorization = staffId;
        cautionSearch = new CautionSearchType(null, null, null, null, null, null, null, null, authorization, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 15L);

        //Search by  activeFlag only
        cautionSearch = new CautionSearchType(null, null, null, null, null, null, null, null, null, Boolean.TRUE);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 10L);

        //Search by subSet and CautionCode
        Set<Long> subSet = new HashSet<Long>();
        subSet = service.getAllCautionType(null);
        cautionSearch = new CautionSearchType(null, CODE_E, CODE_SECURITY, null, null, null, null, null, null, null);
        ret = service.searchCautionType(uc, subSet, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //////////////////////////////////////
        //Partial Search
        //////////////////////////////////////

        //Add comments condition for partial search
        comment = new CommentSearchType(null, null, "SECURITY-V*");
        cautionSearch = new CautionSearchType(null, null, null, null, null, null, null, comment, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //Add comments condition for partial search
        comment = new CommentSearchType(null, null, "*security-v*");
        cautionSearch = new CautionSearchType(null, null, null, null, null, null, null, comment, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        /////////////////////////////////////////////////////////////////
        //Multiple condition search
        /////////////////////////////////////////////////////////////////

        //Search by association and activeFlag is null
        personId = personIds.get(0); //1L;
        cautionSearch = new CautionSearchType(personId, null, null, null, null, null, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 4L);

        //search by association with active flag is true
        cautionSearch = new CautionSearchType(personId, null, null, null, null, null, null, null, null, Boolean.TRUE);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 2L);

        //search by association with active flag is false
        cautionSearch = new CautionSearchType(personId, null, null, null, null, null, null, null, null, Boolean.FALSE);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 2L);

        //Search by association and activeFlag is true, and CautionCode
        cautionSearch = new CautionSearchType(personId, CODE_V, null, null, null, null, null, null, null, Boolean.TRUE);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 1L);

        //Search by association and activeFlag is false, and CautionCode
        cautionSearch = new CautionSearchType(personId, CODE_V, null, null, null, null, null, null, null, Boolean.FALSE);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 0L);

        //Search by association, CautionCode, CautionCategory
        cautionSearch = new CautionSearchType(personId, CODE_V, CODE_SECURITY, null, null, null, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 1L);

        //Search by association, CautionCode, CautionCategory, effectiveDateFrom
        effectiveDateFrom = createPastDate(2);
        cautionSearch = new CautionSearchType(personId, CODE_V, CODE_SECURITY, effectiveDateFrom, null, null, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 1L);

        //Search by association, CautionCode, CautionCategory, effectiveDateFrom, effectiveDataTo
        effectiveDateTo = createPastDate(1);
        cautionSearch = new CautionSearchType(personId, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, null, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 0L);

        //Search by association, CautionCode, CautionCategory, effectiveDateFrom, effectiveDataTo
        effectiveDateTo = createFutureDate(2);
        cautionSearch = new CautionSearchType(personId, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, null, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 1L);

        //Search by association, CautionCode, CautionCategory, effectiveDateFrom,
        //effectiveDataTo, expiryDateFrom
        expiryDateFrom = createPastDate(2);
        cautionSearch = new CautionSearchType(personId, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 1L);

        //Search by association, CautionCode, CautionCategory, effectiveDateFrom,
        //effectiveDataTo, expiryDateFrom, expiryDateTo
        expiryDateTo = createFutureDate(2);
        cautionSearch = new CautionSearchType(personId, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 0L);

        //Search by association, CautionCode, CautionCategory, effectiveDateFrom,
        //effectiveDataTo, expiryDateFrom, expiryDateTo
        expiryDateTo = createFutureDate(50);
        cautionSearch = new CautionSearchType(personId, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 1L);

        //Search by association, CautionCode, CautionCategory, effectiveDateFrom,
        //effectiveDataTo, expiryDateFrom, expiryDateTo, authorization
        cautionSearch = new CautionSearchType(personId, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, null, authorization,
                null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 1L);

        //Search by association, CautionCode, CautionCategory, effectiveDateFrom,
        //effectiveDataTo, expiryDateFrom, expiryDateTo, authorization, comment
        comment = new CommentSearchType(null, null, "SECURITY-V*");
        cautionSearch = new CautionSearchType(personId, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, comment, authorization,
                null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 1L);

        //Search by association, CautionCode, CautionCategory, effectiveDateFrom,
        //effectiveDataTo, expiryDateFrom, expiryDateTo, authorization, comment
        commentDateFrom = createPastDate(2);
        comment = new CommentSearchType(commentDateFrom, null, "SECURITY-V*");
        cautionSearch = new CautionSearchType(personId, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, comment, authorization,
                null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 1L);

        //Search by association, CautionCode, CautionCategory, effectiveDateFrom,
        //effectiveDataTo, expiryDateFrom, expiryDateTo, authorization, comment
        commentDateTo = createPastDate(2);
        comment = new CommentSearchType(commentDateFrom, commentDateTo, "SECURITY-V*");
        cautionSearch = new CautionSearchType(personId, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, comment, authorization,
                null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);

        assert (ret.getCaution().size() == 0L);

        //Search by association, CautionCode, CautionCategory, effectiveDateFrom,
        //effectiveDataTo, expiryDateFrom, expiryDateTo, authorization, comment
        commentDateTo = createFutureDate(2);
        comment = new CommentSearchType(commentDateFrom, commentDateTo, "SECURITY-V*");
        cautionSearch = new CautionSearchType(personId, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, comment, authorization,
                null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 1L);

        //Search by association, CautionCode, CautionCategory, effectiveDateFrom,
        //effectiveDataTo, expiryDateFrom, expiryDateTo, authorization, comment, and activeFlag is true
        cautionSearch = new CautionSearchType(personId, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, comment, authorization,
                Boolean.TRUE);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 1L);

        //Search by association, CautionCode, CautionCategory, effectiveDateFrom,
        //effectiveDataTo, expiryDateFrom, expiryDateTo, authorization, comment, and activeFlag is true
        cautionSearch = new CautionSearchType(personId, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, comment, authorization,
                Boolean.FALSE);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 0L);

        //Search by CautionCode
        cautionSearch = new CautionSearchType(null, CODE_V, null, null, null, null, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //Search by CautionCode and CautionCategory
        cautionSearch = new CautionSearchType(null, CODE_V, CODE_SECURITY, null, null, null, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //Search by CautionCode, CautionCategory, effectiveDateFrom
        effectiveDateFrom = createPastDate(2);
        cautionSearch = new CautionSearchType(null, CODE_V, CODE_SECURITY, effectiveDateFrom, null, null, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //Search by CautionCode, CautionCategory, effectiveDateFrom, effectiveDateTo
        effectiveDateTo = createPastDate(2);
        cautionSearch = new CautionSearchType(null, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, null, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 0L);

        //Search by CautionCode, CautionCategory, effectiveDateFrom, effectiveDateTo
        effectiveDateTo = createFutureDate(2);
        cautionSearch = new CautionSearchType(null, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, null, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //Search by CautionCode, CautionCategory, effectiveDateFrom, effectiveDateTo
        //expiryDateFrom
        expiryDateFrom = createPastDate(2);
        cautionSearch = new CautionSearchType(null, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, null, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //Search by CautionCode, CautionCategory, effectiveDateFrom, effectiveDateTo
        //expiryDateFrom, expiryDateTo
        expiryDateTo = createFutureDate(2);
        cautionSearch = new CautionSearchType(null, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 0L);

        //Search by CautionCode, CautionCategory, effectiveDateFrom, effectiveDateTo
        //expiryDateFrom, expiryDateTo
        expiryDateTo = createFutureDate(50);
        cautionSearch = new CautionSearchType(null, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, null, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //Search by CautionCode, CautionCategory, effectiveDateFrom, effectiveDateTo
        //expiryDateFrom, expiryDateTo, authorization
        cautionSearch = new CautionSearchType(null, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, null, authorization, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //Search by CautionCode, CautionCategory, effectiveDateFrom, effectiveDateTo
        //expiryDateFrom, expiryDateTo, comment
        comment = new CommentSearchType(null, null, "SECURITY-V*");
        cautionSearch = new CautionSearchType(null, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, comment, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //Search by CautionCode, CautionCategory, effectiveDateFrom, effectiveDateTo
        //expiryDateFrom, expiryDateTo, comment
        commentDateFrom = createPastDate(2);
        comment = new CommentSearchType(commentDateFrom, null, "SECURITY-V*");
        cautionSearch = new CautionSearchType(null, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, comment, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        //Search by CautionCode, CautionCategory, effectiveDateFrom, effectiveDateTo
        //expiryDateFrom, expiryDateTo, comment
        commentDateTo = createPastDate(2);
        comment = new CommentSearchType(commentDateFrom, commentDateTo, "SECURITY-V*");
        cautionSearch = new CautionSearchType(null, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, comment, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 0L);

        //Search by CautionCode, CautionCategory, effectiveDateFrom, effectiveDateTo
        //expiryDateFrom, expiryDateTo, comment
        commentDateTo = createFutureDate(2);
        comment = new CommentSearchType(commentDateFrom, commentDateTo, "SECURITY-V*");
        cautionSearch = new CautionSearchType(null, CODE_V, CODE_SECURITY, effectiveDateFrom, effectiveDateTo, expiryDateFrom, expiryDateTo, comment, null, null);
        ret = service.searchCautionType(uc, null, cautionSearch, 0L, 200L, null, null);
        assert (ret.getCaution().size() == 5L);

        log.info("search End");
    }

    private void verifySearchNegative(Set<Long> subsetSearch, CautionSearchType search, Class<?> expectionClazz) {
        try {
            CautionsReturnType ret = service.searchCautionType(uc, subsetSearch, search, 0L, 200L, null, null);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyGetNegative(Long cautionId, Class<?> expectionClazz) {
        try {
            CautionType ret = service.getCautionType(uc, cautionId);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyDeleteNegative(Long cautionId, Class<?> expectionClazz) {
        try {
            Long ret = service.deleteCautionType(uc, service.getCautionType(null, cautionId));
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyCreateNegative(CautionType caution, Class<?> expectionClazz) {
        try {
            CautionType ret = service.createCautionType(uc, caution);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private void verifyUpdateNegative(CautionType caution, Class<?> expectionClazz) {
        try {
            service.updateCautionType(uc, caution);
        } catch (Exception ex) {
            assert (ex.getClass().equals(expectionClazz));
        }
    }

    private CommentType getComment(Long commentId, Date commentDate, String commentText) {
        CommentType ret = new CommentType();
        ret.setCommentIdentification(commentId);
        ret.setUserId("TestUserOne");
        ret.setCommentDate(commentDate);
        ret.setComment(commentText);
        return ret;
    }

    private Long createCaution() {
        //Create new caution
        Date presentDate = BeanHelper.createDate();
        Date effectiveDate = BeanHelper.createDate();

        List<CommentType> comments = new ArrayList<CommentType>();
        comments.add(getComment(null, presentDate, "Comment 1"));
        comments.add(getComment(null, presentDate, "Comment 2"));

        Long personId = psnTest.createPerson();
        CautionType cautionNew = new CautionType(null, CODE_V, CODE_SECURITY, effectiveDate, null, comments, personId, null);
        CautionType retCreate = service.createCautionType(uc, cautionNew);

        assert (retCreate.getCautionId() != null);
        return retCreate.getCautionId();
    }

    private Date createFutureDate(int days) {
        Date current = new Date();
        return BeanHelper.nextNthDays(current, days);
    }

    private Date createPastDate(int days) {
        Date current = new Date();
        return BeanHelper.getPastDate(current, days);
    }
}
