package syscon.arbutus.product.services.organization.contract.ejb;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilitycountactivity.contract.interfaces.FacilityCountActivityService;
import syscon.arbutus.product.services.location.contract.interfaces.LocationService;
import syscon.arbutus.product.services.organization.contract.dto.Organization;
import syscon.arbutus.product.services.organization.contract.dto.OrganizationSearch;
import syscon.arbutus.product.services.organization.contract.dto.OrganizationsReturn;
import syscon.arbutus.product.services.organization.contract.interfaces.OrganizationService;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

@ModuleConfig
public class OrganizationServiceIT extends BaseIT {
    private static Logger log = LoggerFactory.getLogger(OrganizationServiceIT.class);

    final Long SUCCESS = 1L;
    Set<Long> facilities = new HashSet<Long>();
    private Organization organization = null;
    private OrganizationService service;
    private LocationService locService;
    private SupervisionService supService;
    private FacilityService facService;
    private FacilityCountActivityService fcaService;

    public static Long randLong(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return Long.valueOf((long) randomNum);
    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (OrganizationService) JNDILookUp(this.getClass(), OrganizationService.class);
        facService = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        locService = (LocationService) JNDILookUp(this.getClass(), LocationService.class);
        fcaService = (FacilityCountActivityService) JNDILookUp(this.getClass(), FacilityCountActivityService.class);
        supService = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);

        uc = super.initUserContext();
        /*supService.deleteAll(uc);
        locService.deleteAll(uc);
		service.deleteAll(uc);
		fcaService.deleteAll(uc);
		facService.deleteAll(uc);*/
        facilities.add(createFacility(null));
    }

    private void clearServices() {
	   /* locService.deleteAll(uc);
	    facService.deleteAll(uc);
		service.deleteAll(uc);*/

        facilities = new HashSet<Long>();
    }

    private void createData() {
        this.facilities.add(createFacility(null));
    }

    @Test
    public void lookupJNDI() {
        log.info("lookupJNDI Begin");
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "lookupJNDI" })
    public void testGetVersion() {
        String version = service.getVersion(uc);
        assert ("1.1".equals(version));
    }

    @Test(enabled = true)
    public void deleteAll() {
        Long count = service.getCount(uc);
        assert (count > 0L);
        Long retDelete = service.deleteAll(uc);
        Long SUCCESS = 1L;
        assert (retDelete == SUCCESS);// 1 is Code for SUCCESS

        count = service.getCount(uc);
        assert (count == 0L);
    }

    @Test(dependsOnMethods = { "lookupJNDI" })
    public void create() {

        clearServices();

        log.info("org 1");
        Organization org = new Organization();
        Organization ret;
        Organization orgRet = null;

        org.setOrganizationName("New York Department of Correction");
        org.setOrganizationLocalId(null);
        org.setOrganizationAbbreviation("NYDOC");
        org.setOrganizationORIIdentification(null);
        ret = service.create(uc, org);
        assertNotNull(ret);
        assertNotNull(ret.getOrganizationIdentification());
        assertEquals(ret.getOrganizationName(), org.getOrganizationName());
        assertEquals(ret.getOrganizationAbbreviation(), org.getOrganizationAbbreviation());

        log.info("org 2");

        org = new Organization();
        orgRet = null;
        org.setOrganizationName("Los Angeles Department of Correction");
        org.setOrganizationLocalId("LA456-05-356");
        org.setOrganizationAbbreviation("LADOC");
        org.setOrganizationORIIdentification(generateORI());
        Set<String> categories = new HashSet<String>();
        categories.add(new String("NONCJ"));
        categories.add(new String("CJ"));
        categories.add(new String("CORP"));
        org.setOrganizationCategories(categories);
        ret = service.create(uc, org);
        log.info(String.format("org 2.5 ret %s cat %s ", ret.getOrganizationIdentification(), ret.getOrganizationCategories()));
        assertNotNull(ret);
        assertNotNull(ret.getOrganizationIdentification());
        assertEquals(ret.getOrganizationName(), org.getOrganizationName());
        assertEquals(ret.getOrganizationAbbreviation(), org.getOrganizationAbbreviation());
        assertEquals(ret.getOrganizationORIIdentification(), org.getOrganizationORIIdentification());
        assertEquals(ret.getOrganizationCategories(), org.getOrganizationCategories());

        log.info("org 3");

        org = new Organization();

        Set<String> categoriesType = new HashSet<String>();
        categoriesType.add(new String("CJ"));
        // categoriesType.add(new String( "NONCJ"));
        categoriesType.add(new String("CORP"));
        categoriesType.add(new String("PROGRAM"));
        categoriesType.add(new String("GOVT"));

        Set<Long> facilities = new HashSet<Long>();
        facilities.add(createFacility(null));
        facilities.add(createFacility(null));
        facilities.add(createFacility(null));

        org.setOrganizationIdentification(111L);
        org.setOrganizationCategories(categoriesType);
        org.setOrganizationAbbreviation("Richmond Govt");
        org.setOrganizationLocalId("6500 River Road, Richomnd, BC");
        org.setOrganizationName("Richmond Court");
        org.setOrganizationORIIdentification("RCMP1" + randLong(1000, 9999));

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        log.info("org 3");

        Date nextYear = cal.getTime();
        org.setOrganizationTerminationDate(nextYear);

        org.setOrganizationParent(null);

        log.debug(org.toString());

        ret = service.create(uc, org);
        orgRet = ret;
        assertNotNull(orgRet);

        log.info("org 4");

        org.setOrganizationIdentification(orgRet.getOrganizationIdentification());
        organization = orgRet;
    }

    @Test
    public void get() {
        clearServices();

        Long orgId = createOrganization();
        log.info(String.format("org get %s", orgId));
        Organization ret = service.get(uc, orgId);
        assertNotNull(ret);
        //assertEquals(ret, organization);
    }

    @Test
    public void getAll() {
        clearServices();

        Set<Long> ret = service.getAll(uc);
        assertNotNull(ret);
        int size = ret.size();

        Long orgId = createOrganization();
        log.info(String.format("org get %s", orgId));
        ret = service.getAll(uc);
        assertEquals(ret.size(), size + 1);
    }

    @Test
    public void delete() {
        clearServices();

        Long orgId = createOrganization();

        Set<Long> ret = service.getAll(uc);
        assertNotNull(ret);
        int size = ret.size();

        service.delete(uc, orgId);

        ret = service.getAll(uc);
        assertNotNull(ret);
        assert (ret.size() == size - 1);
    }

    @Test
    public void getCount() {
        clearServices();

        Long orgId = createOrganization();
        log.info(String.format("org getCount  %s", orgId));
        Long ret = service.getCount(uc);

        Set<Long> all = service.getAll(uc);

        assertNotNull(ret);
        Long newSize = new Long(all.size());
        assert (ret.equals(newSize));
    }

    @Test(enabled = true)
    public void getStamp() {

        clearServices();
        createData();

        Long orgId = createOrganization();

        StampType retStamp = service.getStamp(null, orgId);

        assert (retStamp != null);
    }

    @Test(enabled = true)
    public void getVersion() {

        clearServices();

        String ver = service.getVersion(uc);

        assert (ver != null);
    }

    @Test
    public void update() {

        clearServices();

        OrganizationSearch search = new OrganizationSearch();
        search.setOrganizationORIIdentification("FBI123456");
        OrganizationsReturn rets = service.search(uc, null, search, null, null, null);
        for (Organization org : rets.getOrganizations()) {
            service.delete(uc, org.getOrganizationIdentification());
        }

        Long orgId = createOrganization();

        Organization org = service.get(uc, orgId);

        log.info(String.format("org update %s", orgId));

        Organization ret = null;

        org.setOrganizationName("BC Court");
        assertEquals(org.getOrganizationName(), "BC Court");

        org.setOrganizationLocalId("1st Ave, Vancouver, BC");
        assertEquals(org.getOrganizationLocalId(), "1st Ave, Vancouver, BC");

        org.setOrganizationParent(null);

        org.setOrganizationORIIdentification("FBI123456");

        org.setOrganizationAbbreviation("Abbr");
        assertEquals(org.getOrganizationAbbreviation(), "Abbr");

        org.setOrganizationTerminationDate(Calendar.getInstance().getTime());

        org.setOrganizationCategories(new HashSet<String>(Arrays.asList(new String[] { new String("CJ"), new String("NONCJ") })));

        org.setFacilities((new HashSet<Long>(Arrays.asList(new Long[] { createFacility(null), createFacility(null) }))));

        log.info(String.format("org update 2 %s", orgId));
        ret = service.update(uc, org);

        assertNotNull(ret);

        log.info(String.format("org update 3 %s", orgId));

        ret = service.update(uc, ret);
        assertNotNull(ret);

        log.info(String.format("org update 4 %s", org));

        ret.setOrganizationORIIdentification(null);
        ret = service.update(uc, ret);
        assertNotNull(ret);
        assertNull(ret.getOrganizationORIIdentification());

        log.info(String.format("org update 4 %s", orgId));

        ret.setOrganizationORIIdentification(null);
        ret = service.update(uc, ret);
        assertNotNull(ret);
        assertNull(ret.getOrganizationORIIdentification());

        log.info(String.format("org update 5 %s", orgId));

        ret.setOrganizationORIIdentification("FBI123456");
        ret = service.update(uc, ret);
        assertNotNull(ret);
        assertEquals(ret.getOrganizationORIIdentification(), "FBI123456");

        log.info(String.format("org update 6 %s", orgId));

        ret.setOrganizationORIIdentification("FBI123456");

        // update abbr and categories
        ret.setOrganizationAbbreviation("Abbr update");
        ret.getOrganizationCategories().removeAll(ret.getOrganizationCategories());
        ret.setOrganizationCategories(new HashSet<String>(Arrays.asList(new String[] { new String("CORP") })));
        ret.setFacilities(null);

        Organization ret2 = service.update(uc, ret);
        assertNotNull(ret2);
        assertEquals(ret2.getOrganizationORIIdentification(), "FBI123456");

        log.info(String.format("org update 7 %s", ret2));

        Organization orgRet = ret2;

        // Can be updated
        assertEquals(orgRet.getOrganizationName(), org.getOrganizationName());
        log.info(String.format("org update 8 %s", orgId));
        assertEquals(orgRet.getOrganizationLocalId(), org.getOrganizationLocalId());
        log.info(String.format("org update 9 %s", orgId));
        assertEquals(orgRet.getOrganizationParent(), org.getOrganizationParent());
        log.info(String.format("org update 10 %s", orgRet));

        log.info(String.format("org update 11 %s %s", orgRet.getOrganizationAbbreviation(), org.getOrganizationAbbreviation()));
        log.info(String.format("org update 11 %s %s", orgRet.getOrganizationORIIdentification(), org.getOrganizationORIIdentification()));
        log.info(String.format("org update 11 %s %s", orgRet.getOrganizationCategories(), org.getOrganizationCategories()));
        log.info(String.format("org update 11 %s %s", orgRet.getOrganizationTerminationDate(), org.getOrganizationTerminationDate()));
        log.info(String.format("org update 11 %s %s", orgRet.getFacilities(), org.getFacilities()));
        assertEquals(orgRet.getOrganizationORIIdentification(), org.getOrganizationORIIdentification());

        // Cannot be updated
        // Check these tests, there is something wrong here
        assertNotEquals(orgRet.getOrganizationAbbreviation(), org.getOrganizationAbbreviation());
        log.info(String.format("org update 12 %s", orgId));
        assertNotEquals(orgRet.getOrganizationTerminationDate(), org.getOrganizationTerminationDate());
        log.info(String.format("org update 13 %s", orgId));
        assertNotEquals(orgRet.getOrganizationCategories(), org.getOrganizationCategories());
        log.info(String.format("org update 14 %s", orgId));
        assertNotEquals(orgRet.getFacilities(), org.getFacilities());
        log.info(String.format("org update 15 %s", orgId));
    }

    @Test
    public void deactivate() {

        clearServices();

        Long orgId = createOrganization();

        Organization org = service.get(uc, orgId);

        Organization ret = null;
        Organization orgRet = null;

        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date yesterday = cal.getTime();

        cal.add(Calendar.DAY_OF_MONTH, 2);
        Date tomorrow = cal.getTime();

        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date today = cal.getTime();

        cal.add(Calendar.YEAR, 1);
        Date nextYear = cal.getTime();

        cal.add(Calendar.YEAR, -2);
        Date lastYear = cal.getTime();

        assertEquals(org.getOrganizationTerminationDate(), nextYear);

        // Tomorrow
        org.setOrganizationTerminationDate(tomorrow);
        ret = service.deactivate(uc, org);
        orgRet = ret;
        assertNotNull(orgRet);
        assertEquals(orgRet.getOrganizationTerminationDate(), tomorrow);
        assertTrue(orgRet.getOrganizationStatus());

        // Next Year
        org.setOrganizationTerminationDate(nextYear);
        ret = service.deactivate(uc, org);
        orgRet = ret;
        assertNotNull(orgRet);
        assertEquals(orgRet.getOrganizationTerminationDate(), nextYear);
        assertTrue(orgRet.getOrganizationStatus());

        // Today
        org.setOrganizationTerminationDate(today);
        ret = service.deactivate(uc, org);
        orgRet = ret;
        assertNotNull(orgRet);
        assertEquals(orgRet.getOrganizationTerminationDate(), today);
        assertFalse(orgRet.getOrganizationStatus());

        // Yesterday
        org.setOrganizationTerminationDate(yesterday);
        ret = service.deactivate(uc, org);
        orgRet = ret;
        assertNotNull(orgRet);
        assertEquals(orgRet.getOrganizationTerminationDate(), yesterday);
        assertFalse(orgRet.getOrganizationStatus());

        // Last year
        org.setOrganizationTerminationDate(lastYear);
        ret = service.deactivate(uc, org);
        orgRet = ret;
        assertNotNull(orgRet);
        assertEquals(orgRet.getOrganizationTerminationDate(), lastYear);
        assertFalse(orgRet.getOrganizationStatus());
    }

    @Test
    public void activate() {

        clearServices();

        Long orgId = createOrganization();
        Organization org = service.get(uc, orgId);

        Organization ret = null;
        Organization orgRet = null;

        //assertFalse(org.getOrganizationStatus());

        ret = service.activate(uc, org);
        orgRet = ret;
        assertNotNull(orgRet);
        assertTrue(orgRet.getOrganizationStatus());
        assertNull(orgRet.getOrganizationTerminationDate());

        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        cal.add(Calendar.YEAR, 2);
        Date nextYear = cal.getTime();

        log.error(String.format("NExt Year %s", nextYear));
        org.setOrganizationTerminationDate(nextYear);
        ret = service.deactivate(uc, org);
        orgRet = ret;
        assertNotNull(orgRet);
        log.error(String.format("NExt Year %s %s", nextYear, orgRet));
        assertEquals(orgRet.getOrganizationTerminationDate().getYear(), nextYear.getYear());
        assertTrue(orgRet.getOrganizationStatus());
    }

    @Test
    public void addOrganizationCategory() {

        clearServices();

        Long orgId = createOrganization();

        Organization org = service.get(uc, orgId);

        log.info(String.format("org addOrganizationCategoryIT 0 %s", org.getOrganizationCategories()));

        assertNotNull(org);
        Long organizationId = org.getOrganizationIdentification();

        Organization ret = null;
        Organization orgRet = null;

        ret = service.addOrganizationCategory(uc, organizationId, new String("NONCJ"));
        orgRet = ret;
        assertNotNull(orgRet);

        log.info(String.format("org addOrganizationCategoryIT 1 %s", orgRet.getOrganizationCategories()));

        assertTrue(orgRet.getOrganizationCategories().containsAll(org.getOrganizationCategories()));
        //assertFalse(organization.getOrganizationCategories().containsAll(orgRet.getOrganizationCategories()));
        Set<String> categories = new HashSet<String>();
        categories.addAll(orgRet.getOrganizationCategories());
        categories.add(new String("NONCJ"));
        assertEquals(orgRet.getOrganizationCategories(), categories);
    }

    @Test
    public void deleteOrganizationCategory() {

        clearServices();

        Long orgId = createOrganization();
        organization = service.get(uc, orgId);

        Organization org = service.get(uc, orgId);

        assertNotNull(org);
        assertEquals(org, organization);
        Long organizationId = org.getOrganizationIdentification();

        Organization ret = null;
        Organization orgRet = null;

        ret = service.deleteOrganizationCategory(uc, organizationId, new String("NONCJ"));
        orgRet = ret;
        assertNotNull(orgRet);

        assertFalse(orgRet.getOrganizationCategories().containsAll(organization.getOrganizationCategories()));
        assertTrue(organization.getOrganizationCategories().containsAll(orgRet.getOrganizationCategories()));
        Set<String> categories = new HashSet<String>();
        categories.addAll(orgRet.getOrganizationCategories());
        categories.add(new String("NONCJ"));
        assertEquals(organization.getOrganizationCategories(), categories);
    }

    @Test
    public void search() {

        clearServices();

        Set<Long> ids = new HashSet<Long>();

        Organization org = new Organization();
        org.setOrganizationAbbreviation("Abbr1");
        org.setOrganizationCategories(new HashSet<String>(Arrays.asList(new String[] { new String("CJ"), new String("NONCJ"), new String("CORP") })));
        org.setOrganizationLocalId("LocalId1");
        org.setOrganizationName("Name1");
        org.setOrganizationORIIdentification(generateORI());
        org.setOrganizationParent(null);

        org.setFacilities((new HashSet<Long>(Arrays.asList(new Long[] { createFacility(null), createFacility(null), createFacility(null) }))));

        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        c.add(Calendar.YEAR, 1);
        Date nextYear = c.getTime();

        c.add(Calendar.YEAR, -2);

        org.setOrganizationTerminationDate(nextYear);

        Organization ret = service.create(uc, org);
        assertNotNull(ret);

        ids.add(ret.getOrganizationIdentification());
        Long parent = ret.getOrganizationParent();

        org = new Organization();
        org.setOrganizationAbbreviation("Abbr2");
        org.setOrganizationCategories(new HashSet<String>(Arrays.asList(new String[] { new String("CJ"), new String("NONCJ"), new String("CORP"), new String("GOVT") })));
        org.setOrganizationLocalId("LocalId2");
        org.setOrganizationName("Name2");
        org.setOrganizationORIIdentification(generateORI());
        org.setOrganizationParent(parent);
        org.setFacilities((new HashSet<Long>(Arrays.asList(new Long[] { createFacility(null), createFacility(null), createFacility(null) }))));
        org.setOrganizationParent(ret.getOrganizationIdentification());
        org.setOrganizationTerminationDate(nextYear);

        ret = service.create(uc, org);
        assertNotNull(ret);

        ids.add(ret.getOrganizationIdentification());
        parent = ret.getOrganizationParent();

        org = new Organization();
        org.setOrganizationAbbreviation("Abbr3");
        org.setOrganizationCategories(new HashSet<String>(Arrays.asList(new String[] { new String("CJ"), new String("NONCJ"), new String("CORP"), new String("GOVT") })));
        org.setOrganizationLocalId("LocalId3");
        org.setOrganizationName("Name3");
        org.setOrganizationORIIdentification(generateORI());
        org.setOrganizationParent(parent);

        org.setOrganizationParent(ret.getOrganizationIdentification());
        org.setOrganizationTerminationDate(nextYear);

        ret = service.create(uc, org);
        assertNotNull(ret);
        ids.add(ret.getOrganizationIdentification());
        OrganizationsReturn rets = null;

        try {
            rets = service.search(uc, null, null, null, null, null);
            assertNotEquals(ret, null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        assertNull(rets);

        OrganizationSearch search = new OrganizationSearch();

        try {
            rets = service.search(uc, null, search, 0L, 30L, null);
            assertNotEquals(ret, null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        assertNull(rets);

        search = new OrganizationSearch();
        try {
            rets = service.search(uc, ids, null, 0L, 30L, null);
            assertNotEquals(ret, null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        ;
        assertNull(rets);

        search = new OrganizationSearch();
        try {
            rets = service.search(uc, ids, search, 0L, 30L, null);
            assertNotEquals(ret, null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }
        assertNull(rets);

        //Search by name
        search = new OrganizationSearch();
        search.setOrganizationName("Name1");
        rets = service.search(uc, ids, search, 0L, 30L, null);

        log.info(String.format("org search rets  0 %s", rets.getOrganizations().size()));
        log.info(String.format("org search rets  1 %s", rets.getTotalSize()));

        assertNotNull(rets);
        assertNotNull(rets.getTotalSize());
        assertEquals(rets.getOrganizations().size(), 1);

        // Search wildcard
        search = new OrganizationSearch();
        search.setOrganizationName("Name*");
        rets = service.search(uc, ids, search, 0L, 30L, null);

        log.info(String.format("org search rets  2 %s", rets.getOrganizations().size()));
        log.info(String.format("org search rets  3 %s", rets.getTotalSize()));

        assertNotNull(rets);
        assertEquals(rets.getOrganizations().size(), 3);

        //search category
        search = new OrganizationSearch();
        search.setOrganizationCategory("CJ");
        rets = service.search(uc, ids, search, null, null, null);
        assertNotNull(rets);

    }

    @Test
    public void getNIEM() {

        clearServices();

        Set<Long> ids = new HashSet<Long>();

        Organization org = new Organization();
        org.setOrganizationAbbreviation("Abbr1");
        org.setOrganizationCategories(new HashSet<String>(Arrays.asList(new String[] { new String("CJ"), new String("NONCJ"), new String("CORP") })));
        org.setOrganizationLocalId("LocalId1");
        org.setOrganizationName("Name1");
        org.setOrganizationORIIdentification(generateORI());
        org.setOrganizationParent(null);

        org.setFacilities((new HashSet<Long>(Arrays.asList(new Long[] { createFacility(null), createFacility(null), createFacility(null) }))));

        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        c.add(Calendar.YEAR, 1);
        Date nextYear = c.getTime();

        c.add(Calendar.YEAR, -2);

        org.setOrganizationTerminationDate(nextYear);

        Organization ret = service.create(uc, org);
        assertNotNull(ret);

        ids.add(ret.getOrganizationIdentification());
        Long parent = ret.getOrganizationParent();

        org = new Organization();
        org.setOrganizationAbbreviation("Abbr2");
        org.setOrganizationCategories(new HashSet<String>(Arrays.asList(new String[] { new String("CJ"), new String("NONCJ"), new String("CORP"), new String("GOVT") })));
        org.setOrganizationLocalId("LocalId2");
        org.setOrganizationName("Name2");
        org.setOrganizationORIIdentification("ORI234567");
        org.setOrganizationORIIdentification(generateORI());
        org.setOrganizationParent(parent);
        org.setFacilities((new HashSet<Long>(Arrays.asList(new Long[] { createFacility(null), createFacility(null), createFacility(null) }))));
        org.setOrganizationParent(ret.getOrganizationIdentification());
        org.setOrganizationTerminationDate(nextYear);

        ret = service.create(uc, org);
        assertNotEquals(ret, null);

        ids.add(ret.getOrganizationIdentification());
        parent = ret.getOrganizationParent();

        org = new Organization();
        org.setOrganizationAbbreviation("Abbr3");
        org.setOrganizationCategories(new HashSet<String>(Arrays.asList(new String[] { new String("CJ"), new String("NONCJ"), new String("CORP"), new String("GOVT") })));
        org.setOrganizationLocalId("LocalId3");
        org.setOrganizationName("Name3");
        org.setOrganizationORIIdentification(generateORI());
        org.setOrganizationParent(parent);

        org.setFacilities((new HashSet<Long>(Arrays.asList(new Long[] { createFacility(null), createFacility(null), createFacility(null) }))));

        org.setOrganizationParent(ret.getOrganizationIdentification());
        org.setOrganizationTerminationDate(nextYear);

        ret = service.create(uc, org);
        assertNotNull(ret);
        ids.add(ret.getOrganizationIdentification());

        try {
            for (Long id : ids) {
                String niem = service.getNiem(uc, id);
                assert (niem != null);
                log.info(niem);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    @Test
    public void getFacilities() {

        clearServices();

        Organization org = new Organization();
        org.setOrganizationAbbreviation("Abbr1");
        org.setOrganizationCategories(new HashSet<String>(Arrays.asList(new String[] { new String("CJ"), new String("NONCJ"), new String("CORP") })));
        org.setOrganizationLocalId("LocalId1");
        org.setOrganizationName("Name1");
        org.setOrganizationORIIdentification(generateORI());
        org.setOrganizationParent(null);

        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        c.add(Calendar.YEAR, 1);
        Date nextYear = c.getTime();

        c.add(Calendar.YEAR, -2);

        org.setOrganizationTerminationDate(nextYear);

        log.info(String.format("org getFacilites  0 %s", org));

        Organization ret = service.create(uc, org);
        assertNotNull(ret);

        log.info(String.format("org getFacilites  1 %s", ret));

        Long orgId = ret.getOrganizationIdentification();
        HashSet<Long> aFacilities = (new HashSet<Long>(Arrays.asList(new Long[] { createFacility(orgId), createFacility(orgId), createFacility(orgId) })));
        ret.setFacilities(aFacilities);

        log.info(String.format("org getFacilites  1.5 %s", ret));

        ret = service.update(uc, ret);

        log.info(String.format("org getFacilites  2 %s", ret));

        Organization retOrg = service.get(uc, ret.getOrganizationIdentification());
        assertEquals(retOrg.getFacilities(), aFacilities);

    }

    @Test
    public void updateFacilites() {

        clearServices();

        Organization org = new Organization();
        org.setOrganizationAbbreviation("Abbr1");
        org.setOrganizationCategories(new HashSet<String>(Arrays.asList(new String[] { new String("CJ"), new String("NONCJ"), new String("CORP") })));
        org.setOrganizationLocalId("LocalId1");
        org.setOrganizationName("Name1");
        org.setOrganizationORIIdentification(generateORI());
        org.setOrganizationParent(null);
        org.setFacilities((new HashSet<Long>(Arrays.asList(new Long[] { createFacility(null), createFacility(null), createFacility(null) }))));

        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        c.add(Calendar.YEAR, 1);
        Date nextYear = c.getTime();

        c.add(Calendar.YEAR, -2);

        org.setOrganizationTerminationDate(nextYear);

        Organization ret = service.create(uc, org);
        assertNotNull(ret);

        Organization retOrg = ret;

        retOrg.getFacilities().removeAll(retOrg.getFacilities());

        Long orgId = retOrg.getOrganizationIdentification();
        HashSet<Long> aFacilities = (new HashSet<Long>(Arrays.asList(new Long[] { createFacility(orgId), createFacility(orgId), createFacility(orgId) })));
        retOrg.setFacilities(aFacilities);

        Long newOrgId = createOrganization();

        log.info(String.format("org updateFacilites  1 %s", retOrg.getFacilities()));

        Organization retOrgU = service.update(uc, retOrg);
        assertNotNull(retOrgU);

        log.info(String.format("org updateFacilites  2 %s ", retOrgU.getFacilities()));
        assertEquals(retOrgU.getFacilities(), retOrg.getFacilities());

        // Try removign facilities
        retOrgU.getFacilities().removeAll(retOrgU.getFacilities());

        Organization retOrgUU = service.update(uc, retOrgU);
        assertEquals(retOrgUU.getFacilities().size(), 0);

        log.info(String.format("org updateFacilites  3 %s %s ", retOrgUU.getFacilities(), retOrg.getFacilities()));

        // update facility with differnt org id
        orgId = retOrgUU.getOrganizationIdentification();
        HashSet<Long> bFacilities = (new HashSet<Long>(Arrays.asList(new Long[] { createFacility(newOrgId), createFacility(newOrgId), createFacility(newOrgId) })));
        retOrgUU.setFacilities(bFacilities);

        Organization retOrgUUU = service.update(uc, retOrgUU);

        log.info(String.format("org updateFacilites  4 %s %s ", retOrgUUU.getFacilities(), retOrgUU.getFacilities()));
        assertEquals(retOrgUUU.getFacilities(), bFacilities);

    }

    private String generateORI() {
        Random rand = new Random();
        String ori = "ORI" + rand.nextLong();
        return ori.substring(0, 9);
    }

    private Long createFacility(Long orgId) {

        Set<Long> orgs = new HashSet<Long>();
        if (!BeanHelper.isEmpty(orgId)) {
			orgs.add(orgId);
		}

        Facility ret = null;
        String category = new String("COMMU");
        Long rand = new Random().nextLong();
        Facility facility = new Facility(-1L, "FacilityCodeIT" + rand, "FacilityName" + rand, new Date(), category, null, null, orgs, null, false);
        //log.info(String.format("org createFacility  11 %s",facility));
        ret = facService.create(uc, facility);
        //log.info(String.format("org createFacility  12 %s",ret));
        assertNotEquals(ret, null);
        return ret.getFacilityIdentification();
    }

    private Long createOrganization() {
        Organization org = new Organization();
        org.setOrganizationAbbreviation("Abbr1");
        org.setOrganizationCategories(new HashSet<String>(Arrays.asList(new String[] { new String("CJ"), new String("NONCJ"), new String("CORP") })));
        org.setOrganizationLocalId("LocalId1");
        org.setOrganizationName("Name1");
        org.setOrganizationORIIdentification(generateORI());
        org.setOrganizationParent(null);
        org.setFacilities((new HashSet<Long>(Arrays.asList(new Long[] { createFacility(null), createFacility(null), createFacility(null) }))));

        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        c.add(Calendar.YEAR, 1);
        Date nextYear = c.getTime();

        c.add(Calendar.YEAR, -2);

        org.setOrganizationTerminationDate(nextYear);

        Organization ret = service.create(uc, org);
        assertNotNull(ret);

        return ret.getOrganizationIdentification();
    }
}