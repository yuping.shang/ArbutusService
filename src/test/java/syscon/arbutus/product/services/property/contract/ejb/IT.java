/**
 * © 2013 by Syscon Justice Systems. All rights reserved.
 * Copying and distribution, any modifications or adaptation of the contents, or creation of derivative
 * works are not allowed without the express and prior written consent of Syscon Justice Systems.
 */
package syscon.arbutus.product.services.property.contract.ejb;

import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.naming.NamingException;

import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.eventlog.contract.dto.TransactLogEntityTypes;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.FacilityInternalLocation;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.LocationAttributeType;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.UsageCategory;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.person.contract.dto.StaffPosition;
import syscon.arbutus.product.services.person.contract.dto.StaffType;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.person.contract.test.ContractTestUtil;
import syscon.arbutus.product.services.property.contract.dto.ContainerType;
import syscon.arbutus.product.services.property.contract.dto.DisposePropertyItemType;
import syscon.arbutus.product.services.property.contract.dto.PropertyItemImageType;
import syscon.arbutus.product.services.property.contract.dto.PropertyItemSearchType;
import syscon.arbutus.product.services.property.contract.dto.PropertyItemType;
import syscon.arbutus.product.services.property.contract.dto.PropertyStatusEnum;
import syscon.arbutus.product.services.property.contract.dto.RecievePropertyContainerType;
import syscon.arbutus.product.services.property.contract.dto.ReleasePropertyItemType;
import syscon.arbutus.product.services.property.contract.dto.ReleaseToPropertyEnum;
import syscon.arbutus.product.services.property.contract.dto.TransactLogType;
import syscon.arbutus.product.services.property.contract.dto.TransferPropertyContainerType;
import syscon.arbutus.product.services.property.contract.interfaces.PropertyService;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

public class IT extends BaseIT {
    private static Logger log = LoggerFactory.getLogger(IT.class);
    private static FacilityInternalLocationService facInService;
    protected UserContext uc = null;
    // shared data
    Object tip;
    private PropertyService service;
    private FacilityService facService;
    private SupervisionService supService;
    private Long facilityId;
    private Long facilityInternalLocationId;
    private Long staffId;
    private Long supervisionId;
    private PersonService personService;
    private Long personId;
    private Long personIdentityId;

    @BeforeClass
    public void beforeTest() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");
        service = (PropertyService) JNDILookUp(this.getClass(), PropertyService.class);
        uc = super.initUserContext();
        facService = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        facInService = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        supService = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        personService = (PersonService) JNDILookUp(this.getClass(), PersonService.class);

        facilityId = createFacility();
        facilityInternalLocationId = createFacilityInternalLocation();
        personId = createPerson();
        personIdentityId = createPersonIdenity();
        staffId = createStaff();
        supervisionId = createSupervision();
        // service.reset(uc);
    }

    private void clientlogin(String user, String password) {
        try {
            SecurityClient client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password);
            client.login();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    @Test
    public void testCreatePropertyItem() {
        clientlogin("devtest", "devtest");
        PropertyItemType ret = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        ret = service.createPropertyItem(uc, propertyItemType);
        assert (ret != null);
    }

    @Test
    public void testEditPropertyItem() {
        clientlogin("devtest", "devtest");
        PropertyItemType ret = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        ret = service.createPropertyItem(uc, propertyItemType);
        assert (ret != null);
        ret.setBarcode("This is an edited Text....");
        PropertyItemType retEdit = service.updatePropertyItem(uc, ret);
        assert (retEdit != null);

    }

    @Test
    public void testDeletePropertyItem() {
        clientlogin("devtest", "devtest");
        PropertyItemType ret = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        ret = service.createPropertyItem(uc, propertyItemType);
        Long retCode = service.deletePropertyItem(uc, ret.getPropertyId());

        assert (retCode > 0);

    }

    @Test
    public void testCreateContainer() {
        clientlogin("devtest", "devtest");
        ContainerType ret = new ContainerType();
        ContainerType containerType = getContainerTypeInstance();
        ret = service.createContainer(uc, containerType);
        assert (ret != null);
    }

    @Test
    public void testEditContainer() {
        clientlogin("devtest", "devtest");
        ContainerType ret = new ContainerType();
        ContainerType containerType = getContainerTypeInstance();
        ret = service.createContainer(uc, containerType);
        assert (ret != null);
        ret.setSeal("New Seal");
        ContainerType retEdit = service.updateContainer(uc, ret);
        assert (retEdit != null);

    }

    @Test
    public void testDeleteContainer() {
        clientlogin("devtest", "devtest");
        ContainerType ret = new ContainerType();
        ContainerType containerType = getContainerTypeInstance();
        ret = service.createContainer(uc, containerType);
        Long retCode = service.deleteContainer(uc, ret.getContainerId());

        assert (retCode > 0);
    }

	/*@Test
    public void testGetPropertyItemsFromContainer() {
		clientlogin("devtest", "devtest");
		ContainerType ret = new ContainerType();
		ContainerType containerType = getContainerTypeInstance();
		ret = service.createContainer(uc, containerType);
		assert (ret != null);
		ret.setType("This is an edited Text....");
		ContainerType retEdit = service.updateContainer(uc, ret);
		assert (retEdit != null);
	}*/

    @Test
    public void testAddPropertyItemsToContainer() {
        clientlogin("devtest", "devtest");
        PropertyItemType pItem = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        pItem = service.createPropertyItem(uc, propertyItemType);
        assert (pItem != null);
        ContainerType cItem = new ContainerType();
        ContainerType containerType = getContainerTypeInstance();
        cItem = service.createContainer(uc, containerType);
        assert (cItem != null);
        List<Long> list = new ArrayList<Long>();
        Long containerId = cItem.getContainerId();
        list.add(pItem.getPropertyId());
        Long id = service.addItemsToContainer(uc, list, containerId, staffId);
        assert (id != null);
    }

    @Test
    public void testRemovePropertyItemsFromContainer() {
        clientlogin("devtest", "devtest");
        PropertyItemType pItem = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        pItem = service.createPropertyItem(uc, propertyItemType);
        assert (pItem != null);
        ContainerType cItem = new ContainerType();
        ContainerType containerType = getContainerTypeInstance();
        cItem = service.createContainer(uc, containerType);
        assert (cItem != null);
        List<Long> list = new ArrayList<Long>();
        Long containerId = cItem.getContainerId();
        list.add(pItem.getPropertyId());

        assert (service.addItemsToContainer(uc, list, containerId, staffId) == 1L);

        assert (service.removeItemsFromContainer(uc, list, containerId, staffId) == 1L);
    }

    @Test
    public void testSearchPropertyItem() {
        clientlogin("devtest", "devtest");
        PropertyItemType pItem = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        pItem = service.createPropertyItem(uc, propertyItemType);

        assert (pItem != null);
        assert (pItem.getPropertyId() > 0);
        List<PropertyItemType> list = new ArrayList<PropertyItemType>();

        PropertyItemSearchType search = new PropertyItemSearchType();
        search.setSupervisionId(pItem.getSupervision());
        list = service.searchProperty(uc, search, null, null, null);
        assert (list != null);
    }

    @Test
    public void testSearchContainerizedPropertyItem() {

        clientlogin("devtest", "devtest");
        PropertyItemType pItem = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        pItem = service.createPropertyItem(uc, propertyItemType);
        assert (pItem != null);
        ContainerType cItem = new ContainerType();
        ContainerType containerType = getContainerTypeInstance();
        cItem = service.createContainer(uc, containerType);
        assert (cItem != null);
        List<Long> list = new ArrayList<Long>();
        Long containerId = cItem.getContainerId();
        list.add(pItem.getPropertyId());
        Long id = service.addItemsToContainer(uc, list, containerId, staffId);
        assert (id != null);

        List<PropertyItemType> listPItem = new ArrayList<PropertyItemType>();
        PropertyItemSearchType search = new PropertyItemSearchType();
        search.setSupervisionId(pItem.getSupervision());
        search.setContainerized(true);

        listPItem = service.getContainerizedItemsForSupervision(uc, pItem.getSupervision());
        assert (listPItem != null);
    }


	/*@Test
    public void testSearchNonEmptyContainersForSupervision() {

		clientlogin("devtest", "devtest");
		PropertyItemType pItem = new PropertyItemType();
		PropertyItemType propertyItemType = getPropertyItemTypeInstance();
		pItem = service.createPropertyItem(uc, propertyItemType);
		assert (pItem != null);
		ContainerType cItem = new ContainerType();
		ContainerType containerType = getContainerTypeInstance();
		cItem = service.createContainer(uc, containerType);
		assert (cItem != null);
		List<Long> list = new ArrayList<Long>();
		Long containerId = cItem.getContainerId();
		list.add(pItem.getPropertyId());
		Long id = service.addItemsToContainer(uc, list, containerId);
		assert (id != null);

		List<ContainerType> listCItem = new ArrayList<ContainerType>();
		PropertyItemSearchType search = new PropertyItemSearchType();
		assert (pItem != null);
		search.setSupervisionId(pItem.getSupervision());
		search.setContainerized(true);

		listCItem = service.getNonEmptyContainersForSupervision(uc, pItem.getSupervision());
		assert (listCItem != null);
	}*/

    @Test
    public void testGetAllContainers() {

        clientlogin("devtest", "devtest");
        ContainerType ret = new ContainerType();
        ContainerType containerType = getContainerTypeInstance();
        ret = service.createContainer(uc, containerType);
        assert (ret != null);
        List<ContainerType> listCItem = new ArrayList<ContainerType>();
        listCItem = service.getAllContainers(uc);
        assert (ret != null);
        assert (listCItem != null);

    }

    @Test
    public void testReleasePropertyList() {

        clientlogin("devtest", "devtest");
        PropertyItemType pItem = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        pItem = service.createPropertyItem(uc, propertyItemType);
        assert (pItem != null);

        ContainerType cItem = new ContainerType();
        ContainerType containerType = getContainerTypeInstance();
        cItem = service.createContainer(uc, containerType);
        assert (cItem != null);

        List<Long> list = new ArrayList<Long>();
        Long containerId = cItem.getContainerId();
        list.add(pItem.getPropertyId());
        Long id = service.addItemsToContainer(uc, list, containerId, staffId);
        assert (id != null);

        Set<Long> hashset = new HashSet<Long>();
        hashset.add(pItem.getPropertyId());
        ReleasePropertyItemType releasePropertyItemType = new ReleasePropertyItemType();
        releasePropertyItemType.setItemIdSet(hashset);
        releasePropertyItemType.setIsContainerSet(false);
        releasePropertyItemType.setReleaseToPropertyType(ReleaseToPropertyEnum.PERSON);
        releasePropertyItemType.setStaff(staffId);
        releasePropertyItemType.setReleaseToTargetId(1L);
        service.releasePropertyList(uc, releasePropertyItemType);

        PropertyItemType pItem1 = new PropertyItemType();
        PropertyItemType propertyItemType1 = getPropertyItemTypeInstance();
        pItem1 = service.createPropertyItem(uc, propertyItemType1);
        assert (pItem1 != null);

        PropertyItemType pItem2 = new PropertyItemType();
        PropertyItemType propertyItemType2 = getPropertyItemTypeInstance();
        pItem2 = service.createPropertyItem(uc, propertyItemType2);
        assert (pItem2 != null);

        ContainerType cItem1 = new ContainerType();
        ContainerType containerType1 = getContainerTypeInstance();
        cItem1 = service.createContainer(uc, containerType1);
        assert (cItem1 != null);

        ContainerType cItem2 = new ContainerType();
        ContainerType containerType2 = getContainerTypeInstance();
        cItem2 = service.createContainer(uc, containerType2);
        assert (cItem1 != null);

        List<Long> list1 = new ArrayList<Long>();
        Long containerId1 = cItem1.getContainerId();
        list1.add(pItem1.getPropertyId());
        Long id1 = service.addItemsToContainer(uc, list1, containerId1, staffId);
        assert (id1 != null);

        List<Long> list2 = new ArrayList<Long>();
        Long containerId2 = cItem2.getContainerId();
        list2.add(pItem2.getPropertyId());
        Long id2 = service.addItemsToContainer(uc, list2, containerId2, staffId);
        assert (id2 != null);

        Set<Long> containerIdSet = new HashSet<Long>();
        containerIdSet.add(cItem1.getContainerId());
        containerIdSet.add(cItem2.getContainerId());
        ReleasePropertyItemType releasePropertyItemType1 = new ReleasePropertyItemType();
        releasePropertyItemType1.setItemIdSet(hashset);
        releasePropertyItemType.setIsContainerSet(true);
        releasePropertyItemType1.setReleaseToPropertyType(ReleaseToPropertyEnum.PERSON);
        releasePropertyItemType1.setStaff(staffId);
        releasePropertyItemType1.setReleaseToTargetId(1L);
        service.releasePropertyList(uc, releasePropertyItemType1);
    }

    @Test
    public void testDisposePropertyList() {

        clientlogin("devtest", "devtest");
        PropertyItemType pItem = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        pItem = service.createPropertyItem(uc, propertyItemType);
        assert (pItem != null);

        ContainerType cItem = new ContainerType();
        ContainerType containerType = getContainerTypeInstance();
        cItem = service.createContainer(uc, containerType);
        assert (cItem != null);

        List<Long> list = new ArrayList<Long>();
        Long containerId = cItem.getContainerId();
        list.add(pItem.getPropertyId());
        Long id = service.addItemsToContainer(uc, list, containerId, staffId);
        assert (id != null);

        Set<Long> propertyIdSet = new HashSet<Long>();
        propertyIdSet.add(pItem.getPropertyId());
        DisposePropertyItemType disposePropertyItemType = new DisposePropertyItemType();
        disposePropertyItemType.setItemIdSet(propertyIdSet);
        disposePropertyItemType.setIsContainerSet(false);
        disposePropertyItemType.setComments("comments");
        disposePropertyItemType.setStaff(staffId);
        disposePropertyItemType.setReason("DESTROYED");
        service.disposePropertyList(uc, disposePropertyItemType);

        PropertyItemType pItem1 = new PropertyItemType();
        PropertyItemType propertyItemType1 = getPropertyItemTypeInstance();
        pItem1 = service.createPropertyItem(uc, propertyItemType1);
        assert (pItem1 != null);

        PropertyItemType pItem2 = new PropertyItemType();
        PropertyItemType propertyItemType2 = getPropertyItemTypeInstance();
        pItem2 = service.createPropertyItem(uc, propertyItemType2);
        assert (pItem2 != null);

        ContainerType cItem1 = new ContainerType();
        ContainerType containerType1 = getContainerTypeInstance();
        cItem1 = service.createContainer(uc, containerType1);
        assert (cItem1 != null);

        ContainerType cItem2 = new ContainerType();
        ContainerType containerType2 = getContainerTypeInstance();
        cItem2 = service.createContainer(uc, containerType2);
        assert (cItem1 != null);

        List<Long> list1 = new ArrayList<Long>();
        Long containerId1 = cItem1.getContainerId();
        list1.add(pItem1.getPropertyId());
        Long id1 = service.addItemsToContainer(uc, list1, containerId1, staffId);
        assert (id1 != null);

        List<Long> list2 = new ArrayList<Long>();
        Long containerId2 = cItem2.getContainerId();
        list2.add(pItem2.getPropertyId());
        Long id2 = service.addItemsToContainer(uc, list2, containerId2, staffId);
        assert (id2 != null);

        Set<Long> containerIdSet = new HashSet<Long>();
        containerIdSet.add(cItem1.getContainerId());
        containerIdSet.add(cItem2.getContainerId());
        DisposePropertyItemType disposePropertyItemType1 = new DisposePropertyItemType();
        disposePropertyItemType1.setItemIdSet(containerIdSet);
        disposePropertyItemType1.setIsContainerSet(true);
        disposePropertyItemType1.setComments("comments");
        disposePropertyItemType1.setStaff(staffId);
        disposePropertyItemType1.setReason("DESTROYED");
        service.disposePropertyList(uc, disposePropertyItemType);

    }

    @Test
    public void testTransferPropertyItems() {
        clientlogin("devtest", "devtest");
        PropertyItemType pItem = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        pItem = service.createPropertyItem(uc, propertyItemType);

        assert (pItem != null);
        assert (pItem.getPropertyId() > 0);

        PropertyItemType pItem1 = new PropertyItemType();
        propertyItemType = getPropertyItemTypeInstance();
        pItem1 = service.createPropertyItem(uc, propertyItemType);
        assert (pItem1 != null);
        ContainerType cItem = new ContainerType();
        ContainerType containerType = getContainerTypeInstance();
        cItem = service.createContainer(uc, containerType);
        assert (cItem != null);
        List<Long> list = new ArrayList<Long>();
        Long containerId = cItem.getContainerId();
        list.add(pItem1.getPropertyId());
        Long id = service.addItemsToContainer(uc, list, containerId, staffId);
        assert (id != null);

        List<TransferPropertyContainerType> items = new ArrayList<TransferPropertyContainerType>();
        TransferPropertyContainerType cTransfer = new TransferPropertyContainerType();
        cTransfer.setArrivalDateTime(createFutureDate(1));
        cTransfer.setTransferDateTime(createPastDate(1));
        cTransfer.setComments("ABC-Container");
        cTransfer.setFromFacility(cItem.getFacility());
        cTransfer.setFromLocation(cItem.getFacilityInternalLocation());
        cTransfer.setIsContainer(Boolean.TRUE);
        cTransfer.setItemId(cItem.getContainerId());
        cTransfer.setStaff(staffId);
        cTransfer.setToFacility(facilityId);

        items.add(cTransfer);

        TransferPropertyContainerType pTransfer = new TransferPropertyContainerType();
        pTransfer.setArrivalDateTime(createFutureDate(1));
        pTransfer.setTransferDateTime(createPastDate(1));
        pTransfer.setComments("ABC-Property");
        pTransfer.setFromFacility(pItem.getFacility());
        pTransfer.setFromLocation(pItem.getFacilityInternalLocation());
        pTransfer.setIsContainer(Boolean.FALSE);
        pTransfer.setItemId(pItem.getPropertyId());
        pTransfer.setStaff(staffId);
        pTransfer.setToFacility(facilityId);

        items.add(pTransfer);

        service.transferPropertyItemsToFacility(uc, items);

    }

    @Test
    public void testRecievePropertyItems() {
        clientlogin("devtest", "devtest");
        PropertyItemType pItem = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        pItem = service.createPropertyItem(uc, propertyItemType);

        assert (pItem != null);
        assert (pItem.getPropertyId() > 0);

        PropertyItemType pItem1 = new PropertyItemType();
        propertyItemType = getPropertyItemTypeInstance();
        pItem1 = service.createPropertyItem(uc, propertyItemType);
        assert (pItem1 != null);
        ContainerType cItem = new ContainerType();
        ContainerType containerType = getContainerTypeInstance();
        cItem = service.createContainer(uc, containerType);
        assert (cItem != null);
        List<Long> list = new ArrayList<Long>();
        Long containerId = cItem.getContainerId();
        list.add(pItem1.getPropertyId());
        Long id = service.addItemsToContainer(uc, list, containerId, staffId);
        assert (id != null);

        List<TransferPropertyContainerType> items = new ArrayList<TransferPropertyContainerType>();
        TransferPropertyContainerType cTransfer = new TransferPropertyContainerType();
        cTransfer.setArrivalDateTime(createFutureDate(1));
        cTransfer.setTransferDateTime(createPastDate(1));
        cTransfer.setComments("ABC-Container");
        cTransfer.setFromFacility(cItem.getFacility());
        cTransfer.setFromLocation(cItem.getFacilityInternalLocation());
        cTransfer.setIsContainer(Boolean.TRUE);
        cTransfer.setItemId(cItem.getContainerId());
        cTransfer.setStaff(staffId);
        cTransfer.setToFacility(facilityId);

        items.add(cTransfer);

        TransferPropertyContainerType pTransfer = new TransferPropertyContainerType();
        pTransfer.setArrivalDateTime(createFutureDate(1));
        pTransfer.setTransferDateTime(createPastDate(1));
        pTransfer.setComments("ABC-Property");
        pTransfer.setFromFacility(pItem.getFacility());
        pTransfer.setFromLocation(pItem.getFacilityInternalLocation());
        pTransfer.setIsContainer(Boolean.FALSE);
        pTransfer.setItemId(pItem.getPropertyId());
        pTransfer.setStaff(staffId);
        pTransfer.setToFacility(facilityId);

        items.add(pTransfer);

        service.transferPropertyItemsToFacility(uc, items);

        List<TransferPropertyContainerType> transferCList = service.getTransferredContainersForFacilityAndSupervision(uc, facilityId, supervisionId);
        List<TransferPropertyContainerType> transferPList = service.getTransferredProItemsForFacilityAndSupervision(uc, facilityId, supervisionId);
        List<RecievePropertyContainerType> recieveItems = new ArrayList<RecievePropertyContainerType>();
        for (TransferPropertyContainerType t : transferCList) {
            RecievePropertyContainerType r = new RecievePropertyContainerType();
            r.setComments("recieve Container");
            r.setcType(t.getcType());
            r.setFromFacility(t.getFromFacility());
            r.setFromLocation(t.getFromLocation());
            r.setIsContainer(t.getIsContainer());
            r.setItemId(t.getItemId());
            r.setpType(t.getpType());
            r.setRecieveDateTime(t.getArrivalDateTime());
            r.setStaff(t.getStaff());
            r.setToFacility(t.getToFacility());
            recieveItems.add(r);
        }

        for (TransferPropertyContainerType t : transferPList) {
            RecievePropertyContainerType r = new RecievePropertyContainerType();
            r.setComments("Recieve Property");
            r.setcType(t.getcType());
            r.setFromFacility(t.getFromFacility());
            r.setFromLocation(t.getFromLocation());
            r.setIsContainer(t.getIsContainer());
            r.setItemId(t.getItemId());
            r.setpType(t.getpType());
            r.setRecieveDateTime(t.getArrivalDateTime());
            r.setStaff(t.getStaff());
            r.setToFacility(t.getToFacility());
            recieveItems.add(r);
        }

        //Test for supervision details.
        //I am commenting it out the following check due to transaction issue at MA search method.
        //assert(!service.getSupervisionListHavingProperties(uc, facilityId, PropertyStatusEnum.TRANSFER).isEmpty());

        service.recieveTransferredPropertyItems(uc, recieveItems);

    }

	/*@Test
    public void testGetActiveContainersBySupervisionId() {

		clientlogin("devtest", "devtest");
		ContainerType ret = new ContainerType();
		ContainerType containerType = getContainerTypeInstance();
		ret = service.createContainer(uc, containerType);
		assert (ret != null);
		List<ContainerType> listCItem = new ArrayList<ContainerType>();
		listCItem = service.getActiveContainersBySupervisionId(uc, supervisionId);
		assert (ret != null);
		assert (listCItem != null);

	}*/

    @Test
    public void testAddPhoto() {
        clientlogin("devtest", "devtest");
        PropertyItemType ret = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        ret = service.createPropertyItem(uc, propertyItemType);
        assert (ret != null);

        // Set/get photo
        byte[] img1 = new byte[] { 1, 1, 1, 1 };

        PropertyItemImageType propertyItemImageType = new PropertyItemImageType();
        propertyItemImageType.setPropertyItemId(ret.getPropertyId());
        propertyItemImageType.setDefaultImage(false);
        propertyItemImageType.setStaff(staffId);
        propertyItemImageType.setPropertyItemImageNumber("1234");
        propertyItemImageType.setPropertyItemImageDescription("propertyItemImageDescription");
        propertyItemImageType.setUploadedDate(new Date());
        propertyItemImageType.setData(img1);
        propertyItemImageType = service.addPhoto(uc, propertyItemImageType);
        assert (propertyItemImageType != null);
    }

    @Test
    public void testUpdatePhoto() {
        clientlogin("devtest", "devtest");
        PropertyItemType ret = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        ret = service.createPropertyItem(uc, propertyItemType);
        assert (ret != null);

        // Set/get photo
        byte[] img1 = new byte[] { 1, 1, 1, 1 };
        byte[] img2 = new byte[] { 1, 1, 1, 1 };

        PropertyItemImageType propertyItemImageType = new PropertyItemImageType();
        propertyItemImageType.setPropertyItemId(ret.getPropertyId());
        propertyItemImageType.setDefaultImage(true);
        propertyItemImageType.setStaff(staffId);
        propertyItemImageType.setPropertyItemImageNumber("1234");
        propertyItemImageType.setPropertyItemImageDescription("propertyItemImageDescription");
        propertyItemImageType.setUploadedDate(new Date());
        propertyItemImageType.setData(img1);
        propertyItemImageType = service.addPhoto(uc, propertyItemImageType);
        assert (propertyItemImageType != null);

        propertyItemImageType.setData(img2);
        PropertyItemImageType retEdit = service.updatePhoto(uc, propertyItemImageType);
        assert (retEdit != null);
    }

    @Test
    public void testDeletePhoto() {
        clientlogin("devtest", "devtest");
        PropertyItemType ret = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        ret = service.createPropertyItem(uc, propertyItemType);
        assert (ret != null);

        // Set/get photo
        byte[] img1 = new byte[] { 1, 1, 1, 1 };

        PropertyItemImageType propertyItemImageType = new PropertyItemImageType();
        propertyItemImageType.setPropertyItemId(ret.getPropertyId());
        propertyItemImageType.setDefaultImage(true);
        propertyItemImageType.setStaff(staffId);
        propertyItemImageType.setPropertyItemImageNumber("1234");
        propertyItemImageType.setPropertyItemImageDescription("propertyItemImageDescription");
        propertyItemImageType.setUploadedDate(new Date());
        propertyItemImageType.setData(img1);
        propertyItemImageType = service.addPhoto(uc, propertyItemImageType);
        assert (propertyItemImageType != null);

        Long retCode = service.deletePhoto(uc, propertyItemImageType.getPropertyItemImageId());
        assert (retCode > 0);
    }

    @Test
    public void testGetPhotos() {
        clientlogin("devtest", "devtest");
        PropertyItemType pItem = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        pItem = service.createPropertyItem(uc, propertyItemType);

        assert (pItem != null);
        assert (pItem.getPropertyId() > 0);

        // Set/get photo
        byte[] img1 = new byte[] { 1, 1, 1, 1 };

        PropertyItemImageType propertyItemImageType = new PropertyItemImageType();
        propertyItemImageType.setPropertyItemId(pItem.getPropertyId());
        propertyItemImageType.setDefaultImage(true);
        propertyItemImageType.setStaff(staffId);
        propertyItemImageType.setPropertyItemImageNumber("1234");
        propertyItemImageType.setPropertyItemImageDescription("propertyItemImageDescription");
        propertyItemImageType.setUploadedDate(new Date());
        propertyItemImageType.setData(img1);
        propertyItemImageType = service.addPhoto(uc, propertyItemImageType);
        propertyItemImageType = service.addPhoto(uc, propertyItemImageType);
        propertyItemImageType = service.addPhoto(uc, propertyItemImageType);
        propertyItemImageType = service.addPhoto(uc, propertyItemImageType);

        List<PropertyItemImageType> list = new ArrayList<PropertyItemImageType>();

        list = service.getPhotos(uc, pItem.getPropertyId());
        assert (list != null);
    }

    @Test
    public void testGetPhoto() {
        clientlogin("devtest", "devtest");
        PropertyItemType pItem = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        pItem = service.createPropertyItem(uc, propertyItemType);

        assert (pItem != null);
        assert (pItem.getPropertyId() > 0);

        // Set/get photo
        byte[] img1 = new byte[] { 1, 1, 1, 1 };

        PropertyItemImageType propertyItemImageType = new PropertyItemImageType();
        propertyItemImageType.setPropertyItemId(pItem.getPropertyId());
        propertyItemImageType.setDefaultImage(true);
        propertyItemImageType.setStaff(staffId);
        propertyItemImageType.setPropertyItemImageNumber("1234");
        propertyItemImageType.setPropertyItemImageDescription("propertyItemImageDescription");
        propertyItemImageType.setUploadedDate(new Date());
        propertyItemImageType.setData(img1);
        propertyItemImageType = service.addPhoto(uc, propertyItemImageType);

        PropertyItemImageType photo = service.getPhoto(uc, propertyItemImageType.getPropertyItemImageId());
        assert (photo != null);
    }

    @Test
    public void testGetTransactLogs() {
        clientlogin("devtest", "devtest");
        PropertyItemType ret = new PropertyItemType();
        PropertyItemType propertyItemType = getPropertyItemTypeInstance();
        ret = service.createPropertyItem(uc, propertyItemType);
        assert (ret != null);
        List<TransactLogType> propertyTransactlogs = service.getTransactLogs(uc, ret.getPropertyId(), TransactLogEntityTypes.Property);
        assert (propertyTransactlogs != null);
        assert (!propertyTransactlogs.isEmpty());

        clientlogin("devtest", "devtest");
        ContainerType container = new ContainerType();
        ContainerType containerType = getContainerTypeInstance();
        container = service.createContainer(uc, containerType);
        assert (ret != null);
        List<TransactLogType> containerTransactlogs = service.getTransactLogs(uc, container.getContainerId(), TransactLogEntityTypes.Container);
        assert (containerTransactlogs != null);
        assert (!containerTransactlogs.isEmpty());
    }

    @Test
    public void testVerifyContainer() {
        clientlogin("devtest", "devtest");
        ContainerType ret = new ContainerType();
        ContainerType containerType = getContainerTypeInstance();
        ret = service.createContainer(uc, containerType);
        assert (ret != null);

        ret.setVerificationStatus("Verified");
        ret.setSeal("VerifiedSeal1234");
        ContainerType retVerify = service.verifyContainer(uc, ret, staffId);
        assert (retVerify != null);
    }

    @Test
    public void testDoesLocationContainActiveItems() {
        clientlogin("devtest", "devtest");
        System.out.println(service.doesLocationContainActiveItems(facilityInternalLocationId));
    }

    @Test
    public void testDoesLocationContainActiveItemsNew() {
        clientlogin("devtest", "devtest");
        System.out.println(service.doesLocationContainActiveItems(uc, facilityInternalLocationId));
    }

    @Test
    public void testDoesTheLocationContainActiveItemsNew() {
        clientlogin("devtest", "devtest");
        System.out.println(service.doesTheLocationContainActiveItems(uc, facilityInternalLocationId));
    }

    private PropertyItemType getPropertyItemTypeInstance() {
        PropertyItemType propertyItemType = new PropertyItemType();
        Date eventDate = new Date();
        Date eventTime = new Date();
        eventTime.setHours(1);
        eventTime.setMinutes(1);
        eventTime.setSeconds(1);
        propertyItemType.setType("CLOTHING");
        propertyItemType.setSubType("SHIRT");
        propertyItemType.setDescription("Description");
        propertyItemType.setQuantity(1F);
        propertyItemType.setCondition("NEW");
        propertyItemType.setActivatedDate(eventDate);
        propertyItemType.setDeactivatedDate(eventDate);
        propertyItemType.setAllowedInCell("Yes");
        propertyItemType.setBarcode("Barcode-" + new Random().nextInt());
        propertyItemType.setModel("model");
        propertyItemType.setColor("BLACK");
        propertyItemType.setMake("make");
        propertyItemType.setMonetaryUnit("USD");
        propertyItemType.setMonetraryValue("1");
        propertyItemType.setStatus(PropertyStatusEnum.REGISTERED.getLabel());
        propertyItemType.setSubStatus("RELINMATE");
        propertyItemType.setSupervision(supervisionId);
        propertyItemType.setStaff(staffId);
        propertyItemType.setFacility(facilityId);
        propertyItemType.setFacilityInternalLocation(facilityInternalLocationId);

        return propertyItemType;
    }

    private ContainerType getContainerTypeInstance() {
        ContainerType containerType = new ContainerType();
        Set<String> tmpSet = new HashSet<String>();
        tmpSet.add("Amiya");
        Date eventDate = new Date();
        Date eventTime = new Date();
        eventTime.setHours(1);
        eventTime.setMinutes(1);
        eventTime.setSeconds(1);
        containerType.setType("Type");
        containerType.setSeal("seal-" + new Random().nextInt());
        containerType.setStaff(staffId);
        containerType.setVerification("verification");
        containerType.setVerificationStatus("verificationStatus");
        containerType.setActivatedDate(eventDate);
        containerType.setDeactivatedDate(eventDate);
        containerType.setFacility(facilityId);
        containerType.setFacilityInternalLocation(facilityInternalLocationId);
        containerType.setSupervision(supervisionId);
        return containerType;
    }

    private Date createFutureDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, day);
        Date futureDate = cal.getTime();
        return futureDate;
    }

    private Date createPastDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, -day);
        Date pastDate = cal.getTime();
        return pastDate;
    }

    private Long createFacility() {
        Facility ret = null;
        String category = new String("CUSTODY");

        Facility facility = new Facility(-1L, "FacilityCodeIT" + new Random().nextLong(), "FacilityName", new Date(), category, null, null, null, null, false);
        ret = facService.create(uc, facility);
        assertNotEquals(ret, null);
        return ret.getFacilityIdentification();
    }

    private Long createFacilityInternalLocation() {
        String phyCode = "PC01";
        String description = "physical location root";
        String level = "FACILITY";
        FacilityInternalLocation pl = createFacilityInternalLocation(level, phyCode, description);
        pl.getComments().add(createComment("root comment"));

        LocationAttributeType prop = new LocationAttributeType();
        prop.getOffenderCategories().add("SUIC");
        pl.setLocationAttribute(prop);
        FacilityInternalLocation ret = facInService.create(uc, pl, UsageCategory.HOU);
        assertNotNull(ret);
        log.info(String.format("ret = %s", ret));
        return ret.getId();
    }

    private FacilityInternalLocation createFacilityInternalLocation(String level, String code, String description) {
        FacilityInternalLocation pt = new FacilityInternalLocation();
        pt.setFacilityId(facilityId);
        pt.setHierarchyLevel(level);
        pt.setLocationCode(code);
        pt.setDescription(description);

        pt.getNonAssociations().add("TOTAL");
        pt.getNonAssociations().add("STG");

        return pt;
    }

    private CommentType createComment(String text) {
        CommentType ct = new CommentType();
        ct.setComment(text);
        ct.setCommentDate(new Date());
        return ct;
    }

    private Long createStaff() {
        StaffType ret = null;
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CPT"));
        staffPositions.add(new StaffPosition("DEP"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 30);
        String category = new String("CON");
        String reason = new String("SICK");

        StaffType staff = new StaffType(null, personIdentityId, category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(personId);
        //
        ret = personService.getStaff(uc, personService.createStaff(uc, staff));
        assert (ret != null);
        log.info(String.format("ret = %s", ret));
        return ret.getStaffID();

    }

    // Helper Method
    private Long createPersonIdenity() {

        Date dob = new Date(1985 - 1900, 3, 10);
        PersonIdentityType peter = new PersonIdentityType(null, personId, null, null, "peter", "S", "S", "Pan", "i", dob, "M", null, null, "offenderNumTest", null, null);

        // PersonIdentityType peter = createIndentity("Peter", "Pan", new
        // Date(84, 3, 10), "M");
        peter = personService.createPersonIdentityType(uc, peter, Boolean.FALSE, Boolean.FALSE);
        return peter.getPersonIdentityId();
    }

    private Long createPerson() {

        syscon.arbutus.product.services.person.contract.dto.PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        instanceForTest.setActiveStatus("ACTIVE");    //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        instanceForTest = personService.createPersonType(uc, instanceForTest);
        return instanceForTest.getPersonIdentification();
    }

    private Long createSupervision() {

        // Create Person Identity

        Long supervisionId = createSupervision(personIdentityId, facilityId, true);
        return supervisionId;

    }

    private Long createSupervision(Long personIdentityId, Long facilityId, boolean isActive) {

        SupervisionType supervision = new SupervisionType();

        Date startDate = new Date();
        Date endDate = null;

        if (!isActive) {
            endDate = new Date();
        }

        supervision.setSupervisionDisplayID("ID" + personIdentityId);
        supervision.setSupervisionStartDate(startDate);
        supervision.setSupervisionEndDate(endDate);
        supervision.setPersonIdentityId(personIdentityId);
        supervision.setFacilityId(facilityId);

        SupervisionType ret = supService.create(uc, supervision);
        assert (ret != null);

        return ret.getSupervisionIdentification();
    }

    @Test
    public void testCreateDefaultContainers() {
        clientlogin("devtest", "devtest");
        ContainerType containerType = getContainerTypeInstance();
        ContainerType containerType1 = getContainerTypeInstance();
        List<ContainerType> containerTypes = new ArrayList<ContainerType>();
        containerTypes.add(containerType);
        containerTypes.add(containerType1);
        containerTypes = service.createDefaultContainers(uc, containerType.getStaff(), containerType.getSupervision(), containerType.getFacility());
        assert (containerTypes != null);
    }
}
