/**
 * © 2013 by Syscon Justice Systems. All rights reserved.
 * Copying and distribution, any modifications or adaptation of the contents, or creation of derivative
 * works are not allowed without the express and prior written consent of Syscon Justice Systems.
 */
package syscon.arbutus.product.services.documentmanagement.contract.ejb;

import javax.naming.NamingException;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.documentmanagement.contract.dto.DocumentEntityType;
import syscon.arbutus.product.services.documentmanagement.contract.dto.DocumentType;
import syscon.arbutus.product.services.documentmanagement.contract.interfaces.DocumentManagementService;
import syscon.arbutus.product.services.person.contract.dto.StaffPosition;
import syscon.arbutus.product.services.person.contract.dto.StaffType;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.person.contract.test.ContractTestUtil;

import static org.testng.Assert.assertNotNull;

public class IT extends BaseIT {
    private static Logger log = LoggerFactory.getLogger(IT.class);
    protected UserContext uc = null;
    // shared data
    Object tip;
    private DocumentManagementService service;
    private PersonService personService;
    private Long staffId;
    private Long supervisionId;
    private Long personId;
    private Long personIdentityId;

    @BeforeClass
    public void beforeClass() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");
        service = (DocumentManagementService) JNDILookUp(this.getClass(), DocumentManagementService.class);
        uc = super.initUserContext();

        personService = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        personId = createPerson();
        personIdentityId = createPersonalIdenity();
        staffId = createStaff();

    }

    private void clientlogin(String user, String password) {
        try {
            SecurityClient client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password);
            client.login();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    @AfterClass
    public void afterClass() {
    }

    @Test
    public void createDocument() {
        DocumentType document = getDocumentObject();
        DocumentType newDoc = service.createDocument(uc, document);
        assertNotNull(newDoc);
        assertNotNull(newDoc.getDocumentId());
    }

    private DocumentType getDocumentObject() {
        DocumentType documentType = new DocumentType();
        byte[] file = new byte[] { 1, 1, 1, 1 };
        documentType.setData(file);
        documentType.setDefaultDocument(false);
        documentType.setDocumentDescription("Description");
        //documentType.setDocumentId(documentEntity.getDocumentId());
        documentType.setDocumentSize(1024L);
        documentType.setDocumentTitle("Title");
        documentType.setDocumentType("PDF");
        documentType.setEntityId(1L);
        documentType.setEntityType(DocumentEntityType.ShiftLog);
        documentType.setUploadedByStaff(staffId);
        documentType.setUploadedDate(new Date());
        return documentType;
    }

    private Long createStaff() {
        StaffType ret = null;
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CPT"));
        staffPositions.add(new StaffPosition("DEP"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 30);
        String category = new String("CON");
        String reason = new String("SICK");

        StaffType staff = new StaffType(null, personIdentityId, category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(personId);
        //
        ret = personService.getStaff(uc, personService.createStaff(uc, staff));
        assert (ret != null);
        log.info(String.format("ret = %s", ret));
        return ret.getStaffID();

    }

    // Helper Method
    private Long createPersonalIdenity() {
        Long lng = new Random().nextLong();

        Date dob = new Date(1985 - 1900, 3, 10);
        PersonIdentityType peter = new PersonIdentityType(null, personId, null, null, "peter" + lng, "S" + lng, "S" + lng, "Pan" + lng, "i", dob, "M", null, null,
                "offenderNumTest" + lng, null, null);

        // PersonIdentityType peter = createIndentity("Peter", "Pan", new
        // Date(84, 3, 10), "M");
        peter = personService.createPersonIdentityType(uc, peter, Boolean.FALSE, Boolean.FALSE);
        return peter.getPersonIdentityId();
    }

    private Long createPerson() {

        syscon.arbutus.product.services.person.contract.dto.PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        instanceForTest.setActiveStatus("ACTIVE"); // new
        // CodeType(ReferenceSet.PERSONSTATUS.name(),
        instanceForTest = personService.createPersonType(uc, instanceForTest);
        return instanceForTest.getPersonIdentification();
    }

}
