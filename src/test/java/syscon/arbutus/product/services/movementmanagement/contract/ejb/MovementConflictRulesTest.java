package syscon.arbutus.product.services.movementmanagement.contract.ejb;

import java.util.*;

import junit.framework.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.contract.dto.ActivityCategory;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.common.*;
import syscon.arbutus.product.services.core.common.adapters.ActivityServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.facility.contract.dto.FacilitiesReturn;
import syscon.arbutus.product.services.facility.contract.dto.FacilitySearch;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.DefaultLocationCategory;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.DefaultLocationType;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.location.contract.dto.Location;
import syscon.arbutus.product.services.movement.contract.dto.*;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType.MovementCategory;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType.MovementDirection;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType.MovementStatus;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType.MovementType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.utility.ServiceAdapter;
import syscon.arbutus.product.services.visitation.contract.dto.*;
import syscon.arbutus.product.services.visitation.contract.ejb.VisitTimeslotIT;
import syscon.arbutus.product.services.visitation.contract.interfaces.VisitationService;

import static org.testng.Assert.assertNotNull;

/**
 * Movement Conflicts Test case
 *
 * @author wmadruga
 * @since 2014-01-15
 */
public class MovementConflictRulesTest extends BaseIT {

    private static final String DCOJL_CODE = "DCOJL";
    private static Long DCOJL = 1L;
    protected MovementService mmService;
    protected VisitationService visService;
    protected FacilityService facService;
    private Long location;
    private Long fromFacility;
    private Long toFacility;
    private Long supervision;
    private Long org;
    private Long offenderPersonIdentity;
    private Long visitorPersonIdentity;
    private CourtMovementType courtFrom06To08;
    private CourtMovementType courtFrom0630To0730;
    private CourtMovementType courtFrom15To16;
    private TemporaryAbsenceType tapFrom0801To0859;
    private VisitType visFrom09To11;
    private VisitType visFrom12To13;
    private MovementService maService;
    private FacilityInternalLocationService facilityInternalLocationService;

    @BeforeClass
    public void beforeClass() {

        uc = super.initUserContext();
        mmService = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
        visService = (VisitationService) JNDILookUp(this.getClass(), VisitationService.class);
        maService = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
        facService = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        facilityInternalLocationService = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);

        FacilityTest facilityTest = new FacilityTest();
        fromFacility = DCOJL; //facilityTest.createFacility();
        toFacility = facilityTest.createFacility();
        DCOJL = getDOJLFacility();

        LocationTest locationTest = new LocationTest();
        Location loc = locationTest.createLocation(false, null, fromFacility);
        location = loc.getLocationIdentification();

        PersonIdentityTest piTest = new PersonIdentityTest();
        offenderPersonIdentity = piTest.createPersonIdentity();
        visitorPersonIdentity = piTest.createPersonIdentity();

        SupervisionTest supervisionTest = new SupervisionTest();
        supervision = supervisionTest.createSupervision(offenderPersonIdentity, fromFacility, true);

        OrganizationTest orgTest = new OrganizationTest();
        org = orgTest.createOrganization();

    }

    @Test
    public void testJNDILookup() {
        assertNotNull(mmService);
        assertNotNull(maService);
        assertNotNull(visService);
        assertNotNull(facService);
        assertNotNull(uc);

    }

    @Test
    public void prepare() {
        buildApprovedVisitors();
        maService.deleteAll(uc);
        visService.deleteAllVisits(uc);
        createExternalMovementAdmission(supervision, fromFacility, fromFacility);

    }

    /**
     * Should create a visit successfully for TODAY from 09am to 11am
     */
    @Test(enabled = true, dependsOnMethods = { "testJNDILookup", "prepare" })
    public void createVisitFrom09To11Success() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 9, 0, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 11, 0, 0);

        VisitType visit = buildVisit(moveDate, returnDate);

        List<ConflictType> visitConflicts = visService.checkVisitConflict(uc, visit);
        Assert.assertTrue(visitConflicts.isEmpty());

        VisitType ret = visService.createVisit(uc, visit);
        Assert.assertNotNull(ret);
        Assert.assertNotNull(ret.getVisitId());
        visFrom09To11 = ret;

    }

    /**
     * Should try to create a court movement for TODAY from 09:30am to 10am
     * and return a visit for TODAY from 09am to 11am as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createVisitFrom09To11Success" })
    public void createCourtFrom0930To10Conflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 9, 30, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 10, 0, 0);

        CourtMovementType court = buildCourtMovement(moveDate, returnDate);

        checkSingleConflictBetween(court, visFrom09To11, true);

    }

    /**
     * Should try to create a temporary absence for TODAY from 10:00am to 11:30am
     * and return a visit for TODAY from 09am to 11am as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createCourtFrom0930To10Conflict" })
    public void createTAPFrom10To1130Conflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 10, 0, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 11, 30, 0);

        TemporaryAbsenceType tap = builTemporaryAbsence(moveDate, returnDate);

        checkSingleConflictBetween(tap, visFrom09To11, true);

    }

    /**
     * Should create a visit for TODAY from 08am to 09:10am
     * and return a visit for TODAY from 09am to 11am as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createTAPFrom10To1130Conflict" })
    public void createVisitFrom08To0910Conflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 0, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 9, 10, 0);

        VisitType visit = buildVisit(moveDate, returnDate);

        List<ConflictType> visitConflicts = visService.checkVisitConflict(uc, visit);
        for (ConflictType visitConflict : visitConflicts) {
            Assert.assertTrue(visitConflict.getCheckType().contentEquals("MOVEMENT"));
            Assert.assertTrue(visitConflict.getDescription().contains("Movement Type: Visitation"));
            Assert.assertTrue(visitConflict.getDescription().contains("09:00"));
            Assert.assertTrue(visitConflict.getDescription().contains("11:00"));
        }

    }

    /**
     * Should create a court movement successfully for TODAY from 06am to 08am
     */
    @Test(enabled = true, dependsOnMethods = { "createVisitFrom08To0910Conflict" })
    public void createCourtFrom06To08Success() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 6, 0, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 0, 0);

        CourtMovementType court = buildCourtMovement(moveDate, returnDate);

        List<CourtMovementType> movements = new ArrayList<CourtMovementType>();
        movements.add(court);
        List<CourtMovementType> ret = mmService.createTwoMovements(uc, movements, false);

        Assert.assertFalse(ret.isEmpty());
        for (CourtMovementType created : ret) {
            Assert.assertTrue(created.getGroupId() != null);
            courtFrom06To08 = created;
        }

    }

    /**
     * Should try to create a court movement for TODAY from 06:30am to 08:30am
     * and return another court movement for TODAY from 06am to 08am as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createCourtFrom06To08Success" })
    public void createCourtFrom0630To0830Conflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 6, 30, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 30, 0);

        CourtMovementType court = buildCourtMovement(moveDate, returnDate);

        checkSingleConflictBetween(court, courtFrom06To08, true);

    }

    /**
     * Should try to create a court movement for TODAY from 05:59am to 08:00am
     * and return another court movement for TODAY from 06am to 08am as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createCourtFrom0630To0830Conflict" })
    public void createCourtFrom0559To08Conflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 5, 59, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 0, 0);

        CourtMovementType court = buildCourtMovement(moveDate, returnDate);

        checkSingleConflictBetween(court, courtFrom06To08, true);

    }

    /**
     * Should try to create a court movement for TODAY from 06:01am to 08:00am
     * and return another court movement for TODAY from 06am to 08am as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createCourtFrom0559To08Conflict" })
    public void createCourtFrom0601To08Conflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 6, 01, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 0, 0);

        CourtMovementType court = buildCourtMovement(moveDate, returnDate);

        checkSingleConflictBetween(court, courtFrom06To08, true);

    }

    /**
     * Should try to create a temporary absence for TODAY from 06:00am to 08:00am
     * and return a court movement for TODAY from 06am to 08am as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createCourtFrom0601To08Conflict" })
    public void createTAPFrom06To08Conflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 6, 0, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 0, 0);

        TemporaryAbsenceType tap = builTemporaryAbsence(moveDate, returnDate);

        checkSingleConflictBetween(tap, courtFrom06To08, true);

    }

    /**
     * Should create a temporary absence for TODAY from 08:01am to 08:59am successfully
     */
    @Test(enabled = true, dependsOnMethods = { "createTAPFrom06To08Conflict" })
    public void createTAPFrom0801To0859Success() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 01, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 59, 0);

        TemporaryAbsenceType tap = builTemporaryAbsence(moveDate, returnDate);

        List<TemporaryAbsenceType> movements = new ArrayList<TemporaryAbsenceType>();
        movements.add(tap);
        checkSingleConflictBetween(tap, courtFrom06To08, false);

        List<TemporaryAbsenceType> ret = mmService.createTwoMovements(uc, movements, false);
        Assert.assertFalse(ret.isEmpty());
        for (TemporaryAbsenceType created : ret) {
            Assert.assertTrue(created.getGroupId() != null);
            tapFrom0801To0859 = created;
        }

    }

    /**
     * Should try to create a court movement for TODAY from 08:10am to 08:30am
     * and return a temporary absence for TODAY from 08:01am to 08:59am as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createTAPFrom0801To0859Success" })
    public void createCourtFrom0810To0830Conflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 10, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 30, 0);

        CourtMovementType court = buildCourtMovement(moveDate, returnDate);

        checkSingleConflictBetween(court, tapFrom0801To0859, true);

    }

    /**
     * Should try to create a court movement for TODAY from 10:00am to 12:00am
     * and return a visit for TODAY from 09am to 11am as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createCourtFrom0810To0830Conflict" })
    public void createCourtFrom10To12Conflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 10, 0, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 12, 0, 0);

        CourtMovementType court = buildCourtMovement(moveDate, returnDate);

        checkSingleConflictBetween(court, visFrom09To11, true);

    }

    /**
     * Should create a temporary absence for TODAY from 08:45am to 11:00am
     * and return a temporary absence for TODAY from 08:01am to 08:59am as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createCourtFrom10To12Conflict" })
    public void createTAPFrom0845To11Conflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 45, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 11, 0, 0);

        TemporaryAbsenceType tap = builTemporaryAbsence(moveDate, returnDate);

        checkSingleConflictBetween(tap, tapFrom0801To0859, true);

    }

    /**
     * Should create a temporary absence for TODAY from 10:00am to 10:20am
     * and return a temporary absence for TODAY from 08:01am to 08:59am as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createTAPFrom0845To11Conflict" })
    public void createTAPFrom10To1020Conflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 10, 0, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 10, 20, 0);

        TemporaryAbsenceType tap = builTemporaryAbsence(moveDate, returnDate);

        checkSingleConflictBetween(tap, visFrom09To11, true);

    }

    /**
     * Should create a visit for TODAY from 06am to 11am
     * and return these conflicts {
     * a visit for TODAY from 09am to 11am
     * a court movement for TODAY from 06am to 08am
     * a temporary absence for TODAY from 08:01am to 08:59am
     * }
     */
    @Test(enabled = true, dependsOnMethods = { "createTAPFrom10To1020Conflict" })
    public void createVisitFrom06To11Conflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 6, 0, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 11, 0, 0);

        VisitType visit = buildVisit(moveDate, returnDate);

        List<ConflictType> visitConflicts = visService.checkVisitConflict(uc, visit);
        Assert.assertTrue(visitConflicts.size() == 3);
        for (ConflictType visitConflict : visitConflicts) {
            Assert.assertTrue(visitConflict.getCheckType().contentEquals("MOVEMENT"));

            if (visitConflict.getDescription().contains("VISIT")) {
                Assert.assertTrue(visitConflict.getDescription().contains("Movement Type: Visitation"));
                Assert.assertTrue(visitConflict.getDescription().contains("09:00"));
                Assert.assertTrue(visitConflict.getDescription().contains("11:00"));
            } else if (visitConflict.getDescription().contains("CRT")) {
                Assert.assertTrue(visitConflict.getDescription().contains("Movement Type: Court Movement"));
                Assert.assertTrue(visitConflict.getDescription().contains("06:00"));
                Assert.assertTrue(visitConflict.getDescription().contains("08:00"));
            } else if (visitConflict.getDescription().contains("TAP")) {
                Assert.assertTrue(visitConflict.getDescription().contains("Movement Type: Temporary Absence"));
                Assert.assertTrue(visitConflict.getDescription().contains("08:01"));
                Assert.assertTrue(visitConflict.getDescription().contains("08:59"));
            }
        }

    }

    /**
     * Should try to create a court movement for TODAY from 08:00am to 09:00am
     * and return these conflicts {
     * a visit for TODAY from 09am to 11am
     * a court movement for TODAY from 06am to 08am
     * a temporary absence for TODAY from 08:01am to 08:59am
     * }
     */
    @Test(enabled = true, dependsOnMethods = { "createVisitFrom06To11Conflict" })
    public void createCourtFrom08To09Conflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 0, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 9, 0, 0);

        CourtMovementType court = buildCourtMovement(moveDate, returnDate);

        TwoMovementsType conflictCheckReturn = mmService.checkSchedulingConflict(uc, court);

        Assert.assertFalse(conflictCheckReturn.getConflicts().isEmpty());
        Assert.assertTrue(conflictCheckReturn.getConflicts().size() == 3);
        for (TwoMovementsType conflict : conflictCheckReturn.getConflicts()) {
            if (conflict.getMovementType().contentEquals("TAP")) {
                Assert.assertTrue(conflict.getGroupId() != tapFrom0801To0859.getGroupId());
            } else if (conflict.getMovementType().contentEquals("CRT")) {
                Assert.assertTrue(conflict.getGroupId() != courtFrom06To08.getGroupId());
            } else if (conflict.getMovementType().contentEquals("VISIT")) {
                Assert.assertTrue(conflict.getGroupId() != visFrom09To11.getVisitId().toString());
            }
        }

    }

    /**
     * Updating the court movement from (06am - 08am) to (07am - 08am)
     * Should not return itself as a conflict.
     * Then change back to original time. Should not return itself as conflict as well.
     */
    @Test(enabled = true, dependsOnMethods = { "createCourtFrom08To09Conflict" })
    public void updatedCourtMovementSuccess() {
        List<CourtMovementType> movements = new ArrayList<CourtMovementType>();

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 7, 0, 0);
        courtFrom06To08.setMoveDate(moveDate);

        movements.add(courtFrom06To08);
        List<CourtMovementType> ret = mmService.updateTwoMovements(uc, movements, false);
        movements.remove(0);
        Assert.assertFalse(ret.isEmpty());
        for (CourtMovementType updated : ret) {
            Assert.assertTrue(updated.getConflicts().isEmpty());
        }

        // change back to original start/end time
        moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 6, 0, 0);

        courtFrom06To08.setMoveDate(moveDate);
        movements.add(courtFrom06To08);
        ret = mmService.updateTwoMovements(uc, movements, false);
        movements.remove(0);
        Assert.assertFalse(ret.isEmpty());
        for (CourtMovementType updated : ret) {
            Assert.assertTrue(updated.getConflicts().isEmpty());
        }
    }

    /**
     * Actualize a court movement out 06am
     */
    @Test(enabled = true, dependsOnMethods = { "updatedCourtMovementSuccess" })
    public void actualizeMovementOUTSuccess() {
        List<CourtMovementType> movements = new ArrayList<CourtMovementType>();
        courtFrom06To08.setMovementOutStatus("COMPLETED");
        movements.add(courtFrom06To08);
        List<CourtMovementType> ret = mmService.updateTwoMovements(uc, movements, false);
        for (CourtMovementType updated : ret) {
            Assert.assertTrue(updated.getConflicts().isEmpty());
            Assert.assertTrue(updated.getMovementOutStatus().contentEquals("COMPLETED"));
        }
    }

    /**
     * Should try to create a court movement for TODAY from 06:30am to 08:30am
     * and return another court movement for TODAY from 06am to 08am as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "actualizeMovementOUTSuccess" })
    public void createCourtFrom0630To0830AfterActualizeOUTConflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 6, 30, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 30, 0);

        CourtMovementType court = buildCourtMovement(moveDate, returnDate);

        checkSingleConflictBetween(court, courtFrom06To08, true);

    }

    /**
     * Should try to create a court movement for TODAY from 05:59am to 08:00am
     * and return another court movement for TODAY from 06am to 08am as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createCourtFrom0630To0830AfterActualizeOUTConflict" })
    public void createCourtFrom0559To08AfterActualizeOUTConflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 5, 59, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 0, 0);

        CourtMovementType court = buildCourtMovement(moveDate, returnDate);

        checkSingleConflictBetween(court, courtFrom06To08, true);

    }

    /**
     * Should try to create a court movement for TODAY from 06:01am to 08:00am
     * and return another court movement for TODAY from 06am to 08am as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createCourtFrom0559To08AfterActualizeOUTConflict" })
    public void createCourtFrom0601To08AfterActualizeOUTConflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 6, 01, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 0, 0);

        CourtMovementType court = buildCourtMovement(moveDate, returnDate);

        checkSingleConflictBetween(court, courtFrom06To08, true);

    }

    /**
     * Should try to create a temporary absence for TODAY from 06:00am to 08:00am
     * and return a court movement for TODAY from 06am to 08am as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createCourtFrom0601To08AfterActualizeOUTConflict" })
    public void createTAPFrom06To08AfterActualizeOUTConflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 6, 0, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 0, 0);

        TemporaryAbsenceType tap = builTemporaryAbsence(moveDate, returnDate);

        checkSingleConflictBetween(tap, courtFrom06To08, true);

    }

    /**
     * Actualize a court movement in 08am
     */
    @Test(enabled = true, dependsOnMethods = { "createTAPFrom06To08AfterActualizeOUTConflict" })
    public void actualizeMovementINSuccess() {
        List<CourtMovementType> movements = new ArrayList<CourtMovementType>();
        courtFrom06To08.setMovementInStatus("COMPLETED");
        movements.add(courtFrom06To08);
        List<CourtMovementType> ret = mmService.updateTwoMovements(uc, movements, false);
        for (CourtMovementType updated : ret) {
            Assert.assertTrue(updated.getConflicts().isEmpty());
            Assert.assertTrue(updated.getMovementOutStatus().contentEquals("COMPLETED"));
        }
    }

    /**
     * Should create a court movement for TODAY from 06:30am to 07:30am successfully
     * because I have just actualized movements OUT and IN, so there are no conflict anymore.
     */
    @Test(enabled = true, dependsOnMethods = { "actualizeMovementINSuccess" })
    public void createCourtFrom0630To0730AfterActualizeINSuccess() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 6, 30, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 7, 30, 0);

        CourtMovementType court = buildCourtMovement(moveDate, returnDate);
        List<CourtMovementType> movements = new ArrayList<CourtMovementType>();
        movements.add(court);

        checkSingleConflictBetween(court, courtFrom06To08, false);
        List<CourtMovementType> ret = mmService.createTwoMovements(uc, movements, false);

        for (CourtMovementType created : ret) {
            Assert.assertTrue(created.getConflicts().isEmpty());
            Assert.assertTrue(created.getGroupId() != null);
            courtFrom0630To0730 = created;
        }

    }

    /**
     * Updates a court movement dates from 08:30am to 08:50am and should return
     * the Temporary Absence as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createCourtFrom0630To0730AfterActualizeINSuccess" })
    public void createCourtFrom0830To0850Conflict() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 30, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 8, 50, 0);

        List<CourtMovementType> movements = new ArrayList<CourtMovementType>();
        courtFrom0630To0730.setMoveDate(moveDate);
        courtFrom0630To0730.setReturnDate(returnDate);
        movements.add(courtFrom0630To0730);
        checkSingleConflictBetween(courtFrom0630To0730, tapFrom0801To0859, true);
    }

    /**
     * Should create a visit successfully for TODAY from 12:00 to 13:00
     */
    @Test(enabled = true, dependsOnMethods = { "createCourtFrom0830To0850Conflict" })
    public void createVisitFrom12To13Success() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 12, 0, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 13, 0, 0);

        VisitType visit = buildVisit(moveDate, returnDate);

        List<ConflictType> visitConflicts = visService.checkVisitConflict(uc, visit);
        Assert.assertTrue(visitConflicts.isEmpty());

        VisitType ret = visService.createVisit(uc, visit);
        Assert.assertNotNull(ret);
        Assert.assertNotNull(ret.getVisitId());
        visFrom12To13 = ret;

    }

    /**
     * Should create a court movement successfully for TODAY from 15:00 to 16:00
     */
    @Test(enabled = true, dependsOnMethods = { "createVisitFrom12To13Success" })
    public void createCourtFrom15To16Success() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 15, 0, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 16, 0, 0);

        CourtMovementType court = buildCourtMovement(moveDate, returnDate);

        List<CourtMovementType> movements = new ArrayList<CourtMovementType>();
        movements.add(court);
        List<CourtMovementType> ret = mmService.createTwoMovements(uc, movements, false);

        Assert.assertFalse(ret.isEmpty());
        for (CourtMovementType created : ret) {
            Assert.assertTrue(created.getGroupId() != null);
            courtFrom15To16 = created;
        }

    }

    /**
     * Updating the visit movement from (12:00 - 13:00) to (15:00 - 16:00)
     * Should return the court movement courtFrom15To16 as a conflict
     */
    @Test(enabled = true, dependsOnMethods = { "createCourtFrom15To16Success" })
    public void updateVisitationReturnConflicts() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 15, 0, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 16, 0, 0);
        visFrom12To13.setStartTime(moveDate);
        visFrom12To13.setEndTime(returnDate);

        List<ConflictType> visitConflicts = visService.checkVisitConflict(uc, visFrom12To13);
        Assert.assertTrue(visitConflicts.size() == 1);
        for (ConflictType visitConflict : visitConflicts) {
            Assert.assertTrue(visitConflict.getCheckType().contentEquals("MOVEMENT"));

            if (visitConflict.getDescription().contains("CRT")) {
                Assert.assertTrue(visitConflict.getDescription().contains("Movement Type: CRT"));
                Assert.assertTrue(visitConflict.getDescription().contains("03:00"));
                Assert.assertTrue(visitConflict.getDescription().contains("04:00"));
            }
        }
    }

    /**
     * Updating the visit movement from (12:00 - 13:00) to (12:30 - 13:30)
     * Should not return any conflicts
     */
    @Test(enabled = true, dependsOnMethods = { "updateVisitationReturnConflicts" })
    public void updateVisitationSuccess() {

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 12, 30, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 13, 30, 0);
        visFrom12To13.setStartTime(moveDate);
        visFrom12To13.setEndTime(returnDate);

        List<ConflictType> visitConflicts = visService.checkVisitConflict(uc, visFrom12To13);
        Assert.assertTrue(visitConflicts.isEmpty());

        VisitType ret = visService.updateVisit(uc, visFrom12To13);
        Assert.assertTrue(ret.getStartTime().compareTo(moveDate) == 0);
        Assert.assertTrue(ret.getEndTime().compareTo(returnDate) == 0);

    }

    /**
     * Create a court movement from (12:00) to (13:00) which will have conflict with a Visit from (12:30) to (13:30)
     * but I will acknowledge the conflict by sending bypassConflicts = true.
     * Now try to actualize the court movement out and should bring up the conflicts for acknowledge again.
     */
    @Test(enabled = true, dependsOnMethods = { "updateVisitationSuccess" })
    public void actualizeAcknowledgedMovementConflict() {
        CourtMovementType acknowledgedCreatedCourt = null;

        Date moveDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 12, 00, 0);
        Date returnDate = BeanHelper.createDateWithTime(BeanHelper.createDate(), 13, 00, 0);

        CourtMovementType court = buildCourtMovement(moveDate, returnDate);

        List<CourtMovementType> movements = new ArrayList<CourtMovementType>();
        movements.add(court);

        //has conflicts
        checkSingleConflictBetween(court, visFrom12To13, true);

        //let's acknowledge and try again
        movements.remove(0);
        court.setBypassConflicts(true);
        movements.add(court);

        // no conflicts now...
        checkSingleConflictBetween(court, visFrom12To13, false);

        // let's create conflict-acknowledged movement then...
        List<CourtMovementType> ret = mmService.createTwoMovements(uc, movements, false);

        Assert.assertFalse(ret.isEmpty());
        for (CourtMovementType created : ret) {
            Assert.assertTrue(created.getGroupId() != null);
            acknowledgedCreatedCourt = created;
        }
        Assert.assertNotNull(acknowledgedCreatedCourt);

        // let's actualize that movement and should bring conflicts again :)
        if (acknowledgedCreatedCourt != null) {
            acknowledgedCreatedCourt.setMovementOutStatus("COMPLETED");
            // check for conflicts and should have

            System.out.println("VALIDATE: ");
            System.out.println(acknowledgedCreatedCourt);
            System.out.println("AGAINST: ");
            System.out.println(visFrom12To13);

            checkSingleConflictBetween(acknowledgedCreatedCourt, visFrom12To13, true);
        }

    }

    // =========================================================================================================================================== \\

    /**
     * Verifies existence of conflict between two objects
     *
     * @param targetMovement,     TwoMovementsType
     * @param supposedToConflict, TwoMovementsType
     */
    private void checkSingleConflictBetween(TwoMovementsType targetMovement, TwoMovementsType supposedToConflict, Boolean conflictIsExpected) {
        TwoMovementsType conflictCheckReturn = mmService.checkSchedulingConflict(uc, targetMovement);

        // verifying conflicts list
        if (conflictIsExpected) {
            Assert.assertFalse(conflictCheckReturn.getConflicts().isEmpty());
        } else {
            Assert.assertTrue(conflictCheckReturn.getConflicts().isEmpty());
        }

        // verifying conflict content
        for (TwoMovementsType conflict : conflictCheckReturn.getConflicts()) {
            if (conflictIsExpected) {
                Assert.assertTrue(conflict.getGroupId() != supposedToConflict.getGroupId());
            } else {
                Assert.assertFalse(conflict.getGroupId() != supposedToConflict.getGroupId());
            }
        }
    }

    /**
     * Verifies existence of conflict between two objects
     *
     * @param targetMovement,     TwoMovementsType
     * @param supposedToConflict, TwoMovementsType
     */
    private void checkSingleConflictBetween(TwoMovementsType targetMovement, VisitType supposedToConflict, Boolean conflictIsExpected) {
        TwoMovementsType conflictCheckReturn = mmService.checkSchedulingConflict(uc, targetMovement);

        // verifying conflicts list
        if (conflictIsExpected) {
            Assert.assertFalse(conflictCheckReturn.getConflicts().isEmpty());
        } else {
            Assert.assertTrue(conflictCheckReturn.getConflicts().isEmpty());
        }

        // verifying conflict content
        for (TwoMovementsType conflict : conflictCheckReturn.getConflicts()) {
            if (conflictIsExpected) {
                Assert.assertTrue(conflict.getGroupId() != supposedToConflict.getVisitId().toString());
            } else {
                Assert.assertFalse(conflict.getGroupId() != supposedToConflict.getVisitId().toString());
            }
        }
    }

    /**
     * Builds a court movement object
     *
     * @param moveDate,   the movement date
     * @param returnDate, the returning date
     * @return, a new court movement object
     */
    private CourtMovementType buildCourtMovement(Date moveDate, Date returnDate) {
        CourtMovementType court = new CourtMovementType();

        court.setSupervisionId(supervision);
        court.setCourtPartRoom(location);
        court.setFromFacilityId(fromFacility);
        court.setToFacilityId(toFacility);

        court.setMoveDate(moveDate);
        court.setReturnDate(returnDate);

        court.setMovementReason("COURT");
        court.setMovementType("CRT");

        court.setComments("created by " + getMethodName()); // running method name as comments

		/* 	Unused attributes

	 		court.setBypassConflicts(false);
			court.setTarget(false);
			court.setOffenderId(null);
			court.setOffenderName(null);
			court.setConflicts(null);
			court.setGroupId(null);
			court.setMovementIn(null);
			court.setMovementInStatus(null);
			court.setMovementOut(null);
			court.setMovementOutCome(null);
			court.setMovementOutStatus(null); 
		*/

        return court;
    }

    /**
     * Builds a temporary absence object
     *
     * @param moveDate,   the movement date
     * @param returnDate, the returning date
     * @return, a new temporary absence object
     */
    private TemporaryAbsenceType builTemporaryAbsence(Date moveDate, Date returnDate) {
        TemporaryAbsenceType tap = new TemporaryAbsenceType();

        tap.setApplicationDate(BeanHelper.createDate());
        tap.setMoveDate(moveDate);
        tap.setReturnDate(returnDate);
        tap.setFromFacilityId(fromFacility);
        tap.setToFacilityId(toFacility);
        tap.setToFacilityLocationId(location);
        tap.setToOffenderLocationId(null);
        tap.setEscortOrganizationId(org);
        tap.setComments("created by " + getMethodName()); // running method name as comments
        tap.setSupervisionId(supervision);

        tap.setMovementReason("HOSP"); // MOVEMENTREASON = HOSP = HOSPITAL
        tap.setMovementType("TAP");
        tap.setTransportation("POL"); //TATRANSPORT = POL = POLICE

        tap.setMovementInStatus("PENDING");
        tap.setMovementOutStatus("PENDING");

		/* 	Unused attributes
            tap.setGroupId(null);
			tap.setMovementIn(null);
			tap.setMovementOut(null);
			tap.setMovementOutCome(null);
			tap.setOffenderId(null);
			tap.setOffenderName(null);
			tap.setTarget(false);
			tap.setApprovalComments(null);
			tap.setApprovalDate(null);
			tap.setApproverId(null);
			tap.setBypassConflicts(false);
			tap.setConflicts(null);
			tap.setEscortDetails(null);
		*/

        return tap;

    }

    /**
     * Builds a visit object
     *
     * @param startTime, the start time
     * @param endTime,   the end time
     * @return, a new visit object
     */
    private VisitType buildVisit(Date startTime, Date endTime) {
        VisitType visit = new VisitType();

        visit.setAttendingVisitors(new HashSet<AttendingVisitorType>());
        visit.setOffenderSupervisionId(supervision);

        visit.setVisitDate(startTime);
        visit.setStartTime(startTime);
        visit.setEndTime(endTime);
        visit.setFacilityId(fromFacility);
        visit.setLocationId(location);

        visit.setStatus("SCHEDULED");

        List<VisitTimeslotType> timeslots = getVisitTimeslot();

        visit.setTimeSlot(timeslots.get(0).getId().toString());
        visit.setVisitType("SOCIAL");
        visit.setComments("created by " + getMethodName()); // running method name as comments

		/*	Unused attributes
			visit.setOffenderAttended(null);
			visit.setCancelReason(null);
			visit.setConflicts(null);
			visit.setOutcome(null);
			visit.setVisitActivityId(null);
			visit.setVisitId(null);
		*/

        return visit;
    }

    private List<VisitTimeslotType> getVisitTimeslot() {

        VisitTimeslotIT timeslotIT = new VisitTimeslotIT();
        timeslotIT.beforeClass();
        timeslotIT.deactivateAll();
        timeslotIT.create();

        VisitTimeslotSearchType searchTimeslot = new VisitTimeslotSearchType();
        searchTimeslot.setFacilityId(fromFacility);
        List<VisitTimeslotType> timeslots = visService.search(uc, searchTimeslot);
        Assert.assertFalse(timeslots.isEmpty());
        return timeslots;
    }

    /**
     * Creates a sibiling for the current offender and enables him as a visitor.
     */
    private void buildApprovedVisitors() {
        ApprovedVisitorType approvedVisitor = new ApprovedVisitorType();

        approvedVisitor.setApprovedVisitorId(null);
        approvedVisitor.setPersonAssociationType("SIBLING");
        approvedVisitor.setPersonRole("FATHER");
        approvedVisitor.setPersonId(null);
        approvedVisitor.setOffenderSupervisionId(supervision);
        approvedVisitor.setVisitorPersonIdentityId(visitorPersonIdentity);
        approvedVisitor.setSupervisionId(supervision);
        approvedVisitor.setSupervisionFacilityId(fromFacility);

        ApprovedVisitorType retApprovedVisitor = visService.addToApprovedVisitors(uc, supervision, approvedVisitor);
        assertNotNull(retApprovedVisitor);
    }

    /**
     * Admits the inmate.
     */
    public void createExternalMovementAdmission(Long supId, Long fromLocationId, Long toLocationId) {
        // External Movement Activity

        // Set Movement Category
        MovementActivityType ma = new MovementActivityType();

        ma.setSupervisionId(supId);
        ma.setMovementCategory(MovementCategory.EXTERNAL.value());
        // Set Movement Type
        ma.setMovementType(MovementType.ADM.code()); // CRT -- Court
        // Set Movement Direction
        ma.setMovementDirection(MovementDirection.IN.code());
        // Set Movement Status
        ma.setMovementStatus(MovementStatus.COMPLETED.code());
        // Set Movement Reason
        ma.setMovementReason(null); // LA -- Legal Aid

        // Set Movement Date
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR, 12);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        Date presentTime = c.getTime();
        ma.setMovementDate(presentTime);

        // Set Comments
        Set<CommentType> comments = new LinkedHashSet<CommentType>();
        CommentType ct1 = new CommentType();
        ct1.setCommentDate(presentTime);
        ct1.setComment("Court appear.");
        comments.add(ct1);
        ma.setCommentText(comments);

        ActivityType activityType = new ActivityType();
        activityType.setSupervisionId(supId);
        activityType.setActiveStatusFlag(true);
        activityType.setPlannedStartDate(new Date());
        activityType.setPlannedEndDate(new Date());
        activityType.setActivityCategory(ActivityCategory.VISITATION.value());

        ActivityType ret = ActivityServiceAdapter.createActivity(uc, activityType);

        Long activityId = ret.getActivityIdentification();
        ma.setActivityId(activityId);

        ExternalMovementActivityType ema = new ExternalMovementActivityType();
        ema.setSupervisionId(supId);
        DefaultLocationType defaultLocation = facilityInternalLocationService.getDefaultLocation(uc, fromLocationId, DefaultLocationCategory.DEFAULT_HOUSING_LOCATION);
        ema.setToFacilityInternalLocationId(defaultLocation.getInternalLocationId());
        //ema.setToFacilityInternalLocationId(createFacilityInternalLocation(fromLocationId));
        ema.setMovementCategory(MovementCategory.EXTERNAL.value());
        // Set Movement Type
        ema.setMovementType(MovementType.ADM.code()); // CRT -- Court
        // Set Movement Direction
        ema.setMovementDirection(MovementDirection.IN.code());
        // Set Movement Status
        ema.setMovementStatus(MovementStatus.COMPLETED.code());
        // Set Movement Reason
        ema.setMovementReason("VIS"); // LA -- Legal Aid

        ema.setMovementDate(presentTime);

        // Set Comments
        ema.setCommentText(comments);

        ema.setActivityId(activityId);
        String toProvinceState = "BC";

        ema.setFromLocationId(fromLocationId);
        ema.setFromFacilityId(fromLocationId);
        ema.setFromOrganizationId(fromLocationId);
        ema.setToProvinceState(toProvinceState);
        ema.setToLocationId(toLocationId);
        ema.setToFacilityId(toLocationId);
        ema.setToOrganizationId(toLocationId);
        ema.setApplicationDate(presentTime);
        ema.setArrestOrganizationId(toLocationId);
        ema.setEscortOrganizationId(toLocationId);
        ema.setReportingDate(presentTime);

        ema.setFromCity("Vancouver");
        ema.setToCity("Richmond");

        ema.setMovementOutcome(null);

        ExternalMovementActivityType emaRet = (ExternalMovementActivityType) maService.create(uc, ema, false);
        assertNotNull(emaRet);

    }

    /**
     * Get the DOJL facility ID
     */
    private Long getDOJLFacility() {
        FacilitySearch search = new FacilitySearch();
        search.setFacilityCode(DCOJL_CODE);
        FacilitiesReturn facResult = facService.search(uc, null, search, null, null, null);
        if (facResult.getFacilities().size() > 0) {
            Facility facility = facResult.getFacilities().iterator().next();

            return facility.getFacilityIdentification();
        } else {
            return DCOJL;
        }
    }

    private String getMethodName() {
        StackTraceElement[] ste = Thread.currentThread().getStackTrace();
        return ste[ste.length - 1].getMethodName();
    }
}