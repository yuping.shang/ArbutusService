package syscon.arbutus.product.services.movementmanagement.contract.ejb;

import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.common.SupervisionTest;
import syscon.arbutus.product.services.movement.contract.dto.CourtMovementType;
import syscon.arbutus.product.services.movement.contract.dto.TwoMovementsSearchType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;
import syscon.arbutus.product.services.realization.util.BeanHelper;

import static org.testng.Assert.assertNotNull;

public class CourtMovementTest extends BaseIT {

    private static final Date MOVE_DATE = BeanHelper.createDateWithTime(BeanHelper.createDate(), 22, 0, 0);
    private static final Date RETURN_DATE = BeanHelper.createDateWithTime(BeanHelper.nextNthDays(MOVE_DATE, 5), 22, 0, 0);
    private static final String MOVE_TYPE_COURT = "CRT";
    //	private static final String MOVE_REASON_VISIT = "VIS";
    private static final String MOVE_REASON_COURT = "COURT";
    private static final String REQUEST_COMMENT = "Comments for this Court Movement request";
    private static Logger log = LoggerFactory.getLogger(CourtMovementTest.class);
    protected ActivityService serviceActivity;
    protected MovementService serviceMovement;
    protected MovementService service;
    private CourtMovementType crt1;
    private Long supervisionId;
    private Long facility1, facility2;

    @BeforeClass
    public void beforeClass() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");

        serviceActivity = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        serviceMovement = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
        service = (MovementService) JNDILookUp(this.getClass(), MovementService.class);

        uc = super.initUserContext();

        FacilityTest facilityTest = new FacilityTest();
        facility1 = facilityTest.createFacility();
        facility2 = facilityTest.createFacility();

        SupervisionTest supervisionTest = new SupervisionTest();
        supervisionId = supervisionTest.createSupervision(facility1, true);

        crt1 = buildCourtMovementObject(supervisionId, facility1, facility2);

    }

    /**
     * JNDI not null test
     */
    @Test
    public void testJNDILookup() {
        assertNotNull(serviceActivity);
        assertNotNull(serviceMovement);
        assertNotNull(uc);

    }

    /**
     * Test to get service version
     */

    @Test(dependsOnMethods = "testJNDILookup")
    public void testGetVersion() {
        String version = service.getVersion(uc);
        assertNotNull(version);
    }

    @Test(enabled = true)
    public void createTest() {
        shouldPassScheduleAdHocCourtMovement();
        shouldPassScheduleIndividualCourtMovementWithoutSomeFields();
    }

    @Test(enabled = true)
    public void updateTest() {
        //		shouldPassUpdatingAdHocCourtMovement(); //out dated - need to update
        shouldPassUpdatingIndividualCourtMovement();
        shouldActualizeMovement();
    }

    @Test(enabled = true, dependsOnMethods = "createTest")
    public void retrieveTest() {
        shouldReturnAllByFacilityId();
        shouldReturnAllByOffenderId();
        searchTest();
    }

    private void shouldActualizeMovement() {

    }

    private CourtMovementType buildCourtMovementObject(Long supId, Long fromFacilityId, Long toFacilityId) {
        CourtMovementType court = new CourtMovementType();
        court.setCourtPartRoom(164L); // Denver County Court
        court.setFromFacilityId(fromFacilityId);
        court.setToFacilityId(toFacilityId);
        court.setSupervisionId(supId);

        court.setMoveDate(MOVE_DATE);
        court.setReturnDate(RETURN_DATE);

        court.setMovementReason(MOVE_REASON_COURT); // Visit
        court.setMovementType(MOVE_TYPE_COURT); // Court Movement
        //		court.setMovementStatus(MOVE_STATUS_PENDING_MOVEMENT); // Pending movement.

        court.setComments(REQUEST_COMMENT);

        return court;
    }

    private void shouldReturnAllByFacilityId() {
        TwoMovementsSearchType crit = new TwoMovementsSearchType();
        crit.setMovementType(MOVE_TYPE_COURT);
        crit.setFromFacilityId(facility1);
        List<CourtMovementType> ret = service.listTwoMovementsBy(uc, crit);
        Assert.assertFalse(ret.isEmpty());
        // Getting one by one.
        for (CourtMovementType court : ret) {
            crit.setFromFacilityId(null);
            crit.setGroupId(court.getGroupId());
            List<CourtMovementType> rets = service.listTwoMovementsBy(uc, crit);
            assertNotNull(rets);
            CourtMovementType dbCourt = rets.get(0);
            assertNotNull(dbCourt);
        }

        crit = new TwoMovementsSearchType();
        crit.setMovementType(MOVE_TYPE_COURT);
        crit.setToFacilityId(facility2);
        ret = service.listTwoMovementsBy(uc, crit);
        Assert.assertFalse(ret.isEmpty());
        // Getting one by one.
        for (CourtMovementType court : ret) {
            crit.setFromFacilityId(null);
            crit.setGroupId(court.getGroupId());
            List<CourtMovementType> rets = service.listTwoMovementsBy(uc, crit);
            assertNotNull(rets);
            CourtMovementType dbCourt = rets.get(0);
            assertNotNull(dbCourt);
        }

    }

    private void shouldReturnAllByOffenderId() {
        TwoMovementsSearchType crit = new TwoMovementsSearchType();
        crit.setOffenderId(supervisionId);
        crit.setMovementType(MOVE_TYPE_COURT);
        List<CourtMovementType> ret = service.listTwoMovementsBy(uc, crit);
        //		Assert.assertFalse(ret.isEmpty());
        // Getting one by one.
        for (CourtMovementType court : ret) {
            crit.setOffenderId(null);
            crit.setGroupId(court.getGroupId());
            List<CourtMovementType> rets = service.listTwoMovementsBy(uc, crit);
            CourtMovementType dbCourt = rets.get(0);
            assertNotNull(dbCourt);
            Assert.assertTrue(court.getOffenderName() != null);
            Assert.assertFalse(court.getOffenderName().contentEquals(""));
        }
    }

    private void searchTest() {
        createSomeData();

        TwoMovementsSearchType crit = new TwoMovementsSearchType();
        crit.setOffenderId(supervisionId);
        crit.setMovementType(MOVE_TYPE_COURT);
        crit.setMoveDate(BeanHelper.nextNthDays(BeanHelper.createDate(), 2));
        crit.setReturnDate(BeanHelper.nextNthDays(BeanHelper.createDate(), 4));
        List<CourtMovementType> ret = service.listTwoMovementsBy(uc, crit);

        Assert.assertTrue(ret.size() == 2); //crt1 and crt2

        for (CourtMovementType court : ret) {
            Boolean moveDateTest1 = court.getMoveDate().after(BeanHelper.nextNthDays(BeanHelper.createDate(), 1));
            Boolean moveDateTest2 = court.getMoveDate().before(BeanHelper.nextNthDays(BeanHelper.createDate(), 5));

            Boolean returnDateTest1 = court.getReturnDate().after(BeanHelper.nextNthDays(BeanHelper.createDate(), 1));
            Boolean returnDateTest2 = court.getReturnDate().before(BeanHelper.nextNthDays(BeanHelper.createDate(), 5));

            Assert.assertTrue(moveDateTest1 || moveDateTest2);
            Assert.assertTrue(returnDateTest1 || returnDateTest2);
        }
    }

    private void createSomeData() {
        List<CourtMovementType> courts = new ArrayList<CourtMovementType>();

        CourtMovementType crt1 = buildCourtMovementObject(supervisionId, facility1, facility2);
        crt1.setMoveDate(BeanHelper.createDate());
        crt1.setReturnDate(BeanHelper.nextNthDays(BeanHelper.createDate(), 2));

        CourtMovementType crt2 = buildCourtMovementObject(supervisionId, facility1, facility2);
        crt2.setMoveDate(BeanHelper.nextNthDays(BeanHelper.createDate(), 3));
        crt2.setReturnDate(BeanHelper.nextNthDays(BeanHelper.createDate(), 4));

        CourtMovementType crt3 = buildCourtMovementObject(supervisionId, facility1, facility2);
        crt3.setMoveDate(BeanHelper.nextNthDays(BeanHelper.createDate(), 5));
        crt3.setReturnDate(BeanHelper.nextNthDays(BeanHelper.createDate(), 6));

        CourtMovementType crt4 = buildCourtMovementObject(supervisionId, facility1, facility2);
        crt4.setMoveDate(BeanHelper.nextNthDays(BeanHelper.createDate(), 7));
        crt4.setReturnDate(BeanHelper.nextNthDays(BeanHelper.createDate(), 8));

        courts.add(crt1);
        courts.add(crt2);
        courts.add(crt3);
        courts.add(crt4);

        List<CourtMovementType> rets = (List<CourtMovementType>) service.createTwoMovements(uc, courts, false);
        Assert.assertFalse(rets.isEmpty());

    }

    private void shouldPassUpdatingIndividualCourtMovement() {
        List<CourtMovementType> toUpdate = new ArrayList<CourtMovementType>();
        TwoMovementsSearchType crit = new TwoMovementsSearchType();
        crit.setOffenderId(supervisionId);
        crit.setMovementType(MOVE_TYPE_COURT);
        crit.setFromFacilityId(facility1);
        List<CourtMovementType> ret = service.listTwoMovementsBy(uc, crit);
        Assert.assertFalse(ret.isEmpty());
        if (!ret.isEmpty()) {
            CourtMovementType court = ret.get(0);
            if (court != null) {
                Date newMoveDate = BeanHelper.createDateWithTime(BeanHelper.nextNthDays(MOVE_DATE, 100), 23, 0, 0);
                Date newReturnDate = BeanHelper.createDateWithTime(BeanHelper.nextNthDays(MOVE_DATE, 100), 23, 30, 0);

                court.setCourtPartRoom(165L);
                court.setMovementReason(MOVE_REASON_COURT);
                Assert.assertTrue(court.getMoveDate() != null);
                Assert.assertTrue(court.getReturnDate() != null);
                court.setMoveDate(newMoveDate);
                court.setReturnDate(newReturnDate);
                toUpdate.add(court);

                List<CourtMovementType> updatedCourts = service.updateTwoMovements(uc, toUpdate, false);
                Assert.assertFalse(updatedCourts.isEmpty());
                CourtMovementType updatedCourt = updatedCourts.get(0);
                Assert.assertTrue(updatedCourt.getConflicts().isEmpty());
                Assert.assertTrue(updatedCourt.getCourtPartRoom() == 165L);
                Assert.assertTrue(updatedCourt.getMovementReason().contentEquals(MOVE_REASON_COURT));
                Assert.assertTrue(updatedCourt.getMoveDate().compareTo(newMoveDate) == 0);
                Assert.assertTrue(updatedCourt.getReturnDate().compareTo(newReturnDate) == 0);
            }
        }
    }

    //	private void shouldPassUpdatingAdHocCourtMovement() {
    //		List<CourtMovementType> toUpdate = new ArrayList<CourtMovementType>();
    //		TwoMovementsSearchType crit = new TwoMovementsSearchType();
    //		crit.setMovementType(MOVE_TYPE_COURT);
    //		crit.setFromFacilityId(facility1);
    //		List<CourtMovementType> ret = service.listTwoMovementsBy(uc, crit);
    //		Assert.assertFalse(ret.isEmpty());
    //		if(!ret.isEmpty()) {
    //			CourtMovementType court = ret.get(0);
    //			if(court != null) {
    //				court.setCourtPartRoom(165L);
    //				court.setMovementReason("FUN"); // Funeral
    //				toUpdate.add(court);
    //				List<CourtMovementType> updatedCourts = service.updateTwoMovements(uc, toUpdate, false);
    //				Assert.assertFalse(updatedCourts.isEmpty());
    //				CourtMovementType updatedCourt = updatedCourts.get(0);
    //				Assert.assertTrue(updatedCourt.getCourtPartRoom() == 165L);
    //				Assert.assertTrue(updatedCourt.getMovementReason().contentEquals("FUN"));
    //			}
    //		}
    //	}

    private void shouldPassScheduleIndividualCourtMovementWithoutSomeFields() {
        List<CourtMovementType> courts = new ArrayList<CourtMovementType>();
        CourtMovementType court = buildCourtMovementObject(supervisionId, facility1, facility2);
        courts.add(court);
        court.setComments(null);
        court.setCourtPartRoom(null);

        List<CourtMovementType> rets = (List<CourtMovementType>) service.createTwoMovements(uc, courts, false);
        assertNotNull(rets);
        for (CourtMovementType ret : rets) {
            Assert.assertTrue(ret.getComments() == null);
        }
    }

    private void shouldPassScheduleAdHocCourtMovement() {

        List<CourtMovementType> courts = new ArrayList<CourtMovementType>();
        courts.add(crt1);
        List<CourtMovementType> rets = (List<CourtMovementType>) service.createTwoMovements(uc, courts, true);
        Assert.assertFalse(rets.isEmpty());
        for (CourtMovementType ret : rets) {
            assertNotNull(ret);
            Assert.assertTrue(ret.getFromFacilityId().compareTo(facility1) == 0);
            Assert.assertTrue(ret.getSupervisionId().compareTo(supervisionId) == 0);
            Assert.assertTrue(ret.getMoveDate().compareTo(MOVE_DATE) == 0);
            Assert.assertTrue(ret.getReturnDate().compareTo(RETURN_DATE) == 0);
            Assert.assertTrue(ret.getMovementReason().contentEquals(MOVE_REASON_COURT));
            Assert.assertTrue(ret.getMovementType().contentEquals(MOVE_TYPE_COURT));
            assertNotNull(ret.getMovementIn());
            assertNotNull(ret.getMovementOut());
            Assert.assertTrue(ret.getComments().contentEquals(REQUEST_COMMENT));
            assertNotNull(ret.getGroupId());

            crt1 = ret;
            System.out.println(crt1.toString());
        }
    }
}