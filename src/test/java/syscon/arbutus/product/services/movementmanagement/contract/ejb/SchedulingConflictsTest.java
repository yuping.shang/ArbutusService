package syscon.arbutus.product.services.movementmanagement.contract.ejb;

import syscon.arbutus.product.services.common.BaseIT;

public class SchedulingConflictsTest extends BaseIT {
/*
    private static Logger log = LoggerFactory.getLogger(SchedulingConflictsTest.class);

	private static final String MOVE_TYPE_COURT = "CRT";
	private static final String MOVE_REASON_VISIT = "VIS";
	private static final String REQUEST_COMMENT = "Comments for this Court Movement request";
	private static final String MOVE_STATUS_APPROVE_REQ = "APPREQ";
	private static final String MOVE_TYPE_TAP = "TAP";
	private static final String TRANSPORT_POLICE = "POL";
	private static final String ESCORT_POLICE = "POLICE";
	
	protected ActivityService serviceActivity;	
	protected MovementService serviceMovement;
	protected MovementManagementService service;
	protected VisitationService serviceVisitation;
	
	private CourtMovementType crt1, crt2, crt3, crt4, crt5, crt6;
	private TemporaryAbsenceType tap1, tap2;
	private Long supervisionId, supervisionId2;
	private Long facility1, facility2, location1, org1, staffId;
	private Date today;
	
	@BeforeClass
	public void beforeClass() throws NamingException, Exception {
		log.info("beforeClass Begin - JNDI lookup");

		serviceActivity = (ActivityService)JNDILookUp(this.getClass(), ActivityService.class);
		serviceMovement = (MovementService)JNDILookUp(this.getClass(), MovementService.class);
		service = (MovementManagementService)JNDILookUp(this.getClass(), MovementManagementService.class);
		serviceVisitation = (VisitationService) JNDILookUp(this.getClass(), VisitationService.class);
		uc = super.initUserContext();
		
		FacilityTest facilityTest = new FacilityTest();
		facility1 = facilityTest.createFacility();
		facility2 = facilityTest.createFacility();
				
		OrganizationTest orgTest = new OrganizationTest();
		org1 = orgTest.createOrganization();
		
		SupervisionTest supervisionTest = new SupervisionTest();
		supervisionId = supervisionTest.createSupervision(facility1, true);
		supervisionId2 = supervisionTest.createSupervision(facility1, true);
		
		LocationTest locationTest = new LocationTest();
		LocationType loc = locationTest.createLocation(false, null, facility1);
		location1 = loc.getLocationIdentification();
		
		StaffTest staffTest = new StaffTest();
		staffId = staffTest.createStaff();
		
		today = BeanHelper.createDate();
		Date inThreeDaysSixAM = BeanHelper.createDateWithTime(BeanHelper.nextNthDays(BeanHelper.createDate(),3), 6, 0, 0);
		Date inFiveDaysSixAM = BeanHelper.createDateWithTime(BeanHelper.nextNthDays(BeanHelper.createDate(),5), 6, 0, 0);
		Date inSevenDaysSixAM = BeanHelper.createDateWithTime(BeanHelper.nextNthDays(BeanHelper.createDate(),7), 6, 0, 0);
		Date inTenDaysSixAM = BeanHelper.createDateWithTime(BeanHelper.nextNthDays(BeanHelper.createDate(),10), 6, 0, 0);
		Date inThirteenDaysSixAM = BeanHelper.createDateWithTime(BeanHelper.nextNthDays(BeanHelper.createDate(),13), 6, 0, 0);
		
		Date inMonthSixAM = BeanHelper.createDateWithTime(BeanHelper.nextNthDays(BeanHelper.createDate(),30), 6, 0, 0);
		Date inMonthNineAM = BeanHelper.createDateWithTime(BeanHelper.nextNthDays(BeanHelper.createDate(),30), 9, 0, 0);
		
		crt1 = buildCourtMovementObject(supervisionId, facility1, facility2, inFiveDaysSixAM, inTenDaysSixAM);
		crt2 = buildCourtMovementObject(supervisionId, facility1, facility2, inThreeDaysSixAM, inSevenDaysSixAM);
		crt3 = buildCourtMovementObject(supervisionId, facility1, facility2, inSevenDaysSixAM, inThirteenDaysSixAM);
		crt4 = buildCourtMovementObject(supervisionId, facility1, facility2, today, inThreeDaysSixAM);
		crt5 = buildCourtMovementObject(supervisionId, facility1, facility2, today, inThreeDaysSixAM);
		crt6 = buildCourtMovementObject(supervisionId2, facility1, facility2, inMonthSixAM, inMonthNineAM);
		
		crt1.setComments(null);
		crt3.setComments(null);
		
		tap1 = buildTemporaryAbsenceObject(supervisionId, facility1, facility2, inThreeDaysSixAM, inSevenDaysSixAM);
		tap2 = buildTemporaryAbsenceObject(supervisionId, facility1, facility2, inSevenDaysSixAM, inThirteenDaysSixAM);
		
		tap1.setComments(null);

	}
	
	@Test
	public void testJNDILookup() {
		assertNotNull(serviceActivity);
		assertNotNull(serviceMovement);
		assertNotNull(uc);

	}
	
	@Test(dependsOnMethods = "testJNDILookup")
	public void testGetVersion(){
		String version = service.getVersion(uc);
		assertNotNull(version);		
	}
	
	@Test(enabled = true)
	public void schedulingConflictsTest() {
		
//    	serviceMovement.deleteAll(uc);
//    	serviceVisitation.deleteAllVisits(uc);
		
		shouldHaveNoConflictsGoingAfterCRT();
		
		shouldConflictWithFirstInTheMoveDateCRT();
		
		shouldConflictWithFirstInTheReturnDateCRT();
		
		shouldConflictWithFirstInTheMoveDateTAP();
		
		shouldConflictWithFirstInTheReturnDateTAP();
		
		shouldConflictWithFirstInTheMoveDateTAPAcknowledged();
		
		shouldHaveNoConflictsGoingBefore();
		
		shouldConflictTimeTests();
		
		shouldNotReturnItselfAsConflict();
		
	}

	@Test(enabled = true)
	public void wor7584() {
		List<CourtMovementType> crts = new ArrayList<CourtMovementType>();
		Date moveDate1 =   BeanHelper.createDateWithTime(BeanHelper.createDate(), 2, 0, 0);
		Date returnDate1 = BeanHelper.createDateWithTime(BeanHelper.createDate(), 3, 0, 0);
		CourtMovementType wor1 = buildCourtMovementObject(supervisionId2, facility1, facility2, moveDate1, returnDate1);
		crts.add(wor1);
		List<CourtMovementType> rets = service.createTwoMovements(uc, crts, false);
		crts.remove(0);
		
		Date moveDate2 =   BeanHelper.createDateWithTime(BeanHelper.createDate(), 2, 1, 0);
		Date returnDate2 = BeanHelper.createDateWithTime(BeanHelper.createDate(), 3, 0, 0);
		CourtMovementType wor2 = buildCourtMovementObject(supervisionId2, facility1, facility2, moveDate2, returnDate2);
		
		TwoMovementsType ret = service.checkSchedulingConflict(uc, wor2);
		Assert.assertFalse(ret.getConflicts().isEmpty());
		
		Date moveDate3 =   BeanHelper.createDateWithTime(BeanHelper.createDate(), 1, 59, 0);
		Date returnDate3 = BeanHelper.createDateWithTime(BeanHelper.createDate(), 3, 0, 0);
		CourtMovementType wor3 = buildCourtMovementObject(supervisionId2, facility1, facility2, moveDate3, returnDate3);
		
		ret = service.checkSchedulingConflict(uc, wor3);
		Assert.assertFalse(ret.getConflicts().isEmpty());
	}
	
	@Test(enabled = true)
	public void shouldNotReturnItselfAsConflict() {
		
		List<CourtMovementType> crts = new ArrayList<CourtMovementType>();
		crts.add(crt6);
		
		List<CourtMovementType> rets = service.createTwoMovements(uc, crts, false);
		
		TwoMovementsType ret = service.checkSchedulingConflict(uc, crt6);
		for (TwoMovementsType conflict : ret.getConflicts()) {
			Assert.assertFalse(crt6.equals(conflict));
		}
		
	}

	@Test(dependsOnMethods="schedulingConflictsTest", enabled = false)
	public void schedulingConflictsRemoteCall() {
		crt3.setComments("goes in seven days, returns in thirteen days - conflicts with first movement");
		TwoMovementsType ret = service.checkSchedulingConflict(uc, crt3);
		for (TwoMovementsType conflict : ret.getConflicts()) {
			Assert.assertFalse(conflict.getComments().contentEquals("goes in seven days, returns in thirteen days - conflicts with first movement"));
			Assert.assertTrue(crt1.getGroupId().contentEquals(conflict.getGroupId()));	
			Assert.assertFalse(crt3.equals(conflict));
		}
	}
	
	//WOR-6841
	@Test(enabled = false)
	public void conflictsApprovalTAPWOR6841() {
		// Court Movement: OUT 2020-10-01 at 10am // IN 2020-10-01 at 11:30am
		createCourtMovementWOR6841();
		
		// Temporary Absence: OUT 2020-10-01 at 10am // IN 2020-10-02 at 09am
		String groupId = createTemporaryAbsenceWOR6841();
		
		approveTAPWOR6841(groupId);
		
	}

	// let's approve the TAP created and acknowledged the conflict.
	private void approveTAPWOR6841(String groupId) {
		if(groupId != null) {
			TwoMovementsSearchType searchType = new TwoMovementsSearchType();
			searchType.setGroupId(groupId);
			searchType.setMovementType(MOVE_TYPE_TAP);

			List<TemporaryAbsenceType> rets = service.listTwoMovementsBy(uc, searchType);
			TemporaryAbsenceType tap = rets.get(0);
			List<TemporaryAbsenceType> taps = new ArrayList<TemporaryAbsenceType>();
			
			tap.setApproverId(staffId);
			tap.setApprovalDate(BeanHelper.createDate());
			tap.setApprovalComments("TAP Approved!");
			tap.setToFacilityId(facility1);
			tap.setToFacilityLocationId(location1);
			taps.add(tap);
			rets = service.createTwoMovements(uc, taps, false);
			for (TemporaryAbsenceType created : rets) {
				Assert.assertFalse(created.getConflicts().isEmpty());
				Assert.assertTrue(created.getConflicts().size() == 1); //should have only one conflict, the court movement.
			}
			
			//acknowledge conflicts on approval.
			taps.remove(0);
			tap.setBypassConflicts(true);
			taps.add(tap);
			
			rets = service.createTwoMovements(uc, taps, false);
			for (TemporaryAbsenceType created : rets) {
				Assert.assertTrue(created.getConflicts().isEmpty());
				Assert.assertTrue(created.getApproverId() == staffId);
				Assert.assertTrue(created.getApprovalComments().contentEquals("TAP Approved!"));
			}
		}
	}

	// try to create a TAP that will have a conflict with a court movement
	private String createTemporaryAbsenceWOR6841() {
		Calendar date = Calendar.getInstance();
		date.set(2020, 10, 1, 10, 0);
		Date tapMove = date.getTime();
		
		date.set(2020, 10, 2, 9, 0);
		Date tapReturn = date.getTime();
		TemporaryAbsenceType tap = buildTemporaryAbsenceObject(supervisionId, facility1, facility2, tapMove, tapReturn);
		
		List<TemporaryAbsenceType> rets;
		List<TemporaryAbsenceType> taps = new ArrayList<TemporaryAbsenceType>();
		taps.add(tap);
		rets = service.createTwoMovements(uc, taps, false);
		for (TemporaryAbsenceType created : rets) {
			Assert.assertFalse(created.getConflicts().isEmpty());
		}
		return acknowledgeConflictWOR6841(tap);
	}

	// let's acknowledge the conflict and create the temporary absence anyways
	private String acknowledgeConflictWOR6841(TemporaryAbsenceType tap) {
		List<TemporaryAbsenceType> taps = new ArrayList<TemporaryAbsenceType>();
		List<TemporaryAbsenceType> rets;
		// acknowledge conflict
		tap.setBypassConflicts(true);
		taps.add(tap);
		rets = service.createTwoMovements(uc, taps, false);
		for (TemporaryAbsenceType created : rets) {
			Assert.assertTrue(created.getConflicts().isEmpty());
			Assert.assertTrue(created.getGroupId() != null);
			return created.getGroupId();
		}
		
		return null;
	}

	// creates a court movement that will be the conflict for other movement (tap)
	private void createCourtMovementWOR6841() {
		Calendar date = Calendar.getInstance();
		date.set(2020, 10, 1, 10, 0);
		Date courtMove = date.getTime();
		
		date.set(2020, 10, 1, 11, 30);
		Date courtReturn = date.getTime();
		CourtMovementType court = buildCourtMovementObject(supervisionId, facility1, facility2, courtMove, courtReturn);
		
		List<CourtMovementType> rets;
		List<CourtMovementType> crts = new ArrayList<CourtMovementType>();
		crts.add(court);
		rets = service.createTwoMovements(uc, crts, false);
		for (CourtMovementType created : rets) {
			Assert.assertTrue(created.getConflicts().isEmpty());
			Assert.assertTrue(created.getGroupId() != null);
		}
	}
	
	private void shouldConflictTimeTests() {
		// TODO Auto-generated method stub
		
	}

	private void shouldNotConflictWithCancelledMovements() {
		List<CourtMovementType> rets;
		List<CourtMovementType> crts = new ArrayList<CourtMovementType>();
		crt5.setComments("goes today, returns in three days - no conflicts with cancelled and others");
		crts.add(crt5);
		rets = service.createTwoMovements(uc, crts, false);
		for (CourtMovementType created : rets) {
			Assert.assertTrue(created.getConflicts().isEmpty());
			Assert.assertTrue(created.getGroupId() != null);
			Assert.assertTrue(created.getComments().contentEquals("goes today, returns in three days - no conflicts with cancelled and others"));
		}
	}

	private void cancelLastMovement() {
		List<CourtMovementType> crts = new ArrayList<CourtMovementType>();
		List<CourtMovementType> fromDB;
		List<CourtMovementType> rets;
		
		TwoMovementsSearchType searchType = new TwoMovementsSearchType();
		searchType.setMovementType(MOVE_TYPE_COURT);
		searchType.setGroupId(crt4.getGroupId());
		fromDB = service.listTwoMovementsBy(uc, searchType);
		if(!fromDB.isEmpty()) {
			crt4 = fromDB.get(0);
			crt4.setComments("cancelled");
			crt4.setMovementInStatus("CANCELLED");
			crt4.setMovementOutStatus("CANCELLED");
			crts.add(crt4);
			rets = service.updateTwoMovements(uc, crts, false);
			for (CourtMovementType cancelledCourt : rets) {
				Assert.assertTrue(cancelledCourt.getMovementInStatus().contentEquals("CANCELLED"));
				Assert.assertTrue(cancelledCourt.getMovementOutStatus().contentEquals("CANCELLED"));			
			}			
		}
	}

	private void shouldHaveNoConflictsGoingBefore() {
		List<CourtMovementType> crts = new ArrayList<CourtMovementType>();
		List<CourtMovementType> rets;
		crt4.setComments("goes today, returns in three days - no conflicts");
		crts.add(crt4);
		rets = service.createTwoMovements(uc, crts, false);
		for (CourtMovementType created : rets) {
			Assert.assertTrue(created.getGroupId() != null);
			Assert.assertTrue(created.getComments().contentEquals("goes today, returns in three days - no conflicts"));
		}
	}

	private void shouldConflictWithFirstInTheReturnDateCRT() {
		List<CourtMovementType> crts = new ArrayList<CourtMovementType>();
		List<CourtMovementType> rets;
		crt3.setComments("goes in seven days, returns in thirteen days - conflicts with first movement");
		crts.add(crt3);
		rets = service.createTwoMovements(uc, crts, false);
		for (CourtMovementType crtConflict : rets) {
			Assert.assertTrue(crtConflict.getGroupId() == null);
			for (TwoMovementsType conflict : crtConflict.getConflicts()) {
				Assert.assertFalse(conflict.getComments().contentEquals("goes in seven days, returns in thirteen days - conflicts with first movement"));
				Assert.assertTrue(crt1.getGroupId().contentEquals(conflict.getGroupId()));	
				Assert.assertFalse(crt3.equals(crtConflict));
			}
		}
	}

	private void shouldConflictWithFirstInTheMoveDateCRT() {
		List<CourtMovementType> crts = new ArrayList<CourtMovementType>();
		List<CourtMovementType> rets;
		crt2.setComments("goes in three days, returns in seven days - conflicts with first movement");
		crts.add(crt2);
		rets = service.createTwoMovements(uc, crts, false);
		for (CourtMovementType crtConflict : rets) {
			Assert.assertTrue(crtConflict.getGroupId() == null);
			for (TwoMovementsType conflict : crtConflict.getConflicts()) {
				Assert.assertFalse(conflict.getComments().contentEquals("goes in three days, returns in seven days - conflicts with first movement"));
				Assert.assertTrue(crt1.getGroupId().contentEquals(conflict.getGroupId()));	
				Assert.assertFalse(crt2.equals(crtConflict));
			}
		}
	}

	private void shouldHaveNoConflictsGoingAfterCRT() {
		List<CourtMovementType> crts = new ArrayList<CourtMovementType>();
		List<CourtMovementType> rets;
		crt1.setComments("goes in five days, returns in ten days - no conflicts");
		crts.add(crt1);
		rets = service.createTwoMovements(uc, crts, false);
		for (CourtMovementType created : rets) {
			Assert.assertTrue(created.getGroupId() != null);
			Assert.assertTrue(created.getComments().contentEquals("goes in five days, returns in ten days - no conflicts"));
			crt1 = created;
		}
	}
	
	private void shouldConflictWithFirstInTheReturnDateTAP() {
		List<TemporaryAbsenceType> taps = new ArrayList<TemporaryAbsenceType>();
		List<TemporaryAbsenceType> rets;
		tap1.setComments("goes in seven days, returns in thirteen days - conflicts with first movement");
		taps.add(tap1);
		rets = service.createTwoMovements(uc, taps, false);
		for (TemporaryAbsenceType crtConflict : rets) {
			Assert.assertTrue(crtConflict.getGroupId() == null);
			for (TwoMovementsType conflict : crtConflict.getConflicts()) {
				Assert.assertFalse(conflict.getComments().contentEquals("goes in seven days, returns in thirteen days - conflicts with first movement"));
				Assert.assertTrue(crt1.getGroupId().contentEquals(conflict.getGroupId()));	
				Assert.assertFalse(tap1.equals(conflict));
			}
		}
	}

	private void shouldConflictWithFirstInTheMoveDateTAP() {
		List<TemporaryAbsenceType> taps = new ArrayList<TemporaryAbsenceType>();
		List<TemporaryAbsenceType> rets;
		tap2.setComments("goes in three days, returns in seven days - conflicts with first movement");
		taps.add(tap2);
		rets = service.createTwoMovements(uc, taps, false);
		for (TemporaryAbsenceType crtConflict : rets) {
			Assert.assertTrue(crtConflict.getGroupId() == null);
			for (TwoMovementsType conflict : crtConflict.getConflicts()) {
				Assert.assertFalse(conflict.getComments().contentEquals("goes in three days, returns in seven days - conflicts with first movement"));
				Assert.assertTrue(crt1.getGroupId().contentEquals(conflict.getGroupId()));	
				Assert.assertFalse(tap2.equals(conflict));
			}
		}
	}

	 //* Show and Tell Demo
	 //* 
	 //* if I disable the ability to acknowledge a conflict and create anyways
	 //* that means: always returns a conflict (if there is any conflict, of course)
	 
	private void shouldConflictWithFirstInTheMoveDateTAPAcknowledged() {
		List<TemporaryAbsenceType> taps = new ArrayList<TemporaryAbsenceType>();
		List<TemporaryAbsenceType> rets;
		tap2.setComments("goes in three days, returns in seven days - conflicts with first movement");
		taps.add(tap2);
		rets = service.createTwoMovements(uc, taps, false);
		for (TemporaryAbsenceType crtConflict : rets) {
			Assert.assertTrue(crtConflict.getGroupId() == null);
			for (TwoMovementsType conflict : crtConflict.getConflicts()) {
				Assert.assertFalse(conflict.getComments().contentEquals("goes in three days, returns in seven days - conflicts with first movement"));
				Assert.assertTrue(crt1.getGroupId().contentEquals(conflict.getGroupId()));	
				Assert.assertFalse(tap2.equals(conflict));
			}
		}
		taps.remove(0);
		tap2.setBypassConflicts(true);
		tap2.setComments("goes in three days, returns in seven days - acknowledged conflicts!");
		
		taps.add(tap2);
		rets = service.createTwoMovements(uc, taps, false);
		for (TemporaryAbsenceType crtCreated : rets) {
			Assert.assertFalse(crtCreated.getGroupId() == null);
			Assert.assertTrue(crtCreated.getConflicts().isEmpty());
			Assert.assertTrue(crtCreated.getComments().contentEquals("goes in three days, returns in seven days - acknowledged conflicts!"));
		}
//		for (TemporaryAbsenceType crtConflict : rets) {
//			Assert.assertTrue(crtConflict.getGroupId() == null);
//			for (TwoMovementsType conflict : crtConflict.getConflicts()) {
//				Assert.assertFalse(conflict.getComments().contentEquals("goes in three days, returns in seven days - acknowledged conflicts!"));
//				Assert.assertTrue(crt1.getGroupId().contentEquals(conflict.getGroupId()));	
//				Assert.assertFalse(tap2.equals(conflict));
//			}
//		}
		
	}
	
	public CourtMovementType buildCourtMovementObject(Long supId, Long fromFacilityId, Long toFacilityId, Date moveDate, Date returnDate) {
		CourtMovementType court = new CourtMovementType();
		court.setCourtPartRoom(location1);
		court.setFromFacilityId(fromFacilityId);
		court.setToFacilityId(toFacilityId);
		court.setSupervisionId(supId);
		
		court.setMoveDate(moveDate);
		court.setReturnDate(returnDate);
		
		court.setMovementReason(MOVE_REASON_VISIT); // Visit
		court.setMovementType(MOVE_TYPE_COURT); // Court Movement

		court.setComments(REQUEST_COMMENT);

		return court;
	}
	
	private TemporaryAbsenceType buildTemporaryAbsenceObject(Long supId, Long fromFacilityId, Long toFacilityId, Date moveDate, Date returnDate) {
		TemporaryAbsenceType tap = new TemporaryAbsenceType();
		
		tap.setApplicationDate(today);
		tap.setToFacilityLocationId(location1);
		tap.setFromFacilityId(fromFacilityId);
		tap.setToFacilityId(toFacilityId);
		tap.setEscortOrganizationId(org1);
		tap.setEscortDetails(ESCORT_POLICE);

		tap.setSupervisionId(supId);
		
		tap.setMoveDate(moveDate);
		tap.setReturnDate(returnDate);
		
		tap.setMovementReason(MOVE_REASON_VISIT); //visit		
		tap.setMovementType(MOVE_TYPE_TAP); //temporary absence
		tap.setMovementInStatus(MOVE_STATUS_APPROVE_REQ); // approval for this movement is required
		tap.setMovementOutStatus(MOVE_STATUS_APPROVE_REQ); // approval for this movement is required
		tap.setTransportation(TRANSPORT_POLICE);

		tap.setComments(REQUEST_COMMENT);

		return tap;
	}
*/
}