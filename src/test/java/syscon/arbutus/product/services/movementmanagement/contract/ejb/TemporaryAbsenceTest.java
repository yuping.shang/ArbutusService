package syscon.arbutus.product.services.movementmanagement.contract.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.*;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.location.contract.dto.Location;
import syscon.arbutus.product.services.movement.contract.dto.OffenderScheduleTransferType;
import syscon.arbutus.product.services.movement.contract.dto.ScheduleTransferMovementSearch;
import syscon.arbutus.product.services.movement.contract.dto.TemporaryAbsenceType;
import syscon.arbutus.product.services.movement.contract.dto.TwoMovementsSearchType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;
import syscon.arbutus.product.services.realization.util.BeanHelper;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class TemporaryAbsenceTest extends BaseIT {

    private static final String REQUEST_COMMENT = "The contact person is Mr. William Madruga";
    private static final String APPROVAL_COMMENT = "Approved for test purposes";
    private static final Date MOVE_DATE = BeanHelper.createDateWithTime(BeanHelper.createDate(), 22, 0, 0);
    private static final Date RETURN_DATE = BeanHelper.nextNthDays(MOVE_DATE, 5);
    private static final String MOVE_STATUS_PENDING_MOVEMENT = "PENDING";
    private static final String MOVE_STATUS_COMPLETED_MOVEMENT = "COMPLETED";
    private static final String MOVE_STATUS_APPROVE_REQ = "APPREQ";
    private static final String MOVE_TYPE_TAP = "TAP";
    private static final String MOVE_REASON_VISIT = "VIS";
    private static final String TRANSPORT_POLICE = "POL";
    private static final String ESCORT_POLICE = "POLICE";
    private static Logger log = LoggerFactory.getLogger(TemporaryAbsenceTest.class);
    protected ActivityService serviceActivity;
    protected MovementService serviceMovement;
    protected MovementService service;
    private TemporaryAbsenceType tap1, tap2, tap3, tap4;
    private Date today;
    private Long staffId, facility1, facility2, org1, toOffenderLocationId, toFacilityLocationId;
    private Long supervision1, supervision2, supervision3, supervision4;

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");

        serviceActivity = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        serviceMovement = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
        service = (MovementService) JNDILookUp(this.getClass(), MovementService.class);

        FacilityTest facilityTest = new FacilityTest();
        facility1 = facilityTest.createFacility();
        facility2 = facilityTest.createFacility();

        SupervisionTest supervisionTest = new SupervisionTest();
        supervision1 = supervisionTest.createSupervision(facility1, true);
        supervision2 = supervisionTest.createSupervision(facility2, true);
        supervision3 = supervisionTest.createSupervision(facility1, true);
        supervision4 = supervisionTest.createSupervision(facility2, true);

        LocationTest locationTest = new LocationTest();
        Location location = locationTest.createLocation(false, supervision1, null);
        toOffenderLocationId = location.getLocationIdentification();

        location = locationTest.createLocation(false, null, facility1);
        toFacilityLocationId = location.getLocationIdentification();

        OrganizationTest orgTest = new OrganizationTest();
        org1 = orgTest.createOrganization();

        uc = super.initUserContext();
        today = BeanHelper.createDate();
        tap1 = buildTemporaryAbsenceObject(supervision1, facility1, facility2, toFacilityLocationId);
        tap2 = buildTemporaryAbsenceObject(supervision2, facility1, facility2, toFacilityLocationId);
        tap3 = buildTemporaryAbsenceObject(supervision3, facility1, facility2, toFacilityLocationId);
        tap4 = buildTemporaryAbsenceObject(supervision4, facility1, facility2, toFacilityLocationId);

        StaffTest staffTest = new StaffTest();
        staffId = staffTest.createStaff();
    }

    /**
     * JNDI not null test
     */
    @Test
    public void testJNDILookup() {
        assertNotNull(serviceActivity);
        assertNotNull(serviceMovement);
        assertNotNull(uc);

    }

    /**
     * Test to get service version
     */

    @Test(dependsOnMethods = "testJNDILookup")
    public void testGetVersion() {
        String version = service.getVersion(uc);
        assertNotNull(version);
    }

    /**
     * Temporary Absences Tests
     * July/2013
     */

    @Test(enabled = true)
    public void createTest() {
        shouldPassCreateTemporaryAbsence();
        shouldPassCreateTemporaryAbsenceAdHoc();
    }

    @Test(enabled = true, dependsOnMethods = "createTest")
    public void updateTest() {
        shouldPassUpdateTemporaryAbsenceWithApproval();
    }

    @Test(enabled = true)
    public void retrieveTest() {
        shouldReturnAllByFacilityId();
        shouldReturnAllByOffenderId();
    }

    @Test(enabled = true)
    public void getOffenderScheduleTransferListTest() {

        ScheduleTransferMovementSearch search = new ScheduleTransferMovementSearch();
        List<OffenderScheduleTransferType> ret = null;
        try {
            ret = service.getOffenderScheduleTransferList(uc, search);
        } catch (InvalidInputException e) {
            Assert.assertNotNull(e);
        }

        search.setFromFacility(facility1);
        search.setFromLocation(null);
        search.setFromScheduledDate(null);
        search.setToScheduledDate(null);
        ret = service.getOffenderScheduleTransferList(uc, search);
        Assert.assertNotNull(ret);

        search.setToFacility(facility1);
        search.setFromFacility(null);
        search.setFromLocation(null);
        search.setFromScheduledDate(null);
        search.setToScheduledDate(null);
        ret = service.getOffenderScheduleTransferList(uc, search);
        Assert.assertNotNull(ret);

    }

    @Test(enabled = false)
    public void cancelTest() {
        TwoMovementsSearchType crit = new TwoMovementsSearchType();
        crit.setGroupId(tap3.getGroupId());
        crit.setMovementType(MOVE_TYPE_TAP);
        List<TemporaryAbsenceType> rets = service.listTwoMovementsBy(uc, crit);
        TemporaryAbsenceType tap = rets.get(0);

        tap.setMovementInStatus("CANCELLED");
        tap.setMovementOutStatus("CANCELLED");
        tap.setApprovalDate(new Date(2013, 8, 15, 0, 0));
        tap.setApproverId(staffId);
        //tap.setMovementOutCome("TA IS CANCELLED");
        tap.setApprovalComments("TA IS CANCELLED");

        List<TemporaryAbsenceType> tas = new ArrayList<TemporaryAbsenceType>();
        tas.add(tap);
        List<TemporaryAbsenceType> ret = service.updateTwoMovements(uc, tas, false);
        Assert.assertNotNull(ret);

        rets = service.listTwoMovementsBy(uc, crit);
        tap = rets.get(0);
        Assert.assertEquals(tap.getMovementInStatus().toString(), "CANCELLED");
        Assert.assertEquals(tap.getMovementOutStatus().toString(), "CANCELLED");

    }

    @Test(enabled = true)
    public void actualizeTAP() {
        shouldActualizeMovementOut();
        shouldActualizeMovementIn();
    }

    private void shouldActualizeMovementOut() {
        List<TemporaryAbsenceType> tas = new ArrayList<TemporaryAbsenceType>();
        tap4.setMoveDate(new Date(2013, 11, 05));
        tap4.setReturnDate(new Date(2013, 11, 05));
        tas.add(tap4);
        List<TemporaryAbsenceType> rets = service.createTwoMovements(uc, tas, false);

        TwoMovementsSearchType searchType = new TwoMovementsSearchType();
        searchType.setGroupId(rets.get(0).getGroupId());
        searchType.setMovementType(MOVE_TYPE_TAP);
        tap4 = (TemporaryAbsenceType) service.listTwoMovementsBy(uc, searchType).get(0);
        tap4.setMovementInStatus(MOVE_STATUS_PENDING_MOVEMENT);
        tap4.setMovementOutStatus(MOVE_STATUS_PENDING_MOVEMENT);
        tap4.setApproverId(staffId);
        tap4.setApprovalDate(today);
        tap4.setApprovalComments(APPROVAL_COMMENT);

        tas.remove(0);
        tas.add(tap4);
        rets = null;
        rets = service.updateTwoMovements(uc, tas, false);
        for (TemporaryAbsenceType tapRet : rets) {
            Assert.assertTrue(tapRet.getMovementOutStatus().contentEquals(MOVE_STATUS_PENDING_MOVEMENT));
        }

        searchType = new TwoMovementsSearchType();
        searchType.setGroupId(rets.get(0).getGroupId());
        searchType.setMovementType(MOVE_TYPE_TAP);
        tap4 = (TemporaryAbsenceType) service.listTwoMovementsBy(uc, searchType).get(0);
        tap4.setMovementOutStatus(MOVE_STATUS_COMPLETED_MOVEMENT);
        tap4.setMoveDate(BeanHelper.createDate());
        tas.remove(0);
        tas.add(tap4);
        rets = null;
        rets = service.updateTwoMovements(uc, tas, false);
        for (TemporaryAbsenceType tapRet : rets) {
            Assert.assertTrue(tapRet.getMovementOutStatus().contentEquals(MOVE_STATUS_COMPLETED_MOVEMENT));
            Assert.assertTrue(tapRet.getMoveDate() != null);
        }

    }

    private void shouldActualizeMovementIn() {
        //		List<TemporaryAbsenceType> tas = new ArrayList<TemporaryAbsenceType>();
        //		TwoMovementsSearchType searchType = new TwoMovementsSearchType();
        //		List<TemporaryAbsenceType> rets;
        //		searchType = new TwoMovementsSearchType();
        //		searchType.setGroupId(tap4.getGroupId());
        //		searchType.setMovementType(MOVE_TYPE_TAP);
        //		tap4 = (TemporaryAbsenceType) service.listTwoMovementsBy(uc, searchType).get(0);
        //		tap4.setMovementInStatus(MOVE_STATUS_COMPLETED_MOVEMENT);
        //		tap4.setMoveDate(BeanHelper.createDate());
        //		tap4.setToFacilityLocationId(toFacilityLocationId);
        //		tas.add(tap4);
        //		rets = null;
        //		rets = service.updateTwoMovements(uc, tas, false);
        //		for (TemporaryAbsenceType tapRet : rets) {
        //			Assert.assertTrue(tapRet.getMovementInStatus().contentEquals(MOVE_STATUS_COMPLETED_MOVEMENT));
        //			Assert.assertTrue(tapRet.getMoveDate() != null);
        //		}

    }

    private void shouldReturnAllByFacilityId() {
        TwoMovementsSearchType crit = new TwoMovementsSearchType();
        crit.setMovementType(MOVE_TYPE_TAP);
        crit.setFromFacilityId(facility1);
        List<TemporaryAbsenceType> ret = service.listTwoMovementsBy(uc, crit);
        Assert.assertFalse(ret.isEmpty());
        // Getting one by one.
        for (TemporaryAbsenceType tap : ret) {
            crit.setFromFacilityId(null);
            crit.setGroupId(tap.getGroupId());
            List<TemporaryAbsenceType> rets = service.listTwoMovementsBy(uc, crit);
            assertNotNull(rets);
            TemporaryAbsenceType dbTap = rets.get(0);
            assertNotNull(dbTap);
            //Assert.assertTrue(dbTap.getOffenderName() != null);
            //Assert.assertFalse(dbTap.getOffenderName().contentEquals(""));
        }

        crit = new TwoMovementsSearchType();
        crit.setMovementType(MOVE_TYPE_TAP);
        crit.setToFacilityId(facility2);
        ret = service.listTwoMovementsBy(uc, crit);
        Assert.assertFalse(ret.isEmpty());
        // Getting one by one.
        for (TemporaryAbsenceType tap : ret) {
            crit.setFromFacilityId(null);
            crit.setGroupId(tap.getGroupId());
            List<TemporaryAbsenceType> rets = service.listTwoMovementsBy(uc, crit);
            assertNotNull(rets);
            TemporaryAbsenceType dbTap = rets.get(0);
            assertNotNull(dbTap);
            //Assert.assertTrue(dbTap.getOffenderName() != null);
            //Assert.assertFalse(dbTap.getOffenderName().contentEquals(""));
        }

    }

    private void shouldReturnAllByOffenderId() {
        TwoMovementsSearchType crit = new TwoMovementsSearchType();
        crit.setMovementType(MOVE_TYPE_TAP);
        crit.setOffenderId(supervision1);
        List<TemporaryAbsenceType> ret = service.listTwoMovementsBy(uc, crit);
        Assert.assertFalse(ret.isEmpty());
        // Getting one by one.
        for (TemporaryAbsenceType tap : ret) {
            crit.setOffenderId(null);
            crit.setGroupId(tap.getGroupId());
            List<TemporaryAbsenceType> rets = service.listTwoMovementsBy(uc, crit);
            TemporaryAbsenceType dbTap = rets.get(0);
            assertNotNull(dbTap);
            //			Assert.assertTrue(dbTap.getOffenderName() != null);
            //			Assert.assertFalse(dbTap.getOffenderName().contentEquals(""));
        }
    }

    private void shouldPassCreateTemporaryAbsence() {
        List<TemporaryAbsenceType> tas = new ArrayList<TemporaryAbsenceType>();
        tas.add(tap1);

        List<TemporaryAbsenceType> rets = service.createTwoMovements(uc, tas, false);
        assertNotNull(rets);
        for (TemporaryAbsenceType ret : rets) {
            Assert.assertTrue(ret.getToFacilityId().compareTo(facility2) == 0);
            Assert.assertTrue(ret.getSupervisionId().compareTo(supervision1) == 0);

            Assert.assertTrue(ret.getMoveDate().compareTo(MOVE_DATE) == 0);
            Assert.assertTrue(ret.getReturnDate().compareTo(RETURN_DATE) == 0);
            Assert.assertTrue(ret.getMovementReason().contentEquals(MOVE_REASON_VISIT));
            Assert.assertTrue(ret.getMovementType().contentEquals(MOVE_TYPE_TAP));
            Assert.assertTrue(ret.getMovementInStatus().contentEquals(MOVE_STATUS_APPROVE_REQ));
            Assert.assertTrue(ret.getMovementOutStatus().contentEquals(MOVE_STATUS_APPROVE_REQ));
            assertNotNull(ret.getMovementIn());
            assertNotNull(ret.getMovementOut());
            Assert.assertTrue(ret.getComments().contentEquals(REQUEST_COMMENT));
            Assert.assertTrue(ret.getTransportation().contentEquals(TRANSPORT_POLICE));
            Assert.assertTrue(ret.getEscortOrganizationId() == org1);
            Assert.assertTrue(ret.getToFacilityLocationId() == toFacilityLocationId);
            Assert.assertTrue(ret.getApplicationDate().compareTo(today) == 0);
            assertNotNull(ret.getGroupId());
            //For the update test, we're using the retrieved object
            tap1 = ret;
        }

        tap3.setMoveDate(BeanHelper.nextNthDays(MOVE_DATE, 30));
        tap3.setReturnDate(BeanHelper.nextNthDays(RETURN_DATE, 30));
        tas.remove(0);
        tas.add(tap3);
        rets = service.createTwoMovements(uc, tas, false);
        assertNotNull(rets);
        tap3 = rets.get(0);

    }

    private void shouldPassCreateTemporaryAbsenceAdHoc() {
        List<TemporaryAbsenceType> tas = new ArrayList<TemporaryAbsenceType>();
        tap2.setMoveDate(new Date(2013, 11, 04));
        tap2.setReturnDate(new Date(2013, 11, 05));
        tas.add(tap2);
        List<TemporaryAbsenceType> rets = service.createTwoMovements(uc, tas, true);
        assertNotNull(rets);
        for (TemporaryAbsenceType ret : rets) {
            Assert.assertTrue(ret.getMovementInStatus().contentEquals(MOVE_STATUS_PENDING_MOVEMENT));
            Assert.assertTrue(ret.getMovementOutStatus().contentEquals(MOVE_STATUS_COMPLETED_MOVEMENT));
            assertNotNull(ret.getGroupId());
        }
    }

    private void shouldPassUpdateTemporaryAbsenceWithApproval() {
        List<TemporaryAbsenceType> tas = new ArrayList<TemporaryAbsenceType>();
        // updating fields

        tap1.setBypassConflicts(true);
        tap1.setToFacilityId(null);
        tap1.setToFacilityLocationId(null);
        tap1.setToOffenderLocationId(toOffenderLocationId);
        tap1.setMovementInStatus(MOVE_STATUS_PENDING_MOVEMENT);
        tap1.setMovementOutStatus(MOVE_STATUS_PENDING_MOVEMENT);
        tap1.setApproverId(staffId);
        tap1.setApprovalDate(today);
        tap1.setApprovalComments(APPROVAL_COMMENT);
        tap1.setComments("New Comment when updating");
        // calling service update
        tas.add(tap1);
        List<TemporaryAbsenceType> ret = service.updateTwoMovements(uc, tas, false);
        // asserting changes
        assertNotNull(ret);
        Assert.assertFalse(ret.isEmpty());

        TwoMovementsSearchType crit = new TwoMovementsSearchType();
        crit.setMovementType(MOVE_TYPE_TAP);
        crit.setGroupId(ret.get(0).getGroupId());

        List<TemporaryAbsenceType> rets = service.listTwoMovementsBy(uc, crit);
        TemporaryAbsenceType tapMovement = rets.get(0);
        assertEquals(tapMovement.getApprovalComments(), APPROVAL_COMMENT);
        assertEquals(tapMovement.getApproverId(), staffId);
        assertEquals(tapMovement.getMovementInStatus(), MOVE_STATUS_PENDING_MOVEMENT);
        assertEquals(tapMovement.getMovementOutStatus(), MOVE_STATUS_PENDING_MOVEMENT);
        assertEquals(tapMovement.getToOffenderLocationId(), toOffenderLocationId);
        //		assertEquals(tapMovement.getApprovalDate(), today);

    }

    private TemporaryAbsenceType buildTemporaryAbsenceObject(Long supId, Long fromFacilityId, Long toFacilityId, Long toFacilityLocationId) {
        TemporaryAbsenceType tap = new TemporaryAbsenceType();

        tap.setApplicationDate(today);
        tap.setFromFacilityId(fromFacilityId);
        tap.setToFacilityLocationId(toFacilityLocationId);
        tap.setToFacilityId(toFacilityId);
        tap.setEscortOrganizationId(org1);
        tap.setEscortDetails(ESCORT_POLICE);

        tap.setSupervisionId(supId);

        tap.setMoveDate(MOVE_DATE);
        tap.setReturnDate(RETURN_DATE);

        tap.setMovementReason(MOVE_REASON_VISIT); //visit
        tap.setMovementType(MOVE_TYPE_TAP); //temporary absence
        tap.setMovementInStatus(MOVE_STATUS_APPROVE_REQ); // approval for this movement is required
        tap.setMovementOutStatus(MOVE_STATUS_APPROVE_REQ); // approval for this movement is required
        tap.setTransportation(TRANSPORT_POLICE);

        tap.setComments(REQUEST_COMMENT);

        return tap;
    }
}