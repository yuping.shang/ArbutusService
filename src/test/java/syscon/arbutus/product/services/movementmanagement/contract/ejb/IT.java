package syscon.arbutus.product.services.movementmanagement.contract.ejb;

import javax.naming.NamingException;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.ArbutusException;
import syscon.arbutus.product.services.movement.contract.dto.ExternalMovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.MovementDetailType;
import syscon.arbutus.product.services.movement.contract.ejb.MovementServiceBean;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * Unit test for Movement Management service
 */
public class IT extends BaseIT {

    public final static String MOVEMENT_DIRECTION = "MovementDirection";
    public final static String MOVEMENT_STATUS = "MovementStatus";
    public final static String MOVEMENT_TYPE = "MovementType";
    public final static String MOVEMENT_REASON = "MovementReason";
    public final static String MOVEMENT_CATEGORY = "MovementCategory";
    public final static String MOVEMENT_OUTCOME = "MovementOutcome";
    public final static String TO_PROVINCE_STATE = "STATEPROVINCE";
    protected static final String USER_NAME = "devtest";
    protected static final String PASSW = "devtest";
    private static final String ACTIVITY_CATEGORY = "MOVEMENT";
    private static final String MOVEMENT_CRT = "CRT";
    private static final String MOVEMENT_CATEGORY_EXTERNAL = "EXTERNAL";
    private static final String MOVEMENT_STATUS_PENDING = "PENDING";
    private static final String MOVEMENT_REASON_LA = "LA";
    private static final String MOVEMENT_OUTCOME_OUT = "Outcome1";
    private static Logger log = LoggerFactory.getLogger(IT.class);
    protected ActivityService serviceActivity;
    protected MovementService serviceMovement;
    protected MovementService service;
    Long fromLocationAssociation = 10000L;
    Long fromFacilityAssociation = 20000L;
    Long fromOrganizationAssociation = 30000L;
    Long toLocationAssociation = 40000L;
    Long toFacilityAssociation = 50000L;
    Long toOrganizationAssociation = 60000L;
    Long arrestOrganizationAssociation = 70000L;
    Long escortOrganizationAssociation = 80000L;

    String toProvinceState = "BC";
    String toCity = "Richmond";
    String fromCity = "Vancouver";
    Date applicationDate, fromApplicationDate, toApplicationDate;
    Date reportingDate, fromReportingDate, toReportingDate;

    Long fromFacilityInternalLocationAssociation = 100000L;
    Long toFacilityInternalLocationAssociation = 110000L;

    Long inId, exId;
    Set<String> refSetMaps = new LinkedHashSet<String>();
    String[] REF_SET = { MOVEMENT_CATEGORY, MOVEMENT_TYPE, MOVEMENT_REASON, MOVEMENT_OUTCOME, MOVEMENT_DIRECTION, MOVEMENT_STATUS, TO_PROVINCE_STATE };

    MovementDetailType mdReturn = new MovementDetailType();

    Long supervisionId = 1L;
    Long approverID = 1L;
    Long activityId;
    Long movementId;

    /**
     * Get JNDI lookup
     */
    @BeforeTest
    public void beforeTest() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");

        serviceActivity = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        serviceMovement = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
        service = (MovementService) JNDILookUp(this.getClass(), MovementService.class);

        uc = super.initUserContext();

    }

    /**
     * JNDI not null test
     */
    @Test
    public void testJNDILookup() {
        assertNotNull(serviceActivity);
        assertNotNull(serviceMovement);
        assertNotNull(uc);

    }

    /**
     * Test to get service version
     */

    @Test(dependsOnMethods = "testJNDILookup")
    public void testGetVersion() {
        String version = service.getVersion(uc);
        assertNotNull(version);
    }

	/*
     * Reset atomic services
	 * 
	 */

    @Test(dependsOnMethods = "testJNDILookup")
    public void reset() {
        //Reset required to setup reference data.
        /*assertEquals(serviceActivity.reset(uc), l(1));
		assertEquals(serviceMovement.reset(uc), l(1));*/
        assertNotNull(uc);
    }


    @Test(dependsOnMethods = "testJNDILookup")
    public void testCeateMovementSchedule(){
        ExternalMovementActivityType  externalMovementActivityType = new ExternalMovementActivityType();
        externalMovementActivityType.setPlannedStartDate(createScheduleStartDate());
//        externalMovementActivityType.setPlannedEndDate(createScheduleEndDate());
        externalMovementActivityType.setMovementType(MovementServiceBean.MovementType.CRT.code());
        externalMovementActivityType.setMovementReason("NEW");
        externalMovementActivityType.setToFacilityId(28L);//Denver County Court
        externalMovementActivityType.setSupervisionId(126L);//Adams,Aaron 15-45392
        Long outMovId = service.createMovementSchedule(uc, externalMovementActivityType,1L);
        log.info("OutMovement Id = " + outMovId);
        assertNotNull(outMovId);

    }




    @Test(dependsOnMethods = "testJNDILookup")
    public  void testUpdateScheduleMovement() {
        ExternalMovementActivityType externalMovementActivityType = new ExternalMovementActivityType();
        externalMovementActivityType.setMovementIdentification(1420L);
        externalMovementActivityType.setActivityId(2806L);
        externalMovementActivityType.setPlannedStartDate(createScheduleStartDate());
        externalMovementActivityType.setPlannedEndDate(createScheduleEndDate());
        externalMovementActivityType.setMovementType(MovementServiceBean.MovementType.TAP.code());
        CommentType  commentType = new CommentType();
        commentType.setCommentDate(new Date());
        commentType.setComment("update by Shen");

        Set<CommentType> comments = new HashSet<CommentType>();
        comments.add(commentType);

        externalMovementActivityType.setCommentText(comments);

        externalMovementActivityType.setMovementReason("FUN");
//        externalMovementActivityType.setToFacilityId(28L);//Denver County Court
//        externalMovementActivityType.setSupervisionId(126L);//Adams,Aaron 15-45392
        externalMovementActivityType.setMovementDirection(MovementServiceBean.MovementDirection.OUT.code());
        externalMovementActivityType.setMovementCategory(MovementServiceBean.MovementCategory.EXTERNAL.code());
        externalMovementActivityType.setMovementStatus(MovementActivityType.MovementStatus.PENDING.code());
        service.updateMovementSchedule(uc, externalMovementActivityType);
        MovementActivityType  movementActivityType = service.getMovementSchedule(uc, externalMovementActivityType.getMovementId());
        assertEquals(2806L, movementActivityType.getActivityId().longValue());
    }


    @Test(dependsOnMethods = "testJNDILookup")
    public  void testCompleteScheduleMovement() {
        ExternalMovementActivityType externalMovementActivityType = new ExternalMovementActivityType();
        externalMovementActivityType.setMovementIdentification(1374L);
        externalMovementActivityType.setActivityId(2743L);
        externalMovementActivityType.setMovementType(MovementServiceBean.MovementType.CRT.code());
        externalMovementActivityType.setMovementReason("NEW");
        externalMovementActivityType.setToFacilityId(28L);//Denver County Court
        externalMovementActivityType.setSupervisionId(126L);//Adams,Aaron 15-45392
        externalMovementActivityType.setMovementDirection(MovementServiceBean.MovementDirection.OUT.code());
        externalMovementActivityType.setMovementCategory(MovementServiceBean.MovementCategory.EXTERNAL.code());
        externalMovementActivityType.setMovementStatus(MovementActivityType.MovementStatus.COMPLETED.code());

        CommentType  commentType = new CommentType();
        commentType.setCommentDate(new Date());
        commentType.setComment("Completed by Howard Shen");

        Set<CommentType> comments = new HashSet<CommentType>();
        comments.add(commentType);

        externalMovementActivityType.setCommentText(comments);
        externalMovementActivityType.setMovementDate(new Date());
        service.completeMovementSchedule(uc, externalMovementActivityType);
    }

    @Test(dependsOnMethods = "testJNDILookup")
    public  void testCancelScheduleMovement() {

        ExternalMovementActivityType externalMovementActivityType = new ExternalMovementActivityType();
        externalMovementActivityType.setMovementIdentification(1375L);
        externalMovementActivityType.setActivityId(2744L);
        externalMovementActivityType.setMovementType(MovementServiceBean.MovementType.CRT.code());
        externalMovementActivityType.setMovementReason("NEW");
        externalMovementActivityType.setToFacilityId(28L);//Denver County Court
        externalMovementActivityType.setSupervisionId(126L);//Adams,Aaron 15-45392
        externalMovementActivityType.setMovementDirection(MovementServiceBean.MovementDirection.OUT.code());
        externalMovementActivityType.setMovementCategory(MovementServiceBean.MovementCategory.EXTERNAL.code());
        externalMovementActivityType.setMovementStatus(MovementActivityType.MovementStatus.CANCELLED.code());

        CommentType commentType = new CommentType();
        commentType.setCommentDate(new Date());
        commentType.setComment("Cancelled by Howard Shen");

        Set<CommentType> comments = new HashSet<CommentType>();
        comments.add(commentType);

        externalMovementActivityType.setCommentText(comments);
        externalMovementActivityType.setMovementDate(new Date());
        service.cancelMovementSchedule(uc, externalMovementActivityType);
        assertNotNull(externalMovementActivityType);


    }

    @Test(dependsOnMethods = "testJNDILookup")
    public  void testDeleteScheduleMovement() {


        service.deletedMovementSchedule(uc,1372L );



    }




    private Date createScheduleEndDate() {

        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.HOUR_OF_DAY, 16);
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.MINUTE, randLong(59).intValue());
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 2016);

        return cal.getTime();

    }

    private Date createScheduleStartDate() {

        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.HOUR_OF_DAY, 9);
        cal.set(Calendar.DAY_OF_MONTH, 30);
        cal.set(Calendar.MINUTE, 46);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 2016);

        return cal.getTime();

    }


    private Long randLong(int i) {
        return BigInteger.valueOf(new Random().nextInt(i)).abs().longValue();
    }

    private Long randLong(int from, int to) {
        Long rand = BigInteger.valueOf(new Random().nextInt(to)).abs().longValue();
        while (rand < from) {
            rand = BigInteger.valueOf(new Random().nextInt(to)).abs().longValue();
        }
        return rand;
    }
    /**
     * Test CreateLinkMovement method
     * @throws ParseException
     * @throws ArbutusException
     */

    //	@Test(enabled = true)
    //	public void testCreateLinkMovement() throws ParseException, ArbutusException{
    //
    //
    //		ExternalMovementActivityType emax = getMovementActivity("OUT");
    //		ActivityType activityx =  getActivity();
    //
    //		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );
    //		Calendar cal = Calendar.getInstance();
    //
    //		cal.setTime( dateFormat.parse( "2014-12-01" ) );
    //		cal.add( Calendar.DATE, 1 );
    //
    //
    //		Date presentTime = cal.getTime();
    //
    //		activityx.setPlannedStartDate(presentTime);
    //		activityx.setPlannedEndDate(presentTime);
    //
    //		emax.setMovementStatus(MOVE_STATUS_PENDING_MOVEMENT);
    //
    //		//Check uc validation
    //		MovementDetailType ret1 = service.createLinkedMovement(null, emax, activityx, supervisionId,false);
    //
    //		Assert.assertNull(ret1);
    //
    //		//Check movement validation
    //		MovementDetailType ret2 = service.createLinkedMovement(uc, null, activityx, supervisionId,false);
    //
    //		Assert.assertNull(ret2);
    //
    //		//Check activity validation
    //		MovementDetailType ret3 = service.createLinkedMovement(uc, emax, null, supervisionId,false);
    //
    //		Assert.assertNull(ret3);
    //
    //		//Check supervisionId validation
    //		MovementDetailType ret4 = service.createLinkedMovement(uc, emax, activityx, null,false);
    //
    //		Assert.assertNull(ret4);
    //
    //		MovementDetailType ret = service.createLinkedMovement(uc, emax, activityx, supervisionId,false);
    //
    //		Assert.assertNotNull(ret);
    //
    //		Set<MovementDetailType> mdlts = ret.getMovements();
    //
    //		for(MovementDetailType md : mdlts){
    //
    //			log.info(md.getActivity().toString());
    //			log.info(md.getMovement().toString());
    //
    //			assertNotNull(md.getMovement());
    //			assertNotNull(md.getActivity());
    //
    //			ActivityType activityz = md.getActivity();
    //
    //			assertEquals(activityz.getAssociations(), activityx.getAssociations());
    //			assertEquals(activityz.getActiveStatusFlag(), activityx.getActiveStatusFlag());
    //			assertEquals(activityz.getActivityAntecedentId(), activityx.getActivityAntecedentId());
    //			assertEquals(activityz.getActivityCategory(), activityx.getActivityCategory());
    //			assertEquals(activityz.getActivityDescendantAssociations(), activityx.getActivityDescendantAssociations());
    //			assertEquals(activityz.getPlannedEndDate(), activityx.getPlannedEndDate());
    //			assertEquals(activityz.getPlannedStartDate(), activityx.getPlannedStartDate());
    //			assertEquals(activityz.getScheduleID(), activityx.getScheduleID());
    //			assertEquals(activityz.getSupervisionId(), new AssociationType(AssociationToClass.SUPERVISION.value(),supervisionId));
    //			assertEquals(activityz.getDataPrivileges(), activityx.getDataPrivileges());
    //
    //			ExternalMovementActivityType emaz = (ExternalMovementActivityType) md.getMovement();
    //
    //			assertEquals(emaz.getApprovalComments(), emax.getApprovalComments());
    //			assertEquals(emaz.getApplicationDate(), emax.getApplicationDate());
    //			assertEquals(emaz.getApprovalDate(), emax.getApprovalDate());
    //			assertEquals(emaz.getApprovalComments(), emax.getApprovalComments());
    //
    //			assertEquals(emaz.getApprovedByStaffId(), emax.getApprovedByStaffId());
    //			assertEquals(emaz.getArrestOrganizationId(), emax.getArrestOrganizationId());
    //			Set<AssociationType> movementAssociations = new HashSet<AssociationType>();
    //			AssociationType activityAssoc = new AssociationType(AssociationToClass.ACTIVITY.value(), activityz.getActivityIdentification());
    //			movementAssociations.add(activityAssoc);
    //			assertEquals(emaz.getAssociations(), movementAssociations);
    //			assertEquals(emaz.getDataPrivileges(), emax.getDataPrivileges());
    //			assertEquals(emaz.getEscortDetails(), emax.getEscortDetails());
    //			assertEquals(emaz.getEscortOrganizationId(), emax.getEscortOrganizationId());
    //			assertEquals(emaz.getFacilities(), emax.getFacilities());
    //			assertEquals(emaz.getFromCity(), emax.getFromCity());
    //			assertEquals(emaz.getFromFacilityId(), emax.getFromFacilityId());
    //			assertEquals(emaz.getFromLocationId(), emax.getFromLocationId());
    //			assertEquals(emaz.getFromOrganizationId(), emax.getFromOrganizationId());
    //
    //			assertEquals(emaz.getMovementCategory(), emax.getMovementCategory());
    //			assertEquals(emaz.getMovementDate(), emax.getMovementDate());
    //			assertEquals(emaz.getMovementDirection(), emax.getMovementDirection());
    //			assertEquals(emaz.getMovementOutcome(), emax.getMovementOutcome());
    //			assertEquals(emaz.getMovementReason(), emax.getMovementReason());
    //			assertEquals(emaz.getMovementStatus(), emax.getMovementStatus());
    //
    //			assertEquals(emaz.getMovementType(), emax.getMovementType());
    //			assertEquals(emaz.getReportingDate(), emax.getReportingDate());
    //
    //			assertEquals(emaz.getSupervisionId(), new AssociationType(AssociationToClass.SUPERVISION.value(),supervisionId));
    //			assertEquals(emaz.getToCity(), emax.getToCity());
    //			assertEquals(emaz.getToFacilityId(), emax.getToFacilityId());
    //			assertEquals(emaz.getToFacilityInternalLocationId(), emax.getToFacilityInternalLocationId());
    //			assertEquals(emaz.getToLocationId(), emax.getToLocationId());
    //			assertEquals(emaz.getToOrganizationId(), emax.getToOrganizationId());
    //			assertEquals(emaz.getToProvinceState(), emax.getToProvinceState());
    //
    //
    //			emaz.setMovementDirection(DIRECTION_IN);
    //
    //			MovementDetailType retupt = service.updateLinkedMovements(uc, emaz, activityz);
    //
    //			assertEquals(retupt.getReturnCode(), SUCCESS);
    //
    //		}
    //
    //
    //	}

    /**
     * Test CrtLnkdMvmntWthDscndnt
     *
     * @throws ParseException
     * @throws ArbutusException
     */

    @Test(enabled = true)
    public void tstCrtLnkdMvmntWthDscndnt() throws ParseException, ArbutusException {

        ExternalMovementActivityType ema = getMovementActivity("OUT");
        ExternalMovementActivityType ema2 = getMovementActivity("IN");

        ActivityType activity = getActivity();

        ActivityType activity2 = getActivity();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateFormat.parse("2012-12-01"));

        cal.add(Calendar.DATE, 1);

        Date presentTime = cal.getTime();

        activity2.setPlannedStartDate(presentTime);
        activity2.setPlannedEndDate(presentTime);

        List<MovementDetailType> mdlts = service.createLinkedMovementWithDescendant(uc, ema, ema2, activity, activity2, supervisionId, false);

        Assert.assertNotNull(mdlts);

        for (MovementDetailType md : mdlts) {

            assertNotNull(md.getMovement());
            assertNotNull(md.getActivity());

            activityId = md.getActivity().getActivityIdentification();
            movementId = md.getMovement().getMovementId();

            ActivityType activityx = md.getActivity();

            assertEquals(activityx.getActiveStatusFlag(), activity.getActiveStatusFlag());
            //assertEquals(activityx.getActivityAntecedentId(), activity.getActivityAntecedentId());
            assertEquals(activityx.getActivityCategory(), activity.getActivityCategory());
            assertEquals(activityx.getActivityDescendantIds(), activity.getActivityDescendantIds());
            //assertEquals(activityx.getPlannedEndDate(), activity.getPlannedEndDate());
            //assertEquals(activityx.getPlannedStartDate(), activity.getPlannedStartDate());
            assertEquals(activityx.getScheduleID(), activity.getScheduleID());
            assertEquals(activityx.getSupervisionId(), supervisionId);

            ExternalMovementActivityType emax = (ExternalMovementActivityType) md.getMovement();

            if (emax.getMovementDirection().equals(ema.getMovementDirection())) {
                assertEquals(emax.getApprovalComments(), ema.getApprovalComments());
                assertEquals(emax.getApplicationDate(), ema.getApplicationDate());
                assertEquals(emax.getApprovalDate(), ema.getApprovalDate());
                assertEquals(emax.getApprovalComments(), ema.getApprovalComments());

                assertEquals(emax.getApprovedByStaffId(), ema.getApprovedByStaffId());
                assertEquals(emax.getArrestOrganizationId(), ema.getArrestOrganizationId());
                assertEquals(emax.getEscortDetails(), ema.getEscortDetails());
                assertEquals(emax.getEscortOrganizationId(), ema.getEscortOrganizationId());
                assertEquals(emax.getFromCity(), ema.getFromCity());
                assertEquals(emax.getFromFacilityId(), ema.getFromFacilityId());
                assertEquals(emax.getFromLocationId(), ema.getFromLocationId());
                assertEquals(emax.getFromOrganizationId(), ema.getFromOrganizationId());

                assertEquals(emax.getMovementCategory(), ema.getMovementCategory());
                assertEquals(emax.getMovementDate(), ema.getMovementDate());
                assertEquals(emax.getMovementDirection(), ema.getMovementDirection());
                assertEquals(emax.getMovementOutcome(), ema.getMovementOutcome());
                assertEquals(emax.getMovementReason(), ema.getMovementReason());
                assertEquals(emax.getMovementStatus(), ema.getMovementStatus());

                assertEquals(emax.getMovementType(), ema.getMovementType());
                assertEquals(emax.getReportingDate(), ema.getReportingDate());

                assertEquals(emax.getSupervisionId(), supervisionId);
                assertEquals(emax.getToCity(), ema.getToCity());
                assertEquals(emax.getToFacilityId(), ema.getToFacilityId());
                assertEquals(emax.getToFacilityInternalLocationId(), ema.getToFacilityInternalLocationId());
                assertEquals(emax.getToLocationId(), ema.getToLocationId());
                assertEquals(emax.getToOrganizationId(), ema.getToOrganizationId());
                assertEquals(emax.getToProvinceState(), ema.getToProvinceState());
            } else if (emax.getMovementDirection().equals(ema2.getMovementDirection())) {
                assertEquals(emax.getApprovalComments(), ema2.getApprovalComments());
                assertEquals(emax.getApplicationDate(), ema2.getApplicationDate());
                assertEquals(emax.getApprovalDate(), ema2.getApprovalDate());
                assertEquals(emax.getApprovalComments(), ema2.getApprovalComments());

                assertEquals(emax.getApprovedByStaffId(), ema2.getApprovedByStaffId());
                assertEquals(emax.getArrestOrganizationId(), ema2.getArrestOrganizationId());
                assertEquals(emax.getEscortDetails(), ema2.getEscortDetails());
                assertEquals(emax.getEscortOrganizationId(), ema2.getEscortOrganizationId());
                assertEquals(emax.getFromCity(), ema2.getFromCity());
                assertEquals(emax.getFromFacilityId(), ema2.getFromFacilityId());
                assertEquals(emax.getFromLocationId(), ema2.getFromLocationId());
                assertEquals(emax.getFromOrganizationId(), ema2.getFromOrganizationId());

                assertEquals(emax.getMovementCategory(), ema2.getMovementCategory());
                assertEquals(emax.getMovementDate(), ema2.getMovementDate());
                assertEquals(emax.getMovementDirection(), ema2.getMovementDirection());
                assertEquals(emax.getMovementOutcome(), ema2.getMovementOutcome());
                assertEquals(emax.getMovementReason(), ema2.getMovementReason());
                assertEquals(emax.getMovementStatus(), ema2.getMovementStatus());

                assertEquals(emax.getMovementType(), ema2.getMovementType());
                assertEquals(emax.getReportingDate(), ema2.getReportingDate());

                assertEquals(emax.getSupervisionId(), supervisionId);
                assertEquals(emax.getToCity(), ema2.getToCity());
                assertEquals(emax.getToFacilityId(), ema2.getToFacilityId());
                assertEquals(emax.getToFacilityInternalLocationId(), ema2.getToFacilityInternalLocationId());
                assertEquals(emax.getToLocationId(), ema2.getToLocationId());
                assertEquals(emax.getToOrganizationId(), ema2.getToOrganizationId());
                assertEquals(emax.getToProvinceState(), ema2.getToProvinceState());
            }

        }

    }

    /**
     * Test Get movement by movementId
     */
    @Test(dependsOnMethods = { "tstCrtLnkdMvmntWthDscndnt" }, enabled = true)
    public void tstGtMvmntDtlsByMvmntId() {

        MovementDetailType ret = service.getMovementDetailByMovementId(uc, movementId);
        Assert.assertNotNull(ret);

    }

    private ExternalMovementActivityType getMovementActivity(String direction) {

        ExternalMovementActivityType ema = new ExternalMovementActivityType();

        Set<String> dataPrivileges = new HashSet<String>();
        dataPrivileges.add("HP");

        ema.setMovementCategory(MOVEMENT_CATEGORY_EXTERNAL);
        // Set Movement Type
        ema.setMovementType(MOVEMENT_CRT); // CRT -- Court
        // Set Movement Direction
        ema.setMovementDirection(direction);
        // Set Movement Status
        ema.setMovementStatus(MOVEMENT_STATUS_PENDING);
        // Set Movement Reason
        ema.setMovementReason(MOVEMENT_REASON_LA); // LA -- Legal Aid

        // Set Movement Date
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR, 12);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        Date presentTime = c.getTime();
        ema.setMovementDate(presentTime);

        // Set Comments
        Set<CommentType> comments = new LinkedHashSet<CommentType>();
        CommentType ct1 = new CommentType();
        ct1.setCommentDate(presentTime);
        ct1.setComment("Court appear.");
        comments.add(ct1);
        CommentType ct2 = new CommentType();
        ct2.setCommentDate(presentTime);
        ct2.setComment("Go to court.");
        comments.add(ct2);
        ema.setCommentText(comments);

        ema.setFromLocationId(fromLocationAssociation);
        ema.setFromFacilityId(fromFacilityAssociation);
        ema.setFromOrganizationId(fromOrganizationAssociation);
        ema.setToProvinceState(toProvinceState);
        ema.setToLocationId(toLocationAssociation);
        ema.setToFacilityId(toFacilityAssociation);
        ema.setToOrganizationId(toOrganizationAssociation);
        applicationDate = presentTime;
        ema.setApplicationDate(applicationDate);
        ema.setArrestOrganizationId(arrestOrganizationAssociation);
        ema.setEscortOrganizationId(escortOrganizationAssociation);
        reportingDate = presentTime;
        ema.setReportingDate(presentTime);

        ema.setFromCity("Vancouver");
        ema.setToCity("Richmond");

        ema.setMovementOutcome(MOVEMENT_OUTCOME_OUT); // Outcome1 does not exist in reference set/code

        ema.setMovementOutcome(null);

        return ema;

    }

    private ActivityType getActivity() throws ArbutusException {

        ActivityType activity = new ActivityType();

        Date moveDate = new Date();

        activity.setActiveStatusFlag(Boolean.TRUE);

        activity.setActivityCategory(ACTIVITY_CATEGORY);

        activity.setPlannedStartDate(moveDate);
        activity.setPlannedEndDate(moveDate);

        return activity;

    }
}