package syscon.arbutus.product.services.userpreference.contract.ejb;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.common.PersonTest;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.userpreference.contract.dto.UserPreferenceSearchType;
import syscon.arbutus.product.services.userpreference.contract.dto.UserPreferenceType;
import syscon.arbutus.product.services.userpreference.contract.dto.UserPreferencesReturnType;
import syscon.arbutus.product.services.userpreference.contract.interfaces.UserPreferenceService;

public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);
    private UserPreferenceService service;
    private PersonService psnService;
    private PersonTest psnTest;
    private FacilityTest facilityTest;

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (UserPreferenceService) JNDILookUp(this.getClass(), UserPreferenceService.class);
        psnService = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        uc = super.initUserContext();
        psnTest = new PersonTest();
        facilityTest = new FacilityTest();
    }

    @Test(dataProvider = "dp")
    public void f(Integer n, String s) {
    }

    @BeforeMethod
    public void beforeMethod() {
        SecurityClient client;
        try {
            client = SecurityClientFactory.getSecurityClient();
            client.setSimple("devtest", "devtest"); // nice regular service user
            client.login();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @DataProvider
    public Object[][] dp() {
        return new Object[][] { new Object[] { 1, "a" }, new Object[] { 2, "b" }, };
    }

    @Test
    public void getCount() {

        service.deleteAll(uc);
        Long ret = service.getCount(uc);

        assert (ret >= 0L);
    }

    @Test
    public void delete() {

        UserPreferenceType uret = null;

        Long personId = psnTest.createPerson();
        Long facilityId = facilityTest.createFacility();
        UserPreferenceType userPreference = new UserPreferenceType(-1L, new String("en"), new String("Default"), new String("Metric"), new String(""), new String(""),
                personId, 1l, facilityId); //-05:00

        uret = service.save(uc, userPreference);
        assert (uret != null);
        Long userPrefId = uret.getUserPreferenceId();

        service.delete(uc, userPrefId);

        try {
            UserPreferenceType ret = service.get(uc, userPrefId);
            assert (ret == null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(DataNotExistException.class));
        }
    }

    @Test(enabled = true)
    public void getStamp() {

        UserPreferenceType uret = null;

        Long personId = psnTest.createPerson();
        Long facilityId = facilityTest.createFacility();
        UserPreferenceType userPreference = new UserPreferenceType(-1L, new String("en"), new String("Default"), new String("Metric"), new String(""), new String(""),
                personId, 1l, facilityId); //-05:00

        uret = service.save(uc, userPreference);
        assert (uret != null);
        Long userPrefId = uret.getUserPreferenceId();

        StampType retStamp = service.getStamp(null, userPrefId);

        assert (retStamp != null);
    }

    @Test(enabled = true)
    public void getVersion() {

        String ret = service.getVersion(uc);
        assert (ret != null);

    }

    @Test
    public void create() {

        UserPreferenceType uret = null;
        PersonTest psnTest = new PersonTest();
        Long personId = psnTest.createPerson();

        Long facilityId = facilityTest.createFacility();
        UserPreferenceType userPreference = new UserPreferenceType(-1L, new String("en"), new String("Default"), new String("Metric"), new String(""), new String(""),
                personId, 1l, facilityId); //-05:00

        uret = service.save(uc, userPreference);
        assert (uret != null);
    }

    @Test
    public void update() {

        UserPreferenceType uret = null;

        Long personId = psnTest.createPerson();

        Long facilityId = facilityTest.createFacility();
        UserPreferenceType userPreference = new UserPreferenceType(-1L, new String("en"), new String("Default"), new String("Metric"), new String(""), new String(""),
                personId, 1l, facilityId); //-05:00

        uret = service.save(uc, userPreference);
        Long userPreferenceId = uret.getUserPreferenceId();

        //
        userPreference.setUserPreferenceId(userPreferenceId);
        userPreference.setUserTimeFormat(new String("-06:00")); //-06:00
        uret = service.save(uc, userPreference);
        assert (uret.getUserTimeFormat().equals("-06:00"));
    }

    @Test
    public void search() {

        UserPreferencesReturnType usret = null;
        UserPreferenceType uret = null;

        Long personId = psnTest.createPerson();
        Long facilityId = facilityTest.createFacility();
        UserPreferenceType userPreference = new UserPreferenceType(-1L, new String("en"), new String("Default"), new String("Metric"), new String(""), new String(""),
                personId, 1l, facilityId); //-05:00

        //
        uret = service.save(uc, userPreference);
        assert (uret != null);
        Long userPreferenceId = uret.getUserPreferenceId();

        personId = psnTest.createPerson();
        userPreference = new UserPreferenceType(-1L, new String("en"), new String("Default"), new String("Metric"), new String(""), new String(""), personId, 1l,
                facilityId); //-05:00

        //
        uret = service.save(uc, userPreference);
        assert (uret != null);

        //
        UserPreferenceSearchType search = new UserPreferenceSearchType(new String("en"), null, null, null);
        usret = service.search(uc, null, search, 0L, 100L, null);
        assert (usret.getTotalSize() > 0L);

    }

    @Test
    public void testGetVersion() {
        String version = service.getVersion(null);
        assert ("1.2".equals(version));
    }

    @Test
    public void get() {

        UserPreferenceType uret = null;

        Long personId = psnTest.createPerson();

        Long facilityId = facilityTest.createFacility();
        UserPreferenceType userPreference = new UserPreferenceType(-1L, new String("en"), new String("Default"), new String("Metric"), new String(""), new String(""),
                personId, 1l, facilityId); //-05:00

        //
        uret = service.save(uc, userPreference);
        assert (uret != null);

        UserPreferenceType uretGet = service.get(uc, uret.getUserPreferenceId());
        log.info(String.format("uretGet.getUserPreferenceId() %s ==  uret.getUserPreferenceId() %s", uretGet.getUserPreferenceId(), uret.getUserPreferenceId()));
        assert (uretGet.getUserPreferenceId().equals(uret.getUserPreferenceId()));

    }

    @Test
    public void retrieve() {

        UserPreferenceType uret = null;

        Long personId = psnTest.createPerson();

        Long facilityId = facilityTest.createFacility();
        UserPreferenceType userPreference = new UserPreferenceType(-1L, new String("en"), new String("Default"), new String("Metric"), new String(""), new String(""),
                personId, 1l, facilityId); //-05:00

        //
        uret = service.save(uc, userPreference);
        assert (uret != null);

        UserPreferenceType uretGet = service.retrieve(uc, personId);
        log.info(String.format("uretGet.getPersonId() %s ==  personId %s", uretGet.getPersonId(), personId));
        assert (uretGet.getPersonId().equals(personId));

    }

    @Test
    public void getAll() {

        UserPreferenceType uret = null;

        Long personId = psnTest.createPerson();

        Long facilityId = facilityTest.createFacility();
        UserPreferenceType userPreference = new UserPreferenceType(-1L, new String("en"), new String("Default"), new String("Metric"), new String(""), new String(""),
                personId, 1l, facilityId); //-05:00

        //
        uret = service.save(uc, userPreference);
        assert (uret != null);

        Set<Long> uretGetAll = service.getAll(uc);
        assert (uretGetAll.size() > 0);

    }
}
