package syscon.arbutus.product.services.movementdetails.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import org.testng.annotations.Optional;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.movement.contract.dto.InternalMovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;

import java.util.*;

@Test(groups = { "internal" }, dependsOnGroups = "external")
public class CreateMovementActivityInternalPositiveTest extends SecureBase {
    private static final Logger LOG = LoggerFactory.getLogger(CreateMovementActivityInternalPositiveTest.class);

    MovementService service;

    Long cautionCount;
    Long cautionID;
    private List<Long> intMovIds = new ArrayList<Long>();
    private int date = 1;

	/*<pre>
     * 	<li>movementIdentification java.lang.Long Ignored when create, Required when update, get,...</li>
	 * 	<li>movementCategory MovementCategory, Required</li>
	 * 	<li>movementType java.lang.String (length <= 64), Required</li>
	 *  <li>movementDirection java.lang.String (length <= 64), Required</li>
	 *  <li>movementStatus java.lang.String (length <= 64), Required</li>
	 *  <li>movementReason java.lang.String (length <= 64), Required</li>
	 *  <li>movementOutcome java.lang.String (length <= 64), Optional</li>
	 *  <li>movementDate java.util.Date, Optional</li>
	 *  <li>commentText java.util.Set&lt;CommentTextType&gt;, Optional</li>
	 *  <li>associations Set&lt;AssociationType> refer to Activity and Supervision, Required</li>
	 *  <li>facilities Set&lt;AssociationType>, Ignored</li>
	 *  <li>dataPrivileges Set&lt;String>, Ignored</li>
	 * </pre>
	 * */

    @Parameters({ "hostNamePortNumber" })
    @BeforeClass
    public void beforeTest(@Optional("localhost:1099") String hostNamePortNumber) {
        LOG.info("-----beforeTest is running-------");
        service = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
        uc = super.initUserContext();

        cautionCount = service.getCount(null);
        LOG.trace("count=" + cautionCount);
    }

    /**
     * The reset method calls the service's reset() method to initialize all reference code/links data of the service to default values in DB.
     */
    @BeforeClass(dependsOnMethods = { "beforeTest" })
    public void reset() {
        /*
        ReferenceCodesReturnType rtn = service.getAllReferenceCodes(uc);
		if(rtn.getReturnCode().equals(SUCCESS) && rtn.getReferenceCodes() != null && rtn.getReferenceCodes().size() > 0){
			//We can retreive reference code back, this assumes the service has been reset already.
		}else{
			LOG.info("Reset...");
			Long returnCode = service.reset(uc);
			Assert.assertEquals( returnCode, SUCCESS );
			LOG.info("Reset successful.");
		}*/
    }

    @DataProvider(name = "NewCautionPositive")
    public Object[][] cautionObject(ITestContext context) {
        Object[][] retObjArr = {

                { "TC 00-01",
                        //parameter1
                        "{'fromFacilityInternalLocationId':1," +//{"toClass":"LocationService","toIdentifier":10000},
                                "'toFacilityInternalLocationId':2}",
                        //parameter3
                        "{'movementCategory':'INTERNAL'," +
                                "'movementType':'CRT'," +
                                "'movementDirection':'OUT'," +
                                "'movementStatus':'PENDING'," +
                                "'movementReason':'LA'," +
                                "'movementOutcome':'SICK'," +
                                "'movementDate':null," +
                                "'movementText':null," +
                                //[{'toClass':'LocationService','toIdentifier':1,'dataPrivileges':null},{'toClass':'OrganizationService','toIdentifier':0,'dataPrivileges':null}]
                                "'associations':[]," +
                                "'facilities':[]," +
                                "'dataPrivileges':[]}", context.getAttribute(IdType.ACTID.value() + 4), context.getAttribute(IdType.SUPID.value() + 1) }, { "TC 00-02",
                //parameter1
                "{'fromFacilityInternalLocationId':2," + "'toFacilityInternalLocationId':4}",
                //parameter3
                "{'movementCategory':'INTERNAL'," +
                        "'movementType':'CRT'," +
                        "'movementDirection':'OUT'," +
                        "'movementStatus':'PENDING'," +
                        "'movementReason':'LA'," +
                        "'movementOutcome':'SICK'," +
                        "'movementDate':null," +
                        "'movementText':null," +
                        //[{'toClass':'LocationService','toIdentifier':1,'dataPrivileges':null},{'toClass':'OrganizationService','toIdentifier':0,'dataPrivileges':null}]
                        "'associations':[]," +
                        "'facilities':[]," +
                        "'dataPrivileges':[]}", context.getAttribute(IdType.ACTID.value() + 5), context.getAttribute(IdType.SUPID.value() + 1) }, { "TC 00-03",
                //parameter1
                "{'fromFacilityInternalLocationId': 2," + "'toFacilityInternalLocationId': 4}",
                //parameter3
                "{'movementCategory':'INTERNAL'," +
                        "'movementType':'CRT'," +
                        "'movementDirection':'IN'," +
                        "'movementStatus':'PENDING'," +
                        "'movementReason':'LA'," +
                        "'movementOutcome':'SICK'," +
                        "'movementDate':null," +
                        "'movementText':null," +
                        //[{'toClass':'LocationService','toIdentifier':1,'dataPrivileges':null},{'toClass':'OrganizationService','toIdentifier':0,'dataPrivileges':null}]
                        "'associations':[]," +
                        "'facilities':[]," +
                        "'dataPrivileges':[]}", context.getAttribute(IdType.ACTID.value() + 6), context.getAttribute(IdType.SUPID.value() + 1) },

        };
        return (retObjArr);
    }

    /*public InternalMovementActivityType(Long movementIdentification,
            CodeType movementCategory, CodeType movementType,intId0"
            CodeType movementDirection, CodeType movementStatus,
            CodeType movementReason, CodeType movementOutcome, Date movementDate,
            Set<CommentType> commentText,
            Set<AssociationType> associations,
            Set<AssociationType> facilities,
            Set<String> dataPrivileges,
            AssociationType fromFacilityInternalLocationAssociation,
            AssociationType toFacilityInternalLocationAssociation)*/
    //@Test (dependsOnMethods = "buildAssociationSet", dataProvider = "NewFacilityPositive")
    @Test(dataProvider = "NewCautionPositive")
    public void testCreateMovementActivityInternalPositive(String tc, String internal, String string, Long actId, Long supId) {
        LOG.info("===== From Create Internal Movement Activity Positive Type Test=======");

        LOG.info("Testing function: create(UserContext, InternalMovementActivityType)");
        LOG.info("\nRunning TC = " + tc);

        MovementActivityType mov = gson.fromJson(string, MovementActivityType.class);
        mov.setSupervisionId(supId);
        mov.setActivityId(actId);

        LOG.info("=========================");

        InternalMovementActivityType imaRet_1 = new InternalMovementActivityType();

        InternalMovementActivityType itn = gson.fromJson(internal, InternalMovementActivityType.class);

        itn.setActivityId(mov.getActivityId());
        itn.setApprovalComments(mov.getApprovalComments());
        itn.setApprovalDate(mov.getApprovalDate());
        itn.setApprovedByStaffId(mov.getApprovedByStaffId());
        itn.setCommentText(mov.getCommentText());
        itn.setFromFacilityInternalLocationId(itn.getFromFacilityInternalLocationId());
        itn.setMovementCategory(mov.getMovementCategory());
        itn.setMovementDate(mov.getMovementDate());
        itn.setMovementDirection(mov.getMovementDirection());
        itn.setMovementIdentification(mov.getMovementId());

        itn.setMovementOutcome(mov.getMovementOutcome());
        itn.setMovementReason(mov.getMovementReason());
        itn.setMovementStatus(mov.getMovementStatus());
        itn.setMovementType(mov.getMovementType());
        itn.setSupervisionId(mov.getSupervisionId());
        itn.setToFacilityInternalLocationId(itn.getToFacilityInternalLocationId());

        imaRet_1 = (InternalMovementActivityType) service.create(uc, itn, Boolean.FALSE);
        LOG.trace(imaRet_1.toString());

        Assert.assertNotNull(imaRet_1);

        InternalMovementActivityType internalMov = imaRet_1;
        internalMov.setMovementStatus("COMPLETED");

        Set<CommentType> comments = new HashSet<CommentType>();
        comments.add(new CommentType(null, new Date(), "comment1"));
        internalMov.setCommentText(comments);

        Calendar cal = Calendar.getInstance();
        cal.set(2012, 9, date, 2, 6, 9);
        date++;
        internalMov.setMovementDate(cal.getTime());

        Assert.assertNotNull(service.update(uc, internalMov));

        intMovIds.add(internalMov.getMovementId());

        LOG.info("=========================");
    }

    @AfterClass
    public void setContext(ITestContext context) {
        for (int i = 0; i < intMovIds.size(); i++) {
            Long id = intMovIds.get(i);
            context.setAttribute(IdType.INTID.value() + i, id);
        }
    }

}
