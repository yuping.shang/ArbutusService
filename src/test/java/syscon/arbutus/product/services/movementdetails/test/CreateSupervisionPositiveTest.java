package syscon.arbutus.product.services.movementdetails.test;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import syscon.arbutus.product.services.common.PersonIdentityTest;
import syscon.arbutus.product.services.supervision.contract.dto.ImprisonmentStatusType;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionConfigVariablesType;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

import java.util.ArrayList;
import java.util.List;

@Test(groups = { "supervision" })
public class CreateSupervisionPositiveTest extends SecureBase {

    private static final Logger LOG = LoggerFactory.getLogger(CreateSupervisionPositiveTest.class);
    SupervisionService supService;
    List<ImprisonmentStatusType> setImprisonmentStatusType = new ArrayList<ImprisonmentStatusType>(); //Empty Set
    List<ImprisonmentStatusType> setImprisonmentStatusType1 = new ArrayList<ImprisonmentStatusType>(); //Set with 1 element
    List<ImprisonmentStatusType> setImprisonmentStatusType2 = new ArrayList<ImprisonmentStatusType>(); //Set with 2 element
    List<ImprisonmentStatusType> setImprisonmentStatusType3 = new ArrayList<ImprisonmentStatusType>(); //Set with 3 element
    private ImprisonmentStatusType imprisonmentStatusType1;
    private ImprisonmentStatusType imprisonmentStatusType2;
    private ImprisonmentStatusType imprisonmentStatusType3;
    private List<Long> supIds = new ArrayList<Long>();

    @Parameters({ "hostNamePortNumber" })
    @BeforeClass
    public void beforeTest(@Optional("localhost:1099") String hostNamePortNumber) {
        LOG.trace("-----beforeTest is running-------");
        supService = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        uc = super.initUserContext();
    }

    @BeforeClass(dependsOnMethods = { "beforeTest" })
    public void reset() {
        LOG.info("Reset...");
        Long returnCode = supService.deleteAll(uc);
        Assert.assertEquals(returnCode, SUCCESS);
        LOG.info("Reset successful.");
    }

    @DataProvider(name = "DP_TCs_01_for_createImprisonmentStatusType")
    public Object[][] createImprisonmentStatusType_TCs_01() {

        Object[][] retObjArr = {
                // fields sequence:
                //TC, statusLanguageCode, statusReferenceCode, statusReferenceCodeDescription, statusReferenceCodeSequence, statusReferenceSet,
                //subStatusLanguageCode, subStatusReferenceCode, subStatusReferenceCodeDescription, subStatusReferenceCodeSequence, subStatusReferenceSet,
                //mImprisonmentStatusStartDate,  mImprisonmentStatusEndDate, returnCode
                { "01-01", "en", "BOP", "Bureau of Prisons Inmate", "1", "ImprisonmentStatus", "en", "FPV", "Federal Parole Violator", "1", "ImprisonmentSubStatus",
                        "2001-03-22", "2008-03-22", "1" },
                { "01-02", "en", "CITY", "City Inmate", "6", "ImprisonmentStatus", "en", "PS", "Punitive Segregation", "4", "ImprisonmentSubStatus", "2002-03-22",
                        "3329-03-22", "1" },
                { "01-03", "en", "USM", "US Marshall Inmate", "4", "ImprisonmentStatus", "en", "SR", "State Ready", "3", "ImprisonmentSubStatus", "2003-03-22",
                        "3330-03-22", "1" },
                //{"04-01", "1"},

                //...
        };
        return (retObjArr);
    }

    @Test(dataProvider = "DP_TCs_01_for_createImprisonmentStatusType", alwaysRun = true)
    public void createImprisonmentStatusType_TC_01(String TC, String statusLanguageCode, String statusReferenceCode, String statusReferenceCodeDescription,
            String statusReferenceCodeSequence, String statusReferenceSet, String subStatusLanguageCode, String subStatusReferenceCode,
            String subStatusReferenceCodeDescription, String subStatusReferenceCodeSequence, String subStatusReferenceSet, String mImprisonmentStatusStartDate,
            String oImprisonmentStatusEndDate, String returnCode) {

        ImprisonmentStatusType IST1 = new ImprisonmentStatusType();
        ImprisonmentStatusType IST2 = new ImprisonmentStatusType();
        ImprisonmentStatusType IST3 = new ImprisonmentStatusType();

        LOG.info("======================================");
        LOG.info("Running TC for create ImprisonmentStatusType = " + TC);
        //// Creating ImprisonmentStatusType

        // Creating ImprisonmentStatusSetType
        String imprisonmentStatusSetType = new String();
        imprisonmentStatusSetType = statusReferenceCode.equals("null") ? null : statusReferenceCode;

        // Creating ImprisonmentSubStatusSetType
        String imprisonmentSubStatusSetType = new String();
        //imprisonmentSubStatusSetType.setLanguageCode(subStatusLanguageCode.equals("null") ? null : subStatusLanguageCode);
        imprisonmentSubStatusSetType = subStatusReferenceCode.equals("null") ? null : subStatusReferenceCode;
        //
        if (TC.equals("01-01")) {
            IST1.setImprisonmentStatus(imprisonmentStatusSetType);
            IST1.setImprisonmentStatusStartDate(mImprisonmentStatusStartDate.equals("null") ? null : TestUtils.convStringToDate(mImprisonmentStatusStartDate));
            IST1.setImprisonmentStatusEndDate(oImprisonmentStatusEndDate.equals("null") ? null : TestUtils.convStringToDate(oImprisonmentStatusEndDate));
            IST1.setImprisonmentSubStatus(imprisonmentSubStatusSetType);
            imprisonmentStatusType1 = IST1;
        } else if (TC.equals("01-02")) {
            IST2.setImprisonmentStatus(imprisonmentStatusSetType);
            IST2.setImprisonmentStatusStartDate(mImprisonmentStatusStartDate.equals("null") ? null : TestUtils.convStringToDate(mImprisonmentStatusStartDate));
            IST2.setImprisonmentStatusEndDate(oImprisonmentStatusEndDate.equals("null") ? null : TestUtils.convStringToDate(oImprisonmentStatusEndDate));
            IST2.setImprisonmentSubStatus(imprisonmentSubStatusSetType);
            imprisonmentStatusType2 = IST2;
        } else if (TC.equals("01-03")) {
            IST3.setImprisonmentStatus(imprisonmentStatusSetType);
            IST3.setImprisonmentStatusStartDate(mImprisonmentStatusStartDate.equals("null") ? null : TestUtils.convStringToDate(mImprisonmentStatusStartDate));
            IST3.setImprisonmentStatusEndDate(oImprisonmentStatusEndDate.equals("null") ? null : TestUtils.convStringToDate(oImprisonmentStatusEndDate));
            IST3.setImprisonmentSubStatus(imprisonmentSubStatusSetType);
            imprisonmentStatusType3 = IST3;
        }

        ////
        if (TC.equals("01-03")) {
            setImprisonmentStatusType1.add(imprisonmentStatusType1);
            //setImprisonmentStatusType2.add(imprisonmentStatusType1);
            //setImprisonmentStatusType2.add(imprisonmentStatusType2);
            setImprisonmentStatusType3.add(imprisonmentStatusType1);
            setImprisonmentStatusType3.add(imprisonmentStatusType2);
            setImprisonmentStatusType3.add(imprisonmentStatusType3);
//            LOG.trace(setImprisonmentStatusType.size());
//            LOG.trace(setImprisonmentStatusType1.size());
//            LOG.trace(setImprisonmentStatusType3.size());
        }
    }

    @DataProvider(name = "DP_TCs_01_for_createSupervisionType")
    public Object[][] createSupervisionType_TCs_01() {

        Object[][] retObjArr = {
                // fields sequence:
                //TC, mSupervisionDisplayID, mSupervisionStartDate, mSupervisionEndDate,oDateOrientationProvided,
                //mSupervisionStatusFlag, mReferenceToClass1, mReferenceToIdentifier1, mReferenceToClass2, mReferenceToIdentifier2, returnCode
                { "01-01", "mSupervisionDisplayID1", "2000-03-22", "null", "null", "true", "PersonIdentityService", "100", "FacilityService", "2", "1" },
                { "01-02", "mSupervisionDisplayID2", "2000-03-22", "null", "2002-03-22", "true", "PersonIdentityService", "101", "FacilityService", "2", "1" },
                { "01-03", "mSupervisionDisplayID3", "2000-03-22", "3333-03-22", "2003-03-22", "true", "PersonIdentityService", "102", "FacilityService", "2", "1" },
                //{"01-04", "dhfghdfg 4365 fgthr3245 gfhnm,ghjfghfdggdhfg$#@ *&^jghdfdn,mn,kk", "2000-03-22", "3333-03-22", "2003-03-22", "true", "PersonIdentityService", "100", "FacilityService" , "2", "1"},
                //{"01-05", "12ывапвапывапвапывапвф  44ЦЫВЫВВВЫЫЫЫЁЁЁЁЁЁЁЁЁЁЮЮЮЮЮЮЮЮЮЮЮЮЮЮ4ЮУ", "2000-03-22", "3333-03-22", "2003-03-22", "true", "PersonIdentityService", "100", "FacilityService" , "2", "1"},
                //{"04-01", "1"},

                //...
        };
        return (retObjArr);
    }

    @Test(dependsOnMethods = "createImprisonmentStatusType_TC_01", dataProvider = "DP_TCs_01_for_createSupervisionType", alwaysRun = true)
    public void createSupervisionType_TC_01(String TC, String mSupervisionDisplayID, String mSupervisionStartDate, String mSupervisionEndDate,
            String oDateOrientationProvided, String mSupervisionStatusFlag, String mReferenceToClass1, String mReferenceToIdentifier1, String mReferenceToClass2,
            String mReferenceToIdentifier2, String returnCode) {

        LOG.info("======================================");
        LOG.info("Running TC for create = " + TC);
        Gson gson = new Gson();
        // Creating SupervisionConfigVariablesType
        SupervisionConfigVariablesType supVarType = new SupervisionConfigVariablesType();
        supVarType.setSupervisionDisplayIDFormat("YYYY-99999");
        supVarType.setSupervisionDisplayIDGenerationFlag(false);
        SupervisionConfigVariablesType supVarReturnType = new SupervisionConfigVariablesType();
        supVarReturnType = supService.createConfigVars(uc, supVarType);
        //LOG.trace(supVarReturnType.getSupervisionConfigurationVariables());
        Assert.assertTrue(supVarReturnType != null);

        // Creating Supervision Type
        SupervisionType supType = new SupervisionType();
        supType.setSupervisionDisplayID(mSupervisionDisplayID.equals("null") ? null : mSupervisionDisplayID);
        supType.setSupervisionStartDate(mSupervisionStartDate.equals("null") ? null : TestUtils.convStringToDate(mSupervisionStartDate));
        supType.setSupervisionEndDate(mSupervisionEndDate.equals("null") ? null : TestUtils.convStringToDate(mSupervisionEndDate));
        supType.setDateOrientationProvided(oDateOrientationProvided.equals("null") ? null : TestUtils.convStringToDate(oDateOrientationProvided));
        //supType.setSupervisionStatusFlag(mSupervisionStatusFlag.equals("true") ? true : false);
        if (TC.equals("01-02")) {
            supType.setImprisonmentStatus(setImprisonmentStatusType1);
        } //setting with 1 ImprisonmentStatusType
        else if (TC.equals("01-03")) {
            supType.setImprisonmentStatus(setImprisonmentStatusType3);
        } //setting with 3 ImprisonmentStatusType
        else {
            supType.setImprisonmentStatus(setImprisonmentStatusType);
        } //setting with 0 ImprisonmentStatusTypes

        Long personIdentityId = mReferenceToIdentifier1.equals("null") ? null : Long.parseLong(mReferenceToIdentifier1);
        supType.setPersonIdentityId(personIdentityId);

        Long facilityId = mReferenceToIdentifier2.equals("null") ? null : Long.parseLong(mReferenceToIdentifier2);
        supType.setFacilityId(facilityId);

        SupervisionType supReturnType = new SupervisionType();
        LOG.trace("supType for create : " + supType);

        personIdentityId = new PersonIdentityTest().createPersonIdentity();
        supType.setPersonIdentityId(personIdentityId);

        supReturnType = supService.create(uc, supType);
        LOG.trace("supReturnType : " + gson.toJson(supReturnType));

        //=================Checking against Return Type=============================
        Assert.assertTrue(supReturnType != null);

        Assert.assertTrue(supReturnType.getSupervisionStatusFlag().equals(mSupervisionStatusFlag.equals("true") ? true : false));
        Assert.assertTrue(supReturnType.getSupervisionDisplayID().equals(mSupervisionDisplayID.equals("null") ? null : mSupervisionDisplayID));
        Assert.assertEquals(supReturnType.getSupervisionEndDate(), (mSupervisionEndDate.equals("null") ? null : TestUtils.convStringToDate(mSupervisionEndDate)));
        Assert.assertEquals(supReturnType.getSupervisionStartDate(), (mSupervisionStartDate.equals("null") ? null : TestUtils.convStringToDate(mSupervisionStartDate)));
        Assert.assertEquals(supReturnType.getDateOrientationProvided(),
                (oDateOrientationProvided.equals("null") ? null : TestUtils.convStringToDate(oDateOrientationProvided)));
        List<ImprisonmentStatusType> setRetType = new ArrayList<ImprisonmentStatusType>();
        setRetType = supReturnType.getImprisonmentStatus();
        ImprisonmentStatusType[] Ss = setRetType.toArray(new ImprisonmentStatusType[0]);
        for (int i = 0; i < Ss.length; i++) {
            Assert.assertTrue((Ss[i].getImprisonmentStatusIdentification() != null) && (Ss[i].getImprisonmentStatusIdentification() > 0L));
            Ss[i].setImprisonmentStatusIdentification(null);
            if (TC.equals("01-02")) {
                Assert.assertTrue(gson.toJson(setImprisonmentStatusType1).contains(gson.toJson(Ss[i])));
            } else if (TC.equals("01-03")) {
                Assert.assertTrue(gson.toJson(setImprisonmentStatusType3).contains(gson.toJson(Ss[i])));
            } else {
                Assert.assertTrue(gson.toJson(setImprisonmentStatusType).contains(gson.toJson(Ss[i])));
            }
        }
        Long supId = supReturnType.getSupervisionIdentification();

        //=================Checking against DB=============================
        SupervisionType supReturnTypeDB = new SupervisionType();
        supReturnTypeDB = supService.get(uc, supId);
        Assert.assertTrue(supReturnTypeDB != null);
        Assert.assertTrue(supReturnTypeDB.getSupervisionStatusFlag().equals(mSupervisionStatusFlag.equals("true") ? true : false));
        Assert.assertTrue(supReturnTypeDB.getSupervisionDisplayID().equals(mSupervisionDisplayID.equals("null") ? null : mSupervisionDisplayID));
        Assert.assertEquals(TestUtils.convDateToString(supReturnTypeDB.getSupervisionEndDate()),
                (mSupervisionEndDate.equals("null") ? null : TestUtils.convDateToString(TestUtils.convStringToDate(mSupervisionEndDate))));
        Assert.assertEquals(TestUtils.convDateToString(supReturnTypeDB.getSupervisionStartDate()),
                (mSupervisionStartDate.equals("null") ? null : TestUtils.convDateToString(TestUtils.convStringToDate(mSupervisionStartDate))));
        Assert.assertEquals(TestUtils.convDateToString(supReturnTypeDB.getDateOrientationProvided()),
                (oDateOrientationProvided.equals("null") ? null : TestUtils.convDateToString(TestUtils.convStringToDate(oDateOrientationProvided))));
        List<ImprisonmentStatusType> setRetTypeDB = new ArrayList<ImprisonmentStatusType>();
        setRetTypeDB = supReturnTypeDB.getImprisonmentStatus();
        ImprisonmentStatusType[] SsDB = setRetTypeDB.toArray(new ImprisonmentStatusType[0]);
        for (int i = 0; i < SsDB.length; i++) {
            Assert.assertTrue((SsDB[i].getImprisonmentStatusIdentification() != null) && (SsDB[i].getImprisonmentStatusIdentification() > 0L));
            SsDB[i].setImprisonmentStatusIdentification(null);
            if (TC.equals("01-02")) {
                Assert.assertTrue(gson.toJson(setImprisonmentStatusType1).contains(gson.toJson(SsDB[i])));
            } else if (TC.equals("01-03")) {
                Assert.assertTrue(gson.toJson(setImprisonmentStatusType3).contains(gson.toJson(SsDB[i])));
            } else {
                Assert.assertTrue(gson.toJson(setImprisonmentStatusType).contains(gson.toJson(SsDB[i])));
            }
        }

        supIds.add(supReturnTypeDB.getSupervisionIdentification());

    }

    @AfterClass
    public void setContext(ITestContext context) {
        for (int i = 0; i < supIds.size(); i++) {
            Long id = supIds.get(i);
            context.setAttribute(IdType.SUPID.value() + i, id);
        }
    }
}

	

	