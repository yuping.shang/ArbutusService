package syscon.arbutus.product.services.movementdetails.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import org.testng.annotations.Optional;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.movement.contract.dto.ExternalMovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;

import java.util.*;

@Test(groups = { "external" }, dependsOnGroups = { "activity", "supervision", "housing" })
public class CreateMovementActivityExternalPositiveTest extends SecureBase {
    private static final Logger LOG = LoggerFactory.getLogger(CreateMovementActivityExternalPositiveTest.class);

    MovementService service;

    Long cautionCount;
    Long cautionID;
    private List<Long> extMovIds = new ArrayList<Long>();
    private int date = 1;

	/*<pre>
     * 	<li>movementIdentification java.lang.Long Ignored when create, Required when update, get,...</li>
	 * 	<li>movementCategory MovementCategory, Required</li>
	 * 	<li>movementType java.lang.String (length <= 64), Required</li>
	 *  <li>movementDirection java.lang.String (length <= 64), Required</li>
	 *  <li>movementStatus java.lang.String (length <= 64), Required</li>
	 *  <li>movementReason java.lang.String (length <= 64), Required</li>
	 *  <li>movementOutcome java.lang.String (length <= 64), Optional</li>
	 *  <li>movementDate java.util.Date, Optional</li>
	 *  <li>commentText java.util.Set&lt;CommentTextType&gt;, Optional</li>
	 *  <li>associations Set&lt;AssociationType> refer to Activity and Supervision, Required</li>
	 *  <li>facilities Set&lt;AssociationType>, Ignored</li>
	 *  <li>dataPrivileges Set&lt;String>, Ignored</li>
	 * </pre>
	 * */

    @Parameters({ "hostNamePortNumber" })
    @BeforeClass
    public void beforeTest(@Optional("localhost:1099") String hostNamePortNumber) {
        LOG.info("-----beforeTest is running-------");
        service = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
        uc = super.initUserContext();

        cautionCount = service.getCount(null);
        //LOG.trace(cautionCount);
    }

    /**
     * The reset method calls the service's reset() method to initialize all reference code/links data of the service to default values in DB.
     */
    @BeforeClass(dependsOnMethods = { "beforeTest" })
    public void reset() {
        /*
        ReferenceCodesReturnType rtn = service.getAllReferenceCodes(uc);
		if(rtn.getReturnCode().equals(SUCCESS) && rtn.getReferenceCodes() != null && rtn.getReferenceCodes().size() > 0){
			//We can retreive reference code back, this assumes the service has been reset already.
		}else{
			LOG.info("Reset...");
			Long returnCode = service.reset(uc);
			Assert.assertEquals( returnCode, SUCCESS );
			LOG.info("Reset successful.");
		}*/
    }

    @DataProvider(name = "NewCautionPositive")
    public Object[][] cautionObject(ITestContext context) {
        Object[][] retObjArr = {
                //TC, Long cautionId, CodeType code, CodeType category,	Date effectiveDate, Date expiryDate, Set<CommentType> comments,	AssociationType authorization, Set<AssociationType> associations,Set<AssociationType> facilities, Set<String> dataPrivileges
                { "TC 00-01", "{'fromLocationId':10000," +//{"toClass":"LocationService","toIdentifier":10000},
                        "'fromOrganizationId':30000," +//{"toClass":"OrganizationService","toIdentifier":30000},
                        "'fromFacilityId':20000," +//{"toClass":"FacilityService","toIdentifier":20000},
                        "'fromCity':'Vancouver'," +//"Vancouver",
                        "'toFacilityInternalLocationId':null," +
                        "'toLocationId':40000," +//{"toClass":"LocationService","toIdentifier":40000},
                        "'toOrganizationId':60000," +//{"toClass":"OrganizationService","toIdentifier":60000},
                        "'toFacilityId':1," +//{"toClass":"FacilityService","toIdentifier":50000},
                        "'toCity':'Richmond'," +//"Richmond",
                        "'toProvinceState':'BC'," +//"BC",
                        "'arrestOrganizationId' :70000," +
                        //{"toClass":"OrganizationService","toIdentifier":70000},
                        "'applicationDate':null," +//null,
                        "'escortOrganizationId':80000," +
                        //{"toClass":"OrganizationService","toIdentifier":80000},
                        "'reportingDate':null }",//null"'movementIdentification':null," +

                        "{'movementCategory':'EXTERNAL'," +
                                "'movementType':'CRT'," +
                                "'movementDirection':'OUT'," +
                                "'movementStatus':'PENDING'," +
                                "'movementReason':'LA'," +
                                // "'movementOutcome':{'set':'MovementOutcome','code':'EXTERNAL'}," +
                                "'movementDate':null," +
                                "'movementText':null," +
                                "'approvedByStaffId':null," +
                                "'approvalDate':null," +
                                "'approvalComments':null," +

                                "'associations':[]," +
                                "'facilities':[]," +
                                "'dataPrivileges':[]}", context.getAttribute(IdType.ACTID.value() + 0), context.getAttribute(IdType.SUPID.value() + 0) },

                { "TC 00-02", "{'fromLocationId':null," +//{"toClass":"LocationService","toIdentifier":10000},
                        "'fromOrganizationId': null," +//{"toClass":"OrganizationService","toIdentifier":30000},
                        "'fromFacilityId': null," +//{"toClass":"FacilityService","toIdentifier":20000},
                        "'fromCity':null," +//"Vancouver",
                        "'toFacilityInternalLocationId':99999," +
                        "'toLocationId':null," +//{"toClass":"LocationService","toIdentifier":40000},
                        "'toOrganizationId':null," +//{"toClass":"OrganizationService","toIdentifier":60000},
                        "'toFacilityId':2," +//{"toClass":"FacilityService","toIdentifier":50000},
                        "'toCity':null," +//"Richmond",
                        "'toProvinceState':null," +//"BC",
                        "'arrestOrganizationId' :null," +
                        //{"toClass":"OrganizationService","toIdentifier":70000},
                        "'applicationDate':null," +//null,
                        "'escortOrganizationId':null," +
                        //{"toClass":"OrganizationService","toIdentifier":80000},
                        "'reportingDate':null }",//null"'movementIdentification':null," +

                        "{'movementCategory':'EXTERNAL'," +
                                "'movementType':'CRT'," +
                                "'movementDirection':'IN'," +
                                "'movementStatus':'PENDING'," +
                                "'movementReason':'LA'," +
                                // "'movementOutcome':{'set':'MovementOutcome','code':'EXTERNAL'}," +
                                "'movementDate':null," +
                                "'movementText':null," +
                                "'movementText':null," +
                                "'movementText':null," +
                                "'approvedByStaffId':null," +
                                "'approvalDate':null," +
                                "'approvalComments':null," +

                                "'associations':[]," +
                                "'facilities':[]," +
                                "'dataPrivileges':[]}", context.getAttribute(IdType.ACTID.value() + 1), context.getAttribute(IdType.SUPID.value() + 0) },

                { "TC 00-03", "{'fromLocationId':10000," +//{"toClass":"LocationService","toIdentifier":10000},
                        "'fromOrganizationId':30000," +//{"toClass":"OrganizationService","toIdentifier":30000},
                        "'fromFacilityId': 20000," +//{"toClass":"FacilityService","toIdentifier":20000},
                        "'fromCity':null," +//"Vancouver",
                        "'toFacilityInternalLocationId':null," +
                        "'toLocationId':40000," +//{"toClass":"LocationService","toIdentifier":40000},
                        "'toOrganizationId':60000," +//{"toClass":"OrganizationService","toIdentifier":60000},
                        "'toFacilityId':1," +//{"toClass":"FacilityService","toIdentifier":50000},
                        "'toCity':null," +//"Richmond",
                        "'toProvinceState':null," +//"BC",
                        "'arrestOrganizationId' :70000," +
                        //{"toClass":"OrganizationService","toIdentifier":70000},
                        "'applicationDate':null," +//null,
                        "'escortOrganizationId':80000," +
                        //{"toClass":"OrganizationService","toIdentifier":80000},
                        "'reportingDate':null }",//null"'movementIdentification':null," +

                        "{'movementCategory':'EXTERNAL'," +
                                "'movementType':'CRT'," +
                                "'movementDirection':'OUT'," +
                                "'movementStatus':'PENDING'," +
                                "'movementReason':'LA'," +
                                // "'movementOutcome':{'set':'MovementOutcome','code':'EXTERNAL'}," +
                                "'approvedByStaffId':null," +
                                "'approvalDate':null," +
                                "'approvalComments':null," +
                                "'associations':[]," +
                                "'facilities':[]," +
                                "'dataPrivileges':[]}", context.getAttribute(IdType.ACTID.value() + 3), context.getAttribute(IdType.SUPID.value() + 1) },

        };
        return (retObjArr);
    }

    //@Test (dependsOnMethods = "buildAssociationSet", dataProvider = "NewFacilityPositive")
    @Test(dataProvider = "NewCautionPositive")
    public void testCreateExternalMovementActivityPositive(String tc, String cautionString, String string, Long actId, Long supId) {
        LOG.info("===== FROM CREATE Movement Activity=======");
        //	MovementActivityType caution_1 = new MovementActivityType();
        //	LOG.trace(gson.toJson(caution_1));//this return empty set of caution type string...

        LOG.info("Testing function: create(UserContext, MovementActivityType)");
        LOG.info("\nRunning TC = " + tc);

        ExternalMovementActivityType ema = new ExternalMovementActivityType();
        ExternalMovementActivityType emaRet = new ExternalMovementActivityType();

        MovementActivityType mov = gson.fromJson(string, MovementActivityType.class);
        mov.setSupervisionId(supId);
        mov.setActivityId(actId);
        //LOG.trace(mov);
        LOG.trace(gson.toJson(mov));
        ExternalMovementActivityType ext = gson.fromJson(cautionString, ExternalMovementActivityType.class);
        //LOG.trace(ext);

        ema.setActivityId(mov.getActivityId());
        ema.setApprovalComments(mov.getApprovalComments());
        ema.setApprovalDate(mov.getApprovalDate());
        ema.setApprovedByStaffId(mov.getApprovedByStaffId());
        ema.setCommentText(mov.getCommentText());
        ema.setMovementCategory(mov.getMovementCategory());
        ema.setMovementDate(mov.getMovementDate());
        ema.setMovementDirection(mov.getMovementDirection());
        ema.setMovementIdentification(mov.getMovementId());
        ema.setMovementOutcome(mov.getMovementOutcome());
        ema.setMovementReason(mov.getMovementReason());
        ema.setMovementStatus(mov.getMovementStatus());
        ema.setMovementType(mov.getMovementType());
        ema.setSupervisionId(mov.getSupervisionId());

        ema.setFromFacilityId(ext.getFromFacilityId());
        ema.setFromOrganizationId(ext.getFromOrganizationId());
        ema.setFromLocationId(ext.getFromLocationId());
        ema.setFromCity(ext.getFromCity());
        ema.setToCity(ext.getToCity());
        ema.setToFacilityId(ext.getToFacilityId());
        ema.setToFacilityInternalLocationId(ext.getToFacilityInternalLocationId());
        ema.setToLocationId(ext.getToLocationId());
        ema.setToOrganizationId(ext.getToOrganizationId());
        ema.setToProvinceState(ext.getToProvinceState());
        ema.setArrestOrganizationId(ext.getArrestOrganizationId());
        ema.setApplicationDate(ext.getApplicationDate());
        ema.setEscortOrganizationId(ext.getEscortOrganizationId());
        ema.setEscortDetails(ext.getEscortDetails());
        ema.setReportingDate(ext.getReportingDate());

        LOG.trace("!!!!=" + gson.toJson(ema));

        emaRet = (ExternalMovementActivityType) service.create(uc, ema, Boolean.FALSE);
        //LOG.trace(emaRet);
        LOG.trace(gson.toJson(emaRet));

        //LOG.trace("Movement ID_1 ="+emaRet.getExternalMovementActivity().getMovementId());

        Assert.assertNotNull(emaRet);

        ExternalMovementActivityType extMov = emaRet;
        extMov.setMovementStatus("COMPLETED");
        extMov.setMovementOutcome("SICK");

        Set<CommentType> comments = new HashSet<CommentType>();
        comments.add(new CommentType(null, new Date(), "comment1"));
        extMov.setCommentText(comments);

        Calendar cal = Calendar.getInstance();
        cal.set(2012, 8, date, 1, 6, 9);
        date++;
        extMov.setMovementDate(cal.getTime());

        Assert.assertNotNull(service.update(uc, extMov));

        extMovIds.add(extMov.getMovementId());

        LOG.info("=========================");
    }

    @AfterClass
    public void setContext(ITestContext context) {
        for (int i = 0; i < extMovIds.size(); i++) {
            Long id = extMovIds.get(i);
            context.setAttribute(IdType.EXTID.value() + i, id);
        }
    }

}

