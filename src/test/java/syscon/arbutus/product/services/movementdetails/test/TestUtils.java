package syscon.arbutus.product.services.movementdetails.test;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestUtils {

    private static final Logger LOG = LoggerFactory.getLogger(TestUtils.class);

    /**
     * Converts test case number in "99-99" format to integer.
     * For example "01-01" returns 101, "10-10" returns 1010.
     *
     * @param tc test case number in "99-99" format
     * @return converted to int test case number.
     * if tc format is incorrect, this method logs an error and shuts down JVM.
     */
    public static int convTcToInt(String tc) {

        int tcInt = 0;
        int tcInt2 = 0;

        try {
            if ((tc.length() == 5) && (tc.substring(2, 3).equals("-"))) {
                tcInt = Integer.valueOf(tc.substring(0, 2));
                tcInt = tcInt * 100;
                tcInt2 = Integer.valueOf(tc.substring(3, 5));
                tcInt += tcInt2;
            }
        } catch (Exception e) {
            tcInt = 0;
            LOG.error("convTcToInt(): incorrect tc format: " + tc);
            LOG.info("Shutting down. Goodbye.");
            System.exit(100);
        }

        return tcInt;
    }

    /**
     * Compares two Date objects, year + month + day only without time. Both or one of them can be null.
     *
     * @param act
     * @param exp
     * @return true if year+month+day in both dates is the same or both are null.
     * false otherwise
     */
    public static boolean areYYYYMMDDEqual(Date act, Date exp) {
        boolean result = false;

        if (act == null && exp == null) {
            result = true;
        } else if (act != null && exp != null) {
            if (convDateToString(act).equals(convDateToString(exp))) {
                result = true;
            }
        }

        return result;
    }

    /**
     * Compares two Date objects including their time. Both or one of them can be null.
     *
     * @param act
     * @param exp
     * @return true if both dates is the same or both are null.
     * false otherwise
     */
    public static boolean areDatesEqual(Date act, Date exp) {
        int delta = 0;
        return areDatesEqual(act, exp, delta);
    }

    public static boolean areDatesEqual(Date act, Date exp, int delta) {
        boolean result = false;

        if (act == null && exp == null) {
            result = true;
        } else if (act != null && exp != null) {
            if (Math.abs(act.getTime() - exp.getTime()) <= delta) {
                result = true;
            }
        }

        return result;
    }

    /**
     * Compares two sets of strings. Order of strings in both sets does not need to be the same.
     *
     * @param set1
     * @param set2
     * @return true if both sets have the same elements, both are empty or both are null
     * false otherwise
     */
    public static boolean areSetsEqual(Set<String> set1, Set<String> set2) {
        boolean result = false;
        boolean strNotFound = false;
        Set<String> set2copy = null;

        // return true if both sets are null or sizes = 0
        if (set1 == null && set2 == null) {
			return true;
		}

        if ((set1 != null && set1.size() == 0) && (set2 != null && set2.size() == 0)) {
			return true;
		}

        // if only one of them is null, return false
        if ((set1 == null && set2 != null) || (set1 != null && set2 == null)) {
			return false;
		}

        if (set1 != null && set2 != null && set1.size() == set2.size()) {
            // make a copy of set2 as we will change it
            set2copy = new HashSet<String>();
            for (String s : set2) {
                set2copy.add(s);
            }

            // now compare objects
            for (String s : set1) {
                if (set2copy.contains(s)) {
                    set2copy.remove(s);
                } else {
                    //string from set1 not found in set2, sets not the same
                    strNotFound = true;
                    break;
                }
            }

            if (strNotFound == false) {
                // all elements of set1 found and removed from set2copy
                // set2copy should be empty if both sets had the same elements
                if (set2copy.size() == 0) {
					result = true;
				}
            }
        }

        return result;
    }

    /**
     * Returns null if input string is null or converted to lower case equals "null"
     * otherwise returns unchanged input string
     *
     * @param input
     * @return
     */
    public static String stringToStringOrNull(String input) {
        String result = null;

        if (input != null && !input.toLowerCase().equals("null")) {
			result = input;
		}
        return result;
    }

    /**
     * Converts String into set of substrings. A seperator character is passed in the second parameter.
     * For example "a:b:c" can be converted into a set of strings, separator = ":".
     *
     * @param string
     * @param separator
     * @return Set<String> or null when text is one of the following: null, "", text.toLowerCase().equals("null")
     */
    public static Set<String> convStringToSet(String text, String separator) {
        Set<String> result = null;

        if (text != null && !text.isEmpty() && !text.toLowerCase().equals("null")) {
            result = new HashSet<String>();

            String[] arr = text.split(separator);
            for (int i = 0; i < arr.length; i++) {
                result.add(arr[i]);
            }
        }

        return result;
    }

    /**
     * Converts String to Date.<p>
     * The input String can have the following values and formats:<br/>
     * null, "null", "today", "todayT", "todayTHH:mm:ss" <p>
     * "tomorrow", "future-year", "future-month",<br/>
     * "tomorrowT", "future-yearT", "future-monthT", <br/>
     * "tomorrowTHH:mm:ss", "future-yearTHH:mm:ss", "future-monthTHH:mm:ss", <p>
     * "yesterday", "past-year", "past-month", <br/>
     * "yesterdayT", "past-yearT", "past-monthT", <br/>
     * "yesterdayTHH:mm:ss", "past-yearTHH:mm:ss", "past-monthTHH:mm:ss", <p>
     * "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-ddTHH:mm:ss"<p>
     * For "tomorrow" for example: <br/>
     * "tomorrow" - returns tomorrow with time 00:00:00<br/>
     * "tomorrowt" - returns tomorrow with current time<br/>
     * "tomorrowt10:15:20" - returns tomorrow date with supplied time<p>
     * other examples: "2012-05-26", "2012-05-26 13:15:10"<p>
     *
     * @param sDate
     * @return if sDate = null, returns null<br/>
     * if toLowerCase().equals("null") - returns null<br/>
     * if toLowerCase().equals("today") - returns current date with time 00:00:00<br/>
     * if toLowerCase().equals("todayT") - returns current date and time<br/>
     * if toLowerCase().equals("todayT99:99:99") - returns current date with supplied time<p>
     * if toLowerCase().equals("future-year") - returns one year ahead with time 00:00:00<br/>
     * if toLowerCase().equals("future-yearT") - returns one year ahead with current time<br/>
     * if toLowerCase().equals("future-yearT99:99:99") - returns one year ahead with supplied time<p>
     * if toLowerCase().equals("future-month") - returns one month ahead with time 00:00:00<br/>
     * if toLowerCase().equals("future-monthT") - returns one month ahead with current time<br/>
     * if toLowerCase().equals("future-monthT99:99:99") - returns one month ahead with supplied time<p>
     * if toLowerCase().equals("yesterday") - returns yesterday with time 00:00:00<br/>
     * if toLowerCase().equals("yesterdayT") - returns yesterday with current time<br/>
     * if toLowerCase().equals("yesterdayt99:99:99") - returns yesterday with supplied time<p>
     * if toLowerCase().equals("tomorrow") - returns tomorrow with time 00:00:00<br/>
     * if toLowerCase().equals("tomorrowT") - returns tomorrow with current time<br/>
     * if toLowerCase().equals("tomorrowT99:99:99") - returns tomorrow with supplied time<p>
     * if toLowerCase().equals("past-year") - returns one year back with time 00:00:00<br/>
     * if toLowerCase().equals("past-yearT") - returns one year back with current time<br/>
     * if toLowerCase().equals("past-yearT99:99:99") - returns one year back with supplied time<p>
     * if toLowerCase().equals("past-month") - returns one month back with time 00:00:00<br/>
     * if toLowerCase().equals("past-monthT") - returns one month back with current time<br/>
     * if toLowerCase().equals("past-monthT99:99:99") - returns one month back with supplied time<p>
     * if "yyyy-MM-dd" - returns supplied date with time set to 00:00:00<br/>
     * if "yyyy-MM-dd HH:mm:ss" - returns supplied date and time<br/>
     * if "yyyy-MM-ddTHH:mm:ss" - returns supplied date and time, as above<br/>
     * if "yyyy-MM-ddT" - returns supplied date and current time<br/>
     * if sDate is in wrong format, this method logs an error message end shuts down JVM<br/>
     */
    public static Date convStringToDate(String sDate) {

        String dateFormat = "yyyy-MM-dd";
        String dateTimeFormat = "yyyy-MM-dd HH:mm:ss";

        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        SimpleDateFormat sdtf = new SimpleDateFormat(dateTimeFormat);

        String sTempDate = null;
        Date date = null;

        if (stringToStringOrNull(sDate) == null) {
			return null;
		}

        if (sDate.toLowerCase().equals("today")) {
            sTempDate = sdf.format(new Date());
        }

        if (sDate.toLowerCase().startsWith("todayt") && sTempDate == null) {
            date = new Date();
            if (sDate.length() == "todayt".length()) {
				sTempDate = sdtf.format(date);
			} else {
				sTempDate = sdf.format(date) + " " + sDate.substring("todayt".length());
			}
        }

        if (sDate.toLowerCase().equals("future-year") && sTempDate == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.YEAR, 1);
            date = cal.getTime();
            sTempDate = sdf.format(date);
        }

        if (sDate.toLowerCase().startsWith("future-yeart") && sTempDate == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.YEAR, 1);
            date = cal.getTime();
            if (sDate.length() == "future-yeart".length()) {
				sTempDate = sdtf.format(date);
			} else {
				sTempDate = sdf.format(date) + " " + sDate.substring("future-yeart".length());
			}
        }

        if (sDate.toLowerCase().equals("future-month") && sTempDate == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.MONTH, 1);
            date = cal.getTime();
            sTempDate = sdf.format(date);
        }

        if (sDate.toLowerCase().startsWith("future-montht") && sTempDate == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.MONTH, 1);
            date = cal.getTime();
            if (sDate.length() == "future-montht".length()) {
				sTempDate = sdtf.format(date);
			} else {
				sTempDate = sdf.format(date) + " " + sDate.substring("future-montht".length());
			}
        }

        if (sDate.toLowerCase().equals("tomorrow") && sTempDate == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.DATE, 1);
            date = cal.getTime();
            sTempDate = sdf.format(date);
        }

        if (sDate.toLowerCase().startsWith("tomorrowt") && sTempDate == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.DATE, 1);
            date = cal.getTime();
            if (sDate.length() == "tomorrowt".length()) {
				sTempDate = sdtf.format(date);
			} else {
				sTempDate = sdf.format(date) + " " + sDate.substring("tomorrowt".length());
			}
        }

        if (sDate.toLowerCase().equals("yesterday") && sTempDate == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.DATE, -1);
            date = cal.getTime();
            sTempDate = sdf.format(date);
        }

        if (sDate.toLowerCase().startsWith("yesterdayt") && sTempDate == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.DATE, -1);
            date = cal.getTime();
            if (sDate.length() == "yesterdayt".length()) {
				sTempDate = sdtf.format(date);
			} else {
				sTempDate = sdf.format(date) + " " + sDate.substring("yesterdayt".length());
			}
        }

        if (sDate.toLowerCase().equals("past-year") && sTempDate == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.YEAR, -1);
            date = cal.getTime();
            sTempDate = sdf.format(date);
        }

        if (sDate.toLowerCase().startsWith("past-yeart") && sTempDate == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.YEAR, -1);
            date = cal.getTime();
            if (sDate.length() == "past-yeart".length()) {
				sTempDate = sdtf.format(date);
			} else {
				sTempDate = sdf.format(date) + " " + sDate.substring("past-yeart".length());
			}
        }

        if (sDate.toLowerCase().equals("past-month") && sTempDate == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.MONTH, -1);
            date = cal.getTime();
            sTempDate = sdf.format(date);
        }

        if (sDate.toLowerCase().startsWith("past-montht") && sTempDate == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.MONTH, -1);
            date = cal.getTime();
            if (sDate.length() == "past-montht".length()) {
				sTempDate = sdtf.format(date);
			} else {
				sTempDate = sdf.format(date) + " " + sDate.substring("past-montht".length());
			}
        }

        // nothing above worked, let try to parse original parameter
        if (sTempDate == null) {
            sTempDate = sDate.replace('T', ' ');
        }

        try {
            if (sTempDate.length() == dateFormat.length()) {
                date = sdf.parse(sTempDate);
            } else {
                date = sdtf.parse(sTempDate);
            }
        } catch (Exception e) {
            LOG.error("convStringToDate(): " + sDate + " is in wrong format " + e.getMessage());
            LOG.info("Shutting down. Goodbye.");
            System.exit(100);
        }

        return date;
    }

    public static String convDateToString(Date DOB) {
        //System.out.println("DOB(date) is : " + DOB.toString());
        if (DOB == null) {
			return null;
		}

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String DOB_str = sdf.format(DOB);
        //System.out.println("DOB as string :" + DOB_str);
        return DOB_str;
    }

    public static String convDateTimeToString(Date DOB) {
        //System.out.println("DOB(date) is : " + DOB.toString());
        if (DOB == null) {
			return null;
		}

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String DOB_str = sdf.format(DOB);
        //System.out.println("DOB as string :" + DOB_str);
        return DOB_str;
    }

    /**
     * removes time portion from the date.
     *
     * @param date
     * @return
     */
    public static Date removeTimeFromDate(Date date) {
        return convStringToDate(convDateToString(date));
    }

	/*public static boolean ObjectsEqual (Object objA, Object objB) {

		boolean result = false;		
		//their hashes have to be ==
		
		// second way
		// verification objects field-by-field
		 
		//System.out.println("objA type is :" + objA.getClass().getName());
		//System.out.println("objB type is :" + objB.getClass().getName());
		
		//Class clsA = objA.getClass();
		//Class clsB = objB.getClass();
		Gson gson = new Gson();
		if (gson.toJson(objA)==gson.toJson(objB)){
			result = true;
	    } else {
	    	result = false;
	      }
	    
		
		// By obtaining a list of all declared fields.
		Field[] fieldsA = clsA.getDeclaredFields();
		Field[] fieldsb = clsB.getDeclaredFields();
		Method[] methodsA = clsA.getDeclaredMethods();

		// By obtaining a list of all public fields, both declared and inherited.
		
		for (int i=0; i<fieldsA.length; i++) {
			System.out.println("objA fields [" + i + "]" + fieldsA[i]);
		}
		for (int i=0; i<methodsA.length; i++) {
			System.out.println("objA methods [" + i + "]" + methodsA[i]);
		}
		try {
		for (int i=0; i<fieldsA.length; i++) {LOG
		    System.out.println("objA field value [" + i + "]" + (clsA.getField(fieldsA[i].toString())).get(objA));
		}
		} catch (IllegalAccessException e) {
		      System.out.println(e);
		} catch (NoSuchFieldException e) {
		      System.out.println(e);
		}
				
		return result;
		
	}*/

    /**
     * This method is used to replace 'QAUtils.convStringToDate(arg0)' with a corresponding date string inside a json string.<br/>
     * The QAUtils.convStringToDate(\"parameter\") in json string should be used without any quotation marks; <p>
     * Example: "{'sampledate': QAUtils.convStringToDate(\"today\"),'facilityDeactivateDate':QAUtils.convStringToDate(\"2012-01-01T23:12:09\"),'otherDate':QAUtils.convStringToDate(\"tomorrowT15:10:25\")}";<p>
     * Gson can accept dates in different format. It can be set by using for example:<p>
     * Gson gson = new GsonBuilder().setDateFormat( "yyyy-MM-dd HH:mm:ss" ).create();<p>
     * To accommodate the above, the same date format has to be passed to this method as a second parameter.</br>
     * If null is passed in second parameter, the default gson format is used.<p>
     *
     * @param String: A whole json string with or without containing QAUtils.convStringToDate(arg0) statement.
     * @param String: date format
     * @return String: a json string with parsed date string
     */
    public static String parseJsonString(String gsonString, String format) {

        String prefix = "QAUtils";
        String noPrefix = null;//gsonString.substring(gsonString.indexOf(prefix));
        String[] tokens = null; //noPrefix.split(",");
        String parsedStr = null;
        String result = null;
        String method = null;
        String parameter = null;
        String dateFormat = null;

        if (gsonString == null || gsonString.length() == 0) {
			return gsonString;
		}

        // establish date format to use
        if (format == null || format.length() == 0) {
            // use default gson date format
            dateFormat = "MMM d, yyyy HH:mm:ss a";
        } else {
            dateFormat = format;
        }

        while (gsonString.contains(prefix)) {
            noPrefix = gsonString.substring(gsonString.indexOf(prefix));
            tokens = noPrefix.split(",");
            for (String t : tokens) {
                parsedStr = t.replace("}", "");
                method = parsedStr.substring(parsedStr.indexOf(".") + 1, parsedStr.indexOf("("));
                parameter = parsedStr.substring(parsedStr.indexOf("\"") + 1, parsedStr.lastIndexOf("\""));
                break;
            }
            try {
                if (method.equalsIgnoreCase("convStringToDate")) {
                    Method m = TestUtils.class.getDeclaredMethod(method, String.class);
                    m.setAccessible(true);
                    Object o = m.invoke(null, parameter);

                    // convert date to required format
                    // result ="'"+ o.toString() +"'";
                    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                    result = "'" + sdf.format((Date) o) + "'";
                    parsedStr = parsedStr.replace("]", "");
                    gsonString = gsonString.replace(parsedStr, result);
                } else {
                    // gsonString =" Error! The wrong function name: "+ method;
                    // break;
                    LOG.error("unsupported QAUtils method name: " + method);
                    LOG.info("Shutting down. Goodbye.");
                    System.exit(100);
                }
            } catch (Exception e) {
                LOG.error("parseJsonString() exception = " + e.getMessage());
                LOG.info("Shutting down. Goodbye.");
                System.exit(100);
            }
        }
        return gsonString;
    }

    /**
     * This method does the same what parseJsonString(String gsonString, String format ) does</br>
     * It replaces QAUtils.convStringToDate() with a date in default gson format.
     *
     * @param gsonString
     * @return String parsed gsonString, QAUtils.convStringToDate() replaced with a generated date
     * in default gson format
     */
    public static String parseJsonString(String gsonString) {
        return parseJsonString(gsonString, null);
    }

}
