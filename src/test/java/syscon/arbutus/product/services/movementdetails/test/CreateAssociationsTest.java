package syscon.arbutus.product.services.movementdetails.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;

@Test(groups = "associations", dependsOnGroups = { "supervision", "activity", "external", "internal" })
public class CreateAssociationsTest extends SecureBase {
    private static final Logger LOG = LoggerFactory.getLogger(CreateAssociationsTest.class);

    private ActivityService actService;

    //private MovementService movService;
    //private SupervisionService supService;
    //private HousingBedManagementActivityService houService;

    @Parameters({ "hostNamePortNumber" })
    @BeforeClass
    public void beforeTest(@Optional("localhost:1099") String hostNamePortNumber) {
        LOG.info("-----beforeTest is running-------");
        actService = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        uc = super.initUserContext();

    	/*serviceName = SUPERVISION_SERVICE;    	
        Assert.assertTrue( lookUpJNDI(), "lookUpJNDI() failed." );
    	supService = (SupervisionService) service1;
    	
    	serviceName = MOVEMENT_ACTIVITY_SERVICE;    	
    	Assert.assertTrue( lookUpJNDI(), "lookUpJNDI() failed." );
    	movService = (MovementService) service1;*/
    	
    	/*serviceName = HOUSING_ACTIVITY_SERVICE;    	
    	Assert.assertTrue( lookUpJNDI(), "lookUpJNDI() failed." );
    	houService = (HousingBedManagementActivityService) service1;*/

    }

    @DataProvider(name = "ActToSup")
    public Object[][] associateActToSup(ITestContext context) {
        Object[][] retObjArr = { { "01", context.getAttribute("actId0"), context.getAttribute("supId0") },
                { "02", context.getAttribute("actId1"), context.getAttribute("supId0") }, { "03", context.getAttribute("actId2"), context.getAttribute("supId0") },

                { "04", context.getAttribute("actId3"), context.getAttribute("supId1") }, { "05", context.getAttribute("actId4"), context.getAttribute("supId1") },
                { "06", context.getAttribute("actId5"), context.getAttribute("supId1") }, { "07", context.getAttribute("actId6"), context.getAttribute("supId1") },

        };
        return retObjArr;
    }

    @DataProvider(name = "SupToAct")
    public Object[][] associateSupToAct(ITestContext context) {
        Object[][] retObjArr = {
				/*{"01", context.getAttribute("supId0"), context.getAttribute("actId0")},
				{"02", context.getAttribute("supId0"), context.getAttribute("actId1")},
				{"03", context.getAttribute("supId0"), context.getAttribute("actId2")},
				
				{"04", context.getAttribute("supId1"), context.getAttribute("actId3")},
				{"05", context.getAttribute("supId1"), context.getAttribute("actId4")},
				{"06", context.getAttribute("supId1"), context.getAttribute("actId5")},
				{"07", context.getAttribute("supId1"), context.getAttribute("actId6")},*/

        };
        return retObjArr;
    }

    @DataProvider(name = "ActToMov")
    public Object[][] associateActToMov(ITestContext context) {
        Object[][] retObjArr = { { "01", context.getAttribute("actId0"), context.getAttribute("extId0") },
                { "02", context.getAttribute("actId1"), context.getAttribute("extId1") },

                { "03", context.getAttribute("actId3"), context.getAttribute("extId2") }, { "04", context.getAttribute("actId4"), context.getAttribute("intId0") },
                { "05", context.getAttribute("actId5"), context.getAttribute("intId1") }, { "06", context.getAttribute("actId6"), context.getAttribute("intId2") },

        };
        return retObjArr;
    }

    @DataProvider(name = "MovToAct")
    public Object[][] associateMovToAct(ITestContext context) {
        Object[][] retObjArr = {
				/*{"01", context.getAttribute("extId0"), context.getAttribute("actId0")},
				{"02", context.getAttribute("extId1"), context.getAttribute("actId1")},				
				
				{"03", context.getAttribute("extId2"), context.getAttribute("actId3")},
				{"04", context.getAttribute("intId0"), context.getAttribute("actId4")},
				{"05", context.getAttribute("intId1"), context.getAttribute("actId5")},*/

        };
        return retObjArr;
    }

    @Test(dataProvider = "ActToSup")
    public void createActToSupAssoc(String caseNum, Long actId, Long supId) {
        LOG.info("=========================");
        LOG.info("Create Activity To Sup Assoc");

        // Create ActivityType
        ActivityType act = actService.get(uc, actId);

        act.setSupervisionId(supId);

        assert (actService.update(uc, act, null, false) != null);

        LOG.info("Create Activity To Sup Assoc success.");

    }

    @Test(dataProvider = "SupToAct")
    public void createSupToActAssoc(String caseNum, Long supId, Long actId) {
		/*LOG.info("=========================");
		LOG.info("Create Sup to Activity Assoc");
		
		// Create ActivityType
		SupervisionType sup = supService.get(uc, supId).getSupervision();
		
		sup.getAssociations().add(new syscon.arbutus.product.services.supervision.contract.dto.AssociationType(syscon.arbutus.product.services.supervision.contract.dto.AssociationToClass.ACTIVITY.value(), actId));
		
		Assert.assertEquals(supService.update(uc, sup).getReturnCode(), SUCCESS);		
		
		LOG.info("Create Sup to Activity Assoc success.");*/

    }

    @Test(dataProvider = "ActToMov")
    public void createActToMovAssoc(String caseNum, Long actId, Long movId) {
        LOG.info("=========================");
        LOG.info("Create Activity To Mov Assoc");

        // Create ActivityType
        //ActivityType act = actService.get(uc, actId).getActivity();
        //act.getAssociations().add(new AssociationType(syscon.arbutus.product.services.activity.contract.dto.AssociationToClass.MOVEMENT.value(), movId));
        //Assert.assertEquals(actService.update(uc, act, null, false).getReturnCode(), SUCCESS);

        //		AssociationType association = new AssociationType(syscon.arbutus.product.services.activity.contract.dto.AssociationToClass.MOVEMENT.value(), movId);
        //		AssociationReturnType ret = actService.createAssociation(uc, actId, association);
        //		Assert.assertEquals(ret.getReturnCode(), SUCCESS);
        //
        //		LOG.info("Create Activity To Mov Assoc success.");

    }

    @Test(dataProvider = "MovToAct")
    public void createMovToActAssoc(String caseNum, Long movId, Long actId) {
        LOG.info("=========================");
        LOG.info("Create Movement to Activity Assoc");
		
		/*// Create ActivityType
		MovementActivityType mov = movService.get(uc, movId).getMovementActivity();
		
		Set<syscon.arbutus.product.services.movementactivity.contract.dto.AssociationType> assocations  = mov.getAssociations();
		
		for (syscon.arbutus.product.services.movementactivity.contract.dto.AssociationType assoc : assocations){
			if(AssociationToClass.ACTIVITY.value().equalsIgnoreCase(assoc.getToClass()))
				assoc.setToIdentifier(actId);
		}
		
		Assert.assertEquals(movService.update(uc, mov).getReturnCode(), SUCCESS);		
		
		LOG.info("Create Movement to Activity Assoc success.");	*/

    }

}
