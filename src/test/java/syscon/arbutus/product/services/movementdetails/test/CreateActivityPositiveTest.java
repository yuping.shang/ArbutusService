package syscon.arbutus.product.services.movementdetails.test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.activity.contract.dto.ScheduleTimeslotType;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;

@Test(groups = "activity", dependsOnGroups = "supervision")
public class CreateActivityPositiveTest extends SecureBase {
    private static final Logger LOG = LoggerFactory.getLogger(CreateActivityPositiveTest.class);

    private ActivityService service;
    private List<Long> actIds = new ArrayList<Long>();

    @Parameters({ "hostNamePortNumber" })
    @BeforeClass
    public void beforeTest(@Optional("localhost:1099") String hostNamePortNumber) {
        LOG.info("-----beforeTest is running-------");

        service = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        uc = super.initUserContext();
    }

    /**
     * The reset method calls the service's reset() method to initialize all reference code/links data of the service to default values in DB.
     */
    @BeforeClass(dependsOnMethods = { "beforeTest" })
    public void reset() {

        //		ReferenceCodesReturnType rtn = service.getAllReferenceCodes(uc);
        //		if(rtn.getReturnCode().equals(SUCCESS) && rtn.getReferenceCodes() != null && rtn.getReferenceCodes().size() > 0){
        //			//We can retreive reference code back, this assumes the service has been reset already.
        //		}else{
        //			LOG.info("Reset...");
        //			Long returnCode = service.reset(uc);
        //			Assert.assertEquals( returnCode, SUCCESS );
        //			LOG.info("Reset successful.");
        //		}
    }

    @DataProvider(name = "Activity")
    public Object[][] createActivity(ITestContext context) {
        Object[][] retObjArr = {
                // ActivityID(N 1-1), ActiveStatusFlag(B 1-1), PlannedStartDate(DT 0-1), PlannedEndDate(DT 0-1), ActivityCategory(Set 1-1),
                // ActivityAntecendentClass(String), ActivityAntecendentReference(N 0-1), ActivityDescentantsClass(String), ActivityDescentantsReference(N 0-*),
                // ScheduleTypeID(N 0-1), FacilityAssociation(N 1-1), ReturnCode, ScheduleTypeID(DT 1-1), TimeslotStartDateTime(DT 1-1), TimeslotEndDateTime(DT 0-1), OverrideCapacity(B 1-1)

                // Case 1:
                // Timeslot (ScheduleTimeslotType) is provided. Both TimeslotStartDateTime and TimeslotEndDateTime should be provided.
                // PlannedStartDate and PlannedEndDate in ActivityType will be ignored in this case.
                // Category is derived based on ScheduleTimeslotType.scheduleTypeID.Category and overrides ActivityType.Category
                //{"01", "11111", "true", "null", "null", "MOVEMENT", "null", "null", "null", "null", "null", "1", "1", Utils.getAnyScheduleID("ASSESSMENT", true).toString(), "2012-06-07 15:00:00", "2012-12-31 17:00:00", "false"},

                // Case 2:
                // Timeslot (ScheduleTimeslotType) is not provided. In this case PlannedStartDate and PlannedEndDate in ActivityType should be specified
                { "02", "11111", "true", "2012-09-11 11:00:00", "2013-12-31 11:30:00", "MOVEMENT", "null", "null", "null", "null", "null", "1", "1", "null", "null",
                        "null", "false" },
                { "02", "11111", "true", "2012-09-11 11:00:00", "2013-12-31 11:30:00", "MOVEMENT", "null", "null", "null", "null", "null", "1", "1", "null", "null",
                        "null", "false" },
                { "02", "11111", "true", "2012-09-11 11:00:00", "2013-12-31 11:30:00", "VISITATION", "null", "null", "null", "null", "null", "1", "1", "null", "null",
                        "null", "false" },

                // Case 3:
                // Try to override Capacity
                { "03", "11111", "true", "2012-09-11 11:00:00", "2013-12-31 11:30:00", "MOVEMENT", "null", "null", "null", "null", "null", "1", "1", "null", "null",
                        "null", "false" },
                { "03", "11111", "true", "2012-09-11 11:00:00", "2013-12-31 11:30:00", "MOVEMENT", "null", "null", "null", "null", "null", "1", "1", "null", "null",
                        "null", "false" },
                { "03", "11111", "true", "2012-09-11 11:00:00", "2013-12-31 11:30:00", "MOVEMENT", "null", "null", "null", "null", "null", "1", "1", "null", "null",
                        "null", "false" },
                { "03", "11111", "true", "2012-09-11 11:00:00", "2013-12-31 11:30:00", "MOVEMENT", "null", "null", "null", "null", "null", "1", "1", "null", "null",
                        "null", "true" },
                // repeat with ScheduleTimeslot. Override = true
                //{"01", "11111", "true", "null", "null", "MOVEMENT", "null", "null", "null", "null", "null", "1", "1", Utils.getAnyScheduleID("DISCIPLINE", false).toString(), "2012-06-07 15:00:00", "2012-12-31 17:00:00", "true"},
        };
        return retObjArr;
    }

    @Test(dataProvider = "Activity")
    public void testCreateActivityWithSchedulePositive(String caseNum, String actID, String actStatus, String actPlanStart, String actPlanEnd, String actCategory,
            String actAntClass, String actAntRef, String actDecClass, String actDecRef, String scheduleID, String facAssociation, String returnCode, String schTypeID,
            String timeslotStartDT, String timeslotEndDT, String overrideCapacity) {
        LOG.info("=========================");
        LOG.info("Testing function: createActivity(UserContext, ActivityType, ScheduleTimeslotType, Boolean)");

        // Create ActivityType
        ActivityType activityType = new ActivityType();
        activityType.setActivityIdentification(actID.equals("null") ? null : Long.valueOf(actID));
        activityType.setActiveStatusFlag(actStatus.equals("null") ? null : Boolean.valueOf(actStatus));
        activityType.setPlannedStartDate(actPlanStart.equals("null") ? null : TestUtils.convStringToDate(actPlanStart));
        activityType.setPlannedEndDate(actPlanEnd.equals("null") ? null : TestUtils.convStringToDate(actPlanEnd));
        if (!actCategory.equals("null")) {
            activityType.setActivityCategory(actCategory);
        }
        if (!actAntRef.equals("null")) {
            activityType.setActivityAntecedentId(Long.valueOf(actAntRef));
        }
        if (!actDecRef.equals("null")) {
            Set<Long> allDescendants = new HashSet<Long>();
            allDescendants.add(Long.valueOf(actDecRef));
            activityType.setActivityDescendantIds(allDescendants);
        }
        activityType.setScheduleID(scheduleID.equals("null") ? null : Long.valueOf(scheduleID));
        if (!facAssociation.equals("null")) {
            Set<Long> facilityAssociations = new HashSet<Long>();
            facilityAssociations.add(Long.valueOf(facAssociation));
            //activityType.setFacilities(facilityAssociations);
        }

        // Create ScheduleTimeslotType
        ScheduleTimeslotType timeslotType = new ScheduleTimeslotType();
        if (schTypeID.equals("null")) {
            timeslotType = null;
        } else {
            timeslotType.setScheduleID(schTypeID.equals("null") ? null : Long.valueOf(schTypeID));
            timeslotType.setTimeslotStartDateTime(timeslotStartDT.equals("null") ? null : TestUtils.convStringToDate(timeslotStartDT));
            timeslotType.setTimeslotEndDateTime(timeslotEndDT.equals("null") ? null : TestUtils.convStringToDate(timeslotEndDT));
            // Check the object
        }

        ActivityType returnType = service.create(uc, activityType, timeslotType, overrideCapacity.equals("null") ? null : Boolean.valueOf(overrideCapacity));
        Assert.assertNotNull(returnType, "ERROR! ActivityType is NULL...");

        // Verify created record
        ActivityType createdRecord = returnType;
        if (caseNum.equals("01")) {
            Assert.assertNotSame(createdRecord.getActivityIdentification(), activityType.getActivityIdentification(), "ERROR! ActivityID is incorrect...");
            Assert.assertEquals(createdRecord.getActiveStatusFlag(), activityType.getActiveStatusFlag(), "ERROR! ActivityStatusFlag is incorrect...");
            Assert.assertTrue(!TestUtils.areDatesEqual(createdRecord.getPlannedStartDate(), activityType.getPlannedStartDate()),
                    "ERROR! PlannedStartDate is incorrect...");
            Assert.assertTrue(!TestUtils.areDatesEqual(createdRecord.getPlannedEndDate(), activityType.getPlannedEndDate()), "ERROR! PlannedEndDate is incorrect...");
            Assert.assertTrue(!createdRecord.getActivityCategory().equals(activityType.getActivityCategory()), "ERROR! ActivityCategory is incorrect...");
            Assert.assertTrue(this.areTwoLongEqual(createdRecord.getActivityAntecedentId(), activityType.getActivityAntecedentId()),
                    "ERROR! AntcendentAssociation is incorrect...");
            Assert.assertTrue(this.areTwoAssociationSetsEqual(createdRecord.getActivityDescendantIds(), activityType.getActivityDescendantIds()),
                    "ERROR! DescendantsAssociation is incorrect...");
            Assert.assertNotSame(createdRecord.getScheduleID(), activityType.getScheduleID(), "ERROR! ScheduleTypeID is incorrect...");
        } else if (caseNum.equals("02")) {
            Assert.assertNotSame(createdRecord.getActivityIdentification(), activityType.getActivityIdentification(), "ERROR! ActivityID is incorrect...");
            Assert.assertEquals(createdRecord.getActiveStatusFlag(), activityType.getActiveStatusFlag(), "ERROR! ActivityStatusFlag is incorrect...");
            Assert.assertTrue(TestUtils.areDatesEqual(createdRecord.getPlannedStartDate(), activityType.getPlannedStartDate()),
                    "ERROR! PlannedStartDate is incorrect...");
            Assert.assertTrue(TestUtils.areDatesEqual(createdRecord.getPlannedEndDate(), activityType.getPlannedEndDate()), "ERROR! PlannedEndDate is incorrect...");
            Assert.assertTrue(createdRecord.getActivityCategory().equals(activityType.getActivityCategory()), "ERROR! ActivityCategory is incorrect...");
            Assert.assertTrue(this.areTwoLongEqual(createdRecord.getActivityAntecedentId(), activityType.getActivityAntecedentId()),
                    "ERROR! AntcendentAssociation is incorrect...");
            Assert.assertTrue(this.areTwoAssociationSetsEqual(createdRecord.getActivityDescendantIds(), activityType.getActivityDescendantIds()),
                    "ERROR! DescendantsAssociation is incorrect...");
            Assert.assertSame(createdRecord.getScheduleID(), activityType.getScheduleID(), "ERROR! ScheduleTypeID is incorrect...");
        }

        LOG.info("=========================");

        actIds.add(createdRecord.getActivityIdentification());

    }

    @AfterClass
    public void setContext(ITestContext context) {
        for (int i = 0; i < actIds.size(); i++) {
            Long id = actIds.get(i);
            context.setAttribute(IdType.ACTID.value() + i, id);
        }
    }

}
