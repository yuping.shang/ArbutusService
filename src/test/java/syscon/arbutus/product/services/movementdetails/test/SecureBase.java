package syscon.arbutus.product.services.movementdetails.test;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.Assert;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.DescriptionType;

/**
 * This is a base class. All other classes in this package extends from it.
 * Please check all "_TODO_" marks in Markers view and change them according to your service.
 */
public class SecureBase extends BaseIT {

    protected static final Long SUCCESS = Long.valueOf(1l);
    protected static final String ERROR = "error";

    protected static final String LANG_EN = "EN";
    protected static final String LANG_FR = "FR";
    protected static final String LANG_ES = "ES";

    /*
     * for testing maximum length
     */
    protected static final String LENGTH_64_EN = "abcdefghi0123456789012345678901234567890123456789012345678901234";
    protected static final String LENGTH_65_EN = "abcdefghi01234567890123456789012345678901234567890123456789012345";

    protected static final String LENGTH_128_EN =
            "abcdefghi0123456789012345678901234567890123456789012345678901234" + "abcdefghi0123456789012345678901234567890123456789012345678901234";

    protected static final String LENGTH_128_FR =
            "Allons enfants de la Patrie, Le jour de gloire est arrivé !     " + "abcdefghi0123456789012345678901234567890123456789012345678901234";

    protected static final String LENGTH_128_UTF_UKR =
            "Арбітр зупиняє матч і відправляє команди в роздягальню - стихія " + "розбушувалася, грім і блискавка, такого дощу ще не бачив ніхто! ";

    protected static final String LENGTH_128_ES =
            "Modifica la configuración controlar el contenido de las imágenes" + "abcdefghi0123456789012345678901234567890123456789012345678901234";

    protected static final String LENGTH_129_EN =
            "abcdefghi0123456789012345678901234567890123456789012345678901234" + "abcdefghi0123456789012345678901234567890123456789012345678901234" + "5";

    protected static final String LENGTH_512_EN =
            "abcdefghi0123456789012345678901234567890123456789012345678901234" + "abcdefghi0123456789012345678901234567890123456789012345678901234"
                    + "abcdefghi0123456789012345678901234567890123456789012345678901234" + "abcdefghi0123456789012345678901234567890123456789012345678901234";

    protected static final String LENGTH_513_EN =
            "abcdefghi0123456789012345678901234567890123456789012345678901234" + "abcdefghi0123456789012345678901234567890123456789012345678901234"
                    + "abcdefghi0123456789012345678901234567890123456789012345678901234" + "abcdefghi0123456789012345678901234567890123456789012345678901234" + "1";

    protected static final String LN_TEST_SEPERATOR = "------------------------------------------ ";

    protected static final Logger LOG = LoggerFactory.getLogger(SecureBase.class);

    protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").serializeNulls().create();

    /**
     * Assembles Set<DescriptionType>. If both codeX and descX are null, description is not added to the set.
     * It is for creating negative tests too.
     *
     * @param lang1
     * @param desc1
     * @param lang2
     * @param desc2
     * @param lang3
     * @param desc3
     * @return
     */
    protected static Set<DescriptionType> assembleDescriptionType(String lang1, String desc1, String lang2, String desc2, String lang3, String desc3) {

        Set<DescriptionType> descSet = new HashSet<DescriptionType>();

        //if (lang1==null && desc1==null && lang2==null && desc2==null && lang3==null & desc3==null)
        //	return descSet;

        //if ( TestUtils.stringToStringOrNull( lang1 ) != null || TestUtils.stringToStringOrNull( desc1 ) != null ) {
        if (!(lang1 == null && desc1 == null)) {
            DescriptionType dt1 = new DescriptionType();
            dt1.setLanguageCode(TestUtils.stringToStringOrNull(lang1));
            dt1.setDescription(TestUtils.stringToStringOrNull(desc1));
            descSet.add(dt1);
        }

        //if ( TestUtils.stringToStringOrNull( lang2 ) != null || TestUtils.stringToStringOrNull( desc2 ) != null ) {
        //if ( lang2 != null || desc2 != null ) {
        if (!(lang2 == null && desc2 == null)) {
            DescriptionType dt2 = new DescriptionType();
            dt2.setLanguageCode(TestUtils.stringToStringOrNull(lang2));
            dt2.setDescription(TestUtils.stringToStringOrNull(desc2));
            descSet.add(dt2);
        }

        //if ( TestUtils.stringToStringOrNull( lang3 ) != null || TestUtils.stringToStringOrNull( desc3 ) != null ) {
        //if ( lang3 != null || desc3 != null ) {
        if (!(lang3 == null && desc3 == null)) {
            DescriptionType dt3 = new DescriptionType();
            dt3.setLanguageCode(TestUtils.stringToStringOrNull(lang3));
            dt3.setDescription(TestUtils.stringToStringOrNull(desc3));
            descSet.add(dt3);
        }

        return descSet;
    }

    protected String[][] getUserData() {
        String[][] retObjArr = {
                //List default userId and password ant it's uc here. These users will be used to log in.
                //Because we running test in a secured jboss server.

                //TC-00-01: a serve test case that will log out current user and log in with QASuperUser with dataPrivileges A, B, C, D and facilities 1,2,3,4
                { "TC-00-01", "devtest", "devtest",
                        "{'consumerApplicationId':'clientAppId','deviceIPAddress':null,'deviceId':121,'facilityId':1,'HTTPSessionId':null,'processStepId':'0','process':orgadm,'task':orgsrch,'timestamp':null,'TBD1':null,'TBD2':null,'TBD3':null}" },
                //
                //TC-00-01: a serve test case that will log out current user and log in with QAUser1 without any dataPrivileges and facilities.
                { "TC-00-01", "QAUser1", "QAUser1",
                        "{'consumerApplicationId':'clientAppId','deviceIPAddress':null,'deviceId':121,'facilityId':1,'HTTPSessionId':null,'processStepId':'0','process':orgadm,'task':orgsrch,'timestamp':null,'TBD1':null,'TBD2':null,'TBD3':null}" },
                //
                //TC-00-01: a serve test case that will log out current user and log in with QAUser2 with data privilege A and facility 1.
                { "TC-00-01", "QAUser2", "QAUser2",
                        "{'consumerApplicationId':'clientAppId','deviceIPAddress':null,'deviceId':121,'facilityId':1,'HTTPSessionId':null,'processStepId':'0','process':orgadm,'task':orgsrch,'timestamp':null,'TBD1':null,'TBD2':null,'TBD3':null}" },
                //
                //TC-00-01: a serve test case that will log out current user and log in with QAUser3 with dataPrivileges A, B and C and facilities 1,2,3.
                { "TC-00-01", "QAUser3", "QAUser3",
                        "{'consumerApplicationId':'clientAppId','deviceIPAddress':null,'deviceId':121,'facilityId':1,'HTTPSessionId':null,'processStepId':'0','process':orgadm,'task':orgsrch,'timestamp':null,'TBD1':null,'TBD2':null,'TBD3':null}" },
                //
                //TC-00-01: a serve test case that will log out current user and log in with QAUser1 without any dataPrivileges and facilities.
                { "TC-00-01", "QAUser4", "QAUser4",
                        "{'consumerApplicationId':'clientAppId','deviceIPAddress':null,'deviceId':121,'facilityId':1,'HTTPSessionId':null,'processStepId':'0','process':orgadm,'task':orgsrch,'timestamp':null,'TBD1':null,'TBD2':null,'TBD3':null}" },
                //
                //TC-00-01: a serve test case that will log out current user and log in with QAUser2 with data privilege A and facility 1.
                { "TC-00-01", "QAUser5", "QAUser5",
                        "{'consumerApplicationId':'clientAppId','deviceIPAddress':null,'deviceId':121,'facilityId':1,'HTTPSessionId':null,'processStepId':'0','process':orgadm,'task':orgsrch,'timestamp':null,'TBD1':null,'TBD2':null,'TBD3':null}" },
                //
                //TC-00-01: a serve test case that will log out current user and log in with QAUser3 with dataPrivileges A, B and C and facilities 1,2,3.
                { "TC-00-01", "QAUser6", "QAUser6",
                        "{'consumerApplicationId':'clientAppId','deviceIPAddress':null,'deviceId':121,'facilityId':1,'HTTPSessionId':null,'processStepId':'0','process':orgadm,'task':orgsrch,'timestamp':null,'TBD1':null,'TBD2':null,'TBD3':null}" },

        };

        return retObjArr;
    }

    /**
     * The login method will login with valid userId, password and uc.
     */
    protected void login(String tc, String userId, String password, String userContextStr) {

        uc = gson.fromJson(userContextStr, UserContext.class);
        uc.setTimestamp((new Date()));

        LOG.trace("userId is: " + userId + ", password is: " + password + ", uc is:" + gson.toJson(uc));

        try {
            SecurityClient client = SecurityClientFactory.getSecurityClient();
            //Need to logout first to make sure the session is not in login status.
            LOG.trace("log out from last user");
            client.logout();

            LOG.trace("-----login is running-------");
            LOG.trace("log in as user. userId and password are: " + userId + "/" + password);
            client.setSimple(userId, password);
            client.login();
            LOG.trace("login successful.");
        } catch (Exception e) {
            e.printStackTrace();
            Assert.assertTrue(false, "Login failed. userId and password are:" + userId + "/" + password);
        }
    }

    /**
     * If no userIndex passed in, Use QASuperUser to login by default.
     */
    protected void login2() {
        String par0 = (String) getUserData()[0][0];
        String par1 = (String) getUserData()[0][1];
        String par2 = (String) getUserData()[0][2];
        String par3 = (String) getUserData()[0][3];
        login(par0, par1, par2, par3);
    }

    /**
     * This method creates ReferenceString.
     * @param set
     * @param code
     * @param sequence
     * @param createDate - can be null
     * @param activeDate - can be null
     * @param deactivationDate - can be null
     * @param lang1
     * @param desc1
     * @param lang2 - can be null
     * @param desc2 - can be null
     * @param lang3 - can be null
     * @param desc3 - can be null
     * @return ReferenceCodeReturnType
     */
    /*protected ReferenceCodeReturnType createReferenceCode(
            String set,
			String code, 
			String sequence, 
			String createDate, 
			String activeDate,
			String deactivationDate,
			String lang1, 
			String desc1, 
			String lang2, 
			String desc2,
			String lang3,
			String desc3 ) {
		l
		ReferenceCodeReturnType  result = null;
		
		ReferenceString ref = assembleReferenceCode( set, code, sequence, createDate, activeDate, deactivationDate,
				lang1, desc1, lang2, desc2, lang3, desc3 );
		
		result = service.createReferenceCode(uc, ref);
		
		LOG.trace("Created reference code: " + gson.toJson(result));
		
		return result;
	}*/

    /**
     * Pick up a user by indenx of user in user data array to login.
     */
    protected void login(int userIndex) {
        String tc = (String) getUserData()[userIndex][0];
        String userId = (String) getUserData()[userIndex][1];
        String password = (String) getUserData()[userIndex][2];
        String userContextStr = (String) getUserData()[userIndex][3];
        login(tc, userId, password, userContextStr);
    }

    /**
     * this method returns reference code type for set and code passed as a parameters or null
     * if reference code not found.
     *
     * @param set
     * @param code
     * @return
     */
	/*protected ReferenceString findReferenceCode( String set, String code ) {
	
		ReferenceString result = null;
		
		ReferenceCodesReturnType refsRet = service.getAllReferenceCodes( uc );

		for ( ReferenceString r : refsRet.getReferenceCodes() ) {
			if ( r.getSet().equals(set) && r.getCode().equals(code) ) {
				result = r;
				break;
			}
		}
		
		if (result == null)
			LOG.error("Reference code (" + set + ", " + code + ")" + "is not found" );
		
		return result;
	}*/
    protected boolean areDescriptionSetsEqual(Set<DescriptionType> act, Set<DescriptionType> exp) {
        boolean result = false;

        if (act != null && exp != null) {
            if (act.size() == exp.size()) {
                if (act.size() == 0) {
					return true;
				}

                int found = 0;
                for (DescriptionType e : exp) {
                    for (DescriptionType a : act) {
                        if ((e.getDescription().equals(a.getDescription())) && (e.getLanguageCode().toUpperCase().equals(a.getLanguageCode()))) {
                            found++;
                            break;
                        }
                    }

                }
                return (found == exp.size());
            }

        } else if (act == null && exp == null) {
            result = true;
        }

        return result;
    }

    /**
     * To assemble and return an object of LinkType.
     *
     * @param set
     * @param code
     * @param linkSet
     * @param linkCode
     * @param linkDeactivationDate - optional
     * @return an object of LinkType
     * @author lhan
     */
    //TODO: If a service does not have LinkType, comment our this method.
	/*protected LinkType assembleLink(
			String set, 
			String code, 
			String linkSet, 
			String linkCode,
			String thirdSet,
			String thirdCode) {
		
		LinkType link = new LinkType();
		String code1 = assembleString(set,code);
		String code2 = assembleString(linkSet,linkCode);
				
		Set<String> codes = new HashSet<String>();
		codes.add(code1);
		codes.add(code2);
		
		//This is to add one more String into the Link for some special test case.
		if(thirdSet instanceof String && thirdCode instanceof String){
			String code3 = assembleString(thirdSet,thirdCode);
			codes.add(code3);
		}	
		
		link.setLink(codes);
		
		return link;
	}*/
    protected void logTestStart(String tc) {
        LOG.info(LN_TEST_SEPERATOR + tc + " started");
    }

    protected void logTestPassed(String tc) {
        LOG.info(LN_TEST_SEPERATOR + tc + " passed");
    }

    /**
     * This method is to compare if the two reference sets contain same references.
     *
     * @param setA is an object of Set<Long>.
     * @param setB is an object of Set<Long>.
     * @return true if the two sets are containing same references, otherwise return false.
     * @author lhan
     */
    protected boolean areTwoAssociationSetsEqual(Set<Long> setA, Set<Long> setB) {
        //If size are not same, then sets do not equal.
        if (setA.size() != setB.size()) {
			return false;
		}

        int found = 0;
        Iterator<Long> iteratorA = setA.iterator();
        while (iteratorA.hasNext()) {
            Long elementA = iteratorA.next();
            Iterator<Long> iteratorB = setB.iterator();
            while (iteratorB.hasNext()) {
                Long elementB = iteratorB.next();
                if (elementA.equals(elementB)) {
                    found++;
                    break;
                }
            }
        }

        //All references in setA are found in setB, return true; otherwise return false.
        return (found == setA.size());
    }

    /**
     * This method is to compare if the two String sets contain same Strings.
     *
     * @param setA is an object of Set<String>.
     * @param setB is an object of Set<String>.
     * @return true if the two sets are containing same Strings, otherwise return false.
     * @author lhan
     */
    protected boolean areTwoStringSetsEqual(Set<String> setA, Set<String> setB) {
        //If size are not same, then the two sets do not equal.
        if (setA.size() != setB.size()) {
			return false;
		}

        int found = 0;
        Iterator<String> iteratorA = setA.iterator();
        while (iteratorA.hasNext()) {
            String elementA = iteratorA.next();
            Iterator<String> iteratorB = setB.iterator();
            while (iteratorB.hasNext()) {
                String elementB = iteratorB.next();
                if (elementA.equalsIgnoreCase(elementB)) {
                    found++;
                    break;
                }
            }
        }

        //All Strings in setA are found in setB, return true; otherwise return false.
        return (found == setA.size());
    }

    /**
     * This method is to check if a String set contains a String.
     *
     * @param setA     is an object of Set<String>.
     * @param codeType is an object of String.
     * @return true if the set contains the codeType, otherwise return false.
     * @author lhan
     */
    protected boolean isStringSetContainString(Set<String> setA, String codeType) {
        //If size are not same, then the two sets do not equal.
        if (setA.size() == 0 || codeType == null) {
			return false;
		}

        int found = 0;
        Iterator<String> iteratorA = setA.iterator();
        while (iteratorA.hasNext()) {
            String elementA = iteratorA.next();
            if (elementA.equalsIgnoreCase(codeType)) {
                found++;
                break;
            }
        }

        //All Strings in setA are found in setB, return true; otherwise return false.
        return (found == 1);
    }

    /**
     * This method is to check if a Descriptions set contains a DescriptionType.
     *
     * @param setA            is an object of Set<DescriptionType>.
     * @param descriptionType is an object of DescriptionType.
     * @return true if the set contains the descriptionType, otherwise return false.
     * @author lhan
     */
    protected boolean isDescriptionsSetContainDescription(Set<DescriptionType> setA, DescriptionType descriptionType) {
        //If size are not same, then the two sets do not equal.
        if (setA.size() == 0 || descriptionType == null) {
			return false;
		}

        int found = 0;
        Iterator<DescriptionType> iteratorA = setA.iterator();
        while (iteratorA.hasNext()) {
            DescriptionType elementA = iteratorA.next();
            if (elementA.getLanguageCode().equalsIgnoreCase(descriptionType.getLanguageCode()) && elementA.getDescription().equals(descriptionType.getDescription())) {

                found++;
                break;
            }
        }

        //All Strings in setA are found in setB, return true; otherwise return false.
        return (found == 1);
    }

    /**
     * This method is to compare if the two String are same in value.
     *
     * @param codeTypeA is an object of String.
     * @param codeTypeB is an object of String.
     * @return true if the two Strings have same values, otherwise return false.
     * @author lhan
     */
    protected boolean areTwoStringEqual(String codeTypeA, String codeTypeB) {
        //If point to same object, then the two Strings equal.
        if (codeTypeA == codeTypeB) {
			return true;
		}

        if (codeTypeA == null && codeTypeB != null) {
			return false;
		}

        if (codeTypeA != null && codeTypeB == null) {
			return false;
		}

        return false;

    }

    /**
     * This method is to compare if the two Longs are same in value.(not include dataPrivileges)
     *
     * @param typeA is an object of Long.
     * @param typeB is an object of Long.
     * @return true if the two Longs have same values, otherwise return false.
     * @author lhan
     */
    protected boolean areTwoLongEqual(Long typeA, Long typeB) {
        //If point to same object, then the two Strings equal.
        if (typeA == typeB) {
			return true;
		}

        if (typeA == null && typeB != null) {
			return false;
		}

        if (typeA != null && typeB == null) {
			return false;
		}

        return false;

    }

    public enum IdType {
        ACTID("actId"),
        SUPID("supId"),
        MOVID("movId"),
        INTID("intId"),
        EXTID("extId"),
        HOUID("houId");

        private final String value;

        /**
         * Constructor.
         *
         * @param v one of the IdType classes.
         */
        IdType(String v) {
            value = v;
        }

        public static IdType fromValue(String v) {
            for (IdType c : IdType.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

        /**
         * String value of this IdType.
         *
         * @return String
         */
        public String value() {
            return value;
        }
    }

}
	