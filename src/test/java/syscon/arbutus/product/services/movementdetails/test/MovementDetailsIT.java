package syscon.arbutus.product.services.movementdetails.test;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityDetailsType;
import syscon.arbutus.product.services.movement.contract.dto.MovementDetailsType;
import syscon.arbutus.product.services.movement.contract.dto.OffenderLocationStatusType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;

@Test(groups = "movementdetails", dependsOnGroups = "associations")
public class MovementDetailsIT extends SecureBase {

    private static final Logger LOG = LoggerFactory.getLogger(MovementDetailsIT.class);

    private MovementService service;

    @BeforeClass
    public void beforeClass() {
        LOG.info("-----beforeTest is running-------");
        service = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
        uc = super.initUserContext();
    }

    @Test
    public void testMovementDetailsService(ITestContext context) {
        LOG.info("run MovementDetailsIT test.");

        List<Long> supIds = null;
        Long supId = null;

        List<MovementActivityDetailsType> rtn1;
        List<MovementDetailsType> rtn2;
        OffenderLocationStatusType rtn3;
        List<OffenderLocationStatusType> rtn4;

        //null required parameter.
        try {
            rtn1 = service.getMostRecentActivityMovementData(uc, null);
        } catch (Exception e) {
        }

        try {
            rtn1 = service.searchMovementDetailsByMovementDate(uc, supId, null, null, null, null, null);
        } catch (Exception e) {
        }

        try {
            rtn1 = service.searchMovementDetailsByScheduledDate(uc, supId, null, null, null, null, null);
        } catch (Exception e) {
        }

        try {
            rtn2 = service.getMostRecentMovementData(uc, null);
        } catch (Exception e) {
        }

        try {
            rtn3 = service.getOffenderLocationStatus(uc, supId);
        } catch (Exception e) {
        }

        try {
            rtn4 = service.getOffenderLocationStatuses(uc, null);
        } catch (Exception e) {
        }

        //invalid required parameter
        supIds = new ArrayList<Long>();
        supId = 9999999L;

        try {
            rtn1 = service.getMostRecentActivityMovementData(uc, new HashSet<Long>(supIds));
        } catch (Exception e) {
        }

        rtn1 = service.searchMovementDetailsByMovementDate(uc, supId, null, null, null, null, null);
        Assert.assertEquals(rtn1.size(), 0);

        rtn1 = service.searchMovementDetailsByScheduledDate(uc, supId, null, null, null, null, null);
        Assert.assertEquals(rtn1.size(), 0);

        try {
            rtn2 = service.getMostRecentMovementData(uc, new HashSet<Long>(supIds));
        } catch (Exception e) {
        }

        try {
            rtn3 = service.getOffenderLocationStatus(uc, supId);
        } catch (Exception e) {
        }
        try {
            rtn4 = service.getOffenderLocationStatuses(uc, new HashSet<Long>(supIds));
        } catch (Exception e) {
        }

        //Positive return test

        //temp Ids for testing.
        //supIds.add(1259L);
        //supIds.add(1260L);
        //supIds.add(1174L);

        int i = 0;
        while (context.getAttribute(IdType.SUPID.value() + i) instanceof Long) {
            supIds.add((Long) context.getAttribute(IdType.SUPID.value() + i));
            i++;
        }

        supId = supIds.get(1);

		/*for(int j = 100000; j<100400; j++){
            supIds.add(j));
		}*/

        //+++++++++++++++++++++++++++++++++test getMostRecentActivityMovementData++++++++++++++++++++++
        LOG.info("run getMostRecentActivityMovementData test.");

        rtn1 = service.getMostRecentActivityMovementData(uc, new HashSet<Long>(supIds));
        Assert.assertEquals(rtn1.size(), 2);

        List<MovementActivityDetailsType> returnSet = rtn1;

        for (MovementActivityDetailsType moveDetail : returnSet) {
            if (moveDetail.getSupervisionIdentification().equals(supIds.get(0))) {
                Assert.assertTrue(moveDetail.getMovementActivityType().getMovementId().equals((Long) context.getAttribute(IdType.EXTID.value() + 1)));
            }
            if (moveDetail.getSupervisionIdentification().equals(supIds.get(1))) {
                Assert.assertTrue(moveDetail.getMovementActivityType().getMovementId().equals((Long) context.getAttribute(IdType.INTID.value() + 2)));
            }
        }

        //+++++++++++++++++++++++++testing searchMovementDetailsByMovementDate+++++++++++++++++++++
        LOG.info("run searchMovementDetailsByMovementDate test.");

        //test defect #2461
        //no-exiting supervison id.
        rtn1 = service.searchMovementDetailsByMovementDate(uc, 99991l, null, null, null, null, null);
        Assert.assertEquals(rtn1.size(), 0);

        //only supId
        rtn1 = service.searchMovementDetailsByMovementDate(uc, supId, null, null, null, null, null);
        Assert.assertEquals(rtn1.size(), 4);

        //both from and to actual date.
        Calendar cal = Calendar.getInstance();
        cal.set(2012, 9, 1, 2, 6, 8);
        Date fromActualDate = cal.getTime();
        cal.set(2012, 9, 2, 2, 6, 10);
        Date toActualDate = cal.getTime();

        rtn1 = service.searchMovementDetailsByMovementDate(uc, supId, fromActualDate, toActualDate, null, null, null);
        Assert.assertEquals(rtn1.size(), 2);

        //only from actual date.
        cal = Calendar.getInstance();
        cal.set(2012, 8, 2, 2, 6, 8);
        fromActualDate = cal.getTime();

        rtn1 = service.searchMovementDetailsByMovementDate(uc, supId, fromActualDate, null, null, null, null);
        Assert.assertEquals(rtn1.size(), 4);

        //only to actual date.
        cal = Calendar.getInstance();
        cal.set(2012, 9, 2, 2, 5, 8);
        toActualDate = cal.getTime();

        rtn1 = service.searchMovementDetailsByMovementDate(uc, supId, null, toActualDate, null, null, null);
        Assert.assertEquals(rtn1.size(), 2);

        //Test defect#2594
        cal = Calendar.getInstance();
        cal.set(1990, 9, 2, 2, 5, 8);
        toActualDate = cal.getTime();

        rtn1 = service.searchMovementDetailsByMovementDate(uc, supId, null, toActualDate, null, null, null);
        Assert.assertNotNull(rtn1);
        Assert.assertEquals(rtn1.size(), 0);
        /*
		//only movement category
		rtn1 = service.searchMovementDetailsByMovementDate(uc, supId, null, null, new CodeType(ReferenceSets.MOVEMENT_CATEGORY.value(), Constants.MOVEMENT_CATEGORY_EXTERNAL), null, null);
		Assert.assertNotNull(rtn1);
		Assert.assertEquals(rtn1.size(), 1);
		
		//only movement category
		rtn1 = service.searchMovementDetailsByMovementDate(uc, supId, null, null, new CodeType(ReferenceSets.MOVEMENT_CATEGORY.value(), Constants.MOVEMENT_CATEGORY_INTERNAL), null, null);
		Assert.assertNotNull(rtn1);
		Assert.assertEquals(rtn1.size(), 3);
		
		//only movement status
		Set<CodeType> movementStatuses = new HashSet<CodeType>();
		movementStatuses.add(new CodeType(ReferenceSets.MOVEMENT_STATUS.value(), Constants.MOVEMENT_STATUS_COMPLETE));
		movementStatuses.add(new CodeType(ReferenceSets.MOVEMENT_STATUS.value(), "PENDING"));		
		
		rtn1 = service.searchMovementDetailsByMovementDate(uc, supId, null, null, null, movementStatuses, null);
		Assert.assertNotNull(rtn1);
		Assert.assertEquals(rtn1.size(), 4);
		
		//only movement type
		rtn1 = service.searchMovementDetailsByMovementDate(uc, supId, null, null, null, null, new CodeType(ReferenceSets.MOVEMENT_TYPE.value(), "CRT"));
		Assert.assertNotNull(rtn1);
		Assert.assertEquals(rtn1.size(), 4);
		
		//test defect#2597
		rtn1 = service.searchMovementDetailsByMovementDate(uc, supId, null, null, null, null, new CodeType(ReferenceSets.MOVEMENT_TYPE.value(), "ADM"));
		Assert.assertNotNull(rtn1);
		Assert.assertEquals(rtn1.size(), 0);
		*/
        //+++++++++++++++++++++++++testing searchMovementDetailsByScheduledDate+++++++++++++++++++++++++++++
        LOG.info("run searchMovementDetailsByScheduledDate test.");

        //only supId
        rtn1 = service.searchMovementDetailsByScheduledDate(uc, supId, null, null, null, null, null);
        Assert.assertNotNull(rtn1);
        Assert.assertEquals(rtn1.size(), 4);

        //both from Schedule date and to Schedule date.
        cal = Calendar.getInstance();
        cal.set(2012, 8, 11, 2, 6, 8);
        Date fromScheduleDate = cal.getTime();
        cal.set(2012, 8, 12, 2, 6, 10);
        Date toScheduleDate = cal.getTime();

        rtn1 = service.searchMovementDetailsByScheduledDate(uc, supId, fromScheduleDate, toScheduleDate, null, null, null);
        Assert.assertNotNull(rtn1);
        Assert.assertEquals(rtn1.size(), 4);

        //only from Schedule date.
        cal = Calendar.getInstance();
        cal.set(2012, 8, 2, 2, 6, 8);
        fromScheduleDate = cal.getTime();

        rtn1 = service.searchMovementDetailsByScheduledDate(uc, supId, fromScheduleDate, null, null, null, null);
        Assert.assertNotNull(rtn1);
        Assert.assertEquals(rtn1.size(), 4);

        //only to Schedule date.
        cal = Calendar.getInstance();
        cal.set(2012, 9, 2, 2, 5, 8);
        toScheduleDate = cal.getTime();

        rtn1 = service.searchMovementDetailsByScheduledDate(uc, supId, null, toScheduleDate, null, null, null);
        Assert.assertNotNull(rtn1);
        Assert.assertEquals(rtn1.size(), 4);

        //test defect#2594
        cal.set(1960, 9, 2, 2, 5, 8);
        toScheduleDate = cal.getTime();

        rtn1 = service.searchMovementDetailsByScheduledDate(uc, supId, null, toScheduleDate, null, null, null);
        Assert.assertNotNull(rtn1);
        Assert.assertEquals(rtn1.size(), 0);
		/*
		//only movement category
		rtn1 = service.searchMovementDetailsByScheduledDate(uc, supId, null, null, new CodeType(ReferenceSets.MOVEMENT_CATEGORY.value(), Constants.MOVEMENT_CATEGORY_EXTERNAL), null, null);
		Assert.assertNotNull(rtn1);
		Assert.assertEquals(rtn1.size(), 1);
		
		//only movement category
		rtn1 = service.searchMovementDetailsByScheduledDate(uc, supId, null, null, new CodeType(ReferenceSets.MOVEMENT_CATEGORY.value(), Constants.MOVEMENT_CATEGORY_INTERNAL), null, null);
		Assert.assertNotNull(rtn1);
		Assert.assertEquals(rtn1.size(), 3);
		
		//only movement status
		movementStatuses = new HashSet<CodeType>();
		movementStatuses.add(new CodeType(ReferenceSets.MOVEMENT_STATUS.value(), Constants.MOVEMENT_STATUS_COMPLETE));
		movementStatuses.add(new CodeType(ReferenceSets.MOVEMENT_STATUS.value(), "PENDING"));		
		
		rtn1 = service.searchMovementDetailsByScheduledDate(uc, supId, null, null, null, movementStatuses, null);
		Assert.assertNotNull(rtn1);
		Assert.assertEquals(rtn1.size(), 4);
		
		//only movement type
		rtn1 = service.searchMovementDetailsByMovementDate(uc, supId, null, null, null, null, new CodeType(ReferenceSets.MOVEMENT_TYPE.value(), "CRT"));
		Assert.assertNotNull(rtn1);
		Assert.assertEquals(rtn1.size(), 4);
		*/

        //+++++++++++++++++++++++++++++++++test getMostRecentMovementData++++++++++++++++++++++
        LOG.info("run getMostRecentMovementData test.");

        //Test defect #2442
        Set<Long> noExistingIds = new HashSet<Long>();
        noExistingIds.add(999991L);
        noExistingIds.add(999992L);
        rtn2 = service.getMostRecentMovementData(uc, new HashSet<Long>(noExistingIds));
        Assert.assertNotNull(rtn2);
        Assert.assertEquals(rtn2.size(), 0);

        rtn2 = service.getMostRecentMovementData(uc, new HashSet<Long>(supIds));
        Assert.assertNotNull(rtn2);
        Assert.assertEquals(rtn2.size(), 2);

        List<MovementDetailsType> returnSet1 = rtn2;

        for (MovementDetailsType moveDetail : returnSet1) {
            if (moveDetail.getSupervisionIdentification().equals(supIds.get(0))) {
                Assert.assertTrue(moveDetail.getToFacilityInternalLocationId().equals(99999L));
            }
            if (moveDetail.getSupervisionIdentification().equals(supIds.get(1))) {
                Assert.assertTrue(moveDetail.getToFacilityInternalLocationId().equals(4L));
            }
        }

        //+++++++++++++++++++++++++++++++++test getOffenderLocationStatus++++++++++++++++++++++
        LOG.info("run getOffenderLocationStatus test.");

        rtn3 = service.getOffenderLocationStatus(uc, supId);
        Assert.assertNotNull(rtn3);
        Assert.assertTrue(rtn3.getCurrentInternalLocationId().equals(4L));

        //+++++++++++++++++++++++++++++++++test getOffenderLocationStatuses++++++++++++++++++++++
        LOG.info("run getOffenderLocationStatuses test.");

        rtn4 = service.getOffenderLocationStatuses(uc, new HashSet<Long>(supIds));

        Assert.assertEquals(rtn4.size(), 2);

        List<OffenderLocationStatusType> returnSet2 = rtn4;

        for (OffenderLocationStatusType moveDetail : returnSet2) {
            if (moveDetail.getSupervisionIdentification().equals(supIds.get(0))) {
                Assert.assertTrue(moveDetail.getCurrentInternalLocationId().equals(99999L));
            }
            if (moveDetail.getSupervisionIdentification().equals(supIds.get(1))) {
                Assert.assertTrue(moveDetail.getCurrentInternalLocationId().equals(4L));
            }
        }
    }

}
