package syscon.arbutus.product.services.movementdetails.test;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingAttributeMismatchType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingBedMgmtActivityType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingLocationAssignType;
import syscon.arbutus.product.services.housing.contract.interfaces.HousingService;

//@Test (dependsOnGroups = "C30",groups = {"F1", "Functional"})
@Test(groups = { "housing" }, dependsOnGroups = "activity")
public class AssignHousingLocationPositiveTest extends SecureBase {
    private static final Logger LOG = LoggerFactory.getLogger(AssignHousingLocationPositiveTest.class);

    private HousingService service;

    @BeforeClass
    public void beforeTest() {
        service = (HousingService) JNDILookUp(this.getClass(), HousingService.class);
        uc = super.initUserContext();
    }

    /**
     * The reset method calls the service's reset() method to initialize all reference code/links data of the service to default values in DB.
     */
    @BeforeClass(dependsOnMethods = { "beforeTest" })
    public void reset() {

        LOG.info("Reset...");
        Long returnCode = service.deleteAll(uc);
        Assert.assertEquals(returnCode, SUCCESS);

    }

    @DataProvider(name = "DP_01_Positive")
    public Object[][] createInstanceType_TCs_01(ITestContext context) {

        Object[][] retObjArr = {

                //
                //this script verifies only activityState == HousingAssigned due to nature the operation

                //TC, jsonForCreateInstanceType, jsonExpectedInstanceType
                //
                //TC-01-01: verify that assignHousingLocation() operation creates a new HousingBedMngmActivityType with mandatory input parameters for InstanceType only;
                { "TC-01-01",
                        //input instance
                        "{'offenderReference': null,'movementReason':'OFFREQ'," +
                                "'locationTo':'111','assignedBy':'1'," +
                                "'assignmentMismatches':null," +
                                "'movementComment':null," +
                                "'facilityId':'1'," +
                                "'associations':[],'facilities':null,'dataPrivileges':null}", context.getAttribute(IdType.SUPID.value() + 0),
                        context.getAttribute(IdType.ACTID.value() + 2) },

                //
                //TC-01-02: verify that assignHousingLocation() operation creates a new HousingBedMngmActivityType with mandatory input parameters for InstanceType and all children sub-types (assignmentMismatches);
                { "TC-01-02",
                        //input instance
                        "{'offenderReference': null,'movementReason':'OFFREQ'," +
                                "'locationTo':'112','assignedBy':'1'," +
                                "'assignmentMismatches':null," +
                                "'movementComment':null," +
                                "'facilityId':'1'," +
                                "'associations':[],'facilities':null,'dataPrivileges':null}", context.getAttribute(IdType.SUPID.value() + 1),
                        context.getAttribute(IdType.ACTID.value() + 6) },

        };
        return (retObjArr);
    }

    @Test(dataProvider = "DP_01_Positive")
    public void assignHousingLocationPositive01(String TC, String jsonForCreateInstanceType, Long supId, Long actId) {

        LOG.info("====================================== Running TC = " + TC);
        LOG.trace("Running TC = " + TC);

        //
        HousingLocationAssignType jsonInstanceType = new HousingLocationAssignType();
        LOG.trace("empty InstanceType is:" + gson.toJson(jsonInstanceType));
        //
        HousingAttributeMismatchType missmatchType = new HousingAttributeMismatchType();
        LOG.trace("empty missmatchType is:" + gson.toJson(missmatchType));
        //
        //get jsonInstanceType from jsonForCreateInstanceType
        jsonInstanceType = gson.fromJson(jsonForCreateInstanceType, HousingLocationAssignType.class);

        jsonInstanceType.setSupervisionId(supId);
        jsonInstanceType.setActivityId(actId);

        LOG.trace("InstanceType before create is:" + gson.toJson(jsonInstanceType));

        //getCount() of Instances before calling the operation
        Long countBefore = null;
        countBefore = service.getCount(uc);
        LOG.trace("countBefore=" + countBefore);
        Assert.assertNotNull(countBefore, "Cannot getCount() before calling the operation");

        jsonInstanceType.setAssignedDate(new Date());
        jsonInstanceType.setOverallOutcome("SUTBL");

        //verify and validate instanceType
        //create Instance from jsonInstanceType
        HousingBedMgmtActivityType retInstanceType = service.assignHousingLocation(uc, jsonInstanceType);

        LOG.trace("Return of creating Instance is " + gson.toJson(retInstanceType));

        assert (retInstanceType != null);
        Assert.assertNotNull(retInstanceType, "Return instance of create() should not be null.");
        Assert.assertNotNull(retInstanceType.getHousingActivityId(), "Return instanceId should NOT be NULL.");

        //creating aggregated boolean result
        boolean result = true;

        //getCount() of Instances after calling the operation
        Long countAfter = null;
        countAfter = service.getCount(uc);
        LOG.trace("countAfter=" + countAfter);

        if ((countAfter.longValue() - countBefore.longValue()) != 1) {
            LOG.error("(countAfter - countBefore) does not match 1. countBefore=" + countBefore + "; countAfter=" + countAfter);
            result = false;
        }

        //verify Instance from get();
        //
        retInstanceType = service.get(uc, retInstanceType.getHousingActivityId());
        LOG.trace("Returned Instance from get() is " + gson.toJson(retInstanceType));

        if (retInstanceType == null) {
            result = false;
        }

        Assert.assertTrue(result, TC + " is failed. See error messages in the log above.");

        LOG.info("Passed TC = " + TC + "======================================");
    }
}

	

