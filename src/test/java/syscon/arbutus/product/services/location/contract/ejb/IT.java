package syscon.arbutus.product.services.location.contract.ejb;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.core.contract.dto.AssociationReturnType;
import syscon.arbutus.product.services.core.contract.dto.AssociationType;
import syscon.arbutus.product.services.core.contract.dto.ReferenceCodeReturnType;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.location.contract.dto.*;
import syscon.arbutus.product.services.location.contract.interfaces.LocationService;

import javax.ejb.EJBTransactionRolledbackException;
import java.math.BigDecimal;
import java.util.*;

import static org.testng.Assert.*;

@ModuleConfig
public class IT extends BaseIT {

    protected static final String SERVICE_NAME = "LocationService";
    private static Logger log = LoggerFactory.getLogger(IT.class);
    // shared data
    Object tip;
    Location lrt, lrtCopy;
    AssociationReturnType rrt;
    ReferenceCodeReturnType rcrt;
    LocationSearchType lst;
    AssociationType rt;
    List<Long> facilityIds = new ArrayList<Long>();
    private LocationService service;

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (LocationService) JNDILookUp(this.getClass(), LocationService.class);
        uc = super.initUserContext();
    }

    @AfterClass
    public void afterClass() {
        //service.deleteAll(uc);
        //FacilityTest facilityTest = new FacilityTest();
        //facilityTest.deleteAll();
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
        service.deleteAll(uc);

        FacilityTest facilityTest = new FacilityTest();
        facilityIds.add(facilityTest.createFacility());
        facilityIds.add(facilityTest.createFacility());
    }

    @Test
    public void testGetVersion() {
        String version = service.getVersion(uc);
        assert ("1.1".equals(version));
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testCreateLocation() {
        Location lrt = null;

        lrt = service.create(uc, createLocation("1", false, facilityIds.get(0)));
        service.create(uc, createLocation("2", false, facilityIds.get(0)));
        service.create(uc, createLocation("3", false, facilityIds.get(0)));
        service.create(uc, createLocation("4", false, facilityIds.get(1)));

        Location in = createLocation("5", false, facilityIds.get(0));

        Gson gson = new Gson();
        String jsonString = gson.toJson(in);
        log.info(jsonString);

        Location newIn = gson.fromJson(jsonString, Location.class);

        lrt = service.create(uc, in);

        Location ret = lrt;

        assertNotNull(ret);
        assertEquals("ADI-B", ret.getLocationTwoDimentionalGeographicalCoordinate().getGeographicDatumCode());

        BigDecimal bd = new BigDecimal(6.89788);
        bd = bd.setScale(6, BigDecimal.ROUND_HALF_UP);
        assertEquals(0, (bd).compareTo(ret.getLocationTwoDimentionalGeographicalCoordinate().getLongtitudeSecondValue()));

        this.lrt = lrt;

        //test create location failed since FK constraints
        try {
            lrt = service.create(uc, createLocation("1", false, 99999L));
        } catch (Exception e) {
            Assert.assertTrue(e instanceof EJBTransactionRolledbackException);
        }

    }

    @Test(dependsOnMethods = { "testCreateLocation" })
    public void testCopyLocation() {
        lrtCopy = service.copy(uc, this.lrt.getLocationIdentification());
        Assert.assertNotNull(lrtCopy);
        Assert.assertNotNull(lrtCopy.getLocationIdentification());
        Assert.assertNotEquals(lrtCopy.getLocationIdentification(), lrt.getLocationIdentification());

        // get the new copied location
        Location ret = service.get(uc, lrtCopy.getLocationIdentification());
        Assert.assertNotNull(ret);
        assertEquals(ret.getLocationAddress().getLocationStreetCategory(), "AVE");
        assertEquals(ret.getLocationPhoneFaxNumber().size(), 1);
    }

    @Test(dependsOnMethods = { "testCopyLocation" })
    public void testGetLocation() {
        Location lrt = service.get(uc, this.lrt.getLocationIdentification());
        Assert.assertNotNull(lrt);
    }

    //	@Test(dependsOnMethods = { "testGetLocation" })
    public void testGetStamp() {
        StampType st = service.getStamp(uc, this.lrt.getLocationIdentification());
        Assert.assertNotNull(st);
    }

    //	@Test(dependsOnMethods = { "testGetStamp" })
    public void testGetLocationCount() {
        Long count = service.getCount(uc);
        assert (count.longValue() != 0);
    }

    @Test(dependsOnMethods = { "testCopyLocation" })
    //	@Test(dependsOnMethods = { "testJNDILookup" })
    public void testGetAllLocations() {
        Set<Long> res = service.getAll(uc);
        assert (res.size() != 0);
    }

    @Test(dependsOnMethods = { "testGetAllLocations" })
    public void defect_1249() {

        service.create(uc, createLocation("1", false, 2l));

        lst = new LocationSearchType();

        // category
        Set<String> lcs = new HashSet<String>();
        lcs.add("01");
        //		lcs.add("11");
        //		lcs.add("18");

        lst.setLocationCategory(lcs);

        LocationsReturnType lsrt = service.search(uc, lst, null, null, null);
        //		System.out.println("lsrt.getCount() = " + lsrt.getCount());

        List<Location> set = lsrt.getLocations();
        for (Location lt : set) {
            //			System.out.println("=> " + lt);
        }
    }

    @Test(dependsOnMethods = { "defect_1249" })
    public void testSearchLocations() {
        lst = new LocationSearchType();

        //		lst.setLocationName("Location Name 1");
        //		lst.setLocationDescription("The locationDescriptionText");
        //		lst.setLocationCrossStreet("the location cross street");
        //		lst.setTypeOfLocation(new String("mailing"));

        // add phone fax
        Set<String> pf = new HashSet<String>();
        pf.add("the locationPhoneFaxNumber 1");
        //		lst.setLocationPhoneFaxNumber(pf);

        // one-to-one
        LocationAddressSearchType la = new LocationAddressSearchType();
        //		la.setInternationalAddressLine("The internationalAddressLine");
        //		la.setAddressBuildingText("the addressBuilding");
        //		la.setLocationCityName("vancouver 2");
        //		la.setLocationCountryName(new String("can"));
        //		la.setLocationStreetNumSuffix("The locationStreetNum");
        //		la.setLocationStreetNumberText(l(12));
        //		la.setLocationStreetName("The locationStreetName");
        //		la.setLocationPostalCode("V5x");
        //		la.setLocationPostalExtensionCode("12-1");
        //		la.setLocationLocalityName("the locality nam");

        //		lst.setLocationAddress(la);

        // one-to-many
        // category
        Set<String> lcs = new HashSet<String>();
        //		lcs.add("BILLING");
        lcs.add("01");
        lcs.add("02");
        //		lcs.add("03");

        //		lst.setLocationCategory(lcs);

        // email
        Set<String> emails = new HashSet<String>();
        emails.add("email2@hotm.com");
        emails.add("cw@abm.com");

        lst.setLocationEmail(emails);

        // locationLocale
        Set<LocationLocaleType> localeSet = new HashSet<LocationLocaleType>();
        LocationLocaleType ll1 = new LocationLocaleType();
        ll1.setLocaleCommunityName("the localeCommunityName ");
        //		ll1.setLocaleDescriptionText("the localeDescriptionText ");
        //		ll1.setLocaleRegionName("the localeRegionName 1");
        localeSet.add(ll1);
        LocationLocaleType ll2 = new LocationLocaleType();
        ll2.setLocaleCommunityName("the localeCommunityName ");
        ll2.setLocaleDescriptionText("the localeDescriptionText ");
        //		ll2.setLocaleRegionName("the localeRegionName 11");
        localeSet.add(ll2);
        localeSet.add(new LocationLocaleType());
        //		lst.setLocationLocale(localeSet);

        //		lst.setLocationActiveFlag(true);

        // Set<String> locationSurroundingAreaDescriptionText;
        Set<String> surrSet = new HashSet<String>();
        surrSet.add("A");
        surrSet.add("A11");
        surrSet.add("A1");
        surrSet.add("B");
        surrSet.add("B1");

        //		lst.setLocationSurroundingAreaDescription(surrSet);

        // Reference
        //		AssociationType rt = new AssociationType();
        //		rt.setToClass(AssociationToClass.FACILITY.getValue());
        //		rt.setToIdentifier(1l);
        //		Set<AssociationType> refSet = new HashSet<AssociationType>();
        //		refSet.add(rt);
        //		lst.setLocationReferences(refSet);

        // start Date
        //		lst.setLocationStartDate(getDate(2012, 5, 8));
        // end Date
        //		lst.setLocationEndDate(getDate(2012, 4, 28));

        LocationsReturnType lsrt = service.search(uc, lst, null, null, null);
        //		System.out.println("lsrt.getCount() = " + lsrt.getCount());

        List<Location> set = lsrt.getLocations();
        for (Location lt : set) {
            //			System.out.println("=> " + lt);
        }
    }

    @Test(dependsOnMethods = { "testSearchLocations" })
    public void testDeleteLocation() {

        Location location = createLocation("1", false, facilityIds.get(1));
        Location rtn = service.create(uc, location);
        Long id = rtn.getLocationIdentification();

        Long rtnDelete = service.delete(uc, id);
        Assert.assertEquals(rtnDelete.longValue(), 1L);

        try {
            rtnDelete = service.delete(uc, 99999L);
        } catch (Exception e) {
            Assert.assertTrue(e instanceof DataNotExistException);
        }

        try {
            rtnDelete = service.delete(uc, null);
        } catch (Exception e) {
            Assert.assertTrue(e instanceof InvalidInputException);
        }

    }

    // now the location record is deleted.
    // create a new record for copy

    /**
     * update ltCopy
     */
    @Test(dependsOnMethods = { "testSearchLocations" })
    public void testUpdateLocation() {
        Location l = this.lrt;

        l.setLocationName("Updated Location Name");
        l.setLocationDescriptionText(null);
        l.setLocationCrossStreet("Updated location cross street");
        l.setLocationActiveFlag(false);

        l.setLocationStartDate(new Date());
        l.setLocationEndDate(new Date());
        l.setLocationPrimaryFlag(false);

        Set<String> typeSet = new HashSet<String>();
        typeSet.add(new String("BILLING"));
        l.setTypeOfLocationType(typeSet);

        //
        LocationAddressType la = new LocationAddressType();
        //		la.setInternationalAddressLine("Updated internationalAddressLine");
        la.setAddressBuildingText("Updated addressBuildingText");
        la.setLocationStreetNumSuffix("Updated locationStreetNumSuffix");
        la.setLocationStreetNumberText("13");
        la.setLocationStreetName("Updated locationStreetName");
        la.setLocationStreetCategory(new String("AVENUE"));
        la.setLocationStreetPredirectionalText(new String("W"));
        la.setLocationStreetPostdirectionalText(new String("S"));
        la.setLocationUnitNumber("14");
        la.setLocationFloorNum(13l);
        la.setLocationCityName("Richmond");
        la.setLocationStateCode(new String("BC"));
        la.setLocationCountryName(new String("US"));
        la.setLocationPostalCode("V6X 1Y2");
        la.setLocationPostalExtensionCode("13-2");
        la.setAddressDeliveryMode(new String("Updated mail mode"));
        la.setAddressDeliveryModeNumber(13l);
        la.setLocationCountyName(new String("Updated County"));
        la.setLocationLocalityName("Updated locality name");

        l.setLocationAddress(la);

        //
        Set<LocationPhoneFaxType> lpfns = new HashSet<LocationPhoneFaxType>();

        LocationPhoneFaxType lpft = new LocationPhoneFaxType();
        lpft.setLocationPhoneFaxNumberCategory(LocationPhoneFaxType.HOME_PHONE);
        lpft.setLocationPhoneFaxNumber("Updated locationPhoneFaxNumber");
        lpft.setLocationPhoneExtension("Updated locationPhoneExtension");

        lpfns.add(lpft);

        l.setLocationPhoneFaxNumber(lpfns);

        //
        Set<String> les = new HashSet<String>();
        les.add("Updated@abm.com");
        les.add("Updated@hotm.com");

        l.setLocationEmail(les);

        // locationLocale;
        Set<LocationLocaleType> lls = new HashSet<LocationLocaleType>();

        LocationLocaleType llt = new LocationLocaleType();
        llt.setLocaleCommunityName("Updated localeCommunityName");
        llt.setLocaleDescriptionText("Updated localeDescriptionText");
        llt.setLocaleDistrictName("Updated localeDistrictName");
        llt.setLocaleEmergencyServiceCityName("Updated localeEmergencyServiceCityName");
        llt.setLocaleFireJurisdictionID("Updated localeFireJurisdictionID");
        llt.setLocaleJudicialDistrictName("Updated localeJudicialDistrictName");
        llt.setLocaleJudicialDistrictCode("Updated localeJudicialDistrictCode");
        llt.setLocaleNeighborhoodName("Updated localeNeighborhoodName");
        llt.setLocalePoliceBeatText("Updated localePoliceBeatText");
        llt.setLocalePoliceGridText("Updated localePoliceGridText");
        llt.setLocalePoliceJurisdictionID("Updated localePoliceJurisdictionID");
        llt.setLocaleRegionName("Updated localeRegionName");
        llt.setLocaleSubdivisionName("Updated localeSubdivisionName");
        llt.setLocaleZoneName("Updated localeZoneName");
        llt.setLocaleEmergencyServiceJurisdictionID("Updated localeEmergencyServiceJurisdictionID");

        lls.add(llt);

        l.setLocationLocale(lls);

        // locationReferences
        l.setAddressCategory(AddressCategory.FACILITY);
        l.setInstanceId(2L);

        // Set<String> locationSurroundingAreaDescriptionText;
        Set<String> lsadts = new HashSet<String>();
        lsadts.add("Updated corrner");

        l.setLocationSurroundingAreaDescriptionText(lsadts);

        // locationTwoDimentionalGeographicalCoordinate
        LocationTwoDimentionalGeographicalCoordinateType ltdgct = new LocationTwoDimentionalGeographicalCoordinateType();
        ltdgct.setGeographicDatumCode(new String("ADI-B"));
        ltdgct.setLatitudeDegreeValue(22L);
        ltdgct.setLatitudeMinuteValue(null);
        ltdgct.setLatitudeSecondValue(new BigDecimal(33.444555));
        ltdgct.setLongtitudeDegreeValue(44L);
        ltdgct.setLongtitudeMinuteValue(55L);
        ltdgct.setLongtitudeSecondValue(new BigDecimal(6.0));

        l.setLocationTwoDimentionalGeographicalCoordinate(ltdgct);

        // set location category
        Set<String> locCategorySet = new HashSet<String>();
        locCategorySet.add(new String("04"));
        locCategorySet.add(new String("08"));
        locCategorySet.add(new String("06"));
        locCategorySet.add(new String("10"));
        l.setLocationCategory(locCategorySet);

        this.lrtCopy = service.update(uc, l);
        Location ret = lrtCopy;
        assertNotNull(ret);
        assertNotNull(ret.getLocationPhoneFaxNumber());
        assertNotSame(ret.getLocationPhoneFaxNumber().size(), 0);
        assertNotNull(ret.getLocationPhoneFaxNumber().toArray(new LocationPhoneFaxType[0])[0].getLocationPhoneFaxNumberCategory());
        assertEquals(ret.getLocationPhoneFaxNumber().toArray(new LocationPhoneFaxType[0])[0].getLocationPhoneFaxNumberCategory(), "MOBILEPHONE");
        assertEquals(ret.getLocationTwoDimentionalGeographicalCoordinate().getGeographicDatumCode(), "ADI-B");
        assertEquals(ret.getTypeOfLocationType().size(), 1);
        assertEquals(ret.getTypeOfLocationType().iterator().next(), "BILLING");
        assertEquals(ret.getLocationCategory().size(), 4);

    }

    /**
     * update locationCopy again with new Address
     */

    @Test(dependsOnMethods = { "testUpdateLocation" })
    public void testUpdateLocationAddressType() {
        // 1. Create location without LocationAddressType
        Location locationType = null;

        locationType = createLocation("6", true, l(1));

        locationType.setLocationAddress(null);
        locationType = service.create(uc, locationType);
        assertNotNull(locationType);

        // 2. Call updateLocationAddress() to set data.
        LocationAddressType la = new LocationAddressType();
        la.setLocationCountryName(new String("USA"));
        //		la.setInternationalAddressLine("Updated2 internationalAddressLine"); // must be set alone only
        la.setAddressBuildingText("Updated2 addressBuildingText");
        la.setLocationStreetNumSuffix("Updated2 locationStreetNumSuffix");
        la.setLocationStreetNumberText("14");
        la.setLocationStreetName("Updated2 locationStreetName");
        la.setLocationStreetCategory(new String("AVE"));
        la.setLocationStreetPredirectionalText(new String("W"));
        la.setLocationStreetPostdirectionalText(new String("S"));
        la.setLocationUnitNumber("15");
        la.setLocationFloorNum(14l);
        la.setLocationCityName("Surrey");
        la.setLocationStateCode(new String("ON"));
        la.setLocationPostalCode("V9X 1X2");
        la.setLocationPostalExtensionCode("13-2");
        la.setAddressDeliveryMode(new String("RR"));
        //		la.setAddressDelieryModeNumber(14l);
        //		la.setLocationCountyName("Updated2 County");
        //		la.setLocationLocalityName("Updated2 locality name");

        Location lrt = service.updateLocationAddress(uc, locationType.getLocationIdentification(), la);
        assertNotNull(lrt);
        assertNotNull(lrt);
        assertNotNull(lrt.getLocationAddress());
        assertEquals(lrt.getLocationAddress().getLocationUnitNumber(), "15");
        assertNotNull(lrt.getLocationAddress().getLocationStreetPredirectionalText());
        assertEquals(lrt.getLocationAddress().getLocationStreetPredirectionalText(), "W");

    }

    /**
     * update locationCopy again with new Locales
     */

    //	@Test(dependsOnMethods = { "testUpdateLocationLocale" })
    public void testUpdateLocationLocale2() {
        LocationLocaleType llt = new LocationLocaleType();
        llt.setLocaleCommunityName("updated2 localeCommunityName");
        llt.setLocaleDescriptionText("updated2 localeDescriptionText");
        llt.setLocaleDistrictName("updated2 localeDistrictName");
        llt.setLocaleEmergencyServiceCityName(" updated2 localeEmergencyServiceCityName");
        llt.setLocaleFireJurisdictionID("updated2 localeFireJurisdictionID");
        llt.setLocaleJudicialDistrictName(" updated2 localeJudicialDistrictName");
        llt.setLocaleJudicialDistrictCode("updated2 localeJudicialDistrictCode");
        llt.setLocaleNeighborhoodName("updated2 localeNeighborhoodName");
        llt.setLocalePoliceBeatText("updated2 localePoliceBeatText");
        llt.setLocalePoliceGridText("updated2 localePoliceGridText");
        llt.setLocalePoliceJurisdictionID("updated2 localePoliceJurisdictionID");
        llt.setLocaleRegionName("updated2 localeRegionName");
        llt.setLocaleSubdivisionName("updated2 localeSubdivisionName");
        llt.setLocaleZoneName("updated2 localeZoneName");
        llt.setLocaleEmergencyServiceJurisdictionID("updated2 localeEmergencyServiceJurisdictionID");

        lrt = service.get(null, this.lrt.getLocationIdentification());

        Set<LocationLocaleType> locales = new HashSet<LocationLocaleType>();
        locales.clear();
        locales.add(llt);

        lrt = service.updateLocationLocale(null, this.lrt.getLocationIdentification(), locales);

        lrt = service.get(null, this.lrt.getLocationIdentification());
        Set<LocationLocaleType> set = lrt.getLocationLocale();
        for (LocationLocaleType ll : set) {
            //			System.out.println("\nlrt.getLocationLocale() = > " + ll);
        }

        assertNotNull(lrt);
    }

    @Test(dependsOnMethods = { "testUpdateLocationAddressType" })
    public void testUpdateLocationLocale() {
        Location l = this.lrt;

        //
        Set<LocationLocaleType> lls = new HashSet<LocationLocaleType>();

        LocationLocaleType llt = new LocationLocaleType();
        llt.setLocaleCommunityName("updated2 localeCommunityName");
        llt.setLocaleDescriptionText("updated2 localeDescriptionText");
        llt.setLocaleDistrictName("updated2 localeDistrictName");
        llt.setLocaleEmergencyServiceCityName(" updated2 localeEmergencyServiceCityName");
        llt.setLocaleFireJurisdictionID("updated2 localeFireJurisdictionID");
        llt.setLocaleJudicialDistrictName(" updated2 localeJudicialDistrictName");
        llt.setLocaleJudicialDistrictCode("updated2 localeJudicialDistrictCode");
        llt.setLocaleNeighborhoodName("updated2 localeNeighborhoodName");
        llt.setLocalePoliceBeatText("updated2 localePoliceBeatText");
        llt.setLocalePoliceGridText("updated2 localePoliceGridText");
        llt.setLocalePoliceJurisdictionID("updated2 localePoliceJurisdictionID");
        llt.setLocaleRegionName("updated2 localeRegionName");
        llt.setLocaleSubdivisionName("updated2 localeSubdivisionName");
        llt.setLocaleZoneName("updated2 localeZoneName");
        llt.setLocaleEmergencyServiceJurisdictionID("updated2 localeEmergencyServiceJurisdictionID");

        LocationLocaleType llt2 = new LocationLocaleType();
        llt2.setLocaleCommunityName("updated2 localeCommunityName");
        llt2.setLocaleDescriptionText("updated2 localeDescriptionText");
        llt2.setLocaleDistrictName("updated2 localeDistrictName");
        llt2.setLocaleEmergencyServiceCityName(" updated2 localeEmergencyServiceCityName");
        llt2.setLocaleFireJurisdictionID("updated2 localeFireJurisdictionID");
        llt2.setLocaleJudicialDistrictName(" updated2 localeJudicialDistrictName");
        llt2.setLocaleJudicialDistrictCode("updated2 localeJudicialDistrictCode");
        llt2.setLocaleNeighborhoodName("updated2 localeNeighborhoodName");
        llt2.setLocalePoliceBeatText("updated2 localePoliceBeatText");
        llt2.setLocalePoliceGridText("updated2 localePoliceGridText");
        llt2.setLocalePoliceJurisdictionID("updated2 localePoliceJurisdictionID");
        llt2.setLocaleRegionName("updated2 localeRegionName");
        llt2.setLocaleSubdivisionName("updated2 localeSubdivisionName");
        llt2.setLocaleZoneName("updated2 localeZoneName");
        llt2.setLocaleEmergencyServiceJurisdictionID("updated2 localeEmergencyServiceJurisdictionID");

        lls.add(llt);
        lls.add(llt2);

        service.updateLocationLocale(uc, l.getLocationIdentification(), lls);

    }

    @Test(dependsOnMethods = { "testUpdateLocationAddressType" })
    public void testGetNIEM() {
        Location l = this.lrt;
        String xml = service.getNIEM(uc, l.getLocationIdentification());
        assertNotNull(xml);
    }

    // ========= reference =====================

    /**
     * create a association
     */

    // ========= reference code =====================

    // =  =  =  =  BUGs  =  =  =  =  =
    @Test(dependsOnMethods = { "testGetNIEM" })
    public void testDefect_590() {
        Location l = new Location();
        l.setLocationName("defect_590");
        l.setLocationStartDate(new Date());
        l.setLocationPrimaryFlag(false);

        Set<String> typeSet = new HashSet<String>();
        typeSet.add(new String("MAILING"));
        l.setTypeOfLocationType(typeSet);
        l.setAddressCategory(AddressCategory.OTHER);

        Location ret = service.create(uc, l);
    }

    //  =  =  =  =  =  =  PRIVATE  =  =  =  =  =  =

    private Long l(int i) {
        return new Long(i);
    }

    private Date getDate(int year, int month, int date) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, date, 0, 0, 0);
        return cal.getTime();
    }

    private Location createLocation(String name, boolean primary, Long refId) {
        Location l = new Location();

        l.setLocationName("Location Name " + name);
        l.setLocationDescriptionText("The locationDescriptionText");
        l.setLocationCrossStreet("the location cross street");
        l.setLocationActiveFlag(null);
        l.setLocationStartDate(getDate(2011, Integer.parseInt(name), 28));
        if (name != null) {
            l.setLocationEndDate(getDate(2012, Integer.parseInt(name), 28));
        }
        l.setLocationPrimaryFlag(primary);

        Set<String> typeSet = new HashSet<String>();
        typeSet.add(new String("MAILING"));
        l.setTypeOfLocationType(typeSet);

        //
        Set<String> lcs = new HashSet<String>();
        lcs.add(new String("0" + name));
        //		lcs.add(new String("1" + name));
        //		lcs.add(new String("18"));

        l.setLocationCategory(lcs);

        // set LocationAddressType
        LocationAddressType la = new LocationAddressType();
        //		la.setInternationalAddressLine("The internationalAddressLine"); // must be set alone only
        la.setLocationCountryName(new String("CAN"));
        la.setAddressBuildingText("the addressBuildingText");
        la.setLocationStreetNumSuffix("The locationStreetNumSuffix");
        la.setLocationStreetNumberText("12");
        la.setLocationStreetName("The locationStreetName");
        la.setLocationStreetCategory(new String("AVE"));
        la.setLocationStreetPredirectionalText(new String("E"));
        la.setLocationStreetPostdirectionalText(new String("E"));
        la.setLocationUnitNumber("13");
        la.setLocationFloorNum(12l);
        la.setLocationCityName("vancouver " + name);
        la.setLocationStateCode(new String("AB"));
        la.setLocationPostalCode("V" + name + "X 1S2");
        la.setLocationPostalExtensionCode("12-1");
        la.setAddressDeliveryMode(new String("MS"));
        la.setAddressDeliveryModeNumber(12l);
        la.setLocationCountyName(new String("037-06"));
        la.setLocationLocalityName("the locality name");

        l.setLocationAddress(la);

        //
        Set<LocationPhoneFaxType> lpfns = new HashSet<LocationPhoneFaxType>();

        LocationPhoneFaxType lpft = new LocationPhoneFaxType();
        lpft.setLocationPhoneFaxNumberCategory(LocationPhoneFaxType.HOME_PHONE);
        lpft.setLocationPhoneFaxNumber("the locationPhoneFaxNumber " + name);
        lpft.setLocationPhoneExtension("the locationPhoneExtension");

        lpfns.add(lpft);

        l.setLocationPhoneFaxNumber(lpfns);

        //
        Set<String> les = new HashSet<String>();
        les.add("cw@abm.com");
        les.add("email2@hotm.com");

        l.setLocationEmail(les);

        // locationLocale;
        Set<LocationLocaleType> lls = new HashSet<LocationLocaleType>();

        LocationLocaleType llt = new LocationLocaleType();
        llt.setLocaleCommunityName("the localeCommunityName ");
        llt.setLocaleDescriptionText("the localeDescriptionText ");
        llt.setLocaleDistrictName("the localeDistrictName");
        llt.setLocaleEmergencyServiceCityName(" the localeEmergencyServiceCityName");
        llt.setLocaleFireJurisdictionID("the localeFireJurisdictionID");
        llt.setLocaleJudicialDistrictName(" the localeJudicialDistrictName");
        llt.setLocaleJudicialDistrictCode("the localeJudicialDistrictCode");
        llt.setLocaleNeighborhoodName("the localeNeighborhoodName");
        llt.setLocalePoliceBeatText("the localePoliceBeatText");
        llt.setLocalePoliceGridText("the localePoliceGridText");
        llt.setLocalePoliceJurisdictionID("the localePoliceJurisdictionID");
        llt.setLocaleRegionName("the localeRegionName " + name);
        llt.setLocaleSubdivisionName("the localeSubdivisionName");
        llt.setLocaleZoneName("the localeZoneName");
        llt.setLocaleEmergencyServiceJurisdictionID("the localeEmergencyServiceJurisdictionID");

        lls.add(llt);

        LocationLocaleType llt2 = new LocationLocaleType();
        llt2.setLocaleCommunityName("the localeCommunityName ");
        llt2.setLocaleDescriptionText("the localeDescriptionText ");
        llt2.setLocaleDistrictName("the localeDistrictName");
        llt2.setLocaleEmergencyServiceCityName(" the localeEmergencyServiceCityName");
        llt2.setLocaleFireJurisdictionID("the localeFireJurisdictionID");
        llt2.setLocaleJudicialDistrictName(" the localeJudicialDistrictName");
        llt2.setLocaleJudicialDistrictCode("the localeJudicialDistrictCode");
        llt2.setLocaleNeighborhoodName("the localeNeighborhoodName");
        llt2.setLocalePoliceBeatText("the localePoliceBeatText");
        llt2.setLocalePoliceGridText("the localePoliceGridText");
        llt2.setLocalePoliceJurisdictionID("the localePoliceJurisdictionID");
        llt2.setLocaleRegionName("the localeRegionName " + name + name);
        llt2.setLocaleSubdivisionName("the localeSubdivisionName");
        llt2.setLocaleZoneName("the localeZoneName");
        llt2.setLocaleEmergencyServiceJurisdictionID("the localeEmergencyServiceJurisdictionID");

        lls.add(llt2);

        l.setLocationLocale(lls);

        // locationReferences 1-to-1
        l.setAddressCategory(AddressCategory.FACILITY);
        l.setInstanceId(refId);

        // Set<String> locationSurroundingAreaDescriptionText;
        Set<String> lsadts = new HashSet<String>();
        lsadts.add("left corrner");

        l.setLocationSurroundingAreaDescriptionText(lsadts);

        // LocationTwoDimentionalGeographicalCoordinateType
        LocationTwoDimentionalGeographicalCoordinateType ltdgct = new LocationTwoDimentionalGeographicalCoordinateType();
        ltdgct.setGeographicDatumCode(new String("ADI-A"));
        ltdgct.setLatitudeDegreeValue(null);
        ltdgct.setLatitudeMinuteValue(33L);
        ltdgct.setLatitudeSecondValue(new BigDecimal(3.123));
        ltdgct.setLongtitudeDegreeValue(44L);
        ltdgct.setLongtitudeMinuteValue(55L);
        ltdgct.setLongtitudeSecondValue(new BigDecimal(6.89788));
        ltdgct.setGeographicDatumCode(new String("ADI-B"));

        l.setLocationTwoDimentionalGeographicalCoordinate(ltdgct);

        // Set<String> locationSurroundingAreaDescriptionText;
        Set<String> surrSet = new HashSet<String>();
        surrSet.add("A");
        surrSet.add("B");
        surrSet.add("A" + name);
        surrSet.add("A" + name + name);
        surrSet.add("B" + name);

        l.setLocationSurroundingAreaDescriptionText(surrSet);

        return l;
    }
}
