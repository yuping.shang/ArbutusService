/**
 * © 2013 by Syscon Justice Systems. All rights reserved.
 * Copying and distribution, any modifications or adaptation of the contents, or creation of derivative
 * works are not allowed without the express and prior written consent of Syscon Justice Systems.
 */
package syscon.arbutus.product.services.eventlog.contract.dto;

import java.util.Date;

import org.testng.annotations.Test;

public class ShiftLogAmendTypeTest {
    @Test
    public void isWellFormed() {
        ShiftLogAmendType data = new ShiftLogAmendType();

        data.setDescription(null);
        data.setDescription("This is Amend Shift Log.");
        data.setEventDate(new Date());
        data.setEventTime(new Date());
        data.setActivityType(null);

        String activity = "xyz";
        data.setActivityType(activity);

        data.setFacilityId(null);
        data.setLocationId(null);

        Long facRef = new Long(1L);
        Long locRef = new Long(101L);
        data.setFacilityId(facRef);
        data.setLocationId(locRef);
    }

    @Test
    public void isValid() {
        ShiftLogAmendType data = new ShiftLogAmendType();

        data.setDescription(null);
        data.setDescription("This is Amend Shift Log.");
        data.setEventDate(new Date());
        data.setEventTime(new Date());
        data.setActivityType(null);

        String activity = "xyz";
        data.setActivityType(activity);

        data.setFacilityId(null);
        data.setLocationId(null);

        Long facRef = new Long(1L);
        Long locRef = new Long(1L);
        data.setFacilityId(facRef);
        data.setLocationId(locRef);

    }
}
