/**
 * © 2013 by Syscon Justice Systems. All rights reserved.
 * Copying and distribution, any modifications or adaptation of the contents, or creation of derivative
 * works are not allowed without the express and prior written consent of Syscon Justice Systems.
 */
package syscon.arbutus.product.services.eventlog.contract.dto;

import java.util.Date;

import org.testng.annotations.Test;

public class ShiftLogTypeTest {
    @Test
    public void isWellFormed() {
        ShiftLogType data = new ShiftLogType();

        data.setEventDate(new Date());

        data.setEventTime(new Date());

        data.setReportingStaff(100L);

        data.setActivityType(null);

        data.setActivityType("caution");

        data.setFacilityId(null);

        data.setLocationId(null);

        data.setFacilityId(1L);
        data.setLocationId(11L);
        data.setNarrativeDesc("This is a Shift Log");
        data.setShiftLogNumber("SL-1001");

        data.setReportingStaff(100L);
    }

    @Test
    public void isValid() {
        ShiftLogType data = new ShiftLogType();

        data.setEventDate(new Date());

        data.setEventTime(new Date());

        data.setActivityId(1L);

        data.setReportingStaff(100L);

        data.setActivityType("caution");

        data.setFacilityId(1L);

        data.setLocationId(11L);

        data.setNarrativeDesc("This is a Shift Log");
        data.setShiftLogNumber("SL-1001");

        data.setReportingStaff(100L);

        data.setNarrativeDesc(
                "jxjhxjvwsxvqhjvxhjswvxhjvwhjvxjhwvxhjvswxvswhvxhjwvsxhjvwhjxv2udf76f76fw2vfxuw2vxdhjswvxhjvswvxhjqsvxhvsqhvxqvjxvsxvjwsvhjswvcvswcvswfd2378fd3df387dv37evxd7ve3vxgyxfwjvfxj3vfuxu3fxu3vfgux3gxvgxf23tyxfw2fxt7fxf2yuwf762fxd78w2gx8g2yuxsgw2gfxuyhjxvw2vxyu2wfdfevxvuzv3uevxd27fyxuwfuxyu32fxyuf2xvqhjxvhjqgx7623fr6xdf2xvuw2vxjgv2xhjsvxhjv3x76vjxhvsjvxv2d76f27vxghw2vghxvwgqvxhf327fxiwvxv2xf236fxyuvwxvhje3vfx76vqhjxvjw2hbvxhjv2hjuxv72fvxv2hjxve3ef68xfvqwbxh,qsbjxvcwuv3uevcjvwnxcvswvxghkvsxvj3vxhjvswxc763xvyue3vxvwhjsvxcsw76xve3jkxcvjkswvxvsuqvxuv76swvxw2bvjxvshvx6e3vxjshvxhjv3suvx378vxhjvswxv27udu72vxjhqxv3fe3vxuvhu3vxjh3vjkf378vxuoewxwldho;ie3cpjrcmpncopjeiocvnrio;nhvg4hf9b45fkjb4jklbv4bnmrcuio4bcipb978cr4bvjkbrilvg4u8bvjkbvcjkdebviocg478bvjklrbvbedv4bvvjkwbncjkbh4uipvg7r4rbcvjk;r4nvjkn4rvg49vhklernvr4hvgeuipvbncvk;bevuipg4rrvne vbfupvguir4bvbrvu8uivbvkerjbcflercuipg4pbvcbr4vfrcgvbjkrbcvbdhxsgc;djcfhcdbcjkbndehcuierh");
    }
}
