/**
 * © 2013 by Syscon Justice Systems. All rights reserved.
 * Copying and distribution, any modifications or adaptation of the contents, or creation of derivative
 * works are not allowed without the express and prior written consent of Syscon Justice Systems.
 */
package syscon.arbutus.product.services.eventlog.contract.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.contract.dto.AssociationType;

public class ShiftLogSearchTypeTest {
    @Test
    public void isWellFormed() {
        ShiftLogSearchType data = new ShiftLogSearchType();

        data.setFromEventDate(new Date());

        data.setFromEventTime(new Date());

        data.setToEventDate(new Date());

        data.setToEventTime(new Date());

        data.setReportingStaff(100L);

        data.setDescription("abc");

        Set<AssociationType> assos = new HashSet<AssociationType>();
        assos.add(new AssociationType());

        assos.clear();
        assos.add(new AssociationType("ActivityService", 1L));

        data.setShiftLogNumber("SL-100");

        String activity = "abc";
        data.setActivityType(activity);

        AssociationType facRef = new AssociationType();
        AssociationType locRef = new AssociationType();

    }

    @Test
    public void isValid() {
        ShiftLogSearchType data = new ShiftLogSearchType();

        data.setFromEventDate(new Date());

        data.setFromEventTime(new Date());

        data.setToEventDate(new Date());

        data.setToEventTime(new Date());

        data.setReportingStaff(100L);

        data.setDescription("abc");

        Set<AssociationType> assos = new HashSet<AssociationType>();
        assos.add(new AssociationType());

        assos.clear();
        assos.add(new AssociationType("ActivityService", 1L));

        data.setShiftLogNumber("SL-100");

        String activity = "abc";
        data.setActivityType(activity);

        AssociationType facRef = new AssociationType();
        AssociationType locRef = new AssociationType();
    }
}
