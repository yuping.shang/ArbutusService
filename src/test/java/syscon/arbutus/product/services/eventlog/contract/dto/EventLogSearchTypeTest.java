/**
 * © 2013 by Syscon Justice Systems. All rights reserved.
 * Copying and distribution, any modifications or adaptation of the contents, or creation of derivative
 * works are not allowed without the express and prior written consent of Syscon Justice Systems.
 */
package syscon.arbutus.product.services.eventlog.contract.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.contract.dto.AssociationType;
import syscon.arbutus.product.services.realization.util.BeanHelper;

public class EventLogSearchTypeTest {
    @Test
    public void isWellFormed() {
        EventLogSearchType data = new EventLogSearchType();

        data.setFromEventDate(new Date());

        data.setFromEventTime(new Date());

        data.setToEventDate(new Date());

        data.setToEventTime(new Date());

        data.setReportingStaff(100L);

        data.setDescription("abc");

        Set<AssociationType> assos = new HashSet<AssociationType>();
        assos.add(new AssociationType());
    }

    @Test
    public void isValid() {
        EventLogSearchType data = new EventLogSearchType();

        data.setFromEventDate(new Date());

        data.setFromEventTime(new Date());

        data.setToEventDate(new Date());

        data.setToEventTime(new Date());

        data.setReportingStaff(100L);

        data.setDescription("abc");

        Set<AssociationType> assos = new HashSet<AssociationType>();
        assos.add(new AssociationType());

        assos.clear();
        assos.add(new AssociationType("ActivityService", 100L));

        data.setFromEventDate(BeanHelper.nextNthDays(new Date(), 1));
    }
}
