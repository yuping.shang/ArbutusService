/**
 * © 2013 by Syscon Justice Systems. All rights reserved.
 * Copying and distribution, any modifications or adaptation of the contents, or creation of derivative
 * works are not allowed without the express and prior written consent of Syscon Justice Systems.
 */
package syscon.arbutus.product.services.eventlog.contract.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.contract.dto.AssociationType;
import syscon.arbutus.product.services.realization.util.BeanHelper;

public class CaseNoteSearchTypeTest {
    @Test
    public void isWellFormed() {
        CaseNoteSearchType data = new CaseNoteSearchType();

        data.setFromEventDate(new Date());

        data.setFromEventTime(new Date());

        data.setToEventDate(new Date());

        data.setToEventTime(new Date());

        data.setReportingStaff(100L);

        data.setDescription("abc");

        Set<AssociationType> assos = new HashSet<AssociationType>();
        assos.add(new AssociationType());

        assos.clear();
        assos.add(new AssociationType("ActivityService", 1L));

        data.setCaseNoteNumber("CL-100");

        data.setOffFirstName("ABC");

        data.setOffLastName("XYZ");

        data.setOffMiddleName("PQR");

    }

    @Test
    public void isValid() {
        CaseNoteSearchType data = new CaseNoteSearchType();

        data.setFromEventDate(new Date());

        data.setFromEventTime(new Date());

        data.setToEventDate(new Date());

        data.setToEventTime(new Date());

        data.setReportingStaff(100L);

        data.setDescription("abc");

        Set<AssociationType> assos = new HashSet<AssociationType>();
        assos.add(new AssociationType());

        assos.clear();
        assos.add(new AssociationType("ActivityService", 1L));

        data.setCaseNoteNumber("CL-100");

        data.setOffFirstName("ABC");

        data.setOffLastName("XYZ");

        data.setOffMiddleName("PQR");

        data.setFromEventDate(BeanHelper.nextNthDays(new Date(), 1));
    }
}
