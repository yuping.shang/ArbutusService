/**
 * © 2013 by Syscon Justice Systems. All rights reserved.
 * Copying and distribution, any modifications or adaptation of the contents, or creation of derivative
 * works are not allowed without the express and prior written consent of Syscon Justice Systems.
 */
package syscon.arbutus.product.services.eventlog.contract.dto;

import java.util.Date;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.realization.util.BeanHelper;

public class CaseNoteAmendTypeTest {
    @Test
    public void isWellFormed() {
        CaseNoteAmendType data = new CaseNoteAmendType();

        data.setDescription(null);
        data.setDescription("This is Amend Case Note.");
        data.setEventDate(new Date());
        data.setEventTime(new Date());
        data.setNoteType(null);

        String note = "xyz";
        String subNote = "abc";
        data.setNoteType(note);
        data.setNoteSubType(subNote);

    }

    @Test
    public void isValid() {
        CaseNoteAmendType data = new CaseNoteAmendType();

        data.setDescription(null);
        data.setDescription("This is Amend Case Note.");
        data.setEventDate(new Date());
        data.setEventTime(new Date());
        data.setNoteType(null);

        String note = "xyz";
        String subNote = "abc";
        data.setNoteType(note);
        data.setNoteSubType(subNote);

        Date futureDate = BeanHelper.nextNthDays(new Date(), 1);
        data.setEventDate(futureDate);

    }
}
