/**
 * © 2013 by Syscon Justice Systems. All rights reserved.
 * Copying and distribution, any modifications or adaptation of the contents, or creation of derivative
 * works are not allowed without the express and prior written consent of Syscon Justice Systems.
 */
package syscon.arbutus.product.services.eventlog.contract.ejb;

import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.naming.NamingException;

import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldMetaType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueType;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.LegalTest;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.eventlog.contract.dto.CaseNoteAmendType;
import syscon.arbutus.product.services.eventlog.contract.dto.CaseNoteSearchType;
import syscon.arbutus.product.services.eventlog.contract.dto.CaseNoteType;
import syscon.arbutus.product.services.eventlog.contract.dto.EventLogImageType;
import syscon.arbutus.product.services.eventlog.contract.dto.EventLogType;
import syscon.arbutus.product.services.eventlog.contract.dto.EventLogType.EventType;
import syscon.arbutus.product.services.eventlog.contract.dto.LegalNoteSearchType;
import syscon.arbutus.product.services.eventlog.contract.dto.LegalNoteType;
import syscon.arbutus.product.services.eventlog.contract.dto.NarrativeTextType;
import syscon.arbutus.product.services.eventlog.contract.dto.PersonType;
import syscon.arbutus.product.services.eventlog.contract.dto.PersonTypes;
import syscon.arbutus.product.services.eventlog.contract.dto.ShiftLogAmendType;
import syscon.arbutus.product.services.eventlog.contract.dto.ShiftLogSearchType;
import syscon.arbutus.product.services.eventlog.contract.dto.ShiftLogType;
import syscon.arbutus.product.services.eventlog.contract.dto.StgCaseNoteType;
import syscon.arbutus.product.services.eventlog.contract.dto.TransactLogEntityTypes;
import syscon.arbutus.product.services.eventlog.contract.dto.TransactLogType;
import syscon.arbutus.product.services.eventlog.contract.ejb.Constants.ActivityType;
import syscon.arbutus.product.services.eventlog.contract.ejb.Constants.NoteSubType;
import syscon.arbutus.product.services.eventlog.contract.ejb.Constants.NoteType;
import syscon.arbutus.product.services.eventlog.contract.interfaces.EventLogService;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.FacilityInternalLocation;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.LocationAttributeType;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.UsageCategory;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.person.contract.dto.StaffPosition;
import syscon.arbutus.product.services.person.contract.dto.StaffType;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.person.contract.test.ContractTestUtil;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

public class IT extends BaseIT {
    private static Logger log = LoggerFactory.getLogger(IT.class);
    protected UserContext uc = null;
    // shared data
    Object tip;
    private EventLogService service;
    private FacilityService facService;
    private FacilityInternalLocationService facilityInternalLocationService;
    private SupervisionService supService;
    private PersonService personService;
    private Long facilityId;
    private Long facilityInternalLocationId;
    private Long staffId;
    private Long supervisionId;
    private Long personId;
    private Long personIdentityId;
    private LegalTest legalTest = new LegalTest();

    public static CommentType createComment(String text) {
        CommentType ct = new CommentType();
        ct.setComment(text);
        ct.setCommentDate(new Date());
        return ct;
    }

    @BeforeClass
    public void beforeClass() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");
        service = (EventLogService) JNDILookUp(this.getClass(), EventLogService.class);
        uc = super.initUserContext();

        facService = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        facilityInternalLocationService = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        supService = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        personService = (PersonService) JNDILookUp(this.getClass(), PersonService.class);

        facilityId = createFacility();
        facilityInternalLocationId = createFacilityInternalLocation();

        personId = createPerson();
        personIdentityId = createPersonalIdenity();
        staffId = createStaff();
        supervisionId = createSupervision();

    }

    private void clientlogin(String user, String password) {
        try {
            SecurityClient client = SecurityClientFactory.getSecurityClient();
            client.logout();
            client.setSimple(user, password);
            client.login();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testGetCCFMetaJSON() {
        clientlogin("devtest", "devtest");
        String ret = null;
        String eventType = "CASENOTE";
        String language = "en";
        ret = service.getCCFMetaForEventLog(uc, eventType, language);
        assert (ret != null);
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testGetCCFMetaDTO() {
        clientlogin("devtest", "devtest");
        ClientCustomFieldMetaType ret = null;
        String eventType = "EventLog";
        ret = service.getClientCustomFieldMetaForEventLog(uc, eventType, "Narrative");
        assert (ret != null);
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testGetAllCCFMetaDTOs() {
        clientlogin("devtest", "devtest");
        List<ClientCustomFieldMetaType> ret = new ArrayList<ClientCustomFieldMetaType>();
        String eventType = "EventLog";
        ret = service.getAllCCFMetaForEventLog(uc, eventType);
        assert (ret != null);
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testSaveCCFValue() {
        clientlogin("devtest", "devtest");
        String eventType = "EventType";
        List<ClientCustomFieldMetaType> metaTypes = service.getAllCCFMetaForEventLog(uc, eventType);
        if (metaTypes == null || metaTypes.isEmpty()) {
            System.out.println("NO Meta type defined for Event Log...");
        } else {
            for (ClientCustomFieldMetaType meta : metaTypes) {
                ClientCustomFieldValueType valueType = new ClientCustomFieldValueType();
                valueType.setCcfId(meta.getCcfId());
                valueType.setDtoBinding(meta.getDtoBinding());
                valueType.setIdBinding(1L);
                valueType.setName(meta.getName());
                valueType.setValue("ABC");

                ClientCustomFieldValueType retValue = service.saveCCFMetaValue(uc, valueType);
            }
        }
    }

    @AfterClass
    public void afterClass() {
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testSaveCCFValueInBulk() {
        clientlogin("devtest", "devtest");
        String eventType = "EventType";
        List<ClientCustomFieldMetaType> metaTypes = service.getAllCCFMetaForEventLog(uc, eventType);
        List<ClientCustomFieldValueType> values = new ArrayList<ClientCustomFieldValueType>();
        if (metaTypes == null || metaTypes.isEmpty()) {
            System.out.println("NO Meta type defined for Event Log...");
        } else {
            for (ClientCustomFieldMetaType meta : metaTypes) {
                ClientCustomFieldValueType valueType = new ClientCustomFieldValueType();
                valueType.setCcfId(meta.getCcfId());
                valueType.setDtoBinding(meta.getDtoBinding());
                valueType.setIdBinding(1L);
                valueType.setName(meta.getName());
                valueType.setValue("ABC");
                values.add(valueType);
            }
            List<ClientCustomFieldValueType> retValues = service.saveCCFMetaValues(uc, values);
        }
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testUpdateCCFValueInBulk() {
        clientlogin("devtest", "devtest");
        String eventType = "EventType";
        List<ClientCustomFieldMetaType> metaTypes = service.getAllCCFMetaForEventLog(uc, eventType);
        List<ClientCustomFieldValueType> values = new ArrayList<ClientCustomFieldValueType>();
        if (metaTypes == null || metaTypes.isEmpty()) {
            System.out.println("NO Meta type defined for Event Log...");
        } else {
            for (ClientCustomFieldMetaType meta : metaTypes) {
                ClientCustomFieldValueType valueType = new ClientCustomFieldValueType();
                valueType.setCcfId(meta.getCcfId());
                valueType.setDtoBinding(meta.getDtoBinding());
                valueType.setIdBinding(1L);
                valueType.setName(meta.getName());
                valueType.setValue("ABC");
                values.add(valueType);
            }
            List<ClientCustomFieldValueType> retValues = service.saveCCFMetaValues(uc, values);
            System.out.println("CCF Values Created is ====>" + retValues.toString());
            values = new ArrayList<ClientCustomFieldValueType>();
            for (ClientCustomFieldValueType value : retValues) {
                value.setValue("This is updated Value....");
                values.add(value);
            }
            retValues = service.updateCCFMetaValues(uc, values);
        }
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testCreateCaseNote() {

        clientlogin("devtest", "devtest");
        CaseNoteType ret = new CaseNoteType();
        CaseNoteType caseNoteType = getCaseNoteTypeInstance();
        // CodeType eventType = new CodeType(ReferenceSets.EventType.value(),
        // EventType.CASENOTE.code());
        // caseNoteType.setCaseNoteNumber(service.getNextEventLogNumber(uc,
        // eventType));
        ret = service.createCaseNote(uc, caseNoteType);

        assert (ret != null);
        assert (ret.getEventIdentification() > 0);

        // Create legal note
        caseNoteType = getCaseNoteTypeInstance();
        caseNoteType.setNoteType("LEG");

        Long caseId = legalTest.createCase(supervisionId).getCaseInfoId();
        LegalNoteType legalNote;
/*TODO: will add back in future.
 * 		LegalNoteType legalNote = new LegalNoteType(1L, 1L, 1L, 1L, 1L, 1L);
		caseNoteType.setLegalNoteType(legalNote);
		ret = service.createCaseNote(uc, caseNoteType);
		assert (ret != null);
		legalNote = ret.getLegalNoteType();
		assert (legalNote.getLegalBailId().equals(1L));
		assert (legalNote.getLegalCaseId().equals(1L));
		assert (legalNote.getLegalChargeId().equals(1L));
		assert (legalNote.getLegalEventId().equals(1L));
		assert (legalNote.getLegalOrderId().equals(1L));
		assert (legalNote.getLegalSentenceId().equals(1L));*/

        // Create legal note only mandatory field has value
        caseNoteType = getCaseNoteTypeInstance();
        caseNoteType.setNoteType("LEG");
        legalNote = new LegalNoteType(caseId, null, null, null, null, null);
        caseNoteType.setLegalNoteType(legalNote);
        ret = service.createCaseNote(uc, caseNoteType);
        assert (ret != null);
        legalNote = ret.getLegalNoteType();
        assert (legalNote.getLegalCaseId().equals(caseId));
        assert (legalNote.getLegalBailId() == null);
        assert (legalNote.getLegalChargeId() == null);
        assert (legalNote.getLegalEventId() == null);
        assert (legalNote.getLegalOrderId() == null);
        assert (legalNote.getLegalSentenceId() == null);

        // Create legal note negative test
        caseNoteType = getCaseNoteTypeInstance();
        caseNoteType.setNoteType("LEG");
        legalNote = new LegalNoteType(null, 1L, 1L, 1L, 1L, 1L);
        caseNoteType.setLegalNoteType(legalNote);
        try {
            ret = service.createCaseNote(uc, caseNoteType);
            assert (ret == null);
        } catch (Exception e) {
            assert (e instanceof InvalidInputException);
        }
    }

    @Test
    public void testEditCaseNote() {
        clientlogin("devtest", "devtest");
        CaseNoteType ret = new CaseNoteType();
        CaseNoteType caseNoteType = getCaseNoteTypeInstance();
        // CodeType eventType = new CodeType(ReferenceSets.EventType.value(),
        // EventType.CASENOTE.code());
        // caseNoteType.setCaseNoteNumber(service.getNextEventLogNumber(uc,
        // eventType));
        ret = service.createCaseNote(uc, caseNoteType);
        assert (ret != null);
        ret.setNarrativeDesc("This is an edited Text....");
        CaseNoteType retEdit = service.editCaseNote(uc, ret);
        assert (retEdit != null);

    }

    @Test
    public void testDeleteCaseNote() {
        clientlogin("devtest", "devtest");
        CaseNoteType ret = new CaseNoteType();
        CaseNoteType caseNoteType = getCaseNoteTypeInstance();
        ret = service.createCaseNote(uc, caseNoteType);

        CaseNoteAmendType amendCase = new CaseNoteAmendType();

        amendCase.setDescription("This is an amended CaseNote for testing Delete Method...");

        // amendCase.setAssociations(new HashSet<AssociationType>());
        // for (AssociationType asso : ret.getAssociations())
        // {
        // if
        // (asso.getToClass().equalsIgnoreCase(AssociationToClass.ACTIVITY.value()))
        // continue;
        // amendCase.getAssociations().add(asso);
        // }

        amendCase.setEventDate(ret.getEventDate());
        amendCase.setEventTime(ret.getEventTime());
        amendCase.setNoteSubType(ret.getNoteSubType());
        amendCase.setNoteType(ret.getNoteType());

        CaseNoteType amendedCaseNote = service.amendCaseNote(uc, ret.getEventIdentification(), amendCase);

        assert (amendedCaseNote != null);

        Long retCode = service.deleteCaseNote(uc, ret.getEventIdentification());

        assert (retCode > 0);

    }

    @Test
    public void testDefect2702() {
        clientlogin("devtest", "devtest");
        CaseNoteType ret = new CaseNoteType();
        CaseNoteType caseNoteType = getCaseNoteTypeInstance();
        ret = service.createCaseNote(uc, caseNoteType);

        CaseNoteAmendType amendCase = new CaseNoteAmendType();

        amendCase.setDescription("This is an amended CaseNote for testing Delete Method...");
        // amendCase.setAssociations(new HashSet<AssociationType>());
        // for (AssociationType asso : ret.getAssociations())
        // {
        // if
        // (asso.getToClass().equalsIgnoreCase(AssociationToClass.ACTIVITY.value()))
        // continue;
        // amendCase.getAssociations().add(asso);
        // }

        amendCase.setEventDate(ret.getEventDate());
        amendCase.setEventTime(ret.getEventTime());
        amendCase.setNoteSubType(ret.getNoteSubType());
        amendCase.setNoteType(ret.getNoteType());

        CaseNoteType amendedCaseNote = service.amendCaseNote(uc, ret.getEventIdentification(), amendCase);

        assert (amendedCaseNote != null);

        Long retCode = service.deleteCaseNote(uc, amendedCaseNote.getEventIdentification());

        assert (retCode > 0);

        CaseNoteSearchType searchOriginalCN = new CaseNoteSearchType();
        searchOriginalCN.setReportingStaff(staffId);
        searchOriginalCN.setDescription("T E S T");
        Set<CaseNoteType> caseNotes = service.search3CaseNotes(uc, searchOriginalCN, 0l, 2l, null);

        assert (caseNotes != null && caseNotes.size() > 0l);

    }

    @Test
    public void testDeleteShiftLog() {
        clientlogin("devtest", "devtest");

        ShiftLogType ret = new ShiftLogType();
        Date eventDate = new Date();
        Calendar cal = Calendar.getInstance();
        Date eventTime = cal.getTime();

        ShiftLogType r = service.createShiftLog(uc, getShiftLogTypeInstance());

        ShiftLogAmendType shiftLogAmend = new ShiftLogAmendType();
        shiftLogAmend.setEventDate(eventDate);
        shiftLogAmend.setEventTime(eventTime);
        shiftLogAmend.setActivityType(Constants.ActivityType.CELL.code());
        shiftLogAmend.setDescription("This is an Amended Shift log record");

        shiftLogAmend.setFacilityId(facilityId);
        shiftLogAmend.setLocationId(facilityInternalLocationId);
        // Set<Long> supIds = new HashSet<Long>();
        // supIds.add(21L);
        // Set<Long> stfIds = new HashSet<Long>();
        // stfIds.add(1L);
        // shiftLogAmend.setSupervisionIds(supIds);
        // shiftLogAmend.setStaffIds(stfIds);

        Long originalShiftLog = r.getEventIdentification();
        ret = service.amendShiftLog(uc, originalShiftLog, shiftLogAmend);
        assert (ret != null);

        // Long retCode = service.deleteShiftLog(uc,
        // r.getEventIdentification());
        //
        // assert (retCode > 0);

    }

    @Test
    public void testCreateShiftLog() {
        clientlogin("devtest", "devtest");
        ShiftLogType ret = new ShiftLogType();
        ShiftLogType shiftLog = getShiftLogTypeInstance();
        ret = service.createShiftLog(uc, shiftLog);
        assert (ret != null);

    }

    @Test
    public void testAddPerson() {
        clientlogin("devtest", "devtest");
        ShiftLogType ret = new ShiftLogType();
        ShiftLogType shiftLog = getShiftLogTypeInstance();

        ret = service.createShiftLog(uc, shiftLog);
        assert (ret != null);

        log.error(" ret " + ret);

        PersonType person = new PersonType(staffId, "SUS", PersonTypes.Staff);
        ShiftLogType retAddPerson = service.addPersonToShiftLog(uc, ret.getEventIdentification(), person);

        log.error(" retAddPerson " + retAddPerson);

        assert (retAddPerson != null);

    }

    @Test
    public void testDeletePerson() {
        clientlogin("devtest", "devtest");
        ShiftLogType ret = new ShiftLogType();
        ShiftLogType shiftLog = getShiftLogTypeInstance();

        ret = service.createShiftLog(uc, shiftLog);
        assert (ret != null);

        PersonType person = new PersonType(staffId, Constants.RoleTypes.SUS.code(), PersonTypes.Staff);
        ShiftLogType retAddPerson = service.addPersonToShiftLog(uc, ret.getEventIdentification(), person);

        assert (retAddPerson != null);

        ShiftLogType retDelPerson = service.deletePersonFromShiftLog(uc, retAddPerson.getEventIdentification(), person);

        assert (retDelPerson != null);

    }

    @Test
    public void testEditShiftLog() {
        clientlogin("devtest", "devtest");
        ShiftLogType ret = new ShiftLogType();
        ShiftLogType shiftLog = getShiftLogTypeInstance();
        ret = service.createShiftLog(uc, shiftLog);
        assert (ret != null);

        ret.setNarrativeDesc("This is an Edited Shift log Text....");
        Set<Long> staffIds = new HashSet<Long>();
        staffIds.add(staffId);
        ret.setStaffIds(staffIds);
        ShiftLogType retEdit = service.editShiftLog(uc, ret);

        assert (retEdit != null);

    }

    @Test
    public void testAmendCaseNote() {

        clientlogin("devtest", "devtest");
        CaseNoteType ret = new CaseNoteType();
        CaseNoteAmendType caseNoteAmend = new CaseNoteAmendType();
        Date eventDate = new Date();
        Calendar cal = Calendar.getInstance();
        Date eventTime = cal.getTime();
        caseNoteAmend.setEventDate(eventDate);
        caseNoteAmend.setEventTime(eventTime);
        caseNoteAmend.setNoteType("CAU");
        caseNoteAmend.setNoteSubType("ACTIVE");
        caseNoteAmend.setDescription("This is an Amended Case Note record");
        // shiftLogAmend.getAssociations().add(new
        // AssociationType(AssociationToClass.SUPERVISION.value(), 21L));
        // shiftLogAmend.getAssociations().add(new
        // AssociationType(AssociationToClass.STAFF.value(), 1L));

        CaseNoteType r = service.createCaseNote(uc, getCaseNoteTypeInstance());

        Long originalCaseNote = r.getEventIdentification();
        ret = service.amendCaseNote(uc, originalCaseNote, caseNoteAmend);
        assert (ret != null);

    }

    @Test
    public void testAmendShiftLog() {

        clientlogin("devtest", "devtest");
        ShiftLogType ret = new ShiftLogType();
        ShiftLogAmendType shiftLogAmend = new ShiftLogAmendType();
        Date eventDate = new Date();
        Calendar cal = Calendar.getInstance();
        Date eventTime = cal.getTime();
        shiftLogAmend.setEventDate(eventDate);
        shiftLogAmend.setEventTime(eventTime);
        shiftLogAmend.setActivityType(Constants.ActivityType.CELL.code());
        shiftLogAmend.setDescription("This is an Amended Shift log record");
        shiftLogAmend.setFacilityId(facilityId);
        shiftLogAmend.setLocationId(facilityInternalLocationId);
        // shiftLogAmend.getAssociations().add(new
        // AssociationType(AssociationToClass.SUPERVISION.value(), 21L));
        // shiftLogAmend.getAssociations().add(new
        // AssociationType(AssociationToClass.STAFF.value(), 1L));

        ShiftLogType r = service.createShiftLog(uc, getShiftLogTypeInstance());
        Long originalShiftLog = r.getEventIdentification();
        ret = service.amendShiftLog(uc, originalShiftLog, shiftLogAmend);
        assert (ret != null);

    }

    @Test
    public void testDefect2743() {
        clientlogin("devtest", "devtest");

        // Create Shift Log
        ShiftLogType s = getShiftLogTypeInstance();

        Set<Long> supervisionIds = new HashSet<Long>();
        supervisionIds.add(supervisionId);

        s.setSupervisionIds(supervisionIds);

        ShiftLogType ret = service.createShiftLog(uc, s);
        assert (ret != null);

        // Amend Shift Log
        ShiftLogAmendType shiftLogAmend = new ShiftLogAmendType();
        Date eventDate = new Date();
        Calendar cal = Calendar.getInstance();
        Date eventTime = cal.getTime();
        shiftLogAmend.setEventDate(eventDate);
        shiftLogAmend.setEventTime(eventTime);
        shiftLogAmend.setActivityType(Constants.ActivityType.CELL.code());
        shiftLogAmend.setDescription("This is an Amended Shift log record");
        shiftLogAmend.setFacilityId(facilityId);
        shiftLogAmend.setLocationId(facilityInternalLocationId);

        Set<Long> sIds = new HashSet<Long>();

        Date dob = new Date(1985 - 1900, 3, 10);

        Long lng = new Random().nextLong();

        PersonIdentityType peter = new PersonIdentityType(null, createPerson(), null, null, "peter" + lng, "S" + lng, "S" + lng, "Pan" + lng, "i", dob, "M", null, null,
                "offenderNumTest1", null, null);

        // PersonIdentityType peter = createIndentity("Peter", "Pan", new
        // Date(84, 3, 10), "M");
        peter = personService.createPersonIdentityType(uc, peter, Boolean.FALSE, Boolean.FALSE);

        sIds.add(createSupervision(peter.getPersonIdentityId(), facilityId, true));

        shiftLogAmend.setSupervisionIds(sIds);

        ShiftLogType retAmend = service.amendShiftLog(uc, ret.getEventIdentification(), shiftLogAmend);
        assert (retAmend != null);

        Long lng2 = new Random().nextLong();

        PersonIdentityType peter2 = new PersonIdentityType(null, createPerson(), null, null, "peter" + lng2, "S2" + lng2, "S2" + lng2, "Pan1" + lng2, "i", dob, "M", null,
                null, "offenderNumTest2", null, null);

        // PersonIdentityType peter = createIndentity("Peter", "Pan", new
        // Date(84, 3, 10), "M");
        peter2 = personService.createPersonIdentityType(uc, peter2, Boolean.FALSE, Boolean.FALSE);

        sIds.add(createSupervision(peter2.getPersonIdentityId(), facilityId, true));

        retAmend = service.amendShiftLog(uc, ret.getEventIdentification(), shiftLogAmend);
        assert (retAmend != null);

    }

    @Test
    public void testCreateShiftLogForOffender() {
        clientlogin("devtest", "devtest");
        ShiftLogType ret = new ShiftLogType();
        ShiftLogType shiftLog = getShiftLogTypeInstance();
        // CodeType eventType = new CodeType(ReferenceSets.EventType.value(),
        // EventType.SHIFTLOG.code());
        // shiftLog.setShiftLogNumber(service.getNextEventLogNumber(uc,
        // eventType));
        Set<Long> supervisionIds = new HashSet<Long>();
        supervisionIds.add(supervisionId);
        shiftLog.setSupervisionIds(supervisionIds);

        ret = service.createShiftLog(uc, shiftLog);

        assert (ret != null);
    }

    @Test
    public void testSearchCaseNote() {
        clientlogin("devtest", "devtest");
        CaseNoteType caseNoteType = getCaseNoteTypeInstance();
        // CodeType eventType = new CodeType(ReferenceSets.EventType.value(),
        // EventType.CASENOTE.code());
        // caseNoteType.setCaseNoteNumber(service.getNextEventLogNumber(uc,
        // eventType));
        CaseNoteType retCN = service.createCaseNote(uc, caseNoteType);

        assert (retCN != null);
        assert (retCN.getEventIdentification() > 0);
        Set<CaseNoteType> ret = new HashSet<CaseNoteType>();

        CaseNoteSearchType search = new CaseNoteSearchType();
        // search.setNoteType(new CodeType(ReferenceSets.NoteType.value(),
        // "CAU"));
        // search.setFromEventDate(new Date());
        // Date frmEventTime = new Date();
        // frmEventTime.setHours(3);
        // frmEventTime.setMinutes(17);
        // frmEventTime.setSeconds(0);
        // search.setFromEventTime(frmEventTime);
        //
        // search.setToEventDate(new Date());
        // Date toEventTime = new Date();
        // toEventTime.setHours(20);
        // toEventTime.setMinutes(14);
        // toEventTime.setSeconds(0);
        // search.setToEventTime(toEventTime);
        // search.setOffNumber("7678");
        // search.setOffFirstName("abnb");
        // search.setCaseNoteNumber("CN-0.101010101010101012010*");
        // search.setReportingStaff(111L);
        search.setCaseNoteNumber(retCN.getCaseNoteNumber());
        ret = service.search3CaseNotes(uc, search, 0l, 2l, null);

        assert (ret != null);

        search = new CaseNoteSearchType();
        search.setDescription("t h i s");
        // search.setOffNumber("XYZ");
        ret = service.search3CaseNotes(uc, search, 0l, 2l, null);

        assert (ret != null);

        search = new CaseNoteSearchType();
        search.setDescription("c ase, note");
        // search.setOffFirstName("F33");
        ret = service.search3CaseNotes(uc, search, 0l, 2l, null);

        assert (ret != null);

    }

    @Test
    public void testSearchLegalNotes() {
        Long caseId = legalTest.createCase(supervisionId).getCaseInfoId();
        // Test search legal note by case id
        for (int i = 0; i < 10; i++) {
            // Create legal note only mandatory field has value
            CaseNoteType caseNoteType = getCaseNoteTypeInstance();
            caseNoteType.setNoteType("LEG");
            LegalNoteType legalNote = new LegalNoteType(caseId, null, null, null, null, null);
            caseNoteType.setLegalNoteType(legalNote);
            CaseNoteType ret1 = service.createCaseNote(uc, caseNoteType);
            assert (ret1 != null);
            legalNote = ret1.getLegalNoteType();
            assert (legalNote.getLegalCaseId().equals(caseId));
            /*assert (legalNote.getLegalChargeId().equals(3L));
            assert (legalNote.getLegalOrderId().equals(4L));
			assert (legalNote.getLegalEventId().equals(5L));
			assert (legalNote.getLegalBailId().equals(6L));
			assert (legalNote.getLegalSentenceId().equals(7L));*/
        }

        CaseNoteSearchType search = new CaseNoteSearchType();
        LegalNoteSearchType legalSearch = new LegalNoteSearchType();
        legalSearch.setLegalCaseId(caseId);
        search.setLegalNoteSearchType(legalSearch);
        Set<CaseNoteType> ret = service.search3CaseNotes(uc, search, null, null, null);
        assert (ret != null);
        assert (ret.size() == 10);

        // Test search legal note by bail id
        /*search = new CaseNoteSearchType();
        legalSearch = new LegalNoteSearchType();
		legalSearch.setLegalBailId(6L);
		search.setLegalNoteSearchType(legalSearch);
		ret = service.search3CaseNotes(uc, search, null, null, null);
		assert (ret != null);
		assert (ret.size() == 10);*/

        // Test search legal note by case id which is not exist.
        search = new CaseNoteSearchType();
        legalSearch = new LegalNoteSearchType();
        legalSearch.setLegalCaseId(99L);
        search.setLegalNoteSearchType(legalSearch);
        ret = service.search3CaseNotes(uc, search, null, null, null);
        assert (ret != null);
        assert (ret.size() == 0);
    }

    @Test
    public void testGetAmendmentsForEvent() {
        clientlogin("devtest", "devtest");

        // Verify Amendment Search for case Notes
        CaseNoteType ret = new CaseNoteType();
        CaseNoteAmendType caseNoteAmend = new CaseNoteAmendType();
        Date eventDate = new Date();
        Calendar cal = Calendar.getInstance();
        Date eventTime = cal.getTime();
        caseNoteAmend.setEventDate(eventDate);
        caseNoteAmend.setEventTime(eventTime);
        caseNoteAmend.setNoteType("CAU");
        caseNoteAmend.setNoteSubType("ACTIVE");
        caseNoteAmend.setDescription("This is an Amended Case Note record 1");

        CaseNoteType r = service.createCaseNote(uc, getCaseNoteTypeInstance());
        Long originalCaseNote = r.getEventIdentification();
        ret = service.amendCaseNote(uc, originalCaseNote, caseNoteAmend);
        caseNoteAmend.setDescription("This is an Amended Case Note record 2");
        ret = service.amendCaseNote(uc, originalCaseNote, caseNoteAmend);
        caseNoteAmend.setDescription("This is an Amended Case Note record 3");
        ret = service.amendCaseNote(uc, originalCaseNote, caseNoteAmend);

        String eventType = EventType.CASENOTE.code();
        List<EventLogType> amendedSearch = service.getAmendmentsForEvent(uc, r.getEventIdentification(), eventType);
        assert (amendedSearch != null && amendedSearch.size() > 0);

        // Verify Amendment Search for Shift logs
        ShiftLogType retShift = new ShiftLogType();
        ShiftLogAmendType shiftLogAmend = new ShiftLogAmendType();
        eventDate = new Date();
        cal = Calendar.getInstance();
        eventTime = cal.getTime();
        eventTime.setHours(4);
        eventTime.setMinutes(06);
        eventTime.setSeconds(00);
        shiftLogAmend.setEventDate(eventDate);
        shiftLogAmend.setEventTime(eventTime);
        shiftLogAmend.setActivityType(Constants.ActivityType.CELL.code());
        shiftLogAmend.setDescription("This is an Amended Shift log record 1");
        shiftLogAmend.setFacilityId(facilityId);
        shiftLogAmend.setLocationId(facilityInternalLocationId);
        Set<PersonType> persons = new HashSet<PersonType>();
        persons.add(new PersonType(supervisionId, "OFFENDER", PersonTypes.Offender));
        persons.add(new PersonType(personId, "WITNESS", PersonTypes.Person));
        shiftLogAmend.setPersons(persons);

        ShiftLogType s = service.createShiftLog(uc, getShiftLogTypeInstance());
        Long originalShiftLog = s.getEventIdentification();
        retShift = service.amendShiftLog(uc, originalShiftLog, shiftLogAmend);

        shiftLogAmend.setDescription("This is an Amended Shift log record 2");
        retShift = service.amendShiftLog(uc, originalShiftLog, shiftLogAmend);
        shiftLogAmend.setDescription("This is an Amended Shift log record 3");
        retShift = service.amendShiftLog(uc, originalShiftLog, shiftLogAmend);

        eventType = EventType.SHIFTLOG.code();
        amendedSearch = service.getAmendmentsForEvent(uc, s.getEventIdentification(), eventType);
        assert (amendedSearch != null && amendedSearch.size() >= 0);

    }

    @Test
    public void testCR2903CaseNotes() {
        clientlogin("devtest", "devtest");

        // Verify CR for Case Notes
        testSearchCaseNote();
        testDeleteCaseNote();

        // Verify CR for Shift Logs
        testSearchShiftLog();
        testDeleteShiftLog();
    }

    @Test
    public void testSearchShiftLog() {
        clientlogin("devtest", "devtest");
        // reset();
        ShiftLogType shiftLog = getShiftLogTypeInstance();
        shiftLog.setNarrativeDesc("Shift Log Type search testing....");
        service.createShiftLog(uc, shiftLog);

        Set<ShiftLogType> ret = new HashSet<ShiftLogType>();

        // Search Shift Log for Event Log Date-Time Range
        ShiftLogSearchType search = new ShiftLogSearchType();

        Date frmEventTime = new Date();
        frmEventTime.setHours(10);
        frmEventTime.setMinutes(00);
        frmEventTime.setSeconds(00);
        search.setFromEventDate(new Date(113, 4, 27));
        search.setFromEventTime(frmEventTime);
        Date toEventTime = new Date();
        toEventTime.setHours(20);
        toEventTime.setMinutes(00);
        toEventTime.setSeconds(0);
        search.setToEventDate(new Date(113, 4, 27));
        search.setToEventTime(toEventTime);
        ret = service.search3ShiftLogs(uc, search, 0l, 2l, null, null);
        assert (ret != null);

        // Search Shift Log for Facility and activity Type
        search = new ShiftLogSearchType();
        search.setFacilityId(1L);
        search.setActivityType(Constants.ActivityType.CELL.code());
        ret = service.search3ShiftLogs(uc, search, 0l, 2l, null, null);
        assert (ret != null);

        // Search Shift Log for Event date time range and narrative text
        // keyword search
        search = new ShiftLogSearchType();
        /*
		 * frmEventTime = new Date(); frmEventTime.setHours(1);
		 * frmEventTime.setMinutes(1); frmEventTime.setSeconds(0);
		 * search.setFromEventTime(frmEventTime);
		 */
        search.setDescription("s h ift,");
        ret = service.search3ShiftLogs(uc, search, 0l, 2l, null, null);
        assert (ret != null);

        // Negative test for narrative text keyword search. No record should be
        // returned.
        search = new ShiftLogSearchType();
		/*
		 * frmEventTime = new Date(); frmEventTime.setHours(1);
		 * frmEventTime.setMinutes(1); frmEventTime.setSeconds(0);
		 * search.setFromEventTime(frmEventTime);
		 */
        search.setDescription("111, 121");
        ret = service.search3ShiftLogs(uc, search, 0l, 2l, null, null);
        assert (ret == null || ret.isEmpty());
    }

    private ShiftLogType getShiftLogTypeInstance() {
        ShiftLogType shiftLogType = new ShiftLogType();
        // caseNoteType.setCaseNoteNumber(service.getNextEventLogNumber(uc, new
        // CodeType(Refer)));
        Date eventDate = new Date();
        Calendar cal = Calendar.getInstance();
        Date eventTime = cal.getTime();
        eventTime.setHours(4);
        eventTime.setMinutes(06);
        eventTime.setSeconds(00);
        shiftLogType.setEventDate(eventDate);
        shiftLogType.setEventTime(eventTime);
        shiftLogType.setActivityType(Constants.ActivityType.CELL.code());
        shiftLogType.setNarrativeDesc("This is Test ShiftLog");
        shiftLogType.setFacilityId(facilityId);
        shiftLogType.setLocationId(facilityInternalLocationId);
        shiftLogType.setReportingStaff(staffId);
        Set<PersonType> persons = new HashSet<PersonType>();
        persons.add(new PersonType(101L, Constants.RoleTypes.OFF.code(), PersonTypes.Staff));
        persons.add(new PersonType(101L, Constants.RoleTypes.VIC.code(), PersonTypes.Offender));
        shiftLogType.setPersons(persons);
        return shiftLogType;
    }

    private CaseNoteType getCaseNoteTypeInstance() {
        CaseNoteType caseNoteType = new CaseNoteType();
        // caseNoteType.setCaseNoteNumber(service.getNextEventLogNumber(uc, new
        // CodeType(Refer)));
        Date eventDate = new Date();
        Date eventTime = new Date();
        eventTime.setHours(1);
        eventTime.setMinutes(1);
        eventTime.setSeconds(1);
        caseNoteType.setEventDate(eventDate);
        caseNoteType.setEventTime(eventTime);
        caseNoteType.setNarrativeDesc("This is Test Case Note");
        caseNoteType.setNoteType("CAU");
        caseNoteType.setNoteSubType("ACTIVE");
        caseNoteType.setReportingStaff(staffId);
        caseNoteType.setSupervisionId(supervisionId);

        return caseNoteType;
    }

    @SuppressWarnings("unused")
    private Date createFutureDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, day);
        Date futureDate = cal.getTime();
        return futureDate;
    }

    @SuppressWarnings("unused")
    private Date createPastDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, -day);
        Date pastDate = cal.getTime();
        return pastDate;
    }

    @Test
    public void eventLogActivitySearch() {

        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, -10);
        Date pastDate = cal.getTime();

        CaseNoteSearchType caseNoteSearchType = new CaseNoteSearchType();
        caseNoteSearchType.setFromEventDate(pastDate);
        caseNoteSearchType.setToEventDate(now);

        Set<CaseNoteType> caseNoteTypes = service.search3CaseNotes(uc, caseNoteSearchType, null, null, null);

        assert (caseNoteTypes != null);

    }

    @Test
    public void testSaveNarrativeText() {

        String codeType = NoteType.CAU.code();
        String codeSubType = NoteSubType.ACTIVE.code();
        String ret = service.getNarrativeTemplate(uc, EventType.CASENOTE, codeType, codeSubType);

        NarrativeTextType narrativeTextType = new NarrativeTextType();
        narrativeTextType.setCodeType(codeType);
        narrativeTextType.setEventType(EventType.CASENOTE);
        narrativeTextType.setCodeSubType(codeSubType);
        narrativeTextType.setTemplate("template");
        NarrativeTextType narrativeTextTypeResult = service.saveNarrativeText(uc, narrativeTextType);
        if (ret == null) {
            assertNotNull(narrativeTextTypeResult);
        } else {
            assert (narrativeTextTypeResult == null);
        }

    }

    @Test
    public void testUpdateNarrativeText() {
        String codeType = NoteType.GEN.code();
        String codeSubType = NoteSubType.FAIL.code();
        NarrativeTextType narrativeTextType = new NarrativeTextType();
        narrativeTextType.setCodeType(codeType);
        narrativeTextType.setEventType(EventType.CASENOTE);
        narrativeTextType.setCodeSubType(codeSubType);
        narrativeTextType.setTemplate("template");
        NarrativeTextType narrativeTextTypeResult = service.saveNarrativeText(uc, narrativeTextType);
        assertNotNull(narrativeTextTypeResult);
        narrativeTextTypeResult.setTemplate("template-update");
        NarrativeTextType updateResult = service.updateNarrativeText(uc, narrativeTextTypeResult);
        assertNotNull(updateResult);
    }

    @Test(dependsOnMethods = "testSaveNarrativeText")
    public void testGetNarrativeText() {
        List<NarrativeTextType> narrativeTextTypes = service.getNarrativeTextByEventType(uc, EventType.CASENOTE);
        assertNotNull(narrativeTextTypes);

    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void testGetNarrativeTemplate() {

        clientlogin("devtest", "devtest");
        String codeType = NoteType.CAU.code();

        String codeSubType = NoteSubType.ACTIVE.code();
        String ret = service.getNarrativeTemplate(uc, EventType.CASENOTE, codeType, codeSubType);
        assert (ret != null);

        codeType = ActivityType.INV.code();
        ret = service.getNarrativeTemplate(uc, EventType.SHIFTLOG, codeType, null);
        assert (ret == null);
    }

    @Test(dependsOnMethods = "testSaveNarrativeText")
    public void testDeleteGetNarrativeText() {
        List<NarrativeTextType> narrativeTextTypeResults = service.getNarrativeTextByEventType(uc, EventType.CASENOTE);
        assertNotNull(narrativeTextTypeResults);
        assert (!narrativeTextTypeResults.isEmpty());
        NarrativeTextType fromDb = narrativeTextTypeResults.get(0);
        service.deleteNarrativeText(uc, fromDb.getNarrativeTextID());
    }

    @Test
    public void testTransactLog() {
        TransactLogType log = new TransactLogType();
        Date eventDate = new Date();
        Date eventTime = new Date();
        eventTime.setHours(1);
        eventTime.setMinutes(1);
        eventTime.setSeconds(1);
        log.setEventDate(eventDate);
        log.setEventTime(eventTime);
        log.setNarrativeDesc("This is a transactLog");
        log.setReportingStaff(staffId);

        log.setEntityId(1L);
        log.setTransactLogEntityTypes(TransactLogEntityTypes.Property);
        log.setTransactLogType("REGISTERED");

        assertNotNull(service.createTransactLog(uc, log));
        List<TransactLogType> list = service.getTransactLogForEntity(uc, 1L, TransactLogEntityTypes.Property);
        assertNotNull(list);
        assert (!list.isEmpty());
    }

    @Test
    public void testAddPhoto() {
        clientlogin("devtest", "devtest");
        CaseNoteType ret = new CaseNoteType();
        CaseNoteType caseNoteType = getCaseNoteTypeInstance();
        ret = service.createCaseNote(uc, caseNoteType);
        assert (ret != null);

        // Set/get photo
        byte[] img1 = new byte[] { 1, 1, 1, 1 };

        EventLogImageType eventLogImageType = new EventLogImageType();
        eventLogImageType.setEventLogId(ret.getEventIdentification());
        eventLogImageType.setDefaultImage(true);
        eventLogImageType.setStaff(staffId);
        eventLogImageType.setImageNumber("1234");
        eventLogImageType.setImageDescription("eventLogImageDescription");
        eventLogImageType.setUploadedDate(new Date());
        eventLogImageType.getImage().setPhoto(img1);
        eventLogImageType = service.addPhoto(uc, eventLogImageType);
        assert (eventLogImageType != null);
    }

    @Test
    public void testUpdatePhoto() {
        clientlogin("devtest", "devtest");
        CaseNoteType ret = new CaseNoteType();
        CaseNoteType caseNoteType = getCaseNoteTypeInstance();
        ret = service.createCaseNote(uc, caseNoteType);
        assert (ret != null);

        // Set/get photo
        byte[] img1 = new byte[] { 1, 1, 1, 1 };
        byte[] img2 = new byte[] { 1, 1, 1, 1 };

        EventLogImageType eventLogImageType = new EventLogImageType();
        eventLogImageType.setEventLogId(ret.getEventIdentification());
        eventLogImageType.setDefaultImage(true);
        eventLogImageType.setStaff(staffId);
        eventLogImageType.setImageNumber("1234");
        eventLogImageType.setImageDescription("eventLogImageDescription");
        eventLogImageType.setUploadedDate(new Date());
        eventLogImageType.getImage().setPhoto(img1);
        eventLogImageType = service.addPhoto(uc, eventLogImageType);
        assert (eventLogImageType != null);

        eventLogImageType.getImage().setPhoto(img2);
        EventLogImageType retEdit = service.updatePhoto(uc, eventLogImageType);
        assert (retEdit != null);
    }

    @Test
    public void testDeletePhoto() {
        clientlogin("devtest", "devtest");
        CaseNoteType ret = new CaseNoteType();
        CaseNoteType caseNoteType = getCaseNoteTypeInstance();
        ret = service.createCaseNote(uc, caseNoteType);
        assert (ret != null);

        // Set/get photo
        byte[] img1 = new byte[] { 1, 1, 1, 1 };

        EventLogImageType eventLogImageType = new EventLogImageType();
        eventLogImageType.setEventLogId(ret.getEventIdentification());
        eventLogImageType.setDefaultImage(true);
        eventLogImageType.setStaff(staffId);
        eventLogImageType.setImageNumber("1234");
        eventLogImageType.setImageDescription("eventLogImageDescription");
        eventLogImageType.setUploadedDate(new Date());
        eventLogImageType.getImage().setPhoto(img1);
        eventLogImageType = service.addPhoto(uc, eventLogImageType);
        assert (eventLogImageType != null);

        Long retCode = service.deletePhoto(uc, eventLogImageType.getEventLogImageId());
        assert (retCode > 0);
    }

    @Test
    public void testGetPhotos() {
        clientlogin("devtest", "devtest");
        CaseNoteType ret = new CaseNoteType();
        CaseNoteType caseNoteType = getCaseNoteTypeInstance();
        ret = service.createCaseNote(uc, caseNoteType);
        assert (ret != null);
        assert (ret.getEventIdentification() > 0);

        // Set/get photo
        byte[] img1 = new byte[] { 1, 1, 1, 1 };

        EventLogImageType eventLogImageType = new EventLogImageType();
        eventLogImageType.setEventLogId(ret.getEventIdentification());
        eventLogImageType.setDefaultImage(true);
        eventLogImageType.setStaff(staffId);
        eventLogImageType.setImageNumber("1234");
        eventLogImageType.setImageDescription("eventLogImageDescription");
        eventLogImageType.setUploadedDate(new Date());
        eventLogImageType.getImage().setPhoto(img1);
        eventLogImageType = service.addPhoto(uc, eventLogImageType);
        eventLogImageType = service.addPhoto(uc, eventLogImageType);
        eventLogImageType = service.addPhoto(uc, eventLogImageType);
        List<EventLogImageType> list = new ArrayList<EventLogImageType>();

        list = service.getPhotos(uc, ret.getEventIdentification());
        assert (list != null);
    }

    @Test
    public void testGetPhoto() {
        clientlogin("devtest", "devtest");
        CaseNoteType ret = new CaseNoteType();
        CaseNoteType caseNoteType = getCaseNoteTypeInstance();
        ret = service.createCaseNote(uc, caseNoteType);
        assert (ret != null);
        assert (ret.getEventIdentification() > 0);
        // Set/get photo
        byte[] img1 = new byte[] { 1, 1, 1, 1 };

        EventLogImageType eventLogImageType = new EventLogImageType();
        eventLogImageType.setEventLogId(ret.getEventIdentification());
        eventLogImageType.setDefaultImage(true);
        eventLogImageType.setStaff(staffId);
        eventLogImageType.setImageNumber("1234");
        eventLogImageType.setImageDescription("eventLogImageDescription");
        eventLogImageType.setUploadedDate(new Date());
        eventLogImageType.getImage().setPhoto(img1);
        eventLogImageType = service.addPhoto(uc, eventLogImageType);

        EventLogImageType photo = service.getPhoto(uc, eventLogImageType.getEventLogImageId());
        assert (photo != null);
    }

    private Long createFacility() {
        Long lng = new Random().nextLong();

        Facility ret = null;
        String category = new String("CUSTODY");

        Facility facility = new Facility(-1L, "FacilityCodeIT" + lng, "FacilityName", new Date(), category, null, null, null, null, false);
        ret = facService.create(uc, facility);
        assertNotEquals(ret, null);
        return ret.getFacilityIdentification();
    }

    private Long createFacilityInternalLocation() {
        String phyCode = "PC01";
        String description = "physical location root";
        String level = "FACILITY";
        FacilityInternalLocation pl = createFacilityInternalLocation(level, phyCode, description);
        pl.getComments().add(createComment("root comment"));

        LocationAttributeType prop = new LocationAttributeType();
        prop.getOffenderCategories().add("SUIC");
        pl.setLocationAttribute(prop);
        FacilityInternalLocation ret = facilityInternalLocationService.create(uc, pl, UsageCategory.HOU);
        assertNotNull(ret);
        log.info(String.format("ret = %s", ret));
        return ret.getId();
    }

    private FacilityInternalLocation createFacilityInternalLocation(String level, String code, String description) {
        FacilityInternalLocation pt = new FacilityInternalLocation();
        pt.setFacilityId(createFacility());
        pt.setHierarchyLevel(level);
        pt.setLocationCode(code);
        pt.setDescription(description);

        pt.getNonAssociations().add("TOTAL");
        pt.getNonAssociations().add("STG");

        return pt;
    }

    public Long createStaff() {
        StaffType ret = null;
        Set<StaffPosition> staffPositions = new HashSet<>();
        staffPositions.add(new StaffPosition("CPT"));
        staffPositions.add(new StaffPosition("DEP"));
        Date startDate = new Date(2012 - 1900, 2, 1); // March 1st
        Date endDate = new Date(2912 - 1900, 11, 30);
        String category = new String("CON");
        String reason = new String("SICK");

        StaffType staff = new StaffType(null, personIdentityId, category, staffPositions, reason, startDate, endDate, null);
        staff.setPersonId(personId);
        //
        ret = personService.getStaff(uc, personService.createStaff(uc, staff));
        assert (ret != null);
        log.info(String.format("ret = %s", ret));
        return ret.getStaffID();

    }

    // Helper Method
    private Long createPersonalIdenity() {
        Long lng = new Random().nextLong();

        Date dob = new Date(1985 - 1900, 3, 10);
        PersonIdentityType peter = new PersonIdentityType(null, personId, null, null, "peter" + lng, "S" + lng, "S" + lng, "Pan" + lng, "i", dob, "M", null, null,
                "offenderNumTest" + lng, null, null);

        // PersonIdentityType peter = createIndentity("Peter", "Pan", new
        // Date(84, 3, 10), "M");
        peter = personService.createPersonIdentityType(uc, peter, Boolean.FALSE, Boolean.FALSE);
        return peter.getPersonIdentityId();
    }

    private Long createPerson() {

        syscon.arbutus.product.services.person.contract.dto.PersonType instanceForTest = ContractTestUtil.createWellFormedPerson();
        instanceForTest.setActiveStatus("ACTIVE");    //new CodeType(ReferenceSet.PERSONSTATUS.name(),
        instanceForTest = personService.createPersonType(uc, instanceForTest);
        return instanceForTest.getPersonIdentification();
    }

    private Long createSupervision() {

        // Create Person Identity

        Long supervisionId = createSupervision(personIdentityId, facilityId, true);
        return supervisionId;

    }

    public Long createSupervision(Long personIdentityId, Long facilityId, boolean isActive) {

        SupervisionType supervision = new SupervisionType();

        Date startDate = new Date();
        Date endDate = null;

        if (!isActive) {
            endDate = new Date();
        }

        supervision.setSupervisionDisplayID("ID" + personIdentityId);
        supervision.setSupervisionStartDate(startDate);
        supervision.setSupervisionEndDate(endDate);
        supervision.setPersonIdentityId(personIdentityId);
        supervision.setFacilityId(facilityId);

        SupervisionType ret = supService.create(uc, supervision);
        assert (ret != null);

        return ret.getSupervisionIdentification();
    }

    //@Test(dependsOnMethods = { "testJNDILookup" })
    public void testStgCreateCaseNote() {

        clientlogin("devtest", "devtest");
        CaseNoteType ret = new CaseNoteType();

        // Create STg note
        CaseNoteType caseNoteType = getStgCaseNoteTypeInstance();

        ret = service.createCaseNote(uc, caseNoteType);
        assert (ret != null);
        StgCaseNoteType stgcaseNote = ret.getStgCaseNoteType();
        assert (ret.getEventIdentification() > 0);
        assert (stgcaseNote.getStgId() != null);
    }

    private CaseNoteType getStgCaseNoteTypeInstance() {
        CaseNoteType caseNoteType = new CaseNoteType();
        // caseNoteType.setCaseNoteNumber(service.getNextEventLogNumber(uc, new
        // CodeType(Refer)));
        Date eventDate = new Date();
        Date eventTime = new Date();
        eventTime.setHours(1);
        eventTime.setMinutes(1);
        eventTime.setSeconds(1);
        caseNoteType.setEventDate(eventDate);
        caseNoteType.setEventTime(eventTime);
        caseNoteType.setNarrativeDesc("This is STG test Case Note");
        caseNoteType.setNoteType("DISTURBANCE");
        caseNoteType.setNoteSubType("RIOT");
        caseNoteType.setReportingStaff(staffId);
        caseNoteType.setSupervisionId(null);
        StgCaseNoteType stgCaseNoteType = new StgCaseNoteType();
        stgCaseNoteType.setStgId(1L);
        caseNoteType.setStgCaseNoteType(stgCaseNoteType);

        return caseNoteType;
    }

}
