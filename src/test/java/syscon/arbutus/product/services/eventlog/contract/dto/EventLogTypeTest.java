/**
 * © 2013 by Syscon Justice Systems. All rights reserved.
 * Copying and distribution, any modifications or adaptation of the contents, or creation of derivative
 * works are not allowed without the express and prior written consent of Syscon Justice Systems.
 */
package syscon.arbutus.product.services.eventlog.contract.dto;

import java.util.Date;

import org.testng.annotations.Test;
import syscon.arbutus.product.services.realization.util.BeanHelper;

public class EventLogTypeTest {
    @Test
    public void isWellFormed() {
        EventLogType data = new EventLogType();

        data.setEventDate(new Date());

        data.setEventTime(new Date());
        data.setActivityId(1L);
        data.setReportingStaff(100L);
    }

    @Test
    public void isValid() {
        EventLogType data = new EventLogType();

        data.setEventDate(new Date());

        data.setEventTime(new Date());
        data.setReportingStaff(100L);

        Date futureDate = BeanHelper.nextNthDays(new Date(), 1);
        data.setEventDate(futureDate);

    }
}
