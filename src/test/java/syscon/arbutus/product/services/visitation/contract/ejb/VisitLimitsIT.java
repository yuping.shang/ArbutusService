package syscon.arbutus.product.services.visitation.contract.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.visitation.contract.dto.VisitLimitSearchType;
import syscon.arbutus.product.services.visitation.contract.dto.VisitLimitType;
import syscon.arbutus.product.services.visitation.contract.interfaces.VisitationService;

public class VisitLimitsIT extends BaseIT {

    static Logger log = LoggerFactory.getLogger(VisitLimitsIT.class);
    List<VisitLimitType> dbVisitLimits;
    private VisitationService service;
    private Long facility;

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (VisitationService) JNDILookUp(this.getClass(), VisitationService.class);

        uc = super.initUserContext();
        service.deleteAllVisitLimits(uc);

        VisitLimitSearchType visitLimitSearch = new VisitLimitSearchType();
        dbVisitLimits = service.searchVisitLimit(uc, visitLimitSearch);

        FacilityTest facilityTest = new FacilityTest();
        facility = facilityTest.createFacility();
    }

    @Test
    public void justtest() {
        ArrayList<Long> aaa = new ArrayList<Long>();
        aaa.add(1L);
        aaa.add(2L);
        aaa.add(3L);
        aaa.add(4L);

        System.out.println(aaa.toString().replace("[", "").replace("]", ""));
    }

    @Test(enabled = true)
    public void create() {

        List<VisitLimitType> limits = new ArrayList<VisitLimitType>();
        List<VisitLimitType> ret = new ArrayList<VisitLimitType>();

        limits.add(buildSimpleVisitLimitObject("MAX", "WEEK", "MON", 100L));
        limits.add(buildVisitLimitObject("MAX", "LEGAL", "WEEK", "MON", 5L, 1L, 10L, false));
        limits.add(buildVisitLimitObject("MAX", "CONJUG", "WEEK", "MON", 1L, 1L, 3L, true));

        limits.add(buildSimpleVisitLimitObject("MED", "WEEK", "MON", 100L));
        limits.add(buildVisitLimitObject("MED", "LEGAL", "WEEK", "MON", 8L, 2L, 10L, true));

        limits.add(buildSimpleVisitLimitObject("MIN", "WEEK", "TUE", 100L));
        limits.add(buildVisitLimitObject("MIN", "LEGAL", "WEEK", "TUE", 12L, 3L, 10L, true));
        limits.add(buildVisitLimitObject("MIN", "CONJUG", "WEEK", "TUE", 2L, 5L, 7L, true));
        limits.add(buildVisitLimitObject("MIN", "OTHR", "WEEK", "TUE", 11L, 18L, 31L, true));

        ret = service.createVisitLimit(uc, limits);
        Assert.assertFalse(ret.isEmpty());
    }

    @Test(enabled = true, dependsOnMethods = "create")
    public void retrieve() {
        VisitLimitSearchType visitLimitSearch = new VisitLimitSearchType();
        dbVisitLimits = service.searchVisitLimit(uc, visitLimitSearch);
        Assert.assertNotNull(dbVisitLimits);
    }

    @Test(enabled = true, dependsOnMethods = "create")
    public void deactivateMAX() {
        VisitLimitSearchType visitLimitSearch = new VisitLimitSearchType();
        visitLimitSearch.setFacilityId(facility);
        visitLimitSearch.setSecurityLevel("MAX");
        dbVisitLimits = service.searchVisitLimit(uc, visitLimitSearch);

        for (final VisitLimitType limit : dbVisitLimits) {
            if (BeanHelper.isEmpty(limit.getVisitType())) {
                List<Long> deactivateList = new ArrayList<Long>();
                deactivateList.add(limit.getVisitLimitId());
                service.deactivateVisitLimit(uc, deactivateList);
            }
        }

        visitLimitSearch = new VisitLimitSearchType();
        visitLimitSearch.setFacilityId(facility);
        dbVisitLimits = service.searchVisitLimit(uc, visitLimitSearch);
        for (VisitLimitType limit : dbVisitLimits) {
            if (limit.getSecurityLevel().contentEquals("MAX")) {
                Assert.assertFalse(limit.getActive().booleanValue());
            } else {
                Assert.assertTrue(limit.getActive().booleanValue());
            }
        }

    }

    @Test(enabled = true, dependsOnMethods = "deactivateMAX")
    public void activateMAX() {
        VisitLimitSearchType visitLimitSearch = new VisitLimitSearchType();
        visitLimitSearch.setFacilityId(facility);
        visitLimitSearch.setSecurityLevel("MAX");
        dbVisitLimits = service.searchVisitLimit(uc, visitLimitSearch);

        for (final VisitLimitType limit : dbVisitLimits) {
            if (BeanHelper.isEmpty(limit.getVisitType())) {
                service.activateVisitLimit(uc, limit.getVisitLimitId());
            }
        }

        visitLimitSearch = new VisitLimitSearchType();
        visitLimitSearch.setFacilityId(facility);
        dbVisitLimits = service.searchVisitLimit(uc, visitLimitSearch);
        for (VisitLimitType limit : dbVisitLimits) {
            Assert.assertTrue(limit.getActive().booleanValue());
        }

    }

    @Test(enabled = true, dependsOnMethods = "activateMAX")
    public void createNewToDeactivateOthers() {

        List<VisitLimitType> limits = new ArrayList<VisitLimitType>();
        List<VisitLimitType> ret = new ArrayList<VisitLimitType>();

        limits.add(buildSimpleVisitLimitObject("MAX", "WEEK", "TUE", 50L));
        limits.add(buildVisitLimitObject("MAX", "RELIG", "WEEK", "TUE", 50L, 10, 3, false));
        limits.add(buildVisitLimitObject("MAX", "LEGAL", "WEEK", "TUE", 50L, 2, 1, false));

        ret = service.createVisitLimit(uc, limits);
        Assert.assertFalse(ret.isEmpty());
    }

    // Activates MAX WEEK MON back - should deactivate all MAX WEEK TUE
    @Test(enabled = true, dependsOnMethods = "createNewToDeactivateOthers")
    public void activatingDeactivated() {
        VisitLimitSearchType visitLimitSearch = new VisitLimitSearchType();
        visitLimitSearch.setFacilityId(facility);
        visitLimitSearch.setSecurityLevel("MAX");

        dbVisitLimits = service.searchVisitLimit(uc, visitLimitSearch);
        Assert.assertFalse(dbVisitLimits.isEmpty());
        for (VisitLimitType limit : dbVisitLimits) {
            if (BeanHelper.isEmpty((limit.getVisitType()))) {
                if (limit.getCycleStart().contentEquals("MON")) {
                    service.activateVisitLimit(uc, limit.getVisitLimitId());
                }
            }
        }

        visitLimitSearch = new VisitLimitSearchType();
        visitLimitSearch.setFacilityId(facility);
        visitLimitSearch.setSecurityLevel("MAX");
        visitLimitSearch.setCycleStart("TUE");
        dbVisitLimits = service.searchVisitLimit(uc, visitLimitSearch);
        for (VisitLimitType limit : dbVisitLimits) {
            Assert.assertFalse(limit.getActive().booleanValue());
        }
    }

    /**
     * type	   cycle      start	  max_vtros max_vts seclvl     ttl_vts ACTIVE
     * ---------- ---------- ---------- ---------- ------- ---------- ------- ------
     * WEEK       TUE			    MAX 	    50	    0
     * RELIG	   WEEK       TUE		 10	  3 MAX 	    50	    0
     * LEGAL	   WEEK       TUE		  2	  1 MAX 	    50	    0
     * WEEK       MON			    MAX 	   100	    1
     * LEGAL	   WEEK       MON		  1	 10 MAX 	     5	    1
     * CONJUG	   WEEK       MON		  1	  3 MAX 	     1	    1
     * Tuesdays are inactive. I will Activate Tuesday child (not the parent which doesn't have a type)
     * and then I expect this Tuesday to be active, parent Tuesday to be active, other Tuesday remains inactive
     * and all from Monday inactive as well.
     */
    @Test(enabled = true, dependsOnMethods = "activatingDeactivated")
    public void activateOtherCycleStartToDeactivateAllOthers() {
        VisitLimitSearchType visitLimitSearch = new VisitLimitSearchType();
        visitLimitSearch.setFacilityId(facility);
        visitLimitSearch.setSecurityLevel("MAX");
        dbVisitLimits = service.searchVisitLimit(uc, visitLimitSearch);

        for (final VisitLimitType limit : dbVisitLimits) {
            if (!BeanHelper.isEmpty(limit.getVisitType())) {
                if (limit.getCycleStart().contentEquals("TUE") && limit.getVisitType().contentEquals("RELIG")) {
                    service.activateVisitLimit(uc, limit.getVisitLimitId());
                }
            }
        }
       /*
           type	   cycle      start	  max_vtros max_vts seclvl     ttl_vts ACTIVE
	   ---------- ---------- ---------- ---------- ------- ---------- ------- ------
	   	   		   WEEK       TUE			    MAX 	    50	    0
	   RELIG	   WEEK       TUE		 10	  3 MAX 	    50	    1
	   LEGAL	   WEEK       TUE		  2	  1 MAX 	    50	    0

	   	   		   WEEK       MON			    MAX 	   100	    0
	   LEGAL	   WEEK       MON		  1	 10 MAX 	     5	    0
	   CONJUG	   WEEK       MON		  1	  3 MAX 	     1	    0
	   */

        visitLimitSearch.setFacilityId(facility);
        visitLimitSearch.setSecurityLevel("MAX");
        dbVisitLimits = service.searchVisitLimit(uc, visitLimitSearch);

        for (final VisitLimitType limit : dbVisitLimits) {
            if (!BeanHelper.isEmpty(limit.getVisitType())) {
                if (limit.getCycleStart().contentEquals("TUE") && limit.getVisitType().contentEquals("RELIG")) {
                    Assert.assertTrue(limit.getActive());
                } else if (limit.getCycleStart().contentEquals("TUE") && !limit.getVisitType().contentEquals("RELIG")) {
                    Assert.assertFalse(limit.getActive());
                } else {
                    Assert.assertFalse(limit.getActive());
                }
            }
        }

    }

    private VisitLimitType buildSimpleVisitLimitObject(String securityLevel, String cycleType, String cycleStart, long totalNumVisits) {

        VisitLimitType limit = new VisitLimitType();

        limit.setFacilityId(facility);
        limit.setSecurityLevel(securityLevel);
        limit.setCycleType(cycleType);
        limit.setCycleStart(cycleStart);
        limit.setTotalNumVisits(totalNumVisits);
        limit.setActive(true);
        limit.setActivationDate(new Date());

        return limit;
    }

    private VisitLimitType buildVisitLimitObject(String securityLevel, String visitType, String cycleType, String cycleStart, long totalNumVisits,
            long maxNumVisitorsPerType, long maxNumVisitsPerType, Boolean reinstate) {

        VisitLimitType limit = new VisitLimitType();

        limit.setFacilityId(facility);
        limit.setVisitType(visitType);
        limit.setSecurityLevel(securityLevel);
        limit.setCycleType(cycleType);
        limit.setCycleStart(cycleStart);
        limit.setMaxNumVisitorsPerType(maxNumVisitorsPerType);
        limit.setMaxNumVisitsPerType(maxNumVisitsPerType);
        limit.setTotalNumVisits(totalNumVisits);
        limit.setReinstate(reinstate);
        limit.setActive(true);
        limit.setActivationDate(new Date());

        return limit;
    }

}
