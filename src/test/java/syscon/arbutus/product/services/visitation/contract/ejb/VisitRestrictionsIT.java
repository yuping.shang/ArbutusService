package syscon.arbutus.product.services.visitation.contract.ejb;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.PersonIdentityTest;
import syscon.arbutus.product.services.common.SupervisionTest;
import syscon.arbutus.product.services.core.exception.DataExistException;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.visitation.contract.dto.*;
import syscon.arbutus.product.services.visitation.contract.interfaces.VisitationService;

import static org.testng.Assert.*;

public class VisitRestrictionsIT extends BaseIT {

    static Logger log = LoggerFactory.getLogger(VisitRestrictionsIT.class);

    private static Long DCOJL = 1L;
    private VisitationService service;
    private PersonService piService;
    private Long offenderPersonIdentity;
    private Long offenderPerson;
    private Long visitorPersonIdentity;
    private Long visitorPerson;
    private Long supervision;
    private ApprovedVisitorType approvedVisitor;

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (VisitationService) JNDILookUp(this.getClass(), VisitationService.class);
        piService = (PersonService) JNDILookUp(this.getClass(), PersonService.class);

        uc = super.initUserContext();

        PersonIdentityTest piTest = new PersonIdentityTest();
        // Offender
        offenderPersonIdentity = piTest.createPersonIdentity();
        PersonIdentityType offenderPI = piService.getPersonIdentityType(uc, offenderPersonIdentity, true);
        offenderPerson = offenderPI.getPersonId();
        // Visitor
        visitorPersonIdentity = piTest.createPersonIdentity();
        PersonIdentityType visitorPI = piService.getPersonIdentityType(uc, visitorPersonIdentity, true);
        visitorPerson = visitorPI.getPersonId();

        //Approved Visitor to Offender
        approvedVisitor = buildApprovedVisitors();

        SupervisionTest supervisionTest = new SupervisionTest();
        supervision = supervisionTest.createSupervision(offenderPersonIdentity, DCOJL, true);

    }

    @Test(enabled = true)
    public void testJNDILookup() {
        assertNotNull(service);
        assertNotNull(uc);
    }

    @Test(enabled = true, dependsOnMethods = { "testJNDILookup" })
    public void isDataOk() {
        assertNotNull(offenderPersonIdentity);
        assertNotNull(offenderPerson);
        assertNotNull(visitorPersonIdentity);
        assertNotNull(visitorPerson);
        assertNotNull(approvedVisitor);
    }

    @Test(enabled = true, dependsOnMethods = { "isDataOk" })
    public void createOffenderRestriction() {
        OffenderVisitRestrictionsType offenderRestriction = new OffenderVisitRestrictionsType();

        offenderRestriction.setOffenderId(offenderPerson);
        offenderRestriction.setRestrictionDate(new Date()); //Today
        offenderRestriction.setType("BAN");

        OffenderVisitRestrictionsType createdOffenderRestriction = service.createVisitRestriction(uc, offenderRestriction);

        assertNotNull(createdOffenderRestriction);
        assertTrue(createdOffenderRestriction.isActive());

    }

    @Test(enabled = true, dependsOnMethods = { "isDataOk" })
    public void createOffenderVisitorRestriction() {
        OffenderVisitorVisitRestrictionsType offenderVisitorRestriction = new OffenderVisitorVisitRestrictionsType();

        offenderVisitorRestriction.setOffenderId(offenderPerson);
        offenderVisitorRestriction.setRestrictionDate(new Date()); //Today
        offenderVisitorRestriction.setType("BAN");
        offenderVisitorRestriction.setApprovedVisitorId(approvedVisitor.getApprovedVisitorId());
        offenderVisitorRestriction.setVisitorAssociationType(approvedVisitor.getPersonAssociationType());
        offenderVisitorRestriction.setVisitorPersonRole(approvedVisitor.getPersonRole());

        OffenderVisitorVisitRestrictionsType createdOffenderRestriction = service.createVisitRestriction(uc, offenderVisitorRestriction);

        assertNotNull(createdOffenderRestriction);
        assertTrue(createdOffenderRestriction.isActive());
        assertTrue(approvedVisitor.getPersonAssociationType() == createdOffenderRestriction.getVisitorAssociationType());
        assertTrue(approvedVisitor.getPersonRole() == createdOffenderRestriction.getVisitorPersonRole());

    }

    @Test(enabled = true, dependsOnMethods = { "isDataOk" })
    public void createVisitorRestriction() {
        VisitorVisitRestrictionsType restriction = new VisitorVisitRestrictionsType();
        restriction.setPersonId(visitorPerson);
        restriction.setRestrictionDate(new Date());
        restriction.setType("Temporary-BAN");

        VisitorVisitRestrictionsType createdRestriction = service.createVisitRestriction(uc, restriction);

        assertNotNull(createdRestriction);
        assertTrue(createdRestriction.isActive());
        assertNotNull(createdRestriction.getVisitRestrictionId());

    }

    @Test(enabled = true, dependsOnMethods = { "createVisitorRestriction" })
    public void getRestriction() {
        //create a restriction first
        OffenderVisitRestrictionsType offenderRestriction = new OffenderVisitRestrictionsType();

        offenderRestriction.setOffenderId(offenderPerson);
        offenderRestriction.setRestrictionDate(new Date()); //Today
        offenderRestriction.setType("BAN-1");

        OffenderVisitRestrictionsType createdOffenderRestriction = service.createVisitRestriction(uc, offenderRestriction);

        assertNotNull(createdOffenderRestriction);
        assertTrue(createdOffenderRestriction.isActive());
        assertNotNull(createdOffenderRestriction.getVisitRestrictionId());
        //try to get it through service
        OffenderVisitRestrictionsType fetchedOffenderRestriction = service.getRestriction(uc, createdOffenderRestriction.getVisitRestrictionId());

        assertNotNull(fetchedOffenderRestriction);
        assertNotNull(fetchedOffenderRestriction.getVisitRestrictionId());

        assertTrue(fetchedOffenderRestriction.getVisitRestrictionId() == createdOffenderRestriction.getVisitRestrictionId());
    }

    @Test(enabled = true, dependsOnMethods = { "getRestriction" })
    public void getAllOffenderRestrictions() {
        //create a restriction first
        OffenderVisitRestrictionsType offenderRestriction = new OffenderVisitRestrictionsType();

        offenderRestriction.setOffenderId(offenderPerson);
        offenderRestriction.setRestrictionDate(new Date()); //Today
        offenderRestriction.setType("BAN-3");

        OffenderVisitRestrictionsType createdOffenderRestriction = service.createVisitRestriction(uc, offenderRestriction);

        assertNotNull(createdOffenderRestriction);
        assertTrue(createdOffenderRestriction.isActive());
        assertNotNull(createdOffenderRestriction.getVisitRestrictionId());

        //create second  restriction 
        OffenderVisitorVisitRestrictionsType offenderVisitorRestriction = new OffenderVisitorVisitRestrictionsType();

        offenderVisitorRestriction.setOffenderId(offenderPerson);
        offenderVisitorRestriction.setRestrictionDate(new Date()); //Today
        offenderVisitorRestriction.setType("BAN-4");
        offenderVisitorRestriction.setApprovedVisitorId(approvedVisitor.getApprovedVisitorId());
        offenderVisitorRestriction.setVisitorAssociationType(approvedVisitor.getPersonAssociationType());
        offenderVisitorRestriction.setVisitorPersonRole(approvedVisitor.getPersonRole());

        OffenderVisitorVisitRestrictionsType secondOffenderRestriction = service.createVisitRestriction(uc, offenderVisitorRestriction);

        assertNotNull(secondOffenderRestriction);
        assertTrue(secondOffenderRestriction.isActive());

        //try to get all offender visit restrictions through service
        List<VisitRestrictionsType> restrictions = service.getAllOffenderRestrictions(uc, offenderPerson);

        assertNotNull(restrictions);

        for (VisitRestrictionsType restriction : restrictions) {
            assertNotNull(restriction);
        }

    }

    @Test(enabled = true, dependsOnMethods = { "isDataOk" })
    public void deactivateVisitRestriction() {
        //create a restriction first
        OffenderVisitRestrictionsType offenderRestriction = new OffenderVisitRestrictionsType();

        offenderRestriction.setOffenderId(offenderPerson);
        offenderRestriction.setRestrictionDate(new Date()); //Today
        offenderRestriction.setType("BAN-2");

        OffenderVisitRestrictionsType createdOffenderRestriction = service.createVisitRestriction(uc, offenderRestriction);

        assertNotNull(createdOffenderRestriction);
        assertTrue(createdOffenderRestriction.isActive());
        assertNotNull(createdOffenderRestriction.getVisitRestrictionId());
        //try to deactivate it through service
        OffenderVisitRestrictionsType deactivatedRestriction = service.deactivateVisitRestriction(uc, createdOffenderRestriction.getVisitRestrictionId());

        assertNotNull(deactivatedRestriction);
        assertNotNull(deactivatedRestriction.getVisitRestrictionId());
        assertTrue(deactivatedRestriction.getVisitRestrictionId() == createdOffenderRestriction.getVisitRestrictionId());
        assertTrue(deactivatedRestriction.isActive() == false);
    }

    @Test(enabled = true, dependsOnMethods = { "createOffenderRestriction" })
    public void createOffenderRestrictionException() {
        OffenderVisitRestrictionsType offenderRestriction = new OffenderVisitRestrictionsType();

        offenderRestriction.setOffenderId(offenderPerson);
        offenderRestriction.setRestrictionDate(new Date()); //Today
        offenderRestriction.setType("BAN");

        OffenderVisitRestrictionsType createdOffenderRestriction = null;
        try {

            createdOffenderRestriction = service.createVisitRestriction(uc, offenderRestriction);
        } catch (DataExistException dee) {
            assertTrue(dee != null);
        }
        assertNull(createdOffenderRestriction);

    }

    /**
     * Creates a sibling for the current offender and enables him as a visitor.
     */
    private ApprovedVisitorType buildApprovedVisitors() {
        ApprovedVisitorType approvedVisitor = new ApprovedVisitorType();

        approvedVisitor.setApprovedVisitorId(null);
        approvedVisitor.setPersonAssociationType("SIBLING");
        approvedVisitor.setPersonRole("FATHER");
        approvedVisitor.setPersonId(offenderPerson);
        approvedVisitor.setOffenderSupervisionId(supervision);
        approvedVisitor.setVisitorPersonIdentityId(visitorPersonIdentity);
        approvedVisitor.setSupervisionId(supervision);
        approvedVisitor.setSupervisionFacilityId(DCOJL);

        ApprovedVisitorType retApprovedVisitor = service.addToApprovedVisitors(uc, supervision, approvedVisitor);
        assertNotNull(retApprovedVisitor);
        return retApprovedVisitor;
    }
}