package syscon.arbutus.product.services.visitation.contract.ejb;

import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import junit.framework.Assert;
import syscon.arbutus.product.services.activity.contract.dto.ActivityCategory;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldMetaType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.DTOBindingTypeEnum;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.FieldTypeEnum;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.GroupLabelType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.GroupType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.TranslationType;
import syscon.arbutus.product.services.clientcustomfield.contract.ejb.Constants;
import syscon.arbutus.product.services.clientcustomfield.contract.interfaces.ClientCustomFieldService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.PersonIdentityTest;
import syscon.arbutus.product.services.core.common.adapters.ActivityServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.facility.contract.dto.FacilitiesReturn;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.dto.FacilitySearch;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facility.contract.util.DateUtil;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.DefaultLocationCategory;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.DefaultLocationType;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.movement.contract.dto.ExternalMovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType.MovementCategory;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType.MovementDirection;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType.MovementStatus;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType.MovementType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.supervision.contract.dto.ImprisonmentStatusType;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;
import syscon.arbutus.product.services.visitation.contract.dto.ApprovedVisitorReturnType;
import syscon.arbutus.product.services.visitation.contract.dto.ApprovedVisitorSearchType;
import syscon.arbutus.product.services.visitation.contract.dto.ApprovedVisitorType;
import syscon.arbutus.product.services.visitation.contract.dto.AttendingVisitorType;
import syscon.arbutus.product.services.visitation.contract.dto.VisitEnum;
import syscon.arbutus.product.services.visitation.contract.dto.VisitSearchType;
import syscon.arbutus.product.services.visitation.contract.dto.VisitType;
import syscon.arbutus.product.services.visitation.contract.dto.VisitsReturnType;
import syscon.arbutus.product.services.visitation.contract.interfaces.VisitationService;

public class IT extends BaseIT {

    private static final String DCOJL_CODE = "DCOJL";
    static Logger log = LoggerFactory.getLogger(IT.class);
    private static Long DCOJL = 1L;
    private VisitationService service;
    private PersonIdentityTest piTest;
    private SupervisionService supService;
    private FacilityService facservice;
    private ClientCustomFieldService ccfService;
    private MovementService maSer;
    private FacilityInternalLocationService facInService;
    private ActivityService actSer;

    public static Date parseTime(String strTime) {

        SimpleDateFormat myFormat = new SimpleDateFormat("HH:mm");

        try {
            return myFormat.parse(strTime);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        return null;
    }

    public static Long randLong(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return Long.valueOf((long) randomNum);
    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (VisitationService) JNDILookUp(this.getClass(), VisitationService.class);
        supService = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        maSer = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
        actSer = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        ccfService = (ClientCustomFieldService) JNDILookUp(this.getClass(), ClientCustomFieldService.class);
        facservice = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        facInService = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);

        piTest = new PersonIdentityTest();
        uc = super.initUserContext();
        DCOJL = getDOJLFacility();

        maSer.deleteAll(uc);
        service.deleteAllVisits(uc);
        //        service.deleteAllVisitorList(uc);

    }

    @BeforeMethod
    public void beforeMethod() {
        //service.deleteAllApprovedVisitor(uc);
        //service.deleteAllVisits(uc);

    }

    @Test
    public void createVisit() {
        //
        VisitType retVisit = null;

        //
        Set<AttendingVisitorType> attendingVisitors = new HashSet<AttendingVisitorType>();
        Set<Long> visitorIds = new HashSet<Long>();

        Long visitorListId;
        String personAssociationType = "SIBLING";
        String personRole = "FATHER";
        Long supervisionFacilityId = DCOJL;

        Long offenderPersonIdentityId = piTest.createPersonIdentity();
        Long visitorPersonIdentityId = piTest.createPersonIdentity();

        SupervisionType supervision = createSupervision(offenderPersonIdentityId, supervisionFacilityId);
        createExternalMovementAdmission(supervision.getSupervisionIdentification(), supervisionFacilityId, supervisionFacilityId);

        ApprovedVisitorType approvedVisitor = new ApprovedVisitorType(null, personAssociationType, personRole, null, offenderPersonIdentityId, visitorPersonIdentityId,
                supervision.getSupervisionIdentification(), supervisionFacilityId);

        ApprovedVisitorType retApprovedVisitor = service.addToApprovedVisitors(uc, supervision.getSupervisionIdentification(), approvedVisitor);

        Long locationId = getDefaultLocaiton(supervisionFacilityId);
        String visitType = "SOCIAL";
        Date visitDate = new Date();
        Date startTime = new Date();
        String timeSlot = "WEEKLEY_MAX_SOCAIL";
        Date endTime = new Date();
        String status = "SCHEDULED";
        String outcome = "COMP";
        String cancelReason = "just because so";

        VisitType visit = new VisitType(null, locationId, visitType, null, offenderPersonIdentityId, visitDate, startTime, endTime, timeSlot, status, outcome,
                cancelReason, attendingVisitors);
        //
        retVisit = service.createVisit(uc, visit);

        assert (retVisit != null);

        AttendingVisitorType visitor = new AttendingVisitorType(null, retVisit.getVisitId(), retApprovedVisitor.getApprovedVisitorId(), false);
        AttendingVisitorType retAttendingVisitor = service.addAttendingVisitorToVisit(uc, retVisit.getVisitId(), visitor);
        attendingVisitors.add(retAttendingVisitor);
        visitorIds.add(retAttendingVisitor.getAttendingVisitorId());

        retVisit.setAttendingVisitors(attendingVisitors);

        retVisit = service.getVisit(uc, retVisit.getVisitId());

        for (AttendingVisitorType aVisitor : retVisit.getAttendingVisitors()) {
            assert (visitorIds.contains(aVisitor.getAttendingVisitorId()) == true);
        }

    }

    private SupervisionType createSupervision(Long personIdentityId, Long facilityId) {
        int n = 1;

        SupervisionType supervision = new SupervisionType();

        supervision.setSupervisionDisplayID("dId");
        supervision.setSupervisionStartDate(new Date());
        supervision.setDateOrientationProvided(new Date());
        supervision.setPersonIdentityId(personIdentityId);
        supervision.setFacilityId(facilityId);

        String statusSet = "BOP";

        // create ImprisonmentStatus
        ImprisonmentStatusType imprisonmentStatus = new ImprisonmentStatusType();
        imprisonmentStatus.setImprisonmentStatus(statusSet);
        imprisonmentStatus.setImprisonmentStatusStartDate(new Date());
        supervision.getImprisonmentStatus().add(imprisonmentStatus);
        SupervisionType ret = supService.create(uc, supervision);
        return ret;
    }

    @Test
    public void updateVisit() {
        //
        VisitType retVisit = null;

        //
        Set<AttendingVisitorType> attendingVisitors = new HashSet<AttendingVisitorType>();
        Set<Long> visitorIds = new HashSet<Long>();

        Long visitorListId;
        String personAssociationType = "SIBLING";
        String personRole = "FATHER";
        Long supervisionFacilityId = DCOJL;

        Long offenderPersonIdentityId = piTest.createPersonIdentity();
        Long visitorPersonIdentityId = piTest.createPersonIdentity();

        SupervisionType supervision = createSupervision(offenderPersonIdentityId, supervisionFacilityId);
        Long supervisionId = supervision.getSupervisionIdentification();
        createExternalMovementAdmission(supervisionId, supervisionFacilityId, supervisionFacilityId);

        ApprovedVisitorType visitList = new ApprovedVisitorType(null, personAssociationType, personRole, null, offenderPersonIdentityId, visitorPersonIdentityId,
                supervisionId, supervisionFacilityId);
        ApprovedVisitorType retApprovedVisitor = service.addToApprovedVisitors(uc, supervisionId, visitList);

        visitorPersonIdentityId = piTest.createPersonIdentity();

        createExternalMovementAdmission(supervisionId, supervisionFacilityId, supervisionFacilityId);
        ApprovedVisitorType visitList2 = new ApprovedVisitorType(null, personAssociationType, personRole, null, offenderPersonIdentityId, visitorPersonIdentityId,
                supervisionId, supervisionFacilityId);

        ApprovedVisitorType retApprovedVisitor2 = service.addToApprovedVisitors(uc, supervisionId, visitList2);

        Long locationId = getDefaultLocaiton(supervisionFacilityId);
        String visitType = "SOCAIL";
        Date visitDate = new Date();
        Date startTime = new Date();
        String timeSlot = "WEEKLEY_MAX_SOCAIL";
        Date endTime = BeanHelper.createDateWithTime(startTime, startTime.getHours(), startTime.getMinutes() + 10, 0);
        String status = "SCHEDULED";
        String outcome = "COMP";
        String cancelReason = "just because so";
        Long visitActivityId = null; //VisitationHelper.createVisitActivity(uc, supervisionId, visitDate, visitDate);

        VisitType visit = new VisitType(null, locationId, visitType, visitActivityId, offenderPersonIdentityId, visitDate, startTime, endTime, timeSlot, status, outcome,
                cancelReason, attendingVisitors);
        //
        retVisit = service.createVisit(uc, visit);
        assert (retVisit != null);

        AttendingVisitorType visitor = new AttendingVisitorType(null, retVisit.getVisitId(), retApprovedVisitor.getApprovedVisitorId(), false);
        AttendingVisitorType retAttendingVisitor = service.addAttendingVisitorToVisit(uc, retVisit.getVisitId(), visitor);
        attendingVisitors.add(retAttendingVisitor);
        visitorIds.add(retAttendingVisitor.getAttendingVisitorId());

        AttendingVisitorType visitor2 = new AttendingVisitorType(null, retVisit.getVisitId(), retApprovedVisitor2.getApprovedVisitorId(), false);
        AttendingVisitorType retAttendingVisitor2 = service.addAttendingVisitorToVisit(uc, retVisit.getVisitId(), visitor2);
        attendingVisitors.add(retAttendingVisitor2);
        visitorIds.add(retAttendingVisitor2.getAttendingVisitorId());

        retVisit.setAttendingVisitors(attendingVisitors);
        retVisit.setOutcome("cancel requested BY Visotr");
        retVisit.setStatus(VisitEnum.VisitStatusEnum.CANCELLED.code());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, 10);
        retVisit.setStartTime(cal.getTime());
        retVisit.setVisitType("LAWYER");

        retVisit = service.updateVisit(uc, retVisit);
        assert (retVisit != null);

    }

    @Test
    public void updateVisit2() {
        // It is correct that there are 2 visits
        // we are moving the visitor from one to another

        SupervisionType sup = getNewSupervision(DCOJL);
        VisitType visit = getNewVisit(sup, null, null);
        VisitType visit2 = getNewVisit(sup, null, null);

        ApprovedVisitorType vlt = getNewVisitorListType(sup);
        AttendingVisitorType visitor = getNewVisitor(vlt.getApprovedVisitorId(), visit2.getVisitId());

        ApprovedVisitorType vlt2 = getNewVisitorListType(sup);
        AttendingVisitorType visitor2 = getNewVisitor(vlt2.getApprovedVisitorId(), visit2.getVisitId());

        Set<Long> visitorIds = new HashSet<Long>();

        visitorIds.add(visitor.getAttendingVisitorId());
        visitorIds.add(visitor2.getAttendingVisitorId());

        VisitType retVisit2 = service.getVisit(uc, visit2.getVisitId());
        VisitType retVisit = service.updateVisit(uc, visit.getVisitId(), visitorIds);
        VisitType retVisit3 = service.getVisit(uc, visit2.getVisitId());
        assert (retVisit != null);

    }

    @Test
    public void updateByAttendingVisitors() {
        //

        SupervisionType sup = getNewSupervision(DCOJL);
        VisitType visit = getNewVisit(sup, null, null);

        ApprovedVisitorType vlt = getNewVisitorListType(sup);
        AttendingVisitorType visitor = new AttendingVisitorType(null, visit.getVisitId(), vlt.getApprovedVisitorId(), false);

        Set<AttendingVisitorType> aVisitors = visit.getAttendingVisitors();
        aVisitors.add(visitor);
        visit.setAttendingVisitors(aVisitors);

        visit.setStatus(VisitEnum.VisitStatusEnum.CANCELLED.code());
        visit.setCancelReason(VisitEnum.VisitCancelReasonEnum.NSHOW.code());
        visit.setLocationId(5L);
        Date newVisitDate = DateUtil.createFutureDate(3);
        visit.setVisitDate(newVisitDate);
        visit.setStartTime(newVisitDate);
        visit.setEndTime(newVisitDate);

        VisitType retVisit = service.updateByAttendingVisitors(uc, visit);
        assert (retVisit != null);
    }

    @Test
    public void recordVisit() {
        //

        SupervisionType sup = getNewSupervision(DCOJL);
        VisitType visit = getNewVisit(sup, null, null);

        service.recordVisit(uc, visit.getVisitId(), VisitEnum.VisitStatusEnum.COMPLETED.code(), null);

        VisitType retVisit = service.getVisit(uc, visit.getVisitId());

        assert (retVisit.getStatus().equals(VisitEnum.VisitStatusEnum.COMPLETED.code()));
    }

    @Test
    public void recordVisits() {
        //

        SupervisionType sup = getNewSupervision(DCOJL);
        VisitType visit = getNewVisit(sup, null, null);
        visit = getNewVisit(sup, null, null);
        visit = getNewVisit(sup, null, null);

        service.recordVisits(uc, sup.getSupervisionIdentification(), VisitEnum.VisitStatusEnum.CANCELLED.code(), VisitEnum.VisitCancelReasonEnum.OFFENDERRELEASED.code());

        VisitSearchType vsearch = new VisitSearchType();
        vsearch.setOffenderSupervisionId(sup.getSupervisionIdentification());

        VisitsReturnType ret = service.searchVisits(uc, vsearch, null, null, null);

        for (VisitType vst : ret.getVisits()) {
            assert (vst.getStatus().equals(VisitEnum.VisitStatusEnum.CANCELLED.code()));
            assert (vst.getCancelReason().equals(VisitEnum.VisitCancelReasonEnum.OFFENDERRELEASED.code()));
        }

    }

    @Test
    public void addVisitorFromVisit() {
        //

        SupervisionType sup = getNewSupervision(DCOJL);
        VisitType visit = getNewVisit(sup, null, null);

        ApprovedVisitorType vlt = getNewVisitorListType(sup);
        AttendingVisitorType visitor = getNewVisitor(vlt.getApprovedVisitorId(), visit.getVisitId());

        AttendingVisitorType retVisit = service.addAttendingVisitorToVisit(uc, visit.getVisitId(), visitor);

        assert (retVisit != null);

    }

    @Test
    public void removeVisitorToVisit() {
        //
        SupervisionType sup = getNewSupervision(DCOJL);

        VisitType visit = getNewVisit(sup, null, null);
        int startVisitorCount = visit.getAttendingVisitors().size();

        ApprovedVisitorType vlt = getNewVisitorListType(sup);
        AttendingVisitorType visitor = getNewVisitor(vlt.getApprovedVisitorId(), visit.getVisitId());

        visit = service.getVisit(uc, visit.getVisitId());

        int startVisitorCount2 = visit.getAttendingVisitors().size();

        assert (startVisitorCount != startVisitorCount2);

        service.removeAttendingVisitorFromVisit(uc, visit.getVisitId(), visitor.getAttendingVisitorId());

        visit = service.getVisit(uc, visit.getVisitId());

        int endVisitorCount = visit.getAttendingVisitors().size();

        assert (startVisitorCount == endVisitorCount);

    }

    @Test
    public void addToApprovedVisitorList() {

        Long visitorListId;
        String personAssociationType = "SIBLING";
        String personRole = "FATHER";

        Long supervisionFacilityId = DCOJL;

        Long offenderPersonIdentityId = piTest.createPersonIdentity();
        Long visitorPersonIdentityId = piTest.createPersonIdentity();

        SupervisionType supervision = createSupervision(offenderPersonIdentityId, supervisionFacilityId);
        Long supervisionId = supervision.getSupervisionIdentification();
        createExternalMovementAdmission(supervisionId, supervisionFacilityId, supervisionFacilityId);

        ApprovedVisitorType visitList = new ApprovedVisitorType(null, personAssociationType, personRole, null, offenderPersonIdentityId, visitorPersonIdentityId,
                supervisionId, supervisionFacilityId);
        ApprovedVisitorType retApprovedVisitor = service.addToApprovedVisitors(uc, supervisionId, visitList);

        assert (retApprovedVisitor != null);

        assert (retApprovedVisitor.getPersonRole().equals(personRole));

    }

    @Test
    public void removeFromApprovedVisitorList() {

        Long visitorListId;
        String personAssociationType = "SIBLING";
        String personRole = "FATHER";

        Long supervisionFacilityId = DCOJL;

        Long offenderPersonIdentityId = piTest.createPersonIdentity();
        Long visitorPersonIdentityId = piTest.createPersonIdentity();

        SupervisionType supervision = createSupervision(offenderPersonIdentityId, supervisionFacilityId);
        Long supervisionId = supervision.getSupervisionIdentification();
        createExternalMovementAdmission(supervisionId, supervisionFacilityId, supervisionFacilityId);

        ApprovedVisitorType visitList = new ApprovedVisitorType(null, personAssociationType, personRole, null, offenderPersonIdentityId, visitorPersonIdentityId,
                supervisionId, supervisionFacilityId);
        ApprovedVisitorType retApprovedVisitor = service.addToApprovedVisitors(uc, offenderPersonIdentityId, visitList);

        service.deactiveApprovedVisitor(uc, offenderPersonIdentityId, retApprovedVisitor.getApprovedVisitorId());

        try {
            ApprovedVisitorType retApprovedVisitor2 = service.getApprovedVisitor(uc, retApprovedVisitor.getApprovedVisitorId());
            assert (retApprovedVisitor2 != null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(DataNotExistException.class));
        }
    }

    @Test
    public void searchVisits() {

        SupervisionType sup = getNewSupervision(DCOJL);

        VisitType visit = getNewVisit(sup, null, null);

        VisitSearchType vsearch = new VisitSearchType();
        vsearch.setOffenderSupervisionId(visit.getOffenderSupervisionId());

        VisitsReturnType ret = service.searchVisits(uc, vsearch, null, null, null);

        assert (ret.getTotalSize() == 1);

    }

    @Test(enabled = false)
    public void searchVisitsByStartEndTime() {

        service.deleteAllVisits(uc);

        SupervisionType sup = getNewSupervision(DCOJL);

        VisitType visit = getNewVisit(sup, null, null);

        VisitSearchType vsearch = new VisitSearchType();

        vsearch.setStartTime(parseTime("12:12"));

        vsearch.setEndTime(parseTime("23:12"));

        VisitsReturnType ret = service.searchVisits(uc, vsearch, null, null, null);

        assert (ret.getTotalSize() >= 1);

    }

    @Test
    public void searchVisitsStatus() {

        service.deleteAllVisits(uc);

        SupervisionType sup = getNewSupervision(DCOJL);

        VisitType visit = getNewVisit(sup, null, VisitEnum.VisitStatusEnum.CANCELLED.code());

        VisitSearchType vsearch = new VisitSearchType();
        vsearch.setStatus(VisitEnum.VisitStatusEnum.SCHEDULED.code());
        VisitsReturnType ret = service.searchVisits(uc, vsearch, null, null, null);
        String sts = VisitEnum.VisitStatusEnum.CANCELLED.code();

        assert (ret.getTotalSize().equals(0L));

        vsearch = new VisitSearchType();
        vsearch.setStatus(VisitEnum.VisitStatusEnum.CANCELLED.code());
        ret = service.searchVisits(uc, vsearch, null, null, null);

        assert (ret.getTotalSize().equals(1L));
        assert (ret.getVisits().get(0).getStatus().equals(sts));

    }

    @Test
    public void searchVisitRangeVisitDate() {

        SupervisionType sup = getNewSupervision(DCOJL);

        Date oneDayPast = DateUtil.createPastDate(1);
        Date oneDayFuture = DateUtil.createFutureDate(1);

        VisitType visit = getNewVisit(sup, null, null);
        visit = getNewVisit(sup, oneDayPast, null);
        visit = getNewVisit(sup, oneDayFuture, null);

        Date fromVisitDate = DateUtil.createPastDate(3);
        Date toVisitDate = DateUtil.createFutureDate(3);

        VisitSearchType vsearch = new VisitSearchType();
        vsearch.setFromVisitDate(fromVisitDate);
        vsearch.setToVisitDate(toVisitDate);
        vsearch.setOffenderSupervisionId(visit.getOffenderSupervisionId());

        VisitsReturnType ret = service.searchVisits(uc, vsearch, null, null, null);

        assert (ret.getTotalSize().equals(3L));

    }

    @Test
    public void searchApprovedVisitorList() {

        SupervisionType sup = getNewSupervision(DCOJL);

        ApprovedVisitorType avlisitor = getNewVisitorListType(sup);

        ApprovedVisitorSearchType avsearch = new ApprovedVisitorSearchType();
        avsearch.setOffenderSupervisionId(avlisitor.getOffenderSupervisionId());

        ApprovedVisitorReturnType ret = service.searchApprovedVisitors(uc, avsearch, null, null, null);

        assert (ret.getTotalSize() == 1);

    }

    @Test
    public void deleteVisit() {
        SupervisionType sup = getNewSupervision(DCOJL);

        VisitType visit = getNewVisit(sup, null, null);
        service.deleteVisit(uc, visit.getVisitId());
    }

    @Test(enabled = true)
    public void deleteAll() {
        SupervisionType sup = getNewSupervision(DCOJL);
        VisitType visit = getNewVisit(sup, null, null);

        Long count = service.getVisitCount(uc);
        assert (count > 0L);
        service.deleteAllVisits(uc);

        count = service.getVisitCount(uc);
        assert (count == 0L);

    }

    @Test
    public void testSaveCCFValue() {
        List<ClientCustomFieldMetaType> metaTypes = service.getAllCCFMeta(uc, DTOBindingTypeEnum.VISIT);

        GroupType groupType = new GroupType();
        groupType.setEntity(DTOBindingTypeEnum.VISIT.getCode());
        Set<GroupLabelType> grpLbls = new HashSet<GroupLabelType>();
        grpLbls.add(new GroupLabelType(null, "CCF Visit Group", "en"));
        groupType.setGroupLables(grpLbls);
        groupType.setName("Visit" + randLong(1, 1000));
        groupType.setSequence(1L);

        GroupType retGroup = ccfService.createGroup(uc, groupType);

        ClientCustomFieldMetaType metaType = getCCFMetaTextBoxInstance();
        metaType.setGroup(retGroup.getName());
        metaType.setGroupId(retGroup.getGroupId());
        metaType.setGroupLabel(retGroup.getGroupLables());
        metaType.setGroupSequence(retGroup.getSequence().toString());

        ClientCustomFieldMetaType retMeta = ccfService.save(uc, metaType);

        ClientCustomFieldValueType ccfValue = new ClientCustomFieldValueType();
        ccfValue.setCcfId(retMeta.getCcfId());
        ccfValue.setMetaType(retMeta);
        ccfValue.setCodeValue(null);
        ccfValue.setDtoBinding(retMeta.getDtoBinding());
        ccfValue.setIdBinding(1L);
        ccfValue.setName(retMeta.getName());
        ccfValue.setValue("Working with Syscon.");

        ClientCustomFieldValueType retValue = service.saveCCFMetaValue(uc, ccfValue);

    }

    private SupervisionType getNewSupervision(Long facilityId) {

        Long offenderPersonIdentityId = piTest.createPersonIdentity();
        SupervisionType supervision = createSupervision(offenderPersonIdentityId, facilityId);

        Long supervisionId = supervision.getSupervisionIdentification();

        createExternalMovementAdmission(supervisionId, facilityId, facilityId);

        return supervision;

    }

    private ApprovedVisitorType getNewVisitorListType(SupervisionType supervision) {
        Long visitorListId;
        String personAssociationType = "SIBLING";
        String personRole = "FATHER";

        Long supervisionFacilityId = DCOJL;

        Long visitorPersonIdentityId = piTest.createPersonIdentity();

        ApprovedVisitorType visitList = new ApprovedVisitorType(null, personAssociationType, personRole, null, supervision.getPersonIdentityId(), visitorPersonIdentityId,
                supervision.getSupervisionIdentification(), supervisionFacilityId);
        ApprovedVisitorType retApprovedVisitor = service.addToApprovedVisitors(uc, supervision.getSupervisionIdentification(), visitList);

        return retApprovedVisitor;
    }

    //    @Test(enabled = true)
    //    public void performChecks() {
    //
    //        SupervisionType sup = getNewSupervision(DCOJL);
    //
    //        VisitType retVisit = getNewVisit(sup,null,null);
    //        assert (retVisit != null);
    //
    //       boolean result = service.performTotalNumVisitsCheck(uc, retVisit.getOffenderPersonIdentityId(), retVisit.getVisitDate(),
    //               "OTHR", retVisit.getTimeSlot(), retVisit.getAttendingVisitors().size(),retVisit.getVisitId());
    //    }

    private AttendingVisitorType getNewVisitor(Long visitorListId, Long visitId) {

        AttendingVisitorType visitor = new AttendingVisitorType(null, visitId, visitorListId, false);
        AttendingVisitorType retAttendingVisitor = service.addAttendingVisitorToVisit(uc, visitId, visitor);
        return retAttendingVisitor;
    }

    @Test(enabled = true)
    public void testApprovedVisitorsByAttendingVisitors() {

        SupervisionType sup = getNewSupervision(DCOJL);

        VisitType retVisit = getNewVisit(sup, null, null);

        assert (retVisit != null);

        Set<Long> ids = new HashSet<Long>();

        Set<AttendingVisitorType> attVisitors = retVisit.getAttendingVisitors();
        for (AttendingVisitorType av : attVisitors) {
            ids.add(av.getAttendingVisitorId());
        }

        Map<Long, ApprovedVisitorType> approvedVisitors = service.getApprovedVisitorsByAttendingVistorIds(uc, ids);
        assert (approvedVisitors.size() == retVisit.getAttendingVisitors().size());
    }

    private VisitType getNewVisit(SupervisionType supervision, Date vDate, String vStatus) {
        //
        VisitType retVisit = null;
        //
        Set<AttendingVisitorType> attendingVisitors = new HashSet<AttendingVisitorType>();
        Set<Long> visitorIds = new HashSet<Long>();

        Long visitorListId;
        String personAssociationType = "SIBLING";
        String personRole = "FATHER";

        Long offenderPersonIdentityId = supervision.getPersonIdentityId();
        Long visitorPersonIdentityId = piTest.createPersonIdentity();

        Long supervisionId = supervision.getSupervisionIdentification();

        ApprovedVisitorType visitList = new ApprovedVisitorType(null, personAssociationType, personRole, null, offenderPersonIdentityId, visitorPersonIdentityId,
                supervisionId, supervision.getFacilityId());
        ApprovedVisitorType retApprovedVisitor = service.addToApprovedVisitors(uc, supervisionId, visitList);

        visitorPersonIdentityId = piTest.createPersonIdentity();

        ApprovedVisitorType visitList2 = new ApprovedVisitorType(null, personAssociationType, personRole, null, offenderPersonIdentityId, visitorPersonIdentityId,
                supervisionId, supervision.getFacilityId());

        ApprovedVisitorType retApprovedVisitor2 = service.addToApprovedVisitors(uc, supervisionId, visitList);

        Long facilityId = supervision.getFacilityId();

        Long locationId = getDefaultLocaiton(facilityId);
        Long visitActivityId = null;
        String visitType = "LEGAL";

        Date visitDate = new Date();
        if (vDate != null) {
            visitDate = vDate;
        }

        Date startTime = parseTime("14:00");
        String timeSlot = "WEEKLEY_MAX_SOCAIL";
        Date endTime = parseTime("20:00");
        String status = "SCHEDULED";

        if (vStatus != null) {
            status = vStatus;
        }
        String outcome = "COMP";
        String cancelReason = "just because so";

        VisitType visit = new VisitType(null, locationId, visitType, visitActivityId, offenderPersonIdentityId, visitDate, startTime, endTime, timeSlot, status, outcome,
                cancelReason, attendingVisitors);
        visit.setFacilityId(supervision.getFacilityId());
        //
        retVisit = service.createVisit(uc, visit);
        assert (retVisit != null);

        AttendingVisitorType visitor = new AttendingVisitorType(null, retVisit.getVisitId(), retApprovedVisitor.getApprovedVisitorId(), false);
        AttendingVisitorType retAttendingVisitor = service.addAttendingVisitorToVisit(uc, retVisit.getVisitId(), visitor);
        attendingVisitors.add(retAttendingVisitor);
        visitorIds.add(retAttendingVisitor.getAttendingVisitorId());

        AttendingVisitorType visitor2 = new AttendingVisitorType(null, retVisit.getVisitId(), retApprovedVisitor2.getApprovedVisitorId(), false);
        AttendingVisitorType retAttendingVisitor2 = service.addAttendingVisitorToVisit(uc, retVisit.getVisitId(), visitor2);
        attendingVisitors.add(retAttendingVisitor2);
        visitorIds.add(retAttendingVisitor2.getAttendingVisitorId());

        retVisit.setAttendingVisitors(attendingVisitors);
        retVisit.setOutcome("approval requested BY Visotr");
        retVisit.setStatus(status);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, 10);
        //retVisit.setStartTime(cal.getTime());
        retVisit.setVisitType("LAWYER");

        retVisit = service.updateVisit(uc, retVisit);
        return retVisit;

    }

    public void createExternalMovementAdmission(Long supId, Long fromLocationId, Long toLocationId) {
        // External Movement Activity

        // Set Movement Category
        MovementActivityType ma = new MovementActivityType();

        ma.setSupervisionId(supId);
        ma.setMovementCategory(MovementCategory.EXTERNAL.value());
        // Set Movement Type
        ma.setMovementType(MovementType.ADM.code()); // CRT -- Court
        // Set Movement Direction
        ma.setMovementDirection(MovementDirection.IN.code());
        // Set Movement Status
        ma.setMovementStatus(MovementStatus.COMPLETED.code());
        // Set Movement Reason
        ma.setMovementReason(null); // LA -- Legal Aid

        // Set Movement Date
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR, 12);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        Date presentTime = c.getTime();
        ma.setMovementDate(presentTime);

        // Set Comments
        Set<CommentType> comments = new LinkedHashSet<CommentType>();
        CommentType ct1 = new CommentType();
        ct1.setCommentDate(presentTime);
        ct1.setComment("Court appear.");
        comments.add(ct1);
        ma.setCommentText(comments);

        ActivityType activityType = new ActivityType();
        activityType.setSupervisionId(supId);
        activityType.setActiveStatusFlag(true);
        activityType.setPlannedStartDate(new Date());
        activityType.setPlannedEndDate(new Date());
        activityType.setActivityCategory(ActivityCategory.VISITATION.value());

        ActivityType ret = ActivityServiceAdapter.createActivity(uc, activityType);

        Long activityId = ret.getActivityIdentification();
        ma.setActivityId(activityId);

        ExternalMovementActivityType ema = new ExternalMovementActivityType();
        ema.setSupervisionId(supId);
        DefaultLocationType defaultLocation = facInService.getDefaultLocation(uc, fromLocationId, DefaultLocationCategory.DEFAULT_HOUSING_LOCATION);
        ema.setToFacilityInternalLocationId(defaultLocation.getInternalLocationId());
        //ema.setToFacilityInternalLocationId(createFacilityInternalLocation(fromLocationId));
        ema.setMovementCategory(MovementCategory.EXTERNAL.value());
        // Set Movement Type
        ema.setMovementType(MovementType.ADM.code()); // CRT -- Court
        // Set Movement Direction
        ema.setMovementDirection(MovementDirection.IN.code());
        // Set Movement Status
        ema.setMovementStatus(MovementStatus.COMPLETED.code());
        // Set Movement Reason
        ema.setMovementReason("VIS"); // LA -- Legal Aid

        ema.setMovementDate(presentTime);

        // Set Comments
        ema.setCommentText(comments);

        ema.setActivityId(activityId);
        String toProvinceState = "BC";
        String toCity = "Richmond";
        String fromCity = "Vancouver";

        ema.setFromLocationId(fromLocationId);
        ema.setFromFacilityId(fromLocationId);
        ema.setFromOrganizationId(fromLocationId);
        ema.setToProvinceState(toProvinceState);
        ema.setToLocationId(toLocationId);
        ema.setToFacilityId(toLocationId);
        ema.setToOrganizationId(toLocationId);
        ema.setApplicationDate(presentTime);
        ema.setArrestOrganizationId(toLocationId);
        ema.setEscortOrganizationId(toLocationId);
        ema.setReportingDate(presentTime);

        ema.setFromCity("Vancouver");
        ema.setToCity("Richmond");

        ema.setMovementOutcome(null);

        ExternalMovementActivityType emaRet = (ExternalMovementActivityType) maSer.create(uc, ema, false);
        assertNotNull(emaRet);

    }

    private Long getDefaultLocaiton(Long facilityId) {
        DefaultLocationType defaultLocation = facInService.getDefaultLocation(uc, facilityId, DefaultLocationCategory.DEFAULT_HOUSING_LOCATION);
        return defaultLocation.getInternalLocationId();
    }

    private Long getDOJLFacility() {
        FacilitySearch search = new FacilitySearch();
        search.setFacilityCode(DCOJL_CODE);
        FacilitiesReturn facResult = facservice.search(uc, null, search, null, null, null);
        if (facResult.getFacilities().size() > 0) {
            Facility facility = facResult.getFacilities().iterator().next();

            return facility.getFacilityIdentification();
        } else {
            return DCOJL;
        }
    }

    private ClientCustomFieldMetaType getCCFMetaTextBoxInstance() {
        ClientCustomFieldMetaType metaType = new ClientCustomFieldMetaType();
        metaType.setActiveDate(new Date());
        metaType.setClassification(Constants.Classification.PRIMARY.code());
        metaType.setDeactiveDate(new Date());
        metaType.setDefaultValue("XYZ");
        metaType.setDtoBinding(DTOBindingTypeEnum.VISIT.getCode());
        metaType.setFieldType(FieldTypeEnum.TEXT.getId());
        metaType.setGroupId(1L);

        Set<TranslationType> translations = new HashSet<TranslationType>();
        TranslationType trans = new TranslationType();
        trans.setError("This is Error Message");
        trans.setHelp("This is Help Message");
        trans.setLabel("Narrative Text");
        trans.setLanguage("en");
        translations.add(trans);

        metaType.setLength(3000L);
        metaType.setName("Narrative");
        metaType.setReadOnly(Boolean.FALSE);
        metaType.setReferenceSet(null);
        metaType.setRequired(null);
        metaType.setSequence(1L);
        metaType.setTranslation(translations);
        metaType.setValidationRule("ABC-TextBox");
        metaType.setVisible(Boolean.TRUE);

        return metaType;
    }

    private Long createFacility() {
        //facservice.deleteAll(uc);

        // return 1L;

        Facility ret = null;
        String category = new String("COMMU");

        Facility facility = new Facility(-1L, "FacilityCodeIT" + randLong(1, 1000), "FacilityName", new Date(), category, null, null, null, null, false);
        ret = facservice.create(uc, facility);
        assertNotEquals(ret, null);
        return ret.getFacilityIdentification();
    }
    
    /*private Long createFacilityInternalLocation(Long facId) {
        String phyCode = "PC01";
        String description = "physical location root";
        String level = "FACILITY";
        
        CommentType ct = new CommentType();
        ct.setComment("root comment");
        ct.setCommentDate(new Date());
        
        FacilityInternalLocationType fil = new FacilityInternalLocationType();
        fil.setFacilityId(facId);
        fil.setHierarchyLevel(level);
        fil.setLocationCode(phyCode);
        fil.setDescription(description);

        fil.getNonAssociations().add("TOTAL");
        fil.getNonAssociations().add("STG");
        
        
    
        FacilityInternalLocationType pl = fil;
        pl.getComments().add(ct);

        LocationAttributeType prop = new LocationAttributeType();
        prop.getOffenderCategories().add("SUIC");
        pl.setLocationAttribute(prop);
        FacilityInternalLocationType ret = facInService.create(uc, pl, UsageCategory.HOU);
        assertNotNull(ret);
        log.info(String.format("ret = %s", ret));
        return ret.getId();
    }
    */

    @Test(timeOut = 10000)
    public void visitorCreationConcurrencyTest() {

        String personAssociationType = "SIBLING";
        String personRole = "FATHER";

        Long supervisionFacilityId = DCOJL;
        Long offenderPersonIdentityId = piTest.createPersonIdentity();
        Long visitorPersonIdentityId = piTest.createPersonIdentity();

        final SupervisionType supervision = createSupervision(offenderPersonIdentityId, supervisionFacilityId);
        final ApprovedVisitorType approvedVisitor = new ApprovedVisitorType(null, personAssociationType, personRole, null, offenderPersonIdentityId,
                visitorPersonIdentityId, supervision.getSupervisionIdentification(), supervisionFacilityId);

        Thread firstThread = new Thread("1st-thread") {
            public void run() {
                //create a visitor
                ApprovedVisitorType retApprovedVisitor = service.addToApprovedVisitors(uc, supervision.getSupervisionIdentification(), approvedVisitor);
                Assert.assertNotNull(retApprovedVisitor);
                Assert.assertTrue(retApprovedVisitor.getApprovedVisitorId() != null);
                System.out.println("1st THREAD ==================== CREATED: " + retApprovedVisitor.getApprovedVisitorId());
            }
        };

        Thread secondThread = new Thread("2nd-thread") {
            public void run() {
                try {
                    //create a visitor
                    Thread.sleep(100);
                    ApprovedVisitorType retApprovedVisitor = service.addToApprovedVisitors(uc, supervision.getSupervisionIdentification(), approvedVisitor);
                    System.out.println("2nd THREAD ==================== TRYING TO CREATE AND SHOULD FAIL");
                    if (null == retApprovedVisitor) {
                        System.out.println("2nd THREAD ==================== CAN'T CREATE SECOND");
                    } else {
                        System.out.println("2nd THREAD ==================== CREATED? " + retApprovedVisitor.getApprovedVisitorId());
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ArbutusRuntimeException are) {
                    System.out.println("2nd THREAD ==================== EXCEPTION CAUGHT");
                    Assert.assertTrue(are.getMessage().contentEquals("Visitor already exist!"));
                }
            }
        };

        firstThread.start();
        secondThread.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
