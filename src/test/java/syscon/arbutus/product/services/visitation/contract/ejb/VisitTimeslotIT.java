package syscon.arbutus.product.services.visitation.contract.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.facility.contract.dto.FacilitiesReturn;
import syscon.arbutus.product.services.facility.contract.dto.FacilitySearch;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.DefaultLocationCategory;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.DefaultLocationType;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.visitation.contract.dto.VisitTimeslotSearchType;
import syscon.arbutus.product.services.visitation.contract.dto.VisitTimeslotType;
import syscon.arbutus.product.services.visitation.contract.interfaces.VisitationService;

public class VisitTimeslotIT extends BaseIT {

    private static final String DCOJL_CODE = "DCOJL";
    static Logger log = LoggerFactory.getLogger(VisitTimeslotIT.class);
    private static Long DCOJL = 1L;
    private VisitationService service;
    private FacilityService facservice;
    private FacilityInternalLocationService facilityInternalLocationService;
    private Date sixAM, nineAM, onePM, twoPM, threePM, fivePM, elevenAM;
    private List<VisitTimeslotType> timeslotsToUpdate;

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (VisitationService) JNDILookUp(this.getClass(), VisitationService.class);
        facservice = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        facilityInternalLocationService = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        uc = super.initUserContext();
        DCOJL = getDOJLFacility();

        sixAM = BeanHelper.createDateWithTime(new Date(), 6, 0, 0);
        nineAM = BeanHelper.createDateWithTime(new Date(), 9, 0, 0);
        onePM = BeanHelper.createDateWithTime(new Date(), 13, 0, 0);
        fivePM = BeanHelper.createDateWithTime(new Date(), 17, 0, 0);
        twoPM = BeanHelper.createDateWithTime(new Date(), 14, 0, 0);
        threePM = BeanHelper.createDateWithTime(new Date(), 15, 0, 0);
        elevenAM = BeanHelper.createDateWithTime(new Date(), 11, 0, 0);
    }

    @Test(enabled = true)
    public void deactivateAll() {
        service.deactivateAllVisitTimeslots(uc);

        VisitTimeslotSearchType stype = new VisitTimeslotSearchType();
        stype.setFacilityId(getDOJLFacility());
        stype.setActive(true);
        List<VisitTimeslotType> searchRet = service.search(uc, stype);

        Assert.assertTrue(searchRet.isEmpty());
    }

    @Test(enabled = true, dependsOnMethods = "deactivateAll")
    public void create() {

        List<VisitTimeslotType> timeslots = new ArrayList<VisitTimeslotType>();
        List<VisitTimeslotType> ret = new ArrayList<VisitTimeslotType>();

        timeslots.add(buildVisitTimeslotObject("MON", getDefaultLocation(DCOJL), sixAM, nineAM));
        timeslots.add(buildVisitTimeslotObject("MON", getDefaultLocation(DCOJL), onePM, fivePM));
        timeslots.add(buildVisitTimeslotObject("TUE", getDefaultLocation(DCOJL), sixAM, nineAM));
        timeslots.add(buildVisitTimeslotObject("TUE", getDefaultLocation(DCOJL), onePM, fivePM));
        timeslots.add(buildVisitTimeslotObject("WED", getDefaultLocation(DCOJL), sixAM, nineAM));
        timeslots.add(buildVisitTimeslotObject("WED", getDefaultLocation(DCOJL), onePM, fivePM));

        ret = service.createVisitTimeslot(uc, timeslots);
        Assert.assertFalse(ret.isEmpty());

        Assert.assertTrue(ret.size() == 6L);

        for (VisitTimeslotType timeslot : ret) {
            Assert.assertTrue(timeslot.getId() != null);
            Assert.assertTrue(timeslot.getDayOfWeek() != null);
            Assert.assertTrue(timeslot.getLocationId() != null);
            Assert.assertTrue(timeslot.getStartTime() != null);
            Assert.assertTrue(timeslot.getEndTime() != null);
        }
    }

    /**
     * Just generating more data...
     */
    @Test(enabled = true, dependsOnMethods = "create")
    public void deactivateAndCreateNew() {
        deactivateAll();
        create();
    }

    @Test(enabled = true, dependsOnMethods = "deactivateAndCreateNew")
    public void getAll() {
        timeslotsToUpdate = service.getAllVisitTimeslot(uc);
        Assert.assertFalse(timeslotsToUpdate.isEmpty());
    }

    @Test(enabled = true, dependsOnMethods = "getAll")
    public void getEach() {
        for (VisitTimeslotType timeslot : timeslotsToUpdate) {
            VisitTimeslotType ret = service.getVisitTimeslot(uc, timeslot.getId());
            Assert.assertNotNull(ret);
            Assert.assertTrue(timeslot.getId().equals(ret.getId()));
            Assert.assertTrue(timeslot.getDayOfWeek().equals(ret.getDayOfWeek()));
            Assert.assertTrue(timeslot.getLocationId().equals(ret.getLocationId()));
            Assert.assertTrue(timeslot.getStartTime().equals(ret.getStartTime()));
            Assert.assertTrue(timeslot.getEndTime().equals(ret.getEndTime()));
        }
    }

    @Test(enabled = true, dependsOnMethods = "getEach")
    public void update() {
        // Updating timeslot from MONDAY to FRIDAY
        for (VisitTimeslotType timeslot : timeslotsToUpdate) {
            if (timeslot.getDayOfWeek().contentEquals("MON")) {
                timeslot.setDayOfWeek("FRI");
                VisitTimeslotType updated = service.updateVisitTimeslot(uc, timeslot);
                Assert.assertTrue(updated.getDayOfWeek().contentEquals("FRI"));
            }
        }
    }

    @Test(enabled = true, dependsOnMethods = "update")
    public void checkOverlappingOnCreate() {
        List<VisitTimeslotType> timeslots = new ArrayList<VisitTimeslotType>();
        List<VisitTimeslotType> retList = new ArrayList<VisitTimeslotType>();

        timeslots.add(buildVisitTimeslotObject("FRI", getDefaultLocation(DCOJL), twoPM, threePM));
        retList = service.createVisitTimeslot(uc, timeslots);

        // Verifying the returned conflict
        Assert.assertFalse(retList.isEmpty());
        VisitTimeslotType ret = retList.get(0);

        Assert.assertFalse(ret.getConflicts().isEmpty());
        Assert.assertTrue(ret.getConflicts().size() == 1);
        VisitTimeslotType conflict = ret.getConflicts().get(0);

        Assert.assertTrue(conflict.getDayOfWeek().contentEquals("FRI"));
        Assert.assertTrue(conflict.getLocationId() == getDefaultLocation(DCOJL));
        Assert.assertTrue(conflict.getFacilityId() == getDOJLFacility());
        Assert.assertTrue(conflict.getStartTime().compareTo(onePM) == 0);
        Assert.assertTrue(conflict.getEndTime().compareTo(fivePM) == 0);

    }

    @Test(enabled = true, dependsOnMethods = "checkOverlappingOnCreate")
    public void checkOverlappingOnCreateExactSameExistingTimeslot() {
        List<VisitTimeslotType> timeslots = new ArrayList<VisitTimeslotType>();
        List<VisitTimeslotType> retList = new ArrayList<VisitTimeslotType>();

        timeslots.add(buildVisitTimeslotObject("FRI", getDefaultLocation(DCOJL), onePM, fivePM));
        retList = service.createVisitTimeslot(uc, timeslots);

        // Verifying the returned conflict
        Assert.assertFalse(retList.isEmpty());
        VisitTimeslotType ret = retList.get(0);

        Assert.assertFalse(ret.getConflicts().isEmpty());
        Assert.assertTrue(ret.getConflicts().size() == 1);
        VisitTimeslotType conflict = ret.getConflicts().get(0);

        Assert.assertTrue(conflict.getDayOfWeek().contentEquals("FRI"));
        Assert.assertTrue(conflict.getLocationId() == getDefaultLocation(DCOJL));
        Assert.assertTrue(conflict.getFacilityId() == getDOJLFacility());
        Assert.assertTrue(conflict.getStartTime().compareTo(onePM) == 0);
        Assert.assertTrue(conflict.getEndTime().compareTo(fivePM) == 0);

    }

    @Test(enabled = true, dependsOnMethods = "checkOverlappingOnCreateExactSameExistingTimeslot")
    public void checkOverlappingOnCreateNotBetweenExisting() {
        List<VisitTimeslotType> timeslots = new ArrayList<VisitTimeslotType>();
        List<VisitTimeslotType> retList = new ArrayList<VisitTimeslotType>();

        timeslots.add(buildVisitTimeslotObject("FRI", getDefaultLocation(DCOJL), nineAM, elevenAM));
        retList = service.createVisitTimeslot(uc, timeslots);

        // Verifying the returned conflict
        Assert.assertFalse(retList.isEmpty());
        VisitTimeslotType ret = retList.get(0);

        Assert.assertFalse(ret.getConflicts().isEmpty());
        Assert.assertTrue(ret.getConflicts().size() == 1);
        VisitTimeslotType conflict = ret.getConflicts().get(0);

        Assert.assertTrue(conflict.getDayOfWeek().contentEquals("FRI"));
        Assert.assertTrue(conflict.getLocationId() == getDefaultLocation(DCOJL));
        Assert.assertTrue(conflict.getFacilityId() == getDOJLFacility());
        Assert.assertTrue(conflict.getStartTime().compareTo(sixAM) == 0);
        Assert.assertTrue(conflict.getEndTime().compareTo(nineAM) == 0);

    }

    @SuppressWarnings("deprecation")
    @Test(enabled = true, dependsOnMethods = "checkOverlappingOnCreateNotBetweenExisting")
    public void checkOverlappingOnUpdate() {

        VisitTimeslotType ret = null;
        VisitTimeslotSearchType stype = new VisitTimeslotSearchType();
        stype.setFacilityId(getDOJLFacility());
        stype.setLocationId(getDefaultLocation(DCOJL));
        stype.setDayOfWeek("TUE");
        stype.setActive(true);

        List<VisitTimeslotType> searchRet = service.search(uc, stype);

        Assert.assertNotNull(searchRet);
        for (VisitTimeslotType toUpdate : searchRet) {

            if (toUpdate.getStartTime().getHours() == 13) {
                toUpdate.setStartTime(twoPM);
                toUpdate.setEndTime(threePM);
                ret = service.updateVisitTimeslot(uc, toUpdate);

                Assert.assertNotNull(ret);

                // Verifying the returned conflict
                Assert.assertFalse(ret.getConflicts().isEmpty());
                Assert.assertTrue(ret.getConflicts().size() == 1);
                VisitTimeslotType conflict = ret.getConflicts().get(0);

                Assert.assertTrue(conflict.getDayOfWeek().contentEquals("TUE"));
                Assert.assertTrue(conflict.getLocationId() == getDefaultLocation(DCOJL));
                Assert.assertTrue(conflict.getFacilityId() == getDOJLFacility());
                Assert.assertTrue(conflict.getStartTime().compareTo(onePM) == 0);
                Assert.assertTrue(conflict.getEndTime().compareTo(fivePM) == 0);
            }
        }
    }

    @Test(enabled = true, dependsOnMethods = "checkOverlappingOnUpdate")
    public void search() {
        VisitTimeslotSearchType stype = new VisitTimeslotSearchType();
        stype.setFacilityId(getDOJLFacility());
        stype.setActive(null);
        List<VisitTimeslotType> searchRet = service.search(uc, stype);

        // at least one inactive should be returned.
        long inactiveCounter = 0;
        for (VisitTimeslotType timeslot : searchRet) {
            if (!timeslot.getActive()) {
                inactiveCounter++;
            }
        }
        Assert.assertTrue(inactiveCounter > 0);

    }

    @Test(enabled = true, dependsOnMethods = "search")
    public void activationTest() {

        VisitTimeslotSearchType stype = new VisitTimeslotSearchType();
        stype.setFacilityId(getDOJLFacility());
        stype.setActive(false);
        stype.setDayOfWeek("WED");

        List<VisitTimeslotType> searchRet = service.search(uc, stype);
        Assert.assertFalse(searchRet.isEmpty());

        VisitTimeslotType wedTimeslot = searchRet.get(0);

        // tries to activates on WEDNESDAY timeslot.

        VisitTimeslotType res = service.activateVisitTimeslot(uc, wedTimeslot.getId());

        Assert.assertFalse(res.getActive());
        Assert.assertFalse(res.getConflicts().isEmpty());

        VisitTimeslotType conflict = res.getConflicts().get(0);
        Assert.assertTrue(conflict.getDayOfWeek().contentEquals("WED"));

        Assert.assertTrue(wedTimeslot.getId() != conflict.getId());

        //let's deactivate the active one.
        service.deactivateVisitTimeslot(uc, conflict.getId());

        // now I am able to activate the wedTimeslot, let's try again.

        res = service.activateVisitTimeslot(uc, wedTimeslot.getId());
        Assert.assertTrue(res.getActive());
        Assert.assertTrue(res.getConflicts().isEmpty());
        Assert.assertNotNull(res.getActivationDate());
        Assert.assertNull(res.getDeactivationDate());

        Assert.assertTrue(wedTimeslot.getId() != conflict.getId());

    }

    private VisitTimeslotType buildVisitTimeslotObject(String dayOfWeek, Long locationId, Date startTime, Date endTime) {

        VisitTimeslotType timeslot = new VisitTimeslotType();

        timeslot.setFacilityId(getDOJLFacility());
        timeslot.setDayOfWeek(dayOfWeek);
        timeslot.setLocationId(locationId);
        timeslot.setStartTime(startTime);
        timeslot.setEndTime(endTime);
        timeslot.setActivationDate(new Date());

        return timeslot;
    }

    private Long getDOJLFacility() {
        FacilitySearch search = new FacilitySearch();
        search.setFacilityCode(DCOJL_CODE);
        FacilitiesReturn facResult = facservice.search(uc, null, search, null, null, null);
        if (facResult.getFacilities().size() > 0) {
            Facility facility = facResult.getFacilities().iterator().next();

            return facility.getFacilityIdentification();
        } else {
            return DCOJL;
        }
    }

    private Long getDefaultLocation(Long facilityId) {
        DefaultLocationType defaultLocation = facilityInternalLocationService.getDefaultLocation(uc, facilityId, DefaultLocationCategory.DEFAULT_HOUSING_LOCATION);
        return defaultLocation.getInternalLocationId();
    }

}