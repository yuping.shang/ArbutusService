package syscon.arbutus.product.services.program.contract.ejb;

import javax.naming.NamingException;
import java.math.BigDecimal;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.testng.annotations.*;
import syscon.arbutus.product.services.activity.contract.dto.ScheduleConfigVariableType;
import syscon.arbutus.product.services.activity.contract.dto.ScheduleTimeslotType;
import syscon.arbutus.product.services.activity.contract.dto.TimeslotIterator;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.common.OrganizationTest;
import syscon.arbutus.product.services.common.SupervisionTest;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.program.contract.dto.*;
import syscon.arbutus.product.services.program.contract.interfaces.ProgramService;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

import static org.testng.Assert.*;

public class ProgramServiceIT extends BaseIT {

    final static Long success = 1L;
    private static Logger log = LoggerFactory.getLogger(ProgramServiceIT.class);
    private ProgramService service;
    private ActivityService activityService;
    private SupervisionService supervisionService;
    private List<Long> programIDs = new ArrayList<>();
    private Map<Long, String> programCodeMap = new HashMap<Long, String>();
    private List<Long> programOfferingIDs = new ArrayList<>();
    private List<Long> programAssignmentIDs = new ArrayList<>();
    private List<Long> programAttendanceIDs = new ArrayList<>();

    private Long facilityId;
    private Long facilityId2;
    private Long forInternalLocationId = null;
    private Long organizationId;
    private Long organizationId2;
    private Long organizationId3;
    private Long activityId = 1L;
    private Long supervisionId = 1L;
    private ScheduleConfigVariableType scheduleConfig;

    @BeforeMethod
    public void beforeMethod() {

    }

    @BeforeClass
    public void beforeClass() {
        log.info("Before JNDI lookup");
        service = (ProgramService) JNDILookUp(this.getClass(), ProgramService.class);
        uc = super.initUserContext();

        activityService = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);

        // Facility
        FacilityTest f = new FacilityTest();
        facilityId = f.createFacility();
        facilityId2 = f.createFacility();

        // Organization
        OrganizationTest org = new OrganizationTest();
        organizationId = org.createOrganization();

        organizationId2 = org.createOrganization();
        organizationId3 = org.createOrganization();

        SupervisionTest s = new SupervisionTest();
        supervisionId = s.createSupervision();

        // activityService.deleteAllScheduleConfigVar(uc);
        // ScheduleConfigVariableType scheduleConfig = new ScheduleConfigVariableType(null, ActivityCategory.PROGRAM.value(), null);
        // activityService.createScheduleConfigVars(uc, scheduleConfig);
    }

    @BeforeTest
    public void beforeTest() throws NamingException, Exception {
        // BasicConfigurator.configure();
        programIDs = new ArrayList<>();
        programOfferingIDs = new ArrayList<>();
        programAssignmentIDs = new ArrayList<>();
        programAttendanceIDs = new ArrayList<>();
    }

    @AfterClass
    public void afterClass() {
        OrganizationTest org = new OrganizationTest();
        if (organizationId != null) {
            org.deleteOrganization(organizationId);
        }
        if (organizationId2 != null) {
            org.deleteOrganization(organizationId2);
        }
        if (organizationId3 != null) {
            org.deleteOrganization(organizationId3);
        }

        if (this.facilityId != null) {
            FacilityTest f = new FacilityTest();
            f.deleteFacility(facilityId);
            f.deleteFacility(facilityId2);
        }

        //activityService.deleteScheduleConfigVar(uc, scheduleConfig.getScheduleConfigIdentification());
    }

    private void cleanUp() {
        service.deleteAll(null);
    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    @Test(enabled = true)
    public void createProgram() {
        cleanUp();

        ProgramType ret = null;
        ProgramType program = new ProgramType();
        program.setProgramCode("MATH");
        program.setProgramDescription("Math");
        program.setProgramCategory("COMM");
        // program.setExpiryDate(nextMonth(1));
        // program.setTerminationReason("NOTOFFERED");
        program.setProgramDuration(10L);
        program.setFlexibleStart(true);
        CommentType comment = new CommentType();
        comment.setUserId(uc.getConsumingApplicationId());
        comment.setComment("Math Program");
        comment.setCommentDate(dateTime(new Date()));
        program.setComments(comment);

        Set<TargetInmateType> inmates = new HashSet<>();
        TargetInmateType inmate = new TargetInmateType();
        Set<String> ageRanges = new HashSet<>();
        ageRanges.add("ADULT");
        ageRanges.add("JUVENILE");
        inmate.setAgeRanges(ageRanges);

        Set<String> amenities = new HashSet<>();
        amenities.add("WHEELCHAIR");
        amenities.add("HSENS");
        inmate.setAmenities(amenities);

        Set<String> genders = new HashSet<>();
        genders.add("M");
        genders.add("F");
        inmate.setGenders(genders);

        inmates.add(inmate);
        program.setTargetInmates(inmates);
        program.setProgramCaseWorkStep("CTU");

        ret = service.createProgram(uc, program);
        assertNotNull(ret);
        assertEquals(service.getProgram(uc, ret.getProgramId()), ret);
        this.programIDs.add(ret.getProgramId());
        programCodeMap.put(ret.getProgramId(), ret.getProgramCode());

        program = new ProgramType();
        program.setProgramCode("Computer");
        program.setProgramDescription("Computer Science");
        program.setProgramCategory("COMM");
        program.setExpiryDate(nextMonth(1));
        program.setTerminationReason("NOTOFFERED");
        program.setProgramDuration(10L);
        program.setFlexibleStart(true);
        comment = new CommentType();
        comment.setUserId(uc.getConsumingApplicationId());
        comment.setComment("Computer Program");
        comment.setCommentDate(dateTime(new Date()));
        program.setComments(comment);

        inmates = new HashSet<>();
        inmate = new TargetInmateType();
        ageRanges = new HashSet<>();
        ageRanges.add("ADULT");
        ageRanges.add("JUVENILE");
        inmate.setAgeRanges(ageRanges);

        amenities = new HashSet<>();
        amenities.add("WHEELCHAIR");
        amenities.add("HSENS");
        inmate.setAmenities(amenities);

        genders = new HashSet<>();
        genders.add("M");
        genders.add("F");
        genders.add("U");
        inmate.setGenders(genders);

        inmates.add(inmate);
        program.setTargetInmates(inmates);
        program.setProgramCaseWorkStep("AA");

        ret = service.createProgram(uc, program);
        assertNotNull(ret);
        assertEquals(service.getProgram(uc, ret.getProgramId()), ret);
        this.programIDs.add(ret.getProgramId());
        programCodeMap.put(ret.getProgramId(), ret.getProgramCode());
    }

    @Test(dependsOnMethods = { "createProgram" })
    public void getProgram() {
        for (Long id : programIDs) {
            ProgramType program = service.getProgram(uc, id);
            assertNotNull(program);
            assertNotNull(program.getTargetInmates());
            assertTrue(program.getTargetInmates().size() > 0);
        }

        Set<Long> programs = service.getAllProgram(uc);
        assertTrue(programs.containsAll(programIDs));
        assertTrue(programIDs.containsAll(programs));
    }

    @Test(dependsOnMethods = { "createProgram", "getProgram" })
    public void searchProgram() {
        assertEquals(service.searchProgram(uc, null, null, null, null, null).getTotalSize().intValue(), 2);
        assertEquals(service.searchProgram(uc, null, null, null, null, null).getPrograms().size(), 2);
        assertEquals(service.searchProgram(uc, new HashSet<Long>(Arrays.asList(new Long[] { programIDs.get(0) })), null, null, null, null).getPrograms().size(), 1);
        assertEquals(service.searchProgram(uc, new HashSet<Long>(Arrays.asList(new Long[] { programIDs.get(1) })), null, null, null, null).getPrograms().size(), 1);

        ProgramSearchType search = new ProgramSearchType();
        search.setProgramCategory("COM");
        assertEquals(service.searchProgram(uc, null, search, null, null, null).getPrograms().size(), 0);
        search.setProgramCategory("COMM");
        assertEquals(service.searchProgram(uc, null, search, null, null, null).getPrograms().size(), 2);

        search = new ProgramSearchType();
        search.setProgramDescription("XXXX");
        assertEquals(service.searchProgram(uc, null, search, null, null, null).getPrograms().size(), 0);
        search.setProgramDescription("MATH");
        assertEquals(service.searchProgram(uc, null, search, null, null, null).getPrograms().size(), 1);
        search.setProgramDescription("mat*");
        assertEquals(service.searchProgram(uc, null, search, null, null, null).getPrograms().size(), 1);
        search.setProgramDescription("comp*");
        assertEquals(service.searchProgram(uc, null, search, null, null, null).getPrograms().size(), 1);

        search = new ProgramSearchType();
        search.setFlexibleStart(false);
        assertEquals(service.searchProgram(uc, null, search, null, null, null).getPrograms().size(), 0);
        search.setFlexibleStart(true);
        assertEquals(service.searchProgram(uc, null, search, null, null, null).getPrograms().size(), 2);

        search = new ProgramSearchType();
        //search.setSupervisionId();
        search.setAgeRanges(new HashSet<>(Arrays.asList(new String[] { "ADULT" })));
        assertEquals(service.searchProgram(uc, null, search, null, null, null).getPrograms().size(), 2);

        search = new ProgramSearchType();
        search.setGenders(new HashSet<>(Arrays.asList(new String[] { "X" })));
        assertEquals(service.searchProgram(uc, null, search, null, null, null).getPrograms().size(), 0);
        search.setGenders(new HashSet<>(Arrays.asList(new String[] { "M" })));
        assertEquals(service.searchProgram(uc, null, search, null, null, null).getPrograms().size(), 2);

        search = new ProgramSearchType();
        search.setAmenities(new HashSet<>(Arrays.asList(new String[] { "WHEELCHAIR" })));
        assertEquals(service.searchProgram(uc, null, search, null, null, null).getPrograms().size(), 2);

        search = new ProgramSearchType();
        search.setProgramCaseWorkStep("AA");
        assertEquals(service.searchProgram(uc, null, search, null, null, null).getPrograms().size(), 1);
    }

    @Test(dependsOnMethods = { "searchProgram" })
    public void updateProgram() {
        for (Long id : programIDs) {
            ProgramType program = service.getProgram(uc, id);
            ProgramType ret = service.updateProgram(uc, program);
            assertEquals(ret, program);

            // program.setProgramDescription("After updated");
            Set<TargetInmateType> inmates = new HashSet<>();
            TargetInmateType inmate = new TargetInmateType();
            Set<String> ageRanges = new HashSet<>();
            ageRanges.add("ADULT");
            ageRanges.add("JUVENILE");
            inmate.setAgeRanges(ageRanges);

            Set<String> amenities = new HashSet<>();
            amenities.add("WHEELCHAIR");
            amenities.add("HSENS");
            inmate.setAmenities(amenities);

            Set<String> genders = new HashSet<>();
            genders.add("M");
            genders.add("F");
            genders.add("U");
            inmate.setGenders(genders);

            inmates.add(inmate);
            program.setTargetInmates(inmates);
            assertEquals(service.updateProgram(uc, program), program);
        }
    }

    @Test(dependsOnMethods = { "updateProgram" }, enabled = true)
    public void isDuplicateProgram() {
        assertTrue(service.isDuplicateProgram(uc, "MATH", null));
        assertFalse(service.isDuplicateProgram(uc, "Coke", null));

        assertFalse(service.isDuplicateProgram(uc, "MATH", programIDs.get(0)));
        assertTrue(service.isDuplicateProgram(uc, "MATH", programIDs.get(1)));
        assertFalse(service.isDuplicateProgram(uc, "MATHXXXXX", programIDs.get(1)));
        assertFalse(service.isDuplicateProgram(uc, "Computer", programIDs.get(1)));

        Set<Long> ids = programCodeMap.keySet();
        for (Long id : ids) {
            assertFalse(service.isDuplicateProgram(uc, programCodeMap.get(id), id));
        }
    }

    @Test(dependsOnMethods = { "isDuplicateProgram" }, enabled = true)
    public void createProgramOffering() {
        ProgramOfferingType offering = new ProgramOfferingType();
        offering.setOfferingCode("K200");
        offering.setOfferingDescription("K 200");
        // offering.setStartDate(getDateTime(new Date(), dateTime(1970, 1, 1, 8, 0, 0)));
        offering.setStartDate(BeanHelper.createDateWithTime(BeanHelper.getPastDate(new Date(), 20), 8, 0, 0));
        offering.setEndDate(BeanHelper.createDateWithTime(nextMonth(1), 10, 0, 0));
        offering.setEndTime(dateTime(1970, 1, 1, 10, 0, 0));
        offering.setExpiryDate(null);

        offering.setCapacity(20L);
        offering.setParticipationFee(BigDecimal.valueOf(19.98));
        offering.setSingleOccurrence(false);
        offering.setGenerateSchedule(true);
        offering.setRecurranceDays(new HashSet<>(Arrays.asList(new String[] { "TUE", "THU" })));

        Set<ScheduleSessionDateType> addedDates = new HashSet<>();
        ScheduleSessionDateType addedDate = new ScheduleSessionDateType();
        addedDate.setDate(new LocalDate(offering.getStartDate()).plusDays(2));
        addedDate.setStartTime(new LocalTime(20, 0, 0));
        addedDate.setEndTime(new LocalTime(22, 0, 0));
        addedDates.add(addedDate);
        offering.setAddedDates(addedDates);

        Set<ScheduleSessionDateType> removedDates = new HashSet<>();
        ScheduleSessionDateType removedDate = new ScheduleSessionDateType();
        removedDate.setDate(new LocalDate(offering.getStartDate()).plusDays(6));
        removedDate.setStartTime(new LocalTime(8, 0, 0));
        removedDate.setEndTime(new LocalTime(10, 0, 0));
        removedDates.add(removedDate);
        offering.setRemovedDates(removedDates);

        offering.setDay(10L);
        offering.setDate(BeanHelper.createDateWithTime(BeanHelper.getPastDate(new Date(), 20), 8, 0, 0));
        offering.setLocationId(1000L);
        offering.setLocationDescription("Richmond");
        offering.setInternalLocationId(2000L);
        offering.setOrganizationId(organizationId);
        offering.setPersonId(100L);
        offering.setFacilityId(this.facilityId);
        offering.setTerminationReason("NOTAVAIL");
        CommentType comment = new CommentType();
        comment.setUserId(uc.getConsumingApplicationId());
        comment.setComment("Program Offering");
        comment.setCommentDate(dateTime(new Date()));
        offering.setComments(comment);

        offering.setProgram(service.getProgram(uc, programIDs.get(0)));

        offering.setTargetInmates(createInmates());

        ProgramOfferingType ret = service.createProgramOffering(uc, offering);
        assertNotNull(ret);
        assertNotNull(ret.getOfferingId());
        assertNotNull(ret.getScheduleId());
        assertTrue(ret.getGenerateSchedule());
        assertFalse(ret.getSingleOccurrence());

        offering.setOfferingId(ret.getOfferingId());
        offering.setScheduleId(ret.getScheduleId());
        ret.setStatus("ACTIVE");

        assertEquals(ret, offering);

        programOfferingIDs.add(ret.getOfferingId());
        assertEquals(service.getProgramOffering(uc, ret.getOfferingId()), ret);

        offering = new ProgramOfferingType();
        offering.setOfferingCode("K300");
        offering.setOfferingDescription("K 300");
        offering.setStartDate(getDateTime(new Date(), dateTime(1970, 1, 1, 8, 0, 0)));
        offering.setEndDate(getDateTime(nextMonth(1), dateTime(1970, 1, 1, 14, 30, 0)));
        offering.setEndTime(dateTime(1970, 1, 1, 14, 30, 0));
        offering.setExpiryDate(null);
        offering.setCapacity(20L);
        offering.setParticipationFee(BigDecimal.valueOf(19.98));
        offering.setSingleOccurrence(true);
        offering.setGenerateSchedule(false);
        offering.setDay(10L);
        offering.setDate(dateTime(new Date()));
        offering.setLocationId(1000L);
        offering.setLocationDescription("Richmond");
        offering.setInternalLocationId(2000L);
        offering.setOrganizationId(organizationId2);
        offering.setPersonId(100L);
        offering.setFacilityId(facilityId2);
        offering.setTerminationReason("NOTAVAIL");
        comment = new CommentType();
        comment.setUserId(uc.getConsumingApplicationId());
        comment.setComment("Program Offering");
        comment.setCommentDate(dateTime(new Date()));
        offering.setComments(comment);

        offering.setProgram(service.getProgram(uc, programIDs.get(1)));

        offering.setTargetInmates(createInmates());

        ret = service.createProgramOffering(uc, offering);
        assertNotNull(ret);
        assertNotNull(ret.getOfferingId());

        offering.setOfferingId(ret.getOfferingId());
        offering.setStatus("ACTIVE");
        assertEquals(ret, offering);

        programOfferingIDs.add(ret.getOfferingId());
        assertEquals(service.getProgramOffering(uc, ret.getOfferingId()), ret);
    }

    @Test(dependsOnMethods = { "createProgramOffering" }, enabled = true)
    public void updateProgramOffering() {
        int count = 0;
        for (Long id : programOfferingIDs) {
            ProgramOfferingType programOffering = service.getProgramOffering(uc, id);
            ProgramOfferingType ret = service.updateProgramOffering(uc, programOffering);
            //assertEquals(ret, programOffering);
            if (programOffering.getScheduleId() != null) {
                assertNotNull(ret.getScheduleId());
                count++;
            }
        }
        assertTrue(count > 0);
    }

    @Test(dependsOnMethods = { "updateProgramOffering" }, enabled = true)
    public void getProgramOfferingTimeslot() {
        boolean flag = false;
        for (Long offeringId : this.programOfferingIDs) {
            ProgramOfferingType programOffering = service.getProgramOffering(uc, offeringId);
            if (programOffering.getScheduleId() != null) {
                assertTrue(programOffering.getRecurranceDays().size() > 0);
                flag = true;
                int count = 0;
                TimeslotIterator it = service.getProgramTimeslotByScheduleId(uc, programOffering.getScheduleId(), new Date(), null, null);
                while (it.hasNext() && ++count < 101) {
                    ScheduleTimeslotType timeslot = it.next();
                    assertEquals(timeslot.getScheduleID(), programOffering.getScheduleId());
                    System.out.println(++count + ": Timeslot: " + programOffering.getScheduleId() + ": " + timeslot.getTimeslotStartDateTime() + " -- "
                            + timeslot.getTimeslotEndDateTime());
                }
                it = service.getProgramTimeslotByProgramOfferingId(uc, programOffering.getOfferingId(), new Date(), null, null);
                while (it.hasNext() && ++count < 101) {
                    ScheduleTimeslotType timeslot = it.next();
                    assertEquals(timeslot.getScheduleID(), programOffering.getScheduleId());
                }
            }
        }
        assertTrue(flag);

        System.out.println("======================================");

        ProgramOfferingType programOffering = new ProgramOfferingType(); // service.getProgramOffering(uc, offeringId);
        programOffering.setStartDate(getDateTime(new Date(), dateTime(1970, 1, 1, 8, 0, 0)));
        programOffering.setEndDate(getDateTime(nextMonth(1), dateTime(1970, 1, 1, 14, 30, 0)));
        programOffering.setEndTime(dateTime(1970, 1, 1, 14, 30, 0));
        programOffering.setExpiryDate(null);

        programOffering.setSingleOccurrence(false);
        programOffering.setGenerateSchedule(true);
        programOffering.setRecurranceDays(new HashSet<>(Arrays.asList(new String[] { "MON", "WED" })));

        int count = 0;
        TimeslotIterator it = service.getProgramTimeslot(uc, programOffering, programOffering.getStartDate(), null, null);
        while (it.hasNext() && count < 100) {
            ScheduleTimeslotType timeslot = it.next();
            System.out.println(
                    ++count + ": Timeslot: " + programOffering.getScheduleId() + ": " + timeslot.getTimeslotStartDateTime() + " -- " + timeslot.getTimeslotEndDateTime());
        }
    }

    @Test(dependsOnMethods = { "updateProgramOffering" }, enabled = true)
    public void searchProgramOffering() {
        ProgramOfferingSearchType search = new ProgramOfferingSearchType();
        Set<Long> permittedFacilities = new HashSet<>();
        permittedFacilities.add(facilityId);
        permittedFacilities.add(facilityId2);
        search.setPermittedFacilities(permittedFacilities);

        search.setOfferingDescription("XXXXXXXX");
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 0);

        search.setOfferingDescription("* 20*");
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 1);

        search.setOfferingDescription("K 200");
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 1);

        search.setOfferingDescription("K200");
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 1);

        search = new ProgramOfferingSearchType();
        search.setPermittedFacilities(permittedFacilities);

        search.setGenders(new HashSet<String>(Arrays.asList(new String[] { "M", "F" })));
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 2);

        search.setGenders(new HashSet<String>(Arrays.asList(new String[] { "M" })));
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 2);

        search.setGenders(new HashSet<String>(Arrays.asList(new String[] { "F" })));
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 2);

        search.setGenders(new HashSet<String>(Arrays.asList(new String[] { "U" })));
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 0);

        search = new ProgramOfferingSearchType();
        search.setPermittedFacilities(permittedFacilities);
        search.setAgeRanges(new HashSet<String>(Arrays.asList(new String[] { "ADULT" })));
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 2);

        search = new ProgramOfferingSearchType();
        search.setPermittedFacilities(permittedFacilities);
        search.setAmenities(new HashSet<String>(Arrays.asList(new String[] { "WHEELCHAIR" })));
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 2);

        search = new ProgramOfferingSearchType();
        search.setPermittedFacilities(permittedFacilities);
        search.setProgramCategory("COMM");
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 2);

        search = new ProgramOfferingSearchType();
        search.setPermittedFacilities(permittedFacilities);
        search.setProgramCategory("XXXX");
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 0);

        int count = 0;
        for (Long offeringId : programOfferingIDs) {
            ProgramOfferingType programmOffering = service.getProgramOffering(uc, offeringId);
            ProgramType program = programmOffering.getProgram();
            if (program != null) {
                Long programId = program.getProgramId();
                program = service.getProgram(uc, programId);
                search = new ProgramOfferingSearchType();
                search.setPermittedFacilities(permittedFacilities);
                search.setProgramCode("XXXX");
                assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 0);

                search = new ProgramOfferingSearchType();
                search.setPermittedFacilities(permittedFacilities);
                search.setProgramCode(program.getProgramCode());
                assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 1);

                search = new ProgramOfferingSearchType();
                search.setPermittedFacilities(permittedFacilities);
                search.setProgramCode(program.getProgramCode().substring(0, program.getProgramCode().length() - 2) + "*");
                assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 1);

                search = new ProgramOfferingSearchType();
                search.setPermittedFacilities(permittedFacilities);
                search.setProgramDescription("*m*");
                assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 2);

                search = new ProgramOfferingSearchType();
                search.setPermittedFacilities(permittedFacilities);
                search.setProgramDescription("xx*");
                assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 0);

                search = new ProgramOfferingSearchType();
                search.setPermittedFacilities(permittedFacilities);
                search.setProgramId(programId);
                assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 1);
            }
            count++;
        }
        assertTrue(count > 0);

        search = new ProgramOfferingSearchType();
        search.setPermittedFacilities(permittedFacilities);
        search.setStatus("ACTIVE");
        assertTrue(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size() > 0);

    }

    @Test(dependsOnMethods = { "searchProgramOffering" }, enabled = true)
    public void createProgramAssignment() {
        ProgramAssignmentType programAssignment = new ProgramAssignmentType();
        programAssignment.setReferralDate(dateTime(new Date()));
        programAssignment.setReferralPriority("HIGH");
        programAssignment.setReferralRejectionReason(null);
        programAssignment.setStartDate(dateTime(new Date()));
        programAssignment.setEndDate(nextMonth(1));
        programAssignment.setStatus("CANCELLED");
        //programAssignment.setSuspendReason("HEALTH");
        programAssignment.setAbandonReason(null);

        CommentType comment = new CommentType();
        comment.setUserId(uc.getConsumingApplicationId());
        comment.setComment("Math Program");
        comment.setCommentDate(dateTime(new Date()));
        programAssignment.setComments(comment);
        programAssignment.setSupervisionId(supervisionId);

        ProgramType program = service.getProgram(uc, this.programIDs.get(0));
        programAssignment.setProgram(program);

        ProgramOfferingType programOffering = service.getProgramOffering(uc, this.programOfferingIDs.get(0));
        programAssignment.setProgrammOffering(programOffering);

        Set<ScheduleSessionDateType> addedDates = new HashSet<>();
        ScheduleSessionDateType addedDate = new ScheduleSessionDateType();
        addedDate.setDate(new LocalDate(programOffering.getStartDate()).plusDays(3));
        addedDate.setStartTime(new LocalTime(20, 0, 0));
        addedDate.setEndTime(new LocalTime(22, 0, 0));
        addedDates.add(addedDate);
        programAssignment.setAddedDates(addedDates);

        Set<ScheduleSessionDateType> removedDates = new HashSet<>();
        ScheduleSessionDateType removedDate = new ScheduleSessionDateType();
        removedDate.setDate(new LocalDate(programOffering.getStartDate()).plusDays(6));
        removedDate.setStartTime(new LocalTime(8, 0, 0));
        removedDate.setEndTime(new LocalTime(10, 0, 0));
        removedDates.add(removedDate);
        programAssignment.setRemovedDates(removedDates);

        // ProgramAttendanceType programAttendance = service.getProgramAttendance(uc, programAttendanceIDs.get(0));
        // programAssignment.setProgramAttendance(programAttendance);

        ProgramAssignmentType ret = service.createProgramAssignment(uc, programAssignment);
        assertNotNull(ret);
        assertNotNull(ret.getAssignmentId());
        assertNotNull(ret.getAddedDates());
        assertEquals(ret.getAddedDates().size(), 1);
        assertNotNull(ret.getRemovedDates());
        assertEquals(ret.getRemovedDates().size(), 1);
        programAssignmentIDs.add(ret.getAssignmentId());

        programAssignment = new ProgramAssignmentType();
        programAssignment.setReferralDate(dateTime(new Date()));
        programAssignment.setReferralPriority("LOW");
        programAssignment.setReferralRejectionReason(null);
        programAssignment.setStartDate(dateTime(new Date()));
        programAssignment.setEndDate(nextMonth(1));
        programAssignment.setStatus("APPROVED");
        //programAssignment.setSuspendReason("HEALTH");
        programAssignment.setAbandonReason(null);

        comment = new CommentType();
        comment.setUserId(uc.getConsumingApplicationId());
        comment.setComment("CLEAN");
        comment.setCommentDate(dateTime(new Date()));
        programAssignment.setComments(comment);
        programAssignment.setSupervisionId(supervisionId);

        program = service.getProgram(uc, this.programIDs.get(1));
        programAssignment.setProgram(program);

        programOffering = service.getProgramOffering(uc, this.programOfferingIDs.get(1));
        programAssignment.setProgrammOffering(programOffering);

        // programAttendance = service.getProgramAttendance(uc, programAttendanceIDs.get(1));
        // programAssignment.setProgramAttendance(programAttendance);

        ret = service.createProgramAssignment(uc, programAssignment);
        assertNotNull(ret);
        assertNotNull(ret.getAssignmentId());
        programAssignmentIDs.add(ret.getAssignmentId());

        ProgramOfferingSearchType search = new ProgramOfferingSearchType();
        search.setSupervisionId(1111111L);
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getTotalSize().intValue(), 0);
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 0);

        search = new ProgramOfferingSearchType();
        Set<Long> permittedFacilities = new HashSet<>();
        permittedFacilities.add(facilityId);
        permittedFacilities.add(facilityId2);
        search.setPermittedFacilities(permittedFacilities);
        search.setSupervisionId(supervisionId);
        search.setProgramAssignmentStatus(new HashSet<>(Arrays.asList(new String[] { "CANCELLED", "APPROVED" })));
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getTotalSize().intValue(), 2);
        assertEquals(service.searchProgramOffering(uc, null, search, null, null, null).getProgramOfferings().size(), 2);

        assertEquals(service.getProgramOfferingsBySupervisionId(uc, supervisionId).size(), 2);
        //assertEquals(service.getProgramOfferingsBySupervisionId(uc, 2L).size(), 1);

        ProgramAssignmentSearchType query = new ProgramAssignmentSearchType();
        query.setSupervisionId(1111111L);
        ProgramAssignmentReturnType assignmentRet = service.searchProgramAssignment(uc, null, query, null, null, null);
        assertEquals(assignmentRet.getTotalSize().intValue(), 0);
        assertNull(assignmentRet.getProgramAssignments());

        query = new ProgramAssignmentSearchType();
        query.setSupervisionId(1111111L);
        assignmentRet = service.searchProgramAssignment(uc, null, query, null, null, null);
        assertEquals(assignmentRet.getTotalSize().intValue(), 0);
        assertNull(assignmentRet.getProgramAssignments());

        query = new ProgramAssignmentSearchType();
        query.setSupervisionId(supervisionId);
        assignmentRet = service.searchProgramAssignment(uc, null, query, null, null, null);
        assertEquals(assignmentRet.getTotalSize().intValue(), 2);
        assertEquals(assignmentRet.getProgramAssignments().size(), 2);

        query = new ProgramAssignmentSearchType();
        query.setStatus(new HashSet<String>(Arrays.asList(new String[] { "CANCELLED" })));
        assignmentRet = service.searchProgramAssignment(uc, null, query, null, null, null);
        assertEquals(assignmentRet.getTotalSize().intValue(), 1);
        assertEquals(assignmentRet.getProgramAssignments().size(), 1);
        for (ProgramAssignmentType assignment : assignmentRet.getProgramAssignments()) {
            assertNotNull(assignment.getProgrammOffering().getOfferingId());
            assertNotNull(assignment.getProgram().getProgramId());
        }

        /*ProgramSearchType ps = new ProgramSearchType();
        ps.setSupervisionId(1L);
        ps.setProgramAssignmentStatus(new HashSet<>(Arrays.asList(new String[]{"APPROVED"})));
        ps.setExcludeSupervision(true);
        ps.setAgeRanges(new HashSet<>(Arrays.asList(new String[]{"ADULT"})));
        ps.setGenders(new HashSet<>(Arrays.asList(new String[]{"M"})));
        ProgramReturnType psRet = service.searchProgram(uc, null, ps, null, null, null);
        assertEquals(psRet.getTotalSize().intValue(), 1);
        System.out.println(psRet.getPrograms());
        assertEquals(psRet.getPrograms().size(), 1);
        for (ProgramType p: psRet.getPrograms()) {
            Set<TargetInmateType> inmates = p.getTargetInmates();
            for (TargetInmateType i: inmates) {
                i.getAgeRanges().contains("ADULT");
                i.getAmenities().contains("WHEELCHAIR");
                i.getGenders().contains("M");
            }
        }*/
    }

    @Test(dependsOnMethods = { "createProgramAssignment" }, enabled = true)
    public void isAssigedAlready() {
        /*assertFalse(service.isAssigedAlready(uc, facilityId, programOfferingIDs.get(0)));
        assertTrue(service.isAssigedAlready(uc, facilityId, programOfferingIDs.get(1)));
		assertFalse(service.isAssigedAlready(uc, facilityId2, programOfferingIDs.get(0)));
		assertFalse(service.isAssigedAlready(uc, facilityId2, programOfferingIDs.get(1)));*/
    }

    @Test(dependsOnMethods = { "isAssigedAlready" }, enabled = true)
    public void updateProgramAssignment() {
        int count = 0;
        for (Long id : programAssignmentIDs) {
            count++;
            ProgramAssignmentType programAssignment = service.getProgramAssignment(uc, id);
            String status = programAssignment.getStatus();
            programAssignment.setStatus("REFERRED");
            ProgramAssignmentType ret = service.updateProgramAssignment(uc, programAssignment);
            assertNotNull(ret);
            assertEquals(ret, programAssignment);
            assertEquals(ret.getStatus(), "REFERRED");

            programAssignment.setStatus("COMPLETED");
            ret = service.updateProgramAssignment(uc, programAssignment);
            assertNotNull(ret);
            assertEquals(ret, programAssignment);
            assertEquals(ret.getStatus(), "COMPLETED");

            programAssignment.setStatus("CANCELLED");
            ret = service.updateProgramAssignment(uc, programAssignment);
            assertNotNull(ret);
            assertEquals(ret, programAssignment);
            assertEquals(ret.getStatus(), "CANCELLED");

            programAssignment.setStatus(status);
            ret = service.updateProgramAssignment(uc, programAssignment);
            assertNotNull(ret);
            assertEquals(ret, programAssignment);
            assertEquals(ret.getStatus(), status);

            programAssignment.setStatus("ALLOCATED");
            ret = service.updateProgramAssignment(uc, programAssignment);
            assertNotNull(ret);
            assertEquals(ret, programAssignment);
            assertEquals(ret.getStatus(), "ALLOCATED");
        }
        assertTrue(count > 0);
    }

    @Test(dependsOnMethods = { "updateProgramAssignment" }, enabled = true)
    public void createProgramAttendance() {
        ProgramAttendanceType ret = null;
        ProgramAttendanceType programAttendance = new ProgramAttendanceType();
        programAttendance.setAttendanceDate(getDateOnly(BeanHelper.getPastDate(new Date(), 20)));
        programAttendance.setAttendanceStartTime(getZeroEpochTime(8, 0));
        programAttendance.setAttendanceEndTime(getZeroEpochTime(10, 0));
        // programAttendance.setActivityId(activityId);
        programAttendance.setAttended("YES");

        CommentType comment = new CommentType();
        comment.setUserId(uc.getConsumingApplicationId());
        comment.setComment("Attendance");
        comment.setCommentDate(dateTime(new Date()));
        programAttendance.setComments(comment);

        programAttendance.setPerformance("ASLEEP");
        programAttendance.setProgramAssignment(service.getProgramAssignment(uc, this.programAssignmentIDs.get(0)));
        Long scheduleId = programAttendance.getProgramAssignment().getProgrammOffering().getScheduleId();
        if (scheduleId != null) {
            TimeslotIterator it = service.getProgramTimeslotByScheduleId(uc, scheduleId, BeanHelper.getPastDate(new Date(), 20), null, 60L);
            if (it != null) {
                while (it.hasNext()) {
                    ScheduleTimeslotType scheduleTimeslot = it.next();
                    ret = service.createOrUpdateProgramAttendance(uc, programAttendance, scheduleTimeslot);
                    if (scheduleTimeslot.getTimeslotStartDateTime().after(new Date())) {
                        assertNull(ret);
                    } else {
                        assertNotNull(ret);
                        assertNotNull(ret.getAttendanceId());
                        assertNotNull(ret.getActivityId());
                        assertNotNull(activityService.get(uc, ret.getActivityId()));
                        //assertEquals(ret, service.getProgramAttendance(uc, ret.getAttendanceId()));
                        System.out.println("Attendance: " + ret);
                        programAttendanceIDs.add(ret.getAttendanceId());
                    }
                }
            }
        } else {
            ret = service.createOrUpdateProgramAttendance(uc, programAttendance, null);
            assertNotNull(ret);
            assertNotNull(ret.getAttendanceId());
            assertNotNull(ret.getActivityId());
            //assertEquals(ret, service.getProgramAttendance(uc, ret.getAttendanceId()));
            System.out.println("Attendance: " + ret);
            programAttendanceIDs.add(ret.getAttendanceId());
        }

        programAttendance = new ProgramAttendanceType();
        programAttendance.setAttendanceDate(getDateOnly(new Date()));
        programAttendance.setAttendanceStartTime(getZeroEpochTime(8, 0));
        programAttendance.setAttendanceEndTime(getZeroEpochTime(10, 30));
        // programAttendance.setActivityId(activityId);
        programAttendance.setAttended("NO");

        comment = new CommentType();
        comment.setUserId(uc.getConsumingApplicationId());
        comment.setComment("Not Attended");
        comment.setCommentDate(dateTime(new Date()));
        programAttendance.setComments(comment);

        programAttendance.setPerformance("INTOX");
        programAttendance.setProgramAssignment(service.getProgramAssignment(uc, this.programAssignmentIDs.get(1)));

        scheduleId = programAttendance.getProgramAssignment().getProgrammOffering().getScheduleId();
        if (scheduleId != null) {
            TimeslotIterator it = service.getProgramTimeslotByScheduleId(uc, scheduleId, new Date(), null, 60L);
            if (it != null) {
                while (it.hasNext()) {
                    ret = service.createOrUpdateProgramAttendance(uc, programAttendance, it.next());
                    assertNotNull(ret);
                    assertNotNull(ret.getAttendanceId());
                    assertNotNull(ret.getActivityId());
                    assertNotNull(activityService.get(uc, ret.getActivityId()));
                    //assertEquals(ret, service.getProgramAttendance(uc, ret.getAttendanceId()));
                    System.out.println("Attendance: " + ret);
                    programAttendanceIDs.add(ret.getAttendanceId());
                }
            }
        } else {
            ret = service.createOrUpdateProgramAttendance(uc, programAttendance, null);
            assertNotNull(ret);
            assertNotNull(ret.getAttendanceId());
            assertNotNull(ret.getActivityId());
            //assertEquals(ret, service.getProgramAttendance(uc, ret.getAttendanceId()));
            System.out.println("Attendance: " + ret);
            programAttendanceIDs.add(ret.getAttendanceId());
        }
    }

    @Test(dependsOnMethods = { "createProgramAttendance" }, enabled = true)
    public void updateProgramAttendance() {
    }

    @Test(dependsOnMethods = { "updateProgramAttendance" }, enabled = true)
    public void getEnrolledSupervisions() {
        boolean flag = false;
        for (Long offeringId : programOfferingIDs) {
            List<EnrolledSupervisionType> enrolled = service.getEnrolledSupervisions(uc, offeringId);
            assertNotNull(enrolled);
            assertTrue(enrolled.size() > 0);
            for (EnrolledSupervisionType e : enrolled) {
                assertEquals(e.getSupervisionId(), this.supervisionId);
                assertNotNull(e.getFirstName());
                assertNotNull(e.getLastName());
                assertNotNull(e.getOffenderNumber());
                assertNotNull(e.getPersonId());
                assertNotNull(e.getPersonIdentityId());

                assertTrue(e.getSupervisionId().longValue() > 0L);
                assertTrue(e.getFirstName().length() > 0);
                assertTrue(e.getLastName().length() > 0);
                assertTrue(e.getOffenderNumber().length() > 0);
                assertTrue(e.getPersonId().longValue() > 0L);
                assertTrue(e.getPersonIdentityId().longValue() > 0L);

                flag = true;
            }
        }
        assertTrue(flag);
    }

    @Test(dependsOnMethods = { "updateProgramAssignment", "updateProgramAttendance" }, enabled = true)
    public void deleteProgramAttendance() {
        for (Long id : programAttendanceIDs) {
            ProgramAttendanceType attendance = service.getProgramAttendance(uc, id);
            assertNotNull(attendance);
            service.deleteProgramAttendance(uc, id);
            assertNull(service.getProgramAttendance(uc, id));
        }
    }

    @Test(dependsOnMethods = { "deleteProgramAttendance" }, enabled = true)
    public void deleteProgramAssignment() {
        boolean flag = false;
        for (Long id : programAssignmentIDs) {
            ProgramAssignmentType programAssignment = service.getProgramAssignment(uc, id);
            assertNotNull(programAssignment);
            service.deleteProgramAssignment(uc, id);
            programAssignment = service.getProgramAssignment(uc, id);
            assertNull(programAssignment);
            flag = true;
        }
        assertTrue(flag);
        assertEquals(service.getAllProgramAssignment(uc).size(), 0);
    }

    @Test(dependsOnMethods = { "deleteProgramOffering" }, enabled = true)
    public void deleteProgram() {
        assertTrue(service.getAllProgram(uc).size() > 0);
        for (Long id : programIDs) {
            service.deleteProgram(uc, id);
        }
        assertEquals(service.getAllProgram(uc).size(), 0);
    }

    @Test(dependsOnMethods = { "deleteProgramAssignment" }, enabled = true)
    public void deleteProgramOffering() {
        for (Long id : programOfferingIDs) {
            assertNotNull(service.getProgramOffering(uc, id));
            service.deleteProgramOffering(uc, id);
            assertNull(service.getProgramOffering(uc, id));
        }
    }

    private Set<TargetInmateType> createInmates() {
        Set<TargetInmateType> inmates = new HashSet<>();
        TargetInmateType inmate = new TargetInmateType();
        Set<String> ageRanges = new HashSet<>();
        ageRanges.add("ADULT");
        ageRanges.add("JUVENILE");
        inmate.setAgeRanges(ageRanges);

        Set<String> amenities = new HashSet<>();
        amenities.add("WHEELCHAIR");
        amenities.add("HSENS");
        inmate.setAmenities(amenities);

        Set<String> genders = new HashSet<>();
        genders.add("M");
        genders.add("F");
        inmate.setGenders(genders);
        inmates.add(inmate);

        return inmates;
    }

    private Date nextMonth(int n) {
        return new LocalDate().plusMonths(n).toDateMidnight().toDate();
    }

    private Date nextDays(Date date, int n) {
        return new LocalDate(date).plusDays(n).toDateMidnight().toDate();
    }

    private Date dateTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    private Date dateTime(int year, int month, int day, int hour, int minute, int second) {
        Calendar c = Calendar.getInstance();
        c.clear();
        c.set(Calendar.YEAR, year);
        c.set(year, month + 1, day, hour, minute, second);
        return c.getTime();
    }

    private Date getDateTime(Date date, Date time) {
        if (date == null) {
            return null;
        }
        if (time == null) {
            return date;
        }
        Calendar d = Calendar.getInstance();
        d.setTime(date);
        Calendar t = Calendar.getInstance();
        t.setTime(time);

        d.set(Calendar.HOUR_OF_DAY, t.get(Calendar.HOUR_OF_DAY));
        d.set(Calendar.MINUTE, t.get(Calendar.MINUTE));
        d.set(Calendar.SECOND, 0);
        d.set(Calendar.MILLISECOND, 0);

        return d.getTime();
    }

    private Date getDateOnly(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    private Date getZeroEpochTime(Date dateTime) {
        if (dateTime == null) {
			return null;
		}

        Calendar dt = Calendar.getInstance();
        dt.setTime(dateTime);

        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, 1970);
        c.set(Calendar.MONTH, Calendar.JANUARY);
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, dt.get(Calendar.HOUR_OF_DAY));
        c.set(Calendar.MINUTE, dt.get(Calendar.MINUTE));
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    private Date getZeroEpochTime(int hour, int minute) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, 1970);
        c.set(Calendar.MONTH, Calendar.JANUARY);
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }
}
