package syscon.arbutus.product.services.facility.contract.ejb;

import java.util.*;

import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.common.LocationTest;
import syscon.arbutus.product.services.common.ModuleConfig;
import syscon.arbutus.product.services.common.PersonIdentityTest;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.contract.dto.SearchSetMode;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facility.contract.dto.FacilitiesReturn;
import syscon.arbutus.product.services.facility.contract.dto.FacilitySearch;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facility.contract.util.DateUtil;
import syscon.arbutus.product.services.facilitycountactivity.contract.interfaces.FacilityCountActivityService;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.location.contract.dto.Location;
import syscon.arbutus.product.services.location.contract.interfaces.LocationService;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;
import syscon.arbutus.product.services.organization.contract.dto.Organization;
import syscon.arbutus.product.services.organization.contract.interfaces.OrganizationService;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

import static org.testng.Assert.*;

@ModuleConfig
public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);
    Long parentId = null;
    Set<Long> organizations = new HashSet<Long>();
    private FacilityService service;
    private OrganizationService orgService;
    private PersonService personService;
    private LocationService locService;
    private MovementService maService;
    private ActivityService aService;
    private SupervisionService supService;
    private FacilityCountActivityService fcaService;
    private FacilityInternalLocationService filService;
    private PersonIdentityTest piTest;
    private Long personId = 0L;

    public static Long randLong(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return Long.valueOf((long) randomNum);
    }

    @BeforeMethod
    public void beforeMethod() {
        SecurityClient client;
        try {
            client = SecurityClientFactory.getSecurityClient();
            client.setSimple("devtest", "devtest"); // nice regular service user
            client.login();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @BeforeClass
    public void beforeClass() {
        log.info("beforeClass Begin - JNDI lookup");
        service = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        orgService = (OrganizationService) JNDILookUp(this.getClass(), OrganizationService.class);
        locService = (LocationService) JNDILookUp(this.getClass(), LocationService.class);
        personService = (PersonService) JNDILookUp(this.getClass(), PersonService.class);
        fcaService = (FacilityCountActivityService) JNDILookUp(this.getClass(), FacilityCountActivityService.class);
        uc = super.initUserContext();
        supService = (SupervisionService) JNDILookUp(this.getClass(), SupervisionService.class);
        maService = (MovementService) JNDILookUp(this.getClass(), MovementService.class);
        aService = (ActivityService) JNDILookUp(this.getClass(), ActivityService.class);
        filService = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);

        piTest = new PersonIdentityTest();
    }

    @Test
    public void lookupJNDI() {
        log.info("lookupJNDI Begin");
        assertNotNull(service);
    }

    @Test(enabled = true)
    public void create() {

        clearServices();
        Long lng = 1L;
        createData();

        Facility ret = null;
        String category = new String("COMMU");
        Facility facility = new Facility(-1L, "FacilityCodeIT" + new Random().nextLong(), "FacilityName", new Date(), category, null, null, organizations,
                piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        assertNotEquals(ret, null);

        lng++;

        facility = new Facility(-1L, "FacilityCodeIT" + new Random().nextLong(), "FacilityName1", new Date(), category, null, null, organizations,
                piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        assertNotEquals(ret, null);

        lng++;
        organizations = new HashSet<Long>();
        organizations.add(999L);

        facility = new Facility(-1L, "FacilityCodeIT" + new Random().nextLong(), "FacilityName1", new Date(), category, null, null, organizations,
                piTest.createPersonIdentity(), false);

        lng++;
        try {
            ret = service.create(uc, facility);
            assertEquals(ret, null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(DataNotExistException.class));
        }

    }

    @Test(enabled = true)
    public void createByGivenId() {

        clearServices();
        Long lng = randLong(1, 10000);
        createData();

        Facility ret = null;

        String category = new String("COMMU");

        Facility facility = new Facility(lng, "FacilityCodeIT" + new Random().nextLong(), "FacilityName1", new Date(), category, null, null, organizations,
                piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilityIdentification(), lng);

        lng = randLong(1, 10000);

        facility = new Facility(lng, "FacilityCodeIT2" + new Random().nextLong(), "FacilityName1", new Date(), category, null, null, organizations,
                piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilityIdentification(), lng);

        lng = -1L;

        facility = new Facility(-1L, "FacilityCodeI3" + new Random().nextLong(), "FacilityName1", new Date(), category, null, null, organizations,
                piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        assertNotEquals(ret, null);
        assert (!ret.getFacilityIdentification().equals(lng));

        lng = randLong(1, 10000);

        facility = new Facility(lng, "FacilityCodeI4" + new Random().nextLong(), "FacilityName1", new Date(), category, null, null, organizations,
                piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilityIdentification(), lng);

    }

    @Test(enabled = true)
    public void update() {
        clearServices();
        createData();

        Long lng = 2L;

        Facility ret = null;

        String category = new String("COMMU");
        Facility facility = new Facility(-1L, "FacilityCodeI4" + new Random().nextLong(), "FacilityName1", new Date(), category, null, null, organizations,
                piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        assert (ret != null);

        Facility facilityType = ret;

        facilityType.setFacilityName("FacilityNameUpdate");
        facilityType.setFacilityCategoryText(new String("CUSTODY"));

        // Confirmation Test: Update OrganizationAssociation from 2L to 3L -- Fix Defect ID: 1192, by Y. Shang, Nov.19, 2012
        // Set it to new organization
        Long newOrgId = createOrganization();

        Set<Long> newOrg = new HashSet<Long>();
        newOrg.add(newOrgId);
        facilityType.setOrganizations(newOrg);

        ret = service.update(uc, facilityType);
        assert (ret != null);

        // Long retId = ret.getOrganizations().iterator().next();
        // log.error(String.format("***4 retId=%s",retId));
        // Confirmation Test: Update OrganizationAssociation from 2L to 3L -- Fix Defect ID: 1192 by Y. Shang, Nov.19, 2012
        // assertEquals(retId, newOrgId);
    }

    @Test(enabled = true)
    public void getOrganizations() {

        clearServices();
        createData();

        Facility ret = null;

        log.error(String.format("1"));
        String category = new String("COMMU");
        Facility facility = new Facility(-1L, "FacilityCodeI6" + new Random().nextLong(), "FacilityName1", new Date(), category, null, null, organizations,
                piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        log.error(String.format("2 ret%s", ret));
        assert (ret != null);
        Long facilityId = ret.getFacilityIdentification();
        Set<Long> orgs = ret.getOrganizations();
        log.error(String.format("2 %s", orgs));

        Long orgId = 0L;
        if (orgs.iterator().hasNext()) {
            orgId = orgs.iterator().next();

            Organization orgRet = orgService.get(uc, orgId);
            assert (orgRet != null);

            Long facId = null;
            try {
                facId = orgRet.getFacilities().iterator().next();
            } catch (Exception e) {
                log.error(String.format("Exception = %s ", e));
            }

            log.error(String.format("3 facId =%s facilityId = %s", facId, facilityId));
            assert (facId.equals(facilityId));
        }

    }

    @Test(enabled = true)
    public void active() {

        clearServices();
        createData();

        Facility ret = null;

        String category = new String("COMMU");
        Facility facility = new Facility(-1L, "FacilityCodeI4" + new Random().nextLong(), "FacilityName1" + new Random().nextLong(), new Date(), category, null,
                null, organizations, piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        assert (ret != null);

        Facility facilityType = ret;
        facilityType.setFacilityDeactivateDate(null);

        ret = service.activateFacility(uc, facilityType);
        assert (ret != null);
    }

    @Test(enabled = true)
    public void delete() {
        clearServices();
        createData();

        Facility ret = null;

        String category = new String("COMMU");
        Facility facility = new Facility(-1L, "FacilityCodeI4" + new Random().nextLong(), "FacilityName1" + new Random().nextLong(), new Date(), category, null,
                null, organizations, piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        assert (ret != null);

        Long facilityId = ret.getFacilityIdentification();
        Long retDelete = service.delete(uc, facilityId, personId);
        Long SUCCESS = 1L;
        assert (retDelete == SUCCESS);// 1 is Code for SUCCESS

    }

    @Test(enabled = true)
    public void deleteAll() {
        Long count = service.getCount(uc);
        assert (count > 0L);
        Long retDelete = service.deleteAll(uc);
        Long SUCCESS = 1L;
        assert (retDelete == SUCCESS);// 1 is Code for SUCCESS

        count = service.getCount(uc);
        assert (count == 0L);

    }

    @Test(enabled = true)
    public void get() {

        clearServices();
        createData();

        Facility ret = null;

        String category = new String("COURT");
        Facility facility = new Facility(-1L, "FacilityCodeI4" + new Random().nextLong(), "FacilityName1" + new Random().nextLong(), new Date(), category, null,
                null, organizations, piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        assert (ret != null);

        Long facilityId = ret.getFacilityIdentification();
        ret = service.get(uc, facilityId);
        assert (ret != null);
        assert (ret.getFacilityIdentification().equals(facilityId));

        String facilityCode = ret.getFacilityCode();
        ret = service.get(uc, facilityCode);
        assert (ret != null);
        assert (ret.getFacilityIdentification().equals(facilityId));
    }

    @Test(enabled = true)
    public void getAll() {

        clearServices();
        createData();

        Facility ret = null;

        Set<Long> rets = service.getAll(uc);

        String category = new String("COURT");
        Facility facility = new Facility(-1L, "FacilityCodeI4" + new Random().nextLong(), "FacilityName1" + new Random().nextLong(), new Date(), category, null,
                null, organizations, piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        assert (ret != null);

        Set<Long> rets2 = service.getAll(uc);
        assert (rets2.size() == rets.size() + 1);// there are 2 facilites, 1 is above , one from createData
    }

    @Test(enabled = true)
    public void getCount() {

        clearServices();

        Facility ret = null;

        Long retLng = service.getCount(uc);

        String category = new String("COURT");
        Facility facility = new Facility(-1L, "FacilityCodeI4" + new Random().nextLong(), "FacilityName1" + new Random().nextLong(), new Date(), category, null,
                null, organizations, piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        assert (ret != null);

        Long retLng2 = service.getCount(uc);
        assert (retLng2 == retLng + 1);
    }

    @Test(enabled = true)
    public void getNiem() {

        clearServices();

        Facility ret = null;

        String category = new String("COURT");
        Facility facility = new Facility(-1L, "FacilityCodeI4" + new Random().nextLong(), "FacilityName1" + new Random().nextLong(), new Date(), category, null,
                null, organizations, piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        assert (ret != null);

        Long facilityId = ret.getFacilityIdentification();
        String strNiem = service.getNiem(null, facilityId);

        assert (strNiem != null);
    }

    @Test(enabled = true)
    public void getStamp() {

        clearServices();
        createData();

        Facility ret = null;

        String category = new String("COURT");
        Facility facility = new Facility(-1L, "FacilityCodeI4" + new Random().nextLong(), "FacilityName1" + new Random().nextLong(), new Date(), category, null,
                null, organizations, piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        assert (ret != null);

        Long facilityId = ret.getFacilityIdentification();
        StampType retStamp = service.getStamp(null, facilityId);

        assert (retStamp != null);
    }

    @Test(enabled = true)
    public void getVersion() {

        clearServices();

        String ret = service.getVersion(uc);
        assert (ret != null);

    }

    @Test(enabled = true)
    public void searchCreation() {

        clearServices();
        //createData();

        //Create test data for search
        Date presentDate = DateUtil.createPresentDate();
        Date deactiveDate = DateUtil.createFutureDate(10);
        Long lng = 0L;

        //Active Record without deactiveDate
        for (int i = 1; i <= 5; i++) {
            lng = randLong(1, 1000000);
            ;

            Facility ret = null;

            String category = new String("COURT");
            Facility facility = new Facility(null, "FacilityCode" + lng, "FacilityName" + lng, null, category, new String("USS"), null, null,
                    piTest.createPersonIdentity(), false);
            ret = service.create(uc, facility);
            assert (ret != null);

        }

        //Active Record the deactiveDate 10 days later
        for (int i = 1; i <= 5; i++) {
            lng = randLong(1, 1000000);
            ;

            Facility ret = null;

            String category = new String("COURT");
            Facility facility = new Facility(null, "FacilityCode1" + lng, "FacilityName" + lng, deactiveDate, category, null,
                    createFacilty("FacilityCodeIT0" + new Random().nextLong()), null, piTest.createPersonIdentity(), false);
            ret = service.create(uc, facility);
            assert (ret != null);

        }

        //Inactive Record with deactiveDate is today
        for (int i = 1; i <= 5; i++) {
            lng = randLong(1, 1000000);
            ;

            Facility ret = null;

            String category = new String("COURT");
            Facility facility = new Facility(null, "FacilityCode2" + lng, "FacilityName" + lng, presentDate, category, null, null, null,
                    piTest.createPersonIdentity(), false);
            ret = service.create(uc, facility);
            assert (ret != null);

        }

        //Inactive Record with deactiveDate is 10 days before
        deactiveDate = DateUtil.createPastDate(10);
        for (int i = 1; i <= 5; i++) {
            lng = randLong(1, 1000000);
            ;

            Facility ret = null;

            String category = new String("COURT");
            Facility facility = new Facility(null, "FacilityCode3" + lng, "FacilityName" + lng, presentDate, category, null, null, null,
                    piTest.createPersonIdentity(), false);
            ret = service.create(uc, facility);
            assert (ret != null);

        }

    }

    private void clearServices() {
        //	    orgService.deleteAll(uc);
        //	    locService.deleteAll(uc);
        service.deleteAll(uc);
        //		maService.deleteAll(uc);
        parentId = null;
        organizations = new HashSet<Long>();
    }

    private void createData() {
        this.parentId = createFacilty("FacilityCodeIT0" + new Random().nextLong());
        this.organizations.add(createOrganization());
    }

    @Test(dependsOnMethods = { "searchCreation" }, enabled = true)
    public void search() {

        searchCreation();
        FacilitiesReturn ret = null;

        try {
            ret = service.search(null, null, null, 0L, 10L, null);
            assertNotEquals(ret, null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }

        FacilitySearch search = new FacilitySearch(null, null, null, null, null, null, null, null, null, null, null, null, null);
        ret = null;

        try {
            ret = service.search(null, null, search, 0L, 10L, null);
            assertNotEquals(ret, null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }

        search = new FacilitySearch(null, null, SearchSetMode.ALL, null, null, null, null, null, null, null, null, null, null);
        ret = null;

        try {
            ret = service.search(null, null, search, 0L, 10L, null);
            assertNotEquals(ret, null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }

        search = new FacilitySearch(null, null, SearchSetMode.ANY, null, null, null, null, null, null, null, null, null, null);
        ret = null;

        try {
            ret = service.search(null, null, search, 0L, 10L, null);
            assertNotEquals(ret, null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }

        search = new FacilitySearch(null, null, SearchSetMode.ALL, null, null, null, null, null, null, null, null, null, null);
        ret = null;

        try {
            ret = service.search(null, service.getAll(uc), search, 0L, 10L, null);
            assertNotEquals(ret, null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(javax.ejb.EJBException.class));
        }

        search = new FacilitySearch(null, null, SearchSetMode.ANY, null, null, null, null, null, null, null, null, null, null);
        ret = null;

        try {
            ret = service.search(null, service.getAll(uc), search, 0L, 10L, null);
            assertNotEquals(ret, null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(javax.ejb.EJBException.class));
        }

        /////////////////////////////////////////////////////////////////
        //Search by single condition
        /////////////////////////////////////////////////////////////////

        // search by facilityCode
        search = new FacilitySearch("FacilityCode" + new Random().nextLong(), null, SearchSetMode.ALL, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 10L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 0L);

        //select * from fac_facility where code like 'FacilityCode1%' order by createdatetime;
        search = new FacilitySearch("FacilityCode1", null, SearchSetMode.ALL, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 10L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 0L);

        search = new FacilitySearch("FacilityCode1*", null, SearchSetMode.ALL, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 10L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 10L);
        Set<Facility> facs = ret.getFacilities();
        Iterator<Facility> It = facs.iterator();
        assert ((It.next()).getFacilityContactId().longValue() > 5L);

        //search by facilityName only
        search = new FacilitySearch(null, "FacilityName", null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 30L, null);
        assertNotEquals(ret, null);
        assert (ret.getFacilities().size() > 5L);

        //search by Organization AssociationType only
        search = new FacilitySearch(null, null, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 30L, null);
        assertNotEquals(ret, null);
        // Only 4 are active
        assert (ret.getFacilities().size() > 5L);

        //search by parent facility
        search = new FacilitySearch(null, null, null, null, null, null, createFacilty("FacilityCodeIT0" + new Random().nextLong()), null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 10L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 0L);

        log.error(String.format("ret %s", ret.getFacilities()));

        //search by FacilityType Category Text only
        String category = new String("COURT");
        search = new FacilitySearch(null, null, null, category, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 10L, null);
        assertNotEquals(ret, null);
        assert (ret.getFacilities().size() < 10L);

        //search by Active record only
        search = new FacilitySearch(null, null, null, null, null, true, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 27L, null);
        assertNotEquals(ret, null);
        assert (ret.getFacilities().size() > 20L);

        //search by Active record only
        search = new FacilitySearch(null, null, null, null, null, false, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 17L, null);
        assertNotEquals(ret, null);
        assert (ret.getFacilities().size() < 18);

        //search by Active record only
        //covered above

        //search by facility associations only
        search = new FacilitySearch(null, null, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 27L, null);
        assertNotEquals(ret, null);
        assert (ret.getFacilities().size() < 27);

        //search by facility associations only
        search = new FacilitySearch(null, null, null, null, null, null, createFacilty("FacilityCodeIT0" + new Random().nextLong()), null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 27L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 0);

        /////////////////////////////////////////////////////////////////
        //Multiple condition search
        /////////////////////////////////////////////////////////////////

        //Search by facilityName and Organization AssociationType
        search = new FacilitySearch(null, "FacilityName1", null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 17L, null);
        assertNotEquals(ret, null);
        assert (ret.getFacilities().size() > 0);

        //Search by facilityName , Organization AssociationType and Contact AssociationType
        search = new FacilitySearch(null, "FacilityName1", null, null, null, null, createFacilty("FacilityCodeIT0" + new Random().nextLong()), null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 17L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 0L);

        //Search by facilityName , Organization AssociationType, Contact AssociationType and Category Text
        category = new String("COURT");
        search = new FacilitySearch(null, "FacilityName1", null, category, null, null, createFacilty("FacilityCodeIT0" + new Random().nextLong()), null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 17L, null);
        assertNotEquals(ret, null);
        assert (ret.getFacilities().size() == 0L);

        //Search by facilityName , Organization AssociationType, Contact Associationk, Category Text
        //and ActiveFlag is true
        category = new String("COURT");
        search = new FacilitySearch(null, "FacilityName3", null, category, null, true, createFacilty("FacilityCodeIT0" + new Random().nextLong()), null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 17L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 0L);

        //Search by facilityName , Organization AssociationType, Contact Associationk, Category Text
        //and ActiveFlag is False
        category = new String("COURT");
        search = new FacilitySearch(null, "FacilityName2", null, category, null, false, createFacilty("FacilityCodeIT0" + new Random().nextLong()), null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 17L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 0L);

        //Search by facilityName , Organization AssociationType, Contact Associationk, Category Text
        //and ActiveFlag is true, FacilityType Associations
        category = new String("COURT");
        search = new FacilitySearch(null, "FacilityName1", null, category, null, true, createFacilty("FacilityCodeIT0" + new Random().nextLong()), null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 17L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 0L);

        //Search by facilityName , Organization AssociationType, Contact Associationk, Category Text
        //and ActiveFlag is true, FacilityType Associations
        Set<Long> newLocations = new HashSet<Long>();
        newLocations.add(2L);
        search = new FacilitySearch(null, "FacilityName1", null, null, category, true, createFacilty("FacilityCodeIT0" + new Random().nextLong()), null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 17L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 0L);

        /////////////////////////////////////////////////////////////////
        //Partial condition search
        /////////////////////////////////////////////////////////////////
        search = new FacilitySearch(null, "FacilityName*", null, null, null, true, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assert (ret.getFacilities().size() > 10L);

        //Facility Code is Code varchar2(64 char) not null unique, thus does not support PARTIAL search
        search = new FacilitySearch("Facility", null, null, null, null, true, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 0L);

        //Facility Contact Id Search
        search = new FacilitySearch("Facility*", null, null, null, null, true, null, null, 5L, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 0L);

        /////////////////////////////////////////////////////////////////
        //subset condition search
        /////////////////////////////////////////////////////////////////
        Set<Long> subsetSearch = new HashSet<Long>();
        subsetSearch.add(1L);
        subsetSearch.add(2L);
        search = new FacilitySearch(null, "FacilityName1", null, null, null, true, null, null, null, null, null, null, null);
        ret = service.search(null, subsetSearch, search, 0L, 40L, null);
        assertEquals(ret.getFacilities().size(), 0L);

        /////////////////////////////////////////////////////////////////
        //Association condition search
        /////////////////////////////////////////////////////////////////
        search = new FacilitySearch(null, null, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, subsetSearch, search, 0L, 40L, null);
        assertNotEquals(ret, null);

        newLocations = new HashSet<Long>();
        newLocations.add(null);
        search = new FacilitySearch(null, null, null, null, null, null, null, null, null, null, null, null, null);
        try {
            ret = service.search(null, service.getAll(uc), search, 0L, 10L, null);
            assertNotEquals(ret, null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(javax.ejb.EJBException.class));
        }

        try {
            newLocations = new HashSet<Long>();
            newLocations.add(Long.valueOf("dsad"));
            search = new FacilitySearch(null, null, null, null, null, null, null, null, null, null, null, null, null);
            ret = service.search(null, service.getAll(uc), search, 0L, 10L, null);
            assertNotEquals(ret, null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(NumberFormatException.class));
        }

        /////////////////////////////////////////////////////////////////
        //QC Defect # 361
        /////////////////////////////////////////////////////////////////
        try {
            String code = null;
            search = new FacilitySearch(null, null, null, null, code, null, null, null, null, null, null, null, null);
            ret = service.search(null, null, search, 0L, 40L, null);
            assertNotEquals(ret, null);
            assert (ret.getFacilities().size() > 10L);
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }

        //search by facilityName and facility type        
        search = new FacilitySearch(null, "FacilityName1", null, null, new String("USS"), null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 0L);

    }

    @Test(dependsOnMethods = { "searchCreation", "search" }, enabled = true)
    public void wildcardSearch() {
        searchCreation();

        FacilitiesReturn ret;
        FacilitySearch search;

        // search by facilityCode

        try {
            search = new FacilitySearch("*", null, null, null, null, null, null, null, null, null, null, null, null);
            ret = service.search(null, null, search, 0L, 40L, null);
            assertNotEquals(ret, null);
        } catch (Exception ex) {
            assert (ex.getClass().equals(InvalidInputException.class));
        }

        search = new FacilitySearch("FacilityCode" + new Random().nextLong(), null, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 0L);

        search = new FacilitySearch("FacilityCode1", null, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 1L);

        search = new FacilitySearch("*acilityCode1", null, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 1L);

        search = new FacilitySearch("Facil*tyCode1", null, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 1L);

        search = new FacilitySearch("FacilityCode*", null, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 1L);

        search = new FacilitySearch("*acilit*Code*", null, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assert (ret.getFacilities().size() > 20L);

        search = new FacilitySearch("*acilit*code*", null, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 20L);

        search = new FacilitySearch("*acilit*aode*", null, null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 0L);

        search = new FacilitySearch(null, "FacilityName1", null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 4L);

        search = new FacilitySearch(null, "*acilityName1", null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 4L);

        search = new FacilitySearch(null, "Facil*tyName1", null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 4L);

        search = new FacilitySearch(null, "FacilityName*", null, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 40L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 20L);

        // SetMode = ANY
        search = new FacilitySearch(null, null, SearchSetMode.ANY, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 17L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 17L);

        // SetMode = ANY
        search = new FacilitySearch(null, null, SearchSetMode.ANY, null, null, null, parentId, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 17L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 17L);

        // SetMode = ALL
        search = new FacilitySearch(null, null, SearchSetMode.ALL, null, null, null, parentId, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 17L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 17L);

        // SetMode = ALL
        search = new FacilitySearch(null, null, SearchSetMode.ALL, null, null, null, null, null, null, null, null, null, null);
        ret = service.search(null, null, search, 0L, 17L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 17L);

        // SetMode = ONLY
        search = new FacilitySearch();
        search.setSetMode(SearchSetMode.ONLY);
        ret = service.search(null, null, search, 0L, 17L, null);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilities().size(), 17L);

    }

    @Test
    public void addLocation() {

        Long facilityId = createFacilty("FacilityCodeIT0" + new Random().nextLong());

        LocationTest locTest = new LocationTest();
        Location loc = locTest.createLocationType(true, null, facilityId);

        Location ret = service.addLocation(uc, loc);
        assertNotNull(ret);

    }

    @Test
    public void updateLocation() {

        Location loc = createLocation();
        loc.setLocationCrossStreet("UpdateLocation");
        loc.setLocationName("UpdateLocationNameForFacility");

        Location ret = service.updateLocation(uc, loc);
        assertNotNull(ret);

    }

    @Test
    public void deleteLocation() {

        Location loc = createLocation();

        Long ret = service.deleteLocation(uc, loc.getInstanceId(), loc.getLocationIdentification());
        assertEquals(ret, ReturnCode.Success.returnCode());

    }

    @Test
    public void getLocation() {

        Location loc = createLocation();
        Location ret = service.getLocation(uc, loc.getLocationIdentification());
        assertNotNull(ret);

    }

    @Test
    public void getLocations() {

        Long facilityId = createFacilty("FacilityCodeIT0" + new Random().nextLong());

        LocationTest locTest = new LocationTest();
        Location loc = locTest.createLocationType(true, null, facilityId);
        Location loc2 = locTest.createLocationType(true, null, facilityId);
        Location loc3 = locTest.createLocationType(true, null, facilityId);

        Location ret = service.addLocation(uc, loc);
        assertNotNull(ret);

        Location ret2 = service.addLocation(uc, loc2);
        assertNotNull(ret2);

        Location ret3 = service.addLocation(uc, loc3);
        assertNotNull(ret3);

        List<Location> locsRet = service.getLocations(uc, facilityId);
        assertNotNull(locsRet);

        assertEquals(locsRet.size(), 3L);

    }

    private Location createLocation() {
        Long facilityId = createFacilty("FacilityCodeIT0" + new Random().nextLong());

        LocationTest locTest = new LocationTest();
        Location loc = locTest.createLocationType(true, null, facilityId);

        Location ret = service.addLocation(uc, loc);
        assertNotNull(ret);
        return ret;
    }

    private String generateORI() {
        Random rand = new Random();
        String ori = "ORI" + rand.nextLong();
        return ori.substring(0, 9);
    }

    private Long createOrganization() {
        //orgService.deleteAll(uc);

        Organization org = new Organization();
        Organization ret;

        org.setOrganizationName("Los Angeles Department of Correction");
        org.setOrganizationLocalId("LA456-05-356");
        org.setOrganizationAbbreviation("LADOC");
        org.setOrganizationORIIdentification(generateORI());
        Set<String> categories = new HashSet<String>();
        categories.add("NONCJ");
        categories.add("CJ");
        categories.add("CORP");
        org.setOrganizationCategories(categories);
        ret = orgService.create(uc, org);
        assertNotNull(ret);
        assertNotNull(ret.getOrganizationIdentification());
        assertEquals(ret.getOrganizationName(), org.getOrganizationName());
        assertEquals(ret.getOrganizationAbbreviation(), org.getOrganizationAbbreviation());
        assertEquals(ret.getOrganizationORIIdentification(), org.getOrganizationORIIdentification());
        assertEquals(ret.getOrganizationCategories(), org.getOrganizationCategories());

        Long orgId = ret.getOrganizationIdentification();
        return orgId;
    }

    private Long createFacilty(String code) {

        Set<Long> ll = new HashSet<Long>();

        Facility ret = null;
        String category = new String("COMMU");
        Facility facility = new Facility(null, code, "FacilityName0" + new Random().nextLong(), new Date(), category, null, null, null,
                piTest.createPersonIdentity(), false);
        ret = service.create(uc, facility);
        assertNotEquals(ret, null);
        return ret.getFacilityIdentification();
    }

    @Test
    public void testGetAllAssociatedFacilityToOrganization() {
        List<Long> ret = service.getAllAssociatedFacilityToOrganization(uc);
        assertNotNull(ret);
    }

}
