/**
 *
 */
package syscon.arbutus.product.services.facility.contract.util;

import java.util.Date;
import java.util.Set;
import java.util.regex.Pattern;

import syscon.arbutus.product.services.facility.realization.persistence.MetaSet;

/**
 * @author byu
 */
public class Validator {
    private static Pattern validReferenceCode = Pattern.compile("^[A-Za-z0-9_-]+$");
    private static Pattern validLanguageCode = Pattern.compile("^((en){1}|(fr){1}|(es){1})$");

    /**
     * Returns true if data specified is empty.
     *
     * @param data
     * @return Boolean
     */
    public static Boolean isEmpty(Boolean data) {

        if (data == null) {
			return true;
		}

        return false;
    }

    /**
     * Returns true if data specified is empty.
     *
     * @param data
     * @return Boolean
     */
    public static Boolean isEmpty(Long data) {

        if (data == null) {
			return true;
		}

        return false;
    }

    /**
     * Returns true if data specified is empty.
     *
     * @param data
     * @return Boolean
     */
    public static Boolean isEmpty(String data) {

        if ((data == null) || (data.trim().length() == 0)) {
			return true;
		}

        return false;
    }

    /**
     * Returns true if data specified is empty.
     *
     * @param data
     * @return Boolean
     */
    public static Boolean isEmpty(Date data) {

        if (data == null) {
			return true;
		}

        return false;
    }

    /**
     * Returns true if data specified is empty.
     *
     * @param data
     * @return Boolean
     */
    @SuppressWarnings("rawtypes")
    public static Boolean isEmpty(Set data) {

        if ((data == null) || data.isEmpty()) {
			return true;
		}

        return false;
    }

    /**
     * Returns true if the length of a string is within given limit.
     *
     * @param data
     * @param len
     * @return Boolean
     */
    public static Boolean isWithinLimit(String data, int len) {

        if (data == null) {
			return true;
		}

        if (data.length() > len) {
			return false;
		}

        return true;
    }

    /**
     * Valid the reference set
     *
     * @param data
     * @return Boolean
     */
    public static Boolean isValidReferenceSet(String data) {
        if (isEmpty(data)) {
			return false;
		}

        if (!data.equals(MetaSet.FACILITY_CATEGORY)) {
			return false;
		}

        return true;
    }

    /**
     * Returns true if the string contains letters, numbers and underscore.
     *
     * @param data
     * @return Boolean
     */
    public static Boolean isValidReferenceCode(String data) {

        if (isEmpty(data)) {
			return false;
		}

        return validReferenceCode.matcher(data).find();
    }

    /**
     * Returns true if the string contains ISO 639-1 language codes only.
     *
     * @param data
     * @return Boolean
     */
    public static Boolean isValidLanguageCode(String data) {

        if (isEmpty(data)) {
			return false;
		}

        return validLanguageCode.matcher(data).find();
    }
}
