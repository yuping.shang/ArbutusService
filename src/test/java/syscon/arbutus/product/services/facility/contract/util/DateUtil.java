/**
 *
 */
package syscon.arbutus.product.services.facility.contract.util;

import java.util.Calendar;
import java.util.Date;

/**
 * @author byu
 */
public class DateUtil {

    /**
     * create present date and time
     *
     * @return a Date object
     */
    public static Date createPresentDate() {

        return Calendar.getInstance().getTime();

    }

    /**
     * create present date without time
     *
     * @return a Date object
     */
    public static Date createPresentDateWithoutTime() {

        return getDateWithoutTime(createPresentDate());

    }

    /**
     * create a future date and time
     *
     * @param day number of day
     * @return a Date object
     */
    public static Date createFutureDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, day);
        Date futureDate = cal.getTime();
        return futureDate;
    }

    /**
     * create a past date and time
     *
     * @param day number of day
     * @return a Date object
     */
    public static Date createPastDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, -day);
        Date pastDate = cal.getTime();
        return pastDate;
    }

    /**
     * get a date without time
     *
     * @param date a date to trim the time
     * @return a Date object
     */
    public static Date getDateWithoutTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Date newDate = cal.getTime();
        return newDate;
    }

    public static Boolean isActivatedFlag(Date effectiveDate, Date expiryDate) {

        Date date = createPresentDate();
        if (effectiveDate == null || effectiveDate.after(date)) {
			return Boolean.FALSE;
		}
        if (expiryDate != null && (expiryDate.before(date) || expiryDate.equals(date))) {
			return Boolean.FALSE;
		}

        return Boolean.TRUE;
    }

    public static Boolean isActivatedFlag(Date expiryDate) {

        Date date = createPresentDate();
        if (expiryDate != null && (expiryDate.before(date) || expiryDate.equals(date))) {
			return Boolean.FALSE;
		}

        return Boolean.TRUE;
    }
}
