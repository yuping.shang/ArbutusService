package syscon.arbutus.product.services.signature.contract.ejb;

import javax.naming.NamingException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.signature.contract.dto.GeoLocationType;
import syscon.arbutus.product.services.signature.contract.dto.SignatureSearchType;
import syscon.arbutus.product.services.signature.contract.dto.SignatureType;
import syscon.arbutus.product.services.signature.contract.interfaces.SignatureService;

import static org.testng.AssertJUnit.*;

public class IT extends BaseIT {

    private static Logger log = LoggerFactory.getLogger(IT.class);
    protected UserContext uc = null;
    private SignatureService service;
    private Long singatureId;

    @BeforeClass
    public void beforeClass() throws NamingException, Exception {
        log.info("beforeClass Begin - JNDI lookup");
        service = (SignatureService) JNDILookUp(this.getClass(), SignatureService.class);
        uc = super.initUserContext();

    }

    @Test
    public void testJNDILookup() {
        assertNotNull(service);
    }

    @Test(dependsOnMethods = { "testJNDILookup" })
    public void createSignature() {
        SignatureType signType = getSignatureTypeObject();
        SignatureType newSign = service.createSignature(uc, signType);
        assertNotNull(newSign);
        assertNotNull(newSign.getSignatureId());
        if (newSign != null) {
            singatureId = newSign.getSignatureId();
        }
    }

    private SignatureType getSignatureTypeObject() {
        SignatureType signType = new SignatureType();
        signType.setCreateUserId("devtest");
        signType.setPersonIdentityId(1l);
        byte[] img = new byte[] { -1, 0, 0, 0 };
        signType.setSignature(img);
        return signType;
    }

    private SignatureSearchType getSignatureSearchTypeObject() {
        SignatureSearchType searchType = new SignatureSearchType();
        searchType.setCreatedByUser("devtest");
        return searchType;
    }

    @Test(dependsOnMethods = { "createSignature" })
    public void getSignature() {
        SignatureType type = service.getSignature(uc, singatureId);
        assertNotNull(type);
    }

    @Test(dependsOnMethods = { "getSignature" })
    public void updateSignature() {
        SignatureType type = service.getSignature(uc, singatureId);
        GeoLocationType geoLoc = new GeoLocationType(28.6127, 77.2310);
        type.setLocation(geoLoc);
        service.updateSignature(uc, type);
        type = service.getSignature(uc, singatureId);
        assertNotNull(type);
        assertEquals(type.getLocation(), geoLoc);
    }

    @Test(dependsOnMethods = { "updateSignature" })
    public void searchSignature() {
        SignatureSearchType searchType = getSignatureSearchTypeObject();
        List<SignatureType> returnList = service.searchSignature(uc, searchType, null, null, null);
        assertNotNull(returnList);
    }

    @Test(dependsOnMethods = { "searchSignature" })
    public void deleteSignature() {
        SignatureType type = service.getSignature(uc, singatureId);
        service.deleteSignature(uc, type);
        type = service.getSignature(uc, singatureId);
        assertNull(type);
    }

    @Test(dependsOnMethods = { "deleteSignature" })
    public void deleteAll() {
        createSignature();
        service.deleteAll(uc);
        SignatureType type = service.getSignature(uc, singatureId);
        assertNull(type);
    }
}
