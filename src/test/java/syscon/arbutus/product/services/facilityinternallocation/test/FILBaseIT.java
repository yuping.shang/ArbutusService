package syscon.arbutus.product.services.facilityinternallocation.test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.CapacityType;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.FacilityInternalLocation;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.HierarchyType;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.LocationAttributeType;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.UsageCategory;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.UsageType;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.facilityinternallocation.contract.utils.CollectionUtils;

public class FILBaseIT extends BaseIT {

    protected FacilityInternalLocationService service;

    protected Long facility2Id = 2l;
    protected FacilityInternalLocation faclity2root;
    protected FacilityInternalLocation faclity2rootStorage;
    protected FacilityInternalLocation child211;
    protected FacilityInternalLocation child212;
    protected FacilityInternalLocation child221;
    protected FacilityInternalLocation child222;
    protected FacilityInternalLocation child231;
    protected FacilityInternalLocation child232;
    protected FacilityInternalLocation child223;
    protected FacilityInternalLocation child224;
    protected FacilityInternalLocation child233;
    protected FacilityInternalLocation child234;

    //Storage
    protected FacilityInternalLocation child211s;

    protected Long facility3Id = 3l;
    protected FacilityInternalLocation faclity3root;  //level 1
    protected FacilityInternalLocation child311; //level 2
    protected FacilityInternalLocation child312;
    protected FacilityInternalLocation child321; //level 3
    protected FacilityInternalLocation child322;
    protected FacilityInternalLocation child331; //level 4
    protected FacilityInternalLocation child332;
    protected FacilityInternalLocation child333;
    protected FacilityInternalLocation child3321; //level 5
    protected FacilityInternalLocation child33211; //level 6

    //business logic
    protected void destropLocationTree() {
        if (service != null) {
            delete(child234);
            delete(child233);
            delete(child223);
            delete(child224);
            delete(child231);
            delete(child232);
            delete(child221);
            delete(child222);
            delete(child211s);
            delete(child211);
            delete(child212);
            delete(faclity2root);
            delete(child33211);
            delete(child3321);
            delete(child333);
            delete(child331);
            delete(child332);
            delete(child321);
            delete(child322);
            delete(child311);
            delete(child312);
            delete(faclity3root);
        }
    }

    private void delete(FacilityInternalLocation t) {
        if (t != null) {

            //Delete usage first if there is any usage associated
            FacilityInternalLocation returnType = service.get(uc, t.getId());
            assertNotNull(returnType);
            FacilityInternalLocation filType = returnType;
            if (filType != null && filType.getUsages() != null) {
                for (UsageType usage : filType.getUsages()) {
                    if (usage != null && usage.getUsageId() != null) {
                        Long ret = service.deleteUsage(uc, usage.getUsageId());
                        assertNotNull(ret);
                        assertEquals(ret, ReturnCode.Success.returnCode());
                    }
                }
            }

            Long ret = service.delete(uc, t.getId());
            assertNotNull(ret);
            assertEquals(ret, ReturnCode.Success.returnCode());
        }
    }

    protected void createFacilityInternalLocationTree() {
        //service.deleteAll(uc);
        createFacility(new Facility(1L, "FacilityCodeFIL1", "FacilityName", new Date(), "COMMU", null, null, new HashSet<Long>(), null, false));
        createFacility(new Facility(2L, "FacilityCodeFIL2", "FacilityName", new Date(), "COMMU", null, null, new HashSet<Long>(), null, false));
        createFacility(new Facility(3L, "FacilityCodeFIL3", "FacilityName", new Date(), "COMMU", null, null, new HashSet<Long>(), null, false));
        testCreateAllHierchyLevelslFac2();
        testCreateAllHierchyLevelslFac3();
        faclity2root = createFacilityInternalLocation(facility2Id, null, "FACILITY", "Root2", "physical location root facility 2", null, UsageCategory.HOU);

        //createFacilityInternalLocation(facility2Id, null, "FACILITY", "Root2S", "physical location root facility 2 Storage", null, UsageCategory.STO);

        child211 = createFacilityInternalLocation(facility2Id, faclity2root.getId(), "WING", "Wing 2-1", "first Level 1 child in facility 2", null,
                UsageCategory.HOU); //Swing
        child212 = createFacilityInternalLocation(facility2Id, faclity2root.getId(), "WING", "Wing 2-2", "second Level 1 child in facility 2", null, UsageCategory.HOU);

        child211s = createFacilityInternalLocation(facility2Id, faclity2root.getId(), "FLOOR", "Floor 2-1", "first Level 1 child in facility 2", null,
                UsageCategory.STO); //Swing

        Set<String[]> prop1 = new java.util.HashSet<String[]>();
        prop1.add(new String[] { FacilityInternalLocationMetaSet.INTERNAL_LOCATION_NONASSOCIATION, "STG" });
        prop1.add(new String[] { FacilityInternalLocationMetaSet.INTERNAL_LOCATION_NONASSOCIATION, "TOTAL" });
        child221 = createFacilityInternalLocation(facility2Id, child211.getId(), "UNIT", "Unit 2-1", "first Level 2 child in facility 2", prop1,
                UsageCategory.HOU);  //Floor
        child222 = createFacilityInternalLocation(facility2Id, child211.getId(), "UNIT", "Unit 2-2", "second Level 2 child in facility 2", null, UsageCategory.HOU);

        Set<String[]> prop2 = new java.util.HashSet<String[]>();
        prop2.add(new String[] { FacilityInternalLocationMetaSet.AGE_RANGE, "JUVENILE" });
        prop2.add(new String[] { FacilityInternalLocationMetaSet.LOCATION_PROPERTY, "WHEELCHAIR" });
        prop2.add(new String[] { FacilityInternalLocationMetaSet.LOCATION_PROPERTY, "SHOWER" });
        prop2.add(new String[] { FacilityInternalLocationMetaSet.SECURITY_LEVEL, "MAX" });
        prop2.add(new String[] { FacilityInternalLocationMetaSet.SENTENCE_STATUS, "SENTENCED" });
        prop2.add(new String[] { FacilityInternalLocationMetaSet.OFFENDER_CATEGORY, "SUIC" });
        prop2.add(new String[] { FacilityInternalLocationMetaSet.OFFENDER_GENDER, "M" });
        child231 = createFacilityInternalLocation(facility2Id, child221.getId(), "ROOM", "ROOM 2-1", "first Level 3 child in facility 2", prop2,
                UsageCategory.HOU); //Cell
        String subType = "PC";
        String subType3 = "GP";
        String subType2 = "GYM";
        String subType4 = "VISIT";
        createUsage(UsageCategory.HOU, subType, child231.getId(), "desc Room231", 8l, 2l);
        createUsage(UsageCategory.HOU, subType3, child231.getId(), "desc Room231", 2l, 1l);
        //createUsage(UsageCategory.ACT, subType2, child231.getId(), "desc Room231", 4l, 1l); //Not allowed as per new FIL design two Usage categories can't be  
        //createUsage(UsageCategory.ACT, subType4, child231.getId(), "desc Room231", 4l, 1l); //Not allowed as per new FIL design two Usage categories can't be  

        Set<String[]> prop3 = new java.util.HashSet<String[]>();
        prop3.add(new String[] { FacilityInternalLocationMetaSet.AGE_RANGE, "ADULT" });
        prop3.add(new String[] { FacilityInternalLocationMetaSet.SECURITY_LEVEL, "MIN" });
        prop3.add(new String[] { FacilityInternalLocationMetaSet.SENTENCE_STATUS, "UNSENTENCED" });
        prop3.add(new String[] { FacilityInternalLocationMetaSet.OFFENDER_GENDER, "F" });
        child232 = createFacilityInternalLocation(facility2Id, child221.getId(), "ROOM", "ROOM 2-2", "second Level 3 child in facility 2", prop3,
                UsageCategory.HOU);  //Cell
        createUsage(UsageCategory.HOU, subType, child232.getId(), "desc Room232", 8l, 2l);

        //Create another floor for swing 2
        child223 = createFacilityInternalLocation(facility2Id, child212.getId(), "UNIT", "Unit 2-1", "first Level 2 child in facility 2", null,
                UsageCategory.HOU);  //Floor
        child224 = createFacilityInternalLocation(facility2Id, child212.getId(), "UNIT", "Unit 2-2", "second Level 2 child in facility 2", null, UsageCategory.HOU);

        child233 = createFacilityInternalLocation(facility2Id, child223.getId(), "ROOM", "ROOM 2-1", "first Level 3 child in facility 2", prop2,
                UsageCategory.HOU); //Cell
        createUsage(UsageCategory.HOU, subType, child233.getId(), "desc Room233", 8l, 2l);

        child234 = createFacilityInternalLocation(facility2Id, child223.getId(), "ROOM", "ROOM 2-2", "second Level 3 child in facility 2", prop3,
                UsageCategory.HOU);  //Cell
        createUsage(UsageCategory.HOU, subType, child234.getId(), "desc Room234", 8l, 2l);

        //Level 1
        faclity3root = createFacilityInternalLocation(facility3Id, null, "FACILITY", "Root3", "physical location root facility 3", null, UsageCategory.HOU);
        //Level 2
        child311 = createFacilityInternalLocation(facility3Id, faclity3root.getId(), "WING", "Wing 3-1", "first Level 1 child in facility 3", null, UsageCategory.HOU);
        child312 = createFacilityInternalLocation(facility3Id, faclity3root.getId(), "WING", "Wing 3-2", "second Level 1 child in facility 3", null, UsageCategory.HOU);
        prop3.add(new String[] { FacilityInternalLocationMetaSet.INTERNAL_LOCATION_NONASSOCIATION, "STG" });
        prop3.add(new String[] { FacilityInternalLocationMetaSet.LOCATION_PROPERTY, "WHEELCHAIR" });

        //Level 3
        child321 = createFacilityInternalLocation(facility3Id, child311.getId(), "UNIT", "Unit 3-1", "first Level 2 child in facility 3", prop3, UsageCategory.HOU);
        child322 = createFacilityInternalLocation(facility3Id, child311.getId(), "UNIT", "Unit 3-2", "second Level 2 child in facility 3", null, UsageCategory.HOU);

        //Level 4
        child331 = createFacilityInternalLocation(facility3Id, child321.getId(), "ROOM", "ROOM 3-1", "first Level 3 child in facility 3", null, UsageCategory.HOU);
        //		Set<String> prop332= new java.util.HashSet<String>();
        //		prop332.add(new String[]{FacilityInternalLocationMetaSet.AGERANGE, "JUVENILE"});
        child332 = createFacilityInternalLocation(facility3Id, child321.getId(), "ROOM", "ROOM 3-2", "second Level 3 child in facility 3", null, UsageCategory.HOU);
        child333 = createFacilityInternalLocation(facility3Id, child321.getId(), "ROOM", "ROOM 3-3", "third Level 3 child in facility 3", null, UsageCategory.HOU);

        //Level 5 : Not allowed as per new FIL design
        //child3321 = createFacilityInternalLocation(facility3Id, child332.getId(), "TABLE", "TABLE 3-2-2" , "Level 4 child in facility 3", null, UsageCategory.HOU);

        //Level 6 : Not allowed as per new FIL design
        //		child33211 = createFacilityInternalLocation(facility3Id, child3321.getId(), "CHAIR", "CHAIR 3-2-2-1" , "Level 5 child in facility 3", null, UsageCategory.HOU);
        //		createUsage(UsageCategory.HOU, subType, child33211.getId(), "desc CHAIR3221", 1l, 0l);

    }

    private void addProperties(FacilityInternalLocation fil, String[] ct) {
        if (fil.getLocationAttribute() == null) {
            LocationAttributeType attr = new LocationAttributeType();
            fil.setLocationAttribute(attr);
        }
        String propName = ct[0];
        switch (propName) {
            case FacilityInternalLocationMetaSet.AGE_RANGE:
                fil.getLocationAttribute().getAgeRanges().add(ct[1]);
                break;
            case FacilityInternalLocationMetaSet.INTERNAL_LOCATION_NONASSOCIATION:
                fil.getNonAssociations().add(ct[1]);
                break;
            case FacilityInternalLocationMetaSet.OFFENDER_CATEGORY:
                fil.getLocationAttribute().getOffenderCategories().add(ct[1]);
                break;
            case FacilityInternalLocationMetaSet.OFFENDER_GENDER:
                fil.getLocationAttribute().getGenders().add(ct[1]);
                break;
            case FacilityInternalLocationMetaSet.LOCATION_PROPERTY:
                fil.getLocationAttribute().getLocationProperties().add(ct[1]);
                break;
            case FacilityInternalLocationMetaSet.SECURITY_LEVEL:
                fil.getLocationAttribute().getSecurityLevels().add(ct[1]);
                break;
            case FacilityInternalLocationMetaSet.SENTENCE_STATUS:
                fil.getLocationAttribute().getSentenceStatuses().add(ct[1]);
                break;
        }

    }

    protected FacilityInternalLocation createInternalLocation(Long facilityId, Long parentId, String level, String code, String description, Set<String[]> props,
            UsageCategory usageCategory) {
        return createFacilityInternalLocation(facilityId, parentId, level, code, description, props, usageCategory);
    }

    private FacilityInternalLocation createFacilityInternalLocation(Long facilityId, Long parentId, String level, String code, String description,
            Set<String[]> props, UsageCategory usageCategory) {
        FacilityInternalLocation pt = new FacilityInternalLocation();
        pt.setFacilityId(facilityId);
        pt.setHierarchyLevel(level);
        pt.setLocationCode(code);
        pt.setDescription(description);
        pt.setParentId(parentId);
        if (CollectionUtils.isNotEmpty(props)) {
            for (String[] ct : props) {
                addProperties(pt, ct);
            }
        }

        FacilityInternalLocation ret = service.create(uc, pt, usageCategory);

        assertNotNull(ret);
        FacilityInternalLocation created = ret;
        assertNotNull(created);
        return created;
    }

    protected UsageType createUsage(UsageCategory category, String subtype, Long locationId, String usageDesc, Long optimalCapacity, Long overflowcapacity) {
        UsageType h = new UsageType();
        h.setUsageCategory(category);
        h.setUsageSubtype(subtype);
        h.setUsageCode(subtype + "_" + locationId);
        h.setInternalLocationId(locationId);
        h.setDescription(usageDesc);
        CapacityType cp = new CapacityType();
        cp.setUsageCategory(h.getUsageCategory());
        cp.setUsageType(h.getUsageSubtype());
        cp.setOptimalCapacity(optimalCapacity);
        cp.setOverflowCapacity(overflowcapacity);
        cp.setInternalLocationId(locationId);
        h.setCapacity(cp);
        h.setUsageAllowed(true);

        UsageType ret = service.addUsage(uc, h);
        assertNotNull(ret);
        UsageType created = ret;
        assertNotNull(created);
        return created;
    }

    protected Date createPresentDate() {

        return Calendar.getInstance().getTime();

    }

    protected Date createFutureDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, day);
        Date futureDate = cal.getTime();
        return futureDate;
    }

    protected Date createPastDate(int day) {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, -day);
        Date pastDate = cal.getTime();
        return pastDate;
    }

    private void testCreateAllHierchyLevelslFac2() {
        List<HierarchyType> list = new ArrayList<HierarchyType>();
        list.add(new HierarchyType("FACILITY", "WING", UsageCategory.HOU, facility2Id));
        list.add(new HierarchyType("WING", "UNIT", UsageCategory.HOU, facility2Id));
        list.add(new HierarchyType("UNIT", "ROOM", UsageCategory.HOU, facility2Id));
        list.add(new HierarchyType("FACILITY", "FLOOR", UsageCategory.STO, facility2Id));
        list.add(new HierarchyType("FLOOR", "ROOM", UsageCategory.STO, facility2Id));
        list.add(new HierarchyType("ROOM", "VAULT", UsageCategory.STO, facility2Id));
        list.add(new HierarchyType("VAULT", "SHELF", UsageCategory.STO, facility2Id));
        Map<String, HierarchyType> ret = service.createUpdateLocationHierarchies(uc, list);
    }

    private void testCreateAllHierchyLevelslFac3() {
        List<HierarchyType> list = new ArrayList<HierarchyType>();
        list.add(new HierarchyType("FACILITY", "WING", UsageCategory.HOU, facility3Id));
        list.add(new HierarchyType("WING", "UNIT", UsageCategory.HOU, facility3Id));
        list.add(new HierarchyType("UNIT", "ROOM", UsageCategory.HOU, facility3Id));
        Map<String, HierarchyType> ret = service.createUpdateLocationHierarchies(uc, list);
    }

    private void createFacility(Facility facility) {
        FacilityService facService = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        Facility response = null;
        try {
            response = facService.get(uc, facility.getFacilityIdentification());
        } catch (DataNotExistException e) {
            //
        }
        if (response == null) {
            facService.create(uc, facility);
        }
    }

    public class FacilityInternalLocationMetaSet {
        /**
         * Internal location security level
         */
        public static final String SECURITY_LEVEL = "SECURITYLEVEL";

        /**
         * Internal location offender category
         */
        public static final String OFFENDER_CATEGORY = "OFFENDERCATEGORY";
        /**
         * Internal location offender gender
         */
        public static final String OFFENDER_GENDER = "OFFENDERGENDER";
        /**
         * Internal location age range
         */
        public static final String AGE_RANGE = "AGERANGE";
        /**
         * Internal location hierarchy level
         */
        public static final String HIERARCHY_LEVEL = "HIERARCHYLEVEL";
        /**
         * Internal location non association
         */
        public static final String INTERNAL_LOCATION_NONASSOCIATION = "INTERNALLOCATIONNONASSOCIATION";
        /**
         * Internal location physical properties
         */
        public static final String LOCATION_PROPERTY = "LOCATIONPROPERTY";
        /**
         * Usage category: HOU, ACT, STO. Not configurable
         */
        public static final String USAGE_CATEGORY = "USAGECATEGORY";
        /**
         * Housing usage sub type
         */
        public static final String HOUSING_USAGETYPE = "HOUSINGUSAGETYPE";
        /**
         * Activity usage sub type
         */
        public static final String ACTIVITY_USAGETYPE = "ACTIVITYUSAGETYPE";
        /**
         * Storage usage sub type
         */
        public static final String STORAGE_USAGETYPE = "STORAGEUSAGETYPE";
        /**
         * Deactivate reason
         */
        public static final String DEACTIVATEREASON = "DEACTIVATEREASON";
        /**
         * Storage usage allowed property type
         */
        public static final String PROPERTYITEMTYPE = "PROPERTYITEMTYPE";
        /**
         * Internal location category
         */
        public static final String Int_Loc_Category = "IntLocCategory";

        public static final String SENTENCE_STATUS = "SentenceStatus";
    }
}
