package syscon.arbutus.product.services.facilityinternallocation.test;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.*;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.facilityinternallocation.contract.utils.CollectionUtils;
import syscon.arbutus.product.services.facilityinternallocation.contract.utils.DateTimeUtil;

import static org.testng.Assert.*;

public class QueryFILIT extends FILBaseIT {

    private static Logger log = LoggerFactory.getLogger(QueryFILIT.class);

    @BeforeClass
    public void beforeClass() throws Exception {
        log.info("beforeClass Begin - JNDI lookup");
        if (service == null) {
            service = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        }
        uc = super.initUserContext();
        service.deleteAll(uc);
        createFacilityInternalLocationTree();

    }

    @AfterClass
    public void afterClassCleanup() {
        destropLocationTree();
    }

    //@Test
    public void testReset() {
        Long ret = service.deleteAll(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());
    }

    @Test
    public void testGetHierarchy() {
        HierarchyType ret = service.getRootLocationHierarchy(uc, 2L, UsageCategory.HOU);
        assertNotNull(ret);

    }

    @Test
    public void testGet() {
        FacilityInternalLocation ret = service.get(uc, child211.getId());
        assertNotNull(ret);
        assertTrue(CollectionUtils.isEmpty(ret.getChildren()));
    }

    @Test
    public void testGetWithChildren() {
        FacilityInternalLocation ret = service.get(uc, child211.getId(), true);
        assertNotNull(ret);
        assertNotNull(ret.getChildren());
        assertEquals(ret.getChildren().size(), 2l);
    }

    @Test
    public void testQueryFacilityLevel_Physical() {
        List<InternalLocationIdentifierType> ret = service.getDirectChildLocationIds(uc, LocationCategory.PHYSICAL, null);
        assertNotNull(ret);
        assertEquals(ret.size(), 2l);
    }

    @Test
    public void testQueryFacilityLevel_HOUSING() {
        List<InternalLocationIdentifierType> ret = service.getDirectChildLocationIds(uc, LocationCategory.HOUSING, null);
        assertNotNull(ret);
        assertEquals(ret.size(), 1l);
    }

    @Test
    public void testQueryLevel1() {
        List<InternalLocationIdentifierType> ret = service.getDirectChildLocationIds(uc, LocationCategory.HOUSING, faclity2root.getId());
        assertNotNull(ret);
        assertEquals(ret.size(), 2l);
    }

    @Test
    public void testQueryByFacility() {
        List<InternalLocationIdentifierType> ret = service.getFirstLevelIdsByFacilityId(uc, LocationCategory.HOUSING, facility2Id);
        assertNotNull(ret);
        assertEquals(ret.size(), 1l);
    }

    @Test
    public void testGetCodesFromIds() {
        Set<Long> ids = new HashSet<Long>();
        ids.add(faclity2root.getId());
        ids.add(child211.getId());
        ids.add(child221.getId());
        List<InternalLocationIdentifierType> ret = service.getLocationCodes(uc, ids);
        assertNotNull(ret);
        assertEquals(ret.size(), 3l);
    }

    @Test
    public void testSearch() {
        InternalLocationSearchType searchCriteria = new InternalLocationSearchType();
        searchCriteria.setFacilityId(facility2Id);
        FacilityInternalLocationsReturnType ret = service.search(uc, searchCriteria, null);
        assertNotNull(ret);
        assertEquals(ret.getReturnCode(), ReturnCode.Success.returnCode());
        assertEquals(ret.getCount().longValue(), 12l);
        assertEquals(ret.getInternalLocations().size(), 12);

        //test Defect#2083
        searchCriteria = new InternalLocationSearchType();
        searchCriteria.setDeactivateFromDate(new Date(2013, 01, 01));
        ret = service.search(uc, searchCriteria, null);
        assertNotNull(ret);
        assertEquals(ret.getReturnCode(), ReturnCode.Success.returnCode());

        searchCriteria = new InternalLocationSearchType();
        searchCriteria.setDeactivateToDate(new Date(2013, 01, 01));
        ret = service.search(uc, searchCriteria, null);
        assertNotNull(ret);
        assertEquals(ret.getReturnCode(), ReturnCode.Success.returnCode());
    }

    @Test
    public void testSearchDescription() {
        InternalLocationSearchType searchCriteria = new InternalLocationSearchType();
        searchCriteria.setFacilityId(facility2Id);
        searchCriteria.setDescription("first*");
        FacilityInternalLocationsReturnType ret = service.search(uc, searchCriteria, null);
        assertNotNull(ret);
        assertEquals(ret.getReturnCode(), ReturnCode.Success.returnCode());
        assertEquals(ret.getCount().longValue(), 6l);

        //test Defect#2443
        searchCriteria = new InternalLocationSearchType();
        searchCriteria.setDescription("*child*");
        ret = service.search(uc, searchCriteria, null);
        assertNotNull(ret);
        assertEquals(ret.getReturnCode(), ReturnCode.Success.returnCode());
        assertEquals(ret.getCount(), new Long(18));
    }

    @Test
    public void testAttributes() {
        InternalLocationSearchType searchCriteria = new InternalLocationSearchType();
        LocationAttributeSearchType attrSearch = new LocationAttributeSearchType();
        if (attrSearch.getLocationProperties() == null) {
            attrSearch.setLocationProperties(new HashSet<String>());
        }
        searchCriteria.setLocationAttribute(attrSearch);
        searchCriteria.setUsageCategory(UsageCategory.HOU);
        attrSearch.getLocationNonAssociations().add(new String("STG"));
        FacilityInternalLocationsReturnType ret = service.search(uc, searchCriteria, null);
        assertNotNull(ret);
        assertEquals(ret.getReturnCode(), ReturnCode.Success.returnCode());
        assertTrue(ret.getCount().longValue() >= 2l);
        //assertEquals(ret.getCount().longValue(), 2l);
    }

    @Test(enabled = true)
    public void testAttributesAndOP() {
        InternalLocationSearchType searchCriteria = new InternalLocationSearchType();
        LocationAttributeSearchType attrSearch = new LocationAttributeSearchType();
        if (attrSearch.getLocationProperties() == null) {
            attrSearch.setLocationProperties(new HashSet<String>());
        }
        searchCriteria.setLocationAttribute(attrSearch);

        attrSearch.getLocationNonAssociations().add(new String("STG"));
        attrSearch.getLocationNonAssociations().add(new String("TOTAL"));
        searchCriteria.setUsageCategory(UsageCategory.HOU);
        FacilityInternalLocationsReturnType ret = service.search(uc, searchCriteria, null);
        assertNotNull(ret);
        assertEquals(ret.getReturnCode(), ReturnCode.Success.returnCode());
        //assertEquals(ret.getCount().longValue(), 1l);
        assertTrue(ret.getCount().longValue() >= 1l);
    }

    @Test(enabled = true)
    public void testAttributesAndOPHQL() {
        InternalLocationSearchType searchCriteria = new InternalLocationSearchType();
        searchCriteria.setFacilityId(facility2Id);
        LocationAttributeSearchType attrSearch = new LocationAttributeSearchType();
        if (attrSearch.getLocationProperties() == null) {
            attrSearch.setLocationProperties(new HashSet<String>());
        }
        attrSearch.getLocationProperties().add(new String("SHOWER"));
        attrSearch.getLocationProperties().add(new String("WHEELCHAIR"));
        searchCriteria.setUsageCategory(UsageCategory.HOU);
        searchCriteria.setLocationAttribute(attrSearch);
        FacilityInternalLocationsReturnType ret = service.search(uc, searchCriteria, null);
        assertNotNull(ret);
        assertEquals(ret.getReturnCode(), ReturnCode.Success.returnCode());
        assertEquals(ret.getCount().longValue(), 4l);
    }

    @Test
    public void testQueryUsage() {
        Long internalLocationId = child333.getId();
        //Long internalLocationId = 857L;
        UsageType h = new UsageType();
        h.setUsageCategory(UsageCategory.HOU);
        h.setUsageSubtype(new String("PC"));
        h.setInternalLocationId(internalLocationId);
        h.setDescription("desc child333");
        h.setUsageCode("PC-CHILD333");
        CapacityType cp = new CapacityType();
        cp.setOptimalCapacity(5l);
        cp.setOverflowCapacity(3l);
        cp.setInternalLocationId(internalLocationId);
        h.setCapacity(cp);
        h.setUsageAllowed(true);

        CommentType ct = new CommentType();
        ct.setComment("a good room");
        ct.setCommentDate(DateTimeUtil.getCurrentDate());
        h.getComments().add(ct);

        UsageType ret = service.addUsage(uc, h);
        assertNotNull(ret);

        String reason = new String("REF");
        FacilityInternalLocation dret = service.deactivate(uc, internalLocationId, reason, "EN", "test deactive child");
        assertNotNull(dret);

        InternalLocationSearchType s = new InternalLocationSearchType();
        s.setUsageCategory(UsageCategory.HOU);
        s.setDescription("*333*");
        s.setComment("*room");

        s.setFacilityId(facility3Id);
        s.setActiveFlag(Boolean.FALSE);

        LocationAttributeSearchType attrSearch = new LocationAttributeSearchType();
        if (attrSearch.getLocationProperties() == null) {
            attrSearch.setLocationProperties(new HashSet<String>());
        }
        s.setLocationAttribute(attrSearch);
        //attrSearch.getLocationProperties().add(new String("WHEELCHAIR"));

        Set<Long> subSet = new HashSet<Long>();
        subSet.add(internalLocationId);
        FacilityInternalLocationsReturnType ret2 = service.search(uc, s, subSet);
        assertNotNull(ret2);
        assertEquals(ret2.getReturnCode(), ReturnCode.Success.returnCode());
        assertEquals(ret2.getCount().longValue(), 1l);
    }

    @Test
    public void testFacilityHousingCapacities() {
        FacilityStatisticsType ret = service.getFacilityHousingCapacity(uc, facility2Id);
        assertNotNull(ret);
        FacilityStatisticsType statis = ret;
        assertNotNull(statis);
        Set<CapacityType> capacities = statis.getCapacities();
        assertEquals(capacities.size(), 2l);
    }

    @Test
    public void testFacilityStatics() {
        List<HousingCapacityAttributeType> ret = service.getFacilityHousingCapacityAttribute(uc, facility2Id);
        assertNotNull(ret);
        assertEquals(ret.size(), 5L);
    }

    @Test
    public void testLocationHousingAttributes() {
        //child211.getId()
        LocationAttributeSearchType attributeSearch = new LocationAttributeSearchType();
        attributeSearch.getLocationProperties().add(new String("SHOWER"));
        attributeSearch.getLocationProperties().add(new String("WHEELCHAIR"));
        List<HousingCapacityAttributeType> ret = service.getHousingCapacityAttribute(uc, child211.getId(), attributeSearch, Boolean.TRUE, CapacityAttribute.CAPACITY);
        assertNotNull(ret);
        assertEquals(ret.size(), 3l);
    }

    @Test
    public void testLocationHousingAttibutes6Level() {

        Long internalLocationId = faclity2root.getId();
        //Long internalLocationId = 728L;
        List<HousingCapacityAttributeType> ret = service.getHousingCapacityAttribute(uc, internalLocationId, null, Boolean.TRUE, CapacityAttribute.CAPACITY);
        assertNotNull(ret);
        assertEquals(ret.size(), 5l);
    }

    @Test
    public void testLocationHousingAttributesSet() {
        Set<Long> parentIds = new HashSet<Long>();
        parentIds.add(child211.getId());
        parentIds.add(child212.getId());
        List<GroupHousingCapacityAttributeType> ret = service.getHousingCapacityAttribute(uc, parentIds, null, Boolean.TRUE, CapacityAttribute.CAPACITY);
        assertNotNull(ret);
        assertEquals(ret.size(), 2l);
    }

    @Test
    public void testLocationConflict() {
        Set<Long> checkIds = new HashSet<Long>();
        checkIds.add(child231.getId());
        //	checkIds.add(child233.getId());
        Set<NonAssociationConflictType> nonAssocSet = new HashSet<NonAssociationConflictType>();
        NonAssociationConflictType nonAssoc = new NonAssociationConflictType();
        nonAssoc.setNaInternalLocationId(child232.getId());
        nonAssoc.setNaOffenderSupervisionId(1l);
        nonAssoc.setNonAssociation(new String("STG"));
        nonAssocSet.add(nonAssoc);

        NonAssociationConflictType nonAssoc2 = new NonAssociationConflictType();
        nonAssoc2.setNaInternalLocationId(child232.getId());
        nonAssoc2.setNaOffenderSupervisionId(1l);
        nonAssoc2.setNonAssociation(new String("CELL"));
        nonAssocSet.add(nonAssoc2);

        NonAssociationConflictType nonAssoc3 = new NonAssociationConflictType();
        nonAssoc3.setNaInternalLocationId(child232.getId());
        nonAssoc3.setNaOffenderSupervisionId(1l);
        nonAssoc3.setNonAssociation(new String("STG"));
        nonAssocSet.add(nonAssoc3);

        NonAssociationConflictType nonAssoc4 = new NonAssociationConflictType();
        nonAssoc4.setNaInternalLocationId(child231.getId());
        nonAssoc4.setNaOffenderSupervisionId(1l);
        nonAssoc4.setNonAssociation(new String("TOTAL"));
        nonAssocSet.add(nonAssoc4);

        List<HousingNonAssociationConflictType> ret = service.getHousingNonAssociationConflicts(uc, checkIds, nonAssocSet);
        assertNotNull(ret);
        assertEquals(ret.size(), 1l);
    }

    @Test
    public void testIsLeaf() {
        boolean b = service.isLeafInternalLocation(uc, child211.getId());
        assertFalse(b);

        b = service.isLeafInternalLocation(uc, child231.getId());
        assertTrue(b);
    }

    //@Test
    public void testGetLeafLocationIds() {
        /*
        //Long internalLocationId = child233.getId();
		Long internalLocationId = 1230L;
		String reason = new String( "REF");		 
		FacilityInternalLocationType ret1 = service.deactivate(uc, internalLocationId, reason, "EN", "test deactive child");
		assertNotNull(ret1);
		assertEquals(ret1.getReturnCode(), ReturnCode.Success.returnCode());
		assertFalse(ret1.isActive());
		*/
		/*
		Long usageId = 476L;
		String reason = new String( "REF");	
		String lang = "EN";
		String comment = "Test Deactive Usage";
		
		UsageType ret = service.deactivateUsage(uc, usageId, reason, lang, comment);
		assertNotNull(ret);
		assertEquals(ret.getReturnCode(), ReturnCode.Success.returnCode());
		*/

        //Long parentId = child211.getId();
        Long parentId = 1225L;
        List<InternalLocationIdentifierType> ret = service.getLeafLocationIds(uc, parentId, LocationCategory.ACTIVITY, false);
        assertNotNull(ret);
        assertEquals(ret.size(), 1L);
		
		/*
		ret1 = service.reactivate(uc, internalLocationId, "test reactive child");
		assertNotNull(ret1);
		assertEquals(ret1.getReturnCode(), ReturnCode.Success.returnCode());
		assertTrue(ret1.isActive());
		*/
    }
}
