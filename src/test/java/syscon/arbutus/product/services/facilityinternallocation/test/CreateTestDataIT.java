package syscon.arbutus.product.services.facilityinternallocation.test;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;

import static org.testng.Assert.assertTrue;

public class CreateTestDataIT extends FILBaseIT {

    @BeforeClass
    public void beforeClass() throws Exception {
        if (service == null) {
            service = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        }
        uc = super.initUserContext();
        service.deleteAll(uc);
        createFacilityInternalLocationTree();
    }

    @Test
    public void testCount() {
        Long count = service.getCount(uc);
        assertTrue(count > 0l);
    }

    @AfterClass
    public void afterClassCleanup() {
        destropLocationTree();
    }

}
