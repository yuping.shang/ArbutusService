package syscon.arbutus.product.services.facilityinternallocation.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.*;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.FacilityTest;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.*;
import syscon.arbutus.product.services.facilityinternallocation.contract.ejb.FacilityInternalLocationServiceBean;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;

import static org.testng.Assert.assertNotNull;

public class PerformanceIT extends FILBaseIT {

    private static final String HOUSING_PC = new String("PC");
    private static final String HOUSING_GP = new String("GP");
    private static final String ACTIVITY_GYM = new String("GYM");
    private static final String ACTIVITY_VISIT = new String("VISIT");
    private static final String GENDER_F = new String("F");
    private static final String GENDER_M = new String("M");
    private static final String SENTENCESTATUS_SENTC = new String("SENTENCED");
    private static final String SENTENCESTATUS_UNSTC = new String("UNSENTENCED");
    private static final String SECURITYLEVEL_MAX = new String("MAX");
    private static final String SECURITYLEVEL_MIN = new String("MIN");
    private static final String OFFENDERCATEGORY_SUIC = new String("SUIC");
    private static final String AGERANGE_ADULT = new String("ADULT");
    private static final String AGERANGE_JUVENILE = new String("JUVENILE");
    private static final String LOCATIONPROPERTY_WHEELCHAIR = new String("WHEELCHAIR");
    private static final String LOCATIONPROPERTY_SHOWER = new String("SHOWER");
    private static final String LOCATIONNONASSOCIATION_TOTAL = new String("TOTAL");
    private static Logger log = LoggerFactory.getLogger(PerformanceIT.class);
    private Session session = null;
    private Transaction tx = null;

    //Change the value according data in db.
    private Long rootId = 1L;
    private Long facilityId = 1L;

    private FacilityTest facTest;

    @BeforeClass
    public void beforeClass() {

        log.info("beforeClass Begin - JNDI lookup");
        if (service == null) {
            service = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        }
        uc = super.initUserContext();

        facTest = new FacilityTest();
    }

    //for local debug use only
    @SuppressWarnings("unused")
    private void localLogin() {

        BasicConfigurator.configure();
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("arbutus-facilityinternallocation-test-unit");
        EntityManager em = emf.createEntityManager();
        session = (Session) em.getDelegate();

        service = new FacilityInternalLocationServiceBean(session);
        //init user context and login
        //uc = super.getUserContext();

    }

    @BeforeMethod
    public void beforeMethod() {
        if (session != null) {
            tx = session.beginTransaction();
            tx.begin();
        }
    }

    @AfterMethod
    public void afterMethod() {
        if (tx != null) {
            tx.commit();
        }
    }

    @Test
    public void testDataCreation() {

        //Enable this Test only when you need to generate the performance testing data
        // 2 - 146 (2045) 4/10/50
        // 3 - 2191 (4045) 4/10/100
        // 4 - 6236 (2211) 10/20/10

        //service.reset(uc);
        //Already create for facility 1, 2

        //Will create internal locations = level1 * level2 * level3 (2045)
        long facilityId = facTest.createFacility();
        //long facilityId1 = facTest.createFacility();
        long level1 = 10l;    //wings
        long level2 = 20l;    //unit
        long level3 = 10l;    //room

        resetHierarchyLevel(facilityId);

        createFacilityData(facilityId, level1, level2, level3);
        /*
        for (Long i=1l; i<= 50; i++) {
			createFacilityData(i);
		}
		*/
    }

    private void resetHierarchyLevel(Long facilityId) {
        List<HierarchyType> list = new ArrayList<HierarchyType>();
        list.add(new HierarchyType("FACILITY", "WING", UsageCategory.HOU, facilityId));
        //list.add ( new HierarchyType("FACILITY", "WING", UsageCategory.HOU, 1L));
        list.add(new HierarchyType("WING", "UNIT", UsageCategory.HOU, facilityId));
        //list.add ( new HierarchyType("WING", "ROOM", UsageCategory.HOU, 1L));
        list.add(new HierarchyType("UNIT", "ROOM", UsageCategory.HOU, facilityId));
        list.add(new HierarchyType("ROOM", "BED", UsageCategory.HOU, facilityId));
        list.add(new HierarchyType("BED", null, UsageCategory.HOU, facilityId));

        list.add(new HierarchyType("FACILITY", "WING", UsageCategory.ACT, facilityId));
        list.add(new HierarchyType("WING", "ROOM", UsageCategory.ACT, facilityId));
        list.add(new HierarchyType("ROOM", "TABLE", UsageCategory.ACT, facilityId));
        list.add(new HierarchyType("TABLE", "CHAIR", UsageCategory.ACT, facilityId));
        list.add(new HierarchyType("CHAIR", null, UsageCategory.ACT, facilityId));

        Map<String, HierarchyType> ret = service.createUpdateLocationHierarchies(uc, list);
        assertNotNull(ret);
    }

    @Test
    public void get() {

        Long internalLocationId = rootId;
        FacilityInternalLocation ret = service.get(uc, internalLocationId);
        assertNotNull(ret);

    }

    @Test
    public void getAgeRanges() {

        List<AgeRangeType> ret = service.getAgeRanges(uc);
        assertNotNull(ret);

    }

    @Test
    public void getAll() {

        Set<Long> ret = service.getAll(uc);
        assertNotNull(ret);
    }

    @Test
    public void getAllHierchyLevels() {

        HierarchyType ret = service.getRootLocationHierarchy(uc, 1L, UsageCategory.HOU);
        assertNotNull(ret);

    }

    @Test
    public void getCount() {

        Long ret = service.getCount(uc);
        assertNotNull(ret);
    }

    @Test
    public void getDirectChildLocationIds() {
        Long parentId = rootId;
        List<InternalLocationIdentifierType> ret = service.getDirectChildLocationIds(uc, LocationCategory.HOUSING, parentId);
        assertNotNull(ret);

    }

    @Test
    public void getDirectChildLocationIds2() {
        Long parentId = rootId;
        List<InternalLocationIdentifierType> ret = service.getDirectChildLocationIds(uc, LocationCategory.HOUSING, parentId, true);
        assertNotNull(ret);

    }

    @Test
    public void getFacilityHousingCapacity() {

        FacilityStatisticsType ret = service.getFacilityHousingCapacity(uc, facilityId);
        assertNotNull(ret);

    }

    @Test
    public void getFacilityHousingCapacityAttribute() {

        //Take 5-6 seconds with 2000 FIL locations in db, need to improve the performance.
        List<HousingCapacityAttributeType> ret = service.getFacilityHousingCapacityAttribute(uc, facilityId);
        assertNotNull(ret);

    }

    @Test
    public void getFirstLevelIdsByFacilityId() {

        List<InternalLocationIdentifierType> ret = service.getFirstLevelIdsByFacilityId(uc, LocationCategory.PHYSICAL, facilityId);
        assertNotNull(ret);

    }

    @Test
    public void getHousingNonAssociationConflicts() {

        Set<Long> ids = getChildIds(rootId);
        //Set<Long> ids = getChildIds(28l);
        Set<NonAssociationConflictType> nonAssocConflicts = getNonAssociations();

        List<HousingNonAssociationConflictType> ret = service.getHousingNonAssociationConflicts(uc, ids, nonAssocConflicts);

        assertNotNull(ret);

    }

    @Test
    public void getHousingCapacityAttribute() {

        Long internalLocationId = rootId;
        //Long internalLocationId = 2112l;
        LocationAttributeSearchType locAttributeFilter = getLocationAttributes();
        List<HousingCapacityAttributeType> ret = service.getHousingCapacityAttribute(uc, internalLocationId, locAttributeFilter, true, CapacityAttribute.ALL);
        assertNotNull(ret);

    }

    @Test
    public void getHousingCapacityAttribute2() {

        Long internalLocationId = rootId;
        LocationAttributeSearchType locAttributeFilter = getLocationAttributes();
        List<HousingCapacityAttributeType> ret = service.getHousingCapacityAttribute(uc, internalLocationId, locAttributeFilter, true, CapacityAttribute.ATTRIBUTE);
        assertNotNull(ret);

    }

    @Test
    public void getHousingCapacityAttribute3() {

        Long internalLocationId = rootId;
        LocationAttributeSearchType locAttributeFilter = getLocationAttributes();
        List<HousingCapacityAttributeType> ret = service.getHousingCapacityAttribute(uc, internalLocationId, locAttributeFilter, true, CapacityAttribute.CAPACITY);
        assertNotNull(ret);

    }

    @Test
    public void getHousingCapacityAttributeGroup() {

        Set<Long> internalLocationIds = getChildIds(rootId);
        log.info("getChildIds=" + internalLocationIds);

        LocationAttributeSearchType locAttributeFilter = getLocationAttributes();
        List<GroupHousingCapacityAttributeType> ret = service.getHousingCapacityAttribute(uc, internalLocationIds, locAttributeFilter, true, CapacityAttribute.ALL);
        assertNotNull(ret);

    }

    @Test
    public void getLeafLocationIds() {
        Long parentId = rootId;
        List<InternalLocationIdentifierType> ret = service.getLeafLocationIds(uc, parentId, LocationCategory.HOUSING, true);
        assertNotNull(ret);

    }

    @Test
    public void getLocationCodes() {

        Set<Long> ids = getChildIds(rootId);
        //Set<Long> ids = getChildIds(28l); //modified as per db entries
        List<InternalLocationIdentifierType> ret = service.getLocationCodes(uc, ids);
        assertNotNull(ret);

    }

    @Test
    public void getLeafHousingCapacityAttribute() {
        //Susan expect max 20000 internal location for one facility
        //Take 15 seconds with 4000 FIL locations in db, need to improve the performance.
        List<HousingCapacityAttributeType> ret = service.getLeafHousingCapacityAttribute(uc, facilityId, true, CapacityAttribute.CAPACITY);
        assertNotNull(ret);

    }

    @Test
    public void getLeafHousingCapacityAttribute2() {
        //Susan expect max 20000 internal location for one facility
        //Take 15 seconds with 4000 FIL locations in db, need to improve the performance.
        List<HousingCapacityAttributeType> ret = service.getLeafHousingCapacityAttribute(uc, facilityId, true, CapacityAttribute.ALL);
        assertNotNull(ret);
    }

    @Test
    public void getLeafHousingCapacityAttribute3() {
        //Susan expect max 20000 internal location for one facility
        //Take 15 seconds with 4000 FIL locations in db, need to improve the performance.
        List<HousingCapacityAttributeType> ret = service.getLeafHousingCapacityAttribute(uc, facilityId, true, CapacityAttribute.ATTRIBUTE);
        assertNotNull(ret);

        ret = service.getLeafHousingCapacityAttribute(uc, facilityId, false, CapacityAttribute.CAPACITY);
        assertNotNull(ret);

    }

    @Test
    public void getLeafHousingCapacityAttribute4() {
        //Susan expect max 20000 internal location for one facility
        //Take 15 seconds with 4000 FIL locations in db, need to improve the performance.
        List<HousingCapacityAttributeType> ret = service.getLeafHousingCapacityAttribute(uc, facilityId, false, CapacityAttribute.ALL);
        assertNotNull(ret);

    }

    @Test
    public void search() {

        InternalLocationSearchType searchCriteria = new InternalLocationSearchType();
        searchCriteria.setDeactivateFromDate(createPastDate(1));
        searchCriteria.setDeactivateReason(new String("REF"));
        FacilityInternalLocationsReturnType ret = service.search(uc, searchCriteria, null);
        assertNotNull(ret);

    }

    @Test
    public void search2() {
        Long parentId = rootId;
        //Long parentId = 77L;
        Set<Long> Ids = getChildIds(parentId);
        InternalLocationSearchType searchCriteria = new InternalLocationSearchType();
        searchCriteria.setFacilityId(facilityId);
        FacilityInternalLocationsReturnType ret = service.search(uc, searchCriteria, Ids);
        assertNotNull(ret);

    }

    @Test
    public void testLocationHousingAttributes() {

        //Take 15 seconds with 4000 FIL locations in db, need to improve the performance.
        Long parentId = rootId;
        LocationAttributeSearchType attributeSearch = new LocationAttributeSearchType();
        attributeSearch.getLocationProperties().add(new String("SHOWER"));
        attributeSearch.getLocationProperties().add(new String("WHEELCHAIR"));
        List<HousingCapacityAttributeType> ret = service.getHousingCapacityAttribute(uc, parentId, attributeSearch, Boolean.TRUE, CapacityAttribute.CAPACITY);
        assertNotNull(ret);
        System.out.println(ret.size());
    }

    private void createFacilityData(long facilityId, long level1, long level2, long level3) {

        Set<String[]> nonAssociations = getLocationNonAssociations();
        Set<String[]> locProp1 = getLocationProperties();
        Set<String[]> locProp2 = getLocationProperties2();

        Long rootId = createRoot(facilityId);

        for (Long i = 1l; i <= level1; i++) {

            Long wingId = createLevel1(facilityId, rootId, i, null, UsageCategory.HOU);  //Wing

            for (Long j = 1l; j <= level2; j++) {

                Long untiId = createLevel2(facilityId, wingId, j, nonAssociations, UsageCategory.HOU);  //Unit

                Long k = 1l;
                //Create cell without bed
                for (; k <= level3 / 3; k++) {
                    Long roomId = createLevel3(facilityId, untiId, k, locProp1, UsageCategory.HOU); //Room
                    createHousingUsage(roomId, HOUSING_GP, 8l, 2l);
                    createHousingUsage(roomId, HOUSING_PC, 2l, 1l);
                }

                k = k + 1;
                //Create cell without bed
                //				for (; k<= level3; k++) {
                //					Long roomId = createLevel3(facilityId, untiId, k, locProp1, UsageCategory.HOU); //Room
                //					createHousingUsage(roomId, HOUSING_GP, 8l, 2l);
                //				}

                k = k + 1;
                //Create cell with 2 bed
                for (; k <= level3 * 2 / 3; k++) {
                    Long roomId = createLevel3(facilityId, untiId, k, null, UsageCategory.HOU); //Room

                    for (Long m = 1l; m <= 2; m++) {

                        Long bedId = createLevel4Bed(facilityId, roomId, m, locProp2, UsageCategory.HOU); //Bed
                        createHousingUsage(bedId, HOUSING_GP, 1l, 0l);

                    }
                }

                k = k + 1;
                //Create cell with 8 bed
                //				for (; k<= level3; k++) {
                //					Long roomId = createLevel3(facilityId, untiId, k, null, UsageCategory.HOU); //Room
                //
                //					for (Long m= 1l; m<=8; m++) {
                //
                //						Long bedId = createLevel4Bed(facilityId, roomId, m, locProp2, UsageCategory.HOU); //Bed
                //						createHousingUsage(bedId, HOUSING_GP, 1l, 0l);
                //
                //					}
                //				}

                k = k + 1;
                //Create cell with 4 bed
                for (; k <= level3; k++) {
                    Long roomId = createLevel3(facilityId, untiId, k, null, UsageCategory.HOU); //Room

                    for (Long m = 1l; m <= 4; m++) {

                        Long bedId = createLevel4Bed(facilityId, roomId, m, locProp2, UsageCategory.HOU); //Bed
                        createHousingUsage(bedId, HOUSING_GP, 1l, 0l);

                    }
                }
            }
        }
        //ACTIVITY
        for (Long i = 1l; i <= level1; i++) {

            Long wingId = createLevel1(facilityId, rootId, i, null, UsageCategory.ACT);  //Wing

            for (Long j = 1l; j <= level2; j++) {

                Long roomId = createLevel2Act(facilityId, wingId, j, nonAssociations, UsageCategory.ACT);  //ROOM

                Long k = 1l;
                //Create cell without bed
                for (; k <= level3 / 2; k++) {
                    Long tableId = createLevel3Act(facilityId, roomId, k, locProp1, UsageCategory.ACT); //TABLE
                    createActivityUsage(tableId, ACTIVITY_GYM, 8l, 2l);
                    createActivityUsage(tableId, ACTIVITY_VISIT, 2l, 1l);
                }

                //k = k + 1;
                //Create cell without CHAIR
                //				for (; k<= level3; k++) {
                //					Long chairId = createLevel3(facilityId, roomId, k, locProp1, UsageCategory.ACT); //CHAIR
                //					createHousingUsage(chairId, ACTIVITY_VISIT, 8l, 2l);
                //				}

                k = k + 1;
                //Create cell with 2 CHAIR
                for (; k <= level3 * 2 / 3; k++) {
                    Long tableId = createLevel3Act(facilityId, roomId, k, null, UsageCategory.ACT); //TABLE
                    for (Long m = 1l; m <= 2; m++) {
                        Long chairId = createLevel4(facilityId, tableId, m, locProp2, UsageCategory.ACT); //CHAIR
                        createActivityUsage(chairId, ACTIVITY_VISIT, 1l, 0l);

                    }
                }

                k = k + 1;
                //Create cell with 8 CHAIR
                for (; k <= level3; k++) {
                    Long tableId = createLevel3Act(facilityId, roomId, k, null, UsageCategory.ACT); //TABLE
                    for (Long m = 1l; m <= 8; m++) {
                        Long chairId = createLevel4(facilityId, tableId, m, locProp2, UsageCategory.ACT); //CHAIR
                        createActivityUsage(chairId, ACTIVITY_VISIT, 1l, 0l);
                    }
                }
                k = k + 1;
                //Create cell with 4 CHAIR
                for (; k <= level3; k++) {
                    Long tableId = createLevel3Act(facilityId, roomId, k, null, UsageCategory.HOU); //TABLE
                    for (Long m = 1l; m <= 4; m++) {
                        Long chairId = createLevel4(facilityId, tableId, m, locProp2, UsageCategory.HOU); //CHAIR
                        createActivityUsage(chairId, ACTIVITY_VISIT, 1l, 0l);
                    }
                }
            }
        }
    }

    private Long createRoot(Long facilityId) {
        String level = "FACILITY";
        String code = "Root-facility-" + facilityId;
        String description = "physical location " + code;
        FacilityInternalLocation ret = createInternalLocation(facilityId, null, level, code, description, null, UsageCategory.HOU);
        return ret.getId();
    }

    private Long createLevel1(Long facilityId, Long parentId, Long index, Set<String[]> prop, UsageCategory usageCategory) {
        String level = "WING";
        String code = usageCategory.name() + "-Wing-" + parentId + "-" + index;
        String description = "Level 1 " + code;
        FacilityInternalLocation ret = createInternalLocation(facilityId, parentId, level, code, description, prop, usageCategory); //Swing
        return ret.getId();
    }

    private Long createLevel2(Long facilityId, Long parentId, Long index, Set<String[]> prop, UsageCategory usageCategory) {
        String level = "UNIT";
        String code = usageCategory.name() + "-Unit-" + parentId + "-" + index;
        String description = "Level 2 " + code;
        FacilityInternalLocation ret = createInternalLocation(facilityId, parentId, level, code, description, prop, usageCategory);  //Floor
        return ret.getId();
    }

    private Long createLevel2Act(Long facilityId, Long parentId, Long index, Set<String[]> prop, UsageCategory usageCategory) {
        String level = "ROOM";
        String code = usageCategory.name() + "-Room-" + parentId + "-" + index;
        String description = "Level 2 " + code;
        FacilityInternalLocation ret = createInternalLocation(facilityId, parentId, level, code, description, prop, usageCategory);  //Floor
        return ret.getId();
    }

    private Long createLevel3(Long facilityId, Long parentId, Long index, Set<String[]> prop, UsageCategory usageCategory) {
        String level = "ROOM";
        String code = usageCategory.name() + "-Room-" + parentId + "-" + index;
        String description = "Level 3 " + code;
        FacilityInternalLocation ret = createInternalLocation(facilityId, parentId, level, code, description, prop, usageCategory);  //Cell
        return ret.getId();
    }

    private Long createLevel3Act(Long facilityId, Long parentId, Long index, Set<String[]> prop, UsageCategory usageCategory) {
        String level = "TABLE";
        String code = usageCategory.name() + "-Table-" + parentId + "-" + index;
        String description = "Level 3 " + code;
        FacilityInternalLocation ret = createInternalLocation(facilityId, parentId, level, code, description, prop, usageCategory);  //Cell
        return ret.getId();
    }

    private Long createLevel4Bed(Long facilityId, Long parentId, Long index, Set<String[]> prop, UsageCategory usageCategory) {
        String level = "BED";
        String code = usageCategory.name() + "-Bed-" + parentId + "-" + index;
        String description = "Level 4 " + code;
        FacilityInternalLocation ret = createInternalLocation(facilityId, parentId, level, code, description, prop, usageCategory);  //Cell
        return ret.getId();
    }

    private Long createLevel4(Long facilityId, Long parentId, Long index, Set<String[]> prop, UsageCategory usageCategory) {
        String level = "CHAIR";
        String code = usageCategory.name() + "-Chair-" + parentId + "-" + index;
        String description = "Level 4 " + code;
        FacilityInternalLocation ret = createInternalLocation(facilityId, parentId, level, code, description, prop, usageCategory);  //Cell
        return ret.getId();
    }

    //	private Long createLevel5(Long facilityId, Long parentId, Long index, Set<String[]> prop) {
    //		String level = "CHAIR";
    //		String code = "Chair-" + parentId + "-" + index;
    //		String description = "Level 5 " + code;
    //		FacilityInternalLocationType ret = createInternalLocation(facilityId, parentId, level, code, description, prop, UsageCategory.ACT);  //Cell
    //		return ret.getId();
    //	}

    private Set<String[]> getLocationNonAssociations() {
        Set<String[]> prop = new HashSet<String[]>();
        prop.add(new String[] { FacilityInternalLocationMetaSet.INTERNAL_LOCATION_NONASSOCIATION, "STG" });
        prop.add(new String[] { FacilityInternalLocationMetaSet.INTERNAL_LOCATION_NONASSOCIATION, "TOTAL" });
        return prop;
    }

    private Set<String[]> getLocationProperties() {
        Set<String[]> prop = new HashSet<String[]>();
        prop.add(new String[] { FacilityInternalLocationMetaSet.AGE_RANGE, AGERANGE_JUVENILE });
        prop.add(new String[] { FacilityInternalLocationMetaSet.LOCATION_PROPERTY, LOCATIONPROPERTY_WHEELCHAIR });
        prop.add(new String[] { FacilityInternalLocationMetaSet.LOCATION_PROPERTY, LOCATIONPROPERTY_SHOWER });
        prop.add(new String[] { FacilityInternalLocationMetaSet.SECURITY_LEVEL, SECURITYLEVEL_MAX });
        prop.add(new String[] { FacilityInternalLocationMetaSet.SENTENCE_STATUS, SENTENCESTATUS_SENTC });
        prop.add(new String[] { FacilityInternalLocationMetaSet.OFFENDER_CATEGORY, OFFENDERCATEGORY_SUIC });
        prop.add(new String[] { FacilityInternalLocationMetaSet.OFFENDER_GENDER, GENDER_M });
        return prop;
    }

    private Set<String[]> getLocationProperties2() {
        Set<String[]> prop = new HashSet<String[]>();
        prop.add(new String[] { FacilityInternalLocationMetaSet.AGE_RANGE, AGERANGE_ADULT });
        prop.add(new String[] { FacilityInternalLocationMetaSet.SECURITY_LEVEL, SECURITYLEVEL_MIN });
        prop.add(new String[] { FacilityInternalLocationMetaSet.SENTENCE_STATUS, SENTENCESTATUS_UNSTC });
        prop.add(new String[] { FacilityInternalLocationMetaSet.OFFENDER_GENDER, GENDER_F });
        return prop;
    }

    private void createHousingUsage(Long internalLocationId, String code, Long optimum, Long overflow) {
        String desc = "HOU desc " + code + " " + internalLocationId;
        createUsage(UsageCategory.HOU, code, internalLocationId, desc, optimum, overflow);
    }

    private void createActivityUsage(Long internalLocationId, String code, Long optimum, Long overflow) {
        String desc = "ACT desc " + code + " " + internalLocationId;
        createUsage(UsageCategory.ACT, code, internalLocationId, desc, optimum, overflow);
    }

    private Set<Long> getChildIds(Long parentId) {
        List<InternalLocationIdentifierType> ret = service.getDirectChildLocationIds(uc, LocationCategory.HOUSING, parentId);

        Set<Long> retIds = new HashSet<Long>();

        if (ret != null) {
            for (InternalLocationIdentifierType dto : ret) {
                if (dto != null) {
					retIds.add(dto.getId());
				}
            }
        }
        return retIds;
    }

    private LocationAttributeSearchType getLocationAttributes() {

        Set<String> genders = new HashSet<String>();
        genders.add(GENDER_M);
        Set<String> ageRanges = new HashSet<String>();
        ageRanges.add(AGERANGE_JUVENILE);
        LocationAttributeSearchType ret = new LocationAttributeSearchType();
        ret.setGenders(genders);
        ret.setAgeRanges(ageRanges);

        return ret;
    }

    private Set<NonAssociationConflictType> getNonAssociations() {

        Long naInternalLocationId = rootId;
        Set<NonAssociationConflictType> ret = new HashSet<NonAssociationConflictType>();

        NonAssociationConflictType dto = new NonAssociationConflictType();
        dto.setNaInternalLocationId(naInternalLocationId);
        dto.setNaOffenderSupervisionId(naInternalLocationId);
        dto.setNonAssociation(LOCATIONNONASSOCIATION_TOTAL);

        ret.add(dto);
        return ret;
    }
}
