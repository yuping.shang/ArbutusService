package syscon.arbutus.product.services.facilityinternallocation.test;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.CapacityType;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.FacilityInternalLocation;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.UsageCategory;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.UsageType;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class UsageIT extends FILBaseIT {

    private static Logger log = LoggerFactory.getLogger(UsageIT.class);
    private UsageType housingUsage;
    private UsageType storageUsage;
    private Long personId = 0L;

    @BeforeClass
    public void beforeClass() throws Exception {
        log.info("beforeClass Begin - JNDI lookup");
        if (service == null) {
            service = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        }
        uc = super.initUserContext();

        service.deleteAll(uc);
        createFacilityInternalLocationTree();
    }

    //@AfterClass
    public void afterClassCleanup() {
        destropLocationTree();
        FacilityService facService = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        facService.delete(uc, facility2Id, personId);
    }

    @Test
    public void testAddUsage() {

        //Rule: Verify if this location has any child location, if has, will not allow to add usage against this location.

        UsageType h = new UsageType();
        h.setUsageCategory(UsageCategory.HOU);
        h.setUsageSubtype(new String("PC"));
        Long internalLocationId = child332.getId();
        //Long internalLocationId = 734L;
        h.setInternalLocationId(internalLocationId);
        h.setDescription("desc Room232 again");
        CapacityType cp = new CapacityType();
        cp.setUsageCategory(h.getUsageCategory());
        cp.setUsageType(h.getUsageSubtype());
        cp.setOptimalCapacity(2l);
        cp.setOverflowCapacity(1l);
        cp.setInternalLocationId(internalLocationId);
        h.setCapacity(cp);
        h.setUsageAllowed(true);
        try {
            UsageType ret = service.addUsage(uc, h);
        } catch (Exception e) {
            //TODO:
        }

    }

    @Test(expectedExceptions = InvalidInputException.class)
    public void testInvalidAddActivityUsage() {
        UsageType h = new UsageType();
        h.setUsageCategory(UsageCategory.ACT);
        h.setUsageSubtype(new String("PROGRAM"));
        h.setInternalLocationId(child331.getId());
        h.setDescription("PROGRAM Room 331");
        CapacityType cp = new CapacityType();
        cp.setUsageCategory(h.getUsageCategory());
        cp.setUsageType(h.getUsageSubtype());
        cp.setOptimalCapacity(20l);
        cp.setOverflowCapacity(10l);
        cp.setInternalLocationId(child331.getId());
        h.setCapacity(cp);
        h.setUsageAllowed(true);

        UsageType ret = service.addUsage(uc, h);
        assertNotNull(ret);

        //housingUsage = ret;
        //assertNotNull(housingUsage);
    }

    @Test
    public void testAddHousingUsage_DuplicatedType() {
        UsageType h = new UsageType();
        h.setUsageCategory(UsageCategory.HOU);
        h.setUsageSubtype(new String("PC"));
        h.setInternalLocationId(child232.getId());
        h.setDescription("desc Room232 again");
        CapacityType cp = new CapacityType();
        cp.setUsageCategory(h.getUsageCategory());
        cp.setUsageType(h.getUsageSubtype());
        cp.setOptimalCapacity(2l);
        cp.setOverflowCapacity(1l);
        cp.setInternalLocationId(child232.getId());
        h.setCapacity(cp);
        h.setUsageAllowed(true);

        try {
            UsageType ret = service.addUsage(uc, h);
        } catch (Exception e) {
            //TODO:
        }

    }

    @Test
    public void testAddHousingUsage() {
        UsageType h = new UsageType();
        h.setUsageCategory(UsageCategory.HOU);
        h.setUsageSubtype(new String("PC"));
        h.setInternalLocationId(child331.getId());
        h.setDescription("desc Room231");
        CapacityType cp = new CapacityType();
        cp.setUsageCategory(h.getUsageCategory());
        cp.setUsageType(h.getUsageSubtype());
        cp.setOptimalCapacity(2l);
        cp.setOverflowCapacity(1l);
        cp.setInternalLocationId(child331.getId());
        h.setCapacity(cp);
        h.setUsageAllowed(true);
        h.setUsageCode("HOUSING1");
        UsageType ret = service.addUsage(uc, h);
        assertNotNull(ret);

        housingUsage = ret;

        assertNotNull(housingUsage);

        //resolve ids for comparison
        h.setUsageId(housingUsage.getUsageId());
        //s	ct.setCommentIdentification(c.getComments().iterator().next().getCommentIdentification());
        assertEquals(housingUsage, h);

        FacilityInternalLocation retrivelRet = service.get(uc, child232.getId());
        assertNotNull(retrivelRet);

    }

    @Test(dependsOnMethods = "testAddHousingUsage")
    public void testGetHousingUsage() {
        UsageType ret = service.getUsage(uc, housingUsage.getUsageId());
        assertNotNull(ret);

        UsageType retrieved = ret;
        assertEquals(retrieved, housingUsage);
    }

    @Test(enabled = false, dependsOnMethods = "testGetHousingUsage")
    public void testUpdateHousingUsage() {
        UsageType h = housingUsage;
        h.setUsageSubtype(new String("GP"));
        h.setInternalLocationId(child332.getId());
        h.setDescription("desc modified");
        h.getCapacity().setUsageType(h.getUsageSubtype());
        h.getCapacity().setOptimalCapacity(4l);
        h.getCapacity().setOverflowCapacity(2l);
        h.setUsageAllowed(true);

        UsageType ret = service.updateUsage(uc, h);
        assertNotNull(ret);

        ret = service.getUsage(uc, housingUsage.getUsageId());
        assertNotNull(ret);

        UsageType updated = ret;
        assertEquals(updated, h);
    }

    @Test(expectedExceptions = InvalidInputException.class)
    public void testInvalidUpdate() {
        UsageType h = new UsageType();
        h.setUsageCategory(UsageCategory.HOU);
        h.setUsageSubtype(new String("PC"));
        h.setInternalLocationId(child333.getId());
        h.setUsageAllowed(false);
        h.setUsageCode("INVALIDTEST");
        UsageType ret = service.addUsage(uc, h);
        assertNotNull(ret);

        UsageType created = ret;
        created.setUsageCategory(UsageCategory.ACT);
        ret = service.updateUsage(uc, created);
    }

    @Test(enabled = true)
    public void testDeleteHousingUsage() {
        UsageType h = new UsageType();
        h.setUsageCategory(UsageCategory.HOU);
        h.setUsageSubtype(new String("PC-DELETE-1"));
        h.setUsageCode("PC-DELETE");
        h.setInternalLocationId(child232.getId());
        h.setDescription("desc Room232");
        CapacityType cp = new CapacityType();
        cp.setUsageCategory(h.getUsageCategory());
        cp.setUsageType(h.getUsageSubtype());
        cp.setOptimalCapacity(2l);
        cp.setOverflowCapacity(1l);
        cp.setInternalLocationId(child232.getId());
        h.setCapacity(cp);
        h.setUsageAllowed(true);

        UsageType ret = service.addUsage(uc, h);
        assertNotNull(ret);

        //housingUsage = ret;

        //resolve ids for comparison
        h.setUsageId(ret.getUsageId());
        //s	ct.setCommentIdentification(c.getComments().iterator().next().getCommentIdentification());
        assertEquals(ret, h);

        Long delRet = service.deleteUsage(uc, ret.getUsageId());
        assertEquals(delRet, ReturnCode.Success.returnCode());

        FacilityInternalLocation retrivelRet = service.get(uc, child232.getId());
        assertNotNull(retrivelRet);

    }

    //@Test
    public void testDeactiveUage() {

        Long usageId = 1L;
        String reason = new String("REF");
        String lang = "EN";
        String comment = "Test Deactive Usage";

        UsageType ret = service.deactivateUsage(uc, usageId, reason, lang, comment);
        assertNotNull(ret);

    }

    @Test(expectedExceptions = InvalidInputException.class)
    public void testInvalidAddStorageUsage() {
        //StorageUsageType s = new StorageUsageType();
        UsageType s = new UsageType();
        s.setUsageCategory(UsageCategory.STO);
        s.setUsageSubtype(new String("EVI"));
        Long internalLocationId = child331.getId();
        s.setInternalLocationId(internalLocationId);
        s.setDescription("storage Room331");
        CapacityType cp = new CapacityType();
        cp.setUsageCategory(s.getUsageCategory());
        cp.setUsageType(s.getUsageSubtype());
        cp.setOptimalCapacity(2l);
        cp.setOverflowCapacity(1l);
        cp.setInternalLocationId(internalLocationId);
        s.setCapacity(cp);
        s.setUsageAllowed(true);
        s.setUsageCode("STORAGE1");
        Set<String> properties = new HashSet<String>();
        properties.add(new String("CNTN"));
        properties.add(new String("Bulk"));
        properties.add(new String("VAL"));
        //s.setAllowedPropertyTypes(properties);

        UsageType ret = service.addUsage(uc, s);
        assertNotNull(ret);

        //		storageUsage = (StorageUsageType) ret;
        //		assertNotNull(storageUsage);
        //		assertEquals(storageUsage.getAllowedPropertyTypes().size(), 3);
    }

    @Test
    public void testAddStorageUsage() {
        //StorageUsageType s = new StorageUsageType();
        UsageType s = new UsageType();
        s.setUsageCategory(UsageCategory.STO);
        s.setUsageSubtype(new String("EVI"));
        Long internalLocationId = child211s.getId();
        s.setInternalLocationId(internalLocationId);
        s.setDescription("Storage Floor 2-1");
        CapacityType cp = new CapacityType();
        cp.setUsageCategory(s.getUsageCategory());
        cp.setUsageType(s.getUsageSubtype());
        cp.setOptimalCapacity(2l);
        cp.setOverflowCapacity(1l);
        cp.setInternalLocationId(internalLocationId);
        s.setCapacity(cp);
        s.setUsageAllowed(true);
        s.setUsageCode("STORAGE1");
        Set<String> properties = new HashSet<String>();
        properties.add(new String("CNTN"));
        properties.add(new String("Bulk"));
        properties.add(new String("VAL"));

        UsageType ret = service.addUsage(uc, s);
        storageUsage = ret;
        assertNotNull(ret);
    }

    @Test(dependsOnMethods = { "testAddStorageUsage" })
    public void testGetStorageUsage() {

        Long storageUsageId = storageUsage.getUsageId();

        UsageType ret = service.getUsage(uc, storageUsageId);
        assertNotNull(ret);
        assertNotNull(storageUsage);
    }

    @Test
    public void testUpdateStorageUsage() {

        Long storageUsageId = storageUsage.getUsageId();

        UsageType ret = service.getUsage(uc, storageUsageId);
        ret.setDescription("desc modified");
        assertNotNull(ret);

        assertNotNull(storageUsage);

        ret = service.updateUsage(uc, storageUsage);
        assertNotNull(ret);

    }

    @Test(dependsOnMethods = { "testUpdateStorageUsage" })
    public void testDeleteStorageUsage() {

        Long storageUsageId = storageUsage.getUsageId();

        Long delRet = service.deleteUsage(uc, storageUsageId);
        assertEquals(delRet, ReturnCode.Success.returnCode());
        try {
            UsageType ret = service.getUsage(uc, storageUsageId);
        } catch (Exception e) {
            //TODO:
        }
    }

}
