package syscon.arbutus.product.services.facilityinternallocation.test;

import java.util.*;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.*;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;

import static org.testng.Assert.*;

public class FILIT extends FILBaseIT {

    private List<FacilityInternalLocation> toBeDeleted = new ArrayList<FacilityInternalLocation>();
    private FacilityInternalLocation root;
    private FacilityInternalLocation childL1;

    public static FacilityInternalLocation createFacilityInternalLocation(String level, String code, String description) {
        FacilityInternalLocation pt = new FacilityInternalLocation();
        pt.setFacilityId(1L);
        pt.setHierarchyLevel(level);
        pt.setLocationCode(code);
        pt.setDescription(description);

        pt.getNonAssociations().add("TOTAL");
        pt.getNonAssociations().add("STG");

        return pt;
    }

    public static FacilityInternalLocation createFacilityInternalLocation(Long facilityId, String level, String code, String description) {
        FacilityInternalLocation pt = new FacilityInternalLocation();
        pt.setFacilityId(facilityId);
        pt.setHierarchyLevel(level);
        pt.setLocationCode(code);
        pt.setDescription(description);

        pt.getNonAssociations().add("TOTAL");
        pt.getNonAssociations().add("STG");

        return pt;
    }

    public static CommentType createComment(String text) {
        CommentType ct = new CommentType();
        ct.setComment(text);
        ct.setCommentDate(new Date());
        return ct;
    }

    @BeforeClass
    public void beforeClass() throws Exception {
        if (service == null) {
            service = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        }
        uc = super.initUserContext();

    }

    //@AfterClass
    public void afterClassCleanup() {
        if (service != null) {
            service.delete(uc, childL1.getId());
            service.delete(uc, root.getId());
        }
    }

    //@Test
    public void testRest() {
        Long ret = service.deleteAll(uc);
        assertEquals(ret, ReturnCode.Success.returnCode());
    }

    @Test(enabled = true)
    public void createFacilityByGivenId() {
        Facility facility = new Facility(1L, "FacilityCodeFIL", "FacilityName", new Date(), "COMMU", null, null, new HashSet<Long>(), null, false);
        FacilityService facService = (FacilityService) JNDILookUp(this.getClass(), FacilityService.class);
        Facility ret = facService.create(uc, facility);
        assertNotEquals(ret, null);
        assertEquals(ret.getFacilityIdentification().intValue(), 1);
    }

    @Test
    public void testGetAgeRange() {
        List<AgeRangeType> ret = service.getAgeRanges(uc);
        assertNotNull(ret);

        assertEquals(ret.size(), 2l);
    }

    @Test(enabled = true)
    public void testResetHierarchyLevel() {
        List<HierarchyType> list = new ArrayList<HierarchyType>();
        list.add(new HierarchyType("FACILITY", "WING", UsageCategory.HOU, 1L));
        //list.add ( new HierarchyType("FACILITY", "WING", UsageCategory.HOU, 1L));
        list.add(new HierarchyType("WING", "UNIT", UsageCategory.HOU, 1L));
        //list.add ( new HierarchyType("WING", "ROOM", UsageCategory.HOU, 1L));
        list.add(new HierarchyType("UNIT", "ROOM", UsageCategory.HOU, 1L));
        list.add(new HierarchyType("ROOM", "BED", UsageCategory.HOU, 1L));
        Map<String, HierarchyType> ret = service.createUpdateLocationHierarchies(uc, list);
        assertNotNull(ret);
    }

    @Test(dependsOnMethods = "testResetHierarchyLevel")
    public void testGetHierarchy() {
        HierarchyType ret = service.getRootLocationHierarchy(uc, 1L, UsageCategory.HOU);
        assertNotNull(ret);
    }

    @Test
    public void createFacilityInternalLocation() {
        String phyCode = "PC01";
        String description = "physical location root";
        String level = "FACILITY";
        FacilityInternalLocation pl = createFacilityInternalLocation(level, phyCode, description);
        pl.getComments().add(createComment("root comment"));

        LocationAttributeType prop = new LocationAttributeType();
        prop.getOffenderCategories().add("SUIC");
        pl.setLocationAttribute(prop);
        FacilityInternalLocation ret = service.create(uc, pl, UsageCategory.HOU);
        assertNotNull(ret);
        FacilityInternalLocation created = ret;
        assertNotNull(created);
        root = created;
        toBeDeleted.add(created);
        assertEquals(created.getLocationCode(), phyCode);
        assertEquals(created.getHierarchyLevel(), level);
        assertEquals(created.getDescription(), description);

        assertEquals(created.getLocationAttribute(), prop);
    }

    @Test(dependsOnMethods = "createFacilityInternalLocation")
    public void updateFacilityInternalLocation() {

        CommentType ct = new CommentType();
        ct.setComment("location updated");
        ct.setCommentDate(new Date());
        root.getComments().add(ct);
        FacilityInternalLocation ret = service.update(uc, root);

        assertNotNull(ret);
        FacilityInternalLocation updated = ret;
        assertNotNull(updated);
        root = updated;
        assertEquals(root.getComments().size(), 2);
    }

    @Test(dependsOnMethods = "updateFacilityInternalLocation")
    public void testCreateL1Child() {
        String phyCode = "LEVEL1";
        String description = "physical location child level 1";
        String level = "WING";
        FacilityInternalLocation childl1 = createFacilityInternalLocation(level, phyCode, description);
        childl1.setParentId(root.getId());

        LocationAttributeType prop = new LocationAttributeType();
        childl1.setLocationAttribute(prop);
        prop.getOffenderCategories().add("PRCUS");
        prop.getGenders().add("F");
        prop.getAgeRanges().add("JUVENILE");
        FacilityInternalLocation ret = service.create(uc, childl1, UsageCategory.HOU);

        assertNotNull(ret);
        childL1 = ret;
        assertNotNull(childL1);
        toBeDeleted.add(childL1);
        assertEquals(childL1.getLocationCode(), phyCode);
        assertEquals(childL1.getHierarchyLevel(), level);
        assertEquals(childL1.getDescription(), description);
        assertEquals(childL1.getParentId(), root.getId());
    }

    @Test(dependsOnMethods = "testCreateL1Child")
    public void testUpdateL1Child() {

        LocationAttributeType prop = new LocationAttributeType();
        childL1.setLocationAttribute(prop);
        prop.getOffenderCategories().add("PRCUS");
        prop.getGenders().add("F");
        prop.getAgeRanges().add("JUVENILE");

        FacilityInternalLocation ret = service.update(uc, childL1);
        assertNotNull(ret);

        ret = service.get(uc, root.getId());
        assertNotNull(ret);

        root = ret;
        assertNotNull(root.getLocationAttribute());
        assertEquals(root.getLocationAttribute().getAgeRanges().size(), 1);
        assertEquals(root.getLocationAttribute().getGenders().size(), 1);
        assertEquals(root.getLocationAttribute().getOffenderCategories().size(), 1);
    }

    @Test(dependsOnMethods = "testUpdateL1Child")
    public void createFacilityInternalLocation_DuplicatedCode_P() {
        String phyCode = "PC01";
        String description = "duplicate physical location code";
        String level = "WING";
        FacilityInternalLocation pl = createFacilityInternalLocation(level, phyCode, description);
        pl.setParentId(root.getId());
        try {
            FacilityInternalLocation ret = service.create(uc, pl, UsageCategory.HOU);
            Assert.assertNull(ret);
        } catch (Exception e) {
            fail("should not get here.");
        }
    }

    @Test(dependsOnMethods = "testUpdateL1Child")
    public void testDeleteParentFail() {
        try {
            Long ret = service.delete(uc, root.getId());
            Assert.assertNotEquals(ret, 1L);
        } catch (Exception e) {
            fail("should not get here.");
        }
    }

    @Test(dependsOnMethods = "testUpdateL1Child")
    public void createFacilityInternalLocation_DuplicatedCode() {
        String phyCode = "LEVEL1";
        String description = "duplicate physical location code";
        String level = "WING";
        FacilityInternalLocation pl = createFacilityInternalLocation(level, phyCode, description);
        pl.setParentId(root.getId());
        try {
            FacilityInternalLocation ret = service.create(uc, pl, UsageCategory.HOU);
            Assert.assertNull(ret);
        } catch (Exception e) {
            fail("should not get here.");
        }
    }

    @Test(dependsOnMethods = "testUpdateL1Child")
    public void testDeactivateL1Child() {
        String reason = "REF";
        FacilityInternalLocation ret = service.deactivate(uc, childL1.getId(), reason, "EN", "test deactive child");
        assertNotNull(ret);

        assertFalse(ret.isActive());
    }

    @Test(dependsOnMethods = "testDeactivateL1Child")
    public void testQueryLevel1() {
        List<InternalLocationIdentifierType> ret = service.getDirectChildLocationIds(uc, LocationCategory.PHYSICAL, root.getId(), Boolean.TRUE);
        assertNotNull(ret);
        assertEquals(ret.size(), 0l);

        ret = service.getDirectChildLocationIds(uc, LocationCategory.PHYSICAL, root.getId(), Boolean.FALSE);
        assertNotNull(ret);
        assertEquals(ret.size(), 1l);

        ret = service.getDirectChildLocationIds(uc, LocationCategory.PHYSICAL, root.getId(), null);
        assertNotNull(ret);
        assertEquals(ret.size(), 1l);
    }

    @Test(dependsOnMethods = "testQueryLevel1")
    public void testReactivateL1Child() {
        FacilityInternalLocation ret = service.reactivate(uc, childL1.getId(), "test reactive child");
        assertNotNull(ret);
        assertTrue(ret.isActive());
    }

    @Test(dependsOnMethods = "testReactivateL1Child")
    public void testDeactivateParent() {
        String reason = "CLO";
        FacilityInternalLocation ret = service.deactivate(uc, root.getId(), reason, "EN", "test deactive parent");
        assertNotNull(ret);

        assertFalse(ret.isActive());

        FacilityInternalLocation childRet = service.get(uc, childL1.getId());
        assertNotNull(childRet);
    }

    @Test
    public void testCreateChildLocation() {
        //Rule: verify the parent has usage or not, if has, will not allowed to create a child location.
        String phyCode = "LEVEL6";
        String description = "Add child physical location code";
        String level = "CHAIR";
        FacilityInternalLocation pl = createFacilityInternalLocation(3L, level, phyCode, description);
        Long internalLocationId = 736L;
        pl.setParentId(internalLocationId);
        try {
            FacilityInternalLocation ret = service.create(uc, pl, UsageCategory.HOU);
            Assert.assertNull(ret);
        } catch (Exception e) {
            fail("should not get here.");
        }
    }

    @Test(expectedExceptions = InvalidInputException.class)
    public void testAddUsageNegativeForHousing() {
        UsageType usageType = new UsageType();
        //set mandatory attribute
        usageType.setInternalLocationId(5L);    //Internal Location ID

        usageType.setUsageSubtype("MY_HOU");
        usageType.setUsageCategory(UsageCategory.HOU);
        usageType.setUsageCode("LOCATION_1");
        Set<String> usageProperties = new HashSet<String>();
        usageProperties.add("VIDEO");
        usageType.setUsageProperties(usageProperties);

        service.addUsage(uc, usageType);
    }

    @Test
    public void testGet() {
        FacilityInternalLocation facilityInternalLocationTypeReturn = service.get(uc, 1L);
        System.out.println(facilityInternalLocationTypeReturn.toString());
    }
}
