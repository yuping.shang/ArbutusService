package syscon.arbutus.product.services.facilityinternallocation.test;

import org.apache.log4j.Logger;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.common.BaseIT;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;

/**
 * Created by jliang on 12/15/15.
 */
public class ConfigurationIT extends BaseIT {

    private static Logger log = Logger.getLogger(ConfigurationIT.class);

    private FacilityInternalLocationService service;

    @AfterMethod
    public void afterMethod() {
    }

    @BeforeClass
    public void beforeClass() {

        log.info("beforeClass Begin - JNDI lookup");
        service = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        uc = super.initUserContext();

        SecurityClient client;
        try {
            client = SecurityClientFactory.getSecurityClient();
            client.setSimple("devtest", "devtest"); // nice regular service user
            client.login();
        } catch (Exception e) {
            e.printStackTrace();
        }

        init();
    }

    @AfterClass
    public void afterTest() {

    }

    public void init() {

    }

    @Test(enabled = true)
    public void isHierarchyConfigured() {

        boolean result = service.isHierarchyConfigured(uc, 1L);
        assert(result);

    }

    @Test(enabled = true)
    public void isLocationConfigured() {

        boolean result = service.isLocationConfigured(uc, 1L);
        assert(result);

    }
}
