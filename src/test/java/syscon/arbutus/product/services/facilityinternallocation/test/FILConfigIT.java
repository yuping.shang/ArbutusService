package syscon.arbutus.product.services.facilityinternallocation.test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.DefaultLocationCategory;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.DefaultLocationType;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;

import static org.testng.Assert.*;

public class FILConfigIT extends FILBaseIT {

    @BeforeClass
    public void beforeClass() throws Exception {
        if (service == null) {
            service = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        }
        uc = super.initUserContext();
        createFacilityInternalLocationTree();

    }

    @AfterClass
    public void afterClassCleanup() {
        destropLocationTree();
    }

    @AfterMethod
    public void deleteDefaults() {
        service.deleteDefaultLocation(uc, facility2Id, DefaultLocationCategory.DEFAULT_INTERNAL_LOCATION);
        service.deleteDefaultLocation(uc, facility2Id, DefaultLocationCategory.DEFAULT_HOUSING_LOCATION);
        service.deleteDefaultLocation(uc, facility3Id, DefaultLocationCategory.DEFAULT_INTERNAL_LOCATION);
        service.deleteDefaultLocation(uc, facility3Id, DefaultLocationCategory.DEFAULT_HOUSING_LOCATION);
    }

    @Test
    public void testSetDefaultLocationByCodes() {
        Set<DefaultLocationType> defaultLocations = new HashSet<DefaultLocationType>();
        defaultLocations.add(new DefaultLocationType(facility2Id, DefaultLocationCategory.DEFAULT_INTERNAL_LOCATION, "Wing 2-1", null));
        defaultLocations.add(new DefaultLocationType(facility2Id, DefaultLocationCategory.DEFAULT_HOUSING_LOCATION, "ROOM 2-1", null));
        defaultLocations.add(new DefaultLocationType(facility3Id, DefaultLocationCategory.DEFAULT_INTERNAL_LOCATION, "Wing 3-1", null));
        defaultLocations.add(new DefaultLocationType(facility3Id, DefaultLocationCategory.DEFAULT_HOUSING_LOCATION, "ROOM 3-1", null));
        List<DefaultLocationType> ret = service.setDefaultLocations(uc, defaultLocations);
        assertNotNull(ret);
        assertEquals(ret.size(), 4l);
        DefaultLocationType result = ret.iterator().next();
        assertNotNull(result.getInternalLocationId());
    }

    @Test
    public void testSetDefaultLocation_InvalidCodes() {
        Set<DefaultLocationType> defaultLocations = new HashSet<DefaultLocationType>();
        String code = "CheckIn";
        DefaultLocationCategory category = DefaultLocationCategory.DEFAULT_INTERNAL_LOCATION;
        defaultLocations.add(new DefaultLocationType(facility2Id, category, code, null));
        List<DefaultLocationType> ret = service.setDefaultLocations(uc, defaultLocations);
        assertNotNull(ret);
        assertEquals(ret.size(), 1l);
        DefaultLocationType result = ret.iterator().next();
        assertEquals(result.getFacilityId(), facility2Id);
        assertEquals(result.getLocationCode(), code);
        assertEquals(result.getDefaultLocationCategory(), category);
        assertNull(result.getInternalLocationId());
    }

    @Test
    public void testSetDefaultLocationByIds() {
        Set<DefaultLocationType> defaultLocations = new HashSet<DefaultLocationType>();
        defaultLocations.add(new DefaultLocationType(facility2Id, DefaultLocationCategory.DEFAULT_INTERNAL_LOCATION, null, child211.getId()));
        defaultLocations.add(new DefaultLocationType(facility2Id, DefaultLocationCategory.DEFAULT_HOUSING_LOCATION, null, child233.getId()));
        List<DefaultLocationType> ret = service.setDefaultLocations(uc, defaultLocations);
        assertNotNull(ret);
        assertEquals(ret.size(), 2l);
    }

    @Test
    public void testSetDefaultLocation_Invalid_Ids() {
        Set<DefaultLocationType> defaultLocations = new HashSet<DefaultLocationType>();
        defaultLocations.add(new DefaultLocationType(facility2Id, DefaultLocationCategory.DEFAULT_INTERNAL_LOCATION, null, 1l));
        defaultLocations.add(new DefaultLocationType(facility2Id, DefaultLocationCategory.DEFAULT_HOUSING_LOCATION, null, 2l));
        try {
            List<DefaultLocationType> ret = service.setDefaultLocations(uc, defaultLocations);
        } catch (Exception e) {
            //TODO
        }
    }
}
