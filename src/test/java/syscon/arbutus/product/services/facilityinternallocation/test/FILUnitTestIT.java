package syscon.arbutus.product.services.facilityinternallocation.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.*;
import syscon.arbutus.product.services.facilityinternallocation.contract.ejb.FacilityInternalLocationServiceBean;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.facilityinternallocation.contract.utils.CollectionUtils;

import static org.testng.Assert.*;

/**
 * This unit test for manually test only
 *
 * @author byu
 */
public class FILUnitTestIT extends FILBaseIT {

    private final static String HOUSING_PC = "PC";
    private final static String HOUSING_GP = "GP";

    private Session session = null;
    //private UserContext uc = null;
    private Transaction tx = null;

    @BeforeClass
    public void beforeClass() throws Exception {

        if (service == null) {
            service = (FacilityInternalLocationService) JNDILookUp(this.getClass(), FacilityInternalLocationService.class);
        }
        uc = super.initUserContext();

    }

    //for local debug use only
    @SuppressWarnings("unused")
    private void localLogin() {

        BasicConfigurator.configure();
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("arbutus-facilityinternallocation-test-unit");
        EntityManager em = emf.createEntityManager();
        session = (Session) em.getDelegate();

        service = new FacilityInternalLocationServiceBean(session);

    }

    @BeforeMethod
    public void beforeMethod() {
        if (session != null) {
            tx = session.beginTransaction();
            tx.begin();
        }
    }

    @AfterMethod
    public void afterMethod() {
        if (tx != null) {
            tx.commit();
        }
    }

    @Test
    public void testDataCreation() {
        //service.reset(uc);
        createFacilityInternalLocationTree();
        destropLocationTree();
    }

    @Test
    public void testDeactivateLeafLocation() {

        //Test for defect 1802

        String reason = new String("REF");
        FacilityInternalLocation ret = service.deactivate(uc, 95L, reason, "EN", "test deactive child");
        assertNotNull(ret);

        assertFalse(ret.isActive());

		/*
        FacilityInternalLocationType ret = service.reactivate(uc, 11L, "test reactive child");
		assertNotNull(ret);
		
		assertTrue(ret.isActive());
		*/
    }

    @Test
    public void test1850() {

        InternalLocationSearchType searchCriteria = new InternalLocationSearchType();
        searchCriteria.setCreationFromDate(createPastDate(1));
        FacilityInternalLocationsReturnType ret = service.search(uc, searchCriteria, null);
        assertNotNull(ret);

    }

    @Test
    public void test1852() {

        InternalLocationSearchType searchCriteria = new InternalLocationSearchType();
        searchCriteria.setDeactivateFromDate(createPastDate(1));
        searchCriteria.setDeactivateReason(new String("REF"));
        FacilityInternalLocationsReturnType ret = service.search(uc, searchCriteria, null);
        assertNotNull(ret);

    }

    @Test
    public void test2174() {

        HierarchyType level1 = new HierarchyType("FACILITY", "BLOCK", UsageCategory.HOU, 1L);
        HierarchyType level2 = new HierarchyType("BLOCK", "WING", UsageCategory.HOU, 1L);
        HierarchyType level21 = new HierarchyType("WING", "UNIT", UsageCategory.HOU, 1L);
        List<HierarchyType> levels = new ArrayList<HierarchyType>();
        levels.add(level1);
        levels.add(level2);
        levels.add(level21);

        Map<String, HierarchyType> ret = service.createUpdateLocationHierarchies(uc, levels);
        assertNotNull(ret);

        HierarchyType ret2 = service.getRootLocationHierarchy(uc, 1L, UsageCategory.HOU);
        assertNotNull(ret2);

    }

    //@Test
    public void test1879() {

        Long facilityInternalLocationId = 98L;

        UsageType h = new UsageType();
        h.setUsageCategory(UsageCategory.HOU);
        h.setUsageSubtype("PC");
        h.setInternalLocationId(facilityInternalLocationId);
        h.setDescription("usage desc ROOM 3-3");
        CapacityType cp = new CapacityType();
        cp.setUsageCategory(h.getUsageCategory());
        cp.setUsageType(h.getUsageSubtype());
        cp.setOptimalCapacity(2l);
        cp.setOverflowCapacity(1l);
        cp.setInternalLocationId(facilityInternalLocationId);
        h.setCapacity(cp);
        h.setUsageAllowed(true);

        UsageType ret = service.addUsage(uc, h);
        assertNotNull(ret);
		
		/*
		Long usageId = 161L;		
		
		UsageType h = new UsageType();
		h.setUsageId(usageId);
		h.setUsageCategory(UsageCategory.HOU);
		h.setUsageSubtype(new String(ReferenceCodeSetNames.HOUSINGUSAGETYPE.value(), "GP"));
		h.setInternalLocationId(facilityInternalLocationId);
		h.setDescription("usage updated desc ROOM 3-3");
		CapacityType cp = new CapacityType();
		cp.setUsageCategory(h.getUsageCategory());
		cp.setUsageType(h.getUsageSubtype());
		cp.setOptimalCapacity(8l);
		cp.setOverflowCapacity(2l);
		h.setCapacity(cp);
		h.setUsageAllowed(true);		
		
		UsageType ret = service.updateUsage(uc, h);
		assertNotNull(ret);
			
				
		Long delRet = service.deleteUsage(uc, usageId);
		assertEquals(delRet, ReturnCode.Success.returnCode());
		*/
        FacilityInternalLocation fil = getInternalLocation(facilityInternalLocationId);
        assertNotNull(fil);
    }

    //@Test
    public void test1593() {

        FacilityInternalLocation parent = getInternalLocation(224L);

        LocationAttributeType prop = new LocationAttributeType();
        parent.setLocationAttribute(prop);
        prop.getAgeRanges().add(new String("JUVENILE"));

        FacilityInternalLocation ret = service.update(uc, parent);
        assertNotNull(ret);

        ret = service.get(uc, 168L);
        assertNotNull(ret);

    }

    @Test
    public void test2389() {

        //Test UsageCategory doesn't match UsageSubType
        UsageType h = new UsageType();
        h.setUsageCategory(UsageCategory.ACT);
        h.setUsageSubtype(HOUSING_GP);  //Need a validation for these two values.
        h.setInternalLocationId(55L);
        h.setDescription("usage desc");
        CapacityType cp = new CapacityType();
        cp.setUsageCategory(h.getUsageCategory());
        cp.setUsageType(h.getUsageSubtype());
        cp.setOptimalCapacity(0l);
        cp.setOverflowCapacity(0l);
        cp.setInternalLocationId(55L);
        h.setCapacity(cp);
        h.setUsageAllowed(false);

        UsageType ret = service.addUsage(uc, h);
        assertNotNull(ret);

    }

    @Test
    public void test2236() {

        //Confirmed the issue exist, to be fixed later.
		
		/*
		String reason = new String("REF");		 
		FacilityInternalLocationType ret = service.deactivate(uc, 64L, reason, "EN", "test deactive child");
		assertNotNull(ret);
		
		assertFalse(ret.isActive());
		*/

        FacilityInternalLocation ret = service.reactivate(uc, 65L, "test deactive child");
        assertNotNull(ret);

        assertTrue(ret.isActive());
    }

    @Test
    public void test2443() {

        Long ret2 = service.getCount(uc);
        assertNotNull(ret2);

        //Currently the wildcard search not work.
        InternalLocationSearchType searchCriteria = new InternalLocationSearchType();
        searchCriteria.setDescription("first Level 2 child in facility 2");
        FacilityInternalLocationsReturnType ret = service.search(uc, searchCriteria, null);
        assertNotNull(ret);

    }

    @Test
    public void addUsageNegative() {

        //Test for defect 2389
        UsageType h = new UsageType();
        h.setUsageCategory(UsageCategory.ACT);
        h.setUsageSubtype(HOUSING_PC);
        h.setInternalLocationId(55L);
        h.setDescription("usage desc addUsageNegative");
        CapacityType cp = new CapacityType();
        cp.setUsageCategory(UsageCategory.HOU);
        cp.setUsageType(HOUSING_GP);
        cp.setOptimalCapacity(0l);
        cp.setOverflowCapacity(0l);
        cp.setInternalLocationId(55L);
        h.setCapacity(cp);
        h.setUsageAllowed(false);
        try {
            UsageType ret = service.addUsage(uc, h);
        } catch (Exception e) {
            //TODO:
        }
    }

    @Test
    public void testAddUsage() {

        UsageType h = new UsageType();
        h.setUsageCategory(UsageCategory.HOU);
        h.setUsageSubtype(HOUSING_GP);
        h.setInternalLocationId(500L);
        h.setDescription("usage desc");
        CapacityType cp = new CapacityType();
        cp.setUsageCategory(h.getUsageCategory());
        cp.setUsageType(h.getUsageSubtype());
        cp.setOptimalCapacity(0l);
        cp.setOverflowCapacity(0l);
        h.setCapacity(cp);
        h.setUsageAllowed(false);

        UsageType ret = service.addUsage(uc, h);
        assertNotNull(ret);

    }

    @Test
    public void getFacilityHousingCapacityAttribute() {

        //addUsage(500L, UsageCategory.HOU, HOUSING_GP);

        Long facilityId = 2L;
        List<HousingCapacityAttributeType> ret = service.getFacilityHousingCapacityAttribute(uc, facilityId);
        assertNotNull(ret);

    }

    @Test
    public void getLeafLocationIds() {
        Long parentId = 45L;
        List<InternalLocationIdentifierType> ret = service.getLeafLocationIds(uc, parentId, LocationCategory.HOUSING, true);
        assertNotNull(ret);

    }

    @Test
    public void getDirectChildLocationIds() {

        Long facilityInternalLocationId = 494L;
        List<InternalLocationIdentifierType> ret = service.getDirectChildLocationIds(uc, LocationCategory.HOUSING, facilityInternalLocationId);
        assertNotNull(ret);

    }

    @Test
    public void getLeafHousingCapacityAttribute() {

        Long facilityId = 2L;
        List<HousingCapacityAttributeType> ret = service.getLeafHousingCapacityAttribute(uc, facilityId, true, CapacityAttribute.CAPACITY);
        assertNotNull(ret);

    }

    private FacilityInternalLocation getInternalLocation(Long facilityInternalLocationId) {
        FacilityInternalLocation ret = service.get(uc, facilityInternalLocationId);
        assertNotNull(ret);
        assertNotNull(ret);
        assertTrue(CollectionUtils.isEmpty(ret.getChildren()));
        return ret;
    }

    @SuppressWarnings("unused")
    private void addUsage(Long internalLocationId, UsageCategory category, String code) {

        UsageType h = new UsageType();
        h.setUsageCategory(category);
        h.setUsageSubtype(code);
        h.setInternalLocationId(internalLocationId);
        h.setDescription("usage desc");
        CapacityType cp = new CapacityType();
        cp.setUsageCategory(h.getUsageCategory());
        cp.setUsageType(h.getUsageSubtype());
        cp.setOptimalCapacity(2l);
        cp.setOverflowCapacity(1l);
        cp.setInternalLocationId(internalLocationId);
        h.setCapacity(cp);
        h.setUsageAllowed(true);

        UsageType ret = service.addUsage(uc, h);
        assertNotNull(ret);

    }

}
