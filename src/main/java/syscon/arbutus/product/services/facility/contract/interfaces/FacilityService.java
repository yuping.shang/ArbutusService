package syscon.arbutus.product.services.facility.contract.interfaces;

import java.util.List;
import java.util.Map;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.facility.contract.dto.FacilitiesReturn;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.dto.FacilitySearch;
import syscon.arbutus.product.services.location.contract.dto.Location;

/**
 * The Facility service is used to uniquely-identify, correlate and aggregate
 * facility information used by governmental, corporate, business or
 * international bodies (including businesses) to eliminates redundant data
 * entry by facilitating reuse of previously stored information and automatic
 * update upon entry of new information. A FACILITY may be operated by one or
 * more ORGANIZATION entities and be associated with PERSONS.
 * <p>Types of FACILITY include:
 * <pre>
 * <li>Custody Facilities:  Jails or Prisons, probation / parole offices); police / sheriff departments; courts.</li>
 * <li>Community Facilities: Probation or Parole Offices;</li>
 * <li>General Facilites:  Police Stations, Hospitals, Courts etc.</li>
 * </pre>
 * <p><b>Scope</b>
 * <p>The scope of the Facility Service is limited to creation and maintenance of the facility, as well as the maintenance of the reference data used for the service:
 * <pre>
 * <li>Create facilities;</li>
 * <li>Update facilities;</li>
 * <li>De-activate facilities;</li>
 * <li>Re-activate facilites;</li>
 * </pre>
 * <p>The service will provide the ability for data exchange through NIEM.
 * The Facility Service will have references between persons/organization/locations and a facility e.g. Staff member, inmate, county jail etc. The persons and organizations that are associated to each facility are created in separate services.
 * <p>Parameter Specifications
 * <pre>
 * <li>Required - Must be presented.</li>
 * <li>Optional - If presented it will be considered.  If not presented it will be considered with some default value (even if default value is null).</li>
 * </pre>
 * For success code and error code and description.
 * <li>See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
 * for all error codes and descriptions.</li>
 * <p><b>JNDI Lookup Name:</b> FacilityService
 * <p><b>Example of invocation of the service:</b>
 * <pre>
 * 	Properties p = new Properties();
 * 	properties.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
 * 	properties.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
 * 	properties.put("java.naming.provider.url", hostAddress);  // hostAddress -- specified accordingly
 * 	Context ctx = new InitialContext(p);
 * 	FacilityService service = (FacilityService)ctx.lookup("FacilityService");
 * 	SecurityClient client = SecurityClientFactory.getSecurityClient();
 * 	client.setSimple(userId, password);
 * 	client.login();
 * 	UserContext uc = new UserContext();
 * 	uc.setConsumingApplicationId(clientAppId);
 * 	uc.setFacilityId(facilityId);
 * 	uc.setDeviceId(deviceId);
 * 	uc.setProcess(process);
 * 	uc.setTask(task);
 * 	uc.setTimestamp(timestamp);
 * 	FacilityType orgRet = service.create(uc, ...);
 * 	Long count = service.getCount(uc);
 * 	... ...
 * </pre>
 * <p>Facility Service Class Giagram:
 * <br><img src="uml/Facility - uml.ucls" />
 *
 * @author yshang, updated by byu, updated by jfu
 * @version 2.0 (Based on SDD 2.0)
 * @since April 16, 2012
 */
public interface FacilityService {

    /**
     * Create a single facility instance and association to other service as well.
     * <p><b>Function:</b>
     * <p>Record the following details of a facility:
     * name, code, category, created date, deactivate date, status, type.
     * <p>Ensure the following information are specified by the user when the facility is created:
     * Facility Name, Facility Code, Facility Category.
     * <p>Specify the category of a facility by selecting from a predefined list of values.
     * <p>Identify the organization that runs a facility.
     * <p>All meta data during a create must be active.
     * <p>Throws exception if instance cannot be created.
     *
     * @param uc       - Required
     *                 UserContext
     * @param facility - Required
     *                 FacilityType
     * @return FacilityType
     */
    public Facility create(UserContext uc, Facility facility);

    /**
     * Update a single facility instance and associations.
     * <p>Only allow to update the FacilityName, FacilityContactInformation, FacilityCategoryTet, FacilityType and OrganizationAssociation
     * <p>All meta data during an update must exist but does not need to be active.
     * <p>Association can be added during an update but they cannot be removed.  If a Association must be removed the
     * deleteAssociation method must be invoked.
     * <p>Throws exception if instance cannot be updated.
     *
     * @param uc       - Required
     *                 UserContext
     * @param facility - Required
     *                 FacilityType
     * @return FacilityType
     */
    public Facility update(UserContext uc, Facility facility);

    /**
     * Retrieve a single instance of Facility by using the id.
     * <p>Throws exception if incorrect facilityId is provided as parameter or there is an error.
     *
     * @param uc         - Required
     *                   UserContext
     * @param facilityId - Required
     *                   java.lang.Long
     * @return FacilityType
     */
    public Facility get(UserContext uc, Long facilityId);

    /**
     * Retrieve a single instance of Facility by using the Facility Code.
     * <p>Throws exception if incorrect facilityCode is provided as parameter or there is an error.
     *
     * @param uc           - Required
     *                     UserContext
     * @param facilityCode - Required
     *                     java.lang.String
     * @return FacilityType
     */
    public Facility get(UserContext uc, String facilityCode);

    /**
     * Get Facility Name By Facility Id
     *
     * @param uc         UserContext - Required
     * @param facilityId Long Required
     * @return StringFacility Name
     */
    public String getFacilityNameById(UserContext uc, Long facilityId);

    /**
     * Get Facility Name By Facility Category
     *
     * @param uc                 UserContext - Required
     * category           String Optional
     * @param facilityActiveFlag Boolean - Optional. True: All Active; False: All Inactive; null: Both All Active and Inactive.
     * @param facilityIDs        Set&lt;Long> - Optional. A subset of Facility IDs
     * @return Map&lt;Long, String> Long: Facility Id; String: Facility Name
     */
    public Map<Long, String> getFacilityNameByCategory(UserContext uc, String facilityCategory, Boolean facilityActiveFlag, Set<Long> facilityIDs);

    /**
     * Get All Facility Names
     *
     * @param uc                 UserContext - Required
     * @param facilityActiveFlag Boolean - Optional. True: All Active; False: All Inactive; null: Both All Active and Inactive.
     * @param facilityIDs        Set&lt;Long> - Optional. A subset of Facility IDs
     * @return Map&lt;Long, String> Long: facilityId; String: Facility Name.
     */
    public Map<Long, String> getFacilityNameMap(UserContext uc, Boolean facilityActiveFlag, Set<Long> facilityIDs);

    /**
     * Activate a single facility instance
     * <p>The purpose of the Activate Facility action is to reactivate the facility.
     * <p>a facility to active, if the following conditions exist:
     * <p>1. deactivate date is blank; or 2. deactivate date is the same or later than the system date.
     * <p>Throws exception if instance cannot be activated.
     *
     * @param uc       - Required
     *                 UserContext
     * @param facility - Required
     *                 FacilityType
     * @return FacilityType
     */
    public Facility activateFacility(UserContext uc, Facility facility);

    /**
     * Search for a set of instances based on a specified criteria.
     * The subsetSearch can be used to search only within the specified subset if Id's.
     * <p>Search criteria:
     * Facility Name, Facility Code, Facility Category, Facility Status, Facility Created Date,
     * Facility Deactivated Date, Facility Type.
     * <p>Text item/String will support the asterisk as a wildcard (that matches zero or more characters).
     * <p>Text item/String without a wildcard will be matched as an exact string match.
     * <p>Set Modes support:
     * <pre>
     * 	<li>ONLY-- SET in search criterion will match sets that only have an exact set match.</li>
     * 	<li>ANY-- SET in search criterion will match sets that have at least one of the items.</li>
     *  <li>ALL-- SET in search criterion will match sets that have at least the specified subset.</li>
     * </pre>
     * <pThrows exception if search cannot be completed.
     *
     * @param uc           UserContext
     * @param subsetSearch Set<java.lang.Long> nullable
     * @param search       FacilitySearchType
     *  setMode      String, Default = "ALL" (ONLY, ANY, ALL)
     * @return FacilitiesReturnType
     */
    public FacilitiesReturn search(UserContext uc, Set<Long> subsetSearch, FacilitySearch search, Long startIndex, Long resultSize, String resultOrder);

    /**
     * Count of all instances.
     * <p>Throws exception if there is an error during the count.
     *
     * @param uc UserContext
     * @return java.lang.Long
     */
    public Long getCount(UserContext uc);

    /**
     * Cascading delete of a single instance.
     * <p>Returns success code if instance can be deleted.
     * <p>Throws exception if instance cannot be deleted.
     *
     * @param uc         UserContext - Required
     * @param facilityId java.lang.Long - Required
     * @param personId   java.lang.Long - Required
     * @return Long
     */
    public Long delete(UserContext uc, Long facilityId, Long personId);

    /**
     * Cascading delete of all data.
     * <p>Return success code if all instances, references and reference data deleted.
     * <p>Throws exception if not all instances, references and reference data deleted.
     *
     * @param uc UserContext - Required
     * @return returnCode java.lang.Long
     */
    public Long deleteAll(UserContext uc);

    /**
     * Return a set of all instance identifiers.
     * <p>Throws exception if there is an error during the get all.
     *
     * @param uc UserContext
     * @return Set<java.lang.Long>
     */
    public Set<Long> getAll(UserContext uc);

    /**
     * Return the stamp of a single instance.
     * <p>Returns null if there is an error retrieving the stamp.
     *
     * @param uc         UserContext
     * @param facilityId Long
     * @return StampType
     */
    public StampType getStamp(UserContext uc, Long facilityId);

    /**
     * getNiem -- Get an XML output of the specified business object instance.
     * <p>Returns a xml structure that represents the error if niem XML output cannot be created. <xml><returncode><error/><message/></returncode></xml>
     *
     * @param uc         UserContext - Required
     * @param facilityId java.lang.Long - Required
     * @return String
     */
    public String getNiem(UserContext uc, Long facilityId);

    /**
     * Retrieve the version number in format "MajorVersionNumber.MinorVersionNumber"
     *
     * @param uc
     * @return version number
     */
    public String getVersion(UserContext uc);

    /**
     * Add a location record
     *
     * @param uc       UserContext - required
     * @param location location instance - required
     * @return location type
     * Throws exception if there is an error.
     */
    public Location addLocation(UserContext uc, Location location);

    /**
     * Delete a location record
     *
     * @param uc         UserContext - required
     * @param facilityId facility id - required
     * @param locationId location id - required
     * @return Return code
     * Return success code if instance be deleted.
     * Throws exception if there is an error.
     */
    public Long deleteLocation(UserContext uc, Long facilityId, Long locationId);

    /**
     * Update a location record
     *
     * @param uc       UserContext - required
     * @param location location instance - required
     * @return location type
     * Throws exception if there is an error.
     */
    public Location updateLocation(UserContext uc, Location location);

    /**
     * Get location by location Id.
     *
     * @param uc         UserContext - required
     * @param locationId location id - required
     * @return location type
     * Throws exception if there is an error.
     */
    public Location getLocation(UserContext uc, Long locationId);

    /**
     * Get locations by person Id.
     *
     * @param uc         UserContext - required
     * @param facilityId facility id - required
     * @return a list of location type
     * Throws exception if there is an error.
     */
    public List<Location> getLocations(UserContext uc, Long facilityId);

    /**
     * Return a set of all instance identifiers which are associated to any Organization.
     * <p>Throws exception if there is an error during the get all.
     *
     * userContext UserContext - required
     * @return Set<java.lang.Long> List of Facility Identifier
     */
    public List<Long> getAllAssociatedFacilityToOrganization(UserContext uc);

    /**
     *
     * @param uc
     * @param facility
     */
    public void isDeactivationDateInFuture(UserContext uc, Facility facility);

    /**
     *
     * @param userContext
     * @param facilityId
     */
    public void deleteFacility(UserContext userContext, Long facilityId);

    /**
     * To add association between Organization and facility
     * @param uc
     * @param facilityId
     * @param organizatinId
     */
    public void addAssociatedOrganization(UserContext uc, Long facilityId, Long organizatinId);

    /**
     * Retrieve all active facilities that require internal location configuration.
     *
     * @param uc
     * @param facilitySetId
     * @return
     */
    public List<Facility> getFacilitiesForLocationConfiguration(UserContext uc, Long facilitySetId);

    /**
     * Retrieve all active facilities that have location configuration.
     *
     * @param uc
     * @param facilitySetId
     * @return
     */
    public List<Facility> getFacilitiesForLocation(UserContext uc, Long facilitySetId);
}