package syscon.arbutus.product.services.facility.contract.dto;

/**
 * <p>Java class for ReferenceSet.
 */
public enum ReferenceSet {

    /**
     * Define reference code set for Facilty Category TYPE.
     */
    FACILITY_CATEGORY("FacilityCategory"),
    FACILITY_TYPE("FacilityType");

    private final String value;

    ReferenceSet(String v) {
        value = v;
    }

    public static ReferenceSet fromValue(String v) {
        for (ReferenceSet c : ReferenceSet.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public String value() {
        return value;
    }

}
