package syscon.arbutus.product.services.facility.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Indexed;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.core.services.realization.BaseEntity;
import syscon.arbutus.product.services.organization.realization.persistence.OrganizationEntity;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * FacilityEntity for Facility Service
 *
 * @author yshang, updated by jliang
 * @version 2.0 (Based on SDD 2.0)
 * @DbComment FAC_Facility 'Main table for Facility Service'
 * @DbComment .createUserId 'Create User Id'
 * @DbComment .createDateTime 'Create Date Time'
 * @DbComment .modifyUserId 'Modify User Id'
 * @DbComment .modifyDateTime 'Modify Date Time'
 * @DbComment .invocationContext 'Invocation Context'
 * @since April 16, 2012
 */
@Audited
@Entity
@Indexed
@Table(name = "FAC_Facility")
@SQLDelete(sql = "UPDATE FAC_Facility SET flag = 4 WHERE FacilityId = ? and version = ?")
@Where(clause = "flag = 1")
public class FacilityEntity extends BaseEntity {

    private static final long serialVersionUID = -5193920209461702399L;

    /**
     * @DbComment .facilityId 'Facility Identification, an identification that references Facility'
     */
    @Id
    @DocumentId
    //	@GeneratedValue(strategy = GenerationType.AUTO, generator = "Seq_FAC_Facility_Id")
    //	@SequenceGenerator(name = "Seq_FAC_Facility_Id", sequenceName = "Seq_FAC_Facility_Id", allocationSize=1)
    @Column(name = "FacilityId")
    private Long facilityId;

    /**
     * @DbComment .Code 'Facility Code, a code used to identify the facility'
     */
    @Column(name = "Code", unique = true, nullable = false, length = 64)
    private String facilityCode;

    /**
     * @DbComment .Name 'Facility Name'
     */
    @Column(name = "Name", nullable = false, length = 128)
    private String facilityName;

    /**
     * @DbComment .DeactiveDate 'Facility Deactive Date, the date the facility was deactivated'
     */
    @Column(name = "DeactiveDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date facilityDeactiveDate;

    /**
     * @DbComment .CategoryText 'Facility Category, The general category of facility'
     */
    @MetaCode(set = MetaSet.FACILITY_CATEGORY)
    @Column(name = "CategoryText", nullable = false, length = 64)
    private String facilityCategoryText;

    /**
     * @DbComment .FacilityType 'Facility type'
     */
    @MetaCode(set = MetaSet.FACILITY_TYPE)
    @Column(name = "FacilityType", nullable = true, length = 64)
    private String facilityType;


    /**
     * @DbComment .FacilityContactId 'The PersonIdentityId of Contact Person that is the associated with this facility, this is a static element reference to the parent Facility.'
     */
    @Column(name = "FacilityContactId", nullable = true)
    private Long facilityContactId;



    /**
     * @DbComment .parentOrganizationId 'Parent Organization ID of the this facility'
     */
//    @Column(name = "parentOrganizationId", nullable = true)
//    private Long parentOrganizationId;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="parentOrganizationId")
    private OrganizationEntity parentOrganization;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "FAC_Organization",
            joinColumns = { @JoinColumn(name = "FacilityId", updatable = false)  },
            inverseJoinColumns = { @JoinColumn(name = "OrganizationId", referencedColumnName = "orgId") })
    private Set<OrganizationEntity> organizations;

    @Column(name = "InternalLocationRequired", nullable = true)
    private Boolean internalLocationRequired;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(name = "Flag", nullable = false)
    private Long flag = DataFlag.ACTIVE.value();


    /**
     * Constructor
     */
    public FacilityEntity() {
    }

    @Override
    public Long getId() {
        return facilityId;
    }

    /**
     * Constructor
     *
     * @param facilityId
     * @param facilityCode         - Required
     * @param facilityName         - Required
     * @param facilityDeactiveDate
     * @param facilityCategoryText - Required
     * @param facilityType
     * @param organizations
     */
    public FacilityEntity(Long facilityId, String facilityCode, String facilityName, Date facilityDeactiveDate, String facilityCategoryText, String facilityType,
            Set<OrganizationEntity> organizations, Long facilityContactId, boolean internalLocationRequired) {

        this.facilityId = facilityId;
        this.facilityCode = facilityCode;
        this.facilityName = facilityName;
        this.facilityDeactiveDate = getDateWithoutTime(facilityDeactiveDate);
        this.facilityCategoryText = facilityCategoryText;
        this.facilityType = facilityType;
        this.organizations = organizations;
        this.facilityContactId = facilityContactId;
        this.internalLocationRequired = internalLocationRequired;
    }

    /**
     * get a date without time
     *
     * @param date a date to trim the time
     * @return a Date object
     */
    public static Date getDateWithoutTime(Date date) {

        if (date == null) {
			return null;
		}

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Date newDate = cal.getTime();
        return newDate;
    }

    /**
     * @return the facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * @param facilityId the facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * @return the facilityCode
     */
    public String getFacilityCode() {
        return facilityCode;
    }

    /**
     * @param facilityCode the facilityCode to set
     */
    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    /**
     * @return the organizations
     */
    public Set<OrganizationEntity> getOrganizations() {
        if (organizations == null) {
			organizations = new HashSet<OrganizationEntity>();
		}
        return organizations;
    }

    /**
     * @param organizations the organizations to set
     */
    public void setOrganizations(Set<OrganizationEntity> organizations) {
        this.organizations = organizations;
    }

    /**
     * @return the facilityName
     */
    public String getFacilityName() {
        return facilityName;
    }

    /**
     * @param facilityName the facilityName to set
     */
    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    /**
     * @return the facilityContactId
     */
    public Long getFacilityContactId() {
        return facilityContactId;
    }

    /**
     * @param facilityContactId the facilityContactId to set
     */
    public void setFacilityContactId(Long facilityContactId) {
        this.facilityContactId = facilityContactId;
    }

    /**
     * @return the facilityDeactiveDate
     */
    public Date getFacilityDeactiveDate() {
        return facilityDeactiveDate;
    }

    /**
     * @param facilityDeactiveDate the facilityDeactiveDate to set
     */
    public void setFacilityDeactiveDate(Date facilityDeactiveDate) {
        //To trim the time part to zero
        this.facilityDeactiveDate = getDateWithoutTime(facilityDeactiveDate);
    }

    /**
     * @return the facilityCategoryText
     */
    public String getFacilityCategoryText() {
        return facilityCategoryText;
    }

    /**
     * @param facilityCategoryText the facilityCategoryText to set
     */
    public void setFacilityCategoryText(String facilityCategoryText) {
        this.facilityCategoryText = facilityCategoryText;
    }

    public String getFacilityType() {
        return facilityType;
    }

    public void setFacilityType(String facilityType) {
        this.facilityType = facilityType;
    }

//    public Long getParentOrganizationId() {
//        return parentOrganizationId;
//    }
//
//    public void setParentOrganizationId(Long parentOrganizationId) {
//        this.parentOrganizationId = parentOrganizationId;
//    }

    public OrganizationEntity getParentOrganization() {
        return parentOrganization;
    }

    public void setParentOrganization(OrganizationEntity parentOrganization) {
        this.parentOrganization = parentOrganization;
    }

    public Boolean isInternalLocationRequired() {
        return internalLocationRequired;
    }

    public void setInternalLocationRequired(Boolean internalLocationRequired) {
        this.internalLocationRequired = internalLocationRequired;
    }

    /**
     * @return the flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((facilityCategoryText == null) ? 0 : facilityCategoryText.hashCode());
        result = prime * result + ((facilityCode == null) ? 0 : facilityCode.hashCode());
        result = prime * result + ((facilityContactId == null) ? 0 : facilityContactId.hashCode());
        result = prime * result + ((facilityDeactiveDate == null) ? 0 : facilityDeactiveDate.hashCode());
        result = prime * result + ((facilityId == null) ? 0 : facilityId.hashCode());
        result = prime * result + ((facilityName == null) ? 0 : facilityName.hashCode());
        result = prime * result + ((facilityType == null) ? 0 : facilityType.hashCode());
        result = prime * result + ((flag == null) ? 0 : flag.hashCode());
        result = prime * result + ((organizations == null) ? 0 : organizations.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        FacilityEntity other = (FacilityEntity) obj;
        if (facilityCategoryText == null) {
            if (other.facilityCategoryText != null) {
				return false;
			}
        } else if (!facilityCategoryText.equals(other.facilityCategoryText)) {
			return false;
		}
        if (facilityCode == null) {
            if (other.facilityCode != null) {
				return false;
			}
        } else if (!facilityCode.equals(other.facilityCode)) {
			return false;
		}
        if (facilityContactId == null) {
            if (other.facilityContactId != null) {
				return false;
			}
        } else if (!facilityContactId.equals(other.facilityContactId)) {
			return false;
		}
        if (facilityDeactiveDate == null) {
            if (other.facilityDeactiveDate != null) {
				return false;
			}
        } else if (!facilityDeactiveDate.equals(other.facilityDeactiveDate)) {
			return false;
		}
        if (facilityId == null) {
            if (other.facilityId != null) {
				return false;
			}
        } else if (!facilityId.equals(other.facilityId)) {
			return false;
		}
        if (facilityName == null) {
            if (other.facilityName != null) {
				return false;
			}
        } else if (!facilityName.equals(other.facilityName)) {
			return false;
		}
        if (facilityType == null) {
            if (other.facilityType != null) {
				return false;
			}
        } else if (!facilityType.equals(other.facilityType)) {
			return false;
		}
        if (flag == null) {
            if (other.flag != null) {
				return false;
			}
        } else if (!flag.equals(other.flag)) {
			return false;
		}
        if (organizations == null) {
            if (other.organizations != null) {
				return false;
			}
        } else if (!organizations.equals(other.organizations)) {
			return false;
		}

        return true;
    }

    @Override
    public String toString() {
        return "FacilityEntity [facilityId=" + facilityId + ", facilityCode=" + facilityCode + ", facilityName=" + facilityName + ", facilityDeactiveDate="
                + facilityDeactiveDate + ", facilityCategoryText=" + facilityCategoryText + ", facilityType=" + facilityType + ", facilityContactId=" + facilityContactId + ", organizations=" + organizations + ", version="
                + "]";
    }
}