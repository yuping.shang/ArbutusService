package syscon.arbutus.product.services.facility.contract.ejb;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.DataExistException;
import syscon.arbutus.product.services.core.exception.ErrorCodes;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.core.exception.ValidationException;
import syscon.arbutus.product.services.facility.contract.dto.FacilitiesReturn;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.dto.FacilitySearch;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationService;
import syscon.arbutus.product.services.location.contract.dto.Location;
import syscon.arbutus.product.services.organization.contract.dto.Organization;
import syscon.arbutus.product.services.organization.contract.interfaces.OrganizationService;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.ExceptionHelper;
import syscon.arbutus.product.services.security.contract.interfaces.SecurityServiceLocal;
import syscon.arbutus.product.services.userpreference.contract.dto.UserPreferenceType;
import syscon.arbutus.product.services.userpreference.contract.interfaces.UserPreferenceService;
import syscon.arbutus.product.services.utility.ValidationHelper;
import syscon.arbutus.product.services.utility.ical.util.TimeUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import java.util.*;

/**
 * The Facility service is used to uniquely-identify, correlate and aggregate
 * facility information used by governmental, corporate, business or
 * international bodies (including businesses) to eliminates redundant data
 * entry by facilitating reuse of previously stored information and automatic
 * update upon entry of new information. A FACILITY may be operated by one or
 * more ORGANIZATION entities and be associated with PERSONS.
 * <pre></pre>
 * Types of FACILITY include:
 * <pre>
 * 	<li>Custody Facilities: Jails or Prisons, probation / parole offices); police / sheriff departments; courts.</li>
 * 	<li>Community Facilities: Probation or Parole Offices.</li>
 * 	<li>General Facilites: Police Stations, Hospitals, Courts etc.</li>
 * </pre>
 * Scope:
 * <pre></pre>
 * The scope of the Facility Service is limited to creation and maintenance of
 * the facility, as well as the maintenance of the reference data used for the
 * service:
 * <pre>
 * 	<li>Create facilities;</li>
 *  <li>Update facilities;</li>
 *  <li>De-activate facilities;</li>
 *  <li>Re-activate facilities;</li>
 * </pre>
 * The service will provide the ability for data exchange through NIEM. The
 * Facility Service will have references between persons/organization/locations
 * and a facility e.g. Staff member, inmate, county jail etc. The persons and
 * organizations that are associated to each facility are created in separate
 * services.
 *
 * @author yshang
 * @version 2.0 (Based on SDD 2.0)
 * @since April 16, 2012
 */
@Stateless(mappedName = "FacilityService", description = "The facility service")
@Remote(FacilityService.class)
public class FacilityServiceBean implements FacilityService {

    private static Logger log = LoggerFactory.getLogger(FacilityServiceBean.class);

    public static Long SUCCESS = 1L;

    @Resource
    SessionContext context;

    @PersistenceContext(unitName = "arbutus-pu")
    Session session;

    @EJB
    private FacilityInternalLocationService facilityInternalLocationService;

    @EJB
    private UserPreferenceService userPreferenceService;

    @EJB
    private OrganizationService organizationService;

    @EJB
    private SecurityServiceLocal securityService;

    private FacilityDAO facilityDAO;

    @PostConstruct
    private void init() {
        facilityDAO = new FacilityDAO(context, session);
    }
    /**
     * The Create Facility action allows for the creation of a new facility in
     * the system.
     * <pre></pre>
     * Business Needs Satisfied:
     * <pre>
     * <li>1.2.1.2.1-2 Add new facility</li>
     * <li>1.2.1.2.1-5 Designate justice agency location</li>
     * <li>1.2.1.2.1-6 Designate non-justice agency location</li>
     * <li>Designate program provider service location</li>
     * </pre>
     * Provenance:
     * <pre>
     * <li>FacilityIdentification is system generated</li>
     * <li>FacilityActiveFlag is derived</li>
     * <li>All other elements are manually entered by the user</li>
     * </pre>
     *
     * @param uc       UserContext - Required
     * @param facility FacilityType - Required
     * @return FacilityType
     */
    @Override
    public Facility create(UserContext uc, Facility facility) {

        ValidationHelper.validate(facility);    //WOR-22494

        isDuplicateFacility(uc, null, facility.getFacilityCode());
        isDeactivationDateInFuture(uc, facility); //WOR-22362
        validateFacilityHasParentOrganization(uc, facility);
        validateCreateFacilityWhenOrganizationIsDeactive(uc, facility);
        Facility newFacility = facilityDAO.create(uc, facility);
        return newFacility;
    }

    /**
     * The Update Facility action allows for the update of an existing facility
     * in the system.
     * <pre></pre>
     * Business Needs Satisfied:
     * <pre>
     * <li>1.2.1.2.1-3 Update facility</li>
     * </pre>
     * Provenance:
     * <pre>
     * <li>FacilityName, FacilityLocation, FacilityContactInformation, and FacilityCategoryText are the only updateable elements.</li>
     * </pre>
     *
     * @param uc       UserContext - Required
     * @param facility FacilityType - Required
     * @return FacilityType -- Throws exception if instance cannot be updated.
     */
    @Override
    public Facility update(UserContext uc, Facility facility) {
        isDuplicateFacility(uc, facility.getFacilityIdentification(), facility.getFacilityCode());

        // if deactivating
        if (facility.getFacilityDeactivateDate() != null) {
            if (TimeUtils.getDateWithoutTime(facility.getFacilityDeactivateDate()).compareTo(TimeUtils.getDateWithoutTime(facility.getCreateDateTime())) < 0) {
                throw new InvalidInputException(ErrorCodes.FAC_FACILITY_INVALID_DEACTIVATION_DATE);
            }
            validateFacilityInUse(uc, facility.getFacilityIdentification(), true);
        }
        
        // must have parent organization
        if(null == facility.getParentOrganizationId()) {
            throw new InvalidInputException(ErrorCodes.FAC_FACILITY_PARENT_ORGANIZATION_REQUIRED);
        }

        return facilityDAO.update(uc, facility);
    }
    
    /**
     * Retrieve a single instance of Facility by using the facilityId.
     * <pre></pre>
     * Returns a null instance and error code in return type if incorrect
     * instanceId is provided as parameter.
     *
     * @param uc         UserContext - Required
     * @param facilityId java.lang.Long - Required
     * @return FacilityType -- Throws exception if instance cannot be got
     */
    @Override
    public Facility get(UserContext uc, Long facilityId) {
        return facilityDAO.get(uc, facilityId);
    }

    /**
     * Retrieve a single instance of Facility by using the Facility Code.
     * <pre></pre>
     * Throws exception if incorrect facilityCode is provided as parameter.
     *
     * @param uc           UserContext - Required
     * @param facilityCode java.lang.String - Required
     * @return FacilityType -- Throws exception if instance cannot be got
     */
    @Override
    public Facility get(UserContext uc, String facilityCode) {
        return facilityDAO.get(uc, facilityCode);
    }

    /**
     * Get Facility Name By Facility Id
     *
     * @param uc         UserContext - Required
     * @param facilityId Long Required
     * @return StringFacility Name
     */
    @Override
    public String getFacilityNameById(UserContext uc, Long facilityId) {
        return facilityDAO.getFacilityNameById(uc, facilityId);
    }

    /**
     * Get Facility Name By Facility Category
     *
     * @param uc                 UserContext - Required
     * category           String Optional
     * @param facilityActiveFlag Boolean - Optional. True: All Active; False: All Inactive; null: Both All Active and Inactive.
     * @param facilityIDs        Set&lt;Long> - Optional. A subset of Facility IDs
     * @return Map&lt;Long, String> Long: Facility Id; String: Facility Name
     */
    @Override
    public Map<Long, String> getFacilityNameByCategory(UserContext uc, String facilityCategory, Boolean facilityActiveFlag, Set<Long> facilityIDs) {
        return facilityDAO.getFacilityNameByCategory(uc, facilityCategory, facilityActiveFlag, facilityIDs);
    }

    /**
     * Get All Facility Names
     *
     * @param uc                 UserContext - Required
     * @param facilityActiveFlag Boolean - Optional. True: All Active; False: All Inactive; null: Both All Active and Inactive.
     * @param facilityIDs        Set&lt;Long> - Optional. A subset of Facility IDs
     * @return Map&lt;Long, String> Long: facilityId; String: Facility Name.
     */
    @Override
    public Map<Long, String> getFacilityNameMap(UserContext uc, Boolean facilityActiveFlag, Set<Long> facilityIDs) {
        return facilityDAO.getFacilityNameMap(uc, facilityActiveFlag, facilityIDs);
    }

    /**
     * The purpose of the Activate Facility action is to reactivate the
     * facility.
     * <pre></pre>
     * Provenance:
     * <pre>
     * <li>FacilityDeactivateDate is set to null automatically.</li>
     * <li>FacilityActiveFlag is derived.</li>
     * </pre>
     *
     * @param uc       UserContext - Required
     * @param facility FacilityType - Required
     * @return FacilityType -- Throws exception if instance cannot be activated
     */
    @Override
    public Facility activateFacility(UserContext uc, Facility facility) {
        return facilityDAO.activate(uc, facility);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FacilitiesReturn search(UserContext uc, Set<Long> subsetSearch, FacilitySearch search, Long startIndex, Long resultSize, String resultOrder) {
        return facilityDAO.search(uc, subsetSearch, search, startIndex, resultSize, resultOrder);
    }

    /**
     * Count of all instances.
     * <pre>
     * <li>Throws exception if there is an error during the count.</li>
     * </pre>
     *
     * @param uc UserContext - Required
     * @return java.lang.Long
     */
    @Override
    public Long getCount(UserContext uc) {
        return facilityDAO.getCount(uc);
    }

    /**
     * Cascading delete of a single instance.
     * <pre>
     * <li>Returns an instance and success code if instance can be deleted.</li>
     * <li>Throws exception if instance cannot be deleted.</li>
     * </pre>
     *
     * @param uc         UserContext - Required
     * @param facilityId java.lang.Long - Required
     * @return Long - Throws exception if any error occurs
     */
    @Override
    public Long delete(UserContext uc, Long facilityId, Long personId) {
        validateDelete(uc, facilityId, personId);
        facilityDAO.delete(uc, facilityId);
        return ReturnCode.Success.returnCode();

    }

    private void validateDelete(UserContext uc, Long facilityId, Long personId) {
        validateChildFacilityAsCurrentUserFacility(uc, facilityId, personId);
        validateFacilityInUse(uc, facilityId, false);
    }

    private void validateChildFacilityAsCurrentUserFacility(UserContext uc, Long facilityId, Long personId) {

        UserPreferenceType userPreferenceType = userPreferenceService.retrieve(uc, personId);
        if(userPreferenceType == null){
            return;
        }
        Long userFacilityId = userPreferenceType.getFacilityId();

        if(facilityId.equals(userFacilityId)) {
            throw new ValidationException(ErrorCodes.FAC_CANNOT_BE_DELETED_SINCE_SAME_AS_LOGGED_IN_USER);
        }
    }

    /**
     * Cascading delete of all instances, references and reference data.
     * <pre>
     * <li>Return success code if all instances, references and reference data deleted.</li>
     * <li>Return error code is not all instances, references and reference data deleted.</li>
     * </pre>
     *
     * @param uc UserContext - Required
     * @return returnCode java.lang.Long 1 = Success 0=Failure
     */
    @Override
    public Long deleteAll(UserContext uc) {
        facilityDAO.deleteAll(uc);
        return SUCCESS;
    }

    /**
     * Return a set of all instance identifiers.
     * <pre>
     * <li>Throws exception if there is an error during the get all.</li>
     * </pre>
     *
     * @param uc UserContext - Required
     * @return Set<java.lang.Long>
     */
    @Override
    public Set<Long> getAll(UserContext uc) {
        return facilityDAO.getAll(uc);
    }

    /**
     * Return the stamp of a single instance.
     * <pre>
     * <li>Throws exception if reference cannot be created.</li>
     * </pre>
     *
     * @param uc         UserContext - Required
     * @param facilityId Long - Required
     * @return StampType
     */
    @Override
    public StampType getStamp(UserContext uc, Long facilityId) {

        StampType ret = null;

        // Validation
        if (BeanHelper.isEmpty(facilityId)) {
            return ret;
        }

        //
        try {
            ret = facilityDAO.getStamp(uc, facilityId);
        } catch (Exception ex) {
            ExceptionHelper.handleException(log, ex);
        }

        //
        return ret;
    }

    /**
     * The purpose of the Retrieve NIEM Output is to retrieve NIEM output based
     * on a NIEM subset.
     * <pre></pre>
     * Business Needs Satisfied:
     * <pre>
     * <li>1.1.11.1.2 System shall utilize National Information Exchange Mode (NIEM) for data structures in order to exchange data.</li>
     * </pre>
     *
     * @param uc         UserContext - Required
     * @param facilityId Long - Required
     * @return String
     * <pre>
     * <li>Throws exception in case of exception</li>
     * </pre>
     */
    @Override
    public String getNiem(UserContext uc, Long facilityId) {
        return facilityDAO.getNiem(uc, facilityId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getVersion(UserContext uc) {
        return "2.1";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Location addLocation(UserContext uc, Location location) {
        return facilityDAO.addLocation(uc, location);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long deleteLocation(UserContext uc, Long facilityId, Long locationId) {
        facilityDAO.deleteLocation(uc, facilityId, locationId);
        return ReturnCode.Success.returnCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Location updateLocation(UserContext uc, Location location) {
        return facilityDAO.updateLocation(uc, location);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Location getLocation(UserContext uc, Long locationId) {
        return facilityDAO.getLocation(uc, locationId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Location> getLocations(UserContext uc, Long facilityId) {
        return facilityDAO.getLocations(uc, facilityId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Long> getAllAssociatedFacilityToOrganization(UserContext userContext) {
        ValidationHelper.validate(userContext);
        return facilityDAO.getAllAssociatedFacilityToOrganization();
    }

    /**
     * {@inheritDoc}
     */
    public Boolean isDuplicateFacility(UserContext uc, Long facilityId, String facilityCode) {
        boolean isDuplicateFacility = facilityDAO.isDuplicateFacility(uc, facilityId, facilityCode);

        if(isDuplicateFacility) {
            throw new DataExistException(ErrorCodes.FAC_FACILITY_CODE_ALREADY_EXISTS);
        }

        return isDuplicateFacility;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void isDeactivationDateInFuture(UserContext uc, Facility facility) {

        if(facility.getFacilityDeactivateDate()==null) {
            return;
        }
        
        if(facility.getFacilityDeactivateDate().after(new Date())) {
            throw new InvalidInputException(ErrorCodes.FAC_FACILITY_CANNOT_HAVE_DEACTIVATION_DATE_IN_FUTURE);
        }
    }

    private void validateFacilityHasParentOrganization(UserContext uc, Facility facility) {
        
        if(facility.getParentOrganizationId()==null) {
            throw new DataExistException(ErrorCodes.FAC_FACILITY_PARENT_ORGANIZATION_REQUIRED);
        }
    }
    
    private void validateCreateFacilityWhenOrganizationIsDeactive(UserContext uc, Facility facility) {
        
        Organization organization = organizationService.get(uc, facility.getParentOrganizationId());
        if(organization.getOrganizationTerminationDate()!=null) {
            throw new DataExistException(ErrorCodes.FAC_CANNOT_BE_CREATED_IF_ORGANIZATION_IS_DEACTIVE);
        }
        
    }
    
    private boolean hasConfiguredFacilityInternalLocations(UserContext uc, Long facilityId) {
        return facilityDAO.hasConfiguredFacilityInternalLocations(uc, facilityId);
    }
    
    private boolean hasActiveFacilityInternalLocations(UserContext uc, Long facilityId) {
        return facilityDAO.hasActiveFacilityInternalLocations(uc, facilityId);
    }

    private boolean hasActiveAssociatedOrganizationFacility(UserContext uc, Long facilityId) {
        return facilityDAO.hasActiveAssociatedOrganizationFacility(uc, facilityId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFacility(UserContext uc, Long facilityId) {
        validateFacilityInUse(uc, facilityId, false);
        facilityDAO.delete(uc, facilityId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addAssociatedOrganization(UserContext uc, Long facilityId, Long organizatinId) {
        facilityDAO.addAssociatedOrganization(uc, facilityId, organizatinId);
    }

    private void validateFacilityInUse(UserContext uc, Long facilityId, boolean isDeactivation) {
        
        //deactivation
        if(isDeactivation) {
            
            //must not have active FIL
            if(hasActiveFacilityInternalLocations(uc, facilityId)) {
                throw new InvalidInputException(ErrorCodes.FAC_CANNOT_BE_DEACTIVATED_DUE_TO_ACTIVE_FIL);
            }
            
            //must not have active associated organization
            if(hasActiveAssociatedOrganizationFacility(uc, facilityId)) {
                throw new InvalidInputException(ErrorCodes.FAC_CANNOT_BE_DEACTIVATED_DUE_TO_ACTIVE_ASSOCIATED_ORG);
            }
        }
        //deletion
        else {
            
            //must not have active FIL
            if(hasConfiguredFacilityInternalLocations(uc, facilityId)) {
                throw new InvalidInputException(ErrorCodes.FAC_CANNOT_BE_DELETED_DUE_TO_CONFIGURED_FIL);
            }
            
            //must not have active associated organization
            if(hasActiveAssociatedOrganizationFacility(uc, facilityId)) {
                throw new InvalidInputException(ErrorCodes.FAC_CANNOT_BE_DELETED_DUE_TO_ACTIVE_ASSOCIATED_ORG);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public List<Facility> getFacilitiesForLocationConfiguration(UserContext uc, Long facilitySetId) {

        List<Facility> ret = new ArrayList<Facility>();

        // Retrieve login user id
        String loginId = BeanHelper.getUserId(uc, context);
        if (loginId == null) {
            return ret;
        }

        // Retrieve ids of allowed facilities
        List<Long> ids = securityService.getAllAllowedFacilityIds(uc, loginId, facilitySetId);
        if (ids == null || ids.isEmpty()) {
            return ret;
        }

        //
        List<Facility> facilityList =  facilityDAO.getFacilitiesByLocationConfiguration(uc);
        if (facilityList == null) {
            return ret;
        }
        for (Facility facility : facilityList) {
            if (ids.contains(facility.getFacilityIdentification())) {
                ret.add(facility);
            }
        }

        //
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    public List<Facility> getFacilitiesForLocation(UserContext uc, Long facilitySetId) {

        List<Facility> ret = new ArrayList<Facility>();

        // Retrieve login user id
        String loginId = BeanHelper.getUserId(uc, context);
        if (loginId == null) {
            return ret;
        }

        // Retrieve ids of allowed facilities
        List<Long> ids = securityService.getAllAllowedFacilityIds(uc, loginId, facilitySetId);
        if (ids == null || ids.isEmpty()) {
            return ret;
        }

        //
        List<Facility> facilityList =  facilityDAO.getFacilitiesByLocationConfiguration(uc);
        if (facilityList == null) {
            return ret;
        }
        for (Facility facility : facilityList) {
            if (ids.contains(facility.getFacilityIdentification())) {
                ret.add(facility);
            }
        }

        //
        return ret;
    }
}
