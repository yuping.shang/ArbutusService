package syscon.arbutus.product.services.facility.realization.persistence;

/**
 * The references codes of this service.
 */
public class MetaSet {

    public static final String FACILITY_CATEGORY = "FacilityCategory";
    public static final String FACILITY_TYPE = "FacilityType";

}
