package syscon.arbutus.product.services.facility.contract.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import syscon.arbutus.product.services.core.contract.dto.BaseDto;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;

/**
 * The representation of the FacilityType.
 *
 * @author yshang, updated by jliang
 * @version 2.0 (Based on SDD 2.0)
 * @since April 16, 2012
 */
public class Facility extends BaseDto implements Serializable {

    private static final long serialVersionUID = -8881088052926177197L;

    private Long facilityIdentification;
    @NotNull
    @Size(min=1, max = 64)
    private String facilityCode;
    private Set<Long> organizations;
    @NotNull
    @Size(min=1, max = 64)
    private String facilityName;
    @NotNull
    @Pattern(regexp = "^[A-Za-z0-9_-]+$")
    private String facilityCategoryText;    //ReferenceSet(FacilityCategory)
//    @Pattern(regexp = "^[A-Za-z0-9_-]+$")
    private String facilityType;            //ReferenceSet(FacilityType)
    private Date facilityDeactivateDate;

    private Date  facilityCreateDate;

    private Long facilityContactId;

    /**
     * Parent Organization ID of the this facility
     */
    private Long parentOrganizationId;

    private boolean internalLocationRequired;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(name = "Flag", nullable = false)
    private Long flag = DataFlag.ACTIVE.value();
    
    public Facility() {

    }

    /**
     * @param facilityIdentification Long - Required when update, Ignored when create. An identifier assigned to a facility.
     * @param facilityCode           String - Required. A code used to identify the facility.
     * @param facilityName           String - Required. A name of a facility.
     * @param facilityDeactivateDate Date - Optional. The date the facility was deactivated.
     * @param facilityCategoryText   String - Required. The general category of facility.
     * @param facilityType           String - Optional. The type of facility which is a further refinement of the category.
     * @param parentOrganizationId   Long - required. Parent Id of this Facility.
     * @param organizations          Long - Optional. A reference to the Organization that runs the facility.
     * @param facilityContactId      Long - Optional. facility Contact Id  of this Facility.
     * @param internalLocationRequired boolean - Flag to indicate whether internal location need to be configured.
     */
    public Facility(Long facilityIdentification, String facilityCode, String facilityName, Date facilityDeactivateDate, String facilityCategoryText,
            String facilityType, Long parentOrganizationId, Set<Long> organizations, Long facilityContactId, Boolean internalLocationRequired) {
        this.facilityIdentification = facilityIdentification;
        this.facilityCode = facilityCode;
        this.facilityName = facilityName;
        this.facilityDeactivateDate = facilityDeactivateDate;
        this.facilityCategoryText = facilityCategoryText;
        this.facilityType = facilityType;
        this.parentOrganizationId = parentOrganizationId;
        this.organizations = organizations;
        this.facilityContactId = facilityContactId;
        this.internalLocationRequired = internalLocationRequired == null ? false : internalLocationRequired;
    }

    /**
     * Gets the value of the facilityIdentification property.
     *
     * @return the facilityIdentification -- an identifier assigned to a
     * facility
     */
    public Long getFacilityIdentification() {
        return facilityIdentification;
    }

    /**
     * Sets the value of the facilityIdentification property.
     *
     * @param facilityIdentification the facilityIdentification to set -- an identifier assigned to
     *                               a facility
     */
    public void setFacilityIdentification(Long facilityIdentification) {
        this.facilityIdentification = facilityIdentification;
    }

    /**
     * Gets the value of the facilityCode property.
     *
     * @return the facilityCode -- a code used to identify the facility
     */
    public String getFacilityCode() {
        return facilityCode;
    }

    /**
     * Sets the value of the facilityCode property.
     *
     * @param facilityCode the facilityCode to set -- a code used to identify the
     *                     facility
     */
    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode == null ? null : facilityCode.trim();
    }

    /**
     * Gets the value of the organizations property.
     *
     * @return the organizations -- an reference to the Organization that
     * runs the facility
     */
    public Set<Long> getOrganizations() {
        return organizations;
    }

    /**
     * Sets the value of the organizations property.
     *
     * @param organizations the organizations to set -- an reference to the
     *                      Organization that runs the facility
     */
    public void setOrganizations(Set<Long> organizations) {
        this.organizations = organizations;
    }

    /**
     * Gets the value of the facilityName property.
     *
     * @return the facilityName -- a name of a facility
     */
    public String getFacilityName() {
        return facilityName;
    }

    /**
     * Sets the value of the facilityName property.
     *
     * @param facilityName the facilityName to set -- a name of a facility
     */
    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName == null ? null : facilityName.trim();
    }

    public Date getFacilityCreateDate() {
        return facilityCreateDate;
    }

    public void setFacilityCreateDate(Date facilityCreateDate) {
        this.facilityCreateDate = facilityCreateDate;
    }

    /**
     * Gets the value of facilityContactId
     *
     * @return facilityContactId - that facility Contact Id ,It is person Idetinty Id of Contact Person
     */
    public Long getFacilityContactId() {
        return facilityContactId;
    }

    /**
     * Sets the value of the facilityContactId
     *
     * @param facilityContactId - Facility Contact Id of this Facility
     */
    public void setFacilityContactId(Long facilityContactId) {
        this.facilityContactId = facilityContactId;
    }

    /**
     * Gets the value of the facilityDeactivateDate property.
     *
     * @return the facilityDeactivateDate -- the date the facility was
     * deactivated
     */
    public Date getFacilityDeactivateDate() {
        return facilityDeactivateDate;
    }

    /**
     * Sets the value of the facilityDeactivateDate property.
     *
     * @param facilityDeactivateDate the facilityDeactivateDate to set -- the date the facility was
     *                               deactivated
     */
    public void setFacilityDeactivateDate(Date facilityDeactivateDate) {
        this.facilityDeactivateDate = facilityDeactivateDate;
    }

    /**
     * Gets the value of the facilityCategoryText property.
     *
     * @return the facilityCategoryText -- the type of facility. Custody,
     * Community, Court, Police or General are the only valid values for
     * this element
     */
    public String getFacilityCategoryText() {
        return facilityCategoryText;
    }

    /**
     * Sets the value of the facilityCategoryText property.
     *
     * @param facilityCategoryText the facilityCategoryText to set -- the type of facility.
     *                             Custody, Community, Court, Police or General are the only
     *                             valid values for this element
     */
    public void setFacilityCategoryText(String facilityCategoryText) {
        this.facilityCategoryText = facilityCategoryText;
    }

    /**
     * Gets the value of the facilityType property.
     */
    public String getFacilityType() {
        return facilityType;
    }

    /**
     * Sets the value of the facilityType property.
     */
    public void setFacilityType(String facilityType) {
        this.facilityType = facilityType;
    }

    /**
     * Parent Organization ID of the this facility, Optional
     * <p>A facility has at most one parent organization Id
     *
     * @return Long
     */
    public Long getParentOrganizationId() {
        return parentOrganizationId;
    }

    /**
     * Parent Organization ID of the this facility, Optional
     * <p>A facility has at most one parent organization Id
     *
     * @param parentOrganizationId
     */
    public void setParentOrganizationId(Long parentOrganizationId) {
        this.parentOrganizationId = parentOrganizationId;
    }

    public boolean isInternalLocationRequired() {
        return internalLocationRequired;
    }

    public void setInternalLocationRequired(boolean internalLocationRequired) {
        this.internalLocationRequired = internalLocationRequired;
    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }
    
    @Override
    public String toString() {
        return "FacilityType [facilityIdentification=" + facilityIdentification + ", facilityCode=" + facilityCode + ", organizations=" + organizations
                + ", facilityName=" + facilityName + ", facilityCategoryText=" + facilityCategoryText + ", facilityType=" + facilityType + ", facilityDeactivateDate="
                + facilityDeactivateDate + ", parentOrganizationId=" + parentOrganizationId + ", facilityContactId=" + facilityContactId + ", parentOrganizationId=" + parentOrganizationId + "]";
    }
}
