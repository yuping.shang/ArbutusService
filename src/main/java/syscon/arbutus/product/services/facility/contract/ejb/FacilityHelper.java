package syscon.arbutus.product.services.facility.contract.ejb;

import java.util.*;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.realization.persistence.FacilityEntity;
import syscon.arbutus.product.services.organization.realization.persistence.OrganizationEntity;

public class FacilityHelper {

    /**
     * @param facility, Facility Type
     * @param session,  Session
     * @return FacilityEntity
     */
    public static FacilityEntity toFacilityEntity(Facility facility, Session session) {
        if (facility == null) {
			return null;
		}
        Set<OrganizationEntity> organizations = new HashSet<OrganizationEntity>();
        if (facility.getOrganizations() != null) {
            for (Long orgId : facility.getOrganizations()) {
                OrganizationEntity orgEntity = (OrganizationEntity) session.get(OrganizationEntity.class, orgId);
                if (orgEntity == null) {
                    String message = String.format("The organization does not exist orgId= %s", orgId);
                    throw new DataNotExistException(message);
                }
                organizations.add(orgEntity);
            }
        }

        FacilityEntity ret = new FacilityEntity(facility.getFacilityIdentification(), facility.getFacilityCode(), facility.getFacilityName(),
                facility.getFacilityDeactivateDate(), facility.getFacilityCategoryText(), facility.getFacilityType(), organizations,
                facility.getFacilityContactId(), facility.isInternalLocationRequired());


        Long parentOrgId = facility.getParentOrganizationId();
        if(parentOrgId!=null){
            OrganizationEntity organizationEntity = (OrganizationEntity)session.get(OrganizationEntity.class, parentOrgId);
            ret.setParentOrganization(organizationEntity);

        }

        return ret;
    }

    /**
     * @param facility, Facility Entity
     * @return FacilityType
     */
    public static Facility toFacilityType(FacilityEntity facility) {
        if (facility == null) {
			return null;
		}

        Set<Long> organizations = new HashSet<Long>();

        Set<OrganizationEntity> orgs = facility.getOrganizations();
        if (orgs != null) {
            Iterator<OrganizationEntity> itg = facility.getOrganizations().iterator();
            while (itg.hasNext()) {
                OrganizationEntity orgEntity = (OrganizationEntity) itg.next();
                organizations.add(orgEntity.getId());
            }
        }


        Long parentId = null;
        if(facility.getParentOrganization()!=null){
            parentId = facility.getParentOrganization().getId();

        }

        Facility ret = new Facility(facility.getFacilityId(), facility.getFacilityCode(), facility.getFacilityName(), facility.getFacilityDeactiveDate(),
                facility.getFacilityCategoryText(), facility.getFacilityType(), parentId,organizations, facility.getFacilityContactId(), facility.isInternalLocationRequired());
        ret.setStamp(facility.getStamp());
        ret.setVersion(facility.getVersion());

        return ret;
    }

    /**
     * @param facilities Set<FacilityEntity>, Set of Facility Entities
     * @return Set<FacilityEntity>
     */
    public static Set<Facility> toFacilityTypeSet(Set<FacilityEntity> facilities) {
        Set<Facility> ret = new HashSet<Facility>();

        if (facilities == null) {
			return ret;
		}

        Iterator<FacilityEntity> it = facilities.iterator();
        while (it.hasNext()) {
            ret.add(toFacilityType(it.next()));
        }

        return ret;
    }

    /**
     * @param facility, FacilityEntity
     * @return FacilityType
     */
    public static Facility toFacility(FacilityEntity facility) {
        if (facility == null) {
			return null;
		}

        Set<Long> organizations = new HashSet<Long>();

        Hibernate.initialize(facility.getOrganizations());
        Set<OrganizationEntity> orgs = facility.getOrganizations();
        if (orgs != null) {
            Iterator<OrganizationEntity> itg = facility.getOrganizations().iterator();
            while (itg.hasNext()) {
                OrganizationEntity orgEntity = (OrganizationEntity) itg.next();
                organizations.add(orgEntity.getId());
            }
        }

        Long parentId = null;
        if(facility.getParentOrganization()!=null){
            parentId = facility.getParentOrganization().getId();

        }

        Facility ret = new Facility(facility.getFacilityId(), facility.getFacilityCode(), facility.getFacilityName(), facility.getFacilityDeactiveDate(),
                facility.getFacilityCategoryText(), facility.getFacilityType(), parentId, organizations, facility.getFacilityContactId(), facility.isInternalLocationRequired());
        ret.setStamp(facility.getStamp());
        ret.setVersion(facility.getVersion());
        ret.setFlag(facility.getFlag());
        
        return ret;
    }

    /**
     * @param facilitys, List of FacilityEntity
     * @return Set<FacilityType>
     */
    @SuppressWarnings("rawtypes")
    public static Set<Facility> toFacilitySet(List facilitys) {

        Set<Facility> ret = new HashSet<Facility>();

        if (facilitys == null) {
			return ret;
		}

        Iterator it = facilitys.iterator();
        while (it.hasNext()) {
            FacilityEntity facility = (FacilityEntity) it.next();
            ret.add(toFacility(facility));
        }

        return ret;
    }

    /**
     *
     * @param facilityEntityList
     * @return
     */
    public static List<Facility> toFacilityList(List<FacilityEntity> facilityEntityList) {

        List<Facility> ret = new ArrayList<Facility>();

        if (facilityEntityList == null) {
            return ret;
        }

        Iterator it = facilityEntityList.iterator();
        while (it.hasNext()) {
            FacilityEntity facility = (FacilityEntity) it.next();
            ret.add(toFacility(facility));
        }

        return ret;
    }
}
