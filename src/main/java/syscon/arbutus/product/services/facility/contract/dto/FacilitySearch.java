package syscon.arbutus.product.services.facility.contract.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.SearchSetMode;

/**
 * The representation of the FacilitySearchType.
 *
 * @author yshang, updated by jliang
 * @version 2.0 (Based on SDD 2.0)
 * @since April 16, 2012
 *
 */
public class FacilitySearch implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -639492501494127678L;
    private String facilityCode;
    private String facilityName;
    private Set<Long> organizations;
    private Long parentId;
    private String facilityCategoryText;   //ReferenceSet(FacilityCategory)
    private String facilityType;           //ReferenceSet(FacilityType)
    private Boolean facilityActiveFlag;
    private SearchSetMode setMode;
    private Long facilityContactId;
    private Date createdDateFrom;
    private Date createdDateTo;
    private Date deactivatedDateFrom;
    private Date deactivatedDatTo;
    //Address Search Fields
    private String streetNumber;
    private String streetName;
    private String city;
    private String provience;
    private String country;





    public FacilitySearch() {
    }

    /**
     * At Least one of the parameter should be not null
     *  @param organizationAssociation    a reference to the Organization that runs the facility - Optional.
     * @param facilityContactAssociation the Person who is the contact for the facility. Association to
     *                                   the Person Service - Optional.
     * @param facilityCode               a code to identify a facility - Optional.
     * @param facilityName               a name of a facility - Optional.
     * @param facilityCategoryText       the facilityCategoryText to set -- the type of facility.
*                                   Custody, Community, Court, Police or General are the only
*                                   valid values for this element - Optional.
     * @param facilityType               - Optional.
     * @param facilityActiveFlag         The status of the facility (Active or Inactive) - Optional.
     * @param createdDateFrom
     * @param createdDateTo
     * @param deactivatedDateFrom
     * @param deactivatedDatTo
     */
    public FacilitySearch(String facilityCode, String facilityName, SearchSetMode setMode, String facilityCategoryText, String facilityType,
                          Boolean facilityActiveFlag, Long parentId, Set<Long> organizations, Long facilityContactId, Date createdDateFrom, Date createdDateTo, Date deactivatedDateFrom, Date deactivatedDatTo) {
        this.facilityCode = trimStar(facilityCode);
        this.facilityName = trimStar(facilityName);
        this.setMode = setMode;
        this.organizations = organizations;
        this.parentId = parentId;
        this.facilityCategoryText = facilityCategoryText;
        this.facilityType = facilityType;
        this.facilityActiveFlag = facilityActiveFlag;
        this.facilityContactId = facilityContactId;
        this.createdDateFrom = createdDateFrom;
        this.createdDateTo = createdDateTo;
        this.deactivatedDateFrom = deactivatedDateFrom;
        this.deactivatedDatTo = deactivatedDatTo;
    }

    /**
     * Gets the value of the facilityCode property.
     *
     * @return facilityCode - String
     */
    public String getFacilityCode() {
        return facilityCode;
    }

    /**
     * Sets the value of the facilityCode property.
     *
     * @param facilityCode - The facilityCode to set
     */
    public void setFacilityCode(String facilityCode) {
        this.facilityCode = trimStar(facilityCode);
    }

    /**
     * Gets the value of the facilityName property.
     *
     * @return facilityName - A name of a facility
     */
    public String getFacilityName() {
        return facilityName;
    }

    /**
     * Sets the value of the facilityName property.
     *
     * @param facilityName the facilityName to set - a name of a facility
     */
    public void setFacilityName(String facilityName) {
        this.facilityName = trimStar(facilityName);
    }

    /**
     * Gets the value of the organizationAssociation property.
     *
     * @return organizationAssociation - an association to the Organization that runs the facility
     */
    public Set<Long> getOrganization() {
        return organizations;
    }

    /**
     * Sets the value of the organization property.
     *
     * @param organization the organization to set - a reference to the
     *                     Organizations that runs the facility
     */
    public void setOrganizations(Set<Long> organizations) {
        this.organizations = organizations;
    }

    /**
     * Gets the value of the facilityCategoryText property.
     *
     * @return facilityCategoryText - the type of facility. Custody,
     * Community, Court, Police or General are the only valid values for
     * this element
     */
    public String getFacilityCategoryText() {
        return facilityCategoryText;
    }

    /**
     * Sets the value of the facilityCategoryText property.
     *
     * @param facilityCategoryText the facilityCategoryText to set -- the type of facility.
     *                             Custody, Community, Court, Police or General are the only
     *                             valid values for this element
     */
    public void setFacilityCategoryText(String facilityCategoryText) {
        this.facilityCategoryText = facilityCategoryText;
    }

    /**
     * Gets the value of the facilityType property.
     */
    public String getFacilityType() {
        return facilityType;
    }

    /**
     * Sets the value of the facilityType property.
     */
    public void setFacilityType(String facilityType) {
        this.facilityType = facilityType;
    }

    /**
     * Gets the status of the facility (Active or Inactive)
     * <pre></pre>
     * FacilityActiveFlag is system generated based on
     * <pre></pre>
     * FacilityCreateDate / FacilityDeactivateDate
     * <pre>
     * <li>If create date < = system date and expiration date is null, status = Active</li>
     * <li>If create date < = system date and expiration date is > system date, status = Active</li>
     * <li>If create date > system date, status = Inactive</li>
     * <li>If expiration date < = system date, status = Inactive</li>
     * </pre>
     *
     * @return the facilityActiveFlag Boolean
     */
    public Boolean getFacilityActiveFlag() {
        return facilityActiveFlag;
    }

    /**
     * Sets the status of the facility (Active or Inactive)
     * <pre></pre>
     * FacilityActiveFlag is system generated based on
     * <pre></pre>
     * FacilityCreateDate / FacilityDeactivateDate
     * <pre>
     * <li>If create date < = system date and expiration date is null, status = Active</li>
     * <li>If create date < = system date and expiration date is > system date, status = Active</li>
     * <li>If create date > system date, status = Inactive</li>
     * <li>If expiration date < = system date, status = Inactive</li>
     * </pre>
     *
     * @param facilityActiveFlag Boolean
     *                           the facilityActiveFlag to set
     */
    public void setFacilityActiveFlag(Boolean facilityActiveFlag) {
        this.facilityActiveFlag = facilityActiveFlag;
    }

    /**
     * Gets the value of parent facility
     *
     * @return parentId Long - that parent facility
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * Sets the value of the parentId
     *
     * @param parentId - Parent Facility of this Facility
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * Gets the value of facilityContactId
     *
     * @return facilityContactId - that facility Contact Id ,It is person Idetinty Id of Contact Person
     */
    public Long getFacilityContactId() {
        return facilityContactId;
    }

    /**
     * Sets the value of the facilityContactId
     *
     * @param facilityContactId - Facility Contact Id of this Facility
     */
    public void setFacilityContactId(Long facilityContactId) {
        this.facilityContactId = facilityContactId;
    }

    /**
     * Get the set mode
     *
     * @return SearchSetMode
     */
    public SearchSetMode getSetMode() {
        return setMode;
    }

    /**
     * Set the set mode
     *
     * @param setMode SearchSetMode
     */
    public void setSetMode(SearchSetMode setMode) {
        this.setMode = setMode;
    }

    public Date getCreatedDateFrom() {
        return createdDateFrom;
    }

    public void setCreatedDateFrom(Date createdDateFrom) {
        this.createdDateFrom = createdDateFrom;
    }

    public Date getCreatedDateTo() {
        return createdDateTo;
    }

    public void setCreatedDateTo(Date createdDateTo) {
        this.createdDateTo = createdDateTo;
    }

    public Date getDeactivatedDateFrom() {
        return deactivatedDateFrom;
    }

    public void setDeactivatedDateFrom(Date deactivatedDateFrom) {
        this.deactivatedDateFrom = deactivatedDateFrom;
    }

    public Date getDeactivatedDatTo() {
        return deactivatedDatTo;
    }

    public void setDeactivatedDatTo(Date deactivatedDatTo) {
        this.deactivatedDatTo = deactivatedDatTo;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvience() {
        return provience;
    }

    public void setProvience(String provience) {
        this.provience = provience;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Removes the [] from the search parameter
     *
     * @return SearchSetMode
     */
    private String trimStar(String str) {
        if (str != null) {
            str = str.replaceAll("[*]+", "*");
            if (str.equals("*")) {
                return null;
            }
        }

        return str;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "FacilitySearchType [facilityCode=" + facilityCode + ", facilityName=" + facilityName + ", organizations=" + organizations + ", parentId=" + parentId
                + ", facilityCategoryText=" + facilityCategoryText + ", facilityType=" + facilityType + ", facilityActiveFlag=" + facilityActiveFlag
                + ", createdDateFrom=" + createdDateFrom + ", CreatedDateTo=" + createdDateTo + ",deactivatedDateFrom=" + deactivatedDateFrom + ", deactivatedDateTo="
                + deactivatedDatTo + ", streetNumber=" + streetNumber + ", streetName=" + streetName + ", city=" + city + ", state=" + provience + ", county=" + country
                + "]";
    }

}