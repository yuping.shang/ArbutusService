package syscon.arbutus.product.services.facility.contract.dto;

import java.io.Serializable;
import java.util.Set;

/**
 * The representation of the FacilitiesReturnType.
 *
 * @author yshang
 * @version 2.0 (Based on SDD 2.0)
 * @since April 16, 2012
 */
public class FacilitiesReturn implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5810481335434179700L;
    private Set<Facility> facilities;
    private Long totalSize;

    /**
     * Gets the value of the facilities property.
     *
     * @return the facilities -- the set of FacilityType
     */
    public Set<Facility> getFacilities() {
        return facilities;
    }

    /**
     * Sets the value of the facilities property.
     *
     * @param facilities the facilities to set -- the set of FacilityType
     */
    public void setFacilities(Set<Facility> facilities) {
        this.facilities = facilities;
    }

    /**
     * Gets the value of the count property.
     *
     * @return the count -- the size of the set of FacilityType
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * Sets the value of the count property.
     *
     * @param count the count to set -- the size of the set of FacilityType
     */
    public void setTotalSize(Long count) {
        this.totalSize = count;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "FacilitiesReturnType [facilities=" + facilities + ", totalSize=" + totalSize + "]";
    }

}
