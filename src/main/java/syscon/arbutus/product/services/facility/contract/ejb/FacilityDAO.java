package syscon.arbutus.product.services.facility.contract.ejb;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.SessionContext;

import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.StaleObjectStateException;
import org.hibernate.criterion.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.common.adapters.LocationServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.core.contract.dto.SearchSetMode;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.ArbutusOptimisticLockException;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.ErrorCodes;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facility.contract.dto.FacilitiesReturn;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.dto.FacilitySearch;
import syscon.arbutus.product.services.facility.realization.persistence.FacilityEntity;
import syscon.arbutus.product.services.core.util.StringUtils;
import syscon.arbutus.product.services.facilityinternallocation.realization.persistence.InternalLocationEntity;
import syscon.arbutus.product.services.location.contract.dto.AddressCategory;
import syscon.arbutus.product.services.location.contract.dto.Location;
import syscon.arbutus.product.services.location.realization.persistence.LocationEntity;
import syscon.arbutus.product.services.organization.contract.dto.Organization;
import syscon.arbutus.product.services.organization.contract.ejb.OrganizationHelper;
import syscon.arbutus.product.services.organization.realization.persistence.OrganizationEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * The Facility service is used to uniquely-identify, correlate and aggregate
 * facility information used by governmental, corporate, business or
 * international bodies (including businesses) to eliminates redundant data
 * entry by facilitating reuse of previously stored information and automatic
 * update upon entry of new information. A FACILITY may be operated by one or
 * more ORGANIZATION entities and be associated with PERSONS.
 * <pre></pre>
 * Types of FACILITY include:
 * <pre>
 * 	<li>Custody Facilities: Jails or Prisons, probation / parole offices); police / sheriff departments; courts.</li>
 * 	<li>Community Facilities: Probation or Parole Offices.</li>
 * 	<li>General Facilites: Police Stations, Hospitals, Courts etc.</li>
 * </pre>
 * Scope:
 * <pre></pre>
 * The scope of the Facility Service is limited to creation and maintenance of
 * the facility, as well as the maintenance of the reference data used for the
 * service:
 * <pre>
 * 	<li>Create facilities;</li>
 *  <li>Update facilities;</li>
 *  <li>De-activate facilities;</li>
 *  <li>Re-activate facilities;</li>
 * </pre>
 * The service will provide the ability for data exchange through NIEM. The
 * Facility Service will have references between persons/organization/locations
 * and a facility e.g. Staff member, inmate, county jail etc. The persons and
 * organizations that are associated to each facility are created in separate
 * services.
 *
 * @author yshang, updated by jliang
 * @version 2.0 (Based on SDD 2.0)
 * @since April 16, 2012
 */

public class FacilityDAO {

    private static Logger log = LoggerFactory.getLogger(FacilityDAO.class);

    private static String INSTANCE_ID = "facilityId";

    private int searchMaxLimit = 200;

    private SessionContext context;
    private Session session;

    public FacilityDAO(SessionContext context, Session session) {
        super();
        this.context = context;
        this.session = session;
    }

    /**
     * The Create Facility action allows for the creation of a new facility in
     * the system.
     * <pre></pre>
     * Business Needs Satisfied:
     * <pre>
     * <li>1.2.1.2.1-2 Add new facility</li>
     * <li>1.2.1.2.1-5 Designate justice agency location</li>
     * <li>1.2.1.2.1-6 Designate non-justice agency location</li>
     * <li>Designate program provider service location</li>
     * </pre>
     * Provenance:
     * <pre>
     * <li>FacilityIdentification is system generated</li>
     * <li>FacilityActiveFlag is derived</li>
     * <li>All other elements are manually entered by the user</li>
     * </pre>
     *
     * @param uc       UserContext- Required
     * @param facility Facility- Required
     * @return Facility -- Throws exception if instance cannot be
     * created
     */
    public Facility create(UserContext uc, Facility facility) {

        // Validation
        ValidationHelper.validate(facility);

        // Convert to Entity
        FacilityEntity facilityEntity = FacilityHelper.toFacilityEntity(facility, session);
        ValidationHelper.verifyMetaCodes(facilityEntity, true);

        verifyExistence(facility);

        Long id = facility.getFacilityIdentification();
        if (id == null || id < 1) {
            id = generateId();
            facilityEntity.setFacilityId(id);
        } else {
            FacilityEntity entity = (FacilityEntity) session.get(FacilityEntity.class, id);
            if (entity != null) {
                id = generateId();
            }
            facilityEntity.setFacilityId(id);
        }

        facilityEntity.setStamp(facility.getStamp());
        facilityEntity.setVersion(facility.getVersion());

        // Save facility
        session.save(facilityEntity);

        // Flush changes to db
        session.flush();
        session.clear();

        facilityEntity = BeanHelper.getEntity(session, FacilityEntity.class, facilityEntity.getFacilityId());
        return FacilityHelper.toFacilityType(facilityEntity);
    }

    /**
     * The Update Facility action allows for the update of an existing facility
     * in the system.
     * <pre></pre>
     * Business Needs Satisfied:
     * <pre>
     * <li>1.2.1.2.1-3 Update facility</li>
     * </pre>
     * Provenance:
     * <pre>
     * <li>FacilityName, FacilityLocation, FacilityContactInformation, and FacilityCategoryText are the only updateable elements.</li>
     * </pre>
     *
     * @param uc       UserContext- Required
     * @param facility Facility- Required
     * @return Facility -- Throws exception if instance cannot be
     * updated
     */
    public Facility update(UserContext uc, Facility facility) {

        ValidationHelper.validate(facility);

        if (facility.getFacilityIdentification() == null) {
            throw new InvalidInputException("facilityId or deactivateDate is null");
        }

        FacilityEntity facilityEntity = FacilityHelper.toFacilityEntity(facility, session);
        ValidationHelper.verifyMetaCodes(facilityEntity, false);

        Long facilityId = facilityEntity.getFacilityId();
        //Get existing FacilityEntity
        FacilityEntity oldFacilityEntity = getFacility(facilityId);

        copyProperties(facilityEntity, oldFacilityEntity);

        oldFacilityEntity.setStamp(facility.getStamp());
        oldFacilityEntity.setVersion(facility.getVersion());
        try {
            session.clear();
            session.update(oldFacilityEntity);
            session.flush();
        } catch (StaleObjectStateException ex) {
            log.info("A concurrent issue happen!");
            throw new ArbutusOptimisticLockException(ex);
        }

        Facility ret = FacilityHelper.toFacilityType(oldFacilityEntity);
        return ret;

    }

    /**
     * To add association between Organization and facility
     * @param uc
     * @param facilityId
     * @param organizatinId
     */
  public void addAssociatedOrganization(UserContext uc, Long facilityId, Long organizatinId){

      if(BeanHelper.isEmpty(facilityId) || BeanHelper.isEmpty(organizatinId)){
          throw new InvalidInputException("faciliytId or organizationId is NULL");
      }

      FacilityEntity facilityEntity = getFacility(facilityId);


      OrganizationEntity orgEntity = (OrganizationEntity) session.get(OrganizationEntity.class, organizatinId);
      if (orgEntity == null) {
          String message = String.format("The organization does not exist orgId= %s", organizatinId);
          throw new DataNotExistException(message);
      }
      facilityEntity.getOrganizations().add(orgEntity);
      session.update(facilityEntity);

  }

    /**
     * Retrieve a single instance of Facility by using the facilityId.
     * <pre></pre>
     * Returns a null instance and error code in return type if incorrect
     * instanceId is provided as parameter.
     *
     * @param uc         UserContext- Required
     * @param facilityId java.lang.Long- Required
     * @return Facility -- Throws exception if instance cannot be got
     */
    public Facility get(UserContext uc, Long facilityId) {

        if (BeanHelper.isEmpty(facilityId)) {
            throw new InvalidInputException("facilityId is null");
        }

        // Get by facilityId
        FacilityEntity facilityEntity = getFacility(facilityId);
        return FacilityHelper.toFacility(facilityEntity);
    }

    /**
     * Retrieve a single instance of Facility by using the Facility String.
     * <pre></pre>
     * Returns a null instance in return if incorrect facilityCode is provided
     * as parameter.
     *
     * @param uc           UserContext- Required
     * @param facilityCode java.lang.String- Required
     * @return Facility -- Throws exception if instance cannot be got
     */
    public Facility get(UserContext uc, String facilityCode) {

        if (BeanHelper.isEmpty(facilityCode)) {
            throw new InvalidInputException("facilityCode is null");
        }

        // Get by facilityCode
        Criteria criteria = session.createCriteria(FacilityEntity.class);
        criteria.add(Restrictions.eq("facilityCode", facilityCode));
        FacilityEntity facilityEntity = (FacilityEntity) criteria.uniqueResult();

        if (facilityEntity == null) {
            throw new DataNotExistException(String.format("Could not find the Facility by facilityCode = %s", facilityCode));
        }

        return FacilityHelper.toFacility(facilityEntity);
    }

    /**
     * Get Facility Name By Facility Id
     *
     * @param uc         UserContext - Required
     * @param facilityId Long Required
     * @return StringFacility Name
     */
    public String getFacilityNameById(UserContext uc, Long facilityId) {
        if (facilityId == null) {
            return null;
        }
        Criteria criteria = session.createCriteria(FacilityEntity.class);
        criteria.setCacheable(true);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.setProjection(Projections.property("facilityName"));
        criteria.setMaxResults(1);
        criteria.add(Restrictions.idEq(facilityId));
        return (String) criteria.uniqueResult();
    }

    /**
     * Get Facility Name By Facility Category
     *
     * @param uc               UserContext - Required
     * @param facilityCategory String Optional
     * @return Map&lt;Long, String> Long: Facility Id; String: Facility Name
     */
    public Map<Long, String> getFacilityNameByCategory(UserContext uc, String facilityCategory, Boolean facilityActiveFlag, Set<Long> facilityIDs) {
        Criteria criteria = session.createCriteria(FacilityEntity.class);
        criteria.setCacheable(true);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.setProjection(Projections.projectionList().add(Projections.id()).add(Projections.property("facilityName")));
        criteria.add(Restrictions.eqOrIsNull("facilityCategoryText", facilityCategory));

        filterByFacilityIdsCriteria(facilityIDs, criteria);

        if (!BeanHelper.isEmpty(facilityActiveFlag)) {
            Date date = BeanHelper.createDate();
            Criterion activeCriterion = Restrictions.or(Restrictions.gt("facilityDeactiveDate", date), Restrictions.isNull("facilityDeactiveDate"));

            if (Boolean.TRUE.equals(facilityActiveFlag)) {
                criteria.add(activeCriterion);
            } else if (Boolean.FALSE.equals(facilityActiveFlag)) {
                criteria.add(Restrictions.not(activeCriterion));
            }
        }

        List<?> rows = criteria.list();
        Map<Long, String> ret = new HashMap<>();
        if (rows != null) {
            for (Object row : rows) {
                Object[] cols = (Object[]) row;
                Long facilityId = (Long) cols[0];
                String facilityName = (String) cols[1];
                ret.put(facilityId, facilityName);
            }
        }
        return ret;
    }

    /**
     * Get All Facility Names
     *
     * @param uc                 UserContext - Required
     * @param facilityActiveFlag Boolean - Optional. True: All Active; False: All Inactive; null: Both All Active and Inactive.
     * @return Map&lt;Long, String> Long: facilityId; String: Facility Name.
     */
    public Map<Long, String> getFacilityNameMap(UserContext uc, Boolean facilityActiveFlag, Set<Long> facilityIDs) {
        Criteria criteria = session.createCriteria(FacilityEntity.class);
        criteria.setCacheable(true);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.setProjection(Projections.projectionList().add(Projections.id()).add(Projections.property("facilityName")));

        filterByFacilityIdsCriteria(facilityIDs, criteria);

        if (!BeanHelper.isEmpty(facilityActiveFlag)) {
            Date date = BeanHelper.createDate();
            Criterion activeCriterion = Restrictions.or(Restrictions.gt("facilityDeactiveDate", date), Restrictions.isNull("facilityDeactiveDate"));

            if (Boolean.TRUE.equals(facilityActiveFlag)) {
                criteria.add(activeCriterion);
            } else if (Boolean.FALSE.equals(facilityActiveFlag)) {
                criteria.add(Restrictions.not(activeCriterion));
            }
        }

        List<?> rows = criteria.list();
        Map<Long, String> ret = new HashMap<>();
        if (rows != null) {
            for (Object row : rows) {
                Object[] cols = (Object[]) row;
                Long facilityId = (Long) cols[0];
                String facilityName = (String) cols[1];
                ret.put(facilityId, facilityName);
            }
        }
        return ret;
    }

    /**
     * The purpose of the Activate action is to reactivate the facility.
     * <pre></pre>
     * Provenance:
     * <pre>
     * <li>FacilityDeactivateDate is set to null automatically.</li>
     * <li>FacilityActiveFlag is derived.</li>
     * </pre>
     *
     * @param uc       UserContext- Required
     * @param facility Facility- Required
     * @return Facility -- Throws exception if instance cannot be
     * activated
     */
    public Facility activate(UserContext uc, Facility facility) {

        // Validation
        if (facility != null && facility.getFacilityIdentification() == null) {
            throw new InvalidInputException("facilityId is null");
        }

        // Get by facilityId
        FacilityEntity facilityEntity = getFacility(facility.getFacilityIdentification());

        // Create stamp
        StampEntity stamp = BeanHelper.getModifyStamp(uc, context, facilityEntity.getStamp());
        facilityEntity.setFacilityDeactiveDate(null);
        facilityEntity.setStamp(stamp);

        session.update(facilityEntity);
        session.flush();
        session.clear();

        facilityEntity = BeanHelper.getEntity(session, FacilityEntity.class, facilityEntity.getFacilityId());
        return FacilityHelper.toFacility(facilityEntity);

    }

    /**
     * Returns true if facility is activated
     */
    public boolean isActivated(UserContext uc, Facility facility) {

        if (facility != null && facility.getFacilityIdentification() == null) {
            throw new InvalidInputException("facilityId is null");
        }

        FacilityEntity facilityEntity = getFacility(facility.getFacilityIdentification());
        return facilityEntity.getFacilityDeactiveDate() == null ? false : true;

    }

    /**
     * The Search facility action allows for the search of existing facilities
     * in the system.
     * <pre></pre>
     * Search for a set of instances based on a specified criteria. The ids can
     * be used to search only within the specified subset of Id's. List what
     * Search options are available in comments, EXACT is mandatory.
     * <pre>
     * <li>Throws exception if search cannot be completed. </li>
     * </pre>
     *
     * @param uc           UserContext- Required
     * @param search       FacilitySearch- Required
     * @param subsetSearch Set<java.lang.Long> nullable- Optional
     * @param =            "EXACT" (EXACT, PARTIAL)
     * @return java.util.Set&lt;Facility&gt;
     */
    @SuppressWarnings("rawtypes")
    public FacilitiesReturn search(UserContext uc, Set<Long> subsetSearch, FacilitySearch search, Long startIndex, Long resultSize, String resultOrder) {

        // Validation
        if (search == null) {
            throw new InvalidInputException("search is null");
        }

        SearchSetMode setMode = search.getSetMode();

        if (setMode == null || BeanHelper.isEmpty(setMode.value())) {
            setMode = SearchSetMode.ALL;
        }

        // Wildcard search applies on FacilityCode and FacilityName 
        search.setFacilityCode(BeanHelper.trimStar(search.getFacilityCode()));
        search.setFacilityName(BeanHelper.trimStar(search.getFacilityName()));

        // Validate the referenced objects , if set
        ValidationHelper.validate(search);

        //Check for setMode
        if (!SearchSetMode.ONLY.value().equals(setMode.value()) && !SearchSetMode.ANY.value().equals(setMode.value()) && !SearchSetMode.ALL.value().equals(
                setMode.value())) {
            throw new InvalidInputException(String.format(" Search Set is invalid."));
        }

        // Create criteria
        Criteria criteria = session.createCriteria(FacilityEntity.class, "facility");
        filterByFacilityIdsCriteria(subsetSearch, criteria);

        String facilityCode = search.getFacilityCode();
        String facilityName = search.getFacilityName();
        Long parentId = search.getParentId();
        String facilityCategory = search.getFacilityCategoryText();
        String facilityType = search.getFacilityType();
        Boolean activeFlag = search.getFacilityActiveFlag();
        Long facilityContactId = search.getFacilityContactId();

        org.hibernate.criterion.MatchMode mode = org.hibernate.criterion.MatchMode.EXACT;
        // for this search condition, user can call get method by FacilityCode.
        if (!BeanHelper.isEmpty(facilityCode)) {
            if (BeanHelper.isPartialSearch(facilityCode)) {
                mode = org.hibernate.criterion.MatchMode.ANYWHERE;
                criteria.add(Restrictions.ilike("facilityCode", BeanHelper.toWildCard(facilityCode.toLowerCase())));
            } else {
                criteria.add(Restrictions.eq("facilityCode", facilityCode));
            }
        }

        if (!BeanHelper.isEmpty(facilityName)) {
            if (BeanHelper.isPartialSearch(facilityName)) {
                mode = org.hibernate.criterion.MatchMode.ANYWHERE;
                criteria.add(Restrictions.ilike("facilityName", BeanHelper.toWildCard(facilityName.toLowerCase())));
            } else {
                criteria.add(Restrictions.ilike("facilityName", facilityName.toLowerCase(), mode));
            }
        }

        if (!BeanHelper.isEmpty(facilityCategory)) {
            criteria.add(Restrictions.eq("facilityCategoryText", facilityCategory));
        }

        if (!BeanHelper.isEmpty(facilityType)) {
            criteria.add(Restrictions.eq("facilityType", facilityType));
        }

        setFacilityStatusSearchCriteria(search, criteria, activeFlag); //status criteria
        setCreatedDateRangeCriteria(search, criteria); // Created date range

        if (!BeanHelper.isEmpty(parentId)) {
            criteria.add(Restrictions.eq("parentOrganization.id", parentId));
        }

        if (!BeanHelper.isEmpty(facilityContactId)) {
            criteria.add(Restrictions.eq("facilityContactId", facilityContactId));
        }

//        //WOR-234
//        if (search.getOrganization() != null && !search.getOrganization().isEmpty()) {
//            criteria.createAlias("organizations", "o").setFetchMode("o", FetchMode.JOIN).add(Restrictions.in("o.id", search.getOrganization()));
//        }

        facilityAddressSearchCriteria(search, criteria);

        //criteria.setProjection(Projections.countDistinct("facilityId"));

        Long totalSize = (Long) criteria.setProjection(Projections.countDistinct("facilityId")).uniqueResult();

        //Distinct duplicate entity
        criteria.setProjection(null);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        //Search and only return the predefined max number of records
        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());

            if (resultSize == null || resultSize > searchMaxLimit) {
                criteria.setMaxResults(searchMaxLimit);
            } else {
                criteria.setMaxResults(resultSize.intValue());
            }
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                criteria.addOrder(ord);
            }
        }

        List list = criteria.list();

        FacilitiesReturn ret = new FacilitiesReturn();
        ret.setFacilities(FacilityHelper.toFacilitySet(list));
        ret.setTotalSize(totalSize);

        return ret;

    }

    private void filterByFacilityIdsCriteria(Set<Long> subsetSearch, Criteria criteria) {
        if (!BeanHelper.isEmpty(subsetSearch)) {
            Set<Set<Long>> facilityIdSet = BeanHelper.splitSet(subsetSearch);
            if (!facilityIdSet.isEmpty() && !((Set) (facilityIdSet.toArray()[0])).isEmpty()) {
                Conjunction conjunction = Restrictions.conjunction();
                for (Set<Long> facilityIDs : facilityIdSet) {
                    conjunction.add(Restrictions.in("facilityId", facilityIDs));
                }
                criteria.add(conjunction);
            }
        }
    }

    private void setCreatedDateRangeCriteria(FacilitySearch search, Criteria criteria) {
        if (!BeanHelper.isEmpty(search.getCreatedDateFrom()) && !BeanHelper.isEmpty(search.getCreatedDateTo())) {
            Criterion createDateRangeCriterion = Restrictions.and(Restrictions.ge("createDateTime", search.getCreatedDateFrom()),
                    Restrictions.le("createDateTime", search.getCreatedDateTo()));
            criteria.add(Restrictions.or(createDateRangeCriterion));
        } else if (!BeanHelper.isEmpty(search.getCreatedDateFrom())) {
            criteria.add(Restrictions.or(Restrictions.ge("createDateTime", search.getCreatedDateFrom())));
        } else if (!BeanHelper.isEmpty(search.getCreatedDateTo())) {
            criteria.add(Restrictions.or(Restrictions.le("createDateTime", search.getCreatedDateTo())));
        }
    }

    private void setFacilityStatusSearchCriteria(FacilitySearch search, Criteria criteria, Boolean activeFlag) {
        if (!BeanHelper.isEmpty(activeFlag)) { // if empty we consider Active and Inactive facilities
            Date date = BeanHelper.createDate();
            if (Boolean.TRUE.equals(activeFlag)) {
                Criterion activeCriterion = Restrictions.or(Restrictions.gt("facilityDeactiveDate", date), Restrictions.isNull("facilityDeactiveDate"));
                criteria.add(activeCriterion);
            } else if (Boolean.FALSE.equals(activeFlag)) { //Inactive status search

                // Deactivate date range
                if (!BeanHelper.isEmpty(search.getDeactivatedDateFrom()) && !BeanHelper.isEmpty(search.getDeactivatedDatTo())) {
                    Criterion deactivateDateRangeCriterion = Restrictions.and(Restrictions.isNotNull("facilityDeactiveDate"),
                            Restrictions.ge("facilityDeactiveDate", search.getDeactivatedDateFrom()),
                            Restrictions.le("facilityDeactiveDate", search.getDeactivatedDatTo()));
                    criteria.add(Restrictions.or(deactivateDateRangeCriterion));
                } else if (!BeanHelper.isEmpty(search.getDeactivatedDateFrom())) {
                    criteria.add(Restrictions.or(
                            Restrictions.and(Restrictions.isNotNull("facilityDeactiveDate"), Restrictions.ge("facilityDeactiveDate", search.getDeactivatedDateFrom()))));
                } else if (!BeanHelper.isEmpty(search.getDeactivatedDatTo())) {
                    criteria.add(Restrictions.or(
                            Restrictions.and(Restrictions.isNotNull("facilityDeactiveDate"), Restrictions.le("facilityDeactiveDate", search.getDeactivatedDatTo()))));
                } else {
                    // search for status inactive till date.
                    Criterion activeCriterion = Restrictions.and(Restrictions.isNotNull("facilityDeactiveDate"), Restrictions.lt("facilityDeactiveDate", date));
                    criteria.add(Restrictions.or(activeCriterion));
                }
            }
        }
    }

    private void facilityAddressSearchCriteria(FacilitySearch search, Criteria criteria) {
        if (StringUtils.isNotBlank(search.getStreetNumber()) || StringUtils.isNotBlank(search.getStreetName()) || StringUtils.isNotBlank(search.getCity())
                || StringUtils.isNotBlank(search.getProvience()) || StringUtils.isNotBlank(search.getCountry())) {

            DetachedCriteria locationAddressCriteria = DetachedCriteria.forClass(LocationEntity.class, "le");
            locationAddressCriteria.createAlias("locationAddress", "la");
            locationAddressCriteria.add(Restrictions.eq("addressCategory", AddressCategory.FACILITY));

            if (StringUtils.isNotBlank(search.getStreetNumber())) {
                locationAddressCriteria.add(Restrictions.sqlRestriction(" la1_.locationStreetNumberText like '" + BeanHelper.toPartialSearch(search.getStreetNumber()) + "' "));
            }

            if (StringUtils.isNotBlank(search.getStreetName())) {
                locationAddressCriteria.add(Restrictions.ilike("la.locationStreetName", BeanHelper.toPartialSearch(search.getStreetName())));
            }

            if (StringUtils.isNotBlank(search.getCity())) {
                locationAddressCriteria.add(Restrictions.ilike("la.locationCityName", search.getCity()));
            }

            if (StringUtils.isNotBlank(search.getProvience())) {
                locationAddressCriteria.add(Restrictions.eq("la.locationStateCode", search.getProvience()));
            }

            if (StringUtils.isNotBlank(search.getCountry())) {
                locationAddressCriteria.add(Restrictions.eq("la.locationCountryName", search.getCountry()));
            }

            locationAddressCriteria.setProjection(Property.forName("instanceId"));
            criteria.add(Restrictions.or(Subqueries.propertyIn("facilityId", locationAddressCriteria)));

        }
    }

    /**
     * Return a set of all instance identifiers.
     * <pre>
     * <li>Throws exception if there is an error during the get all.</li>
     * </pre>
     *
     * @param uc UserContext
     * @return Set<java.lang.Long>
     */
    public Set<Long> getAll(UserContext uc) {
        return BeanHelper.getAll(uc, context, session, FacilityEntity.class);
    }

    /**
     * Count of all instances.
     * <pre>
     * <li>Throws exception if there is an error during the count.</li>
     * </pre>
     *
     * @param uc UserContext- Required
     * @return java.lang.Long
     */
    public Long getCount(UserContext uc) {
        return BeanHelper.getCount(uc, context, session, INSTANCE_ID, FacilityEntity.class);
    }

    /**
     * Cascading delete of all instances, references and reference data.
     * <pre>
     * <li>Return success code if all instances, references and reference data deleted.</li>
     * <li>Return error code is not all instances, references and reference data deleted.</li>
     * </pre>
     *
     * @param uc UserContext- Required
     */
    public void deleteAll(UserContext uc) {
        deleteAllFacilities();
    }

    /**
     * Cascading delete of a single instance.
     * <pre>
     * <li>Returns an instance and success code if instance can be deleted.</li>
     * <li>Returns a 0 if instance cannot be deleted.</li>
     * </pre>
     *
     * @param uc         UserContext- Required
     * @param facilityId java.lang.Long- Required
     * @return Facility
     */
    public void delete(UserContext uc, Long facilityId) {
        if (facilityId == null) {
            throw new InvalidInputException("facilityId is null");
        }

        FacilityEntity facilityEntity = getFacility(facilityId);
        facilityEntity.setFlag(DataFlag.DELETE.value());
        session.update(facilityEntity);
        session.flush();
    }

    /**
     * Return the stamp of a single instance.
     * <pre>
     * <li>Throws exception if reference cannot be created.</li>
     * </pre>
     *
     * @param uc         UserContext- Required
     * @param facilityId Long- Required
     * @return Stamp
     */
    public StampType getStamp(UserContext uc, Long facilityId) {

        FacilityEntity facilityEntity = getFacility(facilityId);

        // Get StampEntity out of FacilityEntity.
        // StampEntity can be null as it is a embedded entity. Create one if that is the case.
        StampEntity stamp = facilityEntity.getStamp();
        if (stamp == null) {
            stamp = new StampEntity();
        }
        StampType ret = BeanHelper.toStamp(stamp);

        return ret;

    }

    /**
     * The purpose of the Retrieve NIEM Output is to retrieve NIEM output based
     * on a NIEM subset.
     * <pre></pre>
     * Business Needs Satisfied:
     * <pre>
     * <li>1.1.11.1.2 System shall utilize National Information Exchange Mode (NIEM) for data structures in order to exchange data.</li>
     * </pre>
     *
     * @param uc         UserContext- Required
     * @param facilityId Long- Required
     * @return String -- Throws exception in case of exception
     */
    public String getNiem(UserContext uc, Long facilityId) {

        //
        FacilityEntity facilityEntity = getFacility(facilityId);

        StringBuffer niem = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");

        String strRoot = String.format(
                "<root id=\"%s\" service=\"Facility\" xmlns:s=\"http://niem.gov/niem/structures/2.0\"  xmlns:nc=\"http://niem.gov/niem/niem-core/2.0\" xmlns:sv=\"http://syscon.net/services/extension/1.0\" xmlns:facility-ext=\"http://syscon.net/services/facility/extension/1.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://syscon.net/services/Facility/exchange/1.0 ./exchange/1.0/facility-exchange.xsd\" xmlns=\"http://syscon.net/services/Facility/exchange/1.0\">",
                facilityId);
        niem.append(strRoot);
        niem.append("<nc:Facility>\n");
        niem.append("	<nc:FacilityIdentification>\n");
        niem.append(String.format("	<nc:IdentificationID>%s</nc:IdentificationID>\n", facilityId));
        niem.append("		<nc:IdentificationExpirationDate>\n");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String strDeactiveDate = "";
        if (facilityEntity.getFacilityDeactiveDate() != null) {
            strDeactiveDate = formatter.format(facilityEntity.getFacilityDeactiveDate());
        }
        niem.append(String.format("		<nc:Date>%s</nc:Date>\n", strDeactiveDate));
        niem.append("		</nc:IdentificationExpirationDate>\n");
        niem.append("	</nc:FacilityIdentification>\n");
        niem.append(String.format("	<nc:FacilityName>%s</nc:FacilityName>\n", facilityEntity.getFacilityName()));
        niem.append(String.format("	<nc:FacilityCategoryText>%s</nc:FacilityCategoryText>\n", facilityEntity.getFacilityCategoryText()));
        niem.append("</nc:Facility>\n");
        niem.append("<facility-ext:Organizations>\n");
        niem.append(String.format("	<sv:Association id=\"%s\"/>\n", facilityEntity.getOrganizations()));
        niem.append("</facility-ext:Organizations>\n");
        niem.append("<sv:ServiceAssociations>\n");
        niem.append("</sv:ServiceAssociations>\n");
        niem.append("</root>\n");

        return niem.toString();

    }

    /**
     * @param uc  UserContext- Required
     * @param dto LocationType- Required
     * @return
     */
    public Location addLocation(UserContext uc, Location dto) {

        String functionName = "addLocation";
        // Validation
        if (dto == null || dto.getInstanceId() == null) {
            String message = String.format("Invalid parameter: LocationType=%s.", dto);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        //Verify facility if exist
        getFacility(dto.getInstanceId());

        dto.setAddressCategory(AddressCategory.FACILITY);

        //Create location
        LocationServiceAdapter locAdapter = new LocationServiceAdapter();
        Location ret = locAdapter.create(uc, dto);

        return ret;

    }

    /**
     * @param uc         UserContext- Required
     * @param facilityId Long - Required
     * @param locationId Long - Required
     */
    public void deleteLocation(UserContext uc, Long facilityId, Long locationId) {

        String functionName = "UserContext";
        // Validation
        if (facilityId == null || locationId == null) {
            String message = String.format("facilityId/locationId=%s/%s.", facilityId, locationId);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        //Verify person exist
        getFacility(facilityId);

        LocationServiceAdapter locAdapter = new LocationServiceAdapter();
        Location loc = locAdapter.get(uc, locationId);

        if (!facilityId.equals(loc.getInstanceId())) {
            String message = String.format("Invalid LocationId with the facility. facilityId/locationId=%s/%s.", facilityId, locationId);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        locAdapter.delete(uc, locationId);

    }

    /**
     * @param uc  UserContext- Required
     * @param dto LocationType- Required
     * @return
     */
    public Location updateLocation(UserContext uc, Location dto) {

        String functionName = "updateLocation";

        // Validation
        if (dto == null || dto.getLocationIdentification() == null || dto.getInstanceId() == null) {
            String message = String.format("Invalid parameter: LocationType=%s.", dto);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        //Check the facility if exist
        getFacility(dto.getInstanceId());

        dto.setAddressCategory(AddressCategory.FACILITY);

        //update location
        LocationServiceAdapter locAdapter = new LocationServiceAdapter();
        Location ret = locAdapter.update(uc, dto);

        //Don't need to relationship between person and location

        return ret;
    }

    /**
     * @param uc         UserContext- Required
     * @param locationId Long - Required
     * @return
     */
    public Location getLocation(UserContext uc, Long locationId) {

        String functionName = "getLocation";

        // Validation
        if (locationId == null) {
            String message = "locationId is null.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        //Get location
        LocationServiceAdapter locAdapter = new LocationServiceAdapter();
        Location ret = locAdapter.get(uc, locationId);

        return ret;

    }

    /**
     * @param uc         UserContext- Required
     * @param facilityId Long - Required
     * @return
     */
    public List<Location> getLocations(UserContext uc, Long facilityId) {

        String functionName = "getLocations";
        // Validation
        if (facilityId == null) {
            String message = "facilityId is null.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        //Check the facility if exist
        getFacility(facilityId);

        //update location
        LocationServiceAdapter locAdapter = new LocationServiceAdapter();
        List<Location> ret = locAdapter.searchByFacilityId(uc, facilityId);

        return ret;

    }

    /**
     * Check if a facility exists.
     *
     * @param facilityId the facility identification
     */
    private FacilityEntity getFacility(Long facilityId) {

        FacilityEntity facilityEntity = (FacilityEntity) session.get(FacilityEntity.class, facilityId);
        if (facilityEntity == null) {
            throw new DataNotExistException(String.format("Could not find facility by facilityId = %d.", facilityId), ErrorCodes.FAC_NOT_EXIST_FACILITY);
        }

        return facilityEntity;
    }

    private void verifyExistence(Facility facility) {

        // Check and Set the Organization and Locations
        /*Set<OrganizationEntity> organizations = new HashSet<OrganizationEntity>();
        Set<LocationEntity> locations = new HashSet<LocationEntity>();
		
		if(facility.getOrganizations() != null){
			for ( Long orgId : facility.getOrganizations()){
				OrganizationEntity orgEntity = (OrganizationEntity)session.get(OrganizationEntity.class, orgId);
				if (orgEntity == null) {
					String message = String.format("The organization does not exist orgId= %s", orgId);
					throw new DataNotExistException(message);
				}
				organizations.add(orgEntity);
			}
		}
		
		if(facility.getLocations() != null){
			for ( Long locId : facility.getLocations()){
				LocationEntity locEntity = (LocationEntity)session.get(LocationEntity.class, locId);
				if (locEntity == null) {
					String message = String.format("The location does not exist locId= %s", locId);
					throw new DataNotExistException(message);
				}
				locations.add(locEntity);
			}
		}*/

        //TODO: Check that if the FacilityId exists
    }

    private boolean isUniqueFacilityCode(String code) {

        Criteria criteria = session.createCriteria(FacilityEntity.class);
        criteria.add(Restrictions.eq("facilityCode", code));
        criteria.setProjection(Projections.rowCount());
        if ((Long) criteria.uniqueResult() > 0L) {
            log.debug(code + " is not unique.");
            return false;
        }
        return true;
    }

    /**
     * Delete all facilities,
     */
    private void deleteAllFacilities() {
        session.createSQLQuery("DELETE FROM STF_Facility").executeUpdate();
        session.createSQLQuery("DELETE FROM FAC_Organization").executeUpdate();
        session.createSQLQuery("DELETE FROM FAC_Facility").executeUpdate();

        // Clear Hibernate cache to add to avoid unique key violation.
        session.flush();
        session.clear();
    }

    /**
     * Load service properties such as searchMaxLimit. This will be called when the bean is created.
     */
    @SuppressWarnings("rawtypes")
    @PostConstruct
    private void initialize() {

        ClassLoader loader = this.getClass().getClassLoader();

        try {
            ResourceBundle rb = ResourceBundle.getBundle("service", Locale.getDefault(), loader);
            for (Enumeration keys = rb.getKeys(); keys.hasMoreElements(); ) {
                String key = (String) keys.nextElement();
                if ("searchMaxLimit".equalsIgnoreCase(key)) {
                    String value = rb.getString(key);
                    searchMaxLimit = Integer.parseInt(value);
                    return;
                }
            }
        } catch (Exception ex) {
            // Ignore it
        }

    }

    private void copyProperties(FacilityEntity src, FacilityEntity target) {
        if ((src == null) || (target == null)) {
            return;
        }

        // Set the properties FacilityName, Contact Reference and Facility Category Text
        // which are able to be updated

        target.setFacilityName(src.getFacilityName());
        target.setFacilityCategoryText(src.getFacilityCategoryText());
        target.setFacilityType(src.getFacilityType());
        target.setFacilityContactId(src.getFacilityContactId());
        target.setParentOrganization(src.getParentOrganization());
        target.setOrganizations(src.getOrganizations());
        target.setFacilityDeactiveDate(src.getFacilityDeactiveDate());
        target.setFacilityCode(src.getFacilityCode());
        target.setInternalLocationRequired(src.isInternalLocationRequired());

    }

    public void setSearchMaxLimit(int searchMaxLimit) {
        this.searchMaxLimit = searchMaxLimit;
    }

    private Long generateId() {
        Long id = BeanHelper.getNextId(session, "fac_facility", "facilityId");
        return id;
    }

    /**
     * @return
     */
    public List<Long> getAllAssociatedFacilityToOrganization() {
        @SuppressWarnings("unchecked") List<Long> facilities = session.createSQLQuery("SELECT FACILITYID FROM FAC_Organization").list();
        return facilities;
    }

    public Boolean isDuplicateFacility(UserContext uc, Long facilityId, String facilityCode) {

        Criteria c = session.createCriteria(FacilityEntity.class);

        c.setLockMode(LockMode.NONE);
        c.setCacheMode(CacheMode.IGNORE);

        if (facilityId != null) {
            c.add(Restrictions.ne("facilityId", facilityId));
        }

        if (facilityCode != null) {
            c.add(Restrictions.eq("facilityCode", facilityCode).ignoreCase());
        }

        @SuppressWarnings("unchecked") List<FacilityEntity> searchresult = c.list();
        return !searchresult.isEmpty();
    }

    public Boolean hasConfiguredFacilityInternalLocations(UserContext uc, Long facilityId) {
        Criteria c = session.createCriteria(InternalLocationEntity.class);
        c.setCacheMode(CacheMode.IGNORE);

        if (facilityId != null) {
            c.add(Restrictions.eq("facilityId", facilityId));
        }
        c.add(Restrictions.isNotNull("hierarchyLevelId"));       //not parent FIL
        c.setProjection(Projections.rowCount());
        Long res = (c.uniqueResult() == null) ? 0L: (Long)c.uniqueResult();
        return res > 0;
    }
    
    public Boolean hasActiveFacilityInternalLocations(UserContext uc, Long facilityId) {
        Criteria c = session.createCriteria(InternalLocationEntity.class);
        c.setCacheMode(CacheMode.IGNORE);

        if (facilityId != null) {
            c.add(Restrictions.eq("facilityId", facilityId));
        }
        c.add(Restrictions.eq("active", Boolean.TRUE));             //active=1
        c.add(Restrictions.eq("flag", DataFlag.ACTIVE.value()));    //flag=1
        c.add(Restrictions.isNotNull("hierarchyLevelId"));       //not parent FIL
        c.setProjection(Projections.rowCount());
        Long res = (c.uniqueResult() == null) ? 0L: (Long)c.uniqueResult();
        return res > 0;
    }

    public Organization getParentOrganization(UserContext uc, Facility facility) {
        Long parentOrgId = facility.getParentOrganizationId();
        OrganizationEntity entity = (OrganizationEntity) session.get(OrganizationEntity.class, parentOrgId);
        return OrganizationHelper.toOrganization(entity);
    }
    
    public Boolean hasActiveAssociatedOrganizationFacility(UserContext uc, Long facilityId) {
        
        FacilityEntity facilityEntity = (FacilityEntity) session.get(FacilityEntity.class, facilityId);
        Set<OrganizationEntity> allAssociatedOrganizations = facilityEntity.getOrganizations();

        for(OrganizationEntity organizationEntity: allAssociatedOrganizations) {
            if(organizationEntity.getFlag().equals(DataFlag.ACTIVE.value())) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param uc
     * @return
     */
    public List<Facility> getFacilitiesByLocationConfiguration(UserContext uc) {

        Criteria criteria = session.createCriteria(FacilityEntity.class);
        criteria.add(Restrictions.eq("internalLocationRequired", true));

        //
        List<FacilityEntity> facilityEntityList = criteria.list();
        List<Facility> ret = FacilityHelper.toFacilityList(facilityEntityList);

        //
        return ret;
    }


}