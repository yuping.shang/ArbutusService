package syscon.arbutus.product.services.program.realization.persistence;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Age Group/Range Entity for Program and Service
 *
 * @author YShang
 * @DbComment PRG_AGEGROUP 'Age Group/Range Entity for Program and Service'
 * @since April 6, 2014
 */
@Audited
@Entity
@Table(name = "PRG_AGEGROUP")
public class AgeGroupEntity implements Serializable {

    private static final long serialVersionUID = 9070899851316214727L;

    /**
     * @DbComment PRG_AGEGROUP.ageRangeId 'Id of PRG_AGEGROUP table'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PRG_AGEGROUP_ID")
    @SequenceGenerator(name = "SEQ_PRG_AGEGROUP_ID", sequenceName = "SEQ_PRG_AGEGROUP_ID", allocationSize = 1)
    @Column(name = "ageRangeId")
    private Long ageRangeId;

    /**
     * @DbComment PRG_AGEGROUP.ageRange 'Age Range. Juvenile, Adult,... Metadata: LOCATIONPROPERTY: HSENS, SHOWER, WHEELCHAIR'
     */
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.AGE_RANGE)
    @Column(name = "ageRange", nullable = true, length = 64)
    private String ageRange;

    /**
     * @DbComment PRG_AGEGROUP.targetInmateId 'Target Inmate Id, Id of PRG_AGEGROUP table'
     */
    @ForeignKey(name = "FK_PRG_AGEGROUP")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "targetInmateId", nullable = false)
    private TargetInmateEntity inmate;

    /**
     * @DbComment PRG_AGEGROUP.createUserId 'Create User Id'
     * @DbComment PRG_AGEGROUP.createDateTime 'Create Date Time'
     * @DbComment PRG_AGEGROUP.modifyUserId 'Modify User Id'
     * @DbComment PRG_AGEGROUP.modifyDateTime 'Modify Date Time'
     * @DbComment PRG_AGEGROUP.invocationContext 'Invocation Context'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public AgeGroupEntity() {
        super();
    }

    public Long getAgeRangeId() {
        return ageRangeId;
    }

    public void setAgeRangeId(Long ageRangeId) {
        this.ageRangeId = ageRangeId;
    }

    public String getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(String ageRange) {
        this.ageRange = ageRange;
    }

    public TargetInmateEntity getInmate() {
        return inmate;
    }

    public void setInmate(TargetInmateEntity inmate) {
        this.inmate = inmate;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ageRange == null) ? 0 : ageRange.hashCode());
        result = prime * result + ((ageRangeId == null) ? 0 : ageRangeId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        AgeGroupEntity other = (AgeGroupEntity) obj;
        if (ageRange == null) {
            if (other.ageRange != null) {
				return false;
			}
        } else if (!ageRange.equals(other.ageRange)) {
			return false;
		}
        if (ageRangeId == null) {
            if (other.ageRangeId != null) {
				return false;
			}
        } else if (!ageRangeId.equals(other.ageRangeId)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "AgeGroupEntity [ageRangeId=" + ageRangeId + ", ageRange=" + ageRange + ", stamp=" + stamp + "]";
    }

}
