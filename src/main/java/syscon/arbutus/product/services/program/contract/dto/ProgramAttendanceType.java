package syscon.arbutus.product.services.program.contract.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;
import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * Program Attendance Type for Program and Service
 *
 * @author YShang
 * @since April 6, 2014
 */
@ArbutusConstraint(constraints = { "attendanceStartTime <= attendanceEndTime" })
public class ProgramAttendanceType implements Serializable, Comparable<ProgramAttendanceType> {

    private static final long serialVersionUID = -5172294365154150149L;

    private static final int LENGTH_SMALL = 64;

    /**
     * Identification of a Program Attendance
     */
    private Long attendanceId;

    /**
     * The date the program session occurred.
     * This is based on the details of the scheduled sessions in the program offering.
     * For single session programs this is to be entered by the user.
     */
    @NotNull
    private Date attendanceDate;

    /**
     * The time the session started.
     * This is based on the details of the scheduled sessions in the program offering.
     * Only applies to programs that have multiple sessions.
     */
    @NotNull
    private Date attendanceStartTime;

    /**
     * The time the session ended.
     * This is based on the details of the scheduled sessions in the program offering.
     * Only applies to programs that have multiple sessions.
     */
    @NotNull
    private Date attendanceEndTime;

    /**
     * Identification of an instance of ActivityType created by ActivityService
     */
    private Long activityId;

    /**
     * Confirmation of attendance ie. yes, no or not recorded
     * For single session programs, positive attendance will indicate a successful program completion.
     * Conversely, negative attendance will indicate an unsuccessful program completion (ie. abandonment).
     */
    private String attended;

    /**
     * Indicates the inmate’s performance in the program session
     * eg. late, drunk, stoned, fell asleep.
     * Only applies to programs that have multiple sessions.
     * Note that multiple values may apply.
     * <p>Metadata: PROGRAMPERFORMANCE: ASLEEP, EXPECTED, INTOX, LATE, NOPART</p>
     */
    @Size(max = 64, message = "max length 64")
    private String performance;

    /**
     * Additional optional comments about the attendance
     */
    @Valid
    private CommentType comments;

    /**
     * The inmate’s assignment to the program offering for which the attendance is being recorded
     */
    @NotNull
    @Valid
    private ProgramAssignmentType programAssignment;

    /**
     * help to retrieve program information from attendance
     */
    private Long assignmentId;

    public ProgramAttendanceType() {
        super();
    }

    /**
     * Identification of a Program Attendance
     *
     * @return Long
     */
    public Long getAttendanceId() {
        return attendanceId;
    }

    /**
     * Identification of a Program Attendance
     *
     * @param attendanceId Long
     */
    public void setAttendanceId(Long attendanceId) {
        this.attendanceId = attendanceId;
    }

    /**
     * The date the program session occurred.
     * This is based on the details of the scheduled sessions in the program offering.
     * For single session programs this is to be entered by the user.
     *
     * @return Date
     */
    public Date getAttendanceDate() {
        return attendanceDate;
    }

    /**
     * The date the program session occurred.
     * This is based on the details of the scheduled sessions in the program offering.
     * For single session programs this is to be entered by the user.
     *
     * @param attendanceDate Date
     */
    public void setAttendanceDate(Date attendanceDate) {
        this.attendanceDate = attendanceDate;
    }

    /**
     * The time the session started.
     * This is based on the details of the scheduled sessions in the program offering.
     * Only applies to programs that have multiple sessions.
     *
     * @return Date
     */
    public Date getAttendanceStartTime() {
        return attendanceStartTime;
    }

    /**
     * The time the session started.
     * This is based on the details of the scheduled sessions in the program offering.
     * Only applies to programs that have multiple sessions.
     *
     * @param attendanceStartTime Date
     */
    public void setAttendanceStartTime(Date attendanceStartTime) {
        this.attendanceStartTime = attendanceStartTime;
    }

    /**
     * The time the session ended.
     * This is based on the details of the scheduled sessions in the program offering.
     * Only applies to programs that have multiple sessions.
     *
     * @return Date
     */
    public Date getAttendanceEndTime() {
        return attendanceEndTime;
    }

    /**
     * The time the session ended.
     * This is based on the details of the scheduled sessions in the program offering.
     * Only applies to programs that have multiple sessions.
     *
     * @param attendanceEndTime Date
     */
    public void setAttendanceEndTime(Date attendanceEndTime) {
        this.attendanceEndTime = attendanceEndTime;
    }

    /**
     * Identification of an instance of ActivityType created by ActivityService
     *
     * @return Long
     */
    public Long getActivityId() {
        return activityId;
    }

    /**
     * Identification of an instance of ActivityType created by ActivityService
     *
     * @param activityId Long
     */
    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    /**
     * Confirmation of attendance ie. yes or no.
     * For single session programs, positive attendance will indicate a successful program completion.
     * Conversely, negative attendance will indicate an unsuccessful program completion (ie. abandonment).
     *
     * @return Boolean
     */
    public String getAttended() {
        return attended;
    }

    /**
     * Confirmation of attendance ie. yes or no.
     * For single session programs, positive attendance will indicate a successful program completion.
     * Conversely, negative attendance will indicate an unsuccessful program completion (ie. abandonment).
     *
     * @param attended Boolean
     */
    public void setAttended(String attended) {
        this.attended = attended;
    }

    /**
     * Indicates the inmate’s performance in the program session
     * eg. late, drunk, stoned, fell asleep.
     * Only applies to programs that have multiple sessions.
     * Note that multiple values may apply.
     * <p>Metadata: PROGRAMPERFORMANCE: ASLEEP, EXPECTED, INTOX, LATE, NOPART</p>
     * <p>Max length 64</p>
     *
     * @return String
     */
    public String getPerformance() {
        return performance;
    }

    /**
     * Indicates the inmate’s performance in the program session
     * eg. late, drunk, stoned, fell asleep.
     * Only applies to programs that have multiple sessions.
     * Note that multiple values may apply.
     * <p>Metadata: PROGRAMPERFORMANCE: ASLEEP, EXPECTED, INTOX, LATE, NOPART</p>
     * <p>Max length 64</p>
     *
     * @param performance String
     */
    public void setPerformance(String performance) {
        this.performance = performance;
    }

    /**
     * Additional optional comments about the attendance
     *
     * @return CommentType
     */
    public CommentType getComments() {
        return comments;
    }

    /**
     * Additional optional comments about the attendance
     *
     * @param comments CommentType
     */
    public void setComments(CommentType comments) {
        this.comments = comments;
    }

    /**
     * The inmate’s assignment to the program offering for which the attendance is being recorded
     *
     * @return ProgramAssignmentType
     */
    public ProgramAssignmentType getProgramAssignment() {
        return programAssignment;
    }

    /**
     * The inmate’s assignment to the program offering for which the attendance is being recorded
     *
     * @param programAssignment ProgramAssignmentType
     */
    public void setProgramAssignment(ProgramAssignmentType programAssignment) {
        this.programAssignment = programAssignment;
    }

    public Long getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }

    @Override
    public int compareTo(ProgramAttendanceType o) {
        return this.getAttendanceDate().compareTo(o.attendanceDate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (!(o instanceof ProgramAttendanceType)) {
			return false;
		}

        ProgramAttendanceType that = (ProgramAttendanceType) o;

        if (activityId != null ? !activityId.equals(that.activityId) : that.activityId != null) {
			return false;
		}
        if (attendanceDate != null ? !attendanceDate.equals(that.attendanceDate) : that.attendanceDate != null) {
			return false;
		}
        if (attendanceEndTime != null ? !attendanceEndTime.equals(that.attendanceEndTime) : that.attendanceEndTime != null) {
			return false;
		}
        if (attendanceId != null ? !attendanceId.equals(that.attendanceId) : that.attendanceId != null) {
			return false;
		}
        if (attendanceStartTime != null ? !attendanceStartTime.equals(that.attendanceStartTime) : that.attendanceStartTime != null) {
			return false;
		}
        if (attended != null ? !attended.equals(that.attended) : that.attended != null) {
			return false;
		}
        if (performance != null ? !performance.equals(that.performance) : that.performance != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = attendanceId != null ? attendanceId.hashCode() : 0;
        result = 31 * result + (attendanceDate != null ? attendanceDate.hashCode() : 0);
        result = 31 * result + (attendanceStartTime != null ? attendanceStartTime.hashCode() : 0);
        result = 31 * result + (attendanceEndTime != null ? attendanceEndTime.hashCode() : 0);
        result = 31 * result + (activityId != null ? activityId.hashCode() : 0);
        result = 31 * result + (attended != null ? attended.hashCode() : 0);
        result = 31 * result + (performance != null ? performance.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProgramAttendanceType{" +
                "attendanceId=" + attendanceId +
                ", attendanceDate=" + attendanceDate +
                ", attendanceStartTime=" + attendanceStartTime +
                ", attendanceEndTime=" + attendanceEndTime +
                ", activityId=" + activityId +
                ", attended=" + attended +
                ", performance='" + performance + '\'' +
                ", comments=" + comments +
                '}';
    }
}
