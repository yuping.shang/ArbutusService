package syscon.arbutus.product.services.program.realization.persistence;

/**
 * Reference Set for Oregram and Service
 *
 * @author YShang
 * @since April 6, 2014
 */
public class MetaSet {
    /**
     * Institutional Activity, Accredited Program, Community Service,...
     * Non-configurable reference domain
     */
    public static final String PROGRAM_CATEGORY = "ProgramCategory";

    /**
     * No longer offered, Provider cancelled offering, Facility no longer available, ...
     * Configurable reference domain
     */
    public static final String PROGRAM_TERMINATION_REASON = "ProgramTerminationReason";
    /**
     * CANCEL, NOTAVAIL
     */
    public static final String OFFERING_TERMINATION_REASON = "OfferingTerminationReason";

    /**
     * Male, Female, Unknown,...
     * Configurable reference domain
     */
    public static final String GENDER = "OffenderGender";

    /**
     * Juvenile, Adult,...
     * Configurable reference domain
     */
    public static final String AGE_RANGE = "AgeRange";

    /**
     * Wheelchair Accessible,...
     * Configurable reference domain
     */
    public static final String AMENITIES = "LocationProperty";

    /**
     * Low, Medium, High,...
     * Configurable reference domain
     */
    public static final String REFERRAL_PRIORITY = "ReferralPriority";

    /**
     * No availability, Inmate not suitable,...
     * Configurable reference domain
     */
    public static final String REFERRAL_REJECTION_REASON = "ReferralRejectionReason";

    /**
     * Referred, Cancelled, Approved, Rejected, Allocated, Suspended, Abandoned, Completed.
     * Non-configurable reference domain
     */
    public static final String PROGRAM_ASSIGNMENT_STATUS = "ProgramAssignmentStatus";

    /**
     * Disciplinary action, Reclassification, Health/Medical issues,...
     * Configurable reference domain
     */
    public static final String PROGRAM_SUSPEND_REASON = "ProgramSuspendReason";

    /**
     * Release, NOTCOMP, TRANSFER, ...
     * Configurable reference domain
     */
    public static final String PROGRAM_ABANDONMENT_REASON = "ProgramAbandonmentReason";

    /**
     * Late to session, Intoxicated, Fell asleep, Expected performance
     */
    public static final String PROGRAM_PERFORMANCE = "ProgramPerformance";

    /**
     * Attend AA/NA, CTU Referral, Job Placement, Program Attendance
     */

    public static final String PROGRAM_WORKSTEP = "CaseWorkSteps";
}
