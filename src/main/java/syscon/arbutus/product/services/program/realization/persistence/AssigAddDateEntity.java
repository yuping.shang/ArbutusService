package syscon.arbutus.product.services.program.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Assignment Addition Date Entity for Program and Service
 *
 * @author YShang
 * @DbComment PRG_ASSIGADDDATE 'Assignment Addition Date Entity for Program and Service'
 * @since July 28, 2014
 */
@Audited
@Entity
@Table(name = "PRG_ASSIGADDDATE")
public class AssigAddDateEntity implements Serializable {

    private static final long serialVersionUID = -6389039149804034276L;

    /**
     * @DbComment PRG_ASSIGADDDATE.assigAddDateId 'Id of addition date to schedule recurrence pattern'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PRG_ASSIGADDDATE_ID")
    @SequenceGenerator(name = "SEQ_PRG_ASSIGADDDATE_ID", sequenceName = "SEQ_PRG_ASSIGADDDATE_ID", allocationSize = 1)
    @Column(name = "assigAddDateId")
    private Long assigAddDateId;

    /**
     * @DbComment PRG_ASSIGADDDATE.thedate 'addition date to schedule recurrence pattern'
     */
    @Column(name = "thedate", nullable = false)
    private Date date;

    /**
     * @DbComment PRG_ASSIGADDDATE.startTime 'Start time (time only has meaning)'
     */
    @Column(name = "startTime")
    private Date startTime;

    /**
     * @DbComment PRG_ASSIGADDDATE.endTime 'End time (time only has meaning)'
     */
    @Column(name = "endTime")
    private Date endTime;

    /**
     * @DbComment PRG_ASSIGADDDATE.assignmentId 'Foreign key to PRG_ASSIGNMENT table'
     */
    @ForeignKey(name = "FK_PRG_AGADATE")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "assignmentId", nullable = false)
    private ProgramAssignmentEntity programAssignment;

    /**
     * @DbComment PRG_ASSIGADDDATE.createUserId 'Create User Id'
     * @DbComment PRG_ASSIGADDDATE.createDateTime 'Create Date Time'
     * @DbComment PRG_ASSIGADDDATE.modifyUserId 'Modify User Id'
     * @DbComment PRG_ASSIGADDDATE.modifyDateTime 'Modify Date Time'
     * @DbComment PRG_ASSIGADDDATE.invocationContext 'Invocation Context'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public AssigAddDateEntity() {
    }

    public Long getAssigAddDateId() {
        return assigAddDateId;
    }

    public void setAssigAddDateId(Long assigAddDateId) {
        this.assigAddDateId = assigAddDateId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public ProgramAssignmentEntity getProgramAssignment() {
        return programAssignment;
    }

    public void setProgramAssignment(ProgramAssignmentEntity programAssignment) {
        this.programAssignment = programAssignment;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (!(o instanceof AssigAddDateEntity)) {
			return false;
		}

        AssigAddDateEntity that = (AssigAddDateEntity) o;

        if (date != null ? !date.equals(that.date) : that.date != null) {
			return false;
		}
        if (programAssignment != null && programAssignment.getAssignmentId() != null ?
                !programAssignment.getAssignmentId().equals(that.programAssignment.getAssignmentId()) : that.programAssignment != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = date != null ? date.hashCode() : 0;
        result = 31 * result + (programAssignment != null && programAssignment.getAssignmentId() != null ? programAssignment.getAssignmentId().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AssigAddDateEntity{" +
                "assigAddDateId=" + assigAddDateId +
                ", date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
