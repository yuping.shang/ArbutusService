package syscon.arbutus.product.services.program.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Addition Date Entity for Program and Service
 *
 * @author YShang
 * @DbComment PRG_ADDITIONDATE 'Addition Date Entity for Program and Service'
 * @since July 16, 2014
 */
@Audited
@Entity
@Table(name = "PRG_ADDITIONDATE")
public class AdditionDateEntity implements Serializable {

    private static final long serialVersionUID = -2342629494335876940L;

    /**
     * @DbComment PRG_ADDITIONDATE.additionDateId 'Id of addition date to schedule recurrence pattern'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PRG_ADDITIONDATE_ID")
    @SequenceGenerator(name = "SEQ_PRG_ADDITIONDATE_ID", sequenceName = "SEQ_PRG_ADDITIONDATE_ID", allocationSize = 1)
    @Column(name = "additionDateId")
    private Long additionDateId;

    /**
     * @DbComment PRG_ADDITIONDATE.thedate 'addition date to schedule recurrence pattern'
     */
    @Column(name = "thedate", nullable = false)
    private Date date;

    /**
     * @DbComment PRG_ADDITIONDATE.startTime 'Start time (time only has meaning)'
     */
    @Column(name = "startTime")
    private Date startTime;

    /**
     * @DbComment PRG_ADDITIONDATE.endTime 'End time (time only has meaning)'
     */
    @Column(name = "endTime")
    private Date endTime;
    /**
     * @DbComment PRG_ADDITIONDATE.previousTime 'The schedule session previous time in String format(yyyymmddhhhh)'
     */
    @Column(name = "previousTime", length = 64)
    private String previousTime;

    /**
     * @DbComment PRG_ADDITIONDATE.previousStatus 'The schedule session previous status'
     */
    @Column(name = "previousStatus", length = 64)
    private String previousStatus;

    /**
     * @DbComment PRG_ADDITIONDATE.status 'The schedule session status'
     */
    @Column(name = "status", length = 64)
    private String status;
    /**
     * @DbComment PRG_ADDITIONDATE.offeringId 'Foreign key to PRG_PRGOFFER table'
     */
    @ForeignKey(name = "FK_PRG_ADDITIONDATE")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "offeringId", nullable = false)
    private ProgramOfferingEntity programOffering;

    /**
     * @DbComment PRG_ADDITIONDATE.createUserId 'Create User Id'
     * @DbComment PRG_ADDITIONDATE.createDateTime 'Create Date Time'
     * @DbComment PRG_ADDITIONDATE.modifyUserId 'Modify User Id'
     * @DbComment PRG_ADDITIONDATE.modifyDateTime 'Modify Date Time'
     * @DbComment PRG_ADDITIONDATE.invocationContext 'Invocation Context'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public AdditionDateEntity() {
    }

    public Long getAdditionDateId() {
        return additionDateId;
    }

    public void setAdditionDateId(Long additionDateId) {
        this.additionDateId = additionDateId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getPreviousTime() {
        return previousTime;
    }

    public void setPreviousTime(String previousTime) {
        this.previousTime = previousTime;
    }

    public String getPreviousStatus() {
        return previousStatus;
    }

    public void setPreviousStatus(String previousStatus) {
        this.previousStatus = previousStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProgramOfferingEntity getProgramOffering() {
        return programOffering;
    }

    public void setProgramOffering(ProgramOfferingEntity programOffering) {
        this.programOffering = programOffering;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
        result = 31 * result + (programOffering != null && programOffering.getOfferingId() != null ? programOffering.getOfferingId().hashCode() : 0);
        result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
        result = prime * result + ((previousTime == null) ? 0 : previousTime.hashCode());
        result = prime * result + ((previousStatus == null) ? 0 : previousStatus.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        AdditionDateEntity other = (AdditionDateEntity) obj;
        if (date == null) {
            if (other.date != null) {
				return false;
			}
        } else if (!date.equals(other.date)) {
			return false;
		}
        if (endTime == null) {
            if (other.endTime != null) {
				return false;
			}
        } else if (!endTime.equals(other.endTime)) {
			return false;
		}
        if (programOffering == null) {
            if (other.programOffering != null) {
				return false;
			}
        } else if (programOffering != null && programOffering.getOfferingId() != null ?
                !programOffering.getOfferingId().equals(((AdditionDateEntity) obj).programOffering.getOfferingId()) :
                ((AdditionDateEntity) obj).programOffering != null && ((AdditionDateEntity) obj).programOffering.getOfferingId() != null) {
			return false;
		}
        if (startTime == null) {
            if (other.startTime != null) {
				return false;
			}
        } else if (!startTime.equals(other.startTime)) {
			return false;
		}
        if (previousTime == null) {
            if (other.previousTime != null) {
				return false;
			}
        } else if (!previousTime.equals(other.previousTime)) {
			return false;
		}
        if (previousStatus == null) {
            if (other.previousStatus != null) {
				return false;
			}
        } else if (!previousStatus.equals(other.previousStatus)) {
			return false;
		}
        if (status == null) {
            if (other.status != null) {
				return false;
			}
        } else if (!status.equals(other.status)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "AdditionDateEntity{" +
                "additionDateId=" + additionDateId +
                ", date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", previousTime=" + previousTime +
                ", previousStatus=" + previousStatus +
                ", status=" + status +
                '}';
    }
}
