package syscon.arbutus.product.services.program.realization.persistence;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Program Assignment History Entity for Program and Service
 *
 * @author YShang
 * @DbComment PRG_ASSIGNMENTHIST 'Program Assignment Entity for Program and Service'
 * @since January 28, 2015
 */
@Audited
@Entity
@Table(name = "PRG_ASSIGNMENTHIST")
public class ProgramAssignmentHistoryEntity implements Serializable {

    private static final long serialVersionUID = 3184150856684251009L;

    /**
     * @DbComment PRG_ASSIGNMENTHIST.historyId 'Assignment History Id'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PRG_ASSIGNMENTHIST_ID")
    @SequenceGenerator(name = "SEQ_PRG_ASSIGNMENTHIST_ID", sequenceName = "SEQ_PRG_ASSIGNMENTHIST_ID", allocationSize = 1)
    private Long historyId;

    /**
     * @DbComment PRG_ASSIGNMENTHIST.assignmentId 'Assignment Id'
     */
    @Column(name = "assignmentId")
    private Long assignmentId;

    /**
     * @DbComment PRG_ASSIGNMENTHIST.referralDate 'Date on which the referral to a program offering was made. Only applies if a referral was made and the inmate was not directly placed in the program.'
     */
    private Date referralDate;

    /**
     * @DbComment PRG_ASSIGNMENTHIST.referralPriority 'A general priority for the referral. Only applies if a referral was made and the inmate was not directly placed in the program. Metadata: REFERRALPRIORITY: LOW, MEDIUM, HIGH'
     */
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.REFERRAL_PRIORITY)
    @Column(name = "referralPriority", nullable = true, length = 64)
    private String referralPriority;

    /**
     * @DbComment PRG_ASSIGNMENTHIST.referralRejectionReason 'If the program provider rejects the referral, indicate the reason. Only applies if a referral was made and the inmate was not directly placed in the program. Metadata: REFERRALREJECTIONREASON: NOAVAIL, NOTSUIT'
     */
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.REFERRAL_REJECTION_REASON)
    @Column(name = "referralRejectionReason", nullable = true, length = 64)
    private String referralRejectionReason;

    /**
     * @DbComment PRG_ASSIGNMENTHIST.startDate 'Date on which the inmate starts the program. Only applies when the inmate is placed in a program offering.'
     */
    private Date startDate;

    /**
     * @DbComment PRG_ASSIGNMENTHIST.endDate 'Date on which the inmate stops the program. Only applies when the inmate is placed in a program offering.'
     */
    private Date endDate;

    /**
     * @DbComment PRG_ASSIGNMENTHIST.status 'Indicates the current status of the assignment eg. Referred, Allocated, Completed. Metadata: PROGRAMASSIGNMENTSTATUS: ABANDONED, ALLOCATED, APPROVED, CANCELLED, COMPLETED, REFERRED, REJECTED, SUSPENDED'
     */
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.PROGRAM_ASSIGNMENT_STATUS)
    @Column(name = "status", nullable = true, length = 64)
    private String status;

    /**
     * @DbComment PRG_ASSIGNMENTHIST.suspendReason 'If the inmate is suspended from the program, indicate the reason. Metadata: PROGRAMSUSPENDREASON: DISPACT, HEALTH, RECLASS'
     */
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.PROGRAM_SUSPEND_REASON)
    @Column(name = "suspendReason", nullable = true, length = 64)
    private String suspendReason;

    /**
     * @DbComment PRG_ASSIGNMENTHIST.abandonReason 'If the inmate abandons the program, indicate the reason. Metadata: PROGRAMABANDONMENTREASON: NOTCOMP, RELEASE, TRANSFER'
     */
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.PROGRAM_ABANDONMENT_REASON)
    @Column(name = "abandonReason", nullable = true, length = 64)
    private String abandonReason;

    /**
     * Additional optional comments about the program assignment
     *
     * @DbComment PRG_ASSIGNMENTHIST.CommentUserId 'User ID who created/updated the comments'
     * @DbComment PRG_ASSIGNMENTHIST.CommentDate 'The Date/Time when the comment was created'
     * @DbComment PRG_ASSIGNMENTHIST.CommentText 'The comment text'
     */
    @Valid
    @Embedded
    private CommentEntity comments;

    /**
     * @DbComment PRG_ASSIGNMENTHIST.supervisionId 'Supervision Id. The inmate/supervision who is being referred to or placed in the program'
     */
    @Column(name = "supervisionId")
    private Long supervisionId;

    /**
     * @DbComment PRG_ASSIGNMENTHIST.processId 'Process Id to be applied by JBPM'
     */
    @Column(name = "processId", nullable = true)
    private Long processId;

    /**
     * @DbComment PRG_ASSIGNMENTHIST.version 'Version for Hibernate use'
     */
    @NotAudited
    @Column(name = "version")
    private Long version;

    /**
     * @DbComment PRG_ASSIGNMENTHIST.createUserId 'Create User Id'
     * @DbComment PRG_ASSIGNMENTHIST.createDateTime 'Create Date Time'
     * @DbComment PRG_ASSIGNMENTHIST.modifyUserId 'Modify User Id'
     * @DbComment PRG_ASSIGNMENTHIST.modifyDateTime 'Modify Date Time'
     * @DbComment PRG_ASSIGNMENTHIST.invocationContext 'Invocation Context'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public ProgramAssignmentHistoryEntity() {
        super();
    }

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public Long getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }

    public Date getReferralDate() {
        return referralDate;
    }

    public void setReferralDate(Date referralDate) {
        this.referralDate = referralDate;
    }

    public String getReferralPriority() {
        return referralPriority;
    }

    public void setReferralPriority(String referralPriority) {
        this.referralPriority = referralPriority;
    }

    public String getReferralRejectionReason() {
        return referralRejectionReason;
    }

    public void setReferralRejectionReason(String referralRejectionReason) {
        this.referralRejectionReason = referralRejectionReason;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSuspendReason() {
        return suspendReason;
    }

    public void setSuspendReason(String suspendReason) {
        this.suspendReason = suspendReason;
    }

    public String getAbandonReason() {
        return abandonReason;
    }

    public void setAbandonReason(String abandonReason) {
        this.abandonReason = abandonReason;
    }

    public CommentEntity getComments() {
        return comments;
    }

    public void setComments(CommentEntity comments) {
        this.comments = comments;
    }

    public Long getSupervisionId() {
        return supervisionId;
    }

    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    public Long getProcessId() {
        return processId;
    }

    public void setProcessId(Long processId) {
        this.processId = processId;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((assignmentId == null) ? 0 : assignmentId.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((historyId == null) ? 0 : historyId.hashCode());
        result = prime * result + ((referralDate == null) ? 0 : referralDate.hashCode());
        result = prime * result + ((referralPriority == null) ? 0 : referralPriority.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ProgramAssignmentHistoryEntity other = (ProgramAssignmentHistoryEntity) obj;
        if (assignmentId == null) {
            if (other.assignmentId != null) {
				return false;
			}
        } else if (!assignmentId.equals(other.assignmentId)) {
			return false;
		}
        if (endDate == null) {
            if (other.endDate != null) {
				return false;
			}
        } else if (!endDate.equals(other.endDate)) {
			return false;
		}
        if (historyId == null) {
            if (other.historyId != null) {
				return false;
			}
        } else if (!historyId.equals(other.historyId)) {
			return false;
		}
        if (referralDate == null) {
            if (other.referralDate != null) {
				return false;
			}
        } else if (!referralDate.equals(other.referralDate)) {
			return false;
		}
        if (referralPriority == null) {
            if (other.referralPriority != null) {
				return false;
			}
        } else if (!referralPriority.equals(other.referralPriority)) {
			return false;
		}
        if (startDate == null) {
            if (other.startDate != null) {
				return false;
			}
        } else if (!startDate.equals(other.startDate)) {
			return false;
		}
        if (status == null) {
            if (other.status != null) {
				return false;
			}
        } else if (!status.equals(other.status)) {
			return false;
		}
        if (supervisionId == null) {
            if (other.supervisionId != null) {
				return false;
			}
        } else if (!supervisionId.equals(other.supervisionId)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "ProgramAssignmentHistoryEntity [historyId=" + historyId + ", assignmentId=" + assignmentId + ", referralDate=" + referralDate + ", referralPriority="
                + referralPriority + ", referralRejectionReason=" + referralRejectionReason + ", startDate=" + startDate + ", endDate=" + endDate + ", status=" + status
                + ", suspendReason=" + suspendReason + ", abandonReason=" + abandonReason + ", comments=" + comments + ", supervisionId=" + supervisionId + ", processId="
                + processId + "]";
    }

}
