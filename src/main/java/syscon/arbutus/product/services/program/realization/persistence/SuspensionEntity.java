package syscon.arbutus.product.services.program.realization.persistence;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

@Audited
@Entity
@Table(name = "PRG_SUSPENSION")
public class SuspensionEntity implements Serializable {

    private static final long serialVersionUID = 5541607428126184459L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PRG_SUSPENSION_ID")
    @SequenceGenerator(name = "SEQ_PRG_SUSPENSION_ID", sequenceName = "SEQ_PRG_SUSPENSION_ID", allocationSize = 1)
    @Column(name = "suspensionId")
    private Long id;

    @Column(name = "suspendedDate", nullable = false)
    private Date suspendDate;

    @Column(name = "resumeDate", nullable = true)
    private Date resumeDate;

    /**
     * @DbComment PRG_ASSIGNMENT.suspendReason 'If the inmate is suspended from the program, indicate the reason. Metadata: PROGRAMSUSPENDREASON: DISPACT, HEALTH, RECLASS'
     */
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.PROGRAM_SUSPEND_REASON)
    @Column(name = "suspendReason", nullable = true, length = 64)
    private String suspendReason;

    /**
     * Additional optional comments about the program assignment
     *
     * @DbComment PRG_ASSIGNMENT.CommentUserId 'User ID who created/updated the comments'
     * @DbComment PRG_ASSIGNMENT.CommentDate 'The Date/Time when the comment was created'
     * @DbComment PRG_ASSIGNMENT.CommentText 'The comment text'
     */
    @Valid
    @Embedded
    private CommentEntity comments;

    @ForeignKey(name = "FK_PRG_SUSPENSION")
    @ManyToOne
    @JoinColumn(name = "assignmentId", nullable = false)
    private ProgramAssignmentEntity programAssignment;

    /**
     * @DbComment PRG_ASSIGNMENT.createUserId 'Create User Id'
     * @DbComment PRG_ASSIGNMENT.createDateTime 'Create Date Time'
     * @DbComment PRG_ASSIGNMENT.modifyUserId 'Modify User Id'
     * @DbComment PRG_ASSIGNMENT.modifyDateTime 'Modify Date Time'
     * @DbComment PRG_ASSIGNMENT.invocationContext 'Invocation Context'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public SuspensionEntity() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getSuspendDate() {
        return suspendDate;
    }

    public void setSuspendDate(Date suspendDate) {
        this.suspendDate = suspendDate;
    }

    public Date getResumeDate() {
        return resumeDate;
    }

    public void setResumeDate(Date resumeDate) {
        this.resumeDate = resumeDate;
    }

    public String getSuspendReason() {
        return suspendReason;
    }

    public void setSuspendReason(String suspendReason) {
        this.suspendReason = suspendReason;
    }

    public CommentEntity getComments() {
        return comments;
    }

    public void setComments(CommentEntity comments) {
        this.comments = comments;
    }

    public ProgramAssignmentEntity getProgramAssignment() {
        return programAssignment;
    }

    public void setProgramAssignment(ProgramAssignmentEntity programAssignment) {
        this.programAssignment = programAssignment;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((suspendDate == null) ? 0 : suspendDate.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SuspensionEntity other = (SuspensionEntity) obj;
        if (suspendDate == null) {
            if (other.suspendDate != null) {
				return false;
			}
        } else if (!suspendDate.equals(other.suspendDate)) {
			return false;
		}
        if (id == null) {
            if (other.id != null) {
				return false;
			}
        } else if (!id.equals(other.id)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "SuspensionEntity [id=" + id + ", suspendDate=" + suspendDate + ", resumeDate=" + resumeDate + ", suspendReason=" + suspendReason + ", comments="
                + comments + "]";
    }

}
