package syscon.arbutus.product.services.program.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

/**
 * Addition Date Type for Program and Service
 *
 * @author YShang
 * @since July 16, 2014
 */
@ArbutusConstraint(constraints = { "startTime <= endTime" })
public class ScheduleSessionDateType implements Serializable, Comparable<ScheduleSessionDateType> {

    private static final long serialVersionUID = -3482933900718273323L;

    private Long entityId;
    /**
     * The date to be included from Recurrenc Pattern
     */
    @NotNull
    private LocalDate date;

    /**
     * Start time (time only has meaning)
     */
    private LocalTime startTime;

    /**
     * End time (time only has meaning)
     */
    private LocalTime endTime;
    private String previousTime;
    private String previousStatus;
    private String status;

    public ScheduleSessionDateType() {
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    /**
     * The date to be included from Recurrenc Pattern
     *
     * @return LocalDate
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * The date to be included from Recurrenc Pattern
     *
     * @param date LocalDate
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * Start time (time only has meaning)
     *
     * @return LocalTime
     */
    public LocalTime getStartTime() {
        return startTime;
    }

    /**
     * Start time (time only has meaning)
     *
     * @param startTime LocalTime
     */
    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    /**
     * End time (time only has meaning)
     *
     * @return LocalTime
     */
    public LocalTime getEndTime() {
        return endTime;
    }

    /**
     * End time (time only has meaning)
     *
     * @param endTime LocalTime
     */
    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    /**
     * Get the previous time of session in String format(yyyymmddhhhh)
     *
     * @return String
     */
    public String getPreviousTime() {
        return previousTime;
    }

    /**
     * Set the previous time of session in String format(yyyymmddhhhh)
     *
     * @param previousTime String
     */
    public void setPreviousTime(String previousTime) {
        this.previousTime = previousTime;
    }

    /**
     * Get the previous status of session
     *
     * @return String
     */
    public String getPreviousStatus() {
        return previousStatus;
    }

    /**
     * Set the previous status of session
     *
     * @param previousStatus String
     */
    public void setPreviousStatus(String previousStatus) {
        this.previousStatus = previousStatus;
    }

    /**
     * Get the session status
     *
     * @return String
     */
    public String getStatus() {
        return status;
    }

    /**
     * Set the session status
     *
     * @param status String
     */
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int compareTo(ScheduleSessionDateType o) {
        return this.date.compareTo(o.date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (!(o instanceof ScheduleSessionDateType)) {
			return false;
		}

        ScheduleSessionDateType that = (ScheduleSessionDateType) o;

        if (date != null ? !date.equals(that.date) : that.date != null) {
			return false;
		}
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) {
			return false;
		}
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = date != null ? date.hashCode() : 0;
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ScheduleSessionDateType{" +
                "entityId=" + entityId +
                ", date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", previousTime='" + previousTime + '\'' +
                ", previousStatus='" + previousStatus + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
