package syscon.arbutus.product.services.program.contract.dto;

/**
 * Reference Set for Programs and Services
 *
 * @author YShang
 * @since April 7, 2014
 */
public enum ReferenceSet {
    /**
     * Program Category -- PROGRAMCATEGORY: ACCRED, COMM, INSTITUT
     */
    ProgramCategory,

    /**
     * Program Termination Reason -- PROGRAMTERMINATIONREASON: NOTOFFERED
     */
    ProgramTerminationReason,

    /**
     * Offering Termination Reason -- OFFERINGTERMINATIONREASON: CANCEL, NOTAVAIL
     */
    OfferingTerminationReason,

    /**
     * Referral Rejection Reason -- REFERRALREJECTIONREASON: NOAVAIL, NOTSUIT
     */
    ReferralRejectionReason,

    /**
     * Program Suspend Reason -- PROGRAMSUSPENDREASON: DISPACT, HEALTH, RECLASS
     */
    ProgramSuspendReason,

    /**
     * Program Abandonment Reason -- PROGRAMABANDONMENTREASON: NOTCOMP, RELEASE, TRANSFER
     */
    ProgramAbandonmentReason,

    /**
     * Program Assignment Status -- PROGRAMASSIGNMENTSTATUS: ABANDONED, ALLOCATED, APPROVED, CANCELLED, COMPLETED, REFERRED, REJECTED, SUSPENDED
     */
    ProgramAssignmentStatus,

    /**
     * Program Referral Decision -- PROGRAMREFERRALDECISION: Approve, Reject
     */
    ProgramReferralDecision,

    /**
     * Program Performance -- PROGRAMPERFORMANCE: ASLEEP, EXPECTED, INTOX, LATE, NOPART
     */
    ProgramPerformance,

    /**
     * Referral Priority -- REFERRALPRIORITY: LOW, MEDIUM, HIGH
     */
    ReferralPriority,
    /**
     * Offender Gender -- OFFENDERGENDER: F, M U
     */
    OFFENDERGENDER,
    /**
     * Age Range -- AGRRANGE: ADULT, JUVENILE
     */
    AGERANGE,
    /**
     * Amenity -- LOCATIONPROPERTY: HSENS, SHOWER, WHEELCHAIR
     */
    AMENITY,

    /**
     * Program Work Step -- CASEWORKSTEPS: AA, CTU, JOB, PRGM
     */
    CaseWorkSteps,

    /**
     * ProgramAttendanceRecord -- Attendance Record: Yes, No, Not Recorded
     */
    ProgramAttendanceRecord;

    public static ReferenceSet fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }
}
