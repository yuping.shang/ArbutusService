package syscon.arbutus.product.services.program.contract.dto;

import java.io.Serializable;
import java.util.List;

/**
 * ProgramAssignmentReturnType for Program and Services
 * <p>The return of the Program Assignment from method call searchProgramAssignment</p>
 *
 * @author YShang
 * @since May 27, 2014
 */
public class ProgramAssignmentReturnType implements Serializable {
    private static final long serialVersionUID = 488729741678361980L;

    /**
     * A list of Program Assignments from method call searchProgramAssignment
     */
    private List<ProgramAssignmentType> programAssignments;
    /**
     * Total size of the returning from method call searchProgramAssignment
     */
    private Long totalSize;

    /**
     * A list of Program Assignments from method call searchProgramAssignment
     *
     * @return List&lt;ProgramAssignmentType>
     */
    public List<ProgramAssignmentType> getProgramAssignments() {
        return programAssignments;
    }

    /**
     * A list of Program Assignments from method call searchProgramAssignment
     *
     * @param programAssignments List&lt;ProgramAssignmentType>
     */
    public void setProgramAssignments(List<ProgramAssignmentType> programAssignments) {
        this.programAssignments = programAssignments;
    }

    /**
     * Total size of the returning from method call searchProgramAssignment
     *
     * @return Long
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * Total size of the returning from method call searchProgramAssignment
     *
     * @param totalSize Long
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    @Override
    public String toString() {
        return "ProgramAssignmentReturnType{" +
                "programAssignments=" + programAssignments +
                ", totalSize=" + totalSize +
                '}';
    }
}
