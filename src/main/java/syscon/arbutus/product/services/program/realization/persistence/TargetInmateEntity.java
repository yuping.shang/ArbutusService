package syscon.arbutus.product.services.program.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Target Inmate for Program and Service
 *
 * @author YShang
 * @DbComment PRG_TgInmate 'Target Inmate for Program and Service'
 * @since April 6, 2014
 */
@Audited
@Entity
@Table(name = "PRG_TgInmate")
public class TargetInmateEntity implements Serializable {

    private static final long serialVersionUID = -681654063809363323L;

    /**
     * @DbComment PRG_TgInmate.targetInmateId 'Target Inmate Id'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PRG_TGINMATE_ID")
    @SequenceGenerator(name = "SEQ_PRG_TGINMATE_ID", sequenceName = "SEQ_PRG_TGINMATE_ID", allocationSize = 1)
    @Column(name = "targetInmateId")
    private Long targetInmateId;

    /**
     * Indicates which genders are particularly suitable for the program. <p>Note that multiple values may apply.
     */
    @OneToMany(mappedBy = "inmate", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<GenderEntity> genders;

    /**
     * Indicates which age ranges are particularly suitable for the program. Note that multiple values may apply.
     */
    @OneToMany(mappedBy = "inmate", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<AgeGroupEntity> ageRanges;

    /**
     * Indicates what special amenities are available at the program location
     * (same as Location Properties in Facility Internal Location).
     * This applies more at the program offering level
     * but can be set at the program level as well.
     * <p>Note that multiple values may apply.
     */
    @OneToMany(mappedBy = "inmate", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<AmenityEntity> amenities;

    /**
     * @DbComment PRG_TgInmate.programId 'Program for which the target attributes are being set'
     */
    @ForeignKey(name = "FK_PRG_TgInmate01")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "programId", nullable = true)
    private ProgramEntity program;

    /**
     * @DbComment PRG_TgInmate.offeringId 'Program offering for which the target attributes are being set'
     */
    @ForeignKey(name = "FK_PRG_TgInmate02")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "offeringId", nullable = true)
    private ProgramOfferingEntity programOffering;

    /**
     * @DbComment PRG_TgInmate.createUserId 'Create User Id'
     * @DbComment PRG_TgInmate.createDateTime 'Create Date Time'
     * @DbComment PRG_TgInmate.modifyUserId 'Modify User Id'
     * @DbComment PRG_TgInmate.modifyDateTime 'Modify Date Time'
     * @DbComment PRG_TgInmate.invocationContext 'Invocation Context'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public TargetInmateEntity() {
        super();
    }

    public Long getTargetInmateId() {
        return targetInmateId;
    }

    public void setTargetInmateId(Long targetInmateId) {
        this.targetInmateId = targetInmateId;
    }

    public Set<GenderEntity> getGenders() {
        if (genders == null) {
			genders = new HashSet<>();
		}
        return genders;
    }

    public void setGenders(Set<GenderEntity> genders) {
        this.genders = genders;
    }

    public void addGender(GenderEntity gender) {
        gender.setInmate(this);
        this.getGenders().add(gender);
    }

    public Set<AgeGroupEntity> getAgeRanges() {
        if (ageRanges == null) {
			ageRanges = new HashSet<>();
		}
        return ageRanges;
    }

    public void setAgeRanges(Set<AgeGroupEntity> ageRanges) {
        this.ageRanges = ageRanges;
    }

    public void addAgeRange(AgeGroupEntity ageRange) {
        ageRange.setInmate(this);
        this.getAgeRanges().add(ageRange);
    }

    public Set<AmenityEntity> getAmenities() {
        if (amenities == null) {
			amenities = new HashSet<>();
		}
        return amenities;
    }

    public void setAmenities(Set<AmenityEntity> amenities) {
        this.amenities = amenities;
    }

    public void addAmenity(AmenityEntity amenity) {
        amenity.setInmate(this);
        this.getAmenities().add(amenity);
    }

    public ProgramEntity getProgram() {
        return program;
    }

    public void setProgram(ProgramEntity program) {
        this.program = program;
    }

    public ProgramOfferingEntity getProgramOffering() {
        return programOffering;
    }

    public void setProgramOffering(ProgramOfferingEntity programOffering) {
        this.programOffering = programOffering;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ageRanges == null) ? 0 : ageRanges.hashCode());
        result = prime * result + ((amenities == null) ? 0 : amenities.hashCode());
        result = prime * result + ((genders == null) ? 0 : genders.hashCode());
        result = prime * result + ((targetInmateId == null) ? 0 : targetInmateId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        TargetInmateEntity other = (TargetInmateEntity) obj;
        if (ageRanges == null) {
            if (other.ageRanges != null) {
				return false;
			}
        } else if (!ageRanges.equals(other.ageRanges)) {
			return false;
		}
        if (amenities == null) {
            if (other.amenities != null) {
				return false;
			}
        } else if (!amenities.equals(other.amenities)) {
			return false;
		}
        if (genders == null) {
            if (other.genders != null) {
				return false;
			}
        } else if (!genders.equals(other.genders)) {
			return false;
		}
        if (targetInmateId == null) {
            if (other.targetInmateId != null) {
				return false;
			}
        } else if (!targetInmateId.equals(other.targetInmateId)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "TargetInmateEntity [targetInmateId=" + targetInmateId + ", genders=" + genders + ", ageRanges=" + ageRanges + ", amenities=" + amenities + "]";
    }
}
