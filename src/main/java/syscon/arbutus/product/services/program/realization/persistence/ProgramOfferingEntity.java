package syscon.arbutus.product.services.program.realization.persistence;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Program Offering Entity for Program and Service
 *
 * @author YShang
 * @DbComment PRG_PRGOFFER 'Program Offering Entity for Program and Service'
 * @since April 6, 2014
 */
@Audited
@Entity
@Table(name = "PRG_PRGOFFER")
@SQLDelete(sql = "UPDATE PRG_PRGOFFER SET flag = 4 WHERE offeringId = ? and version = ?")
@Where(clause = "flag = 1")
public class ProgramOfferingEntity implements Serializable {

    private static final long serialVersionUID = -545312764862517427L;

    /**
     * @DbComment PRG_PRGOFFER.offeringId 'Program Offering Id'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PRG_PRGOFFER_ID")
    @SequenceGenerator(name = "SEQ_PRG_PRGOFFER_ID", sequenceName = "SEQ_PRG_PRGOFFER_ID", allocationSize = 1)
    @Column(name = "offeringId")
    private Long offeringId;

    /**
     * @DbComment PRG_PRGOFFER.offeringCode 'The code of the specific program offering eg. K200'
     */
    @NotNull
    @Size(max = 64, message = "max length 64")
    @Column(name = "offeringCode", nullable = false, length = 64)
    private String offeringCode;

    /**
     * @DbComment PRG_PRGOFFER.offeringDescription 'The description of the specific offering eg. Pan Wash'
     */
    @NotNull
    @Size(max = 64, message = "max length 64")
    @Column(name = "offeringDescription", nullable = false, length = 64)
    private String offeringDescription;

    /**
     * @DbComment PRG_PRGOFFER.startDate 'Date on which the program offering starts'
     */
    @NotNull
    @Column(name = "startDate", nullable = false)
    private Date startDate;

    /**
     * @DbComment PRG_PRGOFFER.endDate 'Date on which the program offering ends'
     */
    @Column(name = "endDate")
    private Date endDate;

    /**
     * @DbComment PRG_PRGOFFER.expiryDate 'Date on which the program offering is terminated'
     */
    @Column(name = "expiryDate")
    private Date expiryDate;

    /**
     * @DbComment PRG_PRGOFFER.capacity 'The number of spaces available in the program offering'
     */
    @NotNull
    @Column(name = "capacity", nullable = false)
    private Long capacity;

    /**
     * @DbComment PRG_PRGOFFER.vacancy 'Derived based on Capacity minus the number of inmates allocated to the program offering.'
     */
    @Column(name = "vacancy")
    private Long vacancy;

    /**
     * @DbComment PRG_PRGOFFER.participationFee 'The cost of participating in the program offering'
     */
    @Column(name = "participationFee", nullable = true, precision = 12, scale = 2)
    private BigDecimal participationFee;

    /**
     * @DbComment PRG_PRGOFFER.singleOccurrence 'Indicates whether the program is a one off session (yes) or has a repeating schedule (no).'
     */
    @NotNull
    @Column(name = "singleOccurrence")
    private Boolean singleOccurrence;

    /**
     * @DbComment PRG_PRGOFFER.generateSchedule 'If true then a schedule will be generated for the inmate when they are allocated to the program. If false then no schedule will be generated.'
     */
    @NotNull
    private Boolean generateSchedule;

    /**
     * @DbComment PRG_PRGOFFER.scheduleId 'Schedule Id'
     */
    private Long scheduleId;

    /**
     * @DbComment PRG_PRGOFFER.days 'Indicate the days when the program runs'
     */
    @Column(name = "days")
    private Long day;

    /**
     * @DbComment PRG_PRGOFFER.theDate 'The date on which the program runs. This only applies if a schedule must be generated.'
     */
    @Column(name = "theDate")
    private Date date;

    /**
     * @DbComment PRG_PRGOFFER.startTime 'Start time of the program session'
     */
    private Date startTime;

    /**
     * @DbComment PRG_PRGOFFER.endTime 'End time of the program session'
     */
    private Date endTime;

    @OneToMany(mappedBy = "programOffering", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @BatchSize(size = 20)
    private Set<AdditionDateEntity> addedDates;

    @OneToMany(mappedBy = "programOffering", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @BatchSize(size = 20)
    private Set<ModifiedPatternDateEntity> modifiedDates;

    /**
     * @DbComment PRG_PRGOFFER.locationDescription 'The description of the room where the program is being run. This is used for non-institutional programs.'
     */
    @Size(max = 512, message = "max length 512")
    @Column(length = 512)
    private String locationDescription;

    /**
     * @DbComment PRG_PRGOFFER.terminationReason 'If the program offering is terminated, indicate the reason. Metadata: OFFERINGTERMINATIONREASON: CANCEL, NOTAVAIL'
     */
    @MetaCode(set = MetaSet.OFFERING_TERMINATION_REASON)
    @Size(max = 64, message = "max length 64")
    @Column(length = 64)
    private String terminationReason;

    /**
     * Additional optional comments about the program offering.
     *
     * @DbComment PRG_PRGOFFER.CommentUserId 'User ID who created/updated the comments'
     * @DbComment PRG_PRGOFFER.CommentDate 'The Date/Time when the comment was created'
     * @DbComment PRG_PRGOFFER.CommentText 'The comment text'
     */
    @Embedded
    private CommentEntity comments;

    /**
     * @DbComment PRG_PRGOFFER.organizationId 'The organization that is the provider of the program.'
     */
    @NotNull
    private Long organizationId;

    /**
     * @DbComment PRG_PRGOFFER.programId 'Foreign Key of The program which is being offered.'
     */
    @ForeignKey(name = "Fk_PRG_PRGOFFER")
    @OneToOne
    @JoinColumn(name = "programId")
    private ProgramEntity program;

    /**
     * @DbComment PRG_PRGOFFER.personId 'Person (non-staff) who is delivering the program (if applicable)'
     */
    private Long personId;

    /**
     * @DbComment PRG_PRGOFFER.isStaff 'True: is staff; False/Null: is not staff'
     */
    private Boolean isStaff;

    /**
     * @DbComment PRG_PRGOFFER.facilityId 'The facility where the program is being offered'
     */
    private Long facilityId;

    /**
     * @DbComment PRG_PRGOFFER.locationId 'Location Id, used for non-institutional programs. The address where the program is being offered if not at a facility'
     */
    private Long locationId;

    /**
     * @DbComment PRG_PRGOFFER.interanlLocationId 'Used only for institutional programs. Indicates the actual internal location in the custodial facility where the program is being run.'
     */
    private Long interanlLocationId;

    /**
     * The attributes of the inmates targeted for the program offering.
     * The attributes for the program offering are inherited from the program
     * but can be modified for the specific offering.
     */
    @OneToMany(mappedBy = "programOffering", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<TargetInmateEntity> targetInmates;

    @BatchSize(size = 20)
    @OneToMany(mappedBy = "programOffering", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
    private Set<ProgramAssignmentEntity> programAssignments;

    /**
     * @DbComment PRG_PRGOFFER.flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * @DbComment PRG_PRGOFFER.version 'Version for Hibernate use'
     */
    @NotAudited
    @Version
    @Column(name = "version")
    private Long version;

    /**
     * @DbComment PRG_PRGOFFER.createUserId 'Create User Id'
     * @DbComment PRG_PRGOFFER.createDateTime 'Create Date Time'
     * @DbComment PRG_PRGOFFER.modifyUserId 'Modify User Id'
     * @DbComment PRG_PRGOFFER.modifyDateTime 'Modify Date Time'
     * @DbComment PRG_PRGOFFER.invocationContext 'Invocation Context'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public ProgramOfferingEntity() {
        super();
    }

    public Long getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(Long offeringId) {
        this.offeringId = offeringId;
    }

    public String getOfferingCode() {
        return offeringCode;
    }

    public void setOfferingCode(String offeringCode) {
        this.offeringCode = offeringCode;
    }

    public String getOfferingDescription() {
        return offeringDescription;
    }

    public void setOfferingDescription(String offeringDescription) {
        this.offeringDescription = offeringDescription;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Long getCapacity() {
        return capacity;
    }

    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    public Long getVacancy() {
        return vacancy;
    }

    public void setVacancy(Long vacancy) {
        this.vacancy = vacancy;
    }

    public BigDecimal getParticipationFee() {
        return participationFee;
    }

    public void setParticipationFee(BigDecimal participationFee) {
        this.participationFee = participationFee;
    }

    public Boolean getSingleOccurrence() {
        return singleOccurrence;
    }

    public void setSingleOccurrence(Boolean singleOccurrence) {
        this.singleOccurrence = singleOccurrence;
    }

    public Boolean getGenerateSchedule() {
        return generateSchedule;
    }

    public void setGenerateSchedule(Boolean generateSchedule) {
        this.generateSchedule = generateSchedule;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Long getDay() {
        return day;
    }

    public void setDay(Long day) {
        this.day = day;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Set<AdditionDateEntity> getAddedDates() {
        if (addedDates == null) {
            addedDates = new HashSet<>();
        }
        return addedDates;
    }

    public void setAddedDates(Set<AdditionDateEntity> addedDates) {
        this.addedDates = addedDates;
    }

    public void addAddedDate(AdditionDateEntity additionDate) {
        if (additionDate != null) {
            additionDate.setProgramOffering(this);
            getAddedDates().add(additionDate);
        }
    }

    public Set<ModifiedPatternDateEntity> getModifiedDates() {
        if (modifiedDates == null) {
            modifiedDates = new HashSet<>();
        }
        return modifiedDates;
    }

    public void setModifiedDates(Set<ModifiedPatternDateEntity> modifiedDates) {
        this.modifiedDates = modifiedDates;
    }

    public void addModifiedDate(ModifiedPatternDateEntity modifiedDate) {
        if (modifiedDate != null) {
            modifiedDate.setProgramOffering(this);
            getModifiedDates().add(modifiedDate);
        }
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getTerminationReason() {
        return terminationReason;
    }

    public void setTerminationReason(String terminationReason) {
        this.terminationReason = terminationReason;
    }

    public CommentEntity getComments() {
        return comments;
    }

    public void setComments(CommentEntity comments) {
        this.comments = comments;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public ProgramEntity getProgram() {
        return program;
    }

    public void setProgram(ProgramEntity program) {
        this.program = program;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Boolean getIsStaff() {
        return isStaff;
    }

    public void setIsStaff(Boolean isStaff) {
        this.isStaff = isStaff;
    }

    public Long getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public Long getInteranlLocationId() {
        return interanlLocationId;
    }

    public void setInteranlLocationId(Long interanlLocationId) {
        this.interanlLocationId = interanlLocationId;
    }

    public Set<TargetInmateEntity> getTargetInmates() {
        if (targetInmates == null) {
			targetInmates = new HashSet<>();
		}
        return targetInmates;
    }

    public void setTargetInmates(Set<TargetInmateEntity> targetInmates) {
        this.targetInmates = targetInmates;
    }

    public void addTargetInmate(TargetInmateEntity targetInmate) {
        targetInmate.setProgramOffering(this);
        getTargetInmates().add(targetInmate);
    }

    public Set<ProgramAssignmentEntity> getProgramAssignments() {
        if (programAssignments == null) {
            programAssignments = new HashSet<>();
        }
        return programAssignments;
    }

    public void setProgramAssignments(Set<ProgramAssignmentEntity> programAssignments) {
        this.programAssignments = programAssignments;
    }

    public void addProgramAssignments(ProgramAssignmentEntity programAssignment) {
        programAssignment.setProgramOffering(this);
        getProgramAssignments().add(programAssignment);
    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((capacity == null) ? 0 : capacity.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
        result = prime * result + ((expiryDate == null) ? 0 : expiryDate.hashCode());
        result = prime * result + ((facilityId == null) ? 0 : facilityId.hashCode());
        result = prime * result + ((generateSchedule == null) ? 0 : generateSchedule.hashCode());
        result = prime * result + ((interanlLocationId == null) ? 0 : interanlLocationId.hashCode());
        result = prime * result + ((locationDescription == null) ? 0 : locationDescription.hashCode());
        result = prime * result + ((locationId == null) ? 0 : locationId.hashCode());
        result = prime * result + ((offeringCode == null) ? 0 : offeringCode.hashCode());
        result = prime * result + ((offeringDescription == null) ? 0 : offeringDescription.hashCode());
        result = prime * result + ((organizationId == null) ? 0 : organizationId.hashCode());
        result = prime * result + ((personId == null) ? 0 : personId.hashCode());
        result = prime * result + ((isStaff == null) ? 0 : isStaff.hashCode());
        result = prime * result + ((program == null) ? 0 : program.hashCode());
        result = prime * result + ((offeringId == null) ? 0 : offeringId.hashCode());
        result = prime * result + ((singleOccurrence == null) ? 0 : singleOccurrence.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
        result = prime * result + ((terminationReason == null) ? 0 : terminationReason.hashCode());
        result = prime * result + ((vacancy == null) ? 0 : vacancy.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ProgramOfferingEntity other = (ProgramOfferingEntity) obj;
        if (capacity == null) {
            if (other.capacity != null) {
				return false;
			}
        } else if (!capacity.equals(other.capacity)) {
			return false;
		}
        if (endDate == null) {
            if (other.endDate != null) {
				return false;
			}
        } else if (!endDate.equals(other.endDate)) {
			return false;
		}
        if (endTime == null) {
            if (other.endTime != null) {
				return false;
			}
        } else if (!endTime.equals(other.endTime)) {
			return false;
		}
        if (expiryDate == null) {
            if (other.expiryDate != null) {
				return false;
			}
        } else if (!expiryDate.equals(other.expiryDate)) {
			return false;
		}
        if (facilityId == null) {
            if (other.facilityId != null) {
				return false;
			}
        } else if (!facilityId.equals(other.facilityId)) {
			return false;
		}
        if (generateSchedule == null) {
            if (other.generateSchedule != null) {
				return false;
			}
        } else if (!generateSchedule.equals(other.generateSchedule)) {
			return false;
		}
        if (interanlLocationId == null) {
            if (other.interanlLocationId != null) {
				return false;
			}
        } else if (!interanlLocationId.equals(other.interanlLocationId)) {
			return false;
		}
        if (locationDescription == null) {
            if (other.locationDescription != null) {
				return false;
			}
        } else if (!locationDescription.equals(other.locationDescription)) {
			return false;
		}
        if (locationId == null) {
            if (other.locationId != null) {
				return false;
			}
        } else if (!locationId.equals(other.locationId)) {
			return false;
		}
        if (offeringCode == null) {
            if (other.offeringCode != null) {
				return false;
			}
        } else if (!offeringCode.equals(other.offeringCode)) {
			return false;
		}
        if (offeringDescription == null) {
            if (other.offeringDescription != null) {
				return false;
			}
        } else if (!offeringDescription.equals(other.offeringDescription)) {
			return false;
		}
        if (organizationId == null) {
            if (other.organizationId != null) {
				return false;
			}
        } else if (!organizationId.equals(other.organizationId)) {
			return false;
		}
        if (personId == null) {
            if (other.personId != null) {
				return false;
			}
        } else if (!personId.equals(other.personId)) {
			return false;
		}
        if (isStaff == null) {
            if (other.isStaff != null) {
				return false;
			}
        } else if (!isStaff.equals(other.isStaff)) {
			return false;
		}
        if (program == null) {
            if (other.program != null) {
				return false;
			}
        } else if (!program.equals(other.program)) {
			return false;
		}
        if (offeringId == null) {
            if (other.offeringId != null) {
				return false;
			}
        } else if (!offeringId.equals(other.offeringId)) {
			return false;
		}
        if (singleOccurrence == null) {
            if (other.singleOccurrence != null) {
				return false;
			}
        } else if (!singleOccurrence.equals(other.singleOccurrence)) {
			return false;
		}
        if (startDate == null) {
            if (other.startDate != null) {
				return false;
			}
        } else if (!startDate.equals(other.startDate)) {
			return false;
		}
        if (startTime == null) {
            if (other.startTime != null) {
				return false;
			}
        } else if (!startTime.equals(other.startTime)) {
			return false;
		}
        if (terminationReason == null) {
            if (other.terminationReason != null) {
				return false;
			}
        } else if (!terminationReason.equals(other.terminationReason)) {
			return false;
		}
        if (vacancy == null) {
            if (other.vacancy != null) {
				return false;
			}
        } else if (!vacancy.equals(other.vacancy)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "ProgramOfferingEntity{" +
                "offeringId=" + offeringId +
                ", offeringCode='" + offeringCode + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", offeringDescription='" + offeringDescription + '\'' +
                ", expiryDate=" + expiryDate +
                ", capacity=" + capacity +
                ", vacancy=" + vacancy +
                ", participationFee=" + participationFee +
                ", singleOccurrence=" + singleOccurrence +
                ", generateSchedule=" + generateSchedule +
                ", scheduleId=" + scheduleId +
                ", day=" + day +
                ", date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", locationDescription='" + locationDescription + '\'' +
                ", terminationReason='" + terminationReason + '\'' +
                ", comments=" + comments +
                ", organizationId=" + organizationId +
                ", personId=" + personId +
                ", isStaff=" + isStaff +
                ", facilityId=" + facilityId +
                ", locationId=" + locationId +
                ", interanlLocationId=" + interanlLocationId +
                ", targetInmates=" + targetInmates +
                '}';
    }
}
