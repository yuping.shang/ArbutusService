package syscon.arbutus.product.services.program.contract.dto;

import java.util.Date;

import syscon.arbutus.product.services.person.contract.dto.personrelationship.PersonNonAssociation;

/**
 * Created by YShang on 20/08/2014.
 */
public class ConflictType extends PersonNonAssociation {

    /**
     * outcome -- ReferenceCode of ReferenceSet SUITABILITYDETERMINATIONOUTCOME
     */
    private String outcome;

    public ConflictType(Long personRelationshipId, Long mainPersonId){
        super(personRelationshipId,mainPersonId,null,null,null,null,null,null,null,null);
        this.outcome = outcome;
    }

    /**
     * outcome -- ReferenceCode of ReferenceSet SUITABILITYDETERMINATIONOUTCOME
     *
     * @return String ReferenceCode of ReferenceSet SUITABILITYDETERMINATIONOUTCOME
     */
    public String getOutcome() {
        return outcome;
    }

    /**
     * outcome -- ReferenceCode of ReferenceSet SUITABILITYDETERMINATIONOUTCOME
     *
     * @param outcome String ReferenceCode of ReferenceSet SUITABILITYDETERMINATIONOUTCOME
     */
    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    @Override
    public String toString() {
        return "ConflictType{" +
                "outcome='" + outcome + '\'' +
                "} " + super.toString();
    }
}
