package syscon.arbutus.product.services.program.contract.dto;

import java.io.Serializable;
import java.util.Set;

/**
 * ProgramSearchType for Program and Services
 * <p>Used for search Program</p>
 *
 * @author YShang
 * @since May 24, 2014
 */
public class ProgramSearchType implements Serializable {

    private static final long serialVersionUID = 8614990289199694406L;

    /**
     * Supervision Id
     */
    private Long supervisionId;

    private Boolean excludeSupervision;

    /**
     * The name of the program eg. Kitchen Duty
     */
    private String programDescription;

    /**
     * Indicates the general category that the program falls under
     * eg. Institutional Activity, Accredited Program, Community Service.
     * Different functionality will apply to each type of program within the system.
     * <p>Metadata -- PROGRAMCATEGORY: ACCRED, COMM, INSTITUT</p>
     */
    private String programCategory;

    /**
     * Gender -- Male; Female, Unknown
     */
    private Set<String> genders;

    /**
     * Age Range/Group: Adult; Juvenille
     */
    private Set<String> ageRanges;

    /**
     * Amentity: Wheelchaire Accessible,...
     */
    private Set<String> amenities;

    /**
     * Program Assignment Status -- Metadata: REFERRED; CANCELLED; APPROVED; REJECTED; ALLOCATED; SUSPENDED; ABANDONED; COMPLETED
     */
    private Set<String> programAssignmentStatus;

    /**
     * Derived based on the Expiry Date. It is active
     * if the current date is < Expiry Date. Otherwise it is inactive.
     */
    private String status;

    /**
     * Indicates whether an inmate can start the program part way
     * ie. after the Start Date of the offering has passed (yes) or not (no).
     */
    private Boolean flexibleStart;

    /**
     * If the program is terminated, indicate the reason.
     * <p>Required If Expiry Date is provided.
     * <p>Metadata: OFFERINGTERMINATIONREASON: CANCEL, NOTAVAIL
     */
    private String terminationReason;

    /**
     * Indicates the work step that the program falls under
     * eg. Attend AA/NA, CTU Referral, Job Placement, Program Attendance
     * Different functionality will apply to each type of program within the system.
     * <p>Metadata -- CASEWORKSTEPS: AA, CTU, JOB, PRGM</p>
     */
    private String programCaseWorkStep;

    public ProgramSearchType() {
    }

    /**
     * Supervision Id
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * Supervision Id
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Exclude the supervision
     */
    public Boolean getExcludeSupervision() {
        return excludeSupervision;
    }

    /**
     * Exclude the supervision
     */
    public void setExcludeSupervision(Boolean excludeSupervision) {
        this.excludeSupervision = excludeSupervision;
    }

    /**
     * The name of the program eg. Kitchen Duty
     */
    public String getProgramDescription() {
        return programDescription;
    }

    /**
     * The name of the program eg. Kitchen Duty
     */
    public void setProgramDescription(String programDescription) {
        this.programDescription = programDescription;
    }

    /**
     * Indicates the general category that the program falls under
     * eg. Institutional Activity, Accredited Program, Community Service.
     * Different functionality will apply to each type of program within the system.
     * <p>Metadata -- PROGRAMCATEGORY: ACCRED, COMM, INSTITUT</p>
     */
    public String getProgramCategory() {
        return programCategory;
    }

    /**
     * Indicates the general category that the program falls under
     * eg. Institutional Activity, Accredited Program, Community Service.
     * Different functionality will apply to each type of program within the system.
     * <p>Metadata -- PROGRAMCATEGORY: ACCRED, COMM, INSTITUT</p>
     */
    public void setProgramCategory(String programCategory) {
        this.programCategory = programCategory;
    }

    /**
     * @return programCaseWorkStep
     * * Indicates the work step that the program falls under
     * eg. Attend AA/NA, CTU Referral, Job Placement, Program Attendance
     * Different functionality will apply to each type of program within the system.
     * <p>Metadata -- CASEWORKSTEPS: AA, CTU, JOB, PRGM</p>
     */
    public String getProgramCaseWorkStep() {
        return programCaseWorkStep;
    }

    /**
     * @param programCaseWorkStep * Indicates the work step that the program falls under
     *                            eg. Attend AA/NA, CTU Referral, Job Placement, Program Attendance
     *                            Different functionality will apply to each type of program within the system.
     *                            <p>Metadata -- CASEWORKSTEPS: AA, CTU, JOB, PRGM</p>
     */
    public void setProgramCaseWorkStep(String programCaseWorkStep) {
        this.programCaseWorkStep = programCaseWorkStep;
    }

    /**
     * Gender -- Male; Female, Unknown
     */
    public Set<String> getGenders() {
        return genders;
    }

    /**
     * Gender -- Male; Female, Unknown
     */
    public void setGenders(Set<String> genders) {
        this.genders = genders;
    }

    /**
     * Age Range/Group: Adult; Juvenille
     */
    public Set<String> getAgeRanges() {
        return ageRanges;
    }

    /**
     * Age Range/Group: Adult; Juvenille
     */
    public void setAgeRanges(Set<String> ageRanges) {
        this.ageRanges = ageRanges;
    }

    /**
     * Amentity: Wheelchaire Accessible,...
     */
    public Set<String> getAmenities() {
        return amenities;
    }

    /**
     * Amentity: Wheelchaire Accessible,...
     */
    public void setAmenities(Set<String> amenities) {
        this.amenities = amenities;
    }

    /**
     * Program Assignment Status -- Metadata: REFERRED; CANCELLED; APPROVED; REJECTED; ALLOCATED; SUSPENDED; ABANDONED; COMPLETED
     */
    public Set<String> getProgramAssignmentStatus() {
        return programAssignmentStatus;
    }

    /**
     * Program Assignment Status -- Metadata: REFERRED; CANCELLED; APPROVED; REJECTED; ALLOCATED; SUSPENDED; ABANDONED; COMPLETED
     */
    public void setProgramAssignmentStatus(Set<String> programAssignmentStatus) {
        this.programAssignmentStatus = programAssignmentStatus;
    }

    /**
     * Derived based on the Expiry Date. It is active
     * if the current date is < Expiry Date. Otherwise it is inactive.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Derived based on the Expiry Date. It is active
     * if the current date is < Expiry Date. Otherwise it is inactive.
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Indicates whether an inmate can start the program part way
     * ie. after the Start Date of the offering has passed (yes) or not (no).
     */
    public Boolean getFlexibleStart() {
        return flexibleStart;
    }

    /**
     * Indicates whether an inmate can start the program part way
     * ie. after the Start Date of the offering has passed (yes) or not (no).
     */
    public void setFlexibleStart(Boolean flexibleStart) {
        this.flexibleStart = flexibleStart;
    }

    /**
     * If the program is terminated, indicate the reason.
     * <p>Required If Expiry Date is provided.
     * <p>Metadata: OFFERINGTERMINATIONREASON: CANCEL, NOTAVAIL
     */
    public String getTerminationReason() {
        return terminationReason;
    }

    /**
     * If the program is terminated, indicate the reason.
     * <p>Required If Expiry Date is provided.
     * <p>Metadata: OFFERINGTERMINATIONREASON: CANCEL, NOTAVAIL
     */
    public void setTerminationReason(String terminationReason) {
        this.terminationReason = terminationReason;
    }

    @Override
    public String toString() {
        return "ProgramSearchType [supervisionId=" + supervisionId + ", programDescription=" + programDescription + ", programCategory=" + programCategory + ", genders="
                + genders + ", ageRanges=" + ageRanges + ", amenities=" + amenities + ", status=" + status + ", flexibleStart=" + flexibleStart + ", terminationReason="
                + terminationReason + "]";
    }
}
