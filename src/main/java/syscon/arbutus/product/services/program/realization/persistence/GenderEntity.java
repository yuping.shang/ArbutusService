package syscon.arbutus.product.services.program.realization.persistence;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Gender Entity for Program and Service
 *
 * @author YShang
 * @DbComment PRG_GENDER 'Gender Entity for Program and Service'
 * @since April 6, 2014
 */
@Audited
@Entity
@Table(name = "PRG_GENDER")
public class GenderEntity implements Serializable {

    private static final long serialVersionUID = -7404008072053789236L;

    /**
     * @DbComment PRG_GENDER.genderId 'Gender Id'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PRG_GENDER_ID")
    @SequenceGenerator(name = "SEQ_PRG_GENDER_ID", sequenceName = "SEQ_PRG_GENDER_ID", allocationSize = 1)
    @Column(name = "genderId")
    private Long genderId;

    /**
     * @DbComment PRG_GENDER.gender 'Gender: Male, Female, Unknown,... Metadata: OFFENDERGENDER: F, M U'
     */
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.GENDER)
    @Column(name = "gender", nullable = true, length = 64)
    private String gender;

    /**
     * @DbComment PRG_GENDER.targetInmateId 'targetInmateId, foreign key of FK_PRG_GENDER table'
     */
    @ForeignKey(name = "FK_PRG_GENDER")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "targetInmateId", nullable = false)
    private TargetInmateEntity inmate;

    /**
     * @DbComment PRG_GENDER.createUserId 'Create User Id'
     * @DbComment PRG_GENDER.createDateTime 'Create Date Time'
     * @DbComment PRG_GENDER.modifyUserId 'Modify User Id'
     * @DbComment PRG_GENDER.modifyDateTime 'Modify Date Time'
     * @DbComment PRG_GENDER.invocationContext 'Invocation Context'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public GenderEntity() {
        super();
    }

    public Long getGenderId() {
        return genderId;
    }

    public void setGenderId(Long genderId) {
        this.genderId = genderId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public TargetInmateEntity getInmate() {
        return inmate;
    }

    public void setInmate(TargetInmateEntity inmate) {
        this.inmate = inmate;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((gender == null) ? 0 : gender.hashCode());
        result = prime * result + ((genderId == null) ? 0 : genderId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        GenderEntity other = (GenderEntity) obj;
        if (gender == null) {
            if (other.gender != null) {
				return false;
			}
        } else if (!gender.equals(other.gender)) {
			return false;
		}
        if (genderId == null) {
            if (other.genderId != null) {
				return false;
			}
        } else if (!genderId.equals(other.genderId)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "GenderEntity [genderId=" + genderId + ", gender=" + gender + "]";
    }
}
