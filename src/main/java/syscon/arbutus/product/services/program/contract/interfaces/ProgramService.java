package syscon.arbutus.product.services.program.contract.interfaces;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ScheduleTimeslotType;
import syscon.arbutus.product.services.activity.contract.dto.TimeslotIterator;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldMetaType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueSearchType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.DTOBindingTypeEnum;
import syscon.arbutus.product.services.program.contract.dto.*;

/**
 * The Interface of Program and Service
 * The programs and services module is used to enroll and track inmate participation
 * in various activities such as accredited programs to treat specific offending behavior,
 * work or educational programs and community service.
 * Users will be able to search through a catalogue of programs provided
 * by various organizations and refer inmates to program offerings.
 * An inmate is then placed in an offering and their progress tracked
 * either to completion or abandonment of the program.
 * Functionality Scope:
 * <li>Maintain authorized program providers</li>
 * <li>Maintain a service catalog of programs/courses/activities</li>
 * <li>Specify program fees that are associated to a program offering</li>
 * <li>Cancel a program session</li>
 * <li>Terminate a program offering</li>
 * <li>Terminate an entire program</li
 * <li>Refer an inmate to a program provider or offering</li>
 * <li>Accept or reject the referral</li>
 * <li>Place an inmate in a program offering</li>
 * <li>Maintain a waitlist of inmates requiring placement into a program</li>
 * <li>Record inmate program attendance</li>
 * <li>Track inmate progress in the program</li>
 * <li>Terminate inmate participation in a program</li>
 * <li>Generate a schedule of dates, times and locations for a program</li>
 *
 * @author YShang
 * @since April 6, 2014
 */
public interface ProgramService {

    /**
     * Create Program
     *
     * @param uc      UserContext Required
     * @param program ProgramType Required
     * @return ProgramType
     */
    ProgramType createProgram(UserContext uc, ProgramType program);

    /**
     * Get Program
     *
     * @param uc        UserContext Required
     * @param programId Long Required
     * @return ProgramType
     */
    ProgramType getProgram(UserContext uc, Long programId);

    /**
     * Get Program By Program Code
     *
     * @param uc          UserContext Required
     * @param programCode String Optional. If programCode is null then return null.
     * @return ProgramType
     */
    ProgramType getProgramByCode(UserContext uc, String programCode);

    /**
     * Update Program
     *
     * @param uc      UserContext Required
     * @param program ProgramType Required
     * @return ProgramType
     */
    ProgramType updateProgram(UserContext uc, ProgramType program);

    /**
     * Search Program
     *
     * @param uc          UserContext Required
     * @param programIds  List&lt;Long> optional, a list of programId
     * @param search      ProgramSearchType Required if programIds is not provided; Optional if programIds provided
     * @param startIndex  Long – Optional if null search returns at the start index
     * @param resultSize  Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder String – Optional if null search returns a default order
     * @return ProgramReturnType
     */
    ProgramReturnType searchProgram(UserContext uc, Set<Long> programIds, ProgramSearchType search, Long startIndex, Long resultSize, String resultOrder);

    /**
     * Get Program Codes -- All Codes and Code Descriptions
     *
     * @param uc              UserContext Required
     * @param programCategory String Optional Program Category
     * @return Map&lt;Long, Map&lt;String, String> where key: programId; Map: key -- Program Code; value -- Code Description
     */
    Map<Long, Map<String, String>> getProgramCodes(UserContext uc, String programCategory);

    /**
     * Delete Program
     *
     * @param uc        UserContext Required
     * @param programId Long Required
     */
    void deleteProgram(UserContext uc, Long programId);

    /**
     * To check if the provided Program Code is unique or not
     *
     * @param uc          UserContext Required
     * @param programCode String Required
     * @param programId   Long Required when update; null when create.
     * @return Boolean True: Duplicated; False: Unique.
     */
    Boolean isDuplicateProgram(UserContext uc, String programCode, Long... programId);

    /**
     * Create Program Offering
     *
     * @param uc              UserContext Required
     * @param programOffering ProgramOfferingType Required
     * @return ProgramOfferingType
     */
    ProgramOfferingType createProgramOffering(UserContext uc, ProgramOfferingType programOffering);

    /**
     * Get Program Offering
     *
     * @param uc                UserContext Required
     * @param programOfferingId Long Required
     * @return ProgramOfferingType
     */
    ProgramOfferingType getProgramOffering(UserContext uc, Long programOfferingId);

    /**
     * Get Program Offerings By SupervisionId
     *
     * @param uc            UserContext Required
     * @param supervisionId Long supervisionId
     * @return List&lt;ProgramOfferingType> a list of ProgramOfferingType
     */
    List<ProgramOfferingType> getProgramOfferingsBySupervisionId(UserContext uc, Long supervisionId);

    /**
     * Get Program Offering Vacancy -- Capacity minus the number of inmates allocated to the program offering.
     *
     * @param uc
     * @param programOfferingId
     * @return
     */
    Long getProgramOfferingVacancy(UserContext uc, Long programOfferingId);

    /**
     * Check if the program is already referred to the supervision
     *
     * @param uc            UserContext Required
     * @param supervisionId Long Optional supervisionId
     * @param programId     Long Required Program Id
     * @return Boolean True: Already Referred; False: Not Referred
     */
    Boolean isProgramReferredAlready(UserContext uc, Long supervisionId, Long programId);

    /**
     * Check if the program is already referred to the supervision
     *
     * @param uc                UserContext Required
     * @param programOfferingId Long Required Program Offering Id
     * @param supervisionId     Long Required supervisionId
     * @return Boolean True: Already Referred; False: Not Referred
     */
    Boolean isReferredAlready(UserContext uc, Long supervisionId, Long programOfferingId);

    /**
     * Check if the program is already assigned to the supervision
     *
     * @param uc                UserContext Required
     * @param supervisionId     Long optional supervisionId
     * @param programOfferingId Long Required Program Offering Id
     * @return Boolean True: Already Assigned; False: Not Assigned
     */
    Boolean isAssigedAlready(UserContext uc, Long supervisionId, Long programOfferingId);

    /**
     * Get Enrolled Supervisions for a Program Offering by programOfferingId
     *
     * @param uc                UserContext Required
     * @param programOfferingId Long Required, Program Offering ID
     * @param startIndex        Long – Optional if null search returns at the start index
     * @param resultSize        Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder       String – Optional if null search returns a default order
     * @return EnrolledSupervisionReturnType
     */
    EnrolledSupervisionReturnType getEnrolledSupervisions(UserContext uc, Long programOfferingId, Long startIndex, Long resultSize, String resultOrder);

    /**
     * Get Enrolled Supervisions for a Program Offering by programOfferingId
     *
     * @param uc                UserContext Required
     * @param programOfferingId Long Required, Program Offering ID
     * @return List&lt;EnrolledSupervisionType>
     */
    List<EnrolledSupervisionType> getEnrolledSupervisions(UserContext uc, Long programOfferingId);

    /**
     * Update Program Offering
     *
     * @param uc              UserContext Required
     * @param programOffering ProgramOfferingType Required
     * @return ProgramOfferingType
     */
    ProgramOfferingType updateProgramOffering(UserContext uc, ProgramOfferingType programOffering);

    /**
     * To check if the provided Program Offering Code is unique or not
     *
     * @param uc                  UserContext Required
     * @param programOfferingCode String Required - Program Offering Code
     * @param organizationId      Long Required - organizationId, The provider/organization that is the provider of the program
     * @param facilityId          Long Required - facilityId, The facility where the program is being offered
     * @param startDateTime       Date Required - Date on which the program offering starts
     * @param endDateTime         Date Required - Date on which the program offering ends
     * @param programOfferingId   Long Optional - Required when update; null when create
     * @return Boolean True: Duplicated; False: Unique
     */
    Boolean isDuplicateProgramOffering(UserContext uc, String programOfferingCode, Long organizationId, Long facilityId, Date startDateTime, Date endDateTime,
            Long... programOfferingId);

    /**
     * Search Program Offering
     *
     * @param uc                 UserContext Required
     * @param programOfferingIds Set&lt;Long> Optional A list of Program Offering Id
     * @param search             ProgramOfferingSearchType Required if programOfferingIds is not provided; Optional if programOfferingIds provided
     * @param startIndex         Long – Optional if null search returns at the start index
     * @param resultSize         Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder        String – Optional if null search returns a default order
     * @return ProgramOfferingReturnType
     */
    ProgramOfferingReturnType searchProgramOffering(UserContext uc, Set<Long> programOfferingIds, ProgramOfferingSearchType search, Long startIndex,
            Long resultSize, String resultOrder);

    /**
     * Search Program Offering by session date range
     *
     * @param uc          UserContext Required
     * @param search      ProgramOfferingSearchType Required
     * @param startIndex  Long – Optional if null search returns at the start index
     * @param resultSize  Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder String – Optional if null search returns a default order
     * @return ProgramOfferingReturnType
     */
    ProgramOfferingReturnType searchProgramOfferingBySessionDateRange(UserContext uc, ProgramOfferingSearchType search, Long startIndex, Long resultSize,
            String resultOrder);

    /**
     * getProgramTimeslot to generate an iterator of time slot TimeslotIterator&lt;ScheduleTimeslotType>
     * <p>Limited number of iterator returned when lookupEndDate or count are provided;</p>
     * <p>Unlimited number of iterator returned when lookupEndDate or count are not provided;</p>
     *
     * @param uc                 UserContext - Required
     * @param isGenerateSchedule Boolean generate schedule.
     * @param recurranceDays     Set<String> recurrance days
     * @param lookupStartDate    Date - Required Lookup start date
     * @param lookupEndDate      Date - Optional Lookup end date. Limited number of iterator returned when lookup date provided.
     * @param lookupEndTime      Date - Optional lookup end time.
     * @param count              Long - Optional Number of the time slot. Limited number of iterator returned when lookup date provided.
     * @return
     */
    TimeslotIterator getProgramTimeslot(UserContext uc, Boolean isGenerateSchedule, Set<String> recurranceDays, Date lookupStartDate, Date lookupEndDate,
            Date lookupEndTime, Long count);

    /**
     * getProgramTimeslot by Program Offering to generate an iterator of time slot TimeslotIterator&lt;ScheduleTimeslotType>
     * <p>Limited number of iterator returned when lookup date or count are provided;</p>
     * <p>Unlimited number of iterator returned when lookup date or count are not provided;</p>
     *
     * @param uc              UserContext - Required
     * @param programOffering ProgramOfferingType Required
     * @param lookupDate      Date - Required Lookup date
     * @param lookupEndDate   Date - Optional Lookup end date. Limited number of iterator returned when lookup date provided.
     * @param count           Long - Optional Number of the time slot. Limited number of iterator returned when lookup date provided.
     * @return TimeslotIterator
     */
    TimeslotIterator getProgramTimeslot(UserContext uc, ProgramOfferingType programOffering, Date lookupDate, Date lookupEndDate, Long count);

    /**
     * getProgramTimeslot by Program Assignment to generate an iterator of time slot TimeslotIterator&lt;ScheduleTimeslotType>
     * <p>Limited number of iterator returned when lookup date or count are provided;</p>
     * <p>Unlimited number of iterator returned when lookup date or count are not provided;</p>
     *
     * @param uc                UserContext - Required
     * @param programAssignment ProgramAssignmentType Required
     * @param lookupDate        Date - Required Lookup date (include time part of the date)
     * @param lookupEndDate     Date - Optional Lookup end date. Limited number of iterator returned when lookup date provided.
     * @param endTime           Date - Required the end time of each occurrence of schedule pattern
     * @param count             Long - Optional Number of the time slot. Limited number of iterator returned when lookup date provided.
     * @return TimeslotIterator
     */
    TimeslotIterator getProgramTimeslot(UserContext uc, ProgramAssignmentType programAssignment, Date lookupDate, Date lookupEndDate, Date endTime, Long count);

    /**
     * getProgramTimeslotByScheduleId to generate an iterator of time slot TimeslotIterator&lt;ScheduleTimeslotType>
     * <p>Limited number of iterator returned when lookup date or count are provided;</p>
     * <p>Unlimited number of iterator returned when lookup date or count are not provided;</p>
     *
     * @param uc            UserContext - Required
     * @param scheduleId    Long - Required Schedule Id
     * @param lookupDate    Date - Required Lookup date
     * @param lookupEndDate Date - Optional Lookup end date. Limited number of iterator returned when lookup date provided.
     * @param count         Long - Optional Number of the time slot. Limited number of iterator returned when lookup date provided.
     * @return TimeslotIterator&lt;ScheduleTimeslotType>
     */
    TimeslotIterator getProgramTimeslotByScheduleId(UserContext uc, Long scheduleId, Date lookupDate, Date lookupEndDate, Long count);

    /**
     * getProgramTimeslotByProgramOfferingId to generate an iterator of time slot TimeslotIterator&lt;ScheduleTimeslotType>
     * <p>Limited number of iterator returned when lookup date or count are provided;</p>
     * <p>Unlimited number of iterator returned when lookup date or count are not provided;</p>
     *
     * @param uc                UserContext - Required
     * @param programOfferingId Long - Required Program Offering Id
     * @param lookupDate        Date - Required Lookup date
     * @param lookupEndDate     Date - Optional Lookup end date. Limited number of iterator returned when lookup date provided.
     * @param count             Long - Optional Number of the time slot. Limited number of iterator returned when lookup date provided.
     * @return TimeslotIterator&lt;ScheduleTimeslotType>
     */
    TimeslotIterator getProgramTimeslotByProgramOfferingId(UserContext uc, Long programOfferingId, Date lookupDate, Date lookupEndDate, Long count);

    /**
     * addAdditionDateForOffering to add addition date which is not included in recurrence pattern to the schedule
     *
     * @param uc                UserContext - Required - Required
     * @param programOfferingId Long - Required Program Offering Id
     * @param additionDate      AdditionDateType - Required. addition date which is not included in recurrence pattern to the schedule
     */
    void addAdditionDateForOffering(UserContext uc, Long programOfferingId, ScheduleSessionDateType additionDate);

    /**
     * addAdditionDateSetForOffering to add a set of addition dates which are not included in recurrence pattern to the schedule
     *
     * @param uc                UserContext - Required - Required
     * @param programOfferingId Long - Required Program Offering Id
     * @param additionDates     Set&lt;AdditionDateType> - Required. A set of addition dates which are not included in recurrence pattern to the schedule
     */
    void addAdditionDateSetForOffering(UserContext uc, Long programOfferingId, Set<ScheduleSessionDateType> additionDates);

    /**
     * deleteAdditionDateForOffering to delete addition date which is not included in recurrence pattern to the schedule
     *
     * @param uc                UserContext - Required
     * @param programOfferingId Long - Required Program Offering Id
     * @param additionDate      AdditionDateType - Required. addition date which is not included in recurrence pattern to the schedule
     */
    void deleteAdditionDateForOffering(UserContext uc, Long programOfferingId, ScheduleSessionDateType additionDate);

    /**
     * deleteAdditionDateSetForOffering to delete a set of addition dates which are not included in recurrence pattern to the schedule
     *
     * @param uc                UserContext - Required
     * @param programOfferingId Long - Required Program Offering Id
     * @param additionDates     Set&lt;AdditionDateType> - Required. A set of addition dates which are not included in recurrence pattern to the schedule
     */
    void deleteAdditionDateSetForOffering(UserContext uc, Long programOfferingId, Set<ScheduleSessionDateType> additionDates);

    /**
     * getAdditionDatesForOffering to get a List of Addition Dates for Program Offering
     *
     * @param uc                UserContext - Required
     * @param programOfferingId Long - Required Program Offering Id
     * @param from              LocalDate - Optional Date to start
     * @param to                LocalDate - Optional Date to end
     * @param startTime         LocalTime - Optional
     * @param endTime           LocalTime - Optional
     * @return List&ltAdditionDateType> A List of Addition Dates for Program Offering
     */
    List<ScheduleSessionDateType> getAdditionDatesForOffering(UserContext uc, Long programOfferingId, LocalDate from, LocalDate to, LocalTime startTime,
            LocalTime endTime);

    /**
     * addModifiedPatternDateForOffering to modified pattern date which is included in recurrence pattern of schedule
     *
     * @param uc                UserContext - Required
     * @param programOfferingId Long - Required Program Offering Id
     * @param subtractionDate   ScheduleSessionDateType - Required date which is included in recurrence pattern of schedule to be subtracted
     */
    void addModifiedPatternDateForOffering(UserContext uc, Long programOfferingId, ScheduleSessionDateType subtractionDate);

    /**
     * addModifiedPatternDateSetForOffering to modified a set of pattern dates which are included in recurrence pattern of schedule
     *
     * @param uc                UserContext - Required
     * @param programOfferingId Long - Required Program Offering Id
     * @param subtractionDates  Set&lt;ScheduleSessionDateType> - Required a set of dates which are included in recurrence pattern of schedule to be subtracted
     */
    void addModifiedPatternDateSetForOffering(UserContext uc, Long programOfferingId, Set<ScheduleSessionDateType> subtractionDates);

    /**
     * deleteModifiedPatternDateForOffering to delete modified pattern date which is included in recurrence pattern of schedule
     *
     * @param uc                UserContext - Required
     * @param programOfferingId Long - Required Program Offering Id
     * @param subtractionDate   ScheduleSessionDateType - Required date which is included in recurrence pattern of schedule to be subtracted
     */
    void deleteModifiedPatternDateForOffering(UserContext uc, Long programOfferingId, ScheduleSessionDateType subtractionDate);

    /**
     * deleteModifiedPatternSetForOffering delete a set of subtraction dates which are included in recurrence pattern of schedule
     *
     * @param uc                UserContext - Required
     * @param programOfferingId Long - Required Program Offering Id
     * @param subtractionDates  Set&lt;ScheduleSessionDateType> - Required a set of dates which are included in recurrence pattern of schedule to be subtracted
     */
    void deleteModifiedPatternSetForOffering(UserContext uc, Long programOfferingId, Set<ScheduleSessionDateType> subtractionDates);

    /**
     * getModifiedPatternDatesForOffering to get a list of Subtraction Dates ForOffering which are included in recurrence pattern of schedule
     *
     * @param uc                UserContext - Required
     * @param programOfferingId Long - Required Program Offering Id
     * @param from              LocalDate - Optional Date to start
     * @param to                LocalDate - Optional Date to end
     * @param startTime         LocalTime - Optional
     * @param endTime           LocalTime - Optional
     * @return List&lt;ScheduleSessionDateType> A list of Subtraction Dates ForOffering which are included in recurrence pattern of schedule
     */
    List<ScheduleSessionDateType> getModifiedPatternDatesForOffering(UserContext uc, Long programOfferingId, LocalDate from, LocalDate to, LocalTime startTime,
            LocalTime endTime);

    /**
     * Get Program Offering Codes
     *
     * @param uc UserContext Required
     * @return Map&lt;Long, Map&lt;String, String>> Long: programOfferingId; String: programOfferingCode; String: programOfferingDescription
     */
    Map<Long, Map<String, String>> getProgramOfferingCodes(UserContext uc, String programCategory);

    /**
     * Get Program Facility IDs By Program Code
     *
     * @param uc          UserContext Required
     * @param programCode String Optional Program Code
     * @return Map&lt;Long, String> Long: Facility Id, String: Facility Name
     */
    List<Long> getPorgramFacilityIDsByProgramCode(UserContext uc, String programCode);

    /**
     * Delete Program Offering
     *
     * @param uc                UserContext Required
     * @param programOfferingId Long Required
     */
    void deleteProgramOffering(UserContext uc, Long programOfferingId);

    /**
     * Create Program Assignment
     *
     * @param uc                UserContext Required
     * @param programAssignment ProgramAssignmentType Required
     * @return ProgramAssignmentType
     */
    ProgramAssignmentType createProgramAssignment(UserContext uc, ProgramAssignmentType programAssignment);

    /**
     * Get Program Assignment
     *
     * @param uc                  UserContext Required
     * @param programAssignmentId Long Required
     * @return ProgramAssignmentType
     */
    ProgramAssignmentType getProgramAssignment(UserContext uc, Long programAssignmentId);

    /**
     * Update Program Assignment
     *
     * @param uc                UserContext Required
     * @param programAssignment ProgramAssignmentType Required
     * @return ProgramAssignmentType
     */
    ProgramAssignmentType updateProgramAssignment(UserContext uc, ProgramAssignmentType programAssignment);

    /**
     * searchProgramAssignment Search for Program Assignments
     *
     * @param uc                   UserContext Required
     * @param programAssignmentIds Set&lt;Long> optional, Assignment IDs
     * @param search               ProgramAss
     * @param startIndex
     * @param resultSize
     * @param resultOrder
     * @return
     */
    ProgramAssignmentReturnType searchProgramAssignment(UserContext uc, Set<Long> programAssignmentIds, ProgramAssignmentSearchType search, Long startIndex,
            Long resultSize, String resultOrder);

    /**
     * Delete Program Assignment
     *
     * @param uc                  UserContext Required
     * @param programAssignmentId Long Required
     */
    void deleteProgramAssignment(UserContext uc, Long programAssignmentId);

    /**
     * Abanon All ProgramAssignments For a Supervision
     *
     * @param uc                UserContext Required
     * @param supervisionId     Long Required. Supervision Id
     * @param comment           String optional.
     */
    void abandonProgramAssignmentsForSupervision(UserContext uc, Long supervisionId, Date adanonDate, String comment);

    /**
     * Create or Update Program Attendance
     *
     * @param uc                UserContext Required
     * @param programAttendance ProgramAttendanceType Required
     * @param scheduleTimeslot  ScheduleTimeslotType
     * @return ProgramAttendanceType
     */
    ProgramAttendanceType createOrUpdateProgramAttendance(UserContext uc, ProgramAttendanceType programAttendance, ScheduleTimeslotType scheduleTimeslot);

    /**
     * Get Program Attendance
     *
     * @param uc                  UserContext Required
     * @param programAttendanceId Long Required
     * @return ProgramAttendanceType
     */
    ProgramAttendanceType getProgramAttendance(UserContext uc, Long programAttendanceId);

    /**
     * Get Program Attendance by Supervision, Program Offering, Program Assignment and Session Date/Time
     * <br>At least one parameter has to be provided
     *
     * @param uc                  UserContext Required
     * @param supervisionIDs      List&lt;Long>
     * @param programOfferingId
     * @param programAssignmentId
     * @param sessionDate
     * @param startTime
     * @param endTime
     * @return List&lt;ProgramAssignmentType>
     */
    List<ProgramAttendanceType> getProgramAttendances(UserContext uc, Set<Long> supervisionIDs, Long programOfferingId, Long programAssignmentId, Date sessionDate,
            Date startTime, Date endTime);

    /**
     * Has Any Program Attendance Been Recorded
     * <br>At least one parameter has to be provided
     *
     * @param uc                  UserContext Required
     * @param uc                  UserContext Required
     * @param supervisionIDs      List&lt;Long>
     * @param programOfferingId
     * @param programAssignmentId
     * @param sessionDate
     * @param startTime
     * @param endTime
     * @return Boolean True: At least one Attendance recorded; False: None of Attendances recorded.
     */
    Boolean hasProgramAttendanceRecorded(UserContext uc, Set<Long> supervisionIDs, Long programOfferingId, Long programAssignmentId, Date sessionDate,
            Date startTime, Date endTime);

    /**
     * hasProgramAssignedAlready -- to confirm if a program with programId has been assigned
     *
     * @param uc               UserContext Required
     * @param supervisionId    Long Required
     * @param programId        Required
     * @param assignmentStatus Required
     * @return
     */
    Boolean hasProgramAssignedAlready(UserContext uc, Long supervisionId, Long programId, Set<String> assignmentStatus);

    /**
     * Get Program Attendance by Assignment and Schedule Timeslot
     *
     * @param uc                  UserContext Required
     * @param programAssignmentId Long Required
     * @param scheduleTimeslot    ScheduleTimeslotType
     * @return ProgramAttendanceType
     */
    ProgramAttendanceType getProgramAttendanceIdByAssignmentIdAndScheduleTimeslot(UserContext uc, Long programAssignmentId, ScheduleTimeslotType scheduleTimeslot);

    /**
     * Delete Program Attendance
     *
     * @param uc                  UserContext Required
     * @param programAttendanceId Long Required
     */
    void deleteProgramAttendance(UserContext uc, Long programAttendanceId);

    /**
     * Get All Program IDs
     *
     * @param uc UserContext Required
     * @return &lt;Long>
     */
    Set<Long> getAllProgram(UserContext uc);

    /**
     * Get All Program Offering IDs
     *
     * @param uc UserContext Required
     * @return &lt;Long>
     */
    Set<Long> getAllProgramOffering(UserContext uc);

    /**
     * Get All Program Assignment IDs
     *
     * @param uc UserContext Required
     * @return &lt;Long>
     */
    Set<Long> getAllProgramAssignment(UserContext uc);

    /**
     * Get All Program Attendance IDs
     *
     * @param uc UserContext Required
     * @return &lt;Long>
     */
    Set<Long> getAllProgramAttendance(UserContext uc);

    /**
     * Delete All -- Program, Program Offering, Program Assignment, Program Attendance
     *
     * @param uc UserContext Required
     */
    void deleteAll(UserContext uc);

    boolean isProgramOfferingByOrganization(UserContext uc, Long organizationId);
    ///////////////////////////////////////////////////////////////////
    //Client Custom Field implementation
    ///////////////////////////////////////////////////////////////////

    /**
     * Gets CCF metadata as string
     *
     * @param uc,          {@link UserContext}, required
     * @param legalModule, the module name, required
     * @param language,    required
     * @return CCF metadata as string
     */
    String getCCFMeta(UserContext uc, DTOBindingTypeEnum legalModule, String language);

    /**
     * Gets CCF metadata object
     *
     * @param uc,          {@link UserContext}, required
     * @param legalModule, module name, required
     * @param name,        CCF name, required
     * @return {@link ClientCustomFieldMetaType}
     */
    ClientCustomFieldMetaType getClientCustomFieldMeta(UserContext uc, DTOBindingTypeEnum legalModule, String name);

    /**
     * Gets all metadata as object
     *
     * @param uc,          {@link UserContext}, required
     * @param legalModule, module name, required
     * @return a list of {@link ClientCustomFieldMetaType}
     */
    List<ClientCustomFieldMetaType> getAllCCFMeta(UserContext uc, DTOBindingTypeEnum legalModule);

    /**
     * Saves a CCF value
     *
     * @param uc,       {@link UserContext}, required
     * @param ccfValue, the CCF value
     * @return {@link ClientCustomFieldValueType}
     */
    ClientCustomFieldValueType saveCCFMetaValue(UserContext uc, ClientCustomFieldValueType ccfValue);

    /**
     * Saves a list of CCF values
     *
     * @param uc,        {@link UserContext}, required
     * @param ccfValues, list of values
     * @return a list of {@link ClientCustomFieldValueType}
     */
    List<ClientCustomFieldValueType> saveCCFMetaValues(UserContext uc, List<ClientCustomFieldValueType> ccfValues);

    /**
     * Gets CCF values by search criteria
     *
     * @param uc,         {@link UserContext}, required
     * @param searchType, search object, required
     * @return a list of {@link ClientCustomFieldValueType}
     */
    List<ClientCustomFieldValueType> getCCFMetaValues(UserContext uc, ClientCustomFieldValueSearchType searchType);

    /**
     * Updates CCF value
     *
     * @param uc,       {@link UserContext}, required
     * @param ccfValue, {@link ClientCustomFieldValueType}, required
     * @return
     */
    ClientCustomFieldValueType updateCCFMetaValue(UserContext uc, ClientCustomFieldValueType ccfValue);

    /**
     * Updates a list of CCF values
     *
     * @param uc,        {@link UserContext}, required
     * @param ccfValues, list of {@link ClientCustomFieldValueType}
     * @return a list of {@link ClientCustomFieldValueType}
     */
    List<ClientCustomFieldValueType> updateCCFMetaValues(UserContext uc, List<ClientCustomFieldValueType> ccfValues);

    ////////////////////////////////////////end client custom feild methods///////////////////////////////////////

}
