package syscon.arbutus.product.services.program.contract.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * ProgramOfferingSearchType for Program and Services
 * <p>Used for method call searchProgramOffering</p>
 *
 * @author YShang
 * @since July 24, 2014
 */
public class ProgramOfferingSearchType implements Serializable {
    private static final long serialVersionUID = 5749262371276375840L;

    /**
     * Program Id
     */
    private Long programId;

    /**
     * Program Category
     * <p>MetaData -- PROGRAMCATEGORY: ACCRED, COMM, INSTITUT</p>
     */
    private String programCategory;

    /**
     * Program code
     */
    private String programCode;

    /**
     * Program Description
     */
    private String programDescription;

    /**
     * Program Offering Description
     */
    private String offeringDescription;

    /**
     * Start Date to query from
     */
    private Date fromStartDate;

    /**
     * Start Date to query to
     */
    private Date toStartDate;

    /**
     * End Date to query from
     */
    private Date fromEndDate;

    /**
     * End Date to query to
     */
    private Date toEndDate;

    /**
     * Indicates whether an inmate can start the program part way
     * ie. after the Start Date of the offering has passed (yes) or not (no).
     */
    private Boolean flexibleStart;

    /**
     * Gender -- Male; Female, Unknown
     * <p>Metadata -- OFFENDERGENDER: F, M U</p>
     */
    private Set<String> genders;

    /**
     * Age Range/Group: Adult; Juvenille
     * <p>Metadata -- AGRRANGE: ADULT, JUVENILE</p>
     */
    private Set<String> ageRanges;

    /**
     * Amentity: Wheelchaire Accessible,...
     * <p>Metadata -- LOCATIONPROPERTY: HSENS, SHOWER, WHEELCHAIR</p>
     */
    private Set<String> amenities;

    /**
     * Facility Id
     */
    private Long facilityId;

    /**
     * Provider of the program offering -- Organization Id
     */
    private Long ogranizationId;

    /**
     * Program Assignment Status. Metadata -- PROGRAMASSIGNMENTSTATUS: REFERRED; CANCELLED; APPROVED; REJECTED; ALLOCATED; SUSPENDED; ABANDONED; COMPLETED
     */
    private Set<String> programAssignmentStatus;

    /**
     * Program Offering Status: Active or Inactive
     */
    private String status;

    private Long supervisionId;

    /**
     * Permitted Facilities for a staff
     */
    private Set<Long> permittedFacilities;

    /**
     * Get Program Id
     *
     * @return Long programId
     */
    public Long getProgramId() {
        return programId;
    }

    /**
     * Set Program Id
     *
     * @param programId
     */
    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    /**
     * Indicates the general category that the program falls under
     * eg. Institutional Activity, Accredited Program, Community Service.
     * Different functionality will apply to each type of program within the system.
     * <p>MetaData -- PROGRAMCATEGORY: ACCRED, COMM, INSTITUT</p>
     */
    public String getProgramCategory() {
        return programCategory;
    }

    /**
     * Indicates the general category that the program falls under
     * eg. Institutional Activity, Accredited Program, Community Service.
     * Different functionality will apply to each type of program within the system.
     * <p>MetaData -- PROGRAMCATEGORY: ACCRED, COMM, INSTITUT</p>
     */
    public void setProgramCategory(String programCategory) {
        this.programCategory = programCategory;
    }

    /**
     * The code that uniquely identifies the program Eg. KITCHEN
     */
    public String getProgramCode() {
        return programCode;
    }

    /**
     * The code that uniquely identifies the program Eg. KITCHEN
     */
    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }

    /**
     * The name of the program eg. Kitchen Duty
     */
    public String getProgramDescription() {
        return programDescription;
    }

    /**
     * The name of the program eg. Kitchen Duty
     */
    public void setProgramDescription(String programDescription) {
        this.programDescription = programDescription;
    }

    /**
     * Indicates which genders are particularly suitable for the program.
     * <p>Metadata -- OFFENDERGENDER: F, M U</p>
     */
    public Set<String> getGenders() {
        return genders;
    }

    /**
     * Indicates which genders are particularly suitable for the program.
     * <p>Metadata -- OFFENDERGENDER: F, M U</p>
     */
    public void setGenders(Set<String> genders) {
        this.genders = genders;
    }

    /**
     * Indicates which age ranges are particularly suitable for the program.
     * <p>Metadata -- AGRRANGE: ADULT, JUVENILE</p>
     */
    public Set<String> getAgeRanges() {
        return ageRanges;
    }

    /**
     * Indicates which age ranges are particularly suitable for the program.
     * <p>Metadata -- AGRRANGE: ADULT, JUVENILE</p>
     */
    public void setAgeRanges(Set<String> ageRanges) {
        this.ageRanges = ageRanges;
    }

    /**
     * Indicates what special amenities are available at the program location
     * (same as Location Properties in Facility Internal Location).
     * This applies more at the program offering level
     * but can be set at the program level as well.
     * <p>Note that multiple values may apply.
     * <p>Metadata -- LOCATIONPROPERTY: HSENS, SHOWER, WHEELCHAIR</p>
     */
    public Set<String> getAmenities() {
        return amenities;
    }

    /**
     * Indicates what special amenities are available at the program location
     * (same as Location Properties in Facility Internal Location).
     * This applies more at the program offering level
     * but can be set at the program level as well.
     * <p>Note that multiple values may apply.
     * <p>Metadata -- LOCATIONPROPERTY: HSENS, SHOWER, WHEELCHAIR</p>
     */
    public void setAmenities(Set<String> amenities) {
        this.amenities = amenities;
    }

    /**
     * Program Offering Code/Description
     */
    public String getOfferingDescription() {
        return offeringDescription;
    }

    /**
     * Program Offering Code/Description
     */
    public void setOfferingDescription(String offeringDescription) {
        this.offeringDescription = offeringDescription;
    }

    /**
     * Start Date to query from
     *
     * @return Date
     */
    public Date getFromStartDate() {
        return fromStartDate;
    }

    /**
     * Start Date to query from
     *
     * @param fromStartDate Date
     */
    public void setFromStartDate(Date fromStartDate) {
        this.fromStartDate = fromStartDate;
    }

    /**
     * Start Date to query to
     *
     * @return Date
     */
    public Date getToStartDate() {
        return toStartDate;
    }

    /**
     * Start Date to query to
     *
     * @param toStartDate Date
     */
    public void setToStartDate(Date toStartDate) {
        this.toStartDate = toStartDate;
    }

    /**
     * End Date to query from
     *
     * @return Date
     */
    public Date getFromEndDate() {
        return fromEndDate;
    }

    /**
     * End Date to query from
     *
     * @param fromEndDate Date
     */
    public void setFromEndDate(Date fromEndDate) {
        this.fromEndDate = fromEndDate;
    }

    /**
     * End Date to query to
     *
     * @return Date
     */
    public Date getToEndDate() {
        return toEndDate;
    }

    /**
     * End Date to query to
     *
     * @param toEndDate Date
     */
    public void setToEndDate(Date toEndDate) {
        this.toEndDate = toEndDate;
    }

    /**
     * Indicates whether an inmate can start the program part way
     * ie. after the Start Date of the offering has passed (yes) or not (no).
     *
     * @return Boolean
     */
    public Boolean getFlexibleStart() {
        return flexibleStart;
    }

    /**
     * Indicates whether an inmate can start the program part way
     * ie. after the Start Date of the offering has passed (yes) or not (no).
     *
     * @param flexibleStart Boolean
     */
    public void setFlexibleStart(Boolean flexibleStart) {
        this.flexibleStart = flexibleStart;
    }

    /**
     * Facility Id
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * Facility Id
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * Provider of the program offering -- Organization Id
     */
    public Long getOgranizationId() {
        return ogranizationId;
    }

    /**
     * Provider of the program offering -- Organization Id
     */
    public void setOgranizationId(Long ogranizationId) {
        this.ogranizationId = ogranizationId;
    }

    /**
     * Program Assignment Status. Metadata -- PROGRAMASSIGNMENTSTATUS: REFERRED; CANCELLED; APPROVED; REJECTED; ALLOCATED; SUSPENDED; ABANDONED; COMPLETED
     */
    public Set<String> getProgramAssignmentStatus() {
        return programAssignmentStatus;
    }

    /**
     * Program Assignment Status. Metadata -- PROGRAMASSIGNMENTSTATUS: REFERRED; CANCELLED; APPROVED; REJECTED; ALLOCATED; SUSPENDED; ABANDONED; COMPLETED
     */
    public void setProgramAssignmentStatus(Set<String> programAssignmentStatus) {
        this.programAssignmentStatus = programAssignmentStatus;
    }

    /**
     * Program Offering Status: Active or Inactive
     */
    public String getStatus() {
        return status;
    }

    /**
     * Program Offering Status: Active or Inactive
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Supervision Id
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * Supervision Id
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Permitted Facility Set for the staff
     */
    public Set<Long> getPermittedFacilities() {
        return permittedFacilities;
    }

    /**
     * Permitted Facility Set for the staff
     */
    public void setPermittedFacilities(Set<Long> permittedFacilities) {
        this.permittedFacilities = permittedFacilities;
    }

    @Override
    public String toString() {
        return "ProgramOfferingSearchType [programCategory=" + programCategory + ", programCode=" + programCode + ", programDescription=" + programDescription
                + ", offeringDescription=" + offeringDescription + ", genders=" + genders + ", ageRanges=" + ageRanges + ", amenities=" + amenities + ", facilityId="
                + facilityId + ", ogranizationId=" + ogranizationId + ", status=" + status + ", supervisionId=" + supervisionId + ", getProgramCategory()="
                + getProgramCategory() + ", getProgramCode()=" + getProgramCode() + ", getProgramDescription()=" + getProgramDescription() + ", getGenders()="
                + getGenders() + ", getAgeRanges()=" + getAgeRanges() + ", getAmenities()=" + getAmenities() + ", getOfferingDescription()=" + getOfferingDescription()
                + ", getFacilityId()=" + getFacilityId() + ", getOgranizationId()=" + getOgranizationId() + ", getStatus()=" + getStatus() + ", getSupervisionId()="
                + getSupervisionId() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
    }

}
