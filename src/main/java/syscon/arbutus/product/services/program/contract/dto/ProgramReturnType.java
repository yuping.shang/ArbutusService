package syscon.arbutus.product.services.program.contract.dto;

import java.io.Serializable;
import java.util.List;

/**
 * ProgramReturnType for Program and Services
 * <p>The returning from search program</p>
 *
 * @author YShang
 * @since May 24, 2014
 */
public class ProgramReturnType implements Serializable {

    private static final long serialVersionUID = 1439294366689088990L;

    /**
     * A List of Programs from the search
     */
    private List<ProgramType> programs;

    /**
     * Total size of the return from search
     */
    private Long totalSize;

    public ProgramReturnType() {
    }

    /**
     * A List of Programs from the search
     *
     * @return List&lt;ProgramType>
     */
    public List<ProgramType> getPrograms() {
        return programs;
    }

    /**
     * A List of Programs from the search
     *
     * @param programs List&lt;ProgramType>
     */
    public void setPrograms(List<ProgramType> programs) {
        this.programs = programs;
    }

    /**
     * Total size of the return from search
     *
     * @return Long
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * Total size of the return from search
     *
     * @param totalSize Long
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    @Override
    public String toString() {
        return "ProgramReturnType [programs=" + programs + ", totalSize=" + totalSize + "]";
    }

}
