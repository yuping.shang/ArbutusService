package syscon.arbutus.product.services.program.contract.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.core.contract.dto.CommentType;

public class SuspensionType implements Serializable {

    private static final long serialVersionUID = 4193010765421226752L;
    private static final int LENGTH_SMALL = 64;

    private Long id;

    @NotNull
    private Date suspendDate;

    private Date resumeDate;

    /**
     * If the inmate is suspended from the program, indicate the reason
     * <p>Metadata: PROGRAMSUSPENDREASON: DISPACT, HEALTH, RECLASS</p>
     */
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String suspendReason;

    private CommentType comment;

    public SuspensionType() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getSuspendDate() {
        return suspendDate;
    }

    public void setSuspendDate(Date suspendDate) {
        this.suspendDate = suspendDate;
    }

    public Date getResumeDate() {
        return resumeDate;
    }

    public void setResumeDate(Date resumeDate) {
        this.resumeDate = resumeDate;
    }

    /**
     * If the inmate is suspended from the program, indicate the reason
     * <p>Metadata: PROGRAMSUSPENDREASON: DISPACT, HEALTH, RECLASS</p>
     */
    public String getSuspendReason() {
        return suspendReason;
    }

    /**
     * If the inmate is suspended from the program, indicate the reason
     * <p>Metadata: PROGRAMSUSPENDREASON: DISPACT, HEALTH, RECLASS</p>
     */
    public void setSuspendReason(String suspendReason) {
        this.suspendReason = suspendReason;
    }

    public CommentType getComment() {
        return comment;
    }

    public void setComment(CommentType comment) {
        this.comment = comment;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((suspendDate == null) ? 0 : suspendDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SuspensionType other = (SuspensionType) obj;
        if (suspendDate == null) {
            if (other.suspendDate != null) {
				return false;
			}
        } else if (!suspendDate.equals(other.suspendDate)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "SuspensionType [id=" + id + ", suspendDate=" + suspendDate + ", resumeDate=" + resumeDate + ", suspendReason=" + suspendReason + ", comment=" + comment
                + "]";
    }

}
