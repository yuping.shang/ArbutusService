package syscon.arbutus.product.services.program.contract.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Target Inmate Type for Program and Service
 *
 * @author YShang
 * @since April 6, 2014
 */
public class TargetInmateType implements Serializable {

    private static final long serialVersionUID = -681654063809363323L;

    /**
     * Identification of a target inmate
     */
    private Long targetInmateId;

    /**
     * Indicates which genders are particularly suitable for the program.
     * <p>Note that multiple values may apply.
     * <p>Metadata: OFFENDERGENDER: F, M U</p>
     */
    private Set<String> genders;

    /**
     * Indicates which age ranges are particularly suitable for the program.
     * <p>Note that multiple values may apply.
     * <p>Metadata: AGRRANGE: ADULT, JUVENILE</p>
     */
    private Set<String> ageRanges;

    /**
     * Indicates what special amenities are available at the program location
     * (same as Location Properties in Facility Internal Location).
     * This applies more at the program offering level
     * but can be set at the program level as well.
     * <p>Note that multiple values may apply.
     * <p>Metadata: LOCATIONPROPERTY: HSENS, SHOWER, WHEELCHAIR</p>
     */
    private Set<String> amenities;

    public TargetInmateType() {
        super();
    }

    /**
     * Identification of a target inmate
     *
     * @return Long
     */
    public Long getTargetInmateId() {
        return targetInmateId;
    }

    /**
     * Identification of a target inmate
     *
     * @param targetInmateId Long
     */
    public void setTargetInmateId(Long targetInmateId) {
        this.targetInmateId = targetInmateId;
    }

    /**
     * Indicates which genders are particularly suitable for the program.
     * <p>Note that multiple values may apply.
     * <p>Metadata: OFFENDERGENDER: F, M U</p>
     *
     * @return &lt;String>
     */
    public Set<String> getGenders() {
        if (genders == null) {
			genders = new HashSet<>();
		}
        return genders;
    }

    /**
     * Indicates which genders are particularly suitable for the program.
     * <p>Note that multiple values may apply.
     * <p>Metadata: OFFENDERGENDER: F, M U</p>
     *
     * @param genders &lt;String>
     */
    public void setGenders(Set<String> genders) {
        this.genders = genders;
    }

    /**
     * Indicates which age ranges are particularly suitable for the program.
     * <p>Note that multiple values may apply.
     * <p>Metadata: AGRRANGE: ADULT, JUVENILE</p>
     *
     * @return &lt;String>
     */
    public Set<String> getAgeRanges() {
        if (ageRanges == null) {
			ageRanges = new HashSet<>();
		}
        return ageRanges;
    }

    /**
     * Indicates which age ranges are particularly suitable for the program.
     * <p>Note that multiple values may apply.
     * <p>Metadata: AGRRANGE: ADULT, JUVENILE</p>
     *
     * @param ageRanges &lt;String>
     */
    public void setAgeRanges(Set<String> ageRanges) {
        this.ageRanges = ageRanges;
    }

    /**
     * Indicates what special amenities are available at the program location
     * (same as Location Properties in Facility Internal Location).
     * This applies more at the program offering level
     * but can be set at the program level as well.
     * <p>Note that multiple values may apply.
     * <p>Metadata: LOCATIONPROPERTY: HSENS, SHOWER, WHEELCHAIR</p>
     *
     * @return &lt;String>
     */
    public Set<String> getAmenities() {
        if (amenities == null) {
			amenities = new HashSet<>();
		}
        return amenities;
    }

    /**
     * Indicates what special amenities are available at the program location
     * (same as Location Properties in Facility Internal Location).
     * This applies more at the program offering level
     * but can be set at the program level as well.
     * <p>Note that multiple values may apply.
     * <p>Metadata: LOCATIONPROPERTY: HSENS, SHOWER, WHEELCHAIR</p>
     *
     * @param amenities &lt;String>
     */
    public void setAmenities(Set<String> amenities) {
        this.amenities = amenities;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ageRanges == null) ? 0 : ageRanges.hashCode());
        result = prime * result + ((amenities == null) ? 0 : amenities.hashCode());
        result = prime * result + ((genders == null) ? 0 : genders.hashCode());
        result = prime * result + ((targetInmateId == null) ? 0 : targetInmateId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        TargetInmateType other = (TargetInmateType) obj;
        if (ageRanges == null) {
            if (other.ageRanges != null) {
				return false;
			}
        } else if (!ageRanges.equals(other.ageRanges)) {
			return false;
		}
        if (amenities == null) {
            if (other.amenities != null) {
				return false;
			}
        } else if (!amenities.equals(other.amenities)) {
			return false;
		}
        if (genders == null) {
            if (other.genders != null) {
				return false;
			}
        } else if (!genders.equals(other.genders)) {
			return false;
		}
        if (targetInmateId == null) {
            if (other.targetInmateId != null) {
				return false;
			}
        } else if (!targetInmateId.equals(other.targetInmateId)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "TargetInmateType [targetInmateId=" + targetInmateId + ", genders=" + genders + ", ageRanges=" + ageRanges + ", amenities=" + amenities + "]";
    }

}
