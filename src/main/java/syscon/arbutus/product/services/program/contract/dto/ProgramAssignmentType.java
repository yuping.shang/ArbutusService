package syscon.arbutus.product.services.program.contract.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;
import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * Program Assignment Type for Program and Service
 *
 * @author YShang
 * @since April 6, 2014
 */
@ArbutusConstraint(constraints = { "startDate <= endDate" })
public class ProgramAssignmentType implements Serializable {

    private static final long serialVersionUID = 2052714388245069692L;

    private static final int LENGTH_SMALL = 64;

    /**
     * Identification of a Program Assignment
     */
    private Long assignmentId;

    /**
     * Date on which the referral to a program offering was made.
     * Only applies if a referral was made and the inmate was not directly placed in the program.
     */
    private Date referralDate;

    /**
     * A general priority for the referral.
     * Only applies if a referral was made and the inmate was not directly placed in the program
     * <p>Metadata: REFERRALPRIORITY: LOW, MEDIUM, HIGH</p>
     */
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String referralPriority;

    /**
     * If the program provider rejects the referral, indicate the reason.
     * Only applies if a referral was made and the inmate was not directly placed in the program.
     * <p>REFERRALREJECTIONREASON: NOAVAIL, NOTSUIT</p>
     */
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String referralRejectionReason;

    /**
     * Date on which the inmate starts the program.
     * Only applies when the inmate is placed in a program offering.
     */
    private Date startDate;

    /**
     * Date on which the inmate stops the program.
     * Only applies when the inmate is placed in a program offering.
     */
    private Date endDate;

    /**
     * Date and Time to be subtracted from schedule recurrence pattern
     */
    private Set<ScheduleSessionDateType> removedDates;

    /**
     * Date and Time to be added to schedule recurrence pattern
     */
    private Set<ScheduleSessionDateType> addedDates;

    /**
     * Indicates the current status of the assignment eg. Referred, Allocated, Completed
     * <p>Metadata: PROGRAMASSIGNMENTSTATUS: ABANDONED, ALLOCATED, APPROVED, CANCELLED, COMPLETED, REFERRED, REJECTED, SUSPENDED</p>
     */
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String status;

    /**
     * If the inmate is suspended from the program, indicate the reason
     * <p>Metadata: PROGRAMSUSPENDREASON: DISPACT, HEALTH, RECLASS</p>
     */
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String suspendReason;

    @Valid
    private Set<SuspensionType> suspensions;

    /**
     * If the inmate abandons the program, indicate the reason
     * <p>Metadata: PROGRAMABANDONMENTREASON: NOTCOMP, RELEASE, TRANSFER</p>
     */
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String abandonReason;

    /**
     * Additional optional comments about the program assignment
     */
    @Valid
    private CommentType comments;

    /**
     * The userId who modified this status of the program assignment.
     */
    private String modifiedBy;

    /**
     * The inmate/supervision who is being referred to or placed in the program
     */
    @NotNull
    private Long supervisionId;

    /**
     * The offering to which the inmate is being referred or placed
     */
    private ProgramType program;

    /**
     * The offering to which the inmate is being referred or placed
     */
    private ProgramOfferingType programmOffering;

    /**
     * Allows the user to record the inmate’s attendance in the program for each session
     */
    private Set<ProgramAttendanceType> programAttendances;

    /**
     * Process id to be applied by JBPM
     */
    private Long processId;

    public ProgramAssignmentType() {
        super();
    }

    /**
     * Identification of a Program Assignment
     *
     * @return Long
     */
    public Long getAssignmentId() {
        return assignmentId;
    }

    /**
     * Identification of a Program Assignment
     *
     * @param assignmentId Long
     */
    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }

    /**
     * Date on which the referral to a program offering was made.
     * Only applies if a referral was made and the inmate was not directly placed in the program.
     *
     * @return Date
     */
    public Date getReferralDate() {
        return referralDate;
    }

    /**
     * Date on which the referral to a program offering was made.
     * Only applies if a referral was made and the inmate was not directly placed in the program.
     *
     * @param referralDate Date
     */
    public void setReferralDate(Date referralDate) {
        this.referralDate = referralDate;
    }

    /**
     * A general priority for the referral.
     * Only applies if a referral was made and the inmate was not directly placed in the program
     * <p>Metadata: REFERRALPRIORITY: LOW, MEDIUM, HIGH</p>
     * <p>Max length 64</p>
     *
     * @return String
     */
    public String getReferralPriority() {
        return referralPriority;
    }

    /**
     * A general priority for the referral.
     * Only applies if a referral was made and the inmate was not directly placed in the program
     * <p>Metadata: REFERRALPRIORITY: LOW, MEDIUM, HIGH</p>
     * <p>Max length 64</p>
     *
     * @param referralPriority String
     */
    public void setReferralPriority(String referralPriority) {
        this.referralPriority = referralPriority;
    }

    /**
     * If the program provider rejects the referral, indicate the reason.
     * Only applies if a referral was made and the inmate was not directly placed in the program.
     * <p>REFERRALREJECTIONREASON: NOAVAIL, NOTSUIT</p>
     * <p>Max length 64</p>
     *
     * @return String
     */
    public String getReferralRejectionReason() {
        return referralRejectionReason;
    }

    /**
     * If the program provider rejects the referral, indicate the reason.
     * Only applies if a referral was made and the inmate was not directly placed in the program.
     * <p>REFERRALREJECTIONREASON: NOAVAIL, NOTSUIT</p>
     * <p>Max length 64</p>
     *
     * @param referralRejectionReason String
     */
    public void setReferralRejectionReason(String referralRejectionReason) {
        this.referralRejectionReason = referralRejectionReason;
    }

    /**
     * Date on which the inmate starts the program.
     * Only applies when the inmate is placed in a program offering.
     *
     * @return Date
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Date on which the inmate starts the program.
     * Only applies when the inmate is placed in a program offering.
     *
     * @param startDate Date
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Date on which the inmate stops the program.
     * Only applies when the inmate is placed in a program offering.
     *
     * @return Date
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Date on which the inmate stops the program.
     * Only applies when the inmate is placed in a program offering.
     *
     * @param endDate Date
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Date and Time to be subtracted from schedule recurrence pattern
     *
     * @return Set&lt;ScheduleSessionDateType>
     */
    public Set<ScheduleSessionDateType> getRemovedDates() {
        if (removedDates == null) {
            removedDates = new HashSet<>();
        }
        return removedDates;
    }

    /**
     * Date and Time to be subtracted from schedule recurrence pattern
     *
     * @param removedDates Set&lt;ScheduleSessionDateType>
     */
    public void setRemovedDates(Set<ScheduleSessionDateType> removedDates) {
        this.removedDates = removedDates;
    }

    /**
     * Date and Time to be added to schedule recurrence pattern
     *
     * @return Set&lt;AdditionDateType>
     */
    public Set<ScheduleSessionDateType> getAddedDates() {
        if (addedDates == null) {
            addedDates = new HashSet<>();
        }
        return addedDates;
    }

    /**
     * Date and Time to be added to schedule recurrence pattern
     *
     * @param addedDates Set&lt;AdditionDateType>
     */
    public void setAddedDates(Set<ScheduleSessionDateType> addedDates) {
        this.addedDates = addedDates;
    }

    /**
     * Indicates the current status of the assignment eg. Referred, Allocated, Completed
     * <p>Metadata: PROGRAMASSIGNMENTSTATUS: ABANDONED, ALLOCATED, APPROVED, CANCELLED, COMPLETED, REFERRED, REJECTED, SUSPENDED</p>
     * <p>Max length 64</p>
     *
     * @return String
     */
    public String getStatus() {
        return status;
    }

    /**
     * Indicates the current status of the assignment eg. Referred, Allocated, Completed
     * <p>Metadata: PROGRAMASSIGNMENTSTATUS: ABANDONED, ALLOCATED, APPROVED, CANCELLED, COMPLETED, REFERRED, REJECTED, SUSPENDED</p>
     * <p>Max length 64</p>
     *
     * @param status String
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * If the inmate is suspended from the program, indicate the reason
     * <p>Metadata: PROGRAMSUSPENDREASON: DISPACT, HEALTH, RECLASS</p>
     * <p>Max length 64</p>
     *
     * @return String
     */
    public String getSuspendReason() {
        return suspendReason;
    }

    /**
     * If the inmate is suspended from the program, indicate the reason
     * <p>Metadata: PROGRAMSUSPENDREASON: DISPACT, HEALTH, RECLASS</p>
     * <p>Max length 64</p>
     *
     * @param suspendReason String
     */
    public void setSuspendReason(String suspendReason) {
        this.suspendReason = suspendReason;
    }

    public Set<SuspensionType> getSuspensions() {
        if (suspensions == null) {
            suspensions = new HashSet<>();
        }
        return suspensions;
    }

    public void setSuspensions(Set<SuspensionType> suspensions) {
        this.suspensions = suspensions;
    }

    /**
     * If the inmate abandons the program, indicate the reason
     * <p>Metadata: PROGRAMABANDONMENTREASON: NOTCOMP, RELEASE, TRANSFER</p>
     * <p>Max length 64</p>
     *
     * @return String
     */
    public String getAbandonReason() {
        return abandonReason;
    }

    /**
     * If the inmate abandons the program, indicate the reason
     * <p>Metadata: PROGRAMABANDONMENTREASON: NOTCOMP, RELEASE, TRANSFER</p>
     * <p>Max length 64</p>
     *
     * @param abandonReason String
     */
    public void setAbandonReason(String abandonReason) {
        this.abandonReason = abandonReason;
    }

    /**
     * Additional optional comments about the program assignment
     *
     * @return CommentType
     */
    public CommentType getComments() {
        return comments;
    }

    /**
     * Additional optional comments about the program assignment
     *
     * @param comments CommentType
     */
    public void setComments(CommentType comments) {
        this.comments = comments;
    }

    /**
     * The staff id who modified this status of the program assignment. Optional property
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * The staff id who modified this status of the program assignment. Optional property
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * The inmate/supervision who is being referred to or placed in the program
     *
     * @return Long
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * The inmate/supervision who is being referred to or placed in the program
     *
     * @param supervisionId Long
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * The offering to which the inmate is being referred or placed
     *
     * @return ProgramType
     */
    public ProgramType getProgram() {
        return program;
    }

    /**
     * The offering to which the inmate is being referred or placed
     *
     * @param program ProgramType
     */
    public void setProgram(ProgramType program) {
        this.program = program;
    }

    /**
     * The offering to which the inmate is being referred or placed
     *
     * @return ProgramOfferingType
     */
    public ProgramOfferingType getProgrammOffering() {
        return programmOffering;
    }

    /**
     * The offering to which the inmate is being referred or placed
     *
     * @param programmOffering ProgramOfferingType
     */
    public void setProgrammOffering(ProgramOfferingType programmOffering) {
        this.programmOffering = programmOffering;
    }

    /**
     * Allows the user to record the inmate’s attendance in the program for each session
     *
     * @return &lt;ProgramAttendanceType>
     */
    public Set<ProgramAttendanceType> getProgramAttendances() {
        return programAttendances;
    }

    /**
     * Allows the user to record the inmate’s attendance in the program for each session
     *
     * @param programAttendances &lt;ProgramAttendanceType>
     */
    public void setProgramAttendances(Set<ProgramAttendanceType> programAttendances) {
        this.programAttendances = programAttendances;
    }

    /**
     * ProcessId to be applied by JBPM
     *
     * @return Long
     */
    public Long getProcessId() {
        return processId;
    }

    /**
     * ProcessId to be applied by JBPM
     *
     * @param processId
     */
    public void setProcessId(Long processId) {
        this.processId = processId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((assignmentId == null) ? 0 : assignmentId.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((referralDate == null) ? 0 : referralDate.hashCode());
        result = prime * result + ((referralPriority == null) ? 0 : referralPriority.hashCode());
        result = prime * result + ((referralRejectionReason == null) ? 0 : referralRejectionReason.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ProgramAssignmentType other = (ProgramAssignmentType) obj;
        if (assignmentId == null) {
            if (other.assignmentId != null) {
				return false;
			}
        } else if (!assignmentId.equals(other.assignmentId)) {
			return false;
		}
        if (endDate == null) {
            if (other.endDate != null) {
				return false;
			}
        } else if (!endDate.equals(other.endDate)) {
			return false;
		}
        if (referralDate == null) {
            if (other.referralDate != null) {
				return false;
			}
        } else if (!referralDate.equals(other.referralDate)) {
			return false;
		}
        if (referralPriority == null) {
            if (other.referralPriority != null) {
				return false;
			}
        } else if (!referralPriority.equals(other.referralPriority)) {
			return false;
		}
        if (referralRejectionReason == null) {
            if (other.referralRejectionReason != null) {
				return false;
			}
        } else if (!referralRejectionReason.equals(other.referralRejectionReason)) {
			return false;
		}
        if (startDate == null) {
            if (other.startDate != null) {
				return false;
			}
        } else if (!startDate.equals(other.startDate)) {
			return false;
		}
        if (status == null) {
            if (other.status != null) {
				return false;
			}
        } else if (!status.equals(other.status)) {
			return false;
		}
        if (supervisionId == null) {
            if (other.supervisionId != null) {
				return false;
			}
        } else if (!supervisionId.equals(other.supervisionId)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "ProgramAssignmentType [assignmentId=" + assignmentId + ", referralDate=" + referralDate + ", referralPriority=" + referralPriority
                + ", referralRejectionReason=" + referralRejectionReason + ", startDate=" + startDate + ", endDate=" + endDate + ", removedDates=" + removedDates
                + ", addedDates=" + addedDates + ", status=" + status + ", suspendReason=" + suspendReason + ", suspensions=" + suspensions + ", abandonReason="
                + abandonReason + ", comments=" + comments + ", supervisionId=" + supervisionId + ", program=" + program + ", programAttendances=" + programAttendances
                + ", processId=" + processId + "]";
    }

}
