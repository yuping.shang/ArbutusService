package syscon.arbutus.product.services.program.contract.dto;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;
import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * Program Offering Type for Program and Service
 *
 * @author YShang
 * @since April 6, 2014
 */
@ArbutusConstraint(constraints = { "startDate <= endDate" })
public class ProgramOfferingType implements Serializable {

    private static final long serialVersionUID = -545312764862517427L;

    private static final int LENGTH_SMALL = 64;
    private static final int LENGTH_LARGE = 512;

    /**
     * Identification of a Program Offering
     */
    private Long offeringId;

    /**
     * The code of the specific program offering eg. K200
     */
    @NotNull
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String offeringCode;

    /**
     * The description of the specific offering eg. Pan Wash
     */
    @NotNull
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String offeringDescription;

    /**
     * Date on which the program offering starts
     */
    @NotNull
    private Date startDate = new Date();

    /**
     * Date on which the program offering ends
     */
    private Date endDate;

    /**
     * The end time of the each event of the schedule recurrence pattern
     */
    @NotNull
    private Date endTime;

    /**
     * Date on which the program offering is terminated
     */
    private Date expiryDate;

    /**
     * Derived based on the Expiry Date.
     * It is active if the current date is < Expiry Date.
     * Otherwise it is inactive.
     */
    private String status;

    /**
     * The flag to indicate if this recorder has been modified or not
     */
    private Boolean modified;

    /**
     * The number of spaces available in the program offering
     */
    @NotNull
    private Long capacity;

    /**
     * Derived based on Capacity minus the number of inmates allocated to the program offering.
     */
    private Long vacancy;

    /**
     * The cost of participating in the program offering
     */
    @Digits(integer = 9, fraction = 2)
    private BigDecimal participationFee;

    /**
     * Indicates whether the program is a one off session (yes) or has a repeating schedule (no).
     */
    @NotNull
    private Boolean singleOccurrence;

    /**
     * If true then a schedule will be generated for the inmate
     * when they are allocated to the program.
     * If false then no schedule will be generated.
     */
    @NotNull
    private Boolean generateSchedule;

    /**
     * Identification of ScheduleType
     */
    private Long scheduleId;

    /**
     * The days in a week the program runs
     * <p>MON, TUE, WED, THU, FRI, SAT, SUN</p>
     */
    private Set<String> recurranceDays;

    /**
     * Indicate the days when the program runs
     */
    private Long day;

    /**
     * The date on which the program runs. This only applies if a schedule must be generated.
     */
    private Date date;

    /**
     * Date and Time to be subtracted from schedule recurrence pattern
     */
    private Set<ScheduleSessionDateType> removedDates;

    /**
     * Date and Time to be added to schedule recurrence pattern
     */
    private Set<ScheduleSessionDateType> addedDates;

    /**
     * The description of the room where the program is being run. This is used for non-institutional programs.
     */
    @Size(max = LENGTH_LARGE, message = "max length 512")
    private String locationDescription;

    /**
     * If the program offering is terminated, indicate the reason.
     * <p>Metadata: OFFERINGTERMINATIONREASON: CANCEL, NOTAVAIL</p>
     */
    @Size(max = LENGTH_LARGE, message = "max length 512")
    private String terminationReason;

    /**
     * Additional optional comments about the program offering.
     */
    @Valid
    private CommentType comments;

    /**
     * The organization that is the provider of the program.
     */
    @NotNull
    private Long organizationId;

    /**
     * The program which is being offered.
     */
    private ProgramType program;

    /**
     * Person (non-staff) who is delivering the program (if applicable)
     */
    private Long personId;

    /**
     * isStaff -- True: is staff; False/Null: is not staff
     */
    private Boolean isStaff;

    /**
     * The facility where the program is being offered
     */
    private Long facilityId;

    /**
     * Used for non-institutional programs. The address where the program is being offered if not at a facility
     */
    private Long locationId;

    /**
     * Used only for institutional programs.
     * Indicates the actual internal location in the custodial facility
     * where the program is being run.
     */
    private Long internalLocationId;

    /**
     * The attributes of the inmates targeted for the program offering.
     * The attributes for the program offering are inherited from the program
     * but can be modified for the specific offering.
     */
    private Set<TargetInmateType> targetInmates;

    /**
     * The assignment of a program
     */
    private Set<ProgramAssignmentType> programAssignments;

    public ProgramOfferingType() {
        super();
    }

    /**
     * Identification of a Program Offering
     *
     * @return Long
     */
    public Long getOfferingId() {
        return offeringId;
    }

    /**
     * Identification of a Program Offering
     *
     * @param offeringId Long
     */
    public void setOfferingId(Long offeringId) {
        this.offeringId = offeringId;
    }

    /**
     * The code of the specific program offering eg. K200
     * <p>Max length 64</p>
     *
     * @return String
     */
    public String getOfferingCode() {
        return offeringCode;
    }

    /**
     * The code of the specific program offering eg. K200
     * <p>Max length 64</p>
     *
     * @param offeringCode String
     */
    public void setOfferingCode(String offeringCode) {
        this.offeringCode = offeringCode;
    }

    /**
     * The description of the specific offering eg. Pan Wash
     * <p>Max length 64</p>
     *
     * @return String
     */
    public String getOfferingDescription() {
        return offeringDescription;
    }

    /**
     * The description of the specific offering eg. Pan Wash
     * <p>Max length 64</p>
     *
     * @param offeringDescription
     */
    public void setOfferingDescription(String offeringDescription) {
        this.offeringDescription = offeringDescription;
    }

    /**
     * Date on which the program offering starts
     *
     * @return Date
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Date on which the program offering starts
     *
     * @param startDate Date
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Date on which the program offering ends
     *
     * @return Date
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Date on which the program offering ends
     *
     * @param endDate Date
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * The end time of the each event of the schedule recurrence pattern
     *
     * @return Date
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * The end time of the each event of the schedule recurrence pattern
     *
     * @param endTime Date
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * Date on which the program offering is terminated
     *
     * @return Date
     */
    public Date getExpiryDate() {
        return expiryDate;
    }

    /**
     * Date on which the program offering is terminated
     *
     * @param expiryDate Date
     */
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    /**
     * Derived based on the Expiry Date.
     * It is active if the current date is < Expiry Date.
     * Otherwise it is inactive.
     *
     * @return String
     */
    public String getStatus() {
        return status;
    }

    /**
     * Derived based on the Expiry Date.
     * It is active if the current date is < Expiry Date.
     * Otherwise it is inactive.
     *
     * @param status String
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * The flag to indicate if this recorder has been modified or not
     * <p>True: Modified; False: Not Modified</p>
     */
    public Boolean getModified() {
        return modified;
    }

    /**
     * The flag to indicate if this recorder has been modified or not
     * <p>True: Modified; False: Not Modified</p>
     */
    public void setModified(Boolean modified) {
        this.modified = modified;
    }

    /**
     * The number of spaces available in the program offering
     *
     * @return Long
     */
    public Long getCapacity() {
        return capacity;
    }

    /**
     * The number of spaces available in the program offering
     *
     * @param capacity Long
     */
    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    /**
     * Derived based on Capacity minus the number of inmates allocated to the program offering.
     *
     * @return Long
     */
    public Long getVacancy() {
        return vacancy;
    }

    /**
     * Derived based on Capacity minus the number of inmates allocated to the program offering.
     *
     * @param vacancy Long
     */
    public void setVacancy(Long vacancy) {
        this.vacancy = vacancy;
    }

    /**
     * The cost of participating in the program offering
     *
     * @return BigDecimal
     */
    public BigDecimal getParticipationFee() {
        return participationFee;
    }

    /**
     * The cost of participating in the program offering
     *
     * @param participationFee BigDecimal
     */
    public void setParticipationFee(BigDecimal participationFee) {
        this.participationFee = participationFee;
    }

    /**
     * Indicates whether the program is a one off session (yes) or has a repeating schedule (no).
     *
     * @return Boolean
     */
    public Boolean getSingleOccurrence() {
        return singleOccurrence;
    }

    /**
     * Indicates whether the program is a one off session (yes) or has a repeating schedule (no).
     *
     * @param singleOccurrence Boolean
     */
    public void setSingleOccurrence(Boolean singleOccurrence) {
        this.singleOccurrence = singleOccurrence;
    }

    /**
     * If true then a schedule will be generated for the inmate
     * when they are allocated to the program.
     * If false then no schedule will be generated.
     *
     * @return Boolean
     */
    public Boolean getGenerateSchedule() {
        return generateSchedule;
    }

    /**
     * If true then a schedule will be generated for the inmate
     * when they are allocated to the program.
     * If false then no schedule will be generated.
     *
     * @param generateSchedule Boolean
     */
    public void setGenerateSchedule(Boolean generateSchedule) {
        this.generateSchedule = generateSchedule;
    }

    /**
     * The days in a week the program runs
     * <p>MON, TUE, WED, THU, FRI, SAT, SUN</p>
     */
    public Set<String> getRecurranceDays() {
        return recurranceDays;
    }

    /**
     * The days in a week the program runs
     * <p>MON, TUE, WED, THU, FRI, SAT, SUN</p>
     */
    public void setRecurranceDays(Set<String> recurranceDays) {
        this.recurranceDays = recurranceDays;
    }

    /**
     * Identification of ScheduleType
     */
    public Long getScheduleId() {
        return scheduleId;
    }

    /**
     * Identification of ScheduleType
     */
    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    /**
     * Indicate the days when the program runs
     *
     * @return Long
     */
    public Long getDay() {
        return day;
    }

    /**
     * Indicate the days when the program runs
     *
     * @param day
     */
    public void setDay(Long day) {
        this.day = day;
    }

    /**
     * The date on which the program runs. This only applies if a schedule must be generated.
     *
     * @return Date
     */
    public Date getDate() {
        return date;
    }

    /**
     * The date on which the program runs. This only applies if a schedule must be generated.
     *
     * @param date Date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Date and Time to be subtracted from schedule recurrence pattern
     *
     * @return &lt;ScheduleSessionDateType>
     */
    public Set<ScheduleSessionDateType> getRemovedDates() {
        if (removedDates == null) {
            removedDates = new HashSet<>();
        }
        return removedDates;
    }

    /**
     * Date and Time to be subtracted from schedule recurrence pattern
     *
     * @param removedDates &lt;ScheduleSessionDateType>
     */
    public void setRemovedDates(Set<ScheduleSessionDateType> removedDates) {
        this.removedDates = removedDates;
    }

    /**
     * Date and Time to be added to schedule recurrence pattern
     *
     * @return &lt;AdditionDateType>
     */
    public Set<ScheduleSessionDateType> getAddedDates() {
        if (addedDates == null) {
            addedDates = new HashSet<>();
        }
        return addedDates;
    }

    /**
     * Date and Time to be added to schedule recurrence pattern
     *
     * @param addedDates &lt;AdditionDateType>
     */
    public void setAddedDates(Set<ScheduleSessionDateType> addedDates) {
        this.addedDates = addedDates;
    }

    /**
     * The description of the room where the program is being run. This is used for non-institutional programs.
     * <p>Max length 512</p>
     *
     * @return String
     */
    public String getLocationDescription() {
        return locationDescription;
    }

    /**
     * The description of the room where the program is being run. This is used for non-institutional programs.
     * <p>Max length 512</p>
     *
     * @param locationDescription String
     */
    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    /**
     * If the program offering is terminated, indicate the reason.
     * <p>Metadata: OFFERINGTERMINATIONREASON: CANCEL, NOTAVAIL</p>
     * <p>Max length 512</p>
     *
     * @return String
     */
    public String getTerminationReason() {
        return terminationReason;
    }

    /**
     * If the program offering is terminated, indicate the reason.
     * <p>Metadata: OFFERINGTERMINATIONREASON: CANCEL, NOTAVAIL</p>
     * <p>Max length 512</p>
     *
     * @param terminationReason String
     */
    public void setTerminationReason(String terminationReason) {
        this.terminationReason = terminationReason;
    }

    /**
     * Additional optional comments about the program offering.
     *
     * @return CommentType
     */
    public CommentType getComments() {
        return comments;
    }

    /**
     * Additional optional comments about the program offering.
     *
     * @param comments CommentType
     */
    public void setComments(CommentType comments) {
        this.comments = comments;
    }

    /**
     * The organization that is the provider of the program.
     *
     * @return Long
     */
    public Long getOrganizationId() {
        return organizationId;
    }

    /**
     * The organization that is the provider of the program.
     *
     * @param organizationId Long
     */
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * The program which is being offered.
     *
     * @return ProgramType
     */
    public ProgramType getProgram() {
        return program;
    }

    /**
     * The program which is being offered.
     *
     * @param program ProgramType
     */
    public void setProgram(ProgramType program) {
        this.program = program;
    }

    /**
     * Person (non-staff) who is delivering the program (if applicable)
     *
     * @return Long
     */
    public Long getPersonId() {
        return personId;
    }

    /**
     * Person (non-staff) who is delivering the program (if applicable)
     *
     * @param personId Long
     */
    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    /**
     * isStaff -- True: is staff; False/Null: is not staff
     *
     * @return Boolean
     */
    public Boolean getIsStaff() {
        return isStaff;
    }

    /**
     * isStaff -- True: is staff; False/Null: is not staff
     *
     * @param isStaff Boolean
     */
    public void setIsStaff(Boolean isStaff) {
        this.isStaff = isStaff;
    }

    /**
     * The facility where the program is being offered
     *
     * @return Long
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * The facility where the program is being offered
     *
     * @param facilityId Long
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * Used for non-institutional programs. The address where the program is being offered if not at a facility
     *
     * @return Long
     */
    public Long getLocationId() {
        return locationId;
    }

    /**
     * Used for non-institutional programs. The address where the program is being offered if not at a facility
     *
     * @param locationId Long
     */
    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    /**
     * Used only for institutional programs.
     * Indicates the actual internal location in the custodial facility
     * where the program is being run.
     *
     * @return Long
     */
    public Long getInternalLocationId() {
        return internalLocationId;
    }

    /**
     * Used only for institutional programs.
     * Indicates the actual internal location in the custodial facility
     * where the program is being run.
     *
     * @param internalLocationId Long
     */
    public void setInternalLocationId(Long internalLocationId) {
        this.internalLocationId = internalLocationId;
    }

    /**
     * The attributes of the inmates targeted for the program offering.
     * The attributes for the program offering are inherited from the program
     * but can be modified for the specific offering.
     *
     * @return &lt;TargetInmateType>
     */
    public Set<TargetInmateType> getTargetInmates() {
        return targetInmates;
    }

    /**
     * The attributes of the inmates targeted for the program offering.
     * The attributes for the program offering are inherited from the program
     * but can be modified for the specific offering.
     *
     * @param targetInmates &lt;TargetInmateType>
     */
    public void setTargetInmates(Set<TargetInmateType> targetInmates) {
        this.targetInmates = targetInmates;
    }

    /**
     * The assignment of a program
     *
     * @return &lt;ProgramAssignmentType>
     */
    public Set<ProgramAssignmentType> getProgramAssignments() {
        return programAssignments;
    }

    /**
     * The assignment of a program
     *
     * @param programAssignments &lt;ProgramAssignmentType>
     */
    public void setProgramAssignments(Set<ProgramAssignmentType> programAssignments) {
        this.programAssignments = programAssignments;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((capacity == null) ? 0 : capacity.hashCode());
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((day == null) ? 0 : day.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((expiryDate == null) ? 0 : expiryDate.hashCode());
        result = prime * result + ((facilityId == null) ? 0 : facilityId.hashCode());
        result = prime * result + ((generateSchedule == null) ? 0 : generateSchedule.hashCode());
        result = prime * result + ((internalLocationId == null) ? 0 : internalLocationId.hashCode());
        result = prime * result + ((locationId == null) ? 0 : locationId.hashCode());
        result = prime * result + ((offeringCode == null) ? 0 : offeringCode.hashCode());
        result = prime * result + ((offeringDescription == null) ? 0 : offeringDescription.hashCode());
        result = prime * result + ((offeringId == null) ? 0 : offeringId.hashCode());
        result = prime * result + ((organizationId == null) ? 0 : organizationId.hashCode());
        result = prime * result + ((personId == null) ? 0 : personId.hashCode());
        result = prime * result + ((isStaff == null) ? 0 : isStaff.hashCode());
        result = prime * result + ((singleOccurrence == null) ? 0 : singleOccurrence.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((terminationReason == null) ? 0 : terminationReason.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ProgramOfferingType other = (ProgramOfferingType) obj;
        if (capacity == null) {
            if (other.capacity != null) {
				return false;
			}
        } else if (!capacity.equals(other.capacity)) {
			return false;
		}
        if (date == null) {
            if (other.date != null) {
				return false;
			}
        } else if (!date.equals(other.date)) {
			return false;
		}
        if (day == null) {
            if (other.day != null) {
				return false;
			}
        } else if (!day.equals(other.day)) {
			return false;
		}
        if (endDate == null) {
            if (other.endDate != null) {
				return false;
			}
        } else if (!endDate.equals(other.endDate)) {
			return false;
		}
        if (expiryDate == null) {
            if (other.expiryDate != null) {
				return false;
			}
        } else if (!expiryDate.equals(other.expiryDate)) {
			return false;
		}
        if (facilityId == null) {
            if (other.facilityId != null) {
				return false;
			}
        } else if (!facilityId.equals(other.facilityId)) {
			return false;
		}
        if (generateSchedule == null) {
            if (other.generateSchedule != null) {
				return false;
			}
        } else if (!generateSchedule.equals(other.generateSchedule)) {
			return false;
		}
        if (internalLocationId == null) {
            if (other.internalLocationId != null) {
				return false;
			}
        } else if (!internalLocationId.equals(other.internalLocationId)) {
			return false;
		}
        if (locationId == null) {
            if (other.locationId != null) {
				return false;
			}
        } else if (!locationId.equals(other.locationId)) {
			return false;
		}
        if (offeringCode == null) {
            if (other.offeringCode != null) {
				return false;
			}
        } else if (!offeringCode.equals(other.offeringCode)) {
			return false;
		}
        if (offeringDescription == null) {
            if (other.offeringDescription != null) {
				return false;
			}
        } else if (!offeringDescription.equals(other.offeringDescription)) {
			return false;
		}
        if (offeringId == null) {
            if (other.offeringId != null) {
				return false;
			}
        } else if (!offeringId.equals(other.offeringId)) {
			return false;
		}
        if (organizationId == null) {
            if (other.organizationId != null) {
				return false;
			}
        } else if (!organizationId.equals(other.organizationId)) {
			return false;
		}
        if (personId == null) {
            if (other.personId != null) {
				return false;
			}
        } else if (!personId.equals(other.personId)) {
			return false;
		}
        if (isStaff == null) {
            if (other.isStaff != null) {
				return false;
			}
        } else if (!isStaff.equals(other.isStaff)) {
			return false;
		}
        if (singleOccurrence == null) {
            if (other.singleOccurrence != null) {
				return false;
			}
        } else if (!singleOccurrence.equals(other.singleOccurrence)) {
			return false;
		}
        if (startDate == null) {
            if (other.startDate != null) {
				return false;
			}
        } else if (!startDate.equals(other.startDate)) {
			return false;
		}
        if (terminationReason == null) {
            if (other.terminationReason != null) {
				return false;
			}
        } else if (!terminationReason.equals(other.terminationReason)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "ProgramOfferingType{" +
                "offeringId=" + offeringId +
                ", offeringCode='" + offeringCode + '\'' +
                ", offeringDescription='" + offeringDescription + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", endTime=" + endTime +
                ", expiryDate=" + expiryDate +
                ", status='" + status + '\'' +
                ", modified=" + modified +
                ", capacity=" + capacity +
                ", vacancy=" + vacancy +
                ", participationFee=" + participationFee +
                ", singleOccurrence=" + singleOccurrence +
                ", generateSchedule=" + generateSchedule +
                ", scheduleId=" + scheduleId +
                ", recurranceDays=" + recurranceDays +
                ", day=" + day +
                ", date=" + date +
                ", locationDescription='" + locationDescription + '\'' +
                ", terminationReason='" + terminationReason + '\'' +
                ", comments=" + comments +
                ", organizationId=" + organizationId +
                ", program=" + program +
                ", personId=" + personId +
                ", isStaff=" + isStaff +
                ", facilityId=" + facilityId +
                ", locationId=" + locationId +
                ", internalLocationId=" + internalLocationId +
                ", targetInmates=" + targetInmates +
                '}';
    }
}
