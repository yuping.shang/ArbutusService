package syscon.arbutus.product.services.program.contract.dto;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * ProgramAssignmentSearchType for Program and Services
 * <p>Used for search Program Assignment Search</p>
 *
 * @author YShang
 * @since May 27, 2014
 */
public class ProgramAssignmentSearchType implements Serializable {

    private static final long serialVersionUID = -3110213732122859149L;

    private static final int LENGTH_SMALL = 64;

    /**
     * Date on which the referral to a program offering was made.
     * Only applies if a referral was made and the inmate was not directly placed in the program.
     */
    private Date fromReferralDate;

    /**
     * Date on which the referral to a program offering was made.
     * Only applies if a referral was made and the inmate was not directly placed in the program.
     */
    private Date toReferralDate;

    /**
     * A general priority for the referral.
     * Only applies if a referral was made and the inmate was not directly placed in the program
     */
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String referralPriority;

    /**
     * If the program provider rejects the referral, indicate the reason.
     * Only applies if a referral was made and the inmate was not directly placed in the program.
     */
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String referralRejectionReason;

    /**
     * Date on which the inmate starts the program.
     * Only applies when the inmate is placed in a program offering.
     */
    private Date fromStartDate;

    /**
     * Date on which the inmate starts the program.
     * Only applies when the inmate is placed in a program offering.
     */
    private Date toStartDate;

    /**
     * Date on which the inmate stops the program.
     * Only applies when the inmate is placed in a program offering.
     */
    private Date fromEndDate;

    /**
     * Date on which the inmate stops the program.
     * Only applies when the inmate is placed in a program offering.
     */
    private Date toEndDate;

    /**
     * Indicates the current status of the assignment eg. Referred, Allocated, Completed
     */
    private Set<String> status;

    /**
     * If the inmate is suspended from the program, indicate the reason
     */
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String suspendReason;

    /**
     * If the inmate abandons the program, indicate the reason
     */
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String abandonReason;

    /**
     * Additional optional comments about the program assignment
     */
    private String comments;

    /**
     * The inmate/supervision who is being referred to or placed in the program
     */
    private Long supervisionId;

    /**
     * Program Offering Id of the Program Assienment
     */
    private Long programOfferingId;

    /**
     * Program Id of the Program Assienment
     */
    private Long programgId;

    public ProgramAssignmentSearchType() {
    }

    /**
     * Date on which the referral to a program offering was made.
     * Only applies if a referral was made and the inmate was not directly placed in the program.
     */
    public Date getFromReferralDate() {
        return fromReferralDate;
    }

    /**
     * Date on which the referral to a program offering was made.
     * Only applies if a referral was made and the inmate was not directly placed in the program.
     */
    public void setFromReferralDate(Date fromReferralDate) {
        this.fromReferralDate = fromReferralDate;
    }

    /**
     * Date on which the referral to a program offering was made.
     * Only applies if a referral was made and the inmate was not directly placed in the program.
     */
    public Date getToReferralDate() {
        return toReferralDate;
    }

    /**
     * Date on which the referral to a program offering was made.
     * Only applies if a referral was made and the inmate was not directly placed in the program.
     */
    public void setToReferralDate(Date toReferralDate) {
        this.toReferralDate = toReferralDate;
    }

    /**
     * A general priority for the referral.
     * Only applies if a referral was made and the inmate was not directly placed in the program
     */
    public String getReferralPriority() {
        return referralPriority;
    }

    /**
     * A general priority for the referral.
     * Only applies if a referral was made and the inmate was not directly placed in the program
     */
    public void setReferralPriority(String referralPriority) {
        this.referralPriority = referralPriority;
    }

    /**
     * If the program provider rejects the referral, indicate the reason.
     * Only applies if a referral was made and the inmate was not directly placed in the program.
     */
    public String getReferralRejectionReason() {
        return referralRejectionReason;
    }

    /**
     * If the program provider rejects the referral, indicate the reason.
     * Only applies if a referral was made and the inmate was not directly placed in the program.
     */
    public void setReferralRejectionReason(String referralRejectionReason) {
        this.referralRejectionReason = referralRejectionReason;
    }

    /**
     * Date on which the inmate starts the program.
     * Only applies when the inmate is placed in a program offering.
     */
    public Date getFromStartDate() {
        return fromStartDate;
    }

    /**
     * Date on which the inmate starts the program.
     * Only applies when the inmate is placed in a program offering.
     */
    public void setFromStartDate(Date fromStartDate) {
        this.fromStartDate = fromStartDate;
    }

    /**
     * Date on which the inmate starts the program.
     * Only applies when the inmate is placed in a program offering.
     */
    public Date getToStartDate() {
        return toStartDate;
    }

    /**
     * Date on which the inmate starts the program.
     * Only applies when the inmate is placed in a program offering.
     */
    public void setToStartDate(Date toStartDate) {
        this.toStartDate = toStartDate;
    }

    /**
     * Date on which the inmate stops the program.
     * Only applies when the inmate is placed in a program offering.
     */
    public Date getFromEndDate() {
        return fromEndDate;
    }

    /**
     * Date on which the inmate stops the program.
     * Only applies when the inmate is placed in a program offering.
     */
    public void setFromEndDate(Date fromEndDate) {
        this.fromEndDate = fromEndDate;
    }

    /**
     * Date on which the inmate stops the program.
     * Only applies when the inmate is placed in a program offering.
     */
    public Date getToEndDate() {
        return toEndDate;
    }

    /**
     * Date on which the inmate stops the program.
     * Only applies when the inmate is placed in a program offering.
     */
    public void setToEndDate(Date toEndDate) {
        this.toEndDate = toEndDate;
    }

    /**
     * Indicates the current status of the assignment eg. Referred, Allocated, Completed
     */
    public Set<String> getStatus() {
        return status;
    }

    /**
     * Indicates the current status of the assignment eg. Referred, Allocated, Completed
     */
    public void setStatus(Set<String> status) {
        this.status = status;
    }

    /**
     * If the inmate is suspended from the program, indicate the reason
     */
    public String getSuspendReason() {
        return suspendReason;
    }

    /**
     * If the inmate is suspended from the program, indicate the reason
     */
    public void setSuspendReason(String suspendReason) {
        this.suspendReason = suspendReason;
    }

    /**
     * If the inmate abandons the program, indicate the reason
     */
    public String getAbandonReason() {
        return abandonReason;
    }

    /**
     * If the inmate abandons the program, indicate the reason
     */
    public void setAbandonReason(String abandonReason) {
        this.abandonReason = abandonReason;
    }

    /**
     * Additional optional comments about the program assignment
     */
    public String getComments() {
        return comments;
    }

    /**
     * Additional optional comments about the program assignment
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * The inmate/supervision who is being referred to or placed in the program
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * The inmate/supervision who is being referred to or placed in the program
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Program Offering Id of the Program Assienment
     *
     * @return Long programOfferingId
     */
    public Long getProgramOfferingId() {
        return programOfferingId;
    }

    /**
     * Program Offering Id of the Program Assienment
     *
     * @param programOfferingId
     */
    public void setProgramOfferingId(Long programOfferingId) {
        this.programOfferingId = programOfferingId;
    }

    /**
     * Program Id of the Program Assienment
     *
     * @return Long programId
     */
    public Long getProgramgId() {
        return programgId;
    }

    /**
     * Program Id of the Program Assienment
     *
     * @param programId
     */
    public void setProgramgId(Long programgId) {
        this.programgId = programgId;
    }

    @Override
    public String toString() {
        return "ProgramAssignmentSearchType{" + "fromReferralDate=" + fromReferralDate + ", toReferralDate=" + toReferralDate + ", referralPriority='" + referralPriority
                + '\'' + ", referralRejectionReason='" + referralRejectionReason + '\'' + ", fromStartDate=" + fromStartDate + ", toStartDate=" + toStartDate
                + ", fromEndDate=" + fromEndDate + ", toEndDate=" + toEndDate + ", status=" + status + ", suspendReason='" + suspendReason + '\'' + ", abandonReason='"
                + abandonReason + '\'' + ", comments='" + comments + '\'' + ", supervisionId=" + supervisionId + '}';
    }
}
