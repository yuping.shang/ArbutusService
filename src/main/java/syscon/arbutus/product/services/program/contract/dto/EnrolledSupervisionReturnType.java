package syscon.arbutus.product.services.program.contract.dto;

import java.io.Serializable;
import java.util.List;

/**
 * EnrolledSupervisionReturnType for Program and Services
 * <p>The return of enrolled supervision from method call getEnrolledSupervisions</p>
 *
 * @author YShang
 * @since July 24, 2014
 */
public class EnrolledSupervisionReturnType implements Serializable {

    private static final long serialVersionUID = -3981268826566159608L;

    /**
     * Enrolled Supervisions
     */
    private List<EnrolledSupervisionType> enrolledSupervisions;

    /**
     * Total size from the method call getEnrolledSupervisions
     */
    private Long totalSize;

    public EnrolledSupervisionReturnType() {
        super();
    }

    /**
     * Enrolled Supervisions
     *
     * @return List&lt;EnrolledSupervisionType>
     */
    public List<EnrolledSupervisionType> getEnrolledSupervisions() {
        return enrolledSupervisions;
    }

    /**
     * Enrolled Supervisions
     *
     * @param enrolledSupervisions List&lt;EnrolledSupervisionType>
     */
    public void setEnrolledSupervisions(List<EnrolledSupervisionType> enrolledSupervisions) {
        this.enrolledSupervisions = enrolledSupervisions;
    }

    /**
     * Total size from the method call getEnrolledSupervisions
     *
     * @return Long
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * Total size from the method call getEnrolledSupervisions
     *
     * @param totalSize Long
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    @Override
    public String toString() {
        return "EnrolledSupervisionReturnType [enrolledSupervisions=" + enrolledSupervisions + ", totalSize=" + totalSize + "]";
    }

}
