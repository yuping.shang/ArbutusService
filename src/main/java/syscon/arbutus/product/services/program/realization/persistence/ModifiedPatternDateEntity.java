package syscon.arbutus.product.services.program.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Modified Schedule Pattern Date Entity for Program and Service
 *
 * @author YShang
 * @DbComment PRG_ModifiedPatternDATE 'Modified Schedule Pattern Date Entity for Program and Service'
 * @since July 16, 2014
 */
@Audited
@Entity
@Table(name = "PRG_ModifiedPatternDATE")
public class ModifiedPatternDateEntity implements Serializable {

    private static final long serialVersionUID = -9218840218086661535L;

    /**
     * @DbComment PRG_ModifiedPatternDATE.modifiedPatternDateId 'Id of modified date to schedule recurrence pattern'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PRG_MODIFIEDPATTERNDATE_ID")
    @SequenceGenerator(name = "SEQ_PRG_MODIFIEDPATTERNDATE_ID", sequenceName = "SEQ_PRG_MODIFIEDPATTERNDATE_ID", allocationSize = 1)
    @Column(name = "modifiedPatternDateId")
    private Long modifiedPatternDateId;

    /**
     * @DbComment PRG_ModifiedPatternDATE.thedate 'the modified date to schedule recurrence pattern'
     */
    @Column(name = "thedate", nullable = false)
    private Date date;

    /**
     * @DbComment PRG_ModifiedPatternDATE.startTime 'Start time (time only has meaning)'
     */
    @Column(name = "startTime")
    private Date startTime;

    /**
     * @DbComment PRG_ModifiedPatternDATE.endTime 'End time (time only has meaning)'
     */
    @Column(name = "endTime")
    private Date endTime;

    /**
     * @DbComment PRG_ModifiedPatternDATE.previousTime 'The schedule session previous time in String format(yyyymmddhhhh)'
     */
    @Column(name = "previousTime", length = 64)
    private String previousTime;

    /**
     * @DbComment PRG_ModifiedPatternDATE.previousStatus 'The schedule session previous status'
     */
    @Column(name = "previousStatus", length = 64)
    private String previousStatus;

    /**
     * @DbComment PRG_ModifiedPatternDATE.status 'The schedule session status'
     */
    @Column(name = "status", length = 64)
    private String status;

    /**
     * @DbComment PRG_ModifiedPatternDATE.offeringId 'Foreign key to PRG_PRGOFFER table'
     */
    @ForeignKey(name = "FK_PRG_SUBTRACTIONDATE")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "offeringId", nullable = false)
    private ProgramOfferingEntity programOffering;

    /**
     * @DbComment PRG_ModifiedPatternDATE.createUserId 'Create User Id'
     * @DbComment PRG_ModifiedPatternDATE.createDateTime 'Create Date Time'
     * @DbComment PRG_ModifiedPatternDATE.modifyUserId 'Modify User Id'
     * @DbComment PRG_ModifiedPatternDATE.modifyDateTime 'Modify Date Time'
     * @DbComment PRG_ModifiedPatternDATE.invocationContext 'Invocation Context'
     */
    @Embedded
    @NotAudited
    private StampEntity stamp;

    public ModifiedPatternDateEntity() {
    }

    public Long getModifiedPatternDateId() {
        return modifiedPatternDateId;
    }

    public void setModifiedPatternDateId(Long modifiedPatternDateId) {
        this.modifiedPatternDateId = modifiedPatternDateId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getPreviousTime() {
        return previousTime;
    }

    public void setPreviousTime(String previousTime) {
        this.previousTime = previousTime;
    }

    public String getPreviousStatus() {
        return previousStatus;
    }

    public void setPreviousStatus(String previousStatus) {
        this.previousStatus = previousStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProgramOfferingEntity getProgramOffering() {
        return programOffering;
    }

    public void setProgramOffering(ProgramOfferingEntity programOffering) {
        this.programOffering = programOffering;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
        result = prime * result + (programOffering != null && programOffering.getOfferingId() != null ? programOffering.getOfferingId().hashCode() : 0);
        result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
        result = prime * result + ((previousTime == null) ? 0 : previousTime.hashCode());
        result = prime * result + ((previousStatus == null) ? 0 : previousStatus.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ModifiedPatternDateEntity other = (ModifiedPatternDateEntity) obj;
        if (date == null) {
            if (other.date != null) {
				return false;
			}
        } else if (!date.equals(other.date)) {
			return false;
		}
        if (endTime == null) {
            if (other.endTime != null) {
				return false;
			}
        } else if (!endTime.equals(other.endTime)) {
			return false;
		}
        if (programOffering == null) {
            if (other.programOffering != null) {
				return false;
			}
        } else if (programOffering != null && programOffering.getOfferingId() != null ?
                !programOffering.getOfferingId().equals(((ModifiedPatternDateEntity) obj).programOffering.getOfferingId()) :
                ((ModifiedPatternDateEntity) obj).programOffering != null && ((ModifiedPatternDateEntity) obj).programOffering.getOfferingId() != null) {
			return false;
		}
        if (startTime == null) {
            if (other.startTime != null) {
				return false;
			}
        } else if (!startTime.equals(other.startTime)) {
			return false;
		}
        if (previousTime == null) {
            if (other.previousTime != null) {
				return false;
			}
        } else if (!previousTime.equals(other.previousTime)) {
			return false;
		}
        if (previousStatus == null) {
            if (other.previousStatus != null) {
				return false;
			}
        } else if (!previousStatus.equals(other.previousStatus)) {
			return false;
		}
        if (status == null) {
            if (other.status != null) {
				return false;
			}
        } else if (!status.equals(other.status)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "ModifiedPatternDateEntity{" +
                "modifiedPatternDateId=" + modifiedPatternDateId +
                ", date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", previousTime=" + previousTime +
                ", previousStatus=" + previousStatus +
                ", status=" + status +
                '}';
    }
}
