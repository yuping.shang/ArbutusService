package syscon.arbutus.product.services.program.contract.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Program Offering Return Type -- The return of the program offering search
 *
 * @author YShang
 * @since April 17, 2014
 */
public class ProgramOfferingReturnType implements Serializable {
    private static final long serialVersionUID = -567583856085306798L;

    /**
     * A list of Program Offering from method call searchProgramOffering
     */
    private List<ProgramOfferingType> programOfferings;

    /**
     * Total size of the returning from method call searchProgramOffering
     */
    private Long totalSize;

    public ProgramOfferingReturnType() {
        super();
    }

    /**
     * A list of Program Offering from the result of search
     *
     * @return A list of Program Offering from the result of search
     */
    public List<ProgramOfferingType> getProgramOfferings() {
        return programOfferings;
    }

    /**
     * A list of Program Offering from the result of search
     *
     * @param programOfferings List&lt;ProgramOfferingType>
     */
    public void setProgramOfferings(List<ProgramOfferingType> programOfferings) {
        this.programOfferings = programOfferings;
    }

    /**
     * Totla Size of the recorder of Program Offering from the result of search
     *
     * @return Totla Size of the recorder of Program Offering from the result of search
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * Totla Size of the recorder of Program Offering from the result of search
     *
     * @param totalSize Long
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    @Override
    public String toString() {
        return "ProgramOfferingReturnType [programOfferings=" + programOfferings + ", totalSize=" + totalSize + "]";
    }
}
