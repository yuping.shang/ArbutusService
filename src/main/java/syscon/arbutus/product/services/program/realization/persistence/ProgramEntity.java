package syscon.arbutus.product.services.program.realization.persistence;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Program Entity for Program and Service
 *
 * @author YShang
 * @DbComment PRG_Program 'Program Entity for Program and Service'
 * @since April 6, 2014
 */
@Audited
@Entity
@Table(name = "PRG_Program")
@SQLDelete(sql = "UPDATE PRG_Program SET flag = 4 WHERE programId = ? and version = ?")
@Where(clause = "flag = 1")
public class ProgramEntity implements Serializable {

    private static final long serialVersionUID = -5630778725505529827L;

    /**
     * @DbComment PRG_Program.programId 'Identification of a Program'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PRG_PROGRAM_ID")
    @SequenceGenerator(name = "SEQ_PRG_PROGRAM_ID", sequenceName = "SEQ_PRG_PROGRAM_ID", allocationSize = 1)
    @Column(name = "programId")
    private Long programId;

    /**
     * @DbComment PRG_Program.programCode 'The code that uniquely identifies the program Eg. KITCHEN'
     */
    @NotNull
    @Size(max = 64, message = "max length 64")
    @Column(name = "programCode", nullable = false, length = 64)
    private String programCode;

    /**
     * @DbComment PRG_Program.programDescription 'The name of the program eg. Kitchen Duty'
     */
    @NotNull
    @Size(max = 64, message = "max length 64")
    @Column(name = "programDescription", nullable = false, length = 64)
    private String programDescription;

    /**
     * @DbComment PRG_Program.programCategory 'Indicates the general category that the program falls under, eg. Institutional Activity, Accredited Program, Community Service. Different functionality will apply to each type of program within the system. Metadata: PROGRAMCATEGORY: ACCRED, COMM, INSTITUT'
     */
    @NotNull
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.PROGRAM_CATEGORY)
    @Column(name = "programCategory", nullable = false, length = 64)
    private String programCategory;

    /**
     * @DbComment PRG_Program.caseWorkStep 'Indicates the caseWorkStep assigned to the program'
     */
    // @NotNull
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.PROGRAM_WORKSTEP)
    @Column(name = "programCaseWorkStep", nullable = true, length = 64)
    private String programCaseWorkStep;

    /**
     * @DbComment PRG_Program.expiryDate 'Date on which the program is no longer offered'
     */
    @Column(name = "expiryDate")
    private Date expiryDate;

    /**
     * @DbComment PRG_Program.programDuration 'Number of days that the program runs for'
     */
    @NotNull
    @Column(name = "programDuration", nullable = false)
    private Long programDuration;

    /**
     * @DbComment PRG_Program.flexibleStart 'Indicates whether an inmate can start the program part way, ie. after the Start Date of the offering has passed (yes) or not (no).'
     */
    @NotNull
    @Column(name = "flexibleStart", nullable = false)
    private Boolean flexibleStart;

    /**
     * @DbComment PRG_Program.terminationReason 'If the program is terminated, indicate the reason. Required If Expiry Date is provided. Metadata: PROGRAMTERMINATIONREASON: NOTOFFERED'
     */
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.PROGRAM_TERMINATION_REASON)
    @Column(name = "terminationReason", length = 64)
    private String terminationReason;

    /**
     * @DbComment PRG_Program.CommentUserId 'User ID who created/updated the comments'
     * @DbComment PRG_Program.CommentDate 'The Date/Time when the comment was created'
     * @DbComment PRG_Program.CommentText 'The comment text'
     */
    @Embedded
    private CommentEntity comments;

    @OneToMany(mappedBy = "program", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
    private Set<TargetInmateEntity> targetInmates;

    /**
     * @DbComment PRG_Program.flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * @DbComment PRG_Program.version 'Version for Hibernate use'
     */
    @NotAudited
    @Version
    @Column(name = "version")
    private Long version;

    /**
     * @DbComment PRG_PROGRAM.createUserId 'Create User Id'
     * @DbComment PRG_PROGRAM.createDateTime 'Create Date Time'
     * @DbComment PRG_PROGRAM.modifyUserId 'Modify User Id'
     * @DbComment PRG_PROGRAM.modifyDateTime 'Modify Date Time'
     * @DbComment PRG_PROGRAM.invocationContext 'Invocation Context'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public ProgramEntity() {
        super();
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getProgramCode() {
        return programCode;
    }

    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }

    public String getProgramDescription() {
        return programDescription;
    }

    public void setProgramDescription(String programDescription) {
        this.programDescription = programDescription;
    }

    public String getProgramCategory() {
        return programCategory;
    }

    public void setProgramCategory(String programCategory) {
        this.programCategory = programCategory;
    }

    public String getProgramCaseWorkStep() {
        return programCaseWorkStep;
    }

    public void setProgramCaseWorkStep(String programCaseWorkStep) {
        this.programCaseWorkStep = programCaseWorkStep;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Long getProgramDuration() {
        return programDuration;
    }

    public void setProgramDuration(Long programDuration) {
        this.programDuration = programDuration;
    }

    public Boolean getFlexibleStart() {
        return flexibleStart;
    }

    public void setFlexibleStart(Boolean flexibleStart) {
        this.flexibleStart = flexibleStart;
    }

    public String getTerminationReason() {
        return terminationReason;
    }

    public void setTerminationReason(String terminationReason) {
        this.terminationReason = terminationReason;
    }

    public CommentEntity getComments() {
        return comments;
    }

    public void setComments(CommentEntity comments) {
        this.comments = comments;
    }

    public Set<TargetInmateEntity> getTargetInmates() {
        if (targetInmates == null) {
			targetInmates = new HashSet<>();
		}
        return targetInmates;
    }

    public void setTargetInmates(Set<TargetInmateEntity> targetInmates) {
        this.targetInmates = targetInmates;
    }

    public void addTargetInmate(TargetInmateEntity targetInmate) {
        targetInmate.setProgram(this);
        getTargetInmates().add(targetInmate);
    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((expiryDate == null) ? 0 : expiryDate.hashCode());
        result = prime * result + ((flexibleStart == null) ? 0 : flexibleStart.hashCode());
        result = prime * result + ((programCategory == null) ? 0 : programCategory.hashCode());
        result = prime * result + ((programCode == null) ? 0 : programCode.hashCode());
        result = prime * result + ((programDescription == null) ? 0 : programDescription.hashCode());
        result = prime * result + ((programDuration == null) ? 0 : programDuration.hashCode());
        result = prime * result + ((programId == null) ? 0 : programId.hashCode());
        result = prime * result + ((terminationReason == null) ? 0 : terminationReason.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ProgramEntity other = (ProgramEntity) obj;
        if (expiryDate == null) {
            if (other.expiryDate != null) {
				return false;
			}
        } else if (!expiryDate.equals(other.expiryDate)) {
			return false;
		}
        if (flexibleStart == null) {
            if (other.flexibleStart != null) {
				return false;
			}
        } else if (!flexibleStart.equals(other.flexibleStart)) {
			return false;
		}
        if (programCategory == null) {
            if (other.programCategory != null) {
				return false;
			}
        } else if (!programCategory.equals(other.programCategory)) {
			return false;
		}
        if (programCode == null) {
            if (other.programCode != null) {
				return false;
			}
        } else if (!programCode.equals(other.programCode)) {
			return false;
		}
        if (programDescription == null) {
            if (other.programDescription != null) {
				return false;
			}
        } else if (!programDescription.equals(other.programDescription)) {
			return false;
		}
        if (programDuration == null) {
            if (other.programDuration != null) {
				return false;
			}
        } else if (!programDuration.equals(other.programDuration)) {
			return false;
		}
        if (programId == null) {
            if (other.programId != null) {
				return false;
			}
        } else if (!programId.equals(other.programId)) {
			return false;
		}
        if (terminationReason == null) {
            if (other.terminationReason != null) {
				return false;
			}
        } else if (!terminationReason.equals(other.terminationReason)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "ProgramEntity [programId=" + programId + ", programCode=" + programCode + ", programDescription=" + programDescription + ", programCategory="
                + programCategory + ", expiryDate=" + expiryDate + ", programDuration=" + programDuration + ", flexibleStart=" + flexibleStart + ", terminationReason="
                + terminationReason + ", comments=" + comments + ", targetInmates=" + targetInmates + "]";
    }
}
