package syscon.arbutus.product.services.program.contract.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * Program Type for Program and Service
 *
 * @author YShang
 * @author ashish.kumar
 * @since April 6, 2014
 */
/**
 * @author ashish.kumar
 *
 */

/**
 * @author ashish.kumar
 */
public class ProgramType implements Serializable {

    private static final long serialVersionUID = -5630778725505529827L;

    private static final int LENGTH_SMALL = 64;

    /**
     * Identification of a Program
     */
    private Long programId;

    /**
     * The code that uniquely identifies the program Eg. KITCHEN
     */
    @NotNull
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String programCode;

    /**
     * The name of the program eg. Kitchen Duty
     */
    @NotNull
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String programDescription;

    /**
     * Indicates the general category that the program falls under
     * eg. Institutional Activity, Accredited Program, Community Service.
     * Different functionality will apply to each type of program within the system.
     * <p>Metadata -- PROGRAMCATEGORY: ACCRED, COMM, INSTITUT</p>
     */
    @NotNull
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String programCategory;

    /**
     * Indicates the work step that the program falls under
     * eg. Attend AA/NA, CTU Referral, Job Placement, Program Attendance
     * Different functionality will apply to each type of program within the system.
     * <p>Metadata -- CASEWORKSTEPS: AA, CTU, JOB, PRGM</p>
     */
    // @NotNull
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String programCaseWorkStep;

    /**
     * Date on which the program is no longer offered
     */
    private Date expiryDate;

    /**
     * Derived based on the Expiry Date. It is active
     * if the current date is < Expiry Date. Otherwise it is inactive.
     */
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String status;

    /**
     * Number of days that the program runs for
     */
    @NotNull
    private Long programDuration;

    /**
     * Indicates whether an inmate can start the program part way
     * ie. after the Start Date of the offering has passed (yes) or not (no).
     */
    @NotNull
    private Boolean flexibleStart;

    /**
     * If the program is terminated, indicate the reason.
     * <p>Required If Expiry Date is provided.
     * <p>Metadata: OFFERINGTERMINATIONREASON: CANCEL, NOTAVAIL
     */
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String terminationReason;

    /**
     * Comment
     */
    @Valid
    private CommentType comments;

    /**
     * Target Inmates
     */
    private Set<TargetInmateType> targetInmates;

    public ProgramType() {
        super();
    }

    /**
     * Identification of a Program
     * <p>Optional when create; Required when update</p>
     *
     * @return Long
     */
    public Long getProgramId() {
        return programId;
    }

    /**
     * Identification of a Program
     * <p>Optional when create; Required when update</p>
     *
     * @param programId Long
     */
    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    /**
     * The code that uniquely identifies the program Eg. KITCHEN
     * <p>Max length 64; Required</p>
     *
     * @return String
     */
    public String getProgramCode() {
        return programCode;
    }

    /**
     * The code that uniquely identifies the program Eg. KITCHEN
     * <p>Max length 64; Required</p>
     *
     * @param programCode String
     */
    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }

    /**
     * The name of the program eg. Kitchen Duty
     * <p>Max length 64; Required</p>
     *
     * @return String
     */
    public String getProgramDescription() {
        return programDescription;
    }

    /**
     * The name of the program eg. Kitchen Duty
     * <p>Max length 64; Required</p>
     *
     * @param programDescription String
     */
    public void setProgramDescription(String programDescription) {
        this.programDescription = programDescription;
    }

    /**
     * Indicates the general category that the program falls under
     * eg. Institutional Activity, Accredited Program, Community Service.
     * Different functionality will apply to each type of program within the system.
     * <p>ReferenceCode of ReferenceSet -- PROGRAMCATEGORY: ACCRED, COMM, INSTITUT</p>
     * <p>Max length 64; Required</p>
     *
     * @return String
     */
    public String getProgramCategory() {
        return programCategory;
    }

    /**
     * @param programCategory 'Indicates the caseWorkStep assigned to the program'
     *                        Attend AA/NA, CTU Referral, Job Placement, Program Attendance
     */
    public void setProgramCategory(String programCategory) {
        this.programCategory = programCategory;
    }

    /**
     * @return programCaseWorkStep
     * 'Indicates the caseWorkStep assigned to the program'
     * Attend AA/NA, CTU Referral, Job Placement, Program Attendance
     */
    public String getProgramCaseWorkStep() {
        return programCaseWorkStep;
    }

    public void setProgramCaseWorkStep(String programCaseWorkStep) {
        this.programCaseWorkStep = programCaseWorkStep;
    }

    /**
     * Date on which the program is no longer offered
     * <p>Optional</p>
     *
     * @return Date
     */
    public Date getExpiryDate() {
        return expiryDate;
    }

    /**
     * Date on which the program is no longer offered
     * <p>Optional</p>
     *
     * @param expiryDate Date
     */
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    /**
     * Derived based on the Expiry Date. It is active
     * if the current date is < Expiry Date. Otherwise it is inactive.
     * <p>Optional</p>
     *
     * @return String
     */
    public String getStatus() {
        return status;
    }

    /**
     * Derived based on the Expiry Date. It is active
     * if the current date is < Expiry Date. Otherwise it is inactive.
     * <p>Optional</p>
     *
     * @param status String
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Number of days that the program runs for
     * <p>Required</p>
     *
     * @return Long
     */
    public Long getProgramDuration() {
        return programDuration;
    }

    /**
     * Number of days that the program runs for
     * <p>Required</p>
     *
     * @param programDuration Long
     */
    public void setProgramDuration(Long programDuration) {
        this.programDuration = programDuration;
    }

    /**
     * Indicates whether an inmate can start the program part way
     * ie. after the Start Date of the offering has passed (yes) or not (no).
     * <p>Required</p>
     *
     * @return Boolean
     */
    public Boolean getFlexibleStart() {
        return flexibleStart;
    }

    /**
     * Indicates whether an inmate can start the program part way
     * ie. after the Start Date of the offering has passed (yes) or not (no).
     * <p>Required</p>
     *
     * @param flexibleStart Boolean
     */
    public void setFlexibleStart(Boolean flexibleStart) {
        this.flexibleStart = flexibleStart;
    }

    /**
     * If the program is terminated, indicate the reason.
     * <p>Required If Expiry Date is provided.
     * <p>Metadata -- PROGRAMCATEGORY: ACCRED, COMM, INSTITUT</p>
     *
     * @return String
     */
    public String getTerminationReason() {
        return terminationReason;
    }

    /**
     * If the program is terminated, indicate the reason.
     * <p>Required If Expiry Date is provided.
     * <p>Metadata -- PROGRAMCATEGORY: ACCRED, COMM, INSTITUT</p>
     *
     * @param terminationReason String
     */
    public void setTerminationReason(String terminationReason) {
        this.terminationReason = terminationReason;
    }

    /**
     * Comment
     * <p>Optional</p>
     *
     * @return CommentType
     */
    public CommentType getComments() {
        return comments;
    }

    /**
     * Comment
     * <p>Optional</p>
     *
     * @param comments CommentType
     */
    public void setComments(CommentType comments) {
        this.comments = comments;
    }

    /**
     * Target Inmates
     * <p>Optional</p>
     *
     * @return A set of target inmates
     */
    public Set<TargetInmateType> getTargetInmates() {
        return targetInmates;
    }

    /**
     * Target Inmates
     * <p>Optional</p>
     *
     * @param targetInmates A set of target inmates
     */
    public void setTargetInmates(Set<TargetInmateType> targetInmates) {
        this.targetInmates = targetInmates;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((expiryDate == null) ? 0 : expiryDate.hashCode());
        result = prime * result + ((flexibleStart == null) ? 0 : flexibleStart.hashCode());
        result = prime * result + ((programCategory == null) ? 0 : programCategory.hashCode());
        result = prime * result + ((programCode == null) ? 0 : programCode.hashCode());
        result = prime * result + ((programDescription == null) ? 0 : programDescription.hashCode());
        result = prime * result + ((programDuration == null) ? 0 : programDuration.hashCode());
        result = prime * result + ((programId == null) ? 0 : programId.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((terminationReason == null) ? 0 : terminationReason.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ProgramType other = (ProgramType) obj;
        if (expiryDate == null) {
            if (other.expiryDate != null) {
				return false;
			}
        } else if (!expiryDate.equals(other.expiryDate)) {
			return false;
		}
        if (flexibleStart == null) {
            if (other.flexibleStart != null) {
				return false;
			}
        } else if (!flexibleStart.equals(other.flexibleStart)) {
			return false;
		}
        if (programCategory == null) {
            if (other.programCategory != null) {
				return false;
			}
        } else if (!programCategory.equals(other.programCategory)) {
			return false;
		}
        if (programCode == null) {
            if (other.programCode != null) {
				return false;
			}
        } else if (!programCode.equals(other.programCode)) {
			return false;
		}
        if (programDescription == null) {
            if (other.programDescription != null) {
				return false;
			}
        } else if (!programDescription.equals(other.programDescription)) {
			return false;
		}
        if (programDuration == null) {
            if (other.programDuration != null) {
				return false;
			}
        } else if (!programDuration.equals(other.programDuration)) {
			return false;
		}
        if (programId == null) {
            if (other.programId != null) {
				return false;
			}
        } else if (!programId.equals(other.programId)) {
			return false;
		}
        if (status == null) {
            if (other.status != null) {
				return false;
			}
        } else if (!status.equals(other.status)) {
			return false;
		}
        if (terminationReason == null) {
            if (other.terminationReason != null) {
				return false;
			}
        } else if (!terminationReason.equals(other.terminationReason)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "ProgramType [programId=" + programId + ", programCode=" + programCode + ", programDescription=" + programDescription + ", programCategory="
                + programCategory + ", expiryDate=" + expiryDate + ", status=" + status + ", programDuration=" + programDuration + ", flexibleStart=" + flexibleStart
                + ", terminationReason=" + terminationReason + ", comments=" + comments + ", targetInmates=" + targetInmates + "]";
    }

}
