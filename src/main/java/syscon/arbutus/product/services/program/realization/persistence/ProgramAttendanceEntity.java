package syscon.arbutus.product.services.program.realization.persistence;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Program Attendance Entity for Program and Service
 *
 * @author YShang
 * @DbComment PRG_ATTENDANCE 'Program Attendance Entity for Program and Service'
 * @since April 6, 2014
 */
@Audited
@Entity
@Table(name = "PRG_ATTENDANCE")
@SQLDelete(sql = "UPDATE PRG_ATTENDANCE SET flag = 4 WHERE attendanceId = ? and version = ?")
@Where(clause = "flag = 1")
public class ProgramAttendanceEntity implements Serializable {

    private static final long serialVersionUID = -5172294365154150149L;

    /**
     * @DbComment PRG_ATTENDANCE.attendanceId 'Program Attendance Id'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PRG_ATTENDANCE_ID")
    @SequenceGenerator(name = "SEQ_PRG_ATTENDANCE_ID", sequenceName = "SEQ_PRG_ATTENDANCE_ID", allocationSize = 1)
    @Column(name = "attendanceId")
    private Long attendanceId;

    /**
     * @DbComment PRG_ATTENDANCE.attendanceDate 'The date the program session occurred. This is based on the details of the scheduled sessions in the program offering. For single session programs this is to be entered by the user.'
     */
    @NotNull
    @Column(name = "attendanceDate", nullable = false)
    private Date attendanceDate;

    /**
     * @DbComment PRG_ATTENDANCE.attendanceStartTime 'The time the session started. This is based on the details of the scheduled sessions in the program offering. Only applies to programs that have multiple sessions.'
     */
    @NotNull
    @Column(name = "attendanceStartTime", nullable = false)
    private Date attendanceStartTime;

    /**
     * @DbComment PRG_ATTENDANCE.attendanceEndTime 'The time the session ended. This is based on the details of the scheduled sessions in the program offering. Only applies to programs that have multiple sessions.'
     */
    @NotNull
    @Column(name = "attendanceEndTime", nullable = false)
    private Date attendanceEndTime;

    /**
     * @DbComment PRG_ATTENDANCE.activityId 'Activity Id. Id of ACT_Activity Table'
     */
    @NotNull
    @Column(name = "activityId", nullable = false)
    private Long activityId;

    /**
     * @DbComment PRG_ATTENDANCE.attended 'Confirmation of attendance ie. yes or no. For single session programs, positive attendance will indicate a successful program completion. Conversely, negative attendance will indicate an unsuccessful program completion (ie. abandonment).'
     */
    @NotNull
    @Column(name = "attended", nullable = false)
    private String attended;

    /**
     * @DbComment PRG_ATTENDANCE.performance 'Indicates the inmate’s performance in the program session, eg. late, drunk, stoned, fell asleep. Only applies to programs that have multiple sessions. Note that multiple values may apply. Metadata: PROGRAMPERFORMANCE: ASLEEP, EXPECTED, INTOX, LATE, NOPART'
     */
    @Column(name = "performance", nullable = true)
    private String performance;

    /**
     * @DbComment PRG_ATTENDANCE.flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * @DbComment PRG_ATTENDANCE.version 'Version for Hibernate use'
     */
    @NotAudited
    @Version
    @Column(name = "version")
    private Long version;

    /**
     * Additional optional comments about the attendance
     *
     * @DbComment PRG_ATTENDANCE.CommentUserId 'User ID who created/updated the comments'
     * @DbComment PRG_ATTENDANCE.CommentDate 'The Date/Time when the comment was created'
     * @DbComment PRG_ATTENDANCE.CommentText 'The comment text'
     */
    @Valid
    @Embedded
    private CommentEntity comments;

    /**
     * @DbComment PRG_ATTENDANCE.assignmentId 'The inmate’s assignment to the program offering for which the attendance is being recorded'
     */
    @ForeignKey(name = "FK_PRG_ATTENDANCE02")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "assignmentId", nullable = false)
    private ProgramAssignmentEntity programAssignment;

    /**
     * @DbComment PRG_ATTENDANCE.createUserId 'Create User Id'
     * @DbComment PRG_ATTENDANCE.createDateTime 'Create Date Time'
     * @DbComment PRG_ATTENDANCE.modifyUserId 'Modify User Id'
     * @DbComment PRG_ATTENDANCE.modifyDateTime 'Modify Date Time'
     * @DbComment PRG_ATTENDANCE.invocationContext 'Invocation Context'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public ProgramAttendanceEntity() {
        super();
    }

    public Long getAttendanceId() {
        return attendanceId;
    }

    public void setAttendanceId(Long attendanceId) {
        this.attendanceId = attendanceId;
    }

    public Date getAttendanceDate() {
        return attendanceDate;
    }

    public void setAttendanceDate(Date attendanceDate) {
        this.attendanceDate = attendanceDate;
    }

    public Date getAttendanceStartTime() {
        return attendanceStartTime;
    }

    public void setAttendanceStartTime(Date attendanceStartTime) {
        this.attendanceStartTime = attendanceStartTime;
    }

    public Date getAttendanceEndTime() {
        return attendanceEndTime;
    }

    public void setAttendanceEndTime(Date attendanceEndTime) {
        this.attendanceEndTime = attendanceEndTime;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getAttended() {
        return attended;
    }

    public void setAttended(String attended) {
        this.attended = attended;
    }

    public String getPerformance() {
        return performance;
    }

    public void setPerformance(String performance) {
        this.performance = performance;
    }

    public CommentEntity getComments() {
        return comments;
    }

    public void setComments(CommentEntity comments) {
        this.comments = comments;
    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    public ProgramAssignmentEntity getProgramAssignment() {
        return programAssignment;
    }

    public void setProgramAssignment(ProgramAssignmentEntity programAssignment) {
        this.programAssignment = programAssignment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (!(o instanceof ProgramAttendanceEntity)) {
			return false;
		}

        ProgramAttendanceEntity that = (ProgramAttendanceEntity) o;

        if (activityId != null ? !activityId.equals(that.activityId) : that.activityId != null) {
			return false;
		}
        if (attendanceDate != null ? !attendanceDate.equals(that.attendanceDate) : that.attendanceDate != null) {
			return false;
		}
        if (attendanceEndTime != null ? !attendanceEndTime.equals(that.attendanceEndTime) : that.attendanceEndTime != null) {
			return false;
		}
        if (attendanceId != null ? !attendanceId.equals(that.attendanceId) : that.attendanceId != null) {
			return false;
		}
        if (attendanceStartTime != null ? !attendanceStartTime.equals(that.attendanceStartTime) : that.attendanceStartTime != null) {
			return false;
		}
        if (attended != null ? !attended.equals(that.attended) : that.attended != null) {
			return false;
		}
        if (performance != null ? !performance.equals(that.performance) : that.performance != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = attendanceId != null ? attendanceId.hashCode() : 0;
        result = 31 * result + (attendanceDate != null ? attendanceDate.hashCode() : 0);
        result = 31 * result + (attendanceStartTime != null ? attendanceStartTime.hashCode() : 0);
        result = 31 * result + (attendanceEndTime != null ? attendanceEndTime.hashCode() : 0);
        result = 31 * result + (activityId != null ? activityId.hashCode() : 0);
        result = 31 * result + (attended != null ? attended.hashCode() : 0);
        result = 31 * result + (performance != null ? performance.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProgramAttendanceEntity{" +
                "attendanceId=" + attendanceId +
                ", attendanceDate=" + attendanceDate +
                ", attendanceStartTime=" + attendanceStartTime +
                ", attendanceEndTime=" + attendanceEndTime +
                ", activityId=" + activityId +
                ", attended=" + attended +
                ", performance=" + performance +
                ", comments=" + comments +
                '}';
    }
}
