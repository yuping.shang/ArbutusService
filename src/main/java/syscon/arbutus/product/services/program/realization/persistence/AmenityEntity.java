package syscon.arbutus.product.services.program.realization.persistence;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Amenity Entity for Program and Service
 *
 * @author YShang
 * @DbComment PRG_AMENITY 'Amenity Entity for Program and Service'
 * @since April 6, 2014
 */
@Audited
@Entity
@Table(name = "PRG_AMENITY")
public class AmenityEntity implements Serializable {

    private static final long serialVersionUID = 8933889582168374624L;

    /**
     * @DbComment PRG_AMENITY.amenityId 'Amenity Id'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PRG_AMENITY_ID")
    @SequenceGenerator(name = "SEQ_PRG_AMENITY_ID", sequenceName = "SEQ_PRG_AMENITY_ID", allocationSize = 1)
    @Column(name = "amenityId")
    private Long amenityId;

    /**
     * @DbComment PRG_AMENITY.amenity 'Amenity, Wheelchair Accessible,... Metadata: LOCATIONPROPERTY: HSENS, SHOWER, WHEELCHAIR'
     */
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.AMENITIES)
    @Column(name = "amenity", nullable = true, length = 64)
    private String amenity;

    /**
     * @DbComment PRG_AMENITY.targetInmateId 'targetInmateId, foreign key of FK_PRG_AMENITY table'
     */
    @ForeignKey(name = "FK_PRG_AMENITY")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "targetInmateId", nullable = false)
    private TargetInmateEntity inmate;

    /**
     * @DbComment PRG_AMENITY.createUserId 'Create User Id'
     * @DbComment PRG_AMENITY.createDateTime 'Create Date Time'
     * @DbComment PRG_AMENITY.modifyUserId 'Modify User Id'
     * @DbComment PRG_AMENITY.modifyDateTime 'Modify Date Time'
     * @DbComment PRG_AMENITY.invocationContext 'Invocation Context'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public AmenityEntity() {
        super();
    }

    public Long getAmenityId() {
        return amenityId;
    }

    public void setAmenityId(Long amenityId) {
        this.amenityId = amenityId;
    }

    public String getAmenity() {
        return amenity;
    }

    public void setAmenity(String amenity) {
        this.amenity = amenity;
    }

    public TargetInmateEntity getInmate() {
        return inmate;
    }

    public void setInmate(TargetInmateEntity inmate) {
        this.inmate = inmate;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((amenity == null) ? 0 : amenity.hashCode());
        result = prime * result + ((amenityId == null) ? 0 : amenityId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        AmenityEntity other = (AmenityEntity) obj;
        if (amenity == null) {
            if (other.amenity != null) {
				return false;
			}
        } else if (!amenity.equals(other.amenity)) {
			return false;
		}
        if (amenityId == null) {
            if (other.amenityId != null) {
				return false;
			}
        } else if (!amenityId.equals(other.amenityId)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "AmenityEntity [amenityId=" + amenityId + ", amenity=" + amenity + "]";
    }
}
