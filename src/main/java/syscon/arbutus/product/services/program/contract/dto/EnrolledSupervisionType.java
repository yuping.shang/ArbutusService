package syscon.arbutus.product.services.program.contract.dto;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * EnrolledSupervisionType for Program and Services
 *
 * @author YShang
 * @since July 24, 2014
 */
public class EnrolledSupervisionType implements Serializable {

    private static final long serialVersionUID = 9214184502633155233L;

    /**
     * Supervision Id, the supervision/inmate enrolled
     */
    private Long supervisionId;

    /**
     * Offender Number/Inmate Id
     */
    @Size(max = 64, message = "Max length 64")
    private String offenderNumber;

    /**
     * Person Identity Id
     */
    private Long personIdentityId;

    /**
     * First name of the supervision/inmate
     */
    @Size(max = 64, message = "Max length 64")
    private String firstName;

    /**
     * Last name of the supervision/inmate
     */
    @Size(max = 64, message = "Max length 64")
    private String lastName;

    /**
     * Person Id
     */
    private Long personId;

    /**
     * Birth data of the supervision/inmate
     */
    private Date dateOfBirth;

    /**
     * sex 'Gender' of the supervision/inmate
     */
    @Size(max = 64, message = "Max length 64")
    private String sex;

    public EnrolledSupervisionType() {
        super();
    }

    /**
     * Supervision Id, the supervision/inmate enrolled
     *
     * @return Long
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * Supervision Id, the supervision/inmate enrolled
     *
     * @param supervisionId Long
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Offender Number/Inmate Id
     * <p>Max length 64</p>
     *
     * @return String
     */
    public String getOffenderNumber() {
        return offenderNumber;
    }

    /**
     * Offender Number/Inmate Id
     * <p>Max length 64</p>
     *
     * @param offenderNumber String
     */
    public void setOffenderNumber(String offenderNumber) {
        this.offenderNumber = offenderNumber;
    }

    /**
     * Person Identity Id
     *
     * @return Long
     */
    public Long getPersonIdentityId() {
        return personIdentityId;
    }

    /**
     * Person Identity Id
     *
     * @param personIdentityId Long
     */
    public void setPersonIdentityId(Long personIdentityId) {
        this.personIdentityId = personIdentityId;
    }

    /**
     * First name of the supervision/inmate
     * <p>Max length 64</p>
     *
     * @return String
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * First name of the supervision/inmate
     * <p>Max length 64</p>
     *
     * @param firstName String
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Last name of the supervision/inmate
     * <p>Max length 64</p>
     *
     * @return String
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Last name of the supervision/inmate
     * <p>Max length 64</p>
     *
     * @param lastName String
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Person Id
     *
     * @return Long
     */
    public Long getPersonId() {
        return personId;
    }

    /**
     * Person Id
     *
     * @param personId Long
     */
    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((personId == null) ? 0 : personId.hashCode());
        result = prime * result + ((personIdentityId == null) ? 0 : personIdentityId.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        EnrolledSupervisionType other = (EnrolledSupervisionType) obj;
        if (personId == null) {
            if (other.personId != null) {
				return false;
			}
        } else if (!personId.equals(other.personId)) {
			return false;
		}
        if (personIdentityId == null) {
            if (other.personIdentityId != null) {
				return false;
			}
        } else if (!personIdentityId.equals(other.personIdentityId)) {
			return false;
		}
        if (supervisionId == null) {
            if (other.supervisionId != null) {
				return false;
			}
        } else if (!supervisionId.equals(other.supervisionId)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "EnrolledSupervisionType [supervisionId=" + supervisionId + ", offenderNumber=" + offenderNumber + ", personIdentityId=" + personIdentityId
                + ", firstName=" + firstName + ", lastName=" + lastName + ", personId=" + personId + "]";
    }
}
