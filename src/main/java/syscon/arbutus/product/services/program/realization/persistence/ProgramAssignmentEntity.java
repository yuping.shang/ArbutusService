package syscon.arbutus.product.services.program.realization.persistence;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Program Assignment Entity for Program and Service
 *
 * @author YShang
 * @DbComment PRG_ASSIGNMENT 'Program Assignment Entity for Program and Service'
 * @since April 6, 2014
 */
@Audited
@Entity
@Table(name = "PRG_ASSIGNMENT")
@SQLDelete(sql = "UPDATE PRG_ASSIGNMENT SET flag = 4 WHERE assignmentId = ? and version = ?")
@Where(clause = "flag = 1")
public class ProgramAssignmentEntity implements Serializable {

    private static final long serialVersionUID = 2052714388245069692L;

    /**
     * @DbComment PRG_ASSIGNMENT.assignmentId 'Assignment Id'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PRG_ASSIGNMENT_ID")
    @SequenceGenerator(name = "SEQ_PRG_ASSIGNMENT_ID", sequenceName = "SEQ_PRG_ASSIGNMENT_ID", allocationSize = 1)
    @Column(name = "assignmentId")
    private Long assignmentId;

    /**
     * @DbComment PRG_ASSIGNMENT.referralDate 'Date on which the referral to a program offering was made. Only applies if a referral was made and the inmate was not directly placed in the program.'
     */
    private Date referralDate;

    /**
     * @DbComment PRG_ASSIGNMENT.referralPriority 'A general priority for the referral. Only applies if a referral was made and the inmate was not directly placed in the program. Metadata: REFERRALPRIORITY: LOW, MEDIUM, HIGH'
     */
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.REFERRAL_PRIORITY)
    @Column(name = "referralPriority", nullable = true, length = 64)
    private String referralPriority;

    /**
     * @DbComment PRG_ASSIGNMENT.referralRejectionReason 'If the program provider rejects the referral, indicate the reason. Only applies if a referral was made and the inmate was not directly placed in the program. Metadata: REFERRALREJECTIONREASON: NOAVAIL, NOTSUIT'
     */
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.REFERRAL_REJECTION_REASON)
    @Column(name = "referralRejectionReason", nullable = true, length = 64)
    private String referralRejectionReason;

    /**
     * @DbComment PRG_ASSIGNMENT.startDate 'Date on which the inmate starts the program. Only applies when the inmate is placed in a program offering.'
     */
    private Date startDate;

    /**
     * @DbComment PRG_ASSIGNMENT.endDate 'Date on which the inmate stops the program. Only applies when the inmate is placed in a program offering.'
     */
    private Date endDate;

    @OneToMany(mappedBy = "programAssignment", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @BatchSize(size = 20)
    private Set<AssigAddDateEntity> addedDates;

    @OneToMany(mappedBy = "programAssignment", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @BatchSize(size = 20)
    private Set<AssigSubtractDateEntity> removedDates;

    /**
     * @DbComment PRG_ASSIGNMENT.status 'Indicates the current status of the assignment eg. Referred, Allocated, Completed. Metadata: PROGRAMASSIGNMENTSTATUS: ABANDONED, ALLOCATED, APPROVED, CANCELLED, COMPLETED, REFERRED, REJECTED, SUSPENDED'
     */
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.PROGRAM_ASSIGNMENT_STATUS)
    @Column(name = "status", nullable = true, length = 64)
    private String status;

    /**
     * @DbComment PRG_ASSIGNMENT.suspendReason 'If the inmate is suspended from the program, indicate the reason. Metadata: PROGRAMSUSPENDREASON: DISPACT, HEALTH, RECLASS'
     */
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.PROGRAM_SUSPEND_REASON)
    @Column(name = "suspendReason", nullable = true, length = 64)
    private String suspendReason;

    /**
     * @DbComment PRG_ASSIGNMENT.abandonReason 'If the inmate abandons the program, indicate the reason. Metadata: PROGRAMABANDONMENTREASON: NOTCOMP, RELEASE, TRANSFER'
     */
    @Size(max = 64, message = "max length 64")
    @MetaCode(set = MetaSet.PROGRAM_ABANDONMENT_REASON)
    @Column(name = "abandonReason", nullable = true, length = 64)
    private String abandonReason;

    /**
     * Additional optional comments about the program assignment
     *
     * @DbComment PRG_ASSIGNMENT.CommentUserId 'User ID who created/updated the comments'
     * @DbComment PRG_ASSIGNMENT.CommentDate 'The Date/Time when the comment was created'
     * @DbComment PRG_ASSIGNMENT.CommentText 'The comment text'
     */
    @Valid
    @Embedded
    private CommentEntity comments;

    /**
     * @DbComment PRG_ASSIGNMENT.supervisionId 'Supervision Id. The inmate/supervision who is being referred to or placed in the program'
     */
    @Column(name = "supervisionId")
    private Long supervisionId;

    /**
     * @DbComment PRG_ASSIGNMENT.programId 'Program Id. The offering to which the inmate is being referred or placed'
     */
    @ForeignKey(name = "FK_PRG_ASSIGNMENT01")
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "programId", nullable = true)
    private ProgramEntity program;

    /**
     * @DbComment PRG_ASSIGNMENT.offeringId 'Program Offering Id. The offering to which the inmate is being referred or placed'
     */
    @ForeignKey(name = "FK_PRG_ASSIGNMENT02")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "offeringId", nullable = true)
    private ProgramOfferingEntity programOffering;

    /**
     * Allows the user to record the inmate’s attendance in the program for each session
     */
    @OneToMany(mappedBy = "programAssignment", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
    @BatchSize(size = 20)
    private Set<ProgramAttendanceEntity> programAttendances;

    @OneToMany(mappedBy = "programAssignment", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @BatchSize(size = 20)
    private Set<SuspensionEntity> suspensions;

    /**
     * @DbComment PRG_ASSIGNMENT.processId 'Process Id to be applied by JBPM'
     */
    @Column(name = "processId", nullable = true)
    private Long processId;

    /**
     * @DbComment PRG_ASSIGNMENT.flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * @DbComment PRG_ASSIGNMENT.version 'Version for Hibernate use'
     */
    @NotAudited
    @Version
    @Column(name = "version")
    private Long version;

    /**
     * @DbComment PRG_ASSIGNMENT.createUserId 'Create User Id'
     * @DbComment PRG_ASSIGNMENT.createDateTime 'Create Date Time'
     * @DbComment PRG_ASSIGNMENT.modifyUserId 'Modify User Id'
     * @DbComment PRG_ASSIGNMENT.modifyDateTime 'Modify Date Time'
     * @DbComment PRG_ASSIGNMENT.invocationContext 'Invocation Context'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public ProgramAssignmentEntity() {
        super();
    }

    public Long getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }

    public Date getReferralDate() {
        return referralDate;
    }

    public void setReferralDate(Date referralDate) {
        this.referralDate = referralDate;
    }

    public String getReferralPriority() {
        return referralPriority;
    }

    public void setReferralPriority(String referralPriority) {
        this.referralPriority = referralPriority;
    }

    public String getReferralRejectionReason() {
        return referralRejectionReason;
    }

    public void setReferralRejectionReason(String referralRejectionReason) {
        this.referralRejectionReason = referralRejectionReason;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Set<AssigAddDateEntity> getAddedDates() {
        if (addedDates == null) {
			addedDates = new HashSet<>();
		}
        return addedDates;
    }

    public void setAddedDates(Set<AssigAddDateEntity> addedDates) {
        this.addedDates = addedDates;
    }

    public void addAddedDate(AssigAddDateEntity addDateEntity) {
        addDateEntity.setProgramAssignment(this);
        getAddedDates().add(addDateEntity);
    }

    public Set<AssigSubtractDateEntity> getRemovedDates() {
        if (removedDates == null) {
			removedDates = new HashSet<>();
		}
        return removedDates;
    }

    public void setRemovedDates(Set<AssigSubtractDateEntity> removedDates) {
        this.removedDates = removedDates;
    }

    public void addRemovedDate(AssigSubtractDateEntity removedDate) {
        removedDate.setProgramAssignment(this);
        getRemovedDates().add(removedDate);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSuspendReason() {
        return suspendReason;
    }

    public void setSuspendReason(String suspendReason) {
        this.suspendReason = suspendReason;
    }

    public Set<SuspensionEntity> getSuspensions() {
        if (suspensions == null) {
            suspensions = new HashSet<>();
        }
        return suspensions;
    }

    public void setSuspensions(Set<SuspensionEntity> suspensions) {
        this.suspensions = suspensions;
    }

    public void addSuspension(SuspensionEntity suspension) {
        if (suspension != null) {
            suspension.setProgramAssignment(this);
            this.getSuspensions().add(suspension);
        }
    }

    public String getAbandonReason() {
        return abandonReason;
    }

    public void setAbandonReason(String abandonReason) {
        this.abandonReason = abandonReason;
    }

    public CommentEntity getComments() {
        return comments;
    }

    public void setComments(CommentEntity comments) {
        this.comments = comments;
    }

    public Long getSupervisionId() {
        return supervisionId;
    }

    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    public ProgramEntity getProgram() {
        return program;
    }

    public void setProgram(ProgramEntity program) {
        this.program = program;
    }

    public ProgramOfferingEntity getProgramOffering() {
        return programOffering;
    }

    public void setProgramOffering(ProgramOfferingEntity programOffering) {
        this.programOffering = programOffering;
    }

    public Set<ProgramAttendanceEntity> getProgramAttendances() {
        if (programAttendances == null) {
            programAttendances = new HashSet<>();
        }
        return programAttendances;
    }

    public void setProgramAttendances(Set<ProgramAttendanceEntity> programAttendances) {
        this.programAttendances = programAttendances;
    }

    public void addProgramAttendance(ProgramAttendanceEntity programAttendance) {
        if (programAttendance != null) {
            programAttendance.setProgramAssignment(this);
            getProgramAttendances().add(programAttendance);
        }
    }

    public Long getProcessId() {
        return processId;
    }

    public void setProcessId(Long processId) {
        this.processId = processId;
    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((abandonReason == null) ? 0 : abandonReason.hashCode());
        result = prime * result + ((assignmentId == null) ? 0 : assignmentId.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((referralDate == null) ? 0 : referralDate.hashCode());
        result = prime * result + ((referralPriority == null) ? 0 : referralPriority.hashCode());
        result = prime * result + ((referralRejectionReason == null) ? 0 : referralRejectionReason.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        result = prime * result + ((suspendReason == null) ? 0 : suspendReason.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ProgramAssignmentEntity other = (ProgramAssignmentEntity) obj;
        if (abandonReason == null) {
            if (other.abandonReason != null) {
				return false;
			}
        } else if (!abandonReason.equals(other.abandonReason)) {
			return false;
		}
        if (assignmentId == null) {
            if (other.assignmentId != null) {
				return false;
			}
        } else if (!assignmentId.equals(other.assignmentId)) {
			return false;
		}
        if (endDate == null) {
            if (other.endDate != null) {
				return false;
			}
        } else if (!endDate.equals(other.endDate)) {
			return false;
		}
        if (referralDate == null) {
            if (other.referralDate != null) {
				return false;
			}
        } else if (!referralDate.equals(other.referralDate)) {
			return false;
		}
        if (referralPriority == null) {
            if (other.referralPriority != null) {
				return false;
			}
        } else if (!referralPriority.equals(other.referralPriority)) {
			return false;
		}
        if (referralRejectionReason == null) {
            if (other.referralRejectionReason != null) {
				return false;
			}
        } else if (!referralRejectionReason.equals(other.referralRejectionReason)) {
			return false;
		}
        if (startDate == null) {
            if (other.startDate != null) {
				return false;
			}
        } else if (!startDate.equals(other.startDate)) {
			return false;
		}
        if (status == null) {
            if (other.status != null) {
				return false;
			}
        } else if (!status.equals(other.status)) {
			return false;
		}
        if (supervisionId == null) {
            if (other.supervisionId != null) {
				return false;
			}
        } else if (!supervisionId.equals(other.supervisionId)) {
			return false;
		}
        if (suspendReason == null) {
            if (other.suspendReason != null) {
				return false;
			}
        } else if (!suspendReason.equals(other.suspendReason)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "ProgramAssignmentEntity [assignmentId=" + assignmentId + ", referralDate=" + referralDate + ", referralPriority=" + referralPriority
                + ", referralRejectionReason=" + referralRejectionReason + ", startDate=" + startDate + ", endDate=" + endDate + ", addedDates=" + addedDates
                + ", removedDates=" + removedDates + ", status=" + status + ", suspendReason=" + suspendReason + ", abandonReason=" + abandonReason + ", comments="
                + comments + ", supervisionId=" + supervisionId + ", suspensions=" + suspensions + ", processId=" + processId + "]";
    }

}
