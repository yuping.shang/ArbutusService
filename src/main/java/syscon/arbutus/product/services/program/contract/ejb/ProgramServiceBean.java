package syscon.arbutus.product.services.program.contract.ejb;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.PersistenceContext;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.*;
import org.hibernate.engine.spi.LoadQueryInfluencers;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.internal.CriteriaImpl;
import org.hibernate.internal.SessionImpl;
import org.hibernate.loader.OuterJoinLoader;
import org.hibernate.loader.criteria.CriteriaLoader;
import org.hibernate.persister.entity.OuterJoinLoadable;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.kie.api.cdi.KReleaseId;
import org.kie.api.runtime.KieSession;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.*;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldMetaType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueSearchType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.DTOBindingTypeEnum;
import syscon.arbutus.product.services.core.common.adapters.ActivityServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.housing.contract.ejb.BusinessRuleHelper;
import syscon.arbutus.product.services.housing.realization.dto.housingsuitability.BusinessRuleMapping;
import syscon.arbutus.product.services.housing.realization.dto.housingsuitability.BusinessRuleReaction;
import syscon.arbutus.product.services.individualassociationsearch.contract.interfaces.IndividualAssociationSearchService;
import syscon.arbutus.product.services.legal.contract.ejb.ClientCustomFieldHandler;
import syscon.arbutus.product.services.person.realization.persistence.personidentity.PersonIdentityEntity;
import syscon.arbutus.product.services.program.contract.dto.*;
import syscon.arbutus.product.services.program.contract.interfaces.ProgramService;
import syscon.arbutus.product.services.program.realization.persistence.*;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.ExceptionHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.supervision.realization.persistence.SupervisionEntity;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * The implementation of the interface of Program and Service
 *
 * @author YShang
 * @since April 6, 2014
 */
@Stateless(mappedName = "ProgramService", description = "The Program service")
@Remote(ProgramService.class)
public class ProgramServiceBean implements ProgramService {
    public static final Long SUCCESS = 1L;
    private static final String ACTIVE = "ACTIVE";
    private static final String INACTIVE = "INACTIVE";
    public static int SEARCH_MAX_LIMIT = 200;
    private static Logger log = LoggerFactory.getLogger(ProgramServiceBean.class);
    // Program Assignment Status
    private final String ABANDONED = "ABANDONED";
    private final String ALLOCATED = "ALLOCATED";
    private final String REFERRED = "REFERRED";
    private final String APPROVED = "APPROVED";
    private final String CANCELLED = "CANCELLED";
    private final String COMPLETED = "COMPLETED";
    private final String REJECTED = "REJECTED";
    private final String SUSPENDED = "SUSPENDED";
    private final String PROGRAM_CATEGORY_INSTITUT = "INSTITUT";
    private final String LOCATION_NON_ASSOCIATION_TOTAL = "TOTAL";


    @Resource
    private SessionContext context;

    @PersistenceContext(unitName = "arbutus-pu")
    private Session session;

    @EJB
    private IndividualAssociationSearchService individualAssociationSearchService;

    @EJB
    private FacilityService   facilityService;

    @Inject
    @KReleaseId(groupId = "syscon.arbutus.product", artifactId = "housing", version = "1.0.0")
    private KieSession housingKSession;


    private BusinessRuleMapping codeMapping = null;
    private BusinessRuleReaction reactionMapping = null;

    @Override
    public ProgramType createProgram(UserContext uc, ProgramType program) {
        ProgramEntity entity = createProgramEntity(uc, program);
        ProgramType ret = toProgramType(entity);
        return ret;
    }

    private ProgramEntity createProgramEntity(UserContext uc, ProgramType program) {
        ValidationHelper.validate(program);
        StampEntity stamp = getCreateStamp(uc);
        validateProgramType(program);
        ProgramEntity entity = this.toProgramEntity(program, stamp);
        entity.setProgramId(null);
        ValidationHelper.verifyMetaCodes(entity, true);
        session.persist(entity);
        return entity;
    }

    @Override
    public ProgramType getProgram(UserContext uc, Long programId) {
        ProgramType ret = this.toProgramType(get(programId, ProgramEntity.class));
        return ret;
    }

    @Override
    public ProgramType getProgramByCode(UserContext uc, String programCode) {
        if (programCode == null || programCode.trim().isEmpty()) {
            return null;
        }
        Criteria c = session.createCriteria(ProgramEntity.class);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        c.setMaxResults(1);
        c.add(Restrictions.eq("programCode", programCode));
        ProgramEntity entity = (ProgramEntity) c.uniqueResult();
        return toProgramType(entity);
    }

    @Override
    public ProgramType updateProgram(UserContext uc, ProgramType program) {
        ProgramEntity entity = updateProgramEntity(uc, program);
        ProgramType ret = this.toProgramType(entity);
        return ret;
    }

    private ProgramEntity updateProgramEntity(UserContext uc, ProgramType program) {
        if (program == null || program.getProgramId() == null) {
            throw new InvalidInputException("Invalid argument: null programId");
        }
        ValidationHelper.validate(program);
        Long id = program.getProgramId();
        ProgramEntity savedEntity = BeanHelper.getEntity(session, ProgramEntity.class, id);
        StampEntity modifyStamp = getModifyStamp(uc, savedEntity.getStamp());
        ProgramEntity entity = this.toProgramEntity(program, modifyStamp);
        entity.setVersion(savedEntity.getVersion());
        ValidationHelper.verifyMetaCodes(entity, false);
        Iterator<TargetInmateEntity> it = savedEntity.getTargetInmates().iterator();
        while (it.hasNext()) {
            session.delete(it.next());
        }
        session.merge(entity);
        return entity;
    }

    @Override
    public ProgramReturnType searchProgram(UserContext uc, Set<Long> programIds, ProgramSearchType search, Long startIndex, Long resultSize, String resultOrder) {

        Criteria criteria = session.createCriteria(ProgramEntity.class);
        if (!BeanHelper.isEmpty(programIds)) {
            Set<Set<Long>> IDs = BeanHelper.splitSet(programIds);
            if (IDs.isEmpty() || ((Set<?>) (IDs.toArray()[0])).isEmpty()) {
                ProgramReturnType ret = new ProgramReturnType();
                ret.setPrograms(new ArrayList<ProgramType>());
                ret.setTotalSize(0L);
                return ret;
            }

            Disjunction disjunction = Restrictions.disjunction();
            for (Set<Long> ids : IDs) {
                disjunction.add(Restrictions.in("programId", ids));
            }
            criteria.add(disjunction);
        }

        if (search != null) {
            Long supervisionId = search.getSupervisionId();
            if (supervisionId != null) {
                Criteria assignmentCriteria = session.createCriteria(ProgramAssignmentEntity.class);
                assignmentCriteria.createCriteria("program", "p");
                assignmentCriteria.setProjection(Projections.property("p.programId"));
                assignmentCriteria.add(Restrictions.eq("supervisionId", supervisionId));
                Set<String> programAssignmentStatus = search.getProgramAssignmentStatus();
                if (!BeanHelper.isEmpty(programAssignmentStatus)) {
                    assignmentCriteria.add(Restrictions.in("status", programAssignmentStatus));
                }

                @SuppressWarnings("unchecked") List<Long> programIDs = assignmentCriteria.list();
                Set<Set<Long>> IDs = BeanHelper.splitSet(programIDs);
                if (IDs.isEmpty() || ((Set<?>) (IDs.toArray()[0])).isEmpty()) {
                    ProgramReturnType ret = new ProgramReturnType();
                    ret.setPrograms(new ArrayList<ProgramType>());
                    ret.setTotalSize(0L);
                    return ret;
                }

                Disjunction disjunction = Restrictions.disjunction();
                for (Set<Long> ids : IDs) {
                    disjunction.add(Restrictions.in("programId", ids));
                }
                criteria.add(disjunction);
            }

            String programCategory = search.getProgramCategory();
            if (!BeanHelper.isEmpty(programCategory)) {
                criteria.add(Restrictions.eq("programCategory", programCategory));
            }

            String programWorkStep = search.getProgramCaseWorkStep();
            if (!BeanHelper.isEmpty(programWorkStep)) {
                criteria.add(Restrictions.eq("programCaseWorkStep", programWorkStep));
            }

            String programCodeDescription = search.getProgramDescription();
            if (!BeanHelper.isEmpty(programCodeDescription)) {
                criteria.add(Restrictions.or(Restrictions.ilike("programCode", BeanHelper.strWildcardParser(programCodeDescription)),
                        Restrictions.ilike("programDescription", BeanHelper.strWildcardParser(programCodeDescription))));
            }

            Boolean flexibleStart = search.getFlexibleStart();
            if (flexibleStart != null) {
                criteria.add(Restrictions.eq("flexibleStart", flexibleStart));
            }

            String terminationReason = search.getTerminationReason();
            if (!BeanHelper.isEmpty(terminationReason)) {
                criteria.add(Restrictions.eq("terminationReason", terminationReason));
            }

            String status = search.getStatus();
            if (!BeanHelper.isEmpty(status)) {
                if (status.equalsIgnoreCase("ACTIVE")) {
                    criteria.add(Restrictions.or(Restrictions.isNull("expiryDate"), Restrictions.gt("expiryDate", new Date())));
                } else if (status.equalsIgnoreCase("INACTIVE")) {
                    criteria.add(Restrictions.and(Restrictions.isNotNull("expiryDate"), Restrictions.le("expiryDate", new Date())));
                }
            }

            Set<String> genders = search.getGenders();
            Set<String> ageRanges = search.getAgeRanges();
            Set<String> amenities = search.getAmenities();
            if (!BeanHelper.isEmpty(genders) || !BeanHelper.isEmpty(ageRanges) || !BeanHelper.isEmpty(amenities)) {
                DetachedCriteria dc = DetachedCriteria.forClass(TargetInmateEntity.class);
                dc.setProjection(Property.forName("program.programId"));
                if (!BeanHelper.isEmpty(genders)) {
                    dc.createCriteria("genders", "g");
                    dc.add(Restrictions.in("g.gender", genders));
                }

                if (!BeanHelper.isEmpty(ageRanges)) {
                    dc.createCriteria("ageRanges", "a");
                    dc.add(Restrictions.in("a.ageRange", ageRanges));
                }

                if (!BeanHelper.isEmpty(amenities)) {
                    dc.createCriteria("amenities", "am");
                    dc.add(Restrictions.in("am.amenity", amenities));
                }
                criteria.add(Subqueries.propertyIn("programId", dc));
            }
        }

        Long totalSize = (Long) criteria.setProjection(Projections.countDistinct("programId")).uniqueResult();
        criteria.setProjection(null);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());
            criteria.setMaxResults(resultSize.intValue());
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                criteria.addOrder(ord);
            }
        }

        // Search and only return the predefined max number of records
        criteria.setLockMode(LockMode.NONE);
        criteria.setCacheMode(CacheMode.IGNORE);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        @SuppressWarnings("unchecked") List<ProgramEntity> list = criteria.list();
        if (resultSize == null && list.size() > SEARCH_MAX_LIMIT) {
            throw new ArbutusRuntimeException("Search result is over " + SEARCH_MAX_LIMIT);
        }

        ProgramReturnType ret = new ProgramReturnType();
        List<ProgramType> programs = toProgramTypeList(list);
        ret.setPrograms(programs);
        ret.setTotalSize(totalSize);
        return ret;
    }

    @Override
    public Boolean isDuplicateProgram(UserContext uc, String programCode, Long... programId) {
        String functionName = "isDuplicateProgram";
        if (BeanHelper.isEmpty(programCode)) {
            String message = "programCode is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        Criteria criteria = session.createCriteria(ProgramEntity.class);
        criteria.add(Restrictions.ilike("programCode", programCode));
        criteria.setProjection(Projections.countDistinct("programId"));
        Long id = null;
        if (programId != null && programId.length > 0) {
            id = programId[0];
            if (id != null) {
                criteria.add(Restrictions.eq("programId", id));
            }
        }
        criteria.setMaxResults(1);
        Long count = (Long) criteria.uniqueResult();
        if ((count == null || count.longValue() == 0L) && id == null) {
            return false;
        } else if ((count == null || count != null && count.longValue() == 0L) && id != null) {
            return isDuplicateProgram(uc, programCode);
        } else if ((count != null && count.longValue() == 1L) && id != null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public Map<Long, Map<String, String>> getProgramCodes(UserContext uc, String programCategory) {
        Criteria c = session.createCriteria(ProgramEntity.class);
        c.setCacheable(true);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        c.setProjection(Projections.projectionList().add(Projections.id()).add(Projections.property("programCode")).add(Projections.property("programDescription")));
        if (!BeanHelper.isEmpty(programCategory)) {
            c.add(Restrictions.eq("programCategory", programCategory));
        }
        Date now = new Date();
        c.add(Restrictions.or(Restrictions.isNull("expiryDate"), Restrictions.gt("expiryDate", now)));
        @SuppressWarnings("rawtypes") List rows = c.list();
        Map<Long, Map<String, String>> ret = new HashMap<>();
        for (Object row : rows) {
            Object[] cols = (Object[]) row;
            Long programId = (Long) cols[0];
            String code = (String) cols[1];
            String description = (String) cols[2];
            Map<String, String> m = new HashMap<String, String>();
            m.put(code, description);
            ret.put(programId, m);
        }
        return ret;
    }

    @Override
    public void deleteProgram(UserContext uc, Long programId) {
        ProgramEntity entity = get(programId, ProgramEntity.class);
        if (entity != null) {
            session.delete(entity);
        }
    }

    @Override
    public ProgramOfferingType createProgramOffering(UserContext uc, ProgramOfferingType programOffering) {
        ValidationHelper.validate(programOffering);

        if (Boolean.TRUE.equals(programOffering.getGenerateSchedule()) && Boolean.FALSE.equals(programOffering.getSingleOccurrence()) && BeanHelper.isEmpty(
                programOffering.getRecurranceDays())) {
            String functionName = "createProgramOffering";
            String message = "Invalid argument: schedule recurrance days are required when single occurrance is false and generate schedule is true.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        ProgramEntity programEntity = null;
        if (programOffering.getProgram() != null) {
            Long programId = programOffering.getProgram().getProgramId();
            if (programId != null) {
                programEntity = BeanHelper.getEntity(session, ProgramEntity.class, programId);
            }
        }
        StampEntity modifyStamp = getCreateStamp(uc);
        ProgramOfferingEntity entity = this.toProgramOfferingEntity(programOffering, modifyStamp);
        if (programEntity == null) {
            entity.setProgram(null);
        } else {
            entity.setProgram(programEntity);
        }
        ValidationHelper.verifyMetaCodes(entity, true);
        addProgramScheduleForOffering(uc, entity, programOffering.getRecurranceDays());

        facilityService.addAssociatedOrganization(uc, programOffering.getFacilityId(), programOffering.getOrganizationId());
        session.persist(entity);

        ProgramOfferingType ret = this.toProgramOfferingType(uc, entity, true);
        return ret;
    }

    @Override
    public ProgramOfferingType updateProgramOffering(UserContext uc, ProgramOfferingType programOffering) {
        if (programOffering == null || programOffering.getOfferingId() == null) {
            throw new InvalidInputException("Invalid argument: null offeringId");
        }
        Long id = programOffering.getOfferingId();
        ValidationHelper.validate(programOffering);
        ProgramEntity programEntity = null;
        if (programOffering.getProgram() != null) {
            Long programId = programOffering.getProgram().getProgramId();
            if (programId != null) {
                programEntity = BeanHelper.getEntity(session, ProgramEntity.class, programId);
            }
        }
        ProgramOfferingEntity savedEntity = BeanHelper.getEntity(session, ProgramOfferingEntity.class, id);
        StampEntity stamp = getModifyStamp(uc, savedEntity.getStamp());
        Long scheduleId = savedEntity.getScheduleId();
        /*
        List<AdditionDateType> additionDates = getAdditionDatesForOffering(uc, programOffering.getOfferingId(), programOffering.getStartDate() == null ? null : new LocalDate(programOffering.getStartDate()), programOffering.getEndDate() == null ? null : new LocalDate(programOffering.getEndDate()), null, null);
        if (!BeanHelper.isEmpty(additionDates)) {
            programOffering.getAddedDates().addAll(additionDates);
        }

        List<ScheduleSessionDateType> subtractionDates = getModifiedPatternDatesForOffering(uc, programOffering.getOfferingId(), programOffering.getStartDate() == null ? null : new LocalDate(programOffering.getStartDate()), programOffering.getEndDate() == null ? null : new LocalDate(programOffering.getEndDate()), null, null);
        if (!BeanHelper.isEmpty(subtractionDates)) {
            programOffering.getModifiedDates().addAll(subtractionDates);
        }

        if (!BeanHelper.isEmpty(programOffering.getAddedDates()) && !BeanHelper.isEmpty(programOffering.getModifiedDates())) {
            Iterator<AdditionDateType> ita = programOffering.getAddedDates().iterator();
            while (ita.hasNext()) {
                AdditionDateType additionDateType = ita.next();
                Iterator<ScheduleSessionDateType> its = programOffering.getModifiedDates().iterator();
                while (its.hasNext()) {
                    ScheduleSessionDateType subtractionDateType = its.next();
                    if (toExDateType(additionDateType).equals(toExDateType(subtractionDateType))) {
                        ita.remove();
                        its.remove();
                    }
                }
            }
        }
        */
        ProgramOfferingEntity entity = this.toProgramOfferingEntity(programOffering, stamp);
        ValidationHelper.verifyMetaCodes(entity, false);
        entity.setScheduleId(scheduleId);
        if (programEntity == null) {
            entity.setProgram(null);
        } else {
            entity.setProgram(programEntity);
        }
        entity.setVersion(savedEntity.getVersion());
        /*
        for (AdditionDateEntity additionDateEntity : savedEntity.getAddedDates()) {
            session.delete(additionDateEntity);
        }
        savedEntity.getAddedDates().clear();

        for (SubtractionDateEntity subtractionDateEntity : savedEntity.getModifiedDates()) {
            session.delete(subtractionDateEntity);
        }
        savedEntity.getModifiedDates().clear();
        */
      /*Iterator<TargetInmateEntity> it = savedEntity.getTargetInmates().iterator();
      while (it.hasNext()) {
          session.delete(it.next());
      }*/
        updateProgramScheduleForOffering(uc, entity, programOffering.getRecurranceDays());
        session.merge(entity);

        ProgramOfferingType ret = this.toProgramOfferingType(uc, entity, true);
        return ret;
    }

    @Override
    public ProgramOfferingType getProgramOffering(UserContext uc, Long programOfferingId) {
        return this.toProgramOfferingType(uc, get(programOfferingId, ProgramOfferingEntity.class), true);
    }

    @Override
    public List<ProgramOfferingType> getProgramOfferingsBySupervisionId(UserContext uc, Long supervisionId) {

        if (supervisionId == null) {
            String functionName = "getProgramOfferingsBySupervisionId";
            String message = "Invalid Argument: supervisionId is required";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Criteria criteria = session.createCriteria(ProgramOfferingEntity.class);
        criteria.addOrder(Order.desc("offeringId"));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        Criteria assignmentCriteria = session.createCriteria(ProgramAssignmentEntity.class);
        assignmentCriteria.createCriteria("programOffering", "offering");
        assignmentCriteria.setProjection(Projections.property("offering.offeringId"));
        assignmentCriteria.add(Restrictions.eq("supervisionId", supervisionId));
        @SuppressWarnings("unchecked") List<Long> offeringIds = assignmentCriteria.list();
        Set<Set<Long>> IDs = BeanHelper.splitSet(offeringIds);
        if (IDs.isEmpty() || ((Set<?>) (IDs.toArray()[0])).isEmpty()) {
            return new ArrayList<ProgramOfferingType>();
        }

        Disjunction disjunction = Restrictions.disjunction();
        for (Set<Long> ids : IDs) {
            disjunction.add(Restrictions.in("offeringId", ids));
        }
        criteria.add(disjunction);

        @SuppressWarnings({ "unchecked" }) List<ProgramOfferingEntity> entities = criteria.list();

        List<ProgramOfferingType> ret = toProgramOfferingTypeList(uc, entities);
        return ret;
    }

    public ProgramOfferingType getProgramOfferingByScheduleId(UserContext uc, Long scheduleId) {
        if (scheduleId == null) {
            String functionName = "getProgramOfferingByScheduleId";
            String message = "Invalid Argument: scheduleId is required";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        Criteria criteria = session.createCriteria(ProgramOfferingEntity.class);
        criteria.add(Restrictions.eq("scheduleId", scheduleId));
        criteria.setMaxResults(1);
        criteria.addOrder(Order.desc("offeringId"));
        ProgramOfferingEntity programOffering = (ProgramOfferingEntity) criteria.uniqueResult();
        return toProgramOfferingType(uc, programOffering, true);
    }

    @Override
    public Long getProgramOfferingVacancy(UserContext uc, Long programOfferingId) {
        if (programOfferingId == null) {
            String functionName = "getProgramOfferingVacancy";
            String message = "Invalid Argument: programOfferingId is required";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Criteria offeringCriteria = session.createCriteria(ProgramOfferingEntity.class);
        offeringCriteria.add(Restrictions.eq("offeringId", programOfferingId));
        offeringCriteria.setProjection(Projections.property("capacity"));
        offeringCriteria.setMaxResults(1);
        offeringCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        Long capacity = (Long) offeringCriteria.uniqueResult();
        if (capacity == null || capacity.longValue() == 0L) {
            capacity = 0L;
        }

        Criteria assignmentCriteria = session.createCriteria(ProgramAssignmentEntity.class);
        assignmentCriteria.createCriteria("programOffering", "offering");
        assignmentCriteria.add(Restrictions.eq("offering.offeringId", programOfferingId));
        assignmentCriteria.add(Restrictions.in("status", new HashSet<String>(Arrays.asList(new String[] { ALLOCATED }))));

        assignmentCriteria.setProjection(Projections.countDistinct("assignmentId"));
        assignmentCriteria.setMaxResults(1);
        assignmentCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        Long occupied = (Long) assignmentCriteria.uniqueResult();

        return occupied == null ? 0L : capacity - occupied;
    }

    @Override
    public Boolean isProgramReferredAlready(UserContext uc, Long supervisionId, Long programId) {
        if (supervisionId == null || programId == null) {
            String functionName = "getProgramOfferingVacancy";
            String message = "Invalid Argument: supervisionId and programId are required";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Criteria assignmentCriteria = session.createCriteria(ProgramAssignmentEntity.class);
        if (supervisionId != null) {
            assignmentCriteria.add(Restrictions.eq("supervisionId", supervisionId));
        }

        Disjunction disjunction = Restrictions.disjunction();
        disjunction.add(Restrictions.ilike("status", this.REFERRED));
        disjunction.add(Restrictions.ilike("status", this.ALLOCATED));
        disjunction.add(Restrictions.ilike("status", this.ABANDONED));
        disjunction.add(Restrictions.ilike("status", this.COMPLETED));
        disjunction.add(Restrictions.ilike("status", this.SUSPENDED));
        assignmentCriteria.add(disjunction);

        assignmentCriteria.add(Restrictions.eq("program.programId", programId));

        assignmentCriteria.setProjection(Projections.countDistinct("assignmentId"));
        assignmentCriteria.setMaxResults(1);
        assignmentCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        Long count = (Long) assignmentCriteria.uniqueResult();
        if (count == null || count == 0L) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public Boolean isReferredAlready(UserContext uc, Long supervisionId, Long programOfferingId) {
        if (supervisionId == null || programOfferingId == null) {
            String functionName = "getProgramOfferingVacancy";
            String message = "Invalid Argument: supervisionId and programOfferingId are required";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Criteria assignmentCriteria = session.createCriteria(ProgramAssignmentEntity.class);
        assignmentCriteria.add(Restrictions.eq("supervisionId", supervisionId));

        Disjunction disjunction = Restrictions.disjunction();
        disjunction.add(Restrictions.ilike("status", this.REFERRED));
        disjunction.add(Restrictions.ilike("status", this.ALLOCATED));
        //disjunction.add(Restrictions.ilike("status", this.ABANDONED));
        disjunction.add(Restrictions.ilike("status", this.COMPLETED));
        disjunction.add(Restrictions.ilike("status", this.SUSPENDED));
        assignmentCriteria.add(Restrictions.and(disjunction));

        assignmentCriteria.add(Restrictions.eq("programOffering.offeringId", programOfferingId));

        assignmentCriteria.setProjection(Projections.countDistinct("assignmentId"));
        assignmentCriteria.setMaxResults(1);
        assignmentCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        Long count = (Long) assignmentCriteria.uniqueResult();
        if (count == null || count.longValue() == 0L) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public Boolean isAssigedAlready(UserContext uc, Long supervisionId, Long programOfferingId) {
        if (programOfferingId == null) {
            String functionName = "getProgramOfferingVacancy";
            String message = "Invalid Argument: programOfferingId is required";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Criteria assignmentCriteria = session.createCriteria(ProgramAssignmentEntity.class);
        if (supervisionId != null) {
            assignmentCriteria.add(Restrictions.eq("supervisionId", supervisionId));
        }

        Disjunction disjunction = Restrictions.disjunction();
        disjunction.add(Restrictions.ilike("status", this.ALLOCATED));
        disjunction.add(Restrictions.ilike("status", this.ABANDONED));
        disjunction.add(Restrictions.ilike("status", this.COMPLETED));
        disjunction.add(Restrictions.ilike("status", this.SUSPENDED));

        assignmentCriteria.add(Restrictions.and(disjunction));

        assignmentCriteria.add(Restrictions.eq("programOffering.offeringId", programOfferingId));

        assignmentCriteria.setProjection(Projections.countDistinct("assignmentId"));
        assignmentCriteria.setMaxResults(1);
        assignmentCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        Long count = (Long) assignmentCriteria.uniqueResult();
        if (count == null || count.longValue() == 0L) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public EnrolledSupervisionReturnType getEnrolledSupervisions(UserContext uc, Long programOfferingId, Long startIndex, Long resultSize, String resultOrder) {

        if (programOfferingId == null) {
            String functionName = "getEnrolledSupervisions";
            String message = "Invalid Argument: programOfferingId is required";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Criteria criteria = session.createCriteria(ProgramAssignmentEntity.class);
        criteria.add(Restrictions.eq("programOffering.offeringId", programOfferingId));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        criteria.add(Restrictions.in("status", new HashSet<String>(Arrays.asList(new String[] { ALLOCATED }))));

        Long totalSize = (Long) criteria.setProjection(Projections.countDistinct("assignmentId")).uniqueResult();
        criteria.setProjection(null);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());
            criteria.setMaxResults(resultSize.intValue());
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                criteria.addOrder(ord);
            }
        }

        // Search and only return the predefined max number of records
        criteria.setLockMode(LockMode.NONE);
        criteria.setCacheMode(CacheMode.IGNORE);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.addOrder(Order.desc("supervisionId"));

        @SuppressWarnings({ "unchecked" }) List<ProgramAssignmentEntity> assignmentEntities = criteria.list();

        List<EnrolledSupervisionType> enrolledSupervisions = new ArrayList<>();
        for (ProgramAssignmentEntity assignmentEntity : assignmentEntities) {
            Long supervisionId = assignmentEntity.getSupervisionId();
            if (supervisionId != null) {
                SupervisionEntity supervision = BeanHelper.findEntity(session, SupervisionEntity.class, supervisionId);
                if (supervision != null) {
                    EnrolledSupervisionType enrolled = new EnrolledSupervisionType();
                    enrolled.setSupervisionId(supervisionId);
                    enrolled.setPersonIdentityId(supervision.getPersonIdentityId());
                    PersonIdentityEntity pi = BeanHelper.findEntity(session, PersonIdentityEntity.class, supervision.getPersonIdentityId());
                    if (pi != null) {
                        enrolled.setFirstName(pi.getFirstName());
                        enrolled.setLastName(pi.getLastName());
                        enrolled.setOffenderNumber(pi.getOffenderNumber());
                        enrolled.setDateOfBirth(pi.getDateOfBirth());
                        enrolled.setSex(pi.getSex());
                    }
                    enrolled.setPersonId(supervision.getPersonId());
                    enrolledSupervisions.add(enrolled);
                }
            }
        }
        EnrolledSupervisionReturnType ret = new EnrolledSupervisionReturnType();
        ret.setEnrolledSupervisions(enrolledSupervisions);
        ret.setTotalSize(totalSize);
        return ret;
    }

    @Override
    public List<EnrolledSupervisionType> getEnrolledSupervisions(UserContext uc, Long programOfferingId) {
        EnrolledSupervisionReturnType ret = getEnrolledSupervisions(uc, programOfferingId, null, null, null);
        if (ret == null) {
            return null;
        } else {
            return ret.getEnrolledSupervisions();
        }
    }

    @Override
    public Map<Long, Map<String, String>> getProgramOfferingCodes(UserContext uc, String programCategory) {
        Criteria c = session.createCriteria(ProgramOfferingEntity.class);
        c.setCacheable(true);
        c.setProjection(Projections.projectionList().add(Projections.id()).add(Projections.property("offeringCode")).add(Projections.property("offeringDescription")));

        Date now = new Date();
        c.add(Restrictions.or(Restrictions.isNull("expiryDate"), Restrictions.gt("expiryDate", now)));

        c.createCriteria("program", "prg");
        c.add(Restrictions.eqOrIsNull("prg.programCategory", programCategory));

        @SuppressWarnings("rawtypes") List rows = c.list();
        Map<Long, Map<String, String>> ret = new HashMap<>();
        for (Object row : rows) {
            Object[] cols = (Object[]) row;
            Long offeringId = (Long) cols[0];
            String code = (String) cols[1];
            String description = (String) cols[2];
            Map<String, String> m = new HashMap<String, String>();
            m.put(code, description);
            ret.put(offeringId, m);
        }
        return ret;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> getPorgramFacilityIDsByProgramCode(UserContext uc, String programCode) {
        Criteria c = session.createCriteria(ProgramOfferingEntity.class);
        c.setProjection(Projections.property("facilityId"));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        c.add(Restrictions.eqOrIsNull("prg.programCode", programCode));
        return c.list();
    }

    @Override
    public void deleteProgramOffering(UserContext uc, Long programOfferingId) {
        ProgramOfferingEntity entity = get(programOfferingId, ProgramOfferingEntity.class);
        if (entity != null) {
            Long scheduleId = entity.getScheduleId();
            if (scheduleId != null) {
                Schedule schedule = ActivityServiceAdapter.getSchedule(uc, scheduleId);
                if (schedule != null) {
                    schedule.setTerminationDate(new Date());
                    ActivityServiceAdapter.updateSchedule(uc, schedule);
                    ActivityServiceAdapter.deleteSchedule(uc, scheduleId);
                }
            }

            for (AdditionDateEntity additionDateEntity : entity.getAddedDates()) {
                session.delete(additionDateEntity);
            }
            entity.getAddedDates().clear();

            for (ModifiedPatternDateEntity modifiedPatternDateEntity : entity.getModifiedDates()) {
                session.delete(modifiedPatternDateEntity);
            }
            entity.getModifiedDates().clear();

            session.delete(entity);
        }
    }

    @Override
    public Boolean isDuplicateProgramOffering(UserContext uc, String programOfferingCode, Long organizationId, Long facilityId, Date startDateTime, Date endDateTime,
            Long... programOfferingId) {

        if (BeanHelper.isEmpty(programOfferingCode) || BeanHelper.isEmpty(organizationId) || BeanHelper.isEmpty(facilityId) || BeanHelper.isEmpty(startDateTime)
                || BeanHelper.isEmpty(endDateTime) || startDateTime.compareTo(endDateTime) > 0) {
            String functionName = "isDuplicateProgramOffering";
            String message = "Invalid Argument: programOfferingCode, organizationId, facilityId, startDateTime, endDateTime are required: " + programOfferingCode + ", "
                    + organizationId + ", " + facilityId + ", " + startDateTime + ", " + endDateTime;
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Criteria criteria = session.createCriteria(ProgramOfferingEntity.class);
        criteria.setProjection(Projections.countDistinct("offeringId"));
        criteria.setMaxResults(1);
        criteria.add(Restrictions.ilike("offeringCode", programOfferingCode));
        criteria.add(Restrictions.eq("organizationId", organizationId));
        criteria.add(Restrictions.eq("facilityId", facilityId));

        Long offeringId = null;
        if (programOfferingId != null && programOfferingId.length > 0) {
            offeringId = programOfferingId[0];
            if (offeringId != null) {
                criteria.add(Restrictions.eq("offeringId", offeringId));
            }
        }

        Date startDate = getDateOnly(startDateTime);
        criteria.add(Restrictions.eq("startDate", startDate));

        Date startTime = getZeroEpochTime(startDateTime);
        Date endTime = getZeroEpochTime(endDateTime);

        Disjunction disjunct = Restrictions.disjunction();

        disjunct.add(Restrictions.and(Restrictions.ge("startTime", startTime), Restrictions.le("endTime", endTime)));

        disjunct.add(Restrictions.and(Restrictions.le("startTime", startTime), Restrictions.ge("endTime", endTime)));

        disjunct.add(
                Restrictions.and(Restrictions.and(Restrictions.ge("startTime", startTime), Restrictions.lt("startTime", endTime)), Restrictions.le("endTime", endTime)));

        disjunct.add(
                Restrictions.and(Restrictions.and(Restrictions.lt("startTime", startTime), Restrictions.ge("endTime", startTime)), Restrictions.le("endTime", endTime)));

        Long count = (Long) criteria.uniqueResult();
        if (count == null) {
            count = 0L;
        }
        if (count.longValue() == 0L) {
            return false;
        } else {
            if (offeringId == null) {
                return true;
            } else {
                if (count.longValue() == 1L) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    @Override
    public ProgramOfferingReturnType searchProgramOffering(UserContext uc, Set<Long> programOfferingIds, ProgramOfferingSearchType search, Long startIndex,
            Long resultSize, String resultOrder) {

        Criteria criteria = session.createCriteria(ProgramOfferingEntity.class);
        if (!BeanHelper.isEmpty(programOfferingIds)) {
            Set<Set<Long>> IDs = BeanHelper.splitSet(programOfferingIds);
            if (IDs.isEmpty() || ((Set<?>) (IDs.toArray()[0])).isEmpty()) {
                ProgramOfferingReturnType ret = new ProgramOfferingReturnType();
                ret.setProgramOfferings(new ArrayList<ProgramOfferingType>());
                ret.setTotalSize(0L);
                return ret;
            }

            Disjunction disjunction = Restrictions.disjunction();
            for (Set<Long> ids : IDs) {
                disjunction.add(Restrictions.in("offeringId", ids));
            }
            criteria.add(disjunction);
        }

        if (search != null) {
            Long supervisionId = search.getSupervisionId();
            if (supervisionId != null) {
                DetachedCriteria assignmentCriteria = DetachedCriteria.forClass(ProgramAssignmentEntity.class);
                assignmentCriteria.createCriteria("programOffering", "offering");
                assignmentCriteria.setProjection(Projections.property("offering.offeringId"));
                assignmentCriteria.add(Restrictions.eq("supervisionId", supervisionId));
                Set<String> programAssignmentStatus = search.getProgramAssignmentStatus();
                if (!BeanHelper.isEmpty(programAssignmentStatus)) {
                    Set<String> assignmentStatusSet = new HashSet<>(
                            Arrays.asList(new String[] { REFERRED, CANCELLED, APPROVED, ALLOCATED, REFERRED, SUSPENDED, ABANDONED, COMPLETED }));
                    assignmentStatusSet.removeAll(programAssignmentStatus);
                    assignmentCriteria.add(Restrictions.in("status", assignmentStatusSet));
                }
                @SuppressWarnings("unchecked") List<Long> offeringIds = assignmentCriteria.getExecutableCriteria(session).list();
                if (!BeanHelper.isEmpty(offeringIds)) {
                    Disjunction disj = Restrictions.disjunction();
                    disj.add(Subqueries.propertyNotIn("offeringId", assignmentCriteria));
                    disj.add(Restrictions.isNull("programAssignments"));
                    disj.add(Restrictions.isEmpty("programAssignments"));
                    criteria.add(disj);
                }
            }

            Long facilityId = search.getFacilityId();
            if (facilityId != null) {
                criteria.add(Restrictions.eq("facilityId", facilityId));
            }

            Set<Long> permittedFacilities = search.getPermittedFacilities();
            if (!BeanHelper.isEmpty(permittedFacilities)) {
                Set<Set<Long>> ff = BeanHelper.splitSet(permittedFacilities);
                if (ff.isEmpty() || ((Set<?>) (ff.toArray()[0])).isEmpty()) {
                    ProgramOfferingReturnType ret = new ProgramOfferingReturnType();
                    ret.setProgramOfferings(new ArrayList<ProgramOfferingType>());
                    ret.setTotalSize(0L);
                    return ret;
                }

                Disjunction disjunction = Restrictions.disjunction();
                for (Set<Long> ids : ff) {
                    disjunction.add(Restrictions.in("facilityId", ids));
                }
                criteria.add(disjunction);
            } else {
                ProgramOfferingReturnType ret = new ProgramOfferingReturnType();
                ret.setProgramOfferings(new ArrayList<ProgramOfferingType>());
                ret.setTotalSize(0L);
                return ret;
            }

            Long organizationId = search.getOgranizationId();
            if (organizationId != null) {
                criteria.add(Restrictions.eq("organizationId", organizationId));
            }

            Date fromStartDate = search.getFromStartDate();
            if (fromStartDate != null) {
                criteria.add(Restrictions.ge("startDate", fromStartDate));
            }

            Date toStartDate = search.getToStartDate();
            if (toStartDate != null) {
                criteria.add(Restrictions.le("startDate", toStartDate));
            }

            Date fromEndDate = search.getFromEndDate();
            if (fromEndDate != null) {
                criteria.add(Restrictions.or(Restrictions.ge("endDate", fromEndDate), Restrictions.isNull("endDate")));
            }

            Date toEndDate = search.getToEndDate();
            if (toEndDate != null) {
                criteria.add(Restrictions.and(Restrictions.le("endDate", toEndDate), Restrictions.isNotNull("endDate")));
            }

            Date now = new Date();
            criteria.createCriteria("program", "p");

            Long programId = search.getProgramId();
            if (programId != null) {
                criteria.add(Restrictions.eq("p.programId", programId));
            }

            Boolean flexibleStart = search.getFlexibleStart();
            if (flexibleStart != null) {
                criteria.add(Restrictions.eq("p.flexibleStart", flexibleStart));
            }

            Disjunction disjunction = Restrictions.disjunction();
            disjunction.add(Restrictions.or(Restrictions.gt("startDate", getDateOnly(now)),
                    Restrictions.and(Restrictions.eq("startDate", getDateOnly(now)), Restrictions.gt("startTime", this.getZeroEpochTime(now)))));

            if (supervisionId != null) {
                disjunction.add(Restrictions.and(Restrictions.eq("p.flexibleStart", Boolean.TRUE), Restrictions.or(Restrictions.isNull("endDate"),
                        Restrictions.or(Restrictions.gt("endDate", getDateOnly(now)),
                                Restrictions.and(Restrictions.eq("endDate", getDateOnly(now)), Restrictions.gt("startTime", this.getZeroEpochTime(now)))))));
            } else {
                disjunction.add(Restrictions.or(Restrictions.isNull("endDate"), Restrictions.or(Restrictions.gt("endDate", getDateOnly(now)),
                        Restrictions.and(Restrictions.eq("endDate", getDateOnly(now)), Restrictions.gt("startTime", this.getZeroEpochTime(now))))));
            }

            String status = search.getStatus();
            if (!BeanHelper.isEmpty(status)) {
                if (status.equalsIgnoreCase("ACTIVE")) {
                    criteria.add(Restrictions.and(Restrictions.or(Restrictions.isNull("expiryDate"), Restrictions.gt("expiryDate", new Date())), disjunction));
                } else if (status.equalsIgnoreCase("INACTIVE")) {
                    criteria.add(Restrictions.or(Restrictions.and(Restrictions.isNotNull("expiryDate"), Restrictions.lt("expiryDate", new Date())),
                            Restrictions.not(disjunction)));
                }
            }

            if (supervisionId != null) {
                SupervisionEntity supervisionEntity = BeanHelper.getEntity(session, SupervisionEntity.class, supervisionId);
                if (BeanHelper.isEmpty(search.getProgramCategory())) {
                    criteria.add(Restrictions.or(Restrictions.ne("p.programCategory", PROGRAM_CATEGORY_INSTITUT),
                            Restrictions.and(Restrictions.eq("p.programCategory", PROGRAM_CATEGORY_INSTITUT),
                                    Restrictions.eq("facilityId", supervisionEntity.getFacilityId()))));
                } else {
                    if (search.getProgramCategory().equalsIgnoreCase(PROGRAM_CATEGORY_INSTITUT)) {
                        criteria.add(Restrictions.and(Restrictions.eq("p.programCategory", PROGRAM_CATEGORY_INSTITUT),
                                Restrictions.eq("facilityId", supervisionEntity.getFacilityId())));
                    } else {
                        criteria.add(Restrictions.eq("p.programCategory", search.getProgramCategory()));
                    }
                }
            }

            String programCategory = search.getProgramCategory();
            String programCode = search.getProgramCode();
            String programDescription = search.getProgramDescription();
            Set<String> genders = search.getGenders();
            Set<String> ageRanges = search.getAgeRanges();
            Set<String> amenities = search.getAmenities();
            if (!BeanHelper.isEmpty(programCategory) || !BeanHelper.isEmpty(programDescription) || !BeanHelper.isEmpty(programCode) || !BeanHelper.isEmpty(genders)
                    || !BeanHelper.isEmpty(ageRanges) || !BeanHelper.isEmpty(amenities)) {

                if (!BeanHelper.isEmpty(programCategory) && supervisionId == null) {
                    criteria.add(Restrictions.eq("p.programCategory", programCategory));
                }

                if (!BeanHelper.isEmpty(programCode)) {
                    criteria.add(Restrictions.ilike("p.programCode", BeanHelper.strWildcardParser(programCode)));
                }

                if (!BeanHelper.isEmpty(programDescription)) {
                    criteria.add(Restrictions.ilike("p.programDescription", BeanHelper.strWildcardParser(programDescription)));
                }

                if (!BeanHelper.isEmpty(genders) || !BeanHelper.isEmpty(ageRanges) || !BeanHelper.isEmpty(amenities)) {
                    DetachedCriteria dc = DetachedCriteria.forClass(TargetInmateEntity.class);
                    dc.setProjection(Property.forName("programOffering.offeringId"));
                    if (!BeanHelper.isEmpty(genders)) {
                        dc.createCriteria("genders", "g");
                        dc.add(Restrictions.in("g.gender", genders));
                    }

                    if (!BeanHelper.isEmpty(ageRanges)) {
                        dc.createCriteria("ageRanges", "a");
                        dc.add(Restrictions.in("a.ageRange", ageRanges));
                    }

                    if (!BeanHelper.isEmpty(amenities)) {
                        dc.createCriteria("amenities", "am");
                        dc.add(Restrictions.in("am.amenity", amenities));
                    }

                    criteria.add(Subqueries.propertyIn("offeringId", dc));
                }
            }

            if (!BeanHelper.isEmpty(genders) || !BeanHelper.isEmpty(ageRanges) || !BeanHelper.isEmpty(amenities)) {
                criteria.createCriteria("targetInmates", "tInmates");
                if (!BeanHelper.isEmpty(genders)) {
                    criteria.createCriteria("tInmates.genders", "tGenders");
                    criteria.add(Restrictions.in("tGenders.gender", genders));
                }

                if (!BeanHelper.isEmpty(ageRanges)) {
                    criteria.createCriteria("tInmates.ageRanges", "tAgeRanges");
                    criteria.add(Restrictions.in("tAgeRanges.ageRange", ageRanges));
                }

                if (!BeanHelper.isEmpty(amenities)) {
                    criteria.createCriteria("tInmates.amenities", "tAmenities");
                    criteria.add(Restrictions.in("tAmenities.amenity", amenities));
                }
            }

            String offeringCodeDescription = search.getOfferingDescription();
            if (!BeanHelper.isEmpty(offeringCodeDescription)) {
                criteria.add(Restrictions.or(Restrictions.ilike("offeringCode", BeanHelper.strWildcardParser(offeringCodeDescription)),
                        Restrictions.ilike("offeringDescription", BeanHelper.strWildcardParser(offeringCodeDescription))));
            }
        }

        Long totalSize = (Long) criteria.setProjection(Projections.countDistinct("offeringId")).uniqueResult();
        criteria.setProjection(null);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());
            criteria.setMaxResults(resultSize.intValue());
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                criteria.addOrder(ord);
            }
        }

        // Search and only return the predefined max number of records
        criteria.setLockMode(LockMode.NONE);
        criteria.setCacheMode(CacheMode.IGNORE);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        @SuppressWarnings("unchecked") List<ProgramOfferingEntity> list = criteria.list();
        if (resultSize == null && list.size() > SEARCH_MAX_LIMIT) {
            throw new ArbutusRuntimeException("Search result is over " + SEARCH_MAX_LIMIT);
        }

        ProgramOfferingReturnType ret = new ProgramOfferingReturnType();
        List<ProgramOfferingType> programOfferings = toProgramOfferingTypeList(uc, list);
        ret.setProgramOfferings(programOfferings);
        ret.setTotalSize(totalSize);
        return ret;
    }

    @Override
    public ProgramOfferingReturnType searchProgramOfferingBySessionDateRange(UserContext uc, ProgramOfferingSearchType search, Long startIndex, Long resultSize,
            String resultOrder) {
        log.info("searchProgramOfferingBySessionDateRange:" + search);
        Criteria criteria = session.createCriteria(ProgramOfferingEntity.class);

        if (search != null) {
            DetachedCriteria assignmentCriteria = DetachedCriteria.forClass(ProgramAssignmentEntity.class);
            assignmentCriteria.createCriteria("programOffering", "offering");
            assignmentCriteria.setProjection(Projections.property("offering.offeringId"));
            Set<String> programAssignmentStatus = search.getProgramAssignmentStatus();
            if (!BeanHelper.isEmpty(programAssignmentStatus)) {
                Set<String> assignmentStatusSet = new HashSet<>(
                        Arrays.asList(new String[] { REFERRED, CANCELLED, APPROVED, ALLOCATED, REFERRED, SUSPENDED, ABANDONED, COMPLETED }));
                assignmentStatusSet.removeAll(programAssignmentStatus);
                assignmentCriteria.add(Restrictions.in("status", assignmentStatusSet));
            }

            List<Long> offeringIds = assignmentCriteria.getExecutableCriteria(session).list();
            criteria.add(Subqueries.propertyIn("offeringId", assignmentCriteria));

            Long organizationId = search.getOgranizationId();
            if (organizationId != null) {
                criteria.add(Restrictions.eq("organizationId", organizationId));
            }

            Long facilityId = search.getFacilityId();
            if (facilityId != null) {
                criteria.add(Restrictions.eq("facilityId", facilityId));
            }

            Date fromStartDate = search.getFromStartDate();
            Date toEndDate = search.getToEndDate();

            Disjunction disjunction = Restrictions.disjunction();
            disjunction.add(Restrictions.isNotNull("endDate"));

            if (fromStartDate != null && toEndDate != null) {
                criteria.add(disjunction);
                //               criteria.add(Restrictions.or(Restrictions.and(Restrictions.le("startDate", fromStartDate), Restrictions.ge("endDate", fromStartDate)), Restrictions.and(Restrictions.le("startDate", toEndDate), Restrictions.ge("endDate", toEndDate)), Restrictions.and(Restrictions.ge("startDate", fromStartDate), Restrictions.le("endDate", toEndDate)), Restrictions.and(Restrictions.ge("startDate", fromStartDate), Restrictions.le("endDate", toEndDate))));
                //               criteria.add(Restrictions.or(Restrictions.and(Restrictions.le("startDate", fromStartDate), Restrictions.ge("endDate", fromStartDate))));
                criteria.add(Restrictions.or(Restrictions.and(Restrictions.le("startDate", fromStartDate), Restrictions.ge("endDate", fromStartDate)),
                        Restrictions.and(Restrictions.le("startDate", toEndDate), Restrictions.ge("endDate", toEndDate)),
                        Restrictions.and(Restrictions.ge("startDate", fromStartDate), Restrictions.le("endDate", toEndDate)),
                        Restrictions.and(Restrictions.ge("startDate", fromStartDate))));
            } else if (fromStartDate != null) {
                criteria.add(disjunction);
                criteria.add(Restrictions.ge("endDate", fromStartDate));
            } else if (toEndDate != null) {
                criteria.add(Restrictions.le("startDate", toEndDate));
            }

            criteria.createCriteria("program", "p");
            String programCategory = search.getProgramCategory();
            String programCode = search.getProgramCode();
            String programDescription = search.getProgramDescription();

            if (!BeanHelper.isEmpty(programCategory)) {
                criteria.add(Restrictions.eq("p.programCategory", programCategory));
            }

            if (!BeanHelper.isEmpty(programCode)) {
                criteria.add(Restrictions.ilike("p.programCode", BeanHelper.strWildcardParser(programCode)));
            }

            if (!BeanHelper.isEmpty(programDescription)) {
                criteria.add(Restrictions.ilike("p.programDescription", BeanHelper.strWildcardParser(programDescription)));
            }

            String offeringCodeDescription = search.getOfferingDescription();
            if (!BeanHelper.isEmpty(offeringCodeDescription)) {
                criteria.add(Restrictions.or(Restrictions.ilike("offeringCode", BeanHelper.strWildcardParser(offeringCodeDescription)),
                        Restrictions.ilike("offeringDescription", BeanHelper.strWildcardParser(offeringCodeDescription))));
            }

        }

        Long totalSize = (Long) criteria.setProjection(Projections.countDistinct("offeringId")).uniqueResult();
        criteria.setProjection(null);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());
            criteria.setMaxResults(resultSize.intValue());
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                criteria.addOrder(ord);
            }
        }

        // Search and only return the predefined max number of records
        criteria.setLockMode(LockMode.NONE);
        criteria.setCacheMode(CacheMode.IGNORE);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        @SuppressWarnings("unchecked") List<ProgramOfferingEntity> list = criteria.list();
        if (resultSize == null && list.size() > SEARCH_MAX_LIMIT) {
            throw new ArbutusRuntimeException("Search result is over " + SEARCH_MAX_LIMIT);
        }

        ProgramOfferingReturnType ret = new ProgramOfferingReturnType();
        List<ProgramOfferingType> programOfferings = toProgramOfferingTypeList(uc, list);

        ret.setProgramOfferings(programOfferings);
        ret.setTotalSize(totalSize);
        return ret;
    }

    public String toSql(Criteria criteria) {
        try {
            CriteriaImpl c = (CriteriaImpl) criteria;
            SessionImpl s = (SessionImpl) c.getSession();
            SessionFactoryImplementor factory = (SessionFactoryImplementor) s.getSessionFactory();
            String[] implementors = factory.getImplementors(c.getEntityOrClassName());
            CriteriaLoader loader = new CriteriaLoader((OuterJoinLoadable) factory.getEntityPersister(implementors[0]), factory, c, implementors[0],
                    (LoadQueryInfluencers) s.getEnabledFilters());
            Field f = OuterJoinLoader.class.getDeclaredField("sql");
            f.setAccessible(true);
            return (String) f.get(loader);
        } catch (Exception e) {
            //throw new RuntimeException(e); 
        }

        return "";
    }

    public boolean inRecurrenceDays(Date date, Set<String> recurranceDays) {
        int dayOfWeek = new DateTime(date).getDayOfWeek();
        Set<Integer> dayOfWeeks = new HashSet<>();

        for (String wkDay : recurranceDays) {
            if (wkDay.equalsIgnoreCase("MON")) {
                dayOfWeeks.add(Integer.valueOf(1));
            }
            if (wkDay.equalsIgnoreCase("TUE")) {
                dayOfWeeks.add(Integer.valueOf(2));
            }
            if (wkDay.equalsIgnoreCase("WED")) {
                dayOfWeeks.add(Integer.valueOf(3));
            }
            if (wkDay.equalsIgnoreCase("THU")) {
                dayOfWeeks.add(Integer.valueOf(4));
            }
            if (wkDay.equalsIgnoreCase("FRI")) {
                dayOfWeeks.add(Integer.valueOf(5));
            }
            if (wkDay.equalsIgnoreCase("SAT")) {
                dayOfWeeks.add(Integer.valueOf(6));
            }
            if (wkDay.equalsIgnoreCase("SUN")) {
                dayOfWeeks.add(Integer.valueOf(7));
            }
        }

        return dayOfWeeks.contains(Integer.valueOf(dayOfWeek));
    }

    @Override
    public TimeslotIterator getProgramTimeslotByScheduleId(UserContext uc, Long scheduleId, Date lookupDate, Date lookupEndDate, Long count) {
        return ActivityServiceAdapter.generateTimeslotIterator(uc, scheduleId, lookupDate, lookupEndDate, count);
    }

    @Override
    public TimeslotIterator getProgramTimeslotByProgramOfferingId(UserContext uc, Long programOfferingId, Date lookupDate, Date lookupEndDate, Long count) {
        ProgramOfferingEntity programOffering = BeanHelper.findEntity(session, ProgramOfferingEntity.class, programOfferingId);
        if (programOffering == null || programOffering.getScheduleId() == null) {
            return null;
        }
        return getProgramTimeslotByScheduleId(uc, programOffering.getScheduleId(), lookupDate, lookupEndDate, count);
    }

    @Override
    public void addAdditionDateForOffering(UserContext uc, Long programOfferingId, ScheduleSessionDateType additionDate) {
        String functionName = "addAdditionDateForOffering";
        if (programOfferingId == null) {
            String message = "Invalid Argument: programOfferingId is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        if (additionDate == null) {
            return;
        }
        ValidationHelper.validate(additionDate);
        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);
        AdditionDateEntity additionDateEntity = toAdditionDateEntity(additionDate, stamp);
        ValidationHelper.verifyMetaCodes(additionDateEntity, true);
        ProgramOfferingEntity programOffering = (ProgramOfferingEntity) session.get(ProgramOfferingEntity.class, programOfferingId);
        if (programOffering == null) {
            return;
        }

        LocalDate from = additionDate.getDate();
        LocalDate to = additionDate.getDate().plusDays(1);
        LocalTime startTime = additionDate.getStartTime();
        LocalTime endTime = additionDate.getEndTime();
        List<ScheduleSessionDateType> subtractionDates = getModifiedPatternDatesForOffering(uc, programOfferingId, from, to, startTime, endTime);
        if (subtractionDates != null) {
            this.deleteModifiedPatternSetForOffering(uc, programOfferingId, new HashSet<>(subtractionDates));
        } else {
            programOffering.addAddedDate(additionDateEntity);
            session.update(programOffering);
        }
    }

    @Override
    public void addAdditionDateSetForOffering(UserContext uc, Long programOfferingId, Set<ScheduleSessionDateType> additionDates) {
        if (BeanHelper.isEmpty(additionDates)) {
            return;
        }
        for (ScheduleSessionDateType additionDate : additionDates) {
            addAdditionDateForOffering(uc, programOfferingId, additionDate);
        }
    }

    @Override
    public void deleteAdditionDateForOffering(UserContext uc, Long programOfferingId, ScheduleSessionDateType additionDate) {
        String functionName = "deleteAdditionDateForOffering";
        if (programOfferingId == null) {
            String message = "Invalid Argument: programOfferingId is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(additionDate);
        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);
        Criteria criteria = session.createCriteria(AdditionDateEntity.class);
        criteria.createCriteria("programOffering", "offering");
        criteria.add(Restrictions.eq("offering.offeringId", programOfferingId));
        AdditionDateEntity additionDateEntity = this.toAdditionDateEntity(additionDate, stamp);
        Date localDate = additionDateEntity.getDate();
        if (localDate != null) {
            criteria.add(Restrictions.eq("date", localDate));
        }
        Date startTime = additionDateEntity.getStartTime();
        if (startTime != null) {
            criteria.add(Restrictions.eq("startTime", startTime));
        }
        Date endTime = additionDateEntity.getEndTime();
        if (endTime != null) {
            criteria.add(Restrictions.eq("endTime", endTime));
        }
        @SuppressWarnings("unchecked") Iterator<AdditionDateEntity> it = criteria.list().iterator();
        while (it.hasNext()) {
            session.delete(it.next());
        }
    }

    @Override
    public void deleteAdditionDateSetForOffering(UserContext uc, Long programOfferingId, Set<ScheduleSessionDateType> additionDates) {
        if (BeanHelper.isEmpty(additionDates)) {
            return;
        }
        for (ScheduleSessionDateType additionDate : additionDates) {
            deleteAdditionDateForOffering(uc, programOfferingId, additionDate);
        }
    }

    @Override
    public List<ScheduleSessionDateType> getAdditionDatesForOffering(UserContext uc, Long programOfferingId, LocalDate from, LocalDate to, LocalTime startTime,
            LocalTime endTime) {
        // String functionName = "getAdditionDatesForOffering";
        if (programOfferingId == null) {
         /*String message = "Invalid Argument: programOfferingId is required.";
         LogHelper.error(log, functionName, message);
         throw new InvalidInputException(message);*/
            return null;
        }
        Criteria criteria = session.createCriteria(AdditionDateEntity.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.createCriteria("programOffering", "offering");
        criteria.add(Restrictions.eq("offering.offeringId", programOfferingId));

        if (from != null) {
            criteria.add(Restrictions.ge("date", from.toDateTimeAtStartOfDay().toDate()));
        }
        if (to != null) {
            criteria.add(Restrictions.le("date", to.toDateTimeAtStartOfDay().toDate()));
        }
        if (startTime != null) {
            Calendar start = Calendar.getInstance();
            start.clear();
            start.set(Calendar.YEAR, 1970);
            start.set(Calendar.MONTH, Calendar.JANUARY);
            start.set(Calendar.DAY_OF_MONTH, 1);

            start.set(Calendar.HOUR_OF_DAY, startTime.getHourOfDay());
            start.set(Calendar.MINUTE, startTime.getMinuteOfHour());
            start.set(Calendar.SECOND, startTime.getSecondOfMinute());
            start.set(Calendar.MILLISECOND, 0);

            criteria.add(Restrictions.eq("startTime", start.getTime()));
        }
        if (endTime != null) {
            Calendar end = Calendar.getInstance();
            end.clear();
            end.set(Calendar.YEAR, 1970);
            end.set(Calendar.MONTH, Calendar.JANUARY);
            end.set(Calendar.DAY_OF_MONTH, 1);

            end.set(Calendar.HOUR_OF_DAY, endTime.getHourOfDay());
            end.set(Calendar.MINUTE, endTime.getMinuteOfHour());
            end.set(Calendar.SECOND, endTime.getSecondOfMinute());
            end.set(Calendar.MILLISECOND, 0);

            criteria.add(Restrictions.eq("endTime", end.getTime()));
        }

        @SuppressWarnings("unchecked") List<AdditionDateEntity> additionDateEntityList = criteria.list();

        List<ScheduleSessionDateType> ret = this.toAdditionDateList(additionDateEntityList);
        return ret;
    }

    @Override
    public void addModifiedPatternDateForOffering(UserContext uc, Long programOfferingId, ScheduleSessionDateType subtractionDate) {
        String functionName = "addModifiedPatternDateForOffering";
        if (programOfferingId == null) {
            String message = "Invalid Argument: programOfferingId is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        if (subtractionDate == null) {
            return;
        }
        ValidationHelper.validate(subtractionDate);
        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);
        ModifiedPatternDateEntity modifiedPatternDateEntity = toModifiedPatternDateEntity(subtractionDate, stamp);
        ValidationHelper.verifyMetaCodes(modifiedPatternDateEntity, true);
        ProgramOfferingEntity programOffering = (ProgramOfferingEntity) session.get(ProgramOfferingEntity.class, programOfferingId);
        if (programOffering == null) {
            return;
        }
        LocalDate from = subtractionDate.getDate();
        LocalDate to = subtractionDate.getDate().plusDays(1);
        LocalTime startTime = subtractionDate.getStartTime();
        LocalTime endTime = subtractionDate.getEndTime();
        List<ScheduleSessionDateType> additionDates = this.getAdditionDatesForOffering(uc, programOfferingId, from, to, startTime, endTime);
        if (additionDates != null) {
            this.deleteAdditionDateSetForOffering(uc, programOfferingId, new HashSet<>(additionDates));
        } else {
            programOffering.addModifiedDate(modifiedPatternDateEntity);
            session.update(programOffering);
        }
    }

    @Override
    public void addModifiedPatternDateSetForOffering(UserContext uc, Long programOfferingId, Set<ScheduleSessionDateType> subtractionDates) {
        if (BeanHelper.isEmpty(subtractionDates)) {
            return;
        }
        for (ScheduleSessionDateType subtractionDate : subtractionDates) {
            addModifiedPatternDateForOffering(uc, programOfferingId, subtractionDate);
        }
    }

    @Override
    public void deleteModifiedPatternDateForOffering(UserContext uc, Long programOfferingId, ScheduleSessionDateType subtractionDate) {
        String functionName = "deleteAdditionDateForOffering";
        if (programOfferingId == null) {
            String message = "Invalid Argument: programOfferingId is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(subtractionDate);
        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);
        Criteria criteria = session.createCriteria(ModifiedPatternDateEntity.class);
        criteria.createCriteria("programOffering", "offering");
        criteria.add(Restrictions.eq("offering.offeringId", programOfferingId));
        ModifiedPatternDateEntity modifiedPatternDateEntity = this.toModifiedPatternDateEntity(subtractionDate, stamp);
        Date localDate = modifiedPatternDateEntity.getDate();
        if (localDate != null) {
            criteria.add(Restrictions.eq("date", localDate));
        }
        Date startTime = modifiedPatternDateEntity.getStartTime();
        if (startTime != null) {
            criteria.add(Restrictions.eq("startTime", startTime));
        }
        Date endTime = modifiedPatternDateEntity.getEndTime();
        if (endTime != null) {
            criteria.add(Restrictions.eq("endTime", endTime));
        }
        @SuppressWarnings("unchecked") Iterator<AdditionDateEntity> it = criteria.list().iterator();
        while (it.hasNext()) {
            session.delete(it.next());
        }
    }

    @Override
    public void deleteModifiedPatternSetForOffering(UserContext uc, Long programOfferingId, Set<ScheduleSessionDateType> subtractionDates) {
        if (BeanHelper.isEmpty(subtractionDates)) {
            return;
        }
        for (ScheduleSessionDateType subtractionDate : subtractionDates) {
            deleteModifiedPatternDateForOffering(uc, programOfferingId, subtractionDate);
        }
    }

    @Override
    public List<ScheduleSessionDateType> getModifiedPatternDatesForOffering(UserContext uc, Long programOfferingId, LocalDate from, LocalDate to, LocalTime startTime,
            LocalTime endTime) {
        String functionName = "getAdditionDatesForOffering";
        if (programOfferingId == null) {
         /*String message = "Invalid Argument: programOfferingId is required.";
         LogHelper.error(log, functionName, message);
         throw new InvalidInputException(message);*/
            return null;
        }
        Criteria criteria = session.createCriteria(ModifiedPatternDateEntity.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.createCriteria("programOffering", "offering");
        criteria.add(Restrictions.eq("offering.offeringId", programOfferingId));

        if (from != null) {
            criteria.add(Restrictions.ge("date", from.toDateTimeAtStartOfDay().toDate()));
        }
        if (to != null) {
            criteria.add(Restrictions.le("date", to.toDateTimeAtStartOfDay().toDate()));
        }
        if (startTime != null) {
            Calendar start = Calendar.getInstance();
            start.clear();
            start.set(Calendar.YEAR, 1970);
            start.set(Calendar.MONTH, Calendar.JANUARY);
            start.set(Calendar.DAY_OF_MONTH, 1);

            start.set(Calendar.HOUR_OF_DAY, startTime.getHourOfDay());
            start.set(Calendar.MINUTE, startTime.getMinuteOfHour());
            start.set(Calendar.SECOND, startTime.getSecondOfMinute());
            start.set(Calendar.MILLISECOND, 0);

            criteria.add(Restrictions.eq("startTime", start.getTime()));
        }
        if (endTime != null) {
            Calendar end = Calendar.getInstance();
            end.clear();
            end.set(Calendar.YEAR, 1970);
            end.set(Calendar.MONTH, Calendar.JANUARY);
            end.set(Calendar.DAY_OF_MONTH, 1);

            end.set(Calendar.HOUR_OF_DAY, endTime.getHourOfDay());
            end.set(Calendar.MINUTE, endTime.getMinuteOfHour());
            end.set(Calendar.SECOND, endTime.getSecondOfMinute());
            end.set(Calendar.MILLISECOND, 0);

            criteria.add(Restrictions.eq("endTime", end.getTime()));
        }

        @SuppressWarnings("unchecked") List<ModifiedPatternDateEntity> modifiedPatternDateEntityList = criteria.list();

        List<ScheduleSessionDateType> ret = this.toSubtractionDateTypeList(modifiedPatternDateEntityList);
        return ret;
    }

    @Override
    public TimeslotIterator getProgramTimeslot(UserContext uc, Boolean isGenerateSchedule, Set<String> recurranceDays, Date lookupStartDate, Date lookupEndDate,
            Date lookupEndTime, Long count) {

        String functionName = "getProgramTimeslot";

        if (lookupStartDate == null) {
            String message = "Invalid argument: lookupDate is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        if (Boolean.TRUE.equals(isGenerateSchedule) && BeanHelper.isEmpty(recurranceDays)) {
            String message = "Invalid argument: schedule recurrance days are required when single occurrance is false and generate schedule is true.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        if (Boolean.TRUE.equals(isGenerateSchedule) && !BeanHelper.isEmpty(recurranceDays)) {
            DailyWeeklyCustomRecurrencePatternType dailyRecurrencePattern = new DailyWeeklyCustomRecurrencePatternType(1L, false, recurranceDays);

            return ActivityServiceAdapter.generateTimrslotIterator(uc, dailyRecurrencePattern, lookupStartDate, lookupEndDate, lookupEndTime, count);
        }

        return null;
    }

    @Override
    public TimeslotIterator getProgramTimeslot(UserContext uc, ProgramOfferingType programOffering, Date lookupDate, Date lookupEndDate, Long count) {
        Date lookupStartDate = programOffering.getStartDate();
        Date lookupEndTime = programOffering.getEndTime();
        boolean isGenerateSchedule = programOffering.getGenerateSchedule();
        Set<String> recurranceDays = programOffering.getRecurranceDays();
        return getProgramTimeslot(uc, isGenerateSchedule, recurranceDays, lookupStartDate, lookupEndDate, lookupEndTime, count);
    }

    @Override
    public TimeslotIterator getProgramTimeslot(UserContext uc, ProgramAssignmentType programAssignment, Date lookupDate, Date lookupEndDate, Date endTime, Long count) {
        String functionName = "getProgramTimeslot";
        if (lookupDate == null) {
            String message = "Invalid argument: lookupDate is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        if (programAssignment == null) {
            String message = "Invalid argument: programAssignment is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        Long assignmentId = programAssignment.getAssignmentId();
        Long supervisionId = programAssignment.getSupervisionId();
        if (assignmentId == null || supervisionId == null) {
            String message = "Invalid argument: Program Assignment Id and supervision Id are required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        ProgramOfferingEntity offeringEntity = getProgramOfferingEntity(assignmentId, supervisionId);

        lookupDate = getDateTime(lookupDate, offeringEntity.getStartTime());

        Long scheduleId = offeringEntity.getScheduleId();
        Schedule schedule = ActivityServiceAdapter.getSchedule(uc, scheduleId);
        RecurrencePatternType scheduleRecurrencePattern = schedule.getRecurrencePattern();
        Set<ExDateType> inDates = scheduleRecurrencePattern.getIncludeDateTimes();
        if (BeanHelper.isEmpty(inDates)) {
            inDates = new HashSet<>();
        }
        Set<ExDateType> exDates = scheduleRecurrencePattern.getExcludeDateTimes();
        if (BeanHelper.isEmpty(exDates)) {
            exDates = new HashSet<>();
        }

        Set<ScheduleSessionDateType> addedDates = programAssignment.getAddedDates();
        Set<ScheduleSessionDateType> offeringDadedDates = toAdditionDateSet(offeringEntity.getAddedDates());
        if (offeringDadedDates != null && !offeringDadedDates.isEmpty()) {
            addedDates.addAll(offeringDadedDates);
        }
        for (ScheduleSessionDateType additionDate : addedDates) {
            ExDateType inDate = new ExDateType();
            inDate.setDate(additionDate.getDate());
            inDate.setStartTime(additionDate.getStartTime());
            inDate.setEndTime(additionDate.getEndTime());
            inDates.add(inDate);
        }

        Set<ScheduleSessionDateType> removedDates = programAssignment.getRemovedDates();
        Set<ScheduleSessionDateType> offeringRemovedDates = toSubtractionDateTypeSet(offeringEntity.getModifiedDates());
        if (offeringRemovedDates != null && !offeringRemovedDates.isEmpty()) {
            removedDates.addAll(offeringRemovedDates);
        }
        for (ScheduleSessionDateType subtractionDate : removedDates) {
            ExDateType exDate = new ExDateType();
            exDate.setDate(subtractionDate.getDate());
            exDate.setStartTime(subtractionDate.getStartTime());
            exDate.setEndTime(subtractionDate.getEndTime());
            exDates.add(exDate);
        }

        scheduleRecurrencePattern.setIncludeDateTimes(inDates);
        scheduleRecurrencePattern.setExcludeDateTimes(exDates);
        return ActivityServiceAdapter.generateTimrslotIterator(uc, scheduleRecurrencePattern, lookupDate, lookupEndDate, endTime, count);
    }

    @Override
    public ProgramAssignmentType createProgramAssignment(UserContext uc, ProgramAssignmentType programAssignment) {
        ValidationHelper.validate(programAssignment);
        if (programAssignment.getAssignmentId() != null) {
            return updateProgramAssignment(uc, programAssignment);
        } else if (programAssignment.getProgrammOffering() != null) {
            Set<ProgramAssignmentType> assignments = programAssignment.getProgrammOffering().getProgramAssignments();
            if (assignments != null && assignments.size() != 0) {
                for (ProgramAssignmentType assignment : assignments) {
                    if (assignment.getAssignmentId() != null && assignment.getSupervisionId() != null
                            && assignment.getSupervisionId() == programAssignment.getSupervisionId() && assignment.getProgrammOffering().getOfferingId() != null
                            && programAssignment.getProgrammOffering() != null
                            && assignment.getProgrammOffering().getOfferingId() == programAssignment.getProgrammOffering().getOfferingId()) {
                        programAssignment.setAssignmentId(assignment.getAssignmentId());
                        return updateProgramAssignment(uc, programAssignment);
                    }
                }
            }
        }

        ProgramEntity programEntity = getProgramEntity(session, programAssignment.getProgram());
        ProgramOfferingEntity programOfferingEntity = getProgramOfferingEntity(session, programAssignment.getProgrammOffering());
        // ProgramAttendanceEntity programAttendanceEntity = getProgramAttendanceEntity(session, programAssignment.getProgramAttendance());

        StampEntity stamp = getCreateStamp(uc);
        ProgramAssignmentEntity entity = toProgramAssignmentEntity(programAssignment, stamp);
        // entity.setAssignmentId(null);
        if (programOfferingEntity != null) {
            Set<ProgramAssignmentEntity> assignments = programOfferingEntity.getProgramAssignments();
            if (assignments != null) {
                for (ProgramAssignmentEntity assignment : assignments) {
                    if (assignment.getProgramOffering() != null && programAssignment.getProgrammOffering() != null
                            && assignment.getProgramOffering().getOfferingId() == programAssignment.getProgrammOffering().getOfferingId()
                            && assignment.getSupervisionId() == programAssignment.getSupervisionId()) {
                        entity.setAssignmentId(assignment.getAssignmentId());
                        entity.setVersion(assignment.getVersion());
                        break;
                    }
                }
            }
        }

        if (entity.getStartDate() != null && programOfferingEntity != null && programOfferingEntity.getStartDate() != null) {
            if (entity.getStartDate().compareTo(programOfferingEntity.getStartDate()) < 0) {
                entity.setStartDate(programOfferingEntity.getStartDate());
            }
        }
        if (programOfferingEntity != null && programOfferingEntity.getStartDate() != null) {
            if (entity.getEndDate() != null && programOfferingEntity.getEndDate() != null && entity.getEndDate().compareTo(programOfferingEntity.getEndDate()) > 0) {
                entity.setEndDate(programOfferingEntity.getEndDate());
            }
        }
        if ((entity.getStartDate() == null || entity.getEndDate() == null) && programOfferingEntity != null && programOfferingEntity.getSingleOccurrence()) {
            entity.setStartDate(programOfferingEntity.getStartDate());
            entity.setEndDate(getDateTime(programOfferingEntity.getEndDate(), programOfferingEntity.getEndTime()));
        }

        ValidationHelper.verifyMetaCodes(entity, true);

        setProgramAssignmentEntity(entity, programEntity, programOfferingEntity, null);

        if (entity.getAssignmentId() == null) {
            session.persist(entity);
        } else {
            session.merge(entity);
        }

        ProgramAssignmentHistoryEntity historyEntity = toProgramAssignmentHistoryEntity(entity);
        if (historyEntity != null) {
            session.save(historyEntity);
        }

        ProgramAssignmentType ret = toProgramAssignmentType(uc, entity);
        return ret;
    }

    @Override
    public ProgramAssignmentType getProgramAssignment(UserContext uc, Long programAssignmentId) {
        ProgramAssignmentEntity programAssignmentEntity = get(programAssignmentId, ProgramAssignmentEntity.class);
        ProgramAssignmentType ret = this.toProgramAssignmentType(uc, programAssignmentEntity);
        return ret;
    }

    @Override
    public ProgramAssignmentType updateProgramAssignment(UserContext uc, ProgramAssignmentType programAssignment) {
        if (programAssignment == null || programAssignment.getAssignmentId() == null) {
            throw new InvalidInputException("Invalid argument: null assignmentId");
        }
        ValidationHelper.validate(programAssignment);
        Long id = programAssignment.getAssignmentId();
        ProgramAssignmentEntity savedEntity = BeanHelper.getEntity(session, ProgramAssignmentEntity.class, id);
        boolean statusChanged = !savedEntity.getStatus().equals(programAssignment.getStatus());
        ProgramEntity programEntity = getProgramEntity(session, programAssignment.getProgram());
        ProgramOfferingEntity programOfferingEntity = getProgramOfferingEntity(session, programAssignment.getProgrammOffering());
        // ProgramAttendanceEntity programAttendanceEntity = getProgramAttendanceEntity(session, programAssignment.getProgramAttendance());

        StampEntity modifyStamp = getModifyStamp(uc, savedEntity.getStamp());
        ProgramAssignmentEntity entity = toProgramAssignmentEntity(programAssignment, modifyStamp);

        if (entity.getStartDate() != null && programOfferingEntity != null && programOfferingEntity.getStartDate() != null) {
            if (entity.getStartDate().compareTo(programOfferingEntity.getStartDate()) < 0) {
                entity.setStartDate(programOfferingEntity.getStartDate());
            }
        }
        if (programOfferingEntity != null && programOfferingEntity.getStartDate() != null) {
            if (entity.getEndDate() != null && programOfferingEntity.getEndDate() != null && entity.getEndDate().compareTo(programOfferingEntity.getEndDate()) > 0) {
                entity.setEndDate(programOfferingEntity.getEndDate());
            }
        }
        if (programOfferingEntity != null && programOfferingEntity.getSingleOccurrence()) {
            entity.setStartDate(programOfferingEntity.getStartDate());
            entity.setEndDate(getDateTime(programOfferingEntity.getEndDate(), programOfferingEntity.getEndTime()));
        }

        entity.setVersion(savedEntity.getVersion());
        ValidationHelper.verifyMetaCodes(entity, false);

        setProgramAssignmentEntity(entity, programEntity, programOfferingEntity, null);
        for (AssigAddDateEntity assigAddDate : savedEntity.getAddedDates()) {
            session.delete(assigAddDate);
        }
        for (AssigSubtractDateEntity assigSubtractDate : savedEntity.getRemovedDates()) {
            session.delete(assigSubtractDate);
        }

        for (SuspensionEntity suspension : savedEntity.getSuspensions()) {
            session.delete(suspension);
        }

        session.merge(entity);

        if (statusChanged) {
            ProgramAssignmentHistoryEntity historyEntity = toProgramAssignmentHistoryEntity(entity);
            if (historyEntity != null) {
                session.save(historyEntity);
            }
        }

        ProgramAssignmentType ret = toProgramAssignmentType(uc, entity);
        return ret;
    }

    @Override
    public ProgramAssignmentReturnType searchProgramAssignment(UserContext uc, Set<Long> programAssignmentIds, ProgramAssignmentSearchType search, Long startIndex,
            Long resultSize, String resultOrder) {

        if (BeanHelper.isEmpty(programAssignmentIds)) {
            ValidationHelper.validateSearchType(search);
        }
        ProgramAssignmentReturnType ret = new ProgramAssignmentReturnType();
        Criteria criteria = session.createCriteria(ProgramAssignmentEntity.class);
        if (!BeanHelper.isEmpty(programAssignmentIds)) {
            Set<Set<Long>> IDs = BeanHelper.splitSet(programAssignmentIds);
            if (IDs.isEmpty() || ((Set<?>) (IDs.toArray()[0])).isEmpty()) {
                ret = new ProgramAssignmentReturnType();
                ret.setProgramAssignments(new ArrayList<ProgramAssignmentType>());
                ret.setTotalSize(0L);
                return ret;
            }

            Disjunction disjunction = Restrictions.disjunction();
            for (Set<Long> ids : IDs) {
                disjunction.add(Restrictions.in("assignmentId", ids));
            }
            criteria.add(disjunction);
        }

        if (search != null) {
            Long supervisionId = search.getSupervisionId();
            if (supervisionId != null) {
                criteria.add(Restrictions.eq("supervisionId", supervisionId));
            }

            Long offeringId = search.getProgramOfferingId();
            if (offeringId != null) {
                criteria.add(Restrictions.eq("programOffering.offeringId", offeringId));
            }

            Long programId = search.getProgramgId();
            if (programId != null) {
                criteria.add(Restrictions.eq("program.programId", programId));
            }

            Set<String> status = search.getStatus();
            if (!BeanHelper.isEmpty(status)) {
                criteria.add(Restrictions.in("status", status));
            }

            Date fromReferralDate = search.getFromReferralDate();
            if (fromReferralDate != null) {
                criteria.add(Restrictions.ge("referralDate", fromReferralDate));
            }
            Date toReferralDate = search.getToReferralDate();
            if (toReferralDate != null) {
                criteria.add(Restrictions.le("referralDate", toReferralDate));
            }

            Date fromStartDate = search.getFromStartDate();
            if (fromReferralDate != null) {
                criteria.add(Restrictions.ge("startDate", fromStartDate));
            }
            Date toStartDate = search.getToStartDate();
            if (toStartDate != null) {
                criteria.add(Restrictions.le("startDate", toStartDate));
            }

            Date fromEndDate = search.getFromEndDate();
            if (fromEndDate != null) {
                criteria.add(Restrictions.ge("endDate", fromEndDate));
            }
            Date toEndDate = search.getToEndDate();
            if (toEndDate != null) {
                criteria.add(Restrictions.le("endDate", toEndDate));
            }

            String referralPriority = search.getReferralPriority();
            if (!BeanHelper.isEmpty(referralPriority)) {
                criteria.add(Restrictions.eq("referralPriority", referralPriority));
            }

            String referralRejectionReason = search.getReferralRejectionReason();
            if (!BeanHelper.isEmpty(referralRejectionReason)) {
                criteria.add(Restrictions.eq("referralRejectionReason", referralRejectionReason));
            }

            String suspendReason = search.getSuspendReason();
            if (!BeanHelper.isEmpty(suspendReason)) {
                criteria.add(Restrictions.eq("suspendReason", suspendReason));
            }

            String abandonReason = search.getAbandonReason();
            if (!BeanHelper.isEmpty(abandonReason)) {
                criteria.add(Restrictions.eq("abandonReason", abandonReason));
            }

            String comments = search.getComments();
            if (!BeanHelper.isEmpty(comments)) {
                criteria.createCriteria("comments", "cmm");
                criteria.add(Restrictions.ilike("cmm.commentText", BeanHelper.strWildcardParser(comments)));
            }

        }

        Long totalSize = (Long) criteria.setProjection(Projections.countDistinct("assignmentId")).uniqueResult();
        criteria.setProjection(null);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());
            criteria.setMaxResults(resultSize.intValue());
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                criteria.addOrder(ord);
            }
        } else {
            criteria.addOrder(Order.desc("referralDate").desc("assignmentId"));
        }

        // Search and only return the predefined max number of records
        criteria.setLockMode(LockMode.NONE);
        criteria.setCacheMode(CacheMode.IGNORE);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        @SuppressWarnings("unchecked") List<ProgramAssignmentEntity> list = criteria.list();
        if (resultSize == null && list.size() > SEARCH_MAX_LIMIT) {
            throw new ArbutusRuntimeException("Search result is over " + SEARCH_MAX_LIMIT);
        }

        List<ProgramAssignmentType> programAssignments = toProgramAssignmentTypeList(uc, list);
        ret.setProgramAssignments(programAssignments);
        ret.setTotalSize(totalSize);
        return ret;
    }

    @Override
    public void deleteProgramAssignment(UserContext uc, Long programAssignmentId) {
        ProgramAssignmentEntity entity = get(programAssignmentId, ProgramAssignmentEntity.class);
        if (entity != null) {
            for (AssigAddDateEntity addDate : entity.getAddedDates()) {
                session.delete(addDate);
            }
            for (AssigSubtractDateEntity subtractDate : entity.getRemovedDates()) {
                session.delete(subtractDate);
            }
            for (SuspensionEntity suspension : entity.getSuspensions()) {
                session.delete(suspension);
            }
            session.delete(entity);
        }
    }

    @Override
    public void abandonProgramAssignmentsForSupervision(UserContext uc, Long supervisionId, Date adanonDate, String comment) {
        ProgramAssignmentSearchType search = new ProgramAssignmentSearchType();
        search.setSupervisionId(supervisionId);
        search.setStatus(new HashSet<>(Arrays.asList(new String[]{APPROVED, REFERRED, ALLOCATED, SUSPENDED})));
        ProgramAssignmentReturnType searchResult = searchProgramAssignment(uc, null, search, null, null, null);
        List<ProgramAssignmentType> assignments = searchResult.getProgramAssignments();
        if (assignments != null) {
            for (ProgramAssignmentType assignment: assignments) {
                assignment.setStatus(ABANDONED);
                CommentType cmn = new CommentType();
                cmn.setComment(comment);
                cmn.setCommentDate(adanonDate);
                assignment.setComments(cmn);
                updateProgramAssignment(uc, assignment);
            }
        }
    }

    @Override
    public ProgramAttendanceType createOrUpdateProgramAttendance(UserContext uc, ProgramAttendanceType programAttendance, ScheduleTimeslotType scheduleTimeslot) {
        String functionName = "createOrUpdateProgramAttendance";
        if (programAttendance.getProgramAssignment() == null || programAttendance.getProgramAssignment().getProgrammOffering() == null) {
            String message = "Invalid Argument: program assignment and program offering are required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ProgramOfferingType offering = programAttendance.getProgramAssignment().getProgrammOffering();
        Set<ScheduleSessionDateType> addedDates = offering.getAddedDates();
        Map<Date, LocalTime[]> mAddedDate = new HashMap<>();
        if (addedDates != null) {
            for (ScheduleSessionDateType a : addedDates) {
                LocalTime[] t = new LocalTime[] { a.getStartTime(), a.getEndTime() };
                mAddedDate.put(a.getDate().toDateTimeAtStartOfDay().toDate(), t);
            }
        }
        Set<ScheduleSessionDateType> removedDates = offering.getRemovedDates();
        Map<Date, LocalTime[]> mRemovedDate = new HashMap<>();
        if (removedDates != null) {
            for (ScheduleSessionDateType s : removedDates) {
                LocalTime[] t = new LocalTime[] { s.getStartTime(), s.getEndTime() };
                mRemovedDate.put(s.getDate().toDateTimeAtStartOfDay().toDate(), t);
            }
        }
        if (scheduleTimeslot != null) {
            Date timeslotStartTime = scheduleTimeslot.getTimeslotStartDateTime();
            Date timeslotEndTime = scheduleTimeslot.getTimeslotEndDateTime();
            Date timeslotStartDate = getDateOnly(timeslotStartTime);
            if (mAddedDate.keySet().contains(timeslotStartDate)) {
                programAttendance.setAttendanceDate(timeslotStartDate);
                programAttendance.setAttendanceStartTime(getZeroEpochTime(mAddedDate.get(timeslotStartDate)[0]));
                programAttendance.setAttendanceEndTime(getZeroEpochTime(mAddedDate.get(timeslotStartDate)[1]));
            } else if (mRemovedDate.keySet().contains(timeslotStartDate)) {
                programAttendance.setAttendanceDate(timeslotStartDate);
                programAttendance.setAttendanceStartTime(getZeroEpochTime(mRemovedDate.get(timeslotStartDate)[0]));
                programAttendance.setAttendanceEndTime(getZeroEpochTime(mRemovedDate.get(timeslotStartDate)[1]));
            } else {
                programAttendance.setAttendanceDate(getDateOnly(timeslotStartDate));
                programAttendance.setAttendanceStartTime(this.getZeroEpochTime(timeslotStartTime));
                programAttendance.setAttendanceEndTime(this.getZeroEpochTime(timeslotEndTime));
            }
        }
        ValidationHelper.validate(programAttendance);
        Long assignmentId = programAttendance.getProgramAssignment().getAssignmentId();
        if (assignmentId == null) {
            String message = "Invalid Argument: assignmentId of Program Attendance is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        if (programAttendance.getAttendanceDate().after(new Date()) || programAttendance.getAttended() == null) {
            return null;
        }
        StampEntity stamp = getCreateStamp(uc);
        ProgramAttendanceEntity entity = toProgramAttendanceEntity(programAttendance, stamp);
        if (programAttendance.getAttendanceId() == null) {
            ValidationHelper.verifyMetaCodes(entity, true);
        } else {
            ValidationHelper.verifyMetaCodes(entity, false);
        }
        ProgramAssignmentEntity programAssignment = this.get(assignmentId, ProgramAssignmentEntity.class);
        ProgramOfferingEntity programOffering = programAssignment.getProgramOffering();
        if (programAssignment.getProgramOffering() == null) {
            return null;
        }

        Long scheduleId = programOffering.getScheduleId();
        if (scheduleId == null && scheduleTimeslot != null) {
            scheduleId = scheduleTimeslot.getScheduleID();
        }

        Long activityId = programAttendance.getActivityId();
        if (activityId != null) {
            ActivityType activity = ActivityServiceAdapter.get(uc, activityId);
            if (activity == null) {
                return null;
            }
            if (activity.getScheduleID() == null) {
                activity.setScheduleID(scheduleId);
            }
            activity.setPlannedStartDate(getDateTime(programAttendance.getAttendanceDate(), programAttendance.getAttendanceStartTime()));
            activity.setPlannedEndDate(getDateTime(programAttendance.getAttendanceDate(), programAttendance.getAttendanceEndTime()));
            ActivityServiceAdapter.updateActivity(uc, activity);
        } else {
            ActivityType activity = new ActivityType();
            activity.setActivityCategory(ActivityCategory.PROGRAM.value());
            activity.setSupervisionId(programAssignment.getSupervisionId());
            activity.setActiveStatusFlag(true);
            activity.setScheduleID(scheduleId);
            activity.setPlannedStartDate(getDateTime(programAttendance.getAttendanceDate(), programAttendance.getAttendanceStartTime()));
            activity.setPlannedEndDate(getDateTime(programAttendance.getAttendanceDate(), programAttendance.getAttendanceEndTime()));
            activity = ActivityServiceAdapter.createActivity(uc, activity);
            activityId = activity.getActivityIdentification();
        }
        entity.setActivityId(activityId);
        ProgramAttendanceType ret = null;
        if (programAttendance.getAttendanceId() == null) {
            programAssignment.addProgramAttendance(entity);
            session.persist(entity);
        } else {
            ProgramAttendanceEntity savedEntity = BeanHelper.getEntity(session, ProgramAttendanceEntity.class, programAttendance.getAttendanceId());
            savedEntity.setAttended(programAttendance.getAttended());
            savedEntity.setPerformance(programAttendance.getPerformance());
            savedEntity.setComments(entity.getComments());
            session.update(savedEntity);
        }
        ret = toProgramAttendanceType(uc, entity);
        ret.setProgramAssignment(programAttendance.getProgramAssignment());
        return ret;
    }

    @Override
    public ProgramAttendanceType getProgramAttendance(UserContext uc, Long programAttendanceId) {
        ProgramAttendanceEntity programAttendanceEntity = get(programAttendanceId, ProgramAttendanceEntity.class);
        if (programAttendanceEntity == null) {
            return null;
        }
        Long assignmentId = programAttendanceEntity.getProgramAssignment().getAssignmentId();
        ProgramAssignmentEntity programAssignmentEntity = this.get(assignmentId, ProgramAssignmentEntity.class);
        ProgramAssignmentType programAssignment = this.toProgramAssignmentType(uc, programAssignmentEntity);
        ProgramAttendanceType programAttendanceType = toProgramAttendanceType(uc, programAttendanceEntity);
        programAttendanceType.setProgramAssignment(programAssignment);
        return programAttendanceType;
    }

    @Override
    public List<ProgramAttendanceType> getProgramAttendances(UserContext uc, Set<Long> supervisionIDs, Long programOfferingId, Long programAssignmentId, Date sessionDate,
            Date startTime, Date endTime) {
        if (BeanHelper.isEmpty(supervisionIDs) && programOfferingId == null && programAssignmentId == null && sessionDate == null && startTime == null
                && endTime == null) {
            return null;
        }

        Criteria c = session.createCriteria(ProgramAttendanceEntity.class);
        c.createCriteria("programAssignment", "assignment");
        if (!BeanHelper.isEmpty(supervisionIDs)) {
            c.add(Restrictions.in("assignment.supervisionId", supervisionIDs));
        }
        if (programOfferingId != null) {
            c.add(Restrictions.eq("assignment.programOffering.offeringId", programOfferingId));
        }
        if (programAssignmentId != null) {
            c.add(Restrictions.eq("assignment.assignmentId", programAssignmentId));
        }
        if (sessionDate != null) {
            c.add(Restrictions.eq("attendanceDate", getDateOnly(sessionDate)));
        }
        if (startTime != null) {
            c.add(Restrictions.eq("attendanceStartTime", getZeroEpochTime(startTime)));
        }
        if (endTime != null) {
            c.add(Restrictions.eq("attendanceEndTime", getZeroEpochTime(endTime)));
        }

        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        c.addOrder(Order.asc("attendanceId"));

        @SuppressWarnings("unchecked") List<ProgramAttendanceEntity> list = c.list();
        return toProgramAttendanceTypeList(uc, list);
    }

    @Override
    public Boolean hasProgramAttendanceRecorded(UserContext uc, Set<Long> supervisionIDs, Long programOfferingId, Long programAssignmentId, Date sessionDate,
            Date startTime, Date endTime) {
        if (BeanHelper.isEmpty(supervisionIDs) && programOfferingId == null && programAssignmentId == null && sessionDate == null && startTime == null
                && endTime == null) {
            return false;
        }

        Criteria c = session.createCriteria(ProgramAttendanceEntity.class);
        c.createCriteria("programAssignment", "assignment");
        if (!BeanHelper.isEmpty(supervisionIDs)) {
            c.add(Restrictions.in("assignment.supervisionId", supervisionIDs));
        }
        if (programOfferingId != null) {
            c.add(Restrictions.eq("assignment.programOffering.offeringId", programOfferingId));
        }
        if (programAssignmentId != null) {
            c.add(Restrictions.eq("assignment.assignmentId", programAssignmentId));
        }
        if (sessionDate != null) {
            c.add(Restrictions.eq("attendanceDate", getDateOnly(sessionDate)));
        }
        if (startTime != null) {
            c.add(Restrictions.eq("attendanceStartTime", getZeroEpochTime(startTime)));
        }
        if (endTime != null) {
            c.add(Restrictions.eq("attendanceEndTime", getZeroEpochTime(endTime)));
        }

        c.setProjection(Projections.rowCount());
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        Long count = (Long) c.uniqueResult();
        return count == null ? false : (count == 0L ? false : true);
    }

    @Override
    public Boolean hasProgramAssignedAlready(UserContext uc, Long supervisionId, Long programId, Set<String> programAssignmentStatus) {
        String functionName = "hasProgramAssignedAlready";
        if (supervisionId == null || programId == null || programAssignmentStatus == null || programAssignmentStatus.isEmpty()) {
            String message = "Invalid Argument: all parameters must be provided.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Criteria criteria = session.createCriteria(ProgramOfferingEntity.class);
        criteria.setProjection(Projections.countDistinct("offeringId"));

        DetachedCriteria assignmentCriteria = DetachedCriteria.forClass(ProgramAssignmentEntity.class);
        assignmentCriteria.createCriteria("programOffering", "offering");
        assignmentCriteria.setProjection(Projections.property("offering.offeringId"));
        assignmentCriteria.add(Restrictions.eq("supervisionId", supervisionId));
        assignmentCriteria.add(Restrictions.in("status", programAssignmentStatus));

        @SuppressWarnings("unchecked") List<Long> offeringIds = assignmentCriteria.getExecutableCriteria(session).list();
        if (offeringIds == null || offeringIds.isEmpty()) {
            return false;
        }
        criteria.add(Restrictions.in("offeringId", offeringIds));

        criteria.createCriteria("program", "p");
        criteria.add(Restrictions.eq("p.programId", programId));

        Long count = (Long) criteria.uniqueResult();
        if (count == null || count == 0L) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public ProgramAttendanceType getProgramAttendanceIdByAssignmentIdAndScheduleTimeslot(UserContext uc, Long programAssignmentId,
            ScheduleTimeslotType scheduleTimeslot) {
        if (programAssignmentId == null || scheduleTimeslot == null) {
            return null;
        }
        if (scheduleTimeslot.getTimeslotStartDateTime() == null || scheduleTimeslot.getTimeslotEndDateTime() == null) {
            return null;
        }

        Criteria c = session.createCriteria(ProgramAttendanceEntity.class);
        c.add(Restrictions.eq("attendanceDate", getDateOnly(scheduleTimeslot.getTimeslotStartDateTime())));
        c.add(Restrictions.or(Restrictions.eq("attendanceStartTime", scheduleTimeslot.getTimeslotStartDateTime()),
                Restrictions.eq("attendanceStartTime", this.getZeroEpochTime(scheduleTimeslot.getTimeslotStartDateTime()))));
        c.add(Restrictions.or(Restrictions.eq("attendanceEndTime", scheduleTimeslot.getTimeslotEndDateTime()),
                Restrictions.eq("attendanceEndTime", this.getZeroEpochTime(scheduleTimeslot.getTimeslotEndDateTime()))));

        c.createCriteria("programAssignment", "assignment");
        c.add(Restrictions.eq("assignment.assignmentId", programAssignmentId));
        c.setMaxResults(1);
        ProgramAttendanceType ret = null;
        if (c.uniqueResult() != null) {
			ret = this.toProgramAttendanceType(uc, (ProgramAttendanceEntity) c.uniqueResult());
		}
        return ret;
    }

    @Override
    public void deleteProgramAttendance(UserContext uc, Long programAttendanceId) {
        ProgramAttendanceEntity entity = get(programAttendanceId, ProgramAttendanceEntity.class);
        if (entity != null) {
            // ProgramAssignmentEntity assignment = entity.getProgramAssignment();
            // assignment.getProgramAttendances().remove(entity);
            session.delete(entity);
        }
    }

    @Override
    public Set<Long> getAllProgram(UserContext uc) {
        return getAll(ProgramEntity.class);
    }

    @Override
    public Set<Long> getAllProgramOffering(UserContext uc) {
        return getAll(ProgramOfferingEntity.class);
    }

    @Override
    public Set<Long> getAllProgramAssignment(UserContext uc) {
        return getAll(ProgramAssignmentEntity.class);
    }

    @Override
    public Set<Long> getAllProgramAttendance(UserContext uc) {
        return getAll(ProgramAttendanceEntity.class);
    }

    @Override
    public void deleteAll(UserContext uc) {
        session.createQuery("DELETE AssigAddDateEntity").executeUpdate();
        session.createQuery("DELETE AssigSubtractDateEntity").executeUpdate();

        session.createSQLQuery("UPDATE PRG_ATTENDANCE SET flag = 1").executeUpdate();
        session.createSQLQuery("DELETE PRG_ATTENDANCE").executeUpdate();

        session.createSQLQuery("UPDATE PRG_ASSIGNMENT SET flag = 1").executeUpdate();
        session.createSQLQuery("DELETE PRG_ASSIGNMENT").executeUpdate();

        session.createSQLQuery("DELETE PRG_ASSIGNMENTHIST").executeUpdate();

        session.createQuery("DELETE AgeGroupEntity").executeUpdate();
        session.createQuery("DELETE AmenityEntity").executeUpdate();
        session.createQuery("DELETE GenderEntity").executeUpdate();
        session.createQuery("DELETE TargetInmateEntity").executeUpdate();

        Set<Long> offeringIds = this.getAllProgramOffering(uc);
        if (offeringIds != null) {
            for (Long offeringId : offeringIds) {
                deleteProgramOffering(uc, offeringId);
            }
        }

        session.createQuery("DELETE AdditionDateEntity").executeUpdate();
        session.createQuery("DELETE SubtractionDateEntity").executeUpdate();
        session.createQuery("DELETE SuspensionEntity").executeUpdate();

        session.createSQLQuery("UPDATE PRG_PRGOFFER SET flag = 1").executeUpdate();
        session.createSQLQuery("DELETE PRG_PRGOFFER").executeUpdate();

        session.createSQLQuery("UPDATE PRG_Program SET flag = 1").executeUpdate();
        session.createSQLQuery("DELETE PRG_Program").executeUpdate();
    }

    @Override
    public boolean isProgramOfferingByOrganization(UserContext uc, Long organizationId) {
        if (!BeanHelper.isEmpty(organizationId)) {
            return false;
        }
        Criteria c = session.createCriteria(ProgramOfferingEntity.class);
        c.add(Restrictions.eq("organizationId", organizationId));
        return !c.list().isEmpty();
    }

    /**
     * To load the set of the initial Reference Codes
     */
    @PostConstruct
    private void init() {
        if (log.isDebugEnabled()) {
			log.debug("init: Begin ... ...");
		}

        ClassLoader loader = this.getClass().getClassLoader();

        try {
            ResourceBundle rb = ResourceBundle.getBundle("service", Locale.getDefault(), loader);
            for (@SuppressWarnings("rawtypes") Enumeration keys = rb.getKeys(); keys.hasMoreElements(); ) {
                String key = (String) keys.nextElement();
                if ("searchMaxLimit".equalsIgnoreCase(key)) {
                    String value = rb.getString(key);
                    SEARCH_MAX_LIMIT = Integer.parseInt(value);
                    return;
                }
            }
        } catch (Exception ex) {
            log.error("init: property file open failure.");
        }

        if (log.isDebugEnabled()) {
			log.debug("init: End ... ...");
		}
    }

    @SuppressWarnings("unchecked")
    private <T extends Object> T get(Long id, Class<T> entityClass) {
        if (id == null) {
            String functionName = "get";
            String message = "Entity Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        return (T) session.get(entityClass, id);
    }

    @SuppressWarnings("unchecked")
    private Set<Long> getAll(Class<?> entityClass) {
        Criteria criteria = session.createCriteria(entityClass);
        criteria.setProjection(Projections.id());
        return new HashSet<Long>(criteria.list());
    }

    private ProgramEntity getProgramEntity(Session session, ProgramType program) {
        ProgramEntity programEntity = null;
        if (program != null) {
            if (program.getProgramId() != null) {
                programEntity = BeanHelper.getEntity(session, ProgramEntity.class, program.getProgramId());
            }
        }
        return programEntity;
    }

    private ProgramOfferingEntity getProgramOfferingEntity(Session session, ProgramOfferingType programOffering) {
        ProgramOfferingEntity programOfferingEntity = null;
        if (programOffering != null) {
            if (programOffering.getOfferingId() != null) {
                programOfferingEntity = BeanHelper.getEntity(session, ProgramOfferingEntity.class, programOffering.getOfferingId());
            }
        }
        return programOfferingEntity;
    }

    private ProgramOfferingEntity getProgramOfferingEntity(Long assignmentId, Long supervisionId) {
        Criteria criteria = session.createCriteria(ProgramOfferingEntity.class);
        criteria.createCriteria("programAssignments", "assignment");
        criteria.add(Restrictions.eq("assignment.assignmentId", assignmentId));
        criteria.add(Restrictions.eq("assignment.supervisionId", supervisionId));

        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.setMaxResults(1);
        return (ProgramOfferingEntity) criteria.uniqueResult();
    }

    private ProgramAttendanceEntity getProgramAttendanceEntity(Session session, ProgramAttendanceType programAttendance) {
        ProgramAttendanceEntity programAttendanceEntity = null;
        if (programAttendance != null) {
            if (programAttendance.getAttendanceId() != null) {
                programAttendanceEntity = BeanHelper.getEntity(session, ProgramAttendanceEntity.class, programAttendance.getAttendanceId());
            }
        }
        return programAttendanceEntity;
    }

    private void setProgramAssignmentEntity(ProgramAssignmentEntity programAssignmentEntity, ProgramEntity programEntity, ProgramOfferingEntity programOfferingEntity,
            ProgramAttendanceEntity programAttendanceEntity) {
        if (programEntity == null) {
            programAssignmentEntity.setProgram(null);
        } else {
            programAssignmentEntity.setProgram(programEntity);
        }

        if (programOfferingEntity == null) {
            programAssignmentEntity.setProgramOffering(null);
        } else {
            programAssignmentEntity.setProgramOffering(programOfferingEntity);
        }

      /*if (programAttendanceEntity == null) {
          programAssignmentEntity.setProgramAttendance(null);
      } else {
      	programAssignmentEntity.setProgramAttendance(programAttendanceEntity);
      }*/
    }

    private void addProgramScheduleForOffering(UserContext uc, ProgramOfferingEntity offering, Set<String> recurranceDays) {
        if (Boolean.TRUE.equals(offering.getGenerateSchedule())) {
            Schedule schedule = new Schedule();
            schedule.setScheduleCategory(ActivityCategory.PROGRAM.name());
            schedule.setFacilityId(offering.getFacilityId());
            Long organizationId = offering.getOrganizationId();
            Long locationId = offering.getLocationId();
            Long internalLocationId = offering.getInteranlLocationId();
            if (internalLocationId != null) {
                schedule.setAtInternalLocationIds(new HashSet<>(Arrays.asList(new Long[] { internalLocationId })));
            } else if (organizationId != null) {
                schedule.setOrganizationIds(new HashSet<>(Arrays.asList(new Long[] { organizationId })));
            } else if (locationId != null) {
                schedule.setLocationIds(new HashSet<>(Arrays.asList(new Long[] { locationId })));
            }

            schedule.setEffectiveDate(offering.getStartDate());
            schedule.setTerminationDate(offering.getEndDate());
            schedule.setScheduleStartTime(timeFormat(offering.getStartTime()));
            schedule.setScheduleEndTime(timeFormat(offering.getEndTime()));

            // Once recurrence pattern
            if (BeanHelper.isEmpty(recurranceDays)) {
                OnceRecurrencePatternType onceRecurrencePattern = new OnceRecurrencePatternType(new LocalDate(offering.getStartDate()).toDateTimeAtStartOfDay().toDate());
                Set<AdditionDateEntity> addedDates = offering.getAddedDates();
                if (addedDates != null && !addedDates.isEmpty()) {
                    Set<ExDateType> additionDates = new HashSet<>();
                    for (AdditionDateEntity addedDate : addedDates) {
                        additionDates.add(toExDateType(addedDate));
                    }
                    onceRecurrencePattern.setIncludeDateTimes(additionDates);
                }
                Set<ModifiedPatternDateEntity> removedDates = offering.getModifiedDates();
                if (removedDates != null && !removedDates.isEmpty()) {
                    Set<ExDateType> subtractionDates = new HashSet<>();
                    for (ModifiedPatternDateEntity subDate : removedDates) {
                        subtractionDates.add(toExDateType(subDate));
                    }
                    onceRecurrencePattern.setExcludeDateTimes(subtractionDates);
                }
                schedule.setRecurrencePattern(onceRecurrencePattern);
            }

            // Daily recurrence pattern
            if (!BeanHelper.isEmpty(recurranceDays)) {
                DailyWeeklyCustomRecurrencePatternType dailyRecurrencePattern = new DailyWeeklyCustomRecurrencePatternType(1L, false, recurranceDays);
                Set<AdditionDateEntity> addedDates = offering.getAddedDates();
                if (addedDates != null && !addedDates.isEmpty()) {
                    Set<ExDateType> additionDates = new HashSet<>();
                    for (AdditionDateEntity addedDate : addedDates) {
                        additionDates.add(toExDateType(addedDate));
                    }
                    dailyRecurrencePattern.setIncludeDateTimes(additionDates);
                }
                Set<ModifiedPatternDateEntity> removedDates = offering.getModifiedDates();
                if (removedDates != null && !removedDates.isEmpty()) {
                    Set<ExDateType> subtractionDates = new HashSet<>();
                    for (ModifiedPatternDateEntity subDate : removedDates) {
                        subtractionDates.add(toExDateType(subDate));
                    }
                    dailyRecurrencePattern.setExcludeDateTimes(subtractionDates);
                }
                schedule.setRecurrencePattern(dailyRecurrencePattern);
            }

            schedule = ActivityServiceAdapter.createSchedule(uc, schedule);

            offering.setScheduleId(schedule.getScheduleIdentification());
        }
    }

    private void updateProgramScheduleForOffering(UserContext uc, ProgramOfferingEntity offering, Set<String> recurranceDays) {
        if (Boolean.TRUE.equals(offering.getGenerateSchedule())) {

            Schedule schedule = null;

            if (offering.getScheduleId() == null) {
                addProgramScheduleForOffering(uc, offering, recurranceDays);
                return;
            }

            schedule = ActivityServiceAdapter.getSchedule(uc, offering.getScheduleId());
            if (schedule == null) {
                addProgramScheduleForOffering(uc, offering, recurranceDays);
                return;
            }

            schedule.setScheduleCategory(ActivityCategory.PROGRAM.name());
            schedule.setFacilityId(offering.getFacilityId());
            Long organizationId = offering.getOrganizationId();
            Long locationId = offering.getLocationId();
            Long internalLocationId = offering.getInteranlLocationId();
            if (internalLocationId != null) {
                schedule.setAtInternalLocationIds(new HashSet<>(Arrays.asList(new Long[] { internalLocationId })));
            } else if (organizationId != null) {
                schedule.setOrganizationIds(new HashSet<>(Arrays.asList(new Long[] { organizationId })));
            } else if (locationId != null) {
                schedule.setLocationIds(new HashSet<>(Arrays.asList(new Long[] { locationId })));
            }

            schedule.setEffectiveDate(offering.getStartDate());
            schedule.setTerminationDate(offering.getEndDate());
            schedule.setScheduleStartTime(timeFormat(offering.getStartTime()));
            schedule.setScheduleEndTime(timeFormat(offering.getEndTime()));

            // Once Recurrence Pattern
            if (BeanHelper.isEmpty(recurranceDays)) {
                OnceRecurrencePatternType onceRecurrencePattern = new OnceRecurrencePatternType(new LocalDate(offering.getStartDate()).toDateTimeAtStartOfDay().toDate());
                Set<AdditionDateEntity> addedDates = offering.getAddedDates();
                if (addedDates != null && !addedDates.isEmpty()) {
                    Set<ExDateType> additionDates = new HashSet<>();
                    for (AdditionDateEntity addedDate : addedDates) {
                        additionDates.add(toExDateType(addedDate));
                    }
                    onceRecurrencePattern.setIncludeDateTimes(additionDates);
                }
                Set<ModifiedPatternDateEntity> removedDates = offering.getModifiedDates();
                if (removedDates != null && !removedDates.isEmpty()) {
                    Set<ExDateType> subtractionDates = new HashSet<>();
                    for (ModifiedPatternDateEntity subDate : removedDates) {
                        subtractionDates.add(toExDateType(subDate));
                    }
                    onceRecurrencePattern.setExcludeDateTimes(subtractionDates);
                }

                Set<ExDateType> aDates = new HashSet<>();
                if (onceRecurrencePattern.getIncludeDateTimes() != null) {
                    aDates = new HashSet<>(onceRecurrencePattern.getIncludeDateTimes());
                }

                Set<ExDateType> rDates = new HashSet<>();
                if (onceRecurrencePattern.getExcludeDateTimes() != null) {
                    rDates = new HashSet<>(onceRecurrencePattern.getExcludeDateTimes());
                }
                if (aDates.retainAll(rDates)) {
                    if (onceRecurrencePattern.getIncludeDateTimes() != null) {
                        onceRecurrencePattern.getIncludeDateTimes().removeAll(aDates);
                    }
                    if (onceRecurrencePattern.getExcludeDateTimes() != null) {
                        onceRecurrencePattern.getExcludeDateTimes().removeAll(aDates);
                    }
                }

                schedule.setRecurrencePattern(onceRecurrencePattern);
            }

            // Daily Recurrence Pattern
            if (!BeanHelper.isEmpty(recurranceDays)) {
                DailyWeeklyCustomRecurrencePatternType dailyRecurrencePattern = new DailyWeeklyCustomRecurrencePatternType(1L, false, recurranceDays);
                Set<AdditionDateEntity> addedDates = offering.getAddedDates();
                if (addedDates != null && !addedDates.isEmpty()) {
                    Set<ExDateType> additionDates = new HashSet<>();
                    for (AdditionDateEntity addedDate : addedDates) {
                        additionDates.add(toExDateType(addedDate));
                    }
                    dailyRecurrencePattern.setIncludeDateTimes(additionDates);
                }
                Set<ModifiedPatternDateEntity> removedDates = offering.getModifiedDates();
                if (removedDates != null && !removedDates.isEmpty()) {
                    Set<ExDateType> subtractionDates = new HashSet<>();
                    for (ModifiedPatternDateEntity subDate : removedDates) {
                        subtractionDates.add(toExDateType(subDate));
                    }
                    dailyRecurrencePattern.setExcludeDateTimes(subtractionDates);
                }

                Set<ExDateType> aDates = new HashSet<>();
                if (dailyRecurrencePattern.getIncludeDateTimes() != null) {
                    aDates = new HashSet<>(dailyRecurrencePattern.getIncludeDateTimes());
                }

                Set<ExDateType> rDates = new HashSet<>();
                if (dailyRecurrencePattern.getExcludeDateTimes() != null) {
                    rDates = new HashSet<>(dailyRecurrencePattern.getExcludeDateTimes());
                }
                if (aDates.retainAll(rDates)) {
                    if (dailyRecurrencePattern.getIncludeDateTimes() != null) {
                        dailyRecurrencePattern.getIncludeDateTimes().removeAll(aDates);
                    }
                    if (dailyRecurrencePattern.getExcludeDateTimes() != null) {
                        dailyRecurrencePattern.getExcludeDateTimes().removeAll(aDates);
                    }
                }

                schedule.setRecurrencePattern(dailyRecurrencePattern);
            }

            schedule = ActivityServiceAdapter.updateSchedule(uc, schedule);
            offering.setScheduleId(schedule.getScheduleIdentification());
        }
    }

    /**
     * *********************************
     * Type Conversion
     * **********************************
     */
    private ProgramEntity toProgramEntity(ProgramType from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        ProgramEntity to = new ProgramEntity();
        to.setProgramId(from.getProgramId());
        to.setProgramCode(from.getProgramCode());
        to.setProgramCategory(from.getProgramCategory());
        to.setProgramDescription(from.getProgramDescription());
        to.setProgramDuration(from.getProgramDuration());
        to.setFlexibleStart(from.getFlexibleStart());
        to.setExpiryDate(from.getExpiryDate());
        to.setTerminationReason(from.getTerminationReason());
        to.setComments(toCommentEntity(from.getComments()));
        to.setProgramCaseWorkStep(from.getProgramCaseWorkStep());
        Set<TargetInmateType> inmates = from.getTargetInmates();
        if (inmates != null) {
            for (TargetInmateType inmate : inmates) {
                to.addTargetInmate(toTargetInmateEntity(inmate, stamp));
            }
        }
        to.setStamp(stamp);

        return to;
    }

    private TargetInmateEntity toTargetInmateEntity(TargetInmateType from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        TargetInmateEntity to = new TargetInmateEntity();

        to.setTargetInmateId(from.getTargetInmateId());

        Set<String> genders = from.getGenders();
        if (!BeanHelper.isEmpty(genders)) {
            for (String g : genders) {
                GenderEntity gender = new GenderEntity();
                gender.setGender(g);
                gender.setStamp(stamp);
                to.addGender(gender);
            }
        }
        Set<String> ageRanges = from.getAgeRanges();
        if (!BeanHelper.isEmpty(ageRanges)) {
            for (String a : ageRanges) {
                AgeGroupEntity ageRange = new AgeGroupEntity();
                ageRange.setAgeRange(a);
                ageRange.setStamp(stamp);
                to.addAgeRange(ageRange);
            }
        }
        Set<String> amenities = from.getAmenities();
        if (!BeanHelper.isEmpty(amenities)) {
            for (String a : amenities) {
                AmenityEntity amenity = new AmenityEntity();
                amenity.setAmenity(a);
                amenity.setStamp(stamp);
                to.addAmenity(amenity);
            }
        }
        to.setStamp(stamp);
        return to;
    }

    private CommentEntity toCommentEntity(CommentType from) {
        if (from == null) {
			return null;
		}

        CommentEntity to = new CommentEntity();
        to.setCommentUserId(from.getUserId());
        to.setCommentText(from.getComment());
        to.setCommentDate(from.getCommentDate());

        return to;
    }

    private CommentType toCommentType(CommentEntity from) {
        if (from == null) {
			return null;
		}

        CommentType to = new CommentType();
        to.setComment(from.getCommentText());
        to.setCommentDate(from.getCommentDate());
        to.setUserId(from.getCommentUserId());

        return to;
    }

    private List<ProgramType> toProgramTypeList(List<ProgramEntity> from) {
        if (from == null) {
			return null;
		}

        List<ProgramType> to = new ArrayList<>();
        for (ProgramEntity frm : from) {
            to.add(toProgramType(frm));
        }
        return to;
    }

    private ProgramType toProgramType(ProgramEntity from) {
        if (from == null) {
			return null;
		}

        ProgramType to = new ProgramType();
        to.setProgramId(from.getProgramId());
        to.setProgramCode(from.getProgramCode());
        to.setProgramCategory(from.getProgramCategory());
        to.setProgramDescription(from.getProgramDescription());
        to.setProgramDuration(from.getProgramDuration());
        to.setTerminationReason(from.getTerminationReason());
        to.setFlexibleStart(from.getFlexibleStart());
        to.setExpiryDate(from.getExpiryDate());
        if (from.getExpiryDate() == null || from.getExpiryDate().compareTo(new Date()) > 0) {
            to.setStatus(ACTIVE);
        } else {
            to.setStatus(INACTIVE);
        }
        to.setComments(toCommentType(from.getComments()));
        to.setTargetInmates(toTargetInmateTypeSet(from.getTargetInmates()));
        to.setProgramCaseWorkStep(from.getProgramCaseWorkStep());
        return to;
    }

    private Set<TargetInmateType> toTargetInmateTypeSet(Set<TargetInmateEntity> from) {
        if (from == null) {
			return null;
		}

        Set<TargetInmateType> to = new HashSet<>();
        for (TargetInmateEntity frm : from) {
            to.add(toTargetInmateType(frm));
        }

        return to;
    }

    private TargetInmateType toTargetInmateType(TargetInmateEntity from) {
        if (from == null) {
			return null;
		}

        TargetInmateType to = new TargetInmateType();

        to.setTargetInmateId(from.getTargetInmateId());

        Set<GenderEntity> genders = from.getGenders();
        for (GenderEntity g : genders) {
            to.getGenders().add(g.getGender());
        }

        Set<AgeGroupEntity> ageRanges = from.getAgeRanges();
        for (AgeGroupEntity a : ageRanges) {
            to.getAgeRanges().add(a.getAgeRange());
        }

        Set<AmenityEntity> amenities = from.getAmenities();
        for (AmenityEntity a : amenities) {
            to.getAmenities().add(a.getAmenity());
        }
        return to;
    }

    private ProgramOfferingEntity toProgramOfferingEntity(ProgramOfferingType from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        ProgramOfferingEntity to = new ProgramOfferingEntity();
        to.setOfferingId(from.getOfferingId());
        to.setOfferingCode(from.getOfferingCode());
        to.setOfferingDescription(from.getOfferingDescription());
        to.setScheduleId(from.getScheduleId());
        to.setStartDate(getDateOnly(from.getStartDate()));
        to.setStartTime(getZeroEpochTime(from.getStartDate()));
        to.setEndDate(getDateOnly(from.getEndDate()));
        to.setEndTime(getZeroEpochTime(from.getEndTime()));
        to.setExpiryDate(from.getExpiryDate());
        to.setCapacity(from.getCapacity());
        to.setVacancy(from.getVacancy());
        to.setParticipationFee(from.getParticipationFee());
        to.setSingleOccurrence(from.getSingleOccurrence());
        to.setGenerateSchedule(from.getGenerateSchedule());
        to.setDay(from.getDay());
        to.setDate(from.getDate());
        to.setLocationDescription(from.getLocationDescription());
        to.setTerminationReason(from.getTerminationReason());
        to.setComments(toCommentEntity(from.getComments()));
        to.setOrganizationId(from.getOrganizationId());
        to.setPersonId(from.getPersonId());
        to.setIsStaff(from.getIsStaff());
        to.setFacilityId(from.getFacilityId());
        to.setLocationId(from.getLocationId());
        to.setInteranlLocationId(from.getInternalLocationId());
        Set<TargetInmateType> inmates = from.getTargetInmates();
        if (inmates != null) {
            for (TargetInmateType inmate : inmates) {
                to.addTargetInmate(toTargetInmateEntity(inmate, stamp));
            }
        }
        Set<ProgramAssignmentType> programAssignments = from.getProgramAssignments();
        if (programAssignments != null) {
            for (ProgramAssignmentType assignment : programAssignments) {
                to.addProgramAssignments(toProgramAssignmentEntity(assignment, stamp));
            }
        }
        Set<ScheduleSessionDateType> addedDates = from.getAddedDates();
        if (addedDates != null) {
            for (ScheduleSessionDateType addedDate : addedDates) {
                to.addAddedDate(toAdditionDateEntity(addedDate, stamp));
            }
        }
        Set<ScheduleSessionDateType> removedDates = from.getRemovedDates();
        if (removedDates != null) {
            for (ScheduleSessionDateType removedDate : removedDates) {
                to.addModifiedDate(toModifiedPatternDateEntity(removedDate, stamp));
            }
        }
        // to.setProgram(toProgramEntity(from.getProgram(), stamp));
        to.setStamp(stamp);

        return to;
    }

    private AdditionDateEntity toAdditionDateEntity(ScheduleSessionDateType from, StampEntity stamp) {
        if (from == null) {
            return null;
        }
        AdditionDateEntity to = new AdditionDateEntity();
        to.setAdditionDateId(from.getEntityId());
        to.setDate(from.getDate().toDateTimeAtStartOfDay().toDate());

        Calendar start = Calendar.getInstance();
        start.clear();
        start.set(Calendar.YEAR, 1970);
        start.set(Calendar.MONTH, Calendar.JANUARY);
        start.set(Calendar.DAY_OF_MONTH, 1);
        if (from.getStartTime() != null) {
            start.set(Calendar.HOUR_OF_DAY, from.getStartTime().getHourOfDay());
            start.set(Calendar.MINUTE, from.getStartTime().getMinuteOfHour());
            start.set(Calendar.SECOND, from.getStartTime().getSecondOfMinute());
            start.set(Calendar.MILLISECOND, 0);
        }
        to.setStartTime(start.getTime());

        Calendar end = Calendar.getInstance();
        end.clear();
        end.set(Calendar.YEAR, 1970);
        end.set(Calendar.MONTH, Calendar.JANUARY);
        end.set(Calendar.DAY_OF_MONTH, 1);
        if (from.getEndTime() != null) {
            end.set(Calendar.HOUR_OF_DAY, from.getEndTime().getHourOfDay());
            end.set(Calendar.MINUTE, from.getEndTime().getMinuteOfHour());
            end.set(Calendar.SECOND, from.getEndTime().getSecondOfMinute());
            end.set(Calendar.MILLISECOND, 0);
        }
        to.setEndTime(end.getTime());
        to.setPreviousTime(from.getPreviousTime());
        to.setPreviousStatus(from.getPreviousStatus());
        to.setStatus(from.getStatus());

        to.setStamp(stamp);
        return to;
    }

    private ModifiedPatternDateEntity toModifiedPatternDateEntity(ScheduleSessionDateType from, StampEntity stamp) {
        if (from == null) {
            return null;
        }
        ModifiedPatternDateEntity to = new ModifiedPatternDateEntity();
        to.setModifiedPatternDateId(from.getEntityId());
        to.setDate(from.getDate().toDateTimeAtStartOfDay().toDate());
        Calendar start = Calendar.getInstance();
        start.clear();
        start.set(Calendar.YEAR, 1970);
        start.set(Calendar.MONTH, Calendar.JANUARY);
        start.set(Calendar.DAY_OF_MONTH, 1);
        if (from.getStartTime() != null) {
            start.set(Calendar.HOUR_OF_DAY, from.getStartTime().getHourOfDay());
            start.set(Calendar.MINUTE, from.getStartTime().getMinuteOfHour());
            start.set(Calendar.SECOND, from.getStartTime().getSecondOfMinute());
            start.set(Calendar.MILLISECOND, 0);
        }
        to.setStartTime(start.getTime());

        Calendar end = Calendar.getInstance();
        end.clear();
        end.set(Calendar.YEAR, 1970);
        end.set(Calendar.MONTH, Calendar.JANUARY);
        end.set(Calendar.DAY_OF_MONTH, 1);
        if (from.getEndTime() != null) {
            end.set(Calendar.HOUR_OF_DAY, from.getEndTime().getHourOfDay());
            end.set(Calendar.MINUTE, from.getEndTime().getMinuteOfHour());
            end.set(Calendar.SECOND, from.getEndTime().getSecondOfMinute());
            end.set(Calendar.MILLISECOND, 0);
        }
        to.setEndTime(end.getTime());
        to.setPreviousTime(from.getPreviousTime());
        to.setPreviousStatus(from.getPreviousStatus());
        to.setStatus(from.getStatus());

        to.setStamp(stamp);
        return to;
    }

    private AssigAddDateEntity toAssigAddDateEntity(ScheduleSessionDateType from, StampEntity stamp) {
        if (from == null) {
            return null;
        }
        AssigAddDateEntity to = new AssigAddDateEntity();
        to.setDate(from.getDate().toDateTimeAtStartOfDay().toDate());

        Calendar start = Calendar.getInstance();
        start.clear();
        start.set(Calendar.YEAR, 1970);
        start.set(Calendar.MONTH, Calendar.JANUARY);
        start.set(Calendar.DAY_OF_MONTH, 1);
        if (from.getStartTime() != null) {
            start.set(Calendar.HOUR_OF_DAY, from.getStartTime().getHourOfDay());
            start.set(Calendar.MINUTE, from.getStartTime().getMinuteOfHour());
            start.set(Calendar.SECOND, from.getStartTime().getSecondOfMinute());
            start.set(Calendar.MILLISECOND, 0);
        }
        to.setStartTime(start.getTime());

        Calendar end = Calendar.getInstance();
        end.clear();
        end.set(Calendar.YEAR, 1970);
        end.set(Calendar.MONTH, Calendar.JANUARY);
        end.set(Calendar.DAY_OF_MONTH, 1);
        if (from.getEndTime() != null) {
            end.set(Calendar.HOUR_OF_DAY, from.getEndTime().getHourOfDay());
            end.set(Calendar.MINUTE, from.getEndTime().getMinuteOfHour());
            end.set(Calendar.SECOND, from.getEndTime().getSecondOfMinute());
            end.set(Calendar.MILLISECOND, 0);
        }
        to.setEndTime(end.getTime());

        to.setStamp(stamp);

        return to;
    }

    private AssigSubtractDateEntity toAssigSubDateEntity(ScheduleSessionDateType from, StampEntity stamp) {
        if (from == null) {
            return null;
        }
        AssigSubtractDateEntity to = new AssigSubtractDateEntity();
        to.setDate(from.getDate().toDateTimeAtStartOfDay().toDate());

        Calendar start = Calendar.getInstance();
        start.clear();
        start.set(Calendar.YEAR, 1970);
        start.set(Calendar.MONTH, Calendar.JANUARY);
        start.set(Calendar.DAY_OF_MONTH, 1);
        if (from.getStartTime() != null) {
            start.set(Calendar.HOUR_OF_DAY, from.getStartTime().getHourOfDay());
            start.set(Calendar.MINUTE, from.getStartTime().getMinuteOfHour());
            start.set(Calendar.SECOND, from.getStartTime().getSecondOfMinute());
            start.set(Calendar.MILLISECOND, 0);
        }
        to.setStartTime(start.getTime());

        Calendar end = Calendar.getInstance();
        end.clear();
        end.set(Calendar.YEAR, 1970);
        end.set(Calendar.MONTH, Calendar.JANUARY);
        end.set(Calendar.DAY_OF_MONTH, 1);
        if (from.getEndTime() != null) {
            end.set(Calendar.HOUR_OF_DAY, from.getEndTime().getHourOfDay());
            end.set(Calendar.MINUTE, from.getEndTime().getMinuteOfHour());
            end.set(Calendar.SECOND, from.getEndTime().getSecondOfMinute());
            end.set(Calendar.MILLISECOND, 0);
        }
        to.setEndTime(end.getTime());

        to.setStamp(stamp);
        return to;
    }

    private List<ProgramOfferingType> toProgramOfferingTypeList(UserContext uc, List<ProgramOfferingEntity> from) {
        if (from == null) {
            return new ArrayList<>();
        }
        List<ProgramOfferingType> to = new ArrayList<>();
        for (ProgramOfferingEntity frm : from) {
            to.add(toProgramOfferingType(uc, frm, true));
        }
        return to;
    }

    private ProgramOfferingType toProgramOfferingType(UserContext uc, ProgramOfferingEntity from, boolean setAssignments) {
        if (from == null) {
			return null;
		}

        ProgramOfferingType to = new ProgramOfferingType();
        to.setOfferingId(from.getOfferingId());
        to.setOfferingCode(from.getOfferingCode());
        to.setOfferingDescription(from.getOfferingDescription());
        to.setStartDate(getDateTime(from.getStartDate(), from.getStartTime()));
        to.setEndDate(getDateTime(from.getEndDate(), from.getEndTime()));
        to.setEndTime(from.getEndTime());
        to.setExpiryDate(from.getExpiryDate());
        if (from.getExpiryDate() == null) {
            Date now = new Date();
            if (from.getEndDate() != null && getDateTime(from.getEndDate(), from.getStartTime()).compareTo(now) < 0) {
                to.setStatus(INACTIVE);
            } else {
                to.setStatus(ACTIVE);
            }
        } else {
            to.setStatus(INACTIVE);
        }
        to.setCapacity(from.getCapacity());
        to.setVacancy(from.getVacancy());
        to.setParticipationFee(from.getParticipationFee());
        to.setSingleOccurrence(from.getSingleOccurrence());
        to.setGenerateSchedule(from.getGenerateSchedule());
        to.setScheduleId(from.getScheduleId());
        if (Boolean.TRUE.equals(from.getGenerateSchedule()) && from.getScheduleId() != null) {
            Schedule schedule = ActivityServiceAdapter.getSchedule(uc, from.getScheduleId());
            if (schedule != null) {
                RecurrencePatternType recurrencePattern = schedule.getRecurrencePattern();
                if (recurrencePattern != null && recurrencePattern instanceof DailyWeeklyCustomRecurrencePatternType) {
                    Set<String> days = ((DailyWeeklyCustomRecurrencePatternType) recurrencePattern).getRecurOnDays();
                    if (!BeanHelper.isEmpty(days)) {
                        to.setRecurranceDays(new HashSet<String>());
                        for (String day : days) {
                            to.getRecurranceDays().add(day);
                        }
                    }
                }
            }
        }
        to.setDay(from.getDay());
        to.setDate(from.getDate());
        to.setLocationDescription(from.getLocationDescription());
        to.setTerminationReason(from.getTerminationReason());
        to.setComments(toCommentType(from.getComments()));
        to.setOrganizationId(from.getOrganizationId());
        to.setPersonId(from.getPersonId());
        to.setIsStaff(from.getIsStaff());
        to.setFacilityId(from.getFacilityId());
        to.setLocationId(from.getLocationId());
        to.setInternalLocationId(from.getInteranlLocationId());
        to.setTargetInmates(toTargetInmateTypeSet(from.getTargetInmates()));
        to.setProgram(toProgramType(from.getProgram()));
        if (setAssignments) {
            Set<ProgramAssignmentEntity> programAssignmentEntitySet = from.getProgramAssignments();
            if (programAssignmentEntitySet != null) {
                Set<ProgramAssignmentType> programAssignmentTypeSet = new HashSet<>();
                for (ProgramAssignmentEntity pa : programAssignmentEntitySet) {
                    programAssignmentTypeSet.add(this.toProgramAssignmentType(uc, pa));
                }
                to.setProgramAssignments(programAssignmentTypeSet);
            }
        }
        to.setAddedDates(toAdditionDateSet(from.getAddedDates()));
        to.setRemovedDates(toSubtractionDateTypeSet(from.getModifiedDates()));

        return to;
    }

    private Set<ScheduleSessionDateType> toAdditionDateSet(Set<AdditionDateEntity> from) {
        if (from == null || from.isEmpty()) {
            return null;
        }
        Set<ScheduleSessionDateType> to = new HashSet<>();
        for (AdditionDateEntity f : from) {
            to.add(toAdditionDateType(f));
        }
        return to;
    }

    private List<ScheduleSessionDateType> toAdditionDateList(List<AdditionDateEntity> from) {
        if (from == null || from.isEmpty()) {
            return null;
        }
        List<ScheduleSessionDateType> to = new ArrayList<>();
        for (AdditionDateEntity f : from) {
            to.add(toAdditionDateType(f));
        }
        return to;
    }

    private ScheduleSessionDateType toAdditionDateType(AdditionDateEntity from) {
        if (from == null) {
            return null;
        }
        ScheduleSessionDateType to = new ScheduleSessionDateType();
        to.setEntityId(from.getAdditionDateId());
        to.setDate(new LocalDate(from.getDate()));
        to.setStartTime(new LocalTime(from.getStartTime()));
        to.setEndTime(new LocalTime(from.getEndTime()));
        to.setPreviousTime(from.getPreviousTime());
        to.setPreviousStatus(from.getPreviousStatus());
        to.setStatus(from.getStatus());
        return to;
    }

    private Set<ScheduleSessionDateType> toSubtractionDateTypeSet(Set<ModifiedPatternDateEntity> from) {
        if (from == null || from.isEmpty()) {
            return null;
        }
        Set<ScheduleSessionDateType> to = new HashSet<>();
        for (ModifiedPatternDateEntity f : from) {
            to.add(toSubtractionDateType(f));
        }
        return to;
    }

    private List<ScheduleSessionDateType> toSubtractionDateTypeList(List<ModifiedPatternDateEntity> from) {
        if (from == null || from.isEmpty()) {
            return null;
        }
        List<ScheduleSessionDateType> to = new ArrayList<>();
        for (ModifiedPatternDateEntity f : from) {
            to.add(toSubtractionDateType(f));
        }
        return to;
    }

    private ScheduleSessionDateType toSubtractionDateType(ModifiedPatternDateEntity from) {
        if (from == null) {
            return null;
        }
        ScheduleSessionDateType to = new ScheduleSessionDateType();
        to.setEntityId(from.getModifiedPatternDateId());
        to.setDate(new LocalDate(from.getDate()));
        to.setStartTime(new LocalTime(from.getStartTime()));
        to.setEndTime(new LocalTime(from.getEndTime()));
        to.setPreviousTime(from.getPreviousTime());
        to.setPreviousStatus(from.getPreviousStatus());
        to.setStatus(from.getStatus());
        return to;
    }

    private Set<ScheduleSessionDateType> toAssignmentAdditionDateTypeSet(Set<AssigAddDateEntity> from) {
        if (from == null || from.isEmpty()) {
            return null;
        }
        Set<ScheduleSessionDateType> to = new HashSet<>();
        for (AssigAddDateEntity f : from) {
            to.add(toAssignmentAdditionDateType(f));
        }
        return to;
    }

    private ScheduleSessionDateType toAssignmentAdditionDateType(AssigAddDateEntity from) {
        if (from == null) {
            return null;
        }
        ScheduleSessionDateType to = new ScheduleSessionDateType();
        to.setDate(new LocalDate(from.getDate()));
        to.setStartTime(new LocalTime(from.getStartTime()));
        to.setEndTime(new LocalTime(from.getEndTime()));
        return to;
    }

    private Set<ScheduleSessionDateType> toAssignmentSubtractDateTypeSet(Set<AssigSubtractDateEntity> from) {
        if (from == null) {
            return null;
        }
        Set<ScheduleSessionDateType> to = new HashSet<>();
        for (AssigSubtractDateEntity f : from) {
            to.add(toAssignmentSubtractDateType(f));
        }
        return to;
    }

    private ScheduleSessionDateType toAssignmentSubtractDateType(AssigSubtractDateEntity from) {
        if (from == null) {
            return null;
        }
        ScheduleSessionDateType to = new ScheduleSessionDateType();
        to.setDate(new LocalDate(from.getDate()));
        to.setStartTime(new LocalTime(from.getStartTime()));
        to.setEndTime(new LocalTime(from.getEndTime()));
        return to;
    }

    private ProgramAssignmentEntity toProgramAssignmentEntity(ProgramAssignmentType from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        ProgramAssignmentEntity to = new ProgramAssignmentEntity();
        to.setAssignmentId(from.getAssignmentId());
        to.setReferralDate(from.getReferralDate());
        to.setReferralPriority(from.getReferralPriority());
        to.setReferralRejectionReason(from.getReferralRejectionReason());
        to.setStartDate(from.getStartDate());
        to.setEndDate(from.getEndDate());
        to.setStatus(from.getStatus());
        to.setSuspendReason(from.getSuspendReason());
        to.setAbandonReason(from.getAbandonReason());
        to.setComments(toCommentEntity(from.getComments()));
        to.setSupervisionId(from.getSupervisionId());
        to.setProcessId(from.getProcessId());
        Set<ScheduleSessionDateType> addedDates = from.getAddedDates();

        to.setProgram(toProgramEntity(from.getProgram(), stamp));
        // to.setProgramOffering(toProgramOfferingEntity(from.getProgrammOffering(), stamp));
        Set<ProgramAttendanceType> programAttendances = from.getProgramAttendances();
        if (!BeanHelper.isEmpty(programAttendances)) {
            for (ProgramAttendanceType attendance : programAttendances) {
                to.addProgramAttendance(toProgramAttendanceEntity(attendance, stamp));
            }
        }
        to.setStamp(stamp);

        if (addedDates != null) {
            for (ScheduleSessionDateType additionDate : addedDates) {
                to.addAddedDate(toAssigAddDateEntity(additionDate, stamp));
            }
        }
        Set<ScheduleSessionDateType> removedDates = from.getRemovedDates();
        if (removedDates != null) {
            for (ScheduleSessionDateType subtractionDate : removedDates) {
                to.addRemovedDate(toAssigSubDateEntity(subtractionDate, stamp));
            }
        }
        Set<SuspensionType> suspensions = from.getSuspensions();
        if (suspensions != null) {
            for (SuspensionType suspension : suspensions) {
                to.addSuspension(toSuspensionEntity(suspension, stamp));
            }
        }

        return to;
    }

    private ProgramAssignmentHistoryEntity toProgramAssignmentHistoryEntity(ProgramAssignmentEntity from) {
        if (from == null) {
            return null;
        }
        ProgramAssignmentHistoryEntity to = new ProgramAssignmentHistoryEntity();
        to.setAssignmentId(from.getAssignmentId());
        to.setReferralDate(from.getReferralDate());
        to.setReferralPriority(from.getReferralPriority());
        to.setReferralRejectionReason(from.getReferralRejectionReason());
        to.setStartDate(from.getStartDate());
        to.setEndDate(from.getEndDate());
        to.setStatus(from.getStatus());
        to.setSuspendReason(from.getSuspendReason());
        to.setAbandonReason(from.getAbandonReason());
        to.setComments(from.getComments());
        to.setSupervisionId(from.getSupervisionId());
        to.setProcessId(from.getProcessId());
        to.setVersion(from.getVersion());
        to.setStamp(from.getStamp());
        return to;
    }

    private SuspensionEntity toSuspensionEntity(SuspensionType from, StampEntity stamp) {
        if (from == null) {
            return null;
        }
        SuspensionEntity to = new SuspensionEntity();
        to.setId(from.getId());
        if (from.getSuspendDate() == null) {
            to.setSuspendDate(new Date());
        } else {
            to.setSuspendDate(from.getSuspendDate());
        }
        to.setResumeDate(from.getResumeDate());
        to.setSuspendReason(from.getSuspendReason());
        to.setComments(this.toCommentEntity(from.getComment()));
        to.setStamp(stamp);
        return to;
    }

    private List<ProgramAssignmentType> toProgramAssignmentTypeList(UserContext uc, List<ProgramAssignmentEntity> from) {
        if (BeanHelper.isEmpty(from)) {
            return null;
        }
        List<ProgramAssignmentType> to = new ArrayList<>();
        for (ProgramAssignmentEntity frm : from) {
            to.add(toProgramAssignmentType(uc, frm));
        }
        return to;
    }

    private ProgramAssignmentType toProgramAssignmentType(UserContext uc, ProgramAssignmentEntity from) {
        if (from == null) {
			return null;
		}

        ProgramAssignmentType to = new ProgramAssignmentType();
        to.setAssignmentId(from.getAssignmentId());
        to.setReferralDate(from.getReferralDate());
        to.setReferralPriority(from.getReferralPriority());
        to.setReferralRejectionReason(from.getReferralRejectionReason());
        to.setStartDate(from.getStartDate());
        to.setEndDate(from.getEndDate());
        to.setStatus(from.getStatus());
        to.setSuspendReason(from.getSuspendReason());
        to.setAbandonReason(from.getAbandonReason());
        to.setComments(toCommentType(from.getComments()));
        if (from.getStamp() != null) {
            to.setModifiedBy(from.getStamp().getModifyUserId());
        }
        to.setSupervisionId(from.getSupervisionId());
        to.setAddedDates(toAssignmentAdditionDateTypeSet(from.getAddedDates()));
        to.setRemovedDates(toAssignmentSubtractDateTypeSet(from.getRemovedDates()));
        to.setProgram(toProgramType(from.getProgram()));
        to.setProgrammOffering(this.toProgramOfferingType(uc, from.getProgramOffering(), false));
        if (to.getProgrammOffering() != null && to.getProgrammOffering().getSingleOccurrence()) {
            ProgramOfferingType offering = to.getProgrammOffering();
            if (to.getStartDate() == null) {
                to.setStartDate(offering.getStartDate());
            }
            if (to.getEndDate() == null) {
                to.setEndDate(this.getDateTime(offering.getEndDate(), offering.getEndTime()));
            }
        }
        to.setProgramAttendances(toProgramAttendanceTypeSet(uc, from.getProgramAttendances()));
        to.setProcessId(from.getProcessId());
        Set<SuspensionEntity> suspensions = from.getSuspensions();
        if (suspensions != null) {
            to.setSuspensions(toSuspensionTypeSet(from.getSuspensions()));
        }

        return to;
    }

    private Set<SuspensionType> toSuspensionTypeSet(Set<SuspensionEntity> from) {
        if (from == null) {
            return null;
        }
        Set<SuspensionType> to = new HashSet<>();
        for (SuspensionEntity suspension : from) {
            to.add(toSuspensionType(suspension));
        }
        return to;
    }

    private SuspensionType toSuspensionType(SuspensionEntity from) {
        if (from == null) {
            return null;
        }
        SuspensionType to = new SuspensionType();
        to.setId(from.getId());
        to.setSuspendDate(from.getSuspendDate());
        to.setResumeDate(from.getResumeDate());
        to.setSuspendReason(from.getSuspendReason());
        to.setComment(toCommentType(from.getComments()));
        return to;
    }

    private ProgramAttendanceEntity toProgramAttendanceEntity(ProgramAttendanceType from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        ProgramAttendanceEntity to = new ProgramAttendanceEntity();
        to.setAttendanceId(from.getAttendanceId());
        to.setAttendanceDate(from.getAttendanceDate());
        to.setAttendanceStartTime(getZeroEpochTime(from.getAttendanceStartTime()));
        to.setAttendanceEndTime(getZeroEpochTime(from.getAttendanceEndTime()));
        to.setActivityId(from.getActivityId());
        to.setAttended(from.getAttended());
        to.setPerformance(from.getPerformance());
        to.setComments(toCommentEntity(from.getComments()));
        to.setProgramAssignment(toProgramAssignmentEntity(from.getProgramAssignment(), stamp));
        to.setStamp(stamp);

        return to;
    }

    private Set<ProgramAttendanceType> toProgramAttendanceTypeSet(UserContext uc, Set<ProgramAttendanceEntity> from) {
        if (from == null) {
			return null;
		}

        Set<ProgramAttendanceType> to = new HashSet<>();
        for (ProgramAttendanceEntity programAttendance : from) {
            to.add(toProgramAttendanceType(uc, programAttendance));
        }
        return to;
    }

    private List<ProgramAttendanceType> toProgramAttendanceTypeList(UserContext uc, List<ProgramAttendanceEntity> from) {
        if (from == null) {
			return null;
		}

        List<ProgramAttendanceType> to = new ArrayList<>();
        for (ProgramAttendanceEntity programAttendance : from) {
            if (!to.contains(programAttendance)) {
                to.add(toProgramAttendanceType(uc, programAttendance));
            }
        }
        return to;
    }

    private ProgramAttendanceType toProgramAttendanceType(UserContext uc, ProgramAttendanceEntity from) {
        if (from == null) {
			return null;
		}

        ProgramAttendanceType to = new ProgramAttendanceType();
        to.setAttendanceId(from.getAttendanceId());
        to.setAttendanceDate(from.getAttendanceDate());
        to.setAttendanceStartTime(getDateTime(from.getAttendanceDate(), from.getAttendanceStartTime()));
        to.setAttendanceEndTime(getDateTime(from.getAttendanceDate(), from.getAttendanceEndTime()));
        to.setActivityId(from.getActivityId());
        to.setAttended(from.getAttended());
        to.setPerformance(from.getPerformance());
        to.setComments(toCommentType(from.getComments()));
        // to.setProgramAssignment(toProgramAssignmentType(uc, from.getProgramAssignment()));
        to.setAssignmentId(from.getProgramAssignment().getAssignmentId());
        return to;
    }

    private ExDateType toExDateType(AdditionDateEntity from) {
        if (from == null) {
            return null;
        }
        ExDateType to = new ExDateType();
        to.setDate(new LocalDate(from.getDate()));
        to.setStartTime(new LocalTime(from.getStartTime()));
        to.setEndTime(new LocalTime(from.getEndTime()));
        return to;
    }

    private ExDateType toExDateType(ModifiedPatternDateEntity from) {
        if (from == null) {
            return null;
        }
        ExDateType to = new ExDateType();
        to.setDate(new LocalDate(from.getDate()));
        to.setStartTime(new LocalTime(from.getStartTime()));
        to.setEndTime(new LocalTime(from.getEndTime()));
        return to;
    }

    private ExDateType toExDateType(ScheduleSessionDateType from) {
        if (from == null) {
            return null;
        }
        ExDateType to = new ExDateType();
        to.setDate(from.getDate());
        to.setStartTime(from.getStartTime());
        to.setEndTime(from.getEndTime());
        return to;
    }

    private Set<ExDateType> toInDateTypeSet(Set<ScheduleSessionDateType> from) {
        if (from == null) {
            return null;
        }
        Set<ExDateType> to = new HashSet<>();
        for (ScheduleSessionDateType frm : from) {
            to.add(toExDateType(frm));
        }
        return to;
    }

    private void validateProgramType(ProgramType program) {
        if (program.getExpiryDate() != null && BeanHelper.isEmpty(program.getTerminationReason())) {
            String functionName = "validateProgramType";
            String message = "Invalid Argument: Termination Reason is required when Expiry Date is provided.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
    }

    private Date getZeroEpochTime(Date dateTime) {
        if (dateTime == null) {
			return null;
		}

        Calendar dt = Calendar.getInstance();
        dt.setTime(dateTime);

        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, 1970);
        c.set(Calendar.MONTH, Calendar.JANUARY);
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, dt.get(Calendar.HOUR_OF_DAY));
        c.set(Calendar.MINUTE, dt.get(Calendar.MINUTE));
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    private Date getZeroEpochTime(LocalTime localTime) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, 1970);
        c.set(Calendar.MONTH, Calendar.JANUARY);
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, localTime.getHourOfDay());
        c.set(Calendar.MINUTE, localTime.getMinuteOfHour());
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    private Date getDateTime(Date date, Date time) {
        if (date == null) {
            return null;
        }
        if (time == null) {
            return date;
        }
        Calendar d = Calendar.getInstance();
        d.setTime(date);
        Calendar t = Calendar.getInstance();
        t.setTime(time);

        d.set(Calendar.HOUR_OF_DAY, t.get(Calendar.HOUR_OF_DAY));
        d.set(Calendar.MINUTE, t.get(Calendar.MINUTE));
        d.set(Calendar.SECOND, 0);
        d.set(Calendar.MILLISECOND, 0);

        return d.getTime();
    }

    private Date getDateOnly(Date dateTime) {
        if (dateTime == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(dateTime);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    private String timeFormat(Date date) {
        if (date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            return sdf.format(date);
        }
        return null;
    }

    private StampEntity getCreateStamp(UserContext uc) {
        if (uc == null) {
            return null;
        }
        return BeanHelper.getCreateStamp(uc, context);
    }

    private StampEntity getModifyStamp(UserContext uc, StampEntity stamp) {
        if (uc == null) {
            return null;
        }
        return BeanHelper.getModifyStamp(uc, context, stamp);
    }

    private void loadBusinessRuleMappings() {
        try {
            codeMapping = BusinessRuleHelper.loadCodeMapping(housingKSession);
            reactionMapping = BusinessRuleHelper.loadOutcomeMapping(housingKSession);
        } catch (Exception ex) {
            ExceptionHelper.handleException(log, ex);
        }
    }

    /**
     * **************************
     * CCF implementation
     * *****************************
     */
    @Override
    public String getCCFMeta(UserContext uc, DTOBindingTypeEnum programModule, String language) {
        return ClientCustomFieldHandler.getCCFMeta(uc, context, session, programModule, language);
    }

    @Override
    public ClientCustomFieldMetaType getClientCustomFieldMeta(UserContext uc, DTOBindingTypeEnum programModule, String name) {
        return ClientCustomFieldHandler.getClientCustomFieldMeta(uc, context, session, programModule, name);
    }

    @Override
    public List<ClientCustomFieldMetaType> getAllCCFMeta(UserContext uc, DTOBindingTypeEnum programModule) {
        return ClientCustomFieldHandler.getAllCCFMeta(uc, context, session, programModule);
    }

    @Override
    public ClientCustomFieldValueType saveCCFMetaValue(UserContext uc, ClientCustomFieldValueType ccfValue) {
        return ClientCustomFieldHandler.saveCCFMetaValue(uc, context, session, ccfValue);
    }

    @Override
    public List<ClientCustomFieldValueType> saveCCFMetaValues(UserContext uc, List<ClientCustomFieldValueType> ccfValues) {
        return ClientCustomFieldHandler.saveCCFMetaValues(uc, context, session, ccfValues);
    }

    @Override
    public List<ClientCustomFieldValueType> getCCFMetaValues(UserContext uc, ClientCustomFieldValueSearchType searchType) {
        return ClientCustomFieldHandler.getCCFMetaValues(uc, context, session, searchType);
    }

    @Override
    public ClientCustomFieldValueType updateCCFMetaValue(UserContext uc, ClientCustomFieldValueType ccfValue) {
        return ClientCustomFieldHandler.updateCCFMetaValue(uc, context, session, ccfValue);
    }

    @Override
    public List<ClientCustomFieldValueType> updateCCFMetaValues(UserContext uc, List<ClientCustomFieldValueType> ccfValues) {
        return ClientCustomFieldHandler.updateCCFMetaValues(uc, context, session, ccfValues);
    }
    /** ***************************
     *  end CCF implementation
     ****************************** */
}