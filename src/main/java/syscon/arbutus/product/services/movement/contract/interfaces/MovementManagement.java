package syscon.arbutus.product.services.movement.contract.interfaces;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.location.contract.dto.InmateMoveSearchType;
import syscon.arbutus.product.services.movement.contract.dto.ConflictType;
import syscon.arbutus.product.services.movement.contract.dto.InternalLocationCountType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.MovementDetailType;
import syscon.arbutus.product.services.movement.contract.dto.OffenderScheduleTransferType;
import syscon.arbutus.product.services.movement.contract.dto.ScheduleConflict;
import syscon.arbutus.product.services.movement.contract.dto.ScheduleConflict.EventType;
import syscon.arbutus.product.services.movement.contract.dto.ScheduleMovementType;
import syscon.arbutus.product.services.movement.contract.dto.ScheduleSupervisionType;
import syscon.arbutus.product.services.movement.contract.dto.ScheduleTransferMovementSearch;
import syscon.arbutus.product.services.movement.contract.dto.TwoMovementsSearchType;
import syscon.arbutus.product.services.movement.contract.dto.TwoMovementsType;

/**
 * Interface of Movement Management Service
 * <p> <b>Purpose</b></p>
 * <p>
 * The MovementManagement service provides the capability to create movement activities and associated activities and
 * associate them to an offenderâ€™s supervision period. This service is required to bridge the gap between a movement
 * and its association to an offender.
 * </p>
 * <p/>
 * <p>
 * To this end, this service interacts with several services within the JMS:
 * <li>Supervision</li>
 * <li>Activity</li>
 * <li>Movement Activity</li>
 * <li>Person Identity</li>
 * <li>Facility Internal Location</li>
 * <li>HousingBedManagementActivity</li>
 * <p/>
 * </p>
 * <p/>
 * <p/>
 * <p><b>Scope</b></p>
 * <p>
 * The following actions are in scope for this service:
 * <p/>
 * <li>Create a single movement activity (internal or external) along with a single activity and associate it to a supervision period</li>
 * <li>Update a single movement activity (internal or external) and/or a single associated activity</li>
 * <li>Create two external/internal movement activities and two associated activities and associate them to a supervision period</li>
 * <li>Update two external/internal movement activities and/or two associated activities</li>
 * <li>Helper actions to retrieve MovementDetails by Movement Activity ID and Activity ID</li>
 * <li>Helper actions to retrieve schedule movement transfer by schedule and movement activity by there search types</li>
 * <p/>
 * <p><b>JNDI Lookup Name:</b> MovementManagementService</p>
 * <p/>
 * <p><b>Example of invocation of the service:</b>
 * <pre>
 * 	Properties p = new Properties();
 * 	properties.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
 * 	properties.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
 * 	properties.put("java.naming.provider.url", hostAddress);  // hostAddress -- specified accordingly
 * 	Context ctx = new InitialContext(p);
 * 	MovementManagementService service = (MovementManagementService)ctx.lookup("MovementManagementService");
 * 	SecurityClient client = SecurityClientFactory.getSecurityClient();
 * 	client.setSimple(userId, password);
 * 	client.login();
 * 	UserContext uc = new UserContext();
 * 	uc.setConsumingApplicationId(clientAppId);
 * 	uc.setFacilityId(facilityId);
 * 	uc.setDeviceId(deviceId);
 * 	uc.setProcess(process);
 * 	uc.setTask(task);
 * 	uc.setTimestamp(timestamp);
 * 	MovementManagementService orgRet = service.createLinkedMovement(uc, ...);
 * 	... ...
 * </pre>
 * <p/>
 * <p>Movement management service class diagram:
 * <br/><img src="doc-files/MovementManagementServiceUML.png" />
 * </p>
 *
 * @author bkahlon, wmadruga
 * @version 1.0 (based on SDD 1.0)
 * @since October 24, 2012
 * <p/>
 * <p>Â© 2012 by Syscon Justice Systems. All rights reserved.</p>
 */
public interface MovementManagement {

    /**
     * Gets version of the service.
     * Returns version of service.
     *
     * @param uc UserContext - Required
     * @return java.lang.Long
     */

    public String getVersion(UserContext uc);

    /**
     * Creates a movement activity record along with an activity record and
     * associates it to an offenders supervision period
     * <b>Provenance</b>
     * <li>At least one of Internal Movement Activity Type or External Movement Activity Type must be provided</li>
     * <li>Activity Type and SupervisionIdentification must be provided</li>
     * <li>The RequiresApproval flag is used when creating the movement activity (either internal or external)</li>
     * <li>A movement activity (either internal or external) and an Activity are created</li>
     * <li>The movement Activity is associated to the Activity</li>
     * <li>The Activity is associated to the Supervision</li>
     *
     * @param uc               UserContext - Required
     * @param movement         External/Internal MovementActivityType - Required
     * @param activity         ActivityType - Required
     * @param supervisionId    Long - Required
     * @param RequiresApproval Boolean - Optional
     * @return MovementDetailType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */

    public MovementDetailType createLinkedMovement(UserContext uc, MovementActivityType movement, ActivityType activity, Long supervisionId, Boolean RequiresApproval);

    /**
     * Updates a movement activity record
     * <b>Provenance</b>
     * <li>At least one of the Internal Movement Activity Type, External Movement Activity Type or Activity Type must be provided</li>
     * <li>The details of a movement activity (either internal or external)</li>
     * <li>The associations for the movement activity or activity are not updated</li>
     *
     * @param uc       UserContext - Required
     * @param movement External/Internal MovementActivityType - Required
     * @return MovementDetailReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    public MovementDetailType updateMovementActivity(UserContext uc, MovementActivityType movement);

    /**
     * Creates two external/internal movement activity records along with two associated activity records which are also associated to each other,
     * and correspondingly associated to an offenderâ€™s supervision period
     * <p/>
     * <p>
     * <p/>
     * <b>Provenance</b>
     * <li>Either Both External or Internal Movement Activity Types must be provided</li>
     * <li>Both Activity Types must be provided</li>
     * <li>SupervisionIdentification must be provided</li>
     * <li>The RequiresApproval flag is used when creating the movement activities (either internal or external)</li>
     * <p/>
     * <li>First set of External/Internal Movement Activity and Activity are created and associated together</li>
     * <li>Second set of External/Internal Movement Activity and Activity are created and associated together</li>
     * <li>The Activity from the first set is (statically) associated to the Activity from the second set</li>
     * </p>
     * <p>Note: The second Activity becomes a descendant of the first Activity. The first Activity becomes an antecedent of the second Activity.</p>
     * <p>Both Activities are associated to the Supervision</p>
     *
     * @param uc               UserContext - Required
     * @param movement1        External/Internal MovementActivityType - Required
     * @param movement2        External/Internal MovementActivityType - Required
     * @param activity1        ActivityType - Required
     * @param activity2        ActivityType - Required
     * @param supervisionId    Long - Required
     * @param RequiresApproval Boolean - Optional
     * @return MovementDetailReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */

    public List<MovementDetailType> createLinkedMovementWithDescendant(UserContext uc, MovementActivityType movement1, MovementActivityType movement2,
            ActivityType activity1, ActivityType activity2, Long supervisionId, Boolean RequiresApproval);

    /**
     * Updates two internal/External movement activity records and/or two activity records.
     * <b>Provenance</b>
     * <li>Either two Movement Activity Types and/or two Activity Types must be provided.</li>
     * <li>If two Activity Types are provided they must already be associated to each other. Otherwise return an error.</li>
     * <li>If two Movement Activity Types are provided then they must both be of the same type ie. either internal or external. Otherwise return an error.</li>
     * <li>If two Movement Activity Types are provided they must also already be associated to each other through their associated Activities. Otherwise return an error.</li>
     * <li>The details of two Internal Movement Activities and/or two Activities are updated.</li>
     * <li>The associations for the Internal Movement Activities or Activities are not updated.</li>
     *
     * @param uc        UserContext - Required
     * @param movement1 External/Internal MovementActivityType - Required
     * @param movement2 External/Internal MovementActivityType - Required
     * @param activity1 ActivityType - Optional
     * @param activity2 ActivityType - Optional
     * @return MovementDetailReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */

    public List<MovementDetailType> updateLinkedMovements(UserContext uc, MovementActivityType movement1, MovementActivityType movement2, ActivityType activity1,
            ActivityType activity2);

    /**
     * Retrieve MovementDetails of an individual for a given Movement ID
     *
     * @param uc         UserContext - Required
     * @param movementId Long - Required
     * @return MovementDetailReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */

    public MovementDetailType getMovementDetailByMovementId(UserContext uc, Long movementId);

    /**
     * Retrieve the external movements
     */

    public List<OffenderScheduleTransferType> getScheduledExternalMovements(UserContext uc, ScheduleTransferMovementSearch search);

    /**
     * Retrieve external Schedule transfer movements for a facility.
     *
     * @param uc     UserContext - Required
     * @param search ScheduleTransferMovementSearch - Required
     * @return TransferMovementReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    public List<OffenderScheduleTransferType> getOffenderScheduleTransferList(UserContext uc, ScheduleTransferMovementSearch search);

    /**
     * Retrieve schedule movement for a facility.
     *
     * @param uc           UserContext - Required
     * @param fromFacility Long - Required
     * @param ParentId     Long - Required
     * @param fromDate     Date - Optional
     * @param toDate       Date - Optional
     * @return ScheduleMovementReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    public List<ScheduleMovementType> getScheduleMovements(UserContext uc, Long fromFacility, Long ParentId, Date fromDate, Date toDate);

    /**
     * Retrieve Internal Location Count for a Facility Internal Location.
     *
     * @param uc       UserContext - Required
     * @param parentId Long - Required
     * @return InternalLocationCountReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    public List<InternalLocationCountType> getInternalLocationCounts(UserContext uc, Long parentId);

    /**
     * Retrieve incoming supervisions by Internal Facility Location.
     *
     * @param uc         UserContext - Required
     * @param toFacility Long - Required
     * @param fromDate   Date - Optional
     * @param toDate     Date - Optional
     * @return ScheduleSupervisionReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    public List<ScheduleSupervisionType> getIncomingSupervisionsByFacility(UserContext uc, Long toFacility, Date fromDate, Date toDate);

    /**
     * Retrieve incoming offenders by facility.
     *
     * @param uc         UserContext - Required
     * @param toFacility Long - Required
     * @return OffenderScheduleTransferReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    public List<OffenderScheduleTransferType> getAdmitIncomingTransfer(UserContext uc, Long toFacility);

    /**
     * Creates schedules with two External Movements considering dates of move and return.
     * Type of movement can be Temporary Absence or Court Movement (not linked to a Case)
     *
     * @param uc        UserContext - Required
     * @param movements T extends @{link TwoMovementsType} - Required
     * @param isAdhoc   Boolean - If it is an ad-hoc or not, Required
     * @return a List of T extends @{link TwoMovementsType}
     */
    public <T extends TwoMovementsType> List<T> createTwoMovements(UserContext uc, List<T> movements, Boolean isAdhoc);

    /**
     * Updates schedules with two External Movements considering dates of move and return.
     * Type of movement can be Temporary Absence or Court Movement (not linked to a Case)
     *
     * @param uc        UserContext - Required
     * @param movements T extends @{link TwoMovementsType} - Required
     * @param isAdhoc   Boolean - If it is an ad-hoc or not, Required
     * @return a List of T extends @{link TwoMovementsType}
     */
    public <T extends TwoMovementsType> List<T> updateTwoMovements(UserContext uc, List<T> movements, Boolean isAdhoc);

    /**
     * Lists TwoMovementsType as TemporaryAbsence and CourtMovement based on searchType.
     *
     * @param uc         UserContext - Required
     * @param searchType @{link TwoMovementsSearchType} - Required
     * @return a List of T extends @{link TwoMovementsType}
     */
    public <T extends TwoMovementsType> List<T> listTwoMovementsBy(UserContext uc, TwoMovementsSearchType searchType);

    /**
     * Checks if there is any movement conflict between given movement and its inmate movements on the same time interval
     *
     * @param uc       UserContext - Required
     * @param movement @{link TwoMovementsType} - Required
     * @return movement @{link TwoMovementsType} - Same object is returned with its conflicts inside
     */
    public TwoMovementsType checkSchedulingConflict(UserContext uc, TwoMovementsType movement);

    /**
     * Determine that a capacity conflict exists when moving offender which exceeds the current capacity of the target location.
     * Applies only to InternalMovementActivityType.
     *
     * @param uc        {@link UserContext}, required
     * @param movement, @{link MovementActivityType} - Required
     * @return {@link syscon.arbutus.product.services.movement.contract.dto.ConflictType}
     */
    public ConflictType checkCapacityConflict(UserContext uc, MovementActivityType movement);

    /**
     * Retrieves Map of schedule conflicts with supervision Id.
     *
     * @param uc
     * @param supervisionIds
     * @param checkTypes
     * @param startDate
     * @param endDate
     * @param language
     * @return
     */
    public List<ScheduleConflict> checkSchedulingConflict(UserContext uc, Set<Long> supervisionIds, 
    		List<ScheduleConflict.EventType> checkTypes, Date startDate, Date endDate, EventType eventType, String language);
    
    /**
     * Retrieves Map of schedule conflicts with supervision Id.
     *
     * @param uc
     * @param inmateMoveSearchTypes
     * @param checkTypes
     * @param language
     * @return
     */
    public Map<Long, List<ScheduleConflict>> checkSchedulingConflicts(UserContext uc, Set<InmateMoveSearchType> inmateMoveSearchTypes,
            List<EventType> checkTypes,EventType targetType, String language);
    
	/**
	 * Retrieves Map of schedule conflicts with key as scheduleConfilct having supervision Id, event id.
	 * @param uc
	 * @param inmateMoveSearchTypes
	 * @param checkTypes
	 * @param language
	 * @return
	 */
	public Map<ScheduleConflict, List<ScheduleConflict>> checkScheduledConflicts(UserContext uc, Set<InmateMoveSearchType> inmateMoveSearchTypes,
			List<EventType> checkTypes,EventType targetType, String language);
	
	/**
     * Retrieve incoming offenders by facility.
     *
     * @param uc         UserContext - Required
     * @param toFacility Long - Required
     * @param filId Long - Required
     * @return OffenderScheduleTransferReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    public List<OffenderScheduleTransferType> getAdmitIncomingTransfer(UserContext uc, Long toFacility, Long filId);
    
    /**
     * Retrieves OffenderScheduleTransferType by facility and supervisionId
     * @param uc
     * @param toFacility
     * @param supervisionId
     * @return
     */
    public OffenderScheduleTransferType getAdmitIncomingTransferBySupervision(UserContext uc, Long toFacility, Long supervisionId,Long filId);
}