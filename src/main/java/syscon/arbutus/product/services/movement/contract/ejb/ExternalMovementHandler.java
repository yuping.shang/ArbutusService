package syscon.arbutus.product.services.movement.contract.ejb;

import java.util.*;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.StaleObjectStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.core.common.adapters.FacilityInternalLocationAdapter;
import syscon.arbutus.product.services.core.common.adapters.FacilityServiceAdapter;
import syscon.arbutus.product.services.core.common.adapters.PersonServiceAdapter;
import syscon.arbutus.product.services.core.common.adapters.SupervisionServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.ArbutusOptimisticLockException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.FacilityInternalLocation;
import syscon.arbutus.product.services.movement.contract.dto.ExternalMovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivitySearchType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.OffenderScheduleTransferType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementServiceLocal;
import syscon.arbutus.product.services.movement.realization.persistence.ExternalMovementActivityEntity;
import syscon.arbutus.product.services.movement.realization.persistence.InternalMovementActivityEntity;
import syscon.arbutus.product.services.movement.realization.persistence.MovementActivityCommentEntity;
import syscon.arbutus.product.services.movement.realization.persistence.MovementActivityEntity;
import syscon.arbutus.product.services.organization.realization.persistence.OrganizationEntity;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * Created by hshen on 1/22/16.
 */
public class ExternalMovementHandler  extends   DefaultMovementHandler{
    private static Logger log = LoggerFactory.getLogger(ExternalMovementHandler.class);

    public ExternalMovementHandler(Session session, MovementServiceLocal movementService, ActivityService activityService) {
        super(session, movementService, activityService);
    }

    @Override
    public Long create(UserContext uc, MovementActivityType movementActivity, Long facilityId) {

        Long result = null;
        Boolean  hasReturn = movementActivity.getPlannedEndDate() == null ? false : true;

        //create out out activity
        ActivityType outActivity = createScheduleActivity(uc, movementActivity.getPlannedStartDate(), facilityId, movementActivity, false, null);

        ActivityType returnActivity =null;

        if(hasReturn){
            returnActivity = createScheduleActivity(uc, movementActivity.getPlannedEndDate(), facilityId, movementActivity, false, null);
            String groupId = UUID.randomUUID().toString();
            movementActivity.setGroupId(groupId);
        }



        ExternalMovementActivityType externalMove = (ExternalMovementActivityType) movementActivity;

        Long fromFacilityId = externalMove.getFromFacilityId();
        Long fromInternalLocationId = externalMove.getFromFacilityInternalLocationId();

        Long toFacilityId = externalMove.getToFacilityId();
        Long toInternalLocationId = externalMove.getToFacilityInternalLocationId();


        externalMove.setMovementDate(null);
        externalMove.setMovementDirection(MovementServiceBean.MovementDirection.OUT.code());
        externalMove.setMovementCategory(MovementServiceBean.MovementCategory.EXTERNAL.code());
        externalMove.setActivityId(outActivity.getActivityIdentification());
        externalMove.setMovementStatus(MovementServiceBean.MovementStatus.PENDING.code());
        result = movementService.create(uc, externalMove, false).getMovementId();


        if(hasReturn){
            externalMove.setMovementDirection(MovementServiceBean.MovementDirection.IN.code());
            externalMove.setMovementCategory(MovementServiceBean.MovementCategory.EXTERNAL.code());
            externalMove.setMovementDate(null);
            externalMove.setActivityId(returnActivity.getActivityIdentification());
            externalMove.setMovementStatus(MovementServiceBean.MovementStatus.PENDING.code());

            externalMove.setFromFacilityId(toFacilityId);
            externalMove.setFromFacilityInternalLocationId(toInternalLocationId);

            externalMove.setToFacilityId(fromFacilityId);
            externalMove.setToFacilityInternalLocationId(fromInternalLocationId);
            movementService.create(uc, movementActivity, false);

        }

        return result;
    }

    @Override
    public Long createAdhoc(UserContext uc, MovementActivityType movementActivity,  Long facilityId) {

        Long result;
        Boolean hasReturn = movementActivity.getPlannedEndDate() == null ? false : true;
        Long outActivityId = createActivity(uc, movementActivity.getMovementDate(), movementActivity.getSupervisionId(), Boolean.FALSE, null);

        ActivityType returnActivity = null;

        if (hasReturn) {
            returnActivity = createScheduleActivity(uc, movementActivity.getPlannedEndDate(), facilityId, movementActivity, false, null);
            String groupId = UUID.randomUUID().toString();
            movementActivity.setGroupId(groupId);
        }

        ExternalMovementActivityType externalMove = (ExternalMovementActivityType) movementActivity;

        Long fromFacilityId = externalMove.getFromFacilityId();
        Long fromInternalLocationId = externalMove.getFromFacilityInternalLocationId();

        Long toFacilityId = externalMove.getToFacilityId();
        Long toInternalLocationId = externalMove.getToFacilityInternalLocationId();
        ;

        externalMove.setMovementDirection(MovementServiceBean.MovementDirection.OUT.code());
        externalMove.setMovementCategory(MovementServiceBean.MovementCategory.EXTERNAL.code());
        externalMove.setActivityId(outActivityId);
        externalMove.setMovementStatus(MovementServiceBean.MovementStatus.COMPLETED.code());
        result = movementService.create(uc, externalMove, false).getMovementId();

        if (hasReturn) {
            externalMove.setMovementDirection(MovementServiceBean.MovementDirection.IN.code());
            externalMove.setMovementCategory(MovementServiceBean.MovementCategory.EXTERNAL.code());
            externalMove.setActivityId(returnActivity.getActivityIdentification());
            externalMove.setMovementStatus(MovementServiceBean.MovementStatus.PENDING.code());

            externalMove.setFromFacilityId(toFacilityId);
            externalMove.setFromFacilityInternalLocationId(toInternalLocationId);

            externalMove.setToFacilityId(fromFacilityId);
            externalMove.setToFacilityInternalLocationId(fromInternalLocationId);

            movementService.create(uc, externalMove, false);
        }

        return result;
    }


    @Override
    public void update(UserContext uc, MovementActivityType movement) {

        String functionName=this.getClass().getName() + "update";

        if (!(movement instanceof ExternalMovementActivityType)){
            String message = "Invalid Type: ExternalMovementActivityType is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);

        }

        ExternalMovementActivityType  movementActivity = (ExternalMovementActivityType)movement;

        Long id = movementActivity.getMovementId();
        ExternalMovementActivityEntity existEntity = BeanHelper.getEntity(session, ExternalMovementActivityEntity.class, id);
        ExternalMovementActivityEntity updatingMovmentActivityEntity = MovementHelper.toMovementActivityEntity(movementActivity, ExternalMovementActivityEntity.class);

        ValidationHelper.verifyMetaCodes(updatingMovmentActivityEntity, false);
        updateMoveActivityStatus(functionName, existEntity, updatingMovmentActivityEntity.getMovementStatus());



        if(MovementServiceBean.MovementDirection.IN.isEquals(existEntity.getMovementDirection()) ) {
            String  groupId = existEntity.getGroupId();
            ExternalMovementActivityEntity partnerMovementEntity = (ExternalMovementActivityEntity)getPartnerMovement(groupId, id);
            if (null != partnerMovementEntity && !MovementServiceBean.MovementStatus.COMPLETED.isEquals(partnerMovementEntity.getMovementStatus())) {

                String message = "Unsupport operation: its OUT movement is in NON-compleated status.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);

            }

            //only allow to update return date/time and comments
            if (!BeanHelper.isEmpty(updatingMovmentActivityEntity.getCommentText())) {
                updateComments(existEntity, updatingMovmentActivityEntity);
            } else {
                updatingMovmentActivityEntity.setCommentText(new HashSet<MovementActivityCommentEntity>());
                updateComments(existEntity, updatingMovmentActivityEntity);

            }

            try {

                session.merge(existEntity);
                //update out Activity
                Long activityId = updatingMovmentActivityEntity.getActivityId();
                updateActivityDate(uc, movementActivity.getPlannedEndDate(), null, activityId);
            }catch(StaleObjectStateException ex){
                throw new ArbutusOptimisticLockException(ex);

            }

            return ;
        }


        if (!BeanHelper.isEmpty(updatingMovmentActivityEntity.getCommentText())) {
            updateComments(existEntity, updatingMovmentActivityEntity);
        } else {
            updatingMovmentActivityEntity.setCommentText(new HashSet<MovementActivityCommentEntity>());
            updateComments(existEntity, updatingMovmentActivityEntity);

        }

         updateExternalMovementProps(existEntity, updatingMovmentActivityEntity);



        try {

            session.merge(existEntity);
            //updateActivity
            Long activityId =  updatingMovmentActivityEntity.getActivityId();
            updateActivityDate(uc, movementActivity.getPlannedStartDate(), null, activityId);


            if (existEntity.getGroupId() != null) {
                //update partner IN movement and activity
                    String hql = "FROM ExternalMovementActivityEntity ma where ma.groupid = '" + existEntity.getGroupId() + "' and ma.movementId != " + existEntity.getMovementId();
                    Query query2 = session.createQuery(hql);
                    @SuppressWarnings("rawtypes") List results = query2.list();
                    if (null != results && !results.isEmpty()) {
                        ExternalMovementActivityEntity inEntity = (ExternalMovementActivityEntity) results.get(0);

                        inEntity.setToFacilityId(movementActivity.getFromFacilityId());
                        inEntity.setToInternalLocationId(movementActivity.getFromFacilityInternalLocationId());

                        inEntity.setFromFacilityId(movementActivity.getToFacilityId());
                        inEntity.setFromInternalLocationId(movementActivity.getFromFacilityInternalLocationId());
                        if (!BeanHelper.isEmpty(updatingMovmentActivityEntity.getCommentText())) {

                            Set<MovementActivityCommentEntity> commentEntities = updatingMovmentActivityEntity.getCommentText();
                            for(MovementActivityCommentEntity commentEntity : commentEntities){
                                commentEntity.setFromIdentifier(inEntity.getMovementId());
                            }

                            updateComments(inEntity, updatingMovmentActivityEntity);
                        } else {
                            updatingMovmentActivityEntity.setCommentText(new HashSet<MovementActivityCommentEntity>());
                            updateComments(inEntity, updatingMovmentActivityEntity);

                        }

                        session.merge(inEntity);
                        updateActivityDate(uc, movementActivity.getPlannedEndDate(), null, inEntity.getActivityId());
                    }


                }
            }catch(StaleObjectStateException ex){
            throw new ArbutusOptimisticLockException(ex);

        }
    }

    @Override
    public List<OffenderScheduleTransferType> search(UserContext uc, MovementActivitySearchType searchType) {
        //only return OUT movement for PENDing movement search
        if(MovementServiceBean.MovementStatus.PENDING.isEquals(searchType.getMovementStatus())){
            searchType.setFilterInOut(true);

        }
        else
        {
            searchType.setFilterInOut(false);


        }


        List<ExternalMovementActivityType> movements=  movementService.search(uc, searchType.getMovementIds(),searchType, 0L, 200L, null).getExternalMovementActivity();

        //Convert to Web form Object
        List<OffenderScheduleTransferType> scheduleList = new ArrayList<OffenderScheduleTransferType>();
        for(ExternalMovementActivityType externalMove :  movements) {
            OffenderScheduleTransferType offender = new OffenderScheduleTransferType();
            offender.setActivityId(externalMove.getActivityId());
            offender.setMovementId(externalMove.getMovementId());
            offender.setGroupId(externalMove.getGroupId());
            offender.setMovementDirection(externalMove.getMovementDirection());
            offender.setbAdhoc(externalMove.isbAdhoc());

            offender.setScheduledTransferDate(externalMove.getPlannedStartDate());
            offender.setScheduledTransferStartTime(externalMove.getPlannedStartDate());
            offender.setScheduledTransferTime(timeFormat.format(externalMove.getPlannedStartDate()));

            offender.setReleaseDate(externalMove.getPlannedStartDate());
            offender.setReleaseTime(externalMove.getPlannedStartDate());

            offender.setReturnDate(externalMove.getPlannedEndDate());
            offender.setReturnTime(externalMove.getPlannedEndDate());
            offender.setMovementDate(externalMove.getMovementDate());
            offender.setMovementType(externalMove.getMovementType());
            offender.setMovementCategory(externalMove.getMovementCategory());

            offender.setFromFacility(externalMove.getFromFacilityId());
            offender.setToFacility(externalMove.getToFacilityId());

            offender.setFromLocation(externalMove.getFromLocationId());

            offender.setToFacilityInternalLocation(externalMove.getToFacilityInternalLocationId());


            if (null != externalMove.getToFacilityInternalLocationId()) {
                FacilityInternalLocation fil = FacilityInternalLocationAdapter.get(uc, externalMove.getToFacilityInternalLocationId());
                if (null != fil) {
                    offender.setToFacilityInternalLocationName(fil.getDescription());
                }
            }

            if (null != externalMove.getToFacilityId()) {
                Facility facilityType = FacilityServiceAdapter.getFacility(uc, externalMove.getToFacilityId());
                if (null != facilityType) {
                    offender.setToFacilityName(facilityType.getFacilityName());
                }
            }

            if (null != externalMove.getFromLocationId()) {
                FacilityInternalLocation fil = FacilityInternalLocationAdapter.get(uc, externalMove.getFromLocationId());
                if (null != fil) {
                    offender.setFromFacilityInternalLocationName(fil.getDescription());

                }

            }

            if (null != externalMove.getFromFacilityId()) {
                Facility facilityType = FacilityServiceAdapter.getFacility(uc, externalMove.getFromFacilityId());
                if (null != facilityType) {
                    offender.setFromFacilityName(facilityType.getFacilityName());
                }
            }


            offender.setMovementReason(externalMove.getMovementReason());
            offender.setCancelReasoncode(externalMove.getCancelReason());
            offender.setMovementStatus(externalMove.getMovementStatus());

            offender.setMovementOutcome(externalMove.getMovementOutcome());
            offender.setParticipateOfficerId(externalMove.getParticipateStaffId());


            offender.setSupervisionId(externalMove.getSupervisionId());
            String strCurrentLocation = SupervisionServiceAdapter.getOffenderCurrentLocationBySupervisionId(uc, externalMove.getSupervisionId());
            offender.setCurrentLocation(strCurrentLocation);

            SupervisionType supervisionType = SupervisionServiceAdapter.get(uc,externalMove.getSupervisionId());
            offender.setSupervisionDisplayId(supervisionType.getSupervisionDisplayID());
            offender.setInmatePersonId(supervisionType.getPersonId());
            offender.setInmateCurrentFacility(supervisionType.getFacilityId());



            Long personIdentityId = supervisionType.getPersonIdentityId();
            PersonIdentityType pi = PersonServiceAdapter.getPersonIdentityType(uc, personIdentityId, null);
            offender.setFirstName(pi.getFirstName());
            offender.setLastName(pi.getLastName());
            offender.setOffenderNumber(pi.getOffenderNumber());
            offender.setPersonIdentifierId(pi.getPersonIdentityId());




            Set<CommentType> commentTypes = externalMove.getCommentText();
            if (null != commentTypes && commentTypes.size() > 0) {
                for (CommentType c : commentTypes) {
                    offender.setComment(c.getComment());
                }
            }


            scheduleList.add(offender);


        }

        return scheduleList;
    }




}
