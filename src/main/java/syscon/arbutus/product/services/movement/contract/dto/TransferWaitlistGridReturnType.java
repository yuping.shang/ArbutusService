package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.List;

public class TransferWaitlistGridReturnType implements Serializable {

    private List<TransferWaitlistGridEntryType> transferWaitlists;

    private Long totalSize;

    public TransferWaitlistGridReturnType() {
    }

    public TransferWaitlistGridReturnType(List<TransferWaitlistGridEntryType> transferWaitlists, Long totalSize) {
        this.transferWaitlists = transferWaitlists;
        this.totalSize = totalSize;
    }

    public List<TransferWaitlistGridEntryType> getTransferWaitlists() {
        return transferWaitlists;
    }

    public void setTransferWaitlists(List<TransferWaitlistGridEntryType> transferWaitlists) {
        this.transferWaitlists = transferWaitlists;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }
}
