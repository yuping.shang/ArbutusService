package syscon.arbutus.product.services.movement.contract.interfaces;

/**
 * Created by jliang on 9/24/15.
 */

import javax.ejb.Local;

/**
 * Created by jliang on 9/24/15.
 */
@Local
public interface MovementServiceLocal extends MovementServiceCommon {
}
