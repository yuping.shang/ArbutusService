package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;

import syscon.arbutus.product.services.core.common.StampInterface;
import syscon.arbutus.product.services.core.contract.dto.BaseDto;

public class LastCompletedSupervisionMovementType extends BaseDto implements Serializable {

    private static final long serialVersionUID = -2183572011286140610L;

    /**
     * Association to an offender’s period of supervision.
     * Static reference to the Supervision service.
     */
    Long supervisionId;

    /**
     * The external or internal movement last completed for an offender
     */
    MovementActivityType lastCompletedMovementActivity;

    /**
     * Association to an offender’s current internal location (if applicable).
     * Static reference to the Facility Internal Location service.
     */
    Long currentInternalLocationId;
    
    
    /**
     * current location description of inmate
     */
    private String currentLocation;

    /**
     * Constructor
     */
    public LastCompletedSupervisionMovementType() {
        super();
    }

    /**
     * Constructor
     *
     * @param supervisionId                 Long. Id of an offender’s period of supervision refer to the Supervision service.
     * @param lastCompletedMovementActivity MovementActivityType. The external or internal movement last completed for an offender.
     * @param currentInternalLocationId     Long. Id of an offender’s current internal location (if applicable) refer to the Facility Internal Location service.
     */
    public LastCompletedSupervisionMovementType(Long supervisionId, MovementActivityType lastCompletedMovementActivity, Long currentInternalLocationId,
            StampInterface stamp, Long version) {
        super();
        this.supervisionId = supervisionId;
        this.lastCompletedMovementActivity = lastCompletedMovementActivity;
        this.currentInternalLocationId = currentInternalLocationId;
        this.setStamp(stamp);
        this.setVersion(version);
    }

    /**
     * Get Supervision Id
     * <p>Id of an offender’s period of supervision refer to the Supervision service
     *
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * Set Supervision Id
     * <p>Id of an offender’s period of supervision refer to the Supervision service
     *
     * @param supervisionId the supervisionAssociation to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Get Last Completed MovementActivity
     * <p>The external or internal movement last completed for an offender
     *
     * @return the lastCompletedMovementActivity
     */
    public MovementActivityType getLastCompletedMovementActivity() {
        return lastCompletedMovementActivity;
    }

    /**
     * Set Last Completed MovementActivity
     * <p>The external or internal movement last completed for an offender
     *
     * @param lastCompletedMovementActivity the lastCompletedMovementActivity to set
     */
    public void setLastCompletedMovementActivity(MovementActivityType lastCompletedMovementActivity) {
        this.lastCompletedMovementActivity = lastCompletedMovementActivity;
    }


    /**
     * Get Current Internal Location Id
     * <p>Id of an offender’s current internal location (if applicable) refer to the Facility Internal Location service
     *
     * @return the currentInternalLocationId
     */
    public Long getCurrentInternalLocationId() {
        return currentInternalLocationId;
    }

    /**
     * Set Current Internal Location Id
     * <p>Id of an offender’s current internal location (if applicable) refer to the Facility Internal Location service
     *
     * @param currentInternalLocationId the currentInternalLocationAssociation to set
     */
    public void setCurrentInternalLocationId(Long currentInternalLocationId) {
        this.currentInternalLocationId = currentInternalLocationId;
    }
    
    /**
     * Get Current Internal Location Description
     * @return currentLocation , the description of current location
     */
    public String getCurrentLocation() {
        return currentLocation;
    }

    /**
     * Set Current Internal Location Description
     * 
     * @param currentLocation , the description of current location
     */
    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((currentInternalLocationId == null) ? 0 : currentInternalLocationId.hashCode());
        result = prime * result + ((lastCompletedMovementActivity == null) ? 0 : lastCompletedMovementActivity.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LastCompletedSupervisionMovementType other = (LastCompletedSupervisionMovementType) obj;
        if (currentInternalLocationId == null) {
            if (other.currentInternalLocationId != null) {
                return false;
            }
        } else if (!currentInternalLocationId.equals(other.currentInternalLocationId)) {
            return false;
        }
        if (lastCompletedMovementActivity == null) {
            if (other.lastCompletedMovementActivity != null) {
                return false;
            }
        } else if (!lastCompletedMovementActivity.equals(other.lastCompletedMovementActivity)) {
            return false;
        }
        if (supervisionId == null) {
            if (other.supervisionId != null) {
                return false;
            }
        } else if (!supervisionId.equals(other.supervisionId)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "LastCompletedSupervisionMovementType [supervisionId=" + supervisionId + ", lastCompletedMovementActivity=" + lastCompletedMovementActivity
                + ", currentInternalLocationId=" + currentInternalLocationId + "]";
    }

}
