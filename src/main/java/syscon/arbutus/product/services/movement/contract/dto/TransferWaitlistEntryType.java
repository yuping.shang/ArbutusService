package syscon.arbutus.product.services.movement.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.core.common.StampInterface;
import syscon.arbutus.product.services.core.contract.dto.BaseDto;

/**
 * TransferWaitlistEntryType for MovementActivity Service
 *
 * @author yshang
 * @version 2.0
 * @since October 25, 2012
 */
public class TransferWaitlistEntryType extends BaseDto implements Serializable {

    private static final long serialVersionUID = 7941203488838104577L;
    @NotNull
    private Long supervisionId;
    @NotNull
    private Date dateAdded;
    @NotNull
    private Long toFacilityId;
    @NotNull
    private String transferReason;
    @NotNull
    private String priority;

    public TransferWaitlistEntryType() {
        super();
    }

    /**
     * Constructor
     *
     * @param supervisionId  Long, required -- Identification of the transfer waitlist entry
     * @param dateAdded      Date, required -- Date an offender was added to the waitlist.
     * @param toFacilityId   Long, required -- The facility that the offender is to be transferred to.
     * @param transferReason String, required. -- The reason for the transfer, reference code of MovementReason set.
     * @param priority       String, required -- The priority of each offender on the waitlist, reference code of TransferWaitlistPriority set.
     */
    public TransferWaitlistEntryType(Long supervisionId, Date dateAdded, Long toFacilityId, String transferReason, String priority, StampInterface stamp, Long version) {
        super();
        this.supervisionId = supervisionId;
        this.dateAdded = dateAdded;
        this.toFacilityId = toFacilityId;
        this.transferReason = transferReason;
        this.priority = priority;
        this.setStamp(stamp);
        this.setVersion(version);
    }

    /**
     * The offender who are awaiting transfers.
     * Static association to the Supervision service.
     * <p>Required for both create and update
     *
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * The offender who are awaiting transfers.
     * Static association to the Supervision service.
     * <p>Required for both create and update
     *
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Date an offender was added to the waitlist, required
     *
     * @return the dateAdded
     */
    public Date getDateAdded() {
        return dateAdded;
    }

    /**
     * Date an offender was added to the waitlist, required
     *
     * @param dateAdded the dateAdded to set
     */
    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    /**
     * The facility that the offender is to be transferred to.
     * Static association to the Facility service, required
     *
     * @return the toFacilityId
     */
    public Long getToFacilityId() {
        return toFacilityId;
    }

    /**
     * The facility that the offender is to be transferred to.
     * Static association to the Facility service, required
     *
     * @param toFacilityId the toFacilityId to set
     */
    public void setToFacilityId(Long toFacilityId) {
        this.toFacilityId = toFacilityId;
    }

    /**
     * The reason for the transfer, reference code of MovementReason set.
     * <p>Required.
     *
     * @return the transferReason
     */
    public String getTransferReason() {
        return transferReason;
    }

    /**
     * The reason for the transfer, reference code of MovementReason set.
     * <p>Required.
     *
     * @param transferReason the transferReason to set
     */
    public void setTransferReason(String transferReason) {
        this.transferReason = transferReason;
    }

    /**
     * The priority of each offender on the waitlist, reference code of TransferWaitlistPriority set.
     * <p>Required.
     *
     * @return the priority
     */
    public String getPriority() {
        return priority;
    }

    /**
     * The priority of each offender on the waitlist, reference code of TransferWaitlistPriority set.
     * <p>Required.
     *
     * @param priority the priority to set
     */
    public void setPriority(String priority) {
        this.priority = priority;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateAdded == null) ? 0 : dateAdded.hashCode());
        result = prime * result + ((priority == null) ? 0 : priority.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        result = prime * result + ((toFacilityId == null) ? 0 : toFacilityId.hashCode());
        result = prime * result + ((transferReason == null) ? 0 : transferReason.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TransferWaitlistEntryType other = (TransferWaitlistEntryType) obj;
        if (dateAdded == null) {
            if (other.dateAdded != null) {
                return false;
            }
        } else if (!dateAdded.equals(other.dateAdded)) {
            return false;
        }
        if (priority == null) {
            if (other.priority != null) {
                return false;
            }
        } else if (!priority.equals(other.priority)) {
            return false;
        }
        if (supervisionId == null) {
            if (other.supervisionId != null) {
                return false;
            }
        } else if (!supervisionId.equals(other.supervisionId)) {
            return false;
        }
        if (toFacilityId == null) {
            if (other.toFacilityId != null) {
                return false;
            }
        } else if (!toFacilityId.equals(other.toFacilityId)) {
            return false;
        }
        if (transferReason == null) {
            if (other.transferReason != null) {
                return false;
            }
        } else if (!transferReason.equals(other.transferReason)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TransferWaitlistEntryType [supervisionId=" + supervisionId + ", dateAdded=" + dateAdded + ", toFacilityId=" + toFacilityId + ", transferReason="
                + transferReason + ", priority=" + priority + "]";
    }

}
