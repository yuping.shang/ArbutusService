package syscon.arbutus.product.services.movement.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Created on 5/28/14.
 *
 * @author hshen
 */
public class LastKnowLocationGridEntryType implements Serializable {
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;

    @NotNull
    private String offenderNumber; //supervision displayId
    @NotNull
    private String gender;
    @NotNull
    private Date dateOfBirth;
    @NotNull
    private Long personId;
    @NotNull
    private Long supervisionId;
    @NotNull
    private Long facilityId;
    @NotNull
    private Long internalLocationId;

    public LastKnowLocationGridEntryType() {
    }

    public LastKnowLocationGridEntryType(String firstName, String lastName, String offenderNumber, String gender, Date dateOfBirth, Long personId, Long supervisionId,
            Long facilityId, Long internalLocationId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.offenderNumber = offenderNumber;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.personId = personId;
        this.supervisionId = supervisionId;
        this.facilityId = facilityId;
        this.internalLocationId = internalLocationId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOffenderNumber() {
        return offenderNumber;
    }

    public void setOffenderNumber(String offenderNumber) {
        this.offenderNumber = offenderNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getSupervisionId() {
        return supervisionId;
    }

    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    public Long getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    public Long getInternalLocationId() {
        return internalLocationId;
    }

    public void setInternalLocationId(Long internalLocationId) {
        this.internalLocationId = internalLocationId;
    }
}
