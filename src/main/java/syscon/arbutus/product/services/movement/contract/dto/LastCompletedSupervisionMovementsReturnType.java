package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.List;

public class LastCompletedSupervisionMovementsReturnType implements Serializable {

    private static final long serialVersionUID = -2183572011286140613L;

    private List<LastCompletedSupervisionMovementType> lastCompletedSupervisionMovements;

    private Long totalSize;

    /**
     * Get a set of Last Completed MovementActivity --
     * The external or internal movement last completed for an offender.
     *
     * @return the lastCompletedSupervisionMovements
     */
    public List<LastCompletedSupervisionMovementType> getLastCompletedSupervisionMovements() {
        return lastCompletedSupervisionMovements;
    }

    /**
     * Set a set of Last Completed MovementActivity --
     * The external or internal movement last completed for an offender.
     *
     * @param lastCompletedSupervisionMovements the lastCompletedSupervisionMovements to set
     */
    public void setLastCompletedSupervisionMovements(List<LastCompletedSupervisionMovementType> lastCompletedSupervisionMovements) {
        this.lastCompletedSupervisionMovements = lastCompletedSupervisionMovements;
    }

    /**
     * @return the totalSize
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * @param totalSize the totalSize to set
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "LastCompletedSupervisionMovementsReturnType [lastCompletedSupervisionMovements=" + lastCompletedSupervisionMovements + ", totalSize=" + totalSize + "]";
    }

}
