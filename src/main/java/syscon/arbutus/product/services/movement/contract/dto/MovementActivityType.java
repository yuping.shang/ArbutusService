package syscon.arbutus.product.services.movement.contract.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.BaseDto;
import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * Movement Activity Type
 *
 * @author yshang
 * @version 2.0 (based on Movement Activity 2.0 SDD)
 * @author ashish.kumar
 * @since October 19, 2012.
 */

public class MovementActivityType extends BaseDto implements Serializable {

    private static final long serialVersionUID = 7941203488838104576L;
    /**
     * Unique identification for the movement activity
     */
    private Long movementId;
    /**
     * Id of an offender’s period of supervision.
     * <p>Static reference to a Supervision.
     * <p>Required
     */
    @NotNull
    private Long supervisionId;
    /**
     * Category for the Movement (EXTERNAL, INTERNAL), required. Reference code of MovementCategory set.
     */
    @NotNull
    private String movementCategory;
    /**
     * The type of movement (APP, VISIT etc.,), required. Reference code of MovementType set.
     */
    @NotNull
    private String movementType;


    @NotNull
    private String movementDirection;
    /**
     * The status of the movement, required. -- PENDING, ONHOLD, COMPLETED, CANCELLED, NOSHOW, APPREQ, DENIED
     */
    @NotNull
    private String movementStatus;
    /**
     * The reason for the Movement, required
     */
    @NotNull
    private String movementReason;
    /**
     * Outcome of a movement, not required
     */
    private String movementOutcome;
    /**
     * Actual movement date, not required
     */
    private Date movementDate;
    /**
     * Set of Comments associated to the movement, not required
     */
    @Valid
    private Set<CommentType> commentText;
    /**
     * Indicates the staff member who approved the Temporary Absence movement.
     * Static association to the Staff service.
     */
    private Long approvedByStaffId;
    /**
     * The date the approval of the Temporary Absence move was given, not required
     */
    private Date approvalDate;
    /**
     * Any comments provided by the approver, not required
     */
    private String approvalComments;
    /**
     * Intermittent Scheduled movement associated intermittent scheduled entity id
     */
    private Long intermittentSenScheduleId;

    private Long activityId;

    private String groupId;

    private Long escapeId;


    private Long inmatePersonId;

    private String supervisionDisplayId;


    /**
     * The date of the activity start, not required
     */

    private Date plannedStartDate;

    /**
     * The date of the activity end, not required
     */
    private Date plannedEndDate;



    /**
     * The reason why the transfer was cancelled.
     */
    private String cancelReason;

    /**
     * The staff to attend the appointment
     */
    private  Long  participateStaffId;

    /**
     * indicate if movement is created in adhoc or scheduled
     */

    private  boolean bAdhoc;


    /**
     * Constructor
     */
    public MovementActivityType() {
        super();
    }

    public boolean isbAdhoc() {
        return bAdhoc;
    }

    public void setbAdhoc(boolean bAdhoc) {
        this.bAdhoc = bAdhoc;
    }

    public Long getParticipateStaffId() {
        return participateStaffId;
    }

    public void setParticipateStaffId(Long participateStaffId) {
        this.participateStaffId = participateStaffId;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public Date getPlannedStartDate() {
        return plannedStartDate;
    }

    public void setPlannedStartDate(Date plannedStartDate) {
        this.plannedStartDate = plannedStartDate;
    }

    public Date getPlannedEndDate() {
        return plannedEndDate;
    }

    public void setPlannedEndDate(Date plannedEndDate) {
        this.plannedEndDate = plannedEndDate;
    }

    /**
     * Unique identification for the movement activity system generated.
     * <p>Ignored when create; Required when update
     *
     * @return the movementIdentification
     */
    public Long getMovementId() {
        return movementId;
    }

    /**
     * Unique identification for the movement activity system generated.
     * <p>Ignored when create; Required when update
     *
     * @param movementId the movementIdentification to set
     */
    public void setMovementIdentification(Long movementId) {
        this.movementId = movementId;
    }

    /**
     * Id of an offender’s period of supervision, refer to a Supervision, required
     *
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * Id of an offender’s period of supervision, refer to a Supervision, required
     *
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Category for the Movement (EXTERNAL, INTERNAL), reference code of MovementCategory set, required
     *
     * @return the movementCategory
     */
    public String getMovementCategory() {
        return movementCategory;
    }

    /**
     * Category for the Movement (EXTERNAL, INTERNAL), reference code of MovementCategory set, required
     *
     * @param movementCategory the movementCategory to set
     */
    public void setMovementCategory(String movementCategory) {
        this.movementCategory = movementCategory;
    }

    /**
     * The type of movement (APP, VISIT etc.,), reference code of MovementType Set, required
     *
     * @return the movementType
     */
    public String getMovementType() {
        return movementType;
    }

    /**
     * The type of movement (APP, VISIT etc.,), reference code of MovementType Set, required
     *
     * @param movementType the movementType to set
     */
    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    /**
     * Movement Direction: IN or OUT, reference code of MovementDirection set, required
     *
     * @return the movementDirection
     */
    public String getMovementDirection() {
        return movementDirection;
    }

    /**
     * Movement Direction: IN or OUT, reference code of MovementDirection set, required
     *
     * @param movementDirection the movementDirection to set
     */
    public void setMovementDirection(String movementDirection) {
        this.movementDirection = movementDirection;
    }

    /**
     * The status of the movement, reference code of MovementStatus set, required. -- PENDING, ONHOLD, COMPLETED, CANCELLED, NOSHOW, APPREQ, DENIED
     *
     * @return the movementStatus
     */
    public String getMovementStatus() {
        return movementStatus;
    }

    /**
     * The status of the movement, reference code of MovementStatus set, required. -- PENDING, ONHOLD, COMPLETED, CANCELLED, NOSHOW, APPREQ, DENIED
     *
     * @param movementStatus the movementStatus to set
     */
    public void setMovementStatus(String movementStatus) {
        this.movementStatus = movementStatus;
    }

    /**
     * The reason for the Movement, reference code of MovementReason set, required
     *
     * @return the movementReason
     */
    public String getMovementReason() {
        return movementReason;
    }

    /**
     * The reason for the Movement, reference code of MovementReason set, required
     *
     * @param movementReason the movementReason to set
     */
    public void setMovementReason(String movementReason) {
        this.movementReason = movementReason;
    }

    /**
     * Outcome of a movement, reference code of MovementOutcome set, optional. -- EXCUSED, NOT EXCUSED, SICK
     *
     * @return the movementOutcome
     */
    public String getMovementOutcome() {
        return movementOutcome;
    }

    /**
     * Outcome of a movement, reference code of MovementOutcome set, optional. -- EXCUSED, NOT EXCUSED, SICK
     *
     * @param movementOutcome the movementOutcome to set
     */
    public void setMovementOutcome(String movementOutcome) {
        this.movementOutcome = movementOutcome;
    }

    /**
     * Actual movement date, optional
     *
     * @return the movementDate
     */
    public Date getMovementDate() {
        return movementDate;
    }

    /**
     * Actual movement date, optional
     *
     * @param movementDate the movementDate to set
     */
    public void setMovementDate(Date movementDate) {
        this.movementDate = movementDate;
    }

    /**
     * Set of Comments associated to the movement, optional
     *
     * @return the commentText
     */
    public Set<CommentType> getCommentText() {
        if (commentText == null) {
            commentText = new HashSet<CommentType>();
        }
        return commentText;
    }

    /**
     * Set of Comments associated to the movement, optional
     *
     * @param commentText the commentText to set
     */
    public void setCommentText(Set<CommentType> commentText) {
        this.commentText = commentText;
    }

    /**
     * Id of Staff, Static association to the Staff service, optional.
     * <p>Indicates the staff member who approved the Temporary Absence movement.
     *
     * @return the approvedByStaffId
     */
    public Long getApprovedByStaffId() {
        return approvedByStaffId;
    }

    /**
     * Id of Staff, Static association to the Staff service, optional.
     * <p>Indicates the staff member who approved the Temporary Absence movement.
     *
     * @param approvedByStaffId the approvedByStaffId to set
     */
    public void setApprovedByStaffId(Long approvedByStaffId) {
        this.approvedByStaffId = approvedByStaffId;
    }

    /**
     * The date the approval of the Temporary Absence move was given, optional
     *
     * @return the approvalDate
     */
    public Date getApprovalDate() {
        return approvalDate;
    }

    /**
     * The date the approval of the Temporary Absence move was given, optional
     *
     * @param approvalDate the approvalDate to set
     */
    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    /**
     * Any comments provided by the approver, optional
     *
     * @return the approvalComments
     */
    public String getApprovalComments() {
        return approvalComments;
    }

    /**
     * Any comments provided by the approver, optional
     *
     * @param approvalComments the approvalComments to set
     */
    public void setApprovalComments(String approvalComments) {
        this.approvalComments = approvalComments;
    }

    /**
     * Sets associated Intermittent Scheduled entity id
     *
     * @return
     */
    public Long getIntermittentSenScheduleId() {
        return intermittentSenScheduleId;
    }

    /**
     * Returns associated Intermittent scheduled entity id
     *
     * @param intermittentSenScheduleId
     */
    public void setIntermittentSenScheduleId(Long intermittentSenScheduleId) {
        this.intermittentSenScheduleId = intermittentSenScheduleId;
    }

    /**
     * Id of an Activity, Reference To Activity, required
     *
     * @return the activityId
     */
    public Long getActivityId() {
        return activityId;
    }

    /**
     * Id of an Activity, Reference To Activity, required
     *
     * @param activityId the activityId to set
     */
    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    /**
     * @return the groupId
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     *
     * @return
     */
    public Long getEscapeId() {
        return escapeId;
    }

    /**
     *
     * @param escapeId
     */
    public void setEscapeId(Long escapeId) {
        this.escapeId = escapeId;
    }

    /**
     * @return
     */
    public Long getInmatePersonId() {
        return inmatePersonId;
    }

    /**
     * @param inmatePersonId
     */
    public void setInmatePersonId(Long inmatePersonId) {
        this.inmatePersonId = inmatePersonId;
    }

    /**
     * @return
     */
    public String getSupervisionDisplayId() {
        return supervisionDisplayId;
    }

    /**
     * @param supervisionDisplayId
     */
    public void setSupervisionDisplayId(String supervisionDisplayId) {
        this.supervisionDisplayId = supervisionDisplayId;
    }

    /**
     * Category for Movement (EXTERNAL, INTERNAL)
     */
    public static enum MovementCategory {

        /**
         * Movement Category External
         */
        EXTERNAL,

        /**
         * Movement Category Internal
         */
        INTERNAL,

        /**
         * Movement Category No Movement
         */
        NOMOV;

        public static MovementCategory fromValue(String v) {
            return valueOf(v);
        }

        public String value() {
            return name();
        }

    }

    /**
     * Enumeration of Movement Type
     */
    public enum MovementType {
        ADM, CRT, APP, TAP, TRNOJ, TRNIJ, WR, REL, VISIT, OIC, PROG, BED, MED, CLA, WEEK, ESCP, APPO;

        public String code() {
            return name();
        }
    }

    /**
     * Enumeration of Movement Direction
     */
    public enum MovementDirection {
        OUT, IN;

        public String code() {
            return name();
        }
    }

    /**
     * Enumeration of Movement Status
     */
    public enum MovementStatus {
        PENDING, ONHOLD, COMPLETED, CANCELLED, NOSHOW, APPREQ, DENIED;

        public String code() {
            return name();
        }
    }

}