package syscon.arbutus.product.services.movement.contract.interfaces;

import javax.ejb.Remote;

/**
 * Created by jliang on 9/24/15.
 */
@Remote
public interface MovementService extends MovementServiceCommon {

}