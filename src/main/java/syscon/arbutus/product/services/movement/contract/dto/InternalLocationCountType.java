package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.Set;

/**
 * Definition of Internal location count
 *
 * @author bkahlon
 * @version 1.0
 * @since January 24, 2012
 */
public class InternalLocationCountType implements Serializable {

    private static final long serialVersionUID = -3981555676749703890L;

    private Long locationId;
    private String locationCode;

    private Long optimalCapacity = 0L;
    private Long overflowCapacity;
    private Set<Long> supervisions;
    private Long optimalAvailable;
    private Long overflowAvailable;

    /**
     *
     */
    public InternalLocationCountType() {

    }

    /**
     * Gets the value of the locationId property.
     */
    public Long getLocationId() {
        return locationId;
    }

    /**
     * Sets the value of the locationId property.
     */
    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    /**
     * Gets the value of the locationCode property.
     */
    public String getLocationCode() {
        return locationCode;
    }

    /**
     * Sets the value of the locationCode property.
     */
    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    /**
     * Gets the value of the optimalCapacity property.
     */
    public Long getOptimalCapacity() {
        return optimalCapacity;
    }

    /**
     * Sets the value of the optimalCapacity property.
     */
    public void setOptimalCapacity(Long optimalCapacity) {
        this.optimalCapacity = optimalCapacity;
    }

    /**
     * Gets the value of the overflowCapacity property.
     */
    public Long getOverflowCapacity() {
        return overflowCapacity;
    }

    /**
     * Sets the value of the overflowCapacity property.
     */
    public void setOverflowCapacity(Long overflowCapacity) {
        this.overflowCapacity = overflowCapacity;
    }

    /**
     * Gets the value of the optimalAvailable property.
     */
    public Long getOptimalAvailable() {
        return optimalAvailable;
    }

    /**
     * Sets the value of the optimalAvailable property.
     */
    public void setOptimalAvailable(Long optimalAvailable) {
        this.optimalAvailable = optimalAvailable;
    }

    /**
     * Gets the value of the overflowAvailable property.
     */
    public Long getOverflowAvailable() {
        return overflowAvailable;
    }

    /**
     * Sets the value of the overflowAvailable property.
     */
    public void setOverflowAvailable(Long overflowAvailable) {
        this.overflowAvailable = overflowAvailable;
    }

    /**
     * Gets the value of the supervisions property.
     */
    public Set<Long> getSupervisions() {
        return supervisions;
    }

    /**
     * Sets the value of the supervisions property.
     */
    public void setSupervisions(Set<Long> supervisions) {
        this.supervisions = supervisions;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "InternalLocationCountType [locationId=" + locationId + ", locationCode=" + locationCode + ", optimalCapacity=" + optimalCapacity + ", overflowCapacity="
                + overflowCapacity + ", supervisions=" + supervisions + ", optimalAvailable=" + optimalAvailable + ", overflowAvailable=" + overflowAvailable + "]";
    }

}
