package syscon.arbutus.product.services.movement.contract.dto;

import java.util.Date;

/**
 * dto class to represent a external movement
 */
public class ExternalMovement extends InternalMovement {
    private static final String NAME_FORMAT = "%s, %s";
    private Long id;
    private Long escortOrg;
    private String escortOrgStr;
    private String moveReasonCd;
    private String escortDetails;
    private Boolean outofJurisdiction;
    private Date expectedArrivalDate;
    private Date expectedArrivalTime;
    private String toProvinceState;
    private Long fromFacility;
    private String fromFacilityStr;

    private String priority;
    private Date dateAdded;
    private String transferReason;
    private String[] genders;
    private String name;
    private Date dateOfBirth;
    private Integer age;
    private String sex;
    private String sexStr;

    private Long toFacility;
    private String toFacilityStr;
    private String housingLocation;
    private String[] housingNeeds;
    private String[] ageRanges;
    private String[] cautions;
    private String moveStatus;
    private Long assocedActivityId;
    private Boolean isAdhoc;
    private String moveType;

    public ExternalMovement() {
        super();
    }

    public String getAdhocMode() {
        return MoveEnum.ADHOC.getValue();
    }

    public String getScheduleMode() {
        return MoveEnum.SCHEDULE.getValue();
    }

    public Long getEscortOrg() {
        return escortOrg;
    }

    public void setEscortOrg(Long escortOrg) {
        this.escortOrg = escortOrg;
    }

    public String getMoveReasonCd() {
        return moveReasonCd;
    }

    public void setMoveReasonCd(String moveReasonCd) {
        this.moveReasonCd = moveReasonCd;
    }

    public String getEscortDetails() {
        return escortDetails;
    }

    public void setEscortDetails(String escortDetails) {
        this.escortDetails = escortDetails;
    }

    public Boolean isOutofJurisdiction() {
        return outofJurisdiction;
    }

    public void setOutofJurisdiction(Boolean outofJurisdiction) {
        this.outofJurisdiction = outofJurisdiction;
    }

    public Date getExpectedArrivalDate() {
        return expectedArrivalDate;
    }

    public void setExpectedArrivalDate(Date expectedArrivalDate) {
        this.expectedArrivalDate = expectedArrivalDate;
    }

    public Long getToFacility() {
        return toFacility;
    }

    public void setToFacility(Long toFacility) {
        this.toFacility = toFacility;
    }

    public String getToProvinceState() {
        return toProvinceState;
    }

    public void setToProvinceState(String toProvinceState) {
        this.toProvinceState = toProvinceState;
    }

    public Long getFromFacility() {
        return fromFacility;
    }

    public void setFromFacility(Long fromFacility) {
        this.fromFacility = fromFacility;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String code) {
        this.priority = code;

    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getTransferReason() {
        return transferReason;
    }

    public void setTransferReason(String code) {
        this.transferReason = code;

    }

    public String getDisplayName() {
        // TODO: get format from property file
        return String.format(NAME_FORMAT, getLastName().trim(), getFirstName().trim());

    }

    public Date getExpectedArrivalTime() {
        return expectedArrivalTime;
    }

    public void setExpectedArrivalTime(Date expectedArrivalTime) {
        this.expectedArrivalTime = expectedArrivalTime;
    }

    /**
     * @return the toFacilityStr
     */
    public String getToFacilityStr() {
        return toFacilityStr;
    }

    /**
     * @param toFacilityStr the toFacilityStr to set
     */
    public void setToFacilityStr(String toFacilityStr) {
        this.toFacilityStr = toFacilityStr;
    }

    public String getHousingLocation() {
        return housingLocation;
    }

    public void setHousingLocation(String housingLocation) {
        this.housingLocation = housingLocation;
    }

    /**
     * @return the gender
     */
    public String[] getGenders() {
        return genders;
    }

    /**
     * @param gender the gender to set
     */
    public void setGenders(String[] gender) {
        this.genders = gender;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the dateOfBirth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String[] getHousingNeeds() {
        return housingNeeds;
    }

    public void setHousingNeeds(String[] housingNeeds) {
        this.housingNeeds = housingNeeds;
    }

    public String[] getAgeRanges() {
        return ageRanges;
    }

    public void setAgeRanges(String[] ageRanges) {
        this.ageRanges = ageRanges;
    }

    public String[] getCautions() {
        return cautions;
    }

    public void setCautions(String[] cautions) {
        this.cautions = cautions;
    }

    /**
     * @return the sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * @param sex the sex to set
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getEscortOrgStr() {
        return escortOrgStr;
    }

    public void setEscortOrgStr(String escortOrgStr) {
        this.escortOrgStr = escortOrgStr;
    }

    /**
     * @return the moveStatus
     */
    public String getMoveStatus() {
        return moveStatus;
    }

    /**
     * @param moveStatus the moveStatus to set
     */
    public void setMoveStatus(String moveStatus) {
        this.moveStatus = moveStatus;
    }

    public String getFromFacilityStr() {
        return fromFacilityStr;
    }

    public void setFromFacilityStr(String fromFacilityStr) {
        this.fromFacilityStr = fromFacilityStr;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAssocedActivityId() {
        return assocedActivityId;
    }

    public void setAssocedActivityId(Long assocedActivityId) {
        this.assocedActivityId = assocedActivityId;
    }

    public String getSexStr() {
        return sexStr;
    }

    public void setSexStr(String sexStr) {
        this.sexStr = sexStr;
    }

    public Boolean getIsAdhoc() {
        return isAdhoc;
    }

    public void setIsAdhoc(Boolean isAdhoc) {
        this.isAdhoc = isAdhoc;
    }

    public String getMoveType() {
        return moveType;
    }

    public void setMoveType(String moveType) {
        this.moveType = moveType;
    }

    public enum MoveEnum {

        ADHOC("adhoc"), SCHEDULE("schedule");

        private String value;

        MoveEnum(String type) {
            value = type;
        }

        public String getValue() {
            return value;
        }

    }

}
