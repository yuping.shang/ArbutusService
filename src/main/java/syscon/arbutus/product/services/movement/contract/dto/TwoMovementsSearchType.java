package syscon.arbutus.product.services.movement.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Search type for TwoMovementsType (Temporary Absence / Court Movements)
 * If dates are set, movements will be retrieved between the time frame set by moveDate and returnDate.
 *
 * @author wmadruga
 */
public class TwoMovementsSearchType implements Serializable {

    private static final long serialVersionUID = -2434892538852733266L;

    @NotNull
    private String movementType;
    //ReferenceSet(MOVEMENTTYPE) {TAP, CRT, IMA} figure out how to validate (annotated) if it is one of them.

    private Long fromFacilityId;
    private Long toFacilityId;

    private Long offenderId;
    private String groupId;

    private Date moveDate;
    private Date returnDate;

    private String statusIN;
    private String statusOUT;

    /**
     * Gets the value of the movementType property. Required
     *
     * @return the movementType
     */
    public String getMovementType() {
        return movementType;
    }

    /**
     * Sets the value of the movementType property. Required
     *
     * @param movementType String
     */
    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    /**
     * Gets the value of the fromFacilityId property. Optional if other id property passed.
     *
     * @return the fromFacilityId
     */
    public Long getFromFacilityId() {
        return fromFacilityId;
    }

    /**
     * Sets the value of the fromFacilityId property.  Optional if other id property passed.
     *
     * @param fromFacilityId Long
     */
    public void setFromFacilityId(Long fromFacilityId) {
        this.fromFacilityId = fromFacilityId;
    }

    /**
     * Gets the value of the toFacilityId property. Optional if other id property passed.
     *
     * @return the toFacilityId
     */
    public Long getToFacilityId() {
        return toFacilityId;
    }

    /**
     * Sets the value of the toFacilityId property.  Optional if other id property passed.
     *
     * @param toFacilityId Long
     */
    public void setToFacilityId(Long toFacilityId) {
        this.toFacilityId = toFacilityId;
    }

    /**
     * Gets the value of the offenderId property.  Optional if other id property passed.
     *
     * @return the offenderId
     */
    public Long getOffenderId() {
        return offenderId;
    }

    /**
     * Sets the value of the offenderId property.  Optional if other id property passed.
     *
     * @param offenderId Long
     */
    public void setOffenderId(Long offenderId) {
        this.offenderId = offenderId;
    }

    /**
     * Gets the value of the groupId property. Optional
     *
     * @return the groupId
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Sets the value of groupId property. Optional
     *
     * @param groupId, String
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * Gets the value of the moveDate property. Optional
     *
     * @return the moveDate
     */
    public Date getMoveDate() {
        return moveDate;
    }

    /**
     * Sets the value of moveDate property. Optional
     *
     * @param moveDate, Date
     */
    public void setMoveDate(Date moveDate) {
        this.moveDate = moveDate;
    }

    /**
     * Gets the value of the returnDate property. Optional
     *
     * @return the returnDate
     */
    public Date getReturnDate() {
        return returnDate;
    }

    /**
     * Sets the value of returnDate property. Optional
     *
     * @param returnDate, Date
     */
    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    /**
     * Gets the value of the movement IN status. Optional
     *
     * @return the statusIN
     */
    public String getStatusIN() {
        return statusIN;
    }

    /**
     * Sets the value of movement IN status. Optional
     *
     * @param statusIN, String
     */
    public void setStatusIN(String statusIN) {
        this.statusIN = statusIN;
    }

    /**
     * Gets the value of the movement OUT status. Optional
     *
     * @return the statusOUT
     */
    public String getStatusOUT() {
        return statusOUT;
    }

    /**
     * Sets the value of movement OUT status. Optional
     *
     * @param statusOUT, String
     */
    public void setStatusOUT(String statusOUT) {
        this.statusOUT = statusOUT;
    }

}