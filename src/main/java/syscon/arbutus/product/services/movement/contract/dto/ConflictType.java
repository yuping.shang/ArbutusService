package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;

/**
 * The representation of a conflict
 */
public class ConflictType implements Serializable {

    private static final long serialVersionUID = 4184685531128308181L;

    private String checkType;

    private String description;

    public ConflictType() {
        super();
    }

    /**
     * @param checkType,   required
     * @param description, required
     */
    public ConflictType(String checkType, String description) {
        super();
        this.checkType = checkType;
        this.description = description;
    }

    /**
     * Gets the value of the checkType property
     *
     * @return String
     */
    public String getCheckType() {
        return checkType;
    }

    /**
     * Sets the value of the checkType property
     */
    public void setCheckType(String checkType) {
        this.checkType = checkType;
    }

    /**
     * Gets the value of the description property
     *
     * @return String
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property
     */
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ConflictType [checkType=" + checkType + ", description=" + description + "]";
    }

}