package syscon.arbutus.product.services.movement.realization.util;

import java.util.Date;

import org.hibernate.Query;
import org.hibernate.Session;
import syscon.arbutus.product.services.movement.contract.dto.TwoMovementsSearchType;
import syscon.arbutus.product.services.movement.contract.dto.TwoMovementsType;
import syscon.arbutus.product.services.movement.realization.util.interfaces.MovementsHQLStrategy;

/**
 * Movement HQL Strategy implementation
 * HQL query to get Visitations and return as a TwoMovementsType
 *
 * @author wmadruga
 */
public class VisitationHQL implements MovementsHQLStrategy {

    public Query buildTwoMovementsHQL(Session session, TwoMovementsSearchType searchType, Date dateOut, Date dateIn) {

        StringBuilder hqlFormat = new StringBuilder();

        hqlFormat.append("SELECT new %s(v.visitId, v.startTime, v.endTime, m.movementStatus, m.movementStatus, m.movementType, v.locationId, m.movementCategory) ");
        hqlFormat.append("FROM VisitEntity v, ActivityEntity a, MovementActivityEntity m ");

        hqlFormat.append("WHERE v.activityId = a.activityId ");
        hqlFormat.append("AND m.activityId IN ");
        hqlFormat.append("(SELECT activityId FROM ActivityEntity WHERE antecedentId = v.activityId) ");

        hqlFormat.append("AND m.supervisionId = :supId ");

        //		if(dateOut != null && dateIn != null) {
        //			hqlFormat.append("AND v.startTime >= :dateOut AND v.endTime <= :dateIn ");
        //		}

        hqlFormat.append("AND v.status = 'SCHEDULED'");

        String hql = null;
        hql = String.format(hqlFormat.toString(), TwoMovementsType.class.getName());
        Query query = session.createQuery(hql);

        //		if(dateOut != null && dateIn != null) {
        //			query.setTimestamp("dateOut", dateOut).setTimestamp("dateIn", dateIn);
        //		}
        query.setLong("supId", searchType.getOffenderId());
        return query;
    }

}