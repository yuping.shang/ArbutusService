package syscon.arbutus.product.services.movement.contract.ejb;

public interface Constants {
    public static final String MOVEMENT_CATEGORY_EXTERNAL = "EXTERNAL";
    public static final String MOVEMENT_CATEGORY_INTERNAL = "INTERNAL";
    public static final String MOVEMENT_STATUS_COMPLETE = "COMPLETED";
    public static final String ACTIVITY_CATEGORY_MOVEMENT = "MOVEMENT";

}
