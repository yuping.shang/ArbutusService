package syscon.arbutus.product.services.movement.contract.ejb;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.PersistenceContext;
import java.lang.Boolean;
import java.lang.String;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.StaleObjectStateException;
import org.hibernate.criterion.*;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.kie.api.cdi.KReleaseId;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ActivityCategory;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.activity.contract.dto.Schedule;
import syscon.arbutus.product.services.activity.contract.ejb.ActivityServiceHelper;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.activity.realization.persistence.ActivityEntity;
import syscon.arbutus.product.services.appointment.contract.interfaces.AppointmentService;
import syscon.arbutus.product.services.conflict.contract.dto.NonAssociationType;
import syscon.arbutus.product.services.conflict.contract.ejb.ConflictHandler;
import syscon.arbutus.product.services.core.common.adapters.*;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.ReferenceCodeType;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.*;
import syscon.arbutus.product.services.core.util.DateUtil;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.CapacityType;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.FacilityInternalLocation;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.InternalLocationIdentifierType;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.LocationCategory;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationServiceLocal;
import syscon.arbutus.product.services.housing.contract.dto.housingassignment.UnassignOffenderType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingAssignmentType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.LocationHousingAssignmentType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.OffenderHousingAssignmentType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.OffenderHousingAssignmentsReturnType;
import syscon.arbutus.product.services.housing.contract.ejb.HousingBedManagementActivityHandler;
import syscon.arbutus.product.services.housing.contract.interfaces.HousingServiceLocal;
import syscon.arbutus.product.services.location.contract.dto.InmateMoveSearchType;
import syscon.arbutus.product.services.location.contract.dto.Location;
import syscon.arbutus.product.services.movement.contract.dto.*;
import syscon.arbutus.product.services.movement.contract.dto.ReferenceSet;
import syscon.arbutus.product.services.movement.contract.dto.ScheduleConflict.EventType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementServiceLocal;
import syscon.arbutus.product.services.movement.realization.persistence.*;
import syscon.arbutus.product.services.movement.realization.util.CourtMovementHQL;
import syscon.arbutus.product.services.movement.realization.util.MovementsHQLContext;
import syscon.arbutus.product.services.movement.realization.util.TemporaryAbsenceHQL;
import syscon.arbutus.product.services.movement.realization.util.VisitationHQL;
import syscon.arbutus.product.services.organization.contract.dto.Organization;
import syscon.arbutus.product.services.organization.contract.interfaces.OrganizationService;
import syscon.arbutus.product.services.organization.realization.persistence.OrganizationEntity;
import syscon.arbutus.product.services.person.contract.dto.PersonType;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonServiceLocal;
import syscon.arbutus.product.services.person.realization.persistence.personidentity.PersonIdentityEntity;
import syscon.arbutus.product.services.program.contract.interfaces.ProgramService;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.referencedata.contract.interfaces.ReferenceDataService;
import syscon.arbutus.product.services.registry.contract.dto.FacilitySetType;
import syscon.arbutus.product.services.reservation.contract.dto.ReservationRequest;
import syscon.arbutus.product.services.reservation.contract.interfaces.ReservationServiceLocal;
import syscon.arbutus.product.services.supervision.contract.dto.*;
import syscon.arbutus.product.services.supervision.contract.ejb.SupervisionHandler;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;
import syscon.arbutus.product.services.utility.ValidationHelper;
import syscon.arbutus.product.services.vine.gov.niem.niem.proxy.xsd._2.*;
import syscon.arbutus.product.services.visitation.contract.dto.VisitEnum;
import syscon.arbutus.product.services.visitation.contract.interfaces.VisitationService;

/**
 * Implementation of MovementActivity Service
 * <pre>
 * To uniquely-identify a single or set of related actions, events or process steps, for all movements of incarcerated inmates, both external and internal:
 * <li>External movements include all events in which inmates are moved in and out of a custodial facility: admission (reception), release (discharge), transfers from one custodial facility to another, temporary absences (court movements, hospital visits, compassionate grounds Ã¢â‚¬â€œ escorted or unescorted), escapes and recaptures.</li>
 * <li>Internal movements include all events in which inmates move to and from their living units to other security-controlled areas within the custodial facility: (food service, internal work assignments, and visiting areas).</li>
 * </pre>
 *
 * @author yshang
 * @version 1.0 (based on Movement Activity 1.0 SDD)
 * @since February 10, 2012.
 */

@Stateless(mappedName = "MovementService", description = "Movement Services")
public class MovementServiceBean implements MovementServiceLocal, MovementService {
    /**
     * Default serial version id
     */
    private static final long serialVersionUID = 1L;
    private static final String USAGETYPE_ACTIVITY = "ACT";
    private static final String DIRECTION_OUT = "OUT";
    private static final String DIRECTION_IN = "IN";
    private static final String STATUS_PENDING = "PENDING";
    private static final String STATUS_COMPLETE = "COMPLETED";
    private static final String CATEGORY_EXTERNAL = "EXTERNAL";
    private static final String CATEGORY_INTERNAL = "INTERNAL";
    private static final String TYPE_ADM = "ADM";
    private static final String TYPE_TRNIJ = "TRNIJ";
    private static final String TYPE_TAP = "TAP";
    private static final String TYPE_CRT = "CRT";
    private static final String TYPE_APP = "APP";
    private static final String TYPE_REL = "REL";
    private static final String TYPE_APPO = "APPO";
    private static final String TYPE_ESCP = "ESCP";
    private static final String TYPE_TRNOJ = "TRNOJ";
    private static final String TYPE_INT = "INT";
    private static final String INMATE_STATUS_ACTIVE = "ACTIVE";
    private static final String INMATE_STATUS_INACTIVE = "INACTIVE";
    private static final String INMATE_STATUS_INTRANSIT = "INTRANSIT";
    private static final String INMATE_INOUT_IN = "IN";
    private static final String INMATE_INOUT_OUT = "OUT";

    private static final String MOVEMENT_OUTCOME_CODE = null;
    private static Logger log = LoggerFactory.getLogger(MovementServiceBean.class);
    private static int SEARCH_MAX_LIMIT = 200;
    private final Long success = 1L;

    @Resource
    SessionContext context;

    @PersistenceContext(unitName = "arbutus-pu")
    private Session session;

    @EJB
    private SupervisionService supervisionService;

    @Inject
    private FacilityInternalLocationServiceLocal facilityInternalLocationService;

    @EJB
    private FacilityService facilityService;

    @EJB
    private OrganizationService organizationService;

    @EJB
    private HousingServiceLocal housingService;

    @EJB
    private AppointmentService appointmentService;

    @EJB
    private VisitationService visitationService;

    @EJB
    private ProgramService programService;

    @EJB
    private ActivityService activityService;

    @EJB
    private ReferenceDataService referenceDataService;

    @EJB
    private PersonServiceLocal personService;
    
    @EJB
    private ReservationServiceLocal  reservationserviceLocale;

    @Inject
    @KReleaseId(groupId = "syscon.arbutus.product", artifactId = "movements", version = "1.0.0")
    private KieSession ksession;

    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    private ConflictHandler conflictHandler;
    private Set<Long> supervisionIdSet;

    /**
     * To load the set of the initial Reference Codes
     */
    @PostConstruct
    private void init() {
        if (log.isDebugEnabled()) {
            log.debug("init: Begin ... ...");
        }

        ClassLoader loader = this.getClass().getClassLoader();

        try {
            ResourceBundle rb = ResourceBundle.getBundle("service", Locale.getDefault(), loader);
            for (@SuppressWarnings("rawtypes") Enumeration keys = rb.getKeys(); keys.hasMoreElements(); ) {
                String key = (String) keys.nextElement();
                if ("searchMaxLimit".equalsIgnoreCase(key)) {
                    String value = rb.getString(key);
                    SEARCH_MAX_LIMIT = Integer.parseInt(value);
                    return;
                }

            }
        } catch (Exception ex) {
            log.error("init: property file open failure.");
        }

        if (log.isDebugEnabled()) {
            log.debug("init: End ... ...");
        }
    }

    @Override
    public void createInternalMovement(UserContext uc,InternalMovement internalMovement, Long facilityId, Boolean adhoc , Boolean approveRequired) {

        String status;
        if(adhoc){
            status = MovementStatus.COMPLETED.code();
        }else {

            status = MovementStatus.PENDING.code();
        }

        String groupId = UUID.randomUUID().toString();
        InternalMovementActivityType movementOut = MovementHelper.buildInternalMovement(internalMovement, MovementActivityType.MovementDirection.OUT.code(), status);
        movementOut.setGroupId(groupId);
        InternalMovementActivityType movementIn = MovementHelper.buildInternalMovement(internalMovement, MovementActivityType.MovementDirection.IN.code(), status);
        movementIn.setGroupId(groupId);
        ActivityType activityOut = MovementHelper.buildInternalActivity(internalMovement, MovementActivityType.MovementStatus.COMPLETED.code());
        ActivityType activityIn = MovementHelper.buildInternalActivity(internalMovement, MovementActivityType.MovementStatus.COMPLETED.code());
        MovementDetailType mdtOUT =createLinkedMovement(uc, movementOut, activityOut, internalMovement.getSupervisionId(), approveRequired);
        activityIn.setActivityAntecedentId(mdtOUT.getActivity().getActivityIdentification());
        MovementDetailType mdtIN = createLinkedMovement(uc, movementIn, activityIn, internalMovement.getSupervisionId(), approveRequired);

    }


    @Override
    public void createTransferMovement(UserContext userContext, Long userId, Long staffId,  ExternalMovement movement, Boolean isAdhoc) {
        String status;
        if (isAdhoc) {
            status = MovementStatus.COMPLETED.code();
        } else {
            status = MovementStatus.PENDING.code();
        }

        SupervisionClosureConfigurationSearchType supervisionClosureConfigurationSearchType = new SupervisionClosureConfigurationSearchType();
        supervisionClosureConfigurationSearchType.setMovementReason(movement.getMoveReasonCd());
        supervisionClosureConfigurationSearchType.setMovementType(MovementType.TRNIJ.name());


        ActivityType  activityType =
                MovementHelper.buildInternalActivity(movement.getSupervisionId(), movement.getFromFacility(), movement.getMoveDate(), movement.getMoveTime(), null, null,
                        status);
        activityType.setScheduleID(createSchedule(userContext, movement).getScheduleIdentification());
        activityType = activityService.create(userContext, activityType, null, true);
        ExternalMovementActivityType externalMovementActivityType = MovementHelper.createExternalMovementActivity(activityType.getActivityIdentification(), movement,
                status);
        MovementHelper.setMovementCommentText(userId, movement.getComment(), externalMovementActivityType);
        create(userContext, externalMovementActivityType, false);

        //remove  offernder from WaitList
        TransferWaitlistSearchType searchCriteria = new TransferWaitlistSearchType();
        searchCriteria.setFacilityId(movement.getFromFacility());
        searchCriteria.setSupervisionId(movement.getSupervisionId());
        List<TransferWaitlistType> waitLists = searchTransferWaitlist(userContext, searchCriteria, null, null, null).getTransferWaitlists();
        if(waitLists!=null && waitLists.size()>0){
            Set<Long> supIds = new HashSet<Long>();
            supIds.add(movement.getSupervisionId());
            deleteTransferWaitlistEntry(userContext, movement.getFromFacility(), supIds);

        }

        if(isAdhoc){
            List<SupervisionClosureConfigurationType>  configs = searchSupervisionClosureConfiguration(userContext, supervisionClosureConfigurationSearchType, null, null,null).getSupervisionClosureConfigurations();

            if (configs != null && configs.size() > 0 && configs.get(0).getIsReleaseBedFlag()) {
                unAssignHousing(userContext,staffId, movement, activityType);
            }

            //To Update Type of Interest To IN TRANSIT
            supervisionService.updateFOIForSupervision(userContext, movement.getSupervisionId(), TypeOfInterestEnum.IN_TRANSIT);

            if (configs.size() > 0 && configs.get(0).getIsCleanSchedulesFlag()) {
                supervisionService.cleanUpWhileSupervisionTransfer(userContext, movement.getSupervisionId(), staffId);
            }

            if (configs.size() > 0 && configs.get(0).getIsCloseSupervisionFlag()) {
                supervisionService.close(userContext, movement.getSupervisionId(),
                        DateUtil.combineDateTime(movement.getMoveDate(), movement.getMoveTime()));
            }
        }
    }

    private Schedule createSchedule(UserContext userContext, ExternalMovement offenderSchType) {
        Schedule schedule = new Schedule();
        schedule.setScheduleCategory(ActivityCategory.MOVEMENT.value());
        schedule.setFacilityId(offenderSchType.getFromFacility());
        schedule.setScheduleStartTime(ActivityServiceHelper.getTimeOnly(offenderSchType.getMoveDate()));
        schedule.setEffectiveDate(new Date());
        schedule.setScheduleEndTime(ActivityServiceHelper.getTimeOnly(offenderSchType.getExpectedArrivalDate()));
        return activityService.createSchedule(userContext, schedule);
    }


    private void unAssignHousing(UserContext userContext, Long staffId, ExternalMovement movement, ActivityType activityType ) {


        Set<Long> associationSet = new HashSet<Long>();
        associationSet.add(movement.getSupervisionId());

        List<OffenderHousingAssignmentType> assignmentTypes = housingService.retrieveCurrentAssignmentsByOffenders( userContext, associationSet).getOffenderHousingAssignments();

        if(assignmentTypes!=null && assignmentTypes.size()>0){

            String comment = movement.getComment();
            if (comment == null || comment.trim().isEmpty()) {
                comment = "Housing Unassigned";
            }

            CommentType commentType = new CommentType();
            commentType.setCommentDate(movement.getMoveDate());
            commentType.setComment(comment);
            commentType.setUserId(staffId.toString());
            UnassignOffenderType unassignOffender = new UnassignOffenderType(movement.getSupervisionId(),
                    staffId,
                    activityType.getActivityIdentification(),
                    movement.getFromFacility(),
                    DateUtil.combineDateTime(movement.getMoveDate(), movement.getMoveTime()), movement.getMoveReasonCd(), MOVEMENT_OUTCOME_CODE, commentType);

            housingService.unAssignOffenderFromHousingLocation(userContext, unassignOffender);

        }
    }

    @Override
    public String combineAddress(final Location location) {
        return MovementHelper.combineAddress(location);
    }

    /**
     * create -- Create a Movement Activity.
     * <p>Movement Activity must have an association to ACTIVITY Additional Information:
     * The corresponding Activity must always exist before the Movement Activity is created.
     * <p>If RequiresApproval == TRUE then Set MovementStatus to APPREQ Else Set MovementStatus to PENDING.
     * <p>In case of InternalMovement, if MovementStatus == PENDING then FromInternalLocation is optional Else FromInternalLocation is mandatory
     *
     * @param uc               UserContext - Required
     * @param movementActivity -- either ExternalMovementActivityType or
     *                         InternalMovementActivityType.
     * @param requiresApproval Boolean
     * @return MovementActivityReturnType
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T extends MovementActivityType> MovementActivityType create(UserContext uc, T movementActivity, Boolean requiresApproval) {

        try { // The QA is saying that throwing ArbutusRuntimeException does not rollback the trans.
            if (log.isDebugEnabled()) {
                log.debug("create MovementActivity: begin");
            }

            String functionName = "create";

            //WOR-20035
            requiresApproval = false;
            //End WOR-20035

            ValidationHelper.validate(movementActivity);

            // Process business logic.

            // Check if the Activity associated has already been referred by another instance of Movement Activity
            if (hasAssociatedToOther(session, movementActivity.getActivityId())) {
                String message = "The Activity " + movementActivity.getActivityId() +
                        " has already been associated to another Movement Activity.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }

            /** If RequiresApproval == TRUE then
             *    Set MovementStatus to APPREQ
             */
            if (Boolean.TRUE.equals(requiresApproval)) {
                movementActivity.setMovementStatus(MovementStatus.APPREQ.code());
            }

            // Check if movement activity type is one of supported and return if not.
            if (movementActivity instanceof ExternalMovementActivityType) {
                verifyForExternalMovementCreation(uc, movementActivity);
            } else if (movementActivity instanceof InternalMovementActivityType) {
                verifyForInternalMovementCreation(uc, movementActivity, requiresApproval);
            }

            // Set Id = null because it is to be generated by system
            movementActivity.setMovementIdentification(null);
            MovementActivityEntity mae = null;
            if (movementActivity instanceof ExternalMovementActivityType) {

                MovementActivityType lastMovement = getSupervisionEscapeMovement(uc, movementActivity.getSupervisionId());

                if (lastMovement != null && ReferenceSet.ESCAPE_LINK.value().equals(lastMovement.getMovementType())) {
                    Long escapeId = getEscapeByMovementId(uc, lastMovement.getMovementId());
                    movementActivity.setEscapeId(escapeId);
                }
                mae = MovementHelper.toMovementActivityEntity(movementActivity, ExternalMovementActivityEntity.class);
            } else {
                mae = MovementHelper.toMovementActivityEntity(movementActivity, InternalMovementActivityEntity.class);
            }

            ValidationHelper.verifyMetaCodes(mae, true);

            // Save the MovementActivity to obtain the identifier
            session.save(mae);
            String movementComments = "";
            Long movementActivityId = mae.getMovementId();
            Set<MovementActivityCommentEntity> comments = mae.getCommentText();
            if (comments != null && !comments.isEmpty()) {
                for (MovementActivityCommentEntity cmnt : comments) {
                    cmnt.setFromIdentifier(movementActivityId);
                    movementComments = movementComments + "; " + cmnt.getCommentText();
                    session.save(cmnt);
                }
            }


            if(!mae.getMovementCategory().equalsIgnoreCase(MovementCategory.NOMOV.code())){
                updateLastCompletedSupervisionMovement(uc, mae);
            }


            if (MovementType.REL.code().equalsIgnoreCase(mae.getMovementType()) || MovementType.TRNOJ.code().equalsIgnoreCase(mae.getMovementType())) {
                closeSupervision(uc, mae.getSupervisionId(), mae.getMovementReason(), MovementType.valueOf(mae.getMovementType()), mae.getMovementDate(), movementComments.trim().isEmpty() ? null : movementComments.trim());
            }

            // Transform to MovementActivity from MovementActivityEntity
            T ret = (T) MovementHelper.toMovementActivityType(mae, movementActivity.getClass());

            if (log.isDebugEnabled()) {
                log.debug("Created MovementActivity: " + ret);
                log.debug("create MovementActivity: end");
            }
            return ret;
        } catch (Exception e) {
            try {
                context.setRollbackOnly();
            } catch (Exception ex) {
            }

            if (e instanceof ArbutusRuntimeException) {
                throw (ArbutusRuntimeException) e;
            } else {
                throw new ArbutusRuntimeException(e);
            }
        }
    }

    private MovementActivityType getSupervisionEscapeMovement(UserContext uc, Long supervisionId) {
        MovementActivityType lastMovement = null;
        PersonIdentityType personIdentity = personService.getPersonIdentityBySupervisionId(uc, supervisionId);
        if(personIdentity != null && personIdentity.getPersonIdentityId() != null) {
            Long personIdentityId = personIdentity.getPersonIdentityId();

            return getLastMovement(uc, personIdentityId);
        }
        return null;
    }

    private MovementActivityType getLastMovement(UserContext uc, Long personIdentityId) {
        MovementActivityType lastMovement = null;
        SupervisionType supervision = supervisionService.getActiveOrLastClosedSupervisionByPersonIdentity(uc, personIdentityId);
        lastMovement = getSupervisionLastMovement(uc, supervision);
        if (lastMovement == null) {
            supervision = supervisionService.getLastClosedSupervision(uc, personIdentityId);
            lastMovement = getSupervisionLastMovement(uc, supervision);
        }
        return lastMovement;
    }

    /**
     * get Escape details by Person Identity
     *
     * @param uc
     * @param personIdentityId
     * @return escape recapture details
     */
    @Override
    public EscapeRecapture getEscapeByPersonIdentity(UserContext uc, Long personIdentityId) {
        MovementActivityType lastMovement = getLastMovement(uc, personIdentityId);
        if (lastMovement != null && ReferenceSet.ESCAPE_LINK.value().equals(lastMovement.getMovementType())) {
            Long escapeId = getEscapeByMovementId(uc, lastMovement.getMovementId());
            return getEscapeDetailsById(uc, escapeId);
        }
        return null;
    }

    private MovementActivityType getSupervisionLastMovement(UserContext uc, SupervisionType supervision) {
        MovementActivityType lastMovement= null;
        if (supervision != null && supervision.getSupervisionIdentification() != null) {
            Long supervisionId = supervision.getSupervisionIdentification();
            Long lastMovementId = getLastSupervisionMovementId(uc, supervisionId);
            if (lastMovementId != null) {
                lastMovement = get(uc, lastMovementId);
            }
        }
        return lastMovement;
    }

    /**
     * get EscapeId by MovementId
     * @param uc
     * @param lastMovementId
     * @return escapeId
     */
    private Long getEscapeByMovementId(UserContext uc, Long lastMovementId) {
        Query query = session.createQuery("select e from EscapeOffenderEntity e where e.movementId = :movementId");
        query.setParameter("movementId", lastMovementId);
        List<EscapeOffenderEntity> list =  query.list();
        if(!list.isEmpty()){
            return list.get(0).getEscapeId();
        }
        return null;
    }

    /**
     * get Last completed movement id
     *
     * @param uc
     * @param supervisionId
     * @return last movement Id
     */
    private Long getLastSupervisionMovementId(UserContext uc, Long supervisionId) {
        Query query = session.createQuery("select lm from LastCompletedSupervisionMovementEntity lm where lm.supervisionId = :supervisionId");
        query.setParameter("supervisionId", supervisionId);

        List<LastCompletedSupervisionMovementEntity> list =  query.list();
        if(!list.isEmpty()){
            return list.get(0).getMovementActivityId();
        }
        return null;
    }

    private Long getLastMovementByPersonIdentity(UserContext uc, Long personIdentityId) {
        Set<Long> identities = new HashSet<>();
        identities.add(personIdentityId);
        LastCompletedSupervisionMovementType lm = getLastCompletedSupervisionMovementByPersonIdentityId(uc, personIdentityId);
        return lm == null? null : lm.getLastCompletedMovementActivity().getMovementId();
    }

    /**
     * delete -- Delete an existing Movement by Id from the system.
     *
     * @param uc UserContext
     * @param id Long Movement Activity Identifier
     * @return MovementActivityReturnType
     */
    @Override
    public Long delete(UserContext uc, Long id) {
        if (log.isDebugEnabled()) {
            log.debug("delete: begin");
        }

        // Get entity by id
        MovementActivityEntity mae = BeanHelper.getEntity(session, MovementActivityEntity.class, id);

        session.createQuery("delete MovementActivityCommentEntity c where c.fromIdentifier = " + id).executeUpdate();
        session.createQuery("delete MovementActivityEntity ma where ma.movementId = " + id).executeUpdate();

        session.flush();
        session.clear();

        updateLastCompletedSupervisionMovementWhenDeleteMA(uc, mae);

        if (log.isDebugEnabled()) {
            log.debug("deleted: " + mae.getMovementId());
        }

        if (log.isDebugEnabled()) {
            log.debug("delete: end");
        }

        return success;
    }

    /**
     * deleteAll -- Delete all Movement Activities, associated References and
     * Reference Data.
     *
     * @param uc UserContext
     * @return java.lang.Long
     */
    @Override
    public Long deleteAll(UserContext uc) {
        if (log.isDebugEnabled()) {
            log.debug("deleteAll: begin");
        }

        deleteAllMovementActivities(uc);
        deleteAllTransferWaitlist(uc);
        deleteLastCompletedSupervisionMovement(uc);

        if (log.isDebugEnabled()) {
            log.debug("deleteAll: end");
        }

        return success;
    }

    @Override
    public <T extends MovementActivityType> boolean update(UserContext uc, List<T> movementActivityList) {
        for(T t: movementActivityList) {
            update(uc, t);
        }
        return true;
    }


    @Override
    public void actualizeScheduledInternalMovement(UserContext userContext, OffenderScheduleTransferType schedule) {

        MovementActivitySearchType searchType = new MovementActivitySearchType();
        searchType.setSupervisionId(schedule.getSupervisionId());
        searchType.setGroupId(schedule.getGroupId());
        searchType.setMovementCategory(MovementCategory.INTERNAL.code());
        searchType.setMovementStatus(MovementStatus.PENDING.code());
        List<InternalMovementActivityType> internalMovementActivityTypes = search(userContext, searchType, null,null,null).getInternalMovementActivity();

        for(InternalMovementActivityType move : internalMovementActivityTypes){
            move.setMovementStatus(MovementStatus.COMPLETED.code());
            move.setMovementDate(DateUtil.combineDateTime(schedule.getMovementDate(), schedule.getMovementTime()));
            if(schedule.getComment()!=null && !schedule.getComment().isEmpty()){
                CommentType  commentType =new CommentType();
                commentType.setComment(schedule.getComment());
                commentType.setCommentDate(DateUtil.getCurrentDate());
                move.getCommentText().add(commentType);

            }
            update(userContext, move);

        }


    }


    @Override
    public <T extends MovementActivityType> MovementActivityType update(UserContext uc, T movementActivity) {

        if (log.isDebugEnabled()) {
            log.debug("update MovementActivity: begin");
        }

        String functionName = "update";

        if (movementActivity == null || movementActivity.getMovementId() == null) {
            String message = "Invalid argument: MovementActivity Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(movementActivity);

        // Get entity by id
        Long id = movementActivity.getMovementId();
        MovementActivityEntity mae = BeanHelper.getEntity(session, MovementActivityEntity.class, id);

        //MovementActivityEntity mae = BeanHelper.findEntity(session, MovementActivityEntity.class, id);



        MovementActivityEntity upd;

        if (movementActivity instanceof ExternalMovementActivityType) {
            upd = MovementHelper.toMovementActivityEntity(movementActivity, ExternalMovementActivityEntity.class);
        } else {
            upd = MovementHelper.toMovementActivityEntity(movementActivity, InternalMovementActivityEntity.class);
        }

        ValidationHelper.verifyMetaCodes(upd, false);

        // Process business logic.
        // Check if arguments exist in the ReferenceSet&Code&Language. Return
        // error Invalidated Argument(s) if not.
        verifyForUpdate(uc, movementActivity);

        // Copy properties to update

        // Movement Type
        String type = upd.getMovementType();
        if (!BeanHelper.isEmpty(type)) {
            mae.setMovementType(type.trim());
        }

        // Movement Direction
        String direction = upd.getMovementDirection();
        if (!BeanHelper.isEmpty(direction)) {
            mae.setMovementDirection(direction.trim());
        }

        // Movement Status
        String status = upd.getMovementStatus();
        if (!BeanHelper.isEmpty(status)) {
            // Check business logical
            // Status ONHOLD can be changed to PENDING or CANCELLED.
            if (MovementStatus.ONHOLD.isEquals(mae.getMovementStatus())) {
                if (!(MovementStatus.PENDING.isEquals(status) || MovementStatus.CANCELLED.isEquals(status) || MovementStatus.ONHOLD.isEquals(status))) {
                    String message = "Status ONHOLD can be chaged to PENDING or CANCELLED or ONHOLD itself.";
                    LogHelper.error(log, functionName, message);
                    throw new InvalidInputException(message);
                }
            }

            // Status PENDING cannot be changed to APPREQ or DENIED
            if (MovementStatus.PENDING.isEquals(mae.getMovementStatus())) {
                if (MovementStatus.APPREQ.isEquals(status) || MovementStatus.DENIED.isEquals(status)) {
                    String message = "Status PENDING cannot be changed to APPREQ or DENIED.";
                    LogHelper.error(log, functionName, message);
                    throw new InvalidInputException(message);
                }

            }

            // Status APPREQ can be changed to PENDING or DENIED
            if (MovementStatus.APPREQ.isEquals(mae.getMovementStatus())) {
                if (!(MovementStatus.PENDING.isEquals(status) || MovementStatus.DENIED.isEquals(status) || MovementStatus.CANCELLED.isEquals(status)
                        || MovementStatus.APPREQ.isEquals(status))) {
                    String message = "Status APPREQ can only be changed to PENDING, DENIED OR CANCELLED.";
                    LogHelper.error(log, functionName, message);
                    throw new InvalidInputException(message);
                }
            }

            // Status COMPLETED, CANCELLED, NOSHOW and DENIED cannot be changed to other status.
            if ((MovementStatus.COMPLETED.isEquals(mae.getMovementStatus()) ||
                    MovementStatus.CANCELLED.isEquals(mae.getMovementStatus()) ||
                    MovementStatus.NOSHOW.isEquals(mae.getMovementStatus()) ||
                    MovementStatus.DENIED.isEquals(mae.getMovementStatus())) && !mae.getMovementStatus().equals(upd.getMovementStatus())) {

                String message = "Status " + mae.getMovementStatus() + " cannot be changed to other status.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }

            mae.setMovementStatus(upd.getMovementStatus().trim());
        }

        // wmadruga 29-jul-2013 to handle TAP movement approval - WOR-3582
        mae.setApprovalComments(upd.getApprovalComments());
        mae.setApprovalDate(upd.getApprovalDate());
        mae.setApprovedByStaffId(upd.getApprovedByStaffId());

        // Movement Reason
        String reason = upd.getMovementReason();
        if (!BeanHelper.isEmpty(reason)) {
            mae.setMovementReason(reason);
        }

        // Movement Outcome
        mae.setMovementOutcome(upd.getMovementOutcome());

        // Movement Date
        mae.setMovementDate(upd.getMovementDate());

        if (!status.equals(mae.getMovementStatus()) && (status.equalsIgnoreCase(MovementStatus.NOSHOW.code()) || status.equalsIgnoreCase(MovementStatus.COMPLETED.code()))
                && BeanHelper.isEmpty(upd.getCommentText())) {

            log.error("Invalid argument: MovementComment is required when MovementStatus changed.");
            return null;
        }
        if (!BeanHelper.isEmpty(upd.getCommentText())) {
            updateComments(mae, upd);
        } else {
            upd.setCommentText(new HashSet<MovementActivityCommentEntity>());
            updateComments(mae, upd);

        }

        MovementActivityType ret;

        if (movementActivity instanceof ExternalMovementActivityType) {
            ExternalMovementActivityEntity from = (ExternalMovementActivityEntity) upd;
            ExternalMovementActivityEntity to = (ExternalMovementActivityEntity) mae;

            // new field used by TAP
            to.setTransportation(from.getTransportation());

            to.setFromLocationId(from.getFromLocationId());
            to.setFromOrganizationId(from.getFromOrganizationId());
            to.setFromFacilityId(from.getFromFacilityId());

            to.setFromCity(from.getFromCity());

            to.setToLocationId(from.getToLocationId());
            to.setToOrganizationId(from.getToOrganizationId());
            to.setToFacilityId(from.getToFacilityId());
            to.setToCity(from.getToCity());
            to.setToProvinceState(from.getToProvinceState());

            to.setApplicationDate(from.getApplicationDate());
            to.setArrestOrganizationId(from.getArrestOrganizationId());
            to.setEscortOrganizationId(from.getEscortOrganizationId());
            to.setEscortDetails(from.getEscortDetails());
            to.setReportingDate(from.getReportingDate());

            to.setToInternalLocationId(from.getToInternalLocationId());

            //updating dates in the activity for WOR-19833 Movement type "Temporary Absence" in edit mode the data didn't get saved

            if (null != mae.getActivityId() && (mae.getMovementType().contentEquals(MovementActivityType.MovementType.TAP.code()) || mae.getMovementType().contentEquals(
                    MovementActivityType.MovementType.CRT.code()))) {

                ActivityType activity = ActivityServiceAdapter.getActivity(uc, mae.getActivityId());
                ActivityType activityAntecedentDescident = null;

                if (null != activity) {
                    if (null != activity.getActivityAntecedentId()) {
                        activityAntecedentDescident = ActivityServiceAdapter.getActivity(uc, activity.getActivityAntecedentId());

                    } else {

                        String hql = "FROM ActivityEntity ae where ae.antecedentId = " + movementActivity.getActivityId();
                        Query query2 = session.createQuery(hql);
                        List results = query2.list();

                        if (null != results && !results.isEmpty()) {
                            ActivityEntity entity = (ActivityEntity) results.get(0);
                            activityAntecedentDescident = activity;
                            activity = ActivityServiceAdapter.getActivity(uc, entity.getActivityId());

                        }

                    }

                }

                if (null != ((ExternalMovementActivityType) movementActivity).getPlannedEndDate()) {
                    activity.setPlannedStartDate(((ExternalMovementActivityType) movementActivity).getPlannedEndDate());
                    activity.setPlannedEndDate(((ExternalMovementActivityType) movementActivity).getPlannedEndDate());
                    ActivityServiceAdapter.updateActivity(uc, activity);
                }
                if (null != activityAntecedentDescident && null != ((ExternalMovementActivityType) movementActivity).getPlannedStartDate()) {
                    activityAntecedentDescident.setPlannedStartDate(((ExternalMovementActivityType) movementActivity).getPlannedStartDate());
                    activityAntecedentDescident.setPlannedEndDate(((ExternalMovementActivityType) movementActivity).getPlannedStartDate());
                    ActivityServiceAdapter.updateActivity(uc, activityAntecedentDescident);
                }

            }

            if (null != mae.getActivityId() && mae.getMovementType().contentEquals(MovementActivityType.MovementType.TRNIJ.code())) {

                ActivityType activity = ActivityServiceAdapter.getActivity(uc, mae.getActivityId());
                if (null != ((ExternalMovementActivityType) movementActivity).getPlannedStartDate()) {
                    activity.setPlannedStartDate(((ExternalMovementActivityType) movementActivity).getPlannedStartDate());
                    activity.setPlannedEndDate(((ExternalMovementActivityType) movementActivity).getPlannedEndDate());
                    ActivityServiceAdapter.updateActivity(uc, activity);
                }

            }

            // Update/Merge the MovementActivity
            session.merge(to);

            // Update Movement reason for related entity WOR-20156
            if (null != to.getGroupId()) {
                String hql = "FROM MovementActivityEntity ma where ma.groupid = '"
                        + to.getGroupId()
                        + "' and ma.movementId != "
                        + to.getMovementId();
                Query query2 = session.createQuery(hql);
                @SuppressWarnings("rawtypes")
                List results = query2.list();

                if (null != results && !results.isEmpty()) {
                    MovementActivityEntity entity2 = (MovementActivityEntity) results
                            .get(0);
                    entity2.setMovementReason(to.getMovementReason());
                    session.merge(entity2);
                }
            }

            if (log.isDebugEnabled()) {
                log.debug("update MovementActivityEntity: " + to);
            }

            updateLastCompletedSupervisionMovement(uc, to);

            // Transform to MovementActivity from MovementActivityEntity
            ret = MovementHelper.toMovementActivityType(to, movementActivity.getClass());
        } else {
            InternalMovementActivityEntity from = (InternalMovementActivityEntity) upd;
            InternalMovementActivityEntity to = (InternalMovementActivityEntity) mae;

            to.setFromInternalLocationId(from.getFromInternalLocationId());
            to.setToInternalLocationId(from.getToInternalLocationId());

            ActivityType activity = ActivityServiceAdapter.getActivity(uc, mae.getActivityId());
            ActivityType activityAntecedentDescident = null;

            if (null != activity) {
                if (null != activity.getActivityAntecedentId()) {
                    activityAntecedentDescident = ActivityServiceAdapter.getActivity(uc, activity.getActivityAntecedentId());

                } else {

                    String hql = "FROM ActivityEntity ae where ae.antecedentId = " + movementActivity.getActivityId();
                    Query query2 = session.createQuery(hql);
                    List results = query2.list();

                    if (null != results && !results.isEmpty()) {
                        ActivityEntity entity = (ActivityEntity) results.get(0);
                        activityAntecedentDescident = activity;
                        activity = ActivityServiceAdapter.getActivity(uc, entity.getActivityId());

                    }

                }

            }

            if (null != ((InternalMovementActivityType) movementActivity).getPlannedEndDate()) {
                activity.setPlannedStartDate(((InternalMovementActivityType) movementActivity).getPlannedEndDate());
                activity.setPlannedEndDate(((InternalMovementActivityType) movementActivity).getPlannedEndDate());
                ActivityServiceAdapter.updateActivity(uc, activity);
            }
            if (null != activityAntecedentDescident && null != ((InternalMovementActivityType) movementActivity).getPlannedStartDate()) {
                activityAntecedentDescident.setPlannedStartDate(((InternalMovementActivityType) movementActivity).getPlannedStartDate());
                activityAntecedentDescident.setPlannedEndDate(((InternalMovementActivityType) movementActivity).getPlannedStartDate());
                ActivityServiceAdapter.updateActivity(uc, activityAntecedentDescident);
            }

            // Update/Merge the MovementActivity
            session.merge(to);
            if (log.isDebugEnabled()) {
                log.debug("update MovementActivityEntity: " + to);
            }

            updateLastCompletedSupervisionMovement(uc, to);

            // Transform to MovementActivity from MovementActivityEntity
            ret = MovementHelper.toMovementActivityType(to, movementActivity.getClass());
        }

        if (log.isDebugEnabled()) {
            log.debug("Updated MovementActivity: " + ret);
        }

        return ret;
    }


    private void updateMoveActivityStatus(String functionName, MovementActivityEntity mae, String status) {
       if (!BeanHelper.isEmpty(status)) {
            // Check business logical
            // Status ONHOLD can be changed to PENDING or CANCELLED.
            if (MovementStatus.ONHOLD.isEquals(mae.getMovementStatus())) {
                if (!(MovementStatus.PENDING.isEquals(status) || MovementStatus.CANCELLED.isEquals(status) || MovementStatus.ONHOLD.isEquals(status))) {
                    String message = "Status ONHOLD can be chaged to PENDING or CANCELLED or ONHOLD itself.";
                    LogHelper.error(log, functionName, message);
                    throw new InvalidInputException(message);
                }
            }

            // Status PENDING cannot be changed to APPREQ or DENIED
            if (MovementStatus.PENDING.isEquals(mae.getMovementStatus())) {
                if (MovementStatus.APPREQ.isEquals(status) || MovementStatus.DENIED.isEquals(status)) {
                    String message = "Status PENDING cannot be changed to APPREQ or DENIED.";
                    LogHelper.error(log, functionName, message);
                    throw new InvalidInputException(message);
                }

            }

            // Status APPREQ can be changed to PENDING or DENIED
            if (MovementStatus.APPREQ.isEquals(mae.getMovementStatus())) {
                if (!(MovementStatus.PENDING.isEquals(status) || MovementStatus.DENIED.isEquals(status) || MovementStatus.CANCELLED.isEquals(status)
                        || MovementStatus.APPREQ.isEquals(status))) {
                    String message = "Status APPREQ can only be changed to PENDING, DENIED OR CANCELLED.";
                    LogHelper.error(log, functionName, message);
                    throw new InvalidInputException(message);
                }
            }

            // Status COMPLETED, CANCELLED, NOSHOW and DENIED cannot be changed to other status.
            if ((MovementStatus.COMPLETED.isEquals(mae.getMovementStatus()) ||
                    MovementStatus.CANCELLED.isEquals(mae.getMovementStatus()) ||
                    MovementStatus.NOSHOW.isEquals(mae.getMovementStatus()) ||
                    MovementStatus.DENIED.isEquals(mae.getMovementStatus())) && !mae.getMovementStatus().equals(status)) {

                String message = "Status " + mae.getMovementStatus() + " cannot be changed to other status.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }

            mae.setMovementStatus(status);
        }
    }

    /**
     * get -- Get/Retrieve a Movement Activity by Id.
     *
     * @param uc UserContext
     * @param id java.lang.Long
     * @return MovementActivityReturnType
     */
    @Override
    public MovementActivityType get(UserContext uc, Long id) {

        if (log.isDebugEnabled()) {
            log.debug("get: begin");
        }

        MovementActivityEntity mae = BeanHelper.getEntity(session, MovementActivityEntity.class, id);

        MovementActivityType ret = null;
        if (mae instanceof ExternalMovementActivityEntity) {
            ret = MovementHelper.toMovementActivityType(mae, ExternalMovementActivityType.class);
        } else if (mae instanceof InternalMovementActivityEntity) {
            ret = MovementHelper.toMovementActivityType(mae, InternalMovementActivityType.class);
        } else {
            ret = MovementHelper.toMovementActivityType(mae, MovementActivityType.class);
        }

        if (log.isDebugEnabled()) {
            log.debug("get: end");
        }

        return ret;
    }

    /**
     * getExternalMovementActivity -- Get/Retrieve an External Movement Activity by Id.
     *
     * @param uc UserContext
     * @param id java.lang.Long
     * @return ExternalMovementActivityType
     */
    @Override
    public ExternalMovementActivityType getExternalMovementActivity(UserContext uc, Long id) {

        if (log.isDebugEnabled()) {
            log.debug("get: begin");
        }

        ExternalMovementActivityEntity mae = BeanHelper.getEntity(session, ExternalMovementActivityEntity.class, id);
        ExternalMovementActivityType ret = MovementHelper.toMovementActivityType(mae, ExternalMovementActivityType.class);

        if (log.isDebugEnabled()) {
            log.debug("get: end");
        }

        return ret;
    }

    /**
     * getAll -- Get/Retrieve a set of Ids of all Movement Activity.
     *
     * @param uc UserContext
     * @return java.util.Set<Long>
     */
    @Override
    public Set<Long> getAll(UserContext uc) {
        if (log.isDebugEnabled()) {
            log.debug("getAll: begin");
        }

        Set<Long> ret = new HashSet<Long>();

        Set<Long> retInt = getAllInternalMovementActivities(uc);
        if (!BeanHelper.isEmpty(retInt)) {
            ret.addAll(retInt);
        }

        Set<Long> retExt = getAllExternalMovementActivities(uc);
        if (!BeanHelper.isEmpty(retExt)) {
            ret.addAll(retExt);
        }

        if (log.isDebugEnabled()) {
            log.debug("getAll: end");
        }

        return ret;
    }

    /**
     * getCount -- Get the total number of Movement Activities.
     *
     * @param uc UserContext
     * @return Long
     */
    @Override
    public Long getCount(UserContext uc) {
        if (log.isDebugEnabled()) {
            log.debug("getCount: begin");
        }

        Long retInt = getCountOfInternalMovementActivities(uc);
        Long retExt = getCountOfExternalMovementActivities(uc);

        Long ret = 0L;
        if (retInt != null) {
            ret += retInt;
        }
        if (retExt != null) {
            ret += retExt;
        }

        if (log.isDebugEnabled()) {
            log.debug("getCount: end");
        }

        return ret;
    }

    /**
     * search -- Search Movement.
     * <pre>
     * <li>At least one of the search fields must be specified in the search criteria.</li>
     * </pre>
     *
     * @param uc                     UserContext
     * @param movementActivitySearch MovementActivitySearchType
     * @return MovementActivitiesReturnType
     */
    @Override
    public MovementActivitiesReturnType search(UserContext uc, MovementActivitySearchType movementActivitySearch, Long startIndex, Long resultSize, String resultOrder) {

        if (log.isDebugEnabled()) {
            log.debug("search: begin");
        }

        MovementActivitiesReturnType ret = search(uc, null, movementActivitySearch, startIndex, resultSize, resultOrder);

        if (log.isDebugEnabled()) {
            log.debug("search: end");
        }

        return ret;
    }

    /**
     * search --Search Movement Activities.
     * <pre>
     *   <li>Search for a set of Movement Activities based on a specified criteria MovementActivitySearchType.</li>
     *   <li>The subsetSearch can be used to search only within the specified subset if Id's.</li>
     *   <li>List what Search options are available in comments, EXACT is mandatory.</li>
     * </pre>
     *
     * @param uc          UserContext
     * @param ids         java.util.Set<Long> a set of Ids of movement activities.
     *                    SOUNDEX)
     * @param mas
     * @param startIndex
     * @param resultSize
     * @param resultOrder
     * @return MovementActivitiesReturnType
     */
    @Override
    public MovementActivitiesReturnType search(UserContext uc, Set<Long> ids, MovementActivitySearchType mas, Long startIndex, Long resultSize, String resultOrder) {

        if (log.isDebugEnabled()) {
            log.debug("search: begin");
        }

        ValidationHelper.validate(mas);

        if (resultSize == null || resultSize > SEARCH_MAX_LIMIT) {
            resultSize = Long.valueOf(SEARCH_MAX_LIMIT);
        }

        Criteria c = session.createCriteria(MovementActivityEntity.class);

        //		c.setMaxResults(SEARCH_MAX_LIMIT + 1);
        //		c.setFetchSize(SEARCH_MAX_LIMIT  + 1); // keeping it for a better performance
        //http://benjisimon.blogspot.ca/2007/07/hibernate-performance-tip-setfetchsize.html

        if (ids != null && !ids.isEmpty()) {
            c.add(Restrictions.in("movementId", ids));
        }

        Set<Long> activityIds = mas.getActivityIds();
        if (activityIds != null && !activityIds.isEmpty()) {
            c.add(Restrictions.in("activityId", activityIds));
        }

        String groupId = mas.getGroupId();
        if(groupId!=null){
            c.add(Restrictions.eq("groupid", groupId));

        }

        Long supervision = mas.getSupervisionId();
        if (supervision != null) {
            c.add(Restrictions.eq("supervisionId", supervision));
        }

        String category = mas.getMovementCategory();
        if (!BeanHelper.isEmpty(category)) {
            c.add(Restrictions.eq("movementCategory", category));
        }

        String type = mas.getMovementType();
        if (!BeanHelper.isEmpty(type)) {
            c.add(Restrictions.eq("movementType", type));
        }

        String direction = mas.getMovementDirection();
        if (!BeanHelper.isEmpty(direction)) {
            c.add(Restrictions.eq("movementDirection", direction));
        }

        String reason = mas.getMovementReason();
        if (!BeanHelper.isEmpty(reason)) {
            c.add(Restrictions.eq("movementReason", reason));
        }

        String status = mas.getMovementStatus();
        if (!BeanHelper.isEmpty(status)) {
            c.add(Restrictions.eq("movementStatus", status));
        }

        String outcome = mas.getMovementOutcome();
        if (!BeanHelper.isEmpty(outcome)) {
            c.add(Restrictions.eq("movementOutcome", outcome));
        }

        Date fromMovementDate = mas.getFromMovementDate();
        Date toMovementDate = mas.getToMovementDate();
        if (fromMovementDate != null && toMovementDate != null) {
            c.add(Restrictions.and(Restrictions.ge("movementDate", fromMovementDate), Restrictions.le("movementDate", toMovementDate)));
        } else if (fromMovementDate != null) {
            c.add(Restrictions.ge("movementDate", fromMovementDate));
        } else if (toMovementDate != null) {
            c.add(Restrictions.le("movementDate", toMovementDate));
        }

        //WOR-10080 : wmadruga
        // Planned date (from Activity) when searching for pending movements
        Date plannedStartDate = mas.getPlannedStartDate();
        Date plannedEndDate = mas.getPlannedEndDate();

        if (null != plannedEndDate) {
            plannedEndDate = BeanHelper.createDateWithTime(plannedEndDate, 23, 59, 0);
        }

        DetachedCriteria activityQuery = DetachedCriteria.forClass(ActivityEntity.class, "activity");

        if (plannedStartDate != null && plannedEndDate != null) {
            activityQuery.add(
                    Restrictions.and(Restrictions.ge("activity.plannedStartDate", plannedStartDate), Restrictions.le("activity.plannedEndDate", plannedEndDate)));
        } else if (plannedStartDate != null) {
            activityQuery.add(Restrictions.ge("activity.plannedStartDate", plannedStartDate));
        } else if (plannedEndDate != null) {
            activityQuery.add(Restrictions.le("activity.plannedEndDate", plannedEndDate));
        }

        if (plannedStartDate != null || plannedEndDate != null) {
            activityQuery.setProjection(Projections.property("activityId"));
            c.add(Restrictions.and(Subqueries.propertyIn("activityId", activityQuery)));
        }
        // end of WOR-10080 : wmadruga

        String comment = mas.getCommentText();
        if (comment != null && !comment.trim().equals("")) {
            c.createCriteria("commentText", "commentText");
            c.add(Restrictions.ilike("commentText.commentText", strPatternParser(comment)));
        }

        Long approvedByStaffId = mas.getApprovedByStaffId();
        if (approvedByStaffId != null) {
            c.add(Restrictions.eq("approvedByStaffId", approvedByStaffId));
        }

        Date fromApprovalDate = mas.getFromApprovalDate();
        if (fromApprovalDate != null) {
            c.add(Restrictions.ge("approvalDate", fromApprovalDate));
        }
        Date toApprovalDate = mas.getToApprovalDate();
        if (toApprovalDate != null) {
            c.add(Restrictions.le("approvalDate", toApprovalDate));
        }

        String approvalComments = mas.getApprovalComments();
        if (approvalComments != null) {
            c.add(Restrictions.ilike("approvalComments", strPatternParser(approvalComments)));
        }

        Long fromLocationId = mas.getFromLocationId();
        if (fromLocationId != null) {
            c.add(Restrictions.eq("fromLocationId", fromLocationId));
        }

        Long fromOrganizationId = mas.getFromOrganizationId();
        if (fromOrganizationId != null) {
            c.add(Restrictions.eq("fromOrganizationId", fromOrganizationId));
        }

        Long fromFacilityId = mas.getFromFacilityId();
        Long toFacilityId = mas.getToFacilityId();

        //log.info("MovementServiceBean search fromFacilityId:"+fromFacilityId);
        //log.info("MovementServiceBean search toFacilityId:"+toFacilityId);

        if (null != fromFacilityId && null != toFacilityId ) {
            if (MovementType.TRNIJ.isEqualsIgnoreCase(mas.getMovementType())) {

                c.add(Restrictions.eq("fromFacilityId", fromFacilityId));
                c.add(Restrictions.eq("toFacilityId", toFacilityId));
            } else {
                c.add(Restrictions.or(
                        Restrictions.eq("fromFacilityId", fromFacilityId),
                        Restrictions.eq("toFacilityId", toFacilityId)));

            }
        } else {

            if (fromFacilityId != null) {
                c.add(Restrictions.eq("fromFacilityId", fromFacilityId));
            }

            if (toFacilityId != null) {
                c.add(Restrictions.eq("toFacilityId", toFacilityId));
            }

        }

        String fromCity = mas.getFromCity();
        if (fromCity != null && !fromCity.trim().equals("")) {
            c.add(Restrictions.ilike("fromCity", strPatternParser(fromCity)));
        }

        String toCity = mas.getToCity();
        if (toCity != null && !toCity.trim().equals("")) {
            c.add(Restrictions.ilike("toCity", strPatternParser(toCity)));
        }

        String toProvState = mas.getToProvinceState();
        if (toProvState != null && !toProvState.trim().equals("")) {
            c.add(Restrictions.ilike("toProvinceState", strPatternParser(toProvState)));
        }

        Long toLocationId = mas.getToLocationId();
        if (toLocationId != null) {
            c.add(Restrictions.eq("toLocationId", toLocationId));
        }

        Long toOrganizationId = mas.getToOrganizationId();
        if (toOrganizationId != null) {
            c.add(Restrictions.eq("toOrganizationId", toOrganizationId));
        }

        Long arrestOrganizationId = mas.getArrestOrganizationId();
        if (arrestOrganizationId != null) {
            c.add(Restrictions.eq("arrestOrganizationId", arrestOrganizationId));
        }

        Date fromApplyDate = mas.getFromApplicationDate();
        Date toApplyDate = mas.getToApplicationDate();
        if (fromApplyDate != null && toApplyDate != null) {
            c.add(Restrictions.and(Restrictions.ge("applicationDate", fromApplyDate), Restrictions.le("applicationDate", toApplyDate)));
        } else if (fromApplyDate != null) {
            c.add(Restrictions.ge("applicationDate", fromApplyDate));
        } else if (toApplyDate != null) {
            c.add(Restrictions.le("applicationDate", toApplyDate));
        }

        Long escortOrganizationId = mas.getEscortOrganizationId();
        if (escortOrganizationId != null) {
            c.add(Restrictions.eq("escortOrganizationId", escortOrganizationId));
        }

        Date fromReportDate = mas.getFromReportingDate();
        Date toReportDate = mas.getToReportingDate();
        if (fromReportDate != null && toReportDate != null) {
            c.add(Restrictions.and(Restrictions.ge("reportingDate", fromReportDate), Restrictions.le("reportingDate", toReportDate)));
        } else if (fromReportDate != null) {
            c.add(Restrictions.ge("reportingDate", fromReportDate));
        } else if (toReportDate != null) {
            c.add(Restrictions.le("reportingDate", toReportDate));
        }

        Long fromInternalLocationId = mas.getFromFacilityInternalLocationId();
        Long toInternalLocationId = mas.getToFacilityInternalLocationId();
        String locationCategory = mas.getLocationCategory();

        //WOR-16988: based on location category to retrieve leaf locations.
        LocationCategory locCategory = LocationCategory.HOUSING;
        if (!BeanHelper.isEmpty(locationCategory)) {
            if (locationCategory.equalsIgnoreCase(LocationCategory.HOUSING.name())) {
                locCategory = LocationCategory.HOUSING;
            } else if (locationCategory.equalsIgnoreCase(LocationCategory.ACTIVITY.name())) {
                locCategory = LocationCategory.ACTIVITY;
            }

            if (fromInternalLocationId != null) {
                Set<Long> fromIds = MovementHelper.getLeafLocations(uc, fromInternalLocationId, locCategory, facilityInternalLocationService);
                if (fromIds != null && !fromIds.isEmpty()) {
                    c.add(Restrictions.in("fromInternalLocationId", fromIds));
                } else {
                    c.add(Restrictions.eq("fromInternalLocationId", fromInternalLocationId));
                }
            }

            if (toInternalLocationId != null) {
                Set<Long> toIds = MovementHelper.getLeafLocations(uc, toInternalLocationId, locCategory, facilityInternalLocationService);
                if (toIds != null && !toIds.isEmpty()) {
                    c.add(Restrictions.in("toInternalLocationId", toIds));
                } else {
                    c.add(Restrictions.eq("toInternalLocationId", toInternalLocationId));
                }
            }

        } else {
            if (fromInternalLocationId != null) {
                c.add(Restrictions.eq("fromInternalLocationId", fromInternalLocationId));
            }
            if (toInternalLocationId != null) {
                c.add(Restrictions.eq("toInternalLocationId", toInternalLocationId));
            }
        }

        Long totalSize = (Long) c.setProjection(Projections.countDistinct("movementId")).uniqueResult();

        c.setProjection(null);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            c.setFirstResult(startIndex.intValue());
            c.setMaxResults(resultSize.intValue());
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                c.addOrder(ord);
            }
        }

        @SuppressWarnings("unchecked")
        List<MovementActivityEntity> lst = c.list();

        //WOR-20391
        if (!lst.isEmpty() && mas.isFilterInOut()) {
            lst = filterScheduledInOutMovement(lst, mas);
        }


        MovementActivitiesReturnType ret = new MovementActivitiesReturnType();
        List<ExternalMovementActivityType> externalMovementActivities = new ArrayList<ExternalMovementActivityType>();
        List<InternalMovementActivityType> internalMovementActivities = new ArrayList<InternalMovementActivityType>();



        for (MovementActivityEntity entity : lst) {
            if (entity instanceof ExternalMovementActivityEntity) {
                externalMovementActivities.add((ExternalMovementActivityType) MovementHelper.toMovementActivityType(entity, ExternalMovementActivityType.class));
            } else if (entity instanceof InternalMovementActivityEntity) {
                internalMovementActivities.add((InternalMovementActivityType) MovementHelper.toMovementActivityType(entity, InternalMovementActivityType.class));
            }
        }
        if(!lst.isEmpty()){
            Set<Long>   movementActivityIds = new HashSet<>();
            for(MovementActivityEntity entity : lst){
                movementActivityIds.add(entity.getActivityId());

            }
//             movementActivityIds = lst.stream().map(m->m.getActivityId()).collect(Collectors.toSet());
            Criteria  actCrieria = session.createCriteria(ActivityEntity.class);
            actCrieria.add(Restrictions.in("activityId",movementActivityIds ));
            List<ActivityEntity>  activityEntities = actCrieria.list();
            Map<Long, ActivityEntity> activityEntityMap = new HashMap<>(activityEntities.size());
            for(ActivityEntity act: activityEntities){
                activityEntityMap.put(act.getActivityId(), act);
            }
            for(ExternalMovementActivityType  e: externalMovementActivities){
                if(MovementCategory.NOMOV.isEquals(e.getMovementCategory())){
                    e.setPlannedEndDate(activityEntityMap.get(e.getActivityId()).getPlannedEndDate());

                }

                if(MovementStatus.COMPLETED.isEquals(e.getMovementStatus())){
                    //movementDate will be taken as completed date
                    if(null!=activityEntityMap.get(e.getActivityId()).getScheduleID()){
                        //Schedule complete
                        e.setPlannedStartDate(activityEntityMap.get(e.getActivityId()).getPlannedStartDate());
                        e.setbAdhoc(false);
                    }else {
                        //Adhoc complete
                        e.setbAdhoc(true);
                        e.setPlannedStartDate(null);

                    }

                }

                e.setPlannedStartDate(activityEntityMap.get(e.getActivityId()).getPlannedStartDate());

            }

            for(InternalMovementActivityType  e : internalMovementActivities){

                if(MovementStatus.COMPLETED.isEquals(e.getMovementStatus())){
                    if(null!=activityEntityMap.get(e.getActivityId()).getScheduleID()){
                        //Schedule complete
                        e.setPlannedStartDate(activityEntityMap.get(e.getActivityId()).getPlannedStartDate());
                        e.setbAdhoc(false);
                    }else {
                        //Adhoc complete
                        e.setbAdhoc(true);
                        e.setPlannedStartDate(null);

                    }

                }

                e.setPlannedStartDate(activityEntityMap.get(e.getActivityId()).getPlannedStartDate());


            }


        }

        ret.setExternalMovementActivity(externalMovementActivities);
        ret.setInternalMovementActivity(internalMovementActivities);
        ret.setTotalSize(totalSize);

        if (log.isDebugEnabled()) {
            log.debug("search: end");
        }

        return ret;
    }

    //WOR-20391
    private List<MovementActivityEntity> filterScheduledInOutMovement(List<MovementActivityEntity> mAList, MovementActivitySearchType mas) {
        List<MovementActivityEntity> movActList = new ArrayList<MovementActivityEntity>();

        Map<String, MovementActivityEntity> inMap = new HashMap<String, MovementActivityEntity>();
        Map<String, MovementActivityEntity> outMap = new HashMap<String, MovementActivityEntity>();
        List<String> groupIdList = new ArrayList<String>();

        if (null != mas.getMovementDirection() && DIRECTION_IN.equals(mas.getMovementDirection())) {
            for (MovementActivityEntity entity : mAList) {
                String hql = "FROM MovementActivityEntity ma where ma.groupid = '" + entity.getGroupId() + "' and ma.movementId != "
                        + entity.getMovementId() + " and (ma.movementStatus = 'COMPLETED' or ma.movementStatus = 'CANCELLED') ";
                Query query2 = session.createQuery(hql);
                List results = query2.list();

                if (null != results && !results.isEmpty()) {
                    movActList.add(entity);
                }
            }
        } else {
            for (MovementActivityEntity entity : mAList) {

                if (DIRECTION_IN.equals(entity.getMovementDirection())) {
                    inMap.put(entity.getGroupId(), entity);
                    System.out.println("DIRECTION_IN : *** " + entity.getGroupId() + " : " + entity.getMovementId());
                }

                if (DIRECTION_OUT.equals(entity.getMovementDirection())) {
                    outMap.put(entity.getGroupId(), entity);
                    System.out.println("DIRECTION_OUT : *** " + entity.getGroupId() + " : " + entity.getMovementId());
                }
            }

            for (MovementActivityEntity entity : mAList) {
                if (inMap.containsKey(entity.getGroupId()) && outMap.containsKey(entity.getGroupId()) && entity.getMovementStatus().equals(STATUS_PENDING)) {
                    if (!groupIdList.contains(entity.getGroupId())) {
                        movActList.add(outMap.get(entity.getGroupId()));
                        groupIdList.add(entity.getGroupId());
                    }

                } else {
                    movActList.add(entity);
                }

            }
        }

        return movActList;
    }

    @Override
    public Set<LastCompletedSupervisionMovementType> getLastCompletedSupervisionMovementBySupervisions(UserContext uc, Set<Long> supervisionIds) {

        if (log.isDebugEnabled()) {
            log.debug("getLastCompletedSupervisionMovementBySupervisions: begin");
        }

        String functionName = "getLastCompletedSupervisionMovementBySupervisions";
        if (supervisionIds == null || supervisionIds.size() == 0) {
            String message = "Invalid argument: supervision(s) is/are required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Criteria c = session.createCriteria(LastCompletedSupervisionMovementEntity.class);
        c.add(Restrictions.in("supervisionId", supervisionIds));
        @SuppressWarnings("unchecked")
        Set<LastCompletedSupervisionMovementEntity> supervisionMovements = new HashSet<LastCompletedSupervisionMovementEntity>(c.list());

        Set<LastCompletedSupervisionMovementType> ret = new HashSet<LastCompletedSupervisionMovementType>();
        for (LastCompletedSupervisionMovementEntity m : supervisionMovements) {
            BeanHelper.getEntity(session, MovementActivityEntity.class, m.getMovementActivityId());
            LastCompletedSupervisionMovementType movement = new LastCompletedSupervisionMovementType(m.getSupervisionId(), get(uc, m.getMovementActivityId()),
                    m.getInternalLocationId(), m.getStamp(), m.getVersion());

            ret.add(movement);
        }

        if (log.isDebugEnabled()) {
            log.debug("getLastCompletedSupervisionMovementBySupervisions: end");
        }

        return ret;
    }

    @Override
    public LastCompletedSupervisionMovementType getLastCompletedSupervisionMovementByPersonIdentityId(UserContext uc, Long personIdentityId) {
        SupervisionSearchType search = new SupervisionSearchType();
        search.setPersonIdentityId(personIdentityId);
        List<SupervisionType> supervisionTypeList = MovementHelper.searchSupervision(uc, search);
        if (supervisionTypeList.isEmpty()) {
            return null;
        }

        Set<Long> supervisionIds = new HashSet<>();
        for (SupervisionType supervision: supervisionTypeList) {
            supervisionIds.add(supervision.getSupervisionIdentification());
        }

        Criteria c = session.createCriteria(LastCompletedSupervisionMovementEntity.class);
        c.add(Restrictions.in("supervisionId", supervisionIds));

        @SuppressWarnings("unchecked")
        Set<LastCompletedSupervisionMovementEntity> supervisionMovements = new HashSet<LastCompletedSupervisionMovementEntity>(c.list());

        for (LastCompletedSupervisionMovementEntity m : supervisionMovements) {
            BeanHelper.getEntity(session, MovementActivityEntity.class, m.getMovementActivityId());
            LastCompletedSupervisionMovementType movement = new LastCompletedSupervisionMovementType(m.getSupervisionId(), get(uc, m.getMovementActivityId()),
                    m.getInternalLocationId(), m.getStamp(), m.getVersion());

            return movement;
        }

        return null;
    }

    @Override
    public Map<Long, LastCompletedSupervisionMovementType> getLastCompletedSupervisionMovementByPersonIdentityIds(UserContext uc, Set<Long> personIdentityIds) {

        if (log.isDebugEnabled()) {
            log.debug("getLastCompletedSupervisionMovementByPersonIdentityIds: begin");
        }

        //Key:supervisionId, Value: personIdentityId
        Map<Long, Long> supToPersonIdentiryIdMap = new HashMap<>();

        SupervisionSearchType searchType = new SupervisionSearchType();
        searchType.setSupervisionStatusFlag(true);
        for (Long id : personIdentityIds) {
            searchType.setPersonIdentityId(id);
            List<SupervisionType> supervisionTypeList = MovementHelper.searchSupervision(uc, searchType);
            if (supervisionTypeList != null && supervisionTypeList.size() > 0) {
                Long superId = supervisionTypeList.get(0).getSupervisionIdentification();
                supToPersonIdentiryIdMap.put(superId, id);
            }
        }

        Set<Long> supervisionIds = supToPersonIdentiryIdMap.keySet();
        if (supervisionIds.size() == 0) {
            return null;
        }

        Criteria c = session.createCriteria(LastCompletedSupervisionMovementEntity.class);
        c.add(Restrictions.in("supervisionId", supervisionIds));

        @SuppressWarnings("unchecked")
        Set<LastCompletedSupervisionMovementEntity> supervisionMovements = new HashSet<LastCompletedSupervisionMovementEntity>(c.list());

        Map<Long, LastCompletedSupervisionMovementType> ret = new HashMap<>();

        for (LastCompletedSupervisionMovementEntity m : supervisionMovements) {
            BeanHelper.getEntity(session, MovementActivityEntity.class, m.getMovementActivityId());
            LastCompletedSupervisionMovementType movement = new LastCompletedSupervisionMovementType(m.getSupervisionId(), get(uc, m.getMovementActivityId()),
                    m.getInternalLocationId(), m.getStamp(), m.getVersion());

            ret.put(supToPersonIdentiryIdMap.get(m.getSupervisionId()), movement);
        }

        if (log.isDebugEnabled()) {
            log.debug("getLastCompletedSupervisionMovementByPersonIdentityIds: end");
        }

        return ret;
    }

    @Override
    public Set<LastCompletedSupervisionMovementType> getLastCompletedSupervisionMovementByLocations(UserContext uc, Set<Long> facilityInternalLocationIds) {

        if (log.isDebugEnabled()) {
            log.debug("getLastCompletedSupervisionMovementByLocations: begin");
        }

        String functionName = "getLastCompletedSupervisionMovementBySupervisions";
        if (facilityInternalLocationIds == null || facilityInternalLocationIds.size() == 0) {
            String message = "Invalid argument: Facility Internal Location(s) is/are required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Criteria c = session.createCriteria(LastCompletedSupervisionMovementEntity.class);
        c.add(Restrictions.in("internalLocationId", facilityInternalLocationIds));
        @SuppressWarnings("unchecked")
        Set<LastCompletedSupervisionMovementEntity> supervisionMovements = new HashSet<LastCompletedSupervisionMovementEntity>(c.list());

        Set<LastCompletedSupervisionMovementType> ret = new HashSet<LastCompletedSupervisionMovementType>();
        for (LastCompletedSupervisionMovementEntity m : supervisionMovements) {
            MovementActivityEntity mae = BeanHelper.findEntity(session, MovementActivityEntity.class, m.getMovementActivityId());
            LastCompletedSupervisionMovementType movement = new LastCompletedSupervisionMovementType(m.getSupervisionId(),
                    mae == null ? null : get(uc, m.getMovementActivityId()), m.getInternalLocationId(), m.getStamp(), m.getVersion());

            ret.add(movement);
            FacilityInternalLocation facilityInternalLocation = facilityInternalLocationService.get(uc, movement.getCurrentInternalLocationId());
            if (null != facilityInternalLocation) {
                movement.setCurrentLocation(facilityInternalLocation.getDescription());
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("getLastCompletedSupervisionMovementByLocations: end");
        }

        return ret;
    }

    @Override
    public MovementActivityType getLastCompletedSupervisionMovement(UserContext uc, Long supervisionId) {

        LastCompletedSupervisionMovementEntity lcsmEntity = (LastCompletedSupervisionMovementEntity)session.get(LastCompletedSupervisionMovementEntity.class, supervisionId);
        if (lcsmEntity == null) {
            return null;
        }
        MovementActivityEntity mEntity = (MovementActivityEntity)session.get(MovementActivityEntity.class, lcsmEntity.getMovementActivityId());
        if (mEntity == null) {
            return null;
        }

        return MovementHelper.toMovementActivityType(mEntity);
    }
    @Override
    public TransferWaitlistType addTransferWaitlistEntry(UserContext uc, Long facilityId, Set<TransferWaitlistEntryType> transferWaitlistEntries) {

        if (log.isDebugEnabled()) {
            log.debug("addTransferWaitlist: begin");
        }

        String functionName = "addTransferWaitlistEntry";
        if (facilityId == null) {
            String message = "Invalid argument: facilityId is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(transferWaitlistEntries);

        TransferWaitlistType ret = new TransferWaitlistType();
        ret.setFacilityId(facilityId);
        for (TransferWaitlistEntryType entry : transferWaitlistEntries) {
            Long supervision = entry.getSupervisionId();
            TransferWaitlistEntryPK entryId = new TransferWaitlistEntryPK(facilityId, supervision);

            TransferWaitlistEntryEntity entryEntity = BeanHelper.findEntity(session, TransferWaitlistEntryEntity.class, entryId);
            if (entryEntity != null) {
                log.error("Invalid argument: Offender " + supervision + " is already within the waitlist " + facilityId);
                log.debug("For a given facility’s transfer waitlist, an offender can only be listed once within the waitlist.");
                throw new InvalidInputException("Invalid argument: Offender " + supervision + " is already within the waitlist " + facilityId);
            }

            entryEntity = toTransferWaitlistEntryEntity(facilityId, entry);
            ValidationHelper.verifyMetaCodes(entryEntity, true);
            session.persist(entryEntity);
            ret.getTransferWaitlistEntries().add(toTransferWaitlistEntryType(entryEntity));
        }

        if (log.isDebugEnabled()) {
            log.debug("addTransferWaitlist: end");
        }

        return ret;
    }

    @Override
    public TransferWaitlistType updateTransferWaitlistEntry(UserContext uc, Long facilityId, Set<TransferWaitlistEntryType> transferWaitlistEntries) {

        if (log.isDebugEnabled()) {
            log.debug("updateTransferWaitlistEntry: begin");
        }

        ValidationHelper.validate(transferWaitlistEntries);

        String functionName = "updateTransferWaitlistEntry";
        if (facilityId == null) {
            String message = "Invalid argument: facilityId is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        TransferWaitlistType ret = new TransferWaitlistType();
        ret.setFacilityId(facilityId);
        Set<TransferWaitlistEntryEntity> newEntries = toTransferWaitlistEntryEntitySet(facilityId, transferWaitlistEntries);
        if (newEntries != null && !newEntries.isEmpty()) {
            for (TransferWaitlistEntryEntity newEntry : newEntries) {
                TransferWaitlistEntryPK entryId = new TransferWaitlistEntryPK(facilityId, newEntry.getSupervision());
                TransferWaitlistEntryEntity currEntry = BeanHelper.findEntity(session, TransferWaitlistEntryEntity.class, entryId);
                if (currEntry != null) {
                    currEntry.setDateAdded(newEntry.getDateAdded());
                    currEntry.setPriority(newEntry.getPriority());
                    currEntry.setToFacility(newEntry.getToFacility());
                    currEntry.setTransferReason(newEntry.getTransferReason());
                    currEntry.setStamp(newEntry.getStamp());
                    currEntry.setVersion(newEntry.getVersion());
                    currEntry = (TransferWaitlistEntryEntity) session.merge(currEntry);
                    ret.getTransferWaitlistEntries().add(this.toTransferWaitlistEntryType(currEntry));
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("updateTransferWaitlistEntry: end");
        }

        return ret;
    }

    @Override
    public void deleteTransferWaitlistEntry(UserContext uc, Long facilityId, Set<Long> supervisionIds) {

        log.info("deleteTransferWaitlistEntry: begin");
        String functionName = "deleteTransferWaitlistEntry";
        if (facilityId == null || (supervisionIds == null || supervisionIds.size() == 0)) {
            String message = "Invalid argument: facility and supervisionIds are required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        TransferWaitlistType ret = new TransferWaitlistType();
        ret.setFacilityId(facilityId);
        for (Long supervision : supervisionIds) {
            TransferWaitlistEntryPK id = new TransferWaitlistEntryPK(facilityId, supervision);
            TransferWaitlistEntryEntity entry = BeanHelper.findEntity(session, TransferWaitlistEntryEntity.class, id);
            if (entry != null) {
                session.delete(entry);
                session.flush();
            }
        }
        log.info("deleteTransferWaitlistEntry: end");
        return;
    }

    @Override
    public TransferWaitlistType getTransferWaitlist(UserContext uc, Long facilityId) {

        if (log.isDebugEnabled()) {
            log.debug("getTransferWaitlist: begin");
        }

        String functionName = "getTransferWaitlist";
        if (facilityId == null) {
            String message = "Invalid argument: facility is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Criteria criteria = session.createCriteria(TransferWaitlistEntryEntity.class);
        criteria.add(Restrictions.eq("transFacilityId", facilityId));
        @SuppressWarnings("unchecked")
        List<TransferWaitlistEntryEntity> list = criteria.list();

        TransferWaitlistType ret = new TransferWaitlistType();
        ret.setFacilityId(facilityId);
        ret.setTransferWaitlistEntries(toTransferWaitlistEntryTypeSet(new HashSet<>(list)));

        if (log.isDebugEnabled()) {
            log.debug("getTransferWaitlist: end");
        }

        return ret;
    }

    @Override
    public TransferWaitlistEntryType getTransferWaitlistEntry(UserContext uc, Long facilityId, Long supervisionId) {

        if (log.isDebugEnabled()) {
            log.debug("getTransferWaitlistEntry: begin");
        }

        String functionName = "getTransferWaitlistEntry";
        if (facilityId == null) {
            String message = "Invalid argument: facility is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        TransferWaitlistEntryPK id = new TransferWaitlistEntryPK(facilityId, supervisionId);
        TransferWaitlistEntryEntity entry = BeanHelper.getEntity(session, TransferWaitlistEntryEntity.class, id);

        TransferWaitlistEntryType ret = toTransferWaitlistEntryType(entry);

        if (log.isDebugEnabled()) {
            log.debug("getTransferWaitlistEntry: end");
        }

        return ret;
    }

    @Override
    public void indexDataBase() {
        FullTextSession fullTextSession = Search.getFullTextSession(session);
        fullTextSession.createIndexer(TransferWaitListDatagridViewEntity.class).start();
        fullTextSession.flushToIndexes();
    }

    @Override
    public TransferWaitlistGridReturnType searchTransferWaitlistGrid(UserContext uc, TransferWaitlistSearchType search, Long startIndex, Long resultSize,
                                                                     String resultOrder, String strSearchWords) {
        if (log.isDebugEnabled()) {
            log.debug("searchTransferWaitlistGrid: begin");
        }

        ValidationHelper.validateSearchType(search);

        TransferWaitlistGridReturnType ret = new TransferWaitlistGridReturnType();

        if (null != strSearchWords && !strSearchWords.trim().isEmpty()) {

            String searchKeyWords;
            List<TransferWaitListDatagridViewEntity> result;
            searchKeyWords = strSearchWords.trim().toUpperCase();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            // Get Query Builder

            QueryBuilder qb = fullTextSession.getSearchFactory().buildQueryBuilder().forEntity(TransferWaitListDatagridViewEntity.class).get();
            org.apache.lucene.search.Query luceneQuery = qb.bool().should(
                    qb.keyword().onFields("firstName", "lastName", "offenderNumber", "name", "transferReason", "priority").matching(searchKeyWords).createQuery()).should(
                    qb.keyword().onField("dateAdded").ignoreAnalyzer().matching(searchKeyWords).createQuery()).createQuery();

            org.hibernate.search.FullTextQuery query = fullTextSession.createFullTextQuery(luceneQuery, TransferWaitListDatagridViewEntity.class);

            query.enableFullTextFilter("facility").setParameter("facilityId", search.getFacilityId());

            //Pagination
            if (startIndex != null && resultSize != null && startIndex >= 0L && resultSize > 0L) {
                query.setFirstResult(startIndex.intValue());
                query.setMaxResults(resultSize.intValue());
            }

            //Sorting
            if (resultOrder != null && resultOrder.trim().length() > 0) {

                List<Order> orders = BeanHelper.toOrderCriteria(resultOrder);
                List<org.apache.lucene.search.SortField> fields = new ArrayList<>();

                for (Order order : orders) {
                    String[] strArray = order.toString().split(" ");

                    switch (strArray[0]) {
                        case "name":
                            fields.add(new SortField("facilityName", SortField.STRING, strArray[1].equalsIgnoreCase("ASC") ? false : true));
                            break;

                        case "transferReason":
                            fields.add(new SortField("reason", SortField.STRING, strArray[1].equalsIgnoreCase("ASC") ? false : true));
                            break;
                        case "priority":
                            fields.add(new SortField("transPriority", SortField.STRING, strArray[1].equalsIgnoreCase("ASC") ? false : true));
                            break;

                        default:
                            fields.add(new SortField(strArray[0], SortField.STRING, strArray[1].equalsIgnoreCase("ASC") ? false : true));
                            break;

                    }

                }
                org.apache.lucene.search.SortField[] sortFields = fields.toArray(new org.apache.lucene.search.SortField[fields.size()]);
                org.apache.lucene.search.Sort sort = new Sort(sortFields);
                query.setSort(sort);
            }

            result = query.list();

            List<TransferWaitlistGridEntryType> tmp = new ArrayList<>();

            for (TransferWaitListDatagridViewEntity v : result) {

                tmp.add(new TransferWaitlistGridEntryType(v.getFirstName(), v.getLastName(), v.getOffenderNumber(), v.getName(), v.getDateAdded(), v.getTransferReason(),
                        v.getPriority(), v.getToFacility(), v.getFromFacility(), v.getSupervisionId()));

            }

            ret.setTransferWaitlists(tmp);
            ret.setTotalSize(new Long(query.getResultSize()));

        } else {

            StringBuilder hqlTotal = new StringBuilder();
            hqlTotal.append("select count(*) from TransferWaitListDatagridViewEntity wa ");
            hqlTotal.append("WHERE wa.fromFacility = :fromFacility ");
            Query qryTotal = session.createQuery(hqlTotal.toString());
            qryTotal.setParameter("fromFacility", search.getFacilityId());

            Long totalSize = (Long) qryTotal.uniqueResult();

            StringBuilder hqlFormat = new StringBuilder();

            //            hqlFormat.append("SELECT new %s(pi.firstName, pi.lastName, pi.offenderNumber,fac.facilityName, wa.dateAdded, wa.transferReason, wa.priority,wa.toFacility) ");
            //            hqlFormat.append("FROM TransferWaitlistEntryEntity wa, PersonIdentityEntity pi, SupervisionEntity sup, FacilityEntity fac ");
            //            hqlFormat.append("WHERE wa.transFacilityId = :fromFacility ");
            //            hqlFormat.append("AND wa.supervision = sup.supervisionIdentification ");
            //            hqlFormat.append("AND sup.personIdentityId = pi.personIdentityId ");
            //            hqlFormat.append("AND wa.toFacility = fac.facilityId ");
            //            hqlFormat.append("AND wa.toFacility = fac.facilityId ");

            hqlFormat.append(
                    "SELECT new %s(wa.firstName, wa.lastName,wa.offenderNumber,wa.name,wa.dateAdded,wa.transferReason,wa.priority,wa.toFacility,wa.fromFacility,wa.supervisionId) ");
            hqlFormat.append("FROM TransferWaitListDatagridViewEntity wa ");
            hqlFormat.append("WHERE wa.fromFacility = :fromFacility ");

            //Result order
            if (resultOrder != null && resultOrder.trim().length() > 0) {
                hqlFormat.append("ORDER BY ");
                List<Order> orders = BeanHelper.toOrderCriteria(resultOrder);
                for (Order order : orders) {
                    hqlFormat.append(order.toString());
                    hqlFormat.append(",");

                }
                hqlFormat.deleteCharAt(hqlFormat.length() - 1);
            }

            String sql = String.format(hqlFormat.toString(), TransferWaitlistGridEntryType.class.getName());

            Query query = session.createQuery(sql);
            query.setParameter("fromFacility", search.getFacilityId());

            //Pagination
            if (startIndex != null && resultSize != null && startIndex >= 0L && resultSize > 0L) {
                query.setFirstResult(startIndex.intValue());
                query.setMaxResults(resultSize.intValue());
            }

            List<TransferWaitlistGridEntryType> result = query.list();
            ret.setTransferWaitlists(result);
            ret.setTotalSize(totalSize);

        }

        return ret;
    }

    @Override
    public TransferWaitlistsReturnType searchTransferWaitlist(UserContext uc, TransferWaitlistSearchType search, Long startIndex, Long resultSize, String resultOrder) {
        if (log.isDebugEnabled()) {
            log.debug("searchTransferWaitlist: begin");
        }

        ValidationHelper.validateSearchType(search);

        if (resultSize == null || resultSize > SEARCH_MAX_LIMIT) {
            resultSize = Long.valueOf(SEARCH_MAX_LIMIT);
        }

        Criteria c = session.createCriteria(TransferWaitlistEntryEntity.class);
        //		c.setMaxResults(SEARCH_MAX_LIMIT + 1);
        //		c.setFetchSize(SEARCH_MAX_LIMIT  + 1); // keeping it for a better performance
        //http://benjisimon.blogspot.ca/2007/07/hibernate-performance-tip-setfetchsize.html

        Long facilityId = search.getFacilityId();
        if (facilityId != null) {
            c.add(Restrictions.eq("transFacilityId", facilityId));
        }

        Long supervision = search.getSupervisionId();
        if (supervision != null) {
            c.add(Restrictions.eq("supervision", supervision));
        }

        Long toFacility = search.getToFacilityId();
        if (toFacility != null) {
            c.add(Restrictions.eq("toFacility", toFacility));
        }

        Date fromDateAdded = search.getFromDateAdded();
        if (fromDateAdded != null) {
            c.add(Restrictions.ge("dateAdded", fromDateAdded));
        }

        Date toDateAdded = search.getToDateAdded();
        if (toDateAdded != null) {
            c.add(Restrictions.le("dateAdded", toDateAdded));
        }

        String priority = search.getPriority();
        if (!BeanHelper.isEmpty(priority)) {
            c.add(Restrictions.eq("priority", priority));
        }

        String reason = search.getTransferReason();
        if (!BeanHelper.isEmpty(reason)) {
            c.add(Restrictions.eq("transferReason", reason));
        }

        Long totalSize = (Long) c.setProjection(Projections.rowCount()).uniqueResult();

        c.setProjection(null);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            c.setFirstResult(startIndex.intValue());
            c.setMaxResults(resultSize.intValue());
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                c.addOrder(ord);
            }
        }

        @SuppressWarnings("unchecked")
        List<TransferWaitlistEntryEntity> entriesEntity = c.list();

        Map<Long, Set<TransferWaitlistEntryType>> map = new HashMap<>();
        for (TransferWaitlistEntryEntity entryEntity : entriesEntity) {
            Long k = entryEntity.getTransFacilityId();
            TransferWaitlistEntryType entry = toTransferWaitlistEntryType(entryEntity);
            if (entry == null || k == null) {
                continue;
            }

            Set<TransferWaitlistEntryType> v = map.get(k);
            if (BeanHelper.isEmpty(v)) {
                v = new HashSet<>();
                v.add(entry);
                map.put(k, v);
            } else {
                v.add(entry);
                map.put(k, v);
            }
        }
        Set<Long> ids = map.keySet();
        List<TransferWaitlistType> lst = new ArrayList<>();
        for (Long id : ids) {
            lst.add(new TransferWaitlistType(id, map.get(id)));
        }

        TransferWaitlistsReturnType ret = new TransferWaitlistsReturnType();
        ret.setTransferWaitlists(lst);
        ret.setTotalSize(totalSize);

        if (log.isDebugEnabled()) {
            log.debug("searchTransferWaitlist: end");
        }

        return ret;
    }

    @Override
    public CourtMovementReturnTimeConfigurationType setCourtMovementReturnTimeConfiguration(UserContext uc, CourtMovementReturnTimeConfigurationType configuration) {

        if (log.isDebugEnabled()) {
            log.debug("setCourtMovementReturnTimeConfiguration: begin");
        }
        ValidationHelper.validate(configuration);
        String returnTime = configuration.getReturnTime();

        if (returnTime == null || !Pattern.compile("^[0-9]?[0-9]:[0-9][0-9]$").matcher(returnTime).find() || Integer.valueOf(returnTime.split(":")[0]) < 0
                || Integer.valueOf(returnTime.split(":")[0]) >= 24 || Integer.valueOf(returnTime.split(":")[1]) < 0 || Integer.valueOf(returnTime.split(":")[1]) >= 60) {

            String message = "Invalid argument: returnTime: " + returnTime;
            LogHelper.error(log, "setCourtMovementReturnTimeConfiguration", message);
            throw new InvalidInputException(message);
        }

        session.createQuery("delete CourtMovementReturnTimeConfigEntity").executeUpdate();

        CourtMovementReturnTimeConfigEntity config = new CourtMovementReturnTimeConfigEntity();
        config.setReturnTime(configuration.getReturnTime());

        session.save(config);

        if (log.isDebugEnabled()) {
            log.debug("setCourtMovementReturnTimeConfiguration: end");
        }

        return configuration;
    }

    @Override
    public CourtMovementReturnTimeConfigurationType getCourtMovementReturnTimeConfiguration(UserContext uc) {

        if (log.isDebugEnabled()) {
            log.debug("getCourtMovementReturnTimeConfiguration: begin");
        }

        Criteria c = session.createCriteria(CourtMovementReturnTimeConfigEntity.class);
        c.setMaxResults(1);
        CourtMovementReturnTimeConfigEntity config = (CourtMovementReturnTimeConfigEntity) c.uniqueResult();

        CourtMovementReturnTimeConfigurationType ret = new CourtMovementReturnTimeConfigurationType();
        if (config == null) {
            ret.setReturnTime("16:00");
        } else {
            ret.setReturnTime(config.getReturnTime());
            ret.setStamp(config.getStamp());
            ret.setVersion(config.getVersion());
        }

        if (log.isDebugEnabled()) {
            log.debug("getCourtMovementReturnTimeConfiguration: end");
        }

        return ret;
    }

    @Override
    public void setSupervisionClosureConfiguration(UserContext uc, Set<SupervisionClosureConfigurationType> configurations) {

        if (log.isDebugEnabled()) {
            log.debug("setSupervisionClosureConfiguration: begin");
        }

        ValidationHelper.validate(configurations);

        Set<SupervisionClosureConfigurationType> ret = new HashSet<SupervisionClosureConfigurationType>();

        for (SupervisionClosureConfigurationType conf : configurations) {
            SupervisionClosureConfigurationEntity config = findSuperClosureConfig(conf.getMovementType(), conf.getMovementReason());
            if (config != null) {
                ValidationHelper.verifyMetaCodes(config, false);
                config.setIsCloseSupervisionFlag(conf.getIsCloseSupervisionFlag());
                config.setIsCleanSchedulesFlag(conf.getIsCleanSchedulesFlag());
                config.setIsCloseLegalFlag(conf.getIsCloseLegalFlag());
                config.setIsReleaseBedFlag(conf.getIsReleaseBedFlag());
                config.setDefaultAdmissionReason(conf.getDefaultAdmissionReason());
                config.setStamp(conf.getStamp());
                config.setVersion(conf.getVersion());
                session.update(config);
                ret.add(conf);
            } else {
                SupervisionClosureConfigurationEntity entity = new SupervisionClosureConfigurationEntity();
                entity.setMovementType(conf.getMovementType());
                entity.setMovementReason(conf.getMovementReason());
                entity.setIsCloseSupervisionFlag(conf.getIsCloseSupervisionFlag());
                entity.setIsCleanSchedulesFlag(conf.getIsCleanSchedulesFlag());
                entity.setIsCloseLegalFlag(conf.getIsCloseLegalFlag());
                entity.setIsReleaseBedFlag(conf.getIsReleaseBedFlag());
                entity.setDefaultAdmissionReason(conf.getDefaultAdmissionReason());
                entity.setStamp(conf.getStamp());
                ValidationHelper.verifyMetaCodes(entity, true);
                session.persist(entity);
                ret.add(conf);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("setSupervisionClosureConfiguration: end");
        }
    }

    @Override
    public SupervisionClosureConfigurationType getSupervisionClosureConfiguration(UserContext uc, MovementActivityType.MovementType movementType, String movementReason) {
        String functionName = "getSupervisionClosureConfiguration";
        if (movementType == null || movementReason == null || movementReason.trim().isEmpty()) {
            String message = "movementType and movementReason are required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        SupervisionClosureConfigurationEntity conf = findSuperClosureConfig(movementType.code(), movementReason);
        return conf == null? null : new SupervisionClosureConfigurationType(
                conf.getMovementType(), conf.getMovementReason(), conf.getIsCloseSupervisionFlag(),
                conf.getIsCleanSchedulesFlag(), conf.getIsCloseLegalFlag(), conf.getIsReleaseBedFlag(),
                conf.getDefaultAdmissionReason(), conf.getStamp(), conf.getVersion());
    }

    @Override
    public SupervisionClosureConfigurationsReturnType searchSupervisionClosureConfiguration(UserContext uc, SupervisionClosureConfigurationSearchType search,
                                                                                            Long startIndex, Long resultSize, String resultOrder) {

        if (log.isDebugEnabled()) {
            log.debug("searchSupervisionClosureConfiguration: begin");
        }

        ValidationHelper.validateSearchType(search);
        if (resultSize == null || resultSize > SEARCH_MAX_LIMIT) {
            resultSize = Long.valueOf(SEARCH_MAX_LIMIT);
        }

        Criteria c = session.createCriteria(SupervisionClosureConfigurationEntity.class);

        String movementType = search.getMovementType();
        if (movementType != null) {
            c.add(Restrictions.eq("movementType", movementType));
        }

        String movementReason = search.getMovementReason();
        if (movementReason != null) {
            c.add(Restrictions.ilike("movementReason", movementReason));
        }

        Boolean isCloseSupervisionFlag = search.getIsCloseSupervisionFlag();
        if (isCloseSupervisionFlag != null) {
            c.add(Restrictions.eq("isCloseSupervisionFlag", isCloseSupervisionFlag));
        }

        Boolean isCleanScheulesFlag = search.getIsCleanSchedulesFlag();

        if (isCleanScheulesFlag != null) {
            c.add(Restrictions.eq("isCleanSchedulesFlag", isCleanScheulesFlag));
        }

        Boolean isCloseLegalsFlag = search.getIsCloseLegalsFlag();
        if (isCloseLegalsFlag != null) {
            c.add(Restrictions.eq("isCloseLegalFlag", isCloseLegalsFlag));
        }

        Boolean isReleaseBedFlag = search.getIsReleaseBedFlag();
        if (isReleaseBedFlag != null) {
            c.add(Restrictions.eq("isReleaseBedFlag", isReleaseBedFlag));
        }

        String defaultAdmissionReason = search.getDefaultAdmissionReason();
        if (defaultAdmissionReason != null) {
            c.add(Restrictions.eq("defaultAdmissionReason", defaultAdmissionReason));
        }

        Long totalSize = (Long) c.setProjection(Projections.countDistinct("id")).uniqueResult();
        c.setProjection(null);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        @SuppressWarnings("unchecked")
        List<SupervisionClosureConfigurationEntity> configs = c.list();

        List<SupervisionClosureConfigurationType> lst = new ArrayList<SupervisionClosureConfigurationType>();
        for (SupervisionClosureConfigurationEntity conf : configs) {
            lst.add(new SupervisionClosureConfigurationType(conf.getMovementType(), conf.getMovementReason(), conf.getIsCloseSupervisionFlag(),
                    conf.getIsCleanSchedulesFlag(), conf.getIsCloseLegalFlag(), conf.getIsReleaseBedFlag(), conf.getDefaultAdmissionReason(), conf.getStamp(), conf.getVersion()));
        }

        SupervisionClosureConfigurationsReturnType ret = new SupervisionClosureConfigurationsReturnType();
        ret.setSupervisionClosureConfigurations(lst);
        ret.setTotalSize(totalSize);

        if (log.isDebugEnabled()) {
            log.debug("searchSupervisionClosureConfiguration: end");
        }

        return ret;
    }

    @Override
    public void setTransferWaitlistPriorityConfiguration(UserContext uc, Set<TransferWaitlistPriorityConfigurationType> configurations) {

        if (log.isDebugEnabled()) {
            log.debug("setTransferWaitlistPriorityConfiguration: begin");
        }

        ValidationHelper.validate(configurations);

        for (TransferWaitlistPriorityConfigurationType conf : configurations) {
            String entryPriority = conf.getTransferWaitlistEntryPriority();
            Long level = conf.getPriorityLevel();
            TransferWaitlistPriorityConfigEntity configEntity = findTransferWaitlistPriorityConfigEntity(entryPriority);
            if (configEntity != null) {
                configEntity.setPriorityLevel(level);
                ValidationHelper.verifyMetaCodes(configEntity, true);
                configEntity.setStamp(conf.getStamp());
                configEntity.setVersion(conf.getVersion());
                session.update(configEntity);
            } else {
                configEntity = new TransferWaitlistPriorityConfigEntity();
                configEntity.setTransferWaitlistEntryPriority(entryPriority);
                configEntity.setPriorityLevel(level);
                session.save(configEntity);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("setTransferWaitlistPriorityConfiguration: end");
        }
    }

    @Override
    public TransferWaitlistPrioritiesConfigurationReturnType searchTransferWaitlistPriorityConfiguration(UserContext uc,
                                                                                                         TransferWaitlistPriorityConfigurationSearchType search, Long startIndex, Long resultSize, String resultOrder) {

        if (log.isDebugEnabled()) {
            log.debug("searchTransferWaitlistPriorityConfiguration: begin");
        }

        ValidationHelper.validateSearchType(search);

        if (resultSize == null || resultSize > SEARCH_MAX_LIMIT) {
            resultSize = new Long(SEARCH_MAX_LIMIT);
        }

        Criteria c = session.createCriteria(TransferWaitlistPriorityConfigEntity.class);

        String priority = search.getTransferWaitlistEntryPriority();
        if (priority != null) {
            c.add(Restrictions.ilike("transferWaitlistEntryPriority", priority));
        }

        Long level = search.getPriorityLevel();
        if (level != null) {
            c.add(Restrictions.eq("priorityLevel", level));
        }

        Long totalSize = (Long) c.setProjection(Projections.countDistinct("id")).uniqueResult();

        c.setProjection(null);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            c.setFirstResult(startIndex.intValue());
            c.setMaxResults(resultSize.intValue());
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                c.addOrder(ord);
            }
        }

        @SuppressWarnings("unchecked")
        List<TransferWaitlistPriorityConfigEntity> entities = c.list();

        List<TransferWaitlistPriorityConfigurationType> lst = new ArrayList<TransferWaitlistPriorityConfigurationType>();
        for (TransferWaitlistPriorityConfigEntity entity : entities) {
            lst.add(new TransferWaitlistPriorityConfigurationType(entity.getTransferWaitlistEntryPriority(), entity.getPriorityLevel()));
        }

        TransferWaitlistPrioritiesConfigurationReturnType ret = new TransferWaitlistPrioritiesConfigurationReturnType();
        ret.setTransferWaitlistPriorityConfiguration(lst);
        ret.setTotalSize(totalSize);

        if (log.isDebugEnabled()) {
            log.debug("searchTransferWaitlistPriorityConfiguration: end");
        }

        return ret;
    }

    @Override
    public Long deleteAllTransferWaitlist(UserContext uc) {
        session.createQuery("delete TransferWaitlistEntryEntity").executeUpdate();
        // session.createQuery("delete CourtMovementReturnTimeConfigEntity").executeUpdate();
        // session.createQuery("delete SupervisionClosureConfigurationEntity").executeUpdate();
        session.createQuery("delete TransferWaitlistPriorityConfigEntity").executeUpdate();

        return success;
    }

    /**
     * getNiem -- Get NIEM (National Information Exchange Model) of a Movement
     * Activity specified by Id.
     *
     * @param uc UserContext
     * @param id java.lang.Long Id of a Movement Activity
     * @return java.lang.String
     */
    @Override
    public String getNiem(UserContext uc, Long id) {
        return null;
    }

    /**
     * getStamp -- Get Stamp of a Movement Activity.
     *
     * @param uc UserContext
     * @param id java.lang.Long Id of a Movement Activity
     * @return StampType
     */
    @Override
    public StampType getStamp(UserContext uc, Long id) {
        if (log.isDebugEnabled()) {
            log.debug("getStamp: begin ... ...");
        }

        StampType ret = new StampType();

        MovementActivityEntity ma = BeanHelper.getEntity(session, MovementActivityEntity.class, id);

        ret.setCreateUserId(ma.getStamp().getCreateUserId());
        ret.setCreateDateTime(ma.getStamp().getCreateDateTime());
        ret.setModifyUserId(ma.getStamp().getModifyUserId());
        ret.setModifyDateTime(ma.getStamp().getModifyDateTime());
        ret.setInvocationContext(ma.getStamp().getInvocationContext());

        if (log.isDebugEnabled()) {
            log.debug("getStamp: begin ... ...");
        }

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getVersion(UserContext uc) {
        return "2.0";
    }

    private Set<Long> getAllInternalMovementActivities(UserContext uc) {
        if (log.isDebugEnabled()) {
            log.debug("getAllInternalMovementActivities: begin");
        }

        Set<Long> ret = BeanHelper.getAll(uc, context, session, InternalMovementActivityEntity.class);

        if (log.isDebugEnabled()) {
            log.debug("getAllInternalMovementActivities: end");
        }

        return ret;
    }

    private Set<Long> getAllExternalMovementActivities(UserContext uc) {
        if (log.isDebugEnabled()) {
            log.debug("getAllExternalMovementActivities: begin");
        }

        Set<Long> ret = BeanHelper.getAll(uc, context, session, ExternalMovementActivityEntity.class);

        if (log.isDebugEnabled()) {
            log.debug("getAllExternalMovementActivities: end");
        }

        return ret;
    }

    private SupervisionClosureConfigurationEntity findSuperClosureConfig(String movementType, String movementReason) {

        Criteria c = session.createCriteria(SupervisionClosureConfigurationEntity.class);
        c.setMaxResults(1);
        c.add(Restrictions.eq("movementType", movementType));
        c.add(Restrictions.eq("movementReason", movementReason));

        return (SupervisionClosureConfigurationEntity) c.uniqueResult();
    }

    private TransferWaitlistPriorityConfigEntity findTransferWaitlistPriorityConfigEntity(String entryPriority) {
        Criteria c = session.createCriteria(TransferWaitlistPriorityConfigEntity.class);
        c.add(Restrictions.ilike("transferWaitlistEntryPriority", entryPriority));
        c.setMaxResults(1);
        return (TransferWaitlistPriorityConfigEntity) c.uniqueResult();
    }

    /**
     * Retrieves the count of external movement activities
     *
     * @param uc
     * @return the count of external movement activities
     */
    private Long getCountOfInternalMovementActivities(UserContext uc) {
        if (log.isDebugEnabled()) {
            log.debug("getCountOfInternalMovementActivities: begin");
        }

        Long ret = BeanHelper.getCount(uc, context, session, "movementId", InternalMovementActivityEntity.class);

        if (log.isDebugEnabled()) {
            log.debug("getCountOfInternalMovementActivities: end");
        }

        return ret;
    }

    /**
     * Retrieves the count of external movement activities
     *
     * @param uc
     * @return the count of external movement activities
     */
    private Long getCountOfExternalMovementActivities(UserContext uc) {

        Long ret = BeanHelper.getCount(uc, context, session, "movementId", ExternalMovementActivityEntity.class);

        return ret;
    }

    //////////////////////////////////////////////////////////////
    // private methods for business logic operations
    /////////////////////////////////////////////////////////

    private Long deleteLastCompletedSupervisionMovement(UserContext uc) throws ArbutusRuntimeException {
        session.createQuery("delete LastCompletedSupervisionMovementEntity").executeUpdate();

        return success;
    }

    private void saveOrUpdateLastCompletedSupervisionMovement(UserContext uc, Long supervisionId, Long movementActivityId, Long facilityId, Long facilityInternalLocationId,
            String movementType, String movementDirection) {

        LastCompletedSupervisionMovementEntity entity = (LastCompletedSupervisionMovementEntity) session.get(LastCompletedSupervisionMovementEntity.class, supervisionId);

        if (entity == null) {
            entity = new LastCompletedSupervisionMovementEntity(supervisionId, movementActivityId, facilityInternalLocationId);
            session.save(entity);
        } else {
            entity.setMovementActivityId(movementActivityId);
            entity.setInternalLocationId(facilityInternalLocationId);
            session.update(entity);
        }

        // Update inmate status
        String newOffenderStatus = calculateNewOffenderStatus(movementType, movementDirection);
        String newOffenderInOut = calculateNewOffenderInOut(uc, movementType, supervisionId, facilityId);
        if (newOffenderStatus != null || newOffenderInOut != null) {
            PersonType personType = personService.getPersonTypeBySupervisionId(uc, supervisionId, false);
            personService.updateOffenderStatus(uc, personType.getPersonIdentification(),
                        newOffenderStatus == null ? personType.getOffenderStatus() : newOffenderStatus,
                        newOffenderInOut == null ? personType.getOffenderInOut() : newOffenderInOut);
        }

    }

    // Calculate new offender status, return null if there is no change
    private String calculateNewOffenderStatus(String movementType, String movementDirection) {

        String status = null;
        switch (movementType) {
            case TYPE_ADM:
                status = INMATE_STATUS_ACTIVE;
                break;
            case TYPE_TRNIJ:
                status = INMATE_STATUS_INTRANSIT;
                break;
            case TYPE_REL:
            case TYPE_ESCP:
            case TYPE_TRNOJ:
                status = INMATE_STATUS_INACTIVE;
                break;
            case TYPE_INT:
                status = DIRECTION_IN.equalsIgnoreCase(movementDirection) ? INMATE_STATUS_ACTIVE : INMATE_STATUS_INACTIVE;
                break;
            default:
                break;
        }

        //
        return  status;
    }

    // Calculate new offender in/out status, return null if there is no change
    private String calculateNewOffenderInOut(UserContext uc, String movementType, Long supervisionId, Long facilityId) {

        String inout = null;

        //
        switch(movementType) {
            case TYPE_ADM:
                inout = INMATE_INOUT_IN;
                break;
            case TYPE_TRNIJ:
            case TYPE_TRNOJ:
            case TYPE_REL:
            case TYPE_ESCP:
                inout = INMATE_INOUT_OUT;
            case TYPE_CRT:
            case TYPE_TAP:
            case TYPE_INT:
            case TYPE_APPO:
                SupervisionType supervisionType = supervisionService.get(uc, supervisionId);
                if (supervisionType != null) {
                    inout = supervisionType.getFacilityId().equals(facilityId) ? INMATE_INOUT_IN : INMATE_INOUT_OUT;
                }
                break;
            default:
                break;

        }

        //
        return inout;
    }

    private <T extends MovementActivityEntity> boolean bLatestCompletedMovement(UserContext uc, T mae) {
        if (mae.getMovementDate() == null) {
            return false;
        }
        Criteria c = session.createCriteria(MovementActivityEntity.class);
        c.add(Restrictions.eq("supervisionId", mae.getSupervisionId()));
        c.add(Restrictions.eq("movementStatus", MovementStatus.COMPLETED.code()));
        c.add(Restrictions.isNotNull("movementDate"));
        c.add(Restrictions.ge("movementDate", mae.getMovementDate()));
        c.addOrder(Order.desc("movementDate")).addOrder(Order.desc("movementId"));
        c.setMaxResults(2);

        @SuppressWarnings("unchecked")
        List<MovementActivityEntity> mas = c.list();
        if (mas == null || mas.size() == 0) {
            return true;
        } else if (mas.size() == 1) {
            if (mas.get(0).getMovementId().longValue() != mae.getMovementId().longValue()) {
                return false;
            } else {
                return true;
            }
        } else {
            MovementActivityEntity to = mas.get(0);
            MovementActivityEntity from = mas.get(1);
            MovementActivityEntity last = new MovementActivityEntity();
            if (to instanceof InternalMovementActivityEntity && from instanceof InternalMovementActivityEntity) {
                if (((InternalMovementActivityEntity) to).getFromInternalLocationId().equals(((InternalMovementActivityEntity) from).getToInternalLocationId())) {
                    last = to;
                } else if (((InternalMovementActivityEntity) from).getFromInternalLocationId().equals(((InternalMovementActivityEntity) to).getToInternalLocationId())) {
                    last = from;
                } else {
                    last = to;
                }
            } else {
                last = to;
            }
            if (mae.getMovementDate().equals(last.getMovementDate())) {
                return true;
            } else {
                return false;
            }
        }
    }

    private <T extends MovementActivityEntity> void updateLastCompletedSupervisionMovement(UserContext uc, T mae) {

        if (log.isDebugEnabled()) {
            log.debug("updateLastCompletedSupervisionMovement: begin");
        }
        if(MovementCategory.NOMOV.isEquals(mae.getMovementCategory())){
            return;
        }

        /**
         * If MovementStatus == COMPLETED
         * then Create/update the Last Completed Supervision Movement Type for the offender
         */
        if (MovementStatus.COMPLETED.code().equalsIgnoreCase(mae.getMovementStatus()) && bLatestCompletedMovement(uc, mae)) {
            if (mae instanceof ExternalMovementActivityEntity) {
                /**
                 * If MovementDirection == OUT,
                 *   CurrentInternalLocationReference is set to null
                 */
                if (MovementDirection.OUT.code().equalsIgnoreCase(mae.getMovementDirection())) {
                    saveOrUpdateLastCompletedSupervisionMovement(uc, mae.getSupervisionId(), mae.getMovementId(), ((ExternalMovementActivityEntity) mae).getToFacilityId(),
                            null, mae.getMovementType(), mae.getMovementDirection());
                }
                /**
                 * if MovementDirection == IN,
                 * 	 CurrentInternalLocationReference is set to ToFacilityInternalLocationReference of the external movement
                 */
                else if (MovementDirection.IN.code().equalsIgnoreCase(mae.getMovementDirection())) {
                    Long toFacInLocAssociation = ((ExternalMovementActivityEntity) mae).getToInternalLocationId();
                    saveOrUpdateLastCompletedSupervisionMovement(uc, mae.getSupervisionId(), mae.getMovementId(), ((ExternalMovementActivityEntity) mae).getToFacilityId(),
                            toFacInLocAssociation, mae.getMovementType(), mae.getMovementDirection());
                }
            }

            if (mae instanceof InternalMovementActivityEntity) {
                /**
                 * if MovementDirection == IN,
                 * 	 CurrentInternalLocationReference is set to ToFacilityInternalLocationReference of the internal movement
                 */
                if (MovementDirection.IN.code().equalsIgnoreCase(mae.getMovementDirection())) {

                    Long toFacLocAssoc = ((InternalMovementActivityEntity) mae).getToInternalLocationId();
                    saveOrUpdateLastCompletedSupervisionMovement(uc, mae.getSupervisionId(), mae.getMovementId(), null,
                            toFacLocAssoc, mae.getMovementType(), mae.getMovementDirection());
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("updateLastCompletedSupervisionMovement: end");
        }
    }

    /**
     * If MovementStatus == COMPLETED then
     * <p>Create/update the Last Completed Supervision Movement Type for the offender
     * <p>If MovementDirection == OUT, CurrentInternalLocationReference is set to null
     * <p>Else if MovementDirection == IN,
     * <p>CurrentInternalLocationReference is set to
     * ToFacilityInternalLocationReference of the external movement
     *
     * @param <T>
     * @param uc
     * @param mae
     */
    private <T extends MovementActivityEntity> boolean updateLastCompletedSupervisionMovementWhenDeleteMA(UserContext uc, T mae) {

        if (log.isDebugEnabled()) {
            log.debug("updateLastCompletedSupervisionMovementBeforeDeleteMA: begin");
        }

        if (!MovementStatus.COMPLETED.code().equalsIgnoreCase(mae.getMovementStatus())) {
            return true;
        }

        Long supervisionId = mae.getSupervisionId();
        LastCompletedSupervisionMovementEntity entity = (LastCompletedSupervisionMovementEntity) session.get(LastCompletedSupervisionMovementEntity.class, supervisionId);

        if (entity == null) {
            return true;
        }

        Criteria c = session.createCriteria(MovementActivityEntity.class);
        c.add(Restrictions.eq("supervisionId", supervisionId));
        c.add(Restrictions.eq("movementStatus", MovementStatus.COMPLETED.code()));
        c.addOrder(Order.desc("movementDate"));
        c.setMaxResults(1);

        MovementActivityEntity maEntity = (MovementActivityEntity) c.uniqueResult();
        if (maEntity != null) {
            Long toFacInLoc = maEntity.getToInternalLocationId();
            if (toFacInLoc != null &&
                    !maEntity.getMovementStatus().equals(MovementDirection.OUT.code()) &&
                    !maEntity.getMovementCategory().equals(MovementCategory.EXTERNAL.code())) {
                entity.setInternalLocationId(toFacInLoc);
            } else {
                entity.setInternalLocationId(null);
            }

            entity.setMovementActivityId(maEntity.getMovementId());
            session.update(entity);
        } else {
            session.delete(entity);
        }

        session.flush();
        session.clear();

        if (log.isDebugEnabled()) {
            log.debug("updateLastCompletedSupervisionMovementBeforeDeleteMA: end");
        }

        return true;
    }

    private void deleteAllMovementActivities(UserContext uc) throws ArbutusRuntimeException {
        //Set<Long> allMovementActivities = getAll(uc);
        //for (Long id : allMovementActivities) {
        //	delete(uc, id);
        //}

        session.createQuery("delete MovementActivityCommentEntity").executeUpdate();
        session.createQuery("delete ExternalMovementActivityEntity").executeUpdate();
        session.createQuery("delete InternalMovementActivityEntity").executeUpdate();

        // Clear Hibernate cache to add to avoid unique key violation.
        session.flush();
        session.clear();
    }


    private void updateComments(MovementActivityEntity oldEntity, MovementActivityEntity newEntity) {
        // Delete privileges
        Iterator<MovementActivityCommentEntity> icomment = oldEntity.getCommentText().iterator();
        while (icomment.hasNext()) {
            MovementActivityCommentEntity comment = (MovementActivityCommentEntity) icomment.next();
            session.delete(comment);
        }

        oldEntity.setCommentText(newEntity.getCommentText());

        // Save new privileges set
        icomment = oldEntity.getCommentText().iterator();
        while (icomment.hasNext()) {
            MovementActivityCommentEntity comment = (MovementActivityCommentEntity) icomment.next();
            comment.setCommentIdentification(null);
            comment.setMovementActivity(oldEntity);
            session.save(comment);
        }
    }


    private void updateCommentsnew(MovementActivityEntity oldEntity, MovementActivityEntity newEntity) {

        String status = newEntity.getMovementStatus();
        if (!status.equals(oldEntity.getMovementStatus()) && (status.equalsIgnoreCase(MovementStatus.NOSHOW.code()) || status.equalsIgnoreCase(MovementStatus.COMPLETED.code()))
                && BeanHelper.isEmpty(newEntity.getCommentText())) {
            String message = "Invalid argument: MovementComment is required when MovementStatus changed";
            LogHelper.error(log, "updateComments", message);
            throw new InvalidInputException(message);
        }


//        if(!BeanHelper.isEmpty(oldEntity.getCommentText())){
            Iterator<MovementActivityCommentEntity> icomment = oldEntity.getCommentText().iterator();
            while (icomment.hasNext()) {
                MovementActivityCommentEntity comment = icomment.next();
                session.delete(comment);
            }
//        }

        if (!BeanHelper.isEmpty(newEntity.getCommentText())) {

            oldEntity.setCommentText(newEntity.getCommentText());

            icomment = oldEntity.getCommentText().iterator();
            while (icomment.hasNext()) {
                MovementActivityCommentEntity comment =  icomment.next();
                comment.setCommentIdentification(null);
                comment.setMovementActivity(oldEntity);
                session.save(comment);
            }

        }

    }

    /**
     * Process business logic.
     * Check if arguments exist in the ReferenceSet&Code&Language. Return error
     * Invalidated Argument(s) if not.
     * Check Movement Type
     * Check Movement Direction
     * Check Movement Status
     * Check Movement Reason
     * Check Movement Activity Reference -- must be one is Supervision and
     * another Activity
     *
     * @param ma T extends MovementActivity
     * @return Boolean
     */
    private <T extends MovementActivityType> void verifyForExternalMovementCreation(UserContext uc, T ma) {

        String functionName = "isExternalValidated";
        // **********************
        // Required Field Check
        // **********************
//
//        // Movement Activity Reference in case of movementActivity is instance
//        // of ExternalMovementActivity
//        if (!MovementCategory.EXTERNAL.isEquals(ma.getMovementCategory())) {
//            String message = "MovementCategory must be EXTERNAL.";
//            LogHelper.error(log, functionName, message);
//            throw new InvalidInputException(message);
//        }
//
//        // MovementCategory:External <==> MovementType:*
//        String type = ma.getMovementType();
//        Set<CodeType> types = BeanHelper.getLinkCodes(uc, new CodeType(MetaSet.MOVEMENT_CATEGORY, MovementCategory.EXTERNAL.name()), MetaSet.MOVEMENT_TYPE);
//
//        if (!types.contains(new CodeType(MetaSet.MOVEMENT_TYPE.toUpperCase(), type))) {
//            String message = "Wrong Code: Code '" + type + "' of MovementType for External must be in the set of " + types;
//            LogHelper.error(log, functionName, message);
//            throw new InvalidInputException(message);
//        }

        // Movement Status must be PENDING, COMPLETED OR APPREQ when created
        String status = ma.getMovementStatus();
        if (!MovementStatus.PENDING.isEquals(status) && !MovementStatus.COMPLETED.isEquals(status) && !MovementStatus.APPREQ.isEquals(status)) {
            String message = "Movement Status must be PENDING, COMPLETED or APPREQ when created. Wrong code: " + status;
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        if (MovementStatus.COMPLETED.isEquals(status)) {
            if (ma.getMovementDate() == null) {
                String message = "MovementDate is required when status is COMPLETED";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }

        // *******************
        // Constraint Check
        // *******************

        // Type ADM -- Direction IN -- (Reason RECA)
        if (MovementType.ADM.isEqualsIgnoreCase(ma.getMovementType())) {
            if (!MovementDirection.IN.isEqualsIgnoreCase(ma.getMovementDirection())) {
                String message = "MovementDirection must be 'IN' when MovementType is 'ADM'.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }

        // Type CRT -- Direction IN | OUT
        // Already checked in the section of "Required Field Check"

        // Type TRN -- Direction OUT
        if (MovementType.TRNOJ.isEqualsIgnoreCase(ma.getMovementType()) || MovementType.TRNIJ.isEqualsIgnoreCase(ma.getMovementType())) {
            if (!MovementDirection.OUT.isEqualsIgnoreCase(ma.getMovementDirection())) {
                String message = "MovementDirection must be 'OUT' when MovementType is 'TRN'.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }

        // Type TAP -- Direction IN | OUT
        // Already checked in the section of "Required Field Check"

        // Type WR -- Direction IN | OUT
        // Already checked in the section of "Required Field Check"

        // Type REL -- Direction OUT -- (Reason ESC | DEATH)
        if (MovementType.REL.isEqualsIgnoreCase(ma.getMovementType())) {
            if (!MovementDirection.OUT.isEqualsIgnoreCase(ma.getMovementDirection())) {
                String message = "MovementDirection must be 'OUT' when MovementType is 'REL'.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }

        // Type MED -- Direction OUT
        if (MovementType.MED.isEqualsIgnoreCase(ma.getMovementType())) {
            if (!MovementDirection.OUT.isEqualsIgnoreCase(ma.getMovementDirection())) {
                String message = "MovementDirection must be 'OUT' when MovementType is 'MED'.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }

    }

    /**
     * Process business logic.
     * Check if arguments exist in the ReferenceSet&Code&Language. Return error
     * Invalidated Argument(s) if not.
     * Check Movement Type
     * Check Movement Direction
     * Check Movement Status
     * Check Movement Reason
     * Check Movement Activity Reference -- must be one is Supervision and
     * another Activity
     *
     * @param ma T extends MovementActivity
     * @return Boolean
     */
    private <T extends MovementActivityType> void verifyForInternalMovementCreation(UserContext uc, T ma, Boolean requiresApproval) {
        String functionName = "isInternalValidated";
        // **********************
        // Required Field Check
        // **********************

        // Movement Activity Reference in case of movementActivity is instance
        // of InternalMovementActivity

        if (!MovementCategory.INTERNAL.isEquals(ma.getMovementCategory())) {
            String message = "MovementCategory must be INTERNAL.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        // MovementCategory:Internal <==> MovementType:*
        // Constraint Check
        // Type VIS | OIC(DISC) | APP | PROG | BED | MED | CRT | CLA
        String type = ma.getMovementType();
        Set<CodeType> types = BeanHelper.getLinkCodes(uc, new CodeType(MetaSet.MOVEMENT_CATEGORY, MovementCategory.INTERNAL.name()), MetaSet.MOVEMENT_TYPE);

        if (!types.contains(new CodeType(MetaSet.MOVEMENT_TYPE.toUpperCase(), type))) {
            String message = "Wrong Code: Code '" + type + "' of MovementType for Internal must be in the set of " + types;
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        // Movement Status must be PENDING when created
        /** This commented implementation is for SDD1.0 only.
         String status = ReferenceCodeHelper.toCode(ma.getMovementStatus());
         if (!MovementStatus.PENDING.isEquals(status)) {
         log.error("Movement Status must be PENDING when created. Wrong code: " + status);
         return Boolean.FALSE;
         } */
        /** This implementation is for SDD2.0
         *  If MovementStatus == CPMPLETED then
         *    fromFacilityInternalLocationAssociation is required.
         */
        String status = ma.getMovementStatus();
        if (MovementStatus.COMPLETED.equals(status)) {
            if (ma.getMovementDate() == null) {
                String message = "MovementDate is required when status changed to COMPLETED";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }

            Long fromFacilityInternalLocationId = ((InternalMovementActivityType) ma).getFromFacilityInternalLocationId();
            if (fromFacilityInternalLocationId == null) {
                String message = "FromFacilityInternalLocationId is required if Movement Status is COMPLETED.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }

        // Movement Status must be PENDING, COMPLETED OR APPREQ when created
        if (!MovementStatus.PENDING.isEquals(status) && !MovementStatus.COMPLETED.isEquals(status) && !MovementStatus.APPREQ.isEquals(status)) {
            String message = "Movement Status must be PENDING, COMPLETED or APPREQ when created. Wrong code: " + status;
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

    }

    /**
     * Process business logic.
     * Check if arguments exist in the ReferenceSet&Code&Language. Return error
     * Invalidated Argument(s) if not.
     * Check Movement Type
     * Check Movement Direction
     * Check Movement Status
     * Check Movement Reason
     * Check Movement Activity Reference -- must be one is Supervision and
     * another Activity
     *
     * @param ma T extends MovementActivity
     * @return Boolean
     */
    private <T extends MovementActivityType> void verifyForUpdate(UserContext uc, T ma) {
        String functionName = "isUpdateValidated";
        // Movement Type
        if (ma instanceof ExternalMovementActivityType) {
            // MovementCategory:External <==> MovementType:*
            String etype = ma.getMovementType();
            Set<CodeType> etypes = BeanHelper.getLinkCodes(uc, new CodeType(MetaSet.MOVEMENT_CATEGORY, MovementCategory.EXTERNAL.name()), MetaSet.MOVEMENT_TYPE);

            if (!etypes.contains(new CodeType(MetaSet.MOVEMENT_TYPE.toUpperCase(), etype))) {
                String message = "Wrong Code: Code '" + etype + "' of MovementType for External must be in the set of " + etypes;
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        } else if (ma instanceof InternalMovementActivityType) {
            // MovementCategory:Internal <==> MovementType:*
            // Constraint Check
            // Type VIS | OIC(DISC) | APP | PROG | BED | MED | CRT | CLA
            String itype = ma.getMovementType();
            Set<CodeType> itypes = BeanHelper.getLinkCodes(uc, new CodeType(MetaSet.MOVEMENT_CATEGORY, MovementCategory.INTERNAL.name()), MetaSet.MOVEMENT_TYPE);

            if (!itypes.contains(new CodeType(MetaSet.MOVEMENT_TYPE.toUpperCase(), itype))) {
                String message = "Wrong Code: Code '" + itype + "' of MovementType for Internal must be in the set of " + itypes;
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }

        // Movement Status
        String status = ma.getMovementStatus();

		/* Removed based on SDD2.0 svn:17556
           if (// MovementStatus.ONHOLD.isEquals(status)   || // Commented out based on SDD2.0
			MovementStatus.COMPLETED.isEquals(status) ||
			// MovementStatus.CANCELLED.isEquals(status) || // Commented out based on SDD2.0
			MovementStatus.NOSHOW.isEquals(status)) {

			// Outcome is required when status changed to ONHOLD, COMPLETED, CANCELLED, NOSHOW
			if (isEmpty(outcome)) {
				log.error("MovementOutcome is required when status changed to " + MovementStatus.codeSet());
				return Boolean.FALSE;
			}

			 Removed according to the Change Request from BA MovementActivity2.0 SDD svn: 17556
			 * // Comment is required when status changed
			if (isEmpty(ma.getCommentText())) {
				log.error("Comment is required when status changed");
				return Boolean.FALSE;
			}
		}*/

        if (MovementStatus.COMPLETED.isEquals(status)) {
            if (ma.getMovementDate() == null) {
                String message = "MovementDate is required when status changed to COMPLETED";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }

            if (ma instanceof ExternalMovementActivityType) {
                if (ma.getMovementDirection() != null) {
                    if (MovementDirection.IN.name().equalsIgnoreCase(ma.getMovementDirection()) && !MovementType.CRT.name().equalsIgnoreCase(ma.getMovementType())) {
                        if (((ExternalMovementActivityType) ma).getToFacilityInternalLocationId() == null) {
                            String message = "ToFacilityInternalLocationAssociation is required when status changed to COMPLETED and MovementDirection is IN.";
                            LogHelper.error(log, functionName, message);
                            throw new InvalidInputException(message);
                        }
                    }
                }
            }

            if (ma instanceof InternalMovementActivityType) {
                if (((InternalMovementActivityType) ma).getFromFacilityInternalLocationId() == null) {
                    String message = "FromFacilityInternalLocationAssociation is required when status changed to COMPLETED";
                    LogHelper.error(log, functionName, message);
                    throw new InvalidInputException(message);
                }
            }
        }

        // Exit in case of InternalMovementActivity
        if (ma instanceof InternalMovementActivityType) {
            return;
        }

        // *********************************
        // Constraint Check in External case
        // *********************************

        // Type ADM -- Direction IN -- (Reason RECA)
        if (ExternalMovementType.ADM.isEqualsIgnoreCase(ma.getMovementType())) {
            if (!MovementDirection.IN.isEqualsIgnoreCase(ma.getMovementDirection())) {
                String message = "MovementDirection must be 'IN' when MovementType is 'ADM'.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }

        // Type CRT -- Direction IN | OUT
        // Already checked in the section of "Required Field Check"

        // Type TRNOj or TRNIJ -- Direction OUT
        if (ExternalMovementType.TRNOJ.isEqualsIgnoreCase(ma.getMovementType()) || ExternalMovementType.TRNIJ.isEqualsIgnoreCase(ma.getMovementType())) {
            if (!MovementDirection.OUT.isEqualsIgnoreCase(ma.getMovementDirection())) {
                String message = "MovementDirection must be 'OUT' when MovementType is 'TRN'.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }

        // Type TAP -- Direction IN | OUT
        // Already checked in the section of "Required Field Check"

        // Type WR -- Direction IN | OUT
        // Already checked in the section of "Required Field Check"

        // Type REL -- Direction OUT -- (Reason ESC | DEATH)
        if (MovementType.REL.isEqualsIgnoreCase(ma.getMovementType())) {
            if (!MovementDirection.OUT.isEqualsIgnoreCase(ma.getMovementDirection())) {
                String message = "MovementDirection must be 'OUT' when MovementType is 'REL'.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }

        // Type MED -- Direction OUT
        if (MovementType.MED.isEqualsIgnoreCase(ma.getMovementType())) {
            if (!MovementDirection.OUT.isEqualsIgnoreCase(ma.getMovementDirection())) {
                String message = "MovementDirection must be 'OUT' when MovementType is 'MED'.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }
    }

    private boolean hasAssociatedToOther(Session session, Long activityId) {
        Criteria c = session.createCriteria(MovementActivityEntity.class);
        c.add(Restrictions.eq("activityId", activityId));
        c.setProjection(Projections.countDistinct("movementId"));
        Long count = (Long) c.uniqueResult();
        if (count != null && (count.longValue() > 0)) {
            return true;
        } else {
            return false;
        }
    }

    ////////////////////////////////////////////////////////
    // Utilities
    ////////////////////////////////////////////////////////
    private String strPatternParser(String str) throws ArbutusRuntimeException {

        return str == null || str.length() == 0 ? str : str.matches("^\\*+$") ? "" : str.replaceAll("\\*+", "%");
    }

    ///////////////////////////////////////////////////////
    // Business Logic Verifications
    //////////////////////////////////////////////////////

    /**
     * Implement method apply which is inherited from Transform interface
     *
     * @param from CommentEntity
     * @return to CommentType
     */
    private CommentType toComment(MovementActivityCommentEntity from) {

        // null is just null
        if (from == null) {
            return null;
        }

        CommentType to = new CommentType();
        to.setCommentIdentification(from.getCommentIdentification());
        to.setComment(from.getCommentText());
        to.setCommentDate(from.getCommentDate());

        return to;
    }

    private Set<TransferWaitlistEntryType> toTransferWaitlistEntryTypeSet(Set<TransferWaitlistEntryEntity> from) {
        if (from == null) {
            return null;
        }

        Set<TransferWaitlistEntryType> to = new HashSet<TransferWaitlistEntryType>();

        for (TransferWaitlistEntryEntity waitlist : from) {
            TransferWaitlistEntryType entry = toTransferWaitlistEntryType(waitlist);
            if (entry != null) {
                to.add(entry);
            }
        }

        return to;
    }

    private TransferWaitlistEntryType toTransferWaitlistEntryType(TransferWaitlistEntryEntity from) {
        if (from == null) {
            return null;
        }

        TransferWaitlistEntryType to = new TransferWaitlistEntryType();
        //to.setId(from.getTransFacilityId());
        to.setSupervisionId(from.getSupervision());
        to.setDateAdded(from.getDateAdded());
        to.setToFacilityId(from.getToFacility());
        to.setPriority(from.getPriority());
        to.setTransferReason(from.getTransferReason());
        to.setStamp(from.getStamp());
        to.setVersion(from.getVersion());

        return to;
    }

    //	private Date getFormattedDate(String dateStr) {
    //		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.mmm");
    //		Date date = null;
    //		try {
    //			date = df.parse(dateStr);
    //		} catch (ParseException e) {
    //			log.info("Unable to get formatted date");
    //		}
    //		return date;
    //	}

    //////////////////////////////////////////////////////////////
    // private methods for type convert
    //////////////////////////////////////////////////////////////

    private TransferWaitlistEntryEntity toTransferWaitlistEntryEntity(Long facilityId, TransferWaitlistEntryType from) {

        if (facilityId == null) {
            return null;
        }

        TransferWaitlistEntryEntity to = new TransferWaitlistEntryEntity();
        if (from == null) {
            return to;
        }

        to.setTransFacilityId(facilityId);
        to.setSupervision(from.getSupervisionId());
        to.setToFacility(from.getToFacilityId());
        to.setDateAdded(from.getDateAdded());
        to.setTransferReason(from.getTransferReason());
        to.setPriority(from.getPriority());
        to.setStamp(from.getStamp());
        to.setVersion(from.getVersion());

        return to;
    }

    private Set<TransferWaitlistEntryEntity> toTransferWaitlistEntryEntitySet(Long facilityId, Set<TransferWaitlistEntryType> from) {
        if (from == null) {
            return null;
        }

        Set<TransferWaitlistEntryEntity> to = new HashSet<TransferWaitlistEntryEntity>();
        for (TransferWaitlistEntryType frm : from) {
            to.add(toTransferWaitlistEntryEntity(facilityId, frm));
        }

        return to;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.movement.contract.interfaces.MovementActivityService#getAlwaysCloseSupervisionFlag(syscon.arbutus.product.security.UserContext)
     */
    @Override
    public boolean getAlwaysCloseSupervisionFlag(UserContext uc) {
        //TODO - This method will be implemented when WOr-5928 will be worked upon.
        return false;
    }

    private LastKnowLocationHandler getLastKnowLocationHandler() {
        return new LastKnowLocationHandler(session, context);
    }

    @Override
    public List<LastKnowLocationGridEntryType> createLastKnowLocation(UserContext uc, List<LastKnowLocationType> lastKnowLocationList) {
        return getLastKnowLocationHandler().createLastKnowLocation(uc, lastKnowLocationList);

    }

    @Override
    public List<LastKnowLocationType> searchLastKnowLocation(UserContext uc, LastKnowLocationSearchType search) {
        return getLastKnowLocationHandler().searchLastKnowLocation(uc, search);
    }

    @Override
    public LastKnowLocationGridReturnType searchLastKnowLocationGrid(UserContext uc, LastKnowLocationSearchType search, Long startIndex, Long resultSize,
                                                                     String resultOrder, String filer) {
        return getLastKnowLocationHandler().searchLastKnowLocationGrid(uc, search, startIndex, resultSize, resultOrder, filer);
    }

    @Override
    public void indexDataBaseLastKnowLocation() {
        getLastKnowLocationHandler().indexDataBaseLastKnowLocation();

    }

    @Override
    public List<ScheduleConflict> getExternalMovementBySupervisionId(UserContext uc, Long supervisionId, List<String> types) {

        StringBuilder hqlFormat = new StringBuilder();
        //ma1 = OUT		//ma2 = IN

        hqlFormat.append("SELECT new %s(ma1.movementId, ma1.supervisionId, ");
        hqlFormat.append("ma1.toFacilityId, ma1.toLocationId, ");
        hqlFormat.append("act1.plannedStartDate, act2.plannedEndDate, ma1.movementType) ");
        hqlFormat.append("FROM ExternalMovementActivityEntity AS ma1, ExternalMovementActivityEntity AS ma2, ");
        hqlFormat.append("ActivityEntity AS act1, ActivityEntity AS act2 ");

        hqlFormat.append("WHERE ma1.movementType IN (:moveTypes) ");
        hqlFormat.append("AND ma1.groupid = ma2.groupid ");
        hqlFormat.append("AND ma1.movementId <> ma2.movementId ");
        hqlFormat.append("AND ma1.activityId = act1.activityId ");
        hqlFormat.append("AND ma2.activityId = act2.activityId ");
        hqlFormat.append("AND act2.antecedentId = act1.activityId ");
        hqlFormat.append("AND ma1.movementDirection = 'OUT' ");
        hqlFormat.append("AND ma2.movementStatus = 'PENDING' ");
        hqlFormat.append("AND ma1.supervisionId = :supervisionId ");
        hqlFormat.append("ORDER BY act1.plannedStartDate ");

        String hql = String.format(hqlFormat.toString(), ScheduleConflict.class.getName());

        Query query = session.createQuery(hql);
        query.setParameterList("moveTypes", types);
        query.setParameter("supervisionId", supervisionId);

        return query.list();

    }

    /**
     * Creates a movement activity record along with an activity record and
     * associates it to an offender’s supervision period
     * <b>Provenance</b>
     * <li>At least one of Internal Movement Activity Type or External Movement Activity Type must be provided</li>
     * <li>Activity Type and SupervisionIdentification must be provided</li>
     * <li>The RequiresApproval flag is used when creating the movement activity (either internal or external)</li>
     * <li>A movement activity (either internal or external) and an Activity are created</li>
     * <li>The movement Activity is associated to the Activity</li>
     * <li>The Activity is associated to the Supervision</li>
     *
     * @param uc               UserContext - Required
     * @param movement         External/Internal MovementActivityType - Required
     * @param activity         ActivityType - Required
     * @param supervisionId    Long - Required
     * @param RequiresApproval Boolean - Optional
     * @return MovementDetailReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */

    // Create Linked Movement Activity
    @Override
    public MovementDetailType createLinkedMovement(UserContext uc, MovementActivityType movement, ActivityType activity, Long supervisionId, Boolean RequiresApproval) {

        String methodName = "createLinkedMovement";

        MovementDetailType md = new MovementDetailType();

        // Validate inputs
        if (uc == null) {
            String message = "UserContext is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (supervisionId == null) {
            String message = "supervisionId is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (movement == null) {
            String message = "movement is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        ValidationHelper.validate(activity);

        if (!(movement instanceof ExternalMovementActivityType || movement instanceof InternalMovementActivityType)) {
            String message = "movement must be ExternalMovementActivityType or InternalMovementActivityType.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        // create association from activity to supervision and movement to supervision
        activity.setSupervisionId(supervisionId);
        movement.setSupervisionId(supervisionId);

        // create activity
        ActivityType activityRet = ActivityServiceAdapter.createActivity(uc, activity);

        if (activityRet == null) {
            String message = "Error while creating Activity.";
            LogHelper.error(log, methodName, message);
            throw new ArbutusRuntimeException(message);
        }

        Long activityId = activityRet.getActivityIdentification();

        // create Association from Movement to Activity
        movement.setActivityId(activityId);

        ValidationHelper.validate(movement);

        if (movement instanceof ExternalMovementActivityType) {

            ExternalMovementActivityType ema = (ExternalMovementActivityType) movement;

            ValidationHelper.validate(ema);

            ExternalMovementActivityType emaRet = MovementServiceAdapter.createExternalMovementActivity(uc, ema, RequiresApproval);

            if (emaRet == null) {
                String message = "Error while creating external movement.";
                LogHelper.error(log, methodName, message);
                throw new ArbutusRuntimeException(message);
            }
            md.setMovement(emaRet);
        }

        if (movement instanceof InternalMovementActivityType) {

            InternalMovementActivityType ima = (InternalMovementActivityType) movement;

            ValidationHelper.validate(ima);

            InternalMovementActivityType imaRet = MovementServiceAdapter.createInternalMovementActivity(uc, ima, RequiresApproval);

            if (imaRet == null) {
                String message = "Error while creating internal movement.";
                LogHelper.error(log, methodName, message);
                throw new ArbutusRuntimeException(message);
            }
            md.setMovement(imaRet);
        }

        md.setActivity(activityRet);
        return md;
    }

    /**
     * Updates a movement activity record and/or an activity record
     * <b>Provenance</b>
     * <li>At least one of the Internal Movement Activity Type, External Movement Activity Type or Activity Type must be provided</li>
     * <li>The details of a movement activity (either internal or external) and/or an activity is updated</li>
     * <li>The associations for the movement activity or activity are not updated</li>
     *
     * @param uc       UserContext - Required
     * @param movement External/Internal MovementActivityType - Required
     * @param activity ActivityType - Optional
     * @return MovementDetailReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    // Update Linked Movement Activity
    private MovementDetailType updateLinkedMovements(UserContext uc, MovementActivityType movement, ActivityType activity) {

        String methodName = "updateLinkedMovements";

        MovementDetailType md = new MovementDetailType();

        // Validate inputs

        if (uc == null) {
            String message = "UserContext is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (movement == null) {
            String message = "movement is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (!(movement instanceof ExternalMovementActivityType || movement instanceof InternalMovementActivityType)) {
            String message = "movement must be ExternalMovementActivityType or InternalMovementActivityType.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (activity == null) {
            String message = "activity is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (movement instanceof ExternalMovementActivityType) {

            ExternalMovementActivityType ema = (ExternalMovementActivityType) movement;

            ValidationHelper.validate(ema);

            ExternalMovementActivityType retMovement = MovementServiceAdapter.updateExternalMovementActivity(uc, ema);

            if (retMovement == null) {
                String message = "Error while updating external movement.";
                LogHelper.error(log, methodName, message);
                throw new ArbutusRuntimeException(message);
            }

            md.setMovement(retMovement);

        }

        if (movement instanceof InternalMovementActivityType) {

            InternalMovementActivityType ima = (InternalMovementActivityType) movement;

            ValidationHelper.validate(ima);

            InternalMovementActivityType retMovement = MovementServiceAdapter.updateInternalMovementActivity(uc, ima);

            if (retMovement == null) {
                String message = "Error while updating external movement.";
                LogHelper.error(log, methodName, message);
                throw new ArbutusRuntimeException(message);
            }

            md.setMovement(retMovement);
        }

        if (activity != null) {

            ValidationHelper.validate(activity);

            ActivityType retActivity = ActivityServiceAdapter.updateActivity(uc, activity);

            if (retActivity == null) {
                String message = "Error while updating activity.";
                LogHelper.error(log, methodName, message);
                throw new ArbutusRuntimeException(message);
            }

            md.setActivity(retActivity);
        }

        return md;
    }

    @Override
    public MovementDetailType updateMovementActivity(UserContext uc, MovementActivityType movement) {

        String methodName = "updateMovementActivity";
        log.info("Entering updateMovementActivity ...");

        MovementDetailType md = new MovementDetailType();

        // Validate inputs
        if (uc == null) {
            String message = "UserContext is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (movement == null) {
            String message = "movement is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (!(movement instanceof ExternalMovementActivityType || movement instanceof InternalMovementActivityType)) {
            String message = "movement must be ExternalMovementActivityType or InternalMovementActivityType.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (movement instanceof ExternalMovementActivityType) {

            ExternalMovementActivityType ema = (ExternalMovementActivityType) movement;

            ValidationHelper.validate(ema);

            ExternalMovementActivityType retMovement = MovementServiceAdapter.updateExternalMovementActivity(uc, ema);

            if (retMovement == null) {
                String message = "Error while updating external movement.";
                LogHelper.error(log, methodName, message);
                throw new ArbutusRuntimeException(message);
            }

            md.setMovement(retMovement);
            ActivityType activity = ActivityServiceAdapter.getActivity(uc, retMovement.getActivityId());
            md.setActivity(activity);
        }

        if (movement instanceof InternalMovementActivityType) {

            InternalMovementActivityType ima = (InternalMovementActivityType) movement;

            ValidationHelper.validate(ima);

            InternalMovementActivityType retMovement = MovementServiceAdapter.updateInternalMovementActivity(uc, ima);

            if (retMovement == null) {
                String message = "Error while updating internal movement.";
                LogHelper.error(log, methodName, message);
                throw new ArbutusRuntimeException(message);
            }

            md.setMovement(retMovement);
            ActivityType activity = ActivityServiceAdapter.getActivity(uc, retMovement.getActivityId());
            md.setActivity(activity);
        }

        return md;
    }

    /**
     * Creates two external/internal movement activity records along with two associated activity records which are also associated to each other,
     * and correspondingly associated to an offender’s supervision period
     * <p/>
     * <p>
     * <p/>
     * <b>Provenance</b>
     * <li>Either Both External or Internal Movement Activity Types must be provided</li>
     * <li>Both Activity Types must be provided</li>
     * <li>SupervisionIdentification must be provided</li>
     * <li>The RequiresApproval flag is used when creating the movement activities (either internal or external)</li>
     * <p/>
     * <li>First set of External/Internal Movement Activity and Activity are created and associated together</li>
     * <li>Second set of External/Internal Movement Activity and Activity are created and associated together</li>
     * <li>The Activity from the first set is (statically) associated to the Activity from the second set</li>
     * </p>
     * <p>Note: The second Activity becomes a descendant of the first Activity. The first Activity becomes an antecedent of the second Activity.</p>
     * <p>Both Activities are associated to the Supervision</p>
     *
     * @param uc               UserContext - Required
     * @param movement1        External/Internal MovementActivityType - Required
     * @param movement2        External/Internal MovementActivityType - Required
     * @param activity1        ActivityType - Required
     * @param activity2        ActivityType - Required
     * @param supervisionId    Long - Required
     * @param RequiresApproval Boolean - Optional
     * @return MovementDetailReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */

    // create Linked Movement Activities with Descendant
    @Override
    public List<MovementDetailType> createLinkedMovementWithDescendant(UserContext uc, MovementActivityType movement1, MovementActivityType movement2,
                                                                       ActivityType activity1, ActivityType activity2, Long supervisionId, Boolean RequiresApproval) {

        String methodName = "createLinkedMovementWithDescendant";
        log.info("Entering " + methodName);

        List<MovementDetailType> moveDtls = new ArrayList<MovementDetailType>();

        // Validate inputs

        if (uc == null) {
            String message = "UserContext is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (activity1 == null || activity2 == null) {
            String message = "activity is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (movement1 == null || movement2 == null) {
            String message = "movement is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (!((movement1 instanceof ExternalMovementActivityType && movement2 instanceof ExternalMovementActivityType) || (
                movement1 instanceof InternalMovementActivityType && movement2 instanceof InternalMovementActivityType))) {

            String message = "Both movements must be either ExternalMovementActivityType or InternalMovementActivityType.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);

        }

        if (supervisionId == null) {
            String message = "supervisionId is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        // link first set of movement and activity
        MovementDetailType movementLink1 = createLinkedMovement(uc, movement1, activity1, supervisionId, RequiresApproval);

        if (movementLink1 == null) {
            String message = "Error while creating link for first movement.";
            LogHelper.error(log, methodName, message);
            throw new ArbutusRuntimeException(message);
        }

        Long activityId1 = null;
        if (movementLink1.getActivity() != null) {
            if (movementLink1.getActivity().getActivityIdentification() != null) {
                activityId1 = movementLink1.getActivity().getActivityIdentification();
            }
        }

        // link second set of movement and activity
        activity2.setActivityAntecedentId(activityId1);

        MovementDetailType movementLink2 = createLinkedMovement(uc, movement2, activity2, supervisionId, RequiresApproval);

        if (movementLink2 == null) {
            String message = "Error while creating link for second movement.";
            LogHelper.error(log, methodName, message);
            throw new ArbutusRuntimeException(message);
        }

        moveDtls.add(movementLink1);
        moveDtls.add(movementLink2);
        return moveDtls;
    }

    /**
     * Updates two internal/External movement activity records and/or two activity records.
     * <b>Provenance</b>
     * <li>Either two Movement Activity Types and/or two Activity Types must be provided.</li>
     * <li>If two Activity Types are provided they must already be associated to each other. Otherwise return an error.</li>
     * <li>If two Movement Activity Types are provided then they must both be of the same type ie. either internal or external. Otherwise return an error.</li>
     * <li>If two Movement Activity Types are provided they must also already be associated to each other through their associated Activities. Otherwise return an error.</li>
     * <li>The details of two Internal Movement Activities and/or two Activities are updated.</li>
     * <li>The associations for the Internal Movement Activities or Activities are not updated.</li>
     *
     * @param uc        UserContext - Required
     * @param movement1 External/Internal MovementActivityType - Required
     * @param movement2 External/Internal MovementActivityType - Required
     * @param activity1 ActivityType - Optional
     * @param activity2 ActivityType - Optional
     * @return MovementDetailReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    @Override
    public List<MovementDetailType> updateLinkedMovements(UserContext uc, MovementActivityType movement1, MovementActivityType movement2, ActivityType activity1,
                                                          ActivityType activity2) {

        List<MovementDetailType> moveDtls = new ArrayList<MovementDetailType>();

        String methodName = "createLinkedMovement";

        // Validate inputs

        if (uc == null) {
            String message = "UserContext is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (!((movement1 instanceof ExternalMovementActivityType && movement2 instanceof ExternalMovementActivityType) || (
                movement1 instanceof InternalMovementActivityType && movement2 instanceof InternalMovementActivityType))) {

            String message = "Both movements must be either ExternalMovementActivityType or InternalMovementActivityType.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);

        }

        if (!isMovementAssociated(uc, movement1, movement2)) {
            String message = "Movements not associated.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        MovementDetailType mdret1 = null;
        MovementDetailType mdret2 = null;

        // update Activities
        if (activity1 != null && activity2 != null && isActivitiesAssociated(activity1, activity2)) {

            mdret1 = updateLinkedMovements(uc, movement1, activity1);
            if (mdret1 == null) {
                String message = "Error while updating linked movements.";
                LogHelper.error(log, methodName, message);
                throw new ArbutusRuntimeException(message);
            }

            mdret2 = updateLinkedMovements(uc, movement2, activity2);
            if (mdret2 == null) {
                String message = "Error while updating linked movements.";
                LogHelper.error(log, methodName, message);
                throw new ArbutusRuntimeException(message);
            }
        } else {

            mdret1 = updateLinkedMovements(uc, movement1, null);
            if (mdret1 == null) {
                String message = "Error while updating linked movements.";
                LogHelper.error(log, methodName, message);
                throw new ArbutusRuntimeException(message);
            }

            mdret2 = updateLinkedMovements(uc, movement2, null);
            if (mdret2 == null) {
                String message = "Error while updating linked movements.";
                LogHelper.error(log, methodName, message);
                throw new ArbutusRuntimeException(message);
            }
        }

        moveDtls.add(mdret1);
        moveDtls.add(mdret2);
        return moveDtls;

    }

    /**
     * Retrieve MovementDetails of an individual for a given Movement ID
     *
     * @param uc         UserContext - Required
     * @param movementId Long - Required
     * @return Set of MovementDetailReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    // Get Movement details by Movement Id
    @Override
    public MovementDetailType getMovementDetailByMovementId(UserContext uc, Long movementId) {

        MovementDetailType md = new MovementDetailType();

        String methodName = "getMovementDetailByMovementId";

        // Validate inputs

        if (uc == null) {
            String message = "UserContext is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (movementId == null) {
            String message = "movement is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        MovementActivityType retMovement = MovementServiceAdapter.get(uc, movementId);

        if (retMovement == null) {
            String message = "Error while getting movement.";
            LogHelper.error(log, methodName, message);
            throw new ArbutusRuntimeException(message);
        }

        if (retMovement.getActivityId() != null) {
            ActivityType activity = ActivityServiceAdapter.getActivity(uc, retMovement.getActivityId());
            md.setActivity(activity);
        } else {
            String message = "Error while getting movement's activity .";
            LogHelper.error(log, methodName, message);
            throw new ArbutusRuntimeException(message);
        }

        md.setMovement(retMovement);

        return md;
    }

    /**
     * get Scheduled Internal movements
     */
    @Override
    public List<OffenderScheduleTransferType> getScheduledInternalMovements(UserContext uc, ScheduleTransferMovementSearch search) {

        String methodName = "getScheduledInternalMovements";

        if (uc == null) {
            String message = "UserContext is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        ValidationHelper.validate(search);

        List<OffenderScheduleTransferType> scheduleList = new ArrayList<OffenderScheduleTransferType>();

        MovementActivitySearchType maSearch = new MovementActivitySearchType();

        maSearch.setMovementCategory(CATEGORY_INTERNAL);
        if (search.getFromFacilityInternalLocation() != null) {
            maSearch.setFromFacilityInternalLocationId(search.getFromFacilityInternalLocation());
        }

        if (search.getToFacilityInternalLocation() != null) {
            maSearch.setToFacilityInternalLocationId(search.getToFacilityInternalLocation());
        }

        if (search.getMovementDirection() != null) {
            maSearch.setMovementDirection(search.getMovementDirection());
        }

        if (search.getMovementStatus() != null) {
            maSearch.setMovementStatus(search.getMovementStatus());
        }
        if (search.getMovementType() != null) {
            maSearch.setMovementType(search.getMovementType());
        }

        if (search.getFromScheduledDate() != null) {
            maSearch.setPlannedStartDate(search.getFromScheduledDate());
        }
        if (search.getToScheduledDate() != null) {
            maSearch.setPlannedEndDate(search.getToScheduledDate());
        }

        maSearch.setFilterInOut(search.isFilterInOut());

        MovementActivitiesReturnType retMovements = MovementServiceAdapter.searchMovementActivity(uc, maSearch);

        List<InternalMovementActivityType> InternalMoves = new ArrayList<InternalMovementActivityType>();

        if (retMovements.getInternalMovementActivity() != null && retMovements.getInternalMovementActivity().size() > 0) {
            InternalMoves = retMovements.getInternalMovementActivity();

        }

        SupervisionHandler handler = new SupervisionHandler(context, session);

        supervisionIdSet = new HashSet<Long>();
        for (InternalMovementActivityType ma : InternalMoves) {
            if(!supervisionIdSet.contains(ma.getSupervisionId())){
                supervisionIdSet.add(ma.getSupervisionId());
            }
        }
        for (InternalMovementActivityType ma : InternalMoves) {
            OffenderScheduleTransferType offender = new OffenderScheduleTransferType();

            offender.setActivityId(ma.getActivityId());
            offender.setParticipateOfficerId(ma.getParticipateStaffId());
            ActivityType activity = ActivityServiceAdapter.getActivity(uc, ma.getActivityId());
            offender.setScheduledTransferDate(activity.getPlannedStartDate());
            offender.setScheduledTransferTime(timeFormat.format(activity.getPlannedStartDate()));
            offender.setScheduledTransferStartTime(ma.getPlannedStartDate());

            if (!isMovePlanned(search.getFromScheduledDate(), search.getToScheduledDate(), offender.getScheduledTransferDate())) {
                continue;
            }
            offender.setMovementId(ma.getMovementId());
            offender.setMovementType(ma.getMovementType());

            offender.setFromFacilityInternalLocation(ma.getFromFacilityInternalLocationId());
            offender.setToFacilityInternalLocation(ma.getToFacilityInternalLocationId());
            offender.setMovementReason(ma.getMovementReason());
            offender.setMovementStatus(ma.getMovementStatus());
            offender.setSupervisionId(ma.getSupervisionId());
            Long personIdentityId = SupervisionServiceAdapter.get(uc, ma.getSupervisionId()).getPersonIdentityId();
            PersonIdentityType pi = PersonServiceAdapter.getPersonIdentityType(uc, personIdentityId, null);
            offender.setFirstName(pi.getFirstName());
            offender.setLastName(pi.getLastName());
            offender.setOffenderNumber(pi.getOffenderNumber());
            offender.setPersonIdentifierId(pi.getPersonIdentityId());

            offender.setMovementDirection(ma.getMovementDirection());

            SupervisionType supervisionType = null;
            FacilityInternalLocation fil = null;

            if (null != ma.getSupervisionId()) {
                String currentLocation = SupervisionServiceAdapter.getOffenderCurrentLocationBySupervisionId(uc, ma.getSupervisionId());
                offender.setCurrentLocation(currentLocation);

                supervisionType = handler.get(ma.getSupervisionId());
                if (null != supervisionType) {
                    offender.setSupervisionDisplayId(supervisionType.getSupervisionDisplayID());
                    offender.setInmatePersonId(supervisionType.getPersonId());
                    offender.setInmateCurrentFacility(supervisionType.getFacilityId());
                }
            }

            offender.setActivityId(ma.getActivityId());

            if (null != ma.getFromFacilityInternalLocationId()) {
                fil = facilityInternalLocationService.get(uc, ma.getFromFacilityInternalLocationId());
                if (null != fil) {
                    offender.setFromFacilityInternalLocationName(fil.getDescription());
                }

            }

            if (null != ma.getToFacilityInternalLocationId()) {
                fil = facilityInternalLocationService.get(uc, ma.getToFacilityInternalLocationId());
                if (null != fil) {
                    offender.setToFacilityInternalLocationName(fil.getDescription());
                }

            }
            Set<CommentType> commentTypes = ma.getCommentText();
            if (null != commentTypes && commentTypes.size() > 0) {
                for (CommentType c : commentTypes) {
                    offender.setComment(c.getComment());
                }
            }

            offender.setMovementDate(ma.getMovementDate());
            offender.setMovementCategory(ma.getMovementCategory());
            offender.setMovementOutcome(ma.getMovementOutcome());
            offender.setFromFacility(search.getFromFacility());

            conflictHandler = new ConflictHandler(context, session, supervisionService);
            checkNonAssociationConflicts(uc, offender);
            scheduleList.add(offender);
        }
        return scheduleList;
    }

    @Override
    public List<OffenderScheduleTransferType> searchMovements(UserContext uc, MovementActivitySearchType searchType) {
        ValidationHelper.validate(searchType);
        DefaultMovementHandler movementHandler = selectMovementHandler(uc, searchType.getMovementType(), searchType.getMovementReason());
        List<OffenderScheduleTransferType>  movements = movementHandler.search(uc, searchType);
        Set<Long> supervisionIds = new HashSet<>();
        for(OffenderScheduleTransferType  movement : movements){
            supervisionIds.add(movement.getSupervisionId());
        }
        SupervisionSearchType search = new SupervisionSearchType();
        search.setSupervisionStatusFlag(true);
        List<SupervisionType>   supervisionTypes= supervisionService.search(uc, supervisionIds, search, 0L, 200L, null).getSupervisions();
        Map<Long, SupervisionType> maps = new HashMap<Long, SupervisionType>();
        for (SupervisionType supervisionType : supervisionTypes) {
            maps.put(supervisionType.getSupervisionIdentification(), supervisionType);
        }


        if(null!=searchType.getInterestFaclityId()){
            Iterator<OffenderScheduleTransferType> iterator = movements.iterator();
            while(iterator.hasNext()){
                OffenderScheduleTransferType  offender = iterator.next();
                if(!searchType.getInterestFaclityId().equals(maps.get(offender.getSupervisionId()).getFacilityId())){
                    iterator.remove();
                }

            }

            return movements;

        }
        if(null!=searchType.getFacilitySetId()){

            FacilitySetType  facilitySet = RegistryServiceAdapter.getFacilitySet(uc,searchType.getFacilitySetId(), true);
            List<Long>  ids = facilitySet.getFacilityList();


            Iterator<OffenderScheduleTransferType> iterator = movements.iterator();
            while(iterator.hasNext()){
                OffenderScheduleTransferType  offender = iterator.next();
                if(!ids.contains(maps.get(offender.getSupervisionId()).getFacilityId())){
                    iterator.remove();
                }

            }

            return  movements;

        }

        return movements;
    }

    @Override
    public List<OffenderScheduleTransferType> getScheduledExternalMovements(UserContext uc, ScheduleTransferMovementSearch search) {

        String methodName = "getOffenderScheduleTransferList";

        if (uc == null) {
            String message = "UserContext is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        ValidationHelper.validate(search);

        //WOR-10159: Should add time part as 23:59:59 for toScheduledDate.
        Date toScheduleDate = search.getToScheduledDate();
        toScheduleDate = BeanHelper.createDateWithTime(toScheduleDate, 23, 59, 59);
        search.setToScheduledDate(toScheduleDate);

        // Either FromFacilityId or ToFacilityId are required.
        if (search.getFromFacility() == null && search.getToFacility() == null) {
            String message = "Invalid schedule input search. - Either FromFacilityId or ToFacilityId are required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        List<OffenderScheduleTransferType> scheduleList = new ArrayList<OffenderScheduleTransferType>();
        MovementActivitySearchType maSearch = new MovementActivitySearchType();


        if(!MovementActivityType.MovementType.APPO.code().equalsIgnoreCase(search.getMovementType())){
            maSearch.setMovementCategory(CATEGORY_EXTERNAL);
        }
        if (search.getFromFacility() != null) {
            maSearch.setFromFacilityId(search.getFromFacility());
        }

        if (search.getToFacility() != null) {
            maSearch.setToFacilityId(search.getToFacility());
        }

        if (search.getMovementDirection() != null) {
            maSearch.setMovementDirection(search.getMovementDirection());
        }

        if (search.getMovementStatus() != null) {
            maSearch.setMovementStatus(search.getMovementStatus());
        }
        if (search.getMovementType() != null) {
            maSearch.setMovementType(search.getMovementType());
        }

        // WOR-10080 : wmadruga
        // Scheduled movements should be verified against activity planned date instead of movement move date.
        if (search.getFromScheduledDate() != null) {
            maSearch.setPlannedStartDate(search.getFromScheduledDate());
        }
        if (search.getToScheduledDate() != null) {
            maSearch.setPlannedEndDate(search.getToScheduledDate());
        }
        // end of WOR-10080 : wmadruga

        maSearch.setFilterInOut(search.isFilterInOut());

        MovementActivitiesReturnType retMovements = MovementServiceAdapter.searchMovementActivity(uc, maSearch);

        List<ExternalMovementActivityType> externalMoves = new ArrayList<ExternalMovementActivityType>();
        if (retMovements.getExternalMovementActivity() != null && retMovements.getExternalMovementActivity().size() > 0) {
            externalMoves = retMovements.getExternalMovementActivity();

        }

        supervisionIdSet = new HashSet<Long>();
        for (ExternalMovementActivityType ma : externalMoves) {
            if(!supervisionIdSet.contains(ma.getSupervisionId())){
                supervisionIdSet.add(ma.getSupervisionId());
            }
        }

        SupervisionHandler handler = new SupervisionHandler(context, session);
        for (ExternalMovementActivityType ma : externalMoves) {
            OffenderScheduleTransferType offender = new OffenderScheduleTransferType();

            offender.setActivityId(ma.getActivityId());
            offender.setParticipateOfficerId(ma.getParticipateStaffId());
            offender.setMovementId(ma.getMovementId());
            offender.setGroupId(ma.getGroupId());
            offender.setMovementDirection(ma.getMovementDirection());
            offender.setCancelReasoncode(ma.getCancelReason());
            offender.setScheduledTransferStartTime(ma.getPlannedStartDate());
            offender.setReturnTime(ma.getPlannedEndDate());
            ActivityType activity = ActivityServiceAdapter.getActivity(uc, ma.getActivityId());
            ActivityType activityAntecedentDescident = null;
            //activity is returning null if Activity flag is 0 and is throwing NPE
            if (null != activity) {

                if (ma.getMovementType().contentEquals(MovementActivityType.MovementType.TAP.code())) {
                    if (ma.getMovementDirection().contentEquals(MovementActivityType.MovementDirection.OUT.code())) {
                        offender.setScheduledTransferDate(activity.getPlannedStartDate());
                        offender.setScheduledTransferTime(timeFormat.format(activity.getPlannedStartDate()));
                    } else if (ma.getMovementDirection().contentEquals(MovementActivityType.MovementDirection.IN.code())) {
                        offender.setScheduledTransferDate(activity.getPlannedEndDate());
                        offender.setScheduledTransferTime(timeFormat.format(activity.getPlannedEndDate()));
                    }
                } else {
                    offender.setScheduledTransferDate(activity.getPlannedStartDate());
                    offender.setScheduledTransferTime(timeFormat.format(activity.getPlannedStartDate()));
                }

            }
            Long activityInId = null;
            if (null != activity && (ma.getMovementType().contentEquals(MovementActivityType.MovementType.TAP.code()) || ma.getMovementType().contentEquals(
                    MovementActivityType.MovementType.CRT.code()))) {
                if (null != activity.getActivityAntecedentId()) {
                    activityAntecedentDescident = ActivityServiceAdapter.getActivity(uc, activity.getActivityAntecedentId());
                    if (null != activityAntecedentDescident) {
                        activityInId = activityAntecedentDescident.getActivityIdentification();
                    }

                } else {

                    String hql = "FROM ActivityEntity ae where ae.antecedentId = " + activity.getActivityIdentification();
                    Query query2 = session.createQuery(hql);
                    List results = query2.list();

                    if (null != results && !results.isEmpty()) {
                        ActivityEntity entity = (ActivityEntity) results.get(0);
                        activityInId = entity.getActivityId();
                        activityAntecedentDescident = activity;
                        activity = ActivityServiceAdapter.getActivity(uc, entity.getActivityId());

                    }

                }

                if (null != activity.getPlannedStartDate()) {
                    offender.setReturnDate(activity.getPlannedStartDate());
                    offender.setReturnTime(activity.getPlannedStartDate());
                }
                if (null != activityAntecedentDescident && null != activityAntecedentDescident.getPlannedStartDate()) {
                    offender.setReleaseDate(activityAntecedentDescident.getPlannedStartDate());
                    offender.setReleaseTime(activityAntecedentDescident.getPlannedStartDate());
                }
                setAssociatedMovementProperty(offender);

            }

            if (!isMovePlanned(search.getFromScheduledDate(), search.getToScheduledDate(), offender.getScheduledTransferDate())) {
                continue;
            }
            offender.setMovementType(ma.getMovementType());
            offender.setFromFacility(ma.getFromFacilityId());
            offender.setToFacility(ma.getToFacilityId());
            offender.setGroupId(ma.getGroupId());
            offender.setFromCity(ma.getFromCity());
            offender.setToCity(ma.getToCity());
            offender.setFromLocation(ma.getFromLocationId());
            offender.setToLocation(ma.getToLocationId());
            offender.setToOrganization(ma.getToOrganizationId());
            offender.setToFacilityInternalLocation(ma.getToFacilityInternalLocationId());
            offender.setMovementReason(ma.getMovementReason());
            offender.setMovementStatus(ma.getMovementStatus());
            offender.setEscortOrgId(ma.getEscortOrganizationId());
            offender.setEscortDetals(ma.getEscortDetails());
            offender.setApproverId(ma.getApprovedByStaffId());
            offender.setApprovalDate(ma.getApprovalDate());
            offender.setApprovalcomments(ma.getApprovalComments());

            offender.setSupervisionId(ma.getSupervisionId());
            Long personIdentityId = SupervisionServiceAdapter.get(uc, ma.getSupervisionId()).getPersonIdentityId();
            PersonIdentityType pi = PersonServiceAdapter.getPersonIdentityType(uc, personIdentityId, null);
            offender.setFirstName(pi.getFirstName());
            offender.setLastName(pi.getLastName());
            offender.setOffenderNumber(pi.getOffenderNumber());
            offender.setPersonIdentifierId(pi.getPersonIdentityId());

            SupervisionType supervisionType = null;
            FacilityInternalLocation fil = null;
            Facility facilityType = null;
            if (null != ma.getSupervisionId()) {
                String strCurrentLocation = SupervisionServiceAdapter.getOffenderCurrentLocationBySupervisionId(uc, ma.getSupervisionId());
                offender.setCurrentLocation(strCurrentLocation);

                supervisionType = handler.get(ma.getSupervisionId());
                if (null != supervisionType) {
                    offender.setSupervisionDisplayId(supervisionType.getSupervisionDisplayID());
                    offender.setInmatePersonId(supervisionType.getPersonId());
                    offender.setInmateCurrentFacility(supervisionType.getFacilityId());
                }
            }

            offender.setActivityId(ma.getActivityId());

            if (null != ma.getToFacilityInternalLocationId()) {
                fil = facilityInternalLocationService.get(uc, ma.getToFacilityInternalLocationId());
                if (null != fil) {
                    offender.setToFacilityInternalLocationName(fil.getDescription());
                }
            }

            if (null != ma.getToFacilityId()) {
                facilityType = FacilityServiceAdapter.getFacility(uc, ma.getToFacilityId());
                if (null != facilityType) {
                    offender.setToFacilityName(facilityType.getFacilityName());
                }
            }

            if (null != ma.getFromLocationId()) {
                fil = facilityInternalLocationService.get(uc, ma.getFromLocationId());
                if (null != fil) {
                    offender.setFromFacilityInternalLocationName(fil.getDescription());

                }

            }

            if (null != ma.getFromFacilityId()) {
                facilityType = FacilityServiceAdapter.getFacility(uc, ma.getFromFacilityId());
                if (null != facilityType) {
                    offender.setFromFacilityName(facilityType.getFacilityName());
                }
            }

            offender.setTransportation(ma.getTransportation());
            offender.setApplicationDate(ma.getApplicationDate());
            offender.setMovementDate(ma.getMovementDate());
            offender.setReportingDate(ma.getReportingDate());

            offender.setMovementCategory(ma.getMovementCategory());
            offender.setMovementOutcome(ma.getMovementOutcome());
            offender.setSupervisionId(ma.getSupervisionId());
            Set<CommentType> commentTypes = ma.getCommentText();
            if (null != commentTypes && commentTypes.size() > 0) {
                for (CommentType c : commentTypes) {
                    offender.setComment(c.getComment());
                }
            }


            // Moved business logic from webapp to service layer (It was written before in BulkMovementModel)

            HousingAssignmentType housingAssignmentType = null;
            Set<Long> associationSet = new HashSet<Long>();
            associationSet.add(offender.getSupervisionId());

            HousingBedManagementActivityHandler houBedMangActhandler = new HousingBedManagementActivityHandler(context, session);
            OffenderHousingAssignmentsReturnType ret = houBedMangActhandler.retrieveCurrentAssignmentsByOffenders(uc, associationSet);

            if(null != ret) {
                List<OffenderHousingAssignmentType> setType = ret.getOffenderHousingAssignments();
                Iterator it = setType.iterator();
                while (it.hasNext()) {
                    OffenderHousingAssignmentType offType = (OffenderHousingAssignmentType) it.next();
                    housingAssignmentType = offType.getHousingAssignment();
                }

                if (null != housingAssignmentType && null != housingAssignmentType.getLocationTo()) {
                    fil = facilityInternalLocationService.get(uc, housingAssignmentType.getLocationTo());
                    offender.setHousing(fil.getDescription());
                    if (offender.getEscortOrgId() != null) {
                        Long orgId = offender.getEscortOrgId();
                        OrganizationEntity orgEntity = (OrganizationEntity) session.get(OrganizationEntity.class, orgId);
                        if (orgEntity != null) {
                            offender.setEscortOrgName(orgEntity.getOrganizationName());
                        }
                    }

                }
            }

            if(!"APPO".equals(maSearch.getMovementType())) {
                conflictHandler = new ConflictHandler(context, session, supervisionService);
                checkNonAssociationConflicts(uc, offender);
            }

            scheduleList.add(offender);
        }

        return scheduleList;
    }

    /**
     * Retrieve external Schedule transfer movements for a facility.
     *
     * @param uc     UserContext - Required
     * @param search ScheduleTransferMovementSearch - Required
     * @return TransferMovementReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    @Override
    public List<OffenderScheduleTransferType> getOffenderScheduleTransferList(UserContext uc, ScheduleTransferMovementSearch search) {

        List<OffenderScheduleTransferType> ret = new ArrayList<OffenderScheduleTransferType>();
        String methodName = "getOffenderScheduleTransferList";

        if (uc == null) {
            String message = "UserContext is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        ValidationHelper.validate(search);

        // Either FromFacilityId or ToFacilityId are required.
        if (search.getFromFacility() == null && search.getToFacility() == null) {
            String message = "Invalid schedule input search. - Either FromFacilityId or ToFacilityId are required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        SupervisionSearchType svSearch = new SupervisionSearchType();

        svSearch.setFacilityId(search.getFromFacility());
        svSearch.setSupervisionStatusFlag(Boolean.TRUE);

        List<SupervisionType> retSupervisions = MovementHelper.searchSupervision(uc, svSearch);

        Set<OffenderScheduleTransferType> schedule = new HashSet<OffenderScheduleTransferType>();

        MovementActivitySearchType maSearch = new MovementActivitySearchType();

        if (search.getFromFacility() != null) {
            maSearch.setFromFacilityId(search.getFromFacility());
        }

        if (search.getToFacility() != null) {
            maSearch.setToFacilityId(search.getToFacility());
        }

        if (search.getFromFacilityInternalLocation() != null) {
            maSearch.setFromFacilityInternalLocationId(search.getFromFacilityInternalLocation());
        }

        if (search.getFromLocation() != null) {
            maSearch.setFromLocationId(search.getFromLocation());
        }

        if (search.getFromOrganization() != null) {
            maSearch.setFromOrganizationId(search.getFromOrganization());
        }

        if (search.getFromCity() != null) {
            maSearch.setFromCity(search.getFromCity());
        }

        if (search.getToFacilityInternalLocation() != null) {
            maSearch.setToFacilityInternalLocationId(search.getToFacilityInternalLocation());
        }

        if (search.getToLocation() != null) {
            maSearch.setToLocationId(search.getToLocation());
        }

        if (search.getToOrganization() != null) {
            maSearch.setToOrganizationId(search.getToOrganization());
        }

        if (search.getToCity() != null) {
            maSearch.setToCity(search.getToCity());
        }

        if (search.getMovementStatus() != null) {
            maSearch.setMovementStatus(search.getMovementStatus());
        }

        if (search.getMovementType() != null) {
            maSearch.setMovementType(search.getMovementType());
        }

        for (SupervisionType supervision : retSupervisions) {
            Long supervisionId = supervision.getSupervisionIdentification();

            maSearch.setSupervisionId(supervisionId);
            MovementActivitiesReturnType retMovements = MovementServiceAdapter.searchMovementActivity(uc, maSearch);

            List<ExternalMovementActivityType> ema = new ArrayList<ExternalMovementActivityType>();
            if (retMovements.getExternalMovementActivity() != null) {
                if (retMovements.getExternalMovementActivity().size() > 0) {
                    ema = retMovements.getExternalMovementActivity();
                }
            }
            for (ExternalMovementActivityType ma : ema) {
                OffenderScheduleTransferType offender = new OffenderScheduleTransferType();

                offender.setMovementId(ma.getMovementId());

                if (ma.getGroupId() != null) {
                    offender.setGroupId(ma.getGroupId());
                }
                if (ma.getFromCity() != null) {
                    offender.setFromCity(ma.getFromCity());
                }

                if (ma.getToCity() != null) {
                    offender.setToCity(ma.getToCity());
                }

                if (ma.getFromLocationId() != null) {
                    offender.setFromLocation(ma.getFromLocationId());
                }

                if (ma.getToLocationId() != null) {
                    offender.setToLocation(ma.getToLocationId());
                }

                if (ma.getFromOrganizationId() != null) {
                    offender.setFromOrganization(ma.getFromOrganizationId());
                }

                if (ma.getToOrganizationId() != null) {
                    offender.setToOrganization(ma.getToOrganizationId());
                }

                if (ma.getToFacilityInternalLocationId() != null) {
                    offender.setToFacilityInternalLocation(ma.getToFacilityInternalLocationId());
                }

                if (ma.getFromFacilityId() != null) {
                    offender.setFromFacility(ma.getFromFacilityId());
                }
                if (ma.getToFacilityId() != null) {
                    offender.setToFacility(ma.getToFacilityId());
                }

                if (ma.getMovementReason() != null) {
                    offender.setMovementReason(ma.getMovementReason());
                }

                Long activityId = ma.getActivityId();
                offender.setActivityId(activityId);
                ActivityType activity = ActivityServiceAdapter.getActivity(uc, activityId);

                // wmadruga WOR-4535 - filling different dates for different movements in TAP
                if (ma.getMovementType().contentEquals(MovementActivityType.MovementType.TAP.code())) {
                    if (ma.getMovementDirection().contentEquals(MovementActivityType.MovementDirection.OUT.code())) {
                        offender.setScheduledTransferDate(activity.getPlannedStartDate());
                        offender.setScheduledTransferTime(timeFormat.format(activity.getPlannedStartDate()));
                    } else if (ma.getMovementDirection().contentEquals(MovementActivityType.MovementDirection.IN.code())) {
                        offender.setScheduledTransferDate(activity.getPlannedEndDate());
                        offender.setScheduledTransferTime(timeFormat.format(activity.getPlannedEndDate()));
                    }
                } else {
                    offender.setScheduledTransferDate(activity.getPlannedStartDate());
                    offender.setScheduledTransferTime(timeFormat.format(activity.getPlannedStartDate()));
                }

                offender.setSupervisionId(supervisionId);
                Long personIdentityId = supervision.getPersonIdentityId();
                PersonIdentityType pi = PersonServiceAdapter.getPersonIdentityType(uc, personIdentityId, null);
                offender.setFirstName(pi.getFirstName());
                offender.setLastName(pi.getLastName());
                offender.setOffenderNumber(pi.getOffenderNumber());
                offender.setPersonIdentifierId(pi.getPersonIdentityId());

                schedule.add(offender);
            }
        }
        if (!schedule.isEmpty()) {
            for (OffenderScheduleTransferType schOff : schedule) {
                if (isMovePlanned(search.getFromScheduledDate(), search.getToScheduledDate(), schOff.getScheduledTransferDate())) {
                    ret.add(schOff);
                }
            }
        }

        return ret;
    }

    /**
     * Retrieve schedule movement for a facility.
     *
     * @param uc             UserContext - Required
     * @param fromFacilityId Long - Required
     * @param parentId       Long - Required
     * @param fromDate       Date - Optional
     * @param toDate         Date - Optional
     * @return ScheduleMovementReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    @Override
    public List<ScheduleMovementType> getScheduleMovements(UserContext uc, Long fromFacilityId, Long parentId, Date fromDate, Date toDate) {

        List<ScheduleMovementType> ret = new ArrayList<ScheduleMovementType>();

        String methodName = "getScheduleMovements";

        if (uc == null) {
            String message = "UserContext is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (fromFacilityId == null) {
            String message = "fromFacilityId is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (parentId == null) {
            String message = "parentId is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        List<InternalLocationIdentifierType> childLocations = MovementHelper.getDirectChildLocations(uc, LocationCategory.HOUSING, parentId, facilityInternalLocationService);

        Set<Long> locations = new HashSet<Long>();
        for (InternalLocationIdentifierType fil : childLocations) {
            locations.add(fil.getId());
        }

        Long count = 0L;

        for (Long loc : locations) {

            ScheduleMovementType schedule = new ScheduleMovementType();

            schedule.setLocationId(loc);
            String locationCode = MovementHelper.getLocationCode(uc, loc, facilityInternalLocationService);
            if (locationCode != null) {
                schedule.setLocationCode(locationCode);
            }
            Set<Long> scheduleTransferOut = new HashSet<Long>();
            Set<Long> onTransferWaitlist = new HashSet<Long>();
            Set<Long> scheduleCourtMove = new HashSet<Long>();
            Set<Long> scheduleTemporaryAbsence = new HashSet<Long>();

            count = count + 1;

            Set<SupervisionLocationType> supervisions = new HashSet<SupervisionLocationType>();

            Set<Long> locationTos = MovementHelper.getLeafLocations(uc, loc, LocationCategory.HOUSING, facilityInternalLocationService);

            List<LocationHousingAssignmentType> locaAssigns = MovementHelper.getLocationHousingAssignments(uc, locationTos);

            if (locaAssigns != null) {
                for (LocationHousingAssignmentType assign : locaAssigns) {
                    List<OffenderHousingAssignmentType> offenderHousings = assign.getOffenderHousingAssignments();
                    for (OffenderHousingAssignmentType offender : offenderHousings) {
                        Long sv = null;
                        Long loca = null;
                        if (offender.getSupervisionId() != null && offender.getHousingAssignment().getLocationTo() != null) {
                            sv = offender.getSupervisionId();
                            loca = offender.getHousingAssignment().getLocationTo();
                            supervisions.add(new SupervisionLocationType(loca, sv));
                        }
                    }
                }
            }

            for (SupervisionLocationType sv : supervisions) {

                Long supervisionId = sv.getSupervisionId();

                MovementActivitySearchType maSearch = new MovementActivitySearchType();

                maSearch.setSupervisionId(supervisionId);
                maSearch.setMovementStatus(STATUS_PENDING);
                MovementActivitiesReturnType retMovements = MovementServiceAdapter.searchMovementActivity(uc, maSearch);

                Set<ExternalMovementActivityType> externalMoves = new HashSet<ExternalMovementActivityType>();

                if (retMovements.getExternalMovementActivity() != null) {
                    if (retMovements.getExternalMovementActivity().size() > 0) {
                        externalMoves.addAll(retMovements.getExternalMovementActivity());
                    }
                }

                for (ExternalMovementActivityType ma : externalMoves) {
                    ActivityType activity = getActivity(uc, ma);
                    Date planStartDate = activity.getPlannedStartDate();

                    if (isMovePlanned(fromDate, toDate, planStartDate)) {
                        if (ma.getMovementType() != null) {
                            if (ma.getMovementType().contentEquals(TYPE_CRT)) {
                                scheduleCourtMove.add(supervisionId);
                            } else if (ma.getMovementType().contentEquals(TYPE_TAP)) {
                                scheduleTemporaryAbsence.add(supervisionId);
                            } else if (ma.getMovementType().contentEquals(TYPE_TRNIJ)) {
                                if (ma.getMovementCategory() != null && ma.getMovementCategory().contentEquals(CATEGORY_EXTERNAL)) {
                                    scheduleTransferOut.add(supervisionId);
                                }
                            }
                        }
                    }
                }

                Boolean wlists = MovementHelper.getTransferWaitlistsFound(uc, fromFacilityId, supervisionId, fromDate, toDate);
                if (wlists) {
                    onTransferWaitlist.add(supervisionId);
                }
            }

            schedule.setScheduleTransferOut(scheduleTransferOut);
            schedule.setOnTransferWaitlist(onTransferWaitlist);
            schedule.setScheduleCourtMove(scheduleCourtMove);
            schedule.setScheduleTemporaryAbsence(scheduleTemporaryAbsence);

            ret.add(schedule);

        }
        return ret;
    }

    /**
     * Retrieve Internal Location Count for a Facility Internal Location.
     *
     * @param uc       UserContext - Required
     * @param parentId Long - Required
     * @return InternalLocationCountReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    @Override
    public List<InternalLocationCountType> getInternalLocationCounts(UserContext uc, Long parentId) {

        String methodName = "getInternalLocationCounts";

        if (uc == null) {
            String message = "UserContext is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }
        if (parentId == null) {
            String message = "parentId is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        List<InternalLocationCountType> locations = new ArrayList<InternalLocationCountType>();

        List<InternalLocationIdentifierType> loca = MovementHelper.getDirectChildLocations(uc, LocationCategory.ACTIVITY, parentId, facilityInternalLocationService);

        for (InternalLocationIdentifierType locty : loca) {

            Long totalOptimalCapacity = 0L;
            Long totalOverflowCapacity = 0L;

            Long optimalAvailable = 0L;
            Long overflowAvailable = 0L;

            InternalLocationCountType loc = new InternalLocationCountType();
            if (locty.getUniqueCode() != null) {
                loc.setLocationId(locty.getId());
                loc.setLocationCode(locty.getUniqueCode());
                Long locationId = locty.getId();
                FacilityInternalLocation fil = MovementHelper.getFacilityInternalLocation(uc, locationId, facilityInternalLocationService);
                Set<CapacityType> capacities = fil.getCapacities();
                for (CapacityType ct : capacities) {

                    String usageCategory = ct.getUsageCategory().value();
                    Long overflowCapacity = ct.getOverflowCapacity();
                    totalOverflowCapacity = (usageCategory.equals(USAGETYPE_ACTIVITY)) ? totalOverflowCapacity + overflowCapacity : totalOverflowCapacity;
                    overflowAvailable = totalOverflowCapacity;

                    Long optimalCapacity = ct.getOptimalCapacity();
                    totalOptimalCapacity = (usageCategory.equals(USAGETYPE_ACTIVITY)) ? totalOptimalCapacity + optimalCapacity : totalOptimalCapacity;
                    optimalAvailable = totalOptimalCapacity;
                }

                //Set<Long> leafLocations = getLeafLocations(uc, fil.getId(), LocationCategory.HOUSING);

                Set<Long> leafLocations = new HashSet<Long>();
                leafLocations.add(fil.getId());

                Set<Long> supervisions = MovementHelper.getSupervisionByLocation(uc, leafLocations);

                Integer offenderIn = supervisions.size();

                if (offenderIn >= totalOptimalCapacity) {
                    optimalAvailable = 0L;
                    overflowAvailable = overflowAvailable - (offenderIn - totalOptimalCapacity);
                } else {
                    optimalAvailable = totalOptimalCapacity - offenderIn;
                }

                loc.setOptimalCapacity(totalOptimalCapacity);
                loc.setOverflowCapacity(totalOverflowCapacity);
                loc.setSupervisions(supervisions);

                loc.setOptimalAvailable(optimalAvailable);
                loc.setOverflowAvailable(overflowAvailable);

                locations.add(loc);
            }
        }

        return locations;

    }

    /**
     * Retrieve incoming supervisions by Internal Facility Location.
     *
     * @param uc         UserContext - Required
     * @param toFacility Long - Required
     * @param fromDate   Date - Optional
     * @param toDate     Date - Optional
     * @return ScheduleSupervisionReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    @Override
    public List<ScheduleSupervisionType> getIncomingSupervisionsByFacility(UserContext uc, Long toFacility, Date fromDate, Date toDate) {

        List<ScheduleSupervisionType> ret = new ArrayList<ScheduleSupervisionType>();

        String methodName = "getIncomingSupervisionsByFacility";

        if (uc == null) {
            String message = "UserContext is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (toFacility == null) {
            String message = "toFacility is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        Long count = 0L;

        MovementActivitySearchType search = new MovementActivitySearchType();

        search.setMovementType(TYPE_TRNIJ);
        search.setToFacilityId(toFacility);
        search.setMovementStatus(STATUS_COMPLETE);
        search.setMovementDirection(DIRECTION_OUT);
        //Incoming Supervision shall be based on Reporting date
        if (fromDate != null) {
            search.setFromReportingDate(fromDate);
        }
        if (toDate != null) {
            search.setToReportingDate(toDate);
        }
        MovementActivitiesReturnType movements = MovementServiceAdapter.searchMovementActivity(uc, search);
        if (movements.getExternalMovementActivity() != null) {
            if (movements.getExternalMovementActivity().size() > 0) {
                Set<Long> supervisions = new HashSet<Long>();
                for (ExternalMovementActivityType ema : movements.getExternalMovementActivity()) {
                    if (ema.getSupervisionId() != null) {
                        supervisions.add(ema.getSupervisionId());
                    }

                }
                //HPQC#2184 get last completed supervisions
                if (supervisions != null || !supervisions.isEmpty()) {

                    Set<ExternalMovementActivityType> externalMoves = MovementHelper.getLastCompletedMovementBySupervision(uc, supervisions);
                    if (externalMoves != null) {
                        for (ExternalMovementActivityType ema : externalMoves) {
                            if (ema.getSupervisionId() != null) {
                                ret.add(new ScheduleSupervisionType(ema.getSupervisionId()));
                                count = count + 1;
                            }
                        }
                    }
                }
            }
        }
        return ret;

    }

    private  List<OffenderScheduleTransferType> getAdmitIncomingTransferSearch(UserContext uc, Long toFacility, Long supervisionIdentity) {
        String methodName = "getAdmitIncomingTransferSearch";

        if (uc == null) {
            String message = "UserContext is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        if (toFacility == null) {
            String message = "toFacility is required.";
            LogHelper.error(log, methodName, message);
            throw new InvalidInputException(message);
        }

        //		Long count = 0L;
        MovementActivitySearchType search = new MovementActivitySearchType();

        search.setMovementType(TYPE_TRNIJ);
        search.setToFacilityId(toFacility);
        search.setMovementStatus(STATUS_COMPLETE);
        search.setMovementDirection(DIRECTION_OUT);
        if(supervisionIdentity !=null){
            search.setSupervisionId(supervisionIdentity);
        }

        MovementActivitiesReturnType movements = search(uc, search, 0L, 200L, null);
        Set<Long> supervisions = new HashSet<Long>();
        if (movements.getExternalMovementActivity() != null) {
            for (ExternalMovementActivityType ema : movements.getExternalMovementActivity()) {
                if (ema.getSupervisionId() != null) {
                    supervisions.add(ema.getSupervisionId());
                }
            }
        }

        List<OffenderScheduleTransferType> schedule = new ArrayList<OffenderScheduleTransferType>();

        if (!supervisions.isEmpty()) {
            Set<ExternalMovementActivityType> moves = MovementHelper.getLastCompletedMovementBySupervision(uc, supervisions);
            if (moves != null) {
                for (ExternalMovementActivityType ema : moves) {
                    if (ema.getSupervisionId() != null) {
                        Long supervisionId = ema.getSupervisionId();
                        SupervisionType supervision = SupervisionServiceAdapter.get(uc, supervisionId);

                        PersonIdentityType pi = PersonServiceAdapter.getPersonIdentityType(uc, supervision.getPersonIdentityId(), null);
                        OffenderScheduleTransferType offender = new OffenderScheduleTransferType();
                        offender.setFirstName(pi.getFirstName());
                        offender.setLastName(pi.getLastName());
                        offender.setOffenderNumber(pi.getOffenderNumber());
                        offender.setPersonIdentifierId(pi.getPersonIdentityId());
                        if (ema.getReportingDate() != null) {
                            offender.setScheduledTransferDate(ema.getReportingDate());
                            offender.setScheduledTransferTime(timeFormat.format(ema.getReportingDate()));
                        }
                        if (ema.getToFacilityId() != null) {
                            offender.setToFacility(ema.getToFacilityId());
                        }
                        if (ema.getFromFacilityId() != null) {
                            offender.setFromFacility(ema.getFromFacilityId());
                        }
                        offender.setMovementId(ema.getMovementId());
                        offender.setSupervisionId(supervisionId);
                        offender.setMovementReason(ema.getMovementReason());
                        offender.setInmatePersonId(pi.getPersonId());
                        schedule.add(offender);
                    }
                }
            }
        }

        return schedule;
    }
    /**
     * Retrieve incoming offenders by facility.
     *
     * @param uc         UserContext - Required
     * @param toFacility Long - Required
     * @return OffenderScheduleTransferReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    @Override
    public List<OffenderScheduleTransferType> getAdmitIncomingTransfer(UserContext uc, Long toFacility) {
        return getAdmitIncomingTransferSearch(uc,toFacility,null);

    }

    /**
     * Only handler TCP, CRT goes to  createMovementSchedule directly via its template
     * @param uc        UserContext - Required
     * @param movements T extends @{link TwoMovementsType} - Required
     * @param isAdhoc   Boolean - If it is an ad-hoc or not, Required
     * @param <T>
     * @return
     **/
    @Deprecated
    @SuppressWarnings("unchecked")
    @Override
    public <T extends TwoMovementsType> List<T> createTwoMovements(UserContext uc, List<T> movements, Boolean isAdhoc) {


        List<T> rets = new ArrayList<T>();
        Boolean approvalRequired = true;


        for (T movement : movements) {
            if (movement instanceof TemporaryAbsenceType) {
                TemporaryAbsenceType temporaryAbsenceType = (TemporaryAbsenceType) movement;
                if (temporaryAbsenceType.getToFacilityLocationId() == null && temporaryAbsenceType.getToOrganizationLocationId() == null &&
                        temporaryAbsenceType.getToOffenderLocationId() == null) {
                    throw new InvalidInputException("ToFacilityLocationId or ToOrganizationLocationId or ToOffenderLocationId not provided.");
                }
                ValidationHelper.validate(temporaryAbsenceType);
                approvalRequired = false;
                movement.setMovementInStatus(STATUS_PENDING);
            } else if (movement instanceof CourtMovementType) {
                approvalRequired = false;
                movement.setMovementInStatus(STATUS_PENDING);
                movement.setMovementOutStatus(STATUS_PENDING);
                ValidationHelper.validate(((CourtMovementType) movement));
            }

            // verifies scheduling conflicts against movement.
            //			checkSchedulingConflict(uc, movement);

            // if there is any conflicts, return the original object with the conflicts to be shown.
            //			if(!movement.getConflicts().isEmpty()) {
            //				rets.add(movement);
            //			}
            //			// if there is no conflicts, create the new movement and return to client.
            //			else {
            movement.setGroupId(UUID.randomUUID().toString());

            MovementActivityType moveIn = TwoMovementsHelper.toMovementActivityType(movement, DIRECTION_IN, isAdhoc);
            MovementActivityType moveOut = TwoMovementsHelper.toMovementActivityType(movement, DIRECTION_OUT, isAdhoc);

            List<MovementDetailType> ret = new ArrayList<MovementDetailType>();

            ActivityType activOUT = TwoMovementsHelper.buildActivity(movement, DIRECTION_OUT);
            ActivityType activIN = TwoMovementsHelper.buildActivity(movement, DIRECTION_IN);

            // if ad-hoc we setup new statuses.
            if (isAdhoc) {
                approvalRequired = false;
            }

            MovementDetailType mdtOUT = createLinkedMovement(uc, moveOut, activOUT, movement.getSupervisionId(), approvalRequired);
            ret.add(mdtOUT);
            activIN.setActivityAntecedentId(mdtOUT.getActivity().getActivityIdentification());

            MovementDetailType mdtIN = createLinkedMovement(uc, moveIn, activIN, movement.getSupervisionId(), approvalRequired);
            ret.add(mdtIN);

            rets.add((T) TwoMovementsHelper.toTwoMovementsType(ret));
            //			}
        }

        return rets;


    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends TwoMovementsType> List<T> updateTwoMovements(UserContext uc, List<T> movements, Boolean isAdhoc) {
        List<T> rets = new ArrayList<T>();

        for (T movement : movements) {
            if (movement instanceof TemporaryAbsenceType) {
                if (((TemporaryAbsenceType) movement).getToFacilityLocationId() == null && ((TemporaryAbsenceType) movement).getToOffenderLocationId() == null) {
                    throw new InvalidInputException("ToFacilityLocationId or ToOffenderLocationId not provided.");
                }
                ValidationHelper.validate(((TemporaryAbsenceType) movement));
            } else if (movement instanceof CourtMovementType) {
                ValidationHelper.validate(((CourtMovementType) movement));
            }

            // verifies scheduling conflicts against movement.
            //			checkSchedulingConflict(uc, movement);

            // if there is any conflicts, return the original object with the conflicts to be shown.
            //			if(!movement.getConflicts().isEmpty()) {
            //				rets.add(movement);
            //			}
            //			// if there is no conflicts, update movement and return to client.
            //			else {
            List<ExternalMovementActivityType> updatedObjects = new ArrayList<ExternalMovementActivityType>();

            MovementActivityType movRet = MovementServiceAdapter.getExternalMovementActivity(uc, movement.getMovementIn());
            ActivityType act = getActivity(uc, movRet);
            updatedObjects.add(TwoMovementsHelper.updateObject(uc, movement, movRet, act, isAdhoc));

            movRet = MovementServiceAdapter.getExternalMovementActivity(uc, movement.getMovementOut());

            act = getActivity(uc, movRet);
            updatedObjects.add(TwoMovementsHelper.updateObject(uc, movement, movRet, act, isAdhoc));

            List<MovementDetailType> ret = new ArrayList<MovementDetailType>();
            if (updatedObjects.size() > 0) {
                ret.add(updateMovementActivity(uc, updatedObjects.get(0)));
                ret.add(updateMovementActivity(uc, updatedObjects.get(1)));
            }
            rets.add((T) TwoMovementsHelper.toTwoMovementsType(ret));
            //			}
        }
        return rets;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends TwoMovementsType> List<T> listTwoMovementsBy(UserContext uc, TwoMovementsSearchType searchType) {
        ValidationHelper.validate(searchType);

        Date dateOut = null;
        Date dateIn = null;
        Query query = null;
        List<T> ret = new ArrayList<T>();

        if (searchType.getMoveDate() != null) {
            dateOut = searchType.getMoveDate();
        }
        if (searchType.getReturnDate() != null) {

            dateIn = BeanHelper.createDateWithTime(searchType.getReturnDate(), 23, 59, 0);
            //shifting a minute to get better results
            //			dateOut = BeanHelper.createDateWithTime(searchType.getMoveDate(), searchType.getMoveDate().getHours(), searchType.getMoveDate().getMinutes()-1, 0);
            //			dateIn = BeanHelper.createDateWithTime(searchType.getReturnDate(), searchType.getReturnDate().getHours(), searchType.getReturnDate().getMinutes()+1, 0);
        }

        // Using Strategy to define the query
        if (searchType.getMovementType().contentEquals(MovementType.TAP.toString())) {
            MovementsHQLContext context = new MovementsHQLContext(new TemporaryAbsenceHQL());
            query = context.buildTwoMovementsHQL(session, searchType, dateOut, dateIn);
        } else if (searchType.getMovementType().contentEquals(MovementType.CRT.toString())) {
            MovementsHQLContext context = new MovementsHQLContext(new CourtMovementHQL());
            query = context.buildTwoMovementsHQL(session, searchType, dateOut, dateIn);
        } else if (searchType.getMovementType().contentEquals(MovementType.VISIT.toString())) {
            MovementsHQLContext context = new MovementsHQLContext(new VisitationHQL());
            query = context.buildTwoMovementsHQL(session, searchType, dateOut, dateIn);
        }

        if (query != null) {
            ret = query.list();
        }

        Iterator<T> iterator = ret.iterator();
        while (iterator.hasNext()) {
            T movement = iterator.next();
            //filter out  movements scheduled out of dataOut and dataIn

            if (dateOut != null && movement.getMoveDate().before(dateOut)) {
                iterator.remove();
                continue;
            }
            if (dateIn != null && movement.getReturnDate().after(dateIn)) {
                iterator.remove();
                continue;
            }

            // Visitation - filling up other information
            if (movement.getMovementType().contentEquals(MovementType.VISIT.toString())) {
                // TwoMovements groupId = VisitationId
                // getting activity related to visit
                StringBuilder ahql = new StringBuilder();
                ahql.append("FROM ActivityEntity act where act.activityId = ");
                ahql.append("(SELECT vis.activityId FROM VisitEntity vis where vis.visitId = ");
                ahql.append(movement.getGroupId());
                ahql.append(") ");
                Query visitActivityQuery = session.createQuery(ahql.toString());
                ActivityEntity visitActivity = (ActivityEntity) visitActivityQuery.uniqueResult();

                movement.setSupervisionId(visitActivity.getSupervisionId());
                movement.setMovementReason("Visitation");

                // getting movement related to visit
                StringBuilder mhql = new StringBuilder();
                mhql.append("FROM MovementActivityEntity m where m.activityId = ");
                mhql.append("(SELECT act.activityId FROM ActivityEntity act where act.antecedentId = ");
                mhql.append(visitActivity.getActivityId());
                mhql.append(") ");
                Query visitMovementQuery = session.createQuery(mhql.toString());
                MovementActivityEntity visitMovement = (MovementActivityEntity) visitMovementQuery.uniqueResult();

                //destination...
                movement.setMovementReason(visitMovement.getMovementReason());

            }

            // getting comments
            String commentHql = "FROM MovementActivityCommentEntity where movementActivity.movementId = "
                    + movement.getMovementOut(); //whatever the movement (IN or OUT), comment is the same.
            Query commentQuery = session.createQuery(commentHql);
            List<MovementActivityCommentEntity> comments = commentQuery.list();
            if (!comments.isEmpty()) {
                movement.setComments(comments.get(0).getCommentText());
            } else {
                movement.setComments(null);
            }

            // getting Offender Name
            if (searchType.getOffenderId() != null) {
                String personHql = "FROM PersonIdentityEntity WHERE personIdentityId = " + searchType.getOffenderId();
                Query personQuery = session.createQuery(personHql);
                PersonIdentityEntity identity = (PersonIdentityEntity) personQuery.uniqueResult();
                if (identity != null) {
                    movement.setOffenderId(identity.getPersonIdentityId());
                    movement.setOffenderName(identity.getFirstName() + " " + identity.getLastName());
                }
            }

            // setting values for Facility Location ID or Offender Location ID
            if (movement.getMovementType() != null) {
                if (movement.getMovementType().contentEquals(MovementType.TAP.toString())) {
                    ExternalMovementActivityEntity move = null;
                    StringBuilder moveHql = new StringBuilder();
                    moveHql.append("FROM ExternalMovementActivityEntity WHERE movementId = " +
                            movement.getMovementIn() + " ");
                    Query moveQuery = session.createQuery(moveHql.toString());
                    move = (ExternalMovementActivityEntity) moveQuery.uniqueResult();
                    if (move != null) {
                        //getting toFacilityLocationId,
                        if (move.getToFacilityId() != null) {
                            ((TemporaryAbsenceType) movement).setToFacilityLocationId(move.getToLocationId());
                        }
                        //getting toOffenderLocationId
                        else {
                            ((TemporaryAbsenceType) movement).setToOffenderLocationId(move.getToLocationId());
                        }
                    }
                }
            }

            // if the movement is completed we need to get movement date instead of planned date
            // also we need to re-validate if date is still between the target date range.
            if (!movement.getMovementType().contentEquals(MovementType.VISIT.code())) {
                if (movement.getMovementOutStatus() != null && movement.getMovementInStatus() != null) {
                    if (movement.getMovementOutStatus().contentEquals(MovementStatus.COMPLETED.name()) || movement.getMovementInStatus().contentEquals(
                            MovementStatus.COMPLETED.name())) {

                        MovementActivityEntity moveOut = null;
                        MovementActivityEntity moveIn = null;

                        StringBuilder moveOutHql = new StringBuilder();
                        StringBuilder moveInHql = new StringBuilder();
                        if (movement.getMovementOutStatus().contentEquals(MovementStatus.COMPLETED.name())) {
                            moveOutHql.append("FROM MovementActivityEntity WHERE movementId = " +
                                    movement.getMovementOut() + " ");
                            if (dateOut != null) {
                                moveOutHql.append("AND movementDate > :dateOut ");
                            }
                        }

                        if (movement.getMovementInStatus().contentEquals(MovementStatus.COMPLETED.name())) {
                            moveInHql.append("FROM MovementActivityEntity WHERE movementId = " +
                                    movement.getMovementIn() + " ");
                            if (dateIn != null) {
                                moveInHql.append("AND movementDate >= :dateIn ");
                            }
                        }
                        if (moveOutHql.length() > 0) {
                            Query movementQuery = session.createQuery(moveOutHql.toString());
                            if (dateOut != null) {
                                movementQuery.setTimestamp("dateOut", dateOut);
                            }
                            moveOut = (MovementActivityEntity) movementQuery.uniqueResult();
                            if (moveOut != null) {
                                if (moveOut.getMovementDate() != null) {
                                    movement.setMoveDate(moveOut.getMovementDate());
                                }
                            }
                        }
                        if (moveInHql.length() > 0) {
                            Query movementQuery = session.createQuery(moveInHql.toString());
                            if (dateIn != null) {
                                movementQuery.setTimestamp("dateIn", dateIn);
                            }
                            moveIn = (MovementActivityEntity) movementQuery.uniqueResult();
                            if (moveIn != null) {
                                if (moveIn.getMovementDate() != null) {
                                    movement.setMoveDate(moveIn.getMovementDate());
                                }
                            }
                        }
                    }
                }
            }
        }
        return ret;
    }

    private Boolean isMovePlanned(Date fromDate, Date toDate, Date plannedDate) {
        if (plannedDate != null) {
            plannedDate = dateTruncate(plannedDate);
            if (fromDate != null && toDate != null && plannedDate.compareTo(fromDate) >= 0 && plannedDate.compareTo(toDate) <= 0) {
                return Boolean.TRUE;
            } else if (fromDate != null && toDate == null && plannedDate.compareTo(fromDate) >= 0) {
                return Boolean.TRUE;
            } else if (fromDate == null && toDate != null && plannedDate.compareTo(toDate) <= 0) {
                return Boolean.TRUE;

            } else if (fromDate == null && toDate == null) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    private Date dateTruncate(Date fecha) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(fecha);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    // Validate Movement Activities are associated
    private Boolean isMovementAssociated(UserContext uc, MovementActivityType movement1, MovementActivityType movement2) {

        ActivityType activity1 = getActivity(uc, movement1);
        ActivityType activity2 = getActivity(uc, movement2);

        if (isActivitiesAssociated(activity1, activity2)) {
            return true;
        }
        return false;
    }

    // Get activity for Movement Activity
    private ActivityType getActivity(UserContext uc, MovementActivityType movement) {
        if (movement == null || movement.getActivityId() == null) {
            return null;
        }
        ActivityType activity = ActivityServiceAdapter.getActivity(uc, movement.getActivityId());
        return activity;
    }

    // Validate Associated Activities
    private Boolean isActivitiesAssociated(ActivityType activity1, ActivityType activity2) {

        if ((activity1 == null || activity2 == null) || (activity1.getActivityAntecedentId() == null && activity2.getActivityAntecedentId() == null)) {
            return false;
        }
        if (activity1.getActivityIdentification().equals(activity2.getActivityAntecedentId())) {
            return true;
        } else if (activity2.getActivityIdentification().equals(activity1.getActivityAntecedentId())) {
            return true;
        }

        return false;
    }

    @Override
    public TwoMovementsType checkSchedulingConflict(UserContext uc, TwoMovementsType movement) {
        return ConflictsHandler.checkSchedulingConflict(ksession, uc, movement);
    }

    @Override
    public ConflictType checkCapacityConflict(UserContext uc, MovementActivityType movement) {
        return ConflictsHandler.checkCapacityConflict(uc, (InternalMovementActivityType) movement);
    }

    @Override
    public List<ScheduleConflict> checkSchedulingConflict(UserContext uc, Set<Long> supervisionIds, List<ScheduleConflict.EventType> checkTypes, Date startDate,
                                                          Date endDate, EventType targetType, String language) {
        //Do validation
        if (supervisionIds.size() < 1 || startDate == null) {
            String errorMsg = "Invalid Exception " + supervisionIds.size() + startDate;
            log.error(errorMsg);
            throw new InvalidInputException(errorMsg);

        }

        long start = System.nanoTime();

        Long supervisionId = supervisionIds.toArray(new Long[supervisionIds.size()])[0];
        List<ScheduleConflict> result = new ArrayList<ScheduleConflict>();

        getConflictSchedule(uc, checkTypes, startDate, endDate, targetType, language, supervisionId, result);

        long estimatedTime = (System.nanoTime() - start) / 1000000; // milliseconds
        log.debug("checkScheduleConflict total: elapsed milliseconds: " + estimatedTime);
        return result;
    }

    private void getConflictSchedule(UserContext uc, List<ScheduleConflict.EventType> checkTypes, Date startDate, Date endDate, EventType targetType, String language,
                                     Long supervisionId, List<ScheduleConflict> result) {

        if (supervisionId == null || startDate == null) {
            String errorMsg = "Invalid Exception " + "supervisionId: " + supervisionId + "startDate: " + startDate;
            log.error(errorMsg);
            throw new InvalidInputException(errorMsg);

        }

        List<ConfigMovementDurationType> configMovementDurationTypes = getConfigMovementDurations(uc, null);

        //check again TAP, CRT
        List<String> types = new ArrayList<String>();
        if (checkTypes.contains(ScheduleConflict.EventType.CRT_EXTERNAL_MOVEMENT)) {
            types.add("CRT");
        }
        if (checkTypes.contains(ScheduleConflict.EventType.TAP_EXTERNAL_MOVEMENT)) {
            types.add("TAP");
        }
        if (types.size() > 0) {
            List<ScheduleConflict> conflicts = this.getExternalMovementBySupervisionId(uc, supervisionId, types);
            if (conflicts.size() > 0) {
                for (ScheduleConflict conflict : conflicts) {
                    String location = "";
                    if (conflict.getToFacilityId() != null) {
                        location = FacilityServiceAdapter.getFacility(uc, conflict.getToFacilityId()).getFacilityName();
                    }
                    if (conflict.getToLocationId() != null) {
                        location = MovementHelper.getLocationAddressById(uc, conflict.getToLocationId());
                    }
                    conflict.setLocation(location);
                    if (conflict.getExternalMoveType().equalsIgnoreCase(MovementServiceBean.MovementType.CRT.code())) {
                        conflict.setDescription("Court Appearance");
                    }
                    if (conflict.getExternalMoveType().equalsIgnoreCase(MovementServiceBean.MovementType.TAP.code())) {
                        conflict.setDescription("Temporary Absence");
                    }
                }
                result.addAll(conflicts);
            }
        }

        //check against Visitation
        if (checkTypes.contains(ScheduleConflict.EventType.VISIT)) {
            List<ScheduleConflict> visits = MovementHelper.searchVisits(uc, supervisionId, language, facilityInternalLocationService);
            if (visits != null && visits.size() > 0) {
                result.addAll(visits);
            }
        }

        //check again Appointments
        if (checkTypes.contains(ScheduleConflict.EventType.APPOINTMENT)) {
            List<ScheduleConflict> appointments = MovementHelper.search(uc, supervisionId, language);
            if (appointments.size() > 0) {
                result.addAll(appointments);
            }
        }

        if (endDate == null) {
            if (ScheduleConflict.EventType.INTERNAL_MOVEMENT.equals(targetType)) {
                endDate = getDefaultEndDateForScheduleConflict(uc, startDate, startDate, "internal", configMovementDurationTypes);
            } else if (ScheduleConflict.EventType.TRN_EXTERNAL_MOVEMENT.equals(targetType)) {
                endDate = getDefaultEndDateForScheduleConflict(uc, startDate, startDate, "Transfers", configMovementDurationTypes);
            } else if (ScheduleConflict.EventType.HEARING.equals(targetType)) {
                endDate = getDefaultEndDateForScheduleConflict(uc, startDate, startDate, "Hearings", configMovementDurationTypes);
            }
        }

        //InternalMovement
        if (checkTypes.contains(ScheduleConflict.EventType.INTERNAL_MOVEMENT)) {
            if (configMovementDurationTypes == null || configMovementDurationTypes.isEmpty()) {
                configMovementDurationTypes = getConfigMovementDurations(uc, null);
            }

            Date startDateModified = startDate;
            Date endDateForInterMov = null;
            if (endDate == null) {
                endDateForInterMov = startDate;
            } else {
                endDateForInterMov = endDate;
            }

            endDateForInterMov = getDefaultEndDateForScheduleConflict(uc, startDateModified, endDateForInterMov, "internal", configMovementDurationTypes);

            startDateModified = getModifiedStartDateForScheduleConflict(uc, startDateModified, endDateForInterMov, "internal", configMovementDurationTypes);

            MovementActivitySearchType searchType = new MovementActivitySearchType();
            searchType.setMovementStatus(MovementActivityType.MovementStatus.PENDING.code());
            searchType.setMovementCategory(MovementActivityType.MovementCategory.INTERNAL.value());
            searchType.setMovementDirection(MovementActivityType.MovementDirection.OUT.code());
            searchType.setMovementType(MovementActivityType.MovementType.APP.code());
            searchType.setSupervisionId(supervisionId);
            searchType.setPlannedStartDate(startDateModified);
            searchType.setPlannedEndDate(endDateForInterMov);
            /*List<ScheduleConflict> internalMoves = MovementHelper.searchInternalMovement(uc, searchType);
            if (internalMoves.size() > 0) {
                result.addAll(internalMoves);
            }*/
            Iterator<InternalMovementActivityType> iterator = this.search(uc, searchType, 0L, 200L, null).getInternalMovementActivity().iterator();
            Boolean bInstanceEvent = false;
            if (searchType.getPlannedEndDate() == null) {
                //to against an Zero duration event
                bInstanceEvent = true;
            }
            while (iterator.hasNext()) {
                InternalMovementActivityType move = iterator.next();
                ActivityEntity activityEntity = (ActivityEntity) session.get(ActivityEntity.class, move.getActivityId());
                Date plannedStartDate = activityEntity.getPlannedStartDate();
                if (bInstanceEvent && searchType.getPlannedStartDate().after(plannedStartDate)) {
                    iterator.remove();
                    continue;
                }
                ScheduleConflict conflict = new ScheduleConflict();
                conflict.setEventId(move.getMovementId());
                conflict.setSupervisionId(move.getSupervisionId());
                conflict.setType(ScheduleConflict.EventType.INTERNAL_MOVEMENT);
                conflict.setStartDate(plannedStartDate);
                Date plannedEndDate = getDefaultEndDateForScheduleConflict(uc, plannedStartDate, plannedStartDate, "internal", configMovementDurationTypes);
                if (plannedEndDate != null) {
                    conflict.setEndDate(plannedEndDate);
                }
                conflict.setToLocationId(move.getToFacilityInternalLocationId());
                conflict.setLocation(MovementHelper.getFacilityInternalLocation(uc, move.getToFacilityInternalLocationId(), facilityInternalLocationService).getDescription());
                conflict.setDescription("Internal Movement");
                result.add(conflict);
            }
        }

        //Transfer

        if (checkTypes.contains(ScheduleConflict.EventType.TRN_EXTERNAL_MOVEMENT)) {
            MovementActivitySearchType searchType = new MovementActivitySearchType();
            searchType.setMovementStatus(MovementActivityType.MovementStatus.PENDING.code());
            searchType.setMovementCategory(MovementActivityType.MovementCategory.EXTERNAL.value());
            searchType.setMovementType(MovementActivityType.MovementType.TRNIJ.code());
            searchType.setMovementDirection(MovementActivityType.MovementDirection.OUT.code());
            searchType.setSupervisionId(supervisionId);
            searchType.setMovementStatus(MovementActivityType.MovementStatus.PENDING.code());
            /*List<ScheduleConflict> internalMoves = MovementHelper.searchTransferMovement(uc, searchType);
            if (internalMoves.size() > 0) {
                result.addAll(internalMoves);
            }*/
            Iterator<ExternalMovementActivityType> iterator = this.search(uc, searchType, 0L, 200L, null).getExternalMovementActivity().iterator();
            while (iterator.hasNext()) {
                ExternalMovementActivityType move = iterator.next();
                ScheduleConflict conflict = new ScheduleConflict();
                conflict.setEventId(move.getMovementId());
                conflict.setSupervisionId(move.getSupervisionId());
                conflict.setType(ScheduleConflict.EventType.TRN_EXTERNAL_MOVEMENT);
                if (move.getMovementDate() == null) {
                    ActivityEntity activityEntity = (ActivityEntity) session.get(ActivityEntity.class, move.getActivityId());
                    Date plannedStartDate = activityEntity.getPlannedStartDate();
                    if (plannedStartDate != null && searchType.getPlannedStartDate() != null &&
                            plannedStartDate.after(searchType.getPlannedStartDate())) {
                        iterator.remove();
                        continue;
                    }
                    conflict.setStartDate(plannedStartDate);
                } else {
                    conflict.setStartDate(move.getMovementDate());
                }

                if (move.getReportingDate() != null) {
                    conflict.setEndDate(move.getReportingDate());
                } else {
                    Date plannedEndDate = getDefaultEndDateForScheduleConflict(uc, conflict.getStartDate(), conflict.getStartDate(), "Transfers",
                            configMovementDurationTypes);
                    if (plannedEndDate != null) {
                        conflict.setEndDate(plannedEndDate);
                    }
                }
                conflict.setToLocationId(move.getToFacilityId());
                conflict.setLocation(FacilityServiceAdapter.getFacility(uc, move.getToFacilityId()).getFacilityName());
                conflict.setDescription("Transfer");
                result.add(conflict);
            }
        }

        //Release
        if (checkTypes.contains(ScheduleConflict.EventType.REL_EXTERNAL_MOVEMENT)) {
            List<ScheduleConflict> releases = MovementHelper.searchScheduledReleases(uc, supervisionId);
            if (releases != null && releases.size() > 0) {
                result.addAll(releases);

            }
        }

        //Hearing
        if (checkTypes.contains(ScheduleConflict.EventType.HEARING)) {
            Date internalMovementsDuration = null;
            for (ConfigMovementDurationType configMovementDurationTypeTemp : configMovementDurationTypes) {
                if ("Hearings".equalsIgnoreCase(configMovementDurationTypeTemp.getMovementType())) {
                    SimpleDateFormat dateformat = new SimpleDateFormat("HH:mm");
                    if (configMovementDurationTypeTemp != null) {
                        try {
                            internalMovementsDuration = dateformat.parse(configMovementDurationTypeTemp.getDuration());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                }
            }

            List<ScheduleConflict> hearing = MovementHelper.searchHearings(uc, supervisionId, internalMovementsDuration, facilityInternalLocationService);
            if (hearing != null && hearing.size() > 0) {
                result.addAll(hearing);
            }
        }

        //Programming
        if (checkTypes.contains(ScheduleConflict.EventType.PROGRAM)) {
            /*List<ScheduleConflict> programConflicts =
                    MovementHelper.getConflictsOfferingSessions(uc, supervisionId, startDate, endDate);*/
            List<ScheduleConflict> programConflicts = ConflictServiceAdapter.checkScheduleConflict(uc, supervisionId, checkTypes, new LocalDateTime(startDate),
                    new LocalDateTime(endDate));
            if (programConflicts != null && programConflicts.size() > 0) {
                result.addAll(programConflicts);
            }
        }

        Iterator<ScheduleConflict> iterator = result.iterator();
        while (iterator.hasNext()) {
            ScheduleConflict schedule = iterator.next();
            if (schedule.getType() != null && ScheduleConflict.EventType.PROGRAM.name().equalsIgnoreCase(schedule.getType().name())) {
                continue;
            }
            if (schedule.getStartDate() == null) {
                iterator.remove();
                continue;
            }
            if (endDate == null) {
                if (schedule.getEndDate() == null) {
                    if (!startDate.equals(schedule.getStartDate())) {
                        iterator.remove();
                    }
                } else {
                    if (((schedule.getStartDate() != null) && (startDate.before(schedule.getStartDate()))) || ((schedule.getEndDate() != null) && (startDate.after(
                            schedule.getEndDate())))) {
                        //no conflict
                        iterator.remove();
                    }
                }

            } else {
                if (schedule.getEndDate() == null) {
                    if (endDate.before(schedule.getStartDate()) || startDate.after(schedule.getStartDate())) {
                        //no conflict
                        iterator.remove();
                    }
                } else {
                    if (endDate.before(schedule.getStartDate()) || startDate.after(schedule.getEndDate())) {
                        //no conflict
                        iterator.remove();
                    }
                }
            }
        }
        if (!result.isEmpty()) {
            MovementHelper.populateSupervisionIdinConflict(uc, result);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MovementDetailsType> getMostRecentMovementData(UserContext uc, Set<Long> supervisionIds) {

        //Validate parameter.
        if (!MovementHelper.isSetValid(supervisionIds, Boolean.TRUE)) {
            String message = "supervision ids can not be null and empty.";
            LogHelper.error(log, "getMostRecentMovementData", message);
            throw new InvalidInputException(message);
        }

        //Get a set of LastCompletedSupervisionMovementType by supvision Ids.
        Set<LastCompletedSupervisionMovementType> mostRecentMovements = MovementServiceAdapter.getLastCompletedSupervisionMovementBySupervisions(uc, supervisionIds);

        //Composite MovementDetailsReturnType
        return new ArrayList<MovementDetailsType>(MovementHelper.generateMovementDetailsTypeSet(mostRecentMovements));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MovementActivityDetailsType> getMostRecentActivityMovementData(UserContext uc, Set<Long> supervisionIds) {

        //Validate parameter.
        if (!MovementHelper.isSetValid(supervisionIds, Boolean.TRUE)) {
            String message = "supervision ids can not be null and empty.";
            LogHelper.error(log, "getMostRecentActivityMovementData", message);
            throw new InvalidInputException(message);
        }
        //Get a set of LastCompletedSupervisionMovementType by supvision Ids.
        Set<LastCompletedSupervisionMovementType> lastcompletedSupervisionMovements = MovementServiceAdapter.getLastCompletedSupervisionMovementBySupervisions(uc,
                supervisionIds);

        //generate subActivityIds for searching.
        Set<Long> subActivityIds = MovementHelper.getAssociatedActivityIds(lastcompletedSupervisionMovements);

        //Map each supervision id and it's activities for later usage.
        HashMap<Long, Set<ActivityType>> supervisionIdActivitySetMap = MovementHelper.mapSupervisionIdActivitySet(
                MovementHelper.searchActivitiesByMovementCategory(uc, supervisionIds, null, null, subActivityIds));

        //get MovementActivities which is most recent completed.
        Set<MovementActivityType> lastcompletedMovements = MovementHelper.getMostRecentMovements(lastcompletedSupervisionMovements);

        //composite MovementActivityDetailsReturnType
        return new ArrayList<>(MovementHelper.generateMovementActivityDetailsTypeSet(supervisionIdActivitySetMap, lastcompletedMovements, true));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OffenderLocationStatusType getOffenderLocationStatus(UserContext uc, Long supervisionId) {

        //Validate parameter.
        if (supervisionId == null) {
            String message = "supervision id can not be null.";
            LogHelper.error(log, "getOffenderLocationStatus", message);
            throw new InvalidInputException(message);
        }

        Set<Long> supIds = new HashSet<Long>();
        supIds.add(supervisionId);

        List<OffenderLocationStatusType> rtn = getOffenderLocationStatuses(uc, supIds);

        return rtn.get(0);
    }

    /**
     * {@inheritDoc}l
     */
    @Override
    public List<OffenderLocationStatusType> getOffenderLocationStatuses(UserContext uc, Set<Long> supervisionIds) {

        //Validate parameters.
        if (!MovementHelper.isSetValid(supervisionIds, Boolean.TRUE)) {
            String message = "supervision ids can not be null and empty.";
            LogHelper.error(log, "getOffenderLocationStatuses", message);
            throw new InvalidInputException(message);
        }
        //Search Supervisions by the SupervisionIds for later use.
        Set<SupervisionType> supervisions = MovementHelper.searchSupervisions(uc, supervisionIds, SEARCH_MAX_LIMIT);

        //Search HousingAssignments by a set of supervision identifications .
        Set<OffenderHousingAssignmentType> housingAssignments = MovementHelper.retrieveCurrentAssignmentsByOffenders(uc, supervisionIds);

        //Get a set of LastCompletedSupervisionMovementType by supvision Ids.
        Set<LastCompletedSupervisionMovementType> mostRecentMovements = MovementServiceAdapter.getLastCompletedSupervisionMovementBySupervisions(uc, supervisionIds);

        //Composite OffenderLocationStatusesReturnType
        return new ArrayList<OffenderLocationStatusType>(MovementHelper.generateOffenderLocationStatusTypeSet(supervisions, housingAssignments, mostRecentMovements));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MovementActivityDetailsType> searchMovementDetailsByMovementDate(UserContext uc, Long supervisionIdentification, Date fromActualDate, Date toActualDate,
                                                                                 String movementCategory, Set<String> movementStatuses, String movementType) {

        //Validate parameters.
        if (supervisionIdentification == null) {
            String message = "supervision ids can not be null.";
            LogHelper.error(log, "searchMovementDetailsByMovementDate", message);
            throw new InvalidInputException(message);
        }

        Set<Long> supervisionIdentifications = new HashSet<Long>();
        supervisionIdentifications.add(supervisionIdentification);

        //Map each supervision id and it's activities for later usage.
        HashMap<Long, Set<ActivityType>> supervisionIdActivitySetMap = MovementHelper.mapSupervisionIdActivitySet(
                MovementHelper.searchActivitiesByMovementCategory(uc, supervisionIdentifications, null, null, null));

        //Search MovementActivities by passed-in search criteria.
        Set<MovementActivityType> movementActivities = MovementHelper.getMovementActivities(uc,
                MovementHelper.getMovementActivityIdsByActivityIds(uc, supervisionIdActivitySetMap.keySet()), fromActualDate, toActualDate, movementCategory,
                movementStatuses, movementType, SEARCH_MAX_LIMIT);

        //Composite MovementActivityDetailsReturnType
        return new ArrayList<MovementActivityDetailsType>(MovementHelper.generateMovementActivityDetailsTypeSet(supervisionIdActivitySetMap, movementActivities, false));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MovementActivityDetailsType> searchMovementDetailsByScheduledDate(UserContext uc, Long supervisionIdentification, Date fromScheduledDate,
                                                                                  Date toScheduledDate, String movementCategory, Set<String> movementStatuses, String movementType) {

        //Validate parameters.
        if (supervisionIdentification == null) {
            String message = "supervision ids can not be null.";
            LogHelper.error(log, "searchMovementDetailsByMovementDate", message);
            throw new InvalidInputException(message);
        }

        Set<Long> supervisionIdentifications = new HashSet<Long>();
        supervisionIdentifications.add(supervisionIdentification);

        //Map each supervision id and it's activities for later usage.
        HashMap<Long, Set<ActivityType>> supervisionIdActivitySetMap = MovementHelper.mapSupervisionIdActivitySet(
                MovementHelper.searchActivitiesByMovementCategory(uc, supervisionIdentifications, fromScheduledDate, toScheduledDate, null));

        //Search MovementActivities by passed-in search criteria.
        Set<MovementActivityType> movementActivities = MovementHelper.getMovementActivities(uc,
                MovementHelper.getMovementActivityIdsByActivityIds(uc, supervisionIdActivitySetMap.keySet()), null, null, movementCategory, movementStatuses,
                movementType, SEARCH_MAX_LIMIT);

        //Composite MovementActivityDetailsReturnType.
        return new ArrayList<MovementActivityDetailsType>(MovementHelper.generateMovementActivityDetailsTypeSet(supervisionIdActivitySetMap, movementActivities, false));

    }

    @Override
    public void updateScheduleMovementStatus(UserContext uc, List<Long> movementids, String movementStatus) {
        //MAServiceAdapter.updateScheduleMovementStatus(uc, session, movementids, movementStatus);

        MovementActivityEntity mae;
        ActivityEntity ae;
        if (!movementids.isEmpty()) {
            for (Long id : movementids) {
                mae = BeanHelper.getEntity(session, MovementActivityEntity.class, id);

                if (null != mae) {
                    ae = BeanHelper.getEntity(session, ActivityEntity.class, mae.getActivityId());

                    if (MovementStatus.ONHOLD.isEquals(movementStatus) && mae.getMovementStatus().equals(MovementStatus.PENDING.code())) {
                        mae.setMovementStatus(MovementStatus.ONHOLD.code());
                        //ae.setFlag(0L);
                    }

                    if (MovementStatus.RELEASE.isEquals(movementStatus) && mae.getMovementStatus().equals(MovementStatus.ONHOLD.code())) {
                        mae.setMovementStatus(MovementStatus.PENDING.code());
                        //ae.setFlag(1L);
                    }

                    if (MovementStatus.CANCELLED.isEquals(movementStatus) && mae.getMovementStatus().equals(MovementStatus.ONHOLD.code())) {
                        mae.setMovementStatus(MovementStatus.CANCELLED.code());
                        //ae.setFlag(0L);
                    }

                    if (MovementStatus.CANCELLED.isEquals(movementStatus) && mae.getMovementStatus().equals(MovementStatus.PENDING.code())) {
                        mae.setMovementStatus(MovementStatus.CANCELLED.code());
                        //ae.setFlag(0L);

                    }

                    session.merge(mae);
                    session.merge(ae);
                    deleteCancelAssociatedMovement(mae, MovementStatus.CANCELLED, session);
                }

            }
        }
    }

    private void deleteCancelAssociatedMovement(MovementActivityEntity entity, MovementStatus actionStatus, Session session) {

        String hql = "FROM MovementActivityEntity ma where ma.groupid = '" + entity.getGroupId() + "' and ma.movementId != " + entity.getMovementId();
        Query query2 = session.createQuery(hql);
        @SuppressWarnings("rawtypes") List results = query2.list();

        if (null != results && !results.isEmpty()) {
            entity = (MovementActivityEntity) results.get(0);

            switch (actionStatus) {
                case CANCELLED:
                    entity.setMovementStatus(MovementStatus.CANCELLED.code());
                    break;

                case DELETED:
                    entity.setFlag(0L);
                    break;

            }
            session.merge(entity);

        }

    }

    @Override
    public Map<Long, List<ScheduleConflict>> checkSchedulingConflicts(UserContext uc, Set<InmateMoveSearchType> inmateMoveSearchTypes, List<EventType> checkTypes,
                                                                      EventType targetType, String language) {
        Map<Long, List<ScheduleConflict>> scheduleConflicts = new HashMap<Long, List<ScheduleConflict>>();
        // Do validation
        if (inmateMoveSearchTypes.size() < 1) {
            String errorMsg = "Invalid Exception " + inmateMoveSearchTypes.size();
            log.error(errorMsg);
            throw new InvalidInputException(errorMsg);

        }

        long start = System.nanoTime();

        // Long supervisionId = supervisionIds.toArray(new
        // Long[supervisionIds.size()])[0];
        // List<ScheduleConflict> result = new ArrayList<ScheduleConflict>();

        for (InmateMoveSearchType inmateMoveSearchType : inmateMoveSearchTypes) {
            List<ScheduleConflict> result = new ArrayList<ScheduleConflict>();
            getConflictSchedule(uc, checkTypes, inmateMoveSearchType.getStartDate(), inmateMoveSearchType.getEndDate(), targetType, language,
                    inmateMoveSearchType.getSupervisionId(), result);
            if (!result.isEmpty()) {
                // for (ScheduleConflict scheduleConflict : result) {
                //
                // }
                scheduleConflicts.put(inmateMoveSearchType.getSupervisionId(), result);

            }
        }

        long estimatedTime = (System.nanoTime() - start) / 1000000; // milliseconds
        log.debug("checkScheduleConflict total: elapsed milliseconds: " + estimatedTime);

        return scheduleConflicts;
    }

    @Override
    public Map<ScheduleConflict, List<ScheduleConflict>> checkScheduledConflicts(UserContext uc, Set<InmateMoveSearchType> inmateMoveSearchTypes,
                                                                                 List<EventType> checkTypes, EventType targetType, String language) {
        Map<ScheduleConflict, List<ScheduleConflict>> scheduleConflicts = new HashMap<ScheduleConflict, List<ScheduleConflict>>();

        if (inmateMoveSearchTypes.size() < 1) {
            String errorMsg = "Invalid Exception " + inmateMoveSearchTypes.size();
            log.error(errorMsg);
            throw new InvalidInputException(errorMsg);

        }

        long start = System.nanoTime();
        for (InmateMoveSearchType inmateMoveSearchType : inmateMoveSearchTypes) {
            List<ScheduleConflict> result = new ArrayList<ScheduleConflict>();
            getConflictSchedule(uc, checkTypes, inmateMoveSearchType.getStartDate(), inmateMoveSearchType.getEndDate(), targetType, language,
                    inmateMoveSearchType.getSupervisionId(), result);
            if (!result.isEmpty()) {
                for (Iterator iterator = result.iterator(); iterator.hasNext(); ) {
                    ScheduleConflict scheduleConflict = (ScheduleConflict) iterator.next();
                    if (scheduleConflict.getEventId().equals(inmateMoveSearchType.getEventId())) {
                        iterator.remove();
                    }

                }
                ScheduleConflict keyScheduleConflict = new ScheduleConflict(inmateMoveSearchType.getEventId(), inmateMoveSearchType.getSupervisionId(), null, null, null,
                        null, null);
                scheduleConflicts.put(keyScheduleConflict, result);

            }
        }

        long estimatedTime = (System.nanoTime() - start) / 1000000; // milliseconds
        log.debug("checkScheduleConflict total: elapsed milliseconds: " + estimatedTime);

        return scheduleConflicts;
    }

    @Override
    public List<ConfigMovementDurationType> getConfigMovementDurations(UserContext uc, ConfigMovementDurationType configMovementDurationType) {

        if (log.isDebugEnabled()) {
            log.debug("getConfigMovementDurations: begin");
        }

        Criteria c = session.createCriteria(ConfigMovementDurationEntity.class);
        if (configMovementDurationType != null && configMovementDurationType.getMovementType() != null) {
            c.add(Restrictions.eq("movementType", configMovementDurationType.getMovementType()));
        }
        @SuppressWarnings("unchecked")
        List<ConfigMovementDurationEntity> configs = c.list();
        List<ConfigMovementDurationType> configMovementDurationTypes = new ArrayList<ConfigMovementDurationType>();

        for (ConfigMovementDurationEntity configMovementDurationEntity : configs) {
            ConfigMovementDurationType durationType = new ConfigMovementDurationType();
            durationType.setDuration(configMovementDurationEntity.getDuration());
            durationType.setMovementType(configMovementDurationEntity.getMovementType());
            configMovementDurationTypes.add(durationType);
        }

        if (log.isDebugEnabled()) {
            log.debug("getConfigMovementDurations: end");
        }

        return configMovementDurationTypes;
    }

    @Override
    public ConfigMovementDurationType setConfigMovementDuration(UserContext uc, ConfigMovementDurationType configuration) {
        if (log.isDebugEnabled()) {
            log.debug("setConfigMovementDuration: begin");
        }
        ValidationHelper.validate(configuration);
        String duration = configuration.getDuration();

        if (duration == null || !Pattern.compile("^[0-9]?[0-9]:[0-9][0-9]$").matcher(duration).find() || Integer.valueOf(duration.split(":")[0]) < 0
                || Integer.valueOf(duration.split(":")[0]) >= 24 || Integer.valueOf(duration.split(":")[1]) < 0 || Integer.valueOf(duration.split(":")[1]) >= 60) {

            String message = "Invalid argument: duration: " + duration;
            LogHelper.error(log, "setConfigMovementDuration", message);
            throw new InvalidInputException(message);
        }

        ConfigMovementDurationEntity durationEntity = (ConfigMovementDurationEntity) session.get(ConfigMovementDurationEntity.class, configuration.getMovementType());
        if (durationEntity != null) {
            durationEntity.setDuration(configuration.getDuration());
            StampEntity modifyStamp = BeanHelper.getModifyStamp(uc, context, durationEntity.getStamp());
            durationEntity.setStamp(modifyStamp);
            session.update(durationEntity);
        } else {
            ConfigMovementDurationEntity config = new ConfigMovementDurationEntity();
            config.setDuration(configuration.getDuration());
            config.setMovementType(configuration.getMovementType());
            StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
            config.setStamp(createStamp);
            session.save(config);
        }

        if (log.isDebugEnabled()) {
            log.debug("setConfigMovementDuration: end");
        }
        return configuration;
    }

    @Override
    public void setConfigMovementDuration(UserContext uc, List<ConfigMovementDurationType> movementDurations) {
        if (log.isDebugEnabled()) {
            log.debug("setConfigMovementDuration: begin");
        }

        for (ConfigMovementDurationType configuration : movementDurations) {
            ValidationHelper.validate(configuration);
            String duration = configuration.getDuration();

            if (duration == null || !Pattern.compile("^[0-9]?[0-9]:[0-9][0-9]$").matcher(duration).find() || Integer.valueOf(duration.split(":")[0]) < 0
                    || Integer.valueOf(duration.split(":")[0]) >= 24 || Integer.valueOf(duration.split(":")[1]) < 0 || Integer.valueOf(duration.split(":")[1]) >= 60) {

                String message = "Invalid argument: duration: " + duration;
                LogHelper.error(log, "setConfigMovementDuration", message);
                throw new InvalidInputException(message);
            }
        }

        for (ConfigMovementDurationType configuration : movementDurations) {

            ConfigMovementDurationEntity durationEntity = (ConfigMovementDurationEntity) session.get(ConfigMovementDurationEntity.class, configuration.getMovementType());
            if (durationEntity != null) {
                durationEntity.setDuration(configuration.getDuration());
                StampEntity modifyStamp = BeanHelper.getModifyStamp(uc, context, durationEntity.getStamp());
                durationEntity.setStamp(modifyStamp);
                session.update(durationEntity);
            } else {
                ConfigMovementDurationEntity config = new ConfigMovementDurationEntity();
                config.setDuration(configuration.getDuration());
                config.setMovementType(configuration.getMovementType());
                StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
                config.setStamp(createStamp);
                session.save(config);
            }

        }
        if (log.isDebugEnabled()) {
            log.debug("setConfigMovementDuration: end");
        }

    }

    @Override
    public void updateScheduleMovementStatus(UserContext uc, List<Long> movementids, String movementStatus, String commentText,String reasonCode) {
        //MAServiceAdapter.updateScheduleMovementStatus(uc, session, movementids, movementStatus, comment);
        MovementActivityEntity mae;
        MovementActivityCommentEntity comment;
        if (!movementids.isEmpty()) {
            for (Long id : movementids) {
                mae = BeanHelper.getEntity(session, MovementActivityEntity.class, id);

                if (null != mae) {

                    if (MovementStatus.ONHOLD.isEquals(movementStatus) && mae.getMovementStatus().equals(MovementStatus.PENDING.code())) {
                        mae.setMovementStatus(MovementStatus.ONHOLD.code());

                    }

                    if (MovementStatus.RELEASE.isEquals(movementStatus) && mae.getMovementStatus().equals(MovementStatus.ONHOLD.code())) {
                        mae.setMovementStatus(MovementStatus.PENDING.code());

                    }

                    if (MovementStatus.CANCELLED.isEquals(movementStatus) && mae.getMovementStatus().equals(MovementStatus.ONHOLD.code())) {
                        mae.setMovementStatus(MovementStatus.CANCELLED.code());
                        ExternalMovementActivityEntity emae = (ExternalMovementActivityEntity)mae;
                        emae.setCancelreason(reasonCode);

                    }

                    if (MovementStatus.CANCELLED.isEquals(movementStatus) && mae.getMovementStatus().equals(MovementStatus.PENDING.code())) {
                        mae.setMovementStatus(MovementStatus.CANCELLED.code());
                        ExternalMovementActivityEntity emae = (ExternalMovementActivityEntity)mae;
                        emae.setCancelreason(reasonCode);
                    }

                    if (null != commentText) {

                        Iterator<MovementActivityCommentEntity> icomment = mae.getCommentText().iterator();
                        while (icomment.hasNext()) {
                            MovementActivityCommentEntity com = (MovementActivityCommentEntity) icomment.next();
                            session.delete(com);
                        }

                        comment = new MovementActivityCommentEntity();
                        comment.setCommentIdentification(null);
                        comment.setFromIdentifier(mae.getMovementId());
                        comment.setMovementActivity(mae);
                        comment.setCommentText(commentText);
                        comment.setCommentDate(new Date());

                        mae.getCommentText().add(comment);

                        mae.getCommentText().add(comment);

                        session.save(comment);

                    }
                    session.merge(mae);
                    deleteCancelAssociatedMovement(mae, MovementStatus.CANCELLED, session);

                }

            }
        }
    }

    @Override
    public boolean isExternalMovementByOrganization(UserContext uc, Long organizationId) {
        if (BeanHelper.isEmpty(organizationId)) {
            return false;
        }
        Criteria c = session.createCriteria(ExternalMovementActivityEntity.class);
        c.add(Restrictions.disjunction().add(Restrictions.eq("fromOrganizationId", organizationId)).add(Restrictions.eq("toOrganizationId", organizationId)).add(
                Restrictions.eq("arrestOrganizationId", organizationId)).add(Restrictions.eq("escortOrganizationId", organizationId)));

        return !c.list().isEmpty();

    }

    @Override
    public boolean isInmateEscaped(UserContext uc, Long supervisionId){
        // todo
        return false;
    }

    private boolean validateEscape(UserContext uc, EscapeRecapture escapeRecapture) {
        if (escapeRecapture.getEscapeDate() == null) {
            throw new ValidationException(ErrorCodes.SUP_RELEASEOFFENDER_P01_ERR_ESCAPE_DATETIME);
        }

        if (escapeRecapture.getLastSeenDate() == null && escapeRecapture.getLastSeenTime() != null) {
            throw new ValidationException(ErrorCodes.SUP_RELEASEOFFENDER_P01_ERR_LASTSEEN_DATE_EMPTY);
        }

        if (escapeRecapture.getLastSeenDate() != null && escapeRecapture.getLastSeenTime() == null) {
            throw new ValidationException(ErrorCodes.SUP_RELEASEOFFENDER_P01_ERR_LASTSEEN_TIME_EMPTY);
        }

        if (!DateUtil.validatePastOnly(escapeRecapture.getEscapeDateTime())) {
            throw new ValidationException(ErrorCodes.SUP_RELEASEOFFENDER_P01_ERR_ESCAPE_DATE_SHOULD_NOT_BE_FUTURE);
        }

        if (null != escapeRecapture.getLastSeenDate()) {
            if (!DateUtil.validatePastOnly(DateUtil.combineDateTime(escapeRecapture.getLastSeenDate(), escapeRecapture.getLastSeenTime()))) {
                throw new ValidationException(ErrorCodes.SUP_RELEASEOFFENDER_P01_ERR_LASTSEEN_DATE_PAST);
            }
        }

        Date lastSeenDateTime = DateUtil.combineDateTime(escapeRecapture.getLastSeenDate(), escapeRecapture.getLastSeenTime());
        if (null != escapeRecapture.getEscapeDateTime() && null != lastSeenDateTime
                && escapeRecapture.getEscapeDateTime().before(lastSeenDateTime)) {
            throw new ValidationException(ErrorCodes.SUP_RELEASEOFFENDER_P01_ERR_ESCAPEDATE);
        }

        Set<Long> supervisions = new HashSet<>();
        Long supervisionId = escapeRecapture.getSupervisionId();
        supervisions.add(supervisionId);
        Set<LastCompletedSupervisionMovementType> lastCompleteMovement = this.getLastCompletedSupervisionMovementBySupervisions(uc, supervisions);
        if (lastCompleteMovement != null) {
            if (escapeRecapture.getRecaptureDate() == null) {
                for (LastCompletedSupervisionMovementType lcm : lastCompleteMovement) {
                    MovementActivityType movement = lcm.getLastCompletedMovementActivity();
                    if (!movement.getMovementType().equalsIgnoreCase(MovementType.TRNIJ.code()) &&
                            movement.getMovementType().equalsIgnoreCase(MovementType.TRNOJ.code())) {
                        FacilityOfInterestType openFacilityOfInterest = supervisionService.getActiveSupervisionFOI(uc, supervisionId);
                        if (openFacilityOfInterest == null) {
                            throw new ValidationException(ErrorCodes.SUP_RELEASEOFFENDER_P01_ERR_FACILITY_OF_INTEREST_WAS_CLOSED_ALREADY);
                        }
                    }

                    if (!movement.getMovementDate().before(escapeRecapture.getEscapeDateTime())) {
                        throw new ValidationException(ErrorCodes.SUP_RELEASEOFFENDER_P01_ERR_ESCAPEBFORELASTMOVEMENT);
                    }
                }
            }
        }

        // Escape date cannot be before the latest admission date
        Date admissionDate = getAdmissionDate(uc, escapeRecapture.getSupervisionId());
        if (admissionDate != null && !admissionDate.before(escapeRecapture.getEscapeDateTime())) {
            throw new ValidationException(ErrorCodes.SUP_RELEASEOFFENDER_ERR_ESCAPE_DATE_BEFORE_ADMISSION_DATE);
        }

        return true;
    }

    private boolean validateRecapture(UserContext uc, EscapeRecapture escapeRecapture) {
        if (escapeRecapture.getArrestingOrganization() == null) {
            throw new ValidationException(ErrorCodes.SUP_RELEASEOFFENDER_P01_ERR_ARREST_ORGANIZATION);
        }
        if (escapeRecapture.getRecaptureDate() == null || escapeRecapture.getRecaptureTime() == null) {
            throw new ValidationException(ErrorCodes.SUP_RELEASEOFFENDER_P01_ERR_RECAPTURE_DATETIME);
        }
        Date escapeDateTime = escapeRecapture.getEscapeDateTime();
        Date recaptureDateTime = DateUtil.combineDateTime(escapeRecapture.getRecaptureDate(), escapeRecapture.getRecaptureTime());
        if (!escapeDateTime.before(recaptureDateTime)) {
            throw new ValidationException(ErrorCodes.SUP_RELEASEOFFENDER_P01_ERR_RECAPTURE_DATETIME_BEFORE_ESCAPE);
        }
        if (escapeRecapture.getReadmissionDate() != null) {
            Date readmissionDateTime = escapeRecapture.getReadmissionDateTime();
            if (escapeDateTime.after(readmissionDateTime)) {
                throw new ValidationException(ErrorCodes.SUP_RELEASEOFFENDER_P01_ERR_ESCAPE_DATETIME_AFTER_READMISSION);
            }
            /* WOR-23239: Nathan Hedges's comment:
               I would suggest removing the constraint on recapture date/time - it is a thoughtful edit check though, not adding a terrific amount of value.
            if (recaptureDateTime.after(readmissionDateTime)) {
                throw new ValidationException(ErrorCodes.SUP_RELEASEOFFENDER_P01_ERR_RECAPTURE_DATETIME_AFTER_READMISSION);
            }*/
        }

        validateEscape(uc, escapeRecapture);
        return true;
    }

    private Date getAdmissionDate(UserContext uc, Long supervisionId) {
        return (Date)session.createQuery("select movementDate from MovementActivityEntity " +
                "where supervisionId = :supervisionId and movementCategory = :movementCategory " +
                "and movementType = :movementType and movementDirection = :movementDirection " +
                "and movementStatus = :movementStatus and movementDate is not null " +
                "order by supervisionId desc")
                .setLong("supervisionId", supervisionId)
                .setString("movementCategory", MovementCategory.EXTERNAL.code())
                .setString("movementType", MovementType.ADM.code())
                .setString("movementDirection", MovementDirection.IN.code())
                .setString("movementStatus", MovementStatus.COMPLETED.code())
                .setFetchSize(1)
                .setMaxResults(1)
                .uniqueResult();
    }

    public List<EscapeRecapture> convertMetaDescriptions(UserContext uc, List<EscapeRecapture> escapeRecaptureList, String language) {
        if (escapeRecaptureList != null) {
            for (EscapeRecapture escapeRecapture: escapeRecaptureList) {
                getEscapeMetaDescription(uc, language, escapeRecapture);
            }
        }
        return escapeRecaptureList;
    }

    private EscapeRecapture getEscapeMetaDescription(UserContext uc, String language, EscapeRecapture escapeRecapture) {
        setEscapeFacility(uc, escapeRecapture);
        setArrestOrganization(uc, escapeRecapture);
        setReadmission(uc, escapeRecapture);
        addMetaDescription(uc, escapeRecapture, language);
        return escapeRecapture;
    }

    private void setEscapeFacility(UserContext uc, EscapeRecapture escapeRecapture) {
        escapeRecapture.setEscapeFacilityDescription(facilityService.getFacilityNameById(uc, escapeRecapture.getEscapeFacility()));
    }

    private void setArrestOrganization(UserContext uc, EscapeRecapture escapeRecapture) {
        Long arrestOrganizationId = escapeRecapture.getArrestingOrganization();
        if (arrestOrganizationId != null) {
            Organization organization = organizationService.get(uc, arrestOrganizationId);
            escapeRecapture.setArrestingOrganizationName(organization.getOrganizationName());
        }
    }

    private void setReadmission(UserContext uc, EscapeRecapture escapeRecapture) {
        escapeRecapture.setReadmissionFacilityName(facilityService.getFacilityNameById(uc, escapeRecapture.getReadmissionFacility()));
    }

    private void addMetaDescription(UserContext uc, EscapeRecapture escapeRecapture, String language) {
        if (escapeRecapture == null) {
            return;
        }

        String escapeReason = escapeRecapture.getEscapeReason();
        if (!BeanHelper.isEmpty(escapeReason)) {
            escapeRecapture.setEscapeReasonDescription(getMetaDataDescription("MOVEMENTREASON", escapeReason, language));
        }

        String fromCustody = escapeRecapture.getFromCustody();
        if (!BeanHelper.isEmpty(fromCustody)) {
            escapeRecapture.setFromCustodyDescription(getMetaDataDescription("ESCAPECUSTODY", fromCustody, language));
        }

        String securityBreached = escapeRecapture.getSecurityBreached();
        if (!BeanHelper.isEmpty(securityBreached)) {
            escapeRecapture.setSecurityBreachedDescription(getMetaDataDescription("SECURITYLEVEL", securityBreached, language));
        }

        String circumstance = escapeRecapture.getCircumstance();
        if (!BeanHelper.isEmpty(circumstance)) {
            escapeRecapture.setCircumstanceDescription(getMetaDataDescription("ESCAPECIRCUMSTANCE", circumstance, language));
        }

        String readmissionReason = escapeRecapture.getReadmissionReason();
        if (!BeanHelper.isEmpty(readmissionReason)) {
            escapeRecapture.setReadmissionReasonDescription(getMetaDataDescription("MOVEMENTREASON", readmissionReason, language));
        }
    }

    private String getMetaDataDescription(String set, String code, String language) {
        CodeType codeType = new CodeType(set, code);
        return referenceDataService.getDescriptionFor(codeType, language);
    }

    private void createEscapeMovement(UserContext uc, EscapeRecapture escape, ActivityType activity) {
        ExternalMovementActivityType movementActivity = new ExternalMovementActivityType();
        movementActivity.setActivityId(activity.getActivityIdentification());
        movementActivity.setSupervisionId(escape.getSupervisionId());
        movementActivity.setMovementCategory(MovementCategory.EXTERNAL.code());
        movementActivity.setMovementType(MovementType.ESCP.code());
        movementActivity.setMovementDirection(MovementDirection.OUT.code());
        movementActivity.setMovementReason(escape.getEscapeReason());
        movementActivity.setMovementStatus(MovementStatus.COMPLETED.code());
        movementActivity.setMovementOutcome(null);
        movementActivity.setMovementDate(escape.getEscapeDate());
        Date now = new Date();
        String escapeComments = escape.getEscapeComments();
        if (!BeanHelper.isEmpty(escapeComments)) {
            CommentType comment = new CommentType();
            comment.setComment(escapeComments);
            comment.setCommentDate(now);
            movementActivity.getCommentText().add(comment);
        }
        // todo binding movement and escape, such as movementActivity.setEscapeRecaptureId(eacspe.getEscapeRecaptureId()); or some way which depends on how data model is going to be defined.
        create(uc, movementActivity, false);
    }

    //WOR-20532
    private void setAssociatedMovementProperty(OffenderScheduleTransferType type){
        MovementActivityEntity entity;
        String hql = "FROM MovementActivityEntity ma where ma.groupid = '" + type.getGroupId() + "' and ma.movementId != "
                + type.getMovementId();
        Query query2 = session.createQuery(hql);
        @SuppressWarnings("rawtypes")
        List results = query2.list();

        if (null != results && !results.isEmpty()) {
            entity = (MovementActivityEntity) results.get(0);
            type.setAssociatedMovementId(entity.getMovementId());
            type.setAssociatedMovementStatus(entity.getMovementStatus());

            if(type.getMovementDirection().equalsIgnoreCase(MovementDirection.IN.code())){
                type.setMovementInId(type.getMovementId());
                type.setMovementOutId(entity.getMovementId());

            }
            if(type.getMovementDirection().equalsIgnoreCase(MovementDirection.OUT.code())){
                type.setMovementOutId(type.getMovementId());
                type.setMovementInId(entity.getMovementId());

            }
        }

    }

    /**
     * @param uc
     * @param startDate
     * @param endDate
     * @param eventType
     * @param configMovementDurationTypes
     * @return Date
     */
    private Date getDefaultEndDateForScheduleConflict(UserContext uc, Date startDate, Date endDate, String eventType,
                                                      List<ConfigMovementDurationType> configMovementDurationTypes) {
        ConfigMovementDurationType configMovementDurationType = null;
        Date internalMovementsDuration = null;
        for (ConfigMovementDurationType configMovementDurationTypeTemp : configMovementDurationTypes) {
            if (eventType.equalsIgnoreCase(configMovementDurationTypeTemp.getMovementType())) {
                SimpleDateFormat dateformat = new SimpleDateFormat("HH:mm");
                configMovementDurationType = configMovementDurationTypeTemp;
                if (configMovementDurationType != null) {
                    try {
                        internalMovementsDuration = dateformat.parse(configMovementDurationType.getDuration());
                        if (internalMovementsDuration != null) {
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(internalMovementsDuration);
                            int hours = calendar.get(Calendar.HOUR_OF_DAY);
                            int minutes = calendar.get(Calendar.MINUTE);
                            calendar.setTime(startDate);
                            calendar.add(Calendar.HOUR, hours);
                            calendar.add(Calendar.MINUTE, minutes);
                            endDate = calendar.getTime();
                            return endDate;
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
        }

        return endDate;
    }

    /**
     * @param uc
     * @param startDate
     * @param endDate
     * @param eventType
     * @param configMovementDurationTypes
     * @return Date
     */
    private Date getModifiedStartDateForScheduleConflict(UserContext uc, Date startDate, Date endDate, String eventType,
                                                         List<ConfigMovementDurationType> configMovementDurationTypes) {
        ConfigMovementDurationType configMovementDurationType = null;
        Date internalMovementsDuration = null;
        for (ConfigMovementDurationType configMovementDurationTypeTemp : configMovementDurationTypes) {
            if (eventType.equalsIgnoreCase(configMovementDurationTypeTemp.getMovementType())) {
                SimpleDateFormat dateformat = new SimpleDateFormat("HH:mm");
                configMovementDurationType = configMovementDurationTypeTemp;
                if (configMovementDurationType != null) {
                    try {
                        internalMovementsDuration = dateformat.parse(configMovementDurationType.getDuration());
                        if (internalMovementsDuration != null) {
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(internalMovementsDuration);
                            int hours = calendar.get(Calendar.HOUR_OF_DAY);
                            int minutes = calendar.get(Calendar.MINUTE);
                            Calendar calendarModified = Calendar.getInstance();
                            calendarModified.setTime(startDate);
                            calendarModified.add(Calendar.HOUR, -hours);
                            calendarModified.add(Calendar.MINUTE, -minutes);
                            startDate = calendarModified.getTime();
                            return startDate;
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
        }

        return startDate;
    }
    
    
    public List<OffenderScheduleTransferType> getAdmitIncomingTransfer(UserContext uc, Long toFacility, Long filId, Long admitingFacility) {
    	List<OffenderScheduleTransferType> offenderList = getAdmitIncomingTransfer(uc, toFacility, filId);
    	Set<Long> supervisionIds = new HashSet<>();
    	for (OffenderScheduleTransferType type : offenderList) {
    		supervisionIds.add(type.getSupervisionId());
    	}
    	List<ReservationRequest>  reservationRequestList = reservationserviceLocale.getResevationOnFacility(uc, admitingFacility, supervisionIds);
    	Map<Long, ReservationRequest> reservationMap = new HashMap<>();
    	for(ReservationRequest reservationRequest2 : reservationRequestList) {
    		reservationMap.put(reservationRequest2.getSupervisionId(), reservationRequest2);
    	}
    	for(OffenderScheduleTransferType type : offenderList) {
    		ReservationRequest reservationRequest = reservationMap.get(type.getSupervisionId());
    		if(reservationRequest!=null) {
    			type.setReserveHousingId(reservationRequest.getInternalLocationId());
    			type.setReservHousingDesc(reservationRequest.getInternalLocationDesc());
    			type.setReserveFacilityId(reservationRequest.getFacilityId());
    			type.setReservationId(reservationRequest.getReservationId());
    		}
    	}
    	return offenderList;
    }

    /**
     * Retrieve incoming offenders by facility.
     *
     * @param uc         UserContext - Required
     * @param toFacility Long - Required
     *                   * @param filId Long - Required
     * @return OffenderScheduleTransferReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    @Override
    public List<OffenderScheduleTransferType> getAdmitIncomingTransfer(UserContext uc, Long toFacility, Long filId) {

        List<OffenderScheduleTransferType> schedule = getAdmitIncomingTransfer(uc, toFacility);
        ConflictHandler conflictHandler = new ConflictHandler(context, session, supervisionService);
        List<NonAssociationType> nonAssociationTypeList = null;
        Set<Long> supervisions = new HashSet<Long>();

        for (OffenderScheduleTransferType type : schedule) {
            if (type.getSupervisionId() != null) {
                supervisions.add(type.getSupervisionId());
            }
        }

        for (OffenderScheduleTransferType type : schedule) {

            nonAssociationTypeList = conflictHandler.checkNonAssociationConflictForHousingMovement(uc, type.getSupervisionId(), supervisions, filId, null, null);

            type.setNonAssociationConflictsForHousing(nonAssociationTypeList);

        }

        return schedule;

    }

    @Override
    public OffenderScheduleTransferType getAdmitIncomingTransferBySupervision(UserContext uc, Long toFacility, Long supervisionId,Long filId) {
        List<OffenderScheduleTransferType> schedule = getAdmitIncomingTransferSearch(uc, toFacility, supervisionId);
        Set<Long> supervisions = new HashSet<Long>();
        ConflictHandler conflictHandler = new ConflictHandler(context, session, supervisionService);
        List<NonAssociationType> nonAssociationTypeList = null;

        for (OffenderScheduleTransferType type : schedule) {
            if (type.getSupervisionId() != null) {
                supervisions.add(type.getSupervisionId());
            }
        }
        for (OffenderScheduleTransferType type : schedule) {

            nonAssociationTypeList = conflictHandler.checkNonAssociationConflictForHousingMovement(uc, type.getSupervisionId(), supervisions, filId, null, null);

            type.setNonAssociationConflictsForHousing(nonAssociationTypeList);

        }
        return (schedule.size()>0)?schedule.get(0):null;
    }

    @Override
    public boolean isInternalLocationCurrentlyUsed(UserContext uc, Long locationId) {
        MovementServiceDAO handler = new MovementServiceDAO(context, session);
        return handler.isInternalLocationCurrentlyUsed(locationId);
    }

    @Override
    public boolean isInternalLocationUsed(UserContext uc, Long locationId) {
        MovementServiceDAO handler = new MovementServiceDAO(context, session);
        return handler.isInternalLocationUsed(locationId);
    }

    @Override
    public Long addEscape(UserContext uc, EscapeRecapture escapeRecapture) {

        // Bean validation
        ValidationHelper.validate(escapeRecapture);
        if (escapeRecapture.getRecaptureDate() == null) {
            validateEscape(uc, escapeRecapture);
        } else {
            validateRecapture(uc, escapeRecapture);
        }

        // Retrieve supervision for validation and facility id
        SupervisionType supervisionType = supervisionService.get(uc, escapeRecapture.getSupervisionId());
        if (supervisionType == null) {
            throw new InvalidInputException("Could not find supervision by supervision id = " + escapeRecapture.getSupervisionId(), ErrorCodes.GENERIC_INVALID_ID);
        }

//        // Optimistically lock the person by acquiring person object.
//        PersonType personType = personService.getPersonBySupervisionId(uc, escapeRecapture.getSupervisionId());
//        if (personType == null) {
//            throw new InvalidInputException("Could not find person by supervision id = " + escapeRecapture.getSupervisionId(), ErrorCodes.GENERIC_INVALID_ID);
//        }

        // Create records
        MovementServiceDAO handler = new MovementServiceDAO(context, session);
        ActivityType activityType = MovementHelper.toActivityType(escapeRecapture);
        activityType = activityService.create(uc, activityType, null, true);
        Long movementId = handler.createExternalMovement(uc, escapeRecapture, supervisionType.getFacilityId(), activityType.getActivityIdentification());
        MovementActivityEntity movement = BeanHelper.getEntity(session, MovementActivityEntity.class, movementId);
        updateLastCompletedSupervisionMovement(uc,movement);
        Long escapeId = handler.createEscape(uc, escapeRecapture, movementId);
//
//        // Validate lock by update person
//        personService.updatePersonType(uc, personType);
        closeSupervision(uc, escapeRecapture.getSupervisionId(), escapeRecapture.getEscapeReason(), MovementType.ESCP, escapeRecapture.getEscapeDateTime(), escapeRecapture.getEscapeComments());
        return escapeId;
    }

    @Override
    public void updateEscape(UserContext uc, EscapeRecapture escapeRecapture) {
        // Bean validation
        ValidationHelper.validate(escapeRecapture);
        ExternalMovementActivityEntity lastEscapeMovement = getLastEscapeMovement(uc, escapeRecapture.getSupervisionId(), escapeRecapture.getEscapeReason());
        if (escapeRecapture.getRecaptureDate() == null) {
            validateEscape(uc, escapeRecapture);
        } else {
            validateRecapture(uc, escapeRecapture);
        }
        MovementServiceDAO handler = new MovementServiceDAO(context, session);
        handler.createEscapeComment(uc, lastEscapeMovement, escapeRecapture);
        handler.updateEscape(uc, escapeRecapture);
    }

    private ExternalMovementActivityEntity getLastEscapeMovement(UserContext uc, Long supervisionId, String movementReason) {
        Criteria criteria = session.createCriteria(ExternalMovementActivityEntity.class);
        criteria.add(Restrictions.eq("supervisionId", supervisionId));
        criteria.add(Restrictions.eq("movementType", MovementType.ESCP.code()));
        criteria.add(Restrictions.eq("movementReason", movementReason));
        criteria.setFetchSize(1);
        criteria.setMaxResults(1);
        criteria.addOrder(Order.desc("movementId"));
        return (ExternalMovementActivityEntity)criteria.uniqueResult();
    }

    @Override
    public List<EscapeRecapture> getEscapesBySupervisionId(UserContext uc, Long supervisionId) {
        if (supervisionId == null) {
            throw new InvalidInputException("Invalid input supervision id (null)", ErrorCodes.GENERIC_INVALID_ID);
        }
        MovementServiceDAO handler = new MovementServiceDAO(context, session);
        return convertMetaDescriptions(uc, handler.getEscapesBySupervisionId(uc, supervisionId), null);
    }

    @Override
    public EscapeRecapture getEscapeDetailsById(UserContext uc, Long escapeId) {
        if (escapeId == null) {
            throw new InvalidInputException("Invalid input escape id (null)", ErrorCodes.GENERIC_INVALID_ID);
        }
        MovementServiceDAO handler = new MovementServiceDAO(context, session);
        return getEscapeMetaDescription(uc, null, handler.getEscapeDetailsById(uc, escapeId));
    }

    @Override
    public boolean isOffenderEscaped(UserContext uc, Long supervisionId) {
        if (supervisionId == null) {
            throw new InvalidInputException("Invalid input supervision id (null)", ErrorCodes.GENERIC_INVALID_ID);
        }
        MovementServiceDAO handler = new MovementServiceDAO(context, session);
        return handler.isOffenderEscaped(uc, supervisionId);
    }

    @Override
    public EscapeRecapture getOffenderLastEscaped(UserContext uc, Long supervisionId, String lang){
        if (supervisionId == null) {
            throw new InvalidInputException("Invalid input supervision id (null)", ErrorCodes.GENERIC_INVALID_ID);
        }
        MovementServiceDAO handler = new MovementServiceDAO(context, session);
        EscapeRecapture escapeRecapture = handler.getEscapesWithoutRecaptureBySupervisionId(uc, supervisionId);
        if (escapeRecapture == null) {
            return null;
        }

        // WOR-23232 - must populate escape reason description which is from a "linked" set.
        CodeType codeType = new CodeType(ReferenceSet.MOVEMENT_TYPE.value(),ReferenceSet.ESCAPE_LINK.value());
        List<ReferenceCodeType> list =  referenceDataService.getReferenceCodesForLinkReferenceSet(uc, codeType, ReferenceSet.MOVEMENT_REASON.value(), true, lang);
        list = list.stream().filter(code -> code.getCode().equals(escapeRecapture.getEscapeReason())).collect(Collectors.toList());
        if (!list.isEmpty() && list.get(0).getDescriptions() != null && !list.get(0).getDescriptions().isEmpty()) {
            escapeRecapture.setEscapeReasonDescription(list.get(0).getDescriptions().iterator().next().getDescription());
        }

        return escapeRecapture;
    }

    private void closeSupervision(UserContext uc, final Long supervisionId, final String movementReasonMetaCode, MovementType movementType, Date closeDateTime, String comment) {
        Date closureDateTime = closeDateTime == null? new Date() : closeDateTime;
        // Close Supervision
        supervisionService.closeFacilityOfInterest(uc, supervisionId, closureDateTime);
        // Cancel Scheduled Release
        supervisionService.closeScheduledRelease(uc, supervisionId, movementType.code(), movementReasonMetaCode, comment);
        CodeType movementReasonSetCode = new CodeType(ReferenceSet.MOVEMENT_REASON.value(), movementReasonMetaCode);
        List<ReferenceCodeType> movementTypeSetCodeList = referenceDataService.getReferenceCodesForLinkReferenceSet(uc, movementReasonSetCode,
                ReferenceSet.MOVEMENT_TYPE.value(), true, "en");
        if (movementTypeSetCodeList == null || movementTypeSetCodeList.isEmpty()) {
            return;
        }

        for (ReferenceCodeType movementTypeSetCode: movementTypeSetCodeList) {
            if (movementTypeSetCode.getCode().equalsIgnoreCase(movementType.code())) {
                SupervisionClosureConfigurationEntity supervisionClosure = getSupervisionClosure(movementTypeSetCode.getCode(), movementReasonMetaCode);
                if (supervisionClosure != null) {
                    // Clean All Schedules
                    if (supervisionClosure.getIsCleanSchedulesFlag()) {
                        // Appointment
                    	appointmentService.cancelAppointmentForSupervision(uc, supervisionId, null, null, null);
                        // Visitation
                        visitationService.cancelVisitForSupervision(uc, supervisionId, VisitEnum.VisitCancelReasonEnum.NONATTEND.code());
                        // Program and Services
                        programService.abandonProgramAssignmentsForSupervision(uc, supervisionId, closureDateTime, comment);
                        // All Scheduled Internal and External Movements
                        cancelScheduledMovementsForSupervision(uc, supervisionId, closureDateTime, movementReasonMetaCode, comment);
                        supervisionService.closeScheduledRelease(uc, supervisionId, null, movementReasonMetaCode, comment);
                    }
                    // Release Bed
                    if (supervisionClosure.getIsReleaseBedFlag()) {
                        housingService.unassignHousingLocationForSupervision(uc, supervisionId, null, closureDateTime, comment);
                    }
                    // Close Legal
                    if (supervisionClosure.getIsCloseLegalFlag()) {
                        // todo
                    }

                    if (supervisionClosure.getIsCloseSupervisionFlag()) {
                        supervisionService.close(uc, supervisionId, closureDateTime);
                    }
                }
            }
        }
    }

    private SupervisionClosureConfigurationEntity getSupervisionClosure(String movementTypeMetaCode, String movementReasonMetaCode) {
        Criteria criteria = session.createCriteria(SupervisionClosureConfigurationEntity.class);
        criteria.add(Restrictions.eq("movementType", movementTypeMetaCode));
        criteria.add(Restrictions.eq("movementReason", movementReasonMetaCode));
        criteria.addOrder(Order.desc("id"));
        criteria.setMaxResults(1);
        criteria.setFetchSize(1);
        return (SupervisionClosureConfigurationEntity)criteria.uniqueResult();
    }

    private void cancelScheduledMovementsForSupervision(UserContext uc, Long supervisionId, Date cancelDate, String movementReasonMetaCode, String comment) {
        FacilityOfInterestType foi = supervisionService.getActiveSupervisionFOI(uc, supervisionId);
        if (foi != null) {
            deleteTransferWaitlistEntry(uc, foi.getFacilityId(), new HashSet<>(Arrays.asList(new Long[] { supervisionId })));
        }
        Date now = new Date();
        String hqlQuery = "Select ma.activityId from MovementActivityEntity ma " +
                "where supervisionId = :supervisionId " +
                "and (movementDate is null or movementDate > :movementDate) " +
                "and (movementStatus = 'PENDING' or movementStatus = 'ONHOLD' or movementStatus = 'APPREQ') ";
        List<Long> activityIdList = session.createQuery(hqlQuery)
                .setLong("supervisionId", supervisionId)
                .setDate("movementDate", now)
                .list();
        for (Long activityId: activityIdList) {
            ActivityType activity = activityService.get(uc, activityId);
            activity.setActiveStatusFlag(false);
            activityService.update(uc, activity, null, true);
        }

        String hqlUpdate = "update MovementActivityEntity " +
                "set movementStatus = :movementStatus, approvalComments = :comment, approvalDate = :cancelDate " +
                "where supervisionId = :supervisionId " +
                "and (movementDate is null or movementDate > :movementDate) " +
                "and (movementStatus = 'PENDING' or movementStatus = 'ONHOLD' or movementStatus = 'APPREQ') ";
        session.createQuery(hqlUpdate)
                .setString("movementStatus", MovementStatus.CANCELLED.code())
                .setLong("supervisionId", supervisionId).setString("comment", comment)
                .setDate("cancelDate", cancelDate)
                .setDate("movementDate", now)
                .executeUpdate();
    }

   /*
    * (non-Javadoc)
    * @see syscon.arbutus.product.services.movement.contract.interfaces.MovementServiceCommon#cancelInternalScheduledMovementsForSupervision(syscon.arbutus.product.security.UserContext, java.lang.Long, java.util.Date, java.lang.String)
    * 
    * Cancel All Pending Internal schedules of Supervision.  
    * For WOR-21799
    */
    @Override
    public void cancelInternalScheduledMovementsForSupervision(UserContext uc, Long supervisionId, Date cancelDate, String movementReasonMetaCode) {
        FacilityOfInterestType foi = supervisionService.getActiveSupervisionFOI(uc, supervisionId);
        if (foi != null) {
            deleteTransferWaitlistEntry(uc, foi.getFacilityId(), new HashSet<>(Arrays.asList(new Long[] { supervisionId })));
        }
        Date now = new Date();
        String hqlQuery = "Select ma.activityId from MovementActivityEntity ma " +
                "where supervisionId = :supervisionId " +
                "and (movementDate is null or movementDate > :movementDate) " +
                "and (DTYPE is InternalMovementActivityEntity) " +
                "and (movementStatus = 'PENDING' or movementStatus = 'ONHOLD' or movementStatus = 'APPREQ') ";
        List<Long> activityIdList = session.createQuery(hqlQuery)
                .setLong("supervisionId", supervisionId)
                .setDate("movementDate", now)
                .list();
        for (Long activityId: activityIdList) {
            ActivityType activity = activityService.get(uc, activityId);
            activity.setActiveStatusFlag(false);
            activityService.update(uc, activity, null, true);
        }

        String hqlUpdate = "update MovementActivityEntity " +
                "set movementStatus = :movementStatus, approvalDate = :cancelDate " +
                "where supervisionId = :supervisionId " +
                "and (movementDate is null or movementDate > :movementDate) " +
                "and (DTYPE is InternalMovementActivityEntity) " +
                "and (movementStatus = 'PENDING' or movementStatus = 'ONHOLD' or movementStatus = 'APPREQ') ";
        session.createQuery(hqlUpdate)
                .setString("movementStatus", MovementStatus.CANCELLED.code())
                .setLong("supervisionId", supervisionId)
                .setDate("cancelDate", cancelDate)
                .setDate("movementDate", now)
                .executeUpdate();
    }

    private void checkNonAssociationConflicts(UserContext uc,
                                              OffenderScheduleTransferType offender) {
        List<NonAssociationType> nonAssociationTypeList;
        NonAssociationType.EventType eventType = null;
        Date startDateTime = null;
        Date endDateTime = null;
        Long facility = null;
        Long toFacilityInternalLocationId = null;
        Long toLocationId = null;

        if (null != offender.getReturnDate()
                && null != offender.getReturnTime()) {
            startDateTime = DateUtil.combineDateTime(offender.getReturnDate(), offender.getReturnTime());
        } else {
            startDateTime = offender.getReturnDate();
        }

        if (null != offender.getReleaseDate()
                && null != offender.getReleaseTime()) {
            endDateTime = DateUtil.combineDateTime(offender.getReleaseDate(), offender.getReleaseTime());
        } else {
            endDateTime = offender.getReleaseDate();
        }

        LocalDate sessionDate = new LocalDate(startDateTime);
        LocalTime startTime = new LocalTime(startDateTime);
        LocalTime endTime = null;
        if (endDateTime != null) {
            endTime = new LocalTime(endDateTime);
        }

        switch (offender.getMovementType()) {

            case TYPE_APP:
                eventType = NonAssociationType.EventType.INTERNAL_MOVEMENT;
                toFacilityInternalLocationId = offender
                        .getToFacilityInternalLocation();
                facility = offender.getFromFacility();
                break;
            case TYPE_TRNIJ:
                eventType = NonAssociationType.EventType.TRN_EXTERNAL_MOVEMENT;
                facility = offender.getToFacility();
                break;
            case TYPE_TAP:
                eventType = NonAssociationType.EventType.TAP_EXTERNAL_MOVEMENT;
                facility = offender.getToFacility() != null ? offender.getToFacility() : offender.getToOrganization();
                if(facility==null){
                    log.info("checkNonAssociationConflicts: Ignore empty destination facility  of TAP for Inmate" + offender.getFirstName() + offender.getLastName() );
                    return;

                }
                toLocationId = offender.getToLocation();
                toFacilityInternalLocationId = toLocationId;
                break;
            case TYPE_CRT:
                eventType = NonAssociationType.EventType.CRT_EXTERNAL_MOVEMENT;
                facility = offender.getToFacility();
                break;
            case TYPE_REL:
                eventType = NonAssociationType.EventType.REL_EXTERNAL_MOVEMENT;
                facility = offender.getFromFacility();
                break;
            case TYPE_APPO:
                eventType = NonAssociationType.EventType.APPOINTMENT;
                facility = offender.getFromFacility();
                break;
        }

        log.info("MovementServiceBean checkNonAssociationConflicts : Sup Id " + offender.getSupervisionId() + "supervisionIdSet: " + supervisionIdSet + " facility "
                + facility + "  toFacilityInternalLocationId " + toFacilityInternalLocationId + " sessionDate " + sessionDate + " startTime " +
                startTime + " endTime " + endTime + " eventType " + eventType);
        nonAssociationTypeList = conflictHandler
                .checkNonAssociationConflictByOnceAndDailyPattern(uc, offender.getSupervisionId(), supervisionIdSet, facility, toFacilityInternalLocationId, sessionDate,
                        startTime, endTime, eventType, null, null, null);

        offender.setNonAssociationTypeList(nonAssociationTypeList);

    }

    /**pwd
     *
     *
     *
     *
     *
     * @param uc
     * @param configuration
     */
    @Override
    public void createMovementCategoryConfiguration(UserContext uc, MovementCategoryConfiguration configuration) {

        MovementServiceDAO dao = new MovementServiceDAO(context, session);
        dao.createCategoryConfiguration(uc, configuration);
    }

    /**
     *
     * @param uc
     * @param configuration
     */
    @Override
    public void updateMovementCategoryConfiguration(UserContext uc, MovementCategoryConfiguration configuration) {

        MovementServiceDAO dao = new MovementServiceDAO(context, session);
        dao.updateCategoryConfiguration(uc, configuration);
    }


    /**
     *
     * @param uc
     * @param configuration
     */
    @Override
    public void deleteMovementCategoryConfiguration(UserContext uc, MovementCategoryConfiguration configuration) {

        MovementServiceDAO dao = new MovementServiceDAO(context, session);
        dao.deleteCategoryConfiguration(uc, configuration);
    }


    /**
     *
     * @param uc
     * @param movementType
     * @param movementReason
     * @return
     */
    @Override
    public MovementCategoryConfiguration getMovementCategoryConfiguration(UserContext uc, String movementType, String movementReason) {

        MovementServiceDAO dao = new MovementServiceDAO(context, session);
        return dao.getMovementCategoryConfiguration(uc, movementType, movementReason);

    }

    /**
     *
     * @param uc
     * @return
     */
    @Override
    public List<MovementCategoryConfiguration> getAllCategoryConfigurations(UserContext uc) {

        MovementServiceDAO dao = new MovementServiceDAO(context, session);
        return dao.getAllCategoryConfigurations(uc);
    }

    /**
     * schedule one time movement
     *
     * IMO: movementDate  should not be populated when scheduling movement
     *
     * @param uc
     * @param movementActivity
     * @return OutActivityId or OutMovementId if any
     */
    @Override
    public Long createMovementSchedule(UserContext uc, MovementActivityType movementActivity,Long facilityId) {
        DefaultMovementHandler movementHandler = selectMovementHandler(uc,movementActivity.getMovementType(), movementActivity.getMovementReason());
        return  movementHandler.create(uc, movementActivity, facilityId);



    }

    /**
     * IMO: only  movement date of MovementActivityType should be empty unless completed
     * @param uc
     * @param movementActivity
     * @param facilityId
     * @return
     */

    @Override
    public Long createAdhocMovement(UserContext uc, MovementActivityType movementActivity,Long facilityId) {
        ValidationHelper.validate(movementActivity);
        DefaultMovementHandler movementHandler = selectMovementHandler(uc,movementActivity.getMovementType(), movementActivity.getMovementReason());
        Long movementId = movementHandler.createAdhoc(uc, movementActivity, facilityId);
        MovementActivityEntity movementActivityEntity = BeanHelper.getEntity(session, MovementActivityEntity.class, movementId);
        updateLastCompletedSupervisionMovement(uc,movementActivityEntity);
        return movementId;

    }

    private DefaultMovementHandler selectMovementHandler(UserContext uc, String movementType, String movementReason) {

        if (movementType  == null) {
            String message = "MovementType are  required.";
            LogHelper.error(log, "selectMovementHandler", message);
            throw new InvalidInputException(message);
        }


        DefaultMovementHandler  movementHandler ;
        switch (MovementType.valueOf(movementType)) {
            case TRNIJ:
                movementHandler = new TransferMovementHandler(session,this, activityService);
                break;

            case TAP:
                movementHandler = new TAPMovementHandler(session,this, activityService);
                break;

            case APPO:
            case CRT:
                if (movementReason==null) {
                    String message = " MovemeMntReason are  required.";
                    LogHelper.error(log, "selectMovementHandler", message);
                    throw new InvalidInputException(message);
                }
                MovementCategoryConfiguration   movementConf = getMovementCategoryConfiguration(uc, movementType,movementReason);
                String category = movementConf.getMovementCategory();
//                String category = "EXTERNAL";
                switch (MovementCategory.valueOf(category)){
                    case NOMOV:
                        movementHandler = new NoMovementHandler(session,this, activityService);
                        break;
                    case INTERNAL:
                        movementHandler = new InternalMovementHandler(session,this, activityService);
                        break;
                    case EXTERNAL:
                        movementHandler = new ExternalMovementHandler(session,this, activityService);
                        break;
                    default:
                        throw new ArbutusRuntimeException("selectMovementHandler: Invalid Category " + category);

                }
                break;

            default:
                throw new ArbutusRuntimeException("selectMovementHandler: Invalid movementType " + movementType);

        }

        return movementHandler;
    }

    @Override
    public void updateMovementSchedule(UserContext uc, MovementActivityType movementActivity) {

        String functionName = "update";
        if (movementActivity == null || movementActivity.getActivityId() == null) {
            String message = "Invalid argument: Activity Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        verifyForUpdate(uc, movementActivity);
        DefaultMovementHandler movementHandler = selectMovementHandler(uc, movementActivity.getMovementType(), movementActivity.getMovementReason());
        movementHandler.update(uc, movementActivity);
    }



    @Override
    public void completeMovementSchedule(UserContext uc, MovementActivityType movementActivity) {
        String functionName = "completeMovementSchedule";
        if (movementActivity == null || movementActivity.getMovementId() == null) {
            String message = "Invalid argument: MovementActivity Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(movementActivity);


        if(!MovementStatus.COMPLETED.isEquals(movementActivity.getMovementStatus())){
            String message = "Invalid argument: Movement Status : " + movementActivity.getMovementStatus();
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        DefaultMovementHandler movementHandler = selectMovementHandler(uc,movementActivity.getMovementType(), movementActivity.getMovementReason());
        movementHandler.completeMovement(uc, movementActivity);
        updateLastCompletedSupervisionMovement(uc, MovementHelper.toMovementActivityEntity(movementActivity, MovementActivityEntity.class));

    }



    public void cancelMovementSchedule(UserContext uc, MovementActivityType  movementActivity){
        String functionName = "cancelMovementSchedule";
        if (movementActivity == null || movementActivity.getMovementId() == null) {
            String message = "Invalid argument: MovementActivity Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(movementActivity);

        verifyForUpdate(uc, movementActivity);

        if(!MovementStatus.CANCELLED.isEquals(movementActivity.getMovementStatus())){
            String message = "Invalid argument: Movement Status : " + movementActivity.getMovementStatus();
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);

        }

        // Get entity by id
        Long id = movementActivity.getMovementId();
        MovementActivityEntity existEntity = BeanHelper.getEntity(session, MovementActivityEntity.class, id);

        String category = existEntity.getMovementCategory();


        if(!(MovementStatus.PENDING.isEquals(existEntity.getMovementStatus()) || MovementStatus.ONHOLD.isEquals(existEntity.getMovementStatus()))){
            String message = "Status ("+ existEntity.getMovementStatus()+ ") can be changed to CANCELED";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        if(movementActivity.getMovementDate()==null){
            movementActivity.setMovementDate(DateUtil.getCurrentDate());

        }


        existEntity.setMovementDate(movementActivity.getMovementDate());
        existEntity.setMovementStatus(movementActivity.getMovementStatus());
        existEntity.setCancelreason(movementActivity.getCancelReason());


        Set<CommentType> comments = movementActivity.getCommentText();
        Date cancelDate = movementActivity.getMovementDate();
        String cancelReasoon = movementActivity.getCancelReason();

        MovementActivityEntity updatingMovmentActivityEntity;
        if (movementActivity instanceof ExternalMovementActivityType) {
            updatingMovmentActivityEntity = MovementHelper.toMovementActivityEntity(movementActivity, ExternalMovementActivityEntity.class);

        }else
        {
            updatingMovmentActivityEntity = MovementHelper.toMovementActivityEntity(movementActivity, InternalMovementActivityEntity.class);

        }


        if (!BeanHelper.isEmpty(updatingMovmentActivityEntity.getCommentText())) {
            updateComments(existEntity, updatingMovmentActivityEntity);
        } else {
            updatingMovmentActivityEntity.setCommentText(new HashSet<MovementActivityCommentEntity>());
            updateComments(existEntity, updatingMovmentActivityEntity);

        }

        existEntity.setMovementCategory(category);

        try {
            session.merge(existEntity);
        }catch(StaleObjectStateException ex){
            throw new ArbutusOptimisticLockException(ex);

        }

        ActivityType activity = activityService.get(uc, movementActivity.getActivityId());
        activity.setActiveStatusFlag(Boolean.FALSE);
        activityService.update(uc, activity, null, Boolean.TRUE);


        if(MovementCategory.NOMOV.isEquals(category)){
            return;

        }

        if(movementActivity.getMovementDirection().equalsIgnoreCase(MovementDirection.OUT.code()) && existEntity.getGroupId()!=null){
            //cancel IN movementment together
            String hql = "FROM MovementActivityEntity ma where ma.groupid = '"
                    + existEntity.getGroupId()
                    + "' and ma.movementId != "
                    + existEntity.getMovementId();
            Query query2 = session.createQuery(hql);
            List results = query2.list();
            if (null != results && !results.isEmpty()){
                MovementActivityEntity entity = (MovementActivityEntity) results.get(0);
                MovementActivityType   inMovementActivityType =  MovementHelper.toMovementActivityType(entity);
                inMovementActivityType.setCancelReason(cancelReasoon);
                inMovementActivityType.setMovementStatus(MovementStatus.CANCELLED.code());
                inMovementActivityType.setMovementDate(cancelDate);
                inMovementActivityType.setCommentText(comments);
                cancelMovementSchedule(uc, inMovementActivityType);
            }

        }

    }



    @Override
    public MovementActivityType getPartenerMovement(UserContext uc,String groupId, Long movementId) {

        if(null==groupId || null==movementId){

            String message = "Invalid input: groupId or movementId " +  groupId + " "  + movementId ;
            LogHelper.error(log, "getPartenerMovement", message);
            throw new InvalidInputException(message);

        }

        MovementActivityType movementActivityType=null;

         String hql = "FROM MovementActivityEntity ma where ma.groupid = '"
                    + groupId
                    + "' and ma.movementId != "
                    + movementId;
            Query query2 = session.createQuery(hql);
            List results = query2.list();
            if (null != results && !results.isEmpty()) {
                MovementActivityEntity entity = (MovementActivityEntity) results.get(0);
                movementActivityType = MovementHelper.toMovementActivityType(entity);
                ActivityType activityType = activityService.get(uc, entity.getActivityId());
                movementActivityType.setPlannedStartDate(activityType.getPlannedStartDate());

            }

        return movementActivityType;

    }



    @Override
    public MovementActivityType getMovementSchedule(UserContext uc, Long movementId) {


        MovementActivityType movementActivityType = get(uc, movementId);

        ActivityType  activityType1 = activityService.get(uc, movementActivityType.getActivityId());

        if(MovementCategory.NOMOV.isEquals(movementActivityType.getMovementCategory())){

            movementActivityType.setPlannedStartDate(activityType1.getPlannedStartDate());
            movementActivityType.setPlannedEndDate(activityType1.getPlannedEndDate());

            return  movementActivityType;
        }

        return movementActivityType;

    }


    @Override
    public void deletedMovementSchedule(UserContext uc, Long movementId) {
        //check
        MovementActivityType movementActivityType = get(uc, movementId);
        if(!MovementStatus.PENDING.isEquals(movementActivityType.getMovementStatus())){

            String message = "Invalid operation: Cannot delete movement with status " +  movementActivityType.getMovementStatus();
            LogHelper.error(log, "deletedMovementSchedule", message);
            throw new InvalidInputException(message);

        }

        MovementActivityEntity  partenerMovement = null;
        if(movementActivityType.getGroupId()!=null){
            String hql = "FROM MovementActivityEntity ma where ma.groupid = '"
                    + movementActivityType.getGroupId()
                    + "' and ma.movementId != "
                    + movementActivityType.getMovementId();
            Query query2 = session.createQuery(hql);
            List results = query2.list();
            if (null != results && !results.isEmpty()){
                partenerMovement = (MovementActivityEntity) results.get(0);
                if(!MovementStatus.PENDING.isEquals(partenerMovement.getMovementStatus())){
                    String message = "Invalid operation: Cannot delete movement with status " +  movementActivityType.getMovementStatus();
                    LogHelper.error(log, "deletedMovementSchedule", message);
                    throw new InvalidInputException(message);

                }


            }

        }

        //Delete
        deleteMovementActivitySchedule(uc,movementId);
        if(null!=partenerMovement){
            deleteMovementActivitySchedule(uc,partenerMovement.getMovementId());

        }

    }

    private void deleteMovementActivitySchedule(UserContext uc, Long movementId) {
        MovementActivityEntity movementActivityEntity = BeanHelper.getEntity(session, MovementActivityEntity.class, movementId);
        Long actionId = movementActivityEntity.getActivityId();
        movementActivityEntity.setFlag(4L);
        session.update(movementActivityEntity);
        ActivityType  activityType = activityService.get(uc, actionId);

        Long scheduleId = activityType.getScheduleID();
        activityService.delete(uc, actionId);
        if(scheduleId!=null){
            activityService.deleteSchedule(uc,scheduleId);

        }


    }


    /**
     * Category for Movement (EXTERNAL, INTERNAL)
     */
    public static enum MovementCategory {

        /**
         * Movement Category External
         */
        EXTERNAL,

        NOMOV,

        /**
         * Movement Category Internal
         */
        INTERNAL;

        public static Set<String> codeSet() {
            Set<String> codeSet = new LinkedHashSet<String>();

            MovementCategory[] values = values();
            for (int i = 0; i < values.length; i++) {
                codeSet.add(values[i].code());
            }

            return codeSet;
        }

        public String code() {
            return name();
        }

        public boolean isEquals(String code) {
            return code().equals(code);
        }

        public boolean isEqualsIgnoreCase(String code) {
            return code().equalsIgnoreCase(code);
        }

    }

    /**
     * Enumeration of Movement Type
     */
    public static enum MovementType {
        ADM, CRT, TRNOJ, TRNIJ, TAP, WR, REL, VISIT, OIC, APP, PROG, BED, MED, CLA, WEEK, ESCP,APPO;

        public static Set<String> codeSet() {
            Set<String> codeSet = new LinkedHashSet<String>();

            MovementType[] values = values();
            for (int i = 0; i < values.length; i++) {
                codeSet.add(values[i].code());
            }

            return codeSet;
        }

        public String code() {
            return name();
        }

        public boolean isEquals(String code) {
            return code().equals(code);
        }

        public boolean isEqualsIgnoreCase(String code) {
            return code().equalsIgnoreCase(code);
        }
    }

    /**
     * Enumeration of Movement Direction
     */
    public static enum MovementDirection {
        OUT, IN;

        public static Set<String> codeSet() {
            Set<String> codeSet = new LinkedHashSet<String>();

            MovementDirection[] values = values();
            for (int i = 0; i < values.length; i++) {
                codeSet.add(values[i].code());
            }

            return codeSet;
        }

        public String code() {
            return name();
        }

        public boolean isEquals(String code) {
            return code().equals(code);
        }

        public boolean isEqualsIgnoreCase(String code) {
            return code().equalsIgnoreCase(code);
        }
    }

    /**
     * Enumeration of Movement Status
     */
    public static enum MovementStatus {
        PENDING, ONHOLD, COMPLETED, CANCELLED, NOSHOW, APPREQ, DENIED, RELEASE, DELETED;

        public static Set<String> codeSet() {
            Set<String> codeSet = new LinkedHashSet<String>();

            MovementStatus[] values = values();
            for (int i = 0; i < values.length; i++) {
                codeSet.add(values[i].code());
            }

            return codeSet;


        }

        public String code() {
            return name();
        }

        public boolean isEquals(String code) {
            return code().equals(code);
        }

        public boolean isEqualsIgnoreCase(String code) {
            return code().equalsIgnoreCase(code);
        }
    }


    /**
     * Enumeration of External Movement Type
     */
    public static enum ExternalMovementType {
        ADM, CRT, TRNOJ, TRNIJ, TAP, WR, REL, MED, WEEK, ESCP, APPO;

        public static Set<String> codeSet() {
            Set<String> codeSet = new LinkedHashSet<String>();

            ExternalMovementType[] values = values();
            for (int i = 0; i < values.length; i++) {
                codeSet.add(values[i].code());
            }

            return codeSet;
        }

        public String code() {
            return name();
        }

        public boolean isEquals(String code) {
            return code().equals(code);
        }

        public boolean isEqualsIgnoreCase(String code) {
            return code().equalsIgnoreCase(code);
        }
    }

    /**
     * Enumeration of Internal Movement Type
     */
    public static enum InternalMovementType {
        VISIT, OIC, APP, PROG, BED, MED, CRT, CLA, APPO;

        public static Set<String> codeSet() {
            Set<String> codeSet = new LinkedHashSet<String>();

            InternalMovementType[] values = values();
            for (int i = 0; i < values.length; i++) {
                codeSet.add(values[i].code());
            }

            return codeSet;
        }

        public String code() {
            return name();
        }

        public boolean isEquals(String code) {
            return code().equals(code);
        }

        public boolean isEqualsIgnoreCase(String code) {
            return code().equalsIgnoreCase(code);
        }
    }
}
