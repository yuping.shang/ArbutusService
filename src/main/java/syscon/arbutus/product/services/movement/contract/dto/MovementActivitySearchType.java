package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

/**
 * Movement Activity Search Type for search
 * <p>At least one criteria must be specified in MovementActivitySearchType
 *
 * @author yshang
 * @version 1.0 (based on Movement Activity 1.0 SDD)
 * @since February 10, 2012.
 */
@ArbutusConstraint(constraints = { "fromMovementDate <= toMovementDate", "fromApprovalDate <= toApprovalDate", "fromApplicationDate <= toApplicationDate",
        "fromReportingDate <= toReportingDate" })
public class MovementActivitySearchType implements Serializable {

    private static final long serialVersionUID = -803931667304949799L;

    private Set<Long> movementIds;

    /**
     * Association to supervision
     */
    private Long supervisionId;

    /**
     * Category for the Movement (EXTERNAL, INTERNAL)
     */
    private String movementCategory;

    /**
     * The type of movement (APP, VISIT etc.,)
     */
    private String movementType;

    /**
     * Activity Id
     */
    private Set<Long> activityIds;

    /**
     * Movement Direction: IN or OUT
     */
    private String movementDirection;

    /**
     * The status of the movement
     */
    private String movementStatus;

    /**
     * The reason for the Movement
     */
    private String movementReason;

    /**
     * Outcome of the movement
     */
    private String movementOutcome;

    /**
     * Search for movements that happened from this date
     */
    private Date fromMovementDate;

    /**
     * Search for movements that happened until this date
     */
    private Date toMovementDate;

    /**
     * Comments related to the Movement
     */
    private String commentText;

    /**
     * Indicates the staff member who approved the Temporary Absence movement.
     * Static association to the Staff service.
     */
    private Long approvedByStaffId;

    /**
     * Search for the approval of the Temporary Absence move from this date
     */
    private Date fromApprovalDate;

    /**
     * Search for the approval of the Temporary Absence move until this date
     */
    private Date toApprovalDate;

    /**
     * Any comments provided by the approver, not required
     */
    private String approvalComments;

    /**
     * The location (address) the offender is coming from
     */
    private Long fromLocationId;

    /**
     * The organization the offender is coming from. This field will be used
     * mainly for those agencies that are not part of the Syscon JMS system in
     * the case of external movements. Will be used for custodial facility
     * internal location if it is an internal movement.
     */
    private Long fromOrganizationId;

    /**
     * The Facility the offender is coming from. Will be used to reference the
     * Facility Service.
     */
    private Long fromFacilityId;

    /**
     * City the offender is from
     */
    private String fromCity;

    /**
     * The location (address) the offender is going to.
     */
    private Long toLocationId;

    /**
     * The organization the offender is going to. This field will be used mainly
     * for those agencies that are not part of the Syscon JMS system in the case
     * of external movements. Will be used for custodial facility internal
     * location if it is an internal movement
     */
    private Long toOrganizationId;

    /**
     * The Facility the offender is going to. Will be used to reference the
     * Facility Service.
     */
    private Long toFacilityId;

    /**
     * City the offender is going to.
     */
    private String toCity;

    /**
     * State or Province the offender is going to.
     */
    private String toProvinceState;

    /**
     * Identifier for the arresting agency noted on the admission form.
     */
    private Long arrestOrganizationId;

    /**
     * The date of the application to search from
     */
    private Date fromApplicationDate;

    /**
     * The date of the application to search to
     */
    private Date toApplicationDate;

    /**
     * Organization escorting offender to inst.
     */
    private Long escortOrganizationId;

    /**
     * Estimated Date of reporting at the receiving facility (Transfer) to
     * search from
     */
    private Date fromReportingDate;

    /**
     * Estimated Date of reporting at the receiving facility (Transfer) to
     * search to
     */
    private Date toReportingDate;

    /**
     * Escort Details
     */
    private String escortDetails;

    /**
     * Internal location category, e.g. Housing/Activity etc.
     */
    private String locationCategory;

    /**
     * From Internal location reference for an internal movement
     */
    private Long fromFacilityInternalLocationId;

    /**
     * To Internal Location details for an internal movement
     */
    private Long toFacilityInternalLocationId;

    /**
     * Activity Start Date (Dates from pending movements are stored in activity)
     */
    private Date plannedStartDate;

    /**
     * Activity End Date (Dates from pending movements are stored in activity)
     */
    private Date plannedEndDate;
    
    /**
     * display only out movement if both in/out is there for a group id and out is pending and filterInOut is true
     */
    private boolean filterInOut;


    private  String  groupId;

    private  Long  interestFaclityId;

    private  Long  facilitySetId;


    /**
     * Constructor
     */
    public MovementActivitySearchType() {
        super();
    }

    public Set<Long> getMovementIds() {
        return movementIds;
    }

    public void setMovementIds(Set<Long> movementIds) {
        this.movementIds = movementIds;
    }

    public Long getFacilitySetId() {
        return facilitySetId;
    }

    public void setFacilitySetId(Long facilitySetId) {
        this.facilitySetId = facilitySetId;
    }

    public Long getInterestFaclityId() {
        return interestFaclityId;
    }

    public void setInterestFaclityId(Long interestFaclityId) {
        this.interestFaclityId = interestFaclityId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * Id of an offender’s period of supervision, refer to a Supervision
     *
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * Id of an offender’s period of supervision, refer to a Supervision
     *
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Category for the Movement (EXTERNAL, INTERNAL), reference code of MovementCategory set
     *
     * @return the movementCategory
     */
    public String getMovementCategory() {
        return movementCategory;
    }

    /**
     * Category for the Movement (EXTERNAL, INTERNAL), reference code of MovementCategory set
     *
     * @param movementCategory the movementCategory to set
     */
    public void setMovementCategory(String movementCategory) {
        this.movementCategory = movementCategory;
    }

    /**
     * The type of movement (APP, VISIT etc.,), reference code of MovementType Set
     *
     * @return the movementType
     */
    public String getMovementType() {
        return movementType;
    }

    /**
     * The type of movement (APP, VISIT etc.,), reference code of MovementType Set
     *
     * @param movementType the movementType to set
     */
    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    /**
     * Id of an Activity, Reference To Activity, required
     *
     * @return the activityIds
     */
    public Set<Long> getActivityIds() {
        return activityIds;
    }

    /**
     * Id of an Activity, Reference To Activity, required
     *
     * @param activityIds the activityIds to set
     */
    public void setActivityIds(Set<Long> activityIds) {
        this.activityIds = activityIds;
    }

    /**
     * Movement Direction: IN or OUT, reference code of MovementDirection set
     *
     * @return the movementDirection
     */
    public String getMovementDirection() {
        return movementDirection;
    }

    /**
     * Movement Direction: IN or OUT, reference code of MovementDirection set
     *
     * @param movementDirection the movementDirection to set
     */
    public void setMovementDirection(String movementDirection) {
        this.movementDirection = movementDirection;
    }

    /**
     * The status of the movement, reference code of MovementStatus set
     *
     * @return the movementStatus
     */
    public String getMovementStatus() {
        return movementStatus;
    }

    /**
     * The status of the movement, reference code of MovementStatus set
     *
     * @param movementStatus the movementStatus to set
     */
    public void setMovementStatus(String movementStatus) {
        this.movementStatus = movementStatus;
    }

    /**
     * The reason for the Movement, reference code of MovementReason set
     *
     * @return the movementReason
     */
    public String getMovementReason() {
        return movementReason;
    }

    /**
     * The reason for the Movement, reference code of MovementReason set
     *
     * @param movementReason the movementReason to set
     */
    public void setMovementReason(String movementReason) {
        this.movementReason = movementReason;
    }

    /**
     * Outcome of a movement, reference code of MovementOutcome set
     *
     * @return the movementOutcome
     */
    public String getMovementOutcome() {
        return movementOutcome;
    }

    /**
     * Outcome of a movement, reference code of MovementOutcome set
     *
     * @param movementOutcome the movementOutcome to set
     */
    public void setMovementOutcome(String movementOutcome) {
        this.movementOutcome = movementOutcome;
    }

    /**
     * Actual movement date from
     *
     * @return the fromMovementDate
     */
    public Date getFromMovementDate() {
        return fromMovementDate;
    }

    /**
     * Actual movement date from
     *
     * @param fromMovementDate the fromMovementDate to set
     */
    public void setFromMovementDate(Date fromMovementDate) {
        this.fromMovementDate = fromMovementDate;
    }

    /**
     * Actual movement date to
     *
     * @return the toMovementDate
     */
    public Date getToMovementDate() {
        return toMovementDate;
    }

    /**
     * Actual movement date to
     *
     * @param toMovementDate the toMovementDate to set
     */
    public void setToMovementDate(Date toMovementDate) {
        this.toMovementDate = toMovementDate;
    }

    /**
     * Comments on the movement
     *
     * @return the commentText
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * Comments on the movement
     *
     * @param commentText the commentText to set
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    /**
     * Id of Staff, Static association to the Staff service.
     * <p>Indicates the staff member who approved the Temporary Absence movement.
     *
     * @return the approvedByStaffId
     */
    public Long getApprovedByStaffId() {
        return approvedByStaffId;
    }

    /**
     * Id of Staff, Static association to the Staff service.
     * <p>Indicates the staff member who approved the Temporary Absence movement.
     *
     * @param approvedByStaffId the approvedByStaffId to set
     */
    public void setApprovedByStaffId(Long approvedByStaffId) {
        this.approvedByStaffId = approvedByStaffId;
    }

    /**
     * The date the approval of the Temporary Absence move was given the search from
     *
     * @return the fromApprovalDate
     */
    public Date getFromApprovalDate() {
        return fromApprovalDate;
    }

    /**
     * The date the approval of the Temporary Absence move was given the search from
     *
     * @param fromApprovalDate the fromApprovalDate to set
     */
    public void setFromApprovalDate(Date fromApprovalDate) {
        this.fromApprovalDate = fromApprovalDate;
    }

    /**
     * The date the approval of the Temporary Absence move was given the search to
     *
     * @return the toApprovalDate
     */
    public Date getToApprovalDate() {
        return toApprovalDate;
    }

    /**
     * The date the approval of the Temporary Absence move was given the search to
     *
     * @param toApprovalDate the toApprovalDate to set
     */
    public void setToApprovalDate(Date toApprovalDate) {
        this.toApprovalDate = toApprovalDate;
    }

    /**
     * Any comments provided by the approver
     *
     * @return the approvalComments
     */
    public String getApprovalComments() {
        return approvalComments;
    }

    /**
     * Any comments provided by the approver
     *
     * @param approvalComments the approvalComments to set
     */
    public void setApprovalComments(String approvalComments) {
        this.approvalComments = approvalComments;
    }

    /**
     * The location (address) the offender is coming from
     *
     * @return the fromLocationId
     */
    public Long getFromLocationId() {
        return fromLocationId;
    }

    /**
     * The location (address) the offender is coming from
     *
     * @param fromLocationId the fromLocationId to set
     */
    public void setFromLocationId(Long fromLocationId) {
        this.fromLocationId = fromLocationId;
    }

    /**
     * The organization the offender is coming from
     *
     * @return the fromOrganizationId
     */
    public Long getFromOrganizationId() {
        return fromOrganizationId;
    }

    /**
     * The organization the offender is coming from
     *
     * @param fromOrganizationId the fromOrganizationId to set
     */
    public void setFromOrganizationId(Long fromOrganizationId) {
        this.fromOrganizationId = fromOrganizationId;
    }

    /**
     * The Facility the offender is coming from. refer to the Facility Service
     *
     * @return the fromFacilityId
     */
    public Long getFromFacilityId() {
        return fromFacilityId;
    }

    /**
     * The Facility the offender is coming from. refer to the Facility Service
     *
     * @param fromFacilityId the fromFacilityId to set
     */
    public void setFromFacilityId(Long fromFacilityId) {
        this.fromFacilityId = fromFacilityId;
    }

    /**
     * City the offender is from
     *
     * @return the fromCity
     */
    public String getFromCity() {
        return fromCity;
    }

    /**
     * City the offender is from
     *
     * @param fromCity the fromCity to set
     */
    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    /**
     * The location (address) the offender is going to
     *
     * @return the toLocationId
     */
    public Long getToLocationId() {
        return toLocationId;
    }

    /**
     * The location (address) the offender is going to
     *
     * @param toLocationId the toLocationId to set
     */
    public void setToLocationId(Long toLocationId) {
        this.toLocationId = toLocationId;
    }

    /**
     * The organization the offender is going to
     *
     * @return the toOrganizationId
     */
    public Long getToOrganizationId() {
        return toOrganizationId;
    }

    /**
     * The organization the offender is going to
     *
     * @param toOrganizationId the toOrganizationId to set
     */
    public void setToOrganizationId(Long toOrganizationId) {
        this.toOrganizationId = toOrganizationId;
    }

    /**
     * The Facility the offender is going to. Will be used to reference the Facility Service
     *
     * @return the toFacilityId
     */
    public Long getToFacilityId() {
        return toFacilityId;
    }

    /**
     * The Facility the offender is going to. Will be used to reference the Facility Service
     *
     * @param toFacilityId the toFacilityId to set
     */
    public void setToFacilityId(Long toFacilityId) {
        this.toFacilityId = toFacilityId;
    }

    /**
     * City the offender is going to
     *
     * @return the toCity
     */
    public String getToCity() {
        return toCity;
    }

    /**
     * City the offender is going to
     *
     * @param toCity the toCity to set
     */
    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    /**
     * State or Province the offender is going to
     *
     * @return the toProvinceState
     */
    public String getToProvinceState() {
        return toProvinceState;
    }

    /**
     * State or Province the offender is going to
     *
     * @param toProvinceState the toProvinceState to set
     */
    public void setToProvinceState(String toProvinceState) {
        this.toProvinceState = toProvinceState;
    }

    /**
     * Identifier for the arresting agency noted on the admission form
     *
     * @return the arrestOrganizationId
     */
    public Long getArrestOrganizationId() {
        return arrestOrganizationId;
    }

    /**
     * Identifier for the arresting agency noted on the admission form
     *
     * @param arrestOrganizationId the arrestOrganizationId to set
     */
    public void setArrestOrganizationId(Long arrestOrganizationId) {
        this.arrestOrganizationId = arrestOrganizationId;
    }

    /**
     * The date of the application from for search
     *
     * @return the fromApplicationDate
     */
    public Date getFromApplicationDate() {
        return fromApplicationDate;
    }

    /**
     * The date of the application from for search
     *
     * @param fromApplicationDate the fromApplicationDate to set
     */
    public void setFromApplicationDate(Date fromApplicationDate) {
        this.fromApplicationDate = fromApplicationDate;
    }

    /**
     * The date of the application to for search
     *
     * @return the toApplicationDate
     */
    public Date getToApplicationDate() {
        return toApplicationDate;
    }

    /**
     * The date of the application to for search
     *
     * @param toApplicationDate the toApplicationDate to set
     */
    public void setToApplicationDate(Date toApplicationDate) {
        this.toApplicationDate = toApplicationDate;
    }

    /**
     * Organization escorting offender
     *
     * @return the escortOrganizationId
     */
    public Long getEscortOrganizationId() {
        return escortOrganizationId;
    }

    /**
     * Organization escorting offender
     *
     * @param escortOrganizationId the escortOrganizationId to set
     */
    public void setEscortOrganizationId(Long escortOrganizationId) {
        this.escortOrganizationId = escortOrganizationId;
    }

    /**
     * Estimated Date of reporting at the receiving facility (Transfer) from for search
     *
     * @return the fromReportingDate
     */
    public Date getFromReportingDate() {
        return fromReportingDate;
    }

    /**
     * Estimated Date of reporting at the receiving facility (Transfer) from for search
     *
     * @param fromReportingDate the fromReportingDate to set
     */
    public void setFromReportingDate(Date fromReportingDate) {
        this.fromReportingDate = fromReportingDate;
    }

    /**
     * Estimated Date of reporting at the receiving facility (Transfer) to for search
     *
     * @return the toReportingDate
     */
    public Date getToReportingDate() {
        return toReportingDate;
    }

    /**
     * Estimated Date of reporting at the receiving facility (Transfer) to for search
     *
     * @param toReportingDate the toReportingDate to set
     */
    public void setToReportingDate(Date toReportingDate) {
        this.toReportingDate = toReportingDate;
    }

    /**
     * Escort Details
     *
     * @return the escortDetails
     */
    public String getEscortDetails() {
        return escortDetails;
    }

    /**
     * Escort Details
     *
     * @param escortDetails the escortDetails to set
     */
    public void setEscortDetails(String escortDetails) {
        this.escortDetails = escortDetails;
    }

    /**
     * Get the Internal Location Category, refer to FacilityInternalLocation's Location Category.
     *
     * @return
     */
    public String getLocationCategory() {
        return locationCategory;
    }

    /**
     * Set the Internal Location Category, refer to FacilityInternalLocation's Location Category.
     * Need to set this value when from/to internal location are not leaf.
     *
     * @param locationCategory
     */
    public void setLocationCategory(String locationCategory) {
        this.locationCategory = locationCategory;
    }

    /**
     * From Facility Internal Location association for an internal movement
     *
     * @return the fromFacilityInternalLocationId
     */
    public Long getFromFacilityInternalLocationId() {
        return fromFacilityInternalLocationId;
    }

    /**
     * From Facility Internal Location association for an internal movement
     *
     * @param fromFacilityInternalLocationId the fromFacilityInternalLocationId to set
     */
    public void setFromFacilityInternalLocationId(Long fromFacilityInternalLocationId) {
        this.fromFacilityInternalLocationId = fromFacilityInternalLocationId;
    }

    /**
     * To Facility Internal Location association for an internal movement
     *
     * @return the toFacilityInternalLocationId
     */
    public Long getToFacilityInternalLocationId() {
        return toFacilityInternalLocationId;
    }

    /**
     * To Facility Internal Location association for an internal movement
     *
     * @param toFacilityInternalLocationId the toFacilityInternalLocationId to set
     */
    public void setToFacilityInternalLocationId(Long toFacilityInternalLocationId) {
        this.toFacilityInternalLocationId = toFacilityInternalLocationId;
    }

    /**
     * Movement planned date
     *
     * @return Date
     */
    public Date getPlannedEndDate() {
        return plannedEndDate;
    }

    /**
     * * Movement planned date
     *
     * @param plannedEndDate
     */
    public void setPlannedEndDate(Date plannedEndDate) {
        this.plannedEndDate = plannedEndDate;
    }

    /**
     * * Movement planned date
     *
     * @return Date
     */
    public Date getPlannedStartDate() {
        return plannedStartDate;
    }

    /**
     * * Movement planned date
     *
     * @param plannedStartDate
     */
    public void setPlannedStartDate(Date plannedStartDate) {
        this.plannedStartDate = plannedStartDate;
    }
    
    /**
     * @return filterInOut
     */
    public boolean isFilterInOut() {
        return filterInOut;
    }

    /**
     * @param filterInOut
     */
    public void setFilterInOut(boolean filterInOut) {
        this.filterInOut = filterInOut;
    }

    /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
    @Override
    public String toString() {
        return "MovementActivitySearchType [supervisionId=" + supervisionId + ", movementCategory=" + movementCategory + ", movementType=" + movementType
                + ", activityIds=" + activityIds + ", movementDirection=" + movementDirection + ", movementStatus=" + movementStatus + ", movementReason="
                + movementReason + ", movementOutcome=" + movementOutcome + ", fromMovementDate=" + fromMovementDate + ", toMovementDate=" + toMovementDate
                + ", commentText=" + commentText + ", approvedByStaffId=" + approvedByStaffId + ", fromApprovalDate=" + fromApprovalDate + ", toApprovalDate="
                + toApprovalDate + ", approvalComments=" + approvalComments + ", fromLocationId=" + fromLocationId + ", fromOrganizationId=" + fromOrganizationId
                + ", fromFacilityId=" + fromFacilityId + ", fromCity=" + fromCity + ", toLocationId=" + toLocationId + ", toOrganizationId=" + toOrganizationId
                + ", toFacilityId=" + toFacilityId + ", toCity=" + toCity + ", toProvinceState=" + toProvinceState + ", arrestOrganizationId=" + arrestOrganizationId
                + ", fromApplicationDate=" + fromApplicationDate + ", toApplicationDate=" + toApplicationDate + ", escortOrganizationId=" + escortOrganizationId
                + ", fromReportingDate=" + fromReportingDate + ", toReportingDate=" + toReportingDate + ", escortDetails=" + escortDetails + ", locationCategory="
                + locationCategory + ", fromFacilityInternalLocationId=" + fromFacilityInternalLocationId + ", toFacilityInternalLocationId="
                + toFacilityInternalLocationId + "]";
    }

}