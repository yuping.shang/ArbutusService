package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;

/**
 * Definition of Schedule Supervision
 *
 * @author bkahlon
 * @version 1.0
 * @since January 24, 2012
 */
public class ScheduleSupervisionType implements Serializable {

    private static final long serialVersionUID = -3981555676749703890L;

    private Long supervisionId;

    /**
     * Constructor
     *
     * @param supervisionId, optional
     */
    public ScheduleSupervisionType(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Gets the value of the supervisionId property.
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * Sets the value of the supervisionId property.
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

}
