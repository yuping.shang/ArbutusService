package syscon.arbutus.product.services.movement.realization.util;

import java.util.Date;

import org.hibernate.Query;
import org.hibernate.Session;
import syscon.arbutus.product.services.movement.contract.dto.CourtMovementType;
import syscon.arbutus.product.services.movement.contract.dto.TwoMovementsSearchType;
import syscon.arbutus.product.services.movement.realization.util.interfaces.MovementsHQLStrategy;

/**
 * Movement HQL Strategy implementation
 * HQL query to get CourtMovements and return as a TwoMovementsType
 *
 * @author wmadruga
 */
public class CourtMovementHQL implements MovementsHQLStrategy {

    public Query buildTwoMovementsHQL(Session session, TwoMovementsSearchType searchType, Date dateOut, Date dateIn) {

        StringBuilder hqlFormat = new StringBuilder();
        //ma1 = OUT		//ma2 = IN

        hqlFormat.append("SELECT new %s(ma1.groupid, ma1.supervisionId, ");
        hqlFormat.append("ma1.applicationDate, ma2.movementId, ma1.movementId, ");
        hqlFormat.append("ma1.fromFacilityId, ma1.toFacilityId, ");
        hqlFormat.append("act1.plannedStartDate, act2.plannedEndDate, ");

        hqlFormat.append("ma1.movementType, ma1.movementReason, ");

        hqlFormat.append("ma2.movementStatus, ma1.movementStatus, ma1.movementCategory ");

        hqlFormat.append(",ma1.movementOutcome, ma1.toInternalLocationId) ");

        hqlFormat.append("FROM ExternalMovementActivityEntity AS ma1, ExternalMovementActivityEntity AS ma2, ");
        hqlFormat.append("ActivityEntity AS act1, ActivityEntity AS act2 ");

        hqlFormat.append("WHERE ma1.movementType = :moveType ");
        hqlFormat.append("AND ma1.groupid = ma2.groupid ");
        hqlFormat.append("AND ma1.movementId <> ma2.movementId ");
        hqlFormat.append("AND ma1.activityId = act1.activityId ");
        hqlFormat.append("AND ma2.activityId = act2.activityId ");
        hqlFormat.append("AND act2.antecedentId = act1.activityId ");
        hqlFormat.append("AND ma1.movementDirection = 'OUT' ");

        if (searchType.getFromFacilityId() != null) {
            hqlFormat.append("AND ma1.fromFacilityId = " + searchType.getFromFacilityId() + " ");
        }

        if (searchType.getToFacilityId() != null) {
            hqlFormat.append("AND ma1.toFacilityId = " + searchType.getToFacilityId() + " ");
        }

        if (searchType.getGroupId() != null) {
            hqlFormat.append("AND ma1.groupid = '" + searchType.getGroupId() + "' ");
        }

        if (searchType.getOffenderId() != null) {
            hqlFormat.append("AND ma1.supervisionId = " + searchType.getOffenderId() + " ");
        }

        //        if(dateOut != null && dateIn != null) {
        //            hqlFormat.append("AND ((act1.plannedStartDate <= :dateIn OR act2.plannedEndDate >= :dateOut) ");
        //            hqlFormat.append("OR (act2.plannedEndDate >= :dateOut OR act1.plannedStartDate <= :dateIn)) ");
        //        }

        if (searchType.getStatusOUT() != null && searchType.getStatusIN() != null) {
            hqlFormat.append("AND (ma1.movementStatus = '" + searchType.getStatusOUT() + "' ");
            hqlFormat.append("OR ma2.movementStatus = '" + searchType.getStatusIN() + "') ");
        } else {
            if (searchType.getStatusOUT() != null) {
                hqlFormat.append("AND ma1.movementStatus = '" + searchType.getStatusOUT() + "' ");
            }
            if (searchType.getStatusIN() != null) {
                hqlFormat.append("AND ma2.movementStatus = '" + searchType.getStatusIN() + "' ");
            }
        }

        if (searchType.getFromFacilityId() != null) {
            hqlFormat.append("AND ma1.fromFacilityId = " + searchType.getFromFacilityId() + " ");
        }

        hqlFormat.append("ORDER BY ma1.groupid ");

        String hql = null;
        hql = String.format(hqlFormat.toString(), CourtMovementType.class.getName());

        Query query = session.createQuery(hql);
        query.setParameter("moveType", searchType.getMovementType());
        //
        //		if(dateOut != null && dateIn != null) {
        //			query.setTimestamp("dateOut", dateOut).setTimestamp("dateIn", dateIn);
        //		}

        return query;
    }

}