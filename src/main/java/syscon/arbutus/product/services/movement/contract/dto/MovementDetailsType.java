package syscon.arbutus.product.services.movement.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * The representation of the MovementDetails type
 *
 * @author lhan
 */
public class MovementDetailsType implements Serializable {
    private static final long serialVersionUID = 567467379527477031L;

    /**
     * An offender’s supervision period. Static reference to the Supervision service. required
     */
    @NotNull
    private Long supervisionIdentification;

    /**
     * Category for the Movement (EXTERNAL, INTERNAL), required
     */
    @NotNull
    private String movementCategory;

    /**
     * Movement Direction: IN or OUT, required
     */
    @NotNull
    private String movementDirection;

    /**
     * The Facility the offender is going to. Static reference to the Facility Service. Optional
     */
    private Long toFacilityId;

    /**
     * To Internal Location reference for an internal movement. Optional
     */
    private Long toFacilityInternalLocationId;

    /**
     * Default constructor
     */
    public MovementDetailsType() {
    }

    /**
     * Constructor
     *
     * @param supervisionIdentification    Long - An offender’s supervision period. Static reference to the Supervision service. required
     * @param movementCategory             String - Category for the Movement (EXTERNAL, INTERNAL), required
     * @param movementDirection            String - Movement Direction: IN or OUT, required
     * @param toFacilityId                 Long - The Facility the offender is going to. Static reference to the Facility Service. Optional
     * @param toFacilityInternalLocationId Long - To Internal Location reference for an internal movement. Optional
     */
    public MovementDetailsType(Long supervisionIdentification, String movementCategory, String movementDirection, Long toFacilityId, Long toFacilityInternalLocationId) {
        super();
        this.supervisionIdentification = supervisionIdentification;
        this.movementCategory = movementCategory;
        this.movementDirection = movementDirection;
        this.toFacilityId = toFacilityId;
        this.toFacilityInternalLocationId = toFacilityInternalLocationId;
    }

    /**
     * An offender’s supervision period. Static reference to the Supervision service. required
     *
     * @return the supervisionIdentification
     */
    public Long getSupervisionIdentification() {
        return supervisionIdentification;
    }

    /**
     * An offender’s supervision period. Static reference to the Supervision service. required
     *
     * @param supervisionIdentification the supervisionIdentification to set
     */
    public void setSupervisionIdentification(Long supervisionIdentification) {
        this.supervisionIdentification = supervisionIdentification;
    }

    /**
     * Category for the Movement (EXTERNAL, INTERNAL), required
     * A code value of MovementCategory reference code set
     *
     * @return the movementCategory
     */
    public String getMovementCategory() {
        return movementCategory;
    }

    /**
     * Category for the Movement (EXTERNAL, INTERNAL), required
     * A code value of MovementCategory reference code set
     *
     * @param movementCategory the movementCategory to set
     */
    public void setMovementCategory(String movementCategory) {
        this.movementCategory = movementCategory;
    }

    /**
     * Movement Direction: IN or OUT, required
     * A code value of MovementDirection reference code set
     *
     * @return the movementDirection
     */
    public String getMovementDirection() {
        return movementDirection;
    }

    /**
     * Movement Direction: IN or OUT, required
     * A code value of MovementDirection reference code set
     *
     * @param movementDirection the movementDirection to set
     */
    public void setMovementDirection(String movementDirection) {
        this.movementDirection = movementDirection;
    }

    /**
     * The Facility the offender is going to. Static reference to the Facility Service. Optional
     *
     * @return the toFacilityId
     */
    public Long getToFacilityId() {
        return toFacilityId;
    }

    /**
     * The Facility the offender is going to. Static reference to the Facility Service. Optional
     *
     * @param toFacilityId the toFacilityId to set
     */
    public void setToFacilityId(Long toFacilityId) {
        this.toFacilityId = toFacilityId;
    }

    /**
     * To Internal Location reference for an internal movement. Optional
     *
     * @return the toFacilityInternalLocationId
     */
    public Long getToFacilityInternalLocationId() {
        return toFacilityInternalLocationId;
    }

    /**
     * To Internal Location reference for an internal movement. Optional
     *
     * @param toFacilityInternalLocationId the toFacilityInternalLocationId to set
     */
    public void setToFacilityInternalLocationId(Long toFacilityInternalLocationId) {
        this.toFacilityInternalLocationId = toFacilityInternalLocationId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "MovementDetailsType [movementDetails=" + supervisionIdentification + ", movementCategory=" + movementCategory + ", movementDirection=" + movementDirection
                + ", toFacilityId=" + toFacilityId + ", toFacilityInternalLocationId=" + toFacilityInternalLocationId + "]";
    }
}
