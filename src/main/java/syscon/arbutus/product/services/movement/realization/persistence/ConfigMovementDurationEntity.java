package syscon.arbutus.product.services.movement.realization.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import syscon.arbutus.product.services.core.services.realization.BaseEntity;

/**
 * ConfigMovementDurationEntity for Movement Service
 *
 * @author rajiv.kumar
 *
 * @DbComment MA_ConfigDuration 'Configure Movement Duration table for Movement Activity Service'
 * @DbComment .createUserId 'User ID who created the object'
 * @DbComment .createDateTime 'Date and time when the object was created'
 * @DbComment .modifyUserId 'User ID who last updated the object'
 * @DbComment .modifyDateTime 'Date and time when the object was last updated'
 * @DbComment .invocationContext 'Invocation context when the create/update action called'
 * @DbComment .version 'the version number of record updated'
 */
@Audited
@Entity
@Table(name = "MA_ConfigDuration")
public class ConfigMovementDurationEntity extends BaseEntity implements Serializable {
	private static final long serialVersionUID = -8000994301150585316L;

	/**
	 * @DbComment movementType 'States the movement type. Supported types are ADM, CRT, TRN, TAP, WR, REL, VISIT, OIC, APP, PROG, BED, MED, CLA'
	 */
	@Id
	@Column(name = "movementType", nullable = false)
	private String movementType;

	/**
	 * @DbComment duration 'Duration of a movement from the start date/time to the end date/time'
	 */
	@Column(name = "duration", nullable = true)
	private String duration;

	public ConfigMovementDurationEntity() {
		super();
	}

	public ConfigMovementDurationEntity(String movementType, String duration) {
		super();
		this.movementType = movementType;
		this.duration = duration;
	}

	@Override
	public Serializable getId() {
		return movementType;
	}

	public String getMovementType() {
		return movementType;
	}

	public void setMovementType(String movementType) {
		this.movementType = movementType;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((duration == null) ? 0 : duration.hashCode());
		result = prime * result
				+ ((movementType == null) ? 0 : movementType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ConfigMovementDurationEntity other = (ConfigMovementDurationEntity) obj;
		if (duration == null) {
			if (other.duration != null) {
				return false;
			}
		} else if (!duration.equals(other.duration)) {
			return false;
		}
		if (movementType == null) {
			if (other.movementType != null) {
				return false;
			}
		} else if (!movementType.equals(other.movementType)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ConfigMovementDuration [movementType=" + movementType
				+ ", duration=" + duration + "]";
	}

}
