package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;

/**
 * Definition of Supervision Location
 *
 * @author bkahlon
 * @version 1.0
 * @since January 24, 2012
 */
public class SupervisionLocationType implements Serializable {

    private static final long serialVersionUID = -3981555676749703890L;

    private Long supervisionId;
    private Long locationId;

    /**
     *
     */
    public SupervisionLocationType() {

    }

    /**
     * @param locationId,    optional
     * @param supervisionId, optional
     */
    public SupervisionLocationType(Long locationId, Long supervisionId) {
        super();
        this.locationId = locationId;
        this.supervisionId = supervisionId;
    }

    public SupervisionLocationType(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Gets the value of the locationId property.
     */
    public Long getLocationId() {
        return locationId;
    }

    /**
     * Sets the value of the locationId property.
     */
    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    /**
     * Gets the value of the supervisionId property.
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * Sets the value of the supervisionId property.
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

}
