package syscon.arbutus.product.services.movement.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * The representation of the OffenderLocationStatus type
 *
 * @author lhan
 */
public class OffenderLocationStatusType implements Serializable {

    private static final long serialVersionUID = -1311975179157110574L;
    /**
     * An offender’s supervision period. Static reference to the Supervision service. required
     */
    @NotNull
    private Long supervisionIdentification;
    /**
     * Indicates the offender’s current IN/OUT status at their assigned facility based on latest completed movement. required
     */
    @NotNull
    private String inOUtStatus;
    /**
     * The current housing location of an offender. Static reference to the FacilityInternalLocation service obtained via the Housing Bed Management Activity service. required
     */
    @NotNull
    private Long housingLocationId;
    /**
     * The current location of an offender within the facility. Static reference to the FacilityInternalLocation service obtained via the Movement Activity service. optional
     */
    private Long currentInternalLocationId;
    /**
     * The current location (address) the offender is at. Static reference to the Location service obtained via the Movement Activity service. optional
     */
    private Long currentAddressId;
    /**
     * The current organization the offender is at. Static reference to the Organization service obtained via the Movement Activity service.  optional
     */
    private Long currentOrganizationId;
    /**
     * The current facility the offender is at. Static reference to the Facility service obtained via the Movement Activity service.  optional
     */
    private Long currentFacilityId;
    /**
     * The current city the offender is at obtained via the Movement Activity service. optional
     */
    private String currentCity;
    /**
     * The current State or Province the offender is in obtained via the Movement Activity service. optional
     */
    private String currentProvinceState;

    /**
     * Default constructor
     */
    public OffenderLocationStatusType() {
    }

    /**
     * Constructor
     *
     * @param supervisionIdentification Long - An offender’s supervision period. Static reference to the Supervision service. required
     * @param inOUtStatus               String - Indicates the offender’s current IN/OUT status at their assigned facility based on latest completed movement. required
     * @param housingLocationId         Long - The current housing location of an offender. Static reference to the FacilityInternalLocation service obtained via the Housing Bed Management Activity service. required
     * @param currentInternalLocationId Long - The current location of an offender within the facility. Static reference to the FacilityInternalLocation service obtained via the Movement Activity service. optional
     * @param currentAddressId          Long - The current location (address) the offender is at. Static reference to the Location service obtained via the Movement Activity service. optional
     * @param currentOrganizationId     Long - The current organization the offender is at. Static reference to the Organization service obtained via the Movement Activity service.  optional
     * @param currentFacilityId         Long - The current facility the offender is at. Static reference to the Facility service obtained via the Movement Activity service.  optional
     * @param currentCity               String - The current city the offender is at obtained via the Movement Activity service. optional
     * @param currentProvinceState      String - The current State or Province the offender is in obtained via the Movement Activity service. optional
     */
    public OffenderLocationStatusType(Long supervisionIdentification, String inOUtStatus, Long housingLocationId, Long currentInternalLocationId, Long currentAddressId,
            Long currentOrganizationId, Long currentFacilityId, String currentCity, String currentProvinceState) {
        this.supervisionIdentification = supervisionIdentification;
        this.inOUtStatus = inOUtStatus;
        this.housingLocationId = housingLocationId;
        this.currentInternalLocationId = currentInternalLocationId;
        this.currentAddressId = currentAddressId;
        this.currentOrganizationId = currentOrganizationId;
        this.currentFacilityId = currentFacilityId;
        this.currentCity = currentCity;
        this.currentProvinceState = currentProvinceState;
    }

    /**
     * An offender’s supervision period. Static reference to the Supervision service. required
     *
     * @return the supervisionIdentification
     */
    public Long getSupervisionIdentification() {
        return supervisionIdentification;
    }

    /**
     * An offender’s supervision period. Static reference to the Supervision service. required
     *
     * @param supervisionIdentification the supervisionIdentification to set
     */
    public void setSupervisionIdentification(Long supervisionIdentification) {
        this.supervisionIdentification = supervisionIdentification;
    }

    /**
     * Indicates the offender’s current IN/OUT status at their assigned facility based on latest completed movement. required
     *
     * @return the inOUtStatus
     */
    public String getInOUtStatus() {
        return inOUtStatus;
    }

    /**
     * Indicates the offender’s current IN/OUT status at their assigned facility based on latest completed movement. required
     *
     * @param inOUtStatus the inOUtStatus to set
     */
    public void setInOUtStatus(String inOUtStatus) {
        this.inOUtStatus = inOUtStatus;
    }

    /**
     * The current housing location of an offender. Static reference to the FacilityInternalLocation service obtained via the Housing Bed Management Activity service. required
     *
     * @return the housingLocationId
     */
    public Long getHousingLocationId() {
        return housingLocationId;
    }

    /**
     * The current housing location of an offender. Static reference to the FacilityInternalLocation service obtained via the Housing Bed Management Activity service. required
     *
     * @param housingLocationId the housingLocationId to set
     */
    public void setHousingLocationId(Long housingLocationId) {
        this.housingLocationId = housingLocationId;
    }

    /**
     * The current location of an offender within the facility. Static reference to the FacilityInternalLocation service obtained via the Movement Activity service. optional
     *
     * @return the currentInternalLocationId
     */
    public Long getCurrentInternalLocationId() {
        return currentInternalLocationId;
    }

    /**
     * The current location of an offender within the facility. Static reference to the FacilityInternalLocation service obtained via the Movement Activity service. optional
     *
     * @param currentInternalLocationId the currentInternalLocationId to set
     */
    public void setCurrentInternalLocationId(Long currentInternalLocationId) {
        this.currentInternalLocationId = currentInternalLocationId;
    }

    /**
     * The current location (address) the offender is at. Static reference to the Location service obtained via the Movement Activity service. optional
     *
     * @return the currentAddressId
     */
    public Long getCurrentAddressId() {
        return currentAddressId;
    }

    /**
     * The current location (address) the offender is at. Static reference to the Location service obtained via the Movement Activity service. optional
     *
     * @param currentAddressId the currentAddressId to set
     */
    public void setCurrentAddressId(Long currentAddressId) {
        this.currentAddressId = currentAddressId;
    }

    /**
     * The current organization the offender is at. Static reference to the Organization service obtained via the Movement Activity service.  optional
     *
     * @return the currentOrganizationId
     */
    public Long getCurrentOrganizationId() {
        return currentOrganizationId;
    }

    /**
     * The current organization the offender is at. Static reference to the Organization service obtained via the Movement Activity service.  optional
     *
     * @param currentOrganizationId the currentOrganizationId to set
     */
    public void setCurrentOrganizationId(Long currentOrganizationId) {
        this.currentOrganizationId = currentOrganizationId;
    }

    /**
     * The current facility the offender is at. Static reference to the Facility service obtained via the Movement Activity service.  optional
     *
     * @return the currentFacilityId
     */
    public Long getCurrentFacilityId() {
        return currentFacilityId;
    }

    /**
     * The current facility the offender is at. Static reference to the Facility service obtained via the Movement Activity service.  optional
     *
     * @param currentFacilityId the currentFacilityId to set
     */
    public void setCurrentFacilityId(Long currentFacilityId) {
        this.currentFacilityId = currentFacilityId;
    }

    /**
     * The current city the offender is at obtained via the Movement Activity service. optional
     *
     * @return the currentCity
     */
    public String getCurrentCity() {
        return currentCity;
    }

    /**
     * The current city the offender is at obtained via the Movement Activity service. optional
     *
     * @param currentCity the currentCity to set
     */
    public void setCurrentCity(String currentCity) {
        this.currentCity = currentCity;
    }

    /**
     * The current State or Province the offender is in obtained via the Movement Activity service. optional
     * A code value of StateProvince reference code set.
     *
     * @return the currentProvinceState
     */
    public String getCurrentProvinceState() {
        return currentProvinceState;
    }

    /**
     * The current State or Province the offender is in obtained via the Movement Activity service. optional
     * A code value of StateProvince reference code set.
     *
     * @param currentProvinceState the currentProvinceState to set
     */
    public void setCurrentProvinceState(String currentProvinceState) {
        this.currentProvinceState = currentProvinceState;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "OffenderLocationStatusType [supervisionIdentification=" + supervisionIdentification + ", inOUtStatus=" + inOUtStatus + ", housingLocationId="
                + housingLocationId + ", currentInternalLocationId=" + currentInternalLocationId + ", currentAddressId=" + currentAddressId + ", currentOrganizationId="
                + currentOrganizationId + ", currentFacilityId=" + currentFacilityId + ", currentCity=" + currentCity + ", currentProvinceState=" + currentProvinceState
                + "]";
    }

    /**
     * Enumeration of IN-OUT Status
     */
    public enum InOutStatus {
        OUT, IN;

        public static boolean contains(String s) {
            for (InOutStatus status : values()) {
                if (status.name().equals(s)) {
                    return true;
                }
            }
            return false;
        }

        public String code() {
            return name();
        }
    }
}
