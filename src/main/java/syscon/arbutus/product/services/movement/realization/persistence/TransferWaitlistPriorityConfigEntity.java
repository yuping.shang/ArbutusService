package syscon.arbutus.product.services.movement.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.core.services.realization.BaseEntity;
import syscon.arbutus.product.services.realization.model.MetaCode;

/**
 * TransferWaitlistPriorityConfigEntity for Movement Activity Service
 *
 * @author yshang, Syscon Justice Systems
 * @version 1.0
 * @DbComment MA_TranWaitPrior 'Transfer Wait list Priority Configuration table for Movement Activity Service'
 * @DbComment .createUserId 'User ID who created the object'
 * @DbComment .createDateTime 'Date and time when the object was created'
 * @DbComment .modifyUserId 'User ID who last updated the object'
 * @DbComment .modifyDateTime 'Date and time when the object was last updated'
 * @DbComment .invocationContext 'Invocation context when the create/update action called'
 * @DbComment .version 'the version number of record updated'
 * @since September 27, 2012
 */
@Audited
@Entity
@Table(name = "MA_TranWaitPrior")
public class TransferWaitlistPriorityConfigEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 7941203488838104583L;

    /**
     * @DbComment WaitPriorityID 'Transfer Wait list Priority Configuration Configuration ID'
     */
    @Id
    @Column(name = "WaitPriorityID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_MA_TRANSWAITPRIOR_ID")
    @SequenceGenerator(name = "SEQ_MA_TRANSWAITPRIOR_ID", sequenceName = "SEQ_MA_TRANSWAITPRIOR_ID", allocationSize = 1)
    private Long id;

    /**
     * @DbComment priority 'description of a priority level for the transfer wait list (e.g., Low, Medium, High, Urgent)'
     */
    @Column(name = "priority", nullable = false)
    @MetaCode(set = MetaSet.TRANSFER_WAITLIST_PRIORITY)
    private String transferWaitlistEntryPriority;

    /**
     * @DbComment priorLevel 'The level of priority is determined by this value. The highest level is 1, and the lower priorities are larger numbers'
     */
    @Column(name = "priorLevel", nullable = false)
    private Long priorityLevel;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the transferWaitlistEntryPriority
     */
    public String getTransferWaitlistEntryPriority() {
        return transferWaitlistEntryPriority;
    }

    /**
     * @param transferWaitlistEntryPriority the transferWaitlistEntryPriority to set
     */
    public void setTransferWaitlistEntryPriority(String transferWaitlistEntryPriority) {
        this.transferWaitlistEntryPriority = transferWaitlistEntryPriority;
    }

    /**
     * @return the priorityLevel
     */
    public Long getPriorityLevel() {
        return priorityLevel;
    }

    /**
     * @param priorityLevel the priorityLevel to set
     */
    public void setPriorityLevel(Long priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((transferWaitlistEntryPriority == null) ? 0 : transferWaitlistEntryPriority.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TransferWaitlistPriorityConfigEntity other = (TransferWaitlistPriorityConfigEntity) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (transferWaitlistEntryPriority == null) {
            if (other.transferWaitlistEntryPriority != null) {
                return false;
            }
        } else if (!transferWaitlistEntryPriority.equals(other.transferWaitlistEntryPriority)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TransferWaitlistPriorityConfigEntity [id=" + id + ", transferWaitlistEntryPriority=" + transferWaitlistEntryPriority + ", priorityLevel=" + priorityLevel
                + ", stamp=" + getStamp() + "]";
    }

}
