package syscon.arbutus.product.services.movement.contract.ejb;

import javax.ejb.SessionContext;
import java.util.*;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.StaleObjectStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.core.common.adapters.FacilityInternalLocationAdapter;
import syscon.arbutus.product.services.core.common.adapters.FacilityServiceAdapter;
import syscon.arbutus.product.services.core.common.adapters.PersonServiceAdapter;
import syscon.arbutus.product.services.core.common.adapters.SupervisionServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.ArbutusOptimisticLockException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.FacilityInternalLocation;
import syscon.arbutus.product.services.movement.contract.dto.*;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementServiceLocal;
import syscon.arbutus.product.services.movement.realization.persistence.ExternalMovementActivityEntity;
import syscon.arbutus.product.services.movement.realization.persistence.InternalMovementActivityEntity;
import syscon.arbutus.product.services.movement.realization.persistence.MovementActivityCommentEntity;
import syscon.arbutus.product.services.movement.realization.persistence.MovementActivityEntity;
import syscon.arbutus.product.services.organization.realization.persistence.OrganizationEntity;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.contract.ejb.SupervisionHandler;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * Created by hshen on 1/22/16.
 */
public class TAPMovementHandler extends   DefaultMovementHandler {

    private static Logger log = LoggerFactory.getLogger(NoMovementHandler.class);

    public TAPMovementHandler(Session session, MovementServiceLocal movementService, ActivityService activityService) {
        super(session, movementService, activityService);
    }

    @Override
    public Long create(UserContext uc, MovementActivityType movementActivity, Long facilityId) {

            String functionName=this.getClass().getName() + "create";
            if (!(movementActivity instanceof ExternalMovementActivityType)) {
                String message = "Invalid Type: ExternalMovementActivityType is required.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);


            }


            ExternalMovementActivityType tapMovement = (ExternalMovementActivityType) movementActivity;
            Long result = null;
            String groupId = UUID.randomUUID().toString();

            //create out out activity
            ActivityType outActivity = createScheduleActivity(uc, tapMovement.getPlannedStartDate(), facilityId, tapMovement, false, null);

            ActivityType returnActivity = createScheduleActivity(uc, tapMovement.getPlannedEndDate(), facilityId, tapMovement, false, null);

            tapMovement.setGroupId(groupId);

            Long fromFacilityId = tapMovement.getFromFacilityId();
            Long fromLocationId = tapMovement.getFromLocationId();
            Long fromOrganizationId =  tapMovement.getFromOrganizationId();

            Long toFacilityId = tapMovement.getToFacilityId();
            Long toOrganizationId = tapMovement.getToOrganizationId();
            Long toLocationId = tapMovement.getToLocationId();



            tapMovement.setMovementDate(null);
            tapMovement.setMovementDirection(MovementServiceBean.MovementDirection.OUT.code());
            tapMovement.setMovementCategory(MovementServiceBean.MovementCategory.EXTERNAL.code());
            tapMovement.setActivityId(outActivity.getActivityIdentification());
            tapMovement.setMovementStatus(MovementServiceBean.MovementStatus.PENDING.code());
            result = movementService.create(uc, tapMovement, false).getMovementId();


          // create IN movement
            tapMovement.setMovementDirection(MovementServiceBean.MovementDirection.IN.code());
            tapMovement.setMovementCategory(MovementServiceBean.MovementCategory.EXTERNAL.code());
            tapMovement.setMovementDate(null);
            tapMovement.setActivityId(returnActivity.getActivityIdentification());
            tapMovement.setMovementStatus(MovementServiceBean.MovementStatus.PENDING.code());

            tapMovement.setFromFacilityId(toFacilityId);
            tapMovement.setFromLocationId(toLocationId);
            tapMovement.setFromOrganizationId(toOrganizationId);

            tapMovement.setToLocationId(fromLocationId);
            tapMovement.setToFacilityId(fromFacilityId);
            tapMovement.setToOrganizationId(fromOrganizationId);

            movementService.create(uc, tapMovement, false);
            return result;


    }

    @Override
    public Long createAdhoc(UserContext uc, MovementActivityType movementActivity, Long facilityId) {

        String functionName=this.getClass().getName() + "createAdhoc";
        if (!(movementActivity instanceof ExternalMovementActivityType)) {
            String message = "Invalid Type: ExternalMovementActivityType is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);


        }

        //to check the planned startDate in the past
        ExternalMovementActivityType tapMovement = (ExternalMovementActivityType) movementActivity;
        Long result = null;
        String groupId = UUID.randomUUID().toString();

        //create out out activity
        Long outActivityId = createActivity(uc, movementActivity.getMovementDate(), movementActivity.getSupervisionId(), Boolean.FALSE, null);
        ActivityType returnActivity = createScheduleActivity(uc, tapMovement.getPlannedEndDate(), facilityId, tapMovement, false, null);

        tapMovement.setGroupId(groupId);

        Long fromFacilityId = tapMovement.getFromFacilityId();
        Long fromLocationId = tapMovement.getFromLocationId();
        Long fromOrganizationId =  tapMovement.getFromOrganizationId();

        Long toFacilityId = tapMovement.getToFacilityId();
        Long toOrganizationId = tapMovement.getToOrganizationId();
        Long toLocationId = tapMovement.getToLocationId();



        tapMovement.setMovementDate(movementActivity.getMovementDate());
        tapMovement.setMovementDirection(MovementServiceBean.MovementDirection.OUT.code());
        tapMovement.setMovementCategory(MovementServiceBean.MovementCategory.EXTERNAL.code());
        tapMovement.setActivityId(outActivityId);
        tapMovement.setMovementStatus(MovementServiceBean.MovementStatus.COMPLETED.code());
        result = movementService.create(uc, tapMovement, false).getMovementId();


        // create IN movement
        tapMovement.setMovementDirection(MovementServiceBean.MovementDirection.IN.code());
        tapMovement.setMovementCategory(MovementServiceBean.MovementCategory.EXTERNAL.code());
        tapMovement.setMovementDate(null);
        tapMovement.setActivityId(returnActivity.getActivityIdentification());
        tapMovement.setMovementStatus(MovementServiceBean.MovementStatus.PENDING.code());


        tapMovement.setFromFacilityId(toFacilityId);
        tapMovement.setFromLocationId(toLocationId);
        tapMovement.setFromOrganizationId(toOrganizationId);

        tapMovement.setToLocationId(fromLocationId);
        tapMovement.setToFacilityId(fromFacilityId);
        tapMovement.setToOrganizationId(fromOrganizationId);

        movementService.create(uc, tapMovement, false);
        return result;

    }

    @Override
    public void update(UserContext uc, MovementActivityType movement) {

        String functionName=this.getClass().getName() + "update";

        if (!(movement instanceof ExternalMovementActivityType)){
            String message = "Invalid Type: ExternalMovementActivityType is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);

        }

        ExternalMovementActivityType  movementActivity = (ExternalMovementActivityType)movement;

        Long id = movementActivity.getMovementId();
        ExternalMovementActivityEntity existEntity = BeanHelper.getEntity(session, ExternalMovementActivityEntity.class, id);
        String  groupId = existEntity.getGroupId();


        MovementActivityEntity updatingMovmentActivityEntity = MovementHelper.toMovementActivityEntity(movementActivity, ExternalMovementActivityEntity.class);

        ValidationHelper.verifyMetaCodes(updatingMovmentActivityEntity, false);
        updateMoveActivityStatus(functionName, existEntity, updatingMovmentActivityEntity.getMovementStatus());

        if(MovementServiceBean.MovementDirection.IN.isEquals(existEntity.getMovementDirection()) ) {
            ExternalMovementActivityEntity partnerMovementEntity = (ExternalMovementActivityEntity)getPartnerMovement(groupId, id);
            if (null != partnerMovementEntity && !MovementServiceBean.MovementStatus.COMPLETED.isEquals(partnerMovementEntity.getMovementStatus())) {

                String message = "Unsupport operation: its OUT movement is in NON-compleated status.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);

            }

            //only allow to update return date/time and comments

            if (!BeanHelper.isEmpty(updatingMovmentActivityEntity.getCommentText())) {
                updateComments(existEntity, updatingMovmentActivityEntity);
            } else {
                updatingMovmentActivityEntity.setCommentText(new HashSet<MovementActivityCommentEntity>());
                updateComments(existEntity, updatingMovmentActivityEntity);

            }


            existEntity.setEscortOrganizationId(movementActivity.getEscapeId());
            existEntity.setTransportation(movementActivity.getTransportation());
            existEntity.setEscortDetails(movementActivity.getEscortDetails());

            try {

                session.merge(existEntity);
                //update out Activity
                Long activityId = updatingMovmentActivityEntity.getActivityId();
                updateActivityDate(uc, movementActivity.getPlannedEndDate(), null, activityId);
            }catch(StaleObjectStateException ex){
                throw new ArbutusOptimisticLockException(ex);

            }

            return;

        }


        //Copy properties to update
        existEntity.setApprovalComments(updatingMovmentActivityEntity.getApprovalComments());
        existEntity.setApprovalDate(updatingMovmentActivityEntity.getApprovalDate());
        existEntity.setApprovedByStaffId(updatingMovmentActivityEntity.getApprovedByStaffId());
        existEntity.setMovementReason(updatingMovmentActivityEntity.getMovementReason());
        existEntity.setMovementOutcome(updatingMovmentActivityEntity.getMovementOutcome());



        if (!BeanHelper.isEmpty(updatingMovmentActivityEntity.getCommentText())) {
            updateComments(existEntity, updatingMovmentActivityEntity);
        } else {
            updatingMovmentActivityEntity.setCommentText(new HashSet<MovementActivityCommentEntity>());
            updateComments(existEntity, updatingMovmentActivityEntity);

        }

        updateExternalMovementProps(existEntity, updatingMovmentActivityEntity);

        try {
            session.merge(existEntity);

            //update out Activity
            Long activityId =  updatingMovmentActivityEntity.getActivityId();
            updateActivityDate(uc, movementActivity.getPlannedStartDate(), null, activityId);


            //update partner IN movement and activity

                String hql = "FROM ExternalMovementActivityEntity ma where ma.groupid = '" + existEntity.getGroupId() + "' and ma.movementId != " + existEntity.getMovementId();
                Query query2 = session.createQuery(hql);
                @SuppressWarnings("rawtypes") List results = query2.list();
                if (null != results && !results.isEmpty()) {
                    ExternalMovementActivityEntity inEntity = (ExternalMovementActivityEntity) results.get(0);

                    inEntity.setMovementReason(movementActivity.getMovementReason());
                    inEntity.setApplicationDate(movementActivity.getApplicationDate());
                    inEntity.setEscapeId(movementActivity.getEscapeId());
                    inEntity.setTransportation(movementActivity.getTransportation());
                    inEntity.setEscortDetails(movementActivity.getEscortDetails());

                    if (!BeanHelper.isEmpty(updatingMovmentActivityEntity.getCommentText())) {

                        Set<MovementActivityCommentEntity> commentEntities = updatingMovmentActivityEntity.getCommentText();
                        for(MovementActivityCommentEntity commentEntity : commentEntities){
                            commentEntity.setFromIdentifier(inEntity.getMovementId());
                        }

                        updateComments(inEntity, updatingMovmentActivityEntity);
                    } else {
                        updatingMovmentActivityEntity.setCommentText(new HashSet<MovementActivityCommentEntity>());
                        updateComments(inEntity, updatingMovmentActivityEntity);

                    }

                    inEntity.setApprovedByStaffId(movementActivity.getApprovedByStaffId());
                    inEntity.setApprovalDate(movementActivity.getApprovalDate());
                    inEntity.setToFacilityId(movementActivity.getFromFacilityId());

                    inEntity.setFromFacilityId(movementActivity.getToFacilityId());
                    inEntity.setFromLocationId(movementActivity.getToLocationId());
                    inEntity.setFromOrganizationId(movementActivity.getToOrganizationId());
                    session.merge(inEntity);
                    updateActivityDate(uc, movementActivity.getPlannedEndDate(), null, inEntity.getActivityId());

                }

        }catch(StaleObjectStateException ex){
            throw new ArbutusOptimisticLockException(ex);

        }


    }



    @Override
    public List<OffenderScheduleTransferType>   search( UserContext uc, MovementActivitySearchType searchType) {

        //only return OUT movement for PENDing movement search
        if(MovementServiceBean.MovementStatus.PENDING.isEquals(searchType.getMovementStatus())){
            searchType.setFilterInOut(true);

        }
        else
        {
            searchType.setFilterInOut(false);
        }

        List<ExternalMovementActivityType> movements=  movementService.search(uc, searchType.getMovementIds(),searchType, 0L, 200L, null).getExternalMovementActivity();

        //Convert to Web form Object
        List<OffenderScheduleTransferType> scheduleList = new ArrayList<OffenderScheduleTransferType>();
        for(ExternalMovementActivityType tap :  movements) {
            OffenderScheduleTransferType offender = new OffenderScheduleTransferType();
            offender.setActivityId(tap.getActivityId());
            offender.setMovementId(tap.getMovementId());
            offender.setGroupId(tap.getGroupId());
            offender.setMovementDirection(tap.getMovementDirection());
            offender.setbAdhoc(tap.isbAdhoc());

            offender.setScheduledTransferDate(tap.getPlannedStartDate());
            offender.setScheduledTransferStartTime(tap.getPlannedStartDate());
            offender.setScheduledTransferTime(timeFormat.format(tap.getPlannedStartDate()));

            offender.setReleaseDate(tap.getPlannedStartDate());
            offender.setReleaseTime(tap.getPlannedStartDate());

            offender.setReturnDate(tap.getPlannedEndDate());
            offender.setReturnTime(tap.getPlannedEndDate());

            offender.setApplicationDate(tap.getApplicationDate());


            offender.setMovementDate(tap.getMovementDate());

            offender.setMovementType(tap.getMovementType());
            offender.setMovementCategory(tap.getMovementCategory());

            offender.setFromFacility(tap.getFromFacilityId());
            offender.setToFacility(tap.getToFacilityId());

            offender.setFromLocation(tap.getFromLocationId());
            offender.setToLocation(tap.getToLocationId());

            offender.setToOrganization(tap.getToOrganizationId());
            offender.setFromOrganization(tap.getToOrganizationId());


            if (null != tap.getToFacilityId()) {
                Facility facilityType = FacilityServiceAdapter.getFacility(uc, tap.getToFacilityId());
                if (null != facilityType) {
                    offender.setToFacilityName(facilityType.getFacilityName());
                }
            }

            if (null != tap.getFromFacilityId()) {
                Facility facilityType = FacilityServiceAdapter.getFacility(uc, tap.getFromFacilityId());
                if (null != facilityType) {
                    offender.setFromFacilityName(facilityType.getFacilityName());
                }
            }



            offender.setMovementReason(tap.getMovementReason());
            offender.setCancelReasoncode(tap.getCancelReason());

            offender.setMovementStatus(tap.getMovementStatus());

            offender.setApproverId(tap.getApprovedByStaffId());
            offender.setApprovalDate(tap.getApprovalDate());
            offender.setApprovalcomments(tap.getApprovalComments());
            offender.setTransportation(tap.getTransportation());

            offender.setMovementOutcome(tap.getMovementOutcome());


            offender.setSupervisionId(tap.getSupervisionId());
            String strCurrentLocation = SupervisionServiceAdapter.getOffenderCurrentLocationBySupervisionId(uc, tap.getSupervisionId());
            offender.setCurrentLocation(strCurrentLocation);

            SupervisionType supervisionType = SupervisionServiceAdapter.get(uc,tap.getSupervisionId());
            offender.setSupervisionDisplayId(supervisionType.getSupervisionDisplayID());
            offender.setInmatePersonId(supervisionType.getPersonId());
            offender.setInmateCurrentFacility(supervisionType.getFacilityId());



            Long personIdentityId = supervisionType.getPersonIdentityId();
            PersonIdentityType pi = PersonServiceAdapter.getPersonIdentityType(uc, personIdentityId, null);
            offender.setFirstName(pi.getFirstName());
            offender.setLastName(pi.getLastName());
            offender.setOffenderNumber(pi.getOffenderNumber());
            offender.setPersonIdentifierId(pi.getPersonIdentityId());



            Set<CommentType> commentTypes = tap.getCommentText();
            if (null != commentTypes && commentTypes.size() > 0) {
                for (CommentType c : commentTypes) {
                    offender.setComment(c.getComment());
                }
            }


            offender.setEscortOrgId(tap.getEscortOrganizationId());
            offender.setEscortDetals(tap.getEscortDetails());
            if (offender.getEscortOrgId() != null) {
                Long orgId = offender.getEscortOrgId();
                OrganizationEntity orgEntity = (OrganizationEntity) session.get(OrganizationEntity.class, orgId);
                if (orgEntity != null) {
                    offender.setEscortOrgName(orgEntity.getOrganizationName());
                }
            }



            scheduleList.add(offender);


        }

        return scheduleList;

    }



    public static  MovementActivityType toMovementActivityType(TwoMovementsType mvmnt, Boolean adhoc) {

        if (mvmnt instanceof TemporaryAbsenceType) {

            TemporaryAbsenceType temporaryAbsenceType = (TemporaryAbsenceType) mvmnt;

            if (temporaryAbsenceType.getToFacilityLocationId() == null && temporaryAbsenceType.getToOrganizationLocationId() == null &&
                    temporaryAbsenceType.getToOffenderLocationId() == null) {
                throw new InvalidInputException("ToFacilityLocationId or ToOrganizationLocationId or ToOffenderLocationId not provided.");
            }
            ValidationHelper.validate(temporaryAbsenceType);

            ExternalMovementActivityType movement = new ExternalMovementActivityType();


            movement.setMovementType(MovementServiceBean.MovementType.TAP.code());
            movement.setApplicationDate(temporaryAbsenceType.getApplicationDate());
            movement.setTransportation(temporaryAbsenceType.getTransportation());
            movement.setEscortDetails(temporaryAbsenceType.getEscortDetails());
            movement.setApprovalComments(temporaryAbsenceType.getApprovalComments());
            movement.setApprovalDate(temporaryAbsenceType.getApprovalDate());
            if (temporaryAbsenceType.getApproverId() != null) {
                movement.setApprovedByStaffId(temporaryAbsenceType.getApproverId());
            }
            if (temporaryAbsenceType.getEscortOrganizationId() != null) {
                movement.setEscortOrganizationId(temporaryAbsenceType.getEscortOrganizationId());
            }


            movement.setCommentText(toCommentTypeSet(mvmnt.getComments()));
            movement.setMovementCategory(MovementServiceBean.MovementCategory.EXTERNAL.code());
            movement.setSupervisionId(mvmnt.getSupervisionId());
            movement.setMovementReason(mvmnt.getMovementReason());
            movement.setMovementOutcome(mvmnt.getMovementOutCome());

            movement.setFromFacilityId(mvmnt.getFromFacilityId());

            if (mvmnt.getToFacilityId() != null) {
                movement.setToFacilityId(mvmnt.getToFacilityId());
            }



            if (temporaryAbsenceType.getToOffenderLocationId() != null) {
                movement.setToLocationId(temporaryAbsenceType.getToOffenderLocationId());
            }
            if (temporaryAbsenceType.getToFacilityLocationId() != null) {
                movement.setToLocationId(temporaryAbsenceType.getToFacilityLocationId());
            }else if (temporaryAbsenceType.getToOrganizationLocationId() != null) {
                movement.setToLocationId(temporaryAbsenceType.getToOrganizationLocationId());
            }


            if (temporaryAbsenceType.getToOrganizationId() != null){
                movement.setToOrganizationId(temporaryAbsenceType.getToOrganizationId());
            }

            if(adhoc){
                movement.setMovementDate(mvmnt.getMoveDate());
                movement.setPlannedStartDate(mvmnt.getMoveDate());
                movement.setPlannedEndDate(mvmnt.getReturnDate());


            }else
            {
                movement.setMovementDate(null);
                movement.setPlannedStartDate(mvmnt.getMoveDate());
                movement.setPlannedEndDate(mvmnt.getReturnDate());

            }

            return  movement;



        }else
        {
            //Court no long hanlder here
            return  null;

        }


    }


    private static Set<CommentType> toCommentTypeSet(String text) {
        Set<CommentType> comments = null;
        if (text != null) {
            comments = new HashSet<CommentType>();
            CommentType comment = new CommentType();
            comment.setComment(text);
            comment.setCommentDate(BeanHelper.createDate());
            comments.add(comment);
        }
        return comments;
    }

}
