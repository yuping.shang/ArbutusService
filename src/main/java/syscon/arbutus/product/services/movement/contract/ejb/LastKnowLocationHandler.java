package syscon.arbutus.product.services.movement.contract.ejb;

import javax.ejb.SessionContext;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.movement.contract.dto.LastKnowLocationGridEntryType;
import syscon.arbutus.product.services.movement.contract.dto.LastKnowLocationGridReturnType;
import syscon.arbutus.product.services.movement.contract.dto.LastKnowLocationSearchType;
import syscon.arbutus.product.services.movement.contract.dto.LastKnowLocationType;
import syscon.arbutus.product.services.movement.realization.persistence.LastKnowLocationDataGridViewEntity;
import syscon.arbutus.product.services.movement.realization.persistence.LastKnowLocationEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionSearchType;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * Created by dev on 5/21/14.
 */
public class LastKnowLocationHandler {
    private static Logger log = LoggerFactory.getLogger(LastKnowLocationHandler.class);
    private Session session;
    private SessionContext context;

    public LastKnowLocationHandler() {
    }

    public LastKnowLocationHandler(Session session, SessionContext context) {
        this.session = session;
        this.context = context;
    }

    public List<LastKnowLocationGridEntryType> createLastKnowLocation(UserContext uc, List<LastKnowLocationType> lastKnowLocationList) {
        List<LastKnowLocationGridEntryType> result = new ArrayList<LastKnowLocationGridEntryType>();
        LastKnowLocationSearchType searchType = new LastKnowLocationSearchType();
        searchType.setActive(true);

        SupervisionSearchType supervisionSearchType = new SupervisionSearchType();

        for (LastKnowLocationType location : lastKnowLocationList) {
            ValidationHelper.validate(location);
            //            supervisionSearchType.setFacilityId(location.getFacilityId());
            supervisionSearchType.setSupervisionDisplayID(location.getSupervisionDisplayId());
            supervisionSearchType.setFacilityId(location.getFacilityId());
            supervisionSearchType.setSupervisionStatusFlag(true);
            List<SupervisionType> supervisionTypeList = MovementHelper.searchSupervision(uc, supervisionSearchType);
            if (supervisionTypeList == null || supervisionTypeList.isEmpty()) {
                log.error("Failed to create Last Known Location, due to invalid supervision Display Id: " + location.getSupervisionDisplayId());
                return null;
            }

            searchType.setFacilityId(location.getFacilityId());
            searchType.setSupervisionId(supervisionTypeList.get(0).getSupervisionIdentification());
            location.setSupervisionId(supervisionTypeList.get(0).getSupervisionIdentification());
            List<LastKnowLocationType> previousLocations = searchLastKnowLocation(uc, searchType);
            if (previousLocations.size() > 0) {
                for (LastKnowLocationType type : previousLocations) {
                    updateLastKnowLocation(uc, type);

                }
            }
            LastKnowLocationEntity entity = toLasKnowtLocationEntity(location);
            entity.setActive(true);
            session.save(entity);
            session.flush();
            location.setRecordId(entity.getRecordID());
            LastKnowLocationDataGridViewEntity v = (LastKnowLocationDataGridViewEntity) session.get(LastKnowLocationDataGridViewEntity.class,
                    location.getSupervisionDisplayId());
            result.add(new LastKnowLocationGridEntryType(v.getFirstName(), v.getLastName(), v.getOffenderNumber(), v.getGender(), v.getDateOfBirth(), v.getPersonId(),
                    v.getSupervisionId(), v.getFacilityId(), v.getSupervisionId()));
        }

        return result;
    }

    private void updateLastKnowLocation(UserContext uc, LastKnowLocationType type) {
        Long id = type.getRecordId();
        LastKnowLocationEntity entity = (LastKnowLocationEntity) session.get(LastKnowLocationEntity.class, id);
        entity.setActive(false);
        session.update(entity);
    }

    public List<LastKnowLocationType> searchLastKnowLocation(UserContext uc, LastKnowLocationSearchType searchType) {
        List<LastKnowLocationType> result = new ArrayList<LastKnowLocationType>();
        Criteria c = session.createCriteria(LastKnowLocationEntity.class);
        if (!BeanHelper.isEmpty(searchType.getFacilityId())) {
            c.add(Restrictions.eq("facilityId", searchType.getFacilityId()));
        }
        if (!BeanHelper.isEmpty(searchType.getInternalLocationId())) {
            c.add(Restrictions.eq("internalLocationId", searchType.getInternalLocationId()));
        }

        if (!BeanHelper.isEmpty(searchType.getSupervisionId())) {
            c.add(Restrictions.eq("supervisionId", searchType.getSupervisionId()));
        }

        if (!BeanHelper.isEmpty(searchType.getActive())) {
            c.add(Restrictions.eq("active", searchType.getActive()));
        }

        List<LastKnowLocationEntity> entities = c.list();
        for (LastKnowLocationEntity entity : entities) {
            result.add(toLasKnowtLocationType(entity));
        }

        return result;
    }

    public LastKnowLocationEntity toLasKnowtLocationEntity(LastKnowLocationType location) {
        LastKnowLocationEntity entity = new LastKnowLocationEntity();
        entity.setFacilityId(location.getFacilityId());
        entity.setInternalLocationId(location.getInternalLocationId());
        entity.setSupervisionId(location.getSupervisionId());
        entity.setStamp(location.getStamp());
        entity.setVersion(location.getVersion());
        return entity;

    }

    private LastKnowLocationType toLasKnowtLocationType(LastKnowLocationEntity entity) {
        LastKnowLocationType type = new LastKnowLocationType();
        type.setRecordId(entity.getRecordID());
        type.setFacilityId(entity.getFacilityId());
        type.setInternalLocationId(entity.getInternalLocationId());
        type.setSupervisionId(entity.getSupervisionId());
        type.setIsLatestLocation(entity.getActive());
        type.setStamp(entity.getStamp());
        type.setVersion(entity.getVersion());
        return type;
    }

    public LastKnowLocationGridReturnType searchLastKnowLocationGrid(UserContext uc, LastKnowLocationSearchType search, Long startIndex, Long resultSize,
            String resultOrder, String strSearchWords) {

        if (log.isDebugEnabled()) {
            log.debug("searchLastKnowLocationGrid: begin");
        }

        ValidationHelper.validateSearchType(search);

        LastKnowLocationGridReturnType ret = new LastKnowLocationGridReturnType();

        if (null != strSearchWords && !strSearchWords.trim().isEmpty()) {

            String searchKeyWords;
            List<LastKnowLocationDataGridViewEntity> result = new ArrayList<LastKnowLocationDataGridViewEntity>();
            searchKeyWords = strSearchWords.trim().toUpperCase();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            // Get Query Builder

            QueryBuilder qb = fullTextSession.getSearchFactory().buildQueryBuilder().forEntity(LastKnowLocationDataGridViewEntity.class).get();
            org.apache.lucene.search.Query luceneQuery = qb.bool().should(
                    qb.keyword().onFields("firstName", "lastName", "offenderNumber", "gender").matching(searchKeyWords).createQuery()).should(
                    qb.keyword().onField("dateOfBirth").ignoreAnalyzer().matching(searchKeyWords).createQuery()).createQuery();

            org.hibernate.search.FullTextQuery query = fullTextSession.createFullTextQuery(luceneQuery, LastKnowLocationDataGridViewEntity.class);

            query.enableFullTextFilter("lastKnowFacility").setParameter("facilityId", search.getFacilityId());
            query.enableFullTextFilter("lastKnowLocation").setParameter("internalLocationId", search.getInternalLocationId());

            //Pagination
            if (startIndex != null && resultSize != null && startIndex >= 0L && resultSize > 0L) {
                query.setFirstResult(startIndex.intValue());
                query.setMaxResults(resultSize.intValue());
            }

            //Sorting
            if (resultOrder != null && resultOrder.trim().length() > 0) {

                List<Order> orders = BeanHelper.toOrderCriteria(resultOrder);
                List<org.apache.lucene.search.SortField> fields = new ArrayList<org.apache.lucene.search.SortField>();

                for (Order order : orders) {
                    String[] strArray = order.toString().split(" ");
                    fields.add(new SortField(strArray[0], SortField.STRING, strArray[1].equalsIgnoreCase("ASC") ? false : true));

                }
                org.apache.lucene.search.SortField[] sortFields = fields.toArray(new org.apache.lucene.search.SortField[fields.size()]);
                org.apache.lucene.search.Sort sort = new Sort(sortFields);
                query.setSort(sort);
            }

            List<LastKnowLocationGridEntryType> tmp = new ArrayList<LastKnowLocationGridEntryType>();
            result = query.list();

            for (LastKnowLocationDataGridViewEntity v : result) {

                tmp.add(new LastKnowLocationGridEntryType(v.getFirstName(), v.getLastName(), v.getOffenderNumber(), v.getGender(), v.getDateOfBirth(), v.getPersonId(),
                        v.getSupervisionId(), v.getFacilityId(), v.getSupervisionId()));

            }
            ret.setInmateList(tmp);
            ret.setTotalSize(new Long(query.getResultSize()));

        } else {

            StringBuilder hqlTotal = new StringBuilder();
            hqlTotal.append("select count(*) from LastKnowLocationDataGridViewEntity wa ");
            hqlTotal.append("WHERE wa.facilityId = :facilityId ");
            hqlTotal.append("AND wa.internalLocationId = :locationId ");
            Query qryTotal = session.createQuery(hqlTotal.toString());
            qryTotal.setParameter("facilityId", search.getFacilityId());
            qryTotal.setParameter("locationId", search.getInternalLocationId());

            Long totalSize = (Long) qryTotal.uniqueResult();

            StringBuilder hqlFormat = new StringBuilder();

            hqlFormat.append(
                    "SELECT new %s(wa.firstName, wa.lastName,wa.offenderNumber,wa.gender,wa.dateOfBirth,wa.personId,wa.supervisionId,wa.facilityId,wa.internalLocationId) ");
            hqlFormat.append("FROM LastKnowLocationDataGridViewEntity wa ");
            hqlFormat.append("WHERE wa.facilityId = :facilityId ");
            hqlFormat.append("AND wa.internalLocationId = :locationId ");

            //Result order
            if (resultOrder != null && resultOrder.trim().length() > 0) {
                hqlFormat.append("ORDER BY ");
                List<Order> orders = BeanHelper.toOrderCriteria(resultOrder);
                for (Order order : orders) {
                    hqlFormat.append(order.toString());
                    hqlFormat.append(",");

                }
                hqlFormat.deleteCharAt(hqlFormat.length() - 1);
            }

            String sql = String.format(hqlFormat.toString(), LastKnowLocationGridEntryType.class.getName());

            Query query = session.createQuery(sql);
            query.setParameter("facilityId", search.getFacilityId());
            query.setParameter("locationId", search.getInternalLocationId());

            //Pagination
            if (startIndex != null && resultSize != null && startIndex >= 0L && resultSize > 0L) {
                query.setFirstResult(startIndex.intValue());
                query.setMaxResults(resultSize.intValue());
            }
            List<LastKnowLocationGridEntryType> result = query.list();
            ret.setInmateList(result);
            ret.setTotalSize(totalSize);
        }

        return ret;
    }

    public void indexDataBaseLastKnowLocation() {
        FullTextSession fullTextSession = Search.getFullTextSession(session);
        fullTextSession.createIndexer(LastKnowLocationDataGridViewEntity.class).start();
        fullTextSession.flushToIndexes();
    }
}
