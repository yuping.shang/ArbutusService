package syscon.arbutus.product.services.movement.contract.ejb;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.StaleObjectStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.core.common.adapters.FacilityServiceAdapter;
import syscon.arbutus.product.services.core.common.adapters.PersonServiceAdapter;
import syscon.arbutus.product.services.core.common.adapters.SupervisionServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.ArbutusOptimisticLockException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.core.util.DateUtil;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.movement.contract.dto.ExternalMovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivitySearchType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.OffenderScheduleTransferType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementServiceLocal;
import syscon.arbutus.product.services.movement.realization.persistence.ExternalMovementActivityEntity;
import syscon.arbutus.product.services.movement.realization.persistence.InternalMovementActivityEntity;
import syscon.arbutus.product.services.movement.realization.persistence.MovementActivityCommentEntity;
import syscon.arbutus.product.services.movement.realization.persistence.MovementActivityEntity;
import syscon.arbutus.product.services.organization.realization.persistence.OrganizationEntity;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * Created by hshen on 1/21/16.
 */
public class NoMovementHandler extends   DefaultMovementHandler {


    private static Logger log = LoggerFactory.getLogger(NoMovementHandler.class);

    public NoMovementHandler(Session session, MovementServiceLocal movementService, ActivityService activityService) {
        super(session, movementService, activityService);
    }

    @Override
    public Long create(UserContext uc, MovementActivityType movementActivity, Long facilityId) {

        ActivityType activity = createScheduleActivity(uc, movementActivity.getPlannedStartDate(), facilityId, movementActivity, false,
                movementActivity.getPlannedEndDate());
        movementActivity.setMovementDate(null);
        movementActivity.setMovementCategory(MovementServiceBean.MovementCategory.NOMOV.code());
        movementActivity.setActivityId(activity.getActivityIdentification());
        movementActivity.setMovementStatus(MovementServiceBean.MovementStatus.PENDING.code());
        return movementService.create(uc, movementActivity, false).getMovementId();

    }

    @Override
    public Long createAdhoc(UserContext uc, MovementActivityType movementActivity, Long facilityId) {

        Long activityId = createActivity(uc, movementActivity.getMovementDate(), movementActivity.getSupervisionId(), Boolean.FALSE, movementActivity.getPlannedEndDate());

        movementActivity.setMovementCategory(MovementServiceBean.MovementCategory.NOMOV.code());
        movementActivity.setActivityId(activityId);
        movementActivity.setMovementStatus(MovementServiceBean.MovementStatus.COMPLETED.code());
        return  movementService.create(uc, movementActivity, false).getMovementId();

    }

    @Override
    public void update(UserContext uc, MovementActivityType movementActivity) {

        String functionName=this.getClass().getName() + "update";


        Long id = movementActivity.getMovementId();
        MovementActivityEntity existEntity = BeanHelper.getEntity(session, MovementActivityEntity.class, id);
        String category = existEntity.getMovementCategory();


        MovementActivityEntity updatingMovmentActivityEntity;

        if (movementActivity instanceof ExternalMovementActivityType) {
            updatingMovmentActivityEntity = MovementHelper.toMovementActivityEntity(movementActivity, ExternalMovementActivityEntity.class);
        } else {
            updatingMovmentActivityEntity = MovementHelper.toMovementActivityEntity(movementActivity, InternalMovementActivityEntity.class);
        }

        ValidationHelper.verifyMetaCodes(updatingMovmentActivityEntity, false);


        updateMoveActivityStatus(functionName, existEntity, updatingMovmentActivityEntity.getMovementStatus());



        //Copy properties to update
        existEntity.setApprovalComments(updatingMovmentActivityEntity.getApprovalComments());
        existEntity.setApprovalDate(updatingMovmentActivityEntity.getApprovalDate());
        existEntity.setApprovedByStaffId(updatingMovmentActivityEntity.getApprovedByStaffId());
        existEntity.setMovementReason(updatingMovmentActivityEntity.getMovementReason());
        existEntity.setMovementOutcome(updatingMovmentActivityEntity.getMovementOutcome());
        existEntity.setParticipateStaffId(updatingMovmentActivityEntity.getParticipateStaffId());


        if (!BeanHelper.isEmpty(updatingMovmentActivityEntity.getCommentText())) {
            updateComments(existEntity, updatingMovmentActivityEntity);
        } else {
            updatingMovmentActivityEntity.setCommentText(new HashSet<MovementActivityCommentEntity>());
            updateComments(existEntity, updatingMovmentActivityEntity);

        }

        if (movementActivity instanceof ExternalMovementActivityType) {

            updateExternalMovementProps(existEntity, updatingMovmentActivityEntity);

        }else {
            updateInternalMovementProps(existEntity, updatingMovmentActivityEntity);

        }


        try {

            existEntity.setMovementCategory(category);
            session.merge(existEntity);

            //updateActivity
            Long activityId =  updatingMovmentActivityEntity.getActivityId();
            updateActivityDate(uc, movementActivity.getPlannedStartDate(), movementActivity.getPlannedEndDate(), activityId);

        }catch(StaleObjectStateException ex){
            throw new ArbutusOptimisticLockException(ex);

        }

    }


    @Override
    public List<OffenderScheduleTransferType>   search(UserContext uc, MovementActivitySearchType searchType) {


        List<ExternalMovementActivityType> movements=  movementService.search(uc, searchType, 0L, 200L, null).getExternalMovementActivity();

        //Convert to Web form Object
        List<OffenderScheduleTransferType> scheduleList = new ArrayList<OffenderScheduleTransferType>();
        for(ExternalMovementActivityType movement :  movements) {
            OffenderScheduleTransferType offender = new OffenderScheduleTransferType();

            offender.setActivityId(movement.getActivityId());
            offender.setMovementId(movement.getMovementId());
            offender.setMovementDirection(movement.getMovementDirection());
            offender.setbAdhoc(movement.isbAdhoc());

            offender.setMovementDate(movement.getMovementDate());
            offender.setScheduledTransferDate(movement.getPlannedStartDate());
            offender.setScheduledTransferStartTime(movement.getPlannedStartDate());
            offender.setScheduledTransferTime(timeFormat.format(movement.getPlannedStartDate()));

            offender.setReleaseDate(movement.getPlannedStartDate());
            offender.setReleaseTime(movement.getPlannedStartDate());

            offender.setReturnDate(movement.getPlannedEndDate());
            offender.setReturnTime(movement.getPlannedEndDate());

            offender.setMovementType(movement.getMovementType());
            offender.setMovementCategory(movement.getMovementCategory());

            offender.setFromFacility(movement.getFromFacilityId());



            if (null != movement.getFromFacilityId()) {
                Facility facilityType = FacilityServiceAdapter.getFacility(uc, movement.getFromFacilityId());
                if (null != facilityType) {
                    offender.setFromFacilityName(facilityType.getFacilityName());
                }
            }




            offender.setMovementReason(movement.getMovementReason());
            offender.setCancelReasoncode(movement.getCancelReason());

            offender.setMovementStatus(movement.getMovementStatus());
            offender.setParticipateOfficerId(movement.getParticipateStaffId());


            offender.setSupervisionId(movement.getSupervisionId());
            String strCurrentLocation = SupervisionServiceAdapter.getOffenderCurrentLocationBySupervisionId(uc, movement.getSupervisionId());
            offender.setCurrentLocation(strCurrentLocation);

            SupervisionType supervisionType = SupervisionServiceAdapter.get(uc,movement.getSupervisionId());
            offender.setSupervisionDisplayId(supervisionType.getSupervisionDisplayID());
            offender.setInmatePersonId(supervisionType.getPersonId());
            offender.setInmateCurrentFacility(supervisionType.getFacilityId());



            Long personIdentityId = supervisionType.getPersonIdentityId();
            PersonIdentityType pi = PersonServiceAdapter.getPersonIdentityType(uc, personIdentityId, null);
            offender.setFirstName(pi.getFirstName());
            offender.setLastName(pi.getLastName());
            offender.setOffenderNumber(pi.getOffenderNumber());
            offender.setPersonIdentifierId(pi.getPersonIdentityId());


            Set<CommentType> commentTypes = movement.getCommentText();
            if (null != commentTypes && commentTypes.size() > 0) {
                for (CommentType c : commentTypes) {
                    offender.setComment(c.getComment());
                }
            }

            scheduleList.add(offender);


        }

        return scheduleList;

    }



}
