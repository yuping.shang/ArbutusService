package syscon.arbutus.product.services.movement.contract.interfaces;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.housing.realization.dto.housingassignment.*;
import syscon.arbutus.product.services.location.contract.dto.Location;
import syscon.arbutus.product.services.movement.contract.dto.*;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivity;

/**
 * Interface of MovementActivity Service
 * <p>To uniquely-identify a single or set of related actions, events or process steps, for all movements of incarcerated inmates, both external and internal:
 * <p>External movements include all events in which inmates are moved in and out of a custodial facility: admission (reception), release (discharge), transfers from one custodial facility to another, temporary absences (court movements, hospital visits, compassionate grounds Ã¢â‚¬â€œ escorted or unescorted), escapes and recaptures.
 * <p>Internal movements include all events in which inmates move to and from their living units to other security-controlled areas within the custodial facility: (food service, internal work assignments, and visiting areas).
 * <p><p>
 * Movement Activity is an augmentation for an activity. The service deals with two types of movements: Internal and External.
 * <p>Internal Movement: Movement of an offender from one part of the custodial facility to another:
 * <p>Internal Appointment (APP),
 * Visitation (VISIT),
 * Offence in Custody (OIC/DISC),
 * Program (PROGRAM),
 * Bed assignment (BED),
 * Medical (MED),
 * Court Appearance (CRT),
 * Classification (CLA).
 * <p>External Movement: Movement of an offender to and from a Custodial Facility and within the community:
 * <p>Admission / Return to Custody (ADM),
 * Court Appearance (CRT), Transfer (TRN),
 * Temporary Absence (TAP),
 * Work Release (WR),
 * Release (REL),
 * Escapees: Will be modelled as a Release (REL),
 * Recapture: Will be modelled as Admission (ADM),
 * Death: Will be modelled as a Release (REL),
 * Medical (MED),
 * Intermittent: Modelled as a Release (REL) and then an Admission (ADM).
 * <p><b>JNDI Lookup Name:</b> MovementService
 * <p><b>Example of invocation of the service:</b>
 * <pre>
 * 	Properties p = new Properties();
 * 	properties.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
 * 	properties.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
 * 	properties.put("java.naming.provider.url", hostAddress);  // hostAddress -- specified accordingly
 * 	Context ctx = new InitialContext(p);
 * 	MovementService service = (MovementService)ctx.lookup("MovementService");
 * 	SecurityClient client = SecurityClientFactory.getSecurityClient();
 * 	client.setSimple(userId, password);
 * 	client.login();
 * 	UserContext uc = new UserContext();
 * 	uc.setConsumingApplicationId(clientAppId);
 * 	uc.setFacilityId(facilityId);
 * 	uc.setDeviceId(deviceId);
 * 	uc.setProcess(process);
 * 	uc.setTask(task);
 * 	uc.setTimestamp(timestamp);
 * 	MovementActivityReturnType orgRet = service.create(uc, ...);
 * 	Long count = service.getCount(uc);
 * 	... ...
 * </pre>
 * <img src="doc-files/MovementActivityUML.PNG" /> <br/>
 *
 * @author yshang
 * @version 2.0
 * @since November 9, 2012.
 */

public interface MovementServiceCommon  extends LastKnowLocation, MovementManagement, MovementDetails, Serializable {


    
    String combineAddress(final Location location);

    /**
     * create -- Create a Movement Activity.
     * <p>Movement Activity must have an association to ACTIVITY Additional Information:
     * The corresponding Activity must always exist before the Movement Activity is created.
     * <p>If MovementStatus == COMPLETE then
     * MovementDate and FromInternalLocation is mandatory
     * <p>If RequiresApproval == TRUE then Set MovementStatus to APPREQ Else Set MovementStatus to PENDING.
     * <p>In case of InternalMovement, if MovementStatus == PENDING then FromInternalLocation is optional Else FromInternalLocation is mandatory
     *
     * @param uc               UserContext - Required
     * @param movementActivity either ExternalMovementActivityType or
     *                         InternalMovementActivityType - Required
     * @param requiresApproval Boolean - Required
     * @return MovementActivityType An MovementActivity (External or Internal) instance
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    <T extends MovementActivityType> MovementActivityType create(UserContext uc, T movementActivity, Boolean requiresApproval);

    /**
     * delete -- Delete an existing Movement by Id from the system.
     * <p>This will physically delete the movement from the system.
     *
     * @param uc UserContext - Required
     * @param id Long - Required. Movement Activity Identifier
     * @return Long Return success code if all instances, associations and reference data were deleted.
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    Long delete(UserContext uc, Long id);

    /**
     * deleteAll -- Delete all Movement Activities, Associations and
     * Reference Data.
     *
     * @param uc UserContext - Required
     * @return java.lang.Long Return success code if all instances, references and reference data were deleted.
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    Long deleteAll(UserContext uc);
    
    /**
     * update -- update a bulk Movement Activity
     * <pre>
     * 	<li>Movement Activity must be exist to be updated;</li>
     *  <li>The association to ACTIVITY cannot be updated.</li>
     * </pre>
     * @param uc UserContext - Required
     * @param movementActivityList - List of MovementActivityType ExternalMovementActivityType/InternalMovementActivityType is updated
     * @return
     */
    public <T extends MovementActivityType> boolean update(UserContext uc, List<T> movementActivityList);

    /**
     * update -- Update a Movement Activity.
     * <pre>
     * 	<li>Movement Activity must be exist to be updated;</li>
     *  <li>The association to ACTIVITY cannot be updated.</li>
     * </pre>
     *
     * @param uc               UserContext - Required
     * @param movementActivity MovementActivityType - Required. ExternalMovementActivityType/InternalMovementActivityType is updated
     *                         when type of argument is ExternalMovementActivityType/InternalMovementActivityType.
     * @return MovementActivityType An MovementActivity (External or Internal) instance
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    <T extends MovementActivityType> MovementActivityType update(UserContext uc, T movementActivity);

    /**
     * get -- Get/Retrieve a Movement Activity by Id.
     *
     * @param uc UserContext - Required
     * @param id java.lang.Long -Required
     * @return MovementActivityType An MovementActivity(External or Internal) instance
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    MovementActivityType get(UserContext uc, Long id);

    /**
     * getExternalMovementActivity -- Get/Retrieve an External Movement Activity by Id.
     *
     * @param uc UserContext
     * @param id java.lang.Long
     * @return ExternalMovementActivityType
     */
    ExternalMovementActivityType getExternalMovementActivity(UserContext uc, Long id);

    /**
     * getAll -- Get/Retrieve a set of Ids of all Movement Activity.
     *
     * @param uc UserContext - Required
     * @return java.util.Set<Long> Returns null if there is an error. Return empty set if there is no instances.
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    Set<Long> getAll(UserContext uc);

    /**
     * getCount -- Get the total number of Movement Activities.
     *
     * @param uc UserContext - Required
     * @return Long count of instances
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    Long getCount(UserContext uc);

    /**
     * search -- Search Movement.
     * <p>Text item/String will support the asterisk as a wildcard (that matches zero or more characters).
     * <p>Text item/String without a wildcard will be matched as an exact string match.
     * <pre>
     * <li>At least one of the search fields must be specified in the search criteria.</li>
     * </pre>
     *
     * @param uc                     UserContext - Required
     * @param movementActivitySearch MovementActivitySearchType - Required
     * @return MovementActivitiesType MovementActivity (External or Internal) instances, totolSize
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    MovementActivitiesReturnType search(UserContext uc, MovementActivitySearchType movementActivitySearch, Long startIndex, Long resultSize, String resultOrder);

    /**
     * search --Search Movement Activities.
     * <pre>
     *   <li>Search for a set of Movement Activities based on a specified criteria MovementActivitySearchType.</li>
     *   <li>The subsetSearch can be used to search only within the specified subset if Id's.</li>
     * </pre>
     * <p>Text item/String will support the asterisk as a wildcard (that matches zero or more characters).
     * <p>Text item/String without a wildcard will be matched as an exact string match.
     *
     * @param uc                     UserContext - Required
     * @param ids                    java.util.Set<Long> a set of Ids of movement activities. Optional
     * @param movementActivitySearch MovementActivitySearchType - Required
     * @return MovementActivitiesType MovementActivity (External or Internal) instances, totalSize
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    MovementActivitiesReturnType search(UserContext uc, Set<Long> ids, MovementActivitySearchType movementActivitySearch, Long startIndex, Long resultSize,
            String resultOrder);

    /**
     * Get/Retrieve the last completed movement for offenders based on a set of supervisions
     * <p>All current locations are returned for the requested offenders.
     *
     * @param uc           UserContext - Required
     * @param supervisions Set&lt;Long> - Required. Ids of Supervision
     * @return Set&lt;LastCompletedSupervisionMovementType>
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    Set<LastCompletedSupervisionMovementType> getLastCompletedSupervisionMovementBySupervisions(UserContext uc, Set<Long> supervisions);

    /**
     * Get/Retrieve the last completed movement for offenders based on a set of locations
     * <p>All current locations are returned for the requested offenders.
     *
     * @param uc                        UserContext - Required
     * @param facilityInternalLocations Set&lt;Long> - Required. Associations to Facility Internal Location.
     * @return Set&lt;LastCompletedSupervisionMovementsType>
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    Set<LastCompletedSupervisionMovementType> getLastCompletedSupervisionMovementByLocations(UserContext uc, Set<Long> facilityInternalLocations);

    /**
     *
     * @param uc
     * @param supervisionId
     * @return
     */
    MovementActivityType getLastCompletedSupervisionMovement(UserContext uc, Long supervisionId);

    /**
     * addTransferWaitlistEntry -- Add one or more entries to the transfer waitlist for a facility.
     * <pre>
     *   <li>One or more Transfer Waitlist entries are created.</li>
     *   <li>For a given facility’s transfer waitlist, an offender can only be listed once within the waitlist.</li>
     *   <li>A transfer waitlist entry does not require an Activity instance.</li>
     * </pre>
     *
     * @param uc                      UserContext - Required
     * @param facilityId              Long - Required. A Facility ID
     * @param transferWaitlistEntries Set&lt;TransferWaitlistEntry

Type> - A set of Transfer Waitlist Entry
     * @return TransferWaitlistType
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    TransferWaitlistType addTransferWaitlistEntry(UserContext uc, Long facilityId, Set<TransferWaitlistEntryType> transferWaitlistEntries);

    /**
     * updateTransferWaitlistEntry -- update one or more entries to the transfer waitlist for a facility.
     * <pre>
     *   <li>One or more Transfer Waitlist entries are updated.</li>
     *   <li>For a given facility’s transfer waitlist, an offender can only be listed once within the waitlist.</li>
     *   <li>A transfer waitlist entry does not require an Activity instance.</li>
     * </pre>
     *
     * @param uc                      UserContext - Required
     * @param facilityId              Long - Required. A Facility ID
     * @param transferWaitlistEntries Set&lt;TransferWaitlistEntryType> A set of Transfer Waitlist Entry
     * @return TransferWaitlistType
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    TransferWaitlistType updateTransferWaitlistEntry(UserContext uc, Long facilityId, Set<TransferWaitlistEntryType> transferWaitlistEntries);

    /**
     * deleteTransferWaitlist -- Remove one or more offenders from an existing transfer waitlist for a facility.
     * <p>All SupervisionAssociations within the transfer waitlist that match any of the SupervisionAssociation(s) provided will be removed.
     *
     * @param uc             UserContext
     * @param facilityId     Long - Required -- Facility Identification
     * @param supervisionIds Set&lt;Long> -- A set of Supervision's Id
     * @return TransferWaitlistType
     */
    void deleteTransferWaitlistEntry(UserContext uc, Long facilityId, Set<Long> supervisionIds);

    /**
     * getTransferWaitlist -- Retrieve all offenders currently on the transfer waitlist.
     * <p>Returns return code 1(success) and 0 transfer waitlist entry if no offender(supervision) is wait for a transfer.
     *
     * @param uc         UserContext - Required
     * @param facilityId Long - Required. Facility Identification
     * @return TransferWaitlistReturnType TransferWaitlist instance if success
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    TransferWaitlistType getTransferWaitlist(UserContext uc, Long facilityId);

    /**
     * getTransferWaitlistEntry -- Retrieves a specific transfer waitlist entry.
     * <p>TransferWaitistEntry instance if success.
     *
     * @param uc            UserContext - Required
     * @param facilityId    Long - Required. Facility Identification
     * @param supervisionId Long -- Supervision Identification
     * @return TransferWaitlistEntryType TransferWaitistEntry instance if success.
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    TransferWaitlistEntryType getTransferWaitlistEntry(UserContext uc, Long facilityId, Long supervisionId);

    /**
     * searchTransferWaitlist -- Search for an offender on the transfer waitlist
     * <p>Search may return 0 or more offenders on the transfer waitlist.
     * <p>Exact or partial searches are allowed in the text fields.
     * <p>At least one of the search fields must be specified in the search criteria.
     * <p>Text item/String will support the asterisk as a wildcard (that matches zero or more characters).
     * <p>Text item/String without a wildcard will be matched as an exact string match.
     *
     * @param uc     UserContext - Required
     * @param search TransferWaitlistSearchType -- Required
     * @return TransferWaitlistReturnType TransferWaitlist instance
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    TransferWaitlistsReturnType searchTransferWaitlist(UserContext uc, TransferWaitlistSearchType search, Long startIndex, Long resultSize, String resultOrder);

    void indexDataBase();

    /**
     * searchTransferWaitlistGrid -- Search for offenders on the transfer waitlist
     * <p>Search may return 0 or more offenders on the transfer waitlist.
     * <p>Exact or partial searches are allowed in the text fields.
     * <p>At least one of the search fields must be specified in the search criteria.
     * <p>Text item/String will support the asterisk as a wildcard (that matches zero or more characters).
     * <p>Text item/String without a wildcard will be matched as an exact string match.
     *
     * @param uc     UserContext - Required
     * @param search TransferWaitlistSearchType -- Required
     * @return TransferWaitlistReturnType TransferWaitlist instance
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    TransferWaitlistGridReturnType searchTransferWaitlistGrid(UserContext uc, TransferWaitlistSearchType search, Long startIndex, Long resultSize,
            String resultOrder, String filer);

    /**
     * deleteAllTransferWaitlist -- Delete all Transfer Waitlist
     *
     * @param uc UserContext - Required
     * @return Long Return success code 1L if all instances of TransferWaitlist are deleted.
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    Long deleteAllTransferWaitlist(UserContext uc);

    /**
     * setCourtMovementReturnTimeConfiguration -- Set a configuration variable to store the default return time for all court movements.
     * <p>Only 1 configuration variable can be active at any time.
     *
     * @param uc            UserContext - Required
     * @param configuration CourtMovementReturnTimeConfigurationType
     * @return CourtMovementReturnTimeConfigurationType
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    CourtMovementReturnTimeConfigurationType setCourtMovementReturnTimeConfiguration(UserContext uc, CourtMovementReturnTimeConfigurationType configuration);

    /**
     * getCourtMovementReturnTimeConfiguration -- Retrieve the configuration variable for defining the default court movement return time
     * <p>Returns null instance and error code if Court Movement Return Time not Configured.
     *
     * @param uc UserContext - Required
     * @return CourtMovementReturnTimeConfigurationType
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    CourtMovementReturnTimeConfigurationType getCourtMovementReturnTimeConfiguration(UserContext uc);

    /**
     * SupervisionClosureConfigurationsReturnType -- Sets configuration variables to store the movement type and reason combinations that would result in the closure of an offender’s supervision period.
     * <p>All existing configuration variables are replaced by the new ones. If there are no existing configuration variables then they are created.
     * <p>One Supervision Closure Configuration Type for every combination of ‘Movement Reason’ and  ‘Movement Type’ must be given.
     *
     * @param uc             UserContext - Required
     * @param configurations Set&lt;SupervisionClosureConfigurationType> - Required
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    void setSupervisionClosureConfiguration(UserContext uc, Set<SupervisionClosureConfigurationType> configurations);

    /**
     * Get Supervision Closure Configuration by MovementType and MovementReason
     *
     * @param uc                UserContext - Required
     * @param movementType      MovementActivityType.MovementType - Required
     * @param movementReason    String - Required
     * @return SupervisionClosureConfigurationType
     */
    SupervisionClosureConfigurationType getSupervisionClosureConfiguration(UserContext uc, MovementActivityType.MovementType movementType, String movementReason);

    /**
     * searchSupervisionClosureConfiguration -- Retrieve the configuration variable(s) for defining the movement type and reason combinations that would result in the closure of an offender’s supervision period.
     * <p>At least one search criteria must be provided.
     * <p>Text item/String will support the asterisk as a wildcard (that matches zero or more characters).
     * <p>Text item/String without a wildcard will be matched as an exact string match.
     *
     * @param uc     UserContext - Required
     * @param search SupervisionClosureConfigurationSearchType - Required
     * @return SupervisionClosureConfigurationsReturnType
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    SupervisionClosureConfigurationsReturnType searchSupervisionClosureConfiguration(UserContext uc, SupervisionClosureConfigurationSearchType search,
            Long startIndex, Long resultSize, String resultOrder);

    /**
     * setTransferWaitlistPriorityConfiguration -- This action updates the configuration values for Transfer Waitlist Priorities.
     * <p>All existing configuration variables are replaced by the new one. If there are no existing configuration vaciables then they are created;
     * <p>One Transfer Waitlist Priority Type has to be given for every transfer priority configured in ‘Transfer Waitlist Priority Set’;
     * <p>There must be at least one priority in the Transfer Waitlist Priority Set.
     *
     * @param uc            UserContext - Required
     * @param configuration Set&lt;TransferWaitlistPriorityConfigurationType> - Required
     * @return TransferWaitlistPrioritiesConfigurationReturnType
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    void setTransferWaitlistPriorityConfiguration(UserContext uc, Set<TransferWaitlistPriorityConfigurationType> configuration);

    /**
     * searchTransferWaitlistPriorityConfiguration -- This action retrieves the configuration values for Transfer Waitlist Priorities.
     * <p>At least one search criteria must be provided.
     * <p>Transfer Waitlist Configuration variables must exist.
     * <p>Text item/String will support the asterisk as a wildcard (that matches zero or more characters).
     * <p>Text item/String without a wildcard will be matched as an exact string match.
     *
     * @param uc     UserContext - Required
     * @param search TransferWaitlistPriorityConfigurationSearchType
     * @return TransferWaitlistPrioritiesConfigurationReturnType
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    TransferWaitlistPrioritiesConfigurationReturnType searchTransferWaitlistPriorityConfiguration(UserContext uc,
            TransferWaitlistPriorityConfigurationSearchType search, Long startIndex, Long resultSize, String resultOrder);

    /**
     * getNiem -- Get NIEM (National Information Exchange Model) of a Movement
     * Activity specified by Id.
     *
     * @param uc UserContext - Required
     * @param id java.lang.Long Id of a Movement Activity
     * @return java.lang.String
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    String getNiem(UserContext uc, Long id);

    /**
     * getStamp -- Get Stamp of a Movement Activity.
     *
     * @param uc UserContext - Required
     * @param id java.lang.Long Id of a Movement Activity
     * @return StampType
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    StampType getStamp(UserContext uc, Long id);

    /**
     * Retrieve the version number in format "MajorVersionNumber.MinorVersionNumber"
     *
     * @param uc UserContext - Required
     * @return version number
     * @throws &lt;<T extends ArbutusRuntimeException> thrown in case of failure.
     */
    String getVersion(UserContext uc);

    /**
     * Retrieves all two movements typed requested schedules, like TemporaryAbsence, CourtMovement (not related to a Case)
     *
     * @param uc         UserContext - required.
     * @param searchType @link{TwoMovementsSearchType} - required
     * @return a list of T extends {@link TwoMovementsType}
     */
    <T extends TwoMovementsType> List<T> listTwoMovementsBy(UserContext uc, TwoMovementsSearchType searchType);

    /**
     * WOR-5984 If the setting is set to 'Always close the period of supervision when releasing an inmate' then the system must close the period of supervision
     *
     * @param uc
     * @return
     */
    boolean getAlwaysCloseSupervisionFlag(UserContext uc);

    List<ScheduleConflict> getExternalMovementBySupervisionId(UserContext uc, Long supervisionId, List<String> types);

    /**
     * Retrieve last completed movement for offenders based on person Identity Id
     *
     * @param uc
     * @param personIdentityId
     * @return
     */
    LastCompletedSupervisionMovementType getLastCompletedSupervisionMovementByPersonIdentityId(UserContext uc, Long personIdentityId);

    /**
     * Retrieve last completed movement for offenders based on person Identity Ids
     *
     * @param uc
     * @param personIdentityIds
     * @return
     */
    Map<Long, LastCompletedSupervisionMovementType> getLastCompletedSupervisionMovementByPersonIdentityIds(UserContext uc, Set<Long> personIdentityIds);

    /**
     * @param uc
     * @param search
     * @return List<OffenderScheduleTransferType>
     */
    List<OffenderScheduleTransferType> getScheduledInternalMovements(UserContext uc, ScheduleTransferMovementSearch search);

    /**
     * @param uc
     * @param search
     * @return List<OffenderScheduleTransferType>
     */
    List<OffenderScheduleTransferType> getScheduledExternalMovements(UserContext uc, ScheduleTransferMovementSearch search);

    void updateScheduleMovementStatus(UserContext uc, List<Long> movementids, String movementStatus);

    List<ConfigMovementDurationType> getConfigMovementDurations(
            UserContext uc,
            ConfigMovementDurationType configMovementDurationType);

    ConfigMovementDurationType setConfigMovementDuration(UserContext uc, ConfigMovementDurationType configMovementDurationType);

    /**
     * save all types of configured movement durations
     *
     * @param uc
     * @param movementDurations
     */
    void setConfigMovementDuration(UserContext uc, List<ConfigMovementDurationType> movementDurations);
    /**
     * Update a scheduled movement . 
     * @param uc
     * @param movementids
     * @param movementStatus
     * @param commentText
     * @param reasonCode
     */
    void updateScheduleMovementStatus(UserContext uc, List<Long> movementids, String movementStatus, String commentText,String reasonCode);

    /**
     *
     * @param uc
     * @param organizationId
     * @return
     */
    boolean isExternalMovementByOrganization(UserContext uc, Long organizationId);

    /**
     * Record an escape and apply logic related to an escape.
     *
     * @param uc
     * @param escapeRecapture
     * @return
     */
    public Long addEscape(UserContext uc, EscapeRecapture escapeRecapture);

    /**
     * Update a escape record
     *
     * @param escape
     */
    public void updateEscape(UserContext uc , EscapeRecapture escape);

    /**
     *
     * @param uc
     * @param supervisionId
     * @return
     */
    public List<EscapeRecapture> getEscapesBySupervisionId(UserContext uc, Long supervisionId);

    /**
     *
     * @param uc
     * @param escapeId
     * @return
     */
    public EscapeRecapture getEscapeDetailsById(UserContext uc, Long escapeId);

    /**
     * get Escape details by Person Identity
     *
     * @param uc
     * @param personIdentityId
     * @return escape recapture details
     */
    public EscapeRecapture getEscapeByPersonIdentity(UserContext uc, Long personIdentityId);

    /**
     *
     * @param uc
     * @param supervisionId
     * @return
     */
    public boolean isOffenderEscaped(UserContext uc, Long supervisionId);

    /**
     * get offender last escaped
     *
     * @param uc
     * @param supervisionId
     * @return
     */
    public EscapeRecapture getOffenderLastEscaped(UserContext uc, Long supervisionId, String lang);

    /**
     * find if inmate escaped
     *
     * true - escaped
     * false - not escaped or recaptured
     *
     * @param uc
     * @param supervisionId
     * @return
     */
    public boolean isInmateEscaped(UserContext uc, Long supervisionId);

    /**
     * To check if an internal location is currently used in movement.
     *
     * @param uc
     * @param locationId
     * @return
     */
    public boolean isInternalLocationCurrentlyUsed(UserContext uc, Long locationId);

    /**
     * To check if an internal location is referenced in movement.
     *
     * @param uc
     * @param locationId
     * @return
     */
    public boolean isInternalLocationUsed(UserContext uc, Long locationId);

    /**
     * Escape recapture meta-data conversion
     *
     * @param uc
     * @param escapeRecaptureList
     * @param language
     * @return
     */
    public List<EscapeRecapture> convertMetaDescriptions(UserContext uc, List<EscapeRecapture> escapeRecaptureList, String language);

    /**
     * Create internalMovement
     * @param uc
     * @param internalMovement
     * @param facilityId
     * @param adhoc
     * @param approveRequired
     */
    public void  createInternalMovement (UserContext uc,InternalMovement internalMovement, Long facilityId, Boolean adhoc , Boolean approveRequired);


    /**
     * Create Transfer movement
     * @param userContext
     * @param userId
     * @param staffId
     * @param movement
     * @param isAdhoc
     */
    public void createTransferMovement(UserContext userContext, Long userId,Long staffId, ExternalMovement movement, Boolean isAdhoc);

    /**
     *
     * @param userContext
     * @param mov
     */
    public void actualizeScheduledInternalMovement(UserContext userContext, OffenderScheduleTransferType mov);

    /**
     *  Create a movement category configuration for deriving a movement category from movement type and reason
     *
     * @param uc
     * @param configuration
     */
    public void createMovementCategoryConfiguration(UserContext uc, MovementCategoryConfiguration configuration);

    /**
     * Update a movement category configuration
     *
     * @param uc
     * @param configuration
     */
    public void updateMovementCategoryConfiguration(UserContext uc, MovementCategoryConfiguration configuration);

    /**
     * Delete a movement category configuration
     *
     * @param uc
     * @param configuration
     */
    public void deleteMovementCategoryConfiguration(UserContext uc, MovementCategoryConfiguration configuration);

    /**
     *  Retrieve a movement category configuration
     *
     * @param uc
     * @param movementType
     * @param movementReason
     * @return
     */
    public MovementCategoryConfiguration getMovementCategoryConfiguration(UserContext uc, String movementType, String movementReason);

    /**
     *
     * @param uc
     * @return
     */
    public List<MovementCategoryConfiguration> getAllCategoryConfigurations(UserContext uc);


    public Long createMovementSchedule(UserContext uc, MovementActivityType movementActivity, Long facilityId);


    public Long createAdhocMovement(UserContext uc, MovementActivityType movementActivity,  Long facilityId);


    public void updateMovementSchedule(UserContext uc, MovementActivityType movementActivity);


    public void completeMovementSchedule(UserContext uc, MovementActivityType  movementActivity);


    public void cancelMovementSchedule(UserContext uc, MovementActivityType  movementActivity);


    public MovementActivityType  getPartenerMovement(UserContext uc,String groupId, Long movementId);
    public MovementActivityType  getMovementSchedule(UserContext uc, Long movementId);

    public void deletedMovementSchedule(UserContext uc, Long movementId);

    public List<OffenderScheduleTransferType> searchMovements(UserContext uc, MovementActivitySearchType searchType);

    /**
     * 
     * @param uc
     * @param toFacility
     * @param filId
     * @param admitingFacility
     * @return
     */
    public List<OffenderScheduleTransferType> getAdmitIncomingTransfer(UserContext uc, Long toFacility, Long filId, Long admitingFacility);

    public void cancelInternalScheduledMovementsForSupervision(UserContext uc, Long supervisionId, Date cancelDate, String movementReasonMetaCode/*, String comment*/) ;
}
