package syscon.arbutus.product.services.movement.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.core.services.realization.BaseEntity;
import syscon.arbutus.product.services.realization.model.MetaCode;

/**
 * SupervisionClosureConfigurationEntity for MovementActivity Service
 *
 * @author yshang
 * @version 2.0
 * @DbComment MA_SuperClose 'Supervision Closure Configuration table for Movement Activity Service'
 * @DbComment .createUserId 'User ID who created the object'
 * @DbComment .createDateTime 'Date and time when the object was created'
 * @DbComment .modifyUserId 'User ID who last updated the object'
 * @DbComment .modifyDateTime 'Date and time when the object was last updated'
 * @DbComment .invocationContext 'Invocation context when the create/update action called'
 * @DbComment .version 'the version number of record updated'
 * @since October 25, 2012
 */
@Audited
@Entity
@Table(name = "MA_SuperClose")
public class SupervisionClosureConfigurationEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 3112495972127188577L;

    /**
     * @DbComment ClosureConfID 'Supervision Closure Configuration ID'
     */
    @Id
    @Column(name = "ClosureConfID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_MA_SUPERCLOSE_ID")
    @SequenceGenerator(name = "SEQ_MA_SUPERCLOSE_ID", sequenceName = "SEQ_MA_SUPERCLOSE_ID", allocationSize = 1)
    private Long id;

    /**
     * @DbComment movementType 'The type of the Movement(APP, VISIT, etc..)'
     */
    @Column(name = "movementType", nullable = false)
    @MetaCode(set = syscon.arbutus.product.services.movement.realization.persistence.MetaSet.MOVEMENT_TYPE)
    private String movementType;

    /**
     * @DbComment movementReason 'The reason of the Movement'
     */
    @Column(name = "movementReason", nullable = false)
    @MetaCode(set = syscon.arbutus.product.services.movement.realization.persistence.MetaSet.MOVEMENT_REASON)
    private String movementReason;

    /**
     * @DbComment isCloseSupervisionFlag 'If set to true then the offender supervision period must be closed when a movement is completed with the movement type and reason listed in this configuration object'
     */
    @Column(name = "isCloseSupervisionFlag", nullable = false)
    private Boolean isCloseSupervisionFlag;

    /**
     * @Dbcomment isCleanSchedulesFlag 'If set to true then the offender scheduled movements must be closed when a movement is completed with the movement type and reason listed in this configuration object'
     */
    @Column(name = "isCleanSchedulesFlag", nullable = false)
    private Boolean isCleanSchedulesFlag;

    /**
     * @Dbcomment isCloseLegalFlag 'If set to true then the offender legal stuff must be checked when a movement is completed with the movement type and reason listed in this configuration object'
     */
    @Column(name = "isCloseLegalFlag", nullable = false)
    private Boolean isCloseLegalFlag;

    /**
     * @Dbcomment isReleaseBedFlag 'If set to true then the offender bed  must be released when a movement is completed with the movement type and reason listed in this configuration object'
     */
    @Column(name = "isReleaseBedFlag", nullable = false)
    private Boolean isReleaseBedFlag;

    /**
     * @DbComment defaultAdmissionReason 'The MovementReason which is linked to MovementType:ESCP, REL, TRNIJ, TRNOJ.'
     */
    @MetaCode(set = MetaSet.MOVEMENT_REASON)
    @Column(name = "defaultAdmissionReason", nullable = true, length = 64)
    private String defaultAdmissionReason;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the movementType
     */
    public String getMovementType() {
        return movementType;
    }

    /**
     * @param movementType the movementType to set
     */
    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    /**
     * @return the movementReason
     */
    public String getMovementReason() {
        return movementReason;
    }

    /**
     * @param movementReason the movementReason to set
     */
    public void setMovementReason(String movementReason) {
        this.movementReason = movementReason;
    }

    /**
     * @return the isCloseSupervisionFlag
     */
    public Boolean getIsCloseSupervisionFlag() {
        return isCloseSupervisionFlag;
    }

    /**
     * @param isCloseSupervisionFlag the isCloseSupervisionFlag to set
     */
    public void setIsCloseSupervisionFlag(Boolean isCloseSupervisionFlag) {
        this.isCloseSupervisionFlag = isCloseSupervisionFlag;
    }

    public Boolean getIsCleanSchedulesFlag() {
        return isCleanSchedulesFlag;
    }

    public void setIsCleanSchedulesFlag(Boolean isCleanSchedulesFlag) {
        this.isCleanSchedulesFlag = isCleanSchedulesFlag;
    }

    public Boolean getIsCloseLegalFlag() {
        return isCloseLegalFlag;
    }

    public void setIsCloseLegalFlag(Boolean isCloseLegalFlag) {
        this.isCloseLegalFlag = isCloseLegalFlag;
    }

    public Boolean getIsReleaseBedFlag() {
        return isReleaseBedFlag;
    }

    public void setIsReleaseBedFlag(Boolean isReleaseBedFlag) {
        this.isReleaseBedFlag = isReleaseBedFlag;
    }

    public String getDefaultAdmissionReason() {
        return defaultAdmissionReason;
    }

    public void setDefaultAdmissionReason(String defaultAdmissionReason) {
        this.defaultAdmissionReason = defaultAdmissionReason;
    }

    /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((movementReason == null) ? 0 : movementReason.hashCode());
        result = prime * result + ((movementType == null) ? 0 : movementType.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SupervisionClosureConfigurationEntity other = (SupervisionClosureConfigurationEntity) obj;
        if (movementReason == null) {
            if (other.movementReason != null) {
				return false;
			}
        } else if (!movementReason.equals(other.movementReason)) {
			return false;
		}
        if (movementType == null) {
            if (other.movementType != null) {
				return false;
			}
        } else if (!movementType.equals(other.movementType)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SupervisionClosureConfigurationEntity [id=" + id + ", movementType=" + movementType + ", movementReason=" + movementReason + ", isCloseSupervisionFlag="
                + isCloseSupervisionFlag + ", stamp=" + getStamp() + "]";
    }

}
