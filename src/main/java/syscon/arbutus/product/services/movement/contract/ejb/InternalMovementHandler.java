package syscon.arbutus.product.services.movement.contract.ejb;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.StaleObjectStateException;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.core.common.adapters.FacilityInternalLocationAdapter;
import syscon.arbutus.product.services.core.common.adapters.FacilityServiceAdapter;
import syscon.arbutus.product.services.core.common.adapters.PersonServiceAdapter;
import syscon.arbutus.product.services.core.common.adapters.SupervisionServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.ArbutusOptimisticLockException;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.FacilityInternalLocation;
import syscon.arbutus.product.services.movement.contract.dto.*;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementServiceLocal;
import syscon.arbutus.product.services.movement.realization.persistence.ExternalMovementActivityEntity;
import syscon.arbutus.product.services.movement.realization.persistence.InternalMovementActivityEntity;
import syscon.arbutus.product.services.movement.realization.persistence.MovementActivityCommentEntity;
import syscon.arbutus.product.services.movement.realization.persistence.MovementActivityEntity;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * Created by hshen on 1/22/16.
 */
public class InternalMovementHandler extends   DefaultMovementHandler {

    public InternalMovementHandler(Session session, MovementServiceLocal movementService, ActivityService activityService) {
        super(session, movementService, activityService);
    }

    @Override
    public Long create(UserContext uc, MovementActivityType movementActivity, Long facilityId) {

        ActivityType activity = createScheduleActivity(uc, movementActivity.getPlannedStartDate(), facilityId, movementActivity, false,
                movementActivity.getPlannedEndDate());
        movementActivity.setMovementDate(null);
        movementActivity.setMovementCategory(MovementServiceBean.MovementCategory.INTERNAL.code());
        movementActivity.setActivityId(activity.getActivityIdentification());
        movementActivity.setMovementStatus(MovementServiceBean.MovementStatus.PENDING.code());
        return movementService.create(uc, movementActivity, false).getMovementId();


    }

    @Override
    public Long createAdhoc(UserContext uc, MovementActivityType movementActivity, Long facilityId) {
        Long activityId = createActivity(uc, movementActivity.getMovementDate(), movementActivity.getSupervisionId(), Boolean.FALSE, movementActivity.getPlannedEndDate());
        movementActivity.setMovementCategory(MovementServiceBean.MovementCategory.INTERNAL.code());
        movementActivity.setActivityId(activityId);
        movementActivity.setMovementStatus(MovementServiceBean.MovementStatus.COMPLETED.code());
        return  movementService.create(uc, movementActivity, false).getMovementId();
    }

    @Override
    public void update(UserContext uc, MovementActivityType movementActivity) {

        String functionName=this.getClass().getName() + "update";


        Long id = movementActivity.getMovementId();
        InternalMovementActivityEntity  existEntity = BeanHelper.getEntity(session, InternalMovementActivityEntity.class, id);
        String category = existEntity.getMovementCategory();


        InternalMovementActivityEntity updatingMovmentActivityEntity =MovementHelper.toMovementActivityEntity(movementActivity, InternalMovementActivityEntity.class);

        ValidationHelper.verifyMetaCodes(updatingMovmentActivityEntity, false);

        updateMoveActivityStatus(functionName, existEntity, updatingMovmentActivityEntity.getMovementStatus());


        if (!BeanHelper.isEmpty(updatingMovmentActivityEntity.getCommentText())) {
            updateComments(existEntity, updatingMovmentActivityEntity);
        } else {
            updatingMovmentActivityEntity.setCommentText(new HashSet<MovementActivityCommentEntity>());
            updateComments(existEntity, updatingMovmentActivityEntity);

        }
         updateInternalMovementProps(existEntity, updatingMovmentActivityEntity);

        try {

            existEntity.setMovementCategory(category);
            session.merge(existEntity);
            //updateActivity
            Long activityId =  updatingMovmentActivityEntity.getActivityId();
            updateActivityDate(uc, movementActivity.getPlannedStartDate(), null, activityId);

        }catch(StaleObjectStateException ex){
            throw new ArbutusOptimisticLockException(ex);

        }


    }

    @Override
    public List<OffenderScheduleTransferType> search(UserContext uc, MovementActivitySearchType searchType) {

        List<InternalMovementActivityType> movements=  movementService.search(uc, searchType.getMovementIds(),searchType, 0L, 200L, null).getInternalMovementActivity();

        //Convert to Web form Object
        List<OffenderScheduleTransferType> scheduleList = new ArrayList<OffenderScheduleTransferType>();
        for(InternalMovementActivityType movement :  movements) {
            OffenderScheduleTransferType offender = new OffenderScheduleTransferType();

            offender.setActivityId(movement.getActivityId());
            offender.setMovementId(movement.getMovementId());
            offender.setMovementDirection(movement.getMovementDirection());
            offender.setbAdhoc(movement.isbAdhoc());

            offender.setMovementDate(movement.getMovementDate());
            offender.setScheduledTransferDate(movement.getPlannedStartDate());
            offender.setScheduledTransferStartTime(movement.getPlannedStartDate());
            offender.setScheduledTransferTime(timeFormat.format(movement.getPlannedStartDate()));

            offender.setReleaseDate(movement.getPlannedStartDate());
            offender.setReleaseTime(movement.getPlannedStartDate());

            offender.setReturnDate(movement.getPlannedEndDate());
            offender.setReturnTime(movement.getPlannedEndDate());

            offender.setMovementType(movement.getMovementType());
            offender.setMovementCategory(movement.getMovementCategory());

            offender.setToFacilityInternalLocation(movement.getToFacilityInternalLocationId());


            if (null != movement.getToFacilityInternalLocationId()) {
                FacilityInternalLocation fil = FacilityInternalLocationAdapter.get(uc, movement.getToFacilityInternalLocationId());
                if (null != fil) {
                    offender.setToFacilityInternalLocationName(fil.getDescription());
                }
            }

            offender.setMovementReason(movement.getMovementReason());
            offender.setCancelReasoncode(movement.getCancelReason());

            offender.setMovementStatus(movement.getMovementStatus());
            offender.setParticipateOfficerId(movement.getParticipateStaffId());


            offender.setSupervisionId(movement.getSupervisionId());
            String strCurrentLocation = SupervisionServiceAdapter.getOffenderCurrentLocationBySupervisionId(uc, movement.getSupervisionId());
            offender.setCurrentLocation(strCurrentLocation);

            SupervisionType supervisionType = SupervisionServiceAdapter.get(uc,movement.getSupervisionId());
            offender.setSupervisionDisplayId(supervisionType.getSupervisionDisplayID());
            offender.setInmatePersonId(supervisionType.getPersonId());
            offender.setInmateCurrentFacility(supervisionType.getFacilityId());



            Long personIdentityId = supervisionType.getPersonIdentityId();
            PersonIdentityType pi = PersonServiceAdapter.getPersonIdentityType(uc, personIdentityId, null);
            offender.setFirstName(pi.getFirstName());
            offender.setLastName(pi.getLastName());
            offender.setOffenderNumber(pi.getOffenderNumber());
            offender.setPersonIdentifierId(pi.getPersonIdentityId());



            Set<CommentType> commentTypes = movement.getCommentText();
            if (null != commentTypes && commentTypes.size() > 0) {
                for (CommentType c : commentTypes) {
                    offender.setComment(c.getComment());
                }
            }

            scheduleList.add(offender);


        }

        return scheduleList;

    }





    }





