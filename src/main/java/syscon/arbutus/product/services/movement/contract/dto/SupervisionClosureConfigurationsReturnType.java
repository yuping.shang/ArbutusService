package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.List;

/**
 * SupervisionClosureConfigurationsReturnType for MovementActivity Service
 *
 * @author yshang
 * @version 2.0
 * @since October 25, 2012
 */
public class SupervisionClosureConfigurationsReturnType implements Serializable {

    private static final long serialVersionUID = 7941203488838104586L;

    private List<SupervisionClosureConfigurationType> supervisionClosureConfigurations;

    private Long totalSize;

    /**
     * @return the supervisionClosureConfigurations
     */
    public List<SupervisionClosureConfigurationType> getSupervisionClosureConfigurations() {
        return supervisionClosureConfigurations;
    }

    /**
     * @param supervisionClosureConfigurations the supervisionClosureConfigurations to set
     */
    public void setSupervisionClosureConfigurations(List<SupervisionClosureConfigurationType> supervisionClosureConfigurations) {
        this.supervisionClosureConfigurations = supervisionClosureConfigurations;
    }

    /**
     * @return the totalSize
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * @param totalSize the totalSize to set
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SupervisionClosureConfigurationsReturnType [supervisionClosureConfigurations=" + supervisionClosureConfigurations + ", totalSize=" + totalSize + "]";
    }

}
