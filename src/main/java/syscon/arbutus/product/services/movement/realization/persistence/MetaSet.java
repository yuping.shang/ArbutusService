package syscon.arbutus.product.services.movement.realization.persistence;

import java.io.Serializable;

public class MetaSet implements Serializable {
    private static final long serialVersionUID = -68233800566143157L;

    /**
     * MovementCategory
     */
    public static final String MOVEMENT_CATEGORY = "MovementCategory";
    /**
     * MovementType
     */
    public static final String MOVEMENT_TYPE = "MovementType";
    /**
     * MovementDirection
     */
    public static final String MOVEMENT_DIRECTION = "MovementDirection";
    /**
     * MovementStatus
     */
    public static final String MOVEMENT_STATUS = "MovementStatus";
    /**
     * MovementReason
     */
    public static final String MOVEMENT_REASON = "MovementReason";
    /**
     * MovementOutcome
     */
    public static final String MOVEMENT_OUTCOME = "MovementOutcome";
    /**
     * StateProvince
     */
    public static final String TO_PROVINCE_STATE = "StateProvince";    //Refer to Person Service.
    /**
     * TransferWaitlistPriority
     */
    public static final String TRANSFER_WAITLIST_PRIORITY = "TransferWaitlistPriority";
    /**
     * TATransport
     */
    public static final String TA_TRANSPORT = "TATransport";
    /**
     * TransferCancelReason
     */
    public static final String TRANSFER_CANCEL_REASON = "TransferCancelReason";
    /**
     * EscapeCustody
     */
    public static final String ESCAPE_CUSTODY = "EscapeCustody";
    /**
     * EscapeCircumstance
     */
    public static final String ESCAPE_CIRCUMSTANCE = "EscapeCircumstance";
    /**
     * SecurityLevel
     */
    public static final String SECURITY_LEVEL = "SecurityLevel";
}
