package syscon.arbutus.product.services.movement.contract.interfaces;

import java.util.Date;
import java.util.List;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityDetailsType;
import syscon.arbutus.product.services.movement.contract.dto.MovementDetailsType;
import syscon.arbutus.product.services.movement.contract.dto.OffenderLocationStatusType;

/**
 * Business contract interface for MovementDetails service.
 * <br><pre>
 * Purpose
 * The MovementDetails service provides the capability to retrieve movement and location status information for one or more offenders.
 * This service is required to bridge the gap between a movement and its association to an offender.
 * To this end, this service interacts with several services within the JMS:
 * •	Supervision
 * •	Activity
 * •	Movement Activity
 * •	Housing Bed Management Activity
 * Scope
 * The following actions are in scope for this service:
 * •	Retrieve the most recent completed movement for one or more offenders
 * •	Determine an offender’s current IN/OUT status based on the most recent completed movement
 * Out of Scope
 * N/A
 * </pre>
 * <b>See</b> {@link syscon.arbutus.product.services.movementdetails.contract.dto.ReturnCode}
 * for all error codes and descriptions.
 * <p><b>Service JNDI Lookup Name:</b> MovementDetailsService
 * <p><b>Example of invocation of the service:</b>
 * <pre>
 * 	Properties p = new Properties();
 * 	properties.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
 * 	properties.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
 * 	properties.put("java.naming.provider.url", hostAddress);  // hostAddress -- specified accordingly
 * 	Context ctx = new InitialContext(p);
 * 	MovementDetailsService service = (MovementDetailsService)ctx.lookup("MovementDetailsService");
 * 	SecurityClient client = SecurityClientFactory.getSecurityClient();
 * 	client.setSimple(userId, password);
 * 	client.login();
 * 	UserContext uc = new UserContext();
 * 	uc.setConsumingApplicationId(clientAppId);
 * 	uc.setFacilityId(facilityId);
 * 	uc.setDeviceId(deviceId);
 * 	uc.setProcess(process);
 * 	uc.setTask(task);
 * 	uc.setTimestamp(timestamp);
 * <p>MovementDetails service class diagram:
 * <br/><img src="doc-files/MovementDetailsServiceUML.jpg" />
 *
 * @author lhan
 * @version 1.0 (based on the SDD of MovementDetails1.0)
 */
public interface MovementDetails {

    /**
     * <p>Retrieve the most recent completed movement and associated activity for one or more offender's supervisions. </p>
     * <p/>
     * <p>For each Supervision ID received, find and return the most recent movement where MovementStatus == COMPLETE</p>
     * <p/>
     * <p>The most recent movement can be an Internal or External movement of any type.</p>
     * <p/>
     * <p>The most recent movement is identified based on the Movement Activity Type with the latest MovementDate. </p>
     *
     * @param userContext                UserContext - Required
     * @param supervisionIdentifications Set<Long> - the set of supervision Id - Required
     * @return A list of MovementActivityDetailsType instances.
     * Throw exception if there is an error.
     * Return empty list when no instances could be found.
     */
    public List<MovementActivityDetailsType> getMostRecentActivityMovementData(UserContext userContext, Set<Long> supervisionIdentifications);

    /**
     * <p>Retrieve the most recent completed movement for one or more offender's supervisions. </p>
     * <p/>
     * <p>For each Supervision ID received, find and return the most recent movement  where MovementStatus == COMPLETE</p>
     * <p/>
     * <p>The most recent movement can be an Internal or External movement of any type.</p>
     * <p/>
     * <p>The most recent movement is identified based on the Movement Activity Type with the latest MovementDate. </p>
     *
     * @param userContext                UserContext - Required
     * @param supervisionIdentifications Set<Long> - the set of supervision Id - Required.
     * @return A list of MovementDetailsType instances.
     * Throw exception if there is an error.
     * Return empty list when no instances could be found.
     */
    public List<MovementDetailsType> getMostRecentMovementData(UserContext userContext, Set<Long> supervisionIdentifications);

    /**
     * <p>Determine an offender’s current IN/OUT status based on the most recent completed movement.
     * <p/>
     * <p>For the Supervision ID received, find and return the most recent movement  where MovementStatus == COMPLETE>
     * <p/>
     * <p>The most recent movement  can be an Internal or External movement of any type.
     * <p/>
     * <p>For the Supervision ID received, find the offender’s current housing location.
     * <p/>
     * <p>For the Supervision ID received, find the offender’s current actual location.One of the following must be populated:
     * <p/>
     * <pre>
     * If the most recent movement activity is of category INTERNAL then
     * 	•	CurrentInternalLocationReference must be populated
     * Else if the most recent movement activity is of category EXTERNAL then at least one of the following must be populated
     * 	•	CurrentAddressReference
     * 	•	CurrentOrganizationReference
     * 	•	CurrentFacilityReference
     * 	•	CurrentCity
     * 	•	CurrentProvinceState
     * </pre>
     * <p/>
     * <p>For the Supervision ID received, determine the current IN/OUT status:</p>
     * <pre>
     * o	If the most recent movement activity is of category EXTERNAL and movement direction is OUT then status is “OUT”
     * o	If the most recent movement activity is of category EXTERNAL and movement direction is IN and the ToFacilityReference is the facility the offender is being supervised under then status is “IN”,
     *        otherwise the status is “OUT”
     * o	If the most recent movement activity is of category INTERNAL then status is “IN”
     * </pre>
     *
     * @param userContext               UserContext - Required
     * @param supervisionIdentification Long - the supervision Id - Required.
     * @return OffenderLocationStatusType instance.
     * Throw exception if there is an error.
     */
    public OffenderLocationStatusType getOffenderLocationStatus(UserContext userContext, Long supervisionIdentification);

    /**
     * <p>Determine the current IN/OUT status based on the most recent completed movement for one or more offenders.</p>
     * <p/>
     * <p>For each Supervision ID received, find and return the most recent movement  where MovementStatus == COMPLETE</p>
     * <p/>
     * <p>The most recent movement  can be an Internal or External movement of any type.</p>
     * <p/>
     * <p>For each Supervision ID received, find the offender’s current housing location.</p>
     * <p/>
     * <p>For each Supervision ID received, find the offender’s current actual location. One of the following must be populated:</p>
     * <pre>
     * If the most recent movement activity is of category INTERNAL then
     * 	•	CurrentInternalLocationReference must be populated
     * Else if the most recent movement activity is of category EXTERNAL then at least one of the following must be populated
     * 	•	CurrentAddressReference
     * 	•	CurrentOrganizationReference
     * 	•	CurrentFacilityReference
     * 	•	CurrentCity
     * 	•	CurrentProvinceState
     * </pre>
     * <p/>
     * <p>For each Supervision ID received, determine the current IN/OUT status:</p>
     * <pre>
     * o	If the most recent movement activity is of category EXTERNAL and movement direction is OUT then status is “OUT”
     * o	If the most recent movement activity is of category EXTERNAL and movement direction is IN and the ToFacilityReference is the facility the offender is being supervised under then status is “IN”,
     *        otherwise the status is “OUT”
     * o	If the most recent movement activity is of category INTERNAL then status is “IN”
     * </pre>
     *
     * @param userContext                UserContext - Required
     * @param supervisionIdentifications Set<Long> - the set of supervision Id - Required.
     * @return A list of OffenderLocationStatusType instances.
     * Throw exception if there is an error.
     * Return empty list when no instances could be found.
     */
    public List<OffenderLocationStatusType> getOffenderLocationStatuses(UserContext userContext, Set<Long> supervisionIdentifications);

    /**
     * <p>Returns movement activity details using the actual movement date and various search criteria for a supervison Id.</p>
     * <pre>
     * •	SupervisionIdentification is mandatory
     * •	All other inputs are optional
     * •	If any of MovementStatus, MovementCategory or MovementType are specified only those Movement Activity (and related Activity) objects which match these criteria are returned (in addition to the conditions below)
     * 		Note: More than one MovementStatus may be provided
     * •	If FromActualDate is specified
     * 		o	Return all Movement Activity and related Activity objects for the offender where MovementDate is equal to or greater than FromActualDate
     * •	If ToActualDate is specified
     * 		o	Return all Movement Activity and related Activity objects for the offender where MovementDate is equal to or less than ToActualDate
     * •	If FromActualDate and ToActualDate is specified
     * 		o	Return all Movement Activity and related Activity objects for the offender where MovementDate is equal to or between FromActualDate and ToActualDate
     * •	Otherwise
     * 		o	Return all Movement Activity and related Activity objects for the offender
     * </pre>
     *
     * @param userContext               UserContext - Required
     * @param supervisionIdentification Long - the supervision Id.  Required
     * @param fromActualDate            Date - If fromActualDate is specified, return all Movement Activity and related Activity recor: ds for the offender where MovementDate is equal to or greater than fromActualDate - Optional
     * @param toActualDate              Date - If toActualDate is specified, return all Movement Activity and related Activity recor: ds for the offender where MovementDate is equal to or greater than toActualDate - Optional
     * @param movementCategory          String - the code value in Movement Category Set - Optional
     * @param movementStatuses          Set<String> - the set of reference code values in Movement Status Set - Optional
     * @param movementType              String - the reference code value in Movement Type Set - Optional
     * @return A list of MovementActivityDetailsType instances.
     * Throw exception if there is an error.
     * Return empty list when no instances could be found.
     */
    public List<MovementActivityDetailsType> searchMovementDetailsByMovementDate(UserContext userContext, Long supervisionIdentification, Date fromActualDate,
            Date toActualDate, String movementCategory, Set<String> movementStatuses, String movementType);

    /**
     * <p>Returns movement activity details using the scheduled date and various search criteria for a supervison Id.</p>
     * <pre>
     * •	SupervisionIdentification is mandatory
     * •	All other inputs are optional
     * •	If any of MovementStatus, MovementCategory or MovementType are specified only those Movement Activity (and related Activity) objects which match these criteria are returned (in addition to the conditions below)
     * 		Note: More than one MovementStatus may be provided
     * •	If FromScheduledDate is specified
     * 	o	Return all Movement Activity and related Activity objects for the offender where PlannedStartDate is equal to or greater than FromScheduledDate
     * •	If ToScheduledDate is specified
     * 	o	Return all Movement Activity and related Activity objects for the offender where PlannedStartDate is equal to or less than ToScheduledDate
     * •	If FromScheduledDate and ToScheduledDate is specified
     * 	o	Return all Movement Activity and related Activity objects for the offender where PlannedStartDate is equal to or between FromScheduledDate and ToScheduledDate
     * •	Otherwise
     * 	o	Return all Movement Activity and related Activity objects for the offender
     * </pre>
     *
     * @param userContext               UserContext - Required
     * @param supervisionIdentification Long - the supervision Id. Required
     * @param fromScheduledDate         Date - If fromScheduledDate is specified, return all Movement Activity and related Activities for the offender where PlannedStartDate is equal to or greater than fromScheduledDate - Optional
     * @param toScheduledDate           Date - If toScheduledDate is specified, return all Movement Activity and related Activities for the offender where PlannedStartDate is equal to or less than toScheduledDate - Optional
     * @param movementCategory          String - the reference code value in Movement Category Set - Optional
     * @param movementStatuses          Set<String> - the set of reference code values in Movement Status Set - Optional
     * @param movementType              String - the reference code value in Movement Type Set - Optional
     * @return A list of MovementActivityDetailsType instances.
     * Throw exception if there is an error.
     * Return empty list when no instances could be found.
     */
    public List<MovementActivityDetailsType> searchMovementDetailsByScheduledDate(UserContext userContext, Long supervisionIdentification, Date fromScheduledDate,
            Date toScheduledDate, String movementCategory, Set<String> movementStatuses, String movementType);

    /**
     * Retrieve the version number in format "MajorVersionNumber.MinorVersionNumber"
     *
     * @param userContext - Required
     * @return version number
     */
    public String getVersion(UserContext userContext);

}
