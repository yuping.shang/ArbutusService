package syscon.arbutus.product.services.movement.realization.persistence;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Boost;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.Fields;
import org.hibernate.search.annotations.FullTextFilterDef;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;

/**
 * Created by wmadruga on 5/7/14.
 */
@Entity
@Table(name = "V_TRANSFERWAITLISTGRID")
@Indexed
@Analyzer(impl = org.apache.lucene.analysis.standard.StandardAnalyzer.class)
@FullTextFilterDef(name = "facility", impl = FacilityFilterFactor.class)
public class TransferWaitListDatagridViewEntity {

    @Field(index = Index.YES, analyze = Analyze.NO, store = Store.NO)
    @Boost(5)
    private String firstName;

    @Field(index = Index.YES, analyze = Analyze.NO, store = Store.NO)
    @Boost(5)
    private String lastName;

    @Id
    @Field(index = Index.YES, analyze = Analyze.NO, store = Store.NO)
    private String offenderNumber;

    @Column(name = "name")
    @Fields({ @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO), @Field(name = "facilityName", analyze = Analyze.NO)

    })
    private String name;

    @Field(index = Index.YES, analyze = Analyze.NO, store = Store.NO)
    @FieldBridge(impl = MyDateBridge.class)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateAdded;

    @Fields({ @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO), @Field(name = "reason", analyze = Analyze.NO) })
    private String transferReason;

    @Fields({ @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO), @Field(name = "transPriority", analyze = Analyze.NO) })

    private String priority;

    @Field(index = Index.YES, analyze = Analyze.NO, store = Store.NO)
    private Long toFacility;

    @Field(index = Index.YES, analyze = Analyze.NO, store = Store.NO)
    private Long fromFacility;

    private Long supervisionId;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOffenderNumber() {
        return offenderNumber;
    }

    public void setOffenderNumber(String offenderNumber) {
        this.offenderNumber = offenderNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getTransferReason() {
        return transferReason;
    }

    public void setTransferReason(String transferReason) {
        this.transferReason = transferReason;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Long getToFacility() {
        return toFacility;
    }

    public void setToFacility(Long toFacility) {
        this.toFacility = toFacility;
    }

    public Long getFromFacility() {
        return fromFacility;
    }

    public void setFromFacility(Long fromFacility) {
        this.fromFacility = fromFacility;
    }

    public Long getSupervisionId() {
        return supervisionId;
    }

    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }
}

