package syscon.arbutus.product.services.movement.contract.ejb;

import java.util.*;

import org.hibernate.Session;
import org.hibernate.StaleObjectStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.core.common.adapters.*;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.ArbutusOptimisticLockException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.core.util.DateUtil;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.FacilityInternalLocation;
import syscon.arbutus.product.services.housing.contract.dto.housingassignment.UnassignOffenderType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.OffenderHousingAssignmentType;
import syscon.arbutus.product.services.movement.contract.dto.*;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementServiceLocal;
import syscon.arbutus.product.services.movement.realization.persistence.ExternalMovementActivityEntity;
import syscon.arbutus.product.services.movement.realization.persistence.InternalMovementActivityEntity;
import syscon.arbutus.product.services.movement.realization.persistence.MovementActivityCommentEntity;
import syscon.arbutus.product.services.movement.realization.persistence.MovementActivityEntity;
import syscon.arbutus.product.services.organization.realization.persistence.OrganizationEntity;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.contract.dto.TypeOfInterestEnum;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * Created by hshen on 1/26/16.
 */
public class TransferMovementHandler extends   DefaultMovementHandler {

    private static Logger log = LoggerFactory.getLogger(NoMovementHandler.class);

    public TransferMovementHandler(Session session, MovementServiceLocal movementService, ActivityService activityService) {
        super(session, movementService, activityService);
    }

    @Override
    public Long create(UserContext uc, MovementActivityType movementActivity, Long facilityId) {


        String functionName=this.getClass().getName() + "create";
        if (!(movementActivity instanceof ExternalMovementActivityType)) {
            String message = "Invalid Type: ExternalMovementActivityType is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);


        }

        ExternalMovementActivityType movement = (ExternalMovementActivityType) movementActivity;
        Long result = null;


        //create out out activity
        ActivityType outActivity = createScheduleActivity(uc, movement.getPlannedStartDate(), facilityId, movement, false, null);

        movement.setMovementDate(null);
        movement.setMovementDirection(MovementServiceBean.MovementDirection.OUT.code());
        movement.setMovementCategory(MovementServiceBean.MovementCategory.EXTERNAL.code());
        movement.setActivityId(outActivity.getActivityIdentification());
        movement.setMovementStatus(MovementServiceBean.MovementStatus.PENDING.code());
        result = movementService.create(uc, movement, false).getMovementId();

        //clean up Transfer WaitList
        TransferWaitlistSearchType searchCriteria = new TransferWaitlistSearchType();
        searchCriteria.setFacilityId(facilityId);
        searchCriteria.setSupervisionId(movement.getSupervisionId());
        List<TransferWaitlistType> waitLists = movementService.searchTransferWaitlist(uc, searchCriteria, null, null, null).getTransferWaitlists();
        if(waitLists!=null && waitLists.size()>0){
            Set<Long> supIds = new HashSet<Long>();
            supIds.add(movement.getSupervisionId());
            movementService.deleteTransferWaitlistEntry(uc, facilityId, supIds);

        }

        return result;

    }

    @Override
    public Long createAdhoc(UserContext uc, MovementActivityType movementActivity, Long facilityId) {

        String functionName=this.getClass().getName() + "create";
        if (!(movementActivity instanceof ExternalMovementActivityType)) {
            String message = "Invalid Type: ExternalMovementActivityType is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);


        }


        ExternalMovementActivityType movement = (ExternalMovementActivityType) movementActivity;
        Long result = null;

        //create out out activity
        Long outActivityId = createActivity(uc, movementActivity.getMovementDate(), movementActivity.getSupervisionId(), Boolean.FALSE, null);

        movement.setMovementDate(movementActivity.getMovementDate());
        movement.setMovementDirection(MovementServiceBean.MovementDirection.OUT.code());
        movement.setMovementCategory(MovementServiceBean.MovementCategory.EXTERNAL.code());
        movement.setActivityId(outActivityId);
        movement.setMovementStatus(MovementServiceBean.MovementStatus.COMPLETED.code());
        result = movementService.create(uc, movement, false).getMovementId();

        postCompleteMovement(uc, movement);

        return result;
    }

    private void postCompleteMovement(UserContext uc, ExternalMovementActivityType movement) {
        SupervisionClosureConfigurationSearchType supervisionClosureConfigurationSearchType = new SupervisionClosureConfigurationSearchType();
        supervisionClosureConfigurationSearchType.setMovementReason(movement.getMovementReason());
        supervisionClosureConfigurationSearchType.setMovementType(MovementServiceBean.MovementType.TRNIJ.code());

        List<SupervisionClosureConfigurationType> configs = movementService.searchSupervisionClosureConfiguration(uc, supervisionClosureConfigurationSearchType, null, null,null).getSupervisionClosureConfigurations();

        if (configs != null && configs.size() > 0 && configs.get(0).getIsReleaseBedFlag()) {
            unAssignHousing(uc, movement);
        }

        //To Update Type of Interest To IN TRANSIT
        SupervisionServiceAdapter.updateFOIForSupervision(uc, movement.getSupervisionId(), TypeOfInterestEnum.IN_TRANSIT);

        if (configs.size() > 0 && configs.get(0).getIsCleanSchedulesFlag()) {
        	SupervisionServiceAdapter.cleanUpSchedulesWhileSupervisionTransfer(uc, movement.getSupervisionId(), movement.getParticipateStaffId(),
                    movement.getMovementDate(), movement.getMovementReason());
        }

        if (configs.size() > 0 && configs.get(0).getIsCloseSupervisionFlag()) {
            SupervisionServiceAdapter.close(uc, movement.getSupervisionId(),movement.getMovementDate());
        }
    }

    private void unAssignHousing(UserContext userContext, ExternalMovementActivityType movement ) {


        Long staffId = movement.getParticipateStaffId();

        OffenderHousingAssignmentType  assignmentType = HousingServiceAdapter.getCurrentAssignmentByOffender(userContext, movement.getSupervisionId());


        CommentType commentType= null;
        if(assignmentType!=null){
            String comment;

            if (movement.getCommentText() == null || movement.getCommentText().size()==0) {
                comment = "Housing Unassigned";


            }else
            {
                comment = movement.getCommentText().iterator().next().getComment();

            }


            HousingServiceAdapter.unAssignOffenderFromHousingLocation(userContext, movement.getSupervisionId(), movement.getMovementReason(), staffId,
                    movement.getActivityId(), movement.getFromFacilityId(), comment, movement.getMovementDate());


        }
    }

    @Override
    public void update(UserContext uc, MovementActivityType movement) {


        String functionName=this.getClass().getName() + "update";

        if (!(movement instanceof ExternalMovementActivityType)){
            String message = "Invalid Type: ExternalMovementActivityType is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);

        }

        ExternalMovementActivityType  movementActivity = (ExternalMovementActivityType)movement;

        Long id = movementActivity.getMovementId();
        ExternalMovementActivityEntity existEntity = BeanHelper.getEntity(session, ExternalMovementActivityEntity.class, id);

        ExternalMovementActivityEntity updatingMovmentActivityEntity = MovementHelper.toMovementActivityEntity(movementActivity, ExternalMovementActivityEntity.class);

        ValidationHelper.verifyMetaCodes(updatingMovmentActivityEntity, false);


        updateMoveActivityStatus(functionName, existEntity, updatingMovmentActivityEntity.getMovementStatus());



        if (!BeanHelper.isEmpty(updatingMovmentActivityEntity.getCommentText())) {
            updateComments(existEntity, updatingMovmentActivityEntity);
        } else {
            updatingMovmentActivityEntity.setCommentText(new HashSet<MovementActivityCommentEntity>());
            updateComments(existEntity, updatingMovmentActivityEntity);

        }

        existEntity.setMovementReason(updatingMovmentActivityEntity.getMovementReason());
        existEntity.setEscortOrganizationId(updatingMovmentActivityEntity.getEscortOrganizationId());
        existEntity.setEscortDetails(updatingMovmentActivityEntity.getEscortDetails());
        existEntity.setToFacilityId(updatingMovmentActivityEntity.getToFacilityId());
        existEntity.setReportingDate(updatingMovmentActivityEntity.getReportingDate());



        try {
            session.merge(existEntity);
            //update out Activity
            Long activityId =  updatingMovmentActivityEntity.getActivityId();
            updateActivityDate(uc, movementActivity.getPlannedStartDate(), null, activityId);

        }catch(StaleObjectStateException ex){
            throw new ArbutusOptimisticLockException(ex);

        }


    }

    @Override
    public List<OffenderScheduleTransferType> search(UserContext uc, MovementActivitySearchType searchType){

        List<ExternalMovementActivityType> movements=  movementService.search(uc, searchType.getMovementIds(),searchType, 0L, 200L, null).getExternalMovementActivity();

        //Convert to Web form Object
        List<OffenderScheduleTransferType> scheduleList = new ArrayList<OffenderScheduleTransferType>();
        for(ExternalMovementActivityType movement :  movements) {
            OffenderScheduleTransferType offender = new OffenderScheduleTransferType();

            offender.setActivityId(movement.getActivityId());
            offender.setMovementId(movement.getMovementId());
            offender.setMovementDirection(movement.getMovementDirection());
            offender.setbAdhoc(movement.isbAdhoc());

            offender.setMovementDate(movement.getMovementDate());
            offender.setScheduledTransferDate(movement.getPlannedStartDate());
            offender.setScheduledTransferStartTime(movement.getPlannedStartDate());
            offender.setScheduledTransferTime(timeFormat.format(movement.getPlannedStartDate()));

            offender.setReleaseDate(movement.getPlannedStartDate());
            offender.setReleaseTime(movement.getPlannedStartDate());

            offender.setReturnDate(movement.getReportingDate());
            offender.setReturnTime(movement.getReportingDate());

            offender.setExpectedArrivalDate(movement.getReportingDate());
            offender.setExpectedArrivalTime(movement.getReportingDate());

            offender.setMovementType(movement.getMovementType());
            offender.setMovementCategory(movement.getMovementCategory());

            offender.setFromFacility(movement.getFromFacilityId());
            offender.setToFacility(movement.getToFacilityId());




            if (null != movement.getToFacilityId()) {
                Facility facilityType = FacilityServiceAdapter.getFacility(uc, movement.getToFacilityId());
                if (null != facilityType) {
                    offender.setToFacilityName(facilityType.getFacilityName());
                }
            }



            if (null != movement.getFromFacilityId()) {
                Facility facilityType = FacilityServiceAdapter.getFacility(uc, movement.getFromFacilityId());
                if (null != facilityType) {
                    offender.setFromFacilityName(facilityType.getFacilityName());
                }
            }




            offender.setMovementReason(movement.getMovementReason());
            offender.setCancelReasoncode(movement.getCancelReason());

            offender.setMovementStatus(movement.getMovementStatus());


            offender.setSupervisionId(movement.getSupervisionId());
            String strCurrentLocation = SupervisionServiceAdapter.getOffenderCurrentLocationBySupervisionId(uc, movement.getSupervisionId());
            offender.setCurrentLocation(strCurrentLocation);

            SupervisionType supervisionType = SupervisionServiceAdapter.get(uc,movement.getSupervisionId());
            offender.setSupervisionDisplayId(supervisionType.getSupervisionDisplayID());
            offender.setInmatePersonId(supervisionType.getPersonId());
            offender.setInmateCurrentFacility(supervisionType.getFacilityId());



            Long personIdentityId = supervisionType.getPersonIdentityId();
            PersonIdentityType pi = PersonServiceAdapter.getPersonIdentityType(uc, personIdentityId, null);
            offender.setFirstName(pi.getFirstName());
            offender.setLastName(pi.getLastName());
            offender.setOffenderNumber(pi.getOffenderNumber());
            offender.setPersonIdentifierId(pi.getPersonIdentityId());



            Set<CommentType> commentTypes = movement.getCommentText();
            if (null != commentTypes && commentTypes.size() > 0) {
                for (CommentType c : commentTypes) {
                    offender.setComment(c.getComment());
                }
            }


            offender.setEscortOrgId(movement.getEscortOrganizationId());
            offender.setEscortDetals(movement.getEscortDetails());
            if (offender.getEscortOrgId() != null) {
                Long orgId = offender.getEscortOrgId();
                OrganizationEntity orgEntity = (OrganizationEntity) session.get(OrganizationEntity.class, orgId);
                if (orgEntity != null) {
                    offender.setEscortOrgName(orgEntity.getOrganizationName());
                }
            }



            scheduleList.add(offender);


        }

        return scheduleList;


    }

    @Override
    public  void completeMovement(UserContext uc, MovementActivityType movement){

        preCompleteCheck(movement);
        ExternalMovementActivityType movementActivity= (ExternalMovementActivityType)movement;
        checkLastCompletedMovement(uc, movementActivity);



        ExternalMovementActivityEntity existEntity = BeanHelper.getEntity(session, ExternalMovementActivityEntity.class, movementActivity.getMovementId());
        if(movementActivity.getMovementDate()==null){
            movementActivity.setMovementDate(DateUtil.getCurrentDate());

        }

        existEntity.setMovementDate(movementActivity.getMovementDate());

        existEntity.setMovementStatus(MovementServiceBean.MovementStatus.COMPLETED.code());

        existEntity.setEscortOrganizationId(movementActivity.getEscortOrganizationId());

        MovementActivityEntity  updatingMovmentActivityEntity;
        if (movementActivity instanceof ExternalMovementActivityType) {
            updatingMovmentActivityEntity = MovementHelper.toMovementActivityEntity(movementActivity, ExternalMovementActivityEntity.class);

        }else
        {
            updatingMovmentActivityEntity = MovementHelper.toMovementActivityEntity(movementActivity, InternalMovementActivityEntity.class);

        }

        updateComments(existEntity, updatingMovmentActivityEntity);
        try {
            session.merge(existEntity);
        }catch (StaleObjectStateException ex){
            throw new ArbutusOptimisticLockException(ex);
        }

        ActivityType activity = activityService.get(uc, movementActivity.getActivityId());
        activity.setActiveStatusFlag(Boolean.FALSE);
        activityService.update(uc, activity, null, Boolean.TRUE);

        postCompleteMovement(uc, movementActivity);


    }

    public static ExternalMovementActivityType  toMovementActivityType(String userId,ExternalMovement exMov, Boolean adhoc, Long staffId){

        ExternalMovementActivityType movementActivity = new ExternalMovementActivityType();

        if(adhoc){
            movementActivity.setMovementDate(DateUtil.combineDateTime(exMov.getMoveDate(), exMov.getMoveTime()));
            movementActivity.setMovementStatus(MovementActivityType.MovementStatus.COMPLETED.code());

        }else
        {
            movementActivity.setMovementDate(null);
            movementActivity.setMovementStatus(MovementActivityType.MovementStatus.PENDING.code());
            movementActivity.setPlannedStartDate(DateUtil.combineDateTime(exMov.getMoveDate(), exMov.getMoveTime()));


        }


        if (null != exMov.getExpectedArrivalDate() && exMov.getExpectedArrivalTime() != null) {
            movementActivity.setReportingDate(DateUtil.combineDateTime(exMov.getExpectedArrivalDate(), exMov.getExpectedArrivalTime()));

        } else {
            movementActivity.setReportingDate(exMov.getExpectedArrivalDate());
        }



        movementActivity.setMovementCategory(MovementActivityType.MovementCategory.EXTERNAL.value());
        movementActivity.setMovementType(MovementActivityType.MovementType.TRNIJ.code());
        movementActivity.setMovementDirection(MovementActivityType.MovementDirection.OUT.code());
        movementActivity.setMovementReason(exMov.getMoveReasonCd());
        movementActivity.setMovementOutcome(exMov.getMoveOutcomeCd());
        movementActivity.setEscortDetails(exMov.getEscortDetails());
        /* from facility */
        Long fromFacilityAssociation = exMov.getFromFacility();
        movementActivity.setFromFacilityId(fromFacilityAssociation);
        /* escort organization */
        if (exMov.getEscortOrg() != null && exMov.getEscortOrg() != 0) {
            Long escortOrganizationAssociation = exMov.getEscortOrg();
            movementActivity.setEscortOrganizationId(escortOrganizationAssociation);
        }
        /* toFacility */

        Long toFacilityAssociation = exMov.getToFacility();
        movementActivity.setToFacilityId(toFacilityAssociation);

		/* activity */
        movementActivity.setActivityId(exMov.getAssocedActivityId());
        /* supervision */

        movementActivity.setSupervisionId(exMov.getSupervisionId());
        String comment = exMov.getComment();

        if (comment != null && !comment.isEmpty()) {
            Set<CommentType> comments = new HashSet();
            CommentType temp = new CommentType(null, userId, new Date(), comment);
            comments.add(temp);
            movementActivity.setCommentText(comments);
        }

        movementActivity.setParticipateStaffId(staffId);

        return movementActivity;


    }
}
