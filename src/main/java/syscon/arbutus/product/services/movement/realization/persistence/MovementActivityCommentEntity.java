package syscon.arbutus.product.services.movement.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.core.common.StampInterface;
import syscon.arbutus.product.services.core.services.realization.BaseEntity;

/**
 * CommentEntity for Movement Activity Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment MA_Comment 'Comment table for Movement Activity Service'
 * @DbComment .createUserId 'Create User Id'
 * @DbComment .createDateTime 'Create Date Time'
 * @DbComment .modifyUserId 'Modify User Id'
 * @DbComment .modifyDateTime 'Modify Date Time'
 * @DbComment .invocationContext 'Invocation Context'
 * @DbComment .version 'the version number of record updated'
 * @since September 27, 2012
 */
@Audited
@Entity
@Table(name = "MA_Comment")
public class MovementActivityCommentEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -5376624224584274009L;

    /**
     * @DbComment MoveActCommentID 'Movement Activity Comment Id'
     */
    @Id
    @Column(name = "MoveActCommentID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_MA_COMMENT_ID")
    @SequenceGenerator(name = "SEQ_MA_COMMENT_ID", sequenceName = "SEQ_MA_COMMENT_ID", allocationSize = 1)
    private Long commentIdentification;

    /**
     * @DbComment FromIdentifier 'From Identifier'
     */
    @Column(name = "FromIdentifier", nullable = false)
    private Long fromIdentifier;

    /**
     * @DbComment commentDate 'The comment date'
     */
    @Column(name = "commentDate", nullable = false)
    private Date commentDate;

    /**
     * @DbComment commentText 'The comment text'
     */
    @Column(name = "commentText", nullable = false, length = 512)
    private String commentText;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FromIdentifier", referencedColumnName = "MoveActID", insertable = false, updatable = false)
    private MovementActivityEntity movementActivity;

    public MovementActivityCommentEntity() {
        super();
    }

    /**
     * @param commentIdentification
     * @param fromIdentifier
     * @param commentDate
     * @param commentText
     */
    public MovementActivityCommentEntity(Long commentIdentification, Long fromIdentifier, Date commentDate, String commentText, StampInterface stamp, Long version) {
        super();
        this.commentIdentification = commentIdentification;
        this.fromIdentifier = fromIdentifier;
        this.commentDate = commentDate;
        this.commentText = commentText;
        this.setStamp(stamp);
        this.setVersion(version);
    }

    @Override
    public Long getId() {
        return commentIdentification;
    }

    /**
     * @return the commentIdentification
     */
    public Long getCommentIdentification() {
        return commentIdentification;
    }

    /**
     * @param commentIdentification the commentIdentification to set
     */
    public void setCommentIdentification(Long commentIdentification) {
        this.commentIdentification = commentIdentification;
    }

    /**
     * @return the fromIdentifier
     */
    public Long getFromIdentifier() {
        return fromIdentifier;
    }

    /**
     * @param fromIdentifier the fromIdentifier to set
     */
    public void setFromIdentifier(Long fromIdentifier) {
        this.fromIdentifier = fromIdentifier;
    }

    /**
     * @return the commentDate
     */
    public Date getCommentDate() {
        return commentDate;
    }

    /**
     * @param commentDate the commentDate to set
     */
    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    /**
     * @return the commentText
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * @param commentText the commentText to set
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    /**
     * @return the movementActivity
     */
    public MovementActivityEntity getMovementActivity() {
        return movementActivity;
    }

    /**
     * @param movementActivity the movementActivity to set
     */
    public void setMovementActivity(MovementActivityEntity movementActivity) {
        this.movementActivity = movementActivity;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((commentDate == null) ? 0 : commentDate.hashCode());
        result = prime * result + ((commentText == null) ? 0 : commentText.hashCode());
        result = prime * result + ((fromIdentifier == null) ? 0 : fromIdentifier.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof MovementActivityCommentEntity)) {
            return false;
        }
        MovementActivityCommentEntity other = (MovementActivityCommentEntity) obj;
        if (commentDate == null) {
            if (other.commentDate != null) {
                return false;
            }
        } else if (!commentDate.equals(other.commentDate)) {
            return false;
        }
        if (commentText == null) {
            if (other.commentText != null) {
                return false;
            }
        } else if (!commentText.equals(other.commentText)) {
            return false;
        }
        if (fromIdentifier == null) {
            if (other.fromIdentifier != null) {
                return false;
            }
        } else if (!fromIdentifier.equals(other.fromIdentifier)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CommentEntity [commentIdentification=" + commentIdentification + ", fromIdentifier=" + fromIdentifier + ", commentDate=" + commentDate + ", commentText="
                + commentText + ", stamp=" + getStamp() /* + ", movementActivity=" + movementActivity*/ + "]";
    }

}
