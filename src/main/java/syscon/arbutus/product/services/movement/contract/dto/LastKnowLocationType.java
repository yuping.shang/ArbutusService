package syscon.arbutus.product.services.movement.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

import syscon.arbutus.product.services.core.contract.dto.BaseDto;

/**
 * @author hshen
 * @version 2.0
 * @since May 21, 2014
 */

public class LastKnowLocationType extends BaseDto implements Serializable {
    private static final long serialVersionUID = 3112494972127188579L;

    private Long recordId;

    @NotNull
    private String supervisionDisplayId;

    private Long supervisionId;
    @NotNull
    private Long facilityId;
    @NotNull
    private Long internalLocationId;

    private Boolean isLatestLocation;

    public LastKnowLocationType(String supervisionDisplayId, Long facilityId, Long internalLocationId) {
        this.supervisionDisplayId = supervisionDisplayId;
        this.facilityId = facilityId;
        this.internalLocationId = internalLocationId;
    }

    public LastKnowLocationType() {
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getSupervisionId() {
        return supervisionId;
    }

    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    public Long getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    public Long getInternalLocationId() {
        return internalLocationId;
    }

    public void setInternalLocationId(Long internalLocationId) {
        this.internalLocationId = internalLocationId;
    }

    public Boolean getIsLatestLocation() {
        return isLatestLocation;
    }

    public void setIsLatestLocation(Boolean isLatestLocation) {
        this.isLatestLocation = isLatestLocation;
    }

    public String getSupervisionDisplayId() {
        return supervisionDisplayId;
    }

    public void setSupervisionDisplayId(String supervisionDisplayId) {
        this.supervisionDisplayId = supervisionDisplayId;
    }

    @Override
    public String toString() {
        return "LastKnowLocationType{" +
                "recordId=" + recordId +
                ", supervisionDisplayId='" + supervisionDisplayId + '\'' +
                ", supervisionId=" + supervisionId +
                ", facilityId=" + facilityId +
                ", internalLocationId=" + internalLocationId +
                ", isLatestLocation=" + isLatestLocation +
                '}';
    }
}
