package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * TransferWaitlistSearchType for MovementActivity Service
 * <p>At least one search criteria must be specified.
 *
 * @author yshang
 * @version 2.0
 * @since October 25, 2012
 */
public class TransferWaitlistSearchType implements Serializable {

    private static final long serialVersionUID = 7941203488838104581L;

    private Long facilityId;

    private Long supervisionId;

    private Date fromDateAdded;

    private Date toDateAdded;

    private Long toFacilityId;

    private String transferReason;

    private String priority;

    public TransferWaitlistSearchType() {
        super();
    }

    /**
     * Indicates which facility the transfer waitlist is for. Static association to the Facility service
     *
     * @return the facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * Indicates which facility the transfer waitlist is for. Static association to the Facility service
     *
     * @param facilityId the facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * The list of offenders who are awaiting transfers. Static association to the Supervision service.
     *
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * The list of offenders who are awaiting transfers. Static association to the Supervision service.
     *
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Date an offender was added to the waitlist
     *
     * @return the fromDateAdded
     */
    public Date getFromDateAdded() {
        return fromDateAdded;
    }

    /**
     * Date an offender was added to the waitlist
     *
     * @param fromDateAdded the dateAdded to set
     */
    public void setFromDateAdded(Date fromDateAdded) {
        this.fromDateAdded = fromDateAdded;
    }

    /**
     * @return the toDateAdded
     */
    public Date getToDateAdded() {
        return toDateAdded;
    }

    /**
     * @param toDateAdded the toDateAdded to set
     */
    public void setToDateAdded(Date toDateAdded) {
        this.toDateAdded = toDateAdded;
    }

    /**
     * The facility that the offender is to be transferred to. Static association to the Facility service
     *
     * @return the toFacilityId
     */
    public Long getToFacilityId() {
        return toFacilityId;
    }

    /**
     * The facility that the offender is to be transferred to. Static association to the Facility service
     *
     * @param toFacilityId the toFacilityId to set
     */
    public void setToFacilityId(Long toFacilityId) {
        this.toFacilityId = toFacilityId;
    }

    /**
     * The reason for the transfer
     *
     * @return the transferReason
     */
    public String getTransferReason() {
        return transferReason;
    }

    /**
     * The reason for the transfer
     *
     * @param transferReason the transferReason to set
     */
    public void setTransferReason(String transferReason) {
        this.transferReason = transferReason;
    }

    /**
     * The priority of each offender on the waitlist
     *
     * @return the priority
     */
    public String getPriority() {
        return priority;
    }

    /**
     * The priority of each offender on the waitlist
     *
     * @param priority the priority to set
     */
    public void setPriority(String priority) {
        this.priority = priority;
    }

}
