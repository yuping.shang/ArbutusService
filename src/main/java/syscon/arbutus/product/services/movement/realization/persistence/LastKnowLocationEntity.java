package syscon.arbutus.product.services.movement.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.core.services.realization.BaseEntity;

/**
 * LastKnowLocationEntity for Movement Activity Service
 *
 * @author hshen
 * @version 2.0
 * @DbComment MA_LastLocation 'Last known Location table for Movement Activity Service'
 * @DbComment .createUserId 'Create User Id'
 * @DbComment .createDateTime 'Create Date Time'
 * @DbComment .modifyUserId 'Modify User Id'
 * @DbComment .modifyDateTime 'Modify Date Time'
 * @DbComment .invocationContext 'Invocation Context'
 * @DbComment .version 'the version number of record updated'
 * @since May 21, 2014
 */
@Audited
@Entity
@Table(name = "MA_LastLocation")
public class LastKnowLocationEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 3112494972127188570L;

    /**
     * @DbComment recordID 'Identifier'
     */
    @Id
    @Column(name = "recordID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_MA_LASTLOCATION_ID")
    @SequenceGenerator(name = "SEQ_MA_LASTLOCATION_ID", sequenceName = "SEQ_MA_LASTLOCATION_ID", allocationSize = 1)
    private Long recordID;

    /**
     * @DbComment supervisionId 'Supervision Id'
     */

    @Column(name = "supervisionId")
    private Long supervisionId;

    /**
     * @DbComment facilityId 'Facility Id'
     */
    @Column(name = "facilityId")
    private Long facilityId;

    /**
     * @DbComment internalLocationId 'Facility Internal Location Id'
     */
    @Column(name = "internalLocationId")
    private Long internalLocationId;

    /**
     * @DbComment active 'If set to true then the current location is offender latest location, otherwise, it is an old last known location'
     */
    @Column(name = "active", nullable = false)
    private Boolean active;

    public LastKnowLocationEntity() {
    }

    @Override
    public Long getId() {
        return recordID;
    }

    @Override
    public String toString() {
        return "LastKnowLocationEntity{" +
                "recordID=" + recordID +
                ", supervisionId=" + supervisionId +
                ", facilityId=" + facilityId +
                ", internalLocationId=" + internalLocationId +
                ", active=" + active +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LastKnowLocationEntity that = (LastKnowLocationEntity) o;

        if (!active.equals(that.active)) {
            return false;
        }
        if (!facilityId.equals(that.facilityId)) {
            return false;
        }
        if (!internalLocationId.equals(that.internalLocationId)) {
            return false;
        }
        if (!recordID.equals(that.recordID)) {
            return false;
        }
        if (!supervisionId.equals(that.supervisionId)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = recordID.hashCode();
        result = 31 * result + supervisionId.hashCode();
        result = 31 * result + facilityId.hashCode();
        result = 31 * result + internalLocationId.hashCode();
        result = 31 * result + active.hashCode();
        return result;
    }

    /**
     * @return facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * @param facilityId set the facility
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * @return identifier
     */
    public Long getRecordID() {
        return recordID;
    }

    /**
     * @param recordID set identifier
     */
    public void setRecordID(Long recordID) {
        this.recordID = recordID;
    }

    /**
     * @return the indicator
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active latest known location indicator
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * @return the internalLocationId
     */
    public Long getInternalLocationId() {
        return internalLocationId;
    }

    /**
     * @param internalLocationId the internalLocationId to set
     */
    public void setInternalLocationId(Long internalLocationId) {
        this.internalLocationId = internalLocationId;
    }
}

