package syscon.arbutus.product.services.movement.contract.dto;

/**
 * Reference Set for MovementActivity Service
 *
 * @author yshang
 * @version 2.0
 * @since October 19, 2012
 */
public enum ReferenceSet {

    /**
     * MovementCategory
     */
    MOVEMENT_CATEGORY("MovementCategory"),

    /**
     * MovementType
     */
    MOVEMENT_TYPE("MovementType"),

    /**
     * MovementDirection
     */
    MOVEMENT_DIRECTION("MovementDirection"),

    /**
     * MovementStatus
     */
    MOVEMENT_STATUS("MovementStatus"),

    /**
     * MovementReason
     */
    MOVEMENT_REASON("MovementReason"),

    /**
     * MovementOutcome
     */
    MOVEMENT_OUTCOME("MovementOutcome"),

    /**
     * StateProvince
     */
    TO_PROVINCE_STATE("StateProvince"),        //Refer to Person Service.

    /**
     * TransferWaitlistPriority
     */
    TRANSFER_WAITLIST_PRIORITY("TransferWaitlistPriority"),

    ESCAPE_LINK("ESCP"),

    /**
     * MovementTemplate
     */
    MOVEMENT_TEMPLATE("MovementTemplate"),

    /**
     * MovementAction
     */

    MOVEMENT_ACTION("MovementAction");

    private final String value;

    ReferenceSet(String v) {
        value = v;
    }

    public static ReferenceSet fromValue(String v) {
        for (ReferenceSet c : ReferenceSet.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public String value() {
        return value;
    }

}