package syscon.arbutus.product.services.movement.contract.dto;

import syscon.arbutus.product.services.core.contract.dto.BaseDto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Objects;

/**
 * EscapeRecapture defines escape and recapture related properties
 *
 * Created by yshang on 11/23/15.
 */
public class EscapeRecapture extends BaseDto implements Serializable {

    private static final long serialVersionUID = -2762704646793795675L;

    /**
     * escape/recapture Id
     */
    private Long escapeRecaptureId;

    /**
     * Inmate supervision
     */
    @NotNull
    private Long supervisionId;

    /**
     * Facility escaped from
     */
    private Long escapeFacility;

    /**
     * Description of Escape Facility
     */
    private String escapeFacilityDescription;

    /**
     * Escape Reason
     */
    @NotNull
    @Size(min = 1, max = 64, message = "max length 64")
    private String escapeReason;
    /**
     * Description of Escape Reason
     */
    @Size(min = 1, max = 128, message = "max length 128")
    private String escapeReasonDescription;

    /**
     * Escape Date
     */
    @NotNull
    private Date escapeDate;

    /**
     * Escape Time
     */
    @NotNull
    private Date escapeTime;

    /**
     * Escape from Custody.
     */
    @Size(max = 64, message = "max length 64")
    private String fromCustody;

    @Size(max = 128, message = "max length 128")
    private String fromCustodyDescription;

    /**
     * Escape securityLevel
     */
    @Size(max = 64, message = "max length 64")
    private String securityBreached;

    @Size(max = 128, message = "max length 128")
    private String securityBreachedDescription;

    /**
     * Escape Circumstance.
     */
    @Size(max = 64, message = "max length 64")
    private String circumstance;

    @Size(max = 128, message = "max length 128")
    private String circumstanceDescription;

    /**
     * Escape Last Seen Date
     */
    private Date lastSeenDate;

    /**
     * Escape Last Seen Time
     */
    private Date lastSeenTime;

    /**
     * Escape incident Number
     */
    private String incidentNumber;

    /**
     * Escape Comment
     */
    private String escapeComments;

    /**
     * Recapture Date
     */
    private Date recaptureDate;

    /**
     * Recapture Time
     */
    private Date recaptureTime;

    /**
     * Recapture arresting Organization
     */
    private Long arrestingOrganization;

    /**
     * Arresting Organization Name
     */
    private String arrestingOrganizationName;

    /**
     * Recapture Comments
     */
    private String recaptureComments;

    /**
     * Readmission Date
     */
    private Date readmissionDate;

    /**
     * Readmission Time
     */
    private Date readmissionTime;

    /**
     * Readmission Facility
     */
    private Long readmissionFacility;

    /**
     * Readmission Facility Name
     */
    private String readmissionFacilityName;

    /**
     * Readmission Reason
     */
    @Size(max = 64, message = "max length 64")
    private String readmissionReason;

    /**
     * Readmission Reason Description
     */
    @Size(max = 128, message = "max length 128")
    private String readmissionReasonDescription;

    /**
     * Readmission Comment
     */
    private String readmissionComment;

    /**
     * get Escape Date Time.
     *
     * TODO This has to be removed as DateUtil is not part of service client.
     *
     * @return
     */
    @Deprecated
    public Date getEscapeDateTime(){
        return combineDateTime(escapeDate, escapeTime);
    }

    /**
     * get Recapture Date Time
     *
     * TODO This has to be removed as DateUtil is not part of service client.
     * @return
     */
    @Deprecated
    public Date getRecaptureDateTime(){
        return combineDateTime(recaptureDate, recaptureTime);
    }

    /**
     *
     * TODO This has to be removed as DateUtil is not part of service client.
     * @return
     */
    @Deprecated
    public Date getReadmissionDateTime(){
        return combineDateTime(readmissionDate, readmissionTime);
    }

    //getter and setters

    public Long getEscapeRecaptureId() {
        return escapeRecaptureId;
    }

    public void setEscapeRecaptureId(Long escapeRecaptureId) {
        this.escapeRecaptureId = escapeRecaptureId;
    }

    public Long getSupervisionId() {
        return supervisionId;
    }

    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    public Long getEscapeFacility() {
        return escapeFacility;
    }

    public void setEscapeFacility(Long escapeFacility) {
        this.escapeFacility = escapeFacility;
    }

    public String getEscapeFacilityDescription() {
        return escapeFacilityDescription;
    }

    public void setEscapeFacilityDescription(String escapeFacilityDescription) {

        this.escapeFacilityDescription = escapeFacilityDescription == null ? null : escapeFacilityDescription.trim();
    }

    public String getEscapeReason() {
        return escapeReason;
    }

    public void setEscapeReason(String escapeReason) {
        this.escapeReason = escapeReason == null ? null : escapeReason.trim();
    }

    public String getEscapeReasonDescription() {
        return escapeReasonDescription;
    }

    public void setEscapeReasonDescription(String escapeReasonDescription) {
        this.escapeReasonDescription = escapeReasonDescription == null ? null : escapeReasonDescription.trim();
    }

    public Date getEscapeDate() {
        return escapeDate;
    }

    public void setEscapeDate(Date escapeDate) {
        this.escapeDate = escapeDate;
    }

    public Date getEscapeTime() {
        return escapeTime;
    }

    public void setEscapeTime(Date escapeTime) {
        this.escapeTime = escapeTime;
    }

    public String getFromCustody() {
        return fromCustody;
    }

    public void setFromCustody(String fromCustody) {
        this.fromCustody = fromCustody == null ? null : fromCustody.trim();
    }

    public String getFromCustodyDescription() {
        return fromCustodyDescription;
    }

    public void setFromCustodyDescription(String fromCustodyDescription) {
        this.fromCustodyDescription = fromCustodyDescription == null ? null : fromCustodyDescription.trim();
    }

    public String getSecurityBreached() {
        return securityBreached;
    }

    public void setSecurityBreached(String securityBreached) {
        this.securityBreached = securityBreached == null ? null : securityBreached.trim();
    }

    public String getSecurityBreachedDescription() {
        return securityBreachedDescription;
    }

    public void setSecurityBreachedDescription(String securityBreachedDescription) {
        this.securityBreachedDescription = securityBreachedDescription == null ? null : securityBreachedDescription.trim();
    }

    public String getCircumstance() {
        return circumstance;
    }

    public void setCircumstance(String circumstance) {
        this.circumstance = circumstance;
    }

    public String getCircumstanceDescription() {
        return circumstanceDescription;
    }

    public void setCircumstanceDescription(String circumstanceDescription) {
        this.circumstanceDescription = circumstanceDescription == null ? null : circumstanceDescription.trim();
    }

    public Date getLastSeenDate() {
        return lastSeenDate;
    }

    public void setLastSeenDate(Date lastSeenDate) {
        this.lastSeenDate = lastSeenDate;
    }

    public Date getLastSeenTime() {
        return lastSeenTime;
    }

    public void setLastSeenTime(Date lastSeenTime) {
        this.lastSeenTime = lastSeenTime;
    }

    public String getIncidentNumber() {
        return incidentNumber;
    }

    public void setIncidentNumber(String incidentNumber) {
        this.incidentNumber = incidentNumber == null ? null : incidentNumber.trim();
    }

    public String getEscapeComments() {
        return escapeComments;
    }

    public void setEscapeComments(String escapeComments) {
        this.escapeComments = escapeComments == null ? null : escapeComments.trim();
    }

    public Date getRecaptureDate() {
        return recaptureDate;
    }

    public void setRecaptureDate(Date recaptureDate) {
        this.recaptureDate = recaptureDate;
    }

    public Date getRecaptureTime() {
        return recaptureTime;
    }

    public void setRecaptureTime(Date recaptureTime) {
        this.recaptureTime = recaptureTime;
    }

    public Long getArrestingOrganization() {
        return arrestingOrganization;
    }

    public void setArrestingOrganization(Long arrestingOrganization) {
        this.arrestingOrganization = arrestingOrganization;
    }

    public String getArrestingOrganizationName() {
        return arrestingOrganizationName;
    }

    public void setArrestingOrganizationName(String arrestingOrganizationName) {
        this.arrestingOrganizationName = arrestingOrganizationName;
    }

    public String getRecaptureComments() {
        return recaptureComments;
    }

    public void setRecaptureComments(String recaptureComments) {
        this.recaptureComments = recaptureComments == null ? null : recaptureComments.trim();
    }

    public Date getReadmissionDate() {
        return readmissionDate;
    }

    public void setReadmissionDate(Date readmissionDate) {
        this.readmissionDate = readmissionDate;
    }

    public Date getReadmissionTime() {
        return readmissionTime;
    }

    public void setReadmissionTime(Date readmissionTime) {
        this.readmissionTime = readmissionTime;
    }

    public Long getReadmissionFacility() {
        return readmissionFacility;
    }

    public void setReadmissionFacility(Long readmissionFacility) {
        this.readmissionFacility = readmissionFacility;
    }

    public String getReadmissionFacilityName() {
        return readmissionFacilityName;
    }

    public void setReadmissionFacilityName(String readmissionFacilityName) {
        this.readmissionFacilityName = readmissionFacilityName == null ? null : readmissionFacilityName.trim();
    }

    public String getReadmissionReason() {
        return readmissionReason;
    }

    public void setReadmissionReason(String readmissionReason) {
        this.readmissionReason = readmissionReason == null ? null : readmissionReason.trim();
    }

    public String getReadmissionComment() {
        return readmissionComment;
    }

    public void setReadmissionComment(String readmissionComment) {
        this.readmissionComment = readmissionComment == null ? null : readmissionComment.trim();
    }

    public String getReadmissionReasonDescription() {
        return readmissionReasonDescription;
    }

    public void setReadmissionReasonDescription(String readmissionReasonDescription) {
        this.readmissionReasonDescription = readmissionReasonDescription == null ? null : readmissionReasonDescription.trim();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EscapeRecapture)) return false;
        EscapeRecapture that = (EscapeRecapture) o;
        return Objects.equals(escapeRecaptureId, that.escapeRecaptureId) &&
                Objects.equals(supervisionId, that.supervisionId) &&
                Objects.equals(escapeFacility, that.escapeFacility) &&
                Objects.equals(escapeReason, that.escapeReason) &&
                Objects.equals(escapeDate, that.escapeDate) &&
                Objects.equals(escapeTime, that.escapeTime) &&
                Objects.equals(fromCustody, that.fromCustody) &&
                Objects.equals(securityBreached, that.securityBreached) &&
                Objects.equals(circumstance, that.circumstance) &&
                Objects.equals(lastSeenDate, that.lastSeenDate) &&
                Objects.equals(lastSeenTime, that.lastSeenTime) &&
                Objects.equals(incidentNumber, that.incidentNumber) &&
                Objects.equals(recaptureDate, that.recaptureDate) &&
                Objects.equals(recaptureTime, that.recaptureTime) &&
                Objects.equals(arrestingOrganization, that.arrestingOrganization) &&
                Objects.equals(readmissionDate, that.readmissionDate) &&
                Objects.equals(readmissionTime, that.readmissionTime) &&
                Objects.equals(readmissionFacility, that.readmissionFacility) &&
                Objects.equals(readmissionReason, that.readmissionReason);
    }

    @Override
    public int hashCode() {
        return Objects.hash(escapeRecaptureId, supervisionId, escapeFacility, escapeReason, escapeDate, escapeTime, fromCustody, securityBreached, circumstance, lastSeenDate, lastSeenTime, incidentNumber, recaptureDate, recaptureTime, arrestingOrganization, readmissionDate, readmissionTime, readmissionFacility, readmissionReason);
    }

    @Override
    public String toString() {
        return "EscapeRecapture{" +
                "escapeRecaptureId=" + escapeRecaptureId +
                ", supervisionId=" + supervisionId +
                ", escapeFacility=" + escapeFacility +
                ", escapeFacilityDescription='" + escapeFacilityDescription + '\'' +
                ", escapeReason='" + escapeReason + '\'' +
                ", escapeReasonDescription='" + escapeReasonDescription + '\'' +
                ", escapeDate=" + escapeDate +
                ", escapeTime=" + escapeTime +
                ", fromCustody='" + fromCustody + '\'' +
                ", fromCustodyDescription='" + fromCustodyDescription + '\'' +
                ", securityBreached='" + securityBreached + '\'' +
                ", securityBreachedDescription='" + securityBreachedDescription + '\'' +
                ", circumstance='" + circumstance + '\'' +
                ", circumstanceDescription='" + circumstanceDescription + '\'' +
                ", lastSeenDate=" + lastSeenDate +
                ", lastSeenTime=" + lastSeenTime +
                ", incidentNumber='" + incidentNumber + '\'' +
                ", escapeComments='" + escapeComments + '\'' +
                ", recaptureDate=" + recaptureDate +
                ", recaptureTime=" + recaptureTime +
                ", arrestingOrganization=" + arrestingOrganization +
                ", recaptureComments='" + recaptureComments + '\'' +
                ", readmissionDate=" + readmissionDate +
                ", readmissionTime=" + readmissionTime +
                ", readmissionFacility=" + readmissionFacility +
                ", readmissionFacilityName='" + readmissionFacilityName + '\'' +
                ", readmissionReason='" + readmissionReason + '\'' +
                ", readmissionReasonDescription='" + readmissionReasonDescription + '\'' +
                ", readmissionComment='" + readmissionComment + '\'' +
                '}';
    }

    private Date combineDateTime(Date date, Date time) {
        // Make use of Java 8 Standard API instead of DateUtil.java which should not be included/packaged in Interface/DTO Client jar
        if (date != null && time != null) {
            return Date.from(LocalDateTime.of(
                    LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).toLocalDate(),
                    LocalDateTime.ofInstant(time.toInstant(), ZoneId.systemDefault()).toLocalTime())
                    .atZone(ZoneId.systemDefault())
                    .toInstant());
        }
        return null;
    }
}
