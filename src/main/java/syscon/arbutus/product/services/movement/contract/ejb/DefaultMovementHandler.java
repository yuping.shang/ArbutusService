package syscon.arbutus.product.services.movement.contract.ejb;

import javax.ejb.SessionContext;
import java.text.SimpleDateFormat;
import java.util.*;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.StaleObjectStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ActivityCategory;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.activity.contract.dto.Schedule;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.core.exception.ArbutusOptimisticLockException;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.core.util.DateUtil;
import syscon.arbutus.product.services.movement.contract.dto.*;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementServiceLocal;
import syscon.arbutus.product.services.movement.realization.persistence.ExternalMovementActivityEntity;
import syscon.arbutus.product.services.movement.realization.persistence.InternalMovementActivityEntity;
import syscon.arbutus.product.services.movement.realization.persistence.MovementActivityCommentEntity;
import syscon.arbutus.product.services.movement.realization.persistence.MovementActivityEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * Created by hshen on 1/21/16.
 */
public abstract class DefaultMovementHandler {
    private static Logger log = LoggerFactory.getLogger(DefaultMovementHandler.class);

    protected SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");


    protected  Session session;

    protected MovementServiceLocal movementService;
    protected ActivityService activityService;


    public DefaultMovementHandler(Session session,MovementServiceLocal movementService, ActivityService activityService) {
        this.session=session;
        this.movementService = movementService;
        this.activityService=activityService;
    }

    public abstract Long  create(UserContext uc, MovementActivityType movementActivity, Long facilityId);

    public abstract Long  createAdhoc(UserContext uc, MovementActivityType movementActivity, Long facilityId);
    public abstract void update(UserContext uc, MovementActivityType movementActivity);
    public abstract List<OffenderScheduleTransferType>   search(UserContext uc, MovementActivitySearchType searchType);

    public   void completeMovement(UserContext uc, MovementActivityType movementActivity){
        preCompleteCheck(movementActivity);

        MovementActivityEntity existEntity = BeanHelper.getEntity(session, MovementActivityEntity.class, movementActivity.getMovementId());

        checkLastCompletedMovement(uc, movementActivity);

        if(movementActivity.getMovementDate()==null){
            movementActivity.setMovementDate(DateUtil.getCurrentDate());

        }

        existEntity.setMovementDate(movementActivity.getMovementDate());
        existEntity.setMovementStatus(MovementServiceBean.MovementStatus.COMPLETED.code());

        MovementActivityEntity  updatingMovmentActivityEntity;
        if (movementActivity instanceof ExternalMovementActivityType) {
            updatingMovmentActivityEntity = MovementHelper.toMovementActivityEntity(movementActivity, ExternalMovementActivityEntity.class);

        }else
        {
            updatingMovmentActivityEntity = MovementHelper.toMovementActivityEntity(movementActivity, InternalMovementActivityEntity.class);

        }

        updateComments(existEntity, updatingMovmentActivityEntity);
        try {
            session.merge(existEntity);
        }catch (StaleObjectStateException ex){
            throw new ArbutusOptimisticLockException(ex);
        }

        ActivityType activity = activityService.get(uc, movementActivity.getActivityId());
        activity.setActiveStatusFlag(Boolean.FALSE);
        activityService.update(uc, activity, null, Boolean.TRUE);
    }

    protected void preCompleteCheck(MovementActivityType movementActivity) {
        String functionName = "completeMovement";
        if (movementActivity == null || movementActivity.getMovementId() == null) {
            String message = "Invalid argument: MovementActivity Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(movementActivity);

        if(!MovementServiceBean.MovementStatus.COMPLETED.isEquals(movementActivity.getMovementStatus())){
            String message = "Invalid argument: Movement Status : " + movementActivity.getMovementStatus();
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Long id = movementActivity.getMovementId();
        MovementActivityEntity existEntity = BeanHelper.getEntity(session, MovementActivityEntity.class, id);
        if(!MovementServiceBean.MovementStatus.PENDING.isEquals(existEntity.getMovementStatus())){
            String message = "Status Non PENDING ("+ existEntity.getMovementStatus()+ ") can be chanaged to COMPLETED";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        if(null!=existEntity.getGroupId() && MovementServiceBean.MovementDirection.IN.isEquals(existEntity.getMovementDirection())
                && !MovementServiceBean.MovementStatus.COMPLETED.isEquals(getPartnerMovement(existEntity.getGroupId(), existEntity.getMovementId()).getMovementStatus())){
            String message = "Failed to Complete the IN movement because the OUT Movement is still in UN-Completed";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);


        }

    }

    protected void checkLastCompletedMovement(UserContext uc, MovementActivityType movementActivity) {
        Set<Long> supervisionIds = new HashSet<Long>();
        supervisionIds.add(movementActivity.getSupervisionId());
        Set<LastCompletedSupervisionMovementType> lastCompleteOffenderMovements = movementService.getLastCompletedSupervisionMovementBySupervisions(uc, supervisionIds);
        if(null!=lastCompleteOffenderMovements && !lastCompleteOffenderMovements.isEmpty()){
            LastCompletedSupervisionMovementType lastCompletedMovement =lastCompleteOffenderMovements.iterator().next();
            Date lastCompletedMovementDate = lastCompletedMovement.getLastCompletedMovementActivity().getMovementDate();
            if(movementActivity.getMovementDate().before(lastCompletedMovementDate)){
                String message = "Invalid argument: The movement date is earler than last completed Movement date.";
                LogHelper.error(log, "checkLastCompletedMovement", message);
                throw new InvalidInputException(message);
            }

        }

    }

    protected Long createActivity(UserContext uc, Date occuranceDate, Long supervisionId, Boolean status, Date scheduledEndDate){
        String methodName ="createActivity";

        ActivityType activityType = new ActivityType();
        activityType.setActiveStatusFlag(status);
        activityType.setActivityCategory(ActivityCategory.MOVEMENT.value());
        activityType.setPlannedStartDate(occuranceDate);
        if(scheduledEndDate!=null){
            activityType.setPlannedEndDate(scheduledEndDate);

        }else
        {
            Date plannedEndDate = DateUtil.futureDateTime(occuranceDate);
            activityType.setPlannedEndDate(plannedEndDate);

        }

        activityType.setSupervisionId(supervisionId);
        ActivityType  retActivity = activityService.create(uc, activityType, null, Boolean.TRUE);

        if (retActivity == null) {
            String message = "Error while creating Activity.";
            LogHelper.error(log, methodName, message);
            throw new ArbutusRuntimeException(message);
        }

        return retActivity.getActivityIdentification();
    }



    protected ActivityType createScheduleActivity(UserContext uc, Date scheduleDate, Long facilityId, MovementActivityType movementActivity, Boolean adhoc, Date scheduedEndDate) {
        String methodName = "createScheduleActivity";
        //create schedule
        Schedule schedule = new Schedule();
        schedule.setFacilityId(facilityId);
        schedule.setScheduleCategory(ActivityCategory.MOVEMENT.value());
        schedule.setEffectiveDate(DateUtil.getDateWithoutTime(scheduleDate));
        schedule.setScheduleStartTime(DateUtil.toStringTimeHHMM(scheduleDate));

        Schedule  retSchedule = activityService.createSchedule(uc, schedule);
        if (retSchedule == null) {
            String message = "Error while creating schedule.";
            LogHelper.error(log, methodName, message);
            throw new ArbutusRuntimeException(message);
        }

        //create activity
        ActivityType activityType = new ActivityType();
        activityType.setScheduleID(retSchedule.getScheduleIdentification());
        if(adhoc){
            activityType.setActiveStatusFlag(Boolean.FALSE);
        }else
        {

            activityType.setActiveStatusFlag(Boolean.TRUE);
        }

        activityType.setActivityCategory(ActivityCategory.MOVEMENT.value());
        activityType.setPlannedStartDate(scheduleDate);
        //For instant event, set Planned endDate one minture later than start date

        if(scheduedEndDate!=null){
            activityType.setPlannedEndDate(scheduedEndDate);

        }else
        {
            Date plannedEndDate = DateUtil.futureDateTime(scheduleDate);
            activityType.setPlannedEndDate(plannedEndDate);

        }

        activityType.setSupervisionId(movementActivity.getSupervisionId());
        ActivityType  retActivity = activityService.create(uc, activityType, null, Boolean.TRUE);

        if (retActivity == null) {
            String message = "Error while creating Activity.";
            LogHelper.error(log, methodName, message);
            throw new ArbutusRuntimeException(message);
        }

        return  retActivity;
    }

    protected void updateComments(MovementActivityEntity oldEntity, MovementActivityEntity newEntity) {
        // Delete privileges
        Iterator<MovementActivityCommentEntity> icomment = oldEntity.getCommentText().iterator();
        while (icomment.hasNext()) {
            MovementActivityCommentEntity comment = (MovementActivityCommentEntity) icomment.next();
            session.delete(comment);
        }

        oldEntity.setCommentText(newEntity.getCommentText());

        // Save new privileges set
        icomment = oldEntity.getCommentText().iterator();
        while (icomment.hasNext()) {
            MovementActivityCommentEntity comment = (MovementActivityCommentEntity) icomment.next();
            comment.setCommentIdentification(null);
            comment.setMovementActivity(oldEntity);
            session.save(comment);
        }
    }


    protected void updateActivityDate(UserContext uc, Date updatePlanedStartDate, Date updatePlanedEndDate,Long activityId) {
        ActivityType activity = activityService.get(uc, activityId);
        Date plannedStartDate = activity.getPlannedStartDate();
        if(plannedStartDate.compareTo(updatePlanedStartDate)!=0){
            activity.setPlannedStartDate(updatePlanedStartDate);
            if(updatePlanedEndDate!=null){
                activity.setPlannedEndDate(updatePlanedEndDate);

            }else {
                activity.setPlannedEndDate(DateUtil.futureDateTime(updatePlanedStartDate));
            }
            activityService.update(uc, activity, null, Boolean.TRUE);
            if(activity.getScheduleID()!=null){
                Schedule schedule = activityService.getSchedule(uc, activity.getScheduleID());
                schedule.setScheduleStartTime(DateUtil.toStringTimeHHMM(updatePlanedStartDate));
                schedule.setEffectiveDate(DateUtil.getDateWithoutTime(updatePlanedStartDate));
                activityService.updateSchedule(uc, schedule);
            }

        }
    }



    protected  MovementActivityEntity getPartnerMovement(String groupId, Long movementId) {

        String hql = "FROM MovementActivityEntity ma where ma.groupid = '" + groupId + "' and ma.movementId != " + movementId;
        Query query2 = session.createQuery(hql);
        @SuppressWarnings("rawtypes") List results = query2.list();
        if (null != results && !results.isEmpty()) {
            MovementActivityEntity entity = (MovementActivityEntity) results.get(0);
            return entity;
        }

        return null;

    }


    protected static  void updateExternalMovementProps(MovementActivityEntity dest, MovementActivityEntity source){
        ExternalMovementActivityEntity from = (ExternalMovementActivityEntity) source;
        ExternalMovementActivityEntity to = (ExternalMovementActivityEntity) dest;

        to.setTransportation(from.getTransportation());

        to.setFromLocationId(from.getFromLocationId());
        to.setFromInternalLocationId(from.getFromInternalLocationId());
        to.setFromOrganizationId(from.getFromOrganizationId());
        to.setFromFacilityId(from.getFromFacilityId());

        to.setFromCity(from.getFromCity());

        to.setToLocationId(from.getToLocationId());
        to.setToOrganizationId(from.getToOrganizationId());
        to.setToFacilityId(from.getToFacilityId());
        to.setToCity(from.getToCity());
        to.setToProvinceState(from.getToProvinceState());

        to.setApplicationDate(from.getApplicationDate());
        to.setArrestOrganizationId(from.getArrestOrganizationId());
        to.setEscortOrganizationId(from.getEscortOrganizationId());
        to.setEscortDetails(from.getEscortDetails());
        to.setReportingDate(from.getReportingDate());
        to.setToInternalLocationId(from.getToInternalLocationId());



    }
    protected static void updateInternalMovementProps(MovementActivityEntity  dest,  MovementActivityEntity source){
        InternalMovementActivityEntity from = (InternalMovementActivityEntity) source;
        InternalMovementActivityEntity to = (InternalMovementActivityEntity) dest;
        to.setFromInternalLocationId(from.getFromInternalLocationId());
        to.setToInternalLocationId(from.getToInternalLocationId());


    }


    protected static void updateMoveActivityStatus(String functionName, MovementActivityEntity mae, String status) {
        if (!BeanHelper.isEmpty(status)) {
            // Check business logical
            // Status ONHOLD can be changed to PENDING or CANCELLED.
            if (MovementServiceBean.MovementStatus.ONHOLD.isEquals(mae.getMovementStatus())) {
                if (!(MovementServiceBean.MovementStatus.PENDING.isEquals(status) || MovementServiceBean.MovementStatus.CANCELLED.isEquals(status) || MovementServiceBean.MovementStatus.ONHOLD.isEquals(status))) {
                    String message = "Status ONHOLD can be chaged to PENDING or CANCELLED or ONHOLD itself.";
                    LogHelper.error(log, functionName, message);
                    throw new InvalidInputException(message);
                }
            }

            // Status PENDING cannot be changed to APPREQ or DENIED
            if (MovementServiceBean.MovementStatus.PENDING.isEquals(mae.getMovementStatus())) {
                if (MovementServiceBean.MovementStatus.APPREQ.isEquals(status) || MovementServiceBean.MovementStatus.DENIED.isEquals(status)) {
                    String message = "Status PENDING cannot be changed to APPREQ or DENIED.";
                    LogHelper.error(log, functionName, message);
                    throw new InvalidInputException(message);
                }

            }

            // Status APPREQ can be changed to PENDING or DENIED
            if (MovementServiceBean.MovementStatus.APPREQ.isEquals(mae.getMovementStatus())) {
                if (!(MovementServiceBean.MovementStatus.PENDING.isEquals(status) || MovementServiceBean.MovementStatus.DENIED.isEquals(status) || MovementServiceBean.MovementStatus.CANCELLED.isEquals(status)
                        || MovementServiceBean.MovementStatus.APPREQ.isEquals(status))) {
                    String message = "Status APPREQ can only be changed to PENDING, DENIED OR CANCELLED.";
                    LogHelper.error(log, functionName, message);
                    throw new InvalidInputException(message);
                }
            }

            // Status COMPLETED, CANCELLED, NOSHOW and DENIED cannot be changed to other status.
            if ((MovementServiceBean.MovementStatus.COMPLETED.isEquals(mae.getMovementStatus()) ||
                    MovementServiceBean.MovementStatus.CANCELLED.isEquals(mae.getMovementStatus()) ||
                    MovementServiceBean.MovementStatus.NOSHOW.isEquals(mae.getMovementStatus()) ||
                    MovementServiceBean.MovementStatus.DENIED.isEquals(mae.getMovementStatus())) && !mae.getMovementStatus().equals(status)) {

                String message = "Status " + mae.getMovementStatus() + " cannot be changed to other status.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }

            mae.setMovementStatus(status);
        }
    }

}