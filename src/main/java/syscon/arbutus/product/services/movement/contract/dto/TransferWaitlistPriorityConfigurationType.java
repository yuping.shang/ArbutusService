package syscon.arbutus.product.services.movement.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

import syscon.arbutus.product.services.core.contract.dto.BaseDto;

/**
 * TransferWaitlistPriorityConfigurationType for MovementActivity Service
 * Configuration for Transfer Waitlist Priority
 *
 * @author yshang
 * @version 2.0
 * @since October 25, 2012
 */
public class TransferWaitlistPriorityConfigurationType extends BaseDto implements Serializable {

    private static final long serialVersionUID = 7941203488838104587L;

    /**
     * Textual description of a priority level for
     * the transfer waitlist (e.g., Low, Medium, High, Urgent)
     */
    @NotNull
    private String transferWaitlistEntryPriority;

    /**
     * The level of priority is determined by this value. The highest level is 1,
     * and the lower priorities are larger numbers
     */
    @NotNull
    private Long priorityLevel;

    public TransferWaitlistPriorityConfigurationType() {
        super();
    }

    /**
     * Constructor
     *
     * @param transferWaitlistEntryPriority String - Required
     * @param priorityLevel                 Long - Required
     */
    public TransferWaitlistPriorityConfigurationType(@NotNull String transferWaitlistEntryPriority, @NotNull Long priorityLevel) {
        super();
        this.transferWaitlistEntryPriority = transferWaitlistEntryPriority;
        this.priorityLevel = priorityLevel;
    }

    /**
     * Textual description of a priority level for
     * the transfer waitlist (e.g., Low, Medium, High, Urgent)
     * <p>Required.
     *
     * @return the transferWaitlistEntryPriority
     */
    public String getTransferWaitlistEntryPriority() {
        return transferWaitlistEntryPriority;
    }

    /**
     * Textual description of a priority level for
     * the transfer waitlist (e.g., Low, Medium, High, Urgent)
     * <p>Required.
     *
     * @param transferWaitlistEntryPriority the transferWaitlistEntryPriority to set
     */
    public void setTransferWaitlistEntryPriority(String transferWaitlistEntryPriority) {
        this.transferWaitlistEntryPriority = transferWaitlistEntryPriority;
    }

    /**
     * The level of priority is determined by this value. The highest level is 1,
     * and the lower priorities are larger numbers
     * <p>Required.
     *
     * @return the priorityLevel
     */
    public Long getPriorityLevel() {
        return priorityLevel;
    }

    /**
     * The level of priority is determined by this value. The highest level is 1,
     * and the lower priorities are larger numbers
     * <p>Required.
     *
     * @param priorityLevel the priorityLevel to set
     */
    public void setPriorityLevel(Long priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((transferWaitlistEntryPriority == null) ? 0 : transferWaitlistEntryPriority.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TransferWaitlistPriorityConfigurationType other = (TransferWaitlistPriorityConfigurationType) obj;
        if (transferWaitlistEntryPriority == null) {
            if (other.transferWaitlistEntryPriority != null) {
                return false;
            }
        } else if (!transferWaitlistEntryPriority.equals(other.transferWaitlistEntryPriority)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TransferWaitlistPriorityConfigurationType [transferWaitlistEntryPriority=" + transferWaitlistEntryPriority + ", priorityLevel=" + priorityLevel + "]";
    }

}
