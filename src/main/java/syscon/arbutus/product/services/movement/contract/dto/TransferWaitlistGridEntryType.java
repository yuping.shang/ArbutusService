package syscon.arbutus.product.services.movement.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Enhanced TransferWaitlistEntry for Datagrid
 *
 * @author hshen
 * @author wmmadruga
 * @version 1.0
 * @since April 22, 2014
 */
public class TransferWaitlistGridEntryType implements Serializable {

    @NotNull
    private Long supervisionId;
    @NotNull
    private Date dateAdded;
    @NotNull
    private Long toFacilityId;
    @NotNull
    private String transferReason;
    @NotNull
    private String priority;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    private String offenderNumber;
    @NotNull
    private String toFacilityname;

    private Long fromFacilityId;

    public TransferWaitlistGridEntryType() {
    }

    public TransferWaitlistGridEntryType(String firstName, String lastName, String offenderNumber, String toFacilityname, Date dateAdded, String transferReason,
            String priority, Long toFacilityId, Long fromFacilityId, Long supervisionId) {

        this.dateAdded = dateAdded;
        this.transferReason = transferReason;
        this.priority = priority;
        this.firstName = firstName;
        this.lastName = lastName;
        this.offenderNumber = offenderNumber;
        this.toFacilityname = toFacilityname;
        this.toFacilityId = toFacilityId;
        this.fromFacilityId = fromFacilityId;
        this.supervisionId = supervisionId;

    }

    public Long getSupervisionId() {
        return supervisionId;
    }

    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Long getToFacilityId() {
        return toFacilityId;
    }

    public void setToFacilityId(Long toFacilityId) {
        this.toFacilityId = toFacilityId;
    }

    public String getTransferReason() {
        return transferReason;
    }

    public void setTransferReason(String transferReason) {
        this.transferReason = transferReason;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOffenderNumber() {
        return offenderNumber;
    }

    public void setOffenderNumber(String offenderNumber) {
        this.offenderNumber = offenderNumber;
    }

    public String getToFacilityname() {
        return toFacilityname;
    }

    public void setToFacilityname(String toFacilityname) {
        this.toFacilityname = toFacilityname;
    }

    public Long getFromFacilityId() {
        return fromFacilityId;
    }

    public void setFromFacilityId(Long fromFacilityId) {
        this.fromFacilityId = fromFacilityId;
    }
}
