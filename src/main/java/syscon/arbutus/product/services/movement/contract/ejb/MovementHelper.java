package syscon.arbutus.product.services.movement.contract.ejb;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.*;
import syscon.arbutus.product.services.activity.realization.persistence.ActivityEntity;
import syscon.arbutus.product.services.appointment.contract.dto.Appointment;
import syscon.arbutus.product.services.appointment.contract.dto.AppointmentSearchType;
import syscon.arbutus.product.services.core.common.adapters.*;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.ErrorCodes;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.core.util.DateUtil;
import syscon.arbutus.product.services.discipline.contract.dto.OffenseInCustodyHearingType;
import syscon.arbutus.product.services.discipline.contract.dto.OffenseInCustodySearchType;
import syscon.arbutus.product.services.discipline.contract.dto.OffenseInCustodyType;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.FacilityInternalLocation;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.InternalLocationIdentifierType;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.LocationCategory;
import syscon.arbutus.product.services.facilityinternallocation.contract.interfaces.FacilityInternalLocationServiceLocal;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.LocationHousingAssignmentType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.LocationHousingAssignmentsReturnType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.OffenderHousingAssignmentType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.OffenderHousingAssignmentsReturnType;
import syscon.arbutus.product.services.location.contract.dto.LocationAddressType;
import syscon.arbutus.product.services.location.contract.dto.Location;
import syscon.arbutus.product.services.movement.contract.dto.*;
import syscon.arbutus.product.services.movement.realization.persistence.*;
import syscon.arbutus.product.services.program.contract.dto.ProgramAssignmentSearchType;
import syscon.arbutus.product.services.program.contract.dto.ProgramAssignmentType;
import syscon.arbutus.product.services.program.contract.dto.ProgramOfferingType;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.supervision.contract.dto.*;
import syscon.arbutus.product.services.utility.ValidationHelper;
import syscon.arbutus.product.services.visitation.contract.dto.VisitSearchType;
import syscon.arbutus.product.services.visitation.contract.dto.VisitType;

/**
 * Created by omid.soleimani on 13/03/2015.
 */
public class MovementHelper {
    private static final Logger LOG = LoggerFactory.getLogger(MovementHelper.class);
    private static final String MOVEMENT_CATERGORY = "MOVEMENT";

    /**
     * @param uc
     * @param supervisionIds
     * @param fromScheduledDate
     * @param toScheduledDate
     * @param subActivityIds
     * @return the activities whose MovementCategory is MOVEMENT.
     * @throws syscon.arbutus.product.services.core.exception.ArbutusException
     */
    public static Set<ActivityType> searchActivitiesByMovementCategory(final UserContext uc, final Set<Long> supervisionIds, final Date fromScheduledDate,
            final Date toScheduledDate, final Set<Long> subActivityIds) {

        Set<ActivityType> activities = new HashSet<>();

        ActivitySearchType searchType = new ActivitySearchType();
        searchType.setActivityCategory(Constants.ACTIVITY_CATEGORY_MOVEMENT);
        searchType.setFromPlannedStartDate(fromScheduledDate);
        searchType.setToPlannedStartDate(toScheduledDate);
        searchType.setSupervisionIds(supervisionIds);

        LOG.debug("search type is: " + searchType.toString());

        ActivitiesReturnType rtn = ActivityServiceAdapter.searchActivity(uc, subActivityIds, searchType, null, null, null);
        if (rtn.getActivities() != null && !rtn.getActivities().isEmpty()) {
            activities.addAll(rtn.getActivities());
        }
        return activities;
    }

    /**
     * @param activities
     * @return the map of supervision id to it's associated ActivityType set for passed-in activities.
     */
    public static HashMap<Long, Set<ActivityType>> mapSupervisionIdActivitySet(final Set<ActivityType> activities) {
        HashMap<Long, Set<ActivityType>> rtn = new HashMap<Long, Set<ActivityType>>();
        for (ActivityType act : activities) {
            Long supId = act.getSupervisionId();
            Set<ActivityType> acts = rtn.get(supId);
            if (acts == null) {
                acts = new HashSet<ActivityType>();
                rtn.put(supId, acts);
            }
            acts.add(act);
        }

        return rtn;
    }

    public static Date getDateWithoutTime(final Date input) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(input.getTime());
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    public static List<ScheduleConflict> search(final UserContext uc, final Long supervisionId, final String language) {
        Map<String, String> appointmentTypeMap = ReferenceDataServiceAdapter.getReferenceCodesDescMap(uc, "APPOINTMENTTYPE", language);

        List<ScheduleConflict> conflicts = new ArrayList<ScheduleConflict>();
        AppointmentSearchType searchType = new AppointmentSearchType();
        searchType.setSupervisionId(supervisionId);
        searchType.setStartTime(getDateWithoutTime(new Date(System.currentTimeMillis())));
        Iterator<Appointment> iterator = AppointmentServiceAdapter.search(uc, searchType).iterator();
        while (iterator.hasNext()) {
            Appointment appointment = iterator.next();
            ScheduleConflict conflict = new ScheduleConflict();
            conflict.setEventId(appointment.getAppointmentId());
            conflict.setSupervisionId(supervisionId);
            conflict.setType(ScheduleConflict.EventType.APPOINTMENT);
            conflict.setSubType(appointment.getType());
            conflict.setToLocationId(appointment.getLocationId());
            conflict.setLocation(appointment.getLocationDesc());
            conflict.setStartDate(appointment.getStartTime());
            conflict.setEndDate(appointment.getEndTime());
            String description = appointmentTypeMap.get(appointment.getType());

            if (!description.endsWith("Appointment")) {
                description += " Appointment";
            }

            conflict.setDescription(description);

            conflicts.add(conflict);
        }

        return conflicts;
    }

    public static Date extendHour(final Date date, final int amount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, amount);
        return calendar.getTime();
    }

    public static List<ScheduleConflict> searchHearings(final UserContext uc, final Long supervisionId, Date endDate, FacilityInternalLocationServiceLocal facilityInternalLocationService) {

        List<ScheduleConflict> conflicts = new ArrayList<ScheduleConflict>();
        OffenseInCustodySearchType searchType = new OffenseInCustodySearchType();
        searchType.setSupervisionId(supervisionId);

        List<OffenseInCustodyType> oics = DisciplineServiceAdapter.searchOffenseInCustody(uc, searchType, null, null, null).getOffenseInCustodies();

        for (OffenseInCustodyType oic : oics) {
            List<OffenseInCustodyHearingType> hearingList = oic.getOffenseInCustodyHearings();
            for (OffenseInCustodyHearingType hearing : hearingList) {
                if ("ACT".equalsIgnoreCase(hearing.getHearingStatus())) {
                    ScheduleConflict conflict = new ScheduleConflict();
                    conflict.setEventId(hearing.getId());
                    conflict.setSupervisionId(supervisionId);
                    conflict.setType(ScheduleConflict.EventType.HEARING);
                    conflict.setSubType(hearing.getHearingType());
                    conflict.setStartDate(hearing.getHearingDate());
                    if(endDate!=null){
                    	Calendar calendar = Calendar.getInstance();
                    	calendar.setTime(endDate);
                    	int hours = calendar.get(Calendar.HOUR_OF_DAY);
                    	int minutes = calendar.get(Calendar.MINUTE);
                    	calendar.setTime(hearing.getHearingDate());
                    	calendar.add(Calendar.HOUR, hours);
                    	calendar.add(Calendar.MINUTE, minutes);
                    	endDate = calendar.getTime();
                    	conflict.setEndDate(endDate);
                    } else{
                    	conflict.setEndDate(extendHour(hearing.getHearingDate(), 1));
                    }
                    
                    if (hearing.getLocationId() != null) {
                        FacilityInternalLocation retFIL = facilityInternalLocationService.get(uc, hearing.getLocationId());
                        if (retFIL != null) {
                            conflict.setLocation(retFIL.getDescription());
                        } else {
                            conflict.setLocation(null);
                        }
                    }
                    conflict.setDescription("Hearing");
                    conflicts.add(conflict);
                }
            }
        }
        return conflicts;
    }

    /**
     * Get Leaf Locations
     *
     * @param uc
     * @param parentId
     * @param category
     * @return
     */
    public static Set<Long> getLeafLocations(final UserContext uc, final Long parentId, final LocationCategory category, FacilityInternalLocationServiceLocal facilityInternalLocationService) {
        if (parentId == null || category == null) {
            return null;
        }
        List<InternalLocationIdentifierType> retFIL = facilityInternalLocationService.getLeafLocationIds(uc, parentId, category, true);

        Set<Long> svloc = new HashSet<Long>();
        for (InternalLocationIdentifierType lc : retFIL) {
            svloc.add(lc.getId());
        }
        return svloc;
    }

    /**
     * Get Direct Child Locations
     *
     * @param uc
     * @param category
     * @param parentId
     * @return
     */
    public static List<InternalLocationIdentifierType> getDirectChildLocations(final UserContext uc, final LocationCategory category, final Long parentId, FacilityInternalLocationServiceLocal facilityInternalLocationService) {
        if (parentId == null || category == null) {
            return null;
        }
        List<InternalLocationIdentifierType> loca = facilityInternalLocationService.getDirectChildLocationIds(uc, category, parentId, true);
        return loca != null ? loca : new ArrayList<InternalLocationIdentifierType>();
    }

    /**
     * Get Facility Internal Location
     *
     * @param uc
     * @param LocationId
     * @return
     */
    public static String getLocationCode(final UserContext uc, final Long LocationId, FacilityInternalLocationServiceLocal facilityInternalLocationService) {

        if (LocationId == null) {
            return null;
        }

        FacilityInternalLocation retFIL = facilityInternalLocationService.get(uc, LocationId);

        if (retFIL != null && retFIL.getLocationCode() != null) {
            return retFIL.getLocationCode();
        }

        return null;
    }

    /**
     * Get Facility Internal Location by Location Id
     *
     * @param uc
     * @param LocationId
     * @return
     */
    public static FacilityInternalLocation getFacilityInternalLocation(final UserContext uc, final Long LocationId, FacilityInternalLocationServiceLocal facilityInternalLocationService) {
        if (LocationId == null) {
            return null;
        }
        FacilityInternalLocation retFIL = facilityInternalLocationService.get(uc, LocationId);

        if (retFIL != null) {
            return retFIL;
        }
        return null;
    }

    /**
     * Get Current Housing Assignments by Locations
     *
     * @param uc
     * @param locationTos
     * @return
     */
    public static List<LocationHousingAssignmentType> getLocationHousingAssignments(final UserContext uc, final Set<Long> locationTos) {
        LocationHousingAssignmentsReturnType retHBA = HousingServiceAdapter.retrieveCurrentAssignmentsByLocations(uc, locationTos);
        if (retHBA == null) {
            String message = "getLocationHousingAssignments failed to retrieveCurrentAssignmentsByLocations";
            LogHelper.error(LOG, "getLocationHousingAssignments", message);
            throw new ArbutusRuntimeException(message);
        }
        return retHBA.getLocationHousingAssignments();
    }

    /**
     *
     * @param uc
     * @param supervisionIds
     * @return
     */
    public static Set<OffenderHousingAssignmentType> retrieveCurrentAssignmentsByOffenders(final UserContext uc, final Set<Long> supervisionIds) {
        Set<OffenderHousingAssignmentType> housingAssignments = new HashSet<>();

        OffenderHousingAssignmentsReturnType rtn = HousingServiceAdapter.retrieveCurrentAssignmentsByOffenders(uc, supervisionIds);
        if (rtn == null) {
            String message = String.format("search CurrentAssignmentsByOffenders failed, the supervisionIds=%s.", supervisionIds);
            throw new ArbutusRuntimeException(message);
        }
        if (rtn.getOffenderHousingAssignments() != null && !rtn.getOffenderHousingAssignments().isEmpty()) {
            housingAssignments.addAll(rtn.getOffenderHousingAssignments());
        }

        return housingAssignments;
    }

    /**
     *
     * @param supervisionId
     * @param housingAssignments
     * @return
     */
    public static Long identifyHousingLocationBySupervisonId(final Long supervisionId, final Set<OffenderHousingAssignmentType> housingAssignments) {
        Long rtn = null;

        for (OffenderHousingAssignmentType housingAssign : housingAssignments) {
            if (housingAssign.getSupervisionId().equals(supervisionId)) {
                rtn = housingAssign.getHousingAssignment().getLocationTo();
                break;
            }
        }

        return rtn;
    }

    /**
     *
     * @param uc
     * @param locationId
     * @return
     */
    public static String getLocationAddressById(final UserContext uc, final Long locationId) {
        Location location = new LocationServiceAdapter().get(uc, locationId);
        return combineAddress(location);
    }

    /**
     *
     * @param location
     * @return
     */
    public static String combineAddress(final Location location) {

        LocationAddressType locationAddress = location.getLocationAddress();
        StringBuilder address = new StringBuilder("");

        if (locationAddress != null) {
            if (null != locationAddress.getLocationStreetNumberText()) {
                address.append(locationAddress.getLocationStreetNumberText());
            }

            if (null != locationAddress.getLocationStreetNumSuffix()) {
                address.append(" ");
                address.append(locationAddress.getLocationStreetNumSuffix());
            }
            if (null != locationAddress.getLocationStreetName()) {
                address.append(" ");
                address.append(locationAddress.getLocationStreetName());
            }

            if (null != locationAddress.getLocationStreetCategory()) {
                address.append(" ");
                address.append(locationAddress.getLocationStreetCategory());
            }

            if (null != locationAddress.getLocationCityName()) {
                address.append(",");
                address.append(locationAddress.getLocationCityName());
            }
            if (null != locationAddress.getLocationStateCode()) {
                address.append(",");
                address.append(locationAddress.getLocationStateCode());
            }
            if (null != locationAddress.getLocationCountryName()) {
                address.append(" ");
                address.append(locationAddress.getLocationCountryName());
            }

            if (null != locationAddress.getLocationPostalCode()) {
                address.append(" ");
                address.append(locationAddress.getLocationPostalCode());
            }
        }
        return address.toString().trim();
    }

    public static List<ScheduleConflict> getConflictsOfferingSessions(final UserContext uc, final Long supervisionId, final Date startDate, final Date endDate) {

        ProgramAssignmentSearchType programAssignmentSearchType = new ProgramAssignmentSearchType();
        Set<String> assignmentStatus = new HashSet<String>();
        programAssignmentSearchType.setSupervisionId(supervisionId);
        assignmentStatus.add("ALLOCATED");
        programAssignmentSearchType.setStatus(assignmentStatus);
        List<ProgramAssignmentType> assignments = ProgramServiceAdapter.searchProgramAssignment(uc, null, programAssignmentSearchType, null, null,
                null).getProgramAssignments();
        if (assignments == null || assignments.size() == 0) {
            return null;
        }

        List<ScheduleConflict> conflicts = new ArrayList<ScheduleConflict>();
        for (ProgramAssignmentType assignment : assignments) {

            if (assignment.getProgrammOffering().getSingleOccurrence()) {
                ScheduleConflict conflict = new ScheduleConflict();
                conflict.setEventId(assignment.getAssignmentId());
                conflict.setType(ScheduleConflict.EventType.PROGRAM);
                ProgramOfferingType programOffering = assignment.getProgrammOffering();
                Date startDateTime = programOffering.getStartDate();
                Date endDateTime = BeanHelper.combineDateTime(programOffering.getEndDate(), programOffering.getEndTime());
                conflict.setStartDate(startDateTime);
                conflict.setEndDate(endDateTime);
                conflict.setLocation(assignment.getProgrammOffering().getLocationDescription());
                conflict.setSupervisionId(supervisionId);

                conflict.setDescription("Program: " + assignment.getProgrammOffering().getOfferingDescription());

                conflicts.add(conflict);
            } else {

                Date lookupDate = extendDate(startDate, 0);
                Date lookupEndDate = null;
                if (endDate == null) {
                    lookupEndDate = extendDate(startDate, 1);

                } else {
                    lookupEndDate = extendDate(endDate, 0);
                }

                TimeslotIterator iterator = ProgramServiceAdapter.getProgramTimeslot(uc, assignment, lookupDate, lookupEndDate,
                        assignment.getProgrammOffering().getEndTime(), null);
                if (iterator == null) {
                    continue;
                }
                while (iterator.hasNext()) {
                    ScheduleTimeslotType slot = iterator.next();

                    ScheduleConflict conflict = new ScheduleConflict();

                    conflict.setEventId(assignment.getAssignmentId());
                    conflict.setSupervisionId(supervisionId);
                    conflict.setStartDate(slot.getTimeslotStartDateTime());
                    conflict.setEndDate(slot.getTimeslotEndDateTime());
                    conflict.setType(ScheduleConflict.EventType.PROGRAM);
                    conflict.setLocation(assignment.getProgrammOffering().getLocationDescription());
                    conflict.setDescription("Program: " + assignment.getProgrammOffering().getOfferingDescription());

                    conflicts.add(conflict);
                }
            }
        }

        return conflicts;
    }

    public static Date extendDate(Date date, int amount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, amount);
        return calendar.getTime();
    }

    /**
     * Search Supervision
     *
     * @param uc
     * @param search
     * @return
     */
    public static List<SupervisionType> searchSupervision(final UserContext uc, final SupervisionSearchType search) {

        ValidationHelper.validate(search);

        SupervisionsReturnType ret = SupervisionServiceAdapter.search(uc, null, search, null, null, null);
        if (ret == null) {
            String message = "Search Supervision failed, the SupervisionSearchType=" + search.toString();
            LogHelper.error(LOG, "searchSupervision", message);
            throw new ArbutusRuntimeException(message);
        }
        return ret.getSupervisions();
    }

    public static List<ScheduleConflict> searchScheduledReleases(final UserContext uc, final Long supervisionId) {
        ScheduleReleaseType releaseType = SupervisionServiceAdapter.getConfirmedReleaseType(uc, supervisionId);
        if (null == releaseType) {
            return null;
        }
        List<ScheduleConflict> conflicts = new ArrayList<ScheduleConflict>();
        ScheduleConflict conflict = new ScheduleConflict();
        conflict.setEventId(releaseType.getScheduleReleaseTypeId());
        conflict.setType(ScheduleConflict.EventType.REL_EXTERNAL_MOVEMENT);
        conflict.setSupervisionId(supervisionId);
        conflict.setStartDate(releaseType.getScheduledReleaseDate());
        conflict.setDescription("Release");
        conflicts.add(conflict);
        return conflicts;
    }

    /**
     * @param userContext
     * @param supervisionIds
     * @return the supervions set by invoking SupervisionService.
     * @throws ArbutusRuntimeException
     */
    public static Set<SupervisionType> searchSupervisions(final UserContext userContext, final Set<Long> supervisionIds, final int searchMaxLimit) {
        Set<SupervisionType> supervisions = new HashSet<>();

        List<Set<Long>> list = createListOfLongSet(supervisionIds, searchMaxLimit);

        for (Set<Long> supIds : list) {
            SupervisionSearchType searchType = new SupervisionSearchType();
            searchType.setSupervisionStatusFlag(Boolean.TRUE);

            SupervisionsReturnType searchReturn = SupervisionServiceAdapter.search(userContext, supIds, searchType, 0L, 200L, null);
            if (searchReturn == null) {
                String message = String.format("searchSupervisions failed, searchType=%s.", searchType);
                throw new ArbutusRuntimeException(message);
            }
            if (searchReturn.getSupervisions() != null && !searchReturn.getSupervisions().isEmpty()) {
                supervisions.addAll(searchReturn.getSupervisions());
            }

            searchType.setSupervisionStatusFlag(Boolean.FALSE);
            searchReturn = SupervisionServiceAdapter.search(userContext, supIds, searchType, 0L, 200L, null);
            if (searchReturn == null) {
                String message = String.format("searchSupervisions failed, searchType=%s.", searchType);
                throw new ArbutusRuntimeException(message);
            }
            if (searchReturn.getSupervisions() != null && !searchReturn.getSupervisions().isEmpty()) {
                supervisions.addAll(searchReturn.getSupervisions());
            }
        }

        if (supervisions.isEmpty()) {
            throw new ArbutusRuntimeException(ErrorCodes.MOV_CANNOT_FIND_SUPERVISION);
        }

        return supervisions;
    }

    /**
     * @param supervisions
     * @param supervisionId
     * @return the supervision's associated FacilityIds
     */
    public static Set<Long> getSupervisionAssociatedFacilityIds(final Set<SupervisionType> supervisions, final Long supervisionId) {
        Set<Long> supervisionAssociatedFacilityIds = new HashSet<Long>();

        for (SupervisionType sup : supervisions) {
            if (sup.getSupervisionIdentification().equals(supervisionId)) {
                supervisionAssociatedFacilityIds.add(sup.getFacilityId());
                break;
            }
        }
        return supervisionAssociatedFacilityIds;
    }

    public static List<ScheduleConflict> searchVisits(final UserContext uc, final Long supervisionId, final String language, FacilityInternalLocationServiceLocal facilityInternalLocationService) {
        VisitSearchType search = new VisitSearchType();
        search.setOffenderSupervisionId(supervisionId);
        search.setStatus("SCHEDULED");
        List<VisitType> visits = VisitationServiceAdapter.searchVisits(uc, search, null, null, null).getVisits();
        if (visits.size() == 0) {
            return null;
        }
        Map<String, String> visitTypeMap = ReferenceDataServiceAdapter.getReferenceCodesDescMap(uc, "VISITTYPE", language);

        List<ScheduleConflict> conflicts = new ArrayList<ScheduleConflict>();
        Iterator<VisitType> iterator = visits.iterator();
        while (iterator.hasNext()) {
            VisitType visit = iterator.next();
            ScheduleConflict conflict = new ScheduleConflict();
            conflict.setEventId(visit.getVisitId());
            conflict.setSupervisionId(visit.getOffenderSupervisionId());
            conflict.setType(ScheduleConflict.EventType.VISIT);
            conflict.setSubType(visit.getVisitType());
            conflict.setStartDate(visit.getStartTime());
            conflict.setEndDate(visit.getEndTime());
            conflict.setToLocationId(visit.getLocationId());
            FacilityInternalLocation facilityInternalLocationType = MovementHelper.getFacilityInternalLocation(uc, visit.getLocationId(), facilityInternalLocationService);
            conflict.setLocation(facilityInternalLocationType.getDescription());
            String description = visitTypeMap.get(visit.getVisitType());

            if (!description.endsWith("Visit")) {
                description += " Visit";
            }

            conflict.setDescription(description);

            conflicts.add(conflict);
        }
        return conflicts;
    }

    public static MovementActivityDetailsType generateMovementActivityDetailsType(final Long supId, final Set<ActivityType> activities,
            final MovementActivityType mostResentMovementActivity) {

        MovementActivityDetailsType rtn = new MovementActivityDetailsType();
        rtn.setSupervisionIdentification(supId);

        Long actId = null;

        actId = mostResentMovementActivity.getActivityId();

        for (ActivityType act : activities) {
            if (act.getActivityIdentification().equals(actId)) {
                rtn.setActivityType(act);
                rtn.setMovementActivityType(mostResentMovementActivity);
                return rtn;
            }
        }

        return rtn;
    }

    public static Set<MovementActivityDetailsType> generateMovementActivityDetailsTypeSet(final Map<Long, Set<ActivityType>> supIdToActMap,
            final Set<MovementActivityType> movSet, final boolean onlyMapMostRecentMov) {
        Set<MovementActivityDetailsType> rtn = new HashSet<MovementActivityDetailsType>();

        for (Map.Entry<Long, Set<ActivityType>> entry : supIdToActMap.entrySet()) {
            Long supId = entry.getKey();
            Set<ActivityType> actSet = entry.getValue();

            Set<MovementActivityType> movSetNew = getAsscociatedMovementActivitiesFromActivities(actSet, movSet);

            if (onlyMapMostRecentMov) {
                MovementActivityType mostResentMov = identifyMostRecentMov(movSetNew);
                if (Boolean.TRUE.equals((mostResentMov instanceof MovementActivityType))) {
                    rtn.add(generateMovementActivityDetailsType(supId, actSet, mostResentMov));
                }
            } else {
                for (MovementActivityType movementActivity : movSetNew) {
                    rtn.add(generateMovementActivityDetailsType(supId, actSet, movementActivity));
                }
            }
        }

        return rtn;
    }

    public static Set<MovementDetailsType> generateMovementDetailsTypeSet(final Set<LastCompletedSupervisionMovementType> mostRecentMovements) {
        Set<MovementDetailsType> rtn = new HashSet<MovementDetailsType>();

        for (LastCompletedSupervisionMovementType supvisionMovement : mostRecentMovements) {
            Long supId = supvisionMovement.getSupervisionId();
            MovementActivityType mostResentMov = supvisionMovement.getLastCompletedMovementActivity();

            rtn.add(generateMovementDetailsType(supId, mostResentMov));
        }

        return rtn;
    }

    public static Set<OffenderLocationStatusType> generateOffenderLocationStatusTypeSet(final Set<SupervisionType> supervisions,
            final Set<OffenderHousingAssignmentType> housingAssignments, final Set<LastCompletedSupervisionMovementType> mostRecentMovements) {

        Set<OffenderLocationStatusType> rtn = new HashSet<OffenderLocationStatusType>();

        for (LastCompletedSupervisionMovementType mostRecentMovement : mostRecentMovements) {
            if (mostRecentMovement.getSupervisionId() == null || mostRecentMovement.getLastCompletedMovementActivity() == null) {
                continue;
            }

            Long supId = mostRecentMovement.getSupervisionId();

            Long currentHousingLoc = MovementHelper.identifyHousingLocationBySupervisonId(supId, housingAssignments);

            rtn.add(generateOffenderLocationStatusType(supId, mostRecentMovement.getLastCompletedMovementActivity(), currentHousingLoc,
                    MovementHelper.getSupervisionAssociatedFacilityIds(supervisions, supId)));

        }

        return rtn;
    }

    public static MovementDetailsType generateMovementDetailsType(final Long supId, final MovementActivityType mostResentMov) {

        MovementDetailsType rtn = new MovementDetailsType();
        rtn.setSupervisionIdentification(supId);
        rtn.setMovementCategory(mostResentMov.getMovementCategory());
        rtn.setMovementDirection(mostResentMov.getMovementDirection());

        if (mostResentMov instanceof ExternalMovementActivityType) {
            ExternalMovementActivityType externalMov = (ExternalMovementActivityType) mostResentMov;
            rtn.setToFacilityId(externalMov.getToFacilityId());
            rtn.setToFacilityInternalLocationId(externalMov.getToFacilityInternalLocationId());
        }

        if (mostResentMov instanceof InternalMovementActivityType) {
            InternalMovementActivityType internalMov = (InternalMovementActivityType) mostResentMov;
            rtn.setToFacilityInternalLocationId(internalMov.getToFacilityInternalLocationId());
        }

        LOG.debug("generateMovementDetailsType return:" + rtn);

        return rtn;
    }

    public static OffenderLocationStatusType generateOffenderLocationStatusType(final Long supId, final MovementActivityType mostResentMov, final Long currentHousingLoc,
            final Set<Long> supervisionFacilityReference) {

        OffenderLocationStatusType rtn = new OffenderLocationStatusType();
        rtn.setSupervisionIdentification(supId);
        rtn.setHousingLocationId(currentHousingLoc);

        if (mostResentMov instanceof ExternalMovementActivityType) {
            ExternalMovementActivityType externalMov = (ExternalMovementActivityType) mostResentMov;
            rtn.setCurrentFacilityId(externalMov.getToFacilityId());
            rtn.setCurrentAddressId(externalMov.getToLocationId());
            rtn.setCurrentOrganizationId(externalMov.getToOrganizationId());
            rtn.setCurrentCity(externalMov.getToCity());
            rtn.setCurrentProvinceState(externalMov.getToProvinceState());

            if ("IN".equalsIgnoreCase(externalMov.getMovementDirection()) && externalMov.getToFacilityId() != null &&
                    supervisionFacilityReference.contains(externalMov.getToFacilityId())) {
                rtn.setInOUtStatus(OffenderLocationStatusType.InOutStatus.IN.name());
                rtn.setCurrentInternalLocationId(externalMov.getToFacilityInternalLocationId());
            } else {
                rtn.setInOUtStatus(OffenderLocationStatusType.InOutStatus.OUT.name());
            }

        }

        if (mostResentMov instanceof InternalMovementActivityType) {
            InternalMovementActivityType internalMov = (InternalMovementActivityType) mostResentMov;
            rtn.setCurrentInternalLocationId(internalMov.getToFacilityInternalLocationId());
            rtn.setInOUtStatus(OffenderLocationStatusType.InOutStatus.IN.name());
        }

        LOG.debug("generateOffenderLocationStatusType return:" + rtn);

        return rtn;
    }

    @SuppressWarnings("rawtypes")
    public static Boolean isSetValid(final Set set, final Boolean required) {
        if (required && (set == null || set.isEmpty())) {
            return Boolean.FALSE;
        }

        if (!required && set == null) {
            return Boolean.TRUE;
        }

        return Boolean.TRUE;
    }

    /**
     * Get Transfer Wait List
     *
     * @param uc
     * @param facility
     * @param supervisionId
     * @param fromDateAdded
     * @param toDateAdded
     * @return
     */
    public static Boolean getTransferWaitlistsFound(final UserContext uc, final Long facility, final Long supervisionId, final Date fromDateAdded,
            final Date toDateAdded) {
        if (facility == null || supervisionId == null) {
            return null;
        }

        TransferWaitlistSearchType search = new TransferWaitlistSearchType();

        search.setFacilityId(facility);

        search.setSupervisionId(supervisionId);

        TransferWaitlistsReturnType ret = MovementServiceAdapter.searchTransferWaitlist(uc, search, 0L, 200L, null);

        if (ret.getTotalSize() > 0) {
            return true;
        }

        return false;
    }

    /**
     * Get Last Completed Movement Activity by Supervision
     *
     * @param uc
     * @param supervisions
     * @return
     */
    public static Set<ExternalMovementActivityType> getLastCompletedMovementBySupervision(final UserContext uc, final Set<Long> supervisions) {
        if (supervisions == null || supervisions.isEmpty()) {
            return null;
        }

        Set<LastCompletedSupervisionMovementType> movements = MovementServiceAdapter.getLastCompletedSupervisionMovementBySupervisions(uc, supervisions);

        Set<ExternalMovementActivityType> ret = new HashSet<ExternalMovementActivityType>();
        for (LastCompletedSupervisionMovementType ma : movements) {
            if (ma.getLastCompletedMovementActivity() instanceof ExternalMovementActivityType) {
                ExternalMovementActivityType ema = (ExternalMovementActivityType) ma.getLastCompletedMovementActivity();
                ret.add(ema);
            }
        }
        return ret;
    }

    /**
     * Get Last Completed Supervision Movement Activity by Locations
     *
     * @param uc
     * @param LocationIds
     * @return
     */
    public static Set<Long> getSupervisionByLocation(final UserContext uc, final Set<Long> LocationIds) {

        Set<Long> supervisions = new HashSet<Long>();

        if (LocationIds == null || LocationIds.isEmpty()) {
            return supervisions;
        }

        Set<Long> fil = new HashSet<Long>();
        for (Long locId : LocationIds) {
            fil.add(locId);
        }

        if (fil == null || fil.isEmpty()) {
            return supervisions;
        }

        Set<LastCompletedSupervisionMovementType> movements = MovementServiceAdapter.getLastCompletedSupervisionMovementByLocations(uc, LocationIds);

        for (LastCompletedSupervisionMovementType ma : movements) {
            Long sv = ma.getSupervisionId();
            supervisions.add(sv);
        }

        return supervisions;
    }

    public static List<ScheduleConflict> searchTransferMovement(final UserContext uc, final MovementActivitySearchType search) {
        List<ScheduleConflict> conflicts = new ArrayList<ScheduleConflict>();

        Iterator<ExternalMovementActivityType> iterator = MovementServiceAdapter.searchMovementActivity(uc, search).getExternalMovementActivity().iterator();
        while (iterator.hasNext()) {
            ExternalMovementActivityType move = iterator.next();
            ScheduleConflict conflict = new ScheduleConflict();
            conflict.setEventId(move.getMovementId());
            conflict.setSupervisionId(move.getSupervisionId());
            conflict.setType(ScheduleConflict.EventType.TRN_EXTERNAL_MOVEMENT);
            conflict.setStartDate(move.getMovementDate());
            if (move.getReportingDate() != null) {
                conflict.setEndDate(move.getReportingDate());
            }

            conflict.setToLocationId(move.getToFacilityId());
            conflict.setLocation(FacilityServiceAdapter.getFacility(uc, move.getToFacilityId()).getFacilityName());
            conflict.setDescription("Transfer");
            conflicts.add(conflict);
        }

        return conflicts;
    }

    /**
     * @param uc
     * @param movementActivityIds
     * @param fromActualDate
     * @param toActualDate
     * @param movementCategory
     * @param movementStatus
     * @param movementType
     * @param searchMaxLimit
     * @return the movement activities by invoking MovementService with search criteria.
     * @throws syscon.arbutus.product.services.core.exception.ArbutusException
     */
    public static Set<MovementActivityType> searchMovementActivities(final UserContext uc, final Set<Long> movementActivityIds, final Date fromActualDate,
            final Date toActualDate, final String movementCategory, final String movementStatus, final String movementType, final int searchMaxLimit) {

        Set<MovementActivityType> rtn = new HashSet<MovementActivityType>();

        List<Set<Long>> list = createListOfLongSet(movementActivityIds, searchMaxLimit);

        for (Set<Long> movActIds : list) {
            MovementActivitySearchType searchType = new MovementActivitySearchType();
            searchType.setFromMovementDate(fromActualDate);
            searchType.setToMovementDate(toActualDate);
            searchType.setMovementCategory(movementCategory);
            searchType.setMovementStatus(movementStatus);
            searchType.setMovementType(movementType);

            MovementActivitiesReturnType rtn1 = MovementServiceAdapter.searchMovementActivity(uc, movActIds, searchType);

            if (rtn1.getExternalMovementActivity() != null && !rtn1.getExternalMovementActivity().isEmpty()) {
                rtn.addAll(rtn1.getExternalMovementActivity());
            }
            if (rtn1.getInternalMovementActivity() != null && !rtn1.getInternalMovementActivity().isEmpty()) {
                rtn.addAll(rtn1.getInternalMovementActivity());
            }
        }

        return rtn;
    }

    /**
     * @param uc
     * @param movementActivityIds
     * @param fromActualDate
     * @param toActualDate
     * @param movementCategory
     * @param movementStatuses
     * @param movementType
     * @param searchMaxLimit
     * @return the movement activities by search criteria and whose Ids are in passed-in movementActivityIds .
     * @throws syscon.arbutus.product.services.core.exception.ArbutusException
     */
    public static Set<MovementActivityType> getMovementActivities(final UserContext uc, final Set<Long> movementActivityIds, final Date fromActualDate,
            final Date toActualDate, final String movementCategory, final Set<String> movementStatuses, final String movementType, final int searchMaxLimit) {

        if (movementActivityIds == null) {
            return null;
        }

        Set<MovementActivityType> movementActivities = new HashSet<MovementActivityType>();

        //Search MovementActivities by different search criteria.
        //If all search criteria are null, search all MovementActivities in the set of movementActivityIds.
        if (fromActualDate == null && toActualDate == null && movementCategory == null && movementStatuses == null && movementType == null) {
            movementActivities.addAll(searchMovementActivities(uc, movementActivityIds, null, null, Constants.MOVEMENT_CATEGORY_EXTERNAL, null, null, searchMaxLimit));
            movementActivities.addAll(searchMovementActivities(uc, movementActivityIds, null, null, Constants.MOVEMENT_CATEGORY_INTERNAL, null, null, searchMaxLimit));

        }
        //If the set of MovementStatuses is not null, loop each Movement Status and search.
        else if (movementStatuses != null && movementStatuses.size() > 0) {
            for (String movmentStatus : movementStatuses) {
                movementActivities.addAll(
                        searchMovementActivities(uc, movementActivityIds, fromActualDate, toActualDate, movementCategory, movmentStatus, movementType, searchMaxLimit));
            }
        }
        //If the set of MovementStatuses is null, search by the other search criterie.
        else {
            movementActivities.addAll(
                    searchMovementActivities(uc, movementActivityIds, fromActualDate, toActualDate, movementCategory, null, movementType, searchMaxLimit));
        }

        return movementActivities;
    }

    public static Set<Long> getMovementActivityIdsByActivityIds(final UserContext uc, final Set<Long> activityIds) {

        MovementActivitySearchType movementActivitySearch = new MovementActivitySearchType();
        movementActivitySearch.setActivityIds(activityIds);

        Set<Long> ret = new HashSet<Long>();

        MovementActivitiesReturnType rtn = MovementServiceAdapter.searchMovementActivity(uc, movementActivitySearch, null, null, null);
        List<ExternalMovementActivityType> externalMovements = rtn.getExternalMovementActivity();
        if (externalMovements != null && !externalMovements.isEmpty()) {
            for (ExternalMovementActivityType externalMovement : externalMovements) {
                ret.add(externalMovement.getMovementId());
            }
        }
        List<InternalMovementActivityType> internalMovements = rtn.getInternalMovementActivity();
        if (internalMovements != null && !internalMovements.isEmpty()) {
            for (InternalMovementActivityType internalMovement : internalMovements) {
                ret.add(internalMovement.getMovementId());
            }
        }

        return ret;
    }

    public static List<ScheduleConflict> getExternalMovementBySupervisionId(final UserContext uc, final Long supervisionId, final List<String> types) {

        List<ScheduleConflict> conflicts = MovementServiceAdapter.getExternalMovementBySupervisionId(uc, supervisionId, types);
        for (ScheduleConflict conflict : conflicts) {

            String location = "";
            if (null != conflict.getToFacilityId()) {
                location = FacilityServiceAdapter.getFacility(uc, conflict.getToFacilityId()).getFacilityName();
            }
            if (null != conflict.getToLocationId()) {
                location = MovementHelper.getLocationAddressById(uc, conflict.getToLocationId());
            }
            conflict.setLocation(location);

            if (conflict.getExternalMoveType().equalsIgnoreCase(MovementServiceBean.MovementType.CRT.code())) {
                conflict.setDescription("Court Appearance");
            }

            if (conflict.getExternalMoveType().equalsIgnoreCase(MovementServiceBean.MovementType.TAP.code())) {
                conflict.setDescription("Temporary Absence");
            }

        }
        return conflicts;
    }


    /**
     * @param movementActivities
     * @return the most recent movement activity in the given set of movement activities.
     */
    public static MovementActivityType identifyMostRecentMov(final Set<MovementActivityType> movementActivities) {
        MovementActivityType mostRecentMovementActivity = null;

        for (MovementActivityType mov : movementActivities) {
            if (mostRecentMovementActivity == null) {
                mostRecentMovementActivity = mov;
            }

            if (mov.getMovementDate().after(mostRecentMovementActivity.getMovementDate())) {
                mostRecentMovementActivity = mov;
            }
        }

        return mostRecentMovementActivity;
    }

    public static Set<MovementActivityType> getAsscociatedMovementActivitiesFromActivities(final Set<ActivityType> activitySet,
            final Set<MovementActivityType> movmentActivitySet) {
        Set<MovementActivityType> rtn = new HashSet<MovementActivityType>();

        Iterator<ActivityType> actIterator = activitySet.iterator();
        while (actIterator.hasNext()) {
            ActivityType act = actIterator.next();
            Iterator<MovementActivityType> movIterator = movmentActivitySet.iterator();
            while (movIterator.hasNext()) {
                boolean movfound = false;
                MovementActivityType mov = movIterator.next();
                if (mov.getActivityId().equals(act.getActivityIdentification())) {
                    rtn.add(mov);
                    movfound = true;
                }
                if (movfound) {
                    movIterator.remove();
                    break;
                }
            }
        }
        return rtn;
    }

    public static Set<Long> getAssociatedActivityIds(final Set<LastCompletedSupervisionMovementType> movements) {
        Set<Long> rtn = new HashSet<Long>();

        if (movements == null) {
            rtn = null;
        }

        for (LastCompletedSupervisionMovementType movement : movements) {

            Long id = movement.getLastCompletedMovementActivity().getActivityId();
            rtn.add(id);
        }

        return rtn;
    }

    public static Set<MovementActivityType> getMostRecentMovements(final Set<LastCompletedSupervisionMovementType> movements) {
        Set<MovementActivityType> rtn = new HashSet<MovementActivityType>();

        if (movements == null) {
            rtn = null;
        }

        for (LastCompletedSupervisionMovementType movement : movements) {
            MovementActivityType mov = movement.getLastCompletedMovementActivity();
            rtn.add(mov);
        }

        return rtn;
    }

    /**
     * @param set
     * @return a list which item is a Set<Long>.
     */
    private static List<Set<Long>> createListOfLongSet(final Set<Long> set, int searchMaxLimit) {
        List<Set<Long>> rtn = new ArrayList<Set<Long>>();

        List<Long> longList = new ArrayList<Long>(set);

        if (set.size() <= searchMaxLimit) {
            rtn.add(set);
            return rtn;
        }

        for (int i = 0; i < set.size(); i += searchMaxLimit) {
            Set<Long> setItem;
            if (set.size() - i > searchMaxLimit) {
                setItem = new HashSet<Long>(longList.subList(i, i + searchMaxLimit));
            } else {
                setItem = new HashSet<Long>(longList.subList(i, set.size()));
            }

            rtn.add(setItem);
        }

        return rtn;
    }

	public static void populateSupervisionIdinConflict(final UserContext uc,List<ScheduleConflict> conflicts) {
		List<Long> supervisionIds = new ArrayList<Long>();
		for (ScheduleConflict scheduleConflict : conflicts) {
			supervisionIds.add(scheduleConflict.getSupervisionId());
		}
		List<SupervisionType> supervisionTypes = SupervisionServiceAdapter.get(
				uc, supervisionIds);
		Map<Long, SupervisionType> supervisionMap = new HashMap<Long, SupervisionType>();
		for (SupervisionType supervisionType : supervisionTypes) {
			supervisionMap.put(supervisionType.getSupervisionIdentification(),supervisionType);
		}

		for (ScheduleConflict scheduleConflict : conflicts) {
			scheduleConflict.setSupervisionDisplayId(supervisionMap.get(scheduleConflict.getSupervisionId()).getSupervisionDisplayID());
		}

	}

    /**
     * Transform to MovementActivityEntity from MovementActivityType
     *
     * @param from <F extends MovementActivityType>
     * @return <T extends MovementActivityEntity
     */
    public static <F extends MovementActivityType, T extends MovementActivityEntity> T toMovementActivityEntity(F from, Class<T> clazz) {

        // null is just null
        if (from == null) {
            return null;
        }

        T to = null;
        try {
            to = clazz.newInstance();

            // Properties of MovementActivityType
            to.setMovementId(from.getMovementId());
            to.setActivityId(from.getActivityId());
            to.setSupervisionId(from.getSupervisionId());
            to.setMovementCategory(from.getMovementCategory());
            to.setMovementType(from.getMovementType());
            to.setMovementDirection(from.getMovementDirection());
            to.setMovementStatus(from.getMovementStatus());
            to.setMovementReason(from.getMovementReason());
            to.setMovementOutcome(from.getMovementOutcome());
            to.setMovementDate(from.getMovementDate());
            to.setGroupId(from.getGroupId());
            to.setApprovedByStaffId(from.getApprovedByStaffId());

            to.setApprovalDate(from.getApprovalDate());
            to.setApprovalComments(from.getApprovalComments());
            to.setStamp(from.getStamp());
            to.setVersion(from.getVersion());
            to.setIntermittentSenScheduleId(from.getIntermittentSenScheduleId());
            to.setCancelreason(from.getCancelReason());
            to.setParticipateStaffId(from.getParticipateStaffId());

            // Transform Set<String> to Set<PrivilegeEntity>
            // and Set<CommentType> to Set<CommentEntity>
            Set<CommentType> comments = from.getCommentText();
            if (comments != null && comments.size() > 0) {
                for (CommentType comment : comments) {
                    to.getCommentText().add(
                            new MovementActivityCommentEntity(comment.getCommentIdentification(), from.getMovementId(), comment.getCommentDate(),
                                    comment.getComment(), comment.getStamp(), comment.getVersion()));
                }
            }

            if (from instanceof ExternalMovementActivityType) {
                ExternalMovementActivityEntity toEx = (ExternalMovementActivityEntity) to;

                // Properties of ExternalMovementActivityType
                toEx.setFromLocationId(((ExternalMovementActivityType) from).getFromLocationId());
                toEx.setFromOrganizationId(((ExternalMovementActivityType) from).getFromOrganizationId());
                toEx.setFromFacilityId(((ExternalMovementActivityType) from).getFromFacilityId());
                toEx.setToLocationId(((ExternalMovementActivityType) from).getToLocationId());
                toEx.setToOrganizationId(((ExternalMovementActivityType) from).getToOrganizationId());
                toEx.setToFacilityId(((ExternalMovementActivityType) from).getToFacilityId());
                toEx.setArrestOrganizationId(((ExternalMovementActivityType) from).getArrestOrganizationId());
                toEx.setApplicationDate(((ExternalMovementActivityType) from).getApplicationDate());
                toEx.setEscortOrganizationId(((ExternalMovementActivityType) from).getEscortOrganizationId());
                toEx.setToInternalLocationId(((ExternalMovementActivityType) from).getToFacilityInternalLocationId());

                toEx.setFromCity(((ExternalMovementActivityType) from).getFromCity());
                toEx.setToCity(((ExternalMovementActivityType) from).getToCity());
                toEx.setToProvinceState(((ExternalMovementActivityType) from).getToProvinceState());
                toEx.setEscortDetails(((ExternalMovementActivityType) from).getEscortDetails());
                toEx.setReportingDate(((ExternalMovementActivityType) from).getReportingDate());

                toEx.setTransportation(((ExternalMovementActivityType) from).getTransportation());
                toEx.setEscapeId(((ExternalMovementActivityType) from).getEscapeId());

            }

            if (from instanceof InternalMovementActivityType) {
                InternalMovementActivityEntity toIn = (InternalMovementActivityEntity) to;

                toIn.setFromInternalLocationId(((InternalMovementActivityType) from).getFromFacilityInternalLocationId());
                toIn.setToInternalLocationId(((InternalMovementActivityType) from).getToFacilityInternalLocationId());
            }

        } catch (InstantiationException e) {
            throw new InvalidInputException(e.getMessage());
        } catch (IllegalAccessException e) {
            throw new InvalidInputException(e.getMessage());
        }

        return to;
    }

    public static <F extends MovementActivityEntity, T extends MovementActivityType> T toMovementActivityType(F from, Class<T> clazz) {

        // null is just null
        if (from == null) {
            return null;
        }

        T to = null;
        try {
            to = clazz.newInstance();

            // Properties of MovementActivityType
            to.setMovementIdentification(from.getMovementId());
            to.setActivityId(from.getActivityId());
            to.setSupervisionId(from.getSupervisionId());
            to.setMovementCategory(from.getMovementCategory());
            to.setMovementType(from.getMovementType());
            to.setMovementDirection(from.getMovementDirection());
            to.setMovementStatus(from.getMovementStatus());
            to.setMovementReason(from.getMovementReason());
            to.setMovementOutcome(from.getMovementOutcome());
            to.setMovementDate(from.getMovementDate());
            to.setStamp(from.getStamp());
            to.setVersion(from.getVersion());
            to.setCancelReason(from.getCancelreason());
            to.setParticipateStaffId(from.getParticipateStaffId());

            if (from.getApprovedByStaffId() != null) {
                to.setApprovedByStaffId(from.getApprovedByStaffId());
            }
            to.setApprovalDate(from.getApprovalDate());
            to.setApprovalComments(from.getApprovalComments());

            to.setGroupId(from.getGroupId());
            // Transform Set<CommentEntity> to Set<Comment>
            Set<MovementActivityCommentEntity> comments = from.getCommentText();
            if (comments != null && comments.size() > 0) {
                for (MovementActivityCommentEntity commentEntity : comments) {
                    to.getCommentText().add(toComment(commentEntity));
                }

            }

            // Properties of ExternalMovementActivityType
            if (from instanceof ExternalMovementActivityEntity) {
                ExternalMovementActivityType toEx = (ExternalMovementActivityType) to;

                toEx.setFromLocationId(((ExternalMovementActivityEntity) from).getFromLocationId());
                toEx.setFromOrganizationId(((ExternalMovementActivityEntity) from).getFromOrganizationId());
                toEx.setFromFacilityId(((ExternalMovementActivityEntity) from).getFromFacilityId());
                toEx.setFromCity(((ExternalMovementActivityEntity) from).getFromCity());
                toEx.setToFacilityInternalLocationId(from.getToInternalLocationId());
                toEx.setToLocationId(((ExternalMovementActivityEntity) from).getToLocationId());
                toEx.setToOrganizationId(((ExternalMovementActivityEntity) from).getToOrganizationId());
                toEx.setToFacilityId(((ExternalMovementActivityEntity) from).getToFacilityId());
                toEx.setToCity(((ExternalMovementActivityEntity) from).getToCity());
                toEx.setToProvinceState(((ExternalMovementActivityEntity) from).getToProvinceState());
                toEx.setArrestOrganizationId(((ExternalMovementActivityEntity) from).getArrestOrganizationId());
                toEx.setApplicationDate(((ExternalMovementActivityEntity) from).getApplicationDate());
                toEx.setEscortOrganizationId(((ExternalMovementActivityEntity) from).getEscortOrganizationId());
                toEx.setEscortDetails(((ExternalMovementActivityEntity) from).getEscortDetails());
                toEx.setReportingDate(((ExternalMovementActivityEntity) from).getReportingDate());
                toEx.setTransportation(((ExternalMovementActivityEntity) from).getTransportation());
                to.setEscapeId(((ExternalMovementActivityEntity) from).getEscapeId());

            }

            // Properties of InternalMovementActivityType
            if (from instanceof InternalMovementActivityEntity) {
                InternalMovementActivityType toIn = (InternalMovementActivityType) to;

                toIn.setFromFacilityInternalLocationId(((InternalMovementActivityEntity) from).getFromInternalLocationId());
                toIn.setToFacilityInternalLocationId(from.getToInternalLocationId());
            }
            to.setIntermittentSenScheduleId(from.getIntermittentSenScheduleId());

        } catch (InstantiationException e) {
            throw new InvalidInputException(e.getMessage());
        } catch (IllegalAccessException e) {
            throw new InvalidInputException(e.getMessage());
        }
        return to;
    }

    /**
     * Implement method apply which is inherited from Transform interface
     *
     * @param from CommentEntity
     * @return to CommentType
     */
    private static CommentType toComment(MovementActivityCommentEntity from) {

        // null is just null
        if (from == null) {
            return null;
        }

        CommentType to = new CommentType();
        to.setCommentIdentification(from.getCommentIdentification());
        to.setComment(from.getCommentText());
        to.setCommentDate(from.getCommentDate());
        to.setStamp(from.getStamp());
        to.setVersion(from.getVersion());

        return to;
    }

    /**
     * To type case Movement Activity Entity to Type.
     *
     * @param mae {@link MovementActivityEntity} instance
     * @return {@link MovementActivityType} instance
     */
    public static MovementActivityType toMovementActivityType(MovementActivityEntity mae) {
        MovementActivityType ret = null;
        if (mae instanceof ExternalMovementActivityEntity) {
            ret = toMovementActivityType(mae, ExternalMovementActivityType.class);
        } else if (mae instanceof InternalMovementActivityEntity) {
            ret = toMovementActivityType(mae, InternalMovementActivityType.class);
        } else {
            ret = toMovementActivityType(mae, MovementActivityType.class);
        }

        return ret;
    }

    public static InternalMovementActivityType buildInternalMovement(InternalMovement im, String direction, String status) {

        InternalMovementActivityType movementActivity = new InternalMovementActivityType();
        if (im.getComment()!=null && !im.getComment().isEmpty()) {
            CommentType temp = new CommentType(null, new Date(), im.getComment());
            Set<CommentType> comments = new HashSet<CommentType>();
            comments.add(temp);
            movementActivity.setCommentText(comments);
        }
        if (status.equalsIgnoreCase(MovementActivityType.MovementStatus.COMPLETED.code())) {
            movementActivity.setMovementDate(DateUtil.combineDateTime(im.getMoveDate(), im.getMoveTime()));
        }

        // Set supervision
        movementActivity.setSupervisionId(im.getSupervisionId());
        movementActivity.setMovementCategory(MovementActivityType.MovementCategory.INTERNAL.value());
        movementActivity.setMovementType(MovementActivityType.MovementType.APP.code());
        movementActivity.setMovementStatus(status);
        movementActivity.setMovementReason(im.getMoveReason());
        movementActivity.setMovementDirection(direction);
        movementActivity.setFromFacilityInternalLocationId(im.getFromLocationId());
        movementActivity.setToFacilityInternalLocationId(im.getToLocationId());
        return movementActivity;
    }


    public static ActivityType buildInternalActivity(InternalMovement im, String status) {
        ActivityType activity = new ActivityType();

        activity.setActivityCategory(MOVEMENT_CATERGORY);
        activity.setSupervisionId(im.getSupervisionId());
        if (status.equalsIgnoreCase(MovementActivityType.MovementStatus.PENDING.code())) {
            activity.setActiveStatusFlag(true);

        } else {
            //complete
            activity.setActiveStatusFlag(false);

        }

        Date plannedStartDate = DateUtil.combineDateTime(im.getMoveDate(), im.getMoveTime());
        Date plannedEndDate = DateUtil.futureDateTime(plannedStartDate);

        activity.setPlannedStartDate(plannedStartDate);
        activity.setPlannedEndDate(plannedEndDate);

        return activity;
    }

    public static ActivityType buildInternalActivity(Long supervisionID, Long facilityID, Date moveDate, Date moveTime,
            Long activityAntecedentReference, Set<Long> activityDescendantsReference, String status) {

        if (supervisionID == null || facilityID == null || moveDate == null || moveTime == null) {
            throw new InvalidInputException("Invalid parameters passed in to buildInternalActivity.");
        }

        ActivityType activity = new ActivityType();

        activity.setActivityCategory(MOVEMENT_CATERGORY);
        activity.setSupervisionId(supervisionID);
        if (status.equalsIgnoreCase(MovementActivityType.MovementStatus.PENDING.code())) {
            activity.setActiveStatusFlag(true);

        } else {
            //complete
            activity.setActiveStatusFlag(false);

        }


        // Planned end date is slight later then start end to pass validation
        Date plannedStartDate = DateUtil.combineDateTime(moveDate, moveTime);
        Date plannedEndDate = DateUtil.futureDateTime(plannedStartDate);

        activity.setPlannedStartDate(plannedStartDate);
        activity.setPlannedEndDate(plannedEndDate);

        // Set ActivityAntecedentAssociation
        if (activityAntecedentReference != null) {
            activity.setActivityAntecedentId(activityAntecedentReference);
        }

        // Set ActivityDescendantAssociations
        if (activityDescendantsReference != null) {
            activity.setActivityDescendantIds(activityDescendantsReference);
        }


        return activity;
    }

    /**
     *
     *
     * @param entity
     * @return
     */
    public static EscapeRecapture toEscapeRecapture(EscapeRecaptureEntity entity) {

        EscapeRecapture dto = new EscapeRecapture();
        dto.setEscapeRecaptureId(entity.getEscapeRecaptureId());
        dto.setSupervisionId(entity.getSupervisionId());
        dto.setEscapeFacility(entity.getEscapeFacility());
        dto.setEscapeReason(entity.getEscapeReason());
        dto.setEscapeDate(DateUtil.getDateWithoutTime(entity.getEscapeDate()));
        dto.setEscapeTime(DateUtil.getTimeWithoutDate(entity.getEscapeDate()));
        dto.setFromCustody(entity.getFromCustody());
        dto.setSecurityBreached(entity.getSecurityBreached());
        dto.setCircumstance(entity.getCircumstance());
        dto.setLastSeenDate(DateUtil.getDateWithoutTime(entity.getLastSeenDate()));
        dto.setLastSeenTime(DateUtil.getTimeWithoutDate(entity.getLastSeenDate()));
        dto.setIncidentNumber(entity.getIncidentNumber());
        dto.setEscapeComments(entity.getEscapeComments());
        dto.setRecaptureDate(DateUtil.getDateWithoutTime(entity.getRecaptureDate()));
        dto.setRecaptureTime(DateUtil.getTimeWithoutDate(entity.getRecaptureDate()));
        dto.setArrestingOrganization(entity.getArrestingOrganization());
        dto.setRecaptureComments(entity.getRecaptureComments());
        dto.setReadmissionDate(DateUtil.getDateWithoutTime(entity.getReadmissionDate()));
        dto.setReadmissionTime(DateUtil.getTimeWithoutDate(entity.getReadmissionDate()));
        dto.setReadmissionFacility(entity.getReadmissionFacility());
        dto.setReadmissionReason(entity.getReadmissionReason());
        dto.setReadmissionComment(entity.getReadmissionComment());
        //
        return dto;
    }

    /**
     *
     * @param escapeOffenderEntity
     * @param readmissionMovementActivityEntity
     * @return
     */
    public static EscapeRecapture toEscapeRecapture(EscapeOffenderEntity escapeOffenderEntity, MovementActivityEntity readmissionMovementActivityEntity) {

        EscapeRecapture dto = new EscapeRecapture();

        dto.setEscapeRecaptureId(escapeOffenderEntity.getEscapeId());
        dto.setSupervisionId(escapeOffenderEntity.getMovementActivity().getSupervisionId());
        dto.setEscapeFacility(escapeOffenderEntity.getMovementActivity().getFromFacilityId());
        dto.setEscapeReason(escapeOffenderEntity.getMovementActivity().getMovementReason());
        dto.setEscapeDate(DateUtil.getDateWithoutTime(escapeOffenderEntity.getMovementActivity().getMovementDate()));
        dto.setEscapeTime(DateUtil.getTimeWithoutDate(escapeOffenderEntity.getMovementActivity().getMovementDate()));
        dto.setFromCustody(escapeOffenderEntity.getFromCustody());
        dto.setSecurityBreached(escapeOffenderEntity.getSecurityLevel());
        dto.setCircumstance(escapeOffenderEntity.getCircumstance());
        dto.setLastSeenDate(DateUtil.getDateWithoutTime(escapeOffenderEntity.getLastSeenDate()));
        dto.setLastSeenTime(DateUtil.getTimeWithoutDate(escapeOffenderEntity.getLastSeenDate()));
        dto.setIncidentNumber(escapeOffenderEntity.getIncidentNumber());
        dto.setEscapeComments(escapeOffenderEntity.getEscapeComments());
        dto.setRecaptureDate(DateUtil.getDateWithoutTime(escapeOffenderEntity.getRecapturedDate()));
        dto.setRecaptureTime(DateUtil.getTimeWithoutDate(escapeOffenderEntity.getRecapturedDate()));
        dto.setArrestingOrganization(escapeOffenderEntity.getRecaptureByOrgId());
        dto.setRecaptureComments(escapeOffenderEntity.getRecaptureComments());
        dto.setReadmissionDate(DateUtil.getDateWithoutTime(null));
        dto.setReadmissionTime(DateUtil.getTimeWithoutDate(null));
        dto.setReadmissionFacility(null);
        dto.setReadmissionReason(null);
        dto.setReadmissionComment(null);
        dto.setVersion(escapeOffenderEntity.getVersion());

        //
        return dto;
    }

    /**
     *
     * @param dto
     * @return
     */
    public static EscapeOffenderEntity toEscapeOffenderEntity(EscapeRecapture dto) {

        EscapeOffenderEntity entity = new EscapeOffenderEntity();
        entity.setEscapeId(dto.getEscapeRecaptureId());
        entity.setFromCustody(dto.getFromCustody());
        entity.setCircumstance(dto.getCircumstance());
        entity.setSecurityLevel(dto.getSecurityBreached());
        entity.setIncidentNumber(dto.getIncidentNumber());
        entity.setLastSeenDate(DateUtil.combineDateTime(dto.getLastSeenDate(),dto.getLastSeenTime()));
        entity.setAdjustSentence(false);
        entity.setEscapeComments(dto.getEscapeComments());
        entity.setRecapturedDate(DateUtil.combineDateTime(dto.getRecaptureDate(),dto.getRecaptureTime()));
        entity.setRecaptureByOrgId(dto.getArrestingOrganization());
        entity.setRecaptureComments(dto.getRecaptureComments());
        entity.setVersion(dto.getVersion());

        //
        return entity;
    }

    /**
     *
     * @param dto
     * @return
     */
    public static ActivityEntity toActivityEntity(EscapeRecapture dto) {

        ActivityEntity entity = new ActivityEntity();
        entity.setSupervisionId(dto.getSupervisionId());
        entity.setActiveStatusFlag(true);
        entity.setActivityCategory(ActivityCategory.MOVEMENT.value());
        entity.setPlannedStartDate(DateUtil.combineDateTime(dto.getEscapeDate(), dto.getEscapeTime()));
        entity.setPlannedEndDate(entity.getPlannedStartDate());
        //
        return entity;
    }

    /**
     *
     * @param dto
     * @return
     */
    public static ActivityType toActivityType(EscapeRecapture dto) {

        ActivityType out = new ActivityType();
        out.setSupervisionId(dto.getSupervisionId());
        out.setActiveStatusFlag(false); // Completed
        out.setActivityCategory(ActivityCategory.MOVEMENT.value());
        out.setPlannedStartDate(DateUtil.combineDateTime(dto.getEscapeDate(), dto.getEscapeTime()));
        out.setPlannedEndDate(out.getPlannedStartDate());
        //
        return out;
    }

    /**
     *
     * @param dto
     * @return
     */
    public static ExternalMovementActivityEntity toExternalMovementActivityEntity(EscapeRecapture dto) {

        ExternalMovementActivityEntity entity = new ExternalMovementActivityEntity();
        entity.setMovementType(MovementServiceBean.MovementType.ESCP.code());
        entity.setSupervisionId(dto.getSupervisionId());
        entity.setMovementCategory(MovementServiceBean.MovementCategory.EXTERNAL.code());
        entity.setMovementDirection(MovementServiceBean.MovementDirection.OUT.code());
        entity.setMovementStatus(MovementServiceBean.MovementStatus.COMPLETED.code());
        entity.setMovementReason(dto.getEscapeReason());
        entity.setMovementDate(DateUtil.combineDateTime(dto.getEscapeDate(), dto.getEscapeTime()));
        return entity;
    }


    public static ExternalMovementActivityType  createExternalMovementActivity(Long activityID, ExternalMovement exMov, String status) {
        ExternalMovementActivityType movementActivity = new ExternalMovementActivityType();
        //WOR-6454: MovementDate should not be filled during schedule, only when actualized.
        // Cleaning up the movementDate after used for activity creation
        //movementActivity.setMovementDate(DateUtil.combineDateTime(exMov.getMoveDate(), exMov.getMoveTime()));

        if (status.equalsIgnoreCase(MovementActivityType.MovementStatus.PENDING.code())) {
            movementActivity.setMovementDate(null);
        }

        if (status.equalsIgnoreCase(MovementActivityType.MovementStatus.COMPLETED.code())) {
            movementActivity.setMovementDate(DateUtil.combineDateTime(exMov.getMoveDate(), DateUtil.futureDateTime(exMov.getMoveTime())));
        }



        if (null != exMov.getExpectedArrivalDate() && exMov.getExpectedArrivalTime() != null) {
            movementActivity.setReportingDate(DateUtil.combineDateTime(exMov.getExpectedArrivalDate(), exMov.getExpectedArrivalTime()));
        } else {
            movementActivity.setReportingDate(exMov.getExpectedArrivalDate());
        }



        movementActivity.setMovementCategory(MovementActivityType.MovementCategory.EXTERNAL.value());
        movementActivity.setMovementType(MovementActivityType.MovementType.TRNIJ.code());
        movementActivity.setMovementDirection(MovementActivityType.MovementDirection.OUT.code());
        movementActivity.setMovementStatus(status);
        movementActivity.setMovementReason(exMov.getMoveReasonCd());
        movementActivity.setMovementOutcome(exMov.getMoveOutcomeCd());
        movementActivity.setEscortDetails(exMov.getEscortDetails());
        /* from facility */
        Long fromFacilityAssociation = exMov.getFromFacility();
        movementActivity.setFromFacilityId(fromFacilityAssociation);
        /* escort organization */
        if (exMov.getEscortOrg() != null && exMov.getEscortOrg() != 0) {
            Long escortOrganizationAssociation = exMov.getEscortOrg();
            movementActivity.setEscortOrganizationId(escortOrganizationAssociation);
        }
        /* toFacility */

        Long toFacilityAssociation = exMov.getToFacility();
        movementActivity.setToFacilityId(toFacilityAssociation);

		/* activity */
        movementActivity.setActivityId(activityID);
        /* supervision */

        movementActivity.setSupervisionId(exMov.getSupervisionId());

        return movementActivity;
    }

    public static void setMovementCommentText(Long userId, String comment, MovementActivityType movementActivity) {

        if (comment != null && !comment.isEmpty()) {
            CommentType temp = new CommentType(null, userId == null? null : userId.toString(), new Date(), comment);
            Set<CommentType> comments = movementActivity.getCommentText();
            if (comments == null) {
                comments = new HashSet<CommentType>();
            }
            comments.add(temp);
            movementActivity.setCommentText(comments);
        }
    }

    public static List<EscapeRecapture> toEscapeOffender(List<EscapeOffenderEntity> entities) {
        if(entities.isEmpty()){
            return new ArrayList<>();
        }
        List<EscapeRecapture> list = new ArrayList<>();
        for(EscapeOffenderEntity from : entities){
            EscapeRecapture to = toEscapeOffender(from);
            list.add(to);
        }
        return list;
    }

    public static EscapeRecapture toEscapeOffender(EscapeOffenderEntity from) {
        if (from == null) {
            return null;
        }
        EscapeRecapture to = new EscapeRecapture();
        to.setEscapeRecaptureId(from.getEscapeId());
        to.setSupervisionId(from.getMovementActivity().getSupervisionId());
        to.setEscapeReason(from.getMovementActivity().getMovementReason());
        to.setEscapeFacility(from.getMovementActivity().getFromFacilityId());
        to.setEscapeDate(from.getMovementActivity().getMovementDate());
        to.setEscapeTime(from.getMovementActivity().getMovementDate());
        to.setRecaptureDate(from.getRecapturedDate());
        to.setRecaptureTime(from.getRecapturedDate());
        to.setArrestingOrganization(from.getRecaptureByOrgId());
        to.setRecaptureComments(from.getRecaptureComments());
        to.setFromCustody(from.getFromCustody());
        to.setSecurityBreached(from.getSecurityLevel());
        to.setCircumstance(from.getCircumstance());
        to.setLastSeenDate(from.getLastSeenDate());
        to.setLastSeenTime(from.getLastSeenDate());
        to.setIncidentNumber(from.getIncidentNumber());
        to.setEscapeComments(from.getEscapeComments());
        if(from.getReadmissionMovement()!=null) {
            to.setReadmissionDate(from.getReadmissionMovement().getMovementDate());
            to.setReadmissionTime(from.getReadmissionMovement().getMovementDate());
            to.setReadmissionFacility(from.getReadmissionMovement().getToFacilityId());
            to.setReadmissionReason(from.getReadmissionMovement().getMovementReason());
            if (from.getReadmissionMovement().getCommentText() != null && !from.getReadmissionMovement().getCommentText().isEmpty()) {
                MovementActivityCommentEntity comment = from.getReadmissionMovement().getCommentText().iterator().next();
                to.setReadmissionComment(comment.getCommentText());
            }
        }

        to.setVersion(from.getVersion());
        return to;
    }

    /**
     *
     * @param dto
     * @return
     */
    public static TemplateConfigurationEntity toCategoryConfigurationEntity(MovementCategoryConfiguration dto) {

        TemplateConfigurationEntity entity = new TemplateConfigurationEntity();
        CategoryConfigurationPK entityPK = new CategoryConfigurationPK(dto.getMovementType(), dto.getMovementReason());
        entity.setCategoryConfigurationPK(entityPK);
        entity.setMovementCategory(dto.getMovementCategory());
        entity.setActive(dto.isActive());
        entity.setVersion(dto.getVersion());
        entity.setMovementTemplate(dto.getMovementTemplate());
        entity.setRequireApproval(dto.isRequireApproval());

        //
        return entity;
    }

    /**
     *
     * @param entity
     * @return
     */
    public static MovementCategoryConfiguration toCategoryConfiguration(TemplateConfigurationEntity entity) {

        MovementCategoryConfiguration dto = new MovementCategoryConfiguration();
        dto.setMovementType(entity.getCategoryConfigurationPK().getMovementType());
        dto.setMovementReason(entity.getCategoryConfigurationPK().getMovementReason());
        dto.setMovementCategory(entity.getMovementCategory());
        dto.setActive(entity.getActive());
        dto.setVersion(entity.getVersion());
        dto.setMovementTemplate(entity.getMovementTemplate());
        dto.setRequireApproval(entity.getRequireApproval());
        //
        return dto;
    }

    /**
     *
     * @param entityList
     * @return
     */
    public static List<MovementCategoryConfiguration> toCategoryConfigurationList(List<TemplateConfigurationEntity> entityList) {

        List<MovementCategoryConfiguration> ret = new ArrayList<MovementCategoryConfiguration>();
        if (entityList == null) {
            return ret;
        }

        //
        for (TemplateConfigurationEntity entity : entityList) {
            ret.add(toCategoryConfiguration(entity));
        }

        //
        return ret;
    }

}
