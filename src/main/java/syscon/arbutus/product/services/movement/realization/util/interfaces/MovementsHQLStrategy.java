package syscon.arbutus.product.services.movement.realization.util.interfaces;

import java.util.Date;

import org.hibernate.Query;
import org.hibernate.Session;
import syscon.arbutus.product.services.movement.contract.dto.TwoMovementsSearchType;

/**
 * Movement HQL Strategy interface
 *
 * @author wmadruga
 */
public interface MovementsHQLStrategy {

    public Query buildTwoMovementsHQL(Session session, TwoMovementsSearchType searchType, Date dateOut, Date dateIn);

}
