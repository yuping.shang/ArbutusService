package syscon.arbutus.product.services.movement.realization.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.envers.Audited;

/**
 * ExternalMovementActivityEntity for Movement Activity Service
 *
 * @author yshang, Syscon Justice Systems
 * @version 1.0
 * @DbComment MA_MovementActivity 'Main table for Movement Activity Service'
 * @since September 27, 2012
 */
@Audited
@Entity
public class InternalMovementActivityEntity extends MovementActivityEntity {

    private static final long serialVersionUID = -7696670907627518855L;

    /**
     * @DbComment MA_MovementActivity.fromInternalLocationId 'From Internal location association for an internal movement. Association to Coming From Internal Location'
     */
    @Column(name = "fromInternalLocationId", nullable = true)
    private Long fromInternalLocationId;

    /**
     * Constructor
     */
    public InternalMovementActivityEntity() {
        super();
    }

    /**
     * @return the fromInternalLocationId
     */
    public Long getFromInternalLocationId() {
        return fromInternalLocationId;
    }

    /**
     * @param fromInternalLocationId the fromInternalLocationId to set
     */
    public void setFromInternalLocationId(Long fromInternalLocationId) {
        this.fromInternalLocationId = fromInternalLocationId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((fromInternalLocationId == null) ? 0 : fromInternalLocationId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        InternalMovementActivityEntity other = (InternalMovementActivityEntity) obj;
        if (fromInternalLocationId == null) {
            if (other.fromInternalLocationId != null) {
                return false;
            }
        } else if (!fromInternalLocationId.equals(other.fromInternalLocationId)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "InternalMovementActivityEntity [fromInternalLocationId=" + fromInternalLocationId + ", toString()=" + super.toString() + "]";
    }

}