package syscon.arbutus.product.services.movement.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Indexed;
import syscon.arbutus.product.services.core.common.StampInterface;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.core.services.realization.BaseEntity;
import syscon.arbutus.product.services.realization.model.MetaCode;

/**
 * EscapeOffenderEntity for Supervision Service 1.0
 *
 * @author abatra
 * @version 1.0
 * @DbComment MA_ESCAPE 'Table for Escape details'
 * @since Sep 5, 2014
 */
@Audited
@Entity
@Indexed
@Table(name = "MA_ESCAPE")
@SQLDelete(sql = "UPDATE MA_ESCAPE SET flag = 4 WHERE escapeIdentification = ? and version = ?")
@Where(clause = "flag = 1")
public class EscapeOffenderEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 6732094848477968564L;

    /**
     * @DbComment 'Escape ID'
     */
    @Id
    @DocumentId
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_MA_ESCAPE_ID")
    @SequenceGenerator(name = "SEQ_MA_ESCAPE_ID", sequenceName = "SEQ_MA_ESCAPE_ID", allocationSize = 1)
    @Column(name = "escapeId")
    private Long escapeId;

    /**
     * @DbComment movementId 'The movement Id of the Release Inmate movement on
     * Escape'
     */
    @Column(name = "movementId", nullable = false)
    private Long movementId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "movementId", referencedColumnName = "moveActId", insertable = false, updatable = false)
    private ExternalMovementActivityEntity movementActivity;

    /**
     * @DbComment custody 'The inmate escaped under custody of police, staff etc.'
     */
    @Column(name = "custody", length = 64)
    @MetaCode(set = MetaSet.ESCAPE_CUSTODY)
    private String fromCustody;

    /**
     * @DbComment circumstance 'The inmate escaped under circumstance like institution escape or secure facility'
     */
    @Column(name = "circumstance", length = 64)
    @MetaCode(set = MetaSet.ESCAPE_CIRCUMSTANCE)
    private String circumstance;

    /**
     * @DbComment securityLevel 'The Security Level breached by inmate - maximum, minimum or medium'
     */
    @Column(name = "securityLevel", length = 64)
    @MetaCode(set = MetaSet.SECURITY_LEVEL)
    private String securityLevel;

    /**
     * @DbComment incidentNumber 'The incident number for escape'
     */
    @Column(name = "incidentNumber", length = 64)
    private String incidentNumber;

    /**
     * @DbComment lastSeen 'The date when inmate was last seen'
     */
    @Column(name = "lastSeen")
    private Date lastSeenDate;

    /**
     * @DbComment adjustSentence 'The sentence of the inmate adjusted or not'
     */
    @Column(name = "adjustSentence")
    private Boolean adjustSentence;

    /**
     * @DbComment escapeComments 'Escape Comments'
     */
    @Column(name = "escapeComments", length = 2048)
    private String escapeComments;


    @Column(name = "recapturedDate")
    private Date recapturedDate;

    @Column(name = "recaptureByOrgId")
    private Long recaptureByOrgId;

    /**
     * @DbComment recaptureComments 'Recapture Comments'
     */
    @Column(name = "recaptureComments", length = 2048)
    private String recaptureComments;


    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "escapeOffender")
    private ExternalMovementActivityEntity readmissionMovement;


    public EscapeOffenderEntity() {

    }

    public EscapeOffenderEntity(Long escapeId, Long movementId, String fromCustody, String circumstance,
                                String securityLevel, String incidentNumber, Date lastSeenDate, Boolean adjustSentence,
                                String escapeComments) {
        this.escapeId = escapeId;
        this.movementId = movementId;
        this.fromCustody = fromCustody;
        this.circumstance = circumstance;
        this.securityLevel = securityLevel;
        this.incidentNumber = incidentNumber;
        this.lastSeenDate = lastSeenDate;
        this.adjustSentence = adjustSentence;
        this.escapeComments = escapeComments;

    }

    public EscapeOffenderEntity(Long escapeId, String fromCustody, String circumstance, String securityLevel,
                                String incidentNumber, Date lastSeenDate, Boolean adjustSentence, String escapeComments,
                                Long version, StampInterface stamp) {

        this.escapeId = escapeId;
        this.fromCustody = fromCustody;
        this.circumstance = circumstance;
        this.securityLevel = securityLevel;
        this.incidentNumber = incidentNumber;
        this.lastSeenDate = lastSeenDate;
        this.adjustSentence = adjustSentence;
        this.escapeComments = escapeComments;
        this.setVersion(version);
        this.setStamp(stamp);
    }

    @Override
    public Serializable getId() {
        return getEscapeId();
    }

    /**
     * Gets the value of escapeId
     *
     * @return Long
     */
    public Long getEscapeId() {
        return escapeId;
    }

    /**
     * Sets the value of escapeId
     *
     * @param escapeId
     */
    public void setEscapeId(Long escapeId) {
        this.escapeId = escapeId;
    }

    /**
     *
     * @return
     */
    public Long getMovementId() {
        return movementId;
    }

    /**
     *
     * @param movementId
     */
    public void setMovementId(Long movementId) {
        this.movementId = movementId;
    }

    /**
     *
     * @return
     */
    public ExternalMovementActivityEntity getMovementActivity() {
        return movementActivity;
    }

    /**
     *
     * @param movementActivity
     */
    public void setMovementActivity(ExternalMovementActivityEntity movementActivity) {
        this.movementActivity = movementActivity;
    }

    /**
     * Gets the value of fromCustody
     *
     * @return String
     */
    public String getFromCustody() {
        return fromCustody;
    }

    /**
     * Sets the value of fromCustody
     *
     * @param fromCustody
     */
    public void setFromCustody(String fromCustody) {
        this.fromCustody = fromCustody;
    }

    /**
     * Gets the value of circumstance
     *
     * @return String
     */
    public String getCircumstance() {
        return circumstance;
    }

    /**
     * Sets the value of circumstance
     *
     * @param circumstance
     */
    public void setCircumstance(String circumstance) {
        this.circumstance = circumstance;
    }

    /**
     * Gets the value of securityLevel
     *
     * @return String
     */
    public String getSecurityLevel() {
        return securityLevel;
    }

    /**
     * Sets the value of securityLevel
     *
     * @param securityLevel
     */
    public void setSecurityLevel(String securityLevel) {
        this.securityLevel = securityLevel;
    }

    /**
     * Gets the value of incidentNumber
     *
     * @return String
     */
    public String getIncidentNumber() {
        return incidentNumber;
    }

    /**
     * Sets the value of incidentNumber
     *
     * @param incidentNumber
     */
    public void setIncidentNumber(String incidentNumber) {
        this.incidentNumber = incidentNumber;
    }

    /**
     * Gets the value of lastSeenDate
     *
     * @return Date
     */
    public Date getLastSeenDate() {
        return lastSeenDate;
    }

    /**
     * Sets the value of lastSeenDate
     *
     * @param lastSeenDate
     */
    public void setLastSeenDate(Date lastSeenDate) {
        this.lastSeenDate = lastSeenDate;
    }

    /**
     * Gets the value of adjustSentence
     *
     * @return Boolean
     */
    public Boolean getAdjustSentence() {
        return adjustSentence;
    }

    /**
     * Sets the value of adjustSentence
     *
     * @param adjustSentence
     */
    public void setAdjustSentence(Boolean adjustSentence) {
        this.adjustSentence = adjustSentence;
    }

    /**
     * Gets the value of comments
     *
     * @return String
     */
    public String getEscapeComments() {
        return escapeComments;
    }

    /**
     * Sets the value of comments
     *
     * @param escapeComments
     */
    public void setEscapeComments(String escapeComments) {
        this.escapeComments = escapeComments;
    }

    /**
     *
     * @return
     */
    public Date getRecapturedDate() {
        return recapturedDate;
    }

    /**
     *
     * @param recapturedDate
     */
    public void setRecapturedDate(Date recapturedDate) {
        this.recapturedDate = recapturedDate;
    }

    /**
     *
     * @return
     */
    public Long getRecaptureByOrgId() {
        return recaptureByOrgId;
    }

    /**
     *
     * @param recaptureByOrgId
     */
    public void setRecaptureByOrgId(Long recaptureByOrgId) {
        this.recaptureByOrgId = recaptureByOrgId;
    }

    /**
     *
     * @return
     */
    public String getRecaptureComments() {
        return recaptureComments;
    }

    /**
     *
     * @param recaptureComments
     */
    public void setRecaptureComments(String recaptureComments) {
        this.recaptureComments = recaptureComments;
    }

    /**
     * @return the flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }

    public ExternalMovementActivityEntity getReadmissionMovement() {
        return readmissionMovement;
    }

    public void setReadmissionMovement(ExternalMovementActivityEntity readmissionMovement) {
        this.readmissionMovement = readmissionMovement;
    }
}