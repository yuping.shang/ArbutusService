package syscon.arbutus.product.services.movement.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.BaseDto;

/**
 * TransferWaitlistType for MovementActivity Service
 *
 * @author yshang
 * @version 2.0
 * @since October 25, 2012
 */
public class TransferWaitlistType extends BaseDto implements Serializable {

    private static final long serialVersionUID = 7941203488838104578L;
    @NotNull
    private Long facilityId;

    private Set<TransferWaitlistEntryType> transferWaitlistEntries;

    public TransferWaitlistType() {
        super();
    }

    /**
     * Constructor
     *
     * @param facilityId              Long, Required. Facility Id
     * @param transferWaitlistEntries Set&lt;TransferWaitlistEntryType>, Optional.
     */
    public TransferWaitlistType(Long facilityId, Set<TransferWaitlistEntryType> transferWaitlistEntries) {
        super();
        this.facilityId = facilityId;
        this.transferWaitlistEntries = transferWaitlistEntries;
    }

    /**
     * Indicates which facility the transfer waitlist is for.
     * Static association to the Facility service.
     * <p>Required.
     *
     * @return the facilityAssociation
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * Indicates which facility the transfer waitlist is for.
     * Static association to the Facility service.
     * <p>Required.
     *
     * @param facilityId the facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * An offender who is waitlist for a transfer. Optional.
     *
     * @return the transferWaitlistEntries
     */
    public Set<TransferWaitlistEntryType> getTransferWaitlistEntries() {
        if (transferWaitlistEntries == null) {
            transferWaitlistEntries = new HashSet<TransferWaitlistEntryType>();
        }
        return transferWaitlistEntries;
    }

    /**
     * An offender who is waitlist for a transfer. Optional.
     *
     * @param transferWaitlistEntries the transferWaitlistEntry to set
     */
    public void setTransferWaitlistEntries(Set<TransferWaitlistEntryType> transferWaitlistEntries) {
        this.transferWaitlistEntries = transferWaitlistEntries;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((facilityId == null) ? 0 : facilityId.hashCode());
        result = prime * result + ((transferWaitlistEntries == null) ? 0 : transferWaitlistEntries.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TransferWaitlistType other = (TransferWaitlistType) obj;
        if (facilityId == null) {
            if (other.facilityId != null) {
                return false;
            }
        } else if (!facilityId.equals(other.facilityId)) {
            return false;
        }
        if (transferWaitlistEntries == null) {
            if (other.transferWaitlistEntries != null) {
                return false;
            }
        } else if (!transferWaitlistEntries.equals(other.transferWaitlistEntries)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TransferWaitlistType [facilityId=" + facilityId + ", transferWaitlistEntries=" + transferWaitlistEntries + "]";
    }

}
