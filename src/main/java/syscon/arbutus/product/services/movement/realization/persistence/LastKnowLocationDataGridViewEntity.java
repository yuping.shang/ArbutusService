package syscon.arbutus.product.services.movement.realization.persistence;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Boost;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.FullTextFilterDef;
import org.hibernate.search.annotations.FullTextFilterDefs;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;

/**
 * Created on 5/28/14.
 *
 * @author hshen
 */

@Entity
@Table(name = "V_LastKnowLocationGrid")
@Indexed
@Analyzer(impl = org.apache.lucene.analysis.standard.StandardAnalyzer.class)
@FullTextFilterDefs({ @FullTextFilterDef(name = "lastKnowFacility", impl = LastKnowLocationFacilityFilterFactor.class),
        @FullTextFilterDef(name = "lastKnowLocation", impl = LastKnowLocationInternalLocationFilterFactor.class) })
public class LastKnowLocationDataGridViewEntity {
    @Field(index = Index.YES, analyze = Analyze.NO, store = Store.NO)
    @Boost(5)
    private String firstName;

    @Field(index = Index.YES, analyze = Analyze.NO, store = Store.NO)
    @Boost(5)
    private String lastName;

    @Field(index = Index.YES, analyze = Analyze.NO, store = Store.NO)
    private String gender;

    @Id
    @Column(name = "supervisiondisplayid")
    @Field(index = Index.YES, analyze = Analyze.NO, store = Store.NO)
    private String offenderNumber;

    @Field(index = Index.YES, analyze = Analyze.NO, store = Store.NO)
    @FieldBridge(impl = MyDateBridge.class)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfBirth;

    @Field(index = Index.YES, analyze = Analyze.NO, store = Store.NO)
    private Long internalLocationId;
    @Field(index = Index.YES, analyze = Analyze.NO, store = Store.NO)
    private Long facilityId;

    private Long supervisionId;

    private Long personId;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOffenderNumber() {
        return offenderNumber;
    }

    public void setOffenderNumber(String offenderNumber) {
        this.offenderNumber = offenderNumber;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Long getInternalLocationId() {
        return internalLocationId;
    }

    public void setInternalLocationId(Long internalLocationId) {
        this.internalLocationId = internalLocationId;
    }

    public Long getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    public Long getSupervisionId() {
        return supervisionId;
    }

    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }
}
