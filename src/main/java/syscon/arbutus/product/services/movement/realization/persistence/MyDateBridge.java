package syscon.arbutus.product.services.movement.realization.persistence;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.search.bridge.TwoWayStringBridge;

/**
 * Created by dev on 5/8/14.
 *
 * @author Hshen
 */

public class MyDateBridge implements TwoWayStringBridge

{
    @Override
    public Object stringToObject(String s) {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        try {
            return df.parse(s);
        } catch (ParseException e) {
            return null;
        }

    }

    @Override
    public String objectToString(Object object) {
        if (object instanceof Date) {
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            return df.format((Date) object);

        } else {
            return object.toString();

        }

    }
}
