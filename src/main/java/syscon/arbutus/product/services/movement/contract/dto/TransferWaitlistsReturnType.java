package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.List;

public class TransferWaitlistsReturnType implements Serializable {

    private static final long serialVersionUID = 7941203488838104571L;

    private List<TransferWaitlistType> transferWaitlists;

    private Long totalSize;

    /**
     * @return the transferWaitlists
     */
    public List<TransferWaitlistType> getTransferWaitlists() {
        return transferWaitlists;
    }

    /**
     * @param transferWaitlists the transferWaitlists to set
     */
    public void setTransferWaitlists(List<TransferWaitlistType> transferWaitlists) {
        this.transferWaitlists = transferWaitlists;
    }

    /**
     * @return the totalSize
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * @param totalSize the totalSize to set
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TransferWaitlistsReturnType [transferWaitlists=" + transferWaitlists + ", totalSize=" + totalSize + "]";
    }

}
