package syscon.arbutus.product.services.movement.contract.ejb;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.core.common.adapters.ActivityServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.movement.contract.dto.*;
import syscon.arbutus.product.services.realization.util.BeanHelper;

public class TwoMovementsHelper {
    private static final String DIRECTION_IN = "IN";
    private static final String DIRECTION_OUT = "OUT";
    private static final String TYPE_TAP = "TAP";
    private static final String TYPE_COURT = "CRT";
    private static final String MOVEMENT_STATUS_COMPLETED = "COMPLETED";
    private static final String MOVEMENT_STATUS_PENDING = "PENDING";

    public static MovementActivityType toMovementActivityType(TwoMovementsType mvmnt, String direction, Boolean isAdHoc) {
        ExternalMovementActivityType movement = new ExternalMovementActivityType();

          if (mvmnt instanceof TemporaryAbsenceType) {
            TemporaryAbsenceType temporaryAbsenceType = (TemporaryAbsenceType) mvmnt;
            movement.setMovementType(TYPE_TAP);
            movement.setApplicationDate(temporaryAbsenceType.getApplicationDate());
            movement.setTransportation(temporaryAbsenceType.getTransportation());
            movement.setEscortDetails(temporaryAbsenceType.getEscortDetails());
            movement.setApprovalComments(temporaryAbsenceType.getApprovalComments());
            movement.setApprovalDate(temporaryAbsenceType.getApprovalDate());
            if (temporaryAbsenceType.getApproverId() != null) {
                movement.setApprovedByStaffId(temporaryAbsenceType.getApproverId());
            }
            if (temporaryAbsenceType.getEscortOrganizationId() != null) {
                movement.setEscortOrganizationId(temporaryAbsenceType.getEscortOrganizationId());
            }
        } else if (mvmnt instanceof CourtMovementType) {
            movement.setMovementType(TYPE_COURT);
            if (((CourtMovementType) mvmnt).getCourtPartRoom() != null) {
                movement.setToFacilityInternalLocationId(((CourtMovementType) mvmnt).getCourtPartRoom());
            }
        }

        movement.setGroupId(mvmnt.getGroupId());
        movement.setCommentText(toCommentTypeSet(mvmnt.getComments()));
        movement.setMovementCategory("EXTERNAL");

        if (mvmnt.getSupervisionId() != null) {
            movement.setSupervisionId(mvmnt.getSupervisionId());
        }

        if (mvmnt.getMovementOutCome() != null) {
            movement.setMovementOutcome(mvmnt.getMovementOutCome());
        }

        if (mvmnt.getMovementReason() != null) {
            movement.setMovementReason(mvmnt.getMovementReason());
        }

        if (direction.contentEquals(DIRECTION_IN)) {
            if (isAdHoc) {
                movement.setMovementStatus(MOVEMENT_STATUS_PENDING);
            } else {
                if (mvmnt.getMovementInStatus() != null) {
                    movement.setMovementStatus(mvmnt.getMovementInStatus());
                }
            }

            movement.setMovementDirection(DIRECTION_IN);
            movement.setToLocationId(null);

            if (mvmnt.getFromFacilityId() != null) {
                movement.setToFacilityId(mvmnt.getFromFacilityId());
            }
            if (mvmnt.getToFacilityId() != null) {
                movement.setFromFacilityId(mvmnt.getToFacilityId());
            }
            if (mvmnt instanceof TemporaryAbsenceType) {
                TemporaryAbsenceType temporaryAbsenceType = (TemporaryAbsenceType) mvmnt;
                if (temporaryAbsenceType.getToOffenderLocationId() != null) {
                    movement.setFromLocationId(temporaryAbsenceType.getToOffenderLocationId());
                }
            }
        } else if (direction.contentEquals(DIRECTION_OUT)) {

            if (isAdHoc) {
                movement.setMovementStatus(MOVEMENT_STATUS_COMPLETED);
                movement.setMovementDate(mvmnt.getMoveDate());
            } else {
                if (mvmnt.getMovementOutStatus() != null) {
                    movement.setMovementStatus(mvmnt.getMovementOutStatus());
                }
            }

            movement.setMovementDirection(DIRECTION_OUT);

            if (mvmnt.getFromFacilityId() != null) {
                movement.setFromFacilityId(mvmnt.getFromFacilityId());
            }
            if (mvmnt instanceof TemporaryAbsenceType) {
                TemporaryAbsenceType temporaryAbsenceType = (TemporaryAbsenceType) mvmnt;
                if (temporaryAbsenceType.getToOffenderLocationId() != null) {
                    movement.setToLocationId(temporaryAbsenceType.getToOffenderLocationId());
                }

                if (temporaryAbsenceType.getToFacilityLocationId() != null) {
                    movement.setToLocationId(temporaryAbsenceType.getToFacilityLocationId());
                }else if (temporaryAbsenceType.getToOrganizationLocationId() != null) {
                    movement.setToLocationId(temporaryAbsenceType.getToOrganizationLocationId());
                }
                
                if (temporaryAbsenceType.getToOrganizationId() != null){
                    movement.setToOrganizationId(temporaryAbsenceType.getToOrganizationId());
                }
            }
            if (mvmnt.getToFacilityId() != null) {
                movement.setToFacilityId(mvmnt.getToFacilityId());
            }
        }
        return movement;
    }

    public static TwoMovementsType toTwoMovementsType(List<MovementDetailType> ret) {
        TwoMovementsType movement = null;

        for (MovementDetailType movementDetailType : ret) {
            ExternalMovementActivityType ma = (ExternalMovementActivityType) movementDetailType.getMovement();
            ActivityType act = (ActivityType) movementDetailType.getActivity();

            if (ma.getMovementType().contentEquals(TYPE_TAP)) {
                movement = (movement == null ? new TemporaryAbsenceType() : movement);
                ((TemporaryAbsenceType) movement).setApplicationDate(ma.getApplicationDate());
                ((TemporaryAbsenceType) movement).setEscortDetails(ma.getEscortDetails());
                ((TemporaryAbsenceType) movement).setTransportation(ma.getTransportation());
                if (ma.getEscortOrganizationId() != null) {
                    ((TemporaryAbsenceType) movement).setEscortOrganizationId(ma.getEscortOrganizationId());
                }
                if (ma.getApprovedByStaffId() != null) {
                    ((TemporaryAbsenceType) movement).setApproverId(ma.getApprovedByStaffId());
                }
                ((TemporaryAbsenceType) movement).setApprovalComments(ma.getApprovalComments());
                ((TemporaryAbsenceType) movement).setApprovalDate(ma.getApprovalDate());
            } else if (ma.getMovementType().contentEquals(TYPE_COURT)) {
                movement = (movement == null ? new CourtMovementType() : movement);
                if (ma.getToFacilityInternalLocationId() != null) {
                    ((CourtMovementType) movement).setCourtPartRoom(ma.getToFacilityInternalLocationId());
                }
            }

            if (ma.getActivityId().longValue() == act.getActivityIdentification().longValue()) {
                movement.setMoveDate(act.getPlannedStartDate());
                movement.setReturnDate(act.getPlannedEndDate());
            }

            if (ma.getMovementDirection().contentEquals(DIRECTION_IN)) {
                movement.setMovementIn(ma.getMovementId());
                movement.setMovementInStatus(ma.getMovementStatus());
            } else if (ma.getMovementDirection().contentEquals(DIRECTION_OUT)) {
                movement.setMovementOut(ma.getMovementId());
                movement.setMovementOutStatus(ma.getMovementStatus());

                if (ma.getFromFacilityId() != null) {
                    movement.setFromFacilityId(ma.getFromFacilityId());
                }
                if (ma.getToFacilityId() != null) {
                    movement.setToFacilityId(ma.getToFacilityId());
                    if (ma.getMovementType().contentEquals(TYPE_TAP)) {
                        if (ma.getToLocationId() != null) {
                            ((TemporaryAbsenceType) movement).setToFacilityLocationId(ma.getToLocationId());
                        }
                    }
                } else if (ma.getToOrganizationId() != null) {
                    if (ma.getMovementType().contentEquals(TYPE_TAP)) {
                        if (ma.getToLocationId() != null) {
                            ((TemporaryAbsenceType) movement).setToOrganizationLocationId(ma.getToLocationId());
                        }
                    }
                } else {
                    if (ma.getMovementType().contentEquals(TYPE_TAP)) {
                        if (ma.getToLocationId() != null) {
                            ((TemporaryAbsenceType) movement).setToOffenderLocationId(ma.getToLocationId());
                        }
                    }
                }
            }
            movement.setGroupId(ma.getGroupId());
            movement.setComments(toCommentText(ma.getCommentText()));
            movement.setMovementReason(ma.getMovementReason());
            movement.setMovementType(ma.getMovementType());

            if (ma.getMovementOutcome() != null) {
                movement.setMovementOutCome(ma.getMovementOutcome());
            }
            if (ma.getSupervisionId() != null) {
                movement.setSupervisionId(ma.getSupervisionId());
            }
        }

        return movement;
    }

    public static ExternalMovementActivityType updateObject(UserContext uc, TwoMovementsType mvmt, MovementActivityType movRet, ActivityType act, Boolean isAdHoc) {

        ExternalMovementActivityType movement = (ExternalMovementActivityType) movRet;

        if (mvmt instanceof TemporaryAbsenceType) {
            movement.setTransportation(((TemporaryAbsenceType) mvmt).getTransportation());
            movement.setApplicationDate(((TemporaryAbsenceType) mvmt).getApplicationDate());
            movement.setMovementType(TYPE_TAP);
            movement.setEscortDetails(((TemporaryAbsenceType) mvmt).getEscortDetails());
            if (((TemporaryAbsenceType) mvmt).getEscortOrganizationId() != null) {
                movement.setEscortOrganizationId(((TemporaryAbsenceType) mvmt).getEscortOrganizationId());
            }
            if (((TemporaryAbsenceType) mvmt).getApproverId() != null) {
                movement.setApprovedByStaffId(((TemporaryAbsenceType) mvmt).getApproverId());
            }
            movement.setApprovalComments(((TemporaryAbsenceType) mvmt).getApprovalComments());
            movement.setApprovalDate(((TemporaryAbsenceType) mvmt).getApprovalDate());

        } else if (mvmt instanceof CourtMovementType) {
            if (((CourtMovementType) mvmt).getCourtPartRoom() != null) {
                movement.setToFacilityInternalLocationId(((CourtMovementType) mvmt).getCourtPartRoom());
            }
        }

        movement.setCommentText(toCommentTypeSet(mvmt.getComments()));
        movement.setMovementCategory("EXTERNAL");

        if (mvmt.getSupervisionId() != null) {
            movement.setSupervisionId(mvmt.getSupervisionId());
        }

        if (mvmt.getMovementOutCome() != null) {
            movement.setMovementOutcome(mvmt.getMovementOutCome());
        }

        if (mvmt.getMovementReason() != null) {
            movement.setMovementReason(mvmt.getMovementReason());
        }

        if (movement.getMovementDirection().contentEquals(DIRECTION_IN)) {
            if (mvmt.getMovementInStatus() != null) {
                movement.setMovementStatus(mvmt.getMovementInStatus());
                if (mvmt.getMovementInStatus().contentEquals(MOVEMENT_STATUS_COMPLETED)) {
                    movement.setMovementDate(mvmt.getMoveDate());
                }
            }

            if (mvmt.getFromFacilityId() != null) {
                movement.setToFacilityId(mvmt.getFromFacilityId());
            }
            if (mvmt.getToFacilityId() != null) {
                movement.setFromFacilityId(mvmt.getToFacilityId());
            }
            movement.setToLocationId(null);

            if (mvmt instanceof TemporaryAbsenceType) {
                if (((TemporaryAbsenceType) mvmt).getToOffenderLocationId() != null) {
                    movement.setFromLocationId(((TemporaryAbsenceType) mvmt).getToOffenderLocationId());
                }
            }
        } else if (movement.getMovementDirection().contentEquals(DIRECTION_OUT)) {

            if (isAdHoc) {
                movement.setMovementStatus(MOVEMENT_STATUS_COMPLETED);
                movement.setMovementDate(act.getPlannedStartDate());
            } else {
                if (mvmt.getMovementOutStatus() != null) {
                    movement.setMovementStatus(mvmt.getMovementOutStatus());
                    if (mvmt.getMovementOutStatus().contentEquals(MOVEMENT_STATUS_COMPLETED)) {
                        movement.setMovementDate(mvmt.getMoveDate());
                    }
                }
            }

            if (mvmt.getFromFacilityId() != null) {
                movement.setFromFacilityId(mvmt.getFromFacilityId());
            }
            if (mvmt instanceof TemporaryAbsenceType) {
                if (((TemporaryAbsenceType) mvmt).getToOffenderLocationId() != null) {
                    movement.setToLocationId(((TemporaryAbsenceType) mvmt).getToOffenderLocationId());
                } else if (((TemporaryAbsenceType) mvmt).getToFacilityLocationId() != null) {
                    movement.setToLocationId(((TemporaryAbsenceType) mvmt).getToFacilityLocationId());
                } else {
                    movement.setToLocationId(null);
                }
            }

            if (mvmt.getToFacilityId() != null) {
                movement.setToFacilityId(mvmt.getToFacilityId());
            } else {
                movement.setToFacilityId(null);
            }
        }

        //updating dates in the activity
//        act.setPlannedStartDate(mvmt.getMoveDate());
//        act.setPlannedEndDate(mvmt.getReturnDate());
        ActivityServiceAdapter.updateActivity(uc, act);
        return movement;
    }

    private static Set<CommentType> toCommentTypeSet(String text) {
        Set<CommentType> comments = null;
        if (text != null) {
            comments = new HashSet<CommentType>();
            CommentType comment = new CommentType();
            comment.setComment(text);
            comment.setCommentDate(BeanHelper.createDate());
            comments.add(comment);
        }
        return comments;
    }

    private static String toCommentText(Set<CommentType> comments) {
        // Only one is created, user screen has only one field to fill.
        for (CommentType commentType : comments) {
            return commentType.getComment();
        }
        return null;
    }

    public static ActivityType buildActivity(TwoMovementsType movement, String direction) {
        ActivityType activity = new ActivityType();

        activity.setActivityCategory("MOVEMENT");
        activity.setSupervisionId(movement.getSupervisionId());
        activity.setActiveStatusFlag(true);

        if (direction.contentEquals(DIRECTION_OUT)) {
            activity.setPlannedStartDate(movement.getMoveDate());
            activity.setPlannedEndDate(movement.getMoveDate());
        } else {
            activity.setPlannedStartDate(movement.getReturnDate());
            activity.setPlannedEndDate(movement.getReturnDate());
        }

        return activity;
    }

}