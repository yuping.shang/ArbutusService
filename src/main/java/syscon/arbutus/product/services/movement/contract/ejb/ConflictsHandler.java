package syscon.arbutus.product.services.movement.contract.ejb;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieSession;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.common.adapters.MovementServiceAdapter;
import syscon.arbutus.product.services.movement.contract.dto.ConflictType;
import syscon.arbutus.product.services.movement.contract.dto.InternalMovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType.MovementType;
import syscon.arbutus.product.services.movement.contract.dto.TwoMovementsSearchType;
import syscon.arbutus.product.services.movement.contract.dto.TwoMovementsType;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.rules.drools.KnowledgeBaseExecuter;

public class ConflictsHandler {
    private static Logger log = LoggerFactory.getLogger(ConflictsHandler.class);

    /**
     * Empty constructor
     */
    public ConflictsHandler() {
        super();
    }

	/* *****************************************************
     * 				SCHEDULING CONFLICTS CHECK
	 ***************************************************** */

    /**
     * Checks if there is any movement conflict
     *
     * @param uc   UserContext - Required
     * @param move @{link TwoMovementsType} - Required
     * @return TwoMovementsType @{link TwoMovementsType} - Same object with it's conflicts inside
     */
    public static TwoMovementsType checkSchedulingConflict(KieSession ksession, UserContext uc, TwoMovementsType move) {
        LogHelper.debug(log, "checkSchedulingConflict", "init");

        List<TwoMovementsType> dbMovements = new ArrayList<TwoMovementsType>();

        //getting other movements to validate against.
        dbMovements = getMovements(uc, move);

        List<Object> facts = buildMovementFacts(move, dbMovements);
        KnowledgeBaseExecuter.executeStateful(ksession, null, facts);


        // removing itself as a conflict
        if (!move.getConflicts().isEmpty()) {
            if (move.getConflicts().contains(move)) {
                move.getConflicts().remove(move);
            }
        }

        // building conflicts collection without movements with the same groupId
        Set<TwoMovementsType> conflicts = new HashSet<TwoMovementsType>();
        for (TwoMovementsType conflict : move.getConflicts()) {
            if (move.getGroupId() != null) {
                if (!(move.getGroupId().contentEquals(conflict.getGroupId()))) {
                    conflicts.add(conflict);
                }
            } else {
                conflicts.add(conflict);
            }
        }
        move.setConflicts(conflicts);

        return move;
    }

    /**
     * Builds a list of objects with target and database movements as the facts.
     */
    private static List<Object> buildMovementFacts(TwoMovementsType move, List<TwoMovementsType> dbMovements) {
        List<Object> facts = new ArrayList<Object>();
        move.setTarget(true);
        facts.add(move);
        for (TwoMovementsType dbMove : dbMovements) {
            dbMove.setTarget(false);
            facts.add(dbMove);
        }
        return facts;
    }

    /**
     * Returns a list of movements from database based on search criteria.
     * If nothing is found, returns null.
     */
    @SuppressWarnings("serial")
    private static List<TwoMovementsType> getMovements(UserContext uc, TwoMovementsType move) {
        List<TwoMovementsType> dbMovements = new ArrayList<TwoMovementsType>();
        TwoMovementsSearchType searchType;

        List<String> movementTypes = new ArrayList<String>() {{
            add(MovementType.TAP.toString());
            add(MovementType.CRT.toString());
            add(MovementType.VISIT.toString());
        }};

        for (String moveType : movementTypes) {
            searchType = buildTwoMovementsSearch(move, moveType);
            dbMovements.addAll(MovementServiceAdapter.getAllTwoMovementsRequests(uc, searchType));
        }

        return dbMovements;
    }

    /**
     * Building external movements search type with supervisionID and movement type
     */
    private static TwoMovementsSearchType buildTwoMovementsSearch(TwoMovementsType move, String movementType) {
        TwoMovementsSearchType searchType = new TwoMovementsSearchType();
        searchType.setOffenderId(move.getSupervisionId());
        searchType.setMovementType(movementType);
        searchType.setMoveDate(move.getMoveDate());
        searchType.setReturnDate(move.getReturnDate());
        return searchType;
    }

	/* *****************************************************
     * 				CAPACITY CONFLICTS CHECK
	 ***************************************************** */

    public static ConflictType checkCapacityConflict(UserContext uc, InternalMovementActivityType movement) {
        //TODO: unimplemented
        // got lower priority over Housing Suitability check: February 5th, 2014.
        //throw new ("checkCapacityConflict is not implemented yet.");
        return null;
    }

}