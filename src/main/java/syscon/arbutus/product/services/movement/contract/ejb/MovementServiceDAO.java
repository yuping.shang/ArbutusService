package syscon.arbutus.product.services.movement.contract.ejb;

import javax.ejb.SessionContext;
import java.util.Date;
import java.util.List;

import org.hibernate.*;
import org.hibernate.criterion.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.realization.persistence.ActivityEntity;
import syscon.arbutus.product.services.core.exception.ArbutusOptimisticLockException;
import syscon.arbutus.product.services.core.exception.ErrorCodes;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.core.services.CrudHandler;
import syscon.arbutus.product.services.core.util.DateUtil;
import syscon.arbutus.product.services.movement.contract.dto.*;
import syscon.arbutus.product.services.movement.realization.persistence.*;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;

/**
 * Created by rthodupunuri on 18/09/15.
 */
public class MovementServiceDAO {

    private final static Long SUCCESS = 1L;
    private static Logger log = LoggerFactory.getLogger(MovementServiceDAO.class);
    private int searchMaxLimit = 200;
    private SessionContext context;
    private Session session;

    private final static CrudHandler<EscapeOffenderEntity, EscapeRecapture> escapeRecaptureCRUD = new CrudHandler<EscapeOffenderEntity, EscapeRecapture>(
            EscapeOffenderEntity.class, EscapeRecapture.class) {

        @Override
        public EscapeRecapture toDto(EscapeOffenderEntity from) {
            EscapeRecapture to = new EscapeRecapture();
            to.setEscapeRecaptureId(from.getEscapeId());
            to.setSupervisionId(from.getMovementActivity().getSupervisionId());
            to.setEscapeReason(from.getMovementActivity().getMovementReason());
            to.setEscapeFacility(from.getMovementActivity().getFromFacilityId());
            to.setEscapeDate(from.getMovementActivity().getMovementDate());
            to.setEscapeTime(from.getMovementActivity().getMovementDate());
            to.setRecaptureDate(from.getRecapturedDate());
            to.setRecaptureTime(from.getRecapturedDate());
            to.setArrestingOrganization(from.getRecaptureByOrgId());
            to.setRecaptureComments(from.getRecaptureComments());
            to.setFromCustody(from.getFromCustody());
            to.setSecurityBreached(from.getSecurityLevel());
            to.setCircumstance(from.getCircumstance());
            to.setLastSeenDate(from.getLastSeenDate());
            to.setLastSeenTime(from.getLastSeenDate());
            to.setIncidentNumber(from.getIncidentNumber());
            to.setEscapeComments(from.getEscapeComments());
            to.setVersion(from.getVersion());
            return to;
        }

        @Override
        public EscapeOffenderEntity toEntity(EscapeRecapture from) {
            EscapeOffenderEntity entity = new EscapeOffenderEntity();
            entity.setEscapeId(from.getEscapeRecaptureId());
            entity.setFromCustody(from.getFromCustody());
            entity.setCircumstance(from.getCircumstance());
            entity.setSecurityLevel(from.getSecurityBreached());
            entity.setIncidentNumber(from.getIncidentNumber());
            entity.setLastSeenDate(DateUtil.combineDateTime(from.getLastSeenDate(), from.getLastSeenTime()));
            entity.setAdjustSentence(false);
            entity.setEscapeComments(from.getEscapeComments());
            entity.setRecapturedDate(DateUtil.combineDateTime(from.getRecaptureDate(), from.getRecaptureTime()));
            entity.setRecaptureByOrgId(from.getArrestingOrganization());
            entity.setRecaptureComments(from.getRecaptureComments());
            entity.setVersion(from.getVersion());
            return entity;
        }
    };

    public MovementServiceDAO(SessionContext context, Session session) {
        super();
        this.context = context;
        this.session = session;
    }

    public void setSearchMaxLimit(int searchMaxLimit) {
        this.searchMaxLimit = searchMaxLimit;
    }

    public Long deleteAll(UserContext uc) {
        session.createQuery("delete EscapeOffenderEntity").executeUpdate();

        // Clear Hibernate cache to add to avoid unique key violation.
        session.flush();
        session.clear();

        return SUCCESS;
    }

    public EscapeRecapture getInmateEscapeRecapture(UserContext uc, Long escapeRecaptureId) {
        return escapeRecaptureCRUD.get(session, escapeRecaptureId);
    }

    /**
     * To check if an internal location is currently in use.
     *
     * @param locationId
     * @return
     */
    public boolean isInternalLocationCurrentlyUsed(Long locationId) {
        Criteria criteria = session.createCriteria(LastCompletedSupervisionMovementEntity.class);
        criteria.add(Restrictions.eq("internalLocationId", locationId));
        criteria.setProjection(Projections.rowCount());
        Long count = (Long)criteria.uniqueResult();
        return count == null || count.longValue() == 0L ? false : true;
    }

    /**
     * To check if an internal location is referenced.
     *
     * @param locationId
     * @return
     */
    public boolean isInternalLocationUsed(Long locationId) {
        // Check in last completed movements
        Criteria criteria = session.createCriteria(LastCompletedSupervisionMovementEntity.class);
        criteria.add(Restrictions.eq("internalLocationId", locationId));
        criteria.setProjection(Projections.rowCount());
        Long count = (Long)criteria.uniqueResult();
        if (count != null && count.longValue() > 0L) {
            return true;
        }
        // Check in movements
        criteria = session.createCriteria(InternalMovementActivityEntity.class);
        criteria.add(Restrictions.or(Restrictions.eq("fromInternalLocationId", locationId), Restrictions.eq("toInternalLocationId", locationId)));
        criteria.setProjection(Projections.rowCount());
        count = (Long)criteria.uniqueResult();
        if (count != null && count.longValue() > 0L) {
            return true;
        }
        return false;
    }

    public Long createActivity(UserContext uc, EscapeRecapture escapeRecapture) {
        ActivityEntity entity = MovementHelper.toActivityEntity(escapeRecapture);
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        entity.setStamp(createStamp);
        session.save(entity);
        session.flush();
        return entity.getActivityId();
    }

    public Long createExternalMovement(UserContext uc, EscapeRecapture escapeRecapture, Long facilityId, Long activityId) {
        ExternalMovementActivityEntity entity = MovementHelper.toExternalMovementActivityEntity(escapeRecapture);
        entity.setFromFacilityId(facilityId);
        entity.setActivityId(activityId);
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        entity.setStamp(createStamp);
        session.save(entity);
        createEscapeComment(uc, entity, escapeRecapture);
        return entity.getMovementId();
    }

    /**
     * create escape comment
     *
     * @param uc
     * @param entity
     * @param escapeRecapture
     */
    public void createEscapeComment(UserContext uc, ExternalMovementActivityEntity entity, EscapeRecapture escapeRecapture){
        if (escapeRecapture.getEscapeComments() == null) {
            return;
        }

        MovementActivityCommentEntity commentEntity = new MovementActivityCommentEntity();
        commentEntity.setCommentDate(escapeRecapture.getEscapeDateTime());
        commentEntity.setCommentText(escapeRecapture.getEscapeComments());
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        commentEntity.setFromIdentifier(entity.getMovementId());
        commentEntity.setCreateDateTime(createStamp.getCreateDateTime());
        commentEntity.setCreateUserId(createStamp.getCreateUserId());
        commentEntity.setModifyDateTime(createStamp.getModifyDateTime());
        commentEntity.setModifyUserId(createStamp.getModifyUserId());
        commentEntity.setInvocationContext(createStamp.getInvocationContext());
        entity.getCommentText().add(commentEntity);
        session.save(commentEntity);
    }

    public Long createEscape(UserContext uc, EscapeRecapture escapeRecapture, Long movementId) {
        EscapeOffenderEntity entity = MovementHelper.toEscapeOffenderEntity(escapeRecapture);
        entity.setMovementId(movementId);
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        entity.setStamp(createStamp);
        session.save(entity);
        session.flush();
        return entity.getEscapeId();
    }


    /**
     *
     * @param uc
     * @param movementId
     * @return
     */
    public MovementActivityType getMovement(UserContext uc, Long movementId) {
        MovementActivityEntity movementActivityEntity = BeanHelper.getEntity(session, MovementActivityEntity.class, movementId);
        MovementActivityType ret = null;
        if (movementActivityEntity instanceof ExternalMovementActivityEntity) {
            ret = MovementHelper.toMovementActivityType(movementActivityEntity, ExternalMovementActivityType.class);
        } else if (movementActivityEntity instanceof InternalMovementActivityEntity) {
            ret = MovementHelper.toMovementActivityType(movementActivityEntity, InternalMovementActivityType.class);
        } else {
            ret = MovementHelper.toMovementActivityType(movementActivityEntity, MovementActivityType.class);
        }
        return ret;
    }

    /**
     *
     * @param
     */
    public void updateEscape(UserContext uc, EscapeRecapture escapeRecapture) {
        EscapeOffenderEntity existingEscapeOffenderEntity = BeanHelper.getEntity(session, EscapeOffenderEntity.class, escapeRecapture.getEscapeRecaptureId());
        if (existingEscapeOffenderEntity == null) {
            throw new InvalidInputException("Could not find escape by id = " + escapeRecapture.getEscapeRecaptureId(), ErrorCodes.GENERIC_INVALID_ID);
        }

        EscapeOffenderEntity entity = MovementHelper.toEscapeOffenderEntity(escapeRecapture);
        // Movement id can not be changed
        entity.setMovementId(existingEscapeOffenderEntity.getMovementId());
        StampEntity updateStamp = BeanHelper.getModifyStamp(uc, context, existingEscapeOffenderEntity.getStamp());
        entity.setStamp(updateStamp);
        try {
            session.merge(entity);
        }
        catch (StaleObjectStateException se) {
            throw new ArbutusOptimisticLockException(se);
        }
    }

    /**
     * Retrieve all escapes  of a supervision period.
     *
     * @param uc
     * @param supervisionId
     * @return
     */
    public List<EscapeRecapture> getEscapesBySupervisionId(UserContext uc, Long supervisionId) {
        Query query = session.createQuery("select e from EscapeOffenderEntity e where e.movementActivity.supervisionId = :supervisionId order by e.escapeId DESC");
        query.setParameter("supervisionId", supervisionId);
        List<EscapeOffenderEntity> escapeRecaptureEntityList = query.list();
        return MovementHelper.toEscapeOffender(escapeRecaptureEntityList);
    }

    /**
     * Return escape details
     *
     * @param uc
     * @param escapeId
     * @return
     */
    public EscapeRecapture getEscapeDetailsById(UserContext uc, Long escapeId) {
        Query query = session.createQuery("select e from EscapeOffenderEntity e where e.escapeId = :escapeId");
        query.setParameter("escapeId", escapeId);
        List<EscapeOffenderEntity> escapeRecaptureEntityList = query.list();
        if(!escapeRecaptureEntityList.isEmpty()){
            return MovementHelper.toEscapeOffender(escapeRecaptureEntityList.get(0));
        }
        return  null;
    }

    /**
     *
     * @param uc
     * @param supervisionId
     * @return
     */
    public EscapeRecapture getEscapesWithoutRecaptureBySupervisionId(UserContext uc, Long supervisionId) {
        Query query = session.createQuery("from EscapeOffenderEntity e " +
                "where e.movementActivity.supervisionId = :supervisionId and e.recapturedDate IS NULL " +
                "order by e.escapeId desc ");
        query.setFetchSize(1).setMaxResults(1);
        query.setParameter("supervisionId", supervisionId);
        EscapeOffenderEntity escapeOffenderEntity = (EscapeOffenderEntity)query.uniqueResult();
        return  MovementHelper.toEscapeOffender(escapeOffenderEntity);
    }

    public boolean isOffenderEscaped(UserContext uc, Long supervisionId) {
        Query query = session.createQuery("from EscapeOffenderEntity e where e.movementActivity.supervisionId = :supervisionId and e.recapturedDate IS NULL");
        query.setParameter("supervisionId", supervisionId);
        return query.list().isEmpty()? false: true;
    }

    /**
     *
     * @param uc
     * @param activityId
     */
    public void deleteActivity(UserContext uc, Long activityId) {
        ActivityEntity activityEntity = (ActivityEntity)session.get(ActivityEntity.class, activityId);
        if (activityEntity != null) {
            session.delete(activityEntity);
        }
    }

    /**
     *
     * @param uc
     * @param movementId
     */
    public void deleteMovement(UserContext uc, Long movementId) {
        MovementActivityEntity movementActivityEntity = (MovementActivityEntity)session.get(MovementActivityEntity.class, movementId);
        if (movementActivityEntity != null) {
            session.delete(movementActivityEntity);
        }
    }

    /**
     *
     * @param uc
     * @param escapeId
     */
    public void deleteEscape(UserContext uc, Long escapeId) {
        EscapeOffenderEntity escapeOffenderEntity = (EscapeOffenderEntity)session.get(EscapeOffenderEntity.class, escapeId);
        if (escapeOffenderEntity != null) {
            session.delete(escapeOffenderEntity);
        }
    }

    /**
     *
     * @param uc
     * @param supervisionId
     * @return
     */
    public Date getAdmissionDate(UserContext uc, Long supervisionId) {
        return (Date)session.createQuery("select movementDate from MovementActivityEntity " +
                "where supervisionId = :supervisionId and movementCategory = :movementCategory " +
                "and movementType = :movementType and movementDirection = :movementDirection " +
                "and movementStatus = :movementStatus and movementDate is not null " +
                "order by supervisionId desc")
                .setLong("supervisionId", supervisionId)
                .setString("movementCategory", MovementServiceBean.MovementCategory.EXTERNAL.code())
                .setString("movementType", MovementServiceBean.MovementType.ADM.code())
                .setString("movementDirection", MovementServiceBean.MovementDirection.IN.code())
                .setString("movementStatus", MovementServiceBean.MovementStatus.COMPLETED.code())
                .setFetchSize(1)
                .setMaxResults(1)
                .uniqueResult();
    }

    /**
     *
     * @param uc
     * @param configuration
     */
    public void createCategoryConfiguration(UserContext uc, MovementCategoryConfiguration configuration) {

        TemplateConfigurationEntity entity = MovementHelper.toCategoryConfigurationEntity(configuration);
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        entity.setStamp(createStamp);
        session.save(entity);

    }

    /**
     *
     * @param uc
     * @param configuration
     */
    public void updateCategoryConfiguration(UserContext uc, MovementCategoryConfiguration configuration) {

        TemplateConfigurationEntity existingEntity = (TemplateConfigurationEntity)session.get(TemplateConfigurationEntity.class, new CategoryConfigurationPK(configuration.getMovementType(), configuration.getMovementReason()));
        if (existingEntity == null) {
            throw new InvalidInputException("Could not find movement category configuration by type = " + configuration.getMovementType() + " and reason = " + configuration.getMovementReason(), ErrorCodes.GENERIC_INVALID_ID);
        }

        TemplateConfigurationEntity entity = MovementHelper.toCategoryConfigurationEntity(configuration);
        StampEntity updateStamp = BeanHelper.getModifyStamp(uc, context, existingEntity.getStamp());
        entity.setStamp(updateStamp);
        try {
            session.merge(entity);
        }
        catch (StaleObjectStateException se) {
            throw new ArbutusOptimisticLockException(se);
        }
    }

    /**
     *
     * @param uc
     * @param configuration
     */
    public void deleteCategoryConfiguration(UserContext uc, MovementCategoryConfiguration configuration) {

        TemplateConfigurationEntity existingEntity = (TemplateConfigurationEntity)session.get(TemplateConfigurationEntity.class, new CategoryConfigurationPK(configuration.getMovementType(), configuration.getMovementReason()));
        if (existingEntity == null) {
            throw new InvalidInputException("Could not find movement category configuration by type = " + configuration.getMovementType() + " and reason = " + configuration.getMovementReason(), ErrorCodes.GENERIC_INVALID_ID);
        }

        session.delete(existingEntity);
    }

    /**
     *
     * @param uc
     * @param movementType
     * @param movementReason
     * @return
     *
     * TODO: rename to getTemplateConfiguration()
     *
     */
    public MovementCategoryConfiguration getMovementCategoryConfiguration(UserContext uc, String movementType, String movementReason) {
        TemplateConfigurationEntity existingEntity = (TemplateConfigurationEntity)session.get(TemplateConfigurationEntity.class, new CategoryConfigurationPK(movementType, movementReason));
        if (existingEntity == null) {
            return null;
        }

        //
        return MovementHelper.toCategoryConfiguration(existingEntity);
    }


    /**
     *
     * @param uc
     * @return
     */
    public List<MovementCategoryConfiguration> getAllCategoryConfigurations(UserContext uc) {
        Query query = session.createQuery("from TemplateConfigurationEntity");
        List<TemplateConfigurationEntity> entityList = query.list();
        return MovementHelper.toCategoryConfigurationList(entityList);
    }


}
