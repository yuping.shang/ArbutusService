package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created on 5/28/14.
 *
 * @author hshen
 */
public class LastKnowLocationGridReturnType implements Serializable {

    private List<LastKnowLocationGridEntryType> inmateList;
    private Long totalSize;

    public LastKnowLocationGridReturnType() {
    }

    public List<LastKnowLocationGridEntryType> getInmateList() {
        return inmateList;
    }

    public void setInmateList(List<LastKnowLocationGridEntryType> inmateList) {
        this.inmateList = inmateList;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }
}
