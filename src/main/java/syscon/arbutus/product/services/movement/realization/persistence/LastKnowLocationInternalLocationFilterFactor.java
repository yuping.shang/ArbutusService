package syscon.arbutus.product.services.movement.realization.persistence;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.hibernate.search.annotations.Factory;
import org.hibernate.search.annotations.Key;
import org.hibernate.search.filter.FilterKey;
import org.hibernate.search.filter.StandardFilterKey;

/**
 * Created on 5/28/14.
 *
 * @author hshen
 */
public class LastKnowLocationInternalLocationFilterFactor {
    private Long internalLocationId;

    public void setInternalLocationId(Long internalLocationId) {
        this.internalLocationId = internalLocationId;
    }

    @Key
    public FilterKey getKey() {
        StandardFilterKey key = new StandardFilterKey();
        key.addParameter(internalLocationId);
        return key;

    }

    @Factory
    public Filter getFilter() {
        Query query = new TermQuery(new Term("internalLocationId", internalLocationId.toString()));
        return new CachingWrapperFilter(new QueryWrapperFilter(query));

    }
}
