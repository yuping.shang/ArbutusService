package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.Set;

/**
 * Definition of Schedule Movement Count
 *
 * @author bkahlon
 * @version 1.0
 * @since January 24, 2012
 */
public class ScheduleMovementType implements Serializable {

    private static final long serialVersionUID = -3981555676749703890L;

    private Long locationId;
    private String locationCode;

    private Set<Long> scheduleTransferOut;
    private Set<Long> onTransferWaitlist;
    private Set<Long> scheduleCourtMove;
    private Set<Long> scheduleTemporaryAbsence;

    /**
     * Empty constructor
     */
    public ScheduleMovementType() {

    }

    /**
     * Gets the value of the locationId property.
     */
    public Long getLocationId() {
        return locationId;
    }

    /**
     * Sets the value of the locationId property.
     */
    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    /**
     * Gets the value of the locationCode property.
     */
    public String getLocationCode() {
        return locationCode;
    }

    /**
     * Sets the value of the locationCode property.
     */
    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    /**
     * Gets the value of the scheduleTransferOut property.
     */
    public Set<Long> getScheduleTransferOut() {
        return scheduleTransferOut;
    }

    /**
     * Sets the value of the scheduleTransferOut property.
     */
    public void setScheduleTransferOut(Set<Long> scheduleTransferOut) {
        this.scheduleTransferOut = scheduleTransferOut;
    }

    /**
     * Gets the value of the onTransferWaitlist property.
     */
    public Set<Long> getOnTransferWaitlist() {
        return onTransferWaitlist;
    }

    /**
     * Sets the value of the onTransferWaitlist property.
     */
    public void setOnTransferWaitlist(Set<Long> onTransferWaitlist) {
        this.onTransferWaitlist = onTransferWaitlist;
    }

    /**
     * Gets the value of the scheduleCourtMove property.
     */
    public Set<Long> getScheduleCourtMove() {
        return scheduleCourtMove;
    }

    /**
     * Sets the value of the scheduleCourtMove property.
     */
    public void setScheduleCourtMove(Set<Long> scheduleCourtMove) {
        this.scheduleCourtMove = scheduleCourtMove;
    }

    /**
     * Gets the value of the scheduleTemporaryAbsence property.
     */
    public Set<Long> getScheduleTemporaryAbsence() {
        return scheduleTemporaryAbsence;
    }

    /**
     * Sets the value of the scheduleTemporaryAbsence property.
     */
    public void setScheduleTemporaryAbsence(Set<Long> scheduleTemporaryAbsence) {
        this.scheduleTemporaryAbsence = scheduleTemporaryAbsence;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ScheduleMovementType [locationId=" + locationId + ", locationCode=" + locationCode + ", scheduleTransferOut=" + scheduleTransferOut
                + ", onTransferWaitlist=" + onTransferWaitlist + ", scheduleCourtMove=" + scheduleCourtMove + ", scheduleTemporaryAbsence=" + scheduleTemporaryAbsence
                + "]";
    }

}
