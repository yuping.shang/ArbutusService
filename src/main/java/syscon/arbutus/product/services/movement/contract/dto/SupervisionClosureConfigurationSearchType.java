package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;

/**
 * SupervisionClosureConfigurationSearchType for search the Configuration of Supervision Closure
 * <p>At least one search criteria must be specified.
 *
 * @author yshang
 */
public class SupervisionClosureConfigurationSearchType implements Serializable {

    private static final long serialVersionUID = 7941203488838104585L;

    /**
     * The type of movement (APP, VISIT etc.,)
     */
    private String movementType;

    /**
     * The reason for the Movement
     */
    private String movementReason;

    /**
     * If set to true then the offender’s supervision period
     * must be closed when a movement is completed with the movement type
     * and reason listed in this configuration object
     */
    private Boolean isCloseSupervisionFlag;

    private Boolean isCleanSchedulesFlag;

    private Boolean isCloseLegalsFlag;

    private Boolean isReleaseBedFlag;

    /**
     * The MovementReason which is linked to MovementType:ESCP, REL, TRNIJ, TRNOJ. -- Optional
     */
    private String defaultAdmissionReason;

    public SupervisionClosureConfigurationSearchType() {
        super();
    }

    /**
     * @param movementType           String -- The type of movement (APP, VISIT etc.,)
     * @param movementReason         String -- The reason for the Movement
     * @param isCloseSupervisionFlag Boolean -- If set to true then the offender’s supervision period
     *                               must be closed when a movement is completed with the movement type
     *                               and reason listed in this configuration object
     */
    public SupervisionClosureConfigurationSearchType(String movementType, String movementReason, Boolean isCloseSupervisionFlag) {
        super();
        this.movementType = movementType;
        this.movementReason = movementReason;
        this.isCloseSupervisionFlag = isCloseSupervisionFlag;
    }

    public SupervisionClosureConfigurationSearchType(String movementType, String movementReason, Boolean isCloseSupervisionFlag, Boolean isCleanSchedulesFlag,
            Boolean isCloseLegalsFlag, String defaultAdmissionReason, Boolean isReleaseBedFlag) {
        this.movementType = movementType;
        this.movementReason = movementReason;
        this.isCloseSupervisionFlag = isCloseSupervisionFlag;
        this.isCleanSchedulesFlag = isCleanSchedulesFlag;
        this.isCloseLegalsFlag = isCloseLegalsFlag;
        this.isReleaseBedFlag = isReleaseBedFlag;
        this.defaultAdmissionReason = defaultAdmissionReason;
    }

    public Boolean getIsCleanSchedulesFlag() {
        return isCleanSchedulesFlag;
    }

    public void setIsCleanSchedulesFlag(Boolean isCleanSchedulesFlag) {
        this.isCleanSchedulesFlag = isCleanSchedulesFlag;
    }

    public Boolean getIsCloseLegalsFlag() {
        return isCloseLegalsFlag;
    }

    public void setIsCloseLegalsFlag(Boolean isCloseLegalsFlag) {
        this.isCloseLegalsFlag = isCloseLegalsFlag;
    }

    /**
     * The type of movement (APP, VISIT etc.,)
     *
     * @return the movementType
     */
    public String getMovementType() {
        return movementType;
    }

    /**
     * The type of movement (APP, VISIT etc.,)
     *
     * @param movementType the movementType to set
     */
    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    /**
     * The reason for the Movement
     *
     * @return the movementReason
     */
    public String getMovementReason() {
        return movementReason;
    }

    /**
     * The reason for the Movement
     *
     * @param movementReason the movementReason to set
     */
    public void setMovementReason(String movementReason) {
        this.movementReason = movementReason;
    }

    /**
     * If set to true then the offender’s supervision period
     * must be closed when a movement is completed with the movement type
     * and reason listed in this configuration object
     *
     * @return the isCloseSupervisionFlag
     */
    public Boolean getIsCloseSupervisionFlag() {
        return isCloseSupervisionFlag;
    }

    /**
     * If set to true then the offender’s supervision period
     * must be closed when a movement is completed with the movement type
     * and reason listed in this configuration object
     *
     * @param isCloseSupervisionFlag the isCloseSupervisionFlag to set
     */
    public void setIsCloseSupervisionFlag(Boolean isCloseSupervisionFlag) {
        this.isCloseSupervisionFlag = isCloseSupervisionFlag;
    }

    public Boolean getIsReleaseBedFlag() {
        return isReleaseBedFlag;
    }

    public void setIsReleaseBedFlag(Boolean isReleaseBedFlag) {
        this.isReleaseBedFlag = isReleaseBedFlag;
    }

    public String getDefaultAdmissionReason() {
        return defaultAdmissionReason;
    }

    public void setDefaultAdmissionReason(String defaultAdmissionReason) {
        this.defaultAdmissionReason = defaultAdmissionReason;
    }

    /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((isCloseSupervisionFlag == null) ? 0 : isCloseSupervisionFlag.hashCode());
        result = prime * result + ((movementReason == null) ? 0 : movementReason.hashCode());
        result = prime * result + ((movementType == null) ? 0 : movementType.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SupervisionClosureConfigurationSearchType other = (SupervisionClosureConfigurationSearchType) obj;
        if (isCloseSupervisionFlag == null) {
            if (other.isCloseSupervisionFlag != null) {
				return false;
			}
        } else if (!isCloseSupervisionFlag.equals(other.isCloseSupervisionFlag)) {
			return false;
		}
        if (movementReason == null) {
            if (other.movementReason != null) {
				return false;
			}
        } else if (!movementReason.equals(other.movementReason)) {
			return false;
		}
        if (movementType == null) {
            if (other.movementType != null) {
				return false;
			}
        } else if (!movementType.equals(other.movementType)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SupervisionClosureConfigurationSearchType [movementType=" + movementType + ", movementReason=" + movementReason + ", isCloseSupervisionFlag="
                + isCloseSupervisionFlag + "]";
    }

}
