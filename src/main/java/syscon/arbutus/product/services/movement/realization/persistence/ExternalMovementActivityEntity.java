package syscon.arbutus.product.services.movement.realization.persistence;

import javax.persistence.*;

import java.util.Date;

import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.vine.interceptors.ArbutusInterceptorLog;

/**
 * ExternalMovementActivityEntity for Movement Activity Service
 *
 * @author yshang, Syscon Justice Systems
 * @version 1.0
 * @DbComment MA_MovementActivity 'Main table for Movement Activity Service'
 * @since September 27, 2012
 */
@Audited
@Entity
public class ExternalMovementActivityEntity extends MovementActivityEntity implements ArbutusInterceptorLog {

    private static final long serialVersionUID = 6347763804567963150L;

    /**
     * @DbComment MA_MovementActivity.fromInternalLocationId 'Association to coming from InternalLocation'
     */
    @Column(name = "fromInternalLocationId", nullable = true)
    private Long fromInternalLocationId;


    /**
     * @DbComment MA_MovementActivity.fromLocationId 'Association to Coming From Location.'
     */
    @Column(name = "fromLocationId", nullable = true)
    private Long fromLocationId;

    /**
     * @DbComment MA_MovementActivity.fromOrganizationId 'Association to Coming From Organization.'
     */
    @Column(name = "fromOrganizationId", nullable = true)
    private Long fromOrganizationId;

    /**
     * @DbComment MA_MovementActivity.fromFacilityId 'Association to Coming From Facility.'
     */
    @Column(name = "fromFacilityId", nullable = true)
    private Long fromFacilityId;

    /**
     * @DbComment MA_MovementActivity.fromCity 'The city offender or inmate is coming from'
     */
    @Column(name = "fromCity", nullable = true, length = 128)
    private String fromCity;

    /**
     * @DbComment MA_MovementActivity.toLocationId 'Association to Going To Location'
     */
    @Column(name = "toLocationId", nullable = true)
    private Long toLocationId;

    /**
     * @DbComment MA_MovementActivity.toOrganizationId 'Association to Going To Organization'
     */
    @Column(name = "toOrganizationId", nullable = true)
    private Long toOrganizationId;

    /**
     * @DbComment MA_MovementActivity.toFacilityId 'Association to Going To Facility'
     */
    @Column(name = "toFacilityId", nullable = true)
    private Long toFacilityId;

    /**
     * @DbComment MA_MovementActivity.toCity 'The city offender/inmate is going to'
     */
    @Column(name = "toCity", nullable = true, length = 128)
    private String toCity;

    /**
     * @DbComment MA_MovementActivity.toProvinceState 'The province or state offender/inmate is going to'
     */
    @Column(name = "toProvinceState", nullable = true, length = 128)
    @MetaCode(set = MetaSet.TO_PROVINCE_STATE)
    private String toProvinceState;

    /**
     * @DbComment MA_MovementActivity.arrestOrganizationId 'Association to Arresting Organization'
     */
    @Column(name = "arrestOrganizationId", nullable = true)
    private Long arrestOrganizationId;

    /**
     * @DbComment MA_MovementActivity.applicationDate 'The application date for a movement'
     */
    @Column(name = "applicationDate", nullable = true)
    private Date applicationDate;

    /**
     * @DbComment MA_MovementActivity.escortOrganizationId 'Association to Escorting Organization'
     */
    @Column(name = "escortOrganizationId", nullable = true)
    private Long escortOrganizationId;

    /**
     * @DbComment MA_MovementActivity.escortDetails 'Escort Details'
     */
    @Column(name = "escortDetails", nullable = true, length = 1024)
    private String escortDetails;

    /**
     * @DbComment MA_MovementActivity.reportingDate 'The reporting date for a movement'
     */
    @Column(name = "reportingDate", nullable = true)
    private Date reportingDate;

    /**
     * @DbComment MA_MovementActivity.transportation 'Transportation'
     */
    @Column(name = "transportation", nullable = true, length = 128)
    @MetaCode(set = MetaSet.TA_TRANSPORT)
    private String transportation;

    /**
     * @DbComment escapeId 'Id of escape against that this movement is created after inmate is recaptured.'
     */
    @Column(name = "escapeId", nullable = true)
    private Long escapeId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "escapeId", referencedColumnName = "escapeId", insertable = false, updatable = false)
    private EscapeOffenderEntity escapeOffender;
    
	/**
     * Constructor
     */
    public ExternalMovementActivityEntity() {
        super();
    }

    public Long getFromInternalLocationId() {
        return fromInternalLocationId;
    }

    public void setFromInternalLocationId(Long fromInternalLocationId) {
        this.fromInternalLocationId = fromInternalLocationId;
    }

    /**
     * @return the fromCity
     */
    public String getFromCity() {
        return fromCity;
    }

    /**
     * @param fromCity the fromCity to set
     */
    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    /**
     * @return the toCity
     */
    public String getToCity() {
        return toCity;
    }

    /**
     * @param toCity the toCity to set
     */
    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    /**
     * @return the toProvinceState
     */
    public String getToProvinceState() {
        return toProvinceState;
    }

    /**
     * @param toProvinceState the toProvinceState to set
     */
    public void setToProvinceState(String toProvinceState) {
        this.toProvinceState = toProvinceState;
    }

    /**
     * @return the applicationDate
     */
    public Date getApplicationDate() {
        return applicationDate;
    }

    /**
     * @param applicationDate the applicationDate to set
     */
    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    /**
     * @return the escortDetails
     */
    public String getEscortDetails() {
        return escortDetails;
    }

    /**
     * @param escortDetails the escortDetails to set
     */
    public void setEscortDetails(String escortDetails) {
        this.escortDetails = escortDetails;
    }

    /**
     * @return the reportingDate
     */
    public Date getReportingDate() {
        return reportingDate;
    }

    /**
     * @param reportingDate the reportingDate to set
     */
    public void setReportingDate(Date reportingDate) {
        this.reportingDate = reportingDate;
    }

    /**
     * @return the fromLocationId
     */
    public Long getFromLocationId() {
        return fromLocationId;
    }

    /**
     * @param fromLocationId the fromLocationId to set
     */
    public void setFromLocationId(Long fromLocationId) {
        this.fromLocationId = fromLocationId;
    }

    /**
     * @return the fromOrganizationId
     */
    public Long getFromOrganizationId() {
        return fromOrganizationId;
    }

    /**
     * @param fromOrganizationId the fromOrganizationId to set
     */
    public void setFromOrganizationId(Long fromOrganizationId) {
        this.fromOrganizationId = fromOrganizationId;
    }

    /**
     * @return the fromFacilityId
     */
    public Long getFromFacilityId() {
        return fromFacilityId;
    }

    /**
     * @param fromFacilityId the fromFacilityId to set
     */
    public void setFromFacilityId(Long fromFacilityId) {
        this.fromFacilityId = fromFacilityId;
    }

    /**
     * @return the toLocationId
     */
    public Long getToLocationId() {
        return toLocationId;
    }

    /**
     * @param toLocationId the toLocationId to set
     */
    public void setToLocationId(Long toLocationId) {
        this.toLocationId = toLocationId;
    }

    /**
     * @return the toOrganizationId
     */
    public Long getToOrganizationId() {
        return toOrganizationId;
    }

    /**
     * @param toOrganizationId the toOrganizationId to set
     */
    public void setToOrganizationId(Long toOrganizationId) {
        this.toOrganizationId = toOrganizationId;
    }

    /**
     * @return the toFacilityId
     */
    public Long getToFacilityId() {
        return toFacilityId;
    }

    /**
     * @param toFacilityId the toFacilityId to set
     */
    public void setToFacilityId(Long toFacilityId) {
        this.toFacilityId = toFacilityId;
    }

    /**
     * @return the arrestOrganizationId
     */
    public Long getArrestOrganizationId() {
        return arrestOrganizationId;
    }

    /**
     * @param arrestOrganizationId the arrestOrganizationId to set
     */
    public void setArrestOrganizationId(Long arrestOrganizationId) {
        this.arrestOrganizationId = arrestOrganizationId;
    }

    /**
     * @return the escortOrganizationId
     */
    public Long getEscortOrganizationId() {
        return escortOrganizationId;
    }

    /**
     * @param escortOrganizationId the escortOrganizationId to set
     */
    public void setEscortOrganizationId(Long escortOrganizationId) {
        this.escortOrganizationId = escortOrganizationId;
    }

    /**
     * @return the transportation reference code value
     */
    public String getTransportation() {
        return transportation;
    }

    /**
     * @param transportation , the transportation reference code value
     */
    public void setTransportation(String transportation) {
        this.transportation = transportation;
    }
    

    public Long getEscapeId() {
        return escapeId;
    }

    public void setEscapeId(Long escapeId) {
        this.escapeId = escapeId;
    }

    public EscapeOffenderEntity getEscapeOffender() {
        return escapeOffender;
    }

    public void setEscapeOffender(EscapeOffenderEntity escapeOffender) {
        this.escapeOffender = escapeOffender;
    }

    /*
         * (non-Javadoc)
         *
         * @see java.lang.Object#hashCode()
         */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((arrestOrganizationId == null) ? 0 : arrestOrganizationId.hashCode());
        result = prime * result + ((escortDetails == null) ? 0 : escortDetails.hashCode());
        result = prime * result + ((escortOrganizationId == null) ? 0 : escortOrganizationId.hashCode());
        result = prime * result + ((fromCity == null) ? 0 : fromCity.hashCode());
        result = prime * result + ((fromFacilityId == null) ? 0 : fromFacilityId.hashCode());
        result = prime * result + ((fromLocationId == null) ? 0 : fromLocationId.hashCode());
        result = prime * result + ((fromOrganizationId == null) ? 0 : fromOrganizationId.hashCode());
        result = prime * result + ((toCity == null) ? 0 : toCity.hashCode());
        result = prime * result + ((toFacilityId == null) ? 0 : toFacilityId.hashCode());
        result = prime * result + ((toLocationId == null) ? 0 : toLocationId.hashCode());
        result = prime * result + ((toOrganizationId == null) ? 0 : toOrganizationId.hashCode());
        result = prime * result + ((toProvinceState == null) ? 0 : toProvinceState.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ExternalMovementActivityEntity other = (ExternalMovementActivityEntity) obj;
        if (arrestOrganizationId == null) {
            if (other.arrestOrganizationId != null) {
                return false;
            }
        } else if (!arrestOrganizationId.equals(other.arrestOrganizationId)) {
            return false;
        }
        if (escortDetails == null) {
            if (other.escortDetails != null) {
                return false;
            }
        } else if (!escortDetails.equals(other.escortDetails)) {
            return false;
        }
        if (escortOrganizationId == null) {
            if (other.escortOrganizationId != null) {
                return false;
            }
        } else if (!escortOrganizationId.equals(other.escortOrganizationId)) {
            return false;
        }
        if (fromCity == null) {
            if (other.fromCity != null) {
                return false;
            }
        } else if (!fromCity.equals(other.fromCity)) {
            return false;
        }
        if (fromFacilityId == null) {
            if (other.fromFacilityId != null) {
                return false;
            }
        } else if (!fromFacilityId.equals(other.fromFacilityId)) {
            return false;
        }
        if (fromLocationId == null) {
            if (other.fromLocationId != null) {
                return false;
            }
        } else if (!fromLocationId.equals(other.fromLocationId)) {
            return false;
        }
        if (fromOrganizationId == null) {
            if (other.fromOrganizationId != null) {
                return false;
            }
        } else if (!fromOrganizationId.equals(other.fromOrganizationId)) {
            return false;
        }
        if (toCity == null) {
            if (other.toCity != null) {
                return false;
            }
        } else if (!toCity.equals(other.toCity)) {
            return false;
        }
        if (toFacilityId == null) {
            if (other.toFacilityId != null) {
                return false;
            }
        } else if (!toFacilityId.equals(other.toFacilityId)) {
            return false;
        }
        if (toLocationId == null) {
            if (other.toLocationId != null) {
                return false;
            }
        } else if (!toLocationId.equals(other.toLocationId)) {
            return false;
        }
        if (toOrganizationId == null) {
            if (other.toOrganizationId != null) {
                return false;
            }
        } else if (!toOrganizationId.equals(other.toOrganizationId)) {
            return false;
        }
        if (toProvinceState == null) {
            if (other.toProvinceState != null) {
                return false;
            }
        } else if (!toProvinceState.equals(other.toProvinceState)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ExternalMovementActivityEntity [fromLocationId=" + fromLocationId + ", fromOrganizationId=" + fromOrganizationId
                + ", fromFacilityId=" + fromFacilityId + ", fromCity=" + fromCity + ", toLocationId=" + toLocationId
                + ", toOrganizationId=" + toOrganizationId + ", toFacilityId=" + toFacilityId + ", toCity=" + toCity
                + ", toProvinceState=" + toProvinceState + ", arrestOrganizationId=" + arrestOrganizationId + ", applicationDate=" + applicationDate
                + ", escortOrganizationId=" + escortOrganizationId + ", escortDetails=" + escortDetails + ", reportingDate=" + reportingDate
                + ", transportation=" + transportation + ", toString()=" + super.toString() + "]";
    }

}