package syscon.arbutus.product.services.movement.contract.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;
import syscon.arbutus.product.services.core.contract.dto.ServiceConstants;

/**
 * Two Movements base DTO
 *
 * @author wmadruga
 * @version 1.0
 * @since September 03, 2013.
 */

//@ArbutusConstraint(constraints = "moveDate <= returnDate")
public class TwoMovementsType implements Serializable {

    private static final long serialVersionUID = -4579536886393782187L;
    //Approval attributes, child class must implement ApprovableMovement to gain access.
    @Size(max = ServiceConstants.TEXT_LENGTH_LARGE, message = "Cannot exceed 512 characters")
    protected String approvalComments;
    protected Long approverId;
    protected Date approvalDate;
    @NotNull
    private Date moveDate;
    @NotNull
    private Date returnDate;
    @NotNull
    private Long fromFacilityId;
    @NotNull
    private String movementReason; //ReferenceSet(MovementReason)
    @NotNull
    private String movementInStatus; //ReferenceSet(MovementStatus)
    @NotNull
    private String movementOutStatus; //ReferenceSet(MovementStatus)
    @NotNull
    private String movementType; //ReferenceSet(MovementType)
    @NotNull
    private Long supervisionId;
    @Size(max = ServiceConstants.TEXT_LENGTH_LARGE, message = "Cannot exceed 512 characters")
    private String comments;
    private Long toFacilityId;
    private String groupId;
    private Long movementIn;
    private Long movementOut;
    private Long offenderId;
    //	Client may choose to override existing conflicts and create a movement anyways. Setting this attribute to true should allow that.
    private String offenderName;
    private String movementOutCome; //ReferenceSet(MovementOutcome)
    // defines if this object is a target or not (to be tested against other movements in drools)
    private boolean bypassConflicts;
    private Set<TwoMovementsType> conflicts; // List of existing conflicts for the request
    private boolean target;
    private String movementCategory;

    public TwoMovementsType() {
        super();
    }

    /**
     * Constructor
     *
     * @param groupId           - the grouping id, used to bind two external movements together - optional, null for creation, it must be provided for update.
     * @param supervisionId     - the supervision id, required.
     * @param movementIn        - the IN direction external movement id - optional, null for creation, it must be provided for update.
     * @param movementOut       - the OUT direction external movement id - optional, null for creation, it must be provided for update.
     * @param fromFacilityId    - the facility id where the offender belongs, required.
     * @param toFacilityId      - the facility id where the offender is going to, required - child object should take care of validation - destination will not be always a facility.
     * @param moveDate          - the movement OUT date, required.
     * @param returnDate        - the movement IN date, required.
     * @param movementType      - the movement type, required.
     * @param movementReason    - the movement reason, required.
     * @param movementInStatus  - the movement status for direction IN, required.
     * @param movementOutStatus - the movement status for direction OUT, required.
     * @param movementCategory  - the movement category (INTERNAL / EXTERNAL), required
     * @param comments          - commentaries related to request, optional.
     * @param offenderId        - offender id to be shown in the grid, optional.
     * @param offenderName      - offender name to be shown in the grid, optional.
     * @param movementOutCome   - the movement outcome, optional.
     * @param approverId        - the person id of the approver, optional, it must be provided if it is being approved/rejected.
     * @param approvalDate      - date of the approval/rejection, optional, it must be provided if it is being approved/rejected.
     * @param approvalComments  - commentaries related to the approval/reject, optional, it must be provided if it is being approved/rejected.
     */
    public TwoMovementsType(String groupId, Long supervisionId, Long movementIn, Long movementOut, Long fromFacilityId, Long toFacilityId, Date moveDate, Date returnDate,
            String movementType, String movementReason, String movementInStatus, String movementOutStatus, String movementCategory, String comments, Long offenderId,
            String offenderName, String movementOutCome, Long approverId, Date approvalDate, String approvalComments) {
        super();
        this.groupId = groupId;
        this.supervisionId = supervisionId;
        this.movementIn = movementIn;
        this.movementOut = movementOut;
        this.fromFacilityId = fromFacilityId;
        this.toFacilityId = toFacilityId;
        this.moveDate = moveDate;
        this.returnDate = returnDate;
        this.movementType = movementType;
        this.movementReason = movementReason;
        this.movementInStatus = movementInStatus;
        this.movementOutStatus = movementOutStatus;
        this.movementCategory = movementCategory;
        this.comments = comments;
        this.offenderId = offenderId;
        this.offenderName = offenderName;
        this.movementOutCome = movementOutCome;
        this.approverId = approverId;
        this.approvalDate = approvalDate;
        this.approvalComments = approvalComments;
    }

    /*
     * External Movements Conflict check constructor
     */
    public TwoMovementsType(String groupId, Long supervisionId, Long movementIn, Long movementOut, Long fromFacilityId, Long toFacilityId, Date moveDate, Date returnDate,
            String movementType, String movementReason, String movementInStatus, String movementOutStatus, String movementCategory) {

        super();
        this.groupId = groupId;
        this.supervisionId = supervisionId;
        this.movementIn = movementIn;
        this.movementOut = movementOut;
        this.fromFacilityId = fromFacilityId;
        this.toFacilityId = toFacilityId;
        this.moveDate = moveDate;
        this.returnDate = returnDate;
        this.movementType = movementType;
        this.movementReason = movementReason;
        this.movementInStatus = movementInStatus;
        this.movementOutStatus = movementOutStatus;
        this.movementCategory = movementCategory;
    }

    /*
     * Internal Movements Conflict check constructor
     */
    public TwoMovementsType(Long groupId, Date moveDate, Date returnDate, String statusIn, String statusOut, String movementType, Long toFacilityId,
            String movementCategory) {

        super();
        this.toFacilityId = toFacilityId;
        this.groupId = groupId.toString();
        this.moveDate = moveDate;
        this.returnDate = returnDate;
        this.movementInStatus = statusIn;
        this.movementOutStatus = statusOut;
        this.movementType = movementType;
        this.movementCategory = movementCategory;
    }

    /**
     * Gets the value of the movementCategory property
     *
     * @return String
     */
    public String getMovementCategory() {
        return movementCategory;
    }

    /**
     * Sets the value of the movementCategory property
     */
    public void setMovementCategory(String movementCategory) {
        this.movementCategory = movementCategory;
    }

    /**
     * Gets the value of the moveDate property. the movement OUT date, required.
     *
     * @return the moveDate
     */
    public Date getMoveDate() {
        return moveDate;
    }

    /**
     * Sets the value of the moveDate property. the movement OUT date, required.
     *
     * @param moveDate Date
     */
    public void setMoveDate(Date moveDate) {
        this.moveDate = moveDate;
    }

    /**
     * Gets the value of the returnDate property. the movement IN date, required.
     *
     * @return the returnDate
     */
    public Date getReturnDate() {
        return returnDate;
    }

    /**
     * Sets the value of the returnDate property. the movement IN date, required.
     *
     * @param returnDate Date
     */
    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    /**
     * Gets the value of the fromFacilityId property. the facility id where the offender belongs, required.
     *
     * @return the fromFacilityId
     */
    public Long getFromFacilityId() {
        return fromFacilityId;
    }

    /**
     * Sets the value of the fromFacilityId property. the facility id where the offender belongs, required.
     *
     * @param fromFacilityId Long
     */
    public void setFromFacilityId(Long fromFacilityId) {
        this.fromFacilityId = fromFacilityId;
    }

    /**
     * Gets the value of the movementReason property. the movement reason, required.
     *
     * @return the movementReason
     */
    public String getMovementReason() {
        return movementReason;
    }

    /**
     * Sets the value of the movementReason property. the movement reason, required.
     *
     * @param movementReason String
     */
    public void setMovementReason(String movementReason) {
        this.movementReason = movementReason;
    }

    /**
     * Gets the value of the movementInStatus property. the movement IN status, required.
     *
     * @return the movementInStatus
     */
    public String getMovementInStatus() {
        return movementInStatus;
    }

    /**
     * Sets the value of the movementInStatus property. the movement IN status, required.
     *
     * @param movementInStatus String
     */
    public void setMovementInStatus(String movementInStatus) {
        this.movementInStatus = movementInStatus;
    }

    /**
     * Gets the value of the movementOutStatus property. the movement OUT status, required.
     *
     * @return the movemenOuttStatus
     */
    public String getMovementOutStatus() {
        return movementOutStatus;
    }

    /**
     * Sets the value of the movementOutStatus property. the movement OUT status, required.
     *
     * @param movementOutStatus String
     */
    public void setMovementOutStatus(String movementOutStatus) {
        this.movementOutStatus = movementOutStatus;
    }

    /**
     * Gets the value of the movementType property. the movement type, required.
     *
     * @return the movementType
     */
    public String getMovementType() {
        return movementType;
    }

    /**
     * Sets the value of the movementType property. the movement type, required.
     *
     * @param movementType String
     */
    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    /**
     * Gets the value of the supervisionId property. the supervision id, required.
     *
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * Sets the value of the supervisionId property. the supervision id, required.
     *
     * @param supervisionId Long
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Gets the value of the comments property. commentaries related to request, optional.
     *
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property. commentaries related to request, optional.
     *
     * @param comments String
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * Gets the value of the toFacilityId property.
     * the facility id where the offender is going to, required - child object should take care of validation - destination will not be always a facility.
     *
     * @return the toFacilityId
     */
    public Long getToFacilityId() {
        return toFacilityId;
    }

    /**
     * Sets the value of the toFacilityId property.
     * the facility id where the offender is going to, required - child object should take care of validation - destination will not be always a facility.
     *
     * @param toFacilityId Long
     */
    public void setToFacilityId(Long toFacilityId) {
        this.toFacilityId = toFacilityId;
    }

    /**
     * Gets the value of the groupId property.
     * the grouping id, used to bind two external movements together - optional, null for creation, it must be provided for update.
     *
     * @return the groupId
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Sets the value of the groupId property.
     * the grouping id, used to bind two external movements together - optional, null for creation, it must be provided for update.
     *
     * @param groupId String
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * Gets the value of the movementIn property. the IN direction external movement id - optional, null for creation, it must be provided for update.
     *
     * @return the movementIn
     */
    public Long getMovementIn() {
        return movementIn;
    }

    /**
     * Sets the value of the movementIn property. the IN direction external movement id - optional, null for creation, it must be provided for update.
     *
     * @param movementIn Long
     */
    public void setMovementIn(Long movementIn) {
        this.movementIn = movementIn;
    }

    /**
     * Gets the value of the movementOut property. the OUT direction external movement id - optional, null for creation, it must be provided for update.
     *
     * @return the movementOut
     */
    public Long getMovementOut() {
        return movementOut;
    }

    /**
     * Sets the value of the movementOut property. the OUT direction external movement id - optional, null for creation, it must be provided for update.
     *
     * @param movementOut Long
     */
    public void setMovementOut(Long movementOut) {
        this.movementOut = movementOut;
    }

    /**
     * Gets the value of the offenderId property. offender id to be shown in the grid, optional.
     *
     * @return the offenderId
     */
    public Long getOffenderId() {
        return offenderId;
    }

    /**
     * Sets the value of the offenderId property. offender id to be shown in the grid, optional.
     *
     * @param offenderId Long
     */
    public void setOffenderId(Long offenderId) {
        this.offenderId = offenderId;
    }

    /**
     * Gets the value of the offenderName property. offender name to be shown in the grid, optional.
     *
     * @return the offenderName
     */
    public String getOffenderName() {
        return offenderName;
    }

    /**
     * Sets the value of the offenderName property. offender name to be shown in the grid, optional.
     *
     * @param offenderName String
     */
    public void setOffenderName(String offenderName) {
        this.offenderName = offenderName;
    }

    /**
     * Gets the value of the movementOutCome property. the movement outcome, optional.
     *
     * @return the movementOutCome
     */
    public String getMovementOutCome() {
        return movementOutCome;
    }

    /**
     * Sets the value of the movementOutCome property. the movement outcome, optional.
     *
     * @param movementOutCome String
     */
    public void setMovementOutCome(String movementOutCome) {
        this.movementOutCome = movementOutCome;
    }

    /**
     * Client may choose to override existing conflicts and create a movement anyways.
     * Setting this attribute to true should allow that.
     * Gets the value of the bypassConflicts property. Optional
     *
     * @return the bypassConflicts
     */
    public boolean isBypassConflicts() {
        return bypassConflicts;
    }

    /**
     * Client may choose to override existing conflicts and create a movement anyways.
     * Setting this attribute to true should allow that.
     * Sets the value of bypassConflicts property. Optional
     *
     * @param bypassConflicts boolean
     */
    public void setBypassConflicts(boolean bypassConflicts) {
        this.bypassConflicts = bypassConflicts;
    }

    /**
     * Gets the value of the conflicts property. Optional
     *
     * @return the conflicts
     */
    public Set<TwoMovementsType> getConflicts() {
        if (this.conflicts == null) {
            this.conflicts = new HashSet<TwoMovementsType>();
        }
        return conflicts;
    }

    /**
     * Sets the value of the conflicts property. Optional
     *
     * @param conflicts List<TwoMovementsType>
     */
    public void setConflicts(Set<TwoMovementsType> conflicts) {
        this.conflicts = conflicts;
    }

    /**
     * Gets the value of the target property
     *
     * @return boolean
     */
    public boolean isTarget() {
        return target;
    }

    /**
     * Sets the value of the target property
     */
    public void setTarget(boolean target) {
        this.target = target;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
        result = prime * result + ((fromFacilityId == null) ? 0 : fromFacilityId.hashCode());
        result = prime * result + ((moveDate == null) ? 0 : moveDate.hashCode());
        result = prime * result + ((movementInStatus == null) ? 0 : movementInStatus.hashCode());
        result = prime * result + ((movementOutStatus == null) ? 0 : movementOutStatus.hashCode());
        result = prime * result + ((movementReason == null) ? 0 : movementReason.hashCode());
        result = prime * result + ((movementType == null) ? 0 : movementType.hashCode());
        result = prime * result + ((returnDate == null) ? 0 : returnDate.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TwoMovementsType other = (TwoMovementsType) obj;
        if (groupId == null) {
            if (other.groupId != null) {
                return false;
            }
        } else if (!groupId.equals(other.groupId)) {
            return false;
        }
        if (fromFacilityId == null) {
            if (other.fromFacilityId != null) {
                return false;
            }
        } else if (!fromFacilityId.equals(other.fromFacilityId)) {
            return false;
        }
        if (moveDate == null) {
            if (other.moveDate != null) {
                return false;
            }
        } else if (!moveDate.equals(other.moveDate)) {
            return false;
        }
        if (movementInStatus == null) {
            if (other.movementInStatus != null) {
                return false;
            }
        } else if (!movementInStatus.equals(other.movementInStatus)) {
            return false;
        }
        if (movementOutStatus == null) {
            if (other.movementOutStatus != null) {
                return false;
            }
        } else if (!movementOutStatus.equals(other.movementOutStatus)) {
            return false;
        }
        if (movementReason == null) {
            if (other.movementReason != null) {
                return false;
            }
        } else if (!movementReason.equals(other.movementReason)) {
            return false;
        }
        if (movementType == null) {
            if (other.movementType != null) {
                return false;
            }
        } else if (!movementType.equals(other.movementType)) {
            return false;
        }
        if (returnDate == null) {
            if (other.returnDate != null) {
                return false;
            }
        } else if (!returnDate.equals(other.returnDate)) {
            return false;
        }
        if (supervisionId == null) {
            if (other.supervisionId != null) {
                return false;
            }
        } else if (!supervisionId.equals(other.supervisionId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TwoMovementsType [moveDate=" + moveDate + ", returnDate=" + returnDate + ", fromFacilityId=" + fromFacilityId + ", movementReason=" + movementReason
                + ", movementInStatus=" + movementInStatus + ", movementOutStatus=" + movementOutStatus + ", movementType=" + movementType + ", supervisionId="
                + supervisionId + ", comments=" + comments + ", toFacilityId=" + toFacilityId + ", groupId=" + groupId + ", movementIn=" + movementIn + ", movementOut="
                + movementOut + ", offenderId=" + offenderId + ", offenderName=" + offenderName + ", movementOutCome=" + movementOutCome + ", bypassConflicts="
                + bypassConflicts + ", target=" + target + "]";
    }

}