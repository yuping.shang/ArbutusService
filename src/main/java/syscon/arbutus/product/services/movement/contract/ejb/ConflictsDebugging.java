package syscon.arbutus.product.services.movement.contract.ejb;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.services.movement.contract.dto.TwoMovementsType;

/**
 * Helps debugging movement rules
 *
 * @author wmadruga
 */
public class ConflictsDebugging {

    private static Logger log = LoggerFactory.getLogger(ConflictsDebugging.class);

    protected static void printConflicts(Boolean enabled, TwoMovementsType move, String banner, String message) {
        if (enabled) {
            log.info(banner);
            log.info(message);
            for (TwoMovementsType conflict : move.getConflicts()) {
                log.info(conflict.toString());
            }
        }
    }

    protected static void debuggingBeforeRule(Boolean enabled, TwoMovementsType move, List<TwoMovementsType> dbMovements, List<Object> facts) {
        if (enabled) {
            log.info(" === MOVEMENT SCHEDULING CONFLICT CHECK DEBUGGING BEFORE RULE ===");
            log.info("Movement being created/edited:");
            log.info(move.toString());
            log.info("DB Movements:");

            for (TwoMovementsType dbmove : dbMovements) {
                log.info(dbmove.toString());
            }

            log.info("FACTS:");
            for (Object fact : facts) {
                log.info(fact.toString());
            }
        }
    }

}