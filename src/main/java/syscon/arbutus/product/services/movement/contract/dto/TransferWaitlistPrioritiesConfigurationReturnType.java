package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.List;

/**
 * TransferWaitlistPrioritiesConfigurationReturnType for MovementActivity Service
 *
 * @author yshang
 * @version 2.0
 * @since October 25, 2012
 */
public class TransferWaitlistPrioritiesConfigurationReturnType implements Serializable {

    private static final long serialVersionUID = 7941203488838104589L;

    private List<TransferWaitlistPriorityConfigurationType> transferWaitlistPriorityConfiguration;

    private Long totalSize;

    /**
     * @return the transferWaitlistPriorityConfiguration
     */
    public List<TransferWaitlistPriorityConfigurationType> getTransferWaitlistPriorityConfiguration() {
        return transferWaitlistPriorityConfiguration;
    }

    /**
     * @param transferWaitlistPriorityConfiguration the transferWaitlistPriorityConfiguration to set
     */
    public void setTransferWaitlistPriorityConfiguration(List<TransferWaitlistPriorityConfigurationType> transferWaitlistPriorityConfiguration) {
        this.transferWaitlistPriorityConfiguration = transferWaitlistPriorityConfiguration;
    }

    /**
     * @return the totalSize
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * @param totalSize the totalSize to set
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TransferWaitlistPrioritiesConfigurationReturnType [transferWaitlistPriorityConfiguration=" + transferWaitlistPriorityConfiguration + ", totalSize="
                + totalSize + "]";
    }

}
