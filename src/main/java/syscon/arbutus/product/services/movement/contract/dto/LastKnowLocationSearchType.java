package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;

/**
 * Created by dev on 5/21/14.
 */
public class LastKnowLocationSearchType implements Serializable {
    private static final long serialVersionUID = -8473895489545298799L;

    private Long recordId;
    private Long facilityId;
    private Long internalLocationId;
    private Long supervisionId;
    private Boolean active;

    public LastKnowLocationSearchType() {
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    public Long getInternalLocationId() {
        return internalLocationId;
    }

    public void setInternalLocationId(Long internalLocationId) {
        this.internalLocationId = internalLocationId;
    }

    public Long getSupervisionId() {
        return supervisionId;
    }

    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
