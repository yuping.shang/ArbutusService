package syscon.arbutus.product.services.movement.realization.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.core.services.realization.BaseEntity;

/**
 * CourtMovementReturnTimeConfigEntity for MovementActivity Service
 *
 * @author yshang
 * @version 2.0
 * @DbComment MA_CrtRetTime 'Court Movement Return Time Configuration table for Movement Activity Service'
 * @DbComment .createUserId 'User ID who created the object'
 * @DbComment .createDateTime 'Date and time when the object was created'
 * @DbComment .modifyUserId 'User ID who last updated the object'
 * @DbComment .modifyDateTime 'Date and time when the object was last updated'
 * @DbComment .invocationContext 'Invocation context when the create/update action called'
 * @DbComment .version 'the version number of record updated'
 * @since October 25, 2012
 */
@Audited
@Entity
@Table(name = "MA_CrtRetTime")
public class CourtMovementReturnTimeConfigEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 3112495972127188578L;

    /**
     * @DbComment returnTime 'The default return time for all court movements'
     */
    @Id
    @Column(name = "returnTime", nullable = false)
    private String returnTime;

    public CourtMovementReturnTimeConfigEntity() {
        super();
    }

    public CourtMovementReturnTimeConfigEntity(String returnTime) {
        super();
        this.returnTime = returnTime;
    }

    @Override
    public Serializable getId() {
        return returnTime;
    }

    /**
     * @return the returnTime
     */
    public String getReturnTime() {
        return returnTime;
    }

    /**
     * @param returnTime the returnTime to set
     */
    public void setReturnTime(String returnTime) {
        this.returnTime = returnTime;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((returnTime == null) ? 0 : returnTime.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CourtMovementReturnTimeConfigEntity other = (CourtMovementReturnTimeConfigEntity) obj;
        if (returnTime == null) {
            if (other.returnTime != null) {
                return false;
            }
        } else if (!returnTime.equals(other.returnTime)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CourtMovementReturnTimeConfigEntity [returnTime=" + returnTime + ", stamp=" + getStamp() + "]";
    }

}
