package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;

import syscon.arbutus.product.services.core.contract.dto.BaseDto;

/**
 * Created by jliang on 12/11/15.
 */
public class MovementCategoryConfiguration extends BaseDto implements Serializable {

    private String movementType;
    private String movementReason;
    private String movementCategory;
    private boolean active;
    private String movementTemplate;
    private boolean requireApproval;

    /**
     *
     */
    public MovementCategoryConfiguration() {

    }

    /**
     *
     * @param movementType
     * @param movementReason
     * @param movementCategory
     * @param active
     */
    public MovementCategoryConfiguration(String movementType,
                                         String movementReason,
                                         String movementCategory,
                                         boolean active,
                                         String movementTemplate) {

        this.movementType = movementType;
        this.movementReason = movementReason;
        this.movementCategory = movementCategory;
        this.active = active;
        this.movementTemplate = movementTemplate;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getMovementReason() {
        return movementReason;
    }

    public void setMovementReason(String movementReason) {
        this.movementReason = movementReason;
    }

    public String getMovementCategory() {
        return movementCategory;
    }

    public void setMovementCategory(String movementCategory) {
        this.movementCategory = movementCategory;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getMovementTemplate() {
        return movementTemplate;
    }

    public void setMovementTemplate(String movementTemplate) {
        this.movementTemplate = movementTemplate;
    }

    public boolean isRequireApproval() {
        return requireApproval;
    }

    public void setRequireApproval(boolean requireApproval) {
        this.requireApproval = requireApproval;
    }
}
