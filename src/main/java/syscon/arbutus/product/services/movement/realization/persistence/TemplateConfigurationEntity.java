package syscon.arbutus.product.services.movement.realization.persistence;

import javax.persistence.*;

import java.io.Serializable;

import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.core.services.realization.BaseEntity;

/**
 * Created by jliang on 12/21/15.
 */
@Audited
@Entity
@Table(name = "MA_TemplateConfiguration")
public class TemplateConfigurationEntity extends BaseEntity implements Serializable {

    private final static long serialVersionUID = 181973434912374L;

    @EmbeddedId
    private CategoryConfigurationPK categoryConfigurationPK;

    @Column(name = "movementCategory", length = 64, nullable = false)
    private String movementCategory;

    @Column(name = "active", nullable = false)
    private Boolean active = Boolean.TRUE;

    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    @Column(name = "movementTemplate", length = 64, nullable = false)
    private String movementTemplate;

    @Column(name = "RequireApprovalInd", nullable = false)
    private  Boolean requireApproval = Boolean.FALSE;


    public TemplateConfigurationEntity() {

    }

    @Override
    public CategoryConfigurationPK getId() {
        return categoryConfigurationPK;
    }


    public CategoryConfigurationPK getCategoryConfigurationPK() {
        return categoryConfigurationPK;
    }

    public void setCategoryConfigurationPK(CategoryConfigurationPK categoryConfigurationPK) {
        this.categoryConfigurationPK = categoryConfigurationPK;
    }

    public String getMovementCategory() {
        return movementCategory;
    }

    public void setMovementCategory(String movementCategory) {
        this.movementCategory = movementCategory;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getMovementTemplate() {
        return movementTemplate;
    }

    public void setMovementTemplate(String movementTemplate) {
        this.movementTemplate = movementTemplate;
    }

    public Boolean getRequireApproval() {
        return requireApproval;
    }

    public void setRequireApproval(Boolean requireApproval) {
        this.requireApproval = requireApproval;
    }

    /**
     * @return the flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }
}
