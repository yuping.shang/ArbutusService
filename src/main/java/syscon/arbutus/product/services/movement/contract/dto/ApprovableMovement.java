package syscon.arbutus.product.services.movement.contract.dto;

import java.util.Date;

/**
 * Enables use of approve/deny related fields in Classes extending TwoMovementsType.
 *
 * @author wmadruga
 *         2013-09-05
 */
public interface ApprovableMovement {

    /**
     * Gets the value of the approvalComments property.
     * commentaries related to the approval/reject, optional, it must be provided if it is being approved/rejected.
     *
     * @return the approvalComments
     */
    public String getApprovalComments();

    /**
     * Sets the value of the approvalComments property.
     * commentaries related to the approval/reject, optional, it must be provided if it is being approved/rejected.
     *
     * @param approvalComments String
     */
    public void setApprovalComments(String approvalComments);

    /**
     * Gets the value of the approverId property. the person id of the approver, optional, it must be provided if it is being approved/rejected.
     *
     * @return the approverId
     */
    public Long getApproverId();

    /**
     * Sets the value of the approverId property. the person id of the approver, optional, it must be provided if it is being approved/rejected.
     *
     * @param approverId Long
     */
    public void setApproverId(Long approverId);

    /**
     * Gets the value of the approvalDate property. date of the approval/rejection, optional, it must be provided if it is being approved/rejected.
     *
     * @return the approvalDate
     */
    public Date getApprovalDate();

    /**
     * Sets the value of the approvalDate property. date of the approval/rejection, optional, it must be provided if it is being approved/rejected.
     *
     * @param approvalDate Date
     */
    public void setApprovalDate(Date approvalDate);

}