package syscon.arbutus.product.services.movement.contract.interfaces;

import java.util.List;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.movement.contract.dto.LastKnowLocationGridEntryType;
import syscon.arbutus.product.services.movement.contract.dto.LastKnowLocationGridReturnType;
import syscon.arbutus.product.services.movement.contract.dto.LastKnowLocationSearchType;
import syscon.arbutus.product.services.movement.contract.dto.LastKnowLocationType;

/**
 * Created by dev on 5/21/14.
 */
public interface LastKnowLocation {
    /**
     * Creates new lastKnowLocation with given list.
     *
     * @param uc,                   {@link syscon.arbutus.product.security.UserContext}, required
     * @param lastKnowLocationList, list of {@link syscon.arbutus.product.services.movement.contract.dto.LastKnowLocationType}, required
     * @return list of {@link syscon.arbutus.product.services.movement.contract.dto.LastKnowLocationGridEntryType}
     */
    public List<LastKnowLocationGridEntryType> createLastKnowLocation(UserContext uc, List<LastKnowLocationType> lastKnowLocationList);

    /**
     * Search the lastKnowLocation with given criteria
     *
     * @param uc,     {@link syscon.arbutus.product.security.UserContext}, required
     * @param search, {@link syscon.arbutus.product.services.movement.contract.dto.LastKnowLocationSearchType} - criterias, required
     * @return list of {@link syscon.arbutus.product.services.movement.contract.dto.LastKnowLocationType}
     */
    public List<LastKnowLocationType> searchLastKnowLocation(UserContext uc, LastKnowLocationSearchType search);

    public LastKnowLocationGridReturnType searchLastKnowLocationGrid(UserContext uc, LastKnowLocationSearchType search, Long startIndex, Long resultSize,
            String resultOrder, String filer);

    public void indexDataBaseLastKnowLocation();

}
