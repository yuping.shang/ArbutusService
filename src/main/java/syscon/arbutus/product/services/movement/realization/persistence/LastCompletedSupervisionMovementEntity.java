package syscon.arbutus.product.services.movement.realization.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.core.services.realization.BaseEntity;

/**
 * LastCompletedSupervisionMovementEntity for Movement Activity Service
 *
 * @author yshang
 * @version 2.0
 * @DbComment MA_LastComplSupMove 'Last Completed Supervision Movement table for Movement Activity Service'
 * @DbComment .createUserId 'Create User Id'
 * @DbComment .createDateTime 'Create Date Time'
 * @DbComment .modifyUserId 'Modify User Id'
 * @DbComment .modifyDateTime 'Modify Date Time'
 * @DbComment .invocationContext 'Invocation Context'
 * @DbComment .version 'the version number of record updated'
 * @since Dec 06, 2012
 */
@Audited
@Entity
@Table(name = "MA_LastComplSupMove")
public class LastCompletedSupervisionMovementEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 181973434912377L;

    /**
     * @DbComment supervisionId 'Supervision Id'
     */
    @Id
    @Column(name = "supervisionId")
    private Long supervisionId;

    /**
     * @DbComment movementActivityId 'Movement Activity Id'
     */
    @Column(name = "movementActivityId")
    private Long movementActivityId;

    /**
     * @DbComment 'Current Internal Location Id'
     */
    @Column(name = "InteranlLocationId")
    private Long internalLocationId;



    public LastCompletedSupervisionMovementEntity() {
        super();
    }

    public LastCompletedSupervisionMovementEntity(Long supervisionId, Long movementActivityId, Long internalLocationId) {
        super();
        this.supervisionId = supervisionId;
        this.movementActivityId = movementActivityId;
        this.internalLocationId = internalLocationId;
    }

    @Override
    public Long getId() {
        return supervisionId;
    }

    /**
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * @return the movementActivityId
     */
    public Long getMovementActivityId() {
        return movementActivityId;
    }

    /**
     * @param movementActivityId the movementActivityId to set
     */
    public void setMovementActivityId(Long movementActivityId) {
        this.movementActivityId = movementActivityId;
    }

    /**
     * @return the internalLocationId
     */
    public Long getInternalLocationId() {
        return internalLocationId;
    }

    /**
     * @param internalLocationId the internalLocationId to set
     */
    public void setInternalLocationId(Long internalLocationId) {
        this.internalLocationId = internalLocationId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((movementActivityId == null) ? 0 : movementActivityId.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LastCompletedSupervisionMovementEntity other = (LastCompletedSupervisionMovementEntity) obj;
        if (movementActivityId == null) {
            if (other.movementActivityId != null) {
                return false;
            }
        } else if (!movementActivityId.equals(other.movementActivityId)) {
            return false;
        }
        if (supervisionId == null) {
            if (other.supervisionId != null) {
                return false;
            }
        } else if (!supervisionId.equals(other.supervisionId)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "LastCompletedSupervisionMovementEntity [supervisionId=" + supervisionId + ", movementActivityId=" + movementActivityId + ", internalLocationId="
                + internalLocationId + ", stamp=" + getStamp() + "]";
    }

}
