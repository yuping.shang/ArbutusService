package syscon.arbutus.product.services.movement.realization.persistence;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.hibernate.search.annotations.Factory;
import org.hibernate.search.annotations.Key;
import org.hibernate.search.filter.FilterKey;
import org.hibernate.search.filter.StandardFilterKey;

/**
 * Created on 5/28/14.
 *
 * @author hshen
 */
public class LastKnowLocationFacilityFilterFactor {
    private Long facilityId;

    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    @Key
    public FilterKey getKey() {
        StandardFilterKey key = new StandardFilterKey();
        key.addParameter(facilityId);
        return key;

    }

    @Factory
    public Filter getFilter() {
        Query query = new TermQuery(new Term("facilityId", facilityId.toString()));
        return new CachingWrapperFilter(new QueryWrapperFilter(query));

    }
}
