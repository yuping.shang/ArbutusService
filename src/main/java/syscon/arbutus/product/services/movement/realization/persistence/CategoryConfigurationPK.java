package syscon.arbutus.product.services.movement.realization.persistence;

import javax.persistence.Column;
import java.io.Serializable;

/**
 * Created by jliang on 12/21/15.
 */
public class CategoryConfigurationPK implements Serializable {

    private final static long serialVersionUID = 181973434912374L;

    @Column(name = "MovementType", length = 64, nullable = false)
    private String movementType;

    @Column(name = "MovementReason", length = 64, nullable = false)
    private String movementReason;

    public CategoryConfigurationPK() {

    }

    public CategoryConfigurationPK(String movementType, String movementReason) {
        this.movementType = movementType;
        this.movementReason = movementReason;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getMovementReason() {
        return movementReason;
    }

    public void setMovementReason(String movementReason) {
        this.movementReason = movementReason;
    }
}
