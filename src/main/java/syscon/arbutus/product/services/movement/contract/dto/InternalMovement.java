package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class InternalMovement implements Serializable {

    private static final long serialVersionUID = -3981555676749703891L;
    private Long supervisionId;
    private String lastName;
    private String firstName;
    private String offenderNo;
    private Long fromLocationId;
    private String fromLocation;
    private Long toLocationId;
    private String toLocation;
    private Date moveDate;
    private Date moveTime;
    private String moveReason;
    private String moveOutcomeCd;
    private String moveOutcome;
    private String comment;
    private boolean checked;
    private boolean selected;
    private Long personId;

    public InternalMovement() {

    }

    public InternalMovement(Long supervisionId, String lastName, String firstName, String offenderNo, Long fromLocationId, String fromLocation, Long toLocationId,
            String toLocation, Date moveDate, Date moveTime, String moveReasonCd, String moveReason, String moveOutcomeCd, String moveOutcome, String comment) {
        super();
        this.supervisionId = supervisionId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.offenderNo = offenderNo;
        this.fromLocationId = fromLocationId;
        this.fromLocation = fromLocation;
        this.toLocationId = toLocationId;
        this.toLocation = toLocation;
        this.moveDate = moveDate;
        this.moveTime = moveTime;
        this.moveReason = moveReason;
        this.moveOutcomeCd = moveOutcomeCd;
        this.moveOutcome = moveOutcome;
        this.comment = comment;
    }

    public Long getSupervisionId() {
        return supervisionId;
    }

    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOffenderNo() {
        return offenderNo;
    }

    public void setOffenderNo(String offenderNo) {
        this.offenderNo = offenderNo;
    }

    public Long getFromLocationId() {
        return fromLocationId;
    }

    public void setFromLocationId(Long fromLocationId) {
        this.fromLocationId = fromLocationId;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public Long getToLocationId() {
        return toLocationId;
    }

    public void setToLocationId(Long toLocationId) {
        this.toLocationId = toLocationId;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(String toLocation) {
        this.toLocation = toLocation;
    }

    public Date getMoveDate() {
        return moveDate;
    }

    public void setMoveDate(Date moveDate) {
        this.moveDate = moveDate;
    }

    public Date getMoveTime() {
        return moveTime;
    }

    public void setMoveTime(Date moveTime) {
        this.moveTime = moveTime;
    }

    public String getMoveReason() {
        return moveReason;
    }

    public void setMoveReason(String moveReason) {
        this.moveReason = moveReason;
    }

    public String getMoveOutcomeCd() {
        return moveOutcomeCd;
    }

    public void setMoveOutcomeCd(String moveOutcomeCd) {
        this.moveOutcomeCd = moveOutcomeCd;
    }

    public String getMoveOutcome() {
        return moveOutcome;
    }

    public void setMoveOutcome(String moveOutcome) {
        this.moveOutcome = moveOutcome;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean hasSupervisionId(List<? extends InternalMovement> movements, Long supervisionId) {
        if (supervisionId == null) {
            return false;
        }
        if (null != movements) {
            for (InternalMovement movement : movements) {
                if (null != movement.getSupervisionId()) {
                    if (movement.getSupervisionId().equals(supervisionId)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

}
