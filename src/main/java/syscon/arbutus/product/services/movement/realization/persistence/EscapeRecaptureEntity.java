package syscon.arbutus.product.services.movement.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by jliang on 11/30/15.
 */
public class EscapeRecaptureEntity implements Serializable {

    private static final long serialVersionUID = 6732094848477968564L;

    /**
     * escape/recapture Id
     */
    private Long escapeRecaptureId;

    /**
     * Inmate supervision
     */
    private Long supervisionId;

    /**
     * Facility escaped from
     */
    private Long escapeFacility;

    /**
     * Escape Reason
     */
    private String escapeReason;

    /**
     * Escape Date
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date escapeDate;

    /**
     * Escape from Custody.
     */
    private String fromCustody;

    /**
     * Escape securityLevel
     */
    private String securityBreached;

    /**
     * Escape Circumstance.
     */
    private String circumstance;

    /**
     * Escape Last Seen Date
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastSeenDate;

    /**
     * Escape incident Number
     */
    private String incidentNumber;

    /**
     * Escape Comment
     */
    private String escapeComments;

    /**
     * Recapture Date
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date recaptureDate;

    /**
     * Recapture arresting Organization
     */
    private Long arrestingOrganization;

    /**
     * Recapture Comments
     */
    private String recaptureComments;

    /**
     * Readmission Date
     */
    @Transient
    @Temporal(TemporalType.TIMESTAMP)
    private Date readmissionDate;

    /**
     * Readmission Facility
     */
    @Transient
    private Long readmissionFacility;

    /**
     * Readmission Reason
     */
    @Transient
    private String readmissionReason;

    /**
     * Readmission Comment
     */
    @Transient
    private String readmissionComment;

    private Long version;

    public EscapeRecaptureEntity() {

    }

    public EscapeRecaptureEntity(Long escapeRecaptureId, Long supervisionId, Long escapeFacility, String escapeReason, Date escapeDate, String fromCustody,
            String securityBreached, String circumstance, Date lastSeenDate, String incidentNumber, String escapeComments, Date recaptureDate, Long arrestingOrganization,
            String recaptureComments, Long version) {
        this.escapeRecaptureId = escapeRecaptureId;
        this.supervisionId = supervisionId;
        this.escapeFacility = escapeFacility;
        this.escapeReason = escapeReason;
        this.escapeDate = escapeDate;
        this.fromCustody = fromCustody;
        this.securityBreached = securityBreached;
        this.circumstance = circumstance;
        this.lastSeenDate = lastSeenDate;
        this.incidentNumber = incidentNumber;
        this.escapeComments = escapeComments;
        this.recaptureDate = recaptureDate;
        this.arrestingOrganization = arrestingOrganization;
        this.recaptureComments = recaptureComments;
        this.version = version;
    }

    public Long getEscapeRecaptureId() {
        return escapeRecaptureId;
    }

    public void setEscapeRecaptureId(Long escapeRecaptureId) {
        this.escapeRecaptureId = escapeRecaptureId;
    }

    public Long getSupervisionId() {
        return supervisionId;
    }

    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    public Long getEscapeFacility() {
        return escapeFacility;
    }

    public void setEscapeFacility(Long escapeFacility) {
        this.escapeFacility = escapeFacility;
    }

    public String getEscapeReason() {
        return escapeReason;
    }

    public void setEscapeReason(String escapeReason) {
        this.escapeReason = escapeReason;
    }

    public Date getEscapeDate() {
        return escapeDate;
    }

    public void setEscapeDate(Date escapeDate) {
        this.escapeDate = escapeDate;
    }

    public String getFromCustody() {
        return fromCustody;
    }

    public void setFromCustody(String fromCustody) {
        this.fromCustody = fromCustody;
    }

    public String getSecurityBreached() {
        return securityBreached;
    }

    public void setSecurityBreached(String securityBreached) {
        this.securityBreached = securityBreached;
    }

    public String getCircumstance() {
        return circumstance;
    }

    public void setCircumstance(String circumstance) {
        this.circumstance = circumstance;
    }

    public Date getLastSeenDate() {
        return lastSeenDate;
    }

    public void setLastSeenDate(Date lastSeenDate) {
        this.lastSeenDate = lastSeenDate;
    }

    public String getIncidentNumber() {
        return incidentNumber;
    }

    public void setIncidentNumber(String incidentNumber) {
        this.incidentNumber = incidentNumber;
    }

    public String getEscapeComments() {
        return escapeComments;
    }

    public void setEscapeComments(String escapeComments) {
        this.escapeComments = escapeComments;
    }

    public Date getRecaptureDate() {
        return recaptureDate;
    }

    public void setRecaptureDate(Date recaptureDate) {
        this.recaptureDate = recaptureDate;
    }

    public Long getArrestingOrganization() {
        return arrestingOrganization;
    }

    public void setArrestingOrganization(Long arrestingOrganization) {
        this.arrestingOrganization = arrestingOrganization;
    }

    public String getRecaptureComments() {
        return recaptureComments;
    }

    public void setRecaptureComments(String recaptureComments) {
        this.recaptureComments = recaptureComments;
    }

    public Date getReadmissionDate() {
        return readmissionDate;
    }

    public void setReadmissionDate(Date readmissionDate) {
        this.readmissionDate = readmissionDate;
    }

    public Long getReadmissionFacility() {
        return readmissionFacility;
    }

    public void setReadmissionFacility(Long readmissionFacility) {
        this.readmissionFacility = readmissionFacility;
    }

    public String getReadmissionReason() {
        return readmissionReason;
    }

    public void setReadmissionReason(String readmissionReason) {
        this.readmissionReason = readmissionReason;
    }

    public String getReadmissionComment() {
        return readmissionComment;
    }

    public void setReadmissionComment(String readmissionComment) {
        this.readmissionComment = readmissionComment;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
