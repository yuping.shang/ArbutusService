package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;

/**
 * TransferWaitlistPriorityConfigurationSearchType for search Transfer Waitlist Priority Configuration
 * <p>At least one search criteria must be specified.
 *
 * @author yshang
 */
public class TransferWaitlistPriorityConfigurationSearchType implements Serializable {

    private static final long serialVersionUID = 7941203488838104588L;

    /**
     * Textual description of a priority level for
     * the transfer waitlist (e.g., Low, Medium, High, Urgent)
     */
    private String transferWaitlistEntryPriority;

    /**
     * The level of priority is determined by this value. The highest level is 1,
     * and the lower priorities are larger numbers
     */
    private Long priorityLevel;

    public TransferWaitlistPriorityConfigurationSearchType() {
        super();
    }

    /**
     * Constructor
     *
     * @param transferWaitlistEntryPriority CodeType
     * @param priorityLevel                 Long
     */
    public TransferWaitlistPriorityConfigurationSearchType(String transferWaitlistEntryPriority, Long priorityLevel) {
        super();
        this.transferWaitlistEntryPriority = transferWaitlistEntryPriority;
        this.priorityLevel = priorityLevel;
    }

    /**
     * Textual description of a priority level for
     * the transfer waitlist (e.g., Low, Medium, High, Urgent)
     *
     * @return the transferWaitlistEntryPriority
     */
    public String getTransferWaitlistEntryPriority() {
        return transferWaitlistEntryPriority;
    }

    /**
     * Textual description of a priority level for
     * the transfer waitlist (e.g., Low, Medium, High, Urgent)
     *
     * @param transferWaitlistEntryPriority the transferWaitlistEntryPriority to set
     */
    public void setTransferWaitlistEntryPriority(String transferWaitlistEntryPriority) {
        this.transferWaitlistEntryPriority = transferWaitlistEntryPriority;
    }

    /**
     * The level of priority is determined by this value. The highest level is 1,
     * and the lower priorities are larger numbers
     *
     * @return the priorityLevel
     */
    public Long getPriorityLevel() {
        return priorityLevel;
    }

    /**
     * The level of priority is determined by this value. The highest level is 1,
     * and the lower priorities are larger numbers
     *
     * @param priorityLevel the priorityLevel to set
     */
    public void setPriorityLevel(Long priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((priorityLevel == null) ? 0 : priorityLevel.hashCode());
        result = prime * result + ((transferWaitlistEntryPriority == null) ? 0 : transferWaitlistEntryPriority.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TransferWaitlistPriorityConfigurationSearchType other = (TransferWaitlistPriorityConfigurationSearchType) obj;
        if (priorityLevel == null) {
            if (other.priorityLevel != null) {
                return false;
            }
        } else if (!priorityLevel.equals(other.priorityLevel)) {
            return false;
        }
        if (transferWaitlistEntryPriority == null) {
            if (other.transferWaitlistEntryPriority != null) {
                return false;
            }
        } else if (!transferWaitlistEntryPriority.equals(other.transferWaitlistEntryPriority)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TransferWaitlistPriorityConfigurationSearchType [transferWaitlistEntryPriority=" + transferWaitlistEntryPriority + ", priorityLevel=" + priorityLevel
                + "]";
    }

}
