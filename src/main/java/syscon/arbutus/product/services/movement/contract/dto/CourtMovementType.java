package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Court Movement DTO - This is a court movement unrelated to a case.
 *
 * @author wmadruga
 * @version 1.0
 * @since September 03, 2013.
 */

public class CourtMovementType extends TwoMovementsType implements Serializable {

    private static final long serialVersionUID = -8032769133453141608L;

    private Long courtPartRoom;

    /**
     * Empty Constructor
     */
    public CourtMovementType() {

    }

    /**
     * Constructor
     *
     * @param groupId           - the grouping id, used to bind two external movements together - optional, null for creation, it must be provided for update.
     * @param supervisionId     - the supervision id, required.
     * @param applicationDate   - the date the scheduled movement was requested, required.
     * @param movementIn        - the IN direction external movement id - optional, null for creation, it must be provided for update.
     * @param movementOut       - the OUT direction external movement id - optional, null for creation, it must be provided for update.
     * @param fromFacilityId    - the facility id where the offender belongs, required.
     * @param toFacilityId      - the facility id where the offender is going to, required - child object should take care of validation - destination will not be always a facility.
     * @param moveDate          - the movement OUT date, required.
     * @param returnDate        - the movement IN date, required.
     * @param movementType      - the movement type, required.
     * @param movementReason    - the movement reason, required.
     * @param movementInStatus  - the movement status, required.
     * @param movementOutStatus - the movement status, required.
     * @param comments          - commentaries related to request, optional.
     * @param offenderId        - offender id to be shown in the grid, optional.
     * @param offenderName      - offender name to be shown in the grid, optional.
     * @param movementOutCome   - the movement outcome, optional.
     * @param courtPartRoom     - Indicates the facility internal location where the hearing will take place, optional.
     */
    public CourtMovementType(String groupId, Long supervisionId, Date applicationDate, Long movementIn, Long movementOut, Long fromFacilityId, Long toFacilityId,
            Date moveDate, Date returnDate, String movementType, String movementReason, String movementInStatus, String movementOutStatus, String comments,
            Long offenderId, String offenderName, String movementOutCome, Long courtPartRoom) {

        super.setGroupId(groupId);
        super.setSupervisionId(supervisionId);
        super.setMovementIn(movementIn);
        super.setMovementOut(movementOut);
        super.setFromFacilityId(fromFacilityId);
        super.setToFacilityId(toFacilityId);
        super.setMoveDate(moveDate);
        super.setReturnDate(returnDate);
        super.setMovementType(movementType);
        super.setMovementReason(movementReason);
        super.setMovementInStatus(movementInStatus);
        super.setMovementOutStatus(movementOutStatus);
        super.setComments(comments);
        super.setOffenderId(offenderId);
        super.setOffenderName(offenderName);
        super.setMovementOutCome(movementOutCome);
        this.courtPartRoom = courtPartRoom;
    }

    public CourtMovementType(String groupId, Long supervisionId, Date applicationDate, Long movementIn, Long movementOut, Long fromFacilityId, Long toFacilityId,
            Date moveDate, Date returnDate, String movementType, String movementReason, String movementInStatus, String movementOutStatus, String movementCategory,
            String movementOutCome, Long courtPartRoom) {

        super.setGroupId(groupId);
        super.setSupervisionId(supervisionId);
        super.setMovementIn(movementIn);
        super.setMovementOut(movementOut);
        super.setFromFacilityId(fromFacilityId);
        super.setToFacilityId(toFacilityId);
        super.setMoveDate(moveDate);
        super.setReturnDate(returnDate);
        super.setMovementType(movementType);
        super.setMovementReason(movementReason);
        super.setMovementInStatus(movementInStatus);
        super.setMovementOutStatus(movementOutStatus);
        super.setMovementOutCome(movementOutCome);
        super.setMovementCategory(movementCategory);
        this.courtPartRoom = courtPartRoom;
    }

    /**
     * Gets the value of the courtPartRoom property.
     * Indicates the facility internal location where the hearing will take place, optional.
     *
     * @return the courtPartRoom
     */
    public Long getCourtPartRoom() {
        return courtPartRoom;
    }

    /**
     * Sets the value of the courtPartRoom property.
     * Indicates the facility internal location where the hearing will take place, optional.
     *
     * @param courtPartRoom Long
     */
    public void setCourtPartRoom(Long courtPartRoom) {
        this.courtPartRoom = courtPartRoom;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((courtPartRoom == null) ? 0 : courtPartRoom.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CourtMovementType other = (CourtMovementType) obj;
        if (courtPartRoom == null) {
            if (other.courtPartRoom != null) {
                return false;
            }
        } else if (!courtPartRoom.equals(other.courtPartRoom)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CourtMovementType [courtPartRoom=" + courtPartRoom + ", getMoveDate()=" + getMoveDate() + ", getReturnDate()=" + getReturnDate()
                + ", getFromFacilityId()=" + getFromFacilityId() + ", getMovementReason()=" + getMovementReason() + ", getMovementInStatus()=" + getMovementInStatus()
                + ", getMovementOutStatus()=" + getMovementOutStatus() + ", getMovementType()=" + getMovementType() + ", getSupervisionId()=" + getSupervisionId()
                + ", getComments()=" + getComments() + ", getToFacilityId()=" + getToFacilityId() + ", getGroupId()=" + getGroupId() + ", getMovementIn()="
                + getMovementIn() + ", getMovementOut()=" + getMovementOut() + ", getOffenderId()=" + getOffenderId() + ", getOffenderName()=" + getOffenderName()
                + ", getMovementOutCome()=" + getMovementOutCome() + ", isBypassConflicts()=" + isBypassConflicts() + "]";
    }

}