package syscon.arbutus.product.services.movement.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Schedule Temporary Absence DTO
 *
 * @author wmadruga
 * @version 1.0 (based on Individual Temporary Absence TDD)
 * @since July 16, 2013.
 */
public class TemporaryAbsenceType extends TwoMovementsType implements Serializable, ApprovableMovement {

    private static final long serialVersionUID = 7502297720300764539L;

    @NotNull
    private Date applicationDate;

    private Long escortOrganizationId;

    @NotNull
    private String transportation; //ReferenceSet(TA Transport)

    private String escortDetails;
    private Long toOrganizationId;
    private Long toFacilityLocationId;
    private Long toOrganizationLocationId;
    private Long toOffenderLocationId;

    /**
     * Empty Constructor
     */
    public TemporaryAbsenceType() {

    }
    
    /**
     * Constructor
     *
     * @param groupId              - the grouping id, used to bind two external movements together - optional, null for creation, it must be provided for update.
     * @param supervisionId        - the supervision id, required.
     * @param applicationDate      - the date the scheduled movement was requested, required.
     * @param movementIn           - the IN direction external movement id - optional, null for creation, it must be provided for update.
     * @param movementOut          - the OUT direction external movement id - optional, null for creation, it must be provided for update.
     * @param fromFacilityId       - the facility id where the offender belongs, required.
     * @param toFacilityId         - the facility id where the offender is going to, required - child object should take care of validation - destination will not be always a facility.
     * @param moveDate             - the movement OUT date, required.
     * @param returnDate           - the movement IN date, required.
     * @param movementType         - the movement type, required.
     * @param movementReason       - the movement reason, required.
     * @param movementInStatus     - the movement IN status, required.
     * @param movementOutStatus    - the movement OUT status, required.
     * @param comments             - commentaries related to request, optional.
     * @param offenderId           - offender id to be shown in the grid, optional.
     * @param offenderName         - offender name to be shown in the grid, optional.
     * @param movementOutCome      - the movement outcome, optional.
     * @param approverId           - the person id of the approver, optional, it must be provided if it is being approved/rejected.
     * @param approvalDate         - date of the approval/rejection, optional, it must be provided if it is being approved/rejected.
     * @param approvalComments     - commentaries related to the approval/reject, optional, it must be provided if it is being approved/rejected.
     * @param toFacilityLocationId - address location (facility) for the destination, required if OffenderLocationId is not set.
     * @param toOffenderLocationId - address location (person) for the destination, required if toFacilityLocationId is not set.
     * @param transportation       - ways of transportation, required.
     * @param escortOrganizationId - organization id responsible for the escort, required.
     * @param escortDetails        - escort related details, optional.
     */
    public TemporaryAbsenceType(String groupId, Long supervisionId, Date applicationDate, Long movementIn, Long movementOut, Long fromFacilityId, Long toFacilityId,
            Date moveDate, Date returnDate, String movementType, String movementReason, String movementInStatus, String movementOutStatus, String comments,
            Long offenderId, String offenderName, String movementOutCome, Long approverId, Date approvalDate, String approvalComments, Long toFacilityLocationId,
            Long toOffenderLocationId, String transportation, Long escortOrganizationId, String escortDetails) {

        super.setGroupId(groupId);
        super.setSupervisionId(supervisionId);
        super.setMovementIn(movementIn);
        super.setMovementOut(movementOut);
        super.setFromFacilityId(fromFacilityId);
        super.setToFacilityId(toFacilityId);
        super.setMoveDate(moveDate);
        super.setReturnDate(returnDate);
        super.setMovementType(movementType);
        super.setMovementReason(movementReason);
        super.setMovementInStatus(movementInStatus);
        super.setMovementOutStatus(movementOutStatus);
        super.setComments(comments);
        super.setOffenderId(offenderId);
        super.setOffenderName(offenderName);
        super.setMovementOutCome(movementOutCome);

        this.approvalComments = approvalComments;
        this.applicationDate = approvalDate;
        this.approverId = approverId;
        this.applicationDate = applicationDate;
        this.toFacilityLocationId = toFacilityLocationId;
        this.toOffenderLocationId = toOffenderLocationId;
        this.transportation = transportation;
        this.escortOrganizationId = escortOrganizationId;
        this.escortDetails = escortDetails;
    }

    public TemporaryAbsenceType(String groupId, Long supervisionId, Date applicationDate, Long movementIn, Long movementOut, Long fromFacilityId, Long toFacilityId,
            Date moveDate, Date returnDate, String movementType, String movementReason, String movementInStatus, String movementOutStatus, String movementCategory,
            String movementOutCome, Long approverId, Date approvalDate, String approvalComments, String transportation, Long escortOrganizationId, String escortDetails) {

        super.setGroupId(groupId);
        super.setSupervisionId(supervisionId);
        super.setMovementIn(movementIn);
        super.setMovementOut(movementOut);
        super.setFromFacilityId(fromFacilityId);
        super.setToFacilityId(toFacilityId);
        super.setMoveDate(moveDate);
        super.setReturnDate(returnDate);
        super.setMovementType(movementType);
        super.setMovementReason(movementReason);
        super.setMovementInStatus(movementInStatus);
        super.setMovementOutStatus(movementOutStatus);
        super.setMovementOutCome(movementOutCome);
        super.setMovementCategory(movementCategory);

        this.approvalComments = approvalComments;
        this.approvalDate = approvalDate;
        this.approverId = approverId;
        this.applicationDate = applicationDate;
        this.transportation = transportation;
        this.escortOrganizationId = escortOrganizationId;
        this.escortDetails = escortDetails;
    }

    /**
     * Constructor
     *
     * @param groupId              - the grouping id, used to bind two external movements together - optional, null for creation, it must be provided for update.
     * @param supervisionId        - the supervision id, required.
     * @param applicationDate      - the date the scheduled movement was requested, required.
     * @param movementIn           - the IN direction external movement id - optional, null for creation, it must be provided for update.
     * @param movementOut          - the OUT direction external movement id - optional, null for creation, it must be provided for update.
     * @param fromFacilityId       - the facility id where the offender belongs, required.
     * @param toFacilityId         - the facility id where the offender is going to, required - child object should take care of validation - destination will not be always a facility.
     * @param moveDate             - the movement OUT date, required.
     * @param returnDate           - the movement IN date, required.
     * @param movementType         - the movement type, required.
     * @param movementReason       - the movement reason, required.
     * @param movementInStatus     - the movement IN status, required.
     * @param movementOutStatus    - the movement OUT status, required.
     * @param comments             - commentaries related to request, optional.
     * @param offenderId           - offender id to be shown in the grid, optional.
     * @param offenderName         - offender name to be shown in the grid, optional.
     * @param movementOutCome      - the movement outcome, optional.
     * @param approverId           - the person id of the approver, optional, it must be provided if it is being approved/rejected.
     * @param approvalDate         - date of the approval/rejection, optional, it must be provided if it is being approved/rejected.
     * @param approvalComments     - commentaries related to the approval/reject, optional, it must be provided if it is being approved/rejected.
     * @param toFacilityLocationId - address location (facility) for the destination, required if OffenderLocationId is not set.
     * @param toOffenderLocationId - address location (person) for the destination, required if toFacilityLocationId is not set.
     * @param transportation       - ways of transportation, required.
     * @param escortOrganizationId - organization id responsible for the escort, required.
     * @param escortDetails        - escort related details, optional.
     */
    public TemporaryAbsenceType(String groupId, Long supervisionId, Date applicationDate, Long movementIn, Long movementOut, Long fromFacilityId, Long toFacilityId, Long toOrganizationId,
            Date moveDate, Date returnDate, String movementType, String movementReason, String movementInStatus, String movementOutStatus, String comments,
            Long offenderId, String offenderName, String movementOutCome, Long approverId, Date approvalDate, 
            String approvalComments, Long toFacilityLocationId, Long toOrganizationLocationId,
            Long toOffenderLocationId, String transportation, Long escortOrganizationId, String escortDetails) {

        super.setGroupId(groupId);
        super.setSupervisionId(supervisionId);
        super.setMovementIn(movementIn);
        super.setMovementOut(movementOut);
        super.setFromFacilityId(fromFacilityId);
        super.setToFacilityId(toFacilityId);
        super.setMoveDate(moveDate);
        super.setReturnDate(returnDate);
        super.setMovementType(movementType);
        super.setMovementReason(movementReason);
        super.setMovementInStatus(movementInStatus);
        super.setMovementOutStatus(movementOutStatus);
        super.setComments(comments);
        super.setOffenderId(offenderId);
        super.setOffenderName(offenderName);
        super.setMovementOutCome(movementOutCome);

        this.approvalComments = approvalComments;
        this.applicationDate = approvalDate;
        this.approverId = approverId;
        this.applicationDate = applicationDate;
        this.toOrganizationId = toOrganizationId;
        this.toFacilityLocationId = toFacilityLocationId;
        this.toOrganizationLocationId = toOrganizationLocationId;
        this.toOffenderLocationId = toOffenderLocationId;
        this.transportation = transportation;
        this.escortOrganizationId = escortOrganizationId;
        this.escortDetails = escortDetails;
    }

    public TemporaryAbsenceType(String groupId, Long supervisionId, Date applicationDate, Long movementIn, Long movementOut, Long fromFacilityId, Long toFacilityId, Long toOrganizationId,
            Date moveDate, Date returnDate, String movementType, String movementReason, String movementInStatus, String movementOutStatus, String movementCategory,
            String movementOutCome, Long approverId, Date approvalDate, String approvalComments, String transportation, Long escortOrganizationId, String escortDetails) {

        super.setGroupId(groupId);
        super.setSupervisionId(supervisionId);
        super.setMovementIn(movementIn);
        super.setMovementOut(movementOut);
        super.setFromFacilityId(fromFacilityId);
        super.setToFacilityId(toFacilityId);
        super.setMoveDate(moveDate);
        super.setReturnDate(returnDate);
        super.setMovementType(movementType);
        super.setMovementReason(movementReason);
        super.setMovementInStatus(movementInStatus);
        super.setMovementOutStatus(movementOutStatus);
        super.setMovementOutCome(movementOutCome);
        super.setMovementCategory(movementCategory);

        this.approvalComments = approvalComments;
        this.approvalDate = approvalDate;
        this.approverId = approverId;
        this.applicationDate = applicationDate;
        this.transportation = transportation;
        this.escortOrganizationId = escortOrganizationId;
        this.escortDetails = escortDetails;
        this.toOrganizationId = toOrganizationId;
    }

    /**
     * Gets the value of the applicationDate property. the date the scheduled movement was requested, required.
     *
     * @return the applicationDate
     */
    public Date getApplicationDate() {
        return applicationDate;
    }

    /**
     * Sets the value of the applicationDate property. the date the scheduled movement was requested, required.
     *
     * @param applicationDate Date
     */
    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    /**
     * Gets the value of the escortOrganizationId property. organization id responsible for the escort, required.
     *
     * @return the escortOrganizationId
     */
    public Long getEscortOrganizationId() {
        return escortOrganizationId;
    }

    /**
     * Sets the value of the escortOrganizationId property. organization id responsible for the escort, required.
     *
     * @param escortOrganizationId Long
     */
    public void setEscortOrganizationId(Long escortOrganizationId) {
        this.escortOrganizationId = escortOrganizationId;
    }

    /**
     * Gets the value of the transportation property. ways of transportation, required.
     *
     * @return the transportation
     */
    public String getTransportation() {
        return transportation;
    }

    /**
     * Sets the value of the transportation property. ways of transportation, required.
     *
     * @param transportation String
     */
    public void setTransportation(String transportation) {
        this.transportation = transportation;
    }

    /**
     * Gets the value of the escortDetails property. escort related details, optional.
     *
     * @return the escortDetails
     */
    public String getEscortDetails() {
        return escortDetails;
    }

    /**
     * Sets the value of the escortDetails property. escort related details, optional.
     *
     * @param escortDetails String
     */
    public void setEscortDetails(String escortDetails) {
        this.escortDetails = escortDetails;
    }

    /**
     * Gets the value of the toFacilityLocationId property.
     * address location (facility) for the destination, required if OffenderLocationId is not set.
     *
     * @return the toFacilityLocationId
     */
    public Long getToFacilityLocationId() {
        return toFacilityLocationId;
    }

    /**
     * Sets the value of the toFacilityLocationId property.
     * address location (facility) for the destination, required if OffenderLocationId is not set.
     *
     * @param toFacilityLocationId Long
     */
    public void setToFacilityLocationId(Long toFacilityLocationId) {
        this.toFacilityLocationId = toFacilityLocationId;
    }
    
    /**
     * Gets the value of the toOrganizationLocationId property.
     * address location (organization) for the destination, required if OffenderLocationId is not set.
     *
     * @return the toOrganizationLocationId
     */
    public Long getToOrganizationLocationId() {
        return toOrganizationLocationId;
    }

    /**
     * Sets the value of the toOrganizationLocationId property.
     * address location (organization) for the destination, required if OffenderLocationId is not set.
     *
     * @param toOrganizationLocationId Long
     */
    public void setToOrganizationLocationId(Long toOrganizationLocationId) {
        this.toOrganizationLocationId = toOrganizationLocationId;
    }
    
    /**
     * Gets the value of the toOrganizationId property.
     * address location (organization) for the destination, required if OffenderLocationId is not set.
     *
     * @return the toOrganizationId
     */

    public Long getToOrganizationId() {
        return toOrganizationId;
    }

    /**
     * Sets the value of the toOrganizationId property.
     * address location (organization) for the destination, required if OffenderLocationId is not set.
     *
     * @param toOrganizationId Long
     */
    public void setToOrganizationId(Long toOrganizationId) {
        this.toOrganizationId = toOrganizationId;
    }

    /**
     * Gets the value of the toOffenderLocationId property.
     * address location (person) for the destination, required if toFacilityLocationId is not set.
     *
     * @return the toOffenderLocationId
     */
    public Long getToOffenderLocationId() {
        return toOffenderLocationId;
    }

    /**
     * Sets the value of the toOffenderLocationId property.
     * address location (person) for the destination, required if toFacilityLocationId is not set.
     *
     * @param toOffenderLocationId Long
     */
    public void setToOffenderLocationId(Long toOffenderLocationId) {
        this.toOffenderLocationId = toOffenderLocationId;
    }

    /**
     * Gets the value of the approvalComments property.
     * commentaries related to the approval/reject, optional, it must be provided if it is being approved/rejected.
     *
     * @return the approvalComments
     */
    public String getApprovalComments() {
        return approvalComments;
    }

    /**
     * Sets the value of the approvalComments property.
     * commentaries related to the approval/reject, optional, it must be provided if it is being approved/rejected.
     *
     * @param approvalComments String
     */
    public void setApprovalComments(String approvalComments) {
        this.approvalComments = approvalComments;
    }

    /**
     * Gets the value of the approverId property. the person id of the approver, optional, it must be provided if it is being approved/rejected.
     *
     * @return the approverId
     */
    public Long getApproverId() {
        return approverId;
    }

    /**
     * Sets the value of the approverId property. the person id of the approver, optional, it must be provided if it is being approved/rejected.
     *
     * @param approverId Long
     */
    public void setApproverId(Long approverId) {
        this.approverId = approverId;
    }

    /**
     * Gets the value of the approvalDate property. date of the approval/rejection, optional, it must be provided if it is being approved/rejected.
     *
     * @return the approvalDate
     */
    public Date getApprovalDate() {
        return approvalDate;
    }

    /**
     * Sets the value of the approvalDate property. date of the approval/rejection, optional, it must be provided if it is being approved/rejected.
     *
     * @param approvalDate Date
     */
    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((applicationDate == null) ? 0 : applicationDate.hashCode());
        result = prime * result + ((escortDetails == null) ? 0 : escortDetails.hashCode());
        result = prime * result + ((escortOrganizationId == null) ? 0 : escortOrganizationId.hashCode());
        result = prime * result + ((toFacilityLocationId == null) ? 0 : toFacilityLocationId.hashCode());
        result = prime * result + ((toOffenderLocationId == null) ? 0 : toOffenderLocationId.hashCode());
        result = prime * result + ((transportation == null) ? 0 : transportation.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TemporaryAbsenceType other = (TemporaryAbsenceType) obj;
        if (applicationDate == null) {
            if (other.applicationDate != null) {
                return false;
            }
        } else if (!applicationDate.equals(other.applicationDate)) {
            return false;
        }
        if (escortDetails == null) {
            if (other.escortDetails != null) {
                return false;
            }
        } else if (!escortDetails.equals(other.escortDetails)) {
            return false;
        }
        if (escortOrganizationId == null) {
            if (other.escortOrganizationId != null) {
                return false;
            }
        } else if (!escortOrganizationId.equals(other.escortOrganizationId)) {
            return false;
        }
        if (toFacilityLocationId == null) {
            if (other.toFacilityLocationId != null) {
                return false;
            }
        } else if (!toFacilityLocationId.equals(other.toFacilityLocationId)) {
            return false;
        }
        if (toOffenderLocationId == null) {
            if (other.toOffenderLocationId != null) {
                return false;
            }
        } else if (!toOffenderLocationId.equals(other.toOffenderLocationId)) {
            return false;
        }
        if (transportation == null) {
            if (other.transportation != null) {
                return false;
            }
        } else if (!transportation.equals(other.transportation)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TemporaryAbsenceType [applicationDate=" + applicationDate + ", escortOrganizationId=" + escortOrganizationId + ", transportation=" + transportation
                + ", escortDetails=" + escortDetails + ", toFacilityLocationId=" + toFacilityLocationId + ", toOffenderLocationId=" + toOffenderLocationId
                + ", getMoveDate()=" + getMoveDate() + ", getReturnDate()=" + getReturnDate() + ", getFromFacilityId()=" + getFromFacilityId() + ", getMovementReason()="
                + getMovementReason() + ", getMovementInStatus()=" + getMovementInStatus() + ", getMovementOutStatus()=" + getMovementOutStatus() + ", getMovementType()="
                + getMovementType() + ", getSupervisionId()=" + getSupervisionId() + ", getComments()=" + getComments() + ", getToFacilityId()=" + getToFacilityId()
                + ", getGroupId()=" + getGroupId() + ", getMovementIn()=" + getMovementIn() + ", getMovementOut()=" + getMovementOut() + ", getOffenderId()="
                + getOffenderId() + ", getOffenderName()=" + getOffenderName() + ", getMovementOutCome()=" + getMovementOutCome() + ", isBypassConflicts()="
                + isBypassConflicts() + "]";
    }

}