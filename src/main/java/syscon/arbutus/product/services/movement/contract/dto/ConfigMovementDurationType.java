/**
 * 
 */
package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;

import syscon.arbutus.product.services.core.contract.dto.BaseDto;

/**
 * @author rajiv.kumar
 *
 */
public class ConfigMovementDurationType extends BaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -239286603234920535L;
	private String movementType;
	private String duration;

	public ConfigMovementDurationType(String movementType, String duration) {
		super();
		this.movementType = movementType;
		this.duration = duration;
	}

	public ConfigMovementDurationType() {
		super();
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((duration == null) ? 0 : duration.hashCode());
		result = prime * result
				+ ((movementType == null) ? 0 : movementType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ConfigMovementDurationType other = (ConfigMovementDurationType) obj;
		if (duration == null) {
			if (other.duration != null) {
				return false;
			}
		} else if (!duration.equals(other.duration)) {
			return false;
		}
		if (movementType == null) {
			if (other.movementType != null) {
				return false;
			}
		} else if (!movementType.equals(other.movementType)) {
			return false;
		}
		return true;
	}

	public String getMovementType() {
		return movementType;
	}

	public void setMovementType(String movementType) {
		this.movementType = movementType;
	}

}
