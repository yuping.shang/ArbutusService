package syscon.arbutus.product.services.movement.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

import syscon.arbutus.product.services.core.contract.dto.BaseDto;

/**
 * CourtMovementReturnTimeConfigurationType for MovementActivity Service
 * The Configuration for the default return time for all court movements.
 *
 * @author yshang
 * @version 2.0
 * @since October 25, 2012
 */
public class CourtMovementReturnTimeConfigurationType extends BaseDto implements Serializable {

    private static final long serialVersionUID = 7941203488838104582L;

    /**
     * The default return time for all court movements
     */
    @NotNull
    private String returnTime;

    public CourtMovementReturnTimeConfigurationType() {
        super();
    }

    /**
     * Constructor
     *
     * @param returnTime String, required -- The default return time for all court movements
     */
    public CourtMovementReturnTimeConfigurationType(String returnTime) {
        super();
        this.returnTime = returnTime;
    }

    /**
     * The default return time for all court movements, required.
     *
     * @return the returnTime
     */
    public String getReturnTime() {
        return returnTime;
    }

    /**
     * The default return time for all court movements, required.
     *
     * @param returnTime the returnTime to set
     */
    public void setReturnTime(String returnTime) {
        this.returnTime = returnTime;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((returnTime == null) ? 0 : returnTime.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CourtMovementReturnTimeConfigurationType other = (CourtMovementReturnTimeConfigurationType) obj;
        if (returnTime == null) {
            if (other.returnTime != null) {
                return false;
            }
        } else if (!returnTime.equals(other.returnTime)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CourtMovementReturnTimeConfigurationType [returnTime=" + returnTime + "]";
    }

}
