package syscon.arbutus.product.services.movement.contract.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

import syscon.arbutus.product.services.activity.contract.dto.ActivityType;

/**
 * The representation of the MovementActivityDetails Type
 *
 * @author lhan
 */
public class MovementActivityDetailsType implements Serializable {
    private static final long serialVersionUID = -9173072308876370635L;

    /**
     * The supervision Id, required
     */
    @NotNull
    private Long supervisionIdentification;

    /**
     * The instance of ActivityType, required
     */
    @NotNull
    @Valid
    private ActivityType activityType;

    /**
     * The instance of MovementActivityType, required
     */
    @NotNull
    @Valid
    private MovementActivityType movementActivityType;

    /**
     * Default constructor
     */
    public MovementActivityDetailsType() {
    }

    /**
     * Constructor
     *
     * @param supervisionId        Long - The Supervision ID (from Supervision Type), required
     * @param activityType         ActivityType -  The instance of ActivityType, required
     * @param movementActivityType MovementActivityType - The instance of MovementActivityType. required
     */
    public MovementActivityDetailsType(Long supervisionId, ActivityType activityType, MovementActivityType movementActivityType) {
        this.supervisionIdentification = supervisionId;
        this.activityType = activityType;
        this.movementActivityType = movementActivityType;
    }

    /**
     * The Supervision ID (from Supervision Type), required
     *
     * @return the supervisionIdentification
     */
    public Long getSupervisionIdentification() {
        return supervisionIdentification;
    }

    /**
     * The Supervision ID (from Supervision Type), required
     *
     * @param supervisionIdentification the supervisionIdentification to set
     */
    public void setSupervisionIdentification(Long supervisionIdentification) {
        this.supervisionIdentification = supervisionIdentification;
    }

    /**
     * The instance of ActivityType, required
     *
     * @return the activityType
     */
    public ActivityType getActivityType() {
        return activityType;
    }

    /**
     * The instance of ActivityType, required
     *
     * @param activityType the activityType to set
     */
    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    /**
     * The instance of MovementActivityType. required
     *
     * @return the movementActivityType
     */
    public MovementActivityType getMovementActivityType() {
        return movementActivityType;
    }

    /**
     * The instance of MovementActivityType. required
     *
     * @param movementActivityType the movementActivityType to set
     */
    public void setMovementActivityType(MovementActivityType movementActivityType) {
        this.movementActivityType = movementActivityType;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "MovementActivityDetailsType [supervisionIdentification=" + supervisionIdentification + ", activityType=" + activityType + ", movementActivityType="
                + movementActivityType + "]";
    }

}
