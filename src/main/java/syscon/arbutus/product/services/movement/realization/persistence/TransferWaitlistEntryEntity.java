package syscon.arbutus.product.services.movement.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.core.services.realization.BaseEntity;
import syscon.arbutus.product.services.realization.model.MetaCode;

/**
 * TransferWaitlistEntryEntity for MovementActivity Service
 *
 * @author yshang
 * @version 2.0
 * @DbComment MA_TransWaitEntry 'Transfer Wait list Entry table for Movement Activity Service'
 * @DbComment .createUserId 'User ID who created the object'
 * @DbComment .createDateTime 'Date and time when the object was created'
 * @DbComment .modifyUserId 'User ID who last updated the object'
 * @DbComment .modifyDateTime 'Date and time when the object was last updated'
 * @DbComment .invocationContext 'Invocation context when the create/update action called'
 * @DbComment .version 'the version number of record updated'
 * @since October 25, 2012
 */
@IdClass(TransferWaitlistEntryPK.class)
@Audited
@Entity
@Table(name = "MA_TransWaitEntry")
public class TransferWaitlistEntryEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 7941203188838104583L;

    /**
     * @DbComment transFacilityId 'Transfer Wait list Entry ID'
     */
    @Id
    @Column(name = "transFacilityId")
    private Long transFacilityId;

    /**
     * @DbComment supervision 'The list of offenders who are awaiting transfers. Static association to the Supervision service'
     */
    @Id
    @Column(name = "supervision", nullable = true)
    private Long supervision;

    /**
     * @DbComment dateAdded 'Date an offender was added to the wait list'
     */
    @Column(name = "dateAdded", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateAdded;

    /**
     * @DbComment toFacility 'The facility that the offender is to be transferred to. Static association to the Facility service'
     */
    @Column(name = "toFacility", nullable = true)
    private Long toFacility;

    /**
     * @DbComment transferReason 'The reason for the transfer'
     */
    @Column(name = "transferReason", nullable = true)
    @MetaCode(set = MetaSet.MOVEMENT_REASON)
    private String transferReason;

    /**
     * @DbComment priority 'The priority of each offender on the wait list'
     */
    @Column(name = "priority", nullable = true)
    @MetaCode(set = MetaSet.TRANSFER_WAITLIST_PRIORITY)
    private String priority;

    /**
     * @return the transFacilityId
     */
    public Long getTransFacilityId() {
        return transFacilityId;
    }

    /**
     * @param transFacilityId the transFacilityId to set
     */
    public void setTransFacilityId(Long transFacilityId) {
        this.transFacilityId = transFacilityId;
    }

    /**
     * @return the supervision
     */
    public Long getSupervision() {
        return supervision;
    }

    /**
     * @param supervision the supervision to set
     */
    public void setSupervision(Long supervision) {
        this.supervision = supervision;
    }

    /**
     * @return the dateAdded
     */
    public Date getDateAdded() {
        return dateAdded;
    }

    /**
     * @param dateAdded the dateAdded to set
     */
    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    /**
     * @return the toFacility
     */
    public Long getToFacility() {
        return toFacility;
    }

    /**
     * @param toFacility the toFacility to set
     */
    public void setToFacility(Long toFacility) {
        this.toFacility = toFacility;
    }

    /**
     * @return the transferReason
     */
    public String getTransferReason() {
        return transferReason;
    }

    /**
     * @param transferReason the transferReason to set
     */
    public void setTransferReason(String transferReason) {
        this.transferReason = transferReason;
    }

    /**
     * @return the priority
     */
    public String getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(String priority) {
        this.priority = priority;
    }

    @Override
    public TransferWaitlistEntryPK getId() {
        return new TransferWaitlistEntryPK(transFacilityId, supervision);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((supervision == null) ? 0 : supervision.hashCode());
        result = prime * result + ((transFacilityId == null) ? 0 : transFacilityId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TransferWaitlistEntryEntity other = (TransferWaitlistEntryEntity) obj;
        if (supervision == null) {
            if (other.supervision != null) {
                return false;
            }
        } else if (!supervision.equals(other.supervision)) {
            return false;
        }
        if (transFacilityId == null) {
            if (other.transFacilityId != null) {
                return false;
            }
        } else if (!transFacilityId.equals(other.transFacilityId)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TransferWaitlistEntryEntity [transFacilityId=" + transFacilityId + ", supervision=" + supervision + ", dateAdded=" + dateAdded + ", toFacility="
                + toFacility + ", transferReason=" + transferReason + ", priority=" + priority + ", stamp=" + getStamp() + "]";
    }

}