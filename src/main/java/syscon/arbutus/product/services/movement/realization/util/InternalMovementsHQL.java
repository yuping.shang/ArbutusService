package syscon.arbutus.product.services.movement.realization.util;

import java.util.Date;

import org.hibernate.Query;
import org.hibernate.Session;
import syscon.arbutus.product.services.movement.contract.dto.TwoMovementsSearchType;
import syscon.arbutus.product.services.movement.contract.dto.TwoMovementsType;
import syscon.arbutus.product.services.movement.realization.util.interfaces.MovementsHQLStrategy;

/**
 * Movement HQL Strategy implementation
 * HQL query to get InternalMovements and return as a TwoMovementsType
 *
 * @author wmadruga
 */
public class InternalMovementsHQL implements MovementsHQLStrategy {

    public Query buildTwoMovementsHQL(Session session, TwoMovementsSearchType searchType, Date dateOut, Date dateIn) {

        StringBuilder hqlFormat = new StringBuilder();
        //ma1 = OUT		//ma2 = IN

        hqlFormat.append("SELECT new %s(ma1.groupid, ma1.supervisionId, ");
        hqlFormat.append("ma1.movementId, ma2.movementId, ");
        hqlFormat.append("ma1.frommInternalLocationId, ma1.toInternalLocationId, ");
        hqlFormat.append("act.plannedStartDate, act.plannedEndDate, ");

        hqlFormat.append("ma1.movementType, ma1.movementReason, ");

        hqlFormat.append("ma2.movementStatus, ma1.movementStatus, ma1.movementCategory ) ");

        hqlFormat.append("FROM InternalMovementActivityEntity AS ma1, InternalMovementActivityEntity AS ma2, ActivityEntity AS act ");

        hqlFormat.append("WHERE ma1.movementType = :moveType ");
        hqlFormat.append("AND ma1.groupid = ma2.groupid ");
        hqlFormat.append("AND ma1.movementId <> ma2.movementId ");
        hqlFormat.append("AND ma1.activityId = act.activityId ");
        hqlFormat.append("AND ma1.movementDirection = 'OUT' ");

        if (searchType.getFromFacilityId() != null) {
            hqlFormat.append("AND ma1.fromFacilityId = " + searchType.getFromFacilityId() + " ");
        }

        if (searchType.getToFacilityId() != null) {
            hqlFormat.append("AND ma1.toFacilityId = " + searchType.getToFacilityId() + " ");
        }

        if (searchType.getGroupId() != null) {
            hqlFormat.append("AND ma1.groupid = '" + searchType.getGroupId() + "' ");
        }

        if (searchType.getOffenderId() != null) {
            hqlFormat.append("AND ma1.supervisionId = " + searchType.getOffenderId() + " ");
        }

        if (dateOut != null && dateIn != null) {
            hqlFormat.append("AND ((act.plannedStartDate > :dateOut AND act.plannedStartDate < :dateIn) ");
            hqlFormat.append("OR (act.plannedEndDate > :dateOut AND act.plannedStartDate < :dateIn)) ");
        }

        if (searchType.getStatusOUT() != null && searchType.getStatusIN() != null) {
            hqlFormat.append("AND (ma1.movementStatus = '" + searchType.getStatusOUT() + "' ");
            hqlFormat.append("OR ma2.movementStatus = '" + searchType.getStatusIN() + "') ");
        } else {
            if (searchType.getStatusOUT() != null) {
                hqlFormat.append("AND ma1.movementStatus = '" + searchType.getStatusOUT() + "' ");
            }
            if (searchType.getStatusIN() != null) {
                hqlFormat.append("AND ma2.movementStatus = '" + searchType.getStatusIN() + "' ");
            }
        }

        if (searchType.getFromFacilityId() != null) {
            hqlFormat.append("AND ma1.fromFacilityId = " + searchType.getFromFacilityId() + " ");
        }

        hqlFormat.append("ORDER BY ma1.groupid ");

        String hql = null;
        hql = String.format(hqlFormat.toString(), TwoMovementsType.class.getName());

        Query query = session.createQuery(hql);
        query.setParameter("moveType", searchType.getMovementType());

        if (dateOut != null && dateIn != null) {
            query.setTimestamp("dateOut", dateOut).setTimestamp("dateIn", dateIn);
        }

        return query;
    }

}