package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Definition of an Schedule Transfer Movement search
 *
 * @author bkahlon
 * @version 1.0
 * @since January 24, 2012
 */
public class ScheduleTransferMovementSearch implements Serializable {

    private static final long serialVersionUID = -3981555676749703890L;

    private Long fromFacility;
    private Long toFacility;
    private Date fromScheduledDate;
    private Date toScheduledDate;
    private String movementStatus;
    private String movementType;
    private String movementDirection;

    private String fromCity;
    private Long fromFacilityInternalLocation;
    private Long fromOrganization;
    private Long fromLocation;

    private String toCity;
    private Long toFacilityInternalLocation;
    private Long toOrganization;
    private Long toLocation;
    private boolean filterInOut;

    /**
     *
     */
    public ScheduleTransferMovementSearch() {

    }

    /**
     * @param fromLocation,      optional
     * @param toLocation,        optional
     * @param fromScheduledDate, optional
     * @param toScheduledDate,   optional
     * @param movementStatus,    optional
     * @param movementType,      optional
     */
    public ScheduleTransferMovementSearch(Long fromFacility, Long toFacility, Date fromScheduledDate, Date toScheduledDate, String movementStatus, String movementType) {
        this.fromFacility = fromFacility;
        this.toFacility = toFacility;
        this.fromScheduledDate = fromScheduledDate;
        this.toScheduledDate = toScheduledDate;
        this.movementStatus = movementStatus;
        this.movementType = movementType;
    }

    /**
     * Gets the value of the fromLocation property.
     */
    public Long getFromFacility() {
        return fromFacility;
    }

    /**
     * Sets the value of the fromLocation property.
     */
    public void setFromFacility(Long fromFacility) {
        this.fromFacility = fromFacility;
    }

    /**
     * Gets the value of the toLocation property.
     */
    public Long getToFacility() {
        return toFacility;
    }

    /**
     * Sets the value of the toFacility property.
     */
    public void setToFacility(Long toFacility) {
        this.toFacility = toFacility;
    }

    /**
     * Gets the value of the fromScheduledDate property.
     */
    public Date getFromScheduledDate() {
        return fromScheduledDate;
    }

    /**
     * Sets the value of the fromScheduledDate property.
     */
    public void setFromScheduledDate(Date fromScheduledDate) {
        this.fromScheduledDate = fromScheduledDate;
    }

    /**
     * Gets the value of the toScheduledDate property.
     */
    public Date getToScheduledDate() {
        return toScheduledDate;
    }

    /**
     * Sets the value of the toScheduledDate property.
     */
    public void setToScheduledDate(Date toScheduledDate) {
        this.toScheduledDate = toScheduledDate;
    }

    /**
     * Gets the value of the movementStatus property.
     */
    public String getMovementStatus() {
        return movementStatus;
    }

    /**
     * Sets the value of the movementStatus property.
     */
    public void setMovementStatus(String movementStatus) {
        this.movementStatus = movementStatus;
    }

    /**
     * Gets the value of the movementType property.
     */
    public String getMovementType() {
        return movementType;
    }

    /**
     * Sets the value of the movementType property.
     */
    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    /**
     * Gets the value of the fromCity property.
     */
    public String getFromCity() {
        return fromCity;
    }

    /**
     * Sets the value of the fromCity property.
     */
    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    /**
     * Gets the value of the fromFacilityInternalLocation property.
     */
    public Long getFromFacilityInternalLocation() {
        return fromFacilityInternalLocation;
    }

    /**
     * Sets the value of the fromFacilityInternalLocation property.
     */
    public void setFromFacilityInternalLocation(Long fromFacilityInternalLocation) {
        this.fromFacilityInternalLocation = fromFacilityInternalLocation;
    }

    /**
     * Gets the value of the fromOrganization property.
     */
    public Long getFromOrganization() {
        return fromOrganization;
    }

    /**
     * Sets the value of the fromOrganization property.
     */
    public void setFromOrganization(Long fromOrganization) {
        this.fromOrganization = fromOrganization;
    }

    /**
     * Gets the value of the fromLocation property.
     */
    public Long getFromLocation() {
        return fromLocation;
    }

    /**
     * Sets the value of the fromLocation property.
     */
    public void setFromLocation(Long fromLocation) {
        this.fromLocation = fromLocation;
    }

    /**
     * Gets the value of the toCity property.
     */
    public String getToCity() {
        return toCity;
    }

    /**
     * Sets the value of the toCity property.
     */
    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    /**
     * Gets the value of the toFacilityInternalLocation property.
     */
    public Long getToFacilityInternalLocation() {
        return toFacilityInternalLocation;
    }

    /**
     * Sets the value of the toFacilityInternalLocation property.
     */
    public void setToFacilityInternalLocation(Long toFacilityInternalLocation) {
        this.toFacilityInternalLocation = toFacilityInternalLocation;
    }

    /**
     * Gets the value of the toOrganization property.
     */
    public Long getToOrganization() {
        return toOrganization;
    }

    /**
     * Sets the value of the toOrganization property.
     */
    public void setToOrganization(Long toOrganization) {
        this.toOrganization = toOrganization;
    }

    /**
     * Gets the value of the toLocation property.
     */
    public Long getToLocation() {
        return toLocation;
    }

    /**
     * Sets the value of the toLocation property.
     */
    public void setToLocation(Long toFacility) {
        this.toFacility = toFacility;
    }
    
    /**
     * @return filterInOut
     */
    public boolean isFilterInOut() {
        return filterInOut;
    }

    /**
     * @param filterInOut
     */
    public void setFilterInOut(boolean filterInOut) {
        this.filterInOut = filterInOut;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ScheduleTransferMovementSearch [fromFacility=" + fromFacility + ", toFacility=" + toFacility + ", fromScheduledDate=" + fromScheduledDate
                + ", toScheduledDate=" + toScheduledDate + ", movementStatus=" + movementStatus + ", movementType=" + movementType + ", fromCity=" + fromCity
                + ", fromFacilityInternalLocation=" + fromFacilityInternalLocation + ", fromOrganization=" + fromOrganization + ", fromLocation=" + fromLocation
                + ", toCity=" + toCity + ", toFacilityInternalLocation=" + toFacilityInternalLocation + ", toOrganization=" + toOrganization + ", toLocation="
                + toLocation + "]";
    }

    public String getMovementDirection() {
        return movementDirection;
    }

    public void setMovementDirection(String movementDirection) {
        this.movementDirection = movementDirection;
    }

}
