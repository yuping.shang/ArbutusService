package syscon.arbutus.product.services.movement.contract.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import syscon.arbutus.product.services.core.common.StampInterface;
import syscon.arbutus.product.services.core.contract.dto.BaseDto;

/**
 * SupervisionClosureConfigurationType for MovementActivity Service
 *
 * @author yshang
 * @version 2.0
 * @since October 25, 2012
 */
public class SupervisionClosureConfigurationType extends BaseDto implements Serializable {

    private static final long serialVersionUID = 7941203488838104584L;

    /**
     * The type of movement (APP, VISIT etc.,)
     */
    @NotNull
    private String movementType;

    /**
     * The reason for the Movement
     */
    @NotNull
    private String movementReason;

    /**
     * If set to true then the offender’s supervision period
     * must be closed when a movement is completed with the movement type
     * and reason listed in this configuration object
     */
    @NotNull
    private Boolean isCloseSupervisionFlag;

    private Boolean isCleanSchedulesFlag;

    private Boolean isCloseLegalFlag;
    private Boolean isReleaseBedFlag;

    /**
     * The MovementReason which is linked to MovementType:ESCP, REL, TRNIJ, TRNOJ. -- Optional
     */
    @Size(max = 64)
    private String defaultAdmissionReason;

    public SupervisionClosureConfigurationType() {
        super();
    }

    /**
     * Constructor
     *
     * @param movementType           String, required -- The type of movement (APP, VISIT etc.,)
     * @param movementReason         String, required -- The reason for the Movement
     * @param isCloseSupervisionFlag Boolean, required -- If set to true then the offender’s supervision period
     *                               must be closed when a movement is completed with the movement type
     *                               and reason listed in this configuration object
     */
    public SupervisionClosureConfigurationType(@NotNull String movementType, @NotNull String movementReason, @NotNull Boolean isCloseSupervisionFlag) {
        super();
        this.movementType = movementType;
        this.movementReason = movementReason;
        this.isCloseSupervisionFlag = isCloseSupervisionFlag;
    }

    public SupervisionClosureConfigurationType(String movementType, String movementReason, Boolean isCloseSupervisionFlag, Boolean isCleanSchedulesFlag,
            Boolean isCloseLegalFlag, Boolean isReleaseBedFlag, String defaultAdmissionReason, StampInterface stamp, Long version) {
        this.movementType = movementType;
        this.movementReason = movementReason;
        this.isCloseSupervisionFlag = isCloseSupervisionFlag;
        this.isCleanSchedulesFlag = isCleanSchedulesFlag;
        this.isCloseLegalFlag = isCloseLegalFlag;
        this.isReleaseBedFlag = isReleaseBedFlag;
        this.defaultAdmissionReason = defaultAdmissionReason;
        this.setStamp(stamp);
        this.setVersion(version);
    }

    public Boolean getIsReleaseBedFlag() {
        return isReleaseBedFlag;
    }

    public void setIsReleaseBedFlag(Boolean isReleaseBedFlag) {
        this.isReleaseBedFlag = isReleaseBedFlag;
    }

    public Boolean getIsCleanSchedulesFlag() {
        return isCleanSchedulesFlag;
    }

    public void setIsCleanSchedulesFlag(Boolean isCleanSchedulesFlag) {
        this.isCleanSchedulesFlag = isCleanSchedulesFlag;
    }

    public Boolean getIsCloseLegalFlag() {
        return isCloseLegalFlag;
    }

    public void setIsCloseLegalFlag(Boolean isCloseLegalFlag) {
        this.isCloseLegalFlag = isCloseLegalFlag;
    }

    /**
     * The type of movement (APP, VISIT etc.,), required
     *
     * @return the movementType
     */
    public String getMovementType() {
        return movementType;
    }

    /**
     * The type of movement (APP, VISIT etc.,), required
     *
     * @param movementType the movementType to set
     */
    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    /**
     * The reason for the Movement, required
     *
     * @return the movementReason
     */
    public String getMovementReason() {
        return movementReason;
    }

    /**
     * The reason for the Movement, required
     *
     * @param movementReason the movementReason to set
     */
    public void setMovementReason(String movementReason) {
        this.movementReason = movementReason;
    }

    /**
     * If set to true then the offender’s supervision period
     * must be closed when a movement is completed with the movement type
     * and reason listed in this configuration object
     * <p>required
     *
     * @return the isCloseSupervisionFlag
     */
    public Boolean getIsCloseSupervisionFlag() {
        return isCloseSupervisionFlag;
    }

    /**
     * If set to true then the offender’s supervision period
     * must be closed when a movement is completed with the movement type
     * and reason listed in this configuration object
     * <p>required
     *
     * @param isCloseSupervisionFlag the isCloseSupervisionFlag to set
     */
    public void setIsCloseSupervisionFlag(Boolean isCloseSupervisionFlag) {
        this.isCloseSupervisionFlag = isCloseSupervisionFlag;
    }

    public String getDefaultAdmissionReason() {
        return defaultAdmissionReason;
    }

    public void setDefaultAdmissionReason(String defaultAdmissionReason) {
        this.defaultAdmissionReason = defaultAdmissionReason;
    }

    /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((movementReason == null) ? 0 : movementReason.hashCode());
        result = prime * result + ((movementType == null) ? 0 : movementType.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SupervisionClosureConfigurationType other = (SupervisionClosureConfigurationType) obj;
        if (movementReason == null) {
            if (other.movementReason != null) {
                return false;
            }
        } else if (!movementReason.equals(other.movementReason)) {
            return false;
        }
        if (movementType == null) {
            if (other.movementType != null) {
                return false;
            }
        } else if (!movementType.equals(other.movementType)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SupervisionClosureConfigurationType [movementType=" + movementType + ", movementReason=" + movementReason + ", isCloseSupervisionFlag="
                + isCloseSupervisionFlag + "]";
    }

}
