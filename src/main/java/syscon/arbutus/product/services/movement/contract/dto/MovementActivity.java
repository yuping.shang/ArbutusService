package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.core.contract.dto.BaseDto;

/**
 * Created by jliang on 12/11/15.
 */
public class MovementActivity extends BaseDto implements Serializable {

    private Long supervisionid;
    private String firstName;
    private String lastName;
    private String offenderId;
    private String currentLocation;
    private String movmentCategory;
    private String movementType;
    private String movementReason;
    private String movementDirection;
    private Date movementDate;
    private Date movementTime;

    private Long toFacilityId;
    private Long toInternalLocationId;
    private String comments;
    private Date scheduledReturnDate;
    private Date scheduledReturnTime;
    private Long escortOrgId;
    private String escortDetails;
    private Date scheduledArrivalDate;
    private Date scheduledArrivalTime;
    private Date applicationDate;
    private String transportation;
    private String destinationType;
    private Long destinationId;
    private Long destinationLocationId;
    private Date completeDate;
    private Date completeTime;

}
