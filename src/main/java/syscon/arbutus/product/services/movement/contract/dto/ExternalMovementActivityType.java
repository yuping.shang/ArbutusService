package syscon.arbutus.product.services.movement.contract.dto;

import java.util.Date;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * External Movement Activity Type
 *
 * @author yshang
 * @version 1.0 (based on Movement Activity 1.0 SDD)
 * @since February 10, 2012.
 */
public class ExternalMovementActivityType extends MovementActivityType {

    private static final long serialVersionUID = -2183572011286140609L;

    /**
     * The location (address) the offender is coming from, not required
     */
    private Long fromLocationId;

    /**
     * The organization the offender is coming from, not required
     */
    private Long fromOrganizationId;

    /**
     * The Facility the offender is coming from. Will be used to reference the
     * Facility Service, not required
     */
    private Long fromFacilityId;

    /**
     * City the offender is from, not required
     */
    private String fromCity;

    /**
     * The internal location the offender is going to (only relevant to external
     * movements coming into the facility)
     */
    private Long toFacilityInternalLocationId;

    private Long fromFacilityInternalLocationId;

    /**
     * The location (address) the offender is going to, not required
     */
    private Long toLocationId;

    /**
     * The organization the offender is going to, not required
     */
    private Long toOrganizationId;

    /**
     * The Facility the offender is going to. Will be used to reference the
     * Facility Service, not required
     */
    private Long toFacilityId;

    /**
     * City the offender is going to, not required
     */
    private String toCity;

    /**
     * State or Province the offender is going to, not required
     */
    private String toProvinceState;

    /**
     * Identifier for the arresting agency noted on the admission form, not
     * required
     */
    private Long arrestOrganizationId;

    /**
     * The date of the application, not required
     */
    private Date applicationDate;

    /**
     * Organization escorting offender, not required
     */
    private Long escortOrganizationId;

    /**
     * Escort Details
     */
    private String escortDetails;

    /**
     * Estimated Date of reporting at the receiving facility (Transfer), not
     * required
     */
    private Date reportingDate;

    /**
     * The transportation, not required.
     */
    private String transportation;


    /**
     * Constructor
     */
    public ExternalMovementActivityType() {
        super();
    }

    public Long getFromFacilityInternalLocationId() {
        return fromFacilityInternalLocationId;
    }

    public void setFromFacilityInternalLocationId(Long fromFacilityInternalLocationId) {
        this.fromFacilityInternalLocationId = fromFacilityInternalLocationId;
    }

    /**
     * The location (address) the offender is coming from, optional
     *
     * @return the fromLocationId
     */
    public Long getFromLocationId() {
        return fromLocationId;
    }

    /**
     * The location (address) the offender is coming from, optional
     *
     * @param fromLocationId the fromLocationId to set
     */
    public void setFromLocationId(Long fromLocationId) {
        this.fromLocationId = fromLocationId;
    }

    /**
     * The organization the offender is coming from, optional
     *
     * @return the fromOrganizationId
     */
    public Long getFromOrganizationId() {
        return fromOrganizationId;
    }

    /**
     * The organization the offender is coming from, optional
     *
     * @param fromOrganizationId the fromOrganizationId to set
     */
    public void setFromOrganizationId(Long fromOrganizationId) {
        this.fromOrganizationId = fromOrganizationId;
    }

    /**
     * The Facility the offender is coming from. refer to the Facility Service, optional
     *
     * @return the fromFacilityId
     */
    public Long getFromFacilityId() {
        return fromFacilityId;
    }

    /**
     * The Facility the offender is coming from. refer to the Facility Service, optional
     *
     * @param fromFacilityId the fromFacilityId to set
     */
    public void setFromFacilityId(Long fromFacilityId) {
        this.fromFacilityId = fromFacilityId;
    }

    /**
     * City the offender is from, optional
     *
     * @return the fromCity
     */
    public String getFromCity() {
        return fromCity;
    }

    /**
     * City the offender is from, optional
     *
     * @param fromCity the fromCity to set
     */
    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    /**
     * The internal location the offender is going to (only relevant to external
     * movements coming into the facility), optional
     *
     * @return the toFacilityInternalLocationId
     */
    public Long getToFacilityInternalLocationId() {
        return toFacilityInternalLocationId;
    }

    /**
     * The internal location the offender is going to (only relevant to external
     * movements coming into the facility), optional
     *
     * @param toFacilityInternalLocationId the toFacilityInternalLocationId to set
     */
    public void setToFacilityInternalLocationId(Long toFacilityInternalLocationId) {
        this.toFacilityInternalLocationId = toFacilityInternalLocationId;
    }

    /**
     * The location (address) the offender is going to, optional
     *
     * @return the toLocationId
     */
    public Long getToLocationId() {
        return toLocationId;
    }

    /**
     * The location (address) the offender is going to, optional
     *
     * @param toLocationId the toLocationId to set
     */
    public void setToLocationId(Long toLocationId) {
        this.toLocationId = toLocationId;
    }

    /**
     * The organization the offender is going to, optional
     *
     * @return the toOrganizationId
     */
    public Long getToOrganizationId() {
        return toOrganizationId;
    }

    /**
     * The organization the offender is going to, optional
     *
     * @param toOrganizationId the toOrganizationId to set
     */
    public void setToOrganizationId(Long toOrganizationId) {
        this.toOrganizationId = toOrganizationId;
    }

    /**
     * The Facility the offender is going to. Will be used to reference the Facility Service, optional
     *
     * @return the toFacilityId
     */
    public Long getToFacilityId() {
        return toFacilityId;
    }

    /**
     * The Facility the offender is going to. Will be used to reference the Facility Service, optional
     *
     * @param toFacilityId the toFacilityId to set
     */
    public void setToFacilityId(Long toFacilityId) {
        this.toFacilityId = toFacilityId;
    }

    /**
     * City the offender is going to, optional
     *
     * @return the toCity
     */
    public String getToCity() {
        return toCity;
    }

    /**
     * City the offender is going to, optional
     *
     * @param toCity the toCity to set
     */
    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    /**
     * State or Province the offender is going to, optional
     *
     * @return the toProvinceState
     */
    public String getToProvinceState() {
        return toProvinceState;
    }

    /**
     * State or Province the offender is going to, optional
     *
     * @param toProvinceState the toProvinceState to set
     */
    public void setToProvinceState(String toProvinceState) {
        this.toProvinceState = toProvinceState;
    }

    /**
     * Identifier for the arresting agency noted on the admission form, optional
     *
     * @return the arrestOrganizationId
     */
    public Long getArrestOrganizationId() {
        return arrestOrganizationId;
    }

    /**
     * Identifier for the arresting agency noted on the admission form, optional
     *
     * @param arrestOrganizationId the arrestOrganizationId to set
     */
    public void setArrestOrganizationId(Long arrestOrganizationId) {
        this.arrestOrganizationId = arrestOrganizationId;
    }

    /**
     * The date of the application, optional
     *
     * @return the applicationDate
     */
    public Date getApplicationDate() {
        return applicationDate;
    }

    /**
     * The date of the application, optional
     *
     * @param applicationDate the applicationDate to set
     */
    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    /**
     * Organization escorting offender, optional
     *
     * @return the escortOrganizationId
     */
    public Long getEscortOrganizationId() {
        return escortOrganizationId;
    }

    /**
     * Organization escorting offender, optional
     *
     * @param escortOrganizationId the escortOrganizationId to set
     */
    public void setEscortOrganizationId(Long escortOrganizationId) {
        this.escortOrganizationId = escortOrganizationId;
    }

    /**
     * Escort Details, optional
     *
     * @return the escortDetails
     */
    public String getEscortDetails() {
        return escortDetails;
    }

    /**
     * Escort Details, optional
     *
     * @param escortDetails the escortDetails to set
     */
    public void setEscortDetails(String escortDetails) {
        this.escortDetails = escortDetails;
    }

    /**
     * Estimated Date of reporting at the receiving facility (Transfer), optional
     *
     * @return the reportingDate
     */
    public Date getReportingDate() {
        return reportingDate;
    }

    /**
     * Estimated Date of reporting at the receiving facility (Transfer), optional
     *
     * @param reportingDate the reportingDate to set
     */
    public void setReportingDate(Date reportingDate) {
        this.reportingDate = reportingDate;
    }

    /**
     * The transportation, optional
     *
     * @return the transportation
     */
    public String getTransportation() {
        return transportation;
    }

    /**
     * The transportation, optional
     *
     * @param transportation the transportation to set
     */
    public void setTransportation(String transportation) {
        this.transportation = transportation;
    }



	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((applicationDate == null) ? 0 : applicationDate.hashCode());
        result = prime * result + ((arrestOrganizationId == null) ? 0 : arrestOrganizationId.hashCode());
        result = prime * result + ((escortDetails == null) ? 0 : escortDetails.hashCode());
        result = prime * result + ((escortOrganizationId == null) ? 0 : escortOrganizationId.hashCode());
        result = prime * result + ((fromCity == null) ? 0 : fromCity.hashCode());
        result = prime * result + ((fromFacilityId == null) ? 0 : fromFacilityId.hashCode());
        result = prime * result + ((fromLocationId == null) ? 0 : fromLocationId.hashCode());
        result = prime * result + ((fromOrganizationId == null) ? 0 : fromOrganizationId.hashCode());
        result = prime * result + ((reportingDate == null) ? 0 : reportingDate.hashCode());
        result = prime * result + ((toCity == null) ? 0 : toCity.hashCode());
        result = prime * result + ((toFacilityId == null) ? 0 : toFacilityId.hashCode());
        result = prime * result + ((toFacilityInternalLocationId == null) ? 0 : toFacilityInternalLocationId.hashCode());
        result = prime * result + ((toLocationId == null) ? 0 : toLocationId.hashCode());
        result = prime * result + ((toOrganizationId == null) ? 0 : toOrganizationId.hashCode());
        result = prime * result + ((toProvinceState == null) ? 0 : toProvinceState.hashCode());
        result = prime * result + ((transportation == null) ? 0 : transportation.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ExternalMovementActivityType other = (ExternalMovementActivityType) obj;
        if (applicationDate == null) {
            if (other.applicationDate != null) {
                return false;
            }
        } else if (!applicationDate.equals(other.applicationDate)) {
            return false;
        }
        if (arrestOrganizationId == null) {
            if (other.arrestOrganizationId != null) {
                return false;
            }
        } else if (!arrestOrganizationId.equals(other.arrestOrganizationId)) {
            return false;
        }
        if (escortDetails == null) {
            if (other.escortDetails != null) {
                return false;
            }
        } else if (!escortDetails.equals(other.escortDetails)) {
            return false;
        }
        if (escortOrganizationId == null) {
            if (other.escortOrganizationId != null) {
                return false;
            }
        } else if (!escortOrganizationId.equals(other.escortOrganizationId)) {
            return false;
        }
        if (fromCity == null) {
            if (other.fromCity != null) {
                return false;
            }
        } else if (!fromCity.equals(other.fromCity)) {
            return false;
        }
        if (fromFacilityId == null) {
            if (other.fromFacilityId != null) {
                return false;
            }
        } else if (!fromFacilityId.equals(other.fromFacilityId)) {
            return false;
        }
        if (fromLocationId == null) {
            if (other.fromLocationId != null) {
                return false;
            }
        } else if (!fromLocationId.equals(other.fromLocationId)) {
            return false;
        }
        if (fromOrganizationId == null) {
            if (other.fromOrganizationId != null) {
                return false;
            }
        } else if (!fromOrganizationId.equals(other.fromOrganizationId)) {
            return false;
        }
        if (reportingDate == null) {
            if (other.reportingDate != null) {
                return false;
            }
        } else if (!reportingDate.equals(other.reportingDate)) {
            return false;
        }
        if (toCity == null) {
            if (other.toCity != null) {
                return false;
            }
        } else if (!toCity.equals(other.toCity)) {
            return false;
        }
        if (toFacilityId == null) {
            if (other.toFacilityId != null) {
                return false;
            }
        } else if (!toFacilityId.equals(other.toFacilityId)) {
            return false;
        }
        if (toFacilityInternalLocationId == null) {
            if (other.toFacilityInternalLocationId != null) {
                return false;
            }
        } else if (!toFacilityInternalLocationId.equals(other.toFacilityInternalLocationId)) {
            return false;
        }
        if (toLocationId == null) {
            if (other.toLocationId != null) {
                return false;
            }
        } else if (!toLocationId.equals(other.toLocationId)) {
            return false;
        }
        if (toOrganizationId == null) {
            if (other.toOrganizationId != null) {
                return false;
            }
        } else if (!toOrganizationId.equals(other.toOrganizationId)) {
            return false;
        }
        if (toProvinceState == null) {
            if (other.toProvinceState != null) {
                return false;
            }
        } else if (!toProvinceState.equals(other.toProvinceState)) {
            return false;
        }
        if (transportation == null) {
            if (other.transportation != null) {
                return false;
            }
        } else if (!transportation.equals(other.transportation)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ExternalMovementActivityType [fromLocationId=" + fromLocationId + ", fromOrganizationId=" + fromOrganizationId + ", fromFacilityId=" + fromFacilityId
                + ", fromCity=" + fromCity + ", toFacilityInternalLocationId=" + toFacilityInternalLocationId + ", toLocationId=" + toLocationId + ", toOrganizationId="
                + toOrganizationId + ", toFacilityId=" + toFacilityId + ", toCity=" + toCity + ", toProvinceState=" + toProvinceState + ", arrestOrganizationId="
                + arrestOrganizationId + ", applicationDate=" + applicationDate + ", escortOrganizationId=" + escortOrganizationId + ", escortDetails=" + escortDetails
                + ", reportingDate=" + reportingDate + ", transportation=" + transportation + ", toString()=" + super.toString() + "]";
    }

}