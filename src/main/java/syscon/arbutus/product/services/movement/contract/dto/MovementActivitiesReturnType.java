package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Movement Activities Return Type
 *
 * @author yshang
 * @version 1.0 (based on Movement Activity 1.0 SDD)
 * @since February 10, 2012.
 */
public class MovementActivitiesReturnType implements Serializable {

    private static final long serialVersionUID = 8461949126393656793L;

    /**
     * Set of External Movement Activities returned
     */
    private List<ExternalMovementActivityType> externalMovementActivity;

    /**
     * Set of Internal Movements returned
     */
    private List<InternalMovementActivityType> internalMovementActivity;

    /**
     * Number of Total Movement Activities
     */
    private Long totalSize;

    /**
     * @return the externalMovementActivity
     */
    public List<ExternalMovementActivityType> getExternalMovementActivity() {
        return externalMovementActivity;
    }

    /**
     * @param externalMovementActivity the externalMovementActivity to set
     */
    public void setExternalMovementActivity(List<ExternalMovementActivityType> externalMovementActivity) {
        this.externalMovementActivity = externalMovementActivity;
    }

    /**
     * @return the internalMovementActivity
     */
    public List<InternalMovementActivityType> getInternalMovementActivity() {
        return internalMovementActivity;
    }

    /**
     * @param internalMovementActivity the internalMovementActivity to set
     */
    public void setInternalMovementActivity(List<InternalMovementActivityType> internalMovementActivity) {
        this.internalMovementActivity = internalMovementActivity;
    }

    /**
     * @return the totalSize
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * @param totalSize the totalSize to set
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "MovementActivitiesReturnType [externalMovementActivity=" + externalMovementActivity + ", internalMovementActivity=" + internalMovementActivity
                + ", totalSize=" + totalSize + "]";
    }

}
