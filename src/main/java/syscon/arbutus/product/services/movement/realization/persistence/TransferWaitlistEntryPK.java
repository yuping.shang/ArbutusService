package syscon.arbutus.product.services.movement.realization.persistence;

import javax.persistence.Column;
import java.io.Serializable;

public class TransferWaitlistEntryPK implements Serializable {

    private final static long serialVersionUID = 181973434912377L;

    @Column(name = "transFacilityId")
    private Long transFacilityId;

    @Column(name = "supervision")
    private Long supervision;

    public TransferWaitlistEntryPK() {
        super();
    }

    public TransferWaitlistEntryPK(Long transFacilityId, Long supervision) {
        super();
        this.transFacilityId = transFacilityId;
        this.supervision = supervision;
    }

    /**
     * @return the transFacilityId
     */
    public Long getTransFacilityId() {
        return transFacilityId;
    }

    /**
     * @param transFacilityId the transFacilityId to set
     */
    public void setTransFacilityId(Long transFacilityId) {
        this.transFacilityId = transFacilityId;
    }

    /**
     * @return the supervision
     */
    public Long getSupervision() {
        return supervision;
    }

    /**
     * @param supervision the supervision to set
     */
    public void setSupervision(Long supervision) {
        this.supervision = supervision;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((supervision == null) ? 0 : supervision.hashCode());
        result = prime * result + ((transFacilityId == null) ? 0 : transFacilityId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TransferWaitlistEntryPK other = (TransferWaitlistEntryPK) obj;
        if (supervision == null) {
            if (other.supervision != null) {
                return false;
            }
        } else if (!supervision.equals(other.supervision)) {
            return false;
        }
        if (transFacilityId == null) {
            if (other.transFacilityId != null) {
                return false;
            }
        } else if (!transFacilityId.equals(other.transFacilityId)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TransferWaitlistPK [transFacilityId=" + transFacilityId + ", supervision=" + supervision + "]";
    }

}
