package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by hshen on 1/20/15.
 */
public class ScheduleConflict implements Serializable

{
    private static final long serialVersionUID = -6333516587896828069L;
    private Long eventId;
    private Long supervisionId;
    private String supervisionDisplayId;
    private Long toFacilityId;
    private Long toLocationId;

    private Date startDate;
    private Date endDate;
    private EventType type;

    private String subType;
    private String location;
    private String externalMoveType;
    private String description;

    public ScheduleConflict() {
    }

    public ScheduleConflict(Long eventId, Long supervisionId, Long toFacilityId, Long toLocationId, Date startDate, Date endDate, String type) {
        this.eventId = eventId;
        this.supervisionId = supervisionId;
        this.toFacilityId = toFacilityId;
        this.toLocationId = toLocationId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.externalMoveType = type;
    }

    @Override
    public String toString() {
        return "ScheduleConflict{" +
                "eventId=" + eventId +
                ", supervisionId=" + supervisionId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", description='" + description + '\'' +
                ", location='" + location + '\'' +
                '}';
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExternalMoveType() {
        return externalMoveType;
    }

    public void setExternalMoveType(String externalMoveType) {
        this.externalMoveType = externalMoveType;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Long getToFacilityId() {
        return toFacilityId;
    }

    public void setToFacilityId(Long toFacilityId) {
        this.toFacilityId = toFacilityId;
    }

    public Long getToLocationId() {
        return toLocationId;
    }

    public void setToLocationId(Long toLocationId) {
        this.toLocationId = toLocationId;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public Long getSupervisionId() {
        return supervisionId;
    }

    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (o == null || getClass() != o.getClass()) {
			return false;
		}

        ScheduleConflict that = (ScheduleConflict) o;

        if (eventId != null ? !eventId.equals(that.eventId) : that.eventId != null) {
			return false;
		}
        if (supervisionId != null ? !supervisionId.equals(that.supervisionId) : that.supervisionId != null) {
			return false;
		}
        if (toFacilityId != null ? !toFacilityId.equals(that.toFacilityId) : that.toFacilityId != null) {
			return false;
		}
        if (toLocationId != null ? !toLocationId.equals(that.toLocationId) : that.toLocationId != null) {
			return false;
		}
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) {
			return false;
		}
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) {
			return false;
		}
        return type == that.type;

    }

    @Override
    public int hashCode() {
        int result = eventId != null ? eventId.hashCode() : 0;
        result = 31 * result + (supervisionId != null ? supervisionId.hashCode() : 0);
        result = 31 * result + (toFacilityId != null ? toFacilityId.hashCode() : 0);
        result = 31 * result + (toLocationId != null ? toLocationId.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    public enum EventType {
        TAP_EXTERNAL_MOVEMENT, CRT_EXTERNAL_MOVEMENT, TRN_EXTERNAL_MOVEMENT, REL_EXTERNAL_MOVEMENT,
        INTERNAL_MOVEMENT,
        VISIT, APPOINTMENT, HEARING,
        PROGRAM, SENTENCE

    }

	public String getSupervisionDisplayId() {
		return supervisionDisplayId;
	}

	public void setSupervisionDisplayId(String supervisionDisplayId) {
		this.supervisionDisplayId = supervisionDisplayId;
	}
}
