package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;

import syscon.arbutus.product.services.activity.contract.dto.ActivityType;

/**
 * Definition of the Movement Details type
 * Java class for MovementDetailType complex type.
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <pre>
 * &lt;complexType name="MovementDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Movement" type="{http://dto.contract.movementmanagement.services.product.arbutus.syscon}MovementActivityType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Activity" type="{http://dto.contract.movementmanagement.services.product.arbutus.syscon}ActivityType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 * @author bkahlon
 * @version 1.0 (Based on SDD 1.0)
 * @since October 24, 2012
 */

public class MovementDetailType implements Serializable {

    private static final long serialVersionUID = -7926608024238743683L;

    private MovementActivityType movement;
    private ActivityType activity;

    /**
     * Constructor with field
     *
     * @param movement
     * @param activity
     */
    public MovementDetailType(MovementActivityType movement, ActivityType activity) {
        this.movement = movement;
        this.activity = activity;
    }

    /**
     * Constructor
     */
    public MovementDetailType() {

    }

    /**
     * Gets the value of the activity property.
     */
    public ActivityType getActivity() {
        return activity;
    }

    /**
     * Sets the value of the activity property.
     */
    public void setActivity(ActivityType activity) {
        this.activity = activity;
    }

    /**
     * Gets the value of the movement property.
     */
    public MovementActivityType getMovement() {
        return movement;
    }

    /**
     * Sets the value of the movement property.
     */
    public void setMovement(MovementActivityType movement) {
        this.movement = movement;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "MovementDetailType [movement=" + movement + ", activity=" + activity + "]";
    }

}
