package syscon.arbutus.product.services.movement.realization.util;

import java.util.Date;

import org.hibernate.Query;
import org.hibernate.Session;
import syscon.arbutus.product.services.movement.contract.dto.TwoMovementsSearchType;
import syscon.arbutus.product.services.movement.realization.util.interfaces.MovementsHQLStrategy;

/**
 * Movements HQL Strategy pattern implementation
 *
 * @author wmadruga
 */
public class MovementsHQLContext {

    MovementsHQLStrategy strategy;

    public MovementsHQLContext(MovementsHQLStrategy strategy) {
        super();
        this.strategy = strategy;
    }

    public Query buildTwoMovementsHQL(Session session, TwoMovementsSearchType searchType, Date dateOut, Date dateIn) {
        return this.strategy.buildTwoMovementsHQL(session, searchType, dateOut, dateIn);
    }
}
