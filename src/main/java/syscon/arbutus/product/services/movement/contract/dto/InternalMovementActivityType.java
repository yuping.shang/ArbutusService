package syscon.arbutus.product.services.movement.contract.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

/**
 * @author yshang
 * @version 2.0 (based on Movement Activity 2.0 SDD)
 * @since October 19, 2012.
 */
public class InternalMovementActivityType extends MovementActivityType {

    private static final long serialVersionUID = 1988119323844191014L;

    /**
     * From Internal location association for an internal movement, not required
     */
    private Long fromFacilityInternalLocationId;

    /**
     * To Internal Location association for an internal movement, required
     */
    @NotNull
    private Long toFacilityInternalLocationId;
    
    

    /**
     * Constructor
     */
    public InternalMovementActivityType() {
        super();
    }

    /**
     * From Facility Internal location association(Id) for an internal movement, optional
     *
     * @return the fromFacilityInternalLocationId
     */
    public Long getFromFacilityInternalLocationId() {
        return fromFacilityInternalLocationId;
    }

    /**
     * From Facility Internal location association(Id) for an internal movement, optional
     *
     * @param fromFacilityInternalLocationId the fromFacilityInternalLocationId to set
     */
    public void setFromFacilityInternalLocationId(Long fromFacilityInternalLocationId) {
        this.fromFacilityInternalLocationId = fromFacilityInternalLocationId;
    }

    /**
     * To Facility Internal Location association for an internal movement, required
     *
     * @return the toFacilityInternalLocationId
     */
    public Long getToFacilityInternalLocationId() {
        return toFacilityInternalLocationId;
    }

    /**
     * To Facility Internal Location association for an internal movement, required
     *
     * @param toFacilityInternalLocationId the toFacilityInternalLocationId to set
     */
    public void setToFacilityInternalLocationId(Long toFacilityInternalLocationId) {
        this.toFacilityInternalLocationId = toFacilityInternalLocationId;
    }
    

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((fromFacilityInternalLocationId == null) ? 0 : fromFacilityInternalLocationId.hashCode());
        result = prime * result + ((toFacilityInternalLocationId == null) ? 0 : toFacilityInternalLocationId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        InternalMovementActivityType other = (InternalMovementActivityType) obj;
        if (fromFacilityInternalLocationId == null) {
            if (other.fromFacilityInternalLocationId != null) {
                return false;
            }
        } else if (!fromFacilityInternalLocationId.equals(other.fromFacilityInternalLocationId)) {
            return false;
        }
        if (toFacilityInternalLocationId == null) {
            if (other.toFacilityInternalLocationId != null) {
                return false;
            }
        } else if (!toFacilityInternalLocationId.equals(other.toFacilityInternalLocationId)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "InternalMovementActivityType [fromFacilityInternalLocationId=" + fromFacilityInternalLocationId + ", toFacilityInternalLocationId="
                + toFacilityInternalLocationId + ", toString()=" + super.toString() + "]";
    }

}