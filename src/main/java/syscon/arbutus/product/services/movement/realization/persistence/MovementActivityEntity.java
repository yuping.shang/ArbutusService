package syscon.arbutus.product.services.movement.realization.persistence;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.*;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.core.services.realization.BaseEntity;
import syscon.arbutus.product.services.realization.model.MetaCode;

/**
 * MovementActivityEntity for Movement Activity Service
 *
 * @author yshang, Syscon Justice Systems
 * @version 1.0
 * @DbComment MA_MovementActivity 'Main table for Movement Activity Service'
 * @DbComment .DTYPE 'Dicriminator column for Hibernate use'
 * @DbComment .createUserId 'User ID who created the object'
 * @DbComment .createDateTime 'Date and time when the object was created'
 * @DbComment .modifyUserId 'User ID who last updated the object'
 * @DbComment .modifyDateTime 'Date and time when the object was last updated'
 * @DbComment .invocationContext 'Invocation context when the create/update action called'
 * @DbComment .version 'the version number of record updated'
 * @since September 27, 2012
 */
@Audited
@Entity
@Cacheable(false)
@Table(name = "MA_MovementActivity")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@SQLDelete(sql = "UPDATE MA_MovementActivity SET flag = 4 WHERE MoveActID = ? and version = ?")
@Where(clause = "flag = 1")
public class MovementActivityEntity extends BaseEntity implements Serializable{

    private static final long serialVersionUID = 3112494972127188577L;

    /**
     * @DbComment MoveActID 'Movement Activity Id'
     */
    @Id
    @Column(name = "MoveActID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_MA_MOVEMENTACTIVITY_ID")
    @SequenceGenerator(name = "SEQ_MA_MOVEMENTACTIVITY_ID", sequenceName = "SEQ_MA_MOVEMENTACTIVITY_ID", allocationSize = 1)
    private Long movementId;
    /**
     * @DbComment movementType 'States the movement type. Supported types are ADM, CRT, TRN, TAP, WR, REL, VISIT, OIC, APP, PROG, BED, MED, CLA'
     */
    @Column(name = "movementType", nullable = false, length = 64)
    @MetaCode(set = MetaSet.MOVEMENT_TYPE)
    protected String movementType;
    /**
     * @DbComment activityId 'Activity Id'
     */
    @Column(name = "activityId", nullable = false)
    private Long activityId;
    /**
     * @DbComment supervisionId 'Static Association to Supervision'
     */
    @Column(name = "supervisionId", nullable = false)
    private Long supervisionId;
    /**
     * @DbComment movementCategory 'States the movement category. Supported categories are EXTERNAL and INTERNAL'
     */
    @Column(name = "movementCategory", nullable = false, length = 64)
    @MetaCode(set = MetaSet.MOVEMENT_CATEGORY)
    private String movementCategory;
    /**
     * @DbComment movementDirection 'States the movement direction. Supported directions are OUT, IN'
     */
    @Column(name = "movementDirection", nullable = false, length = 64)
    @MetaCode(set = MetaSet.MOVEMENT_DIRECTION)
    private String movementDirection;

    /**
     * @DbComment movementStatus 'States the movement status. Supported directions are PENDING, ONHOLD, COMPLETED, CANCELLED, NOSHOW'
     */
    @Column(name = "movementStatus", nullable = false, length = 64)
    @MetaCode(set = MetaSet.MOVEMENT_STATUS)
    private String movementStatus;

    /**
     * @DbComment movementReason 'States the movement reason.'
     */
    @Column(name = "movementReason", nullable = false, length = 64)
    @MetaCode(set = MetaSet.MOVEMENT_REASON)
    private String movementReason;

    /**
     * @DbComment movementOutcome 'States the movement outcome.'
     */
    @Column(name = "movementOutcome", nullable = true, length = 64)
    @MetaCode(set = MetaSet.MOVEMENT_OUTCOME)
    private String movementOutcome;

    /**
     * @DbComment movementDate 'States the movement date.'
     */
    @Column(name = "movementDate", nullable = true)
    private Date movementDate;

    @ForeignKey(name = "Fk_MA_Comment")
    @OneToMany(mappedBy = "movementActivity", cascade = {CascadeType.REMOVE},  fetch = FetchType.LAZY)
    @BatchSize(size = 20)
    private Set<MovementActivityCommentEntity> commentText;

    /**
     * @DbComment approvedByStaff 'Association to StaffService.'
     */
    @Column(name = "approvedByStaff", nullable = true)
    private Long approvedByStaffId;

    /**
     * @DbComment approvalDate 'The date the approval of the Temporary Absence move was given.'
     */
    @Column(name = "approvalDate", nullable = true)
    private Date approvalDate;

    /**
     * @DbComment approvalComments 'Any comments provided by the approver.'
     */
    @Column(name = "approvalComments", nullable = true, length = 1024)
    private String approvalComments;

    /**
     * @DbComment toInternalLocationId 'The internal location the offender is going to.'
     */
    @Column(name = "toInternalLocationId", nullable = true)
    private Long toInternalLocationId;

    /**
     * @DbComment interSenScheduleId 'The Intermittent Sentence Schedule Entity associated by this movement.'
     */
    @Column(name = "interSenScheduleId", nullable = true)
    private Long intermittentSenScheduleId;

    /**
     * @DbComment MA_MovementActivity.groupId 'Two movements ID to bind them'
     */
    @Column(name = "groupid", nullable = true, length = 128)
    private String groupid;


    @Column(name = "cancelreason", nullable = true, length = 128)
    @MetaCode(set = MetaSet.TRANSFER_CANCEL_REASON)
    private String cancelreason;


    @Column(name = "participateStaffId", nullable = true)
    private Long participateStaffId;



    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    public MovementActivityEntity() {
        super();
    }

   public Long getParticipateStaffId() {
        return participateStaffId;
    }

    public void setParticipateStaffId(Long participateStaffId) {
        this.participateStaffId = participateStaffId;
    }

    public String getCancelreason() {
        return cancelreason;
    }

    public void setCancelreason(String cancelreason) {
        this.cancelreason = cancelreason;
    }

    @Override
    public Long getId() {
        return movementId;
    }

    /**
     * @return the movementId
     */
    public Long getMovementId() {
        return movementId;
    }

    /**
     * @param movementId the movementId to set
     */
    public void setMovementId(Long movementId) {
        this.movementId = movementId;
    }

    /**
     * @return the activityId
     */
    public Long getActivityId() {
        return activityId;
    }

    /**
     * @param activityId the activityId to set
     */
    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    /**
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * @return the movementCategory
     */
    public String getMovementCategory() {
        return movementCategory;
    }

    /**
     * @param movementCategory the movementCategory to set
     */
    public void setMovementCategory(String movementCategory) {
        this.movementCategory = movementCategory;
    }

    /**
     * @return the movementType
     */
    public String getMovementType() {
        return movementType;
    }

    /**
     * @param movementType the movementType to set
     */
    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    /**
     * @return the movementDirection
     */
    public String getMovementDirection() {
        return movementDirection;
    }

    /**
     * @param movementDirection the movementDirection to set
     */
    public void setMovementDirection(String movementDirection) {
        this.movementDirection = movementDirection;
    }

    /**
     * @return the movementStatus
     */
    public String getMovementStatus() {
        return movementStatus;
    }

    /**
     * @param movementStatus the movementStatus to set
     */
    public void setMovementStatus(String movementStatus) {
        this.movementStatus = movementStatus;
    }

    /**
     * @return the movementReason
     */
    public String getMovementReason() {
        return movementReason;
    }

    /**
     * @param movementReason the movementReason to set
     */
    public void setMovementReason(String movementReason) {
        this.movementReason = movementReason;
    }

    /**
     * @return the movementOutcome
     */
    public String getMovementOutcome() {
        return movementOutcome;
    }

    /**
     * @param movementOutcome the movementOutcome to set
     */
    public void setMovementOutcome(String movementOutcome) {
        this.movementOutcome = movementOutcome;
    }

    /**
     * @return the movementDate
     */
    public Date getMovementDate() {
        return movementDate;
    }

    /**
     * @param movementDate the movementDate to set
     */
    public void setMovementDate(Date movementDate) {
        this.movementDate = movementDate;
    }

    /**
     * @return the commentText
     */
    public Set<MovementActivityCommentEntity> getCommentText() {
        if (commentText == null) {
            commentText = new HashSet<MovementActivityCommentEntity>();
        }
        return commentText;
    }

    /**
     * @param commentText the commentText to set
     */
    public void setCommentText(Set<MovementActivityCommentEntity> commentText) {
        this.commentText = commentText;
    }

    /**
     * @return the approvedByStaffId
     */
    public Long getApprovedByStaffId() {
        return approvedByStaffId;
    }

    /**
     * @param approvedByStaffId the approvedByStaffId to set
     */
    public void setApprovedByStaffId(Long approvedByStaffId) {
        this.approvedByStaffId = approvedByStaffId;
    }

    /**
     * @return the approvalDate
     */
    public Date getApprovalDate() {
        return approvalDate;
    }

    /**
     * @param approvalDate the approvalDate to set
     */
    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    /**
     * @return the approvalComments
     */
    public String getApprovalComments() {
        return approvalComments;
    }

    /**
     * @param approvalComments the approvalComments to set
     */
    public void setApprovalComments(String approvalComments) {
        this.approvalComments = approvalComments;
    }

    /**
     * @return the toInternalLocationId
     */
    public Long getToInternalLocationId() {
        return toInternalLocationId;
    }

    /**
     * @param toInternalLocationId the toInternalLocationId to set
     */
    public void setToInternalLocationId(Long toInternalLocationId) {
        this.toInternalLocationId = toInternalLocationId;
    }

    /**
     * @return the intermittentSenScheduleId
     */
    public Long getIntermittentSenScheduleId() {
        return intermittentSenScheduleId;
    }

    /**
     * @param intermittentSenScheduleId
     */
    public void setIntermittentSenScheduleId(Long intermittentSenScheduleId) {
        this.intermittentSenScheduleId = intermittentSenScheduleId;
    }

    /**
     * @return the flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /**
     * @return the group id
     */
    public String getGroupId() {
        return groupid;
    }

    /**
     * @param groupid, the group id to set
     */
    public void setGroupId(String groupid) {
        this.groupid = groupid;
    }



}