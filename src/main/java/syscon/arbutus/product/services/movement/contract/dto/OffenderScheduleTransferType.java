package syscon.arbutus.product.services.movement.contract.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import syscon.arbutus.product.services.conflict.contract.dto.NonAssociationType;


/**
 * Definition of Offender Schedule Transfer List
 *
 * @author bkahlon
 * @version 1.0
 * @since January 24, 2012
 */
public class OffenderScheduleTransferType implements Serializable {

    private static final long serialVersionUID = -3981555676749703890L;

    private String lastName;
    private String firstName;
    private String offenderNumber;
    private Date scheduledTransferDate;
    private String scheduledTransferTime;
    private Date scheduledTransferStartTime;
    private Long fromFacility;
    private Long toFacility;
    private String movementReason;
    private String movementType;
    private String movementStatus;
    private Long movementId;
    private Long personIdentifierId;
    private Long supervisionId;
    private Long activityId;

    private String fromCity;
    private Long fromFacilityInternalLocation;
    private Long fromOrganization;
    private Long fromLocation;

    private String toCity;
    private Long toFacilityInternalLocation;
    private Long toOrganization;
    private Long toLocation;
    private String groupId;
    private Long escortOrgId;
    private String escortDetals;
    private String escortOrgName;
    private String approvalcomments;
    private String movementDirection;
    private Long inmatePersonId;
    private String supervisionDisplayId;
    private String fromFacilityName;
    private String toFacilityName;
    private String fromFacilityInternalLocationName;
    private String toFacilityInternalLocationName;

    private String transportation;
    private Date applicationDate;
    private Date movementDate;
    private Date reportingDate;

    private Date releaseDate;
    private Date returnDate;
    private Date expectedArrivalDate;
    private Date arrivalDate;
    private Date releaseTime;
    private Date returnTime;
    private Date arrivalTime;
    private Date movementTime;
    private Date expectedArrivalTime;

    //for TAP
    private Long approverId;
    private String approverName;
    private String approvalComments;
    private Date approvalDate;

    //for CRT
    private Long courtRoomId;
    private String courtRoom;

    private String movementCategory;
    private String movementOutcome;
    private String comment;
    private Long inmateCurrentFacility;
    private Long movementInId;
    private Long movementOutId;
    private boolean exchanged;
    private String currentLocation;
    private String associatedMovementStatus;
    private Long associatedMovementId;
    private List<NonAssociationType> nonAssociationConflictsForHousing;
    private List<NonAssociationType> nonAssociationTypeList;
    private String cancelReasoncode;
    private String housing;
    
    private Long reserveHousingId;
    private String reservHousingDesc;
    private Long reserveFacilityId;
    private Long reservationId;
    private Long participateOfficerId;

    private  boolean bAdhoc;

    public boolean isbAdhoc() {
        return bAdhoc;
    }

    public void setbAdhoc(boolean bAdhoc) {
        this.bAdhoc = bAdhoc;
    }

    /**
	 * @return the reserveHousingId
	 */
	public Long getReserveHousingId() {
		return reserveHousingId;
	}

	/**
	 * @param reserveHousingId the reserveHousingId to set
	 */
	public void setReserveHousingId(Long reserveHousingId) {
		this.reserveHousingId = reserveHousingId;
	}

	/**
	 * @return the reservHousingDesc
	 */
	public String getReservHousingDesc() {
		return reservHousingDesc;
	}

	/**
	 * @param reservHousingDesc the reservHousingDesc to set
	 */
	public void setReservHousingDesc(String reservHousingDesc) {
		this.reservHousingDesc = reservHousingDesc;
	}

	/**
     * Empty constructor
     */
    public OffenderScheduleTransferType() {

    }

    public OffenderScheduleTransferType(OffenderScheduleTransferType copy) {
		super();
		this.lastName = copy.lastName;
		this.firstName = copy.firstName;
		this.offenderNumber = copy.offenderNumber;
		this.scheduledTransferDate = copy.scheduledTransferDate;
		this.scheduledTransferTime = copy.scheduledTransferTime;
        this.scheduledTransferStartTime = copy.scheduledTransferStartTime;
		this.fromFacility = copy.fromFacility;
		this.toFacility = copy.toFacility;
		this.movementReason = copy.movementReason;
		this.movementType = copy.movementType;
		this.movementStatus = copy.movementStatus;
		this.movementId = copy.movementId;
		this.personIdentifierId = copy.personIdentifierId;
		this.supervisionId = copy.supervisionId;
		this.activityId = copy.activityId;
		this.fromCity = copy.fromCity;
		this.fromFacilityInternalLocation = copy.fromFacilityInternalLocation;
		this.fromOrganization = copy.fromOrganization;
		this.fromLocation = copy.fromLocation;
		this.toCity = copy.toCity;
		this.toFacilityInternalLocation = copy.toFacilityInternalLocation;
		this.toOrganization = copy.toOrganization;
		this.toLocation = copy.toLocation;
		this.groupId = copy.groupId;
		this.escortOrgId = copy.escortOrgId;
		this.escortDetals = copy.escortDetals;
		this.approvalcomments = copy.approvalcomments;
		this.movementDirection = copy.movementDirection;
		this.inmatePersonId = copy.inmatePersonId;
		this.supervisionDisplayId = copy.supervisionDisplayId;
		this.fromFacilityName = copy.fromFacilityName;
		this.toFacilityName = copy.toFacilityName;
		this.fromFacilityInternalLocationName = copy.fromFacilityInternalLocationName;
		this.toFacilityInternalLocationName = copy.toFacilityInternalLocationName;
		this.transportation = copy.transportation;
		this.applicationDate = copy.applicationDate;
		this.movementDate = copy.movementDate;
		this.reportingDate = copy.reportingDate;
		this.releaseDate = copy.releaseDate;
		this.returnDate = copy.returnDate;
		this.expectedArrivalDate = copy.expectedArrivalDate;
		this.arrivalDate = copy.arrivalDate;
		this.releaseTime = copy.releaseTime;
		this.returnTime = copy.returnTime;
		this.arrivalTime = copy.arrivalTime;
		this.movementTime = copy.movementTime;
		this.expectedArrivalTime = copy.expectedArrivalTime;
		this.approverId = copy.approverId;
		this.approverName = copy.approverName;
		this.approvalComments = copy.approvalComments;
		this.approvalDate = copy.approvalDate;
		this.courtRoomId = copy.courtRoomId;
		this.courtRoom = copy.courtRoom;
		this.movementCategory = copy.movementCategory;
		this.movementOutcome = copy.movementOutcome;
		this.comment = copy.comment;
		this.inmateCurrentFacility = copy.inmateCurrentFacility;
		this.movementInId = copy.movementInId;
		this.movementOutId = copy.movementOutId;
		this.exchanged = copy.exchanged;
		this.currentLocation = copy.currentLocation;
		this.associatedMovementStatus = copy.associatedMovementStatus;
		this.associatedMovementId = copy.associatedMovementId;
		this.cancelReasoncode = copy.cancelReasoncode;
        this.participateOfficerId = copy.participateOfficerId;
	}
	/**
     * @param lastName,              optional
     * @param firstName,             optional
     * @param offenderNumber,        optional
     * @param scheduledTransferDate, optional
     * @param scheduledTransferTime, optional
     * @param fromFacility,          optional
     * @param toFacility,            optional
     * @param movementReason,        optional
     * @param movementId,            optional
     * @param personIdentifierId,    optional
     * @param supervisionId,         optional
     */
    public OffenderScheduleTransferType(String lastName, String firstName, String offenderNumber, Date scheduledTransferDate, String scheduledTransferTime,
            Long fromFacility, Long toFacility, String movementReason, Long movementId, Long personIdentifierId, Long supervisionId) {
        super();
        this.lastName = lastName;
        this.firstName = firstName;
        this.offenderNumber = offenderNumber;
        this.scheduledTransferDate = scheduledTransferDate;
        this.scheduledTransferTime = scheduledTransferTime;
        this.fromFacility = fromFacility;
        this.toFacility = toFacility;
        this.movementReason = movementReason;
        this.movementId = movementId;
        this.personIdentifierId = personIdentifierId;
        this.supervisionId = supervisionId;
    }

    /**
     * Gets the value of the lastName property.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the value of the firstName property.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the value of the offenderNumber property.
     */
    public String getOffenderNumber() {
        return offenderNumber;
    }

    /**
     * Sets the value of the offenderNumber property.
     */
    public void setOffenderNumber(String offenderNumber) {
        this.offenderNumber = offenderNumber;
    }

    /**
     * Gets the value of the scheduledTransferDate property.
     */
    public Date getScheduledTransferDate() {
        return scheduledTransferDate;
    }

    /**
     * Sets the value of the scheduledTransferDate property.
     */
    public void setScheduledTransferDate(Date scheduledTransferDate) {
        this.scheduledTransferDate = scheduledTransferDate;
    }

    /**
     * Gets the value of the scheduledTransferTime property.
     */
    public String getScheduledTransferTime() {
        return scheduledTransferTime;
    }

    /**
     * Sets the value of the scheduledTransferTime property.
     */
    public void setScheduledTransferTime(String scheduledTransferTime) {
        this.scheduledTransferTime = scheduledTransferTime;
    }
    

    /**
	 * @return the scheduledTransferStartTime
	 */
	public Date getScheduledTransferStartTime() {
		return scheduledTransferStartTime;
	}

	/**
	 * @param scheduledTransferStartTime the scheduledTransferStartTime to set
	 */
	public void setScheduledTransferStartTime(Date scheduledTransferStartTime) {
		this.scheduledTransferStartTime = scheduledTransferStartTime;
	}

	/**
     * Gets the value of the fromFacility property.
     */
    public Long getFromFacility() {
        return fromFacility;
    }

    /**
     * Sets the value of the fromFacility property.
     */
    public void setFromFacility(Long fromFacility) {
        this.fromFacility = fromFacility;
    }

    /**
     * Gets the value of the toFacility property.
     */
    public Long getToFacility() {
        return toFacility;
    }

    /**
     * Sets the value of the toFacility property.
     */
    public void setToFacility(Long toFacility) {
        this.toFacility = toFacility;
    }

    /**
     * Gets the value of the movementReason property.
     */
    public String getMovementReason() {
        return movementReason;
    }

    /**
     * Sets the value of the movementReason property.
     */
    public void setMovementReason(String movementReason) {
        this.movementReason = movementReason;
    }

    /**
     * Gets the value of the movementId property.
     */
    public Long getMovementId() {
        return movementId;
    }

    /**
     * Sets the value of the movementId property.
     */
    public void setMovementId(Long movementId) {
        this.movementId = movementId;
    }

    /**
     * Gets the value of the personIdentifierId property.
     */
    public Long getPersonIdentifierId() {
        return personIdentifierId;
    }

    /**
     * Sets the value of the personIdentifierId property.
     */
    public void setPersonIdentifierId(Long personIdentifierId) {
        this.personIdentifierId = personIdentifierId;
    }

    /**
     * Gets the value of the supervisionId property.
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * Sets the value of the supervisionId property.
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Gets the value of the activityId property.
     */
    public Long getActivityId() {
        return activityId;
    }

    /**
     * Sets the value of the activityId property.
     */
    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    /**
     * Gets the value of the fromCity property.
     */
    public String getFromCity() {
        return fromCity;
    }

    /**
     * Sets the value of the fromCity property.
     */
    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    /**
     * Gets the value of the fromFacilityInternalLocation property.
     */
    public Long getFromFacilityInternalLocation() {
        return fromFacilityInternalLocation;
    }

    /**
     * Sets the value of the fromFacilityInternalLocation property.
     */
    public void setFromFacilityInternalLocation(Long fromFacilityInternalLocation) {
        this.fromFacilityInternalLocation = fromFacilityInternalLocation;
    }

    /**
     * Gets the value of the fromOrganization property.
     */
    public Long getFromOrganization() {
        return fromOrganization;
    }

    /**
     * Sets the value of the fromOrganization property.
     */
    public void setFromOrganization(Long fromOrganization) {
        this.fromOrganization = fromOrganization;
    }

    /**
     * Gets the value of the fromLocation property.
     */
    public Long getFromLocation() {
        return fromLocation;
    }

    /**
     * Sets the value of the fromLocation property.
     */
    public void setFromLocation(Long fromLocation) {
        this.fromLocation = fromLocation;
    }

    /**
     * Gets the value of the toCity property.
     */
    public String getToCity() {
        return toCity;
    }

    /**
     * Sets the value of the toCity property.
     */
    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    /**
     * Gets the value of the toFacilityInternalLocation property.
     */
    public Long getToFacilityInternalLocation() {
        return toFacilityInternalLocation;
    }

    /**
     * Sets the value of the toFacilityInternalLocation property.
     */
    public void setToFacilityInternalLocation(Long toFacilityInternalLocation) {
        this.toFacilityInternalLocation = toFacilityInternalLocation;
    }

    /**
     * Gets the value of the toOrganization property.
     */
    public Long getToOrganization() {
        return toOrganization;
    }

    /**
     * Sets the value of the toOrganization property.
     */
    public void setToOrganization(Long toOrganization) {
        this.toOrganization = toOrganization;
    }

    /**
     * Gets the value of the toLocation property.
     */
    public Long getToLocation() {
        return toLocation;
    }

    /**
     * Sets the value of the toLocation property.
     */
    public void setToLocation(Long toLocation) {
        this.toLocation = toLocation;
    }

    /**
     * Gets the value of the groupId property.
     *
     * @return the groupId
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Sets the value of the groupId property.
     *
     * @param groupId Long
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "OffenderScheduleTransferType [lastName=" + lastName + ", firstName=" + firstName + ", offenderNumber=" + offenderNumber + ", scheduledTransferDate="
                + scheduledTransferDate + ", scheduledTransferTime=" + scheduledTransferTime + ", fromFacility=" + fromFacility + ", toFacility=" + toFacility
                + ", movementReason=" + movementReason + ", movementId=" + movementId + ", personIdentifierId=" + personIdentifierId + ", supervisionId=" + supervisionId
                + ", activityId=" + activityId + ", fromCity=" + fromCity + ", fromFacilityInternalLocation=" + fromFacilityInternalLocation + ", fromOrganization="
                + fromOrganization + ", fromLocation=" + fromLocation + ", toCity=" + toCity + ", toFacilityInternalLocation=" + toFacilityInternalLocation
                + ", toOrganization=" + toOrganization + ", toLocation=" + toLocation + ", groupId=" + groupId + "]";
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getEscortDetals() {
        return escortDetals;
    }

    public void setEscortDetals(String escortDetals) {
        this.escortDetals = escortDetals;
    }
    
    /**
	 * @return the escortOrgName
	 */
	public String getEscortOrgName() {
		return escortOrgName;
	}

	/**
	 * @param escortOrgName the escortOrgName to set
	 */
	public void setEscortOrgName(String escortOrgName) {
		this.escortOrgName = escortOrgName;
	}

	public String getApprovalcomments() {
        return approvalcomments;
    }

    public void setApprovalcomments(String approvalcomments) {
        this.approvalcomments = approvalcomments;
    }

    public Long getEscortOrgId() {
        return escortOrgId;
    }

    public void setEscortOrgId(Long escortOrgId) {
        this.escortOrgId = escortOrgId;
    }

    public String getMovementStatus() {
        return movementStatus;
    }

    public void setMovementStatus(String movementStatus) {
        this.movementStatus = movementStatus;
    }

    public String getMovementDirection() {
        return movementDirection;
    }

    public void setMovementDirection(String movementDirection) {
        this.movementDirection = movementDirection;
    }

    public String getSupervisionDisplayId() {
        return supervisionDisplayId;
    }

    public void setSupervisionDisplayId(String supervisionDisplayId) {
        this.supervisionDisplayId = supervisionDisplayId;
    }

    public Long getInmatePersonId() {
        return inmatePersonId;
    }

    public void setInmatePersonId(Long inmatePersonId) {
        this.inmatePersonId = inmatePersonId;
    }

    public String getFromFacilityName() {
        return fromFacilityName;
    }

    public void setFromFacilityName(String fromFacilityName) {
        this.fromFacilityName = fromFacilityName;
    }

    public String getToFacilityName() {
        return toFacilityName;
    }

    public void setToFacilityName(String toFacilityName) {
        this.toFacilityName = toFacilityName;
    }

    public String getFromFacilityInternalLocationName() {
        return fromFacilityInternalLocationName;
    }

    public void setFromFacilityInternalLocationName(String fromFacilityInternalLocationName) {
        this.fromFacilityInternalLocationName = fromFacilityInternalLocationName;
    }

    public String getToFacilityInternalLocationName() {
        return toFacilityInternalLocationName;
    }

    public void setToFacilityInternalLocationName(String toFacilityInternalLocationName) {
        this.toFacilityInternalLocationName = toFacilityInternalLocationName;
    }

    public String getTransportation() {
        return transportation;
    }

    public void setTransportation(String transportation) {
        this.transportation = transportation;
    }

    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public Date getMovementDate() {
        return movementDate;
    }

    public void setMovementDate(Date movementDate) {
        this.movementDate = movementDate;
    }

    public Date getReportingDate() {
        return reportingDate;
    }

    public void setReportingDate(Date reportingDate) {
        this.reportingDate = reportingDate;
    }

    public Long getApproverId() {
        return approverId;
    }

    public void setApproverId(Long approverId) {
        this.approverId = approverId;
    }

    public String getApproverName() {
        return approverName;
    }

    public void setApproverName(String approverName) {
        this.approverName = approverName;
    }

    public String getApprovalComments() {
        return approvalComments;
    }

    public void setApprovalComments(String approvalComments) {
        this.approvalComments = approvalComments;
    }

    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public Long getCourtRoomId() {
        return courtRoomId;
    }

    public void setCourtRoomId(Long courtRoomId) {
        this.courtRoomId = courtRoomId;
    }

    public String getCourtRoom() {
        return courtRoom;
    }

    public void setCourtRoom(String courtRoom) {
        this.courtRoom = courtRoom;
    }

    public String getMovementCategory() {
        return movementCategory;
    }

    public void setMovementCategory(String movementCategory) {
        this.movementCategory = movementCategory;
    }

    public String getMovementOutcome() {
        return movementOutcome;
    }

    public void setMovementOutcome(String movementOutcome) {
        this.movementOutcome = movementOutcome;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    public Date getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Date returnTime) {
        this.returnTime = returnTime;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Date getMovementTime() {
        return movementTime;
    }

    public void setMovementTime(Date movementTime) {
        this.movementTime = movementTime;
    }

    public Date getExpectedArrivalDate() {
        return expectedArrivalDate;
    }

    public void setExpectedArrivalDate(Date expectedArrivalDate) {
        this.expectedArrivalDate = expectedArrivalDate;
    }

    public Date getExpectedArrivalTime() {
        return expectedArrivalTime;
    }

    public void setExpectedArrivalTime(Date expectedArrivalTime) {
        this.expectedArrivalTime = expectedArrivalTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return inmateCurrentFacility
     */
    public Long getInmateCurrentFacility() {
        return inmateCurrentFacility;
    }

    /**
     * @param inmateCurrentFacility
     */
    public void setInmateCurrentFacility(Long inmateCurrentFacility) {
        this.inmateCurrentFacility = inmateCurrentFacility;
    }

    /**
     * @return movementInId
     */
    public Long getMovementInId() {
        return movementInId;
    }

    /**
     * @param movementInId
     */
    public void setMovementInId(Long movementInId) {
        this.movementInId = movementInId;
    }

    /**
     * @return movementOutId
     */
    public Long getMovementOutId() {
        return movementOutId;
    }

    /**
     * @param movementOutId
     */
    public void setMovementOutId(Long movementOutId) {
        this.movementOutId = movementOutId;
    }

    /**
     * @return exchanged
     */
    public boolean isExchanged() {
        return exchanged;
    }

    /**
     * @param exchanged
     */
    public void setExchanged(boolean exchanged) {
        this.exchanged = exchanged;
    }

    /**
     * @return currentLocation
     */
    public String getCurrentLocation() {
        return currentLocation;
    }

    /**
     * @param currentLocation
     */
    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    /**
     * @return associatedMovementStatus
     */
    public String getAssociatedMovementStatus() {
        return associatedMovementStatus;
    }

    /**
     * @param associatedMovementStatus
     */
    public void setAssociatedMovementStatus(String associatedMovementStatus) {
        this.associatedMovementStatus = associatedMovementStatus;
    }

    /**
     * @return associatedMovementId
     */
    public Long getAssociatedMovementId() {
        return associatedMovementId;
    }

    /**
     * @param associatedMovementId
     */
    public void setAssociatedMovementId(Long associatedMovementId) {
        this.associatedMovementId = associatedMovementId;
    }
    
    /**
     * Getter for nonAssociationConflictsForHousing
     * @return  List<NonAssociationType>
     */
    public List<NonAssociationType> getNonAssociationConflictsForHousing() {
        return nonAssociationConflictsForHousing;
    }

    /**
     * Setter for nonAssociationConflictsForHousing
     * @param nonAssociationConflictsForHousing
     */
    public void setNonAssociationConflictsForHousing(List<NonAssociationType> nonAssociationConflictsForHousing) {
        this.nonAssociationConflictsForHousing = nonAssociationConflictsForHousing;
    }

	/**
	 * @return the nonAssociationTypeList
	 */
	public List<NonAssociationType> getNonAssociationTypeList() {
		return nonAssociationTypeList;
	}

	/**
	 * @param nonAssociationTypeList the nonAssociationTypeList to set
	 */
	public void setNonAssociationTypeList(
			List<NonAssociationType> nonAssociationTypeList) {
		this.nonAssociationTypeList = nonAssociationTypeList;
	}
	/**
	 * get the cancel reason code
	 */
	public String getCancelReasoncode() {
		return cancelReasoncode;
	}
	/**
	 * @param cancelReasoncode , cancel reason to set
	 */
	public void setCancelReasoncode(String cancelReasoncode) {
		this.cancelReasoncode = cancelReasoncode;
	}

	/**
	 * @return the housing
	 */
	public String getHousing() {
		return housing;
	}

	/**
	 * @param housing the housing to set
	 */
	public void setHousing(String housing) {
		this.housing = housing;
	}

	/**
	 * @return the reserveFacilityId
	 */
	public Long getReserveFacilityId() {
		return reserveFacilityId;
	}

	/**
	 * @param reserveFacilityId the reserveFacilityId to set
	 */
	public void setReserveFacilityId(Long reserveFacilityId) {
		this.reserveFacilityId = reserveFacilityId;
	}

	/**
	 * @return the reservationId
	 */
	public Long getReservationId() {
		return reservationId;
	}

	/**
	 * @param reservationId the reservationId to set
	 */
	public void setReservationId(Long reservationId) {
		this.reservationId = reservationId;
	}

    public Long getParticipateOfficerId() {
        return participateOfficerId;
    }

    public void setParticipateOfficerId(Long participateOfficerId) {
        this.participateOfficerId = participateOfficerId;
    }
}
