package syscon.arbutus.product.services.activity.contract.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Definition of a Schedule Config Variables return type
 *
 * @author yshang
 * @version 1.0 (Based on SDD 1.0)
 * @since April 27, 2012
 */
public class ScheduleConfigVariablesReturnType implements Serializable {

    private static final long serialVersionUID = 5400986999302150444L;

    private List<ScheduleConfigVariableType> scheduleConfigVariables;
    private Long totalSize;

    /**
     * @return the scheduleConfigVariables
     */
    public List<ScheduleConfigVariableType> getScheduleConfigVariables() {
        return scheduleConfigVariables;
    }

    /**
     * @param scheduleConfigVariables the scheduleConfigVariables to set
     */
    public void setScheduleConfigVariables(List<ScheduleConfigVariableType> scheduleConfigVariables) {
        this.scheduleConfigVariables = scheduleConfigVariables;
    }

    /**
     * @return the totalSize
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * @param totalSize the totalSize to set
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ScheduleConfigVariablesReturnType [scheduleConfigVariables=" + scheduleConfigVariables + ", totalSize=" + totalSize + "]";
    }

}
