package syscon.arbutus.product.services.activity.contract.dto;

/**
 * Created by ian on 9/22/15.
 */
public enum NthWeekday {
    FIRST,
    SECOND,
    THIRD,
    FOURTH,
    LAST;

    public static NthWeekday fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }
}