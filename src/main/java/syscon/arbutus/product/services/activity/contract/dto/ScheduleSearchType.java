package syscon.arbutus.product.services.activity.contract.dto;

import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

/**
 * Definition of a Schedule Search Type
 * <p>At least one of the search criteria must be specified.
 *
 * @author yshang
 * @version 1.0 (Based on SDD 1.0)
 * @since April 27, 2012
 */
@ArbutusConstraint(constraints = { "fromEffectiveDate <= toEffectiveDate", "fromScheduleStartTime <= toScheduleStartTime", "fromScheduleEndTime <= toScheduleEndTime" })
public class ScheduleSearchType implements Serializable {

    private static final long serialVersionUID = 9065866761296771925L;

    private String scheduleCategory;
    private Long facilityId;
    private Long capacity;
    private Date fromEffectiveDate;
    private Date toEffectiveDate;
    private Date fromTerminationDate;
    private Date toTerminationDate;
    private Boolean activeScheduleFlag;
    @Pattern(regexp = "^[0-9]?[0-9]:[0-9][0-9]$")
    private String fromScheduleStartTime;
    @Pattern(regexp = "^[0-9]?[0-9]:[0-9][0-9]$")
    private String toScheduleStartTime;
    @Pattern(regexp = "^[0-9]?[0-9]:[0-9][0-9]$")
    private String fromScheduleEndTime;
    @Pattern(regexp = "^[0-9]?[0-9]:[0-9][0-9]$")
    private String toScheduleEndTime;
    private RecurrencePatternType recurrencePattern;
    private String facilityScheduleReason;

    private Long forInternalLocationId;
    private Set<Long> atInternalLocationIds;
    private Set<Long> locationIds;
    private Set<Long> organizationIds;

    public ScheduleSearchType() {
        super();
    }

    /**
     * @return the scheduleCategory
     */
    public String getScheduleCategory() {
        return scheduleCategory;
    }

    /**
     * @param scheduleCategory the scheduleCategory to set
     */
    public void setScheduleCategory(String scheduleCategory) {
        this.scheduleCategory = scheduleCategory;
    }

    /**
     * @return the facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * @param facilityId the facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * @return the capacity
     */
    public Long getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    /**
     * @return the fromEffectiveDate
     */
    public Date getFromEffectiveDate() {
        return fromEffectiveDate;
    }

    /**
     * @param fromEffectiveDate the fromEffectiveDate to set
     */
    public void setFromEffectiveDate(Date fromEffectiveDate) {
        this.fromEffectiveDate = fromEffectiveDate;
    }

    /**
     * @return the toEffectiveDate
     */
    public Date getToEffectiveDate() {
        return toEffectiveDate;
    }

    /**
     * @param toEffectiveDate the toEffectiveDate to set
     */
    public void setToEffectiveDate(Date toEffectiveDate) {
        this.toEffectiveDate = toEffectiveDate;
    }

    /**
     * @return the fromTerminationDate
     */
    public Date getFromTerminationDate() {
        return fromTerminationDate;
    }

    /**
     * @param fromTerminationDate the fromTerminationDate to set
     */
    public void setFromTerminationDate(Date fromTerminationDate) {
        this.fromTerminationDate = fromTerminationDate;
    }

    /**
     * @return the toTerminationDate
     */
    public Date getToTerminationDate() {
        return toTerminationDate;
    }

    /**
     * @param toTerminationDate the toTerminationDate to set
     */
    public void setToTerminationDate(Date toTerminationDate) {
        this.toTerminationDate = toTerminationDate;
    }

    /**
     * @return the activeScheduleFlag
     */
    public Boolean getActiveScheduleFlag() {
        return activeScheduleFlag;
    }

    /**
     * @param activeScheduleFlag the activeScheduleFlag to set
     */
    public void setActiveScheduleFlag(Boolean activeScheduleFlag) {
        this.activeScheduleFlag = activeScheduleFlag;
    }

    /**
     * @return the fromScheduleStartTime
     */
    public String getFromScheduleStartTime() {
        return fromScheduleStartTime;
    }

    /**
     * @param fromScheduleStartTime the fromScheduleStartTime to set
     */
    public void setFromScheduleStartTime(String fromScheduleStartTime) {
        this.fromScheduleStartTime = fromScheduleStartTime;
    }

    /**
     * @return the toScheduleStartTime
     */
    public String getToScheduleStartTime() {
        return toScheduleStartTime;
    }

    /**
     * @param toScheduleStartTime the toScheduleStartTime to set
     */
    public void setToScheduleStartTime(String toScheduleStartTime) {
        this.toScheduleStartTime = toScheduleStartTime;
    }

    /**
     * @return the fromScheduleEndTime
     */
    public String getFromScheduleEndTime() {
        return fromScheduleEndTime;
    }

    /**
     * @param fromScheduleEndTime the fromScheduleEndTime to set
     */
    public void setFromScheduleEndTime(String fromScheduleEndTime) {
        this.fromScheduleEndTime = fromScheduleEndTime;
    }

    /**
     * @return the toScheduleEndTime
     */
    public String getToScheduleEndTime() {
        return toScheduleEndTime;
    }

    /**
     * @param toScheduleEndTime the toScheduleEndTime to set
     */
    public void setToScheduleEndTime(String toScheduleEndTime) {
        this.toScheduleEndTime = toScheduleEndTime;
    }

    /**
     * @return the recurrencePattern
     */
    public RecurrencePatternType getRecurrencePattern() {
        return recurrencePattern;
    }

    /**
     * @param recurrencePattern the recurrencePattern to set
     */
    public void setRecurrencePattern(RecurrencePatternType recurrencePattern) {
        this.recurrencePattern = recurrencePattern;
    }

    /**
     * @return the facilityScheduleReason
     */
    public String getFacilityScheduleReason() {
        return facilityScheduleReason;
    }

    /**
     * @param facilityScheduleReason the facilityScheduleReason to set
     */
    public void setFacilityScheduleReason(String facilityScheduleReason) {
        this.facilityScheduleReason = facilityScheduleReason;
    }

    /**
     * @return the forInternalLocationId
     */
    public Long getForInternalLocationId() {
        return forInternalLocationId;
    }

    /**
     * @param forInternalLocationId Long the forInteralLocationId to set
     */
    public void setForInternalLocationId(Long forInternalLocationId) {
        this.forInternalLocationId = forInternalLocationId;
    }

    /**
     * @return the atInternalLocationIds
     */
    public Set<Long> getAtInternalLocationIds() {
        return atInternalLocationIds;
    }

    /**
     * @param atInternalLocationIds the atInternalLocationIds to set
     */
    public void setAtInternalLocationIds(Set<Long> atInternalLocationIds) {
        this.atInternalLocationIds = atInternalLocationIds;
    }

    /**
     * @return the locationIds
     */
    public Set<Long> getLocationIds() {
        return locationIds;
    }

    /**
     * @param locationIds the locationIds to set
     */
    public void setLocationIds(Set<Long> locationIds) {
        this.locationIds = locationIds;
    }

    /**
     * @return the organizationIds
     */
    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    /**
     * @param organizationIds the organizationIds to set
     */
    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ScheduleSearchType [scheduleCategory=" + scheduleCategory + ", facilityId=" + facilityId + ", capacity=" + capacity + ", fromEffectiveDate="
                + fromEffectiveDate + ", toEffectiveDate=" + toEffectiveDate + ", fromTerminationDate=" + fromTerminationDate + ", toTerminationDate=" + toTerminationDate
                + ", activeScheduleFlag=" + activeScheduleFlag + ", fromScheduleStartTime=" + fromScheduleStartTime + ", toScheduleStartTime=" + toScheduleStartTime
                + ", fromScheduleEndTime=" + fromScheduleEndTime + ", toScheduleEndTime=" + toScheduleEndTime + ", recurrencePattern=" + recurrencePattern
                + ", facilityScheduleReason=" + facilityScheduleReason + ", forInternalLocationId=" + forInternalLocationId + ", atInternalLocationIds="
                + atInternalLocationIds + ", locationIds=" + locationIds + ", organizationIds=" + organizationIds + "]";
    }

}
