package syscon.arbutus.product.services.activity.contract.dto;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * * Java class for WeeklyRecurrencePatternType
 *
 * @author lhan
 * @version 1.0 (Based on SDD 2.0)
 * @since Oct 15, 2012
 */
public class OnceRecurrencePatternType extends RecurrencePatternType {

    private static final long serialVersionUID = 5820645238087492397L;
    @NotNull
    private Date occurrenceDate;

    /**
     * Constructor
     */
    public OnceRecurrencePatternType() {
        super(0L);
    }

    /**
     * Constructor
     * <ul>
     * <li>occurrenceDate -- Define the date on which the schedule occurs.</li>
     * </ul>
     *
     * @param occurrenceDate Date required. occurrenceDate
     */
    public OnceRecurrencePatternType(Date occurrenceDate) {
        super(0L);
        this.setOccurrenceDate(occurrenceDate);
    }

    /**
     * <ul>
     * <li>occurrenceDate -- the date on which the schedule occurs.</li>
     * </ul>
     *
     * @return Date required. occurrenceDate
     */
    public Date getOccurrenceDate() {
        return occurrenceDate;
    }

    /**
     * <ul>
     * <li>occurrenceDate -- the date on which the schedule occurs.</li>
     * </ul>
     *
     * @param occurrenceDate Date required. occurrenceDate to set
     */
    public void setOccurrenceDate(Date occurrenceDate) {
        this.occurrenceDate = occurrenceDate;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((occurrenceDate == null) ? 0 : occurrenceDate.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (!(obj instanceof OnceRecurrencePatternType)) {
			return false;
		}
        OnceRecurrencePatternType other = (OnceRecurrencePatternType) obj;
        if (occurrenceDate == null) {
            if (other.occurrenceDate != null) {
				return false;
			}
        } else if (!occurrenceDate.equals(other.occurrenceDate)) {
			return false;
		}
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "OnceRecurrencePatternType [occurrenceDate=" + occurrenceDate + "]";
    }

}
