package syscon.arbutus.product.services.activity.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * ACT_EXDATE Exclude Date table for Recurrence Pattern for Activity Service
 *
 * @author YShang
 * @DbComment ACT_EXDATE 'Exclude Date table for Recurrence Pattern for Activity Service'
 * @since June 18, 2014
 */
@Audited
@Entity
@Table(name = "ACT_EXDATE")
public class ExcludeDateEntity implements Serializable, Comparable<ExcludeDateEntity> {

    private static final long serialVersionUID = 3570346247491325680L;

    /**
     * @DbComment ACT_EXDATE.excludeDateId 'Id of Exclude Date table'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_ACT_EXDATE_ID")
    @SequenceGenerator(name = "SEQ_ACT_EXDATE_ID", sequenceName = "SEQ_ACT_EXDATE_ID", allocationSize = 1)
    @Column(name = "excludeDateId")
    private Long excludeDateId;

    /**
     * @DbComment ACT_EXDATE.thedate 'The date to be excluded'
     */
    @Column(name = "thedate", nullable = false)
    private Date date;

    /**
     * @DbComment ACT_EXDATE.startTime 'Start time (time only has meaning)'
     */
    @Column(name = "startTime")
    private Date startTime;

    /**
     * @DbComment ACT_EXDATE.endTime 'End time (time only has meaning)'
     */
    @Column(name = "endTime")
    private Date endTime;

    /**
     * @DbComment ACT_EXDATE.actRPatnId 'Foreign key to ACT_SchRPatn table'
     */
    @ForeignKey(name = "Fk_ACT_EXDATE")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "actRPatnId", nullable = false)
    private RecurrencePatternEntity recurrencePattern;

    /**
     * @DbComment ACT_EXDATE.createUserId 'Create User Id'
     * @DbComment ACT_EXDATE.createDateTime 'Create Date Time'
     * @DbComment ACT_EXDATE.modifyUserId 'Modify User Id'
     * @DbComment ACT_EXDATE.modifyDateTime 'Modify Date Time'
     * @DbComment ACT_EXDATE.invocationContext 'Invocation Context'
     */
    @Embedded
    @NotAudited
    private StampEntity stamp;

    public ExcludeDateEntity() {
    }

    public Long getExcludeDateId() {
        return excludeDateId;
    }

    public void setExcludeDateId(Long excludeDateId) {
        this.excludeDateId = excludeDateId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public RecurrencePatternEntity getRecurrencePattern() {
        return recurrencePattern;
    }

    public void setRecurrencePattern(RecurrencePatternEntity recurrencePattern) {
        this.recurrencePattern = recurrencePattern;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public int compareTo(ExcludeDateEntity o) {
        return this.date.compareTo(o.date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (!(o instanceof ExcludeDateEntity)) {
			return false;
		}

        ExcludeDateEntity that = (ExcludeDateEntity) o;

        if (date != null ? !date.equals(that.date) : that.date != null) {
			return false;
		}
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) {
			return false;
		}
        if (excludeDateId != null ? !excludeDateId.equals(that.excludeDateId) : that.excludeDateId != null) {
			return false;
		}
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = excludeDateId != null ? excludeDateId.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ExcludeDateEntity{" +
                "excludeDateId=" + excludeDateId +
                ", date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
