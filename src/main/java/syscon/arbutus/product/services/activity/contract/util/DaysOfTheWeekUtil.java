package syscon.arbutus.product.services.activity.contract.util;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import syscon.arbutus.product.services.activity.contract.dto.DailyWeeklyCustomRecurrencePatternType;
import syscon.arbutus.product.services.activity.contract.dto.DayOfWeekEnum;
import syscon.arbutus.product.services.activity.contract.dto.DaysOfTheWeek;
import syscon.arbutus.product.services.activity.contract.dto.RecurrencePatternType;

/**
 * Created by ian on 9/8/15.
 */
public class DaysOfTheWeekUtil {

    /**
     * Given a recurrence pattern type, return the DaysOfTheWeek object associated with it.
     * <p>
     *     if the recurrencePattern object is not a "CUSTOM" recurrence pattern, then an empty Optional object is returned
     * </p>
     * @param recurrencePatternType
     * @return
     */
    public Optional<DaysOfTheWeek> generateDaysOfTheWeekFromRecurrencePattern(RecurrencePatternType recurrencePatternType) {
        Optional<DaysOfTheWeek> daysOfTheWeek = Optional.empty();
        if (recurrencePatternType instanceof DailyWeeklyCustomRecurrencePatternType) {
            Set<String> existingDays = ((DailyWeeklyCustomRecurrencePatternType) recurrencePatternType).getRecurOnDays();
            if (existingDays != null) {
                daysOfTheWeek = Optional.of(generateDaysOfTheWeek(existingDays));
            }
        }
        return daysOfTheWeek;
    }

    public Set<String> generateStringSet(DaysOfTheWeek days) {
        Set<String> returnSet = new HashSet<>();
        if (days.monday) {
            returnSet.add(DayOfWeekEnum.MON.getName());
        }
        if (days.tuesday) {
            returnSet.add(DayOfWeekEnum.TUE.getName());
        }
        if (days.wednesday) {
            returnSet.add(DayOfWeekEnum.WED.getName());
        }
        if (days.thursday) {
            returnSet.add(DayOfWeekEnum.THU.getName());
        }
        if (days.friday) {
            returnSet.add(DayOfWeekEnum.FRI.getName());
        }
        if (days.saturday) {
            returnSet.add(DayOfWeekEnum.SAT.getName());
        }
        if (days.sunday) {
            returnSet.add(DayOfWeekEnum.SUN.getName());
        }
        return returnSet;
    }

    public DaysOfTheWeek generateDaysOfTheWeek(Set<String> existingDays) {
        DaysOfTheWeek daysOfTheWeek = new DaysOfTheWeek();
        for (String day : existingDays) {
            if (DayOfWeekEnum.MON.getName().equals(day)) {
                daysOfTheWeek.monday = true;
            }
            if (DayOfWeekEnum.TUE.getName().equals(day)) {
                daysOfTheWeek.tuesday = true;
            }
            if (DayOfWeekEnum.WED.getName().equals(day)) {
                daysOfTheWeek.wednesday = true;
            }
            if (DayOfWeekEnum.THU.getName().equals(day)) {
                daysOfTheWeek.thursday = true;
            }
            if (DayOfWeekEnum.FRI.getName().equals(day)) {
                daysOfTheWeek.friday = true;
            }
            if (DayOfWeekEnum.SAT.getName().equals(day)) {
                daysOfTheWeek.saturday = true;
            }
            if (DayOfWeekEnum.SUN.getName().equals(day)) {
                daysOfTheWeek.sunday = true;
            }
        }
        return daysOfTheWeek;
    }

}
