package syscon.arbutus.product.services.activity.contract.dto;

/**
 * Created by ian on 9/16/15.
 * DTO used specifically in facility counts, the intent of this class is to hide
 * the complexity of RecurrencePatternType by presenting a facade with the AbstractRecurrenceObject
 */
public class SimpleSchedule extends Schedule {

    private AbstractRecurrenceObject recurrenceObject;

    public SimpleSchedule() {

    }

    public SimpleSchedule(SimpleSchedule schedule) {
        super(schedule);
        this.recurrenceObject = schedule.getRecurrenceObject();
    }

    public SimpleSchedule(Schedule schedule, AbstractRecurrenceObject abstractRecurrenceObject) {
        super(schedule);
        this.recurrenceObject = abstractRecurrenceObject;
    }

    public AbstractRecurrenceObject getRecurrenceObject() {
        return recurrenceObject;
    }

    public void setRecurrenceObject(AbstractRecurrenceObject recurrenceObject) {
        this.recurrenceObject = recurrenceObject;
    }
}
