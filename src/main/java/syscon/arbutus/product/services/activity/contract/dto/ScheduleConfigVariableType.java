package syscon.arbutus.product.services.activity.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Definition of a Schedule Config Variable type
 * Java class for ScheduleConfigVariableType
 *
 * @author yshang
 * @version 1.0 (Based on SDD 1.0)
 * @since April 27, 2012
 */
public class ScheduleConfigVariableType implements Serializable {

    private static final long serialVersionUID = -7742941830424310256L;

    private Long scheduleConfigIdentification;
    @NotNull
    private String scheduleCategory; // ReferenceSet(ActivityCategory)
    private Long numOfNextAvailableTimeslots;

    /**
     * Constructor
     */
    public ScheduleConfigVariableType() {
        super();
    }

    /**
     * Constructor
     *
     * @param config required.
     */
    public ScheduleConfigVariableType(ScheduleConfigVariableType config) {
        super();
        this.scheduleConfigIdentification = config.getScheduleConfigIdentification();
        this.scheduleCategory = config.getScheduleCategory();
        this.numOfNextAvailableTimeslots = config.getNumOfNextAvailableTimeslots();
    }

    /**
     * Constructor
     * <ul>
     * <li>scheduleConfigIdentification -- A unique identifier for the set of configuration variable defining the behavior of all schedules of a particular category.</li>
     * <li>scheduleCategory -- Reference code of ActivityCategory set. The type of schedules for which this set of configuration variable are defined.</li>
     * <li>numOfNextAvailableTimeslots -- Define the limit on the number of next available timeslots, to which a user can book activity, that can be generated using all schedules defined for a particular type of activity.</li>
     * </ul>
     *
     * @param scheduleConfigIdentification Ignored when create; Required when update.
     * @param scheduleCategory             Required. Reference code of ActivityCategory set.
     * @param numOfNextAvailableTimeslots  Optional
     */
    public ScheduleConfigVariableType(Long scheduleConfigIdentification, String scheduleCategory, Long numOfNextAvailableTimeslots) {
        super();
        this.scheduleConfigIdentification = scheduleConfigIdentification;
        this.scheduleCategory = scheduleCategory;
        this.numOfNextAvailableTimeslots = numOfNextAvailableTimeslots;
    }

    /**
     * Get Schedule Config Identification
     * <p>A unique identifier for the set of configuration variable defining the behavior of all schedules of a particular category.
     *
     * @return Long Ignored when create; Required when update. scheduleConfigIdentification
     */
    public Long getScheduleConfigIdentification() {
        return scheduleConfigIdentification;
    }

    /**
     * Set Schedule Config Identification
     * <p>A unique identifier for the set of configuration variable defining the behavior of all schedules of a particular category.
     *
     * @param scheduleConfigIdentification Long Ignored when create; Required when update. scheduleConfigIdentification the scheduleConfigIdentification to set
     */
    public void setScheduleConfigIdentification(Long scheduleConfigIdentification) {
        this.scheduleConfigIdentification = scheduleConfigIdentification;
    }

    /**
     * Get Schedule Category
     * <p>Reference code of ActivityCategory set. The type of schedules for which this set of configuration variable are defined.
     *
     * @return String required. scheduleCategory
     */
    public String getScheduleCategory() {
        return scheduleCategory;
    }

    /**
     * Set Schedule Category
     * <p>Reference code of ActivityCategory set. The type of schedules for which this set of configuration variable are defined.
     *
     * @param scheduleCategory String required. scheduleCategory the scheduleCategory to set
     */
    public void setScheduleCategory(String scheduleCategory) {
        this.scheduleCategory = scheduleCategory;
    }

    /**
     * Get NumOfNextAvailableTimeslots
     * <p>Define the limit on the number of next available timeslots, to which a user can book activity, that can be generated using all schedules defined for a particular type of activity.
     *
     * @return Long optional. numOfNextAvailableTimeslots
     */
    public Long getNumOfNextAvailableTimeslots() {
        return numOfNextAvailableTimeslots;
    }

    /**
     * Set NumOfNextAvailableTimeslots
     * <p>Define the limit on the number of next available timeslots, to which a user can book activity, that can be generated using all schedules defined for a particular type of activity.
     *
     * @param numOfNextAvailableTimeslots Long optional. numOfNextAvailableTimeslots the numOfNextAvailableTimeslots to set
     */
    public void setNumOfNextAvailableTimeslots(Long numOfNextAvailableTimeslots) {
        this.numOfNextAvailableTimeslots = numOfNextAvailableTimeslots;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((numOfNextAvailableTimeslots == null) ? 0 : numOfNextAvailableTimeslots.hashCode());
        result = prime * result + ((scheduleCategory == null) ? 0 : scheduleCategory.hashCode());
        result = prime * result + ((scheduleConfigIdentification == null) ? 0 : scheduleConfigIdentification.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ScheduleConfigVariableType other = (ScheduleConfigVariableType) obj;
        if (numOfNextAvailableTimeslots == null) {
            if (other.numOfNextAvailableTimeslots != null) {
				return false;
			}
        } else if (!numOfNextAvailableTimeslots.equals(other.numOfNextAvailableTimeslots)) {
			return false;
		}
        if (scheduleCategory == null) {
            if (other.scheduleCategory != null) {
				return false;
			}
        } else if (!scheduleCategory.equals(other.scheduleCategory)) {
			return false;
		}
        if (scheduleConfigIdentification == null) {
            if (other.scheduleConfigIdentification != null) {
				return false;
			}
        } else if (!scheduleConfigIdentification.equals(other.scheduleConfigIdentification)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ScheduleConfigVariableType [scheduleConfigIdentification=" + scheduleConfigIdentification + ", scheduleCategory=" + scheduleCategory
                + ", numOfNextAvailableTimeslots=" + numOfNextAvailableTimeslots + "]";
    }

}
