package syscon.arbutus.product.services.activity.contract.dto;

/**
 * Created by ian on 9/22/15.
 */
public enum Month {
    JAN,
    FEB,
    MAR,
    APR,
    MAY,
    JUN,
    JUL,
    AUG,
    SEP,
    OCT,
    NOV,
    DEC;

    public static Month fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }
}

