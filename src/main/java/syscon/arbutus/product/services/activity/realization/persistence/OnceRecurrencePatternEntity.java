package syscon.arbutus.product.services.activity.realization.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

import org.hibernate.envers.Audited;

/**
 * OnceRecurrencePatternEntity for Activity Service
 *
 * @author lhan
 * @version 1.0 (based on SDD 2.0)
 * @DbComment ACT_SchRPatn 'Activity Recurrence Pattern for Activity Service'
 * @since Oct 15, 2012
 */
@Audited
@Entity
public class OnceRecurrencePatternEntity extends RecurrencePatternEntity {

    private static final long serialVersionUID = 1151074707248968980L;

    /**
     * @DbComment ACT_SchRPatn.occurrenceDate 'Define the date on which the schedule occurs.'
     */
    @Column(name = "occurrenceDate") //, nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date occurrenceDate;

    public OnceRecurrencePatternEntity() {
        super();
    }

    /**
     * @param id
     * @param recurrenceInterval
     * @param occurrenceDate
     */
    public OnceRecurrencePatternEntity(Long id, Long recurrenceInterval, Date occurrenceDate) {
        super(id, recurrenceInterval);
        this.occurrenceDate = occurrenceDate;
    }

    public OnceRecurrencePatternEntity(OnceRecurrencePatternEntity onceRecurrencePattern) {
        super(onceRecurrencePattern.getId(), onceRecurrencePattern.getRecurrenceInterval());
        this.occurrenceDate = onceRecurrencePattern.getOccurrenceDate();
    }

    public OnceRecurrencePatternEntity(Long id, Long recurrenceInterval, RecurrencePatternEntity frequency) {
        super(id, recurrenceInterval);
    }

    /**
     * @param recurrencePattern
     */
    public OnceRecurrencePatternEntity(RecurrencePatternEntity recurrencePattern) {
        super(recurrencePattern);
    }

    /**
     * @param occurrenceDate
     */
    public OnceRecurrencePatternEntity(Date occurrenceDate) {
        super();
        this.occurrenceDate = occurrenceDate;
    }

    /**
     * isWellFormed -- To validate the value of the properties of the type.
     */
    public Boolean isWellFormed() {
        if (!super.isWellFormed() || occurrenceDate == null) {
			return Boolean.FALSE;
		}
        return Boolean.TRUE;
    }

    /**
     * @return the occurrenceDate
     */
    public Date getOccurrenceDate() {
        return occurrenceDate;
    }

    /**
     * @param occurrenceDate the occurrenceDate to set
     */
    public void setOcurrenceDate(Date occurrenceDate) {
        this.occurrenceDate = occurrenceDate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((occurrenceDate == null) ? 0 : occurrenceDate.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        OnceRecurrencePatternEntity other = (OnceRecurrencePatternEntity) obj;
        if (occurrenceDate == null) {
            if (other.occurrenceDate != null) {
				return false;
			}
        } else if (!occurrenceDate.equals(other.occurrenceDate)) {
			return false;
		}
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "OnceRecurrencePatternEntity [occurrenceDate=" + occurrenceDate + "]";
    }

}
