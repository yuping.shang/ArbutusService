package syscon.arbutus.product.services.activity.realization.persistence;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;

/**
 * WeeklyRecurrencePatternEntity for Activity Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment ACT_SchRPatn 'Activity Recurrence Pattern for Activity Service'
 * @since September 18, 2012
 */
@Audited
@Entity
public class WeeklyRecurrencePatternEntity extends RecurrencePatternEntity {

    private static final long serialVersionUID = -4855338663809998527L;

    @ForeignKey(name = "Fk_ACT_SchWk", inverseName = "Fk_ACT_SchRPatn")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
    @JoinTable(name = "ACT_SCHRPATN_ACT_SCHWK", joinColumns = { @JoinColumn(name = "actRPatnId") },
            inverseJoinColumns = { @JoinColumn(name = "actSchWkId") })
    @Valid
    private Set<WeekdayEntity> recurOnWeekdays;

    public WeeklyRecurrencePatternEntity() {
        super();
    }

    public WeeklyRecurrencePatternEntity(WeeklyRecurrencePatternEntity recurPtn) {
        super(recurPtn);
        this.recurOnWeekdays = recurPtn.getRecurOnWeekdays();
    }

    /**
     * @param id
     * @param recurrenceInterval
     */
    public WeeklyRecurrencePatternEntity(Long id, Long recurrenceInterval, Set<WeekdayEntity> recurOnWeekdays) {
        super(id, recurrenceInterval);
        this.recurOnWeekdays = recurOnWeekdays;
    }

    /**
     * @param id
     * @param recurrenceInterval
     */
    public WeeklyRecurrencePatternEntity(Long id, Long recurrenceInterval) {
        super(id, recurrenceInterval);
    }

    /**
     * @param recurrencePattern
     */
    public WeeklyRecurrencePatternEntity(RecurrencePatternEntity recurrencePattern) {
        super(recurrencePattern);
    }

    /**
     * @param recurOnWeekdays
     */
    public WeeklyRecurrencePatternEntity(Set<WeekdayEntity> recurOnWeekdays) {
        super();
        this.recurOnWeekdays = recurOnWeekdays;
    }

    /**
     * isWellFormed -- To validate the value of the properties of the type.
     */
    public Boolean isWellFormed() {
        if (!super.isWellFormed() || recurOnWeekdays == null) {
			return Boolean.FALSE;
		}
        return Boolean.TRUE;
    }

    /**
     * @return the recurOnWeekdays
     */
    public Set<WeekdayEntity> getRecurOnWeekdays() {
        if (recurOnWeekdays == null) {
			recurOnWeekdays = new HashSet<WeekdayEntity>();
		}
        return recurOnWeekdays;
    }

    /**
     * @param recurOnWeekdays the recurOnWeekdays to set
     */
    public void setRecurOnWeekdays(Set<WeekdayEntity> recurOnWeekdays) {
        this.recurOnWeekdays = recurOnWeekdays;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((recurOnWeekdays == null) ? 0 : recurOnWeekdays.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        WeeklyRecurrencePatternEntity other = (WeeklyRecurrencePatternEntity) obj;
        if (recurOnWeekdays == null) {
            if (other.recurOnWeekdays != null) {
				return false;
			}
        } else if (!recurOnWeekdays.equals(other.recurOnWeekdays)) {
			return false;
		}
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "WeeklyRecurrencePatternEntity [recurOnWeekdays=" + recurOnWeekdays + "]";
    }

}
