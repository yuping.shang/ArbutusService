package syscon.arbutus.product.services.activity.contract.util;

import java.util.Set;

import syscon.arbutus.product.services.activity.contract.dto.*;

/**
 * Created by ian on 9/8/15.
 * RecurrencePatternUtil provides utility functions and ENUMs for interating with recurrence patterns.
 *
 * <p>
 *     currently only implemented for a subset of all available recurrence patterns and should be appended
 *     to on an as needed basis.
 * </p>
 */
public class RecurrencePatternFactory {


    /**
     * generate
     * <p>Given an AbstractRecurrenceObject (any concrete superclass of it),
     * return it's equivalent RecurrencePatternType
     *
     * <li>if the input is null, function will return a new instance of DailyWeeklyCustomRecurrencePatternType</li>
     * </p>
     *
     * @param recurrenceObject - AbstractRecurrenceObject
     * @return RecurrencePatternType
     */
    public RecurrencePatternType generate(AbstractRecurrenceObject recurrenceObject) {
        DailyWeeklyCustomRecurrencePatternType patternType = new DailyWeeklyCustomRecurrencePatternType();
        if (recurrenceObject != null) {
            if (recurrenceObject instanceof WeekdayRecurrenceObject) {
                patternType.setWeekdayOnlyFlag(true);
            } else if (recurrenceObject instanceof CustomRecurrenceObject) {
                patternType.setRecurOnDays(new DaysOfTheWeekUtil().generateStringSet(((CustomRecurrenceObject) recurrenceObject).getDaysOfTheWeek()));
            }
        }
        return patternType;
    }

    /**
     * generate
     * <p>
     *     Given a recurrence enum and a set of days, generate a DailyWeeklyCustomRecurrencePatternType object
     *     <li>
     *         the Set of days can be null for all recurrence types except for CUSTOM
     *     </li>
     * </p>
     *
     * @param recurrenceEnum
     * @param days
     * @return
     */
    public DailyWeeklyCustomRecurrencePatternType generate(RecurrenceEnum recurrenceEnum, Set<String> days) {
        DailyWeeklyCustomRecurrencePatternType patternType = new DailyWeeklyCustomRecurrencePatternType();
        if (RecurrenceEnum.WEEKDAYS.equals(recurrenceEnum)) {
            patternType.setWeekdayOnlyFlag(true);
        } else if (RecurrenceEnum.CUSTOM.equals(recurrenceEnum)) {
            patternType.setRecurOnDays(days);
        }
        return patternType;
    }



}
