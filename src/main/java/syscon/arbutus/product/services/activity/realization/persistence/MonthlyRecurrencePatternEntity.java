package syscon.arbutus.product.services.activity.realization.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.realization.model.MetaCode;

/**
 * MonthlyRecurrencePatternEntity for Activity Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment ACT_SchRPatn 'Activity Recurrence Pattern for Activity Service'
 * @since September 18, 2012
 */
@Entity
@Audited
public class MonthlyRecurrencePatternEntity extends RecurrencePatternEntity {

    private static final long serialVersionUID = -4159122449817685180L;

    /**
     * @DbComment ACT_SchRPatn.dayOfMonth 'Day of Month'
     */
    @Column(name = "dayOfMonth", nullable = true)
    private Long dayOfMonth;

    /**
     * @DbComment ACT_SchRPatn.recurOnWeekday 'Recurrence on Weekday'
     */
    @Column(name = "recurOnWeekday", nullable = true, length = 64)
    @MetaCode(set = MetaSet.WEEKDAY)
    private String recurOnWeekday;

    /**
     * @DbComment ACT_SchRPatn.nthWeekday 'Nth Weekday'
     */
    @Column(name = "nthWeekday", nullable = true, length = 64)
    @MetaCode(set = MetaSet.NTHWEEKDAY)
    private String nthWeekday;

    public MonthlyRecurrencePatternEntity() {
        super();
    }

    public MonthlyRecurrencePatternEntity(MonthlyRecurrencePatternEntity monthlyRecurrencePattern) {
        super(monthlyRecurrencePattern);
        this.dayOfMonth = monthlyRecurrencePattern.getDayOfMonth();
        this.recurOnWeekday = monthlyRecurrencePattern.getRecurOnWeekday();
        this.nthWeekday = monthlyRecurrencePattern.getNthWeekday();
    }

    /**
     * @param id
     * @param recurrenceInterval
     */
    public MonthlyRecurrencePatternEntity(Long id, Long recurrenceInterval) {
        super(id, recurrenceInterval);
    }

    /**
     * @param recurrencePattern
     */
    public MonthlyRecurrencePatternEntity(RecurrencePatternEntity recurrencePattern) {
        super(recurrencePattern);
    }

    public MonthlyRecurrencePatternEntity(Long id, Long recurrenceInterval, Long dayOfMonth, String recurOnWeekday, String nthWeekday) {
        super(id, recurrenceInterval);
        this.dayOfMonth = dayOfMonth;
        this.recurOnWeekday = recurOnWeekday;
        this.nthWeekday = nthWeekday;
    }

    /**
     * @param dayOfMonth
     * @param recurOnWeekday
     * @param nthWeekday
     */
    public MonthlyRecurrencePatternEntity(Long dayOfMonth, String recurOnWeekday, String nthWeekday) {
        super();
        this.dayOfMonth = dayOfMonth;
        this.recurOnWeekday = recurOnWeekday;
        this.nthWeekday = nthWeekday;
    }

    /**
     * isWellFormed -- To validate the value of the properties of the type.
     *
     * @return Boolean
     */
    public Boolean isWellFormed() {
        if (!super.isWellFormed()) {
			return Boolean.FALSE;
		}
        return Boolean.TRUE;
    }

    /**
     * @return the dayOfMonth
     */
    public Long getDayOfMonth() {
        return dayOfMonth;
    }

    /**
     * @param dayOfMonth the dayOfMonth to set
     */
    public void setDayOfMonth(Long dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    /**
     * @return the recurOnWeekday
     */
    public String getRecurOnWeekday() {
        return recurOnWeekday;
    }

    /**
     * @param recurOnWeekday the recurOnWeekday to set
     */
    public void setRecurOnWeekday(String recurOnWeekday) {
        this.recurOnWeekday = recurOnWeekday;
    }

    /**
     * @return the nthWeekday
     */
    public String getNthWeekday() {
        return nthWeekday;
    }

    /**
     * @param nthWeekday the nthWeekday to set
     */
    public void setNthWeekday(String nthWeekday) {
        this.nthWeekday = nthWeekday;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((dayOfMonth == null) ? 0 : dayOfMonth.hashCode());
        result = prime * result + ((nthWeekday == null) ? 0 : nthWeekday.hashCode());
        result = prime * result + ((recurOnWeekday == null) ? 0 : recurOnWeekday.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        MonthlyRecurrencePatternEntity other = (MonthlyRecurrencePatternEntity) obj;
        if (dayOfMonth == null) {
            if (other.dayOfMonth != null) {
				return false;
			}
        } else if (!dayOfMonth.equals(other.dayOfMonth)) {
			return false;
		}
        if (nthWeekday == null) {
            if (other.nthWeekday != null) {
				return false;
			}
        } else if (!nthWeekday.equals(other.nthWeekday)) {
			return false;
		}
        if (recurOnWeekday == null) {
            if (other.recurOnWeekday != null) {
				return false;
			}
        } else if (!recurOnWeekday.equals(other.recurOnWeekday)) {
			return false;
		}
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "MonthlyRecurrencePatternEntity [dayOfMonth=" + dayOfMonth + ", recurOnWeekday=" + recurOnWeekday + ", nthWeekday=" + nthWeekday + "]";
    }

}
