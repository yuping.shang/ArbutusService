package syscon.arbutus.product.services.activity.contract.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Definition of a Timeslot Capacity type
 *
 * @author yshang
 * @version 1.0 (Based on SDD 1.0)
 * @since April 27, 2012
 */
public class TimeslotCapacityType implements Serializable {

    private static final long serialVersionUID = -1757477187121210854L;

    @NotNull
    @Valid
    private ScheduleTimeslotType scheduleTimeslot;
    private Set<Long> activityIDs;
    @NotNull
    private Long capacityTaken;
    @NotNull
    private Long capacityRemaining;

    /**
     * Constructor
     */
    public TimeslotCapacityType() {
        super();
    }

    /**
     * Constructor
     *
     * @param timeslotCapacity required.
     */
    public TimeslotCapacityType(TimeslotCapacityType timeslotCapacity) {
        super();
        this.scheduleTimeslot = timeslotCapacity.getScheduleTimeslot();
        this.activityIDs = timeslotCapacity.getActivityIDs();
        this.capacityTaken = timeslotCapacity.getCapacityTaken();
        this.capacityRemaining = timeslotCapacity.getCapacityRemaining();
    }

    /**
     * Constructor
     * <ul>
     * <li>scheduleTimeslot -- A schedule timeslot.</li>
     * <li>activityIDs -- A unique identifier of activity that is booked to a schedule timeslot.</li>
     * <li>capacityTaken -- Number of activities that are booked to a scheduled timeslot.</li>
     * <li>capacityRemaining -- Number of activities that can be booked to a scheduled timeslot.</li>
     * </ul>
     *
     * @param scheduleTimeslot  Required
     * @param activityIDs       Optional
     * @param capacityTaken     Required
     * @param capacityRemaining Required
     */
    public TimeslotCapacityType(@NotNull @Valid ScheduleTimeslotType scheduleTimeslot, Set<Long> activityIDs, @NotNull Long capacityTaken,
            @NotNull Long capacityRemaining) {
        super();
        this.scheduleTimeslot = scheduleTimeslot;
        this.activityIDs = activityIDs;
        this.capacityTaken = capacityTaken;
        this.capacityRemaining = capacityRemaining;
    }

    /**
     * Get Schedule Timeslot
     * <p>A schedule timeslot.
     *
     * @return ScheduleTimeslotType required. scheduleTimeslot
     */
    public ScheduleTimeslotType getScheduleTimeslot() {
        return scheduleTimeslot;
    }

    /**
     * Set Schedule Timeslot
     * <p>A schedule timeslot.
     *
     * @param scheduleTimeslot ScheduleTimeslotType required. scheduleTimeslot
     *                         the scheduleTimeslot to set
     */
    public void setScheduleTimeslot(ScheduleTimeslotType scheduleTimeslot) {
        this.scheduleTimeslot = scheduleTimeslot;
    }

    /**
     * Get Activity IDs
     * <p>Get a set of Activity IDs
     *
     * @return the activityIDs
     */
    public Set<Long> getActivityIDs() {
        if (activityIDs == null) {
			activityIDs = new HashSet<Long>();
		}
        return activityIDs;
    }

    /**
     * Set Activity IDs
     * <p>Get a set of Activity IDs
     *
     * @param activityIDs the activityIDs to set
     */
    public void setActivityIDs(Set<Long> activityIDs) {
        this.activityIDs = activityIDs;
    }

    /**
     * Get Capacity Taken
     * <p>Number of activities that are booked to a scheduled timeslot.
     *
     * @return Long required. capacityTaken
     */
    public Long getCapacityTaken() {
        return capacityTaken;
    }

    /**
     * Set Capacity Taken
     * <p>Number of activities that are booked to a scheduled timeslot.
     *
     * @param capacityTaken Long required. capacityTaken
     *                      the capacityTaken to set
     */
    public void setCapacityTaken(Long capacityTaken) {
        this.capacityTaken = capacityTaken;
    }

    /**
     * Get Capacity Remaining
     * <p>Number of activities that can be booked to a scheduled timeslot.
     *
     * @return Long required. capacityRemaining
     */
    public Long getCapacityRemaining() {
        return capacityRemaining;
    }

    /**
     * Set Capacity Remaining
     * <p>Number of activities that can be booked to a scheduled timeslot.
     *
     * @param capacityRemaining Long required. capacityRemaining
     *                          the capacityRemaining to set
     */
    public void setCapacityRemaining(Long capacityRemaining) {
        this.capacityRemaining = capacityRemaining;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((activityIDs == null) ? 0 : activityIDs.hashCode());
        result = prime * result + ((capacityRemaining == null) ? 0 : capacityRemaining.hashCode());
        result = prime * result + ((capacityTaken == null) ? 0 : capacityTaken.hashCode());
        result = prime * result + ((scheduleTimeslot == null) ? 0 : scheduleTimeslot.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        TimeslotCapacityType other = (TimeslotCapacityType) obj;
        if (activityIDs == null) {
            if (other.activityIDs != null) {
				return false;
			}
        } else if (!activityIDs.equals(other.activityIDs)) {
			return false;
		}
        if (capacityRemaining == null) {
            if (other.capacityRemaining != null) {
				return false;
			}
        } else if (!capacityRemaining.equals(other.capacityRemaining)) {
			return false;
		}
        if (capacityTaken == null) {
            if (other.capacityTaken != null) {
				return false;
			}
        } else if (!capacityTaken.equals(other.capacityTaken)) {
			return false;
		}
        if (scheduleTimeslot == null) {
            if (other.scheduleTimeslot != null) {
				return false;
			}
        } else if (!scheduleTimeslot.equals(other.scheduleTimeslot)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TimeslotCapacityType [scheduleTimeslot=" + scheduleTimeslot + ", activityIDs=" + activityIDs + ", capacityTaken=" + capacityTaken + ", capacityRemaining="
                + capacityRemaining + "]";
    }

}
