package syscon.arbutus.product.services.activity.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * DailyRecurrencePatternEntity for Activity Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment ACT_SchRPatn 'Activity Recurrence Pattern for Activity Service'
 * @DbComment ACT_SchRPatn.DTYPE 'Discrimination Type'
 * @DbComment ACT_SchRPatn_ACT_SchWk 'Recurrence Pattern and WeeklyDay join table for Activity Service'
 * @DbComment ACT_SchRPatn_ACT_SchWk.ACTRPATNID 'Id of Activity Recurrence Pattern table'
 * @DbComment ACT_SchRPatn_ACT_SchWk.ACTSCHWKID 'Id points to Activity Schedule table'
 * @DbComment ACT_SchRPatn_ACT_SchDay 'Recurrence Pattern and Day join table for Activity Service'
 * @DbComment ACT_SchRPatn_ACT_SchDay.ACTRPATNID 'Id of Activity Recurrence Pattern table'
 * @DbComment ACT_SchRPatn_ACT_SchDay.ACTSCHDAYID 'Id points to Activity Schedule table'
 * @since September 18, 2012
 */
@Audited
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "ACT_SchRPatn")
public class RecurrencePatternEntity implements Serializable {

    private static final long serialVersionUID = -8968455385179465880L;

    /**
     * @DbComment ACT_SchRPatn.actRPatnId 'RecurrencePattern Id'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_ACT_SCHRPATN_ID")
    @SequenceGenerator(name = "SEQ_ACT_SCHRPATN_ID", sequenceName = "SEQ_ACT_SCHRPATN_ID", allocationSize = 1)
    @Column(name = "actRPatnId")
    private Long id;

    /**
     * @DbComment ACT_SchRPatn.recurInterval 'Recurrence Interval'
     */
    @Column(name = "recurInterval", nullable = false)
    private Long recurrenceInterval;

    @OneToMany(mappedBy = "recurrencePattern", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @BatchSize(size = 20)
    private Set<IncludeDateEntity> includeDates;

    @OneToMany(mappedBy = "recurrencePattern", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @BatchSize(size = 20)
    private Set<ExcludeDateEntity> excludeDates;

    /**
     * @DbComment ACT_SchRPatn.version 'Version for Hibernate use'
     */
    // @Version
    @Column(name = "version")
    @NotAudited
    private Long version;

    /**
     * @DbComment ACT_SchRPatn.createUserId 'Create User Id'
     * @DbComment ACT_SchRPatn.createDateTime 'Create Date Time'
     * @DbComment ACT_SchRPatn.modifyUserId 'Modify User Id'
     * @DbComment ACT_SchRPatn.modifyDateTime 'Modify Date Time'
     * @DbComment ACT_SchRPatn.invocationContext 'Invocation Context'
     */
    @Embedded
    @NotAudited
    private StampEntity stamp;

    public RecurrencePatternEntity() {
        super();
    }

    public RecurrencePatternEntity(RecurrencePatternEntity recurrencePattern) {
        super();
        this.id = recurrencePattern.getId();
        this.recurrenceInterval = recurrencePattern.getRecurrenceInterval();
    }

    /**
     * @param id
     * @param recurrenceInterval
     */
    public RecurrencePatternEntity(Long id, Long recurrenceInterval) {
        super();
        this.id = id;
        this.recurrenceInterval = recurrenceInterval;
    }

    /**
     * isWellFormed -- To validate the value of the properties of the type.
     *
     * @return Boolean
     */
    public Boolean isWellFormed() {
        if (recurrenceInterval == null) {
			return Boolean.FALSE;
		}
        return Boolean.TRUE;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the recurrenceInterval
     */
    public Long getRecurrenceInterval() {
        return recurrenceInterval;
    }

    /**
     * @param recurrenceInterval the recurrenceInterval to set
     */
    public void setRecurrenceInterval(Long recurrenceInterval) {
        this.recurrenceInterval = recurrenceInterval;
    }

    public Set<IncludeDateEntity> getIncludeDates() {
        if (includeDates == null) {
			includeDates = new HashSet<>();
		}
        return includeDates;
    }

    public void setIncludeDates(Set<IncludeDateEntity> includeDates) {
        this.includeDates = includeDates;
    }

    public void addIncludeDate(IncludeDateEntity includeDate) {
        includeDate.setRecurrencePattern(this);
        getIncludeDates().add(includeDate);
    }

    public Set<ExcludeDateEntity> getExcludeDates() {
        if (excludeDates == null) {
			excludeDates = new HashSet<>();
		}
        return excludeDates;
    }

    public void setExcludeDates(Set<ExcludeDateEntity> excludeDates) {
        this.excludeDates = excludeDates;
    }

    public void addExcludeDate(ExcludeDateEntity excludeDate) {
        excludeDate.setRecurrencePattern(this);
        getExcludeDates().add(excludeDate);
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((recurrenceInterval == null) ? 0 : recurrenceInterval.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        RecurrencePatternEntity other = (RecurrencePatternEntity) obj;
        if (recurrenceInterval == null) {
            if (other.recurrenceInterval != null) {
				return false;
			}
        } else if (!recurrenceInterval.equals(other.recurrenceInterval)) {
			return false;
		}
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "RecurrencePatternEntity [id=" + id + ", recurrenceInterval=" + recurrenceInterval + ", version=" + version + ", stamp=" + stamp + "]";
    }

}
