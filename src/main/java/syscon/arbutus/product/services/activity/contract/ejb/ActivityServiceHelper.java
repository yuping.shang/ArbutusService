package syscon.arbutus.product.services.activity.contract.ejb;

import java.text.SimpleDateFormat;
import java.util.*;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import syscon.arbutus.product.services.activity.contract.dto.*;
import syscon.arbutus.product.services.activity.contract.util.RecurrenceObjectFactory;
import syscon.arbutus.product.services.activity.realization.persistence.*;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.util.DateUtil;
import syscon.arbutus.product.services.realization.model.Associable;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;

/**
 * Created by ian on 9/22/15.
 */
public class ActivityServiceHelper {

    /**
     * Convert to ScheduleType from ScheduleEntity
     *
     * @param from ScheduleEntity
     * @return ScheduleType
     */
    public static Schedule toSchedule(ScheduleEntity from) {
        // null is just null
        if (from == null) {
            return null;
        }

        Schedule to = new Schedule();
        to.setScheduleIdentification(from.getScheduleIdentification());
        to.setScheduleCategory(from.getScheduleCategory());
        to.setFacilityScheduleReason(from.getFacilityScheduleReason());
        to.setEffectiveDate(BeanHelper.getDateWithoutTime(from.getEffectiveDate()));
        to.setTerminationDate(BeanHelper.getDateWithoutTime(from.getTerminationDate()));
        to.setScheduleStartTime(getTimeOnly(from.getScheduleStartTime()));
        to.setScheduleEndTime(getTimeOnly(from.getScheduleEndTime()));
        to.setCapacity(from.getCapacity());
        to.setFacilityId(from.getFacilityId());
        to.setForInternalLocationId(from.getForInternalLocId());
        to.setAtInternalLocationIds(toLongSet(from.getAtInternalLocIds()));
        to.setLocationIds(toLongSet(from.getLocationIds()));
        to.setOrganizationIds(toLongSet(from.getOrganizationIds()));
        to.setRecurrencePattern(toRecurrencePattern(from.getRecurrencePattern()));

        return to;
    }

    public static  String getTimeOnly(Date date) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return sdf.format(date);
    }

    public static SimpleSchedule toSimpleSchedule(ScheduleEntity from) {
        Schedule to = toSchedule(from);
        return new SimpleSchedule(to, new RecurrenceObjectFactory().generate(to.getRecurrencePattern()));
    }








    /**
     * Convert to a set of ScheduleType from a set of ScheduleEntity
     *
     * @param from Set&lt;ScheduleEntity>
     * @return Set&lt;ScheduleType>
     */
    public static  Set<Schedule> toSchedules(Set<ScheduleEntity> from) {
        // null is just null
        if (from == null) {
            return null;
        }

        // Loop and Convert
        Set<Schedule> to = new HashSet<Schedule>();
        for (ScheduleEntity f : from) {
            to.add(toSchedule(f));
        }
        return to;
    }

    public static  List<Schedule> toSchedulesList(List<ScheduleEntity> from) {
        if (from == null) {
            return null;
        }

        List<Schedule> to = new ArrayList<Schedule>();
        for (ScheduleEntity f : from) {
            to.add(toSchedule(f));
        }
        return to;
    }

    public static List<SimpleSchedule> toSimpleSchedulesList(List<ScheduleEntity> from) {
        if (from == null) {
            return null;
        }

        List<SimpleSchedule> to = new ArrayList<>();
        for (ScheduleEntity f : from) {
            to.add(toSimpleSchedule(f));
        }
        return to;
    }

    /**
     * Convert to RecurrencePatternType from RecurrencePatternEntity
     *
     * @param from RecurrencePatternEntity
     * @return RecurrencePatternType
     */
    public static RecurrencePatternType toRecurrencePattern(RecurrencePatternEntity from) {
        // null is just null
        if (from == null) {
            return null;
        }
        // Convert
        RecurrencePatternType to = null;
        if (from instanceof OnceRecurrencePatternEntity) {
            to = new OnceRecurrencePatternType(((OnceRecurrencePatternEntity) from).getOccurrenceDate());
        } else if (from instanceof DailyRecurrencePatternEntity) {
            to = new DailyWeeklyCustomRecurrencePatternType(((DailyRecurrencePatternEntity) from).getRecurrenceInterval(), ((DailyRecurrencePatternEntity) from).getWeekdayOnlyFlag(),
                    toDays(((DailyRecurrencePatternEntity) from).getRecurrOnDays()));
        } else if (from instanceof WeeklyRecurrencePatternEntity) {
            to = new WeeklyRecurrencePatternType(from.getRecurrenceInterval(), toWeekdays(((WeeklyRecurrencePatternEntity) from).getRecurOnWeekdays()));

        } else if (from instanceof MonthlyRecurrencePatternEntity) {
            to = new MonthlyRecurrencePatternType(((MonthlyRecurrencePatternEntity) from).getRecurrenceInterval(),
                    ((MonthlyRecurrencePatternEntity) from).getDayOfMonth(), ((MonthlyRecurrencePatternEntity) from).getRecurOnWeekday(),
                    ((MonthlyRecurrencePatternEntity) from).getNthWeekday());
        } else if (from instanceof YearlyRecurrencePatternEntity) {
            to = new YearlyRecurrencePatternType(((YearlyRecurrencePatternEntity) from).getRecurrenceInterval(), ((YearlyRecurrencePatternEntity) from).getRecurOnDay(),
                    ((YearlyRecurrencePatternEntity) from).getRecurOnMonth(), ((YearlyRecurrencePatternEntity) from).getRecurOnWeekday(),
                    ((YearlyRecurrencePatternEntity) from).getNthWeekday());
        }
        // to be implemented
        if (!BeanHelper.isEmpty(from.getIncludeDates())) {
            Set<ExDateType> inDates = new HashSet<>();
            for (IncludeDateEntity frm : from.getIncludeDates()) {
                inDates.add(toExDateType(frm));
            }
            to.setIncludeDateTimes(inDates);
        }
        if (!BeanHelper.isEmpty(from.getExcludeDates())) {
            Set<ExDateType> exDates = new HashSet<>();
            for (ExcludeDateEntity frm : from.getExcludeDates()) {
                exDates.add(toExDateType(frm));
            }
            to.setExcludeDateTimes(exDates);
        }
        return to;
    }

    public static ExDateType toExDateType(IncludeDateEntity from) {
        if (from == null) {
            return null;
        }
        ExDateType to = new ExDateType();
        to.setDate(new LocalDate(from.getDate()));
        to.setStartTime(new LocalTime(from.getStartTime()));
        to.setEndTime(new LocalTime(from.getEndTime()));
        return to;
    }

    public static ExDateType toExDateType(ExcludeDateEntity from) {
        if (from == null) {
            return null;
        }
        ExDateType to = new ExDateType();
        to.setDate(new LocalDate(from.getDate()));
        to.setStartTime(new LocalTime(from.getStartTime()));
        to.setEndTime(new LocalTime(from.getEndTime()));
        return to;
    }

    /**
     * Convert to a set of Weekdays from a set of Weekday
     *
     * @param from Set&lt;Weekday>
     * @return Set&lt;Weekdays>
     */
    public static  Set<String> toWeekdays(Set<WeekdayEntity> from) {
        // null is just null
        if (from == null) {
            return null;
        }

        // Loop and Convert
        Set<String> to = new HashSet<String>();
        for (WeekdayEntity f : from) {
            to.add(toWeekdayFromEntity(f));
        }
        return to;
    }

    /**
     * Convert to Weekdays from WeekdayEntity
     *
     * @param from WeekdayEntity
     * @return Weekdays
     */
    public static  String toWeekdayFromEntity(WeekdayEntity from) {
        // null is just null
        if (from == null) {
            return null;
        }

        // Convert
        return from.getWeekday();

    }

    public static Set<String> toDays(Set<DayEntity> from) {
        if (from == null) {
            return null;
        }
        Set<String> to = new HashSet<>();
        for (DayEntity f : from) {
            to.add(toDayFromEntity(f));
        }
        return to;
    }

    public static  String toDayFromEntity(DayEntity from) {
        if (from == null) {
            return null;
        }
        return from.getDay();
    }

    /**
     * Convert to ScheduleEntity from ScheduleType
     *
     * @param from ScheduleType
     * @return ScheduleEntity
     */
    public static ScheduleEntity toScheduleEntity(Schedule from, StampEntity stamp) {
        // null is just null
        if (from == null) {
            return null;
        }

        ScheduleEntity to = new ScheduleEntity();
        to.setScheduleIdentification(from.getScheduleIdentification());
        to.setScheduleCategory(from.getScheduleCategory());
        to.setEffectiveDate(from.getEffectiveDate());
        to.setTerminationDate(from.getTerminationDate());
        to.setScheduleStartTime(DateUtil.getZeroEpochTime(from.getScheduleStartTime()));
        to.setScheduleEndTime(DateUtil.getZeroEpochTime(from.getScheduleEndTime()));
        to.setFacilityScheduleReason(from.getFacilityScheduleReason());
        to.setCapacity(from.getCapacity());
        to.setFacilityId(from.getFacilityId());
        to.setForInternalLocId(from.getForInternalLocationId());
        // to.setAtInternalLocIds(from.getAtInternalLocationIds());
        // to.setLocationIds(from.getLocationIds());
        // to.setOrganizationIds(from.getOrganizationIds());
        to.setAtInternalLocIds(
                toAssociableSet(AssociationToClass.FacilityInternalLocationService.name(), from.getAtInternalLocationIds(), ScheduleInternalLocationEntity.class, stamp));
        to.setLocationIds(toAssociableSet(AssociationToClass.LocationService.name(), from.getLocationIds(), ScheduleLocationEntity.class, stamp));
        to.setOrganizationIds(toAssociableSet(AssociationToClass.OrganizationService.name(), from.getOrganizationIds(), ScheduleOrganizationEntity.class, stamp));
        to.setRecurrencePattern(toRecurrencePatternEntity(from.getRecurrencePattern(), stamp));

        return to;
    }

    /**
     * Convert to RecurrencePatternEntity from RecurrencePatternType
     *
     * @param from RecurrencePatternType
     * @return RecurrencePatternEntity
     */
    public static RecurrencePatternEntity toRecurrencePatternEntity(RecurrencePatternType from, StampEntity stamp) {
        // null is just null
        if (from == null) {
            return null;
        }

        // Convert
        RecurrencePatternEntity to = null;
        if (from instanceof OnceRecurrencePatternType) {
            to = new OnceRecurrencePatternEntity(null, ((OnceRecurrencePatternType) from).getRecurrenceInterval(),
                    ((OnceRecurrencePatternType) from).getOccurrenceDate());

        } else if (from instanceof DailyWeeklyCustomRecurrencePatternType) {
            to = new DailyRecurrencePatternEntity(null, ((DailyWeeklyCustomRecurrencePatternType) from).getRecurrenceInterval(),
                    ((DailyWeeklyCustomRecurrencePatternType) from).isWeekdayOnlyFlag(), toDayEntities(((DailyWeeklyCustomRecurrencePatternType) from).getRecurOnDays()));
        } else if (from instanceof WeeklyRecurrencePatternType) {
            to = new WeeklyRecurrencePatternEntity(null, ((WeeklyRecurrencePatternType) from).getRecurrenceInterval(),
                    toWeekdayEntities(((WeeklyRecurrencePatternType) from).getRecurOnWeekday()));
        } else if (from instanceof MonthlyRecurrencePatternType) {
            to = new MonthlyRecurrencePatternEntity(null, ((MonthlyRecurrencePatternType) from).getRecurrenceInterval(),
                    ((MonthlyRecurrencePatternType) from).getDayOfMonth(), ((MonthlyRecurrencePatternType) from).getRecurOnWeekday(),
                    ((MonthlyRecurrencePatternType) from).getNthWeekday());
        } else if (from instanceof YearlyRecurrencePatternType) {
            to = new YearlyRecurrencePatternEntity(null, ((YearlyRecurrencePatternType) from).getRecurrenceInterval(),
                    ((YearlyRecurrencePatternType) from).getRecurOnDay(), ((YearlyRecurrencePatternType) from).getRecurOnMonth(),
                    ((YearlyRecurrencePatternType) from).getRecurOnWeekday(), ((YearlyRecurrencePatternType) from).getNthWeekday());
        }

        Set<ExDateType> inDates = from.getIncludeDateTimes();
        if (!BeanHelper.isEmpty(inDates)) {
            for (ExDateType inDate : inDates) {
                to.addIncludeDate(toIncludeDateEntity(inDate, stamp));
            }
        }
        Set<ExDateType> exDates = from.getExcludeDateTimes();
        if (!BeanHelper.isEmpty(exDates)) {
            for (ExDateType exDate : exDates) {
                to.addExcludeDate(toExcludeDateEntity(exDate, stamp));
            }
        }

        return to;
    }

    public static IncludeDateEntity toIncludeDateEntity(ExDateType from, StampEntity stamp) {
        if (from == null) {
            return null;
        }
        IncludeDateEntity to = new IncludeDateEntity();
        to.setDate(from.getDate().toDateTimeAtStartOfDay().toDate());

        Calendar start = Calendar.getInstance();
        start.clear();
        start.set(Calendar.YEAR, 1970);
        start.set(Calendar.MONTH, Calendar.JANUARY);
        start.set(Calendar.DAY_OF_MONTH, 1);
        if (from.getStartTime() != null) {
            start.set(Calendar.HOUR_OF_DAY, from.getStartTime().getHourOfDay());
            start.set(Calendar.MINUTE, from.getStartTime().getMinuteOfHour());
            start.set(Calendar.SECOND, from.getStartTime().getSecondOfMinute());
            start.set(Calendar.MILLISECOND, 0);
        }
        to.setStartTime(start.getTime());

        Calendar end = Calendar.getInstance();
        end.clear();
        end.set(Calendar.YEAR, 1970);
        end.set(Calendar.MONTH, Calendar.JANUARY);
        end.set(Calendar.DAY_OF_MONTH, 1);
        if (from.getEndTime() != null) {
            end.set(Calendar.HOUR_OF_DAY, from.getEndTime().getHourOfDay());
            end.set(Calendar.MINUTE, from.getEndTime().getMinuteOfHour());
            end.set(Calendar.SECOND, from.getEndTime().getSecondOfMinute());
            end.set(Calendar.MILLISECOND, 0);
        }
        to.setEndTime(end.getTime());

        to.setStamp(stamp);

        return to;
    }

    public static ExcludeDateEntity toExcludeDateEntity(ExDateType from, StampEntity stamp) {
        if (from == null) {
            return null;
        }
        ExcludeDateEntity to = new ExcludeDateEntity();
        to.setDate(from.getDate().toDateTimeAtStartOfDay().toDate());

        Calendar start = Calendar.getInstance();
        start.clear();
        start.set(Calendar.YEAR, 1970);
        start.set(Calendar.MONTH, Calendar.JANUARY);
        start.set(Calendar.DAY_OF_MONTH, 1);
        if (from.getStartTime() != null) {
            start.set(Calendar.HOUR_OF_DAY, from.getStartTime().getHourOfDay());
            start.set(Calendar.MINUTE, from.getStartTime().getMinuteOfHour());
            start.set(Calendar.SECOND, from.getStartTime().getSecondOfMinute());
            start.set(Calendar.MILLISECOND, 0);
        }
        to.setStartTime(start.getTime());

        Calendar end = Calendar.getInstance();
        end.clear();
        end.set(Calendar.YEAR, 1970);
        end.set(Calendar.MONTH, Calendar.JANUARY);
        end.set(Calendar.DAY_OF_MONTH, 1);
        if (from.getEndTime() != null) {
            end.set(Calendar.HOUR_OF_DAY, from.getEndTime().getHourOfDay());
            end.set(Calendar.MINUTE, from.getEndTime().getMinuteOfHour());
            end.set(Calendar.SECOND, from.getEndTime().getSecondOfMinute());
            end.set(Calendar.MILLISECOND, 0);
        }
        to.setEndTime(end.getTime());

        to.setStamp(stamp);

        return to;
    }

    /**
     * Convert to WeekdayEntity from Weekdays
     *
     * @param from Weekdays
     * @return WeekdayEntity
     */
    public static WeekdayEntity toWeekdayEntity(String from) {
        // null is just null
        if (from == null) {
            return null;
        }

        return new WeekdayEntity(null, from, null, null);
    }

    /**
     * Convert to a set of WeekdayEntity from a set of Weekdays
     *
     * @param from Set&lt;Weekdays>
     * @return Set&lt;WeekdayEntity>
     */
    public static Set<WeekdayEntity> toWeekdayEntities(Set<String> from) {
        // null is just null
        if (from == null) {
            return null;
        }

        // Loop and Convert
        Set<WeekdayEntity> to = new HashSet<WeekdayEntity>();
        for (String f : from) {
            to.add(toWeekdayEntity(f));
        }
        return to;
    }

    public static Set<DayEntity> toDayEntities(Set<String> from) {
        if (from == null) {
            return null;
        }
        Set<DayEntity> to = new HashSet<>();
        for (String f : from) {
            to.add(toDayEntity(f));
        }
        return to;
    }

    public static DayEntity toDayEntity(String from) {
        if (from == null) {
            return null;
        }
        DayEntity to = new DayEntity();
        to.setDay(from);
        return to;
    }

    /**
     * Convert to ScheduleConfigVariableEntity from ScheduleConfigVariableType
     *
     * @param from ScheduleConfigVariableType
     * @return ScheduleConfigVariableEntity
     */
    public static ScheduleConfigVariableEntity toScheduleConfigVariableEntity(ScheduleConfigVariableType from) {
        // null is just null
        if (from == null) {
            return null;
        }

        ScheduleConfigVariableEntity to = new ScheduleConfigVariableEntity();
        to.setScheduleConfigIdentification(from.getScheduleConfigIdentification());
        to.setScheduleCategory(from.getScheduleCategory());
        to.setNumOfNextAvailableTimeslots(from.getNumOfNextAvailableTimeslots());

        return to;
    }

    /**
     * Convert to ActivityType from ActivityEntity
     *
     * @param from ActivityEntity
     * @return ActivityType
     */
    public static ActivityType toActivity(ActivityEntity from) {
        // null is just null
        if (from == null) {
            return null;
        }

        ActivityType to = new ActivityType();
        to.setActivityIdentification(from.getActivityId());
        to.setActivityCategory(from.getActivityCategory());
        to.setActiveStatusFlag(from.getActiveStatusFlag());
        to.setSupervisionId(from.getSupervisionId());
        to.setScheduleID(from.getScheduleID());
        to.setPlannedStartDate(from.getPlannedStartDate());
        to.setPlannedEndDate(from.getPlannedEndDate());
        to.setActivityAntecedentId(from.getAntecedentId());
        Set<Long> activityIds = new HashSet<Long>();
        if (from.getDescendantIds() != null) {
            for (Long id : from.getDescendantIds()) {
                activityIds.add(id);
            }
            to.setActivityDescendantIds(activityIds);
        }

        return to;
    }

    /**
     * Convert to ActivityEntity from ActivityType
     *
     * @param from
     * @return
     */
    public static ActivityEntity toActivityEntity(ActivityType from) {
        // null is just null
        if (from == null) {
            return null;
        }

        ActivityEntity to = new ActivityEntity();
        to.setActivityId(from.getActivityIdentification());
        to.setSupervisionId(from.getSupervisionId());
        to.setActivityCategory(from.getActivityCategory());
        to.setActiveStatusFlag(from.getActiveStatusFlag());
        to.setAntecedentId(from.getActivityAntecedentId());
        to.setDescendantIds(from.getActivityDescendantIds());
        to.setPlannedStartDate(from.getPlannedStartDate());
        to.setPlannedEndDate(from.getPlannedEndDate());
        to.setScheduleID(from.getScheduleID());

        return to;
    }

    public static  Set<ActivityType> toActivities(Set<ActivityEntity> from) {
        if (from == null) {
            return null;
        }

        Set<ActivityType> to = new HashSet<ActivityType>();
        for (ActivityEntity f : from) {
            to.add(toActivity(f));
        }

        return to;
    }

    public static  List<ActivityType> toActivitiesList(List<ActivityEntity> from) {
        if (from == null) {
            return null;
        }

        List<ActivityType> to = new ArrayList<ActivityType>();
        for (ActivityEntity f : from) {
            to.add(toActivity(f));
        }

        return to;
    }



    /**
     * Convert to ScheduleConfigVariableType from ScheduleConfigVariableEntity
     *
     * @param from ScheduleConfigVariableEnrity
     * @return ScheduleConfigVariableType
     */
    public static  ScheduleConfigVariableType toScheduleConfigVariable(ScheduleConfigVariableEntity from) {
        // null is just null
        if (from == null) {
            return null;
        }

        ScheduleConfigVariableType to = new ScheduleConfigVariableType();
        to.setScheduleConfigIdentification(from.getScheduleConfigIdentification());
        to.setScheduleCategory(from.getScheduleCategory());
        to.setNumOfNextAvailableTimeslots(from.getNumOfNextAvailableTimeslots());

        return to;
    }

    /**
     * Convert to a set of ScheduleConfigVariableType from a set of ScheduleConfigVariableEntity
     *
     * @param from Set&lt;ScheduleConfigVariableEntity>
     * @return Set&lt;ScheduleConfigVariableType>
     */
    public static  Set<ScheduleConfigVariableType> toScheduleConfigVariables(Set<ScheduleConfigVariableEntity> from) {
        // null is just null
        if (from == null) {
            return null;
        }
        // Loop and Convert
        Set<ScheduleConfigVariableType> to = new HashSet<ScheduleConfigVariableType>();
        for (ScheduleConfigVariableEntity f : from) {
            to.add(toScheduleConfigVariable(f));
        }
        return to;
    }

    public static  List<ScheduleConfigVariableType> toScheduleConfigVariableLists(List<ScheduleConfigVariableEntity> from) {
        // null is just null
        if (from == null) {
            return null;
        }
        // Loop and Convert
        List<ScheduleConfigVariableType> to = new ArrayList<ScheduleConfigVariableType>();
        for (ScheduleConfigVariableEntity f : from) {
            to.add(toScheduleConfigVariable(f));
        }
        return to;
    }

    public static  <T extends Associable> Set<T> toAssociableSet(String toClass, Set<Long> toIdentifiers, Class<T> clazz, StampEntity stamp) {
        if (toIdentifiers == null) {
            return null;
        }

        Set<T> to = new HashSet<T>();
        for (Long toIdentifier : toIdentifiers) {
            to.add(toAssociable(toClass, toIdentifier, stamp, clazz));
        }
        return to;
    }



    public static  <T extends Associable> T toAssociable(String toClass, Long toIdentifier, StampEntity stamp, Class<T> clazz) {
        if (toIdentifier == null) {
            return null;
        }

        T to = null;
        try {
            to = clazz.newInstance();
            to.setToClass(toClass);
            to.setToIdentifier(toIdentifier);
            to.setStamp(stamp);
        } catch (InstantiationException e) {
            throw new ArbutusRuntimeException(e.getMessage());
        } catch (IllegalAccessException e) {
            throw new ArbutusRuntimeException(e.getMessage());
        }

        return to;
    }

    public static  <T extends Associable> Set<Long> toLongSet(Set<T> from) {
        if (from == null) {
            return null;
        }
        Set<Long> to = new HashSet<Long>();
        for (Associable assoc : from) {
            to.add(assoc.getToIdentifier());
        }
        return to;
    }

    public static enum AssociationToClass {
        FacilityInternalLocationService, LocationService, OrganizationService;

        public static AssociationToClass fromValue(String v) {
            return valueOf(v);
        }

        public String value() {
            return name();
        }
    }

}
