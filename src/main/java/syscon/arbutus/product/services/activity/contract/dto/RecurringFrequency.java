package syscon.arbutus.product.services.activity.contract.dto;

/**
 * <p>Java class for RecurringFrequency.
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="RecurringFrequency">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DAILY"/>
 *     &lt;enumeration value="WEEKLY"/>
 *     &lt;enumeration value="MONTHLY"/>
 *     &lt;enumeration value="YEARLY"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 *
 * @author yshang
 * @version 1.0 (Based on SDD 1.0)
 * @since April 27, 2012
 */
public enum RecurringFrequency {

    /**
     * Daily Recurring
     */
    DAILY,
    /**
     * Weekly Recurring
     */
    WEEKLY,
    /**
     * Monthly Recurring
     */
    MONTHLY,
    /**
     * Yearly Recurring
     */
    YEARLY,
    /**
     * Once Recurring
     */
    ONCE;

    public static RecurringFrequency fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
