package syscon.arbutus.product.services.activity.contract.dto;

import javax.validation.constraints.NotNull;

/**
 * Java class for YearlyRecurrencePatternType
 *
 * @author yshang
 * @version 1.0 (Based on SDD 1.0)
 * @since April 27, 2012
 */
public class YearlyRecurrencePatternType extends RecurrencePatternType {

    private static final long serialVersionUID = 6007987067886801987L;

    private Long recurOnDay;
    @NotNull
    private String recurOnMonth; // ReferenceSet(MONTH)
    private String recurOnWeekday; // ReferenceSet(WEEKDAY)
    private String nthWeekday; // ReferenceSet(NTHWEEKDAY)

    /**
     * Constructor
     */
    public YearlyRecurrencePatternType() {
        super();
    }

    /**
     * Constructor
     * <ul>
     * <li>recurrenceInterval -- Define the number of year before the schedule recurs. E.g RecurringInterval = 3, schedule recurs every 3 years. Default value is 1 year.</li>
     * </ul>
     *
     * @param recurrenceInterval Required.
     */
    public YearlyRecurrencePatternType(Long recurrenceInterval) {
        super(recurrenceInterval);
    }

    /**
     * Constructor
     *
     * @param recurringFrequency required.
     */
    public YearlyRecurrencePatternType(RecurrencePatternType recurringFrequency) {
        super(recurringFrequency);
    }

    /**
     * Constructor
     * <ul>
     * <li>recurOnDay -- Define on which day the schedule recurs.  One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated. If RecurOnDay is populated, RecurOnWeekday, NthWeekday cannot be populated, RecurOnMonth is required.E.g RecurringDate = 9, RecurOnMonth =  March, RecurringInterval = 1 year, schedule recurs every year on March 9.</li>
     * <li>recurOnMonth -- Define on which month of the year the schedule recurs. One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated.</li>
     * <li>recurOnWeekday -- Define on which weekday(s) the schedule recurs.  One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated. If RecurOnWeekday is populated, NthWeeday is required, RecurOnMonth is required, RecurOnDay cannot be populated. E.g Monday, Wednesday, Friday.</li>
     * <li>nthWeekday -- The nth weekday that the schedule recurs. One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated. If NthWeekday is populated, RecurOnWeekday is required, RecurOnMonth is required, RecurOnDay cannot be populated.</li>
     * </ul>
     *
     * @param recurOnDay     Optional
     * @param recurOnMonth   Required. Reference code of MONTH set.
     * @param recurOnWeekday Optional. Reference code of WEEKDAY set.
     * @param nthWeekday     Optional. Reference code of NTHWEEKDAY set.
     */
    public YearlyRecurrencePatternType(Long recurOnDay, @NotNull String recurOnMonth, String recurOnWeekday, String nthWeekday) {
        super();
        this.recurOnDay = recurOnDay;
        this.recurOnMonth = recurOnMonth;
        this.recurOnWeekday = recurOnWeekday;
        this.nthWeekday = nthWeekday;
    }

    /**
     * Constructor
     * <ul>
     * <li>recurrenceInterval -- Define the number of year before the schedule recurs. E.g RecurringInterval = 3, schedule recurs every 3 years. Default value is 1 year.</li>
     * <li>recurOnDay -- Define on which day the schedule recurs.  One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated. If RecurOnDay is populated, RecurOnWeekday, NthWeekday cannot be populated, RecurOnMonth is required.E.g RecurringDate = 9, RecurOnMonth =  March, RecurringInterval = 1 year, schedule recurs every year on March 9.</li>
     * <li>recurOnMonth -- Define on which month of the year the schedule recurs. One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated.</li>
     * <li>recurOnWeekday -- Define on which weekday(s) the schedule recurs.  One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated. If RecurOnWeekday is populated, NthWeeday is required, RecurOnMonth is required, RecurOnDay cannot be populated. E.g Monday, Wednesday, Friday.</li>
     * <li>nthWeekday -- The nth weekday that the schedule recurs. One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated. If NthWeekday is populated, RecurOnWeekday is required, RecurOnMonth is required, RecurOnDay cannot be populated.</li>
     * </ul>
     *
     * @param recurrenceInterval required.
     * @param recurOnDay         optional
     * @param recurOnMonth       optional. Reference code of MONTH set.
     * @param recurOnWeekday     optional. Reference code of WEEKDAY set.
     * @param nthWeekday         optional. Reference code of NTHWEEKDAY set.
     */
    public YearlyRecurrencePatternType(Long recurrenceInterval, Long recurOnDay, @NotNull String recurOnMonth, String recurOnWeekday, String nthWeekday) {
        super(recurrenceInterval);
        this.recurOnDay = recurOnDay;
        this.recurOnMonth = recurOnMonth;
        this.recurOnWeekday = recurOnWeekday;
        this.nthWeekday = nthWeekday;
    }

    /**
     * Get RecurOnDay
     * <p>Define on which day the schedule recurs.  One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated. If RecurOnDay is populated, RecurOnWeekday, NthWeekday cannot be populated, RecurOnMonth is required.E.g RecurringDate = 9, RecurOnMonth =  March, RecurringInterval = 1 year, schedule recurs every year on March 9.
     *
     * @return Long optional. the recurOnDay
     */
    public Long getRecurOnDay() {
        return recurOnDay;
    }

    /**
     * Set RecurOnDay
     * <p>Define on which day the schedule recurs.  One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated. If RecurOnDay is populated, RecurOnWeekday, NthWeekday cannot be populated, RecurOnMonth is required.E.g RecurringDate = 9, RecurOnMonth =  March, RecurringInterval = 1 year, schedule recurs every year on March 9.
     *
     * @param recurOnDay Long optional. recurOnDay the recurOnDay to set
     */
    public void setRecurOnDay(Long recurOnDay) {
        this.recurOnDay = recurOnDay;
    }

    /**
     * Get RecurOnMonth
     * <p>Reference code of MONTH set. Define on which month of the year the schedule recurs. One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated.
     *
     * @return String optional. the recurOnMonth
     */
    public String getRecurOnMonth() {
        return recurOnMonth;
    }

    /**
     * Set RecurOnMonth
     * <p>Reference code of MONTH set. Define on which month of the year the schedule recurs. One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated.
     *
     * @param recurOnMonth String optional. recurOnMonth the recurOnMonth to set
     */
    public void setRecurOnMonth(String recurOnMonth) {
        this.recurOnMonth = recurOnMonth;
    }

    /**
     * Get RecurOnWeekday
     * <p>Reference code of WEEKDAY set. Define on which weekday(s) the schedule recurs.  One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated. If RecurOnWeekday is populated, NthWeeday is required, RecurOnMonth is required, RecurOnDay cannot be populated. E.g Monday, Wednesday, Friday.
     *
     * @return String optional. the recurOnWeekday
     */
    public String getRecurOnWeekday() {
        return recurOnWeekday;
    }

    /**
     * Set RecurOnWeekday
     * <p>Reference code of WEEKDAY set. Define on which weekday(s) the schedule recurs.  One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated. If RecurOnWeekday is populated, NthWeeday is required, RecurOnMonth is required, RecurOnDay cannot be populated. E.g Monday, Wednesday, Friday.
     *
     * @param recurOnWeekday String optional. recurOnWeekday the recurOnWeekday to set
     */
    public void setRecurOnWeekday(String recurOnWeekday) {
        this.recurOnWeekday = recurOnWeekday;
    }

    /**
     * Get NthWeekday
     * <p>Reference code of NTHWEEKDAY set. The nth weekday that the schedule recurs. One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated. If NthWeekday is populated, RecurOnWeekday is required, RecurOnMonth is required, RecurOnDay cannot be populated.
     *
     * @return String optional. the nthWeekday
     */
    public String getNthWeekday() {
        return nthWeekday;
    }

    /**
     * Set NthWeekday
     * <p>Reference code of NTHWEEKDAY set. The nth weekday that the schedule recurs. One of RecurOnDay, RecurOnWeekday, and NthWeekday has to be populated. If NthWeekday is populated, RecurOnWeekday is required, RecurOnMonth is required, RecurOnDay cannot be populated.
     *
     * @param nthWeekday String optional. nthWeekday the nthWeekday to set
     */
    public void setNthWeekday(String nthWeekday) {
        this.nthWeekday = nthWeekday;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((nthWeekday == null) ? 0 : nthWeekday.hashCode());
        result = prime * result + ((recurOnDay == null) ? 0 : recurOnDay.hashCode());
        result = prime * result + ((recurOnMonth == null) ? 0 : recurOnMonth.hashCode());
        result = prime * result + ((recurOnWeekday == null) ? 0 : recurOnWeekday.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (!(obj instanceof YearlyRecurrencePatternType)) {
			return false;
		}
        YearlyRecurrencePatternType other = (YearlyRecurrencePatternType) obj;
        if (nthWeekday == null) {
            if (other.nthWeekday != null) {
				return false;
			}
        } else if (!nthWeekday.equals(other.nthWeekday)) {
			return false;
		}
        if (recurOnDay == null) {
            if (other.recurOnDay != null) {
				return false;
			}
        } else if (!recurOnDay.equals(other.recurOnDay)) {
			return false;
		}
        if (recurOnMonth == null) {
            if (other.recurOnMonth != null) {
				return false;
			}
        } else if (!recurOnMonth.equals(other.recurOnMonth)) {
			return false;
		}
        if (recurOnWeekday == null) {
            if (other.recurOnWeekday != null) {
				return false;
			}
        } else if (!recurOnWeekday.equals(other.recurOnWeekday)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "YearlyRecurrencePatternType [recurOnDay=" + recurOnDay + ", recurOnMonth=" + recurOnMonth + ", recurOnWeekday=" + recurOnWeekday + ", nthWeekday="
                + nthWeekday + "]";
    }

}
