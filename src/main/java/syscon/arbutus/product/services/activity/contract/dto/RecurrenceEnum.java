package syscon.arbutus.product.services.activity.contract.dto;

/**
 * Created by ian on 9/22/15.
 *  the ENUMs below are to be appended when needed!
 */
public enum RecurrenceEnum {
    DAILY,
    WEEKDAYS,
    CUSTOM
}
