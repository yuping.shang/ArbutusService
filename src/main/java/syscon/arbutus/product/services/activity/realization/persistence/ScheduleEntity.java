package syscon.arbutus.product.services.activity.realization.persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * ScheduleEntity for Activity Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment ACT_Sch 'Activity Schedule table for Activity Service'
 * @since September 18, 2012
 */
@Audited
@Entity
@Table(name = "ACT_Sch")
@SQLDelete(sql = "UPDATE ACT_Sch SET flag = 4 WHERE actSchId = ? and version = ?")
@Where(clause = "flag = 1")
public class ScheduleEntity implements Serializable {

    private static final long serialVersionUID = 1178140487794336312L;

    /**
     * @DbComment ACT_Sch.actSchId 'Schedule ID'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_ACT_SCH_ID")
    @SequenceGenerator(name = "SEQ_ACT_SCH_ID", sequenceName = "SEQ_ACT_SCH_ID", allocationSize = 1)
    @Column(name = "actSchId")
    private Long scheduleIdentification;

    /**
     * @DbComment ACT_Sch.category 'Schedule/Activity Category'
     */
    @Column(name = "category", nullable = false, length = 64)
    @MetaCode(set = MetaSet.ACTIVITY_CATEGORY)
    private String scheduleCategory;

    /**
     * @DbComment ACT_Sch.facilityId 'Id of a facility. Each schedule is defined for a particular facility.'
     */
    @Column(name = "facilityId", nullable = false)
    private Long facilityId;

    /**
     * @DbComment ACT_Sch.forInternalLocId 'Id of InternalLocation pointed to. Used for facility scheduling. Indicates for which internal location the activity is being scheduled. This internal location reference cannot be the same as AtInternalLocationReference.'
     */
    @Column(name = "forInternalLocId", nullable = true)
    private Long forInternalLocId;

    @ForeignKey(name = "Fk_ACT_Sch1")
    @OneToMany(mappedBy = "schedule", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @BatchSize(size = 20)
    private Set<ScheduleInternalLocationEntity> atInternalLocIds;

    @ForeignKey(name = "Fk_ACT_Sch2")
    @OneToMany(mappedBy = "schedule", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @BatchSize(size = 20)
    private Set<ScheduleLocationEntity> locationIds;

    @ForeignKey(name = "Fk_ACT_Sch3")
    @OneToMany(mappedBy = "schedule", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @BatchSize(size = 20)
    private Set<ScheduleOrganizationEntity> organizationIds;

    /**
     * @DbComment ACT_Sch.facilityScheduleReason 'Schedule/Facility Schedule Reason. Used by facility scheduling. Specifies the reason for the movement.'
     */
    @Column(name = "facilityScheduleReason", nullable = true, length = 64)
    @MetaCode(set = MetaSet.FACILITY_SCHEDULE_REASON)
    private String facilityScheduleReason;

    /**
     * @DbComment ACT_Sch.capacity 'Capacity of the Schedule. The capacity is the number activities is allowed to use a timeslot of this schedule.'
     */
    @Column(name = "capacity", nullable = true)
    private Long capacity;

    /**
     * @DbComment ACT_Sch.effectiveDate 'Effective Date. The date when the schedule becomes effective.'
     */
    @Column(name = "effectiveDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date effectiveDate;

    /**
     * @DbComment ACT_Sch.terminationDate 'Termination Date. The date when the schedule is no longer effective. If a schedule has recurrence pattern, the recurrence pattern ends on the TerminationDate.'
     */
    @Column(name = "terminationDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date terminationDate;

    /**
     * @DbComment ACT_Sch.startTime 'Schedule Start Time. The start time of a schedule. E.g 08:00.'
     */
    @Column(name = "startTime", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduleStartTime;

    /**
     * @DbComment ACT_Sch.endTime 'Schedule End Time. The end time of a schedule. E.g 16:00.'
     */
    @Column(name = "endTime", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduleEndTime;

    /**
     * @DbComment ACT_Sch_ACT_SCHRPATN. 'Schedule and Recurrence Pattern join table for Activity Service'
     * @DbComment ACT_Sch_ACT_SCHRPATN.ACTRPATNID 'Id of Activity Recurrence Pattern table'
     * @DbComment ACT_Sch_ACT_SCHRPATN.ACTSCHID 'Id of Activity Schedule table'
     */
    @ForeignKey(name = "Fk_ACT_SCHRPATN", inverseName = "Fk_ACT_Sch")
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
    @JoinTable(name = "ACT_SCH_ACT_SCHRPATN", joinColumns = { @JoinColumn(name = "actSchId") },
            inverseJoinColumns = { @JoinColumn(name = "actRPatnId") })
    private RecurrencePatternEntity actSchPtn;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * @DbComment ACT_Sch.createUserId 'Create User Id'
     * @DbComment ACT_Sch.createDateTime 'Create Date Time'
     * @DbComment ACT_Sch.modifyUserId 'Modify User Id'
     * @DbComment ACT_Sch.modifyDateTime 'Modify Date Time'
     * @DbComment ACT_Sch.invocationContext 'Invocation Context'
     */
    @Embedded
    @NotAudited
    private StampEntity stamp;

    /**
     * @DbComment ACT_Sch.version 'Version for Hibernate use'
     */
    @Version
    @NotAudited
    private Long version;

    public ScheduleEntity() {
        super();
    }

    public ScheduleEntity(ScheduleEntity schedule) {
        super();
        this.scheduleIdentification = schedule.getScheduleIdentification();
        this.scheduleCategory = schedule.getScheduleCategory();
        this.facilityId = schedule.getFacilityId();
        this.capacity = schedule.getCapacity();
        this.effectiveDate = schedule.getEffectiveDate();
        this.terminationDate = schedule.getTerminationDate();
        this.scheduleStartTime = schedule.getScheduleStartTime();
        this.scheduleEndTime = schedule.getScheduleEndTime();
        this.actSchPtn = schedule.getRecurrencePattern();
        this.stamp = schedule.getStamp();
        this.facilityScheduleReason = schedule.getFacilityScheduleReason();
        this.forInternalLocId = schedule.getForInternalLocId();
        this.atInternalLocIds = schedule.getAtInternalLocIds();
        this.locationIds = schedule.getLocationIds();
        this.organizationIds = schedule.getOrganizationIds();
    }

    public ScheduleEntity(Long scheduleIdentification, String scheduleCategory, Long facilityId, Long forInternalLocId,
            Set<ScheduleInternalLocationEntity> atInternalLocIds, Set<ScheduleLocationEntity> locationIds, Set<ScheduleOrganizationEntity> organizationIds, Long capacity,
            Date effectiveDate, Date terminationDate, Date scheduleStartTime, Date scheduleEndTime, RecurrencePatternEntity actSchPtn, String facilityScheduleReason) {
        super();
        this.scheduleIdentification = scheduleIdentification;
        this.scheduleCategory = scheduleCategory;
        this.facilityId = facilityId;
        this.capacity = capacity;
        this.effectiveDate = effectiveDate;
        this.terminationDate = terminationDate;
        this.scheduleStartTime = scheduleStartTime;
        this.scheduleEndTime = scheduleEndTime;
        this.actSchPtn = actSchPtn;
        this.facilityScheduleReason = facilityScheduleReason;
        this.forInternalLocId = forInternalLocId;
        this.atInternalLocIds = atInternalLocIds;
        this.locationIds = locationIds;
        this.organizationIds = organizationIds;
    }

    /**
     * @return the scheduleIdentification
     */
    public Long getScheduleIdentification() {
        return scheduleIdentification;
    }

    /**
     * @param scheduleIdentification the scheduleIdentification to set
     */
    public void setScheduleIdentification(Long scheduleIdentification) {
        this.scheduleIdentification = scheduleIdentification;
    }

    /**
     * @return the scheduleCategory
     */
    public String getScheduleCategory() {
        return scheduleCategory;
    }

    /**
     * @param scheduleCategory the scheduleCategory to set
     */
    public void setScheduleCategory(String scheduleCategory) {
        this.scheduleCategory = scheduleCategory;
    }

    /**
     * @return the facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * @param facilityId the facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * @return the forInternalLocId
     */
    public Long getForInternalLocId() {
        return forInternalLocId;
    }

    /**
     * @param forInternalLocId the forInternalLocId to set
     */
    public void setForInternalLocId(Long forInternalLocId) {
        this.forInternalLocId = forInternalLocId;
    }

    /**
     * @return the atInternalLocIds
     */
    public Set<ScheduleInternalLocationEntity> getAtInternalLocIds() {
        if (atInternalLocIds == null) {
			atInternalLocIds = new HashSet<ScheduleInternalLocationEntity>();
		}
        return atInternalLocIds;
    }

    /**
     * @param atInternalLocIds the atInternalLocIds to set
     */
    public void setAtInternalLocIds(Set<ScheduleInternalLocationEntity> atInternalLocIds) {
        getAtInternalLocIds().clear();
        if (atInternalLocIds != null) {
            for (ScheduleInternalLocationEntity entity : atInternalLocIds) {
                entity.setSchedule(this);
            }
            this.atInternalLocIds = atInternalLocIds;
        }
    }

    public void addAtInternalLocId(ScheduleInternalLocationEntity atInternalLocId) {
        atInternalLocId.setSchedule(this);
        getAtInternalLocIds().add(atInternalLocId);
    }

    /**
     * @return the locationIds
     */
    public Set<ScheduleLocationEntity> getLocationIds() {
        if (locationIds == null) {
			locationIds = new HashSet<ScheduleLocationEntity>();
		}
        return locationIds;
    }

    /**
     * @param locationIds the locationIds to set
     */
    public void setLocationIds(Set<ScheduleLocationEntity> locationIds) {
        getLocationIds().clear();
        if (locationIds != null) {
            for (ScheduleLocationEntity entity : locationIds) {
                entity.setSchedule(this);
            }
            this.locationIds = locationIds;
        }
    }

    public void addLocationId(ScheduleLocationEntity locationId) {
        locationId.setSchedule(this);
        getLocationIds().add(locationId);
    }

    /**
     * @return the organizationIds
     */
    public Set<ScheduleOrganizationEntity> getOrganizationIds() {
        if (organizationIds == null) {
			organizationIds = new HashSet<ScheduleOrganizationEntity>();
		}
        return organizationIds;
    }

    /**
     * @param organizationIds the organizationIds to set
     */
    public void setOrganizationIds(Set<ScheduleOrganizationEntity> organizationIds) {
        getOrganizationIds().clear();
        if (organizationIds != null) {
            for (ScheduleOrganizationEntity entity : organizationIds) {
                entity.setSchedule(this);
            }
            this.organizationIds = organizationIds;
        }
    }

    public void addOrganizationId(ScheduleOrganizationEntity organizationId) {
        organizationId.setSchedule(this);
        getOrganizationIds().add(organizationId);
    }

    /**
     * @return the facilityScheduleReason
     */
    public String getFacilityScheduleReason() {
        return facilityScheduleReason;
    }

    /**
     * @param facilityScheduleReason the facilityScheduleReason to set
     */
    public void setFacilityScheduleReason(String facilityScheduleReason) {
        this.facilityScheduleReason = facilityScheduleReason;
    }

    /**
     * @return the capacity
     */
    public Long getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    /**
     * @return the effectiveDate
     */
    public Date getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * @param effectiveDate the effectiveDate to set
     */
    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    /**
     * @return the terminationDate
     */
    public Date getTerminationDate() {
        return terminationDate;
    }

    /**
     * @param terminationDate the terminationDate to set
     */
    public void setTerminationDate(Date terminationDate) {
        this.terminationDate = terminationDate;
    }

    /**
     * @return the scheduleStartTime
     */
    public Date getScheduleStartTime() {
        return scheduleStartTime;
    }

    /**
     * @param scheduleStartTime the scheduleStartTime to set
     */
    public void setScheduleStartTime(Date scheduleStartTime) {
        this.scheduleStartTime = scheduleStartTime;
    }

    /**
     * @return the scheduleEndTime
     */
    public Date getScheduleEndTime() {
        return scheduleEndTime;
    }

    /**
     * @param scheduleEndTime the scheduleEndTime to set
     */
    public void setScheduleEndTime(Date scheduleEndTime) {
        this.scheduleEndTime = scheduleEndTime;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    public RecurrencePatternEntity getRecurrencePattern() {
        return actSchPtn;
    }

    /**
     * @param actSchPtn the actSchPtn to set
     */
    public void setRecurrencePattern(RecurrencePatternEntity actSchPtn) {
        this.actSchPtn = actSchPtn;
    }

    /**
     * @return the flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((actSchPtn == null) ? 0 : actSchPtn.hashCode());
        result = prime * result + ((capacity == null) ? 0 : capacity.hashCode());
        result = prime * result + ((effectiveDate == null) ? 0 : effectiveDate.hashCode());
        result = prime * result + ((scheduleCategory == null) ? 0 : scheduleCategory.hashCode());
        result = prime * result + ((scheduleEndTime == null) ? 0 : scheduleEndTime.hashCode());
        result = prime * result + ((scheduleIdentification == null) ? 0 : scheduleIdentification.hashCode());
        result = prime * result + ((scheduleStartTime == null) ? 0 : scheduleStartTime.hashCode());
        result = prime * result + ((terminationDate == null) ? 0 : terminationDate.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (!(obj instanceof ScheduleEntity)) {
			return false;
		}
        ScheduleEntity other = (ScheduleEntity) obj;
        if (actSchPtn == null) {
            if (other.actSchPtn != null) {
				return false;
			}
        } else if (!actSchPtn.equals(other.actSchPtn)) {
			return false;
		}
        if (capacity == null) {
            if (other.capacity != null) {
				return false;
			}
        } else if (!capacity.equals(other.capacity)) {
			return false;
		}
        if (effectiveDate == null) {
            if (other.effectiveDate != null) {
				return false;
			}
        } else if (!effectiveDate.equals(other.effectiveDate)) {
			return false;
		}
        if (scheduleCategory == null) {
            if (other.scheduleCategory != null) {
				return false;
			}
        } else if (!scheduleCategory.equals(other.scheduleCategory)) {
			return false;
		}
        if (scheduleEndTime == null) {
            if (other.scheduleEndTime != null) {
				return false;
			}
        } else if (!scheduleEndTime.equals(other.scheduleEndTime)) {
			return false;
		}
        if (scheduleIdentification == null) {
            if (other.scheduleIdentification != null) {
				return false;
			}
        } else if (!scheduleIdentification.equals(other.scheduleIdentification)) {
			return false;
		}
        if (scheduleStartTime == null) {
            if (other.scheduleStartTime != null) {
				return false;
			}
        } else if (!scheduleStartTime.equals(other.scheduleStartTime)) {
			return false;
		}
        if (terminationDate == null) {
            if (other.terminationDate != null) {
				return false;
			}
        } else if (!terminationDate.equals(other.terminationDate)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ScheduleEntity [scheduleIdentification=" + scheduleIdentification + ", scheduleCategory=" + scheduleCategory + ", facilityId=" + facilityId
                + ", forInternalLocId=" + forInternalLocId + ", atInternalLocIds=" + atInternalLocIds + ", locationIds=" + locationIds + ", organizationIds="
                + organizationIds + ", facilityScheduleReason=" + facilityScheduleReason + ", capacity=" + capacity + ", effectiveDate=" + effectiveDate
                + ", terminationDate=" + terminationDate + ", scheduleStartTime=" + scheduleStartTime + ", scheduleEndTime=" + scheduleEndTime + ", actSchPtn="
                + actSchPtn + ", stamp=" + stamp + ", version=" + version + "]";
    }

}
