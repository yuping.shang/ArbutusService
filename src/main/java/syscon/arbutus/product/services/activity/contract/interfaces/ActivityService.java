package syscon.arbutus.product.services.activity.contract.interfaces;

import java.util.Date;
import java.util.List;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.*;
import syscon.arbutus.product.services.core.contract.dto.StampType;

/**
 * Interface of Activity Service
 * The Activity service records system events. Activities may be scheduled to
 * occur in the future, in which case the date and time of the occurrence can be
 * pre-planned. Activities may also be unscheduled and recorded as they occur in
 * real time.
 * The Activity service maintains a close association with other activity-based
 * services (MOVEMENT ACTIVITY, CLASSIFICATION ACTIVITY, HOUSING BED MANAGEMENT
 * ACTIVITY, etc), each of which holds a complete record of a system event of a
 * particular type, including information unique to that type of activity. The
 * activity-based services are referred in this document as activity
 * augmentation service, although the relationship between the activity service
 * and the activity augmentation service are peers. The eventual outcome
 * (occurred, did not occur, re-scheduled, canceled, etc), and the actual date
 * and time of the event occurrence, are recorded against the particular type of
 * activities, therefore managed by the activity augmentation services. New
 * business entities are added to this structure through addition of a new
 * activity-based service built on a new activity type.
 * Activity service can reference itself in order to establish relationships
 * among a set of related events. Activities are related to one another by
 * acting as a preceding event or a succeeding event.
 * Scope:
 * <li>Record a scheduled event as an activity;</li>
 * <li>Record an unscheduled event as an activity;</li>
 * <li>Record and update the planned date and time of the activity;</li>
 * <li>Delete an activity (under controlled circumstances);</li>
 * <li>Associate an activity to related activities;</li>
 * <li>Search for an activity;</li>
 * <li>Record a predefined schedule for a type of activity;</li>
 * <li>Update a predefined schedule for a type of activity;</li>
 * <li>Delete a predefined schedule for a type of activity;</li>
 * <li>Search for an activity schedule.</li>
 * Constraints:
 * <li>All constraints for uniqueness in this service cannot be overridden by the user.</li>
 * <b>See</b> {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
 * for all error codes and descriptions.
 * <p><b>Service JNDI Lookup Name:</b> ActivityService
 * <p><b>Example of invocation of the service:</b>
 * <pre>
 * 	Properties p = new Properties();
 * 	properties.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
 * 	properties.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
 * 	properties.put("java.naming.provider.url", hostAddress);  // hostAddress -- specified accordingly
 * 	Context ctx = new InitialContext(p);
 * 	ActivityService service = (ActivityService)ctx.lookup("ActivityService");
 * 	SecurityClient client = SecurityClientFactory.getSecurityClient();
 * 	client.setSimple(userId, password);
 * 	client.login();
 * 	UserContext uc = new UserContext();
 * 	uc.setConsumingApplicationId(clientAppId);
 * 	uc.setFacilityId(facilityId);
 * 	uc.setDeviceId(deviceId);
 * 	uc.setProcess(process);
 * 	uc.setTask(task);
 * 	uc.setTimestamp(timestamp);
 * 	ActivityReturnType orgRet = service.create(uc, ...);
 * 	Long count = service.getCount(uc);
 * 	... ...
 * </pre>
 * <p>Activity service class diagram:
 * <br/><img src="doc-files/ActivityServiceUML.JPG" />
 *
 * @author yshang
 * @version 1.0 (based on SDD 1.0)
 * @since April 27, 2012
 */
public interface ActivityService {
    /**
     * Count of all Activities.
     * Returns the count of all Activities; throws HibernateException if failure.
     *
     * @param uc UserContext - Required
     * @return java.lang.Long the count of all Activities
     */
    public Long getCount(UserContext uc);

    /**
     * Retrieve an Activity by Activity id.
     * Returns an instance of Activity if success;
     * Throws InvalidInputException if incorrect id is provided as parameter otherwise.
     *
     * @param uc UserContext - Required
     * @param id java.lang.Long - Required
     * @return ActivityType Returns an instance of Activity
     */
    public ActivityType get(UserContext uc, Long id);

    /**
     * Retrieve a list of Activities by Schedule id
     *
     * <p>Returns a list of ActivityType if success
     * <li>Category is optional/li>
     * <li>if Category is set to "FACOUNT", the maxresult size will always be 1</li>
     * </p>
     * @param uc UserContext - Required
     * @param scheduleId java.lang.Long - Required
     * @param category String - Optional
     * @return ActivityType Returns an instance of Activity
     */
    public List<ActivityType> getFromScheduleId(UserContext uc, Long scheduleId, String category);

    /**
     * Get/Retrieve Activity Descendants, a set of Activities including the
     * direct and indirect descendants of the current Activity.
     * <p>Provenance
     * <li>All activities that are direct descendants of the current activity should be retrieved;</li>
     * <li>All activities that are indirect descendants of the current activity should be retrieved;</li>
     * <li>The current activity and its direct and indirect descendants form a tree structure. The activities at the leaf level of the tree structure do not have descendants.</li>
     *
     * @param uc       UserContext - Required
     * @param activity ActivityType - Required
     * @return Set&lt;ActivityType> - Returns a set of instances of Decendant ActivityType if success;
     * Returns 0 size of Set of instance of ActivityType if no Decendant exists.
     */
    public Set<ActivityType> getDecendants(UserContext uc, ActivityType activity);

    /**
     * Get/Retrieve Activity Antecedents, a set of Activities including the
     * direct and indirect antecedents of the current Activity.
     * <p>Provenance
     * <li>All activities that are direct antecedents of the current activity should be retrieved;</li>
     * <li>All activities that are indirect antecedents of the current activity should be retrieved;</li>
     * <li>The current activity and its direct and indirect antecedents form a tree structure. The activities at the leaf level of the tree structure do not have antecedents.</li>
     *
     * @param uc       UserContext - Required
     * @param activity ActivityType - Required
     * @return Set&lt;ActivityType> - Returns a set of instances of Antecedent ActivityType if success;
     * Returns 0 size of Set of instance of ActivityType if no Antecedent exists.
     */
    public Set<ActivityType> getAntecedents(UserContext uc, ActivityType activity);

    /**
     * Get/Retrieve Schedule
     *
     * @param uc UserContext - Required
     * @param id Long - Required
     * @return ScheduleType Returns an instance of ScheduleType.
     */
    public SimpleSchedule getSchedule(UserContext uc, Long id);

    /**
     * Get/Retrieve All Schedules
     *
     * @param uc                 UserContext - Required
     * @param scheduleActiveFlag Boolean
     *                           - Optional -- true: get all active schedules; false: get all inactive schedules; null: get all schedules (active and inactive).
     * @return Set&lt;ScheduleType> Returns a set of instances of ScheduleType if success.
     */
    public Set<Schedule> getAllSchedules(UserContext uc, Boolean scheduleActiveFlag);

    /**
     * Get/Retrieve Schedule Config Variable, the configuration variable for a
     * particular type of schedule.
     *
     * @param uc UserContext - Required
     * @param id Long - Required
     * @return ScheduleConfigVariableType
     */
    public ScheduleConfigVariableType getScheduleConfigVar(UserContext uc, Long id);

    /**
     * Get/Retrieve All Schedule Config Variables.
     *
     * @param uc UserContext - Required
     * @return Set&lt;ScheduleConfigVariableType>
     */
    public Set<ScheduleConfigVariableType> getAllScheduleConfigVars(UserContext uc);

    /**
     * Get/Retrieve Timeslot Capacity.
     * <p>
     * Check if the number of activities booked to a scheduled timeslot exceeds
     * the capacity of the schedule, and calculate the remaining capacity of the
     * timeslot.
     * </p>
     * Provenance
     * <li>Remaining Capacity =  Schedule Capacity – number of activities booked to the timeslot;</li>
     * <li>If Remaining Capacity = 0, the timeslot has reached its full capacity;</li>
     * <li>An activity is booked to a timeslot if,
     * <ol>1. Activity status is active;</ol>
     * <ol>2. Activity is associated to the schedule that is used to generate the timeslot;</ol>
     * <ol>3. Activity’s planned start date = the timeslot’s start date/time;</ol>
     * <ol>4. Activity’s planned end date = the timeslot’s end date/time.</ol>
     * </li>
     *
     * @param uc       UserContext - Required
     * @param timeslot ScheduleTimeslotType - Required
     * @return TimeslotCapacityType
     */
    public TimeslotCapacityType getTimeslotCapacity(UserContext uc, ScheduleTimeslotType timeslot);

    /**
     * Get/Retrieve Schedule Capacity.
     * <p>
     * Given a period of time, check if the number of activities booked to any
     * of the timeslot of a schedule exceeds the capacity of the schedule, and
     * calculate the remaining capacity of each timeslot.
     * </p>
     * Provenance
     * <li>All timeslots starts in between the LookupDate (inclusive) and the LookupEndDate (inclusive) are included in the schedule capacity check;</li>
     * <li>LookupDate &gt;= Schedule’s Effective Date;</li>
     * <li>LookupEndDate &lt;= Schedule’s Termination Date.</li>
     *
     * @param uc            UserContext - Required
     * @param scheduleId    Long - Required
     * @param lookupDate    java.util.Date - Required
     * @param lookupEndDate java.util.Date - Required
     * @return ScheduleCapacityType
     */
    public ScheduleCapacityType getScheduleCapacity(UserContext uc, Long scheduleId, Date lookupDate, Date lookupEndDate);

    /**
     * <p>
     * Create an Activity.
     * </p>
     * <p>
     * Returns a null and error code in return type if instance cannot be
     * created.
     * </p>
     * <p>If schedule timeslot is not null,
     * <li>the schedule timeslot’s start date/time and end date/time are copied to the activity’s planned start date and end date;</li>
     * <li>the ScheduleID of the timeslot is copied to the activity;</li>
     * <li>activity cannot be created if the timeslot has reached its capacity, unless OverrideCapacity = True.</li>
     * <p>If schedule timeslot is not provided,
     * <li>User must manually enter the activity’s planned start and end date;</li>
     * <li>the activity’s ScheduleID must be set to null.</li>
     *
     * @param uc               UserContext - Required
     * @param activity         ActivityType - Required
     * @param scheduleTimeslot ScheduleTimeslotType - Optional
     * @param overrideCapacity Boolean - Required
     * @return ActivityType
     */
    public ActivityType create(UserContext uc, ActivityType activity, ScheduleTimeslotType scheduleTimeslot, Boolean overrideCapacity);

    /**
     * Create Activities (a series of new Activities) after schedule change.
     * Provenance
     * <li>A new schedule has been created and is passed in as input;</li>
     * <li>A timetable is generated based on the schedule. The timetable contains a list of next available timeslots to which an activity can be booked. The timeslots start on the schedule’s EffectiveDate;</li>
     * <li>If LookupEndDate is provided,
     * <ol>the timetable must include all timeslots that have start date before the LookupEndDate;</ol>
     * </li>
     * <li>If NumOfNextAvailableTimeslot in the Schedule Config Variable is null, LookupEndDate is required;</li>
     * <li>If LookupEndDate is not provided,
     * <ol>Schedule Config Variable defined for the input schedule must be used;</ol>
     * <ol>the number of timeslots included in the timetable should be limited to the NumOfNextAvailableTimeslot value defined by the Schedule Config VariableType;</ol>
     * </li>
     * <li>A new activity is created for each timeslot contained in the timetable.</li>
     *
     * @param uc            UserContext - Required
     * @param scheduleId    Long - Required
     * @param lookupEndDate Date - Optional
     * @return Set&lt;ActivityType>
     */
    public Set<ActivityType> create(UserContext uc, Long scheduleId, Date lookupEndDate);

    /**
     * Create Schedule, a Schedule for a particular type of Activity.
     * Provenance
     * <li>A schedule is created;</li>
     * <li>A unique schedule identification number is created;</li>
     * <li>Association to a facility is required. Only associates to internal locations that belong to the selected facility are allowed;</li>
     * <li>Association can be made to only one of the location service, organization service or internal location service at a time.</li>
     *
     * @param uc       UserContext - Required
     * @param schedule ScheduleType - Required
     * @return ScheduleType
     */
    public Schedule createSchedule(UserContext uc, Schedule schedule);

    /**
     * Create Schedule Config Variables, a set of configuration variables for a
     * particular type of schedule.
     * Provenance
     * <li>An instance of schedule config variable is created;</li>
     * <li>Each ScheduleCategory must have a Schedule Config Variable;</li>
     * <li>An unique identifier for the schedule config variable is created;</li>
     * <li>Only one instance of schedule config variable is allowed for each ScheduleCategory.</li>
     *
     * @param uc     UserContext - Required
     * @param config ScheduleConfigVariableType - Required
     * @return ScheduleConfigVariableType
     */
    public ScheduleConfigVariableType createScheduleConfigVars(UserContext uc, ScheduleConfigVariableType config);

    /**
     * generateTimeslotList to generate an iterator of time slot List&lt;ScheduleTimeslotType>
     * <p>Limited number of iterator returned when lookup date or count are provided;</p>
     * <p>Unlimited number of iterator returned when lookup date or count are not provided;</p>
     *
     * @param uc            UserContext - Required
     * @param scheduleId    Long - Required Schedule Id
     * @param lookupDate    Date - Required Lookup date
     * @param lookupEndDate Date - Optional Lookup end date. Limited number of iterator returned when lookup date provided.
     * @param count         Long - Optional Number of the time slot. Limited number of iterator returned when lookup date provided.
     * @return List&lt;ScheduleTimeslotType>
     */
    public List<ScheduleTimeslotType> generateTimeslotList(UserContext uc, Long scheduleId, Date lookupDate, Date lookupEndDate, Long count);

    /**
     * generateTimeslot to generate an iterator of time slot TimeslotIterator&lt;ScheduleTimeslotType>
     * <p>Limited number of iterator returned when lookup date or count are provided;</p>
     * <p>Unlimited number of iterator returned when lookup date or count are not provided;</p>
     *
     * @param uc                UserContext - Required
     * @param recurrencePattern RecurrencePatternType - Required
     * @param lookupDate        Date - Required Lookup date (include time part of the date)
     * @param lookupEndDate     Date - Optional Lookup end date. Limited number of iterator returned when lookup date provided.
     * @param endTime           Date - Required the end time of each occurrence of schedule pattern
     * @param count             Long - Optional Number of the time slot. Limited number of iterator returned when lookup date provided.
     * @return TimeslotIterator&lt;ScheduleTimeslotType>
     */
    public TimeslotIterator generateTimeslotIterator(UserContext uc, RecurrencePatternType recurrencePattern, Date lookupDate, Date lookupEndDate, Date endTime,
            Long count);

    /**
     * generateTimeslotIterator to generate an iterator of time slot TimeslotIterator&lt;ScheduleTimeslotType>
     * <p>Limited number of iterator returned when lookup date or count are provided;</p>
     * <p>Unlimited number of iterator returned when lookup date or count are not provided;</p>
     *
     * @param uc            UserContext - Required
     * @param scheduleId    Long - Required Schedule Id
     * @param lookupDate    Date - Required Lookup date
     * @param lookupEndDate Date - Optional Lookup end date. Limited number of iterator returned when lookup date provided.
     * @param count         Long - Optional Number of the time slot. Limited number of iterator returned when lookup date provided.
     * @return TimeslotIterator&lt;ScheduleTimeslotType>
     */
    public TimeslotIterator generateTimeslotIterator(UserContext uc, Long scheduleId, Date lookupDate, Date lookupEndDate, Long count);

    /**
     * Generate Schedule Timetable.
     * Generate a timetable based on all the schedules defined for a type of
     * activity. The timetable contains a list of next available timeslots to
     * which an activity can book. The number of next available timeslots that
     * are included in the timetable is defined by schedule configuration
     * variables.
     * Provenance
     * <li>Only active schedule can be used to generate the timetable;</li>
     * <li>Only schedules of which the ScheduleCategory  is the same as the ScheduleCategory of the Schedule Config Variable Type can be used to generate the timetable;</li>
     * <li>If OverrideCapacity = False, a timeslot should be included in the timetable only if the number of existing activities that have booked to the timeslot is smaller than the capacity of the schedule based on which the timeslot is generated;</li>
     * <li>If OverrideCapcity = True, a timeslot should be included in the timetable regardless the capacity of the schedule;</li>
     * <li>The timetable should include only the timeslots that are dated on or after the LookupDate.</li>
     * <li>LookupDate <= Schedule’s TerminationDate;</li>
     * <li>If LookupEndDate is not provided,
     * <ol>Schedule Config Variable is required;</ol>
     * <ol>The number of timeslots included in the timetable should be limited to the NumOfNextAvailableTimeslot value defined by the Schedule Config VariableType.</ol>
     * </li>
     * <li>If LookupEndDate is provided,
     * <ol>The timetable must include all timeslots that have start date on or after the LookupDate, and before the LookupEndDate;</ol>
     * <ol>NumOfNextAvailableTimeslot is overridden.</ol>
     * </li>
     * <li>If NumOfNextAvailableTimeslot in the Schedule Config Variable is null, LookupEndDate is required.</li>
     *
     * @param uc               UserContext - Required
     * @param config           ScheduleConfigVariableType - Required
     * @param lookupDate       java.util.Date - Required
     * @param lookupEndDate    java.util.Date - Optional
     * @param overrideCapacity Boolean - Required
     * @return Set&lt;ScheduleTimeslotType>
     */
    public Set<ScheduleTimeslotType> generateScheduleTimetable(UserContext uc, ScheduleConfigVariableType config, Date lookupDate, Date lookupEndDate,
            Boolean overrideCapacity);

    /**
     * Get Included Date Time for the schedule and the date
     *
     * @param uc         UserContext - Required
     * @param scheduleId Long - Required
     * @param dateTime   Date - Required
     * @return Date endTime of the date
     */
    public Date getIncludeDateTime(UserContext uc, Long scheduleId, Date dateTime);

    /**
     * <p>
     * Update an Activity. Data Transfer Object properties that cannot be
     * updated will be ignored. No error will be thrown.  Services will validate
     * against a predefined uniqueness criteria. This criteria will be checked
     * by the service during update and return an error if it fails.
     * Associations can be added during an update but they cannot be removed.
     * If an association must be removed the deleteAssociation method must be invoked.
     * </p>
     * <p>
     * Returns a null and error code in return type if instance cannot be
     * updated.
     * </p>
     * Provenance
     * <li>An activity must already exist;</li>
     * <li>Activity identification cannot be updated;</li>
     * <li>Activity Planned Start Date, Planned End Date, Activity Antecedent Association, Activity Descendants Association, ScheduleID can be updated;</li>
     * <li>Activity Supervision Association can only be updated when it has never been persisted in DB before;</li>
     * <li>If schedule timeslot is provided,
     * <ol>the schedule timeslot’s start date/time and end date/time are copied to the activity’s planned start date and end date;</ol>
     * <ol>the ScheduleID of the timeslot is copied to the activity;</ol>
     * <ol>activity cannot be updated if the timeslot has reached its capacity, unless OverrideCapacity = True.</ol>
     * </li>
     * <li>If schedule timeslot is not provided,
     * <ol>User must manually update the activity’s planned start and end date;</ol>
     * <ol>the activity’s ScheduleID must be updated to null.</ol>
     * </li>
     *
     * @param uc               UserContext - Required
     * @param activity         ActivityType - Required
     * @param scheduleTimeslot ScheduleTimeslotType - Optional
     * @param overrideCapacity Boolean - Required
     * @return ActivityType
     */
    public ActivityType update(UserContext uc, ActivityType activity, ScheduleTimeslotType scheduleTimeslot, Boolean overrideCapacity);

    /**
     * Update Schedule
     * Provenance
     * <li>The schedule must exist;</li>
     * <li>The ScheduleIdentification cannot be updated;</li>
     * <li>If schedule has been associated to an activity, ScheduledCategory, FacilityAssociation cannot be updated;</li>
     * <li>Association can be made to only one of the location service, organization service or internal location service at a time.</li>
     *
     * @param uc       UserContext - Required
     * @param schedule ScheduleType - Required
     * @return ScheduleType
     */
    public Schedule updateSchedule(UserContext uc, Schedule schedule);

    /**
     * Update Schedule Config Variable, a set of configuration variable for a
     * particular type of schedule.
     * Provenance
     * <li>The schedule config variable already exists for a particular type of schedule;</li>
     * <li>ScheduleConfigIdentification cannot be updated;</li>
     * <li>ScheduleCategory cannot be updated.</li>
     *
     * @param uc     UserContext - Required
     * @param config ScheduleConfigVariableType - Required
     * @return ScheduleConfigVariableType
     */
    public ScheduleConfigVariableType updateScheduleConfigVars(UserContext uc, ScheduleConfigVariableType config);

    /**
     * Search for a set of Activities based on a specified criteria. The ids can
     * be used to search only within the specified subset of Id's. List what
     * Search options are available in comments, EXACT is default of search
     * match mode.
     * <p>
     * Returns a null and error code in return type if search cannot be
     * completed.
     * </p>
     * Provenance
     * <li>At least one of the search criteria must present when search activity is performed;</li>
     * <li>Allowable search criteria are activity identification, planned start date, planned end date, activity associations, activity antecedent association, activity descendants association;</li>
     * <li>If ActiveStatusFlag is null, return both active and inactive activities.</li>
     * <p>Text item/String will support the asterisk as a wildcard (that matches zero or more characters).
     * <p>Text item/String without a wildcard will be matched as an exact string match.
     * <p>Set Modes support:
     * <pre>
     * 	<li>ANY-- SET in search criterion will match sets that have at least one of the items.</li>
     *  <li>ALL-- SET in search criterion will match sets that have at least the specified subset.</li>
     * </pre>
     *
     * @param uc     UserContext - Required
     * @param ids    java.util.Set<java.lang.Long> - Optional if null search will
     *               occur across all activities.
     * @param search ActivitySearchType - Required
     * @return ActivitiesReturnType
     */
    public ActivitiesReturnType search(UserContext uc, Set<Long> ids, ActivitySearchType search, Long startIndex, Long resultSize, String resultOrder);

    /**
     * Search Schedule
     * Provenance
     * <li>At least one of the search criteria must present when search activity is performed;</li>
     * <li>Allowable search criteria are:
     * <ol>ScheduleCategory, FacilityAssociation, InternalLocationAssociation, Capacity, EffectiveDate, TerminationDate, ActiveScheduleFlag, ScheduleStartTime, ScheduleEndTime, RecurrencePattern and RecurrencePattern extensions;</ol>
     * <li>If ActiveScheduleFlag = null, return both active and inactive schedules.</li>
     * <p>Text item/String will support the asterisk as a wildcard (that matches zero or more characters).
     *
     * @param uc     UserContext - Required
     * @param ids    Set&lt;Long> - Optional if null search will
     *               occur across all schedules.
     * @param search ScheduleSearchType - Required
     * @return SchedulesReturnType
     */
    public SchedulesReturnType searchSchedule(UserContext uc, Set<Long> ids, ScheduleSearchType search, Long startIndex, Long resultSize, String resultOrder);

    /**
     * Search Schedule Config Variable, a set of configuration variable for a
     * particular type of schedule.
     * Provenance
     * <li>At least one of the search criteria must present when search activity is performed;</li>
     * <li>Allowable search criteria are ScheduleCategory, NumOfNextAvailableTimeslots.</li>
     *
     * @param uc     UserContext - Required
     * @param search ScheduleConfigVariableSearchType - Required
     * @return ScheduleConfigVariablesReturnType
     * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
     * for all error codes and descriptions.
     */
    public ScheduleConfigVariablesReturnType searchScheduleConfigVers(UserContext uc, ScheduleConfigVariableSearchType search, Long startIndex, Long resultSize,
            String resultOrder);

    /**
     * Delete an Activity by id.
     * Provenance
     * <li>The Activity must exist;</li>
     * <li>The activity cannot be deleted if the activity has association to descendants.</li>
     * Returns a null and success code if instance can be deleted.
     * Returns a null and error code if instance cannot be deleted.
     *
     * @param uc UserContext - Required
     * @param id java.lang.Long - Required
     * @return Long - Returns 1L if success; Throws ArbutusRuntimeException otherwise.
     */
    public Long delete(UserContext uc, Long id);

    /**
     * Delete Schedule.
     * Provenance
     * <li>Only Inactive schedule can be deleted.</li>
     *
     * @param uc UserContext - Required
     * @param id Long - Required
     * @return Long - Returns 1L if success; Throws ArbutusRuntimeException otherwise.
     */
    public Long deleteSchedule(UserContext uc, Long id);

    /**
     * Delete All Schedule.
     * <p>For maintenance use.</p>
     *
     * @param uc UserContext - Required
     * @return Long - success: 1; failure: Throws ArbutusRuntimeException.
     */
    public Long deleteAllSchedule(UserContext uc);

    /**
     * Delete Schedule Config Variable.
     * <p>For maintenance use.</p>
     *
     * @param uc UserContext - Required
     * @param id Long - Required
     * @return Long - success: 1; failure: Throws ArbutusRuntimeException.
     */
    public Long deleteScheduleConfigVar(UserContext uc, Long id);

    /**
     * Delete All Schedule Config Variable.
     * <p>For maintenance use.</p>
     *
     * @param uc UserContext - Required
     * @return Long - success: 1.
     */
    public Long deleteAllScheduleConfigVar(UserContext uc);

    /**
     * Cancel Activities due to schedule change.
     * Provenance
     * <li>All activities are canceled if they meet the following criteria,
     * <ol>The planned start dates of the activity is after the CancellationDate;</ol>
     * <ol>Activity’s ScheduleID = ScheduleIdentification;</ol>
     * </li>
     * <li>Status of a canceled activity is set to inactive;</li>
     * <li>If deleteActivityFlag = True, canceled activity will be deleted. The activity augmentation associated to the activity must be deleted as well (out of scope of this service).</li>
     *
     * @param uc                 UserContext - Required
     * @param scheduleId         Long - Required
     * @param cancellationDate   java.util.Date - Required
     * @param deleteActivityFlag Boolean - Required
     * @return Set<ActivityType>
     */
    public Set<ActivityType> cancel(UserContext uc, Long scheduleId, Date cancellationDate, Boolean deleteActivityFlag);

    /**
     * Cancel Activities by a Schedule Time slot.
     * Provenance
     * <li>All activities are canceled if they meet the following criteria,
     * <ol>The activity’s planned start date = timeslot’s TimeslotStartDateTime;</ol>
     * <ol>The activity’s planned end date = timeslot’s TimeslotEndDateTime;</ol>
     * <ol>Activity’s ScheduleID = timeslot’s ScheduleID;</ol>
     * </li>
     * <li>Status of a canceled activity is set to inactive;</li>
     * <li>If deleteActivityFlag = True, canceled activity will be deleted. The activity augmentation associated to the activity must be deleted as well (out of scope of this service).</li>
     *
     * @param uc                 UserContext - Required
     * @param timeslot           ScheduleTimeslotType - Required
     * @param deleteActivityFlag Boolean - Required
     * @return Set<ActivityType>
     */
    public Set<ActivityType> cancel(UserContext uc, ScheduleTimeslotType timeslot, Boolean deleteActivityFlag);

    /**
     * Delete all Activities, associations and reference codes.
     * Return success code if all instances, associations and reference codes
     * deleted.
     * Return error code if not all instances, associations and reference codes
     * deleted.
     *
     * @param uc UserContext - Required
     * @return Long Returns 1 if success.
     */
    public Long deleteAll(UserContext uc);

    /**
     * Get a set of all ids of Activities.
     *
     * @param uc UserContext - Required
     * @return Set<java.lang.Long> a set of all ids of Activities.
     */
    public Set<Long> getAll(UserContext uc);


    /**
     * Returns a list of ScheduleActivityLinkType objects given a list of activity ids
     * <p>
     *     This method is useful if users need to know the start time AND recurrence pattern of particular activities (if they have them)
     *     Original purpose was to provide unique identifiers for copying count schedules (if all we have are the activity Ids)
     * </p>
     * @param uc
     * @param activityIds
     * @return
     */
    public List<ScheduleActivityLinkType> getScheduleActivityLinkTypesFromActivityIds(UserContext uc, List<Long> activityIds);

    /**
     * returns true if an input schedule has any overlap in the database
     *
     * <p>
     *     Schedule parameter does not need to have an identification associated.
     *     Assumptions: Start date / Start time / Recurrence Pattern are all not null
     * </p>
     *
     * @param uc UserContext - Required
     * @param schedule Schedule - required
     * @return
     */
    public Boolean hasDuplicateOverlappingSchedule(UserContext uc, Schedule schedule);

    /**
     * Get the stamp of an Activity.
     * Returns null if there is an error retrieving the stamp.
     *
     * @param uc UserContext - Required
     * @param id Long - Required
     * @return StampType
     */
    public StampType getStamp(UserContext uc, Long id);

    /**
     * getNiem -- Get an XML output of the specified business object instance.
     *
     * @param uc         UserContext - Required
     * @param activityId java.lang.Long - Required
     * @return String
     */
    public String getNiem(UserContext uc, Long activityId);

    /**
     * Retrieve the version number in format "MajorVersionNumber.MinorVersionNumber"
     *
     * @param uc
     * @return version number
     */
    public String getVersion(UserContext uc);
}
