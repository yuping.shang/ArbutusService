package syscon.arbutus.product.services.activity.contract.dto;

public enum DayOfWeekEnum {

    MON("MON", 1),
    TUE("TUE", 2),
    WED("WED", 3),
    THU("THU", 4),
    FRI("FRI", 5),
    SAT("SAT", 6),
    SUN("SUN", 0);

    private final String name;
    private final int order;

    DayOfWeekEnum(String name, int order) {
        this.name = name;
        this.order = order;
    }

    public int getOrder() {
        return order;
    }

    public String getName() {
        return name;
    }
}