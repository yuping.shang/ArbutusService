package syscon.arbutus.product.services.activity.contract.ejb;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import java.util.*;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.*;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.activity.contract.util.RecurrencePatternFactory;
import syscon.arbutus.product.services.activity.contract.util.ScheduleRecurrenceUtil;
import syscon.arbutus.product.services.activity.realization.persistence.*;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.*;
import syscon.arbutus.product.services.core.util.DateUtil;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.realization.util.ReferenceDataHelper;
import syscon.arbutus.product.services.utility.ValidationHelper;
import syscon.arbutus.product.services.utility.ical.TimeslotIterable;
import syscon.arbutus.product.services.utility.ical.TimeslotIteratorFactory;

/**
 * ActivityType Service Bean
 * The ActivityType service records system events. Activities may be scheduled to
 * occur in the future, in which case the date and time of the occurrence can be
 * pre-planned. Activities may also be unscheduled and recorded as they occur in
 * real time.
 * The ActivityType service maintains a close association with other activity-based
 * services (MOVEMENT ACTIVITY, CLASSIFICATION ACTIVITY, HOUSING BED MANAGEMENT
 * ACTIVITY, etc), each of which holds a complete record of a system event of a
 * particular type, including information unique to that type of activity. The
 * activity-based services are referred in this document as activity
 * augmentation service, although the relationship between the activity service
 * and the activity augmentation service are peers. The eventual outcome
 * (occurred, did not occur, re-scheduled, canceled, etc), and the actual date
 * and time of the event occurrence, are recorded against the particular type of
 * activities, therefore managed by the activity augmentation services. New
 * business entities are added to this structure through addition of a new
 * activity-based service built on a new activity type.
 * ActivityType service can reference itself in order to establish relationships
 * among a set of related events. Activities are related to one another by
 * acting as a preceding event or a succeeding event.
 * Scope:
 * <li>Record a scheduled event as an activity;</li>
 * <li>Record an unscheduled event as an activity;</li>
 * <li>Record and update the planned date and time of the activity;</li>
 * <li>Delete an activity (under controlled circumstances);</li>
 * <li>Associate an activity to related activities;</li>
 * <li>Search for an activity;</li>
 * <li>Record a predefined schedule for a type of activity;</li>
 * <li>Update a predefined schedule for a type of activity;</li>
 * <li>Delete a predefined schedule for a type of activity;</li>
 * <li>Search for an activity schedule.</li>
 * Constraints:
 * <li>All constraints for uniqueness in this service cannot be overridden by the user.</li>
 *
 * @author yshang
 * @version 1.0 (based on SDD 1.0)
 * @since April 27, 2012
 */
@Stateless(mappedName = "ActivityService")
@Remote(ActivityService.class)
public class ActivityServiceBean implements ActivityService {

    // should deprecate this const
    public static final Long SUCCESS = 1L;
    private static Logger log = LoggerFactory.getLogger(ActivityServiceBean.class);
    private static int SEARCH_MAX_LIMIT = 200;
    private static int TIMESLOTS_MAX_LIMIT = 730;

    private final ScheduleRecurrenceUtil recurrenceUtil = new ScheduleRecurrenceUtil();

    @Resource
    SessionContext context;
    @PersistenceContext(unitName = "arbutus-pu")
    Session session;

    ActivityServiceDAO handler = new ActivityServiceDAO();

    /**
     * To load the set of the initial Reference Codes
     */
    @PostConstruct
    private void init() {
        if (log.isDebugEnabled()) {
            log.debug("init: Begin ... ...");
        }

        ClassLoader loader = this.getClass().getClassLoader();

        try {
            ResourceBundle rb = ResourceBundle.getBundle("service", Locale.getDefault(), loader);
            for (@SuppressWarnings("rawtypes") Enumeration keys = rb.getKeys(); keys.hasMoreElements(); ) {
                String key = (String) keys.nextElement();
                if ("searchMaxLimit".equalsIgnoreCase(key)) {
                    String value = rb.getString(key);
                    SEARCH_MAX_LIMIT = Integer.parseInt(value);
                    return;
                }

                if ("timeslotsMaxLimit".equalsIgnoreCase(key)) {
                    String value = rb.getString(key);
                    TIMESLOTS_MAX_LIMIT = Integer.parseInt(value);
                    return;
                }

            }
        } catch (Exception ex) {
            log.error("init: property file open failure.");
        }

        if (log.isDebugEnabled()) {
            log.debug("init: End ... ...");
        }
    }

    /**
     * Count of all Activities.
     * Returns the count of all Activities; throws HibernateException if failure.
     *
     * @param uc UserContext - Required
     * @return java.lang.Long the count of all Activities
     * @throws HibernateException
     */
    public Long getCount(UserContext uc) {
        Long ret = null;
        if (log.isDebugEnabled()) {
            log.debug("getCount: begin");
        }

        ret = BeanHelper.getCount(uc, context, session, "activityId", ActivityEntity.class);

        if (log.isDebugEnabled()) {
            log.debug("getCount: end");
        }
        return ret;
    }

    /**
     * Retrieve an Activity by Activity id.
     * Returns an instance of Activity if success;
     * Throws InvalidInputException if incorrect id is provided as parameter otherwise.
     *
     * @param uc UserContext - Required
     * @param id java.lang.Long - Required
     * @return ActivityType Returns an instance of Activity
     * @throws InvalidInputException if incorrect id is provided
     */
    public ActivityType get(UserContext uc, Long id) {
        if (log.isDebugEnabled()) {
            log.debug("get: begin");
        }

        if (id == null) {
            String message = "Invalid argument: Activity Id is required.";
            LogHelper.error(log, "get", message);
            throw new InvalidInputException(message);
        }

        ActivityEntity activity = (ActivityEntity) BeanHelper.findEntity(session, ActivityEntity.class, id);
        ActivityType ret = ActivityServiceHelper.toActivity(activity);

        if (log.isDebugEnabled()) {
            log.debug("get: end");
        }

        return ret;
    }

    /**
     * see interface
     * @param uc UserContext - Required
     * @param scheduleId java.lang.Long - Required
     * @param category Schedule Category - Optional
     * @return
     */
    public List<ActivityType> getFromScheduleId(UserContext uc, Long scheduleId, String category) {
        String functionName = "getFromScheduleId";
        if (scheduleId == null) {
            String message = "scheduleId is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message,ErrorCodes.GENERIC_NULL_EMPTY_INPUT);
        }

        Criteria c = session.createCriteria(ActivityEntity.class);
        c.add(Restrictions.eq("scheduleID", scheduleId));

        if (category != null) {
            c.add(Restrictions.eq("activityCategory", category));
        }

        if (!BeanHelper.isEmpty(category) && category.equals(ActivityCategory.FACOUNT.name())) {
            c.add(Restrictions.isNull("antecedentId"));
            c.setMaxResults(1);
        }

        List<ActivityType> list = ActivityServiceHelper.toActivitiesList(c.list());
        return list;
    }

    /**
     * Get/Retrieve Activity Descendants, a set of Activities including the
     * direct and indirect descendants of the current Activity.
     * <p>Provenance
     * <li>All activities that are direct descendants of the current activity should be retrieved;</li>
     * <li>All activities that are indirect descendants of the current activity should be retrieved;</li>
     * <li>The current activity and its direct and indirect descendants form a tree structure. The activities at the leaf level of the tree structure do not have descendants.</li>
     *
     * @param uc       UserContext - Required
     * @param activity ActivityType - Required
     * @return Set&lt;ActivityType> - Returns a set of instances of Decendant ActivityType if success;
     * Returns 0 size of Set of instance of ActivityType if no Decendant exists.
     * @throws InvalidInputException/ArbutusRuntimeException when invalid argument provided.
     */
    public Set<ActivityType> getDecendants(UserContext uc, ActivityType activity) {

        if (log.isDebugEnabled()) {
            log.debug("getDecendants: begin");
        }

        String functionName = "getDecendants";

        if (activity == null) {
            String message = "Activity is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Long id = activity.getActivityIdentification();
        if (id == null) {
            String message = "Activity Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ActivityEntity act = BeanHelper.getEntity(session, ActivityEntity.class, id);

        Set<ActivityType> ret = new HashSet<ActivityType>();
        if (act.getDescendantIds() == null || act.getDescendantIds().size() == 0) {
            log.debug("ActivityType with id " + id + " does not have descendant.");
            return ret;
        }

        Criteria c = session.createCriteria(ActivityEntity.class);
        Set<Long> descendantIDs = new HashSet<Long>();

        Queue<Long> queue = new PriorityQueue<Long>();
        for (Long desc : act.getDescendantIds()) {
            queue.add(desc);
        }

        int count = 0;
        int maxCount = SEARCH_MAX_LIMIT;
        while (!queue.isEmpty() && count < maxCount) {
            Long descendantId = queue.poll();
            descendantIDs.add(descendantId);
            ActivityEntity descendant = BeanHelper.getEntity(session, ActivityEntity.class, descendantId);
            Set<Long> descendants = descendant.getDescendantIds();
            if (descendants != null && descendants.size() != 0) {
                queue.addAll(descendant.getDescendantIds());
            }
            count++;
        }
        c.add(Restrictions.in("activityId", descendantIDs));

        @SuppressWarnings("unchecked") Set<ActivityEntity> activityEntities = new HashSet<ActivityEntity>(c.list());

        ret = ActivityServiceHelper.toActivities(activityEntities);

        if (log.isDebugEnabled()) {
            log.debug("getDecendants: end");
        }

        return ret;
    }

    /**
     * Get/Retrieve Activity Antecedents, a set of Activities including the
     * direct and indirect antecedents of the current Activity.
     * <p>Provenance
     * <li>All activities that are direct antecedents of the current activity should be retrieved;</li>
     * <li>All activities that are indirect antecedents of the current activity should be retrieved;</li>
     * <li>The current activity and its direct and indirect antecedents form a tree structure. The activities at the leaf level of the tree structure do not have antecedents.</li>
     *
     * @param uc       UserContext - Required
     * @param activity ActivityType - Required
     * @return Set&lt;ActivityType> - Returns a set of instances of Antecedent ActivityType if success;
     * Returns 0 size of Set of instance of ActivityType if no Antecedent exists;
     * @throws ArbutusRuntimeException InvalidInputException/ArbutusRuntimeException when invalid argument provided.
     */
    public Set<ActivityType> getAntecedents(UserContext uc, ActivityType activity) {

        if (log.isDebugEnabled()) {
            log.debug("getAntecedents: begin");
        }

        ValidationHelper.validate(activity);

        String functionName = "getAntecedents";

        Long id = activity.getActivityIdentification();
        if (id == null) {
            String message = "Activity Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        ActivityType act = get(uc, id);

        Set<ActivityType> ret = new HashSet<ActivityType>();
        Long antecedent = act.getActivityAntecedentId();
        while (antecedent != null) {
            Long antecedentId = antecedent;
            ActivityType a = get(uc, antecedentId);
            if (a != null) {
                ret.add(a);
                antecedent = a.getActivityAntecedentId();
            } else {
                antecedent = null;
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("getAntecedents: end");
        }
        return ret;
    }

    /**
     * Get/Retrieve Schedule
     *
     * @param uc UserContext - Required
     * @param id Long - Required
     * @return ScheduleType Returns an instance of ScheduleType.
     * @throws InvalidInputException if incorrect id provided.
     */
    public SimpleSchedule getSchedule(UserContext uc, Long id) {
        if (log.isDebugEnabled()) {
            log.debug("getSchedule: begin");
        }

        if (id == null) {
            String message = "Schedule id is required.";
            LogHelper.error(log, "getSchedule", message);
            throw new InvalidInputException(message);
        }
        ScheduleEntity schedule = BeanHelper.findEntity(session, ScheduleEntity.class, id);
        SimpleSchedule ret = ActivityServiceHelper.toSimpleSchedule(schedule);

        if (log.isDebugEnabled()) {
            log.debug("getSchedule: end");
        }
        return ret;
    }

    /**
     * Get/Retrieve All Schedules
     *
     * @param uc                 UserContext - Required
     * @param scheduleActiveFlag Boolean
     *                           - Optional -- true: get all active schedules; false: get all inactive schedules; null: get all schedules (active and inactive).
     * @return Set&lt;ScheduleType> Returns a set of instances of ScheduleType if success.
     */
    public Set<Schedule> getAllSchedules(UserContext uc, Boolean scheduleActiveFlag) {

        if (log.isDebugEnabled()) {
            log.debug("getAllSchedules: begin");
        }

        Criteria c = session.createCriteria(ScheduleEntity.class);

        if (scheduleActiveFlag != null && scheduleActiveFlag.booleanValue()) {
            //Date today = getToday();
            Date today = getPresentTime();
            c.add(Restrictions.or(Restrictions.and(Restrictions.le("effectiveDate", today), Restrictions.isNull("terminationDate")),
                    Restrictions.and(Restrictions.le("effectiveDate", today), Restrictions.gt("terminationDate", today))));
        } else if (scheduleActiveFlag != null && !scheduleActiveFlag.booleanValue()) {
            Date today = getPresentTime();
            c.add(Restrictions.or(Restrictions.gt("effectiveDate", today), Restrictions.lt("terminationDate", today)));
        }

        @SuppressWarnings("unchecked") List<ScheduleEntity> list = c.list();
        if (list == null) {
            return null;
        }

        Set<ScheduleEntity> schedules = new HashSet<ScheduleEntity>(list);

        Set<Schedule> ret = ActivityServiceHelper.toSchedules(schedules);

        if (log.isDebugEnabled()) {
            log.debug("getAllSchedules: end");
        }
        return ret;
    }

    /**
     * Get/Retrieve Schedule Config Variable, the configuration variable for a
     * particular type of schedule.
     *
     * @param uc UserContext - Required
     * @param id Long - Required
     * @return ScheduleConfigVariableType
     * @throws InvalidInputException when invalid id provided
     */
    public ScheduleConfigVariableType getScheduleConfigVar(UserContext uc, Long id) {

        if (log.isDebugEnabled()) {
            log.debug("getScheduleConfigVar: begin");
        }

        if (id == null) {
            String message = "ScheduleConfigVariable id is required.";
            LogHelper.error(log, "getScheduleConfigVar", message);
            throw new InvalidInputException(message);
        }
        ScheduleConfigVariableEntity config = BeanHelper.getEntity(session, ScheduleConfigVariableEntity.class, id);
        ScheduleConfigVariableType ret = ActivityServiceHelper.toScheduleConfigVariable(config);

        if (log.isDebugEnabled()) {
            log.debug("getScheduleConfigVar: end");
        }
        return ret;
    }

    /**
     * Get/Retrieve All Schedule Config Variables.
     *
     * @param uc UserContext - Required
     * @return Set&lt;ScheduleConfigVariableType>
     * @throws HibernateException
     */
    public Set<ScheduleConfigVariableType> getAllScheduleConfigVars(UserContext uc) {
        if (log.isDebugEnabled()) {
            log.debug("getAllScheduleConfigVars: begin");
        }

        Criteria c = session.createCriteria(ScheduleConfigVariableEntity.class);
        @SuppressWarnings("unchecked") Set<ScheduleConfigVariableEntity> configs = new HashSet<ScheduleConfigVariableEntity>(c.list());
        Set<ScheduleConfigVariableType> ret = ActivityServiceHelper.toScheduleConfigVariables(configs);

        if (log.isDebugEnabled()) {
            log.debug("getAllScheduleConfigVars: end");
        }
        return ret;
    }

    /**
     * Get/Retrieve Timeslot Capacity.
     * <p>
     * Check if the number of activities booked to a scheduled timeslot exceeds
     * the capacity of the schedule, and calculate the remaining capacity of the
     * timeslot.
     * </p>
     * Provenance
     * <li>Remaining Capacity =  ScheduleType Capacity – number of activities booked to the timeslot;</li>
     * <li>If Remaining Capacity = 0, the timeslot has reached its full capacity;</li>
     * <li>An activity is booked to a timeslot if,
     * <ol>1. ActivityType status is active;</ol>
     * <ol>2. ActivityType is associated to the schedule that is used to generate the timeslot;</ol>
     * <ol>3. ActivityType’s planned start date = the timeslot’s start date/time;</ol>
     * <ol>4. ActivityType’s planned end date = the timeslot’s end date/time.</ol>
     * </li>
     *
     * @param uc       UserContext - Required
     * @param timeslot ScheduleTimeslotType - Required
     * @return TimeslotCapacityReturnType
     */
    public TimeslotCapacityType getTimeslotCapacity(UserContext uc, ScheduleTimeslotType timeslot) {

        if (log.isDebugEnabled()) {
            log.debug("getTimeslotCapacity: begin");
        }

        ValidationHelper.validate(timeslot);

        Long scheduleId = timeslot.getScheduleID();
        Date start = timeslot.getTimeslotStartDateTime();
        Date end = timeslot.getTimeslotEndDateTime();

        Criteria c = session.createCriteria(ActivityEntity.class);
        c.add(Restrictions.eq("scheduleID", scheduleId));
        c.add(Restrictions.eq("activeStatusFlag", Boolean.TRUE));
        c.add(Restrictions.eq("plannedStartDate", start));
        if (end != null) {
            c.add(Restrictions.eq("plannedEndDate", end));
        }

        c.setProjection(Projections.property("activityId"));

        @SuppressWarnings("unchecked") Set<Long> activityIDs = new HashSet<Long>(c.list());

        Long numOfBooked = 0L;
        if (activityIDs != null) {
            numOfBooked = Long.valueOf(activityIDs.size());
        }

        ScheduleEntity schedule = BeanHelper.getEntity(session, ScheduleEntity.class, scheduleId);

        Long capacityRemaining = 0L;
        if (schedule.getCapacity() != null) {
            capacityRemaining = schedule.getCapacity() - numOfBooked;
        } else {
            capacityRemaining = -numOfBooked;
        }
        capacityRemaining = capacityRemaining < 0L ? 0L : capacityRemaining;

        TimeslotCapacityType ret = new TimeslotCapacityType(timeslot, activityIDs, numOfBooked, capacityRemaining);

        if (log.isDebugEnabled()) {
            log.debug("getTimeslotCapacity: end");
        }
        return ret;
    }

    TimeslotCapacityType getTimeslotCapacity(UserContext uc, ScheduleEntity schedule) {
        if (log.isDebugEnabled()) {
            log.debug("getTimeslotCapacity: begin");
        }

        Long scheduleId = schedule.getScheduleIdentification();
        Date start = schedule.getEffectiveDate();
        Date end = schedule.getTerminationDate();

        Criteria c = session.createCriteria(ActivityEntity.class);
        c.add(Restrictions.eq("scheduleID", scheduleId));
        c.add(Restrictions.eq("activeStatusFlag", Boolean.TRUE));
        c.add(Restrictions.eq("plannedStartDate", start));
        if (end != null) {
            c.add(Restrictions.eq("plannedEndDate", end));
        }

        c.setProjection(Projections.property("activityId"));

        @SuppressWarnings("unchecked") Set<Long> activityIDs = new HashSet<Long>(c.list());

        Long numOfBooked = 0L;
        if (activityIDs != null) {
            numOfBooked = Long.valueOf(activityIDs.size());
        }

        Long capacityRemaining = 0L;
        if (schedule.getCapacity() != null) {
            capacityRemaining = schedule.getCapacity() - numOfBooked;
        } else {
            capacityRemaining = -numOfBooked;
        }
        capacityRemaining = capacityRemaining < 0L ? 0L : capacityRemaining;

        TimeslotCapacityType timeslotCapacity = new TimeslotCapacityType(new ScheduleTimeslotType(start, end, scheduleId), activityIDs, numOfBooked, capacityRemaining);

        if (log.isDebugEnabled()) {
            log.debug("getTimeslotCapacity: end");
        }
        return timeslotCapacity;
    }

    /**
     * Get/Retrieve ScheduleType Capacity.
     * <p>
     * Given a period of time, check if the number of activities booked to any
     * of the timeslot of a schedule exceeds the capacity of the schedule, and
     * calculate the remaining capacity of each timeslot.
     * </p>
     * Provenance
     * <li>All timeslots starts in between the LookupDate (inclusive) and the LookupEndDate (inclusive) are included in the schedule capacity check;</li>
     * <li>LookupDate &gt;= ScheduleType’s Effective Date;</li>
     * <li>LookupEndDate &lt;= ScheduleType’s Termination Date.</li>
     *
     * @param uc            UserContext - Required
     * @param scheduleId    Long - Required
     * @param lookupDate    java.util.Date - Required
     * @param lookupEndDate java.util.Date - Required
     * @return ScheduleCapacityReturnType
     */
    public ScheduleCapacityType getScheduleCapacity(UserContext uc, Long scheduleId, Date lookupDate, Date lookupEndDate) {

        if (log.isDebugEnabled()) {
            log.debug("getScheduleCapacity: begin");
        }

        String functionName = "getScheduleCapacity";

        if (scheduleId == null || lookupDate == null || lookupEndDate == null) {
            String message = "Invalid arguments: scheduleId = " + scheduleId + ", lookupDate = " + lookupDate +
                    ", lookupEndDate = " + lookupEndDate;
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        if (lookupDate.after(lookupEndDate)) {
            String message = "Invalid arguments: lookupDate cannot be after lookuEndDate: lookupDate = " + lookupDate +
                    ", lookupEndDate = " + lookupEndDate;
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        BeanHelper.getEntity(session, ScheduleEntity.class, scheduleId);

        Set<ScheduleTimeslotType> timeslots = this.generateScheduleTimetable(uc, scheduleId, lookupDate, lookupEndDate, false);

        Set<TimeslotCapacityType> timeslotCapacities = new HashSet<TimeslotCapacityType>();
        for (ScheduleTimeslotType timeslot : timeslots) {
            TimeslotCapacityType timeslotCapacity = getTimeslotCapacity(uc, timeslot);
            if (timeslotCapacity != null) {
                timeslotCapacities.add(timeslotCapacity);
            }
        }
        int count = timeslotCapacities.size();

        ScheduleCapacityType ret = new ScheduleCapacityType(scheduleId, timeslotCapacities, Long.valueOf(count));

        if (log.isDebugEnabled()) {
            log.debug("getScheduleCapacity: end");
        }
        return ret;
    }

    /**
     * <p>
     * Create an ActivityType.
     * </p>
     * <p>
     * Returns a null and error code in return type if instance cannot be
     * created.
     * </p>
     * If schedule timeslot is not null,
     * <li>the schedule timeslot’s start date/time and end date/time are copied to the activity’s planned start date and end date;</li>
     * <li>the ScheduleID of the timeslot is copied to the activity;</li>
     * <li>activity cannot be created if the timeslot has reached its capacity, unless OverrideCapacity = True.</li>
     * <p>If schedule timeslot is not provided,
     * <li>User must manually enter the activity’s planned start and end date;</li>
     * <li>the activity’s ScheduleID must be set to null.</li>
     *
     * @param uc               UserContext - Required
     * @param activity         ActivityType - Required
     * @param scheduleTimeslot ScheduleTimeslotType - Optional
     * @param overrideCapacity Boolean - Required
     * @return ActivityReturnType
     */
    public ActivityType create(UserContext uc, ActivityType activity, ScheduleTimeslotType scheduleTimeslot, Boolean overrideCapacity) {

        if (log.isDebugEnabled()) {
            log.debug("create: begin");
        }

        String functionName = "create";

        if (overrideCapacity == null) {
            String message = "overrideCapacity is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        ValidationHelper.validate(activity);
        if (scheduleTimeslot != null) {
            ValidationHelper.validate(scheduleTimeslot);
        }

        if (activity.getPlannedStartDate() != null && activity.getScheduleID() == null && (activity.getPlannedEndDate() != null &&  activity.getPlannedEndDate().before(
                activity.getPlannedStartDate()))) {
            String message =
                    "Invalid argument: plannedStartDate = " + activity.getPlannedStartDate() + ", plannedEndDate = " + activity.getPlannedEndDate() + ", scheduleID = "
                            + activity.getScheduleID();
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        if (scheduleTimeslot != null && (scheduleTimeslot.getScheduleID() == null || scheduleTimeslot.getTimeslotStartDateTime() == null
                || scheduleTimeslot.getTimeslotEndDateTime() == null || scheduleTimeslot.getTimeslotEndDateTime().before(scheduleTimeslot.getTimeslotStartDateTime()))
                || overrideCapacity == null) {
            String message = "Invalid Schedule Timeslot: " + scheduleTimeslot;
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        ActivityEntity entity = ActivityServiceHelper.toActivityEntity(activity);
        ValidationHelper.verifyMetaCodes(entity, true);
        if (entity.getActivityId() != null) {
            entity.setActivityId(null);
        }

        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        entity.setStamp(createStamp);

        Long antecedent = entity.getAntecedentId();
        if (antecedent != null) {
            if (!BeanHelper.bExistedModule(session, ActivityEntity.class, antecedent)) {
                String message = "Activity with id " + antecedent + " does not exist.";
                LogHelper.error(log, functionName, message);
                throw new DataNotExistException(message);
            }
        }

        Set<Long> descendants = entity.getDescendantIds();
        if (descendants != null && !descendants.isEmpty()) {
            if (!BeanHelper.bExistedModules(session, ActivityEntity.class, descendants)) {
                String message = "Activities with ids " + descendants + " do not exist.";
                LogHelper.error(log, functionName, message);
                throw new DataNotExistException(message);
            }
        }

        if (scheduleTimeslot != null) {
            Date start = scheduleTimeslot.getTimeslotStartDateTime();
            Date end = scheduleTimeslot.getTimeslotEndDateTime();
            Long scheduleId = scheduleTimeslot.getScheduleID();
            entity.setPlannedStartDate(start);
            entity.setPlannedEndDate(end);
            entity.setScheduleID(scheduleId);

            ScheduleEntity schedule = BeanHelper.getEntity(session, ScheduleEntity.class, scheduleId);
            entity.setActivityCategory(schedule.getScheduleCategory());

            if (!overrideCapacity.booleanValue()) {
                TimeslotCapacityType timeslotCapacity = getTimeslotCapacity(uc, scheduleTimeslot);
                Long capacityRemaining = 0L;
                if (timeslotCapacity != null) {
                    capacityRemaining = timeslotCapacity.getCapacityRemaining();
                }
                if (capacityRemaining > 0) {
                    entity.setActiveStatusFlag(true);
                    session.persist(entity);
                } else {
                    String errmsg = "ActivityType cannot be created because the timeslot has reached its capacity." + timeslotCapacity;
                    LogHelper.error(log, functionName, errmsg);
                    throw new ArbutusRuntimeException(errmsg);
                }
            } else {
                /* If an activity is active,
                 * it is taking a spot in the capacity of
				 * a timeslot of the activity’s schedule.
				 */
                entity.setActiveStatusFlag(true);
                session.persist(entity);
            }
        } else {
            Date start = activity.getPlannedStartDate();
            Date end = activity.getPlannedEndDate();
            if (!ActivityCategory.FACOUNT.name().equalsIgnoreCase(activity.getActivityCategory()) && start == null ) {
                String errmsg = "Planned Start Date  can be null: " + start;
                LogHelper.error(log, functionName, errmsg);
                throw new ArbutusRuntimeException(errmsg);
            }
            entity.setPlannedStartDate(start);
            entity.setPlannedEndDate(end);
            // entity.setScheduleID(null);
            session.persist(entity);
        }

        Long instanceId = entity.getActivityId();

        // Antecedent's descendant
        if (entity.getAntecedentId() != null) {
            Long antecedentId = entity.getAntecedentId();
            ActivityEntity antecedentEntity = BeanHelper.getEntity(session, ActivityEntity.class, antecedentId);
            antecedentEntity.getDescendantIds().add(instanceId);
        }

        // Descendants' antecedent
        Set<Long> descendantIds = entity.getDescendantIds();
        if (descendantIds != null) {
            for (Long desId : descendantIds) {
                ActivityEntity desEntity = BeanHelper.getEntity(session, ActivityEntity.class, desId);
                if (desEntity.getAntecedentId() != null) {
                    String errmsg = "Invalid argument: the decendant has had an antecedent already. " + desEntity;
                    LogHelper.error(log, functionName, errmsg);
                    throw new InvalidInputException(errmsg);
                }
                desEntity.setAntecedentId(instanceId);
            }
        }

        entity = (ActivityEntity) session.get(ActivityEntity.class, instanceId);
        ActivityType ret = ActivityServiceHelper.toActivity(entity);

        if (log.isDebugEnabled()) {
            log.debug("create: end");
        }
        return ret;
    }

    /**
     * <p>
     * Create an ActivityType.
     * </p>
     * <p>
     * Returns a null and error code in return type if instance cannot be
     * created.
     * </p>
     * If schedule timeslot is not null,
     * <li>the schedule timeslot’s start date/time and end date/time are copied to the activity’s planned start date and end date;</li>
     * <li>the ScheduleID of the timeslot is copied to the activity;</li>
     * <li>activity cannot be created if the timeslot has reached its capacity, unless OverrideCapacity = True.</li>
     * <p>If schedule timeslot is not provided,
     * <li>User must manually enter the activity’s planned start and end date;</li>
     * <li>the activity’s ScheduleID must be set to null.</li>
     *
     * @param uc               UserContext - Required
     * @param activityEntity   ActivityEntity - Required
     * @param scheduleTimeslot ScheduleTimeslotType - Optional
     * @param overrideCapacity Boolean - Required
     * @return ActivityType
     */
    ActivityType create(UserContext uc, ActivityEntity activityEntity, ScheduleTimeslotType scheduleTimeslot, Boolean overrideCapacity) {
        if (log.isDebugEnabled()) {
            log.debug("create: begin");
        }

        String functionName = "create";

        if (overrideCapacity == null) {
            String message = "overrideCapacity is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        ValidationHelper.verifyMetaCodes(activityEntity, true);
        if (scheduleTimeslot != null) {
            ValidationHelper.validate(scheduleTimeslot);
        }

        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);

        Long antecedent = activityEntity.getAntecedentId();
        if (antecedent != null) {
            if (!BeanHelper.bExistedModule(session, ActivityEntity.class, antecedent)) {
                String message = "Activity with id " + antecedent + " does not exist.";
                LogHelper.error(log, functionName, message);
                throw new DataNotExistException(message);
            }
        }

        Set<Long> descendants = activityEntity.getDescendantIds();
        if (descendants != null && !descendants.isEmpty()) {
            if (!BeanHelper.bExistedModules(session, ActivityEntity.class, descendants)) {
                String message = "Activities with ids " + descendants + " do not exist.";
                LogHelper.error(log, functionName, message);
                throw new DataNotExistException(message);
            }
        }

        ActivityEntity entity = new ActivityEntity(activityEntity);
        if (entity.getActivityId() != null) {
            entity.setActivityId(null);
        }

        entity.setStamp(createStamp);

        if (scheduleTimeslot != null) {
            Date start = scheduleTimeslot.getTimeslotStartDateTime();
            Date end = scheduleTimeslot.getTimeslotEndDateTime();
            Long scheduleId = scheduleTimeslot.getScheduleID();
            entity.setPlannedStartDate(start);
            entity.setPlannedEndDate(end);
            entity.setScheduleID(scheduleId);

            if (!overrideCapacity.booleanValue()) {
                TimeslotCapacityType timeslotCapacity = getTimeslotCapacity(uc, scheduleTimeslot);
                Long capacityRemaining = 0L;
                if (timeslotCapacity != null) {
                    capacityRemaining = timeslotCapacity.getCapacityRemaining();
                }
                if (capacityRemaining > 0) {
                    entity.setActiveStatusFlag(true);
                    session.persist(entity);
                } else {
                    String message = "ActivityType cannot be created because the timeslot has reached its capacity." + timeslotCapacity;
                    LogHelper.error(log, functionName, message);
                    throw new ArbutusRuntimeException(message);
                }
            } else {
				/* If an activity is active,
				 * it is taking a spot in the capacity of
				 * a timeslot of the activity’s schedule.
				 */
                entity.setActiveStatusFlag(true);
                session.persist(entity);
            }
        } else {
            Date start = activityEntity.getPlannedStartDate();
            Date end = activityEntity.getPlannedEndDate();
            if (!ActivityCategory.FACOUNT.name().equalsIgnoreCase(activityEntity.getActivityCategory())
                    && (start == null || end == null || start.after(end))) {
                String message = "Both Planned Start Date and Planned End Date must be provided: " + start + ", " + end;
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
            entity.setPlannedStartDate(start);
            entity.setPlannedEndDate(end);
            entity.setScheduleID(null);
            session.persist(entity);
        }

        Long instanceId = entity.getActivityId();

        // Antecedent's descendant
        if (entity.getAntecedentId() != null) {
            Long antecedentId = entity.getAntecedentId();
            ActivityEntity antecedentEntity = BeanHelper.getEntity(session, ActivityEntity.class, antecedentId);
            antecedentEntity.getDescendantIds().add(instanceId);
        }

        // Descendants' antecedent
        Set<Long> descendantIds = entity.getDescendantIds();
        if (descendantIds != null) {
            for (Long desId : descendantIds) {
                ActivityEntity desEntity = BeanHelper.getEntity(session, ActivityEntity.class, desId);
                if (desEntity.getAntecedentId() != null) {
                    String message = "Invalid argument: the decendant has had an antecedent already. " + desEntity;
                    LogHelper.error(log, functionName, message);
                    throw new InvalidInputException(message);
                }
                desEntity.setAntecedentId(instanceId);
            }
        }

        entity = (ActivityEntity) session.get(ActivityEntity.class, instanceId);

        ActivityType ret = ActivityServiceHelper.toActivity(entity);

        if (log.isDebugEnabled()) {
            log.debug("create: end");
        }
        return ret;
    }

    /**
     * Create Activities (a series of new Activities) after schedule change.
     * Provenance
     * <li>A new schedule has been created and is passed in as input;</li>
     * <li>A timetable is generated based on the schedule. The timetable contains a list of next available timeslots to which an activity can be booked. The timeslots start on the schedule’s EffectiveDate;</li>
     * <li>If LookupEndDate is provided,
     * <ol>the timetable must include all timeslots that have start date before the LookupEndDate;</ol>
     * </li>
     * <li>If NumOfNextAvailableTimeslot in the ScheduleType Config Variable is null, LookupEndDate is required;</li>
     * <li>If LookupEndDate is not provided,
     * <ol>ScheduleType Config Variable defined for the input schedule must be used;</ol>
     * <ol>the number of timeslots included in the timetable should be limited to the NumOfNextAvailableTimeslot value defined by the ScheduleType Config VariableType;</ol>
     * </li>
     * <li>A new activity is created for each timeslot contained in the timetable.</li>
     *
     * @param uc            UserContext - Required
     * @param scheduleId    Long - Required
     * @param lookupEndDate Date - Optional
     * @return ActivitiesReturnType
     */
    public Set<ActivityType> create(UserContext uc, Long scheduleId, Date lookupEndDate) {
        if (log.isDebugEnabled()) {
            log.debug("create activities after schedule change: begin");
        }

        String functionName = "create";

        if (scheduleId == null) {
            String message = "Invalid argument: Schedule Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        ScheduleEntity schedule = getActiveScheduleEntity(uc, scheduleId);

        // Get timetable
        ScheduleConfigVariableType config = getScheduleConfigVarByCategory(uc, schedule.getScheduleCategory());
        if (config == null) {
            String message = "Invalid argument: schedule.getScheduleCategory() was not configed.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        if (config.getNumOfNextAvailableTimeslots() == null && lookupEndDate == null) {
            String message = "lookupEndDate is required because numOfNextAvailableTimeslots in ScheduleType Config is null.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Set<ActivityType> ret = new HashSet<ActivityType>();

        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        Date now = DateUtil.getCurrentDate();

        Date lookupDate = schedule.getEffectiveDate();
        if (now.after(schedule.getEffectiveDate())) {
            lookupDate = now;
        }
        Set<ScheduleTimeslotType> timeslots = generateScheduleTimetable(uc, scheduleId, BeanHelper.createDateWithoutTime(lookupDate),
                BeanHelper.createDateWithoutTime(lookupEndDate), false);
        if (timeslots == null || timeslots.size() == 0) {
            String message = "Timeslots cannot be generated by lookuDate, lookupEndDate = " + lookupDate + ", " + lookupEndDate + " and by " + config;
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        for (ScheduleTimeslotType timeslot : timeslots) {

            ActivityEntity act = new ActivityEntity(null,                                // Long activityId
                    null,                                // Long supervisionAssociation
                    Boolean.TRUE,                        // Boolean activeStatusFlag
                    timeslot.getTimeslotStartDateTime(),// Date plannedStartDate
                    timeslot.getTimeslotEndDateTime(),  // Date plannedEndDate
                    schedule.getScheduleCategory(),     // String activityCategory
                    null,                                // Long activityAntecedentId
                    null,                                // Set<Long> activityDescendantIds
                    scheduleId                            // Long scheduleId
            );

            act.setStamp(createStamp);
            ActivityType activity = create(uc, act, timeslot, false);

            if (activity == null) {
                String message = "Invalid Argument: scheduleId, lookupEndDate = " + scheduleId + ", " + lookupEndDate;
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }

            ret.add(activity);

        }

        if (log.isDebugEnabled()) {
            log.debug("create activities after schedule change: end");
        }
        return ret;
    }



    /**
     * Create ScheduleType, a ScheduleType for a particular type of ActivityType.
     * Provenance
     * <li>A schedule is created;</li>
     * <li>A unique schedule identification number is created;</li>
     * <li>Reference to a facility is required. Only references to internal locations that belong to the selected facility are allowed;</li>
     * <li>Reference can be made to only one of the location service, organization service or internal location service at a time.</li>
     *
     * @param uc       UserContext - Required
     * @param schedule ScheduleType - Required
     * @return ScheduleType
     */
    public Schedule createSchedule(UserContext uc, Schedule schedule) {

        if (log.isDebugEnabled()) {
            log.debug("createSchedule: begin");
        }

        String functionName = "createSchedule";

        checkIfExtendedScheduleType(schedule);
        ValidationHelper.validate(schedule);
        verifySchedule(schedule);

        // ForInternalLocationReference cannot be the same as AtInternalLocationReference
        Set<Long> atInLocIds = schedule.getAtInternalLocationIds();
        Long forInLocId = schedule.getForInternalLocationId();
        if (atInLocIds != null && !atInLocIds.isEmpty() && forInLocId != null) {
            if (atInLocIds.contains(forInLocId)) {
                String message = "ForInternalLocationReference cannot be the same as AtInternalLocationReference: " + atInLocIds + " -- " + forInLocId;
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }

        // Reference can be made to only one of the location service, organization service or internal location service at a time
        Set<Long> locIds = schedule.getLocationIds();
        Set<Long> orgIds = schedule.getOrganizationIds();
        if (atInLocIds != null && !atInLocIds.isEmpty() && locIds != null && !locIds.isEmpty()
                || atInLocIds != null && !atInLocIds.isEmpty() && orgIds != null && !orgIds.isEmpty()
                || locIds != null && !locIds.isEmpty() && orgIds != null && !orgIds.isEmpty()) {
            String message = "Reference can be made to only one of the location service, organization service or internal location service at a time: "
                    + " AtInternalLocationIds = " + atInLocIds + ", LocationIds = " + locIds + ", OrganizationIds = " + orgIds;
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        //		RecurrencePatternType pattern = schedule.getRecurrencePattern();
        //		if (pattern != null && !bValidateRecurrencPattern(uc, pattern)) {
        //			String errmsg = "Invalid recurrence pattern: " + schedule.getRecurrencePattern();
        //			log.error(errmsg);
        //			throw new InvalidInputException(errmsg);
        //		}

        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);
        ScheduleEntity scheduleEntity = ActivityServiceHelper.toScheduleEntity(schedule, stamp);
        ValidationHelper.verifyMetaCodes(scheduleEntity, true);
        scheduleEntity.setScheduleIdentification(null);
        /*scheduleEntity.setEffectiveDate(this.getDateAndTime(schedule.getEffectiveDate(), schedule.getScheduleStartTime()));
        scheduleEntity.setTerminationDate(this.getDateAndTime(schedule.getTerminationDate(), schedule.getScheduleEndTime()));*/
        scheduleEntity.setEffectiveDate(BeanHelper.getDateWithoutTime(schedule.getEffectiveDate()));
        if (schedule.getTerminationDate() != null) {
            scheduleEntity.setTerminationDate(BeanHelper.getDateWithoutTime(schedule.getTerminationDate()));
        }

        RecurrencePatternEntity rp = scheduleEntity.getRecurrencePattern();
        if (rp != null) {
            rp.setStamp(stamp);
            scheduleEntity.setRecurrencePattern(rp);
            if (rp instanceof DailyRecurrencePatternEntity) {
                Set<DayEntity> recurrdays = ((DailyRecurrencePatternEntity) rp).getRecurrOnDays();
                if (!BeanHelper.isEmpty(recurrdays)) {
                    for (DayEntity day : recurrdays) {
                        day.setStamp(stamp);
                    }
                }
            } else if (rp instanceof WeeklyRecurrencePatternEntity) {
                Set<WeekdayEntity> weekdays = ((WeeklyRecurrencePatternEntity) rp).getRecurOnWeekdays();
                for (WeekdayEntity weekday : weekdays) {
                    weekday.setStamp(stamp);
                }
            }
        }

        scheduleEntity.setStamp(stamp);
        session.persist(scheduleEntity);
        Schedule ret = ActivityServiceHelper.toSchedule(scheduleEntity);
        // ret.setCapacity(timeslotCapacity.getCapacityRemaining());

        if (log.isDebugEnabled()) {
            log.debug("createSchedule: end");
        }
        return ret;
    }

    /**
     * Create ScheduleType Config Variables, a set of configuration variables for a
     * particular type of schedule.
     * Provenance
     * <li>An instance of schedule config variable is created;</li>
     * <li>Each ScheduleCategory must have a ScheduleType Config Variable;</li>
     * <li>An unique identifier for the schedule config variable is created;</li>
     * <li>Only one instance of schedule config variable is allowed for each ScheduleCategory.</li>
     *
     * @param uc     UserContext - Required
     * @param config ScheduleConfigVariableType - Required
     * @return ScheduleConfigVariableReturnType
     */
    public ScheduleConfigVariableType createScheduleConfigVars(UserContext uc, ScheduleConfigVariableType config) {

        if (log.isDebugEnabled()) {
            log.debug("createScheduleConfigVars: begin");
        }

        String functionName = "createScheduleConfigVars";

        ValidationHelper.validate(config);

        String category = config.getScheduleCategory();
        if (hasCategory(category)) {
            String message = category + " is already persited. -- Only one instance of schedule config variable is allowed for each ScheduleType/ActivityType Category.";
            LogHelper.error(log, functionName, message);
            throw new DataExistException(message);
        }

        // Convert to Entity
        ScheduleConfigVariableEntity entity = ActivityServiceHelper.toScheduleConfigVariableEntity(config);
        ValidationHelper.verifyMetaCodes(entity, true);

        if (entity.getScheduleConfigIdentification() != null) {
            entity.setScheduleConfigIdentification(null);
        }

        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);
        entity.setStamp(stamp);

        session.persist(entity);
        ScheduleConfigVariableType ret = ActivityServiceHelper.toScheduleConfigVariable(entity);

        if (log.isDebugEnabled()) {
            log.debug("createScheduleConfigVars: end");
        }
        return ret;
    }

    @Override
    public List<ScheduleTimeslotType> generateTimeslotList(UserContext uc, Long scheduleId, Date lookupDate, Date lookupEndDate, Long count) {
        if (log.isDebugEnabled()) {
            log.debug("generateTimeslotList by scheduleId: begin");
        }

        String functionName = "generateTimeslotList";

        if (scheduleId == null) {
            String message = "Schedule Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        if (lookupDate == null) {
            String message = "Lookup Date is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        if (lookupEndDate != null && lookupDate.compareTo(lookupEndDate) > 0) {
            String message = "Lookup Date must not be after Lookup End Date.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        List<ScheduleTimeslotType> timeslots = new ArrayList<ScheduleTimeslotType>();

        ScheduleEntity sch = BeanHelper.getEntity(session, ScheduleEntity.class, scheduleId);

        if (lookupEndDate != null) {
            timeslots.addAll(calculateTimeslots(sch, lookupDate, lookupEndDate, null));
        } else {
            ScheduleConfigVariableEntity config = this.getScheduleConfigVarEntityByCategory(uc, sch.getScheduleCategory());
            Long slots = null;
            if (config != null && config.getNumOfNextAvailableTimeslots() != null) {
                slots = config.getNumOfNextAvailableTimeslots();
            } else if (count != null) {
                slots = count;
            } else if (lookupEndDate == null && sch.getTerminationDate() == null) {
                String message = "Invalid argument: scheduleId: " + scheduleId;
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }

            List<ScheduleTimeslotType> schTimeslots = calculateTimeslots(sch, lookupDate, null, slots);
            for (ScheduleTimeslotType timeslot : schTimeslots) {
                if (timeslots.size() < slots) {
                    timeslots.add(timeslot);
                } else {
                    if (count != null && timeslots.size() < count.intValue()) {
                        timeslots.add(timeslot);
                    }
                }
            }

        }

        if (log.isDebugEnabled()) {
            log.debug("generateTimeslotList by scheduleId: end");
        }

        return timeslots;
    }

    @Override
    public TimeslotIterator generateTimeslotIterator(UserContext uc, RecurrencePatternType recurrencePattern, Date lookupDate, Date lookupEndDate, Date endTime,
                                                     Long slots) {
        String functionName = "generateTimeslotIterator";
        if (lookupDate == null) {
            String message = "Invalid Argument: lookupDate is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        if (recurrencePattern == null) {
            String message = "Invalid Argument: recurrencePattern is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        TimeslotIterator ret = null;
        String ical = null;

        Date startDate = this.getDateAndTime(lookupDate, ActivityServiceHelper.getTimeOnly(lookupDate));

        Date endDate = null;
        if (lookupEndDate != null) {
            endDate = BeanHelper.createDateWithoutTime(BeanHelper.nextNthDays(lookupEndDate, 0));
        }

        RecurrencePatternEntity rp = ActivityServiceHelper.toRecurrencePatternEntity(recurrencePattern, null);
        if (rp == null) {
            ical = "RRULE:FREQ=DAILY;COUNT=1";
            ret = generateTimeslots(ical, startDate, lookupEndDate, endTime);
        } else {
            if (rp instanceof OnceRecurrencePatternEntity) {
                Date occurDate = ((OnceRecurrencePatternEntity) rp).getOccurrenceDate();
                if (occurDate.before(startDate) ||
                        lookupDate.after(new DateTime(occurDate).plusHours(22).toDate()) ||
                        endDate != null && occurDate.after(endDate)) {

                    return null;
                } else {
                    ical = "RRULE:FREQ=DAILY;COUNT=1";
                }
                occurDate = this.getDateAndTime(occurDate, ActivityServiceHelper.getTimeOnly(startDate));
                ret = generateTimeslots(ical, occurDate, lookupEndDate, endTime);
                return ret;
            } else if (rp instanceof DailyRecurrencePatternEntity) {
                Set<DayEntity> recurOnDays = ((DailyRecurrencePatternEntity) rp).getRecurrOnDays();
                if (recurOnDays != null && recurOnDays.size() > 0) {
                    ical = "RRULE:FREQ=WEEKLY";

                    if (endDate != null) {
                        ical += ";UNTIL=" + this.lDate(endDate);
                    } else if (slots != null) {
                        ical += ";COUNT=" + slots;
                    }

                    ical += ";BYDAY=";
                    Iterator<DayEntity> dayIter = recurOnDays.iterator();
                    int i = 0;
                    while (dayIter.hasNext()) {
                        if (i > 0) {
                            ical += ",";
                        }
                        DayEntity dayEntity = dayIter.next();
                        String day = dayEntity.getDay();
                        if (day != null && day.trim().length() > 0) {
                            ical += day.substring(0, day.length() - 1);
                        }
                        i++;
                    }

                    Long interval = rp.getRecurrenceInterval();
                    if (interval != null && interval > 1) {
                        ical += ";INTERVAL=" + interval;
                    }
                    // ret = generateTimeslots(ical, startDate, lookupEndDate, endTime);

                } else {
                    ical = "RRULE:FREQ=DAILY";

                    if (endDate != null) {
                        ical += ";UNTIL=" + this.lDate(this.getOneDayPlus(endDate));
                    } else if (slots != null) {
                        ical += ";COUNT=" + slots;
                    }

                    Boolean bWeekday = ((DailyRecurrencePatternEntity) rp).getWeekdayOnlyFlag();
                    if (Boolean.TRUE.equals(bWeekday)) {
                        ical += ";BYDAY=MO,TU,WE,TH,FR";
                    }

                    Long interval = rp.getRecurrenceInterval();
                    if (interval != null && interval > 1) {
                        ical += ";INTERVAL=" + interval;
                    }
                    // ret = generateTimeslots(ical, startDate, lookupEndDate, endTime);
                }

            } else if (rp instanceof WeeklyRecurrencePatternEntity) {
                ical = "RRULE:FREQ=WEEKLY";

                if (endDate != null) {
                    ical += ";UNTIL=" + this.lDate(endDate);
                } else if (slots != null) {
                    ical += ";COUNT=" + slots;
                }

                if (((WeeklyRecurrencePatternEntity) rp).getRecurOnWeekdays().size() > 0) {
                    ical += ";BYDAY=";
                    Iterator<WeekdayEntity> weekdayIter = ((WeeklyRecurrencePatternEntity) rp).getRecurOnWeekdays().iterator();
                    while (weekdayIter.hasNext()) {
                        WeekdayEntity weekdayEntity = weekdayIter.next();
                        String weekday = weekdayEntity.getWeekday();
                        if (weekday != null && weekday.trim().length() > 0) {
                            ical += weekday.substring(0, weekday.length() - 1);
                        }
                        if (weekdayIter.hasNext()) {
                            ical += ",";
                        }
                    }
                }

                Long interval = rp.getRecurrenceInterval();
                if (interval != null && interval > 1) {
                    ical += ";INTERVAL=" + interval;
                }

                // ret = generateTimeslots(ical, startDate, lookupEndDate, endTime);
            } else if (rp instanceof MonthlyRecurrencePatternEntity) {
                ical = "RRULE:FREQ=MONTHLY";

                if (endDate != null) {
                    ical += ";UNTIL=" + this.lDate(endDate);
                } else if (slots != null) {
                    ical += ";COUNT=" + slots;
                }

                Long dayOfMonth = ((MonthlyRecurrencePatternEntity) rp).getDayOfMonth();
                if (dayOfMonth != null && dayOfMonth > 0) {
                    ical += ";BYMONTHDAY=" + dayOfMonth;
                } else {
                    String nthWeekday = ((MonthlyRecurrencePatternEntity) rp).getNthWeekday();
                    String recurOnWeekday = ((MonthlyRecurrencePatternEntity) rp).getRecurOnWeekday();
                    if (recurOnWeekday != null && recurOnWeekday.trim().length() > 0) {
                        recurOnWeekday = recurOnWeekday.substring(0, recurOnWeekday.length() - 1);
                    }
                    if (nthWeekday != null && !nthWeekday.trim().equals("")) {
                        ical += ";BYDAY=" + getNthWeekdayNum(nthWeekday) + recurOnWeekday;
                    } else {
                        ical += ";BYDAY=" + recurOnWeekday;
                    }
                }

                Long interval = rp.getRecurrenceInterval();
                if (interval != null && interval > 1) {
                    ical += ";INTERVAL=" + interval;
                }

                // ret = generateTimeslots(ical, startDate, lookupEndDate, endTime);

            } else if (rp instanceof YearlyRecurrencePatternEntity) {
                ical = "RRULE:FREQ=YEARLY";

                if (endDate != null) {
                    ical += ";UNTIL=" + this.lDate(endDate);
                } else if (slots != null) {
                    ical += ";COUNT=" + slots;
                }

                Long recurOnDay = ((YearlyRecurrencePatternEntity) rp).getRecurOnDay();
                String recurOnMonth = ((YearlyRecurrencePatternEntity) rp).getRecurOnMonth();
                String recurOnWeekday = ((YearlyRecurrencePatternEntity) rp).getRecurOnWeekday();
                String recurOnWeekday2 = null;
                if (recurOnWeekday != null && recurOnWeekday.trim().length() > 0) {
                    recurOnWeekday2 = recurOnWeekday.substring(0, recurOnWeekday.length() - 1);
                }

                String nthWeekday = ((YearlyRecurrencePatternEntity) rp).getNthWeekday();
                if (recurOnDay != null && recurOnDay > 0) {
                    ical += ";BYMONTHDAY=" + recurOnDay;
                    ical += ";BYMONTH=" + getMonthNum(recurOnMonth);
                } else {
                    ical += ";BYDAY=" + getNthWeekdayNum(nthWeekday) + recurOnWeekday2;
                    ical += ";BYMONTH=" + getMonthNum(recurOnMonth);
                }

                Long interval = rp.getRecurrenceInterval();
                if (interval != null && interval > 1) {
                    ical += ";INTERVAL=" + interval;
                }

                // ret = generateTimeslots(ical, startDate, lookupEndDate, endTime);
            }
            //Fixed for WOR-19215, ignore the add/modify dates because it will be handle in program service.
			/*
            if (rp.getIncludeDates() != null) {
                List<IncludeDateEntity> includeDates = new ArrayList<>(rp.getIncludeDates());
                Collections.sort(includeDates);
                String rdate = "RDATE:";
                for (IncludeDateEntity inDate: includeDates) {
                    String inDateTime = dateTimeFormat(inDate.getDate(), inDate.getStartTime());
                    if (!BeanHelper.isEmpty(inDateTime)) {
                        rdate = rdate + inDateTime + ",";
                    }
                }
                if (!rdate.equals("RDATE:")) {
                    ical = ical + "\n" + rdate.substring(0, rdate.lastIndexOf(','));
                }

                List<ExcludeDateEntity> excludeDates = new ArrayList<>(rp.getExcludeDates());
                Collections.sort(includeDates);
                String exdate = "EXDATE:";
                for (ExcludeDateEntity exDate: excludeDates) {
                    String exDateTime = dateTimeFormat(exDate.getDate(), exDate.getStartTime());
                    if (!BeanHelper.isEmpty(exDateTime)) {
                        exdate = exdate + exDateTime + ",";
                    }
                }
                if (!exdate.equals("EXDATE:")) {
                    ical = ical + "\n" + exdate.substring(0, exdate.lastIndexOf(','));
                }
            }
			*/

            ret = generateTimeslots(ical, startDate, lookupEndDate, endTime);
        }

        if (log.isDebugEnabled()) {
            log.debug(ical);
            log.debug("calculateTimeslots: end");
        }

        return ret;
    }

    @Deprecated
    public TimeslotIterator generateTimeslotIterator(UserContext uc, Schedule schedule, Date lookupDate, Date lookupEndDate, Long count) {

        if (log.isDebugEnabled()) {
            log.debug("generateTimeslotIterator: begin");
        }

        ScheduleEntity sch = ActivityServiceHelper.toScheduleEntity(schedule, null);
        sch.setEffectiveDate(this.getDateAndTime(schedule.getEffectiveDate(), schedule.getScheduleStartTime()));
        sch.setTerminationDate(this.getDateAndTime(schedule.getTerminationDate(), schedule.getScheduleEndTime()));

        TimeslotIterator it = calculateTimeslotsIterator(sch, lookupDate, lookupEndDate, count);

        if (log.isDebugEnabled()) {
            log.debug("generateTimeslotIterator: end");
        }

        return it;
    }

    @Override
    public TimeslotIterator generateTimeslotIterator(UserContext uc, Long scheduleId, Date lookupDate, Date lookupEndDate, Long count) {
        if (log.isDebugEnabled()) {
            log.debug("generateTimeslotIterator by scheduleId: begin");
        }

        String functionName = "generateTimeslotIterator";

        if (scheduleId == null) {
            String message = "Schedule Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        if (lookupDate == null) {
            String message = "Lookup Date is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        if (lookupEndDate != null && lookupDate.compareTo(lookupEndDate) > 0) {
            String message = "Lookup Date must not be after Lookup End Date.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        ScheduleEntity sch = BeanHelper.getEntity(session, ScheduleEntity.class, scheduleId);
        return calculateTimeslotsIterator(sch, lookupDate, lookupEndDate, count);
    }

    public Set<ScheduleTimeslotType> generateScheduleTimetable(UserContext uc, Long scheduleId, Date lookupDate, Date lookupEndDate, Boolean overrideCapacity) {

        if (log.isDebugEnabled()) {
            log.debug("generateTimeslotIterator by scheduleId: begin");
        }

        String functionName = "generateTimeslotIterator";

        if (scheduleId == null) {
            String message = "Schedule Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        if (lookupDate == null) {
            String message = "Lookup Date is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        if (lookupEndDate != null && lookupDate.compareTo(lookupEndDate) > 0) {
            String message = "Lookup Date must not be after Lookup End Date.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Set<ScheduleTimeslotType> timeslots = new HashSet<ScheduleTimeslotType>();

        ScheduleEntity sch = BeanHelper.getEntity(session, ScheduleEntity.class, scheduleId);

        if (lookupEndDate != null) {
            timeslots.addAll(calculateTimeslots(sch, lookupDate, lookupEndDate, null));
        } else {
            ScheduleConfigVariableEntity config = this.getScheduleConfigVarEntityByCategory(uc, sch.getScheduleCategory());
            if (config == null) {
                String message = "Invalid argument: scheduleId: " + scheduleId;
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }

            Long slots = config.getNumOfNextAvailableTimeslots();
            Set<ScheduleTimeslotType> schTimeslots = new HashSet<>(calculateTimeslots(sch, lookupDate, null, slots));
            for (ScheduleTimeslotType timeslot : schTimeslots) {
                if (timeslots.size() < slots) {
                    timeslots.add(timeslot);
                } else {
                    if (overrideCapacity.booleanValue()) {
                        timeslots.add(timeslot);
                    }
                }
            }

        }

        if (log.isDebugEnabled()) {
            log.debug("generateTimeslotIterator by scheduleId: end");
        }

        return timeslots;
    }

    /**
     * Generate ScheduleType Timetable.
     * Generate a timetable based on all the schedules defined for a type of
     * activity. The timetable contains a list of next available timeslots to
     * which an activity can book. The number of next available timeslots that
     * are included in the timetable is defined by schedule configuration
     * variables.
     * Provenance
     * <li>Only active schedule can be used to generate the timetable;</li>
     * <li>Only schedules of which the ScheduleCategory  is the same as the ScheduleCategory of the ScheduleType Config Variable Type can be used to generate the timetable;</li>
     * <li>If OverrideCapacity = False, a timeslot should be included in the timetable only if the number of existing activities that have booked to the timeslot is smaller than the capacity of the schedule based on which the timeslot is generated;</li>
     * <li>If OverrideCapcity = True, a timeslot should be included in the timetable regardless the capacity of the schedule;</li>
     * <li>The timetable should include only the timeslots that are dated on or after the LookupDate.</li>
     * <li>LookupDate <= ScheduleType’s TerminationDate;</li>
     * <li>If LookupEndDate is not provided,
     * <ol>ScheduleType Config Variable is required;</ol>
     * <ol>The number of timeslots included in the timetable should be limited to the NumOfNextAvailableTimeslot value defined by the ScheduleType Config VariableType.</ol>
     * </li>
     * <li>If LookupEndDate is provided,
     * <ol>the timetable must include all timeslots that have start date on or after the LookupDate, and before the LookupEndDate;</ol>
     * <ol>NumOfNextAvailableTimeslot is overridden.</ol>
     * </li>
     * <li>If NumOfNextAvailableTimeslot in the ScheduleType Config Variable is null, LookupEndDate is required.</li>
     *
     * @param uc               UserContext - Required
     * @param config           ScheduleConfigVariableType - Required
     * @param lookupDate       java.util.Date - Required
     * @param lookupEndDate    java.util.Date - Optional
     * @param overrideCapacity Boolean - Required
     * @return Set&lt;ScheduleTimeslotType>
     */
    public Set<ScheduleTimeslotType> generateScheduleTimetable(UserContext uc, ScheduleConfigVariableType config, Date lookupDate, Date lookupEndDate,
                                                               Boolean overrideCapacity) {

        if (log.isDebugEnabled()) {
            log.debug("generateTimeslotIterator: begin");
        }

        String functionName = "generateTimeslotIterator";

        ValidationHelper.validate(config);

        Set<ScheduleTimeslotType> timeslots = new HashSet<ScheduleTimeslotType>();
        if (!bValidScheduleConfigVariable(config)) {
            String message = "Invalid argument: ScheduleConfigVariableType:" + config;
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        String category = config.getScheduleCategory();
        Criteria c = session.createCriteria(ScheduleEntity.class);

        // Only schedules of which the ScheduleCategory is the same as
        // the ScheduleCategory of the ScheduleType Config Variable Type
        // can be used to generate the timetable
        c.add(Restrictions.eq("scheduleCategory", category));

        // Only active schedule can be used to generate the timetable
        c.add(Restrictions.or(Restrictions.and(Restrictions.le("effectiveDate", getPresentTime()), Restrictions.isNull("terminationDate")),
                Restrictions.and(Restrictions.le("effectiveDate", getPresentTime()), Restrictions.gt("terminationDate", getPresentTime()))));

        if (lookupEndDate != null) {
            c.add(Restrictions.or(Restrictions.and(Restrictions.isNotNull("terminationDate"), Restrictions.ge("terminationDate", lookupDate)),
                    Restrictions.isNull("terminationDate")));

            @SuppressWarnings("unchecked") Set<ScheduleEntity> schedules = new HashSet<ScheduleEntity>(c.list());
            for (ScheduleEntity sch : schedules) {
                timeslots.addAll(calculateTimeslots(sch, lookupDate, lookupEndDate, null));
            }
        } else {
            Long slots = config.getNumOfNextAvailableTimeslots();

            @SuppressWarnings("unchecked") Set<ScheduleEntity> schedules = new HashSet<ScheduleEntity>(c.list());
            for (ScheduleEntity sch : schedules) {
                Set<ScheduleTimeslotType> schTimeslots = new HashSet<>(calculateTimeslots(sch, lookupDate, null, slots));
                for (ScheduleTimeslotType timeslot : schTimeslots) {
                    if (timeslots.size() < slots) {
                        timeslots.add(timeslot);
                    } else {
                        if (overrideCapacity.booleanValue()) {
                            timeslots.add(timeslot);
                        }
                    }
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("lookupDate, lookupEndDate = " + lookupDate + ", " + lookupEndDate);
            log.debug(config.toString());
            // log.debug(timeslots);
            log.debug("generateTimeslotIterator: end");
        }
        return timeslots;
    }

    @Override
    public Date getIncludeDateTime(UserContext uc, Long scheduleId, Date date) {
        String functionName = "getIncludeDateTime";
        if (scheduleId == null || date == null) {
            String message = "Invalid Argument: scheduleId, date and startTime are required";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ScheduleEntity schedule = BeanHelper.findEntity(session, ScheduleEntity.class, scheduleId);
        if (schedule == null) {
            return null;
        }
        Long patternId = schedule.getRecurrencePattern().getId();
        Criteria criteria = session.createCriteria(RecurrencePatternEntity.class);
        criteria.add(Restrictions.eq("id", patternId));
        criteria.createCriteria("includeDates", "inDates");
        criteria.add(Restrictions.eq("inDates.date", getDateWithoutTime(date)));
        criteria.add(Restrictions.eq("inDates.startTime", DateUtil.getZeroEpochTime(date)));
        criteria.setProjection(Projections.property("inDates.endTime"));
        criteria.setMaxResults(1);
        Date inDate = (Date) criteria.uniqueResult();
        if (inDate == null) {
            return null;
        } else {
            return combineDateAndTime(date, inDate);
        }
    }

    private List<ScheduleTimeslotType> calculateTimeslots(ScheduleEntity schedule, Date lookupDate, Date lookupEndDate, Long slots) {

        if (log.isDebugEnabled()) {
            log.debug("calculateTimeslots: begin");
        }

        List<ScheduleTimeslotType> ret = new ArrayList<ScheduleTimeslotType>();
        String ical = null;
        Date startDate = schedule.getEffectiveDate();
        if (schedule.getEffectiveDate().before(lookupDate)) {
            startDate = this.getDateAndTime(lookupDate, ActivityServiceHelper.getTimeOnly(schedule.getScheduleStartTime()));
        }

        Date endDate = null;
        if (lookupEndDate != null) {
            endDate = BeanHelper.createDateWithoutTime(BeanHelper.nextNthDays(lookupEndDate, 0));
        } else {
            if (schedule.getTerminationDate() != null) {
                endDate = this.getDateAndTime(BeanHelper.nextNthDays(schedule.getTerminationDate(), 0), ActivityServiceHelper.getTimeOnly(schedule.getScheduleEndTime()));
            }
        }

        TimeslotIterator it = calculateTimeslotsIterator(schedule, startDate, endDate, slots);
        if (it != null) {
            while (it.hasNext()) {
                ScheduleTimeslotType scheduleTimeslot = it.next();
                ret.add(scheduleTimeslot);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug(ical);
            log.debug("calculateTimeslots: end");
        }

        return ret;
    }

    private TimeslotIterator calculateTimeslotsIterator(ScheduleEntity schedule, Date lookupDate, Date lookupEndDate, Long slots) {

        if (log.isDebugEnabled()) {
            log.debug("calculateTimeslots: begin");
        }

        TimeslotIterator ret = null;
        String ical = null;

        Date startDate = this.getDateAndTime(schedule.getEffectiveDate(), ActivityServiceHelper.getTimeOnly(schedule.getScheduleStartTime()));
        if (schedule.getEffectiveDate().compareTo(lookupDate) < 0) {
            startDate = this.getDateAndTime(lookupDate, ActivityServiceHelper.getTimeOnly(schedule.getScheduleStartTime()));
        }

        Date endDate = null;
        if (lookupEndDate != null) {
            endDate = BeanHelper.createDateWithoutTime(BeanHelper.nextNthDays(lookupEndDate, 0));
        } else {
            if (schedule.getTerminationDate() != null) {
                endDate = this.getDateAndTime(BeanHelper.nextNthDays(schedule.getTerminationDate(), 0), ActivityServiceHelper.getTimeOnly(schedule.getScheduleEndTime()));
            }
        }

        RecurrencePatternEntity rp = schedule.getRecurrencePattern();
        if (rp == null) {
            ical = "RRULE:FREQ=DAILY;COUNT=1";
            ret = generateTimeslots(ical, startDate, schedule, null);
        } else {
            if (rp instanceof OnceRecurrencePatternEntity) {
                Date occurDate = ((OnceRecurrencePatternEntity) rp).getOccurrenceDate();
                if (occurDate.before(schedule.getEffectiveDate()) ||
                        lookupDate.after(new DateTime(occurDate).plusHours(22).toDate()) ||
                        endDate != null && occurDate.after(endDate)) {

                    return null;
                } else {
                    ical = "RRULE:FREQ=DAILY;COUNT=1";
                }
                occurDate = this.getDateAndTime(occurDate, ActivityServiceHelper.getTimeOnly(schedule.getScheduleStartTime()));
                ret = generateTimeslots(ical, occurDate, schedule, null);
                return ret;
            } else if (rp instanceof DailyRecurrencePatternEntity) {
                Set<DayEntity> recurOnDays = ((DailyRecurrencePatternEntity) rp).getRecurrOnDays();
                if (recurOnDays != null && recurOnDays.size() > 0) {
                    ical = "RRULE:FREQ=WEEKLY";

                    if (endDate != null) {
                        ical += ";UNTIL=" + this.lDate(endDate);
                    } else if (slots != null) {
                        ical += ";COUNT=" + slots;
                    }

                    ical += ";BYDAY=";
                    Iterator<DayEntity> dayIter = recurOnDays.iterator();
                    int i = 0;
                    while (dayIter.hasNext()) {
                        if (i > 0) {
                            ical += ",";
                        }
                        DayEntity dayEntity = dayIter.next();
                        String day = dayEntity.getDay();
                        if (day != null && day.trim().length() > 0) {
                            ical += day.substring(0, day.length() - 1);
                        }
                        i++;
                    }

                    Long interval = rp.getRecurrenceInterval();
                    if (interval != null && interval > 1) {
                        ical += ";INTERVAL=" + interval;
                    }
                    // ret = generateTimeslots(ical, startDate, schedule, slots);

                } else {
                    ical = "RRULE:FREQ=DAILY";

                    if (endDate != null) {
                        ical += ";UNTIL=" + this.lDate(this.getOneDayPlus(endDate));
                    } else if (slots != null) {
                        ical += ";COUNT=" + slots;
                    }

                    Boolean bWeekday = ((DailyRecurrencePatternEntity) rp).getWeekdayOnlyFlag();
                    if (Boolean.TRUE.equals(bWeekday)) {
                        ical += ";BYDAY=MO,TU,WE,TH,FR";
                    }

                    Long interval = rp.getRecurrenceInterval();
                    if (interval != null && interval > 1) {
                        ical += ";INTERVAL=" + interval;
                    }
                    // ret = generateTimeslots(ical, startDate, schedule, slots);
                }

            } else if (rp instanceof WeeklyRecurrencePatternEntity) {
                ical = "RRULE:FREQ=WEEKLY";

                if (endDate != null) {
                    ical += ";UNTIL=" + this.lDate(endDate);
                } else if (slots != null) {
                    ical += ";COUNT=" + slots;
                }

                if (((WeeklyRecurrencePatternEntity) rp).getRecurOnWeekdays().size() > 0) {
                    ical += ";BYDAY=";
                    Iterator<WeekdayEntity> weekdayIter = ((WeeklyRecurrencePatternEntity) rp).getRecurOnWeekdays().iterator();
                    while (weekdayIter.hasNext()) {
                        WeekdayEntity weekdayEntity = weekdayIter.next();
                        String weekday = weekdayEntity.getWeekday();
                        if (weekday != null && weekday.trim().length() > 0) {
                            ical += weekday.substring(0, weekday.length() - 1);
                        }
                        if (weekdayIter.hasNext()) {
                            ical += ",";
                        }
                    }
                }

                Long interval = rp.getRecurrenceInterval();
                if (interval != null && interval > 1) {
                    ical += ";INTERVAL=" + interval;
                }

                // ret = generateTimeslots(ical, startDate, schedule, slots);
            } else if (rp instanceof MonthlyRecurrencePatternEntity) {
                ical = "RRULE:FREQ=MONTHLY";

                if (endDate != null) {
                    ical += ";UNTIL=" + this.lDate(endDate);
                } else if (slots != null) {
                    ical += ";COUNT=" + slots;
                }

                Long dayOfMonth = ((MonthlyRecurrencePatternEntity) rp).getDayOfMonth();
                if (dayOfMonth != null && dayOfMonth > 0) {
                    ical += ";BYMONTHDAY=" + dayOfMonth;
                } else {
                    String nthWeekday = ((MonthlyRecurrencePatternEntity) rp).getNthWeekday();
                    String recurOnWeekday = ((MonthlyRecurrencePatternEntity) rp).getRecurOnWeekday();
                    if (recurOnWeekday != null && recurOnWeekday.trim().length() > 0) {
                        recurOnWeekday = recurOnWeekday.substring(0, recurOnWeekday.length() - 1);
                    }
                    if (nthWeekday != null && !nthWeekday.trim().equals("")) {
                        ical += ";BYDAY=" + getNthWeekdayNum(nthWeekday) + recurOnWeekday;
                    } else {
                        ical += ";BYDAY=" + recurOnWeekday;
                    }
                }

                Long interval = rp.getRecurrenceInterval();
                if (interval != null && interval > 1) {
                    ical += ";INTERVAL=" + interval;
                }

                // ret = generateTimeslots(ical, startDate, schedule, slots);

            } else if (rp instanceof YearlyRecurrencePatternEntity) {
                ical = "RRULE:FREQ=YEARLY";

                if (endDate != null) {
                    ical += ";UNTIL=" + this.lDate(endDate);
                } else if (slots != null) {
                    ical += ";COUNT=" + slots;
                }

                Long recurOnDay = ((YearlyRecurrencePatternEntity) rp).getRecurOnDay();
                String recurOnMonth = ((YearlyRecurrencePatternEntity) rp).getRecurOnMonth();
                String recurOnWeekday = ((YearlyRecurrencePatternEntity) rp).getRecurOnWeekday();
                String recurOnWeekday2 = null;
                if (recurOnWeekday != null && recurOnWeekday.trim().length() > 0) {
                    recurOnWeekday2 = recurOnWeekday.substring(0, recurOnWeekday.length() - 1);
                }

                String nthWeekday = ((YearlyRecurrencePatternEntity) rp).getNthWeekday();
                if (recurOnDay != null && recurOnDay > 0) {
                    ical += ";BYMONTHDAY=" + recurOnDay;
                    ical += ";BYMONTH=" + getMonthNum(recurOnMonth);
                } else {
                    ical += ";BYDAY=" + getNthWeekdayNum(nthWeekday) + recurOnWeekday2;
                    ical += ";BYMONTH=" + getMonthNum(recurOnMonth);
                }

                Long interval = rp.getRecurrenceInterval();
                if (interval != null && interval > 1) {
                    ical += ";INTERVAL=" + interval;
                }

                // ret = generateTimeslots(ical, startDate, schedule, slots);
            }

            //fix WOR-19215, just consider the pattern dates.
			/*
            if (rp.getIncludeDates() != null) {
                List<IncludeDateEntity> includeDates = new ArrayList<>(rp.getIncludeDates());
                Collections.sort(includeDates);
                String rdate = "RDATE:";
                for (IncludeDateEntity inDate: includeDates) {
                    String inDateTime = dateTimeFormat(inDate.getDate(), inDate.getStartTime());
                    if (!BeanHelper.isEmpty(inDateTime)) {
                        rdate = rdate + inDateTime + ",";
                    }
                }
                if (!rdate.equals("RDATE:")) {
                    ical = ical + "\n" + rdate.substring(0, rdate.lastIndexOf(','));
                }

                List<ExcludeDateEntity> excludeDates = new ArrayList<>(rp.getExcludeDates());
                Collections.sort(excludeDates);
                String exdate = "EXDATE:";
                for (ExcludeDateEntity exDate: excludeDates) {
                    String exDateTime = dateTimeFormat(exDate.getDate(), exDate.getStartTime());
                    if (!BeanHelper.isEmpty(exDateTime)) {
                        exdate = exdate + exDateTime + ",";
                    }
                }
                if (!exdate.equals("EXDATE:")) {
                    ical = ical + "\n" + exdate.substring(0, exdate.lastIndexOf(','));
                }
            }
            */
            // log.info("Start Date:" + startDate + "\n" + ical);
            ret = generateTimeslots(ical, startDate, schedule, slots);
        }

        if (log.isDebugEnabled()) {
            log.debug(ical);
            log.debug("calculateTimeslots: end");
        }

        return ret;
    }

    private TimeslotIterator generateTimeslots(String ical, Date startDate, ScheduleEntity schedule, Long count) {

        if (log.isDebugEnabled()) {
            log.debug("generateTimeslots: begin");
        }

        TimeslotIterator it = null;
        TimeZone tzid = TimeZone.getDefault();

        Date timsslotEndDateTime = null;
        if (schedule.getTerminationDate() != null) {
            timsslotEndDateTime = this.getDateAndTime(schedule.getTerminationDate(), ActivityServiceHelper.getTimeOnly(schedule.getScheduleEndTime()));
        }
        try {
            TimeslotIterable di = TimeslotIteratorFactory.createIterable(ical, startDate, // schedule.getEffectiveDate(),
                    tzid, true, schedule.getScheduleIdentification(), timsslotEndDateTime, schedule.getScheduleEndTime());
            it = di.iterator();
        } catch (java.text.ParseException e) {
            log.error("Invalid argument: ical: " + ical);
            log.error("Timezone: " + tzid);
            throw new InvalidInputException("Invalid argument: invalid schedule for generating ScheduleType Timeslots: " + schedule);
        }

        if (log.isDebugEnabled()) {
            log.debug("generateTimeslots: end");
        }

        return it;
    }

    private TimeslotIterator generateTimeslots(String ical, Date startDate, Date endDate, Date endTime) {
        if (log.isDebugEnabled()) {
            log.debug("generateTimeslots: begin");
        }

        TimeslotIterator it = null;
        TimeZone tzid = TimeZone.getDefault();

        try {
            TimeslotIterable di = TimeslotIteratorFactory.createIterable(ical, startDate, // schedule.getEffectiveDate(),
                    tzid, true, null, endDate, endTime);
            it = di.iterator();
        } catch (java.text.ParseException e) {
            log.error("Invalid argument: ical: " + ical);
            log.error("Timezone: " + tzid);
            throw new InvalidInputException("Invalid argument: invalid recurrence pattern");
        }

        if (log.isDebugEnabled()) {
            log.debug("generateTimeslots: end");
        }

        return it;
    }


    private Long getNthWeekdayNum(String nthWeekday) {
        return Long.valueOf(NthWeekday.valueOf(nthWeekday).ordinal() + 1);
    }


    private Long getMonthNum(String recurOnMonth) {
        return Long.valueOf(Month.valueOf(recurOnMonth).ordinal() + 1);
    }



    private Long lDate(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int iYear = c.get(Calendar.YEAR);
        int iMonth = c.get(Calendar.MONTH) + 1;
        int iDay = c.get(Calendar.DAY_OF_MONTH);

        String sDate = "" + iYear;
        sDate += iMonth < 10 ? "0" + iMonth : iMonth;
        sDate += iDay < 10 ? "0" + iDay : iDay;

        return Long.valueOf(sDate);
    }



    /**
     * <p>
     * Update an ActivityType. Data Transfer Object properties that cannot be
     * updated will be ignored. No error will be thrown.  Services will validate
     * against a predefined uniqueness criteria. This criteria will be checked
     * by the service during update and return an error if it fails.
     * Associations can be added during an update but they cannot be removed.
     * If an association must be removed the deleteAssociation method must be invoked.
     * </p>
     * <p>
     * Returns a null and error code in return type if instance cannot be
     * updated.
     * </p>
     * Provenance
     * <li>An activity must already exist;</li>
     * <li>ActivityType identification cannot be updated;</li>
     * <li>ActivityType Planned Start Date, Planned End Date, ActivityType Antecedent Reference, ActivityType Descendants Reference, ScheduleID can be updated;</li>
     * <li>If schedule timeslot is provided,
     * <ol>the schedule timeslot’s start date/time and end date/time are copied to the activity’s planned start date and end date;</ol>
     * <ol>the ScheduleID of the timeslot is copied to the activity;</ol>
     * <ol>activity cannot be updated if the timeslot has reached its capacity, unless OverrideCapacity = True.</ol>
     * </li>
     * <li>If schedule timeslot is not provided,
     * <ol>User must manually update the activity’s planned start and end date;</ol>
     * <ol>the activity’s ScheduleID must be updated to null.</ol>
     * </li>
     *
     * @param uc               UserContext - Required
     * @param activity         ActivityType - Required
     * @param scheduleTimeslot ScheduleTimeslotType - Optional
     * @param overrideCapacity Boolean - Required
     * @return ActivityType
     */
    public ActivityType update(UserContext uc, ActivityType activity, ScheduleTimeslotType scheduleTimeslot, Boolean overrideCapacity) {

        if (log.isDebugEnabled()) {
            log.debug("update: begin");
        }

        String functionName = "update";

        Long id = activity.getActivityIdentification();
        if (id == null) {
            String message = "Activity Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        if (overrideCapacity == null) {
            String message = "overrideCapacity is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(activity);
        if (scheduleTimeslot != null) {
            ValidationHelper.validate(scheduleTimeslot);
        }

        ActivityEntity activityEntity = BeanHelper.getEntity(session, ActivityEntity.class, id);
        ValidationHelper.verifyMetaCodes(activityEntity, false);

        ActivityEntity u = ActivityServiceHelper.toActivityEntity(activity);

        Long antecedent = u.getAntecedentId();
        if (antecedent != null) {
            ActivityEntity antecedentEntity = BeanHelper.getEntity(session, ActivityEntity.class, antecedent);
            antecedentEntity.getDescendantIds().add(id);
            activityEntity.setAntecedentId(antecedent);
        }

        Set<Long> descendants = u.getDescendantIds();
        if (descendants != null && descendants.size() > 0) {
            for (Long desId : descendants) {
                if (!BeanHelper.bExistedModule(session, ActivityEntity.class, desId)) {
                    String message = "Activity with ids " + descendants + " do not exist.";
                    LogHelper.error(log, functionName, message);
                    throw new DataNotExistException(message);
                }
                ActivityEntity descendantEntity = BeanHelper.getEntity(session, ActivityEntity.class, desId);
                if (descendantEntity.getAntecedentId() != null) {
                    ActivityEntity antEntity = BeanHelper.getEntity(session, ActivityEntity.class, descendantEntity.getAntecedentId());
                    antEntity.getDescendantIds().remove(descendantEntity.getAntecedentId());
                }
                descendantEntity.setAntecedentId(id);
                activityEntity.getDescendantIds().add(desId);
            }
        }

        Date start, end;

        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        StampEntity modifyStamp = BeanHelper.getModifyStamp(uc, context, createStamp);

        if (scheduleTimeslot != null) {
            if (Boolean.FALSE.equals(overrideCapacity)) {
                TimeslotCapacityType timeslotCapacity = getTimeslotCapacity(uc, scheduleTimeslot);
                Long capacityRemaining = 0L;
                if (timeslotCapacity != null) {
                    capacityRemaining = timeslotCapacity.getCapacityRemaining();
                }

                if (capacityRemaining == 0L) {
                    String message = "ActivityType cannot be updated because schedule timeslot has reached its capacity.";
                    LogHelper.error(log, functionName, message);
                    throw new InvalidInputException(message);
                }
            }

            start = scheduleTimeslot.getTimeslotStartDateTime();
            end = scheduleTimeslot.getTimeslotEndDateTime();
            activityEntity.setScheduleID(scheduleTimeslot.getScheduleID());
        } else {
            start = u.getPlannedStartDate();
            end = u.getPlannedEndDate();
            activityEntity.setScheduleID(u.getScheduleID());
        }

        activityEntity.setPlannedStartDate(start);
        activityEntity.setPlannedEndDate(end);

        activityEntity.setActiveStatusFlag(u.getActiveStatusFlag());

        //Only update it when supervisionAssoc is null in DB.
        if (activityEntity.getSupervisionId() == null) {
            activityEntity.setSupervisionId(u.getSupervisionId());
        }

        activityEntity.setStamp(modifyStamp);

        ActivityEntity updated = (ActivityEntity) session.merge(activityEntity);

        session.flush();
        session.clear();

        ActivityType ret = ActivityServiceHelper.toActivity(updated);

        if (log.isDebugEnabled()) {
            log.debug("update: end");
        }

        return ret;
    }

    /**
     * Update ScheduleType
     * Provenance
     * <li>The schedule must exist;</li>
     * <li>The ScheduleIdentification cannot be updated;</li>
     * <li>If schedule has been associated to an activity, ScheduledCategory, FacilityReference cannot be updated;</li>
     * <li>Reference can be made to only one of the location service, organization service or internal location service at a time.</li>
     *
     * @param uc       UserContext - Required
     * @param schedule ScheduleType - Required
     * @return ScheduleType
     */
    public Schedule updateSchedule(UserContext uc, Schedule schedule) {

        if (log.isDebugEnabled()) {
            log.debug("updateSchedule: begin");
        }

        String functionName = "updateSchedule";

        if (schedule == null || schedule.getScheduleIdentification() == null) {
            String message = "Schedule/Id must not be null.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        checkIfExtendedScheduleType(schedule);
        ValidationHelper.validate(schedule);

        // ForInternalLocationReference cannot be the same as AtInternalLocationReference
        Set<Long> atInLocIds = schedule.getAtInternalLocationIds();
        Long forInLocId = schedule.getForInternalLocationId();
        if (atInLocIds != null && !atInLocIds.isEmpty() && forInLocId != null) {
            if (atInLocIds.contains(forInLocId)) {
                String message = "ForInternalLocationReference cannot be the same as AtInternalLocationReference: " + atInLocIds + " -- " + forInLocId;
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }

        // Reference can be made to only one of the location service, organization service or internal location service at a time
        Set<Long> locIds = schedule.getLocationIds();
        Set<Long> orgIds = schedule.getOrganizationIds();
        if (atInLocIds != null && !atInLocIds.isEmpty() && locIds != null && !locIds.isEmpty()
                || atInLocIds != null && !atInLocIds.isEmpty() && orgIds != null && !orgIds.isEmpty()
                || locIds != null && !locIds.isEmpty() && orgIds != null && !orgIds.isEmpty()) {
            String message = "Reference can be made to only one of the location service, organization service or internal location service at a time: "
                    + " AtInternalLocationIds = " + atInLocIds + ", LocationIds = " + locIds + ", OrganizationIds = " + orgIds;
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        RecurrencePatternType pattern = schedule.getRecurrencePattern();
        if (pattern != null && !bValidateRecurrencPattern(uc, pattern)) {
            String message = "Invalid recurrence pattern: " + schedule.getRecurrencePattern();
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Long id = schedule.getScheduleIdentification();
        ScheduleEntity e = BeanHelper.getEntity(session, ScheduleEntity.class, id);

        StampEntity modifyStamp = BeanHelper.getModifyStamp(uc, context, e.getStamp());

        ScheduleEntity u = ActivityServiceHelper.toScheduleEntity(schedule, modifyStamp);
        ValidationHelper.verifyMetaCodes(u, false);

        // If schedule has been associated to an activity, ScheduledCategory,
        // FacilityAssociation cannot be updated.
        if (!hasAssociatedToActivity(id)) {
            e.setFacilityId(u.getFacilityId());
            e.setScheduleCategory(u.getScheduleCategory());
        }
        e.setCapacity(u.getCapacity());
        /*e.setEffectiveDate(getDateAndTime(u.getEffectiveDate(), schedule.getScheduleStartTime()));
        e.setTerminationDate(getDateAndTime(u.getTerminationDate(), schedule.getScheduleEndTime()));*/
        e.setEffectiveDate(BeanHelper.getDateWithoutTime(u.getEffectiveDate()));
        if (u.getTerminationDate() != null) {
            e.setTerminationDate(BeanHelper.getDateWithoutTime(u.getTerminationDate()));
        } else {
            e.setTerminationDate(null);
        }
        e.setScheduleStartTime(u.getScheduleStartTime());
        e.setScheduleEndTime(u.getScheduleEndTime());
        e.setFacilityScheduleReason(u.getFacilityScheduleReason());
        e.setForInternalLocId(u.getForInternalLocId());
        e.getAtInternalLocIds().clear();
        for (ScheduleInternalLocationEntity il : u.getAtInternalLocIds()) {
            e.addAtInternalLocId(il);
        }
        e.getLocationIds().clear();
        for (ScheduleLocationEntity l : u.getLocationIds()) {
            e.addLocationId(l);
        }
        e.getOrganizationIds().clear();
        for (ScheduleOrganizationEntity o : u.getOrganizationIds()) {
            e.addOrganizationId(o);
        }

        if (u.getRecurrencePattern() != null) {
            StampEntity stamp;
            if (e.getRecurrencePattern() != null) {
                stamp = e.getRecurrencePattern().getStamp();
                if (stamp != null) {
                    stamp = BeanHelper.getModifyStamp(uc, context, stamp);
                } else {
                    stamp = BeanHelper.getCreateStamp(uc, context);
                }
                u.getRecurrencePattern().setStamp(stamp);
                if (u.getRecurrencePattern() instanceof DailyRecurrencePatternEntity) {
                    Set<DayEntity> recurrdays = ((DailyRecurrencePatternEntity) u.getRecurrencePattern()).getRecurrOnDays();
                    if (!BeanHelper.isEmpty(recurrdays)) {
                        for (DayEntity day : recurrdays) {
                            day.setStamp(stamp);
                        }
                    }
                }
                if (u.getRecurrencePattern() instanceof WeeklyRecurrencePatternEntity) {
                    Set<WeekdayEntity> weekdayEntities = ((WeeklyRecurrencePatternEntity) u.getRecurrencePattern()).getRecurOnWeekdays();
                    for (WeekdayEntity weekday : weekdayEntities) {
                        weekday.setStamp(stamp);
                    }
                }
            } else {
                stamp = BeanHelper.getCreateStamp(uc, context);
                u.getRecurrencePattern().setStamp(stamp);
                if (u.getRecurrencePattern() instanceof DailyRecurrencePatternEntity) {
                    Set<DayEntity> recurrdays = ((DailyRecurrencePatternEntity) u.getRecurrencePattern()).getRecurrOnDays();
                    if (!BeanHelper.isEmpty(recurrdays)) {
                        for (DayEntity day : recurrdays) {
                            day.setStamp(stamp);
                        }
                    }
                }
                if (u.getRecurrencePattern() instanceof WeeklyRecurrencePatternEntity) {
                    Set<WeekdayEntity> weekdayEntities = ((WeeklyRecurrencePatternEntity) u.getRecurrencePattern()).getRecurOnWeekdays();
                    for (WeekdayEntity weekday : weekdayEntities) {
                        weekday.setStamp(stamp);
                    }
                }
            }
        }
        RecurrencePatternEntity rp = e.getRecurrencePattern();
        if (rp != null) {
            session.delete(rp);
        }
        e.setRecurrencePattern(u.getRecurrencePattern());

        if (e.getStamp() != null) {
            modifyStamp = BeanHelper.getModifyStamp(uc, context, e.getStamp());
        } else {
            modifyStamp = BeanHelper.getCreateStamp(uc, context);
        }
        e.setStamp(modifyStamp);

        ScheduleEntity updated = (ScheduleEntity) session.merge(e);
        Schedule ret = ActivityServiceHelper.toSchedule(updated);

        if (log.isDebugEnabled()) {
            log.debug("updateSchedule: end");
        }
        return ret;
    }

    /**
     * Update ScheduleType Config Variable, a set of configuration variable for a
     * particular type of schedule.
     * Provenance
     * <li>The schedule config variable already exists for a particular type of schedule;</li>
     * <li>ScheduleConfigIdentification cannot be updated;</li>
     * <li>ScheduleCategory cannot be updated.</li>
     *
     * @param uc     UserContext - Required
     * @param config ScheduleConfigVariableType - Required
     * @return ScheduleConfigVariableType
     */
    public ScheduleConfigVariableType updateScheduleConfigVars(UserContext uc, ScheduleConfigVariableType config) {

        if (log.isDebugEnabled()) {
            log.debug("updateScheduleConfigVars: begin");
        }

        String functionName = "updateScheduleConfigVars";

        ValidationHelper.validate(config);

        Long id = config.getScheduleConfigIdentification();
        if (id == null) {
            String message = "Schedule Config Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ScheduleConfigVariableEntity e = BeanHelper.getEntity(session, ScheduleConfigVariableEntity.class, id);

        e.setNumOfNextAvailableTimeslots(config.getNumOfNextAvailableTimeslots());
        session.merge(e);
        ScheduleConfigVariableType ret = ActivityServiceHelper.toScheduleConfigVariable(e);

        if (log.isDebugEnabled()) {
            log.debug("updateScheduleConfigVars: end");
        }
        return ret;
    }

    /**
     * Search for a set of Activities based on a specified criteria. The ids can
     * be used to search only within the specified subset of Id's. List what
     * Search options are available in comments, EXACT is default of search
     * match mode.
     * <p>
     * Returns a null and error code in return type if search cannot be
     * completed.
     * </p>
     * Provenance
     * <li>At least one of the search criteria must present when search activity is performed;</li>
     * <li>Allowable search criteria are activity identification, planned start date, planned end date, activity associations, activity antecedent association, activity descendants association;</li>
     * <li>If ActiveStatusFlag is null, return both active and inactive activities.</li>
     * <p>Text item/String will support the asterisk as a wildcard (that matches zero or more characters).
     * <p>Text item/String without a wildcard will be matched as an exact string match.
     * <p>Set Modes support:
     * <pre>
     * 	<li>ONLY-- SET in search criterion will match sets that only have an exact set match.</li>
     * 	<li>ANY-- SET in search criterion will match sets that have at least one of the items.</li>
     *  <li>ALL-- SET in search criterion will match sets that have at least the specified subset.</li>
     * </pre>
     *
     * @param uc     UserContext - Required
     * @param ids    java.util.Set<java.lang.Long> - Optional if null search will
     *               occur across all activities.
     * @param search ActivitySearchType - Required
     *               String, Default = "ALL" (ONLY, ANY, ALL)
     * @return ActivitiesReturnType
     */
    public ActivitiesReturnType search(UserContext uc, Set<Long> ids, ActivitySearchType search, Long startIndex, Long resultSize, String resultOrder) {

        if (log.isDebugEnabled()) {
            log.debug("search: begin");
        }

        String functionName = "search";

        if ((ids == null || ids.isEmpty()) && search == null) {
            String message = "At least one argument must not be null for search Acitivities.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        if (search != null) {
            ValidationHelper.validateSearchType(search);
        }

        if (resultSize == null || resultSize > SEARCH_MAX_LIMIT) {
            resultSize = Long.valueOf(SEARCH_MAX_LIMIT);
        }

        Criteria c = session.createCriteria(ActivityEntity.class);

        if (ids != null && ids.size() > 0) {
            c.add(Restrictions.in("activityId", ids));
        }

        Set<Long> supervisionIdentifications = search.getSupervisionIds();
        if (supervisionIdentifications != null && supervisionIdentifications.size() > 0) {
            c.add(Restrictions.in("supervisionId", supervisionIdentifications));
        }

        Date fromPlannedStartDate = search.getFromPlannedStartDate();
        if (fromPlannedStartDate != null) {
            c.add(Restrictions.ge("plannedStartDate", fromPlannedStartDate));
        }

        Date toPlannedStartDate = search.getToPlannedStartDate();
        if (toPlannedStartDate != null) {
            c.add(Restrictions.le("plannedStartDate", toPlannedStartDate));
        }

        Date fromPlannedEndDate = search.getFromPlannedEndDate();
        if (fromPlannedEndDate != null) {
            c.add(Restrictions.ge("plannedEndDate", fromPlannedEndDate));
        }

        Date toPlannedEndDate = search.getToPlannedEndDate();
        if (toPlannedEndDate != null) {
            c.add(Restrictions.le("plannedEndDate", toPlannedEndDate));
        }

        Long scheduleID = search.getScheduleID();
        if (scheduleID != null) {
            c.add(Restrictions.eq("scheduleID", scheduleID));
        }

        String activityCategory = search.getActivityCategory();
        if (activityCategory != null) {
            c.add(Restrictions.ilike("activityCategory", activityCategory));
        }

        Long antecedent = search.getActivityAntecedentId();
        if (antecedent != null) {
            c.add(Restrictions.eq("antecedentId", antecedent));
        }

        //        Set<Long> descendantIds = search.getActivityDescendantIds();
        //        if (descendantIds != null && !descendantIds.isEmpty()) {
        //	        c.add(Restrictions.in("descendantIds", descendantIds));
        //        }

        Boolean statusFlag = search.getActiveStatusFlag();
        if (statusFlag != null) {
            c.add(Restrictions.eq("activeStatusFlag", statusFlag));
        }

        // Long totalSize = (Long)c.setProjection(Projections.rowCount()).uniqueResult();
        Long totalSize = (Long) c.setProjection(Projections.countDistinct("activityId")).uniqueResult();
        c.setProjection(null);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            c.setFirstResult(startIndex.intValue());
            c.setMaxResults(resultSize.intValue());
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                c.addOrder(ord);
            }
        }

        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        @SuppressWarnings("unchecked") List<ActivityEntity> lst = c.list();

        ActivitiesReturnType ret = new ActivitiesReturnType();
        ret.setActivities(ActivityServiceHelper.toActivitiesList(lst));
        ret.setTotalSize(totalSize == null ? 0L : totalSize);

        if (log.isDebugEnabled()) {
            log.debug("search: end");
        }
        return ret;
    }

    /**
     * Search ScheduleType
     * Provenance
     * <li>At least one of the search criteria must present when search activity is performed;</li>
     * <li>Allowable search criteria are:
     * <ol>ScheduleCategory, FacilityReference, InternalLocationReference, Capacity, EffectiveDate, TerminationDate, ActiveScheduleFlag, ScheduleStartTime, ScheduleEndTime, RecurrencePatternType and RecurrencePatternType extensions;</ol>
     * <li>If ActiveScheduleFlag = null, return both active and inactive schedules.</li>
     *
     * @param uc     UserContext - Required
     * @param search ScheduleSearchType - Required
     * @return SchedulesReturnType
     */
    public SchedulesReturnType searchSchedule(UserContext uc, Set<Long> ids, ScheduleSearchType search, Long startIndex, Long resultSize, String resultOrder) {

        if (log.isDebugEnabled()) {
            log.debug("searchSchedule: begin");
        }

        String functionName = "searchSchedule";

        if ((ids == null || ids.isEmpty()) && search == null) {
            String message = "At least one argument must not be null for search schedules.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        if (search != null) {
            ValidationHelper.validateSearchType(search);
        }

        if (search.getFromScheduleStartTime() != null && !isValidatedScheduleTime(search.getFromScheduleStartTime())) {
            throw new InvalidInputException("Invalid argument: " + search.getFromScheduleStartTime());
        }
        if (search.getFromScheduleEndTime() != null && !isValidatedScheduleTime(search.getFromScheduleEndTime())) {
            throw new InvalidInputException("Invalid argument: " + search.getFromScheduleEndTime());
        }

        if (resultSize == null || resultSize > SEARCH_MAX_LIMIT) {
            resultSize = Long.valueOf(SEARCH_MAX_LIMIT);
        }

        Criteria c = session.createCriteria(ScheduleEntity.class);

        if (ids != null && !ids.isEmpty()) {
            c.add(Restrictions.in("scheduleIdentification", ids));
        }

        String scheduleCategory = search.getScheduleCategory();
        if (scheduleCategory != null) {
            c.add(Restrictions.ilike("scheduleCategory", scheduleCategory));
        }

        String scheduleReason = search.getFacilityScheduleReason();
        if (scheduleReason != null) {
            c.add(Restrictions.ilike("facilityScheduleReason", scheduleReason));
        }

        Long capacity = search.getCapacity();
        if (capacity != null) {
            c.add(Restrictions.eq("capacity", capacity));
        }

        // Date fromEffectiveDate = this.getDateAndTime(search.getFromEffectiveDate(), search.getFromScheduleStartTime());
        // Date toEffectiveDate = this.getDateAndTime(search.getToEffectiveDate(), search.getToScheduleEndTime() == null ? "24:00" : search.getToScheduleEndTime());
        Date fromEffectiveDate = this.getDateWithoutTime(search.getFromEffectiveDate());
        Date toEffectiveDate = this.getOneDayPlus(search.getToEffectiveDate());
        if (fromEffectiveDate != null) {
            c.add(Restrictions.ge("effectiveDate", fromEffectiveDate));
        }
        if (toEffectiveDate != null) {
            c.add(Restrictions.lt("effectiveDate", toEffectiveDate));
        }

        // Date fromTerminationDate = this.getDateAndTime(search.getFromTerminationDate(), search.getFromScheduleStartTime());
        // Date toTerminationDate = this.getDateAndTime(search.getToTerminationDate(), search.getToScheduleEndTime() == null ? "24:00" : search.getToScheduleEndTime());
        Date fromTerminationDate = this.getDateWithoutTime(search.getFromTerminationDate());
        Date toTerminationDate = this.getOneDayPlus(search.getToTerminationDate());
        if (fromTerminationDate != null && toTerminationDate == null) {
            c.add(Restrictions.ge("terminationDate", fromTerminationDate));
        } else if (fromTerminationDate == null && toTerminationDate != null) {
            c.add(Restrictions.lt("terminationDate", toTerminationDate));
        } else if (fromTerminationDate != null && toTerminationDate != null) {
            c.add(Restrictions.and(Restrictions.ge("terminationDate", fromTerminationDate), Restrictions.lt("terminationDate", toTerminationDate)));
        }

        //Boolean flag = search.isActiveScheduleFlag();
        Boolean flag = search.getActiveScheduleFlag();
        if (flag != null) {
            Date today = getToday();
            if (flag.booleanValue()) {
                c.add(Restrictions.or(Restrictions.and(Restrictions.le("effectiveDate", today), Restrictions.isNull("terminationDate")),
                        Restrictions.and(Restrictions.le("effectiveDate", today), Restrictions.gt("terminationDate", today))));
            } else {
                c.add(Restrictions.or(Restrictions.gt("effectiveDate", today), Restrictions.le("terminationDate", today)));
            }
        }

        String fromStart = search.getFromScheduleStartTime();
        String toStart = search.getToScheduleStartTime();
        if (fromStart != null && toStart == null) {
            c.add(Restrictions.ge("scheduleStartTime", DateUtil.getZeroEpochTime(fromStart)));
        } else if (fromStart == null && toStart != null) {
            c.add(Restrictions.le("scheduleStartTime", DateUtil.getZeroEpochTime(toStart)));
        } else if (fromStart != null && toStart != null) {
            c.add(Restrictions.ge("scheduleStartTime", DateUtil.getZeroEpochTime(fromStart)));
            c.add(Restrictions.le("scheduleStartTime", DateUtil.getZeroEpochTime(toStart)));
        }

        String fromEnd = search.getFromScheduleEndTime();
        String toEnd = search.getToScheduleEndTime();
        if (fromEnd != null && toEnd == null) {
            c.add(Restrictions.ge("scheduleEndTime", DateUtil.getZeroEpochTime(fromEnd)));
        } else if (fromEnd == null && toEnd != null) {
            c.add(Restrictions.le("scheduleEndTime", DateUtil.getZeroEpochTime(toEnd)));
        } else if (fromEnd != null && toEnd != null) {
            c.add(Restrictions.ge("scheduleEndTime", DateUtil.getZeroEpochTime(fromEnd)));
            c.add(Restrictions.le("scheduleEndTime", DateUtil.getZeroEpochTime(toEnd)));
        }

        RecurrencePatternType pattern = search.getRecurrencePattern();
        if (pattern != null) {
            c.createCriteria("actSchPtn", "pattern");
            Long interval = pattern.getRecurrenceInterval();
            if (interval != null) {
                c.add(Restrictions.eq("pattern.recurrenceInterval", interval));
            }
            if (pattern instanceof OnceRecurrencePatternType) {
                Date occurrenceDate = ((OnceRecurrencePatternType) pattern).getOccurrenceDate();
                if (occurrenceDate != null) {
                    c.add(Restrictions.eq("pattern.occurrenceDate", occurrenceDate));
                }
            } else if (pattern instanceof DailyWeeklyCustomRecurrencePatternType) {
                Set<String> recurOnDays = ((DailyWeeklyCustomRecurrencePatternType) pattern).getRecurOnDays();
                if (!BeanHelper.isEmpty(recurOnDays)) {
                    c.createCriteria("pattern.recurrOnDays", "recurOnDays");
                    Junction junc = Restrictions.disjunction();
                    for (String day : recurOnDays) {
                        junc.add(Restrictions.eq("recurOnDays.day", day));
                        c.add(junc);
                    }
                }

                Boolean bWeekdayOnly = ((DailyWeeklyCustomRecurrencePatternType) pattern).isWeekdayOnlyFlag();
                if (bWeekdayOnly != null && BeanHelper.isEmpty(recurOnDays)) {
                    c.add(Restrictions.eq("pattern.weekdayOnlyFlag", bWeekdayOnly));
                }
            } else if (pattern instanceof WeeklyRecurrencePatternType) {
                Set<String> weekdays = ((WeeklyRecurrencePatternType) pattern).getRecurOnWeekday();
                if (weekdays != null) {
                    c.createCriteria("pattern.recurOnWeekdays", "onWeekdays");
                    Junction junc = Restrictions.disjunction();
                    for (String wd : weekdays) {
                        junc.add(Restrictions.eq("onWeekdays.weekday", wd));
                        c.add(junc);
                    }
                }
            } else if (pattern instanceof MonthlyRecurrencePatternType) {
                Long dayOfMonth = ((MonthlyRecurrencePatternType) pattern).getDayOfMonth();
                if (dayOfMonth != null) {
                    c.add(Restrictions.eq("pattern.dayOfMonth", dayOfMonth));
                }

                String weekday = ((MonthlyRecurrencePatternType) pattern).getRecurOnWeekday();
                if (weekday != null) {
                    c.add(Restrictions.eq("pattern.recurOnWeekday", weekday));
                }

                String nthWeekday = ((MonthlyRecurrencePatternType) pattern).getNthWeekday();
                if (nthWeekday != null) {
                    c.add(Restrictions.eq("pattern.nthWeekday", nthWeekday));
                }
            } else if (pattern instanceof YearlyRecurrencePatternType) {
                Long recurOnDay = ((YearlyRecurrencePatternType) pattern).getRecurOnDay();
                if (recurOnDay != null) {
                    c.add(Restrictions.eq("pattern.recurOnDay", recurOnDay));
                }

                String month = ((YearlyRecurrencePatternType) pattern).getRecurOnMonth();
                if (month != null) {
                    c.add(Restrictions.eq("pattern.recurOnMonth", month));
                }

                String weekday = ((YearlyRecurrencePatternType) pattern).getRecurOnWeekday();
                if (weekday != null) {
                    c.add(Restrictions.eq("pattern.recurOnWeekday", weekday));
                }

                String nthWeekday = ((YearlyRecurrencePatternType) pattern).getNthWeekday();
                if (nthWeekday != null) {
                    c.add(Restrictions.eq("pattern.nthWeekday", nthWeekday));
                }
            }
        }

        if (search.getFacilityId() != null) {
            c.add(Restrictions.eq("facilityId", search.getFacilityId()));
        }

        if (search.getForInternalLocationId() != null) {
            c.add(Restrictions.eq("forInternalLocId", search.getForInternalLocationId()));
        }

        Set<Long> atInternalLocIds = search.getAtInternalLocationIds();
        if (atInternalLocIds != null && !atInternalLocIds.isEmpty()) {
            BeanHelper.getCriteriaWithModuleAssociations(uc, session, c, "ALL", "scheduleIdentification", atInternalLocIds,
                    ActivityServiceHelper.AssociationToClass.FacilityInternalLocationService.name(), ScheduleInternalLocationEntity.class, "schedule");
        }

        Set<Long> locationIds = search.getLocationIds();
        if (locationIds != null && !locationIds.isEmpty()) {
            BeanHelper.getCriteriaWithModuleAssociations(uc, session, c, "ALL", "scheduleIdentification", locationIds, ActivityServiceHelper.AssociationToClass.LocationService.name(),
                    ScheduleLocationEntity.class, "schedule");
        }

        Set<Long> organizationIds = search.getOrganizationIds();
        if (organizationIds != null && !organizationIds.isEmpty()) {
            BeanHelper.getCriteriaWithModuleAssociations(uc, session, c, "ALL", "scheduleIdentification", organizationIds, ActivityServiceHelper.AssociationToClass.OrganizationService.name(),
                    ScheduleOrganizationEntity.class, "schedule");
        }

        Long totalSize = (Long) c.setProjection(Projections.countDistinct("scheduleIdentification")).uniqueResult();
        c.setProjection(null);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            c.setFirstResult(startIndex.intValue());
            c.setMaxResults(resultSize.intValue());
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                c.addOrder(ord);
            }
        }

        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        @SuppressWarnings("unchecked") List<ScheduleEntity> lst = c.list();

        SchedulesReturnType ret = new SchedulesReturnType();
        ret.setSchedules(ActivityServiceHelper.toSimpleSchedulesList(lst));
        ret.setTotalSize(totalSize);

        if (log.isDebugEnabled()) {
            log.debug("searchSchedule: end");
        }
        return ret;
    }

    public ScheduleConfigVariablesReturnType searchScheduleConfigVers(UserContext uc, ScheduleConfigVariableSearchType search, Long startIndex, Long resultSize,
                                                                      String resultOrder) {

        if (log.isDebugEnabled()) {
            log.debug("searchScheduleConfigVers: begin");
        }

        ValidationHelper.validateSearchType(search);

        if (resultSize == null || resultSize > SEARCH_MAX_LIMIT) {
            resultSize = Long.valueOf(SEARCH_MAX_LIMIT);
        }

        Criteria c = session.createCriteria(ScheduleConfigVariableEntity.class);

        String category = search.getScheduleCategory();
        if (category != null) {
            c.add(Restrictions.ilike("scheduleCategory", category));
        }

        Long numOfSlots = search.getNumOfNextAvailableTimeslots();
        if (numOfSlots != null) {
            c.add(Restrictions.eq("numOfNextAvailableTimeslots", numOfSlots));
        }

        // Total size
        Long totalSize = (Long) c.setProjection(Projections.countDistinct("scheduleConfigIdentification")).uniqueResult();

        c.setProjection(null);

        // Pagination
        if (resultSize == null || resultSize > SEARCH_MAX_LIMIT) {
            resultSize = Long.valueOf(SEARCH_MAX_LIMIT);
        }
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            c.setFirstResult(startIndex.intValue());
            c.setMaxResults(resultSize.intValue());
        }

        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                c.addOrder(ord);
            }
        }

        @SuppressWarnings("unchecked") List<ScheduleConfigVariableEntity> configs = c.list();

        List<ScheduleConfigVariableType> configRet = ActivityServiceHelper.toScheduleConfigVariableLists(configs);
        ScheduleConfigVariablesReturnType ret = new ScheduleConfigVariablesReturnType();
        ret.setScheduleConfigVariables(configRet);
        ret.setTotalSize(totalSize);

        if (log.isDebugEnabled()) {
            log.debug("searchScheduleConfigVers: end");
        }

        return ret;
    }

    /**
     * Delete an ActivityType by id.
     * Provenance
     * <li>The ActivityType must exist;</li>
     * <li>The activity cannot be deleted if the activity has reference to descendants.</li>
     * <p>Returns a null and success code if instance can be deleted.
     * <p>Returns a null and error code if instance cannot be deleted.
     *
     * @param uc UserContext - Required
     * @param id java.lang.Long - Required
     * @return Long
     */
    public Long delete(UserContext uc, Long id) {

        if (log.isDebugEnabled()) {
            log.debug("delete: begin");
        }

        String functionName = "delete";

        if (id == null) {
            String message = "Activity Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        ActivityEntity activity = BeanHelper.getEntity(session, ActivityEntity.class, id);

        if (activity.getDescendantIds() != null && activity.getDescendantIds().size() > 0) {
            String message = "The ActivityType with id " + id + " cannot be deleted because of the existed descendant(s).";
            LogHelper.error(log, functionName, message);
            throw new DataExistException(message);
        }

        Long antecedentId = activity.getAntecedentId();
        if (antecedentId != null) {
            ActivityEntity antecedentActivity = BeanHelper.getEntity(session, ActivityEntity.class, antecedentId);
            Set<Long> descendantIds = antecedentActivity.getDescendantIds();
            if (descendantIds != null) {
                descendantIds.remove(id);
            }
        }

        session.delete(activity);

        if (log.isDebugEnabled()) {
            log.debug("delete: end");
        }

        return SUCCESS;
    }

    /**
     * Delete ScheduleType.
     * Provenance
     * <li>Only Inactive schedule can be deleted.</li>
     *
     * @param uc UserContext - Required
     * @param id Long - Required
     * @return Long
     */
    public Long deleteSchedule(UserContext uc, Long id) {

        if (log.isDebugEnabled()) {
            log.debug("deleteSchedule: begin");
        }

        Long ret = deleteSchedule(uc, id, false);

        if (log.isDebugEnabled()) {
            log.debug("deleteSchedule: end");
        }

        return ret;
    }

    /**
     * Delete ScheduleType.
     * Provenance
     * <li>Only Inactive schedule can be deleted.</li>
     *
     * @param uc UserContext - Required
     * @param id Long - Required
     * @return ScheduleType
     */
    private Long deleteSchedule(UserContext uc, Long id, boolean bAll) {
        if (log.isDebugEnabled()) {
            log.debug("deleteSchedule: begin");
        }

        String functionName = "deleteSchedule";

        if (id == null) {
            String message = "Schedule Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        ScheduleEntity schedule = BeanHelper.getEntity(session, ScheduleEntity.class, id);

        if (!bAll) {
            // Only inactive schedule can be deleted
            if (schedule.getEffectiveDate().before(this.getDateAndTime(getToday(), ActivityServiceHelper.getTimeOnly(schedule.getScheduleEndTime()))) && (schedule.getTerminationDate() == null
                    || schedule.getTerminationDate().after(this.getDateAndTime(getToday(), ActivityServiceHelper.getTimeOnly(schedule.getScheduleEndTime()))))) {

                String message = "ScheduleType with id " + id + " cannot be deleted because it is still active: " + schedule;
                LogHelper.error(log, functionName, message);
                throw new DataExistException(message);
            }

        }

        session.delete(schedule);

        if (log.isDebugEnabled()) {
            log.debug("deleteSchedule: end");
        }

        return SUCCESS;
    }

    /**
     * Delete All ScheduleType.
     * <p>For maintenance use.</p>
     *
     * @param uc UserContext - Required
     * @return Long - success: 1; failure: null or error code.
     */
    public Long deleteAllSchedule(UserContext uc) {
        if (log.isDebugEnabled()) {
            log.debug("deleteAllSchedule: begin");
        }

        session.createSQLQuery("UPDATE ACT_Sch SET flag = 1").executeUpdate();

        Criteria c = session.createCriteria(ScheduleEntity.class);
        c.setProjection(Projections.id());
        @SuppressWarnings("unchecked") Iterator<Long> it = c.list().iterator();
        while (it.hasNext()) {
            Long id = it.next();
            ScheduleEntity entity = BeanHelper.findEntity(session, ScheduleEntity.class, id);
            entity.setRecurrencePattern(null);
        }

        session.flush();
        session.clear();

        session.createQuery("delete ScheduleInternalLocationEntity").executeUpdate();
        session.createQuery("delete ScheduleLocationEntity").executeUpdate();
        session.createQuery("delete ScheduleOrganizationEntity").executeUpdate();
        session.createQuery("delete DayEntity").executeUpdate();
        session.createQuery("delete WeekdayEntity").executeUpdate();
        session.createQuery("delete DailyRecurrencePatternEntity").executeUpdate();
        session.createQuery("delete WeeklyRecurrencePatternEntity").executeUpdate();
        session.createQuery("delete MonthlyRecurrencePatternEntity").executeUpdate();
        session.createQuery("delete YearlyRecurrencePatternEntity").executeUpdate();
        session.createQuery("delete RecurrencePatternEntity").executeUpdate();

        // session.createQuery("delete ScheduleEntity").executeUpdate();
        session.createSQLQuery("delete ACT_SCH").executeUpdate();

        session.flush();
        session.clear();

        if (log.isDebugEnabled()) {
            log.debug("deleteAllSchedule: end");
        }

        return SUCCESS;
    }

    /**
     * Delete ScheduleType Config Variable.
     * <p>For maintenance use.</p>
     *
     * @param uc UserContext - Required
     * @param id Long - Required
     * @return Long
     */
    public Long deleteScheduleConfigVar(UserContext uc, Long id) {
        if (log.isDebugEnabled()) {
            log.debug("deleteScheduleConfigVar: begin");
        }

        String functionName = "deleteScheduleConfigVar";

        if (id == null) {
            String message = "ScheduleConfigVariable Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ScheduleConfigVariableEntity config = BeanHelper.getEntity(session, ScheduleConfigVariableEntity.class, id);
        session.delete(config);

        if (log.isDebugEnabled()) {
            log.debug("deleteScheduleConfigVar: end");
        }

        return SUCCESS;
    }

    /**
     * Delete All ScheduleType Config Variable.
     * <p>For maintenance use.</p>
     *
     * @param uc UserContext - Required
     * @return Long - success: 1; failure: null or error code.
     */
    public Long deleteAllScheduleConfigVar(UserContext uc) {
        return handler.deleteAllScheduleConfigVar(session);
    }

    /**
     * Cancel Activities due to schedule change.
     * Provenance
     * <li>All activities are canceled if they meet the following criteria,
     * <ol>The planned start dates of the activity is after the CancellationDate;</ol>
     * <ol>ActivityType’s ScheduleID = ScheduleIdentification;</ol>
     * </li>
     * <li>Status of a canceled activity is set to inactive;</li>
     * <li>If DeleteActivityFlag = True, canceled activity will be deleted. The activity augmentation referenced by the activity must be deleted as well (out of scope of this service).</li>
     *
     * @param uc                 UserContext - Required
     * @param scheduleId         Long - Required
     * @param cancellationDate   java.util.Date - Required
     * @param deleteActivityFlag Boolean - Required
     * @return ActivitiesReturnType
     */
    public Set<ActivityType> cancel(UserContext uc, Long scheduleId, Date cancellationDate, Boolean deleteActivityFlag) {

        if (log.isDebugEnabled()) {
            log.debug("cancel: begin");
        }

        String functionName = "cancel";

        if (scheduleId == null) {
            String message = "Schedule Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        if (!BeanHelper.bExistedModule(session, ScheduleEntity.class, scheduleId)) {
            String message = "Invalid argument: scheduleId: " + scheduleId;
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Set<ActivityType> ret = new HashSet<ActivityType>();

        Criteria c = session.createCriteria(ActivityEntity.class);
        c.add(Restrictions.eq("scheduleID", scheduleId));
        c.add(Restrictions.ge("plannedStartDate", cancellationDate));

        @SuppressWarnings("unchecked") Set<ActivityEntity> activities = new HashSet<ActivityEntity>(c.list());
        Iterator<ActivityEntity> it = activities.iterator();
        while (it.hasNext()) {
            ActivityEntity act = it.next();
            if (deleteActivityFlag.booleanValue()) {
                delete(uc, act.getActivityId());
            } else {
                act.setActiveStatusFlag(Boolean.FALSE);
                act = (ActivityEntity) session.merge(act);
            }
            ret.add(ActivityServiceHelper.toActivity(act));
        }

        if (log.isDebugEnabled()) {
            log.debug("cancel: end");
        }
        return ret;
    }

    /**
     * Cancel Activities by a ScheduleType Time slot.
     * Provenance
     * <li>All activities are canceled if they meet the following criteria,
     * <ol>The activity’s planned start date = timeslot’s TimeslotStartDateTime;</ol>
     * <ol>The activity’s planned end date = timeslot’s TimeslotEndDateTime;</ol>
     * <ol>ActivityType’s ScheduleID = timeslot’s ScheduleID;</ol>
     * </li>
     * <li>Status of a canceled activity is set to inactive;</li>
     * <li>If DeleteActivityFlag = True, canceled activity will be deleted. The activity augmentation referenced by the activity must be deleted as well (out of scope of this service).</li>
     *
     * @param uc                 UserContext - Required
     * @param timeslot           ScheduleTimeslotType - Required
     * @param deleteActivityFlag Boolean - Required
     * @return ActivitiesReturnType
     */
    public Set<ActivityType> cancel(UserContext uc, ScheduleTimeslotType timeslot, Boolean deleteActivityFlag) {

        if (log.isDebugEnabled()) {
            log.debug("cancel: begin");
        }

        String functionName = "cancel";

        ValidationHelper.validate(timeslot);

        Long scheduleId = timeslot.getScheduleID();
        if (scheduleId == null || !BeanHelper.bExistedModule(session, ScheduleEntity.class, scheduleId)) {
            String message = "Invalid argument: scheduleId: " + scheduleId;
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Set<ActivityType> ret = new HashSet<ActivityType>();

        Date start = timeslot.getTimeslotStartDateTime();
        Date end = timeslot.getTimeslotEndDateTime();

        Criteria c = session.createCriteria(ActivityEntity.class);
        c.add(Restrictions.eq("plannedStartDate", start));
        if (end != null) {
            c.add(Restrictions.eq("plannedEndDate", end));
        }

        c.add(Restrictions.eq("scheduleID", timeslot.getScheduleID()));

        @SuppressWarnings("unchecked") Set<ActivityEntity> activities = new HashSet<ActivityEntity>(c.list());
        Iterator<ActivityEntity> it = activities.iterator();
        while (it.hasNext()) {
            ActivityEntity act = it.next();
            if (deleteActivityFlag.booleanValue()) {
                session.delete(act);
            } else {
                act.setActiveStatusFlag(Boolean.FALSE);
                act = (ActivityEntity) session.merge(act);
            }
            ret.add(ActivityServiceHelper.toActivity(act));
        }

        if (log.isDebugEnabled()) {
            log.debug("cancel: end");
        }
        return ret;
    }

    /**
     * Delete all Activities, references and reference data.
     * <p>Return success code if all instances, references and reference data
     * deleted. <p>Return error code if not all instances, references and reference
     * data deleted.
     *
     * @param uc UserContext - Required
     * @return returnCode java.lang.Long
     */
    public Long deleteAll(UserContext uc) {

        if (log.isDebugEnabled()) {
            log.debug("deleteAll: begin");
        }

        deleteAllScheduleConfigVar(uc);
        deleteAllSchedule(uc);
        handler.deleteAllActivities(session);

        if (log.isDebugEnabled()) {
            log.debug("deleteAll: end");
        }
        return SUCCESS;
    }

    /**
     * Get a set of all ids of Activities.
     * <p>Returns null if there is an error during the get all.
     *
     * @param uc UserContext - Required
     * @return Set<java.lang.Long>
     */
    public Set<Long> getAll(UserContext uc) {

        if (log.isDebugEnabled()) {
            log.debug("getAll: begin");
        }

        Set<Long> ret = BeanHelper.getAll(uc, context, session, ActivityEntity.class);

        if (log.isDebugEnabled()) {
            log.debug("getAll: end");
        }

        return ret;
    }


    /**
     * See documentation
     * @param uc
     * @param activityIds
     * @return
     */
    public List<ScheduleActivityLinkType> getScheduleActivityLinkTypesFromActivityIds(UserContext uc, List<Long> activityIds) {
        return handler.getScheduleActivityLinkTypesFromActivityIds(session, activityIds);

    }

    /**
     * see documentation
     * @param uc UserContext - Required
     * @param schedule Schedule - required
     * @return
     */
    public Boolean hasDuplicateOverlappingSchedule(UserContext uc, Schedule schedule) {

        DateTime startDateTime = new DateTime().withDate(LocalDate.fromDateFields(schedule.getEffectiveDate())).withTime(
                LocalTime.parse(schedule.getScheduleStartTime()));

        Set facilityIds = new HashSet<>();
        facilityIds.add(schedule.getFacilityId());

        List<Schedule> sameTimeSchedules = handler.getSchedulesFromConstraints(session, facilityIds, null, schedule.getScheduleCategory(), startDateTime.toLocalDate(),
                startDateTime.toLocalTime(), null, null, true);

        if (!sameTimeSchedules.isEmpty()){
            return  sameTimeSchedules.stream()
                    .filter(s -> !Objects.equals(s.getScheduleIdentification(), schedule.getScheduleIdentification()))
                    .anyMatch(s -> recurrenceUtil.areSchedulesOverlapping(s, schedule));
        }
        return false;

    }

    /**
     * Get the stamp of an ActivityType.
     * <p>Returns null if there is an error retrieving the stamp.
     *
     * @param uc UserContext - Required
     * @param id Long - Required
     * @return StampType
     */
    public StampType getStamp(UserContext uc, Long id) {

        ActivityEntity act = BeanHelper.getEntity(session, ActivityEntity.class, id);

        StampEntity stamp = act.getStamp();
        if (stamp == null) {
            stamp = new StampEntity();
        }

        return BeanHelper.toStamp(stamp);
    }

    public String getNiem(UserContext uc, Long activityId) {

        if (activityId == null) {
            String message = "Invalid argument: activityId is required.";
            LogHelper.error(log, "getNiem", message);
            throw new InvalidInputException(message);
        }

        ActivityEntity act = BeanHelper.getEntity(session, ActivityEntity.class, activityId);

        StringBuffer niem = new StringBuffer(BeanHelper.toNiemHeader(act.getActivityId(), "ActivityService"));
        niem.append("  <nc:ActivityType>\n");
        niem.append("    <nc:ActivityCategoryText>").append(act.getActivityCategory()).append("</nc:ActivityCategoryText>\n");
        niem.append("    <nc:ActivityDateRange>\n");
        niem.append("      <nc:StartDate>\n");
        niem.append("        <nc:Date>").append(BeanHelper.toISODate(act.getPlannedStartDate())).append("</nc:Date>\n");
        niem.append("      </nc:StartDate>\n");
        niem.append("      <nc:EndDate>\n");
        niem.append("        <nc:Date>").append(BeanHelper.toISODate(act.getPlannedEndDate())).append("</nc:Date>\n");
        niem.append("      </nc:EndDate>\n");
        niem.append("    </nc:ActivityDateRange>\n");
        niem.append("    <nc:ActivityStatus>\n");
        niem.append("      <nc:StatusText>").append(Boolean.TRUE.equals(act.getActiveStatusFlag()) ? "ACTIVE" : "INACTIVE").append("</nc:StatusText>\n");
        niem.append("    </nc:ActivityStatus>\n");
        niem.append("  </nc:ActivityType>\n");

        niem.append("  <ActivityServiceExt:ActivityAntecedentAssociation>\n");
        if (act.getAntecedentId() != null && act.getAntecedentId() != null) {
            niem.append("    <sv:AssociationType id=\"").append(BeanHelper.toNiemAttribute(act.getAntecedentId())).append("\"/>\n");
        }
        niem.append("  </ActivityServiceExt:ActivityAntecedentAssociation>\n");

        niem.append("  <ActivityServiceExt:ActivityDescendantsAssociations>\n");
        if (act.getDescendantIds() != null && act.getDescendantIds().size() > 0) {
            for (Long desc : act.getDescendantIds()) {
                niem.append("    sv:AssociationType service=\"").append("ActivityService").append("\" id=\"").append(BeanHelper.toNiemAttribute(desc)).append("\"/>\n");
            }
        }
        niem.append("  </ActivityServiceExt:ActivityDescendantsAssociations>\n");

        niem.append("</root>\n");

        return niem.toString();
    }

    /**
     * {@inheritDoc}
     */
    public String getVersion(UserContext uc) {
        return "2.0";
    }


    /**
     * Get/Retrieve ScheduleType
     *
     * @param uc UserContext - Required
     * @param id Long - Required
     * @return ScheduleType
     */
    private ScheduleEntity getActiveScheduleEntity(UserContext uc, Long id) {
        if (log.isDebugEnabled()) {
            log.debug("getScheduleEntity: begin");
        }

        String functionName = "getActiveScheduleEntity";

        ScheduleEntity schedule = BeanHelper.getEntity(session, ScheduleEntity.class, id);

        // Filter Active ScheduleType
        //		Date effectiveDate = DateUtil.getDateWithoutTime(schedule.getEffectiveDate());
        Date terminationDate = schedule.getTerminationDate();
        Date now = BeanHelper.createDateWithoutTime();


/*		if (!now.equals(effectiveDate) && (effectiveDate.before(now))
				|| (terminationDate != null && terminationDate.before(now))) {*/
        if ((terminationDate != null && terminationDate.before(now))) {
            String message = "ScheduleType with id " + id + " is not in active.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        if (log.isDebugEnabled()) {
            log.debug("getScheduleEntity: end");
        }
        return schedule;
    }

    private ScheduleConfigVariableType getScheduleConfigVarByCategory(UserContext uc, String category) {
        Criteria c = session.createCriteria(ScheduleConfigVariableEntity.class);
        c.add(Restrictions.eq("scheduleCategory", category));
        ScheduleConfigVariableEntity config = (ScheduleConfigVariableEntity) c.uniqueResult();

        return ActivityServiceHelper.toScheduleConfigVariable(config);
    }

    private ScheduleConfigVariableEntity getScheduleConfigVarEntityByCategory(UserContext uc, String category) {
        Criteria c = session.createCriteria(ScheduleConfigVariableEntity.class);
        c.add(Restrictions.eq("scheduleCategory", category));
        ScheduleConfigVariableEntity config = (ScheduleConfigVariableEntity) c.uniqueResult();

        return config;
    }

    ScheduleTimeslotType getTimeslot(UserContext uc, Date lookupDate, Date lookupEndDate, Long scheduleId) {
        Criteria c = session.createCriteria(ScheduleEntity.class);
        c.add(Restrictions.idEq(scheduleId));
        c.add(Restrictions.ge("effectiveDate", lookupDate));
        if (lookupEndDate != null) {
            c.add(Restrictions.ge("terminationDate", lookupEndDate));
        }

        ScheduleEntity sch = (ScheduleEntity) c.uniqueResult();
        if (sch == null) {
            log.debug("Failure to get ScheduleTimeslotType by id " + scheduleId + ", " + "lookup date " + lookupDate + " and lookup end date " + lookupEndDate);
            return null;
        }

        return new ScheduleTimeslotType(sch.getEffectiveDate(), sch.getTerminationDate(), scheduleId);
    }

    private void verifySchedule(Schedule schedule) {
        Date effectiveDate = schedule.getEffectiveDate();
        Date terminationDate = schedule.getTerminationDate();
        String scheduleStartTime = schedule.getScheduleStartTime();
        String scheduleEndTime = schedule.getScheduleEndTime();
        if (effectiveDate == null || terminationDate != null && terminationDate.before(effectiveDate) || scheduleStartTime == null
                || Integer.valueOf(scheduleStartTime.split(":")[0]) < 0 || Integer.valueOf(scheduleStartTime.split(":")[0]) >= 24
                || Integer.valueOf(scheduleStartTime.split(":")[1]) < 0 || Integer.valueOf(scheduleStartTime.split(":")[1]) >= 60 || scheduleEndTime != null && (
                Integer.valueOf(scheduleEndTime.split(":")[0]) < 0 || Integer.valueOf(scheduleEndTime.split(":")[0]) >= 24
                        || Integer.valueOf(scheduleEndTime.split(":")[1]) < 0 || Integer.valueOf(scheduleEndTime.split(":")[1]) >= 60
                        || Integer.valueOf(scheduleEndTime.split(":")[0]) < Integer.valueOf(scheduleStartTime.split(":")[0])
                        || Integer.valueOf(scheduleEndTime.split(":")[0]) == Integer.valueOf(scheduleStartTime.split(":")[0]) && (
                        Integer.valueOf(scheduleEndTime.split(":")[1]) < Integer.valueOf(scheduleStartTime.split(":")[1])))) {
            String errmsg = "Invalid argument: Schedule: " + schedule;
            log.error(errmsg);
            throw new InvalidInputException(errmsg);
        }
    }

    private boolean hasCategory(String category) {
        Criteria c = session.createCriteria(ScheduleConfigVariableEntity.class);
        c.add(Restrictions.eq("scheduleCategory", category));
        c.setProjection(Projections.rowCount());
        Long count = (Long) c.uniqueResult();
        if (count == null || count == 0L) {
            return false;
        }
        return true;
    }

    private boolean bValidateRecurrencPattern(UserContext uc, RecurrencePatternType pattern) {
        if (pattern instanceof OnceRecurrencePatternType) {
            return true;
        } else if (pattern instanceof DailyWeeklyCustomRecurrencePatternType) {
            return true;
        } else if (pattern instanceof WeeklyRecurrencePatternType) {
            Set<String> weekdays = ((WeeklyRecurrencePatternType) pattern).getRecurOnWeekday();
            for (String weekday : weekdays) {
                if (!bReferenceCode(uc, ReferenceSet.WEEKDAY.value(), weekday, true)) {
                    return false;
                }
            }
        } else if (pattern instanceof MonthlyRecurrencePatternType) {
            String weekday = ((MonthlyRecurrencePatternType) pattern).getRecurOnWeekday();
            if (!bReferenceCode(uc, ReferenceSet.WEEKDAY.value(), weekday, true)) {
                return false;
            }

            String nthWeekday = ((MonthlyRecurrencePatternType) pattern).getNthWeekday();
            if (!bReferenceCode(uc, ReferenceSet.NTHWEEKDAY.value(), nthWeekday, true)) {
                return false;
            }

        } else if (pattern instanceof YearlyRecurrencePatternType) {
            String nthWeekday = ((YearlyRecurrencePatternType) pattern).getRecurOnWeekday();
            if (!bReferenceCode(uc, ReferenceSet.WEEKDAY.value(), nthWeekday, true)) {
                return false;
            }

            nthWeekday = ((YearlyRecurrencePatternType) pattern).getNthWeekday();
            if (!bReferenceCode(uc, ReferenceSet.NTHWEEKDAY.value(), nthWeekday, true)) {
                return false;
            }

            String month = ((YearlyRecurrencePatternType) pattern).getRecurOnMonth();
            if (!bReferenceCode(uc, ReferenceSet.MONTH.value(), month, true)) {
                return false;
            }

        } else {
            return false;
        }

        return true;
    }

    private boolean bReferenceCode(UserContext uc, String refSet, String refCode, Boolean activeFlag) {

        if (refCode == null) {
            return true;
        }

        try {
            Set<CodeType> codes = new HashSet<CodeType>();
            codes.add(new CodeType(refSet, refCode));
            if (activeFlag) {
                ReferenceDataHelper.isActiveReferenceCode(codes);
            } else {
                ReferenceDataHelper.doesExistReferenceCode(codes);
            }
        } catch (Exception e) {
            log.debug(e.getMessage());
            return false;
        }
        return true;
    }

    private boolean hasAssociatedToActivity(Long scheduleId) {
        Criteria c = session.createCriteria(ActivityEntity.class);
        c.add(Restrictions.eq("scheduleID", scheduleId));
        c.setProjection(Projections.count("scheduleID"));

        if (c.list().size() == 0) {
            return true;
        }

        return false;
    }

    private boolean bValidScheduleConfigVariable(ScheduleConfigVariableType config) {
        if (log.isDebugEnabled()) {
            log.debug("bValidScheduleConfigVariable: begin");
        }

        String code = config.getScheduleCategory();
        if (config.getScheduleConfigIdentification() == null || code == null || code.trim().equals("")) {

            return false;
        }
        String category = code;
        ScheduleConfigVariableEntity configEntity = (ScheduleConfigVariableEntity) session.get(ScheduleConfigVariableEntity.class,
                config.getScheduleConfigIdentification());

        if (configEntity == null || !configEntity.getScheduleCategory().equals(category)) {
            return false;
        }

        if (log.isDebugEnabled()) {
            log.debug("bValidScheduleConfigVariable: end");
        }

        return true;
    }

    private Date getDateWithoutTime(Date date) {
        if (date == null) {
            return null;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Date newDate = cal.getTime();
        return newDate;
    }



    private Date getDateAndTime(Date date, String time) {
        if (date == null) {
            return null;
        }

        Calendar c = Calendar.getInstance();
        c.setTime(getDateWithoutTime(date));

        String[] hhmm;
        if (time == null || time.trim().equals("")) {
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
        } else {
            hhmm = time.split(":");
            c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(hhmm[0]));
            c.set(Calendar.MINUTE, Integer.valueOf(hhmm[1]));
        }
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    private Date combineDateAndTime(Date date, Date time) {
        if (date == null) {
            return null;
        }

        Calendar d = Calendar.getInstance();
        d.clear();
        d.setTime(getDateWithoutTime(date));
        if (time != null) {
            Calendar t = Calendar.getInstance();
            t.clear();
            t.setTime(time);

            d.set(Calendar.HOUR_OF_DAY, t.get(Calendar.HOUR_OF_DAY));
            d.set(Calendar.MINUTE, t.get(Calendar.MINUTE));
            d.set(Calendar.SECOND, t.get(Calendar.SECOND));
        }
        d.set(Calendar.MILLISECOND, 0);
        return d.getTime();
    }

    private Date getToday() {
        return getDateWithoutTime(Calendar.getInstance().getTime());
    }

    private Date getPresentTime() {
        return Calendar.getInstance().getTime();
    }


    private Date getOneDayPlus(Date date) {

        if (date == null) {
            return null;
        }

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);

        // Set time fields to zero
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }




    private boolean isValidatedScheduleTime(String scheduleTime) {

        String[] hhmm = scheduleTime.split(":");
        int hh = Integer.valueOf(hhmm[0]);
        int mm = Integer.valueOf(hhmm[1]);
        if (hh < 0 || hh >= 24 || mm < 0 || mm >= 60) {
            return false;
        }

        return true;
    }



    private void checkIfExtendedScheduleType(Schedule schedule) {
        if (schedule instanceof SimpleSchedule) {
            schedule.setRecurrencePattern(new RecurrencePatternFactory().generate(((SimpleSchedule) schedule).getRecurrenceObject()));
        }
    }





}
