package syscon.arbutus.product.services.activity.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * WeekdayEntity for Activity Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment ACT_SchWk 'Weekday table for Schedule Table of Activity Service'
 * @since September 18, 2012
 */
@Entity
@Audited
@Table(name = "ACT_SchWk")
public class WeekdayEntity implements Serializable {

    private static final long serialVersionUID = 423140073329958965L;

    /**
     * @DbComment ACT_SchWk.actSchWkId 'Weekday ID'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_ACT_SCHWK_ID")
    @SequenceGenerator(name = "SEQ_ACT_SCHWK_ID", sequenceName = "SEQ_ACT_SCHWK_ID", allocationSize = 1)
    @Column(name = "actSchWkId")
    private Long id;

    /**
     * @DbComment ACT_SchWk.weekday 'Weekday'
     */
    @Column(name = "weekday", length = 64)
    @MetaCode(set = MetaSet.WEEKDAY)
    private String weekday;

    /**
     * @DbComment ACT_SchWk.version 'Version for Hibernate use'
     */
    // @Version
    @Column(name = "version")
    @NotAudited
    private Long version;

    /**
     * @DbComment ACT_SchWk.createUserId 'Create User Id'
     * @DbComment ACT_SchWk.createDateTime 'Create Date Time'
     * @DbComment ACT_SchWk.modifyUserId 'Modify User Id'
     * @DbComment ACT_SchWk.modifyDateTime 'Modify Date Time'
     * @DbComment ACT_SchWk.invocationContext 'Invocation Context'
     */
    @Embedded
    @NotAudited
    private StampEntity stamp;

    public WeekdayEntity() {
        super();
    }

    public WeekdayEntity(WeekdayEntity weekday) {
        super();
        this.id = weekday.getId();
        this.weekday = weekday.getWeekday();
        this.version = weekday.getVersion();
        this.stamp = weekday.getStamp();
    }

    /**
     * @param id
     * @param weekday
     * @param version
     * @param stamp
     */
    public WeekdayEntity(Long id, String weekday, Long version, StampEntity stamp) {
        super();
        this.id = id;
        this.weekday = weekday;
        this.version = version;
        this.stamp = stamp;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the weekday
     */
    public String getWeekday() {
        return weekday;
    }

    /**
     * @param weekday the weekday to set
     */
    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((weekday == null) ? 0 : weekday.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        WeekdayEntity other = (WeekdayEntity) obj;
        if (weekday != other.weekday) {
			return false;
		}
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "WeekdayEntity [id=" + id + ", weekday=" + weekday + ", version=" + version + ", stamp=" + stamp + "]";
    }

}
