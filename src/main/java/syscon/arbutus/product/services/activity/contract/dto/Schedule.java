package syscon.arbutus.product.services.activity.contract.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

/**
 * ScheduleType for Activity Service
 *
 * @author yshang
 * @version 1.0 (Based on SDD 1.0)
 * @since April 27, 2012
 */
@ArbutusConstraint(constraints = { "effectiveDate <= terminationDate" })
public class Schedule implements Serializable {

    private static final long serialVersionUID = 6718379943492289482L;

    private Long scheduleIdentification;
    @NotNull
    private String scheduleCategory;
    @NotNull
    private Long facilityId;
    private Long forInternalLocationId;
    private Set<Long> atInternalLocationIds;
    private Set<Long> locationIds;
    private Set<Long> organizationIds;
    private Long capacity;
    @NotNull
    private Date effectiveDate;
    private Date terminationDate;
    private Boolean activeScheduleFlag;
    @NotNull
    @Pattern(regexp = "^[0-2]?[0-9]:[0-6][0-9]$")
    private String scheduleStartTime;
    @Pattern(regexp = "^[0-2]?[0-9]:[0-6][0-9]$")
    private String scheduleEndTime;
    private RecurrencePatternType recurrencePattern;
    private String facilityScheduleReason; // ReferenceSet(FacilityScheduleReason)

    /**
     * Constructor
     */
    public Schedule() {
        super();
    }

    /**
     * Constructor
     *
     * @param schedule ScheduleType Required when invoking this constructor
     */
    public Schedule(Schedule schedule) {
        super();
        this.scheduleIdentification = schedule.getScheduleIdentification();
        this.scheduleCategory = schedule.getScheduleCategory();
        this.facilityId = schedule.getFacilityId();
        this.forInternalLocationId = schedule.getForInternalLocationId();
        this.atInternalLocationIds = schedule.getAtInternalLocationIds();
        this.locationIds = schedule.getLocationIds();
        this.organizationIds = schedule.getOrganizationIds();
        this.capacity = schedule.getCapacity();
        this.effectiveDate = schedule.getEffectiveDate();
        this.terminationDate = schedule.getTerminationDate();
        this.scheduleStartTime = schedule.getScheduleStartTime();
        this.scheduleEndTime = schedule.getScheduleEndTime();
        this.recurrencePattern = schedule.getRecurrencePattern();
        this.facilityScheduleReason = schedule.getFacilityScheduleReason();
    }

    /**
     * Constructor
     * <ul>
     * <li>scheduleIdentification -- An unique identifier for a schedule.</li>
     * <li>scheduleCategory -- The type of activity that can use this schedule. Schedule category is a subset of activity category values, i.e not all types of activity have schedules. A schedule cannot be defined for a non-existing type of activity.</li>
     * <li>facilityId -- Reference to a facility. Each schedule is defined for a particular facility.</li>
     * <li>forInternalLocationId -- Reference to a facility internal location. Used for facility scheduling. Indicates for which internal location the activity is being scheduled. This internal location reference cannot be the same as AtInternalLocationReference.</li>
     * <li>atInternalLocationIds -- Reference to the internal locations where an activity using this schedule is allowed to take place. This internal location reference cannot be the same as ForInternalLocationReference.</li>
     * <li>locationIds -- Reference to locations (addresses) where an activity using this schedule is allowed to take place.</li>
     * <li>organizationIds -- Reference to the organizations where an activity using this schedule is allowed to takes place.</li>
     * <li>capacity -- The capacity of the schedule. The capacity is the number activities is allowed to use a timeslot of this schedule.</li>
     * <li>effectiveDate -- The date when the schedule becomes effective.</li>
     * <li>terminationDate -- The date when the schedule is no longer effective. If a schedule has recurrence pattern, the recurrence pattern ends on the TerminationDate.</li>
     * <li>scheduleStartTime -- The start time of a schedule. E.g 08:00.</li>
     * <li>scheduleEndTime -- The end time of a schedule. E.g 16:00.</li>
     * <li>recurrencePattern -- The pattern of recurrence, if the schedule is recurring.</li>
     * <li>facilityScheduleReason -- The facility schedule reason is one of reference codes in FacilityScheduleReason set. Used by facility scheduling. Specifies the reason for the movement.</li>
     * </ul>
     *
     * @param scheduleIdentification Ignored when create; Required when update
     * @param scheduleCategory       Required
     * @param facilityId             Required
     * @param forInternalLocationId  Optional
     * @param atInternalLocationIds  Optional
     * @param locationIds            Optional
     * @param organizationIds        Optional
     * @param capacity               Optional
     * @param effectiveDate          Required
     * @param terminationDate        Optional
     * @param scheduleStartTime      String Required, format: "08:16" or "8:16"
     * @param scheduleEndTime        String Optional, format: "08:16" or "8:16"
     * @param recurrencePattern      Optional
     * @param facilityScheduleReason Optional
     */
    public Schedule(Long scheduleIdentification, @NotNull String scheduleCategory, @NotNull Long facilityId, Long forInternalLocationId, Set<Long> atInternalLocationIds,
                    Set<Long> locationIds, Set<Long> organizationIds, Long capacity, @NotNull Date effectiveDate, Date terminationDate, @NotNull String scheduleStartTime,
                    String scheduleEndTime, RecurrencePatternType recurrencePattern, String facilityScheduleReason) {
        super();
        this.scheduleIdentification = scheduleIdentification;
        this.scheduleCategory = scheduleCategory;
        this.facilityId = facilityId;
        this.forInternalLocationId = forInternalLocationId;
        this.atInternalLocationIds = atInternalLocationIds;
        this.locationIds = locationIds;
        this.organizationIds = organizationIds;
        this.capacity = capacity;
        this.effectiveDate = effectiveDate;
        this.terminationDate = terminationDate;
        this.scheduleStartTime = scheduleStartTime;
        this.scheduleEndTime = scheduleEndTime;
        this.recurrencePattern = recurrencePattern;
        this.facilityScheduleReason = facilityScheduleReason;
    }

    /**
     * Get Schedule Identification
     * <p>An unique identifier for a schedule.
     *
     * @return Long optional, null for creation; required for update. scheduleIdentification
     */
    public Long getScheduleIdentification() {
        return scheduleIdentification;
    }

    /**
     * Set Schedule Identification
     * <p>An unique identifier for a schedule.
     *
     * @param scheduleIdentification Long optional, null for creation; required for update.  the scheduleIdentification to set
     */
    public void setScheduleIdentification(Long scheduleIdentification) {
        this.scheduleIdentification = scheduleIdentification;
    }

    /**
     * Get Schedule Category
     * <p>The type of activity that can use this schedule.
     * Schedule category is a subset of activity category values,
     * i.e not all types of activity have schedules.
     * A schedule cannot be defined for a non-existing type of activity.
     *
     * @return String required. scheduleCategory
     */
    public String getScheduleCategory() {
        return scheduleCategory;
    }

    /**
     * Set Schedule Category
     * <p>The type of activity that can use this schedule.
     * Schedule category is a subset of activity category values,
     * i.e not all types of activity have schedules.
     * A schedule cannot be defined for a non-existing type of activity.
     *
     * @param scheduleCategory String required. scheduleCategory to set
     */
    public void setScheduleCategory(String scheduleCategory) {
        this.scheduleCategory = scheduleCategory;
    }

    /**
     * Get Facility Id
     * <p>Reference to a facility.
     * Each schedule is defined for a particular facility.
     *
     * @return Long required. facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * Set Facility Id
     * <p>Reference to a facility.
     * Each schedule is defined for a particular facility.
     *
     * @param facilityId Long required. facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * Get Capacity
     * <p>The capacity of the schedule.
     * The capacity is the number activities is allowed to use a timeslot of this schedule.
     *
     * @return Long optional. capacity
     */
    public Long getCapacity() {
        return capacity;
    }

    /**
     * Set Capacity
     * <p>The capacity of the schedule.
     * The capacity is the number activities is allowed to use a timeslot of this schedule.
     *
     * @param capacity Long optional. capacity to set
     */
    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    /**
     * Get Effective Date
     * <p>The date when the schedule becomes effective.
     *
     * @return Date required. effectiveDate
     */
    public Date getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Set Effective Date
     * <p>The date when the schedule becomes effective.
     *
     * @param effectiveDate Date required. effectiveDate to set
     */
    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    /**
     * Get Termination Date
     * <p>The date when the schedule is no longer effective.
     * If a schedule has recurrence pattern, the recurrence pattern ends on the TerminationDate.
     *
     * @return Date optional. terminationDate
     */
    public Date getTerminationDate() {
        return terminationDate;
    }

    /**
     * Set Termination Date
     * <p>The date when the schedule is no longer effective.
     * If a schedule has recurrence pattern, the recurrence pattern ends on the TerminationDate.
     *
     * @param terminationDate Date optional. terminationDate to set
     */
    public void setTerminationDate(Date terminationDate) {
        this.terminationDate = terminationDate;
    }

    /**
     * Get Active Schedule Flag
     * <p>Schedule status is “Active” if ActiveScheduleFlag = True. Else, schedule status is “Inactive”.
     * <p>ActiveScheduleFlag is derived based on EffectiveDate and TerminationDate.
     * <li>ActiveScheduleFlag = True, if EffectiveDate <= current date and TerminationDate is blank, or if
     * EffectiveDate <= current date and TerminationDate > current date;</li>
     * <li>ActiveScheduleFlag = False, if EffectiveDate > current date or if TerminationDate <= current date.</li>
     *
     * @return Boolean Derived from Effective Date, Termination Date and Current Date. activeScheduleFlag
     */
    public Boolean getActiveScheduleFlag() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date today = c.getTime();

        activeScheduleFlag = this.getEffectiveDate().compareTo(today) < 0 && (this.getTerminationDate() == null
                || this.getTerminationDate() != null && this.getTerminationDate().compareTo(today) > 0);
        return activeScheduleFlag;
    }

    /**
     * Get Schedule Start Time
     * <p>The start time of a schedule. E.g 08:00.
     *
     * @return String required. scheduleStartTime
     */
    public String getScheduleStartTime() {
        return scheduleStartTime;
    }

    /**
     * Set Schedule Start Time
     * <p>The start time of a schedule. E.g 08:00.
     *
     * @param scheduleStartTime String required. scheduleStartTime to set
     */
    public void setScheduleStartTime(String scheduleStartTime) {
        this.scheduleStartTime = scheduleStartTime;
    }

    /**
     * Get Schedule End Time
     * <p>The end time of a schedule. E.g 16:00.
     *
     * @return String optional. scheduleEndTime
     */
    public String getScheduleEndTime() {
        return scheduleEndTime;
    }

    /**
     * Set Schedule End Time
     * <p>The end time of a schedule. E.g 16:00.
     *
     * @param scheduleEndTime String optional. scheduleEndTime to set
     */
    public void setScheduleEndTime(String scheduleEndTime) {
        this.scheduleEndTime = scheduleEndTime;
    }

    /**
     * Get Recurrence Pattern
     * <p>The pattern of recurrence, if the schedule is recurring.
     *
     * @return RecurrencePatternType optional. recurrencePattern
     */
    public RecurrencePatternType getRecurrencePattern() {
        return recurrencePattern;
    }

    /**
     * Set Recurrence Pattern
     * <p>The pattern of recurrence, if the schedule is recurring.
     *
     * @param recurrencePattern RecurrencePatternType optional. recurrencePattern to set
     */
    public void setRecurrencePattern(RecurrencePatternType recurrencePattern) {
        this.recurrencePattern = recurrencePattern;
    }

    /**
     * Get ForInternalLocationId
     * <p>Reference to a facility internal location.
     * Used for facility scheduling.
     * Indicates for which internal location the activity is being scheduled.
     * This internal location reference cannot be the same as AtInternalLocationReference.
     *
     * @return Long optional. forInternalLocationId
     */
    public Long getForInternalLocationId() {
        return forInternalLocationId;
    }

    /**
     * Set ForInternalLocationId
     * <p>Reference to a facility internal location.
     * Used for facility scheduling.
     * Indicates for which internal location the activity is being scheduled.
     * This internal location reference cannot be the same as AtInternalLocationReference.
     *
     * @param forInternalLocationId Long optional. forInternalLocationId to set
     */
    public void setForInternalLocationId(Long forInternalLocationId) {
        this.forInternalLocationId = forInternalLocationId;
    }

    /**
     * Get Facility Schedule Reason
     * <p>The facility schedule reason is one of referencde codes in FacilityScheduleReason set.
     * Used by facility scheduling.
     * Specifies the reason for the movement.
     *
     * @return String optional. facilityScheduleReason
     */
    public String getFacilityScheduleReason() {
        return facilityScheduleReason;
    }

    /**
     * Set Facility Schedule Reason
     * <p>The facility schedule reason is one of referencde codes in FacilityScheduleReason set.
     * Used by facility scheduling.
     * Specifies the reason for the movement.
     *
     * @param facilityScheduleReason String optional. facilityScheduleReason to set.
     */
    public void setFacilityScheduleReason(String facilityScheduleReason) {
        this.facilityScheduleReason = facilityScheduleReason;
    }

    /**
     * Get AtInternalLocationIds
     * <p>Reference to the internal locations where an activity using this schedule is allowed to take place.
     * This internal location reference cannot be the same as ForInternalLocationReference.
     *
     * @return Set&lt;Long> optional. atInternalLocation
     */
    public Set<Long> getAtInternalLocationIds() {
        return atInternalLocationIds;
    }

    /**
     * Set AtInternalLocationIds
     * <p>Reference to the internal locations where an activity using this schedule is allowed to take place.
     * This internal location reference cannot be the same as ForInternalLocationReference.
     *
     * @param atInternalLocationIds Set&lt;Long> optional. atInternalLocationIds to set
     */
    public void setAtInternalLocationIds(Set<Long> atInternalLocationIds) {
        this.atInternalLocationIds = atInternalLocationIds;
    }

    /**
     * Get Location Ids
     * <p>Reference to locations (addresses) where an activity using this schedule is allowed to take place.
     *
     * @return Set&lt;Long> optional. locationIds
     */
    public Set<Long> getLocationIds() {
        return locationIds;
    }

    /**
     * Set Location Ids
     * <p>Reference to locations (addresses) where an activity using this schedule is allowed to take place.
     *
     * @param locationIds Set&lt;Long> optional. location to set
     */
    public void setLocationIds(Set<Long> locationIds) {
        this.locationIds = locationIds;
    }

    /**
     * Get Organization Ids
     * <p>Reference to the organizations where an activity using this schedule is allowed to takes place.
     *
     * @return Set&lt;Long> optional. organizationIds
     */
    public Set<Long> getOrganizationIds() {
        return organizationIds;
    }

    /**
     * Set Organization Ids
     * <p>Reference to the organizations where an activity using this schedule is allowed to takes place.
     *
     * @param organizationIds Set&lt;Long> optional. organizationIds to set
     */
    public void setOrganizationIds(Set<Long> organizationIds) {
        this.organizationIds = organizationIds;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((activeScheduleFlag == null) ? 0 : activeScheduleFlag.hashCode());
        result = prime * result + ((atInternalLocationIds == null) ? 0 : atInternalLocationIds.hashCode());
        result = prime * result + ((capacity == null) ? 0 : capacity.hashCode());
        result = prime * result + ((effectiveDate == null) ? 0 : effectiveDate.hashCode());
        result = prime * result + ((facilityId == null) ? 0 : facilityId.hashCode());
        result = prime * result + ((facilityScheduleReason == null) ? 0 : facilityScheduleReason.hashCode());
        result = prime * result + ((forInternalLocationId == null) ? 0 : forInternalLocationId.hashCode());
        result = prime * result + ((locationIds == null) ? 0 : locationIds.hashCode());
        result = prime * result + ((organizationIds == null) ? 0 : organizationIds.hashCode());
        result = prime * result + ((recurrencePattern == null) ? 0 : recurrencePattern.hashCode());
        result = prime * result + ((scheduleCategory == null) ? 0 : scheduleCategory.hashCode());
        result = prime * result + ((scheduleEndTime == null) ? 0 : scheduleEndTime.hashCode());
        result = prime * result + ((scheduleIdentification == null) ? 0 : scheduleIdentification.hashCode());
        result = prime * result + ((scheduleStartTime == null) ? 0 : scheduleStartTime.hashCode());
        result = prime * result + ((terminationDate == null) ? 0 : terminationDate.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Schedule other = (Schedule) obj;
        if (activeScheduleFlag == null) {
            if (other.activeScheduleFlag != null) {
                return false;
            }
        } else if (!activeScheduleFlag.equals(other.activeScheduleFlag)) {
            return false;
        }
        if (atInternalLocationIds == null) {
            if (other.atInternalLocationIds != null) {
                return false;
            }
        } else if (!atInternalLocationIds.equals(other.atInternalLocationIds)) {
            return false;
        }
        if (capacity == null) {
            if (other.capacity != null) {
                return false;
            }
        } else if (!capacity.equals(other.capacity)) {
            return false;
        }
        if (effectiveDate == null) {
            if (other.effectiveDate != null) {
                return false;
            }
        } else if (!effectiveDate.equals(other.effectiveDate)) {
            return false;
        }
        if (facilityId == null) {
            if (other.facilityId != null) {
                return false;
            }
        } else if (!facilityId.equals(other.facilityId)) {
            return false;
        }
        if (facilityScheduleReason == null) {
            if (other.facilityScheduleReason != null) {
                return false;
            }
        } else if (!facilityScheduleReason.equals(other.facilityScheduleReason)) {
            return false;
        }
        if (forInternalLocationId == null) {
            if (other.forInternalLocationId != null) {
                return false;
            }
        } else if (!forInternalLocationId.equals(other.forInternalLocationId)) {
            return false;
        }
        if (locationIds == null) {
            if (other.locationIds != null) {
                return false;
            }
        } else if (!locationIds.equals(other.locationIds)) {
            return false;
        }
        if (organizationIds == null) {
            if (other.organizationIds != null) {
                return false;
            }
        } else if (!organizationIds.equals(other.organizationIds)) {
            return false;
        }
        if (recurrencePattern == null) {
            if (other.recurrencePattern != null) {
                return false;
            }
        } else if (!recurrencePattern.equals(other.recurrencePattern)) {
            return false;
        }
        if (scheduleCategory == null) {
            if (other.scheduleCategory != null) {
                return false;
            }
        } else if (!scheduleCategory.equals(other.scheduleCategory)) {
            return false;
        }
        if (scheduleEndTime == null) {
            if (other.scheduleEndTime != null) {
                return false;
            }
        } else if (!scheduleEndTime.equals(other.scheduleEndTime)) {
            return false;
        }
        if (scheduleIdentification == null) {
            if (other.scheduleIdentification != null) {
                return false;
            }
        } else if (!scheduleIdentification.equals(other.scheduleIdentification)) {
            return false;
        }
        if (scheduleStartTime == null) {
            if (other.scheduleStartTime != null) {
                return false;
            }
        } else if (!scheduleStartTime.equals(other.scheduleStartTime)) {
            return false;
        }
        if (terminationDate == null) {
            if (other.terminationDate != null) {
                return false;
            }
        } else if (!terminationDate.equals(other.terminationDate)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ScheduleType [scheduleIdentification=" + scheduleIdentification + ", scheduleCategory=" + scheduleCategory + ", facilityId=" + facilityId
                + ", forInternalLocationId=" + forInternalLocationId + ", atInternalLocationIds=" + atInternalLocationIds + ", locationIds=" + locationIds
                + ", organizationIds=" + organizationIds + ", capacity=" + capacity + ", effectiveDate=" + effectiveDate + ", terminationDate=" + terminationDate
                + ", activeScheduleFlag=" + activeScheduleFlag + ", scheduleStartTime=" + scheduleStartTime + ", scheduleEndTime=" + scheduleEndTime
                + ", recurrencePattern=" + recurrencePattern + ", facilityScheduleReason=" + facilityScheduleReason + "]";
    }

}
