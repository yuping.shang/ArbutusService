package syscon.arbutus.product.services.activity.contract.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by ian on 9/15/15.
 * This class is used to link an activityId to schedule details such as it's start time and recurrence object.
 * The existence of this class is to aid in bridging the module gap between activity and facility counts
 */
public class ScheduleActivityLinkType implements Serializable {

    private Date startTime;
    private Long activityId;
    private AbstractRecurrenceObject recurrenceObject;

    public ScheduleActivityLinkType() {
    }

    public ScheduleActivityLinkType(Date startTime, Long activityId, AbstractRecurrenceObject recurrenceObject) {
        this.startTime = startTime;
        this.activityId = activityId;
        this.recurrenceObject = recurrenceObject;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public AbstractRecurrenceObject getRecurrenceObject() {
        return recurrenceObject;
    }

    public void setRecurrenceObject(AbstractRecurrenceObject recurrenceObject) {
        this.recurrenceObject = recurrenceObject;
    }
}
