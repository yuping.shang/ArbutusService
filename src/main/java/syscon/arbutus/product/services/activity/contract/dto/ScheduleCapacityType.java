package syscon.arbutus.product.services.activity.contract.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Definition of a Schedule Capacity type
 *
 * @author yshang
 * @version 1.0 (Based on SDD 1.0)
 * @since April 27, 2012
 */
public class ScheduleCapacityType implements Serializable {

    private static final long serialVersionUID = -8006290990678942736L;
    @NotNull
    private Long scheduleID;
    @Valid
    private Set<TimeslotCapacityType> timeslotCapacities;
    private Long count;

    /**
     * Constructor
     */
    public ScheduleCapacityType() {
        super();
    }

    /**
     * Constructor
     *
     * @param scheduleCapacity required when invoking this constructor.
     */
    public ScheduleCapacityType(ScheduleCapacityType scheduleCapacity) {
        super();
        this.scheduleID = scheduleCapacity.getScheduleID();
        this.timeslotCapacities = scheduleCapacity.getTimeslotCapacities();
        this.count = scheduleCapacity.getCount();
    }

    /**
     * Constructor
     * <ul>
     * <li>scheduleID -- identifier of schedule.</li>
     * <li>timeslotCapacities -- Contains the capacity information of the set of timeslots that are included in the capacity check.</li>
     * <li>count -- Number of timeslots included in the capacity check.</li>
     * </ul>
     *
     * @param scheduleID         Required
     * @param timeslotCapacities Optional
     * @param count              Ignored
     */
    public ScheduleCapacityType(@NotNull Long scheduleID, @Valid Set<TimeslotCapacityType> timeslotCapacities, Long count) {
        super();
        this.scheduleID = scheduleID;
        this.timeslotCapacities = timeslotCapacities;
        this.count = count;
    }

    /**
     * Get Schedule ID
     * <p>scheduleID a schedule Id
     *
     * @return Long required. the scheduleID
     */
    public Long getScheduleID() {
        return scheduleID;
    }

    /**
     * Set Schedule ID
     * <p>scheduleID a schedule Id
     *
     * @param scheduleID Long required. scheduleID
     *                   the scheduleID to set
     */
    public void setScheduleID(Long scheduleID) {
        this.scheduleID = scheduleID;
    }

    /**
     * Get Timeslot Capacities
     * <p>Contains the capacity information of the set of timeslots that are included in the capacity check.
     *
     * @return Set&lt;TimeslotCapacityType> optional. timeslotCapacities
     */
    public Set<TimeslotCapacityType> getTimeslotCapacities() {
        if (timeslotCapacities == null) {
			timeslotCapacities = new HashSet<TimeslotCapacityType>();
		}
        return timeslotCapacities;
    }

    /**
     * Set Timeslot Capacities
     * <p>Contains the capacity information of the set of timeslots that are included in the capacity check.
     *
     * @param timeslotCapacities Set&lt;TimeslotCapacityType> optional. timeslotCapacities
     *                           the timeslotCapacities to set
     */
    public void setTimeslotCapacities(Set<TimeslotCapacityType> timeslotCapacities) {
        this.timeslotCapacities = timeslotCapacities;
    }

    /**
     * Get Count
     * <p>Number of timeslots included in the capacity check.
     *
     * @return Long optional. count
     */
    public Long getCount() {
        return count;
    }

    /**
     * Set Count
     * <p>Number of timeslots included in the capacity check.
     *
     * @param count Long optional. count
     *              the count to set
     */
    public void setCount(Long count) {
        this.count = count;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((count == null) ? 0 : count.hashCode());
        result = prime * result + ((scheduleID == null) ? 0 : scheduleID.hashCode());
        result = prime * result + ((timeslotCapacities == null) ? 0 : timeslotCapacities.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (!(obj instanceof ScheduleCapacityType)) {
			return false;
		}
        ScheduleCapacityType other = (ScheduleCapacityType) obj;
        if (count == null) {
            if (other.count != null) {
				return false;
			}
        } else if (!count.equals(other.count)) {
			return false;
		}
        if (scheduleID == null) {
            if (other.scheduleID != null) {
				return false;
			}
        } else if (!scheduleID.equals(other.scheduleID)) {
			return false;
		}
        if (timeslotCapacities == null) {
            if (other.timeslotCapacities != null) {
				return false;
			}
        } else if (!timeslotCapacities.equals(other.timeslotCapacities)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ScheduleCapacityType [scheduleID=" + scheduleID + ", timeslotCapacities=" + timeslotCapacities + ", count=" + count + "]";
    }

}
