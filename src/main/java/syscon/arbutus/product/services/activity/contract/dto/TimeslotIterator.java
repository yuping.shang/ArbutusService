package syscon.arbutus.product.services.activity.contract.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;

/**
 * TimeslotIterator for Schedule
 *
 * @author yshang
 * @since May 26, 2014
 */
public interface TimeslotIterator extends Iterator<ScheduleTimeslotType>, Serializable {

    /**
     * skips all dates in the series before the given date.
     *
     * @param newStart non null.
     */
    void advanceTo(Date newStart);

}
