package syscon.arbutus.product.services.activity.contract.dto;

/**
 * Created by ian on 9/22/15.
 */
public enum Day {
    MON,
    TUE,
    WED,
    THU,
    FRI,
    SAT,
    SUN;

    public static Day fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }
}