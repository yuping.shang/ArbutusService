package syscon.arbutus.product.services.activity.contract.dto;

/**
 * <p>ActivityCategory -- Category for Activity Service.
 *
 * @author yshang
 * @version 1.0 (Based on SDD 1.0)
 * @since April 27, 2012
 */
public enum ActivityCategory {

    /**
     * Movement
     */
    MOVEMENT,
    /**
     * Housing and Bed Management
     */
    HBDMGMT,
    /**
     * Facility Count
     */
    FACOUNT,
    /**
     * Visitation
     */
    VISITATION,
    /**
     * Shift Log
     */
    SHIFTLOG,
    /**
     * Case Note
     */
    CASENOTE,
    /**
     * Assessment
     */
    ASSESSMENT,
    /**
     * Case
     */
    CASE,
    /**
     * Incident
     */
    INCIDENT,
    /**
     * Discipline
     */
    DISCIPLINE,

    /**
     * Grievances
     */
    GRIEVANCES,
    /**
     * Program and Services
     */
    PROGRAM;

    public static ActivityCategory fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
