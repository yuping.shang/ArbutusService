package syscon.arbutus.product.services.activity.contract.dto;

/**
 * Created by ian on 9/8/15.
 */
public class WeekdayRecurrenceObject extends AbstractRecurrenceObject {

    public WeekdayRecurrenceObject() {
        this.recurrenceEnum = RecurrenceEnum.WEEKDAYS;
    }

}
