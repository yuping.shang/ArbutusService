package syscon.arbutus.product.services.activity.contract.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Definition of a Schedule return type
 *
 * @author yshang
 * @version 1.0 (Based on SDD 1.0)
 * @since April 27, 2012
 */
public class SchedulesReturnType implements Serializable {

    private static final long serialVersionUID = 6168826942937613447L;

    private List<? extends Schedule> schedules;
    private Long totalSize;

    /**
     * @return the schedules
     */
    public List<? extends Schedule> getSchedules() {
        return schedules;
    }

    /**
     * @param schedules the schedules to set
     */
    public void setSchedules(List<? extends Schedule> schedules) {
        this.schedules = schedules;
    }

    /**
     * @return the totalSize
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * @param totalSize the totalSize to set
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SchedulesReturnType [schedules=" + schedules + ", totalSize=" + totalSize + "]";
    }

}
