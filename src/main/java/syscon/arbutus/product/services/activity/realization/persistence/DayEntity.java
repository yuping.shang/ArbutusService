package syscon.arbutus.product.services.activity.realization.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * DayEntity for Activity Service
 * 
 * @DbComment ACT_SchDay 'Recur on Day table for Schedule Table of Activity Service'
 * 
 * @author yshang
 * @since December 02, 2013
 * @version 1.0
 *
 */
@Audited
@Entity
@Table(name = "ACT_SchDay")
public class DayEntity implements Serializable {

	private static final long serialVersionUID = 8422841211347901855L;
	
	/**
	 * @DbComment ACT_SchDay.actSchDayId 'Recur on day ID'
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_ACT_SCHDAY_ID")
	@SequenceGenerator(name = "SEQ_ACT_SCHDAY_ID", sequenceName = "SEQ_ACT_SCHDAY_ID", allocationSize=1)
	@Column(name = "actSchDayId")
	private Long id;

	/**
	 * @DbComment ACT_SchDay.day 'recur on day. recurOnDays Indicate the specific days of the week to recur on. If RecurOnDays is populated then WeekdayOnlyFlag = false and RecurringInterval is ignored.'
	 */
	@Column(name = "day", length = 64)
	@MetaCode(set = MetaSet.DAY)
	private String day;

	/**
	 * @DbComment ACT_SchDay.createUserId 'Create User Id'
	 * @DbComment ACT_SchDay.createDateTime 'Create Date Time'
	 * @DbComment ACT_SchDay.modifyUserId 'Modify User Id'
	 * @DbComment ACT_SchDay.modifyDateTime 'Modify Date Time'
	 * @DbComment ACT_SchDay.invocationContext 'Invocation Context'
	 */
	@Embedded
	@NotAudited
	private StampEntity stamp;

	public DayEntity() {
		super();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the day
	 */
	public String getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(String day) {
		this.day = day;
	}

	/**
	 * @return the stamp
	 */
	public StampEntity getStamp() {
		return stamp;
	}

	/**
	 * @param stamp the stamp to set
	 */
	public void setStamp(StampEntity stamp) {
		this.stamp = stamp;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DayEntity other = (DayEntity) obj;
		if (day == null) {
			if (other.day != null)
				return false;
		} else if (!day.equals(other.day))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DayEntity [id=" + id + ", day=" + day + "]";
	}

}
