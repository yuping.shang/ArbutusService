package syscon.arbutus.product.services.activity.contract.ejb;

import java.util.*;
import java.util.stream.Collectors;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.*;
import syscon.arbutus.product.services.activity.contract.util.ActivityQueryUtil;
import syscon.arbutus.product.services.activity.contract.util.RecurrenceObjectFactory;
import syscon.arbutus.product.services.activity.realization.persistence.*;

/**
 * Created by ian on 9/22/15.
 *
 * THIS IS THE BEGINNINGS OF A DAO OBJECT FOR ACTIVITY SERVICE
 */
public class ActivityServiceDAO {

    // should deprecate this const
    public static final Long SUCCESS = 1L;

    private static Logger log = LoggerFactory.getLogger(ActivityServiceDAO.class);


    public List<Schedule> getSchedulesFromConstraints(Session session, Set<Long> facilityIds, Set<Long> facilityInternalLocationIdSet, String activityCategory,
            LocalDate sessionDate, LocalTime startTime, LocalTime endTime, RecurrencePatternType recurrencePattern, boolean isTimeStrictEQ) {
        List<ScheduleEntity> scheduleEntities = ActivityQueryUtil.getScheduleCriteria(session, facilityIds, facilityInternalLocationIdSet, activityCategory, sessionDate,
                startTime, endTime, recurrencePattern, isTimeStrictEQ).list();
        return scheduleEntities.stream().map(ActivityServiceHelper::toSchedule).collect(Collectors.toList());
    }

    public Long deleteAllActivities(Session session) {
        if (log.isDebugEnabled()) {
            log.debug("deleteAllActivities: begin");
        }

        session.createSQLQuery("UPDATE ACT_Activity SET flag = 1").executeUpdate();
        Criteria c = session.createCriteria(ActivityEntity.class);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        c.setProjection(Projections.id());
        @SuppressWarnings("unchecked") Iterator<Long> it = c.list().iterator();
        while (it.hasNext()) {
            Long id = it.next();
            ActivityEntity entity = (ActivityEntity) session.get(ActivityEntity.class, id);
            // session.delete(entity);
            entity.setDescendantIds(null);
        }

        session.createQuery("delete ActivityEntity").executeUpdate();

        session.flush();
        session.clear();

        if (log.isDebugEnabled()) {
            log.debug("deleteAllActivities: end");
        }
        return SUCCESS;
    }

    /**
     * Delete All ScheduleType Config Variable.
     * <p>For maintenance use.</p>
     *
     * @param session - Session
     * @return Long - success: 1; failure: null or error code.
     */
    public Long deleteAllScheduleConfigVar(Session session) {
        if (log.isDebugEnabled()) {
            log.debug("deleteAllScheduleConfigVar: begin");
        }

        session.createQuery("delete ScheduleConfigVariableEntity").executeUpdate();

        // Clear Hibernate cache to add to avoid unique key violation.
        session.flush();
        session.clear();

        if (log.isDebugEnabled()) {
            log.debug("deleteAllScheduleConfigVar: end");
        }

        return SUCCESS;
    }

    public List<ScheduleActivityLinkType> getScheduleActivityLinkTypesFromActivityIds(Session session, List<Long> activityIds) {
        List<ScheduleActivityLinkType> returnVar = new ArrayList<>();

        if (activityIds != null && !activityIds.isEmpty()) {
            String hqlQueryString = "SELECT sch.scheduleStartTime, act.activityId, sch.actSchPtn FROM ScheduleEntity sch, ActivityEntity act  "
                    + "WHERE sch.scheduleIdentification = act.scheduleID and act.activityId in :fcaIds";

            Query query = session.createQuery(hqlQueryString);
            query.setParameterList("fcaIds", activityIds);
            List<Object[]> objects = query.list();
            returnVar = objects.stream().map((obj) -> new ScheduleActivityLinkType((Date) obj[0], (Long) obj[1],
                    new RecurrenceObjectFactory().generate(ActivityServiceHelper.toRecurrencePattern((RecurrencePatternEntity) obj[2])))).collect(Collectors.toList());
        }

        return returnVar;
    }



}
