package syscon.arbutus.product.services.activity.contract.dto;

/**
 * Created by ian on 9/22/15.
 */
public enum Weekday {
    MON,
    TUE,
    WED,
    THU,
    FRI;

    public static Weekday fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }
}