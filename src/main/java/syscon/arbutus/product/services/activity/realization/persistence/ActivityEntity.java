package syscon.arbutus.product.services.activity.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * ActivityEntity for Activity Service 1.0
 *
 * @author yshang, Syscon Justice Systems
 * @version 1.0
 * @DbComment ACT_Activity 'Main table for Activity Service'
 * @since September 18, 2012
 */

@Audited
@Entity
@Table(name = "ACT_Activity")
@SQLDelete(sql = "UPDATE ACT_Activity SET flag = 4 WHERE activityId = ? and version = ?")
@Where(clause = "flag = 1")
public class ActivityEntity implements Serializable {

    private static final long serialVersionUID = 2903660313540740928L;

    /**
     * @DbComment ACT_Activity.activityId 'Activity Id'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_ACT_ACTIVITY_ID")
    @SequenceGenerator(name = "SEQ_ACT_ACTIVITY_ID", sequenceName = "SEQ_ACT_ACTIVITY_ID", allocationSize = 1)
    @Column(name = "activityId")
    private Long activityId;

    /**
     * @DbComment ACT_Activity.version 'Version for Hibernate use'
     */
    @Version
    @NotAudited
    @Column(name = "version")
    private Long version;

    /**
     * @DbComment ACT_Activity.supervisionId 'The supervision id of the offender that the activity belongs to.'
     */
    @Column(name = "supervisionId", nullable = true)
    private Long supervisionId;

    /**
     * @DbComment ACT_Activity.status 'Activity status flag -- indicate if the activity is active'
     */
    @Column(name = "status", nullable = false)
    private Boolean activeStatusFlag;

    /**
     * @DbComment ACT_Activity.startDate 'Planned start date of an activity'
     */
    @Column(name = "startDate", nullable = true)
    private Date plannedStartDate;

    /**
     * @DbComment ACT_Activity.endDate 'Planned end date of an activity'
     */
    @Column(name = "endDate", nullable = true)
    private Date plannedEndDate;

    /**
     * @DbComment ACT_Activity.category 'Activity Category'
     */
    @Column(name = "category", nullable = false, length = 64)
    @MetaCode(set = MetaSet.ACTIVITY_CATEGORY)
    private String activityCategory;

    /**
     * @DbComment ACT_Activity.antecedentId 'Antecedent of current activity'
     */
    @Column(name = "antecedentId", nullable = true)
    private Long antecedentId;

    /**
     * @DbComment ACT_Descendant. 'Descendant table for Activity Service'
     * @DbComment ACT_Descendant.activityId 'Foreign Key to ACT_Activity'
     * @DbComment ACT_Descendant.descendant 'Id of ACT_Descendant'
     */
    @ForeignKey(name = "Fk_ACT_Activity")
    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "ACT_Descendant", joinColumns = @JoinColumn(name = "activityId"))
    @Column(name = "descendant")
    private Set<Long> descendantIds;

    /**
     * @DbComment ACT_Activity.scheduleId 'Schedule ID'
     */
    @Column(name = "scheduleId")
    private Long scheduleID;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * @DbComment ACT_Activity.createUserId 'Create User Id'
     * @DbComment ACT_Activity.createDateTime 'Create Date Time'
     * @DbComment ACT_Activity.modifyUserId 'Modify User Id'
     * @DbComment ACT_Activity.modifyDateTime 'Modify Date Time'
     * @DbComment ACT_Activity.invocationContext 'Invocation Context'
     */
    @Embedded
    @NotAudited
    private StampEntity stamp;

    public ActivityEntity() {
        super();
    }

    public ActivityEntity(ActivityEntity activity) {
        super();
        this.activityId = activity.getActivityId();
        this.supervisionId = activity.getSupervisionId();
        this.activeStatusFlag = activity.getActiveStatusFlag();
        this.plannedStartDate = activity.getPlannedStartDate();
        this.plannedEndDate = activity.getPlannedEndDate();
        this.activityCategory = activity.getActivityCategory();
        this.antecedentId = activity.getAntecedentId();
        this.descendantIds = activity.getDescendantIds();
        this.scheduleID = activity.getScheduleID();
    }

    /**
     * @param activityId
     * @param supervisionId
     * @param activeStatusFlag
     * @param plannedStartDate
     * @param plannedEndDate
     * @param activityCategory
     * @param antecedentId
     * @param descendantIds
     * @param scheduleID
     */
    public ActivityEntity(Long activityId, Long supervisionId, Boolean activeStatusFlag, Date plannedStartDate, Date plannedEndDate, String activityCategory,
            Long antecedentId, Set<Long> descendantIds, Long scheduleID) {
        super();
        this.activityId = activityId;
        this.supervisionId = supervisionId;
        this.activeStatusFlag = activeStatusFlag;
        this.plannedStartDate = plannedStartDate;
        this.plannedEndDate = plannedEndDate;
        this.activityCategory = activityCategory;
        this.antecedentId = antecedentId;
        this.descendantIds = descendantIds;
        this.scheduleID = scheduleID;
    }

    /**
     * @return the activityId
     */
    public Long getActivityId() {
        return activityId;
    }

    /**
     * @param activityId the activityId to set
     */
    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    /**
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * @param supervisionId the supervisionAssoc to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the activeStatusFlag
     */
    public Boolean getActiveStatusFlag() {
        return activeStatusFlag;
    }

    /**
     * @param activeStatusFlag the activeStatusFlag to set
     */
    public void setActiveStatusFlag(Boolean activeStatusFlag) {
        this.activeStatusFlag = activeStatusFlag;
    }

    /**
     * @return the plannedStartDate
     */
    public Date getPlannedStartDate() {
        return plannedStartDate;
    }

    /**
     * @param plannedStartDate the plannedStartDate to set
     */
    public void setPlannedStartDate(Date plannedStartDate) {
        this.plannedStartDate = plannedStartDate;
    }

    /**
     * @return the plannedEndDate
     */
    public Date getPlannedEndDate() {
        return plannedEndDate;
    }

    /**
     * @param plannedEndDate the plannedEndDate to set
     */
    public void setPlannedEndDate(Date plannedEndDate) {
        this.plannedEndDate = plannedEndDate;
    }

    /**
     * @return the activityCategory
     */
    public String getActivityCategory() {
        return activityCategory;
    }

    /**
     * @param activityCategory the activityCategory to set
     */
    public void setActivityCategory(String activityCategory) {
        this.activityCategory = activityCategory;
    }

    /**
     * @return the antecedentId
     */
    public Long getAntecedentId() {
        return antecedentId;
    }

    /**
     * @param antecedentId the antecedentId to set
     */
    public void setAntecedentId(Long antecedentId) {
        this.antecedentId = antecedentId;
    }

    /**
     * @return the descendantIds
     */
    public Set<Long> getDescendantIds() {
        if (descendantIds == null) {
			descendantIds = new HashSet<Long>();
		}
        return descendantIds;
    }

    /**
     * @param descendantIds the descendantIds to set
     */
    public void setDescendantIds(Set<Long> descendantIds) {
        this.descendantIds = descendantIds;
    }

    /**
     * @return the scheduleID
     */
    public Long getScheduleID() {
        return scheduleID;
    }

    /**
     * @param scheduleID the scheduleID to set
     */
    public void setScheduleID(Long scheduleID) {
        this.scheduleID = scheduleID;
    }

    /**
     * @return the flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        result = prime * result + ((activeStatusFlag == null) ? 0 : activeStatusFlag.hashCode());
        result = prime * result + ((activityCategory == null) ? 0 : activityCategory.hashCode());
        result = prime * result + ((activityId == null) ? 0 : activityId.hashCode());
        result = prime * result + ((plannedEndDate == null) ? 0 : plannedEndDate.hashCode());
        result = prime * result + ((plannedStartDate == null) ? 0 : plannedStartDate.hashCode());
        result = prime * result + ((scheduleID == null) ? 0 : scheduleID.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (!(obj instanceof ActivityEntity)) {
			return false;
		}
        ActivityEntity other = (ActivityEntity) obj;
        if (supervisionId == null) {
            if (other.supervisionId != null) {
				return false;
			}
        } else if (!supervisionId.equals(other.supervisionId)) {
			return false;
		}
        if (activeStatusFlag == null) {
            if (other.activeStatusFlag != null) {
				return false;
			}
        } else if (!activeStatusFlag.equals(other.activeStatusFlag)) {
			return false;
		}
        if (activityCategory == null) {
            if (other.activityCategory != null) {
				return false;
			}
        } else if (!activityCategory.equals(other.activityCategory)) {
			return false;
		}
        if (activityId == null) {
            if (other.activityId != null) {
				return false;
			}
        } else if (!activityId.equals(other.activityId)) {
			return false;
		}
        if (plannedEndDate == null) {
            if (other.plannedEndDate != null) {
				return false;
			}
        } else if (!plannedEndDate.equals(other.plannedEndDate)) {
			return false;
		}
        if (plannedStartDate == null) {
            if (other.plannedStartDate != null) {
				return false;
			}
        } else if (!plannedStartDate.equals(other.plannedStartDate)) {
			return false;
		}
        if (scheduleID == null) {
            if (other.scheduleID != null) {
				return false;
			}
        } else if (!scheduleID.equals(other.scheduleID)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ActivityEntity [activityId=" + activityId + ", version=" + version + ", supervisionId=" + supervisionId + ", activeStatusFlag=" + activeStatusFlag
                + ", plannedStartDate=" + plannedStartDate + ", plannedEndDate=" + plannedEndDate + ", activityCategory=" + activityCategory + ", antecedentId="
                + antecedentId + ", scheduleID=" + scheduleID + ", stamp=" + stamp + "]";
    }

}
