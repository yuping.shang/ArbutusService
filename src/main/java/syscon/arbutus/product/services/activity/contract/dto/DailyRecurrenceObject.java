package syscon.arbutus.product.services.activity.contract.dto;

/**
 * Created by ian on 9/8/15.
 */
public class DailyRecurrenceObject extends AbstractRecurrenceObject {

    public DailyRecurrenceObject() {
        this.recurrenceEnum = RecurrenceEnum.DAILY;
    }

}
