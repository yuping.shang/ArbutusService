package syscon.arbutus.product.services.activity.contract.util;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import syscon.arbutus.product.services.activity.contract.dto.*;
import syscon.arbutus.product.services.realization.util.BeanHelper;

/**
 * Created by ian on 9/22/15.
 */
public class ScheduleRecurrenceUtil {

    /**
     * calculateRecurrenceEnum
     * <p>returns the equivalent RecurrenceEnum given a particular recurrence pattern
     * </p>
     * @param patternType - RecurrencePatternType
     * @return RecurrenceEnum
     */
    public RecurrenceEnum calculateRecurrenceEnum(RecurrencePatternType patternType) {
        if (patternType == null || !(patternType instanceof DailyWeeklyCustomRecurrencePatternType))
            return null;

        DailyWeeklyCustomRecurrencePatternType pattern = (DailyWeeklyCustomRecurrencePatternType) patternType;

        if (pattern.isWeekdayOnlyFlag() == true) {
            return RecurrenceEnum.WEEKDAYS;
        } else if (pattern.getRecurOnDays() != null && !pattern.getRecurOnDays().isEmpty()) {
            return RecurrenceEnum.CUSTOM;
        } else {
            return RecurrenceEnum.DAILY;
        }
    }

    /**
     * Returns true if two schedules are overlapping. False otherwise. This method does not
     * rely on any database operations. The assumption being the consumer of this utility deals with
     * stale data on it's own.
     * <p>
     *     Only recurrence patterns, start date and end dates are used from the schedule parameters.
     * </p>
     * @param schedule
     * @param newSchedule
     * @return
     */
    public Boolean areSchedulesOverlapping(Schedule schedule, Schedule newSchedule) {

        RecurrenceObjectFactory factory = new RecurrenceObjectFactory();
        Function<Schedule, AbstractRecurrenceObject> getRecurrenceObject =
                s ->(SimpleSchedule.class.equals(s.getClass())) ? ((SimpleSchedule) s).getRecurrenceObject() : factory.generate(s.getRecurrencePattern());

        Boolean[] existingWeekStatus = generateWeekStatus(newSchedule.getEffectiveDate(), schedule.getTerminationDate(), getRecurrenceObject.apply(schedule));
        Boolean[] candidateWeekStatus = generateWeekStatus(newSchedule.getEffectiveDate(), newSchedule.getTerminationDate(),getRecurrenceObject.apply(newSchedule));

        // because our start and end dates might be less than the span of a week
        int maxIterations = Math.min(existingWeekStatus.length, candidateWeekStatus.length);

        return IntStream
                .range(0, maxIterations)
                .anyMatch(index -> (existingWeekStatus[index] & candidateWeekStatus[index]));
    }

    private Boolean[] generateWeekStatus(Date effectiveDate, Date terminationDate, AbstractRecurrenceObject recurrencePattern) {
        // num days in a week, duh
        int daysBetweenDates = 7;
        if (terminationDate != null) {
            daysBetweenDates =  BeanHelper.getDays(effectiveDate, terminationDate).intValue();
        }
        List<Boolean> result = Stream
                .iterate(effectiveDate, date -> BeanHelper.nextNthDays(date, 1))
                .limit(daysBetweenDates)
                .map(d -> isRecurrencePatternOnDay(recurrencePattern, d))
                .collect(Collectors.toList());
        return result.toArray(new Boolean[daysBetweenDates]);
    }

    private Boolean isRecurrencePatternOnDay(AbstractRecurrenceObject pattern, Date date) {

        if (pattern instanceof DailyRecurrenceObject) {
            return true;
        } else  {

            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int dayNumber = cal.get(Calendar.DAY_OF_WEEK);

            if (pattern instanceof WeekdayRecurrenceObject) {
                if (dayNumber == Calendar.SATURDAY || dayNumber == Calendar.SUNDAY) {
                    return false;
                } else {
                    return true;
                }
            } else if (pattern instanceof CustomRecurrenceObject) {
                DaysOfTheWeek days = ((CustomRecurrenceObject) pattern).getDaysOfTheWeek();

                switch (dayNumber) {
                    case Calendar.MONDAY:
                        return days.monday;
                    case Calendar.TUESDAY:
                        return days.tuesday;
                    case Calendar.WEDNESDAY:
                        return days.wednesday;
                    case Calendar.THURSDAY:
                        return days.thursday;
                    case Calendar.FRIDAY:
                        return days.friday;
                    case Calendar.SATURDAY:
                        return days.saturday;
                    case Calendar.SUNDAY:
                        return days.sunday;
                    default:
                        return false;
                }
            }
        }
        return false;
    }
}
