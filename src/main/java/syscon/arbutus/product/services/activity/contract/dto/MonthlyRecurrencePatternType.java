package syscon.arbutus.product.services.activity.contract.dto;

/**
 * * Java class for MonthlyRecurrencePatternType
 *
 * @author yshang
 * @version 1.0 (Based on SDD 1.0)
 * @since April 27, 2012
 */
public class MonthlyRecurrencePatternType extends RecurrencePatternType {

    private static final long serialVersionUID = 8984229927857586353L;

    private Long dayOfMonth;
    private String recurOnWeekday; // ReferenceSet(WEEKDAY)
    private String nthWeekday; // ReferenceSet(NTHWEEKDAY)

    /**
     * Constructor
     */
    public MonthlyRecurrencePatternType() {
        super();
    }

    /**
     * Constructor
     *
     * @param monthlyRecurrencePattern MonthlyRecurrencePatternType, required when invoking this constructor.
     */
    public MonthlyRecurrencePatternType(MonthlyRecurrencePatternType monthlyRecurrencePattern) {
        super();
        this.setRecurrenceInterval(monthlyRecurrencePattern.getRecurrenceInterval());
        this.dayOfMonth = monthlyRecurrencePattern.getDayOfMonth();
        this.recurOnWeekday = monthlyRecurrencePattern.getRecurOnWeekday();
        this.nthWeekday = monthlyRecurrencePattern.getNthWeekday();
    }

    /**
     * Constructor
     * <ul>
     * <li>recurrenceInterval -- Define the number of month before the schedule recurs. E.g RecurringInterval = 3, schedule recurs every 3 month. Default value is 1 month.</li>
     * <li>dayOfMonth -- Define on which day of the month the schedule recurs. E.g DayOfMonth = 10,  schedule recurs on the 10th day of the month. One of DayOfMonth, RecurOnWeekday, and NthWeekday has to be populated. If DayOfMonth is populated, RecurOnWeekday and NthWeekday cannot be populated.</li>
     * <li>recurOnWeekday -- Reference code of WEEKDAY set. Define on which weekday(s) the schedule recurs. E.g Monday, Wednesday, Friday. One of DayOfMonth, RecurOnWeekday, and NthWeekday has to be populated. If RecurOnWeekday is populated, DayOfMonth cannot be populated. NthWeekday is required.</li>
     * <li>nthWeekday -- Reference code of NTHWEEKDAY. The nth weekday that the schedule recurs. One of DayOfMonth, RecurOnWeekday, and NthWeekday has to be populated. If NthWeekday is populated, RecurOnWeekday is required.  E.g NthWeekday = First, RecurOnWeekday = Monday,  the schedule recurs on the first Monday of the month.</li>
     * </ul>
     *
     * @param recurrenceInterval Required.
     * @param dayOfMonth         Optional.
     * @param recurOnWeekday     Optional. Reference code of WEEKDAY set.
     * @param nthWeekday         Optional. Reference code of NTHWEEKDAY.
     */
    public MonthlyRecurrencePatternType(Long recurrenceInterval, Long dayOfMonth, String recurOnWeekday, String nthWeekday) {
        super();
        this.setRecurrenceInterval(recurrenceInterval);
        this.dayOfMonth = dayOfMonth;
        this.recurOnWeekday = recurOnWeekday;
        this.nthWeekday = nthWeekday;
    }

    /**
     * Constructor
     *
     * @param recurrenceInterval required. Define the number of month before the schedule recurs. E.g RecurringInterval = 3, schedule recurs every 3 month. Default value is 1 month.
     */
    public MonthlyRecurrencePatternType(Long recurrenceInterval) {
        super(recurrenceInterval);
    }

    /**
     * Constroctor
     *
     * @param recurrencePattern required.
     */
    public MonthlyRecurrencePatternType(RecurrencePatternType recurrencePattern) {
        super(recurrencePattern);
    }

    /**
     * Constructor
     *
     * @param dayOfMonth     optional. Define on which day of the month the schedule recurs. E.g DayOfMonth = 10,  schedule recurs on the 10th day of the month. One of DayOfMonth, RecurOnWeekday, and NthWeekday has to be populated. If DayOfMonth is populated, RecurOnWeekday and NthWeekday cannot be populated.
     * @param recurOnWeekday optional. Reference code of WEEKDAY set. Define on which weekday(s) the schedule recurs. E.g Monday, Wednesday, Friday. One of DayOfMonth, RecurOnWeekday, and NthWeekday has to be populated. If RecurOnWeekday is populated, DayOfMonth cannot be populated. NthWeekday is required.
     * @param nthWeekday     optional. Reference code of NTHWEEKDAY. The nth weekday that the schedule recurs. One of DayOfMonth, RecurOnWeekday, and NthWeekday has to be populated. If NthWeekday is populated, RecurOnWeekday is required.  E.g NthWeekday = First, RecurOnWeekday = Monday,  the schedule recurs on the first Monday of the month.
     */
    public MonthlyRecurrencePatternType(Long dayOfMonth, String recurOnWeekday, String nthWeekday) {
        super();
        this.dayOfMonth = dayOfMonth;
        this.recurOnWeekday = recurOnWeekday;
        this.nthWeekday = nthWeekday;
    }

    /**
     * Get DayOfMonth
     * <p>Define on which day of the month the schedule recurs. E.g DayOfMonth = 10,  schedule recurs on the 10th day of the month. One of DayOfMonth, RecurOnWeekday, and NthWeekday has to be populated. If DayOfMonth is populated, RecurOnWeekday and NthWeekday cannot be populated.
     *
     * @return Long optional. dayOfMonth
     */
    public Long getDayOfMonth() {
        return dayOfMonth;
    }

    /**
     * Set DayOfMonth
     * <p>Define on which day of the month the schedule recurs. E.g DayOfMonth = 10,  schedule recurs on the 10th day of the month. One of DayOfMonth, RecurOnWeekday, and NthWeekday has to be populated. If DayOfMonth is populated, RecurOnWeekday and NthWeekday cannot be populated.
     *
     * @param dayOfMonth Long optional. dayOfMonth the dayOfMonth to set
     */
    public void setDayOfMonth(Long dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    /**
     * Get RecurOnWeekday
     * <p>Reference code of WEEKDAY set. Define on which weekday(s) the schedule recurs. E.g Monday, Wednesday, Friday. One of DayOfMonth, RecurOnWeekday, and NthWeekday has to be populated. If RecurOnWeekday is populated, DayOfMonth cannot be populated. NthWeekday is required.
     *
     * @return String optional. recurOnWeekday
     */
    public String getRecurOnWeekday() {
        return recurOnWeekday;
    }

    /**
     * Set RecurOnWeekday
     * <p>Reference code of WEEKDAY set. Define on which weekday(s) the schedule recurs. E.g Monday, Wednesday, Friday. One of DayOfMonth, RecurOnWeekday, and NthWeekday has to be populated. If RecurOnWeekday is populated, DayOfMonth cannot be populated. NthWeekday is required.
     *
     * @param recurOnWeekday String optional. recurOnWeekday the recurOnWeekday to set
     */
    public void setRecurOnWeekday(String recurOnWeekday) {
        this.recurOnWeekday = recurOnWeekday;
    }

    /**
     * Get NthWeekday
     * <p>Reference code of NTHWEEKDAY set. The nth weekday that the schedule recurs. One of DayOfMonth, RecurOnWeekday, and NthWeekday has to be populated. If NthWeekday is populated, RecurOnWeekday is required.  E.g NthWeekday = First, RecurOnWeekday = Monday,  the schedule recurs on the first Monday of the month.
     *
     * @return String optional. nthWeekday
     */
    public String getNthWeekday() {
        return nthWeekday;
    }

    /**
     * Set NthWeekday
     * <p>Reference code of NTHWEEKDAY set. The nth weekday that the schedule recurs. One of DayOfMonth, RecurOnWeekday, and NthWeekday has to be populated. If NthWeekday is populated, RecurOnWeekday is required.  E.g NthWeekday = First, RecurOnWeekday = Monday,  the schedule recurs on the first Monday of the month.
     *
     * @param nthWeekday String optional. nthWeekday the nthWeekday to set
     */
    public void setNthWeekday(String nthWeekday) {
        this.nthWeekday = nthWeekday;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((dayOfMonth == null) ? 0 : dayOfMonth.hashCode());
        result = prime * result + ((nthWeekday == null) ? 0 : nthWeekday.hashCode());
        result = prime * result + ((recurOnWeekday == null) ? 0 : recurOnWeekday.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        MonthlyRecurrencePatternType other = (MonthlyRecurrencePatternType) obj;
        if (dayOfMonth == null) {
            if (other.dayOfMonth != null) {
				return false;
			}
        } else if (!dayOfMonth.equals(other.dayOfMonth)) {
			return false;
		}
        if (nthWeekday == null) {
            if (other.nthWeekday != null) {
				return false;
			}
        } else if (!nthWeekday.equals(other.nthWeekday)) {
			return false;
		}
        if (recurOnWeekday == null) {
            if (other.recurOnWeekday != null) {
				return false;
			}
        } else if (!recurOnWeekday.equals(other.recurOnWeekday)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "MonthlyRecurrencePatternType [dayOfMonth=" + dayOfMonth + ", recurOnWeekday=" + recurOnWeekday + ", nthWeekday=" + nthWeekday + "]";
    }

}
