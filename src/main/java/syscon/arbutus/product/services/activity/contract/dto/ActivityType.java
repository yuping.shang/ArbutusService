package syscon.arbutus.product.services.activity.contract.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

/**
 * Definition of an Activity Type
 *
 * @author yshang
 * @version 1.0 (Based on SDD 2.0)
 * @since April 27, 2012
 */
@ArbutusConstraint(constraints = { "plannedStartDate <= plannedEndDate" })
public class ActivityType implements Serializable {

    private static final long serialVersionUID = -5758522903009247196L;

    private Long activityIdentification;
    private Long supervisionId;
    @NotNull
    private Boolean activeStatusFlag;
    private Date plannedStartDate;
    private Date plannedEndDate;
    @NotNull
    private String activityCategory; // ReferenceSet(ActivityCategory)
    private Long activityAntecedentId;
    private Set<Long> activityDescendantIds;
    private Long scheduleID;

    public ActivityType() {
        super();
    }

    /**
     * Constructor
     *
     * @param activity -- ActivityType, Required when invoking this constructor
     */
    public ActivityType(ActivityType activity) {
        super();
        this.activityIdentification = activity.getActivityIdentification();
        this.supervisionId = activity.getSupervisionId();
        this.activeStatusFlag = activity.getActiveStatusFlag();
        this.plannedStartDate = activity.getPlannedStartDate();
        this.plannedEndDate = activity.getPlannedEndDate();
        this.activityCategory = activity.getActivityCategory();
        this.activityAntecedentId = activity.getActivityAntecedentId();
        this.activityDescendantIds = activity.getActivityDescendantIds();
        this.scheduleID = activity.getScheduleID();
    }

    /**
     * Constructor
     * <ul>
     * <li>activityIdentification -- A unique identification number of an activity. Created by the system.</li>
     * <li>supervisionId -- The offender’s supervision that the activity belongs to. The Id of the Supervision. Note: After it's value is persisted in DB, it can not be updated.
     * The new value will be ignored.
     * <li>activeStatusFlag -- Indicate if the activity is active.
     * <ul>
     * The Activity Status is interpreted as“Active” if ActiveStatusFlag = True.
     * </ul>
     * <ul>
     * Activity Status is interpreted as “Inactive” if ActiveStatusFlag = False.
     * </ul>
     * If an activity is active, it is taking a spot in the capacity of a timeslot of the activity’s schedule. When activity status is updated, the corresponding activity
     * augmentation status must be updated as well, and vice versa (out of scope of this service).</li>
     * <li>plannedStartDate -- Planned start date of an activity. The planned start date is either updated by the user for a unscheduled activity, or updated by the system based on
     * a schedule the user selects for the activity.</li>
     * <li>plannedEndDate -- Planned end date of an activity. The planned end date is either updated by the user for an unscheduled activity, or updated by the system based on a
     * schedule the user selects for the activity. Planned end date is required if planned start date is populated.</li>
     * <li>activityCategory -- Activity Category is derived based on the reference to a particular type of activity. For example, if activity has a reference to movement activity,
     * activity category = “Movement”. If activity has a reference to housing bed management activity, activity category = “Housing and Bed Management”.</li>
     * <li>activityAntecedentId -- Reference to an activity that is the direct antecedent of the current activity.</li>
     * <li>activityDescendantIds -- Reference to activities that are direct descendants of the current activity.</li>
     * <li>scheduleID -- The identifier of the schedule. The ScheduleTypeID is stored if the activity is created based on a predefined schedule. The value is blank if the activity
     * is not created based on a predefined schedule.</li>
     * </ul>
     *
     * @param activityIdentification Ignored by create; Required by update.
     * @param supervisionId Optional.
     * @param activeStatusFlag Required.
     * @param plannedStartDate Optional
     * @param plannedEndDate Optional
     * @param activityCategory Required
     * @param activityAntecedentId Optional
     * @param activityDescendantIds Optional
     * @param scheduleID Optional
     */
    public ActivityType(Long activityIdentification, Long supervisionId, @NotNull Boolean activeStatusFlag, Date plannedStartDate, Date plannedEndDate,
                        @NotNull String activityCategory, Long activityAntecedentId, Set<Long> activityDescendantIds, Long scheduleID) {
        super();
        this.activityIdentification = activityIdentification;
        this.supervisionId = supervisionId;
        this.activeStatusFlag = activeStatusFlag;
        this.plannedStartDate = plannedStartDate;
        this.plannedEndDate = plannedEndDate;
        this.activityCategory = activityCategory;
        this.activityAntecedentId = activityAntecedentId;
        this.activityDescendantIds = activityDescendantIds;
        this.scheduleID = scheduleID;
    }

    /**
     * Get Activity Identification
     * <p>
     * A unique identification number of an activity. Created by the system.
     *
     * @return Long, optional, null for creation; required for update. activityIdentification
     */
    public Long getActivityIdentification() {
        return activityIdentification;
    }

    /**
     * Set Activity Identification
     * <p>
     * A unique identification number of an activity. Created by the system.
     *
     * @param activityIdentification Long, optional, null for creation; required for update. activityIdentification to set
     */
    public void setActivityIdentification(Long activityIdentification) {
        this.activityIdentification = activityIdentification;
    }

    /**
     * Get Supervision Id
     * <p>
     * The offender’s supervision that the activity belongs to. The id of the Supervision service. Note: After it's value is persisted in DB, it can not be updated. The new value
     * will be ignored.
     *
     * @return Long optional. supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * Set Supervision Id
     * <p>
     * The offender’s supervision that the activity belongs to. The id of the Supervision service. Note: After it's value is persisted in DB, it can not be updated. The new value
     * will be ignored.
     *
     * @param supervisionId Long optional. supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Get Active Status Flag
     * <p>
     * Indicate if the activity is active.
     * <li>The Activity Status is interpreted as“Active” if ActiveStatusFlag = True.</li>
     * <li>Activity Status is interpreted as “Inactive” if ActiveStatusFlag = False.</li>
     * <p>
     * If an activity is active, it is taking a spot in the capacity of a timeslot of the activity’s schedule.
     * <p>
     * When activity status is updated, the corresponding activity augmentation status must be updated as well, and vice versa (out of scope of this service).
     *
     * @return Boolean required. activeStatusFlag
     */
    public Boolean getActiveStatusFlag() {
        return activeStatusFlag;
    }

    /**
     * Set Active Status Flag
     * <p>
     * Indicate if the activity is active.
     * <li>The Activity Status is interpreted as“Active” if ActiveStatusFlag = True.</li>
     * <li>Activity Status is interpreted as “Inactive” if ActiveStatusFlag = False.</li>
     * <p>
     * If an activity is active, it is taking a spot in the capacity of a timeslot of the activity’s schedule.
     * <p>
     * When activity status is updated, the corresponding activity augmentation status must be updated as well, and vice versa (out of scope of this service).
     *
     * @param activeStatusFlag Boolean required. activeStatusFlag to set
     */
    public void setActiveStatusFlag(Boolean activeStatusFlag) {
        this.activeStatusFlag = activeStatusFlag;
    }

    /**
     * Get Planned Start Date
     * <p>
     * Planned start date of an activity. The planned start date is either updated by the user for a unscheduled activity, or updated by the system based on a schedule the user
     * selects for the activity.
     *
     * @return Date optional. plannedStartDate
     */
    public Date getPlannedStartDate() {
        return plannedStartDate;
    }

    /**
     * Set Planned Start Date
     * <p>
     * Planned start date of an activity. The planned start date is either updated by the user for a unscheduled activity, or updated by the system based on a schedule the user
     * selects for the activity.
     *
     * @param plannedStartDate Date optional. plannedStartDate to set
     */
    public void setPlannedStartDate(Date plannedStartDate) {
        this.plannedStartDate = plannedStartDate;
    }

    /**
     * Get Planned End Date
     * <p>
     * Planned end date of an activity. The planned end date is either updated by the user for an unscheduled activity, or updated by the system based on a schedule the user
     * selects for the activity. Planned end date is required if planned start date is populated.
     *
     * @return Date optional. plannedEndDate
     */
    public Date getPlannedEndDate() {
        return plannedEndDate;
    }

    /**
     * Set Planned End Date
     * <p>
     * Planned end date of an activity. The planned end date is either updated by the user for an unscheduled activity, or updated by the system based on a schedule the user
     * selects for the activity. Planned end date is required if planned start date is populated.
     *
     * @param plannedEndDate Date optional. plannedEndDate to set
     */
    public void setPlannedEndDate(Date plannedEndDate) {
        this.plannedEndDate = plannedEndDate;
    }

    /**
     * Get Activity Category
     * <p>
     * Activity Category is derived based on the reference to a particular type of activity. For example, if activity has a reference to movement activity, activity category =
     * “Movement”. If activity has a reference to housing bed management activity, activity category = “Housing and Bed Management”.
     *
     * @return String required. activityCategory
     */
    public String getActivityCategory() {
        return activityCategory;
    }

    /**
     * Set Activity Category
     * <p>
     * Activity Category is derived based on the reference to a particular type of activity. For example, if activity has a reference to movement activity, activity category =
     * “Movement”. If activity has a reference to housing bed management activity, activity category = “Housing and Bed Management”.
     *
     * @param activityCategory String required. activityCategory to set
     */
    public void setActivityCategory(String activityCategory) {
        this.activityCategory = activityCategory;
    }

    /**
     * Get Activity Antecedent Id
     * <p>
     * Reference to an activity that is the direct antecedent of the current activity.
     *
     * @return Long optional. activityAntecedentId
     */
    public Long getActivityAntecedentId() {
        return activityAntecedentId;
    }

    /**
     * Set Activity Antecedent Id
     * <p>
     * Reference to an activity that is the direct antecedent of the current activity.
     *
     * @param activityAntecedentId Long optional. activityAntecedentId to set
     */
    public void setActivityAntecedentId(Long activityAntecedentId) {
        this.activityAntecedentId = activityAntecedentId;
    }

    /**
     * Get Activity Descendant Ids
     * <p>
     * Reference to activities that are direct descendants of the current activity.
     *
     * @return Set&lt;Long> optional. activityDescendantIds
     */
    public Set<Long> getActivityDescendantIds() {
        if (activityDescendantIds == null) {
            activityDescendantIds = new HashSet<Long>();
        }
        return activityDescendantIds;
    }

    /**
     * Set Activity Descendant Ids
     * <p>
     * Reference to activities that are direct descendants of the current activity.
     *
     * @param activityDescendantIds Set&lt;Long> optional. activityDescendantIds to set
     */
    public void setActivityDescendantIds(Set<Long> activityDescendantIds) {
        this.activityDescendantIds = activityDescendantIds;
    }

    /**
     * Get Schedule ID
     * <p>
     * The identifier of the schedule. The ScheduleTypeID is stored if the activity is created based on a predefined schedule. The value is blank if the activity is not created
     * based on a predefined schedule.
     *
     * @return Long optional. scheduleID
     */
    public Long getScheduleID() {
        return scheduleID;
    }

    /**
     * Set Schedule ID
     * <p>
     * The identifier of the schedule. The ScheduleTypeID is stored if the activity is created based on a predefined schedule. The value is blank if the activity is not created
     * based on a predefined schedule.
     *
     * @param scheduleID Long optional. the scheduleID to set
     */
    public void setScheduleID(Long scheduleID) {
        this.scheduleID = scheduleID;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((activeStatusFlag == null) ? 0 : activeStatusFlag.hashCode());
        result = prime * result + ((activityAntecedentId == null) ? 0 : activityAntecedentId.hashCode());
        result = prime * result + ((activityCategory == null) ? 0 : activityCategory.hashCode());
        result = prime * result + ((activityDescendantIds == null) ? 0 : activityDescendantIds.hashCode());
        result = prime * result + ((activityIdentification == null) ? 0 : activityIdentification.hashCode());
        result = prime * result + ((plannedEndDate == null) ? 0 : plannedEndDate.hashCode());
        result = prime * result + ((plannedStartDate == null) ? 0 : plannedStartDate.hashCode());
        result = prime * result + ((scheduleID == null) ? 0 : scheduleID.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ActivityType other = (ActivityType) obj;
        if (activeStatusFlag == null) {
            if (other.activeStatusFlag != null) {
                return false;
            }
        } else if (!activeStatusFlag.equals(other.activeStatusFlag)) {
            return false;
        }
        if (activityAntecedentId == null) {
            if (other.activityAntecedentId != null) {
                return false;
            }
        } else if (!activityAntecedentId.equals(other.activityAntecedentId)) {
            return false;
        }
        if (activityCategory == null) {
            if (other.activityCategory != null) {
                return false;
            }
        } else if (!activityCategory.equals(other.activityCategory)) {
            return false;
        }
        if (activityDescendantIds == null) {
            if (other.activityDescendantIds != null) {
                return false;
            }
        } else if (!activityDescendantIds.equals(other.activityDescendantIds)) {
            return false;
        }
        if (activityIdentification == null) {
            if (other.activityIdentification != null) {
                return false;
            }
        } else if (!activityIdentification.equals(other.activityIdentification)) {
            return false;
        }
        if (plannedEndDate == null) {
            if (other.plannedEndDate != null) {
                return false;
            }
        } else if (!plannedEndDate.equals(other.plannedEndDate)) {
            return false;
        }
        if (plannedStartDate == null) {
            if (other.plannedStartDate != null) {
                return false;
            }
        } else if (!plannedStartDate.equals(other.plannedStartDate)) {
            return false;
        }
        if (scheduleID == null) {
            if (other.scheduleID != null) {
                return false;
            }
        } else if (!scheduleID.equals(other.scheduleID)) {
            return false;
        }
        if (supervisionId == null) {
            if (other.supervisionId != null) {
                return false;
            }
        } else if (!supervisionId.equals(other.supervisionId)) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ActivityType [activityIdentification=" + activityIdentification + ", supervisionId=" + supervisionId + ", activeStatusFlag=" + activeStatusFlag
                + ", plannedStartDate=" + plannedStartDate + ", plannedEndDate=" + plannedEndDate + ", activityCategory=" + activityCategory + ", activityAntecedentId="
                + activityAntecedentId + ", activityDescendantIds=" + activityDescendantIds + ", scheduleID=" + scheduleID + "]";
    }

}
