package syscon.arbutus.product.services.activity.realization.persistence;

public class MetaSet {
    public static final String ACTIVITY_CATEGORY = "ActivityCategory";
    public static final String RECURRING_FREQUENCY = "RecurringFrequency";
    public static final String WEEKDAY = "WEEKDAY";
    public static final String MONTH = "MONTH";
    public static final String NTHWEEKDAY = "NTHWEEKDAY";
    public static final String DAY = "DAY";
    public static final String FACILITY_SCHEDULE_REASON = "FacilityScheduleReason";
    public static final String APPOINTMENT_TYPE = "APPOINTMENTTYPE";
}
