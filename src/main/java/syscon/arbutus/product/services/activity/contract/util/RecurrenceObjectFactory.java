package syscon.arbutus.product.services.activity.contract.util;


import java.util.Optional;

import syscon.arbutus.product.services.activity.contract.dto.*;

/**
 * Created by ian on 9/8/15.
 *
 * Factory class to generate AbstractRecurrenceObjects from different sets of parameters
 */
public class RecurrenceObjectFactory {

    /**
     * Returns a concrete subclass of AbstractRecurrenceObject based on parameters given
     * <p>
     * DaysOfTheWeek parameter only gets processed if the RecurrenceEnum is of type CUSTOM
     * </p>
     *
     * @param recurrenceType
     * @param daysOfTheWeek
     * @return
     */
    public AbstractRecurrenceObject generate(RecurrenceEnum recurrenceType, DaysOfTheWeek daysOfTheWeek) {
        AbstractRecurrenceObject returnObj = new DailyRecurrenceObject();
        if (isCustomRecurrenceType(recurrenceType)) {
            returnObj = new CustomRecurrenceObject(daysOfTheWeek);
        } else if (isWeekdaysRecurrenceType(recurrenceType)) {
            returnObj = new WeekdayRecurrenceObject();
        }
        return returnObj;
    }

    /**
     * Returns a concrete subclass of AbstractRecurrenceObject based on the RecurrencePatternType given
     *
     * @param pattern
     * @return
     */
    public AbstractRecurrenceObject generate(RecurrencePatternType pattern) {
        Optional<DaysOfTheWeek> days = new DaysOfTheWeekUtil().generateDaysOfTheWeekFromRecurrencePattern(pattern);
        return generate(new ScheduleRecurrenceUtil().calculateRecurrenceEnum(pattern), days.orElseGet(() -> new DaysOfTheWeek()));
    }

    private Boolean isCustomRecurrenceType(RecurrenceEnum recurrenceType) {
        return (recurrenceType != null && RecurrenceEnum.CUSTOM.equals(recurrenceType));
    }

    private Boolean isWeekdaysRecurrenceType(RecurrenceEnum recurrenceType) {
        return (recurrenceType != null && RecurrenceEnum.WEEKDAYS.equals(recurrenceType));
    }

}
