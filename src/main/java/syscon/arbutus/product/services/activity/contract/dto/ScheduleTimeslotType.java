package syscon.arbutus.product.services.activity.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

/**
 * Definition of a Schedule Timeslot type
 */
@ArbutusConstraint(constraints = { "timeslotStartDateTime <= timeslotEndDateTime" })
public class ScheduleTimeslotType implements Comparable<ScheduleTimeslotType>, Serializable {

    private static final long serialVersionUID = 3875031949979622624L;

    @NotNull
    private Date timeslotStartDateTime;
    private Date timeslotEndDateTime;
    @NotNull
    private Long scheduleID;

    /**
     * Constructor
     */
    public ScheduleTimeslotType() {
        super();
    }

    /**
     * Constructor
     *
     * @param scheduleTimeslot Required.
     */
    public ScheduleTimeslotType(ScheduleTimeslotType scheduleTimeslot) {
        super();
        this.timeslotStartDateTime = scheduleTimeslot.getTimeslotStartDateTime();
        this.timeslotEndDateTime = scheduleTimeslot.getTimeslotEndDateTime();
        this.scheduleID = scheduleTimeslot.getScheduleID();
    }

    /**
     * Constructor
     * <ul>
     * <li>timeslotStartDateTime -- The start date and time of a time slot, e.g March 14, 08:00</li>
     * <li>timeslotEndDateTime -- The end date and time of a time slot, e.g March 14, 09:00</li>
     * <li>scheduleID -- The unique identifier of a schedule that is used to generate the timeslot</li>
     * </ul>
     *
     * @param timeslotStartDateTime Required
     * @param timeslotEndDateTime   Optional
     * @param scheduleID            Required
     */
    public ScheduleTimeslotType(@NotNull Date timeslotStartDateTime, Date timeslotEndDateTime, @NotNull Long scheduleID) {
        super();
        this.timeslotStartDateTime = timeslotStartDateTime;
        this.timeslotEndDateTime = timeslotEndDateTime;
        this.scheduleID = scheduleID;
    }

    /**
     * Get Timeslot Start Date Time
     * <p>The start date and time of a time slot, e.g March 14, 08:00
     *
     * @return Date required. timeslotStartDateTime
     */
    public Date getTimeslotStartDateTime() {
        return timeslotStartDateTime;
    }

    /**
     * Set Timeslot Start Date Time
     * <p>The start date and time of a time slot, e.g March 14, 08:00
     * <p>The start date and time of a time slot must be before end date time
     *
     * @param timeslotStartDateTime Date required. timeslotStartDateTime the timeslotStartDateTime to set
     */
    public void setTimeslotStartDateTime(Date timeslotStartDateTime) {
        this.timeslotStartDateTime = timeslotStartDateTime;
    }

    /**
     * Get Timeslot End Date Time
     * <p>The end date and time of a time slot, e.g March 14, 09:00
     * <p>The end date and time of a time slot must be after start date time
     *
     * @return the timeslotEndDateTime
     */
    public Date getTimeslotEndDateTime() {
        return timeslotEndDateTime;
    }

    /**
     * Set Timeslot End Date Time
     * <p>The end date and time of a time slot, e.g March 14, 09:00
     *
     * @param timeslotEndDateTime the timeslotEndDateTime to set
     */
    public void setTimeslotEndDateTime(Date timeslotEndDateTime) {
        this.timeslotEndDateTime = timeslotEndDateTime;
    }

    /**
     * Get Schedule ID
     *
     * @return Long required. scheduleID
     */
    public Long getScheduleID() {
        return scheduleID;
    }

    /**
     * Set Schedule ID
     *
     * @param scheduleID Long required. scheduleID the scheduleID to set
     */
    public void setScheduleID(Long scheduleID) {
        this.scheduleID = scheduleID;
    }

    @Override
    public int compareTo(ScheduleTimeslotType o) {
        return this.getTimeslotStartDateTime().compareTo(o.getTimeslotStartDateTime());
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((scheduleID == null) ? 0 : scheduleID.hashCode());
        result = prime * result + ((timeslotEndDateTime == null) ? 0 : timeslotEndDateTime.hashCode());
        result = prime * result + ((timeslotStartDateTime == null) ? 0 : timeslotStartDateTime.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (!(obj instanceof ScheduleTimeslotType)) {
			return false;
		}
        ScheduleTimeslotType other = (ScheduleTimeslotType) obj;
        if (scheduleID == null) {
            if (other.scheduleID != null) {
				return false;
			}
        } else if (!scheduleID.equals(other.scheduleID)) {
			return false;
		}
        if (timeslotEndDateTime == null) {
            if (other.timeslotEndDateTime != null) {
				return false;
			}
        } else if (!timeslotEndDateTime.equals(other.timeslotEndDateTime)) {
			return false;
		}
        if (timeslotStartDateTime == null) {
            if (other.timeslotStartDateTime != null) {
				return false;
			}
        } else if (!timeslotStartDateTime.equals(other.timeslotStartDateTime)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ScheduleTimeslotType [timeslotStartDateTime=" + timeslotStartDateTime + ", timeslotEndDateTime=" + timeslotEndDateTime + ", scheduleID=" + scheduleID
                + "]";
    }
}
