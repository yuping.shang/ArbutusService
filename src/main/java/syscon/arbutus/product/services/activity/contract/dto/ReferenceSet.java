package syscon.arbutus.product.services.activity.contract.dto;

/**
 * Reference Set for Activity Service
 *
 * @author yshang
 * @version 1.0
 * @since October 15, 2012
 */
public enum ReferenceSet {
    /**
     * Activity Category
     */
    ActivityCategory,
    /**
     * Recurring Frequency
     */
    RecurringFrequency,
    /**
     * Day -- MON, TUE, WED, THU, FRI, SAT, SUN
     */
    DAY,
    /**
     * Weekday
     */
    WEEKDAY,
    /**
     * Month
     */
    MONTH,
    /**
     * Nth Weekday
     */
    NTHWEEKDAY,
    /**
     * Facility Schedule Reason
     */
    FacilityScheduleReason;

    public static ReferenceSet fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }
}
