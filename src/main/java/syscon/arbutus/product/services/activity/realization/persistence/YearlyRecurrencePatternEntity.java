package syscon.arbutus.product.services.activity.realization.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.realization.model.MetaCode;

/**
 * WeeklyRecurrencePatternEntity for Activity Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment ACT_SchRPatn 'Activity Recurrence Pattern for Activity Service'
 * @since September 18, 2012
 */
@Audited
@Entity
public class YearlyRecurrencePatternEntity extends RecurrencePatternEntity {

    private static final long serialVersionUID = -8500090719504538676L;

    /**
     * @DbComment ACT_SchRPatn.recurOnDay 'Recurrence on day'
     */
    @Column(name = "recurOnDay")
    private Long recurOnDay;

    /**
     * @DbComment ACT_SchRPatn.recurOnMonth 'Recurrence on month'
     */
    @Column(name = "recurOnMonth", length = 64) //, nullable = false)
    @MetaCode(set = MetaSet.MONTH)
    private String recurOnMonth;

    /**
     * @DbComment ACT_SchRPatn.recurOnWeekday 'Recurrence on Weekday'
     */
    @Column(name = "recurOnWeekday", nullable = true, length = 64)
    @MetaCode(set = MetaSet.WEEKDAY)
    private String recurOnWeekday;

    /**
     * @DbComment ACT_SchRPatn.nthWeekday 'Nth weekday'
     */
    @Column(name = "nthWeekday", nullable = true, length = 64)
    @MetaCode(set = MetaSet.NTHWEEKDAY)
    private String nthWeekday;

    public YearlyRecurrencePatternEntity() {
        super();
    }

    public YearlyRecurrencePatternEntity(YearlyRecurrencePatternEntity yearlyRecurrencePattern) {
        super(yearlyRecurrencePattern);
        this.recurOnDay = yearlyRecurrencePattern.getRecurOnDay();
        this.recurOnMonth = yearlyRecurrencePattern.getRecurOnMonth();
        this.recurOnWeekday = yearlyRecurrencePattern.getRecurOnWeekday();
        this.nthWeekday = yearlyRecurrencePattern.getNthWeekday();
    }

    /**
     * @param id
     * @param recurrenceInterval
     */
    public YearlyRecurrencePatternEntity(Long id, Long recurrenceInterval) {
        super(id, recurrenceInterval);
    }

    /**
     * @param recurrencePattern
     */
    public YearlyRecurrencePatternEntity(RecurrencePatternEntity recurrencePattern) {
        super(recurrencePattern);
    }

    public YearlyRecurrencePatternEntity(Long id, Long recurrenceInterval, Long recurOnDay, String recurOnMonth, String recurOnWeekday, String nthWeekday) {
        super(id, recurrenceInterval);
        this.recurOnDay = recurOnDay;
        this.recurOnMonth = recurOnMonth;
        this.recurOnWeekday = recurOnWeekday;
        this.nthWeekday = nthWeekday;
    }

    /**
     * @param recurOnDay
     * @param recurOnMonth
     * @param recurOnWeekday
     * @param nthWeekday
     */
    public YearlyRecurrencePatternEntity(Long recurOnDay, String recurOnMonth, String recurOnWeekday, String nthWeekday) {
        super();
        this.recurOnDay = recurOnDay;
        this.recurOnMonth = recurOnMonth;
        this.recurOnWeekday = recurOnWeekday;
        this.nthWeekday = nthWeekday;
    }

    /**
     * isWellFormed -- To validate the value of the properties of the type.
     *
     * @return Boolean
     */
    public Boolean isWellFormed() {
        if (!super.isWellFormed() || recurOnMonth == null) {
			return Boolean.FALSE;
		}
        return Boolean.TRUE;
    }

    /**
     * @return the recurOnDay
     */
    public Long getRecurOnDay() {
        return recurOnDay;
    }

    /**
     * @param recurOnDay the recurOnDay to set
     */
    public void setRecurOnDay(Long recurOnDay) {
        this.recurOnDay = recurOnDay;
    }

    /**
     * @return the recurOnMonth
     */
    public String getRecurOnMonth() {
        return recurOnMonth;
    }

    /**
     * @param recurOnMonth the recurOnMonth to set
     */
    public void setRecurOnMonth(String recurOnMonth) {
        this.recurOnMonth = recurOnMonth;
    }

    /**
     * @return the recurOnWeekday
     */
    public String getRecurOnWeekday() {
        return recurOnWeekday;
    }

    /**
     * @param recurOnWeekday the recurOnWeekday to set
     */
    public void setRecurOnWeekday(String recurOnWeekday) {
        this.recurOnWeekday = recurOnWeekday;
    }

    /**
     * @return the nthWeekday
     */
    public String getNthWeekday() {
        return nthWeekday;
    }

    /**
     * @param nthWeekday the nthWeekday to set
     */
    public void setNthWeekday(String nthWeekday) {
        this.nthWeekday = nthWeekday;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((nthWeekday == null) ? 0 : nthWeekday.hashCode());
        result = prime * result + ((recurOnDay == null) ? 0 : recurOnDay.hashCode());
        result = prime * result + ((recurOnMonth == null) ? 0 : recurOnMonth.hashCode());
        result = prime * result + ((recurOnWeekday == null) ? 0 : recurOnWeekday.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        YearlyRecurrencePatternEntity other = (YearlyRecurrencePatternEntity) obj;
        if (nthWeekday != other.nthWeekday) {
			return false;
		}
        if (recurOnDay == null) {
            if (other.recurOnDay != null) {
				return false;
			}
        } else if (!recurOnDay.equals(other.recurOnDay)) {
			return false;
		}
        if (recurOnMonth != other.recurOnMonth) {
			return false;
		}
        if (recurOnWeekday != other.recurOnWeekday) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "YearlyRecurrencePatternEntity [recurOnDay=" + recurOnDay + ", recurOnMonth=" + recurOnMonth + ", recurOnWeekday=" + recurOnWeekday + ", nthWeekday="
                + nthWeekday + "]";
    }

}
