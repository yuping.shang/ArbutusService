package syscon.arbutus.product.services.activity.contract.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Definition of the Actvities return type
 *
 * @author yshang
 * @version 1.0 (Based on SDD 1.0)
 * @since April 27, 2012
 */
public class ActivitiesReturnType implements Serializable       {

    private static final long serialVersionUID = 4429182382170571085L;

    private List<ActivityType> activities;
    private Long totalSize;

    /**
     * @return the activities
     */
    public List<ActivityType> getActivities() {
        return activities;
    }

    /**
     * @param activities the activities to set
     */
    public void setActivities(List<ActivityType> activities) {
        this.activities = activities;
    }

    /**
     * @return the totalSize
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * @param totalSize the totalSize to set
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ActivitiesReturnType [activities=" + activities + ", totalSize=" + totalSize + "]";
    }

}
