package syscon.arbutus.product.services.activity.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

/**
 * Definition of a Recurrence Pattern type
 *
 * @author yshang
 * @version 1.0 (Based on SDD 1.0)
 * @since April 27, 2012
 */
public class RecurrencePatternType implements Serializable {

    private static final long serialVersionUID = -847159244098994600L;
    @NotNull
    private Long recurrenceInterval = 1L;

    private Set<ExDateType> includeDateTimes;
    private Set<ExDateType> excludeDateTimes;

    public RecurrencePatternType() {
        super();
    }

    public RecurrencePatternType(RecurrencePatternType recurrencePattern) {
        super();
        this.recurrenceInterval = recurrencePattern.getRecurrenceInterval();
    }

    /**
     * Constructor
     *
     * @param recurrenceInterval Long Required, recurrenceInterval the number of recurrence interval, Default is 1
     */
    public RecurrencePatternType(Long recurrenceInterval) {
        super();
        this.recurrenceInterval = recurrenceInterval;
    }

    /**
     * Gets the value of the recurrenceInterval property.
     * <p>recurrenceInterval the number of recurrence interval, Default is 1
     *
     * @return Long required. recurrenceInterval
     */
    public Long getRecurrenceInterval() {
        return recurrenceInterval;
    }

    /**
     * Sets the value of the recurrenceInterval property.
     * <p>recurrenceInterval the number of recurrence interval, Default is 1
     *
     * @param value Long required. recurrenceInterval to set
     */
    public void setRecurrenceInterval(Long value) {
        this.recurrenceInterval = value;
    }

    /**
     * Date times for particular dates included in the schedule
     *
     * @return Set&lt;Date>
     */
    public Set<ExDateType> getIncludeDateTimes() {
        return includeDateTimes;
    }

    /**
     * Date times for particular dates included in the schedule
     */
    public void setIncludeDateTimes(Set<ExDateType> includeDateTimes) {
        this.includeDateTimes = includeDateTimes;
    }

    /**
     * Date times for particular dates excluded in the schedule
     *
     * @return Set&lt;Date>
     */
    public Set<ExDateType> getExcludeDateTimes() {
        return excludeDateTimes;
    }

    public void setExcludeDateTimes(Set<ExDateType> excludeDateTimes) {
        this.excludeDateTimes = excludeDateTimes;
    }

    /**
     * Date times for particular dates excluded in the schedule
     */


    /*
             * (non-Javadoc)
             *
             * @see java.lang.Object#hashCode()
             */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((recurrenceInterval == null) ? 0 : recurrenceInterval.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        RecurrencePatternType other = (RecurrencePatternType) obj;
        if (recurrenceInterval == null) {
            if (other.recurrenceInterval != null) {
				return false;
			}
        } else if (!recurrenceInterval.equals(other.recurrenceInterval)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "RecurrencePatternType{" +
                "recurrenceInterval=" + recurrenceInterval +
                ", includeDateTimes=" + includeDateTimes +
                ", excludeDateTimes=" + excludeDateTimes +
                '}';
    }
}
