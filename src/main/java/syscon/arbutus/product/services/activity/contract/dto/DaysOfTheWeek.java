package syscon.arbutus.product.services.activity.contract.dto;

import java.io.Serializable;


/**
 * Created by ian.rivers on 08/07/2015.
 */
public class DaysOfTheWeek implements Serializable {

    public boolean monday;
    public boolean tuesday;
    public boolean wednesday;
    public boolean thursday;
    public boolean friday;
    public boolean saturday;
    public boolean sunday;

}
