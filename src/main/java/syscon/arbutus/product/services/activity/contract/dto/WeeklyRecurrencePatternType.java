package syscon.arbutus.product.services.activity.contract.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * * Java class for WeeklyRecurrencePatternType
 *
 * @author yshang
 * @version 1.0 (Based on SDD 1.0)
 * @since April 27, 2012
 */
public class WeeklyRecurrencePatternType extends RecurrencePatternType {

    private static final long serialVersionUID = 9138758370010744373L;

    @NotNull
    @Size(min = 1, message = "Cardinality = [1..*]")
    private Set<String> recurOnWeekday; // ReferenceSet(WEEKDAY)

    /**
     * Constructor
     */
    public WeeklyRecurrencePatternType() {
        super();
    }

    /**
     * Constructor
     *
     * @param recurrenceInterval Required. Reference code of WEEKDAY set. Define the number of weeks before the schedule recurs. E.g RecurringInterval = 3, schedule recurs every 3 weeks. Default value is 1 week.
     */
    public WeeklyRecurrencePatternType(Long recurrenceInterval) {
        super(recurrenceInterval);
    }

    /**
     * Constructor
     *
     * @param recurringFrequency required when invoking this constructor.
     */
    public WeeklyRecurrencePatternType(RecurrencePatternType recurringFrequency) {
        super(recurringFrequency);
    }

    /**
     * Constructor
     *
     * @param recurOnWeekday required. Reference code of WEEKDAY set. Define which weekday(s) the schedule recurs. E.g Monday, Wednesday, Friday.
     */
    public WeeklyRecurrencePatternType(Set<String> recurOnWeekday) {
        super();
        this.recurOnWeekday = recurOnWeekday;
    }

    /**
     * Constructor
     * <ul>
     * <li>recurrenceInterval -- Define the number of weeks before the schedule recurs. E.g RecurringInterval = 3, schedule recurs every 3 weeks. Default value is 1 week.</li>
     * <li>recurOnWeekday -- Reference code of WEEKDAY set. Define which weekday(s) the schedule recurs. E.g Monday, Wednesday, Friday.</li>
     * </ul>
     *
     * @param recurrenceInterval required.
     * @param recurOnWeekday     required. Reference code of WEEKDAY set.
     */
    public WeeklyRecurrencePatternType(Long recurrenceInterval, Set<String> recurOnWeekday) {
        super(recurrenceInterval);
        if (recurOnWeekday != null) {
			this.recurOnWeekday = new HashSet<String>(recurOnWeekday);
		}
    }

    /**
     * Constructor
     *
     * @param weeklyRecurrencePattern required.
     */
    public WeeklyRecurrencePatternType(WeeklyRecurrencePatternType weeklyRecurrencePattern) {
        super(weeklyRecurrencePattern.getRecurrenceInterval());
        if (recurOnWeekday != null) {
			this.recurOnWeekday = new HashSet<String>(weeklyRecurrencePattern.getRecurOnWeekday());
		}
    }

    /**
     * Get RecurOnWeekday
     * <p>Reference code of WEEKDAY set. Define the number of weeks before the schedule recurs. E.g RecurringInterval = 3, schedule recurs every 3 weeks. Default value is 1 week.
     *
     * @return Set&lt;String> required. recurOnWeekday
     */
    public Set<String> getRecurOnWeekday() {
        return recurOnWeekday;
    }

    /**
     * Set RecurOnWeekday
     * <p>Reference code of WEEKDAY set. Define the number of weeks before the schedule recurs. E.g RecurringInterval = 3, schedule recurs every 3 weeks. Default value is 1 week.
     *
     * @param recurOnWeekday Set&lt;String> required. recurOnWeekday the recurOnWeekday to set
     */
    public void setRecurOnWeekday(Set<String> recurOnWeekday) {
        this.recurOnWeekday = recurOnWeekday;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((recurOnWeekday == null) ? 0 : recurOnWeekday.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (!(obj instanceof WeeklyRecurrencePatternType)) {
			return false;
		}
        WeeklyRecurrencePatternType other = (WeeklyRecurrencePatternType) obj;
        if (recurOnWeekday == null) {
            if (other.recurOnWeekday != null) {
				return false;
			}
        } else if (!recurOnWeekday.equals(other.recurOnWeekday)) {
			return false;
		}
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "WeeklyRecurrencePatternType [recurOnWeekday=" + recurOnWeekday + "]";
    }

}
