package syscon.arbutus.product.services.activity.realization.persistence;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.Set;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;

/**
 * DailyRecurrencePatternEntity for Activity Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment ACT_SchRPatn 'Activity Recurrence Pattern for Activity Service'
 * @since September 18, 2012
 */
@Audited
@Entity
public class DailyRecurrencePatternEntity extends RecurrencePatternEntity {

    private static final long serialVersionUID = 7143221650971271125L;

    /**
     * @DbComment ACT_SchRPatn.weekdayOnly 'Weekday Only Flag'
     */
    @Column(name = "weekdayOnly") //, nullable = false)
    private Boolean weekdayOnlyFlag;

    @ForeignKey(name = "Fk_ACT_SchDay", inverseName = "Fk_ACT_SchRPatn4")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "ACT_SCHRPATN_ACT_SCHDAY", joinColumns = { @JoinColumn(name = "actRPatnId") },
            inverseJoinColumns = { @JoinColumn(name = "actSchDayId") })
    @Valid
    private Set<DayEntity> recurrOnDays;

    public DailyRecurrencePatternEntity() {
        super();
    }

    /**
     * @param id
     * @param recurrenceInterval
     * @param weekdayOnlyFlag
     */
    public DailyRecurrencePatternEntity(Long id, Long recurrenceInterval, Boolean weekdayOnlyFlag, Set<DayEntity> recurrOnDays) {
        super(id, recurrenceInterval);
        this.weekdayOnlyFlag = weekdayOnlyFlag;
        this.recurrOnDays = recurrOnDays;
    }

    public DailyRecurrencePatternEntity(DailyRecurrencePatternEntity dailyRecurrencePattern) {
        super(dailyRecurrencePattern.getId(), dailyRecurrencePattern.getRecurrenceInterval());
        this.weekdayOnlyFlag = dailyRecurrencePattern.getWeekdayOnlyFlag();
        this.recurrOnDays = dailyRecurrencePattern.getRecurrOnDays();
    }

    public DailyRecurrencePatternEntity(Long id, Long recurrenceInterval, RecurrencePatternEntity frequency) {
        super(id, recurrenceInterval);
    }

    /**
     * @param recurrencePattern
     */
    public DailyRecurrencePatternEntity(RecurrencePatternEntity recurrencePattern) {
        super(recurrencePattern);
    }

    /**
     * @param weekdayOnlyFlag
     */
    public DailyRecurrencePatternEntity(Boolean weekdayOnlyFlag) {
        super();
        this.weekdayOnlyFlag = weekdayOnlyFlag;
    }

    /**
     * isWellFormed -- To validate the value of the properties of the type.
     */
    public Boolean isWellFormed() {
        if (!super.isWellFormed() || weekdayOnlyFlag == null) {
			return Boolean.FALSE;
		}
        return Boolean.TRUE;
    }

    /**
     * @return the weekdayOnlyFlag
     */
    public Boolean getWeekdayOnlyFlag() {
        return weekdayOnlyFlag;
    }

    /**
     * @param weekdayOnlyFlag the weekdayOnlyFlag to set
     */
    public void setWeekdayOnlyFlag(Boolean weekdayOnlyFlag) {
        this.weekdayOnlyFlag = weekdayOnlyFlag;
    }

    /**
     * @return the recurrOnDays
     */
    public Set<DayEntity> getRecurrOnDays() {
        return recurrOnDays;
    }

    /**
     * @param recurrOnDays the recurrOnDays to set
     */
    public void setRecurrOnDays(Set<DayEntity> recurrOnDays) {
        this.recurrOnDays = recurrOnDays;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((recurrOnDays == null) ? 0 : recurrOnDays.hashCode());
        result = prime * result + ((weekdayOnlyFlag == null) ? 0 : weekdayOnlyFlag.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        DailyRecurrencePatternEntity other = (DailyRecurrencePatternEntity) obj;
        if (recurrOnDays == null) {
            if (other.recurrOnDays != null) {
				return false;
			}
        } else if (!recurrOnDays.equals(other.recurrOnDays)) {
			return false;
		}
        if (weekdayOnlyFlag == null) {
            if (other.weekdayOnlyFlag != null) {
				return false;
			}
        } else if (!weekdayOnlyFlag.equals(other.weekdayOnlyFlag)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DailyRecurrencePatternEntity [weekdayOnlyFlag=" + weekdayOnlyFlag + ", recurrOnDays=" + recurrOnDays + "]";
    }

}
