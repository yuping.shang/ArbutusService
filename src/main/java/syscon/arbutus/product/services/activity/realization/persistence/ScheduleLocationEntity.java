package syscon.arbutus.product.services.activity.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.Associable;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * ScheduleLocationEntity for Activity Service
 *
 * @author yshang
 * @DbComment ACT_SchLocation 'ACT_SchLocation table. Associated to Act_Sch table.'
 */
@Audited
@Entity
@Table(name = "ACT_SchLocation")
public class ScheduleLocationEntity implements Serializable, Associable {

    private static final long serialVersionUID = -3064514447823232647L;

    /**
     * @DbComment ACT_SchLocation.AssociationId 'Primary Id of ACT_SchLocation table'
     */
    @Id
    @Column(name = "AssociationId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_ACT_SchLocation_Id")
    @SequenceGenerator(name = "SEQ_ACT_SchLocation_Id", sequenceName = "SEQ_ACT_SchLocation", allocationSize = 1)
    private Long associationId;

    /**
     * @DbComment ACT_SchLocation.ToClass 'Class to be pointed to'
     */
    @Column(name = "ToClass", nullable = false, length = 64)
    private String toClass;

    /**
     * @DbComment ACT_SchLocation.ToIdentifier 'Id of the toClass'
     */
    @Column(name = "ToIdentifier", nullable = false)
    private Long toIdentifier;

    /**
     * @DbComment ACT_SchLocation.FromIdentifier 'Foreign key point to Act_Sch table'
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FromIdentifier", referencedColumnName = "actSchId", nullable = false)
    private ScheduleEntity schedule;

    /**
     * @DbComment ACT_SchLocation.createUserId 'Create User Id'
     * @DbComment ACT_SchLocation.createDateTime 'Create Date Time'
     * @DbComment ACT_SchLocation.modifyUserId 'Modify User Id'
     * @DbComment ACT_SchLocation.modifyDateTime 'Modify Date Time'
     * @DbComment ACT_SchLocation.invocationContext 'Invocation Context'
     */
    @Embedded
    @NotAudited
    private StampEntity stamp;

    /**
     * @return the associationId
     */
    public Long getAssociationId() {
        return associationId;
    }

    /**
     * @param associationId the associationId to set
     */
    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    /**
     * @return the toClass
     */
    public String getToClass() {
        return toClass;
    }

    /**
     * @param toClass the toClass to set
     */
    public void setToClass(String toClass) {
        this.toClass = toClass;
    }

    /**
     * @return the toIdentifier
     */
    public Long getToIdentifier() {
        return toIdentifier;
    }

    /**
     * @param toIdentifier the toIdentifier to set
     */
    public void setToIdentifier(Long toIdentifier) {
        this.toIdentifier = toIdentifier;
    }

    /**
     * @return the schedule
     */
    public ScheduleEntity getSchedule() {
        return schedule;
    }

    /**
     * @param schedule the schedule to set
     */
    public void setSchedule(ScheduleEntity schedule) {
        this.schedule = schedule;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((associationId == null) ? 0 : associationId.hashCode());
        result = prime * result + ((toClass == null) ? 0 : toClass.hashCode());
        result = prime * result + ((toIdentifier == null) ? 0 : toIdentifier.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ScheduleLocationEntity other = (ScheduleLocationEntity) obj;
        if (associationId == null) {
            if (other.associationId != null) {
				return false;
			}
        } else if (!associationId.equals(other.associationId)) {
			return false;
		}
        if (toClass == null) {
            if (other.toClass != null) {
				return false;
			}
        } else if (!toClass.equals(other.toClass)) {
			return false;
		}
        if (toIdentifier == null) {
            if (other.toIdentifier != null) {
				return false;
			}
        } else if (!toIdentifier.equals(other.toIdentifier)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ScheduleLocationEntity [associationId=" + associationId + ", toClass=" + toClass + ", toIdentifier=" + toIdentifier + ", stamp=" + stamp + "]";
    }

}
