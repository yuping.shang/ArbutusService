package syscon.arbutus.product.services.activity.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * ScheduleConfigVariableEntity for Activity Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment ACT_Config 'Schedule Config Variable table for Activity Service'
 * @since September 18, 2012
 */
@Entity
@Audited
@Table(name = "ACT_Config")
public class ScheduleConfigVariableEntity implements Serializable {

    private static final long serialVersionUID = 1517878947288287319L;

    /**
     * @DbComment ACT_Config.actConfigId 'Schedule Config Variable ID'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_ACT_CONFIG_ID")
    @SequenceGenerator(name = "SEQ_ACT_CONFIG_ID", sequenceName = "SEQ_ACT_CONFIG_ID", allocationSize = 1)
    @Column(name = "actConfigId")
    private Long scheduleConfigIdentification;

    /**
     * @DbComment ACT_Config.category 'Schedule/Activity Category'
     */
    @Column(name = "category", unique = true, nullable = false, length = 64)
    @MetaCode(set = MetaSet.ACTIVITY_CATEGORY)
    private String scheduleCategory;

    /**
     * @DbComment ACT_Config.numOfSlots 'Number of Next Available Time slots. The limit on the number of next available time slots, to which a user can book activity, that can be generated using all schedules defined for a particular type of activity.'
     */
    @Column(name = "numOfSlots", nullable = true)
    private Long numOfNextAvailableTimeslots;

    /**
     * @DbComment ACT_Config.createUserId 'Create User Id'
     * @DbComment ACT_Config.createDateTime 'Create Date Time'
     * @DbComment ACT_Config.modifyUserId 'Modify User Id'
     * @DbComment ACT_Config.modifyDateTime 'Modify Date Time'
     * @DbComment ACT_Config.invocationContext 'Invocation Context'
     */
    @Embedded
    @NotAudited
    private StampEntity stamp;

    public ScheduleConfigVariableEntity() {
        super();
    }

    public ScheduleConfigVariableEntity(ScheduleConfigVariableEntity config) {
        super();
        this.scheduleConfigIdentification = config.getScheduleConfigIdentification();
        this.scheduleCategory = config.getScheduleCategory();
        this.numOfNextAvailableTimeslots = config.getNumOfNextAvailableTimeslots();
    }

    /**
     * @param scheduleConfigIdentification
     * @param scheduleCategory
     * @param numOfNextAvailableTimeslots
     */
    public ScheduleConfigVariableEntity(Long scheduleConfigIdentification, String scheduleCategory, Long numOfNextAvailableTimeslots) {
        super();
        this.scheduleConfigIdentification = scheduleConfigIdentification;
        this.scheduleCategory = scheduleCategory;
        this.numOfNextAvailableTimeslots = numOfNextAvailableTimeslots;
    }

    /**
     * @return the scheduleConfigIdentification
     */
    public Long getScheduleConfigIdentification() {
        return scheduleConfigIdentification;
    }

    /**
     * @param scheduleConfigIdentification the scheduleConfigIdentification to set
     */
    public void setScheduleConfigIdentification(Long scheduleConfigIdentification) {
        this.scheduleConfigIdentification = scheduleConfigIdentification;
    }

    /**
     * @return the scheduleCategory
     */
    public String getScheduleCategory() {
        return scheduleCategory;
    }

    /**
     * @param scheduleCategory the scheduleCategory to set
     */
    public void setScheduleCategory(String scheduleCategory) {
        this.scheduleCategory = scheduleCategory;
    }

    /**
     * @return the numOfNextAvailableTimeslots
     */
    public Long getNumOfNextAvailableTimeslots() {
        return numOfNextAvailableTimeslots;
    }

    /**
     * @param numOfNextAvailableTimeslots the numOfNextAvailableTimeslots to set
     */
    public void setNumOfNextAvailableTimeslots(Long numOfNextAvailableTimeslots) {
        this.numOfNextAvailableTimeslots = numOfNextAvailableTimeslots;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((numOfNextAvailableTimeslots == null) ? 0 : numOfNextAvailableTimeslots.hashCode());
        result = prime * result + ((scheduleCategory == null) ? 0 : scheduleCategory.hashCode());
        result = prime * result + ((scheduleConfigIdentification == null) ? 0 : scheduleConfigIdentification.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ScheduleConfigVariableEntity other = (ScheduleConfigVariableEntity) obj;
        if (numOfNextAvailableTimeslots == null) {
            if (other.numOfNextAvailableTimeslots != null) {
				return false;
			}
        } else if (!numOfNextAvailableTimeslots.equals(other.numOfNextAvailableTimeslots)) {
			return false;
		}
        if (scheduleCategory == null) {
            if (other.scheduleCategory != null) {
				return false;
			}
        } else if (!scheduleCategory.equals(other.scheduleCategory)) {
			return false;
		}
        if (scheduleConfigIdentification == null) {
            if (other.scheduleConfigIdentification != null) {
				return false;
			}
        } else if (!scheduleConfigIdentification.equals(other.scheduleConfigIdentification)) {
			return false;
		}
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ScheduleConfigVariableEntity [scheduleConfigIdentification=" + scheduleConfigIdentification + ", scheduleCategory=" + scheduleCategory
                + ", numOfNextAvailableTimeslots=" + numOfNextAvailableTimeslots + ", stamp=" + stamp + "]";
    }

}
