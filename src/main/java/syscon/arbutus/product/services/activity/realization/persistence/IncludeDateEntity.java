package syscon.arbutus.product.services.activity.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * ACT_INDATE Include Date table for Recurrence Pattern for Activity Service
 *
 * @author YShang
 * @DbComment ACT_INDATE 'Include Date table for Recurrence Pattern for Activity Service'
 * @since June 18, 2014
 */
@Audited
@Entity
@Table(name = "ACT_INDATE")
public class IncludeDateEntity implements Serializable, Comparable<IncludeDateEntity> {

    private static final long serialVersionUID = -3184779613877588267L;

    /**
     * @DbComment ACT_INDATE.excludeDateId 'Id of Include Date table'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_ACT_INDATE_ID")
    @SequenceGenerator(name = "SEQ_ACT_INDATE_ID", sequenceName = "SEQ_ACT_INDATE_ID", allocationSize = 1)
    @Column(name = "includeDateId")
    private Long includeDateId;

    /**
     * @DbComment ACT_INDATE.thedate 'The date to be included'
     */
    @Column(name = "thedate", nullable = false)
    private Date date;

    /**
     * @DbComment ACT_INDATE.startTime 'Start time (time only has meaning)'
     */
    @Column(name = "startTime")
    private Date startTime;

    /**
     * @DbComment ACT_INDATE.endTime 'End time (time only has meaning)'
     */
    @Column(name = "endTime")
    private Date endTime;

    /**
     * @DbComment ACT_INDATE.actRPatnId 'Foreign key to ACT_SchRPatn table'
     */
    @ForeignKey(name = "Fk_ACT_INDATE")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "actRPatnId", nullable = false)
    private RecurrencePatternEntity recurrencePattern;

    /**
     * @DbComment ACT_INDATE.createUserId 'Create User Id'
     * @DbComment ACT_INDATE.createDateTime 'Create Date Time'
     * @DbComment ACT_INDATE.modifyUserId 'Modify User Id'
     * @DbComment ACT_INDATE.modifyDateTime 'Modify Date Time'
     * @DbComment ACT_INDATE.invocationContext 'Invocation Context'
     */
    @Embedded
    @NotAudited
    private StampEntity stamp;

    public IncludeDateEntity() {
    }

    public Long getIncludeDateId() {
        return includeDateId;
    }

    public void setIncludeDateId(Long includeDateId) {
        this.includeDateId = includeDateId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public RecurrencePatternEntity getRecurrencePattern() {
        return recurrencePattern;
    }

    public void setRecurrencePattern(RecurrencePatternEntity recurrencePattern) {
        this.recurrencePattern = recurrencePattern;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public int compareTo(IncludeDateEntity o) {
        return this.date.compareTo(o.date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (!(o instanceof IncludeDateEntity)) {
			return false;
		}

        IncludeDateEntity that = (IncludeDateEntity) o;

        if (date != null ? !date.equals(that.date) : that.date != null) {
			return false;
		}
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) {
			return false;
		}
        if (includeDateId != null ? !includeDateId.equals(that.includeDateId) : that.includeDateId != null) {
			return false;
		}
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = includeDateId != null ? includeDateId.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "IncludeDateEntity{" +
                "includeDateId=" + includeDateId +
                ", date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}

