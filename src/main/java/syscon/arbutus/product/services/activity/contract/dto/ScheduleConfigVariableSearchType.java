package syscon.arbutus.product.services.activity.contract.dto;

import java.io.Serializable;

/**
 * Definition of a Schedule Config Variable Search type
 * <p>At least one of the search criteria must be specified.
 *
 * @author yshang
 */
public class ScheduleConfigVariableSearchType implements Serializable {

    private static final long serialVersionUID = -6724297626448063064L;

    private String scheduleCategory;
    private Long numOfNextAvailableTimeslots;

    /**
     * Constructor
     */
    public ScheduleConfigVariableSearchType() {
        super();
    }

    /**
     * Constructor
     *
     * @param config required.
     */
    public ScheduleConfigVariableSearchType(ScheduleConfigVariableSearchType config) {
        super();
        this.scheduleCategory = config.getScheduleCategory();
        this.numOfNextAvailableTimeslots = config.getNumOfNextAvailableTimeslots();
    }

    /**
     * Constructor
     *
     * @param scheduleCategory            Reference code of ActivityCategory set. The type of schedules for which this set of configuration variable are defined.
     * @param numOfNextAvailableTimeslots The limit on the number of next available timeslots, to which a user can book activity, that can be generated using all schedules defined for a particular type of activity.
     */
    public ScheduleConfigVariableSearchType(String scheduleCategory, Long numOfNextAvailableTimeslots) {
        super();
        this.scheduleCategory = scheduleCategory;
        this.numOfNextAvailableTimeslots = numOfNextAvailableTimeslots;
    }

    /**
     * Reference code of ActivityCategory set. The type of schedules for which this set of configuration variable are defined.
     *
     * @return the scheduleCategory
     */
    public String getScheduleCategory() {
        return scheduleCategory;
    }

    /**
     * Reference code of ActivityCategory set. The type of schedules for which this set of configuration variable are defined.
     *
     * @param scheduleCategory the scheduleCategory to set
     */
    public void setScheduleCategory(String scheduleCategory) {
        this.scheduleCategory = scheduleCategory;
    }

    /**
     * The limit on the number of next available timeslots, to which a user can book activity, that can be generated using all schedules defined for a particular type of activity.
     *
     * @return the numOfNextAvailableTimeslots
     */
    public Long getNumOfNextAvailableTimeslots() {
        return numOfNextAvailableTimeslots;
    }

    /**
     * The limit on the number of next available timeslots, to which a user can book activity, that can be generated using all schedules defined for a particular type of activity.
     *
     * @param numOfNextAvailableTimeslots the numOfNextAvailableTimeslots to set
     */
    public void setNumOfNextAvailableTimeslots(Long numOfNextAvailableTimeslots) {
        this.numOfNextAvailableTimeslots = numOfNextAvailableTimeslots;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ScheduleConfigVariableSearchType [scheduleCategory=" + scheduleCategory + ", numOfNextAvailableTimeslots=" + numOfNextAvailableTimeslots + "]";
    }

}
