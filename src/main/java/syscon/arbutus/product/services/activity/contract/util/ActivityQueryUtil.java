package syscon.arbutus.product.services.activity.contract.util;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Restrictions;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import syscon.arbutus.product.services.activity.contract.dto.*;
import syscon.arbutus.product.services.activity.realization.persistence.ScheduleEntity;
import syscon.arbutus.product.services.core.util.DateUtil;
import syscon.arbutus.product.services.realization.util.BeanHelper;

/**
 * Created by ian on 9/22/15.
 */
public class ActivityQueryUtil {

    /**
     * this method should eventually be moved into activityService and replaced by ACTUALLY performing the query and returning the DTO
     * @param session
     * @param facilityId
     * @param facilityInternalLocationId
     * @param activityCategory
     * @param sessionDate
     * @param startTime
     * @param endTime
     * @param recurrencePattern
     * @return
     */
    public static Criteria getScheduleCriteria(Session session, Long facilityId, Long facilityInternalLocationId, String activityCategory, LocalDate sessionDate,
            LocalTime startTime, LocalTime endTime, RecurrencePatternType recurrencePattern) {
        Set<Long> facilityIds = new HashSet<>();
        facilityIds.add(facilityId);
        Set<Long> facilityInternalLocationIdSet = new HashSet<>();
        if (facilityInternalLocationId != null) {
            facilityInternalLocationIdSet.add(facilityInternalLocationId);
        }

        return getScheduleCriteria(session, facilityIds, facilityInternalLocationIdSet, activityCategory, sessionDate, startTime, endTime, recurrencePattern, false);
    }

    /**
     * this method should eventually be moved into activityService and replaced by ACTUALLY performing the query and returning the DTO
     * @param session
     * @param facilityIds
     * @param facilityInternalLocationIdSet
     * @param activityCategory
     * @param sessionDate
     * @param startTime
     * @param endTime
     * @param recurrencePattern
     * @param isTimeStrictEQ
     * @return
     */
    public static Criteria getScheduleCriteria(Session session, Set<Long> facilityIds, Set<Long> facilityInternalLocationIdSet, String activityCategory,
            LocalDate sessionDate, LocalTime startTime, LocalTime endTime, RecurrencePatternType recurrencePattern, boolean isTimeStrictEQ) {

        Criteria c = session.createCriteria(ScheduleEntity.class);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        c.setCacheable(true);

        if (facilityIds != null && !facilityIds.isEmpty()) {
            Disjunction facilityDisjunction = syscon.arbutus.product.services.core.util.QueryUtil.propertyIn("facilityId", facilityIds);
            c.add(facilityDisjunction);
        }
        if (facilityInternalLocationIdSet != null && !facilityInternalLocationIdSet.isEmpty()) {
            c.createCriteria("atInternalLocIds", "internalLocations");
            c.add(Restrictions.ilike("internalLocations.toClass", "FacilityInternalLocationService"));
            Disjunction filDisjunction = syscon.arbutus.product.services.core.util.QueryUtil.propertyIn("internalLocations.toIdentifier", facilityInternalLocationIdSet);
            c.add(filDisjunction);
        }

        if (!BeanHelper.isEmpty(activityCategory)) {
            c.add(Restrictions.eq("scheduleCategory", activityCategory));
        }

        c.createCriteria("actSchPtn", "pattern");

        if (sessionDate != null) {
            syscon.arbutus.product.services.core.util.QueryUtil.effectiveDateCriteria(c, "effectiveDate", "terminationDate", sessionDate, null);
        }

        if (!isTimeStrictEQ) {
            syscon.arbutus.product.services.core.util.QueryUtil.effectiveDateCriteria(c, "scheduleStartTime", "scheduleEndTime",
                    startTime == null ? null : DateUtil.getZeroEpochTime(startTime), endTime == null ? null : DateUtil.getZeroEpochTime(endTime));
        } else {
            if (startTime != null) {
                syscon.arbutus.product.services.core.util.QueryUtil.effectiveDateCriteriaStrictEqTime(c, "scheduleStartTime", "scheduleEndTime",
                        DateUtil.getZeroEpochTime(startTime), DateUtil.getZeroEpochTime(startTime));
            }
        }

        if (recurrencePattern != null) {
            Long interval = recurrencePattern.getRecurrenceInterval();
            if (interval != null) {
                c.add(Restrictions.eq("pattern.recurrenceInterval", interval));
            }
            if (recurrencePattern instanceof OnceRecurrencePatternType) {
                Date occurrenceDate = ((OnceRecurrencePatternType) recurrencePattern).getOccurrenceDate();
                if (occurrenceDate != null) {
                    c.add(Restrictions.eq("pattern.occurrenceDate", occurrenceDate));
                }
            } else if (recurrencePattern instanceof DailyWeeklyCustomRecurrencePatternType) {
                Set<String> recurOnDays = ((DailyWeeklyCustomRecurrencePatternType) recurrencePattern).getRecurOnDays();
                if (recurrencePattern.getRecurrenceInterval() != 1L) {
                    if (!BeanHelper.isEmpty(recurOnDays)) {
                        c.createCriteria("pattern.recurrOnDays", "recurOnDays");
                        Junction junc = Restrictions.disjunction();
                        for (String day : recurOnDays) {
                            junc.add(Restrictions.eq("recurOnDays.day", day));
                            c.add(junc);
                        }
                    }
                }
                Boolean bWeekdayOnly = ((DailyWeeklyCustomRecurrencePatternType) recurrencePattern).isWeekdayOnlyFlag();
                if (bWeekdayOnly != null && BeanHelper.isEmpty(recurOnDays)) {
                    c.add(Restrictions.eq("pattern.weekdayOnlyFlag", bWeekdayOnly));
                }
            } else if (recurrencePattern instanceof WeeklyRecurrencePatternType) {
                Set<String> weekdays = ((WeeklyRecurrencePatternType) recurrencePattern).getRecurOnWeekday();
                if (weekdays != null) {
                    c.createCriteria("pattern.recurOnWeekdays", "onWeekdays");
                    Junction junc = Restrictions.disjunction();
                    for (String wd : weekdays) {
                        junc.add(Restrictions.eq("onWeekdays.weekday", wd));
                        c.add(junc);
                    }
                }
            } else if (recurrencePattern instanceof MonthlyRecurrencePatternType) {
                Long dayOfMonth = ((MonthlyRecurrencePatternType) recurrencePattern).getDayOfMonth();
                if (dayOfMonth != null) {
                    c.add(Restrictions.eq("pattern.dayOfMonth", dayOfMonth));
                }

                String weekday = ((MonthlyRecurrencePatternType) recurrencePattern).getRecurOnWeekday();
                if (weekday != null) {
                    c.add(Restrictions.eq("pattern.recurOnWeekday", weekday));
                }

                String nthWeekday = ((MonthlyRecurrencePatternType) recurrencePattern).getNthWeekday();
                if (nthWeekday != null) {
                    c.add(Restrictions.eq("pattern.nthWeekday", nthWeekday));
                }
            } else if (recurrencePattern instanceof YearlyRecurrencePatternType) {
                Long recurOnDay = ((YearlyRecurrencePatternType) recurrencePattern).getRecurOnDay();
                if (recurOnDay != null) {
                    c.add(Restrictions.eq("pattern.recurOnDay", recurOnDay));
                }

                String month = ((YearlyRecurrencePatternType) recurrencePattern).getRecurOnMonth();
                if (month != null) {
                    c.add(Restrictions.eq("pattern.recurOnMonth", month));
                }

                String weekday = ((YearlyRecurrencePatternType) recurrencePattern).getRecurOnWeekday();
                if (weekday != null) {
                    c.add(Restrictions.eq("pattern.recurOnWeekday", weekday));
                }

                String nthWeekday = ((YearlyRecurrencePatternType) recurrencePattern).getNthWeekday();
                if (nthWeekday != null) {
                    c.add(Restrictions.eq("pattern.nthWeekday", nthWeekday));
                }
            }
        }

        return c;
    }
}
