package syscon.arbutus.product.services.activity.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

/**
 * ExDateType Exclude/Include Date from Recurrenc Pattern
 *
 * @author YShang
 * @since June 17, 2014
 */
public class ExDateType implements Serializable, Comparable<ExDateType> {

    private static final long serialVersionUID = -3107384722352165484L;

    /**
     * The date to be excluded/included from Recurrenc Pattern
     */
    @NotNull
    private LocalDate date;

    /**
     * Start time (time only has meaning)
     */
    private LocalTime startTime;

    /**
     * End time (time only has meaning)
     */
    private LocalTime endTime;

    public ExDateType() {
    }

    public ExDateType(@NotNull LocalDate date, LocalTime startTime, LocalTime endTime) {
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    /**
     * The date to be excluded/included from Recurrenc Pattern
     *
     * @return LocalDate
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * The date to be excluded/included from Recurrenc Pattern
     *
     * @param date LocalDate
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * Start time (time only has meaning)
     *
     * @return LocalTime
     */
    public LocalTime getStartTime() {
        return startTime;
    }

    /**
     * Start time (time only has meaning)
     *
     * @param startTime LocalTime
     */
    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    /**
     * End time (time only has meaning)
     *
     * @return LocalTime
     */
    public LocalTime getEndTime() {
        return endTime;
    }

    /**
     * End time (time only has meaning)
     *
     * @param endTime LocalTime
     */
    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    @Override
    public int compareTo(ExDateType o) {
        return date.compareTo(o.date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (!(o instanceof ExDateType)) {
			return false;
		}

        ExDateType that = (ExDateType) o;

        if (date != null ? !date.equals(that.date) : that.date != null) {
			return false;
		}
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) {
			return false;
		}
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = date != null ? date.hashCode() : 0;
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ExDateType{" +
                "date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
