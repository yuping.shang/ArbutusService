package syscon.arbutus.product.services.activity.contract.dto;


/**
 * Created by ian on 9/8/15.
 */
public class CustomRecurrenceObject extends AbstractRecurrenceObject {

    private DaysOfTheWeek daysOfTheWeek = new DaysOfTheWeek();

    public CustomRecurrenceObject() {
        this.recurrenceEnum = RecurrenceEnum.CUSTOM;
    }

    public CustomRecurrenceObject(DaysOfTheWeek daysOfTheWeek) {
        this.recurrenceEnum = RecurrenceEnum.CUSTOM;
        this.daysOfTheWeek = daysOfTheWeek;
    }





    public DaysOfTheWeek getDaysOfTheWeek() {
        return daysOfTheWeek;
    }

    public void setDaysOfTheWeek(DaysOfTheWeek daysOfTheWeek) {
        this.daysOfTheWeek = daysOfTheWeek;
    }

}
