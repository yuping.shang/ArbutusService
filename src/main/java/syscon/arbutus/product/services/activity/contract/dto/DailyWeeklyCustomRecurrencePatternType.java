package syscon.arbutus.product.services.activity.contract.dto;

import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * * Class for DailyRecurrencePatternType.
 *
 * @author yshang
 * @version 1.0 (Based on SDD 1.0)
 * @since April 27, 2012
 */
public class DailyWeeklyCustomRecurrencePatternType extends RecurrencePatternType {

    private static final long serialVersionUID = -181702805459179643L;

    @NotNull
    private Boolean weekdayOnlyFlag = Boolean.FALSE;

    private Set<String> recurOnDays;

    /**
     * Constructor
     */
    public DailyWeeklyCustomRecurrencePatternType() {
        super();
    }

    /**
     * Constructor
     *
     * @param recurrencePattern RecurrencePatternType required when invoking this constructor.
     */
    public DailyWeeklyCustomRecurrencePatternType(RecurrencePatternType recurrencePattern) {
        super(recurrencePattern);
    }

    /**
     * Constructor
     *
     * @param dailyRecurrencePattern DailyRecurrencePatternType required when invoking this constructor.
     */
    public DailyWeeklyCustomRecurrencePatternType(DailyWeeklyCustomRecurrencePatternType dailyRecurrencePattern) {
        super();
        this.setRecurrenceInterval(dailyRecurrencePattern.getRecurrenceInterval());
        this.weekdayOnlyFlag = dailyRecurrencePattern.isWeekdayOnlyFlag();
        this.recurOnDays = dailyRecurrencePattern.getRecurOnDays();
    }

    /**
     * Constructor
     * <ul>
     * <li>recurrenceInterval -- Define the number of days before the schedule recurs. E.g RecurringInterval = 3, schedule recurs every 3 days. Default value is 1 day.</li>
     * <li>weekdayOnlyFlag -- Indicate whether the number of days specified for the RecurringInterval includes weekend or not. WeekdayOnlyFlag = True, if RecurringInterval includes weekdays only. Else WeekdayOnlyFlag = False. Default value is False.</li>
     * </ul>
     *
     * @param recurrenceInterval Required
     * @param weekdayOnlyFlag    Required
     */
    public DailyWeeklyCustomRecurrencePatternType(Long recurrenceInterval, Boolean weekdayOnlyFlag) {
        super(recurrenceInterval);
        this.weekdayOnlyFlag = weekdayOnlyFlag;
    }

    /**
     * Constructor
     * <ul>
     * <li>recurrenceInterval -- Define the number of days before the schedule recurs. E.g RecurringInterval = 3, schedule recurs every 3 days. Default value is 1 day.</li>
     * <li>weekdayOnlyFlag -- Indicate whether the number of days specified for the RecurringInterval includes weekend or not. WeekdayOnlyFlag = True, if RecurringInterval includes weekdays only. Else WeekdayOnlyFlag = False. Default value is False.</li>
     * <li>recurOnDays -- Indicate the specific days of the week to recur on. If RecurOnDays is populated then WeekdayOnlyFlag = false and RecurringInterval is ignored.</li>
     * </ul>
     *
     * @param recurrenceInterval Required
     * @param weekdayOnlyFlag    Required
     * @param recurOnDays        Optional
     */
    public DailyWeeklyCustomRecurrencePatternType(Long recurrenceInterval, Boolean weekdayOnlyFlag, Set<String> recurOnDays) {
        super(recurrenceInterval);
        this.weekdayOnlyFlag = weekdayOnlyFlag;
        this.recurOnDays = recurOnDays;
    }

    /**
     * Constructor
     * <ul>
     * <li>recurrenceInterval -- Define the number of days before the schedule recurs. E.g RecurringInterval = 3, schedule recurs every 3 days. Default value is 1 day.</li>
     * </ul>
     *
     * @param recurrenceInterval Long Required. recurrenceInterval
     */
    public DailyWeeklyCustomRecurrencePatternType(Long recurrenceInterval) {
        super(recurrenceInterval);
    }

    /**
     * Constructor
     * <ul>
     * <li>weekdayOnlyFlag -- Indicate whether the number of days specified for the RecurringInterval includes weekend or not. WeekdayOnlyFlag = True, if RecurringInterval includes weekdays only. Else WeekdayOnlyFlag = False. Default value is False.</li>
     * </ul>
     *
     * @param weekdayOnlyFlag Boolean Required. weekdayOnlyFlag
     */
    public DailyWeeklyCustomRecurrencePatternType(Boolean weekdayOnlyFlag) {
        super();
        this.weekdayOnlyFlag = weekdayOnlyFlag;
    }

    /**
     * Gets the value of the weekdayOnlyFlag property.
     * <ul>
     * <li>weekdayOnlyFlag -- Indicate whether the number of days specified for the RecurringInterval includes weekend or not. WeekdayOnlyFlag = True, if RecurringInterval includes weekdays only. Else WeekdayOnlyFlag = False. Default value is False.</li>
     * </ul>
     *
     * @return Boolean required. weekdayOnlyFlag
     */
    public Boolean isWeekdayOnlyFlag() {
        return weekdayOnlyFlag;
    }

    /**
     * Sets the value of the weekdayOnlyFlag property.
     * <ul>
     * <li>weekdayOnlyFlag -- Indicate whether the number of days specified for the RecurringInterval includes weekend or not. WeekdayOnlyFlag = True, if RecurringInterval includes weekdays only. Else WeekdayOnlyFlag = False. Default value is False.</li>
     * </ul>
     *
     * @param value Boolean required. weekdayOnlyFlag
     */
    public void setWeekdayOnlyFlag(Boolean value) {
        this.weekdayOnlyFlag = value;
    }

    /**
     * Get Recur On Day
     * recurOnDays -- Indicate the specific days of the week to recur on. If RecurOnDays is populated then WeekdayOnlyFlag = false and RecurringInterval is ignored.
     *
     * @return the recurOnDays optional.
     */
    public Set<String> getRecurOnDays() {
        return recurOnDays;
    }

    /**
     * Set Recur On Day
     * recurOnDays -- Indicate the specific days of the week to recur on. If RecurOnDays is populated then WeekdayOnlyFlag = false and RecurringInterval is ignored.
     *
     * @param recurOnDays optional. the recurOnDays to set
     */
    public void setRecurOnDays(Set<String> recurOnDays) {
        this.recurOnDays = recurOnDays;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((recurOnDays == null) ? 0 : recurOnDays.hashCode());
        result = prime * result + ((weekdayOnlyFlag == null) ? 0 : weekdayOnlyFlag.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        DailyWeeklyCustomRecurrencePatternType other = (DailyWeeklyCustomRecurrencePatternType) obj;
        if (recurOnDays == null) {
            if (other.recurOnDays != null) {
				return false;
			}
        } else if (!recurOnDays.equals(other.recurOnDays)) {
			return false;
		}
        if (weekdayOnlyFlag == null) {
            if (other.weekdayOnlyFlag != null) {
				return false;
			}
        } else if (!weekdayOnlyFlag.equals(other.weekdayOnlyFlag)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DailyRecurrencePatternType [weekdayOnlyFlag=" + weekdayOnlyFlag + ", recurOnDays=" + recurOnDays + "]";
    }

}
