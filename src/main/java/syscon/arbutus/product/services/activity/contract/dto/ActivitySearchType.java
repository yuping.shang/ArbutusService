package syscon.arbutus.product.services.activity.contract.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

/**
 * Definition of an Activity search type
 * <p>At least one of the search criteria must be specified.
 *
 * @author yshang
 * @version 1.0 (Based on SDD 2.0)
 * @since April 27, 2012
 */
@ArbutusConstraint(constraints = { "fromPlannedStartDate <= toPlannedStartDate", "fromPlannedEndDate <= toPlannedEndDate" })
public class ActivitySearchType implements Serializable {

    private static final long serialVersionUID = -4638082071115404095L;

    private Set<Long> supervisionIds;
    private Date fromPlannedStartDate;
    private Date toPlannedStartDate;
    private Date fromPlannedEndDate;
    private Date toPlannedEndDate;
    private Long activityAntecedentId;
    // private Set<Long> activityDescendantIds; activityDescendantIds are activities, search activities directly please.
    private String activityCategory; // ReferenceSet(ActivityCategory)
    private Long scheduleID;
    private Boolean activeStatusFlag;

    public ActivitySearchType() {
        super();
    }

    public ActivitySearchType(ActivitySearchType activitySearch) {
        super();
        this.supervisionIds = activitySearch.supervisionIds;
        this.fromPlannedStartDate = activitySearch.getFromPlannedStartDate();
        this.toPlannedStartDate = activitySearch.getToPlannedEndDate();
        this.fromPlannedEndDate = activitySearch.getFromPlannedEndDate();
        this.toPlannedEndDate = activitySearch.getToPlannedEndDate();
        this.activityAntecedentId = activitySearch.getActivityAntecedentId();
        // this.activityDescendantIds = activitySearch.getActivityDescendantIds();
        this.activityCategory = activitySearch.getActivityCategory();
        this.scheduleID = activitySearch.getScheduleID();
        this.activeStatusFlag = activitySearch.getActiveStatusFlag();
    }

    /**
     * Constructor
     * <p>At least one of the search criteria must be specified.
     *
     * @param supervisionIds       -- The offender’s supervision that the activity belongs to.
     *                             The id of the Supervision service.
     * @param fromPlannedStartDate -- Planned start date of an activity that the search from.
     *                             The planned start date is either updated by the user for a unscheduled activity,
     *                             or updated by the system based on a schedule the user selects for the activity.
     * @param toPlannedStartDate   -- Planned start date of an activity that the search to.
     *                             The planned start date is either updated by the user for a unscheduled activity,
     *                             or updated by the system based on a schedule the user selects for the activity.
     * @param fromPlannedEndDate   -- Planned end date of an activity that the search from.
     *                             The planned end date is either updated by the user for an unscheduled activity,
     *                             or updated by the system based on a schedule the user selects for the activity.
     *                             Planned end date is required if planned start date is populated.
     * @param toPlannedEndDate     -- Planned end date of an activity that the search to.
     *                             The planned end date is either updated by the user for an unscheduled activity,
     *                             or updated by the system based on a schedule the user selects for the activity.
     *                             Planned end date is required if planned start date is populated.
     * @param activityAntecedentId -- Reference to an activity that is the direct antecedent of the current activity.
     * @param activityCategory     -- Activity Category is derived based on the reference to a particular type of activity. For example, if activity has a reference to movement activity, activity category = “Movement”. If activity has a reference to housing bed management activity, activity category = “Housing and Bed Management”.
     * @param scheduleID           -- The identifier of the schedule. The ScheduleTypeID is stored if the activity is created based on a predefined schedule. The value is blank if the activity is not created based on a predefined schedule.
     * @param activeStatusFlag     -- Indicate if the activity is active.
     *                             <ul>The Activity Status is interpreted as“Active” if ActiveStatusFlag = True.</ul>
     *                             <ul>Activity Status is interpreted as “Inactive” if ActiveStatusFlag = False.</ul>
     *                             If an activity is active, it is taking a spot in the capacity of a timeslot of the activity’s schedule.
     */
    public ActivitySearchType(Set<Long> supervisionIds, Date fromPlannedStartDate, Date toPlannedStartDate, Date fromPlannedEndDate, Date toPlannedEndDate,
            Long activityAntecedentId,
            // Set<Long> activityDescendantIds,
            String activityCategory, Long scheduleID, Boolean activeStatusFlag) {
        super();
        this.supervisionIds = supervisionIds;
        this.fromPlannedStartDate = fromPlannedStartDate;
        this.toPlannedStartDate = toPlannedStartDate;
        this.fromPlannedEndDate = fromPlannedEndDate;
        this.toPlannedEndDate = toPlannedEndDate;
        this.activityAntecedentId = activityAntecedentId;
        // this.activityDescendantIds = activityDescendantIds;
        this.activityCategory = activityCategory;
        this.scheduleID = scheduleID;
        this.activeStatusFlag = activeStatusFlag;
    }

    /**
     * Get Supervision Ids
     * <p>The offender’s supervision that the activity belongs to.
     * The id of the Supervision service.
     *
     * @return the supervisionIds
     */
    public Set<Long> getSupervisionIds() {
        return supervisionIds;
    }

    /**
     * Set Supervision Ids
     * <p>The offender’s supervision that the activity belongs to.
     * The id of the Supervision service.
     *
     * @param supervisionIds the supervisionIds to set
     */
    public void setSupervisionIds(Set<Long> supervisionIds) {
        this.supervisionIds = supervisionIds;
    }

    /**
     * Get Planned Start Date that the search from
     * <p>Planned start date of an activity that the search from.
     * The planned start date is either updated by the user for a unscheduled activity,
     * or updated by the system based on a schedule the user selects for the activity.
     *
     * @return the fromPlannedStartDate
     */
    public Date getFromPlannedStartDate() {
        return fromPlannedStartDate;
    }

    /**
     * Set Planned Start Date that the search from
     * <p>Planned start date of an activity from.
     * The planned start date is either updated by the user for a unscheduled activity,
     * or updated by the system based on a schedule the user selects for the activity.
     *
     * @param fromPlannedStartDate the fromPlannedStartDate to set
     */
    public void setFromPlannedStartDate(Date fromPlannedStartDate) {
        this.fromPlannedStartDate = fromPlannedStartDate;
    }

    /**
     * Get ToPlannedStartDate
     * <p>Planned start date of an activity that the search to.
     * The planned start date is either updated by the user for a unscheduled activity,
     * or updated by the system based on a schedule the user selects for the activity.
     *
     * @return the toPlannedStartDate
     */
    public Date getToPlannedStartDate() {
        return toPlannedStartDate;
    }

    /**
     * Set ToPlannedStartDate
     * <p>Planned start date of an activity that the search to.
     * The planned start date is either updated by the user for a unscheduled activity,
     * or updated by the system based on a schedule the user selects for the activity.
     *
     * @param toPlannedStartDate the toPlannedStartDate to set
     */
    public void setToPlannedStartDate(Date toPlannedStartDate) {
        this.toPlannedStartDate = toPlannedStartDate;
    }

    /**
     * Get FromPlannedEndDate
     * <p>Planned end date of an activity that the search from.
     * The planned end date is either updated by the user for an unscheduled activity,
     * or updated by the system based on a schedule the user selects for the activity.
     * Planned end date is required if planned start date is populated.
     *
     * @return the fromPlannedEndDate
     */
    public Date getFromPlannedEndDate() {
        return fromPlannedEndDate;
    }

    /**
     * Set FromPlannedEndDate
     * <p>Planned end date of an activity that the search from.
     * The planned end date is either updated by the user for an unscheduled activity,
     * or updated by the system based on a schedule the user selects for the activity.
     * Planned end date is required if planned start date is populated.
     *
     * @param fromPlannedEndDate the fromPlannedEndDate to set
     */
    public void setFromPlannedEndDate(Date fromPlannedEndDate) {
        this.fromPlannedEndDate = fromPlannedEndDate;
    }

    /**
     * Get ToPlannedEndDate
     * <p>Planned end date of an activity that the search to.
     * The planned end date is either updated by the user for an unscheduled activity,
     * or updated by the system based on a schedule the user selects for the activity.
     * Planned end date is required if planned start date is populated.
     *
     * @return the toPlannedEndDate
     */
    public Date getToPlannedEndDate() {
        return toPlannedEndDate;
    }

    /**
     * Set ToPlannedEndDate
     * <p>Planned end date of an activity that the search to.
     * The planned end date is either updated by the user for an unscheduled activity,
     * or updated by the system based on a schedule the user selects for the activity.
     * Planned end date is required if planned start date is populated.
     *
     * @param toPlannedEndDate the toPlannedEndDate to set
     */
    public void setToPlannedEndDate(Date toPlannedEndDate) {
        this.toPlannedEndDate = toPlannedEndDate;
    }

    /**
     * Get ActivityAntecedentId
     * <p>Reference to an activity that is the direct antecedent of the current activity.
     *
     * @return the activityAntecedentId
     */
    public Long getActivityAntecedentId() {
        return activityAntecedentId;
    }

    /**
     * Set ActivityAntecedentId
     * <p>Reference to an activity that is the direct antecedent of the current activity.
     *
     * @param activityAntecedentId the activityAntecedentId to set
     */
    public void setActivityAntecedentId(Long activityAntecedentId) {
        this.activityAntecedentId = activityAntecedentId;
    }

    //	/**
    //	 * Get ActivityDescendantIds
    //	 *
    //	 * <p>Reference to activities that are direct descendants of the current activity.
    //	 *
    //	 * @return the activityDescendantIds
    //	 */
    //	public Set<Long> getActivityDescendantIds() {
    //		return activityDescendantIds;
    //	}
    //
    //	/**
    //	 * Set ActivityDescendantIds
    //	 *
    //	 * <p>Reference to activities that are direct descendants of the current activity.
    //	 *
    //	 * @param activityDescendantIds the activityDescendantIds to set
    //	 */
    //	public void setActivityDescendantIds(Set<Long> activityDescendantIds) {
    //		this.activityDescendantIds = activityDescendantIds;
    //	}

    /**
     * Get Activity Category
     * <p>Activity Category is derived based on the reference to a particular type of activity.
     * For example, if activity has a reference to movement activity, activity category = “Movement”.
     * If activity has a reference to housing bed management activity, activity category = “Housing and Bed Management”.
     *
     * @return the activityCategory
     */
    public String getActivityCategory() {
        return activityCategory;
    }

    /**
     * Set Activity Category
     * <p>Activity Category is derived based on the reference to a particular type of activity.
     * For example, if activity has a reference to movement activity, activity category = “Movement”.
     * If activity has a reference to housing bed management activity, activity category = “Housing and Bed Management”.
     *
     * @param activityCategory the activityCategory to set
     */
    public void setActivityCategory(String activityCategory) {
        this.activityCategory = activityCategory;
    }

    /**
     * Get Schedule ID
     * <p>The identifier of the schedule.
     * The ScheduleTypeID is stored if the activity is created based on a predefined schedule.
     * The value is blank if the activity is not created based on a predefined schedule.
     *
     * @return the scheduleID
     */
    public Long getScheduleID() {
        return scheduleID;
    }

    /**
     * Set Schedule ID
     * <p>The identifier of the schedule.
     * The ScheduleTypeID is stored if the activity is created based on a predefined schedule.
     * The value is blank if the activity is not created based on a predefined schedule.
     *
     * @param scheduleID the scheduleID to set
     */
    public void setScheduleID(Long scheduleID) {
        this.scheduleID = scheduleID;
    }

    /**
     * Get ActiveStatusFlag
     * <p>Indicate if the activity is active.
     * <ul>The Activity Status is interpreted as“Active” if ActiveStatusFlag = True.</ul>
     * <ul>Activity Status is interpreted as “Inactive” if ActiveStatusFlag = False.</ul>
     * If an activity is active, it is taking a spot in the capacity of a timeslot of the activity’s schedule.
     *
     * @return the activeStatusFlag
     */
    public Boolean getActiveStatusFlag() {
        return activeStatusFlag;
    }

    /**
     * Set ActiveStatusFlag
     * <p>Indicate if the activity is active.
     * <ul>The Activity Status is interpreted as“Active” if ActiveStatusFlag = True.</ul>
     * <ul>Activity Status is interpreted as “Inactive” if ActiveStatusFlag = False.</ul>
     * If an activity is active, it is taking a spot in the capacity of a timeslot of the activity’s schedule.
     *
     * @param activeStatusFlag the activeStatusFlag to set
     */
    public void setActiveStatusFlag(Boolean activeStatusFlag) {
        this.activeStatusFlag = activeStatusFlag;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ActivitySearchType [supervisionIds=" + supervisionIds + ", fromPlannedStartDate=" + fromPlannedStartDate + ", toPlannedStartDate=" + toPlannedStartDate
                + ", fromPlannedEndDate=" + fromPlannedEndDate + ", toPlannedEndDate=" + toPlannedEndDate + ", activityAntecedentId=" + activityAntecedentId
                // + ", activityDescendantIds=" + activityDescendantIds
                + ", activityCategory=" + activityCategory + ", scheduleID=" + scheduleID + ", activeStatusFlag=" + activeStatusFlag + "]";
    }

}
