package syscon.arbutus.product.services.image.contract.dto;

/**
 * Image format enum
 *
 * @author os
 * @version 1.0
 * @since July 28, 2014
 */
public enum ImageFormat {
    PNG,
    JPG,
    JPEG,
    GIF,
    BMP,
    WBMP
}
