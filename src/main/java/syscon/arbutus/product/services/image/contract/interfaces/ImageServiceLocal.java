package syscon.arbutus.product.services.image.contract.interfaces;

import javax.ejb.Local;
import java.util.Collection;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.image.contract.dto.Image;

/**
 * Image service local interface
 *
 * @author os
 * @version 1.0
 * @since July 28, 2014
 */
@Local
public interface ImageServiceLocal extends ImageServiceCommon{
}
