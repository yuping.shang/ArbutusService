package syscon.arbutus.product.services.image.realization.persistence;

import javax.persistence.*;
import java.util.Objects;

import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.core.services.realization.BaseEntity;
import syscon.arbutus.product.services.image.contract.dto.ImageFormat;

/**
 * Entity to store image metadata
 *
 * @author os
 * @version 1.0
 * @since July 28, 2014
 */
@Audited
@Entity
@Table(name = "IMG_IMAGE")
public class ImageEntity extends BaseEntity {

    @Id
    @Column(name = "image_id")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_IMG_IMAGE_ID")
    @SequenceGenerator(name = "SEQ_IMG_IMAGE_ID", sequenceName = "SEQ_IMG_IMAGE_ID", allocationSize = 1)
    private Long id;

    @Column(name = "original_image_width", nullable = false)
    private Integer originalImageWidth;

    @Column(name = "original_image_height", nullable = false)
    private Integer originalImageHeight;

    @Enumerated(EnumType.STRING)
    @Column(name = "original_image_format", nullable = false)
    private ImageFormat originalImageFormat;

    @Column(name = "thumbnail_image_width", nullable = false)
    private Integer thumbnailImageWidth;

    @Column(name = "thumbnail_image_height", nullable = false)
    private Integer thumbnailImageHeight;

    @Enumerated(EnumType.STRING)
    @Column(name = "thumbnail_image_format", nullable = false)
    private ImageFormat thumbnailImageFormat;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @PrimaryKeyJoinColumn
    private OriginalImageEntity originalImageEntity;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @PrimaryKeyJoinColumn
    private ThumbnailImageEntity thumbnailImageEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getOriginalImageWidth() {
        return originalImageWidth;
    }

    public void setOriginalImageWidth(Integer originalImageWidth) {
        this.originalImageWidth = originalImageWidth;
    }

    public Integer getOriginalImageHeight() {
        return originalImageHeight;
    }

    public void setOriginalImageHeight(Integer originalImageHeight) {
        this.originalImageHeight = originalImageHeight;
    }

    public ImageFormat getOriginalImageFormat() {
        return originalImageFormat;
    }

    public void setOriginalImageFormat(ImageFormat originalImageFormat) {
        this.originalImageFormat = originalImageFormat;
    }

    public Integer getThumbnailImageWidth() {
        return thumbnailImageWidth;
    }

    public void setThumbnailImageWidth(Integer thumbnailImageWidth) {
        this.thumbnailImageWidth = thumbnailImageWidth;
    }

    public Integer getThumbnailImageHeight() {
        return thumbnailImageHeight;
    }

    public void setThumbnailImageHeight(Integer thumbnailImageHeight) {
        this.thumbnailImageHeight = thumbnailImageHeight;
    }

    public ImageFormat getThumbnailImageFormat() {
        return thumbnailImageFormat;
    }

    public void setThumbnailImageFormat(ImageFormat thumbnailImageFormat) {
        this.thumbnailImageFormat = thumbnailImageFormat;
    }

    public OriginalImageEntity getOriginalImageEntity() {
        return originalImageEntity;
    }

    public void setOriginalImageEntity(OriginalImageEntity originalImageEntity) {
        this.originalImageEntity = originalImageEntity;
    }

    public ThumbnailImageEntity getThumbnailImageEntity() {
        return thumbnailImageEntity;
    }

    public void setThumbnailImageEntity(ThumbnailImageEntity thumbnailImageEntity) {
        this.thumbnailImageEntity = thumbnailImageEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ImageEntity that = (ImageEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(originalImageWidth, that.originalImageWidth) &&
                Objects.equals(originalImageHeight, that.originalImageHeight) &&
                Objects.equals(originalImageFormat, that.originalImageFormat) &&
                Objects.equals(thumbnailImageWidth, that.thumbnailImageWidth) &&
                Objects.equals(thumbnailImageHeight, that.thumbnailImageHeight) &&
                Objects.equals(thumbnailImageFormat, that.thumbnailImageFormat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, originalImageWidth, originalImageHeight, originalImageFormat, thumbnailImageWidth, thumbnailImageHeight, thumbnailImageFormat);
    }
}
