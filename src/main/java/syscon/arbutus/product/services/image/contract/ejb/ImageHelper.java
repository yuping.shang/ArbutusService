package syscon.arbutus.product.services.image.contract.ejb;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.ErrorCodes;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.image.contract.dto.Image;
import syscon.arbutus.product.services.image.realization.persistence.ImageEntity;
import syscon.arbutus.product.services.image.realization.persistence.OriginalImageEntity;
import syscon.arbutus.product.services.image.realization.persistence.ThumbnailImageEntity;
import syscon.arbutus.product.services.image.realization.util.ImageUtil;

/**
 * ImageHelper for converting entity<->dto
 *
 * @author os
 * @version 1.0
 * @since July 28, 2014
 */
public class ImageHelper {

    private transient static final Logger LOG = LoggerFactory.getLogger(ImageHelper.class);

    public static Image getOriginal(ImageEntity imageEntity) {
        return new Image(imageEntity.getId(), imageEntity.getOriginalImageWidth(), imageEntity.getOriginalImageHeight(), imageEntity.getOriginalImageFormat(),
                imageEntity.getOriginalImageEntity().getPhoto(), imageEntity.getStamp(), imageEntity.getVersion());
    }

    public static Set<Image> getOriginals(List<ImageEntity> imageEntities) {

        if (imageEntities == null) {
            throw new InvalidInputException("imageEntities is null");
        }

        Set<Image> images = new HashSet<>(imageEntities.size());
        for (ImageEntity imageEntity : imageEntities) {
            images.add(getOriginal(imageEntity));
        }

        return images;
    }

    public static Image getThumbnail(ImageEntity imageEntity) {
        return new Image(imageEntity.getId(), imageEntity.getThumbnailImageWidth(), imageEntity.getThumbnailImageHeight(), imageEntity.getThumbnailImageFormat(),
                imageEntity.getThumbnailImageEntity().getPhoto(), imageEntity.getStamp(), imageEntity.getVersion());
    }

    public static Set<Image> getThumbnails(List<ImageEntity> imageEntities) {

        if (imageEntities == null) {
            throw new InvalidInputException("imageEntities is null");
        }

        Set<Image> images = new HashSet<>(imageEntities.size());
        for (ImageEntity imageEntity : imageEntities) {
            images.add(getThumbnail(imageEntity));
        }

        return images;
    }

    public static ImageEntity toImageEntity(final Image input) {
        final Image image;
        try {
            image = ImageUtil.getImage(input);
        } catch (final Exception e) {
            LOG.error(e.getMessage(), e);
            throw new ArbutusRuntimeException(ErrorCodes.IMG_ERROR_CREATE_IMAGE);
        }
        final ImageEntity imageEntity = new ImageEntity();
        imageEntity.setId(image.getId());
        imageEntity.setOriginalImageWidth(image.getWidth());
        imageEntity.setOriginalImageHeight(image.getHeight());
        imageEntity.setOriginalImageFormat(image.getImageFormat());
        imageEntity.setOriginalImageEntity(new OriginalImageEntity(image.getId(), image.getPhoto()));
        imageEntity.setVersion(image.getVersion());
        imageEntity.setStamp(image.getStamp());

        final Image thumbnail;
        try {
            thumbnail = ImageUtil.resizeImage(image);
        } catch (final Exception e) {
            LOG.error(e.getMessage(), e);
            throw new ArbutusRuntimeException(ErrorCodes.IMG_ERROR_CREATE_THUMBNAIL);
        }

        imageEntity.setThumbnailImageWidth(thumbnail.getWidth());
        imageEntity.setThumbnailImageHeight(thumbnail.getHeight());
        imageEntity.setThumbnailImageFormat(thumbnail.getImageFormat());
        imageEntity.setThumbnailImageEntity(new ThumbnailImageEntity(thumbnail.getId(), thumbnail.getPhoto()));

        return imageEntity;
    }
}
