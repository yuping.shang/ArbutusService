package syscon.arbutus.product.services.image.realization.persistence;

import javax.persistence.*;
import java.util.Objects;

import org.hibernate.envers.Audited;

/**
 * Entity to store binary data for thumbnail image
 *
 * @author os
 * @version 1.0
 * @since July 28, 2014
 */
@Audited
@Entity
@Table(name = "IMG_THUMBNAIL_DATA")
public class ThumbnailImageEntity {
    @Id
    @Column(name = "image_ID")
    private Long id;

    /**
     * @DbComment photo 'The image data in bytes'
     */
    @Lob
    @Column(name = "photo", nullable = false)
    private byte[] photo;

    public ThumbnailImageEntity() {
    }

    public ThumbnailImageEntity(Long id, byte[] photo) {
        this.id = id;
        this.photo = photo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ThumbnailImageEntity image = (ThumbnailImageEntity) o;
        return Objects.equals(id, image.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
