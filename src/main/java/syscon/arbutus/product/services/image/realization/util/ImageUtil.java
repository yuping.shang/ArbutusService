package syscon.arbutus.product.services.image.realization.util;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.services.image.contract.dto.Image;
import syscon.arbutus.product.services.image.contract.dto.ImageFormat;

public class ImageUtil {

    private transient static Logger log = LoggerFactory.getLogger(ImageUtil.class);

    public static final int THUMBNAIL_WIDTH = 51;
    public static final int THUMBNAIL_HEIGHT = 68;

    public static Image resizeImage(Image originalImage) throws IOException {
        return resizeImage(originalImage, THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);
    }

    public static Image getImage(final Image input) throws IOException {
        InputStream in = null;
        try {
            if (input.getWidth() == null || input.getHeight() == null) {
                in = new ByteArrayInputStream(input.getPhoto());
                final ImageReader imageReader = getImageReader(input.getPhoto());
                ImageInputStream iis = ImageIO.createImageInputStream(in);
                imageReader.setInput(iis, true);
                ImageReadParam param = imageReader.getDefaultReadParam();
                BufferedImage bufferedImage = imageReader.read(0, param);
                return new Image(input.getId(), bufferedImage.getWidth(), bufferedImage.getHeight(), getImageFormat(imageReader), input.getPhoto(), input.getStamp(),
                        input.getVersion());
            } else if (input.getImageFormat() == null) {
                final ImageReader imageReader = getImageReader(input.getPhoto());
                return new Image(input.getId(), input.getWidth(), input.getHeight(), getImageFormat(imageReader), input.getPhoto(), input.getStamp(), input.getVersion());
            }
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.warn(e.getMessage(), e);
                }
            }
        }
        return input;
    }

    public static ImageReader getImageReader(final byte[] originalImage) throws IOException {
        ImageInputStream imageInputStream = null;
        try {
            imageInputStream = ImageIO.createImageInputStream(new ByteArrayInputStream(originalImage));
            Iterator<ImageReader> iter = ImageIO.getImageReaders(imageInputStream);
            if (!iter.hasNext()) {
                throw new RuntimeException("Image format not found!");
            }
            return iter.next();
        } finally {
            if (imageInputStream != null) {
                try {
                    imageInputStream.close();
                } catch (Exception exp) {
                    log.warn(exp.getMessage(), exp);
                }
            }
        }
    }

    public static ImageFormat getImageFormat(final ImageReader imageReader) throws IOException {
        switch (imageReader.getFormatName().toUpperCase()) {
            case "JPG":
                return ImageFormat.JPG;
            case "JPEG":
                return ImageFormat.JPEG;
            case "PNG":
                return ImageFormat.PNG;
            case "GIF":
                return ImageFormat.GIF;
            case "BMP":
                return ImageFormat.BMP;
            case "WBMP":
                return ImageFormat.WBMP;
        }

        throw new RuntimeException("Image format not found!");
    }

    /*public static byte[] convert(byte[] originalImage, ImageFormat from, ImageFormat to) throws IOException {
        InputStream in = null;
        ByteArrayOutputStream baos = null;

        try {
            in = new ByteArrayInputStream(originalImage);
            Iterator readers = ImageIO.getImageReadersByFormatName(from.name());
            ImageReader reader = (ImageReader) readers.next();
            ImageInputStream iis = ImageIO.createImageInputStream(in);
            reader.setInput(iis, true);

            ImageReadParam param = reader.getDefaultReadParam();
            BufferedImage image = reader.read(0, param);
            baos = new ByteArrayOutputStream();
            ImageIO.write(image, to.name(), baos);
            baos.flush();
            return baos.toByteArray();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.warn(e.getMessage(), e);
                }
            }
            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException e) {
                    log.warn(e.getMessage(), e);
                }
            }
        }
    }*/

    public static Image resizeImage(final Image originalImage, final int width, final int height) throws IOException {

        int thumbWidth = width;
        int thumbHeight = height;

        byte[] resizedImage;

        InputStream in = null;
        ByteArrayOutputStream out = null;
        try {
            in = new ByteArrayInputStream(originalImage.getPhoto());
            Iterator readers = ImageIO.getImageReadersByFormatName(originalImage.getImageFormat().name());
            ImageReader reader = (ImageReader) readers.next();

            ImageInputStream iis = ImageIO.createImageInputStream(in);
            reader.setInput(iis, true);

            ImageReadParam param = reader.getDefaultReadParam();
            BufferedImage image = reader.read(0, param);
            if (image != null) {
                double thumbRatio = (double) thumbWidth / (double) thumbHeight;
                int imageWidth = image.getWidth(null);
                int imageHeight = image.getHeight(null);
                double imageRatio = (double) imageWidth / (double) imageHeight;
                if (thumbRatio < imageRatio) {
                    thumbHeight = (int) (thumbWidth / imageRatio);
                } else {
                    thumbWidth = (int) (thumbHeight * imageRatio);
                }

                // draw original image to thumbnail image object and
                // scale it to the new size on-the-fly
                BufferedImage thumbImage = new BufferedImage(thumbWidth, thumbHeight, BufferedImage.TYPE_INT_RGB);
                Graphics2D graphics2D = thumbImage.createGraphics();
                graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                graphics2D.drawImage(image, 0, 0, thumbWidth, thumbHeight, null);

                // save thumbnail image to outFilename
                out = new ByteArrayOutputStream();

                ImageIO.write(thumbImage, originalImage.getImageFormat().name(), out);
                resizedImage = out.toByteArray();
            } else {
                resizedImage = originalImage.getPhoto();
            }
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.warn(e.getMessage(), e);
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    log.warn(e.getMessage(), e);
                }
            }
        }

        return new Image(originalImage.getId(), thumbWidth, thumbHeight, originalImage.getImageFormat(), resizedImage, originalImage.getStamp(),
                originalImage.getVersion());
    }
}
