package syscon.arbutus.product.services.image.contract.interfaces;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.image.contract.dto.Image;

/**
 * Image service interface
 *
 * @author os
 * @version 1.0
 * @since July 28, 2014
 */
public interface ImageServiceCommon extends Serializable {

    /**
     * @param uc    Caller's user context. - Required
     * @param image Image Instance - Required
     * @return Identifier of created image
     */
    Long create(final UserContext uc, final Image image);

    /**
     * @param uc    Caller's user context. - Required
     * @param images Image Instances - Required
     * @return Identifier of created image
     */
    Set<Long> create(final UserContext uc, final Collection<Image> images);

    /**
     * @param uc    Caller's user context. - Required
     * @param image Image Instance - Required
     */
    void update(final UserContext uc, final Image image);

    /**
     * @param uc    Caller's user context. - Required
     * @param images Image Instances - Required
     */
    void update(final UserContext uc, Collection<Image> images);

    /**
     * @param uc      Caller's user context. - Required
     * @param id      Image Instance Identifier - Required
     */
    void delete(final UserContext uc, final Long id);

    /**
     * @param uc      Caller's user context. - Required
     * @param id      Image Instance Identifier - Required
     * @param version version
     */
    void delete(final UserContext uc, final Long id, final Long version);

    /**
     * @param uc Caller's user context. - Required
     * @param id Image Instance Identifier - Required
     * @return Original image
     */
    Image getOriginal(final UserContext uc, final Long id);

    /**
     * @param uc Caller's user context. - Required
     * @param id Image Instance Identifier - Required
     * @return Thumbnail image
     */
    Image getThumbnail(final UserContext uc, final Long id);

    /**
     * @param uc Caller's user context. - Required
     * @param imageIds Image Instance Identifiers - Required
     * @return Original images
     */
    Set<Image> getOriginals(final UserContext uc, final Collection<Long> imageIds);

    /**
     * @param uc Caller's user context. - Required
     * @param imageIds Image Instance Identifiers - Required
     * @return Thumbnail images
     */
    Set<Image> getThumbnails(final UserContext uc, final Collection<Long> imageIds);

    /**
     * @param uc       Caller's user context. - Required
     * @param imageIds Image Instance Identifiers - Required
     * @param width    width
     * @param height   height
     * @return Resized images
     */
    Set<Image> getResizedImages(final UserContext uc, final Collection<Long> imageIds, final Integer width, final Integer height);

    /**
     * @param uc      uc Caller's user context. - Required
     * @param imageId Image Instance Identifiers - Required
     * @param width   width
     * @param height  height
     * @return Resized image
     */
    Image getResizedImage(final UserContext uc, final Long imageId, final Integer width, final Integer height);

}
