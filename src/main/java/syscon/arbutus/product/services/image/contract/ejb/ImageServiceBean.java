package syscon.arbutus.product.services.image.contract.ejb;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.ErrorCodes;
import syscon.arbutus.product.services.core.services.CrudHandler;
import syscon.arbutus.product.services.image.contract.dto.Image;
import syscon.arbutus.product.services.image.contract.interfaces.ImageService;
import syscon.arbutus.product.services.image.contract.interfaces.ImageServiceLocal;
import syscon.arbutus.product.services.image.realization.persistence.ImageEntity;
import syscon.arbutus.product.services.image.realization.persistence.OriginalImageEntity;
import syscon.arbutus.product.services.image.realization.persistence.ThumbnailImageEntity;
import syscon.arbutus.product.services.image.realization.util.ImageUtil;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * Image service implementation
 *
 * @author os
 * @version 1.0
 * @since July 28, 2014
 */
@Stateless(mappedName = "ImageService", description = "The Image Service")
public class ImageServiceBean implements ImageServiceLocal, ImageService {

    private transient static final Logger LOG = LoggerFactory.getLogger(ImageServiceBean.class);

    @Resource
    private SessionContext context;

    /**
     * Let the container create a session.
     */
    @PersistenceContext(unitName = "arbutus-pu")
    private Session session;

    private final static CrudHandler<ImageEntity, Image> imageDao = new CrudHandler<ImageEntity, Image>(ImageEntity.class, Image.class) {

        @Override
        public Image toDto(final ImageEntity entity) {
            return ImageHelper.getOriginal(entity);
        }

        @Override
        public ImageEntity toEntity(final Image dto) {
            return ImageHelper.toImageEntity(dto);
        }
    };

    @Override
    public Long create(final UserContext uc, final Image image) {
        ValidationHelper.validate(image);

        final ImageEntity imageEntity = imageDao.toEntity(image);

        final OriginalImageEntity originalImageEntity = imageEntity.getOriginalImageEntity();
        final ThumbnailImageEntity thumbnailImageEntity = imageEntity.getThumbnailImageEntity();
        imageEntity.setOriginalImageEntity(null);
        imageEntity.setThumbnailImageEntity(null);

        final Long imageId = (Long) session.save(imageEntity);

        originalImageEntity.setId(imageId);
        thumbnailImageEntity.setId(imageId);

        session.save(originalImageEntity);
        session.save(thumbnailImageEntity);

        return imageId;
    }

    @Override
    public Set<Long> create(final UserContext uc, final Collection<Image> images) {
        Set<Long> ids = new HashSet<>();
        for (Image image : images) {
            ids.add(imageDao.create(session, image).getId());
        }

        return ids;
    }

    @Override
    public void update(final UserContext uc, final Image image) {
        imageDao.update(session, image);
    }

    @Override
    public void update(final UserContext uc, final Collection<Image> images) {
        for (Image image : images) {
            imageDao.update(session, image);
        }
    }

    @Override
    public void delete(final UserContext uc, final Long id) {
        final ImageEntity entity = (ImageEntity) session.get(ImageEntity.class, id);

        if (entity == null) {
            throw new DataNotExistException(String.format("Cannot delete Image with id: %d", id));
        }

        session.delete(entity);
    }

    @Override
    public void delete(final UserContext uc, final Long id, final Long version) {
        imageDao.delete(session, id, version);
    }

    @Override
    public Image getOriginal(final UserContext uc, final Long id) {
        return ImageHelper.getOriginal(imageDao.getEntity(session, id));
    }

    @Override
    public Image getThumbnail(final UserContext uc, final Long id) {
        return ImageHelper.getThumbnail(imageDao.getEntity(session, id));
    }

    @Override
    public Set<Image> getOriginals(UserContext uc, Collection<Long> imageIds) {
        return ImageHelper.getOriginals(imageDao.getEntites(session, imageIds));
    }

    @Override
    public Set<Image> getThumbnails(UserContext uc, Collection<Long> imageIds) {
        return ImageHelper.getThumbnails(imageDao.getEntites(session, imageIds));
    }

    @Override
    public Set<Image> getResizedImages(UserContext uc, Collection<Long> imageIds, Integer width, Integer height) {
        if (width == null || width <= 0) {
            throw new ArbutusRuntimeException(ErrorCodes.IMG_INVALID_WIDTH);
        }
        if (height == null || height <= 0) {
            throw new ArbutusRuntimeException(ErrorCodes.IMG_INVALID_HEIGHT);
        }
        Set<Image> originals = getOriginals(uc, imageIds);
        Set<Image> ret = new HashSet<>(originals.size());
        for (Image original : originals) {
            try {
                ret.add(ImageUtil.resizeImage(original, width, height));
            } catch (Exception e) {
                throw new ArbutusRuntimeException(ErrorCodes.IMG_ERROR_REZISE_IMAGE);
            }
        }
        return ret;
    }

    @Override
    public Image getResizedImage(UserContext uc, Long imageId, Integer width, Integer height) {
        if (width == null || width <= 0) {
            throw new ArbutusRuntimeException(ErrorCodes.IMG_INVALID_WIDTH);
        }
        if (height == null || height <= 0) {
            throw new ArbutusRuntimeException(ErrorCodes.IMG_INVALID_HEIGHT);
        }
        Image original = getOriginal(uc, imageId);
        try {
            return ImageUtil.resizeImage(original, width, height);
        } catch (Exception e) {
            throw new ArbutusRuntimeException(ErrorCodes.IMG_ERROR_REZISE_IMAGE);
        }
    }
}
