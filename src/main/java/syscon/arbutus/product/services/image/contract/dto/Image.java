package syscon.arbutus.product.services.image.contract.dto;

import javax.validation.constraints.NotNull;

import org.apache.commons.codec.binary.Base64;
import syscon.arbutus.product.services.core.common.StampInterface;
import syscon.arbutus.product.services.core.contract.dto.BaseDto;

/**
 * Image DTO
 *
 * @author os
 * @version 1.0
 * @since July 28, 2014
 */
public class Image extends BaseDto {

    private Long id;
    private Integer width;
    private Integer height;
    private ImageFormat imageFormat;
    @NotNull
    private byte[] photo;
    private volatile String base64String;

    public Image() {
    }

    public Image(final Long id, final Integer width, final Integer height, final ImageFormat imageFormat, final byte[] photo, final StampInterface stamp,
            final Long version) {
        this.id = id;
        this.width = width;
        this.height = height;
        this.imageFormat = imageFormat;
        this.photo = photo;
        this.setStamp(stamp);
        this.setVersion(version);
    }

    public Image(byte[] photo, ImageFormat imageFormat) {
        this.imageFormat = imageFormat;
        this.photo = photo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public ImageFormat getImageFormat() {
        return imageFormat;
    }

    public void setImageFormat(ImageFormat imageFormat) {
        this.imageFormat = imageFormat;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(final byte[] photo) {
        this.base64String = null;
        this.photo = photo;
    }

    public String getBase64String() {
        if (base64String == null) {
            base64String = new String(Base64.encodeBase64(photo));
        }
        return base64String;
    }

    public String getBase64ImagePrefix() {
        return String.format("data:image/%s;base64,", imageFormat);
    }
}
