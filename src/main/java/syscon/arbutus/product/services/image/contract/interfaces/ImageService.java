package syscon.arbutus.product.services.image.contract.interfaces;

import javax.ejb.Remote;

/**
 * Image service remote interface
 *
 * @author os
 * @version 1.0
 * @since July 28, 2014
 */
@Remote
public interface ImageService extends ImageServiceCommon {
}
