package syscon.arbutus.product.services.legal.realization.adapters;

import java.util.Date;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.activity.contract.dto.ScheduleTimeslotType;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.core.contract.dto.AssociationType;
import syscon.arbutus.product.services.utility.ServiceAdapter;

/**
 * An adapter class to wrap the API in ActivityService and provide the legal service bean customized API.
 *
 * @author lhan
 */
public class ActivityServiceAdapter {

    private static final String ACTIVITY_CATEGORY_CODE_MOVEMENT = "MOVEMENT";
    private static ActivityService service;

    /**
     * Constructor.
     */
    public ActivityServiceAdapter() {
        super();
    }

    synchronized private static ActivityService getService() {

        if (service == null) {
            service = (ActivityService) ServiceAdapter.JNDILookUp(service, ActivityService.class);
        }
        return service;
    }

    /**
     * @param userContext
     * @param activity
     * @param scheduleTimeslot
     * @param overrideCapacity
     * @return created activity
     */
    public ActivityType createActivity(UserContext userContext, ActivityType activity, ScheduleTimeslotType scheduleTimeslot, boolean overrideCapacity) {

        return getService().create(userContext, activity, scheduleTimeslot, overrideCapacity);
    }

    /**
     * @param uc
     * @param id
     * @param association
     * @return 1L is success
     */
    public Long createAssociation(UserContext uc, Long id, AssociationType association) {

        // AssociationReturnType ret = null;

        // ret = getService().createAssociation(uc, id, association);

        return 1L;
    }

    /**
     * @param uc
     * @param plannedStartDate
     * @return
     */
    public ActivityType generateActivity(UserContext uc, Date plannedStartDate, Date plannedEndDate) {

        ActivityType ret = new ActivityType();
        ret.setPlannedStartDate(plannedStartDate);
        ret.setPlannedEndDate(plannedEndDate);
        ret.setActiveStatusFlag(true);
        ret.setActivityCategory(ACTIVITY_CATEGORY_CODE_MOVEMENT);

        return ret;
    }

    /**
     * @param uc
     * @param id
     */
    public void deleteActivity(UserContext uc, Long id) {
        getService().delete(uc, id);
    }

    /**
     * @param uc
     * @param activityId
     * @return
     */
    public ActivityType getActivity(UserContext uc, Long activityId) {
        return getService().get(uc, activityId);
    }

    /**
     * @param uc
     * @param activityType
     */
    public void updateActivity(UserContext uc, ActivityType activityType) {
        getService().update(uc, activityType, null, false);
    }
}
