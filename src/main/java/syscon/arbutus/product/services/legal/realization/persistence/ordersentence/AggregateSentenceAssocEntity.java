package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.Associable;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * AggregateSentenceAssocEntity for persist Aggregate Sentence association
 *
 * @author yshang
 * @DbComment LEG_OrdAggSentAssc 'Aggregate Sentence table for Order Sentence module of Legal service'
 * @since November 27, 2013
 */
@Audited
@Entity
@Table(name = "LEG_OrdAggSentAssc")
public class AggregateSentenceAssocEntity implements Serializable, Associable {

    private static final long serialVersionUID = -1805481322812145029L;

    /**
     * @DbComment AssociationId 'Identification of table LEG_OrdAggSentAssc'
     */
    @Id
    @Column(name = "AssociationId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_OrdAggSentAssc_Id")
    @SequenceGenerator(name = "SEQ_LEG_OrdAggSentAssc_Id", sequenceName = "SEQ_LEG_OrdAggSentAssc_Id", allocationSize = 1)
    private Long associationId;

    /**
     * @DbComment ToClass 'SENTENCE to associate'
     */
    @Column(name = "ToClass", nullable = false, length = 64)
    private String toClass;

    /**
     * @DbComment ToIdentifier 'Identification of Sentence of the supervision'
     */
    @Column(name = "ToIdentifier", nullable = false)
    private Long toIdentifier;

    /**
     * @DbComment supervisionId 'Supervision Id, the identification of table SUP_SUPERVISION'
     */
    @Column(name = "supervisionId", nullable = false)
    private Long supervisionId;

    /**
     * @DbComment sentenceNumber 'Identifier for the sentence. Will be used  on the UI to specify the sentence relationship (consecutive)'
     */
    @Column(name = "sentenceNumber", nullable = false)
    private Long sentenceNumber;

    /**
     * @DbComment isControllingSentence 'Flag of controlling sentence. True: is controlling sentence; Null or False: no controlling sentence'
     */
    @Column(name = "isControllingSentence", nullable = true)
    private Boolean isControllingSentence;

    /**
     * @DbComment sentenceType 'The type of sentence (Definite, Split, Life etc.,)'
     */
    @Column(name = "sentenceType", nullable = false, length = 64)
    private String sentenceType;

    /**
     * @DbComment initStartDate 'Start date before calculation'
     */
    @Column(name = "initStartDate", nullable = true)
    private Date initStartDate;

    /**
     * @DbComment startDate 'Sentence Start date calculated by SentenceCalculation engine'
     */
    @Column(name = "startDate", nullable = true)
    private Date startDate;

    /**
     * @DbComment endDate 'Sentence End date calculated by SentenceCalculation engine'
     */
    @Column(name = "endDate", nullable = true)
    private Date endDate;

    /**
     * @DbComment prdDate 'Probable release date calculated by SentenceCalculation engine'
     */
    @Column(name = "prdDate", nullable = true)
    private Date prdDate;

    /**
     * @DbComment sentenceDays 'Sentence days calculated by SentenceCalculation engine'
     */
    @Column(name = "sentenceDays", nullable = true)
    private Long sentenceDays;

    /**
     * @DbComment goodTime 'Good time calculated by SentenceCalculation engine'
     */
    @Column(name = "goodTime", nullable = true)
    private Long goodTime;

    /**
     * @DbComment years 'years of duration of the sentence'
     */
    @Column(name = "years", nullable = true)
    private Long years;

    /**
     * @DbComment months 'months of duration of the sentence'
     */
    @Column(name = "months", nullable = true)
    private Long months;

    /**
     * @DbComment weeks 'weeks of duration of the sentence'
     */
    @Column(name = "weeks", nullable = true)
    private Long weeks;

    /**
     * @DbComment days 'days of duration of the sentence'
     */
    @Column(name = "days", nullable = true)
    private Long days;

    /**
     * @DbComment expiryDate 'Expire Date'
     */
    @Column(name = "expiryDate", nullable = true)
    private Date expiryDate;

    /**
     * @DbComment concecutiveId 'concecutiveId -- sentenceNumber'
     */
    @Column(name = "concecutiveId", nullable = true)
    private Long concecutiveId;

    /**
     * @DbComment staffId 'staff Id, the identification of table STF_Staff'
     */
    @Column(name = "staffId", nullable = true)
    private Long staffId;

    /**
     * @DbComment comments 'Comment -- Calculation Reason'
     */
    @Column(name = "comments", nullable = true, length = 64)
    private String comments;

    /**
     * @DbComment calculatingDate 'The date of the staff calculated'
     */
    @Column(name = "calculatingDate", nullable = true)
    private Date calculatingDate;

    /**
     * @DbComment version 'Version for Hibernate use'
     */
    @NotAudited
    @Column(name = "version")
    private Long version;

    /**
     * @DbComment FromIdentifier 'Id of aggregate sentence (table LEG_OrdAggSent)'
     */
    // @ForeignKey(name = "Fk_LEG_OrdAggSentAssc")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FromIdentifier", referencedColumnName = "aggregateId", nullable = false)
    private AggregateSentenceEntity aggregateSentence;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @return the associationId
     */
    public Long getAssociationId() {
        return associationId;
    }

    /**
     * @param associationId the associationId to set
     */
    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    /**
     * @return the toClass
     */
    public String getToClass() {
        return toClass;
    }

    /**
     * @param toClass the toClass to set
     */
    public void setToClass(String toClass) {
        this.toClass = toClass;
    }

    /**
     * @return the toIdentifier
     */
    public Long getToIdentifier() {
        return toIdentifier;
    }

    /**
     * @param toIdentifier the toIdentifier to set
     */
    public void setToIdentifier(Long toIdentifier) {
        this.toIdentifier = toIdentifier;
    }

    /**
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * @return the sentenceNumber
     */
    public Long getSentenceNumber() {
        return sentenceNumber;
    }

    /**
     * @param sentenceNumber the sentenceNumber to set
     */
    public void setSentenceNumber(Long sentenceNumber) {
        this.sentenceNumber = sentenceNumber;
    }

    /**
     * @return the isControllingSentence
     */
    public Boolean getIsControllingSentence() {
        return isControllingSentence;
    }

    /**
     * @param isControllingSentence the isControllingSentence to set
     */
    public void setIsControllingSentence(Boolean isControllingSentence) {
        this.isControllingSentence = isControllingSentence;
    }

    /**
     * @return the sentenceType
     */
    public String getSentenceType() {
        return sentenceType;
    }

    /**
     * @param sentenceType the sentenceType to set
     */
    public void setSentenceType(String sentenceType) {
        this.sentenceType = sentenceType;
    }

    /**
     * @return the initStartDate
     */
    public Date getInitStartDate() {
        return initStartDate;
    }

    /**
     * @param initStartDate the initStartDate to set
     */
    public void setInitStartDate(Date initStartDate) {
        this.initStartDate = initStartDate;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the prdDate
     */
    public Date getPrdDate() {
        return prdDate;
    }

    /**
     * @param prdDate the prdDate to set
     */
    public void setPrdDate(Date prdDate) {
        this.prdDate = prdDate;
    }

    /**
     * @return the sentenceDays
     */
    public Long getSentenceDays() {
        return sentenceDays;
    }

    /**
     * @param sentenceDays the sentenceDays to set
     */
    public void setSentenceDays(Long sentenceDays) {
        this.sentenceDays = sentenceDays;
    }

    /**
     * @return the goodTime
     */
    public Long getGoodTime() {
        return goodTime;
    }

    /**
     * @param goodTime the goodTime to set
     */
    public void setGoodTime(Long goodTime) {
        this.goodTime = goodTime;
    }

    /**
     * @return the years
     */
    public Long getYears() {
        return years;
    }

    /**
     * @param years the years to set
     */
    public void setYears(Long years) {
        this.years = years;
    }

    /**
     * @return the months
     */
    public Long getMonths() {
        return months;
    }

    /**
     * @param months the months to set
     */
    public void setMonths(Long months) {
        this.months = months;
    }

    /**
     * @return the weeks
     */
    public Long getWeeks() {
        return weeks;
    }

    /**
     * @param weeks the weeks to set
     */
    public void setWeeks(Long weeks) {
        this.weeks = weeks;
    }

    /**
     * @return the days
     */
    public Long getDays() {
        return days;
    }

    /**
     * @param days the days to set
     */
    public void setDays(Long days) {
        this.days = days;
    }

    /**
     * @return the expiryDate
     */
    public Date getExpiryDate() {
        return expiryDate;
    }

    /**
     * @param expiryDate the expiryDate to set
     */
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    /**
     * @return the concecutiveId
     */
    public Long getConcecutiveId() {
        return concecutiveId;
    }

    /**
     * @param concecutiveId the concecutiveId to set
     */
    public void setConcecutiveId(Long concecutiveId) {
        this.concecutiveId = concecutiveId;
    }

    /**
     * @return the staffId
     */
    public Long getStaffId() {
        return staffId;
    }

    /**
     * @param staffId the staffId to set
     */
    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the calculatingDate
     */
    public Date getCalculatingDate() {
        return calculatingDate;
    }

    /**
     * @param calculatingDate the calculatingDate to set
     */
    public void setCalculatingDate(Date calculatingDate) {
        this.calculatingDate = calculatingDate;
    }

    /**
     * @return the aggregateSentence
     */
    public AggregateSentenceEntity getAggregateSentence() {
        return aggregateSentence;
    }

    /**
     * @param aggregateSentence the aggregateSentence to set
     */
    public void setAggregateSentence(AggregateSentenceEntity aggregateSentence) {
        this.aggregateSentence = aggregateSentence;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((associationId == null) ? 0 : associationId.hashCode());
        result = prime * result + ((concecutiveId == null) ? 0 : concecutiveId.hashCode());
        result = prime * result + ((sentenceNumber == null) ? 0 : sentenceNumber.hashCode());
        result = prime * result + ((sentenceType == null) ? 0 : sentenceType.hashCode());
        result = prime * result + ((staffId == null) ? 0 : staffId.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        result = prime * result + ((toClass == null) ? 0 : toClass.hashCode());
        result = prime * result + ((toIdentifier == null) ? 0 : toIdentifier.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        AggregateSentenceAssocEntity other = (AggregateSentenceAssocEntity) obj;
        if (associationId == null) {
            if (other.associationId != null) {
				return false;
			}
        } else if (!associationId.equals(other.associationId)) {
			return false;
		}
        if (concecutiveId == null) {
            if (other.concecutiveId != null) {
				return false;
			}
        } else if (!concecutiveId.equals(other.concecutiveId)) {
			return false;
		}
        if (sentenceNumber == null) {
            if (other.sentenceNumber != null) {
				return false;
			}
        } else if (!sentenceNumber.equals(other.sentenceNumber)) {
			return false;
		}
        if (sentenceType == null) {
            if (other.sentenceType != null) {
				return false;
			}
        } else if (!sentenceType.equals(other.sentenceType)) {
			return false;
		}
        if (staffId == null) {
            if (other.staffId != null) {
				return false;
			}
        } else if (!staffId.equals(other.staffId)) {
			return false;
		}
        if (supervisionId == null) {
            if (other.supervisionId != null) {
				return false;
			}
        } else if (!supervisionId.equals(other.supervisionId)) {
			return false;
		}
        if (toClass == null) {
            if (other.toClass != null) {
				return false;
			}
        } else if (!toClass.equals(other.toClass)) {
			return false;
		}
        if (toIdentifier == null) {
            if (other.toIdentifier != null) {
				return false;
			}
        } else if (!toIdentifier.equals(other.toIdentifier)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "AggregateSentenceAssocEntity [associationId=" + associationId + ", toClass=" + toClass + ", toIdentifier=" + toIdentifier + ", supervisionId="
                + supervisionId + ", sentenceNumber=" + sentenceNumber + ", sentenceType=" + sentenceType + ", initStartDate=" + initStartDate + ", startDate="
                + startDate + ", endDate=" + endDate + ", prdDate=" + prdDate + ", sentenceDays=" + sentenceDays + ", goodTime=" + goodTime + ", years=" + years
                + ", months=" + months + ", weeks=" + weeks + ", days=" + days + ", expiryDate=" + expiryDate + ", concecutiveId=" + concecutiveId + ", staffId="
                + staffId + ", comments=" + comments + ", calculatingDate=" + calculatingDate + "]";
    }

}
