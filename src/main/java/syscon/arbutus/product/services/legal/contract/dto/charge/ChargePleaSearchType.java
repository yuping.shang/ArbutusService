package syscon.arbutus.product.services.legal.contract.dto.charge;

import java.io.Serializable;

import syscon.arbutus.product.services.legal.contract.dto.ValidationUtil;

/**
 * The representation of the charge plea search type
 */
public class ChargePleaSearchType implements Serializable {

    private static final long serialVersionUID = 2779828791579488764L;

    private String pleaCode;
    private String pleaComment;

    /**
     * Default constructor
     */
    public ChargePleaSearchType() {
    }

    /**
     * Constructor
     * At least one of the search criteria must be specified.
     *
     * @param pleaCode    String - A kind of plea.
     * @param pleaComment String - Comments regarding the plea
     */
    public ChargePleaSearchType(String pleaCode, String pleaComment) {
        this.pleaCode = pleaCode;
        this.pleaComment = pleaComment;
    }

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set, false otherwise.
     */
    @Deprecated
    public Boolean isWellFormed() {

        if (ValidationUtil.isEmpty(pleaCode) && ValidationUtil.isEmpty(pleaComment)) {
			return false;
		}

        return true;
    }

    /**
     * Verify if properties are set properly.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set and all values are valid, false otherwise.
     */
    @Deprecated
    public Boolean isValid() {

        if (!isWellFormed()) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the pleaCode property. It is a reference code of ChargePlea set.
     *
     * @return String - A kind of plea.
     */
    public String getPleaCode() {
        return pleaCode;
    }

    /**
     * Sets the value of the pleaCode property. It is a reference code of ChargePlea set.
     *
     * @param pleaCode String - A kind of plea.
     */
    public void setPleaCode(String pleaCode) {
        this.pleaCode = pleaCode;
    }

    /**
     * Gets the value of the pleaComment property.
     *
     * @return String - Comments regarding the plea
     */
    public String getPleaComment() {
        return pleaComment;
    }

    /**
     * Sets the value of the pleaComment property.
     *
     * @param pleaComment String - Comments regarding the plea
     */
    public void setPleaComment(String pleaComment) {
        this.pleaComment = pleaComment;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargePleaSearchType [pleaCode=" + pleaCode + ", pleaComment=" + pleaComment + "]";
    }

}
