package syscon.arbutus.product.services.legal.contract.dto.valueholder;

import java.io.Serializable;

//JSON Holder for Charge JSON Data
public class ChargeHolder implements Serializable {

    public String offensedate;
    public String chargecode;
    public String description;
    public String type;
    public String outcome;
    public String status;
    public String primarycharge;
    public String chargeId;
    public ChargeHolder() {
        super();
    }

}