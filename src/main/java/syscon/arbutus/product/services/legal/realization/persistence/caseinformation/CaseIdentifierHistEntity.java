package syscon.arbutus.product.services.legal.realization.persistence.caseinformation;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the case information identifier history entity.
 *
 * @DbComment LEG_CIIdentifierHist 'The case information identifier history table'
 */
//@Audited
@Entity
@Table(name = "LEG_CIIdentifierHist")
public class CaseIdentifierHistEntity implements Serializable {

    private static final long serialVersionUID = -4036698704210829919L;

    /**
     * @DbComment IdentifierHistoryId 'Unique Id for each case identifier history record'
     */
    @Id
    @Column(name = "IdentifierHistoryId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CIIdentifierHist_Id")
    @SequenceGenerator(name = "SEQ_LEG_CIIdentifierHist_Id", sequenceName = "SEQ_LEG_CIIdentifierHist_Id", allocationSize = 1)
    private Long identifierHistoryId;

    /**
     * @DbComment IdentifierId 'Unique Id for each case identifier record'
     */
    @Column(name = "IdentifierId", nullable = false)
    private Long identifierId;

    /**
     * @DbComment IdentifierType 'A kind of identifier or number that can be associated to a criminal case e.g. Court Information Number, Indictment Number'
     */
    @Column(name = "IdentifierType", length = 64, nullable = false)
    private String identifierType;

    /**
     * @DbComment IdentifierValue 'The number or value of the case identifier e.g. 1234567S (Indictment Number)'
     */
    @Column(name = "IdentifierValue", length = 64, nullable = false)
    private String identifierValue;

    /**
     * @DbComment AutoGenerate 'Allows the user to indicate to the system whether or not to auto generate the identifier when creating it. This is set to false by default and cannot be set to true for an update. If set to true the IdentifierValue is ignored.'
     */
    @Column(name = "AutoGenerate", nullable = true)
    private Boolean autoGenerate;

    /**
     * @DbComment IsPrimary 'Indicate if this is a primary identifier for a case.'
     */
    @Column(name = "IsPrimary", nullable = false)
    private Boolean primary;

    /**
     * @DbComment OrganizationId 'Static reference to an Organization which issued the case identifier'
     */
    @Column(name = "OrganizationId", nullable = true)
    private Long organizationId;

    /**
     * @DbComment FacilityId 'Static reference to a Facility which issued the case identifier'
     */
    @Column(name = "FacilityId", nullable = true)
    private Long facilityId;

    /**
     * @DbComment IdentifierComment 'Comments associated to the case identifier'
     */
    @Column(name = "IdentifierComment", length = 1024, nullable = true)
    private String identifierComment;

    /**
     * @DbComment CaseInfoHistoryId 'The case information history instance Id'
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CaseInfoHistoryId", nullable = false)
    private CaseInfoHistEntity caseInfoHistory;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     *
     */
    public CaseIdentifierHistEntity() {
    }

    /**
     * @param identifierHistoryId
     * @param identifierId
     * @param identifierType
     * @param identifierValue
     * @param autoGenerate
     * @param organizationId
     * @param facilityId
     * @param identifierComment
     * @param caseInfoHistory
     * @param stamp
     */
    public CaseIdentifierHistEntity(Long identifierHistoryId, Long identifierId, String identifierType, String identifierValue, Boolean autoGenerate, Long organizationId,
            Long facilityId, String identifierComment, CaseInfoHistEntity caseInfoHistory, StampEntity stamp, Boolean primary) {
        super();
        this.identifierHistoryId = identifierHistoryId;
        this.identifierId = identifierId;
        this.identifierType = identifierType;
        this.identifierValue = identifierValue;
        this.autoGenerate = autoGenerate;
        this.organizationId = organizationId;
        this.facilityId = facilityId;
        this.identifierComment = identifierComment;
        this.caseInfoHistory = caseInfoHistory;
        this.stamp = stamp;
        this.primary = primary;
    }

    /**
     * @return the identifierHistoryId
     */
    public Long getIdentifierHistoryId() {
        return identifierHistoryId;
    }

    /**
     * @param identifierHistoryId the identifierHistoryId to set
     */
    public void setIdentifierHistoryId(Long identifierHistoryId) {
        this.identifierHistoryId = identifierHistoryId;
    }

    /**
     * @return the identifierId
     */
    public Long getIdentifierId() {
        return identifierId;
    }

    /**
     * @param identifierId the identifierId to set
     */
    public void setIdentifierId(Long identifierId) {
        this.identifierId = identifierId;
    }

    /**
     * @return the identifierType
     */
    public String getIdentifierType() {
        return identifierType;
    }

    /**
     * @param identifierType the identifierType to set
     */
    public void setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
    }

    /**
     * @return the identifierValue
     */
    public String getIdentifierValue() {
        return identifierValue;
    }

    /**
     * @param identifierValue the identifierValue to set
     */
    public void setIdentifierValue(String identifierValue) {
        this.identifierValue = identifierValue;
    }

    /**
     * @return the autoGenerate
     */
    public Boolean isAutoGenerate() {
        return autoGenerate;
    }

    /**
     * @param autoGenerate the autoGenerate to set
     */
    public void setAutoGenerate(Boolean autoGenerate) {
        this.autoGenerate = autoGenerate;
    }

    /**
     * @return
     */
    public Boolean isPrimary() {
        return primary;
    }

    /**
     * @param primary
     */
    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

    /**
     * @return the organizationId
     */
    public Long getOrganizationId() {
        return organizationId;
    }

    /**
     * @param organizationId the organizationId to set
     */
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * @return the facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * @param facilityId the facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * @return the identifierComment
     */
    public String getIdentifierComment() {
        return identifierComment;
    }

    /**
     * @param identifierComment the identifierComment to set
     */
    public void setIdentifierComment(String identifierComment) {
        this.identifierComment = identifierComment;
    }

    /**
     * @return the caseInfo
     */
    public CaseInfoHistEntity getCaseInfo() {
        return caseInfoHistory;
    }

    /**
     * @param caseInfo the caseInfo to set
     */
    public void setCaseInfo(CaseInfoHistEntity caseInfoHistory) {
        this.caseInfoHistory = caseInfoHistory;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public String toString() {
        return "CaseIdentifierHistEntity [getIdentifierHistoryId()=" + getIdentifierHistoryId() + ", getIdentifierId()=" + getIdentifierId() + ", getIdentifierType()="
                + getIdentifierType() + ", getIdentifierValue()=" + getIdentifierValue() + ", isAutoGenerate()=" + isAutoGenerate() + ", isPrimary()=" + isPrimary()
                + ", getOrganizationId()=" + getOrganizationId() + ", getFacilityId()=" + getFacilityId() + ", getIdentifierComment()=" + getIdentifierComment()
                + ", getStamp()=" + getStamp() + "]";
    }
}
