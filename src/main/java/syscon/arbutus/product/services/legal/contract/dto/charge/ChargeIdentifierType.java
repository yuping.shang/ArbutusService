package syscon.arbutus.product.services.legal.contract.dto.charge;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * The representation of the charge identifier type
 */
public class ChargeIdentifierType implements Serializable {

    private static final long serialVersionUID = 8313626552906537218L;

    private Long identifierId;
    @NotNull
    private String identifierType;
    @NotNull
    @Size(max = 64, message = "max length 64")
    private String identifierValue;
    private String identifierFormat;
    private Long organizationId;
    private Long facilityId;
    @Size(max = 512, message = "max length 512")
    private String comment;

    /**
     * Default constructor
     */
    public ChargeIdentifierType() {
    }

    /**
     * Constructor
     *
     * @param identifierId     Long - the Unique Id for each charge identifier record, (optional for creation)
     * @param identifierType   String - A set of numbers/identifiers assigned by an arresting agency, prosecuting attorney or court. E.g. CJIS code
     * @param identifierValue  String - The number or value of the charge identifier
     * @param identifierFormat String - The format for the Identifier. (optional)
     * @param organization     Long - Static reference to an Organization which issued the charge identifier
     * @param facility         Long - Static reference to a Facility which issued the charge identifier
     * @param comment          String - Comments associated to the identifier (optional)
     */
    public ChargeIdentifierType(Long identifierId, @NotNull String identifierType, @NotNull @Size(max = 64, message = "max length 64") String identifierValue,
            String identifierFormat, Long organization, Long facility, @Size(max = 512, message = "max length 512") String comment) {
        this.identifierId = identifierId;
        this.identifierType = identifierType;
        this.identifierValue = identifierValue;
        this.identifierFormat = identifierFormat;
        this.organizationId = organization;
        this.facilityId = facility;
        this.comment = comment;
    }

    /**
     * Gets the value of the identifierId property.
     *
     * @return Long - the Unique Id for each charge identifier record
     */
    public Long getIdentifierId() {
        return identifierId;
    }

    /**
     * Sets the value of the identifierId property.
     *
     * @param identifierId Long - the Unique Id for each charge identifier record
     */
    public void setIdentifierId(Long identifierId) {
        this.identifierId = identifierId;
    }

    /**
     * Gets the value of the identifierType property. It is a reference code value of ChargeIdentifierType set.
     *
     * @return String - A set of numbers/identifiers assigned by an arresting agency, prosecuting attorney or court. E.g. CJIS code
     */
    public String getIdentifierType() {
        return identifierType;
    }

    /**
     * Sets the value of the identifierType property. It is a reference code value of ChargeIdentifierType set.
     *
     * @param identifierType String - A set of numbers/identifiers assigned by an arresting agency, prosecuting attorney or court. E.g. CJIS code
     */
    public void setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
    }

    /**
     * Gets the value of the identifierValue property.
     *
     * @return String - The number or value of the charge identifier
     */
    public String getIdentifierValue() {
        return identifierValue;
    }

    /**
     * Sets the value of the identifierValue property.
     *
     * @param identifierValue String - The number or value of the charge identifier
     */
    public void setIdentifierValue(String identifierValue) {
        this.identifierValue = identifierValue;
    }

    /**
     * Gets the value of the identifierFormat property. It is a reference code value of ChargeIdentifierFormat set.
     *
     * @return String - The format for the Identifier.
     */
    public String getIdentifierFormat() {
        return identifierFormat;
    }

    /**
     * Sets the value of the identifierFormat property. It is a reference code value of ChargeIdentifierFormat set.
     *
     * @param identifierFormat String - The format for the Identifier.
     */
    public void setIdentifierFormat(String identifierFormat) {
        this.identifierFormat = identifierFormat;
    }

    /**
     * Gets the value of the organization property.
     *
     * @return Long - Static reference to an Organization which issued the charge identifier
     */
    public Long getOrganizationId() {
        return organizationId;
    }

    /**
     * Sets the value of the organization property.
     *
     * @param organizationId Long - Static reference to an Organization which issued the charge identifier
     */
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * Gets the value of the facility property.
     *
     * @return Long - Static reference to a Facility which issued the charge identifier
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * Sets the value of the facility property.
     *
     * @param facilityId Long - Static reference to a Facility which issued the charge identifier
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * Gets the value of the comment property.
     *
     * @return String - Comments associated to the identifier
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     *
     * @param comment String - Comments associated to the identifier
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((facilityId == null) ? 0 : facilityId.hashCode());
        result = prime * result + ((identifierFormat == null) ? 0 : identifierFormat.hashCode());
        result = prime * result + ((identifierType == null) ? 0 : identifierType.hashCode());
        result = prime * result + ((identifierValue == null) ? 0 : identifierValue.hashCode());
        result = prime * result + ((organizationId == null) ? 0 : organizationId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ChargeIdentifierType other = (ChargeIdentifierType) obj;
        if (comment == null) {
            if (other.comment != null) {
				return false;
			}
        } else if (!comment.equals(other.comment)) {
			return false;
		}
        if (facilityId == null) {
            if (other.facilityId != null) {
				return false;
			}
        } else if (!facilityId.equals(other.facilityId)) {
			return false;
		}
        if (identifierFormat == null) {
            if (other.identifierFormat != null) {
				return false;
			}
        } else if (!identifierFormat.equals(other.identifierFormat)) {
			return false;
		}
        if (identifierType == null) {
            if (other.identifierType != null) {
				return false;
			}
        } else if (!identifierType.equals(other.identifierType)) {
			return false;
		}
        if (identifierValue == null) {
            if (other.identifierValue != null) {
				return false;
			}
        } else if (!identifierValue.equals(other.identifierValue)) {
			return false;
		}
        if (organizationId == null) {
            if (other.organizationId != null) {
				return false;
			}
        } else if (!organizationId.equals(other.organizationId)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeIdentifierType [getIdentifierId()=" + getIdentifierId() + ", getIdentifierType()=" + getIdentifierType() + ", getIdentifierValue()="
                + getIdentifierValue() + ", getIdentifierFormat()=" + getIdentifierFormat() + ", getOrganizationId()=" + getOrganizationId() + ", getFacilityId()="
                + getFacilityId() + ", getComment()=" + getComment() + "]";
    }

}
