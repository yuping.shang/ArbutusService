package syscon.arbutus.product.services.legal.contract.dto.charge;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * The representation of the statute configuration type
 */
public class StatuteConfigType implements Serializable {

    private static final long serialVersionUID = -5987642200920604359L;

    private Long statuteId;
    //StatuteId --> Statute Set
    @NotNull
    private String statuteCode;
    @NotNull
    private Date enactmentDate;
    private Date repealDate;
    @Size(max = 64, message = "max length 64")
    private String statuteSection;
    @Size(max = 128, message = "max length 128")
    private String description;
    private Boolean active;  //derived value
    @NotNull
    private Long jurisdictionId;
    @NotNull
    private Boolean defaultJurisdiction;
    @NotNull
    private Boolean outOfJurisdiction;

    /**
     * Default constructor
     */
    public StatuteConfigType() {
    }

    /**
     * Constructor
     *
     * @param statuteId           Long - The unique Id for each statute record. (optional for creation)
     * @param statuteCode         String - An identifier of a set of laws for a particular jurisdiction
     * @param statuteSection      String - An identifier of a section or category within a code book. (optional)
     * @param description         String - A description of a statute. (optional)
     * @param enactmentDate       Date - A date a statute was enacted and came into effect.
     * @param repealDate          Date - A date a statute was repealed and no longer applies. (optional)
     * @param jurisdictionId      Long - Details about an area in which a statute applies, reference to the Jurisdiction Configuration Type.
     * @param defaultJurisdiction Boolean - True if the charge codes default to this Statute, false otherwise.
     * @param outOfJurisdiction   Boolean - True if this statute is outside the jurisdiction, false otherwise.
     */
    public StatuteConfigType(Long statuteId, @NotNull String statuteCode, @NotNull Date enactmentDate, Date repealDate,
            @Size(max = 64, message = "max length 64") String statuteSection, @Size(max = 128, message = "max length 128") String description,
            @NotNull Long jurisdictionId, @NotNull Boolean defaultJurisdiction, @NotNull Boolean outOfJurisdiction) {
        this.statuteId = statuteId;
        this.statuteCode = statuteCode;
        this.enactmentDate = enactmentDate;
        this.repealDate = repealDate;
        this.statuteSection = statuteSection;
        this.description = description;
        this.jurisdictionId = jurisdictionId;
        this.defaultJurisdiction = defaultJurisdiction;
        this.outOfJurisdiction = outOfJurisdiction;
    }

    /**
     * Gets the value of the statuteId property.
     *
     * @return Long - The unique Id for each statute record.
     */
    public Long getStatuteId() {
        return statuteId;
    }

    /**
     * Sets the value of the statuteId property.
     *
     * @param statuteId Long - The unique Id for each statute record.
     */
    public void setStatuteId(Long statuteId) {
        this.statuteId = statuteId;
    }

    /**
     * Gets the value of the statuteCode property.
     * It is a reference code value of StatuteCode set.
     *
     * @return String - An identifier of a set of laws for a particular jurisdiction
     */
    public String getStatuteCode() {
        return statuteCode;
    }

    /**
     * Sets the value of the statuteCode property.
     * It is a reference code value of StatuteCode set.
     *
     * @param statuteCode String - An identifier of a set of laws for a particular jurisdiction
     */
    public void setStatuteCode(String statuteCode) {
        this.statuteCode = statuteCode;
    }

    /**
     * Gets the value of the enactmentDate property.
     *
     * @return Date - A date a statute was enacted and came into effect.
     */
    public Date getEnactmentDate() {
        return enactmentDate;
    }

    /**
     * Sets the value of the enactmentDate property.
     *
     * @param enactmentDate Date - A date a statute was enacted and came into effect.
     */
    public void setEnactmentDate(Date enactmentDate) {
        this.enactmentDate = enactmentDate;
    }

    /**
     * Gets the value of the repealDate property.
     *
     * @return Date - A date a statute was repealed and no longer applies.
     */
    public Date getRepealDate() {
        return repealDate;
    }

    /**
     * Sets the value of the repealDate property.
     *
     * @param repealDate Date - A date a statute was repealed and no longer applies.
     */
    public void setRepealDate(Date repealDate) {
        this.repealDate = repealDate;
    }

    /**
     * Gets the value of the statuteSection property.
     *
     * @return String - An identifier of a section or category within a code book
     */
    public String getStatuteSection() {
        return statuteSection;
    }

    /**
     * Sets the value of the statuteSection property.
     *
     * @param statuteSection String - An identifier of a section or category within a code book
     */
    public void setStatuteSection(String statuteSection) {
        this.statuteSection = statuteSection;
    }

    /**
     * Gets the value of the description property.
     *
     * @return String - A description of a statute.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param description String - A description of a statute.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the value of the active property.
     *
     * @return Boolean - Indicate the statute is still active or not. (Derived value from repealDate)
     */
    public Boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     *
     * @param active Boolean - Indicate the statute is still active or not. (Derived value, will ignore if set)
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * Gets the value of the jurisdictionId property.
     *
     * @return Long - Details about an area in which a statute applies, reference to the Jurisdiction Configuration Type.
     */
    public Long getJurisdictionId() {
        return jurisdictionId;
    }

    /**
     * Sets the value of the jurisdictionId property.
     *
     * @param jurisdictionId Long - Details about an area in which a statute applies, reference to the Jurisdiction Configuration Type.
     */
    public void setJurisdictionId(Long jurisdictionId) {
        this.jurisdictionId = jurisdictionId;
    }

    /**
     * Gets the value of the defaultJurisdiction property.
     *
     * @return Boolean - True if the charge codes default to this Statute, false otherwise.
     */
    public Boolean isDefaultJurisdiction() {
        return defaultJurisdiction;
    }

    /**
     * Sets the value of the defaultJurisdiction property.
     *
     * @param defaultJurisdiction Boolean - True if the charge codes default to this Statute, false otherwise.
     */
    public void setDefaultJurisdiction(Boolean defaultJurisdiction) {
        this.defaultJurisdiction = defaultJurisdiction;
    }

    /**
     * Gets the value of the outOfJurisdiction property.
     *
     * @return Boolean - True if this statute is outside the jurisdiction, false otherwise.
     */
    public Boolean isOutOfJurisdiction() {
        return outOfJurisdiction;
    }

    /**
     * Gets the value of the outOfJurisdiction property.
     *
     * @param outOfJurisdiction Boolean - True if this statute is outside the jurisdiction, false otherwise.
     */
    public void setOutOfJurisdiction(Boolean outOfJurisdiction) {
        this.outOfJurisdiction = outOfJurisdiction;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StatuteConfigType [statuteId=" + statuteId + ", statuteCode=" + statuteCode + ", enactmentDate=" + enactmentDate + ", repealDate=" + repealDate
                + ", statuteSection=" + statuteSection + ", description=" + description + ", active=" + active + ", jurisdictionId=" + jurisdictionId
                + ", defaultJurisdiction=" + defaultJurisdiction + ", outOfJurisdiction=" + outOfJurisdiction + "]";
    }

}
