/**
 *
 */
package syscon.arbutus.product.services.legal.contract.dto.caseaffiliation;

import java.io.Serializable;
import java.util.List;

/**
 * The representation of the case affiliations return type.
 *
 * @author lhan
 */
public class CaseAffiliationsReturnType implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 769974941861251216L;

    private List<CaseAffiliationType> caseAffiliations;
    private Long totalSize;

    /**
     * Gets the value of the totalSize property.
     *
     * @return the total size of search result
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * Sets the value of the totalSize property.
     *
     * @param totalSize the total size of search result
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /**
     * @return the caseAffiliations
     */
    public List<CaseAffiliationType> getCaseAffiliations() {
        return caseAffiliations;
    }

    /**
     * @param caseAffiliations the caseAffiliations to set
     */
    public void setCaseAffiliations(List<CaseAffiliationType> caseAffiliations) {
        this.caseAffiliations = caseAffiliations;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseAffiliationsReturnType [getTotalSize()=" + getTotalSize() + ", getCaseAffiliations()=" + getCaseAffiliations() + "]";
    }

}
