package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * KeyDateType for Sentence of Legal Service
 * <p>Apply to the Active Supervision period of an offender.
 *
 * @author yshang
 * @since July 19, 2013
 */
public class KeyDateType implements Serializable {

    private static final long serialVersionUID = -5152111447328717760L;

    @NotNull
    private String keyDateType;

    @NotNull
    private Date keyDate;

    private Date keyDateOverride;

    private Date keyDateAdjust;

    private Long overrideByStaffId;

    private String overrideReason;

    /**
     * Constructor
     */
    public KeyDateType() {
        super();
    }

    /**
     * Constructor
     *
     * @param keyDateType       String - Required. The type of Key Date
     * @param keyDate           Date - Required. Key Date
     * @param keyDateOverride   Date - Optional. The date overrides the key date
     * @param keyDateAdjust     Date - Optional. Key Date Adjustments
     * @param overrideByStaffId Long - Optional. The staff who override the key date
     * @param overrideReason    String - Optional, reference code of KeyDateOverrideReason set. Override reason
     */
    public KeyDateType(@NotNull String keyDateType, @NotNull Date keyDate, Date keyDateOverride, Date keyDateAdjust, Long overrideByStaffId, String overrideReason) {
        super();
        this.keyDateType = keyDateType;
        this.keyDate = keyDate;
        this.keyDateOverride = keyDateOverride;
        this.keyDateAdjust = keyDateAdjust;
        this.overrideByStaffId = overrideByStaffId;
        this.overrideReason = overrideReason;
    }

    /**
     * keyDateType, required
     *
     * @return the keyDateType
     */
    public String getKeyDateType() {
        return keyDateType;
    }

    /**
     * keyDateType, required
     *
     * @param keyDateType the keyDateType to set
     */
    public void setKeyDateType(String keyDateType) {
        this.keyDateType = keyDateType;
    }

    /**
     * keyDate, required
     *
     * @return the keyDate
     */
    public Date getKeyDate() {
        return keyDate;
    }

    /**
     * keyDate, required
     *
     * @param keyDate the keyDate to set
     */
    public void setKeyDate(Date keyDate) {
        this.keyDate = keyDate;
    }

    /**
     * @return the keyDateOverride
     */
    public Date getKeyDateOverride() {
        return keyDateOverride;
    }

    /**
     * @param keyDateOverride the keyDateOverride to set
     */
    public void setKeyDateOverride(Date keyDateOverride) {
        this.keyDateOverride = keyDateOverride;
    }

    /**
     * Get Key Date Adjust
     *
     * @return the keyDateAdjust
     */
    public Date getKeyDateAdjust() {
        return keyDateAdjust;
    }

    /**
     * Set Key Date Adjust
     *
     * @param keyDateAdjust the keyDateAdjust to set
     */
    public void setKeyDateAdjust(Date keyDateAdjust) {
        this.keyDateAdjust = keyDateAdjust;
    }

    /**
     * Override by staff(Id)
     *
     * @return the overrideByStaffId
     */
    public Long getOverrideByStaffId() {
        return overrideByStaffId;
    }

    /**
     * Override by staff(Id)
     *
     * @param overrideByStaffId the overrideByStaffId to set
     */
    public void setOverrideByStaffId(Long overrideByStaffId) {
        this.overrideByStaffId = overrideByStaffId;
    }

    /**
     * Override reason, reference code of KeyDateOverrideReason set
     *
     * @return the overrideReason
     */
    public String getOverrideReason() {
        return overrideReason;
    }

    /**
     * Override reason, reference code of KeyDateOverrideReason set
     *
     * @param overrideReason the overrideReason to set
     */
    public void setOverrideReason(String overrideReason) {
        this.overrideReason = overrideReason;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
	 */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((keyDate == null) ? 0 : keyDate.hashCode());
        result = prime * result + ((keyDateAdjust == null) ? 0 : keyDateAdjust.hashCode());
        result = prime * result + ((keyDateOverride == null) ? 0 : keyDateOverride.hashCode());
        result = prime * result + ((keyDateType == null) ? 0 : keyDateType.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        KeyDateType other = (KeyDateType) obj;
        if (keyDate == null) {
            if (other.keyDate != null) {
				return false;
			}
        } else if (!keyDate.equals(other.keyDate)) {
			return false;
		}
        if (keyDateAdjust == null) {
            if (other.keyDateAdjust != null) {
				return false;
			}
        } else if (!keyDateAdjust.equals(other.keyDateAdjust)) {
			return false;
		}
        if (keyDateOverride == null) {
            if (other.keyDateOverride != null) {
				return false;
			}
        } else if (!keyDateOverride.equals(other.keyDateOverride)) {
			return false;
		}
        if (keyDateType == null) {
            if (other.keyDateType != null) {
				return false;
			}
        } else if (!keyDateType.equals(other.keyDateType)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "KeyDateType [keyDateType=" + keyDateType + ", keyDate=" + keyDate + ", keyDateOverride=" + keyDateOverride + ", keyDateAdjust=" + keyDateAdjust
                + ", overrideByStaffId=" + overrideByStaffId + ", overrideReason=" + overrideReason + "]";
    }

}
