package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * SentenceAppealType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 28, 2012
 */
public class SentenceAppealType implements Serializable {

    private static final long serialVersionUID = 1951385696708626038L;

    @NotNull
    private String appealStatus;
    @Size(max = 512, message = "max length 512")
    private String appealComment;

    /**
     * Constructor
     */
    public SentenceAppealType() {
        super();
    }

    /**
     * Constructor
     *
     * @param appealStatus  String, Required. A status of an appeal on a sentence.
     * @param appealComment String(length <= 512), Optional. General comments on the outcome of the appeal.
     */
    public SentenceAppealType(@NotNull String appealStatus, @Size(max = 512, message = "max length 512") String appealComment) {
        super();
        this.appealStatus = appealStatus;
        this.appealComment = appealComment;
    }

    /**
     * Get Appeal Status
     * -- A status of an appeal on a sentence.
     *
     * @return the appealStatus
     */
    public String getAppealStatus() {
        return appealStatus;
    }

    /**
     * Set Appeal Status
     * -- A status of an appeal on a sentence.
     *
     * @param appealStatus the appealStatus to set
     */
    public void setAppealStatus(String appealStatus) {
        this.appealStatus = appealStatus;
    }

    /**
     * Get Appeal Comment
     * -- General comments on the outcome of the appeal.
     *
     * @return the appealComment
     */
    public String getAppealComment() {
        return appealComment;
    }

    /**
     * Set Appeal Comment
     * -- General comments on the outcome of the appeal.
     *
     * @param appealComment the appealComment to set
     */
    public void setAppealComment(String appealComment) {
        this.appealComment = appealComment;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((appealComment == null) ? 0 : appealComment.hashCode());
        result = prime * result + ((appealStatus == null) ? 0 : appealStatus.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SentenceAppealType other = (SentenceAppealType) obj;
        if (appealComment == null) {
            if (other.appealComment != null) {
				return false;
			}
        } else if (!appealComment.equals(other.appealComment)) {
			return false;
		}
        if (appealStatus == null) {
            if (other.appealStatus != null) {
				return false;
			}
        } else if (!appealStatus.equals(other.appealStatus)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SentenceAppealType [appealStatus=" + appealStatus + ", appealComment=" + appealComment + "]";
    }

}
