package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * SentenceKeyDateType for Supervision's Key Dates
 *
 * @author yshang
 * @since July 22, 2013
 */
public class SentenceKeyDateType implements Serializable {

    private static final long serialVersionUID = -1995829790727710307L;
    Set<KeyDateType> keydates;
    @NotNull
    private Long supervisionId;
    private Long staffId;
    private CommentType comment;
    private Boolean reviewRequired;
    private Long historyId;

    private Long supervisionSentenceId;

    private List<Long> aggregateGroupIds;

    private List<Long> sentenceIds;

    /**
     * Constructor
     */
    public SentenceKeyDateType() {
        super();
    }

    /**
     * Constructor
     *
     * @param supervisionId  Long, Required - Supervision Identification
     * @param staffId        Long, Optional - Staff Identification
     * @param comment        CommentType, Optional - Comment from Staff
     * @param reviewRequired Boolean, Optional - Flag of date review required
     * @param keydates       Set&lt;KeyDateType>, Optional - A set of KeyDateType
     */
    public SentenceKeyDateType(@NotNull Long supervisionId, Long staffId, CommentType comment, Boolean reviewRequired, Set<KeyDateType> keydates) {
        super();
        this.supervisionId = supervisionId;
        this.staffId = staffId;
        this.comment = comment;
        this.reviewRequired = reviewRequired;
        this.keydates = keydates;
    }

    /**
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * @return the staffId
     */
    public Long getStaffId() {
        return staffId;
    }

    /**
     * @param staffId the staffId to set
     */
    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    /**
     * @return the comment
     */
    public CommentType getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(CommentType comment) {
        this.comment = comment;
    }

    /**
     * @return the reviewRequired
     */
    public Boolean getReviewRequired() {
        return reviewRequired;
    }

    /**
     * @param reviewRequired the reviewRequired to set
     */
    public void setReviewRequired(Boolean reviewRequired) {
        this.reviewRequired = reviewRequired;
    }

    /**
     * @return the keydates
     */
    public Set<KeyDateType> getKeydates() {
        return keydates;
    }

    /**
     * @param keydates the keydates to set
     */
    public void setKeydates(Set<KeyDateType> keydates) {
        this.keydates = keydates;
    }

    /**
     * @return the historyId
     */
    public Long getHistoryId() {
        return historyId;
    }

    /**
     * @param historyId the historyId to set
     */
    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    /**
     * @return the supervisionSentenceId
     */
    public Long getSupervisionSentenceId() {
        return supervisionSentenceId;
    }

    /**
     * @param supervisionSentenceId the supervisionSentenceId to set
     */
    public void setSupervisionSentenceId(Long supervisionSentenceId) {
        this.supervisionSentenceId = supervisionSentenceId;
    }

    /**
     * @return the aggregateGroupIds
     */
    public List<Long> getAggregateGroupIds() {
        return aggregateGroupIds;
    }

    /**
     * @param aggregateGroupIds the aggregateGroupIds to set
     */
    public void setAggregateGroupIds(List<Long> aggregateGroupIds) {
        this.aggregateGroupIds = aggregateGroupIds;
    }

    /**
     * @return the sentenceIds
     */
    public List<Long> getSentenceIds() {
        return sentenceIds;
    }

    /**
     * @param sentenceIds the sentenceIds to set
     */
    public void setSentenceIds(List<Long> sentenceIds) {
        this.sentenceIds = sentenceIds;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((keydates == null) ? 0 : keydates.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SentenceKeyDateType other = (SentenceKeyDateType) obj;
        if (keydates == null) {
            if (other.keydates != null) {
				return false;
			}
        } else if (!keydates.equals(other.keydates)) {
			return false;
		}
        if (supervisionId == null) {
            if (other.supervisionId != null) {
				return false;
			}
        } else if (!supervisionId.equals(other.supervisionId)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SentenceKeyDateType [supervisionId=" + supervisionId + ", staffId=" + staffId + ", comment=" + comment + ", reviewRequired=" + reviewRequired
                + ", keydates=" + keydates + ", historyId=" + historyId + ", supervisionSentenceId=" + supervisionSentenceId + ", aggregateGroupIds=" + aggregateGroupIds
                + ", sentenceIds=" + sentenceIds + "]";
    }

}
