package syscon.arbutus.product.services.legal.contract.dto.charge;

import java.io.Serializable;
import java.util.List;

public class ChargesReturnType implements Serializable {

    private static final long serialVersionUID = 6247679520393652626L;
    private List<ChargeType> charges;
    private Long totalSize;

    /**
     * @return the charges
     */
    public List<ChargeType> getCharges() {
        return charges;
    }

    /**
     * @param charges the charges to set
     */
    public void setCharges(List<ChargeType> charges) {
        this.charges = charges;
    }

    /**
     * Gets the value of the totalSize property.
     *
     * @return the total size of search result
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * Sets the value of the totalSize property.
     *
     * @param totalSize the total size of search result
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargesReturnType [getCharges()=" + getCharges() + ", getTotalSize()=" + getTotalSize() + "]";
    }

}
