package syscon.arbutus.product.services.legal.realization.persistence.caseactivity;

import javax.persistence.*;

import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * ChargeAssociationHistEntity for Case Activity Service
 *
 * @author lhan
 * @version 1.0
 */
//@Audited
@Entity
@DiscriminatorValue("ChargeAssocHist")
public class CAChargeAssociationHistEntity extends CaseActivityStaticAssocHistEntity {

    private static final long serialVersionUID = -7543799976472222173L;

    /**
     * @DbComment LEG_CAStaticAsscHist.CaseActivityHistId 'Foreign key to LEG_CACaseActHist.CaseActivityHistId'
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CaseActivityHistId", nullable = false)
    private CaseActivityHistEntity caseActivity;

    /**
     * Constructor
     */
    public CAChargeAssociationHistEntity() {
        super();
    }

    /**
     * Constructor.
     *
     * @param associationId
     * @param fromIdentifier
     * @param toIdentifier
     * @param stamp
     * @param caseActivity
     */
    public CAChargeAssociationHistEntity(Long associationId, Long fromIdentifier, Long toIdentifier, StampEntity stamp, CaseActivityHistEntity caseActivity) {
        super(associationId, fromIdentifier, toIdentifier, stamp);
        this.caseActivity = caseActivity;
    }

    /**
     * @param caseActivityHist the caseActivityHist to set
     */
    public void setCaseActivityHist(CaseActivityHistEntity caseActivity) {
        this.caseActivity = caseActivity;
    }

    /**
     * @return the caseActivityHist
     */
    public CaseActivityHistEntity getCaseActivity() {
        return caseActivity;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
	 */
    @Override
    public String toString() {
        return "ChargeAssociationEntity [getAssociationId()=" + getAssociationId() + ", getFromIdentifier()=" + getFromIdentifier() + ", getToIdentifier()="
                + getToIdentifier() + ", getStamp()=" + getStamp() + "]";
    }

}