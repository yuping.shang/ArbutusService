package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * DispositionEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdDispos 'The Disposition table for Order Sentence module of Legal service'
 * @since December 21, 2012
 */
@Audited
@Entity
@Table(name = "LEG_OrdDispos")
public class DispositionEntity implements Serializable {

    private static final long serialVersionUID = -6427128006708996477L;

    /**
     * @DbComment dispositionId 'Unique identification of a disposition instance.'
     */
    @Id
    @Column(name = "dispositionId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDDISPOS_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDDISPOS_ID", sequenceName = "SEQ_LEG_ORDDISPOS_ID", allocationSize = 1)
    private Long dispositionId;

    /**
     * @DbComment dispositionOutcome 'A result or outcome of the disposition.'
     */
    @Column(name = "dispositionOutcome", nullable = false, length = 64)
    @MetaCode(set = MetaSet.ORDER_OUTCOME)
    private String dispositionOutcome;

    /**
     * @DbComment orderStatus 'The status of an order, active or inactive.'
     */
    @Column(name = "orderStatus", nullable = false, length = 64)
    @MetaCode(set = MetaSet.ORDER_STATUS)
    private String orderStatus;

    /**
     * @DbComment dispositionDate 'Date of disposition.'
     */
    @Column(name = "dispositionDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dispositionDate;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * Constructor
     */
    public DispositionEntity() {
        super();
    }

    /**
     * Constructor
     *
     * @param dispositionOutcome CodeType, Required. A result or outcome of the disposition.
     * @param orderStatus        CodeType, Required. The status of an order, active or inactive.
     * @param dispositionDate    Date, Required. Date of disposition.
     * @param stamp              StampEntity, Time stamp.
     */
    public DispositionEntity(String dispositionOutcome, String orderStatus, Date dispositionDate, StampEntity stamp) {
        super();
        this.dispositionOutcome = dispositionOutcome;
        this.orderStatus = orderStatus;
        this.dispositionDate = dispositionDate;
        this.stamp = stamp;
    }

    /**
     * @return the dispositionId
     */
    public Long getDispositionId() {
        return dispositionId;
    }

    /**
     * @param dispositionId the dispositionId to set
     */
    public void setDispositionId(Long dispositionId) {
        this.dispositionId = dispositionId;
    }

    /**
     * @return the dispositionOutcome
     */
    public String getDispositionOutcome() {
        return dispositionOutcome;
    }

    /**
     * @param dispositionOutcome the dispositionOutcome to set
     */
    public void setDispositionOutcome(String dispositionOutcome) {
        this.dispositionOutcome = dispositionOutcome;
    }

    /**
     * @return the orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }

    /**
     * @param orderStatus the orderStatus to set
     */
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * @return the dispositionDate
     */
    public Date getDispositionDate() {
        return dispositionDate;
    }

    /**
     * @param dispositionDate the dispositionDate to set
     */
    public void setDispositionDate(Date dispositionDate) {
        this.dispositionDate = dispositionDate;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dispositionDate == null) ? 0 : dispositionDate.hashCode());
        result = prime * result + ((dispositionId == null) ? 0 : dispositionId.hashCode());
        result = prime * result + ((dispositionOutcome == null) ? 0 : dispositionOutcome.hashCode());
        result = prime * result + ((orderStatus == null) ? 0 : orderStatus.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        DispositionEntity other = (DispositionEntity) obj;
        if (dispositionDate == null) {
            if (other.dispositionDate != null) {
				return false;
			}
        } else if (!dispositionDate.equals(other.dispositionDate)) {
			return false;
		}
        if (dispositionId == null) {
            if (other.dispositionId != null) {
				return false;
			}
        } else if (!dispositionId.equals(other.dispositionId)) {
			return false;
		}
        if (dispositionOutcome == null) {
            if (other.dispositionOutcome != null) {
				return false;
			}
        } else if (!dispositionOutcome.equals(other.dispositionOutcome)) {
			return false;
		}
        if (orderStatus == null) {
            if (other.orderStatus != null) {
				return false;
			}
        } else if (!orderStatus.equals(other.orderStatus)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DispositionEntity [dispositionId=" + dispositionId + ", dispositionOutcome=" + dispositionOutcome + ", orderStatus=" + orderStatus + ", dispositionDate="
                + dispositionDate + "]";
    }

}
