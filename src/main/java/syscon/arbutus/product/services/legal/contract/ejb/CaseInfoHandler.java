package syscon.arbutus.product.services.legal.contract.ejb;

import javax.ejb.SessionContext;
import java.math.BigDecimal;
import java.util.*;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.*;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.common.adapters.DataSecurityServiceAdapter;
import syscon.arbutus.product.services.core.common.adapters.LegalServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.exception.*;
import syscon.arbutus.product.services.datasecurity.contract.dto.DataSecurityRecordType;
import syscon.arbutus.product.services.datasecurity.contract.dto.DataSecurityRelType;
import syscon.arbutus.product.services.datasecurity.contract.dto.ReferenceSet.DataSecurityRecordTypeEnum;
import syscon.arbutus.product.services.datasecurity.contract.dto.ReferenceSet.EntityTypeEnum;
import syscon.arbutus.product.services.datasecurity.contract.ejb.DataSecurityHelper;
import syscon.arbutus.product.services.legal.contract.dto.LegalModule;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivityType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.*;
import syscon.arbutus.product.services.legal.contract.dto.valueholder.ConversionUtils;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.CaseActivityEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseaffiliation.CaseAffiliationEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.*;
import syscon.arbutus.product.services.legal.realization.persistence.charge.ChargeEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.OrderEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.SentenceEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * CaseInfoHandler is handling case information related functions.
 *
 * @author lhan
 */
public class CaseInfoHandler {

    private static Logger log = LoggerFactory.getLogger(CaseInfoHandler.class);

    private SessionContext context;
    private Session session;
    private int searchMaxLimit = 200;

    public CaseInfoHandler(SessionContext context, Session session) {
        super();
        this.context = context;
        this.session = session;
    }

    public CaseInfoHandler(SessionContext context, Session session, int searchMaxLimit) {
        super();
        this.context = context;
        this.session = session;
        this.searchMaxLimit = searchMaxLimit;
    }

    private static Set<CaseInfoModuleAssociationEntity> saveCaseAssociations(List<DataSecurityRecordType> relatedSealed, CaseInfoEntity entity) {
        Set<CaseInfoModuleAssociationEntity> moduleAssociations = new HashSet<CaseInfoModuleAssociationEntity>();
        for (DataSecurityRecordType rel : relatedSealed) {

            if (rel.getEntityType().equals(EntityTypeEnum.CONDITION.code())) {
                CaseInfoModuleAssociationEntity asscEntity = new CaseInfoModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.CONDITION.value());
                asscEntity.setToIdentifier(rel.getEntityId());
                asscEntity.setCaseInfo(entity);
                moduleAssociations.add(asscEntity);
            }

            if (rel.getEntityType().equals(EntityTypeEnum.CHARGE.code())) {
                CaseInfoModuleAssociationEntity asscEntity = new CaseInfoModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.CHARGE.value());
                asscEntity.setToIdentifier(rel.getEntityId());
                asscEntity.setCaseInfo(entity);
                moduleAssociations.add(asscEntity);
            }

            if (rel.getEntityType().equals(EntityTypeEnum.ORDER.code()) || rel.getEntityType().equals(EntityTypeEnum.SENTENCE.code())) {
                CaseInfoModuleAssociationEntity asscEntity = new CaseInfoModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.ORDER_SENTENCE.value());
                asscEntity.setToIdentifier(rel.getEntityId());
                asscEntity.setCaseInfo(entity);
                moduleAssociations.add(asscEntity);
            }

            if (rel.getEntityType().equals(EntityTypeEnum.CASEACTIVITY.code())) {
                CaseInfoModuleAssociationEntity asscEntity = new CaseInfoModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.CASE_ACTIVITY.value());
                asscEntity.setToIdentifier(rel.getEntityId());
                asscEntity.setCaseInfo(entity);
                moduleAssociations.add(asscEntity);
            }
        }

        return moduleAssociations;
    }

    private static Set<CaseInfoModuleAssociationEntity> getReverseOrderAssociations(List<DataSecurityRecordType> relatedSealed, CaseInfoEntity entity) {
        Set<CaseInfoModuleAssociationEntity> moduleAssociations = new HashSet<CaseInfoModuleAssociationEntity>();
        for (DataSecurityRecordType rel : relatedSealed) {

            if (rel.getParentEntityType().equals(EntityTypeEnum.SUPERVISION.code()) && rel.getEntityType().equals(EntityTypeEnum.CASE.code())) {
                CaseInfoModuleAssociationEntity asscEntity = new CaseInfoModuleAssociationEntity();
                asscEntity.setToClass("Supervision");
                asscEntity.setToIdentifier(rel.getParentEntityId());
                asscEntity.setCaseInfo(entity);
                moduleAssociations.add(asscEntity);
            }

        }

        return moduleAssociations;
    }

    public CaseInfoType createCaseInfo(UserContext uc, CaseInfoType caseInfo, Boolean duplicatedCheck) {

        CaseInfoType ret = new CaseInfoType();

        ValidationHelper.validate(caseInfo);

        CaseInfoEntity entity = CaseInfoHelper.toCaseInfoEntity(caseInfo);

        //Check if all reference codes are activated, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(entity, true);

        //CreatedDate is set to the current system date by system.
        entity.setCreatedDate(new Date());

        //Handle CaseIdentifier creatation logic when creating CaseInfo.
        if (entity.getCaseIdentifiers() != null && entity.getCaseIdentifiers().size() > 0) {
            generateCaseIdentifierValues(entity.getCaseIdentifiers());
        }

        // Handle duplicatedCheck.
        if (Boolean.TRUE.equals(duplicatedCheck)) {
            checkDuplicatedCaseEntity(uc, entity);
        }

        // Create stamp
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        updateStamp(entity, createStamp, null);

        // Save the entity and it's cascade.all mapped entities.
        session.persist(entity);

        if (!BeanHelper.isEmpty(entity.isActive()) || !BeanHelper.isEmpty(entity.getCaseStatusReason())) {
            saveStatusChangeHistory(entity, createStamp);
        }

        ret = CaseInfoHelper.toCaseInfoType(entity);

        return ret;
    }

    /**
     * Update CaseInfo.
     *
     * @param uc
     * @param caseInfo
     * @param duplicatedCheck
     * @return CaseInfoType
     */
    public CaseInfoType updateCaseInfo(UserContext uc, CaseInfoType caseInfo, Boolean duplicatedCheck) {
        String functionName = "createCaseInfo";

        CaseInfoType ret = new CaseInfoType();

        // Validation
        if (caseInfo == null || caseInfo.getCaseInfoId() == null) {

            String message = String.format("%s, CaseInfoType=%s.", Constants.DTO_VALIDATION, caseInfo);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        ValidationHelper.validate(caseInfo);

        CaseInfoEntity entity = CaseInfoHelper.toCaseInfoEntity(caseInfo);

        //Check if all reference codes are activated, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(entity, false);

        Long caseInfoId = entity.getCaseInfoId();

        CaseInfoEntity existEntity = getCaseInfo(caseInfoId);

        //handle duplicatedCheck.
        if (Boolean.TRUE.equals(duplicatedCheck)) {
            checkDuplicatedCaseEntity(uc, entity);
        }

        if (needToUpdate(entity, existEntity)) {

            StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);

            //Identify if case status data changed.
            boolean isStatusChanged = isStatusChanged(entity, existEntity);

            // Create the history entity
            CaseInfoHistEntity historyEntity = CaseInfoHelper.toCaseInfoHistEntity(existEntity, createStamp);
            session.save(historyEntity);

            // Business logic to maintain the list of case identifier entity in the case entity.
            reOrgCaseIdentifiers(existEntity, entity);

            // Update existing entity which is in current session.
            CaseInfoHelper.updateExistCaseInfoEntity(existEntity, entity);

            // Update stamp
            StampEntity modifyStamp = BeanHelper.getModifyStamp(uc, context, existEntity.getStamp());
            updateStamp(existEntity, createStamp, modifyStamp);

            // Update the exist entity and it's cascade.all mapped entities.
            session.update(existEntity);

            //If case status data is changed, save status change to history.
            if (isStatusChanged) {
                saveStatusChangeHistory(existEntity, createStamp);
            }
        }

        //Flush changes to db
        session.flush();
        session.clear();

        entity = getCaseInfo(caseInfoId);

        ret = CaseInfoHelper.toCaseInfoType(entity);

        return ret;
    }

    public CaseInfoType getCaseInfo(UserContext uc, Long caseInfoId) {

        String functionName = "createCaseInfo";

        CaseInfoType ret = new CaseInfoType();

        // Validation
        if (caseInfoId == null) {

            String message = String.format("CaseInfoId= %s.", caseInfoId);
            LogHelper.error(log, functionName, message);

            throw new InvalidInputException(message);
        }

        CaseInfoEntity entity = getCaseInfo(caseInfoId);

        ret = CaseInfoHelper.toCaseInfoType(entity);

        return ret;
    }

    public CaseIdentifierConfigType createCaseIdentifierConfig(UserContext uc, CaseIdentifierConfigType caseIdentifierConfig) {

        String functionName = "createCaseIdentifierConfig";

        CaseIdentifierConfigType ret = new CaseIdentifierConfigType();

        // Validation
        if (caseIdentifierConfig == null) {

            String message = String.format("%s, CaseIdentifierConfigType=%s.", Constants.DTO_VALIDATION, caseIdentifierConfig);
            LogHelper.error(log, functionName, message);

            throw new InvalidInputException(message);
        }

        //Validate by annotation way.
        ValidationHelper.validate(caseIdentifierConfig);

        //Verify business logic
        verifyCaseIdentifierConfig(caseIdentifierConfig, true);

        CaseIdentifierConfigEntity entity = CaseInfoHelper.toCaseIdentifierConfigEntity(caseIdentifierConfig);

        //check if all reference codes are activated, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(entity, true);

        // Create stamp
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        entity.setStamp(createStamp);

        session.persist(entity);

        ret = CaseInfoHelper.toCaseIdentifierConfigType(entity);

        return ret;
    }

    public CaseIdentifierConfigType updateCaseIdentifierConfig(UserContext uc, CaseIdentifierConfigType caseIdentifierConfig) {

        String functionName = "updateCaseIdentifierConfig";

        CaseIdentifierConfigType ret = new CaseIdentifierConfigType();

        // Validation, the getIdentifierConfigId cannot be null for update
        if (caseIdentifierConfig == null || BeanHelper.isEmpty(caseIdentifierConfig.getIdentifierConfigId())) {

            String message = String.format("%s, CaseIdentifierConfigType=%s.", Constants.DTO_VALIDATION, caseIdentifierConfig);
            LogHelper.error(log, functionName, message);

            throw new InvalidInputException(message);
        }

        Long caseIndentifierConfigId = caseIdentifierConfig.getIdentifierConfigId();

        ValidationHelper.validate(caseIdentifierConfig);

        //Verify business logic
        verifyCaseIdentifierConfig(caseIdentifierConfig, false);

        CaseIdentifierConfigEntity entityInDB = getCaseIdentifierConfig(caseIndentifierConfigId);

        CaseIdentifierConfigEntity newEntity = CaseInfoHelper.toCaseIdentifierConfigEntity(caseIdentifierConfig);

        //check if all reference codes are activated, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(newEntity, false);

        // Create stamp
        StampEntity createStamp = BeanHelper.getModifyStamp(uc, context, entityInDB.getStamp());
        entityInDB.setStamp(createStamp);
        entityInDB = CaseInfoHelper.updateExistCaseIdentiferConfigEntity(entityInDB, newEntity);

        session.update(entityInDB);
        //Flush changes to db
        session.flush();
        session.clear();

        Long caseIdentifierConfigId = entityInDB.getIdentifierConfigId();

        newEntity = getCaseIdentifierConfig(caseIdentifierConfigId);

        ret = CaseInfoHelper.toCaseIdentifierConfigType(newEntity);

        return ret;
    }

    public List<CaseIdentifierConfigType> searchCaseIdentifierConfig(UserContext uc, CaseIdentifierConfigSearchType caseIdentifierConfigSearch) {

        ValidationHelper.validateSearchType(caseIdentifierConfigSearch);

        List<CaseIdentifierConfigType> ret = new ArrayList<CaseIdentifierConfigType>();

        List<CaseIdentifierConfigEntity> list = searchConfigEntity(caseIdentifierConfigSearch);

        ret = CaseInfoHelper.toCaseIdentifierConfigTypeSet(list);

        return ret;
    }

    public CaseInfosReturnType searchCaseInfo(UserContext uc, CaseInfoSearchType search, Boolean searchHistory, Long startIndex, Long resultSize, String resultOrder) {

        ValidationHelper.validateSearchType(search);

        List<CaseInfoType> ret = new ArrayList<CaseInfoType>();

        if (resultSize == null || resultSize > searchMaxLimit) {
            resultSize = new Long(searchMaxLimit);
        }

        Criteria criteria = generatePagingSearchCaseCriteria(uc, search, null);

        //totalSize = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
        Long totalSize = (Long) criteria.setProjection(Projections.countDistinct("caseInfoId")).uniqueResult();

        criteria.setProjection(null);
        //criteria.setResultTransformer(Criteria.ROOT_ENTITY);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());
            criteria.setMaxResults(resultSize.intValue());
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                criteria.addOrder(ord);
            }
        }

        @SuppressWarnings("unchecked") List<CaseInfoEntity> entities = criteria.list();

        Set<CaseInfoEntity> types = new HashSet<CaseInfoEntity>(entities);

        ret = CaseInfoHelper.toCaseInfoTypeList(types);

        CaseInfosReturnType rtn = new CaseInfosReturnType();
        rtn.setCaseInfos(ret);
        rtn.setTotalSize(totalSize);
        return rtn;
    }

    @Deprecated
    public List<CaseInfoType> searchCaseInfo(UserContext uc, Set<Long> subsetSearch, CaseInfoSearchType search, Boolean searchHistory) {
        String functionName = "searchCaseInfo";

        List<CaseInfoType> ret = new ArrayList<CaseInfoType>();

        // Validation
        if (search == null || !search.isWellFormed() || !search.isValid()) {

            String message = String.format("CaseInfoSearchType= %s.", search);
            LogHelper.error(log, functionName, message);

            throw new InvalidInputException(message);
        }

        ValidationHelper.validate(search);

        List<CaseInfoEntity> entities = searchCaseInfoEntity(uc, search, subsetSearch, searchHistory);

        Set<CaseInfoEntity> types = new HashSet<CaseInfoEntity>(entities);

        ret = CaseInfoHelper.toCaseInfoTypeList(types);

        return ret;
    }

    public List<CaseInfoType> retrieveCaseInfos(UserContext uc, Long supervisionId, Boolean isActive) {
        String functionName = "retrieveCaseInfos";

        List<CaseInfoType> ret = new ArrayList<CaseInfoType>();

        // Validation
        if (supervisionId == null) {

            String message = String.format("SupervisionId= %s.", supervisionId);
            LogHelper.error(log, functionName, message);

            throw new InvalidInputException(message);
        }

        CaseInfoSearchType search = new CaseInfoSearchType();
        search.setSupervisionId(supervisionId);
        search.setActive(isActive);

        List<CaseInfoEntity> entities = searchCaseInfoEntity(uc, search, null, false);

        Set<CaseInfoEntity> types = new HashSet<CaseInfoEntity>(entities);

        ret = CaseInfoHelper.toCaseInfoTypeList(types);

        return ret;
    }

    public List<CaseInfoType> retrieveCaseInfoHistory(UserContext uc, Long caseInfoId, Date historyDateFrom, Date historyDateTo) {
        String functionName = "retrieveCaseInfoHistory";

        List<CaseInfoType> ret = new ArrayList<CaseInfoType>();

        // Validation
        if (caseInfoId == null) {

            String message = String.format("CaseInfoId= %s.", caseInfoId);
            LogHelper.error(log, functionName, message);

            throw new InvalidInputException(message);
        }

        ret = getHistory(uc, caseInfoId, historyDateFrom, historyDateTo);

        return ret;
    }

    public Long deleteCaseInfo(UserContext uc, Long caseInfoId) throws DataExistException {
        String functionName = "deleteCaseInfo";

        Long ret = ReturnCode.Success.returnCode();
        ;

        // Validation
        if (caseInfoId == null) {

            String message = String.format("CaseInfoId= %s.", caseInfoId);
            LogHelper.error(log, functionName, message);

            throw new InvalidInputException(message);
        }

        CaseInfoEntity entity = getCaseInfo(caseInfoId);

        isCaseInfoDeletable(entity);

        //Soft deletion.
        cascadeSoftDeleteCase(entity);

        return ret;
    }

    /////////////////////////////////////Private methods//////////////////////////////////////////

    public Long deleteCaseIdentifier(UserContext uc, CaseIdentifierType caseIdentifier) {
        String functionName = "deleteCaseIdentifier";

        Long ret = null;

        // Validation
        if (caseIdentifier == null) {

            String message = String.format("CaseIdentifier= %s.", caseIdentifier);
            LogHelper.error(log, functionName, message);

            throw new InvalidInputException(message);
        }

        ValidationHelper.validate(caseIdentifier);

        CaseIdentifierEntity entity = (CaseIdentifierEntity) session.get(CaseIdentifierEntity.class, caseIdentifier);

        session.delete(entity);

        ret = ReturnCode.Success.returnCode();

        return ret;
    }

    public List<CaseStatusChangeType> retrieveCaseStatusChangeHistory(UserContext uc, Long caseInfoId, Date historyDateFrom, Date historyDateTo) {

        List<CaseStatusChangeType> ret = new ArrayList<CaseStatusChangeType>();

        // Validation
        if (caseInfoId == null) {

            String message = String.format("CaseInfoId= %s.", caseInfoId);
            LogHelper.error(log, "retrieveCaseStatusChangeHistory", message);
            throw new InvalidInputException(message);
        }

        ret = getCaseStatusChangeHistory(uc, caseInfoId, historyDateFrom, historyDateTo);

        return ret;
    }

    private void saveStatusChangeHistory(CaseInfoEntity caseEntity, StampEntity createStamp) {
        CaseStatusChangeHistEntity statusChangeHistoryEntity = new CaseStatusChangeHistEntity();

        statusChangeHistoryEntity.setCaseInfo(caseEntity);
        statusChangeHistoryEntity.setCaseStatus(caseEntity.isActive());
        statusChangeHistoryEntity.setCaseStatusChangeDate(new Date());
        statusChangeHistoryEntity.setCaseStatusChangeReason(caseEntity.getCaseStatusReason());
        statusChangeHistoryEntity.setComment(caseEntity.getComment());
        statusChangeHistoryEntity.setStamp(createStamp);

        session.save(statusChangeHistoryEntity);
    }

    private boolean isStatusChanged(CaseInfoEntity entity, CaseInfoEntity existEntity) {

        if (entity.isActive() == null) {
            if (existEntity.isActive() != null) {
                return true;
            }
        } else if (!entity.isActive().equals(existEntity.isActive())) {
            return true;
        }

        if (entity.getCaseStatusReason() == null) {
            if (existEntity.getCaseStatusReason() != null) {
                return true;
            }
        } else if (!entity.getCaseStatusReason().equals(existEntity.getCaseStatusReason())) {
            return true;
        }

        return false;
    }

    private List<CaseStatusChangeType> getCaseStatusChangeHistory(UserContext uc, Long caseInfoId, Date historyDateFrom, Date historyDateTo) {

        List<CaseStatusChangeType> rtn;

        Criteria criteria = session.createCriteria(CaseStatusChangeHistEntity.class, "caseStgChgHist");
        criteria.add(Restrictions.eq("caseInfo.caseInfoId", caseInfoId)).addOrder(Order.desc("caseStatusChangeDate"));
        if (!BeanHelper.isEmpty(historyDateFrom)) {
            criteria.add(Restrictions.ge("caseStgChgHist.caseStatusChangeDate", historyDateFrom));
        }
        if (!BeanHelper.isEmpty(historyDateTo)) {
            criteria.add(Restrictions.le("caseStgChgHist.caseStatusChangeDate", historyDateTo));
        }

        @SuppressWarnings("unchecked") List<CaseStatusChangeHistEntity> list = criteria.list();

        rtn = CaseInfoHelper.toCaseStatusChangeTypeList(list);

        return rtn;
    }

    /**
     * Cascade soft deletes the case info, also including it's case identifiers, case events and case affiliations.
     *
     * @param entity
     */
    private void cascadeSoftDeleteCase(CaseInfoEntity entity) {
        //Cascade delete case identifiers.
        Set<CaseIdentifierEntity> identitifers = entity.getCaseIdentifiers();
        for (CaseIdentifierEntity identifierEntity : identitifers) {
            identifierEntity.setFlag(DataFlag.DELETE.value());
        }

        //Cascade delete case activities.
        Set<CaseActivityEntity> caseActivities = entity.getCaseActivities();
        for (CaseActivityEntity caseActivityEntity : caseActivities) {
            caseActivityEntity.setFlag(DataFlag.DELETE.value());
            session.update(caseActivityEntity);
        }

        //Cascade delete case affiliations.
        String queryString = "from CaseAffiliationEntity where affiliatedCaseId =  :caseId";
        Query query = session.createQuery(queryString);
        query.setParameter("caseId", entity.getCaseInfoId());
        @SuppressWarnings("unchecked") List<CaseAffiliationEntity> caseAffiliations = query.list();
        for (CaseAffiliationEntity caseAffiliationEntity : caseAffiliations) {
            caseAffiliationEntity.setFlag(DataFlag.DELETE.value());
            session.update(caseAffiliationEntity);
        }

        //Delete case info itself.
        entity.setFlag(DataFlag.DELETE.value());
        session.update(entity);
    }

    /**
     * Check if the case info entity can be soft deleted.
     * 1, A case cannot be deleted if there are still active charges.
     * 2, Case need to check for any associated entities such as orders, sentences, conditions etc.
     * If they exist the case should not be deleted.
     *
     * @param entity
     */
    private void isCaseInfoDeletable(CaseInfoEntity entity) throws DataExistException {
        Set<CaseInfoModuleAssociationEntity> moduleAssociations = entity.getModuleAssociations();
        for (CaseInfoModuleAssociationEntity assoc : moduleAssociations) {
            //verify order
            if (assoc.getToClass().equalsIgnoreCase(LegalModule.ORDER_SENTENCE.value()) && assoc.getToIdentifier() != null) {
                throw new DataExistException(ErrorCodes.LEG_DELETE_CASE_ORDER_EXIST);
            }

            //verify condition
            if (assoc.getToClass().equalsIgnoreCase(LegalModule.CONDITION.value()) && assoc.getToIdentifier() != null) {
                throw new DataExistException(ErrorCodes.LEG_DELETE_CASE_CONDITION_EXIST);
            }
        }

        //Verify charge
        String queryString = "select count(chargeId) from ChargeEntity where caseInfoId = :caseId";
        Query query = session.createQuery(queryString).setParameter("caseId", entity.getCaseInfoId());
        Long count = (Long) query.uniqueResult();
        if (count > 0) {
            throw new DataExistException(ErrorCodes.LEG_DELETE_CASE_ACTIVE_CHARGE_EXIST);
        }
    }

    /**
     * Check duplicated CaseInfoEntity in DB by the given CaseInfoEntity.
     *
     * @param entity
     */
    private void checkDuplicatedCaseEntity(UserContext uc, CaseInfoEntity entity) {
        Long supervisionId = entity.getSupervisionId();
        Long facilityId = entity.getFacilityId();

        CaseInfoSearchType caseInfoSearch = new CaseInfoSearchType();
        caseInfoSearch.setFacilityId(facilityId);
        caseInfoSearch.setSupervisionId(supervisionId);

        Set<CaseIdentifierEntity> identifierEntities = entity.getCaseIdentifiers();

        if (identifierEntities != null && identifierEntities.size() > 0) {

            checkDuplicatedIdentifier(identifierEntities);

            checkDuplicatedCaseEntityWithIdentifier(uc, identifierEntities, caseInfoSearch);

        } else {

            checkDuplicatedCaseEntityWithoutIdentifier(uc, caseInfoSearch);

        }
    }

    /**
     * Check duplicated CaseInfoEntity without combining Identifier type/value.
     *
     * @param caseInfoSearch
     */
    private void checkDuplicatedCaseEntityWithoutIdentifier(UserContext uc, CaseInfoSearchType caseInfoSearch) {

        List<CaseInfoEntity> caseInfoList = searchCaseInfoEntity(uc, caseInfoSearch, null, false);
        if (caseInfoList != null && caseInfoList.size() > 0) {
            throw new DataExistException(String.format("Duplicated Case Info exists for the combined uniqueness constrataints: SupervisionId: %s and FacilityId: %s.",
                    caseInfoSearch.getSupervisionId(), caseInfoSearch.getFacilityId()));
        }
    }

    /**
     * Check duplicated CaseInfoEntity with combining each Identifier type/value.
     *
     * @param identifierEntities
     * @param caseInfoSearch
     */
    private void checkDuplicatedCaseEntityWithIdentifier(UserContext uc, Set<CaseIdentifierEntity> identifierEntities, CaseInfoSearchType caseInfoSearch) {

        Map<String, Boolean> needDuplicatCheckMap = new HashMap<String, Boolean>();

        for (CaseIdentifierEntity identifierEntity : identifierEntities) {
            String identifierType = identifierEntity.getIdentifierType();
            ;
            String identifierValue = null;

            if (needDuplicatCheckMap.get(identifierType) == null) {
                needDuplicatCheckMap.put(identifierType, getParticipateDuplicateCheckFlag(identifierType));
            }

            if (needDuplicatCheckMap.get(identifierType)) {
                identifierValue = identifierEntity.getIdentifierValue();

                CaseIdentifierSearchType caseIdentifierSearch = new CaseIdentifierSearchType();
                caseIdentifierSearch.setIdentifierType(identifierType);
                caseIdentifierSearch.setIdentifierValue(identifierValue);

                caseInfoSearch.setCaseIdentifierSearch(caseIdentifierSearch);
            } else {
                identifierType = identifierValue = null;
                caseInfoSearch.setCaseIdentifierSearch(null);
            }

            List<CaseInfoEntity> caseInfoList = searchCaseInfoEntity(uc, caseInfoSearch, null, false);

            if (caseInfoList != null && caseInfoList.size() > 0) {
                throw new DataExistException(String.format(
                        "Duplicated CaseInfo exists for the combined uniqueness constrataints: SupervisionId: %s, Case Identifier Type: %s, Case Identifier Value: %s and FacilityId: %s.",
                        caseInfoSearch.getSupervisionId(), identifierType, identifierValue, caseInfoSearch.getFacilityId()));
            }
        }
    }

    /**
     * Get ParticipateDuplicateCheck Flag by a given identifierType.
     *
     * @param identifierType
     * @return boolean
     */
    private boolean getParticipateDuplicateCheckFlag(String identifierType) {

        CaseIdentifierConfigEntity config = getCaseIdentifierConfigEntity(identifierType);

        if (config != null) {
            return config.isDuplicateCheck();
        } else {
            return false;
        }
    }

    /**
     * Get the active CaseIdentifierConfigEntity by identifierType
     *
     * @param identifierType
     * @return CaseIdentifierConfigEntity
     */
    private CaseIdentifierConfigEntity getCaseIdentifierConfigEntity(String identifierType) {
        CaseIdentifierConfigEntity rtn = null;

        CaseIdentifierConfigSearchType searchType = new CaseIdentifierConfigSearchType();
        searchType.setIdentifierType(identifierType);
        searchType.setActive(true);

        List<CaseIdentifierConfigEntity> list = searchConfigEntity(searchType);

        //The list should always have one element.
        if (list != null && list.size() == 1) {
            rtn = list.get(0);
        }

        return rtn;
    }

    /**
     * Check DuplicatedIdentifier by CaseIdentifier Type/Value pair in a given set of CaseIdentifierEntity.
     *
     * @param identifierEntities
     */
    private void checkDuplicatedIdentifier(Set<CaseIdentifierEntity> identifierEntities) {

        if (identifierEntities == null || identifierEntities.size() <= 1) {
            return;
        }

        Set<CaseIdentifierTypeValue> identifierTypeValueSet = new HashSet<CaseIdentifierTypeValue>();

        for (CaseIdentifierEntity identifierEntity : identifierEntities) {
            CaseIdentifierTypeValue typeValue = new CaseIdentifierTypeValue(identifierEntity.getIdentifierType(), identifierEntity.getIdentifierValue());

            identifierTypeValueSet.add(typeValue);
        }

        // HashSet does not add duplicated CaseIdentifierTypeValue.
        if (identifierTypeValueSet.size() < identifierEntities.size()) {
            throw new DataExistException("Duplicated identifier type/value pair exist in the set of CaseInfoType.getCaseIdentifiers().");
        }

    }

    /**
     * Search CaseIdentifierConfigEntity by CaseIdentifierConfigSearchType.
     *
     * @param caseIdentifierConfigSearch
     * @return List<CaseIdentifierConfigEntity>
     */
    private List<CaseIdentifierConfigEntity> searchConfigEntity(CaseIdentifierConfigSearchType caseIdentifierConfigSearch) {
        Criteria c = session.createCriteria(CaseIdentifierConfigEntity.class);

        if (caseIdentifierConfigSearch.isActive() != null) {
            c.add(Restrictions.eq("active", caseIdentifierConfigSearch.isActive()));
        }

        if (caseIdentifierConfigSearch.isPrimary() != null) {
            c.add(Restrictions.eq("primary", caseIdentifierConfigSearch.isPrimary()));
        }

        //    	if(caseIdentifierConfigSearch.isAutoGeneration() != null)
        //    		c.add(Restrictions.eq("autoGenerate", caseIdentifierConfigSearch.isAutoGeneration()));
        //
        //    	if(caseIdentifierConfigSearch.getIdentifierFormat() != null)
        //    		c.add(Restrictions.eq("identifierFormat", caseIdentifierConfigSearch.getIdentifierFormat()));

        if (caseIdentifierConfigSearch.getIdentifierType() != null) {
            c.add(Restrictions.eq("identifierType", caseIdentifierConfigSearch.getIdentifierType()));
        }

        @SuppressWarnings("unchecked") List<CaseIdentifierConfigEntity> list = c.list();

        return list;
    }

    /**
     * Generate a criteria for paging search case infos.
     *
     * @param uc
     * @param search
     * @param searchHistory
     * @return
     */
    private Criteria generatePagingSearchCaseCriteria(UserContext uc, CaseInfoSearchType search, Boolean searchHistory) {

        Criteria criteria = createCaseSearchCriteria(search, null, CaseInfoEntity.class);

        CaseIdentifierSearchType identifierSearch = search.getCaseIdentifierSearch();
        if (identifierSearch != null) {
            criteria = addIdentifierSearchCriterion(criteria, identifierSearch);
        }

        //TODO: Search against history entities to get related case activity ids.
        /*if(searchHistory != null && Boolean.TRUE.equals(searchHistory)){

			Set<Long> idsFromHistory = new HashSet<Long>();
			idsFromHistory.addAll(searchHistoryEntity(search, null));

			CaseInfoSearchType secondeSeach = new CaseInfoSearchType();

			if(idsFromHistory != null && idsFromHistory.size() > 0)
				list.addAll(searchCaseInfoEntity(uc,secondeSeach, idsFromHistory, false));
		}*/

        return criteria;

    }

    /**
     * Search CaseInfoEntity by CaseInfoSearchType.
     *
     * @param search
     * @return List<CaseInfoEntity>
     */
    @Deprecated
    private List<CaseInfoEntity> searchCaseInfoEntity(UserContext uc, CaseInfoSearchType search, Set<Long> subsetSearch, Boolean searchHistory) {

        Criteria c = createCaseSearchCriteria(search, subsetSearch, CaseInfoEntity.class);

        CaseIdentifierSearchType identifierSearch = search.getCaseIdentifierSearch();
        if (identifierSearch != null) {
            c = addIdentifierSearchCriterion(c, identifierSearch);
        }

        @SuppressWarnings("unchecked") List<CaseInfoEntity> list = c.list();

        //Search against history entities to get related case activity ids.
        if (searchHistory != null && Boolean.TRUE.equals(searchHistory)) {

            Set<Long> idsFromHistory = new HashSet<Long>();
            idsFromHistory.addAll(searchHistoryEntity(search, subsetSearch));

            CaseInfoSearchType secondeSeach = new CaseInfoSearchType();

            if (idsFromHistory != null && idsFromHistory.size() > 0) {
                list.addAll(searchCaseInfoEntity(uc, secondeSeach, idsFromHistory, false));
            }
        }

        return list;
    }

    private Collection<? extends Long> searchHistoryEntity(CaseInfoSearchType search, Set<Long> subsetSearch) {
        Criteria c = createCaseSearchCriteria(search, subsetSearch, CaseInfoHistEntity.class);

        CaseIdentifierSearchType identifierSearch = search.getCaseIdentifierSearch();
        if (identifierSearch != null) {
            c = addIdentifierSearchCriterion(c, identifierSearch);
        }

        c.setProjection(Projections.property("caseInfoId"));

        @SuppressWarnings("unchecked") List<Long> list = c.list();

        return list;
    }

    /**
     * Create Criteria by CaseInfoSearchType.
     *
     * @param class1
     * @param search
     * @return Criteria
     */
    private Criteria createCaseSearchCriteria(CaseInfoSearchType search, Set<Long> subsetSearch, @SuppressWarnings("rawtypes") Class class1) {
        Criteria c = session.createCriteria(class1);

        c.setMaxResults(searchMaxLimit + 1);
        c.setFetchSize(searchMaxLimit + 1);
        //c.setLockMode(LockMode.NONE);
        c.setCacheMode(CacheMode.IGNORE);

        //sub set ids.
        if (subsetSearch != null && subsetSearch.size() > 0) {
            c.add(Restrictions.in("caseInfoId", subsetSearch));
        }

        //FacilityAssociation
        Long facilityAssociation = search.getFacilityId();
        if (facilityAssociation != null) {
            c.add(Restrictions.eq("facilityId", facilityAssociation));
        }

        //SupervisionAssociation
        Long supervisionAssociation = search.getSupervisionId();
        if (supervisionAssociation != null) {
            c.add(Restrictions.eq("supervisionId", supervisionAssociation));
        }

        String caseCategory = search.getCaseCategory();
        if (caseCategory != null) {
            c.add(Restrictions.eq("caseCategory", caseCategory));
        }

        String caseType = search.getCaseType();
        if (caseType != null) {
            c.add(Restrictions.eq("caseType", caseType));
        }

        String caseStatusReason = search.getReason();
        if (caseStatusReason != null) {
            c.add(Restrictions.eq("caseStatusReason", caseStatusReason));
        }

        String sentenceStatus = search.getSentenceStatus();
        if (sentenceStatus != null) {
            c.add(Restrictions.eq("sentenceStatus", sentenceStatus));
        }

        Boolean active = search.getActive();
        if (active != null) {
            c.add(Restrictions.eq("active", active));
        }

        Boolean diverted = search.getDiverted();
        if (diverted != null) {
            c.add(Restrictions.eq("diverted", diverted));
        }

        Boolean divertedSuccess = search.getDivertedSuccess();
        if (divertedSuccess != null) {
            c.add(Restrictions.eq("divertedSuccess", divertedSuccess));
        }

        //
        Date createdDateFrom = search.getCreatedDateFrom();
        if (createdDateFrom != null) {
            c.add(Restrictions.ge("createdDate", createdDateFrom));
        }
        Date createdDateTo = search.getCreatedDateTo();
        if (createdDateTo != null) {
            c.add(Restrictions.le("createdDate", createdDateTo));
        }

        //
        Date issuedDateFrom = search.getIssuedDateFrom();
        if (issuedDateFrom != null) {
            c.add(Restrictions.ge("issuedDate", issuedDateFrom));
        }
        Date issuedDateTo = search.getIssuedDateTo();
        if (issuedDateTo != null) {
            c.add(Restrictions.le("issuedDate", issuedDateTo));
        }

        return c;
    }

    /**
     * Add Criterions by CaseIdentifierSearchType into Criteria.
     *
     * @param c
     * @param identifierSearch
     * @return Criteria
     */
    private Criteria addIdentifierSearchCriterion(Criteria c, CaseIdentifierSearchType identifierSearch) {
        c.createAlias("caseIdentifiers", "caseIdentifier");

        if (identifierSearch.getIdentifierType() != null && identifierSearch.getIdentifierType() != null) {
            c.add(Restrictions.eq("caseIdentifier.identifierType", identifierSearch.getIdentifierType()));
        }

        if (identifierSearch.getIdentifierValue() != null) {
            c.add(Restrictions.ilike("caseIdentifier.identifierValue", parseWildcard(identifierSearch.getIdentifierValue())));
        }

        if (identifierSearch.isAutoGeneration() != null) {
            c.add(Restrictions.eq("caseIdentifier.autoGenerate", identifierSearch.isAutoGeneration()));
        }

        if (identifierSearch.getOrganizationId() != null && identifierSearch.getOrganizationId() != null) {
            c.add(Restrictions.eq("caseIdentifier.organizationId", identifierSearch.getOrganizationId()));
        }

        if (identifierSearch.getFacilityId() != null && identifierSearch.getFacilityId() != null) {
            c.add(Restrictions.eq("caseIdentifier.identifierValue", identifierSearch.getFacilityId()));
        }

        if (identifierSearch.getIdentifierComment() != null) {
            c.add(Restrictions.ilike("caseIdentifier.identifierComment", parseWildcard(identifierSearch.getIdentifierComment())));
        }

        return c;
    }

    /**
     * Update stamp information for Case Information instance.
     *
     * @param entity      CaseInfoEntity
     * @param createStamp StampEntity
     * @param modifyStamp StampEntity
     */
    private void updateStamp(CaseInfoEntity entity, StampEntity createStamp, StampEntity modifyStamp) {

        //Update stamp for main instance
        if (modifyStamp == null) {
            entity.setStamp(createStamp);
        } else {
            entity.setStamp(modifyStamp);
        }

        //Update stamp for case Identifiers
        if (entity.getCaseIdentifiers() != null) {
            for (CaseIdentifierEntity identifier : entity.getCaseIdentifiers()) {
                if (identifier != null) {
                    if (identifier.getIdentifierId() == null) {
                        identifier.setStamp(createStamp);
                    } else {
                        if (modifyStamp != null) {
                            identifier.setStamp(modifyStamp);
                        }
                    }
                }
            }
        }

    }

    /**
     * Check if a CaseInfo instance exist
     *
     * @param instanceId Long - the unique Id of case information instance
     * @return CaseInfoEntity
     */
    private CaseInfoEntity getCaseInfo(Long instanceId) {

        CaseInfoEntity entity = (CaseInfoEntity) session.get(CaseInfoEntity.class, instanceId);

        if (entity == null) {
            String message = String.format("[getCaseInfo] Could not find the case info instance by caseInfoId = %d.", instanceId);
            throw new DataNotExistException(message);
        }

        return entity;
    }

    /**
     * Get current court activity and all it's history records by given parameters.
     *
     * @param uc
     * @param caseInfoId
     * @param fromHistoryDate
     * @param toHistoryDate
     * @return Set<CourtActivityType>
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private List<CaseInfoType> getHistory(UserContext uc, Long caseInfoId, Date fromHistoryDate, Date toHistoryDate) {

        CaseInfoEntity currentEntity = getCaseInfo(caseInfoId);

        //Set<AssociationEntity> associations = currentEntity.getAssociations();

        List<CaseInfoType> rtn;

        Criteria criteria = session.createCriteria(CaseInfoEntity.class, "caseInfo");
        criteria.add(Restrictions.eq("caseInfoId", caseInfoId));
        if (!BeanHelper.isEmpty(fromHistoryDate)) {
            criteria.add(Restrictions.ge("stamp.modifyDateTime", fromHistoryDate));
        }
        if (!BeanHelper.isEmpty(toHistoryDate)) {
            criteria.add(Restrictions.le("stamp.modifyDateTime", toHistoryDate));
        }

        List currentList = criteria.list();

        if (currentList.isEmpty()) {
            currentEntity = null;
        }

        Criteria criteria1 = session.createCriteria(CaseInfoHistEntity.class, "caseInfoHist");
        criteria1.add(Restrictions.eq("caseInfoId", caseInfoId));
        if (!BeanHelper.isEmpty(fromHistoryDate)) {
            criteria1.add(Restrictions.ge("stamp.modifyDateTime", fromHistoryDate));
        }
        if (!BeanHelper.isEmpty(toHistoryDate)) {
            criteria1.add(Restrictions.le("stamp.modifyDateTime", toHistoryDate));
        }

        List<CaseInfoHistEntity> historylist = criteria1.list();

        Set<CaseInfoHistEntity> histories = new HashSet<CaseInfoHistEntity>(historylist);

        rtn = CaseInfoHelper.toCaseInfoTypeSet(histories, currentEntity);

        return rtn;
    }

    /**
     * Check if a CaseIdentifierConfig instance exist
     *
     * @param instanceId Long - the unique Id of case identifier config instance
     * @return CaseIdentifierConfigEntity
     */
    private CaseIdentifierConfigEntity getCaseIdentifierConfig(Long instanceId) {

        CaseIdentifierConfigEntity entity = (CaseIdentifierConfigEntity) session.get(CaseIdentifierConfigEntity.class, instanceId);

        if (entity == null) {
            String message = String.format("[getCaseIdentifierConfig] Could not find the case identifier config instance by caseIdentifierConfigId = %d.", instanceId);
            throw new DataNotExistException(message);
        }

        return entity;

    }

    /**
     * Verify case identifier configuration, the configuration rule as below:
     * Atmost 2 case identifiers can be flagged as primary
     * Multiple configuration rules for the same CaseIdentifierType may be created, however only 1 configuration rule per CaseIdentifierType can be active at any time.
     *
     * @param dto     CaseIdentifierConfigType
     */
    @SuppressWarnings("unchecked")
    private void verifyCaseIdentifierConfig(CaseIdentifierConfigType dto, boolean isCreate) {

        String functionName = "verifyCaseIdentifierConfig";

        //Atmost 2 case identifiers can be flagged as primary
        Boolean isPrimary = dto.isPrimary();
        Long identifierId = dto.getIdentifierConfigId();
        if (isPrimary != null && isPrimary) {

            Query query = session.createQuery(
                    "select cicEntity.identifierConfigId from CaseIdentifierConfigEntity cicEntity " + "where cicEntity.primary = :primaryFlag ");
            query.setParameter("primaryFlag", true);
            List<Long> identifierIds = query.list();
            //Long count = (Long)query.uniqueResult();

            if (identifierIds != null && identifierIds.size() == 2) {
                if (isCreate || (!isCreate && identifierId != null && !identifierIds.contains(identifierId))) {
                    String message = String.format("[%s] At most 2 case identifiers can be flagged as primary.", functionName);
                    throw new InvalidInputException(message);
                }
            }

        }

        //Multiple configuration rules for the same CaseIdentifierType may be created,
        //however only 1 configuration rule per CaseIdentifierType can be active at any time.
        if (dto.isActive()) {
            String identifierType = dto.getIdentifierType();
            if (!BeanHelper.isEmpty(identifierType)) {
                Query query = session.createQuery("select cicEntity.identifierConfigId from CaseIdentifierConfigEntity cicEntity "
                        + "where cicEntity.active = :activeFlag and cicEntity.identifierType = :caseIdentifierType ");
                query.setParameter("activeFlag", true);
                query.setParameter("caseIdentifierType", identifierType);
                List<Long> identifierIds = query.list();

                if (identifierIds != null && identifierIds.size() == 1) {
                    if (isCreate || (!isCreate && identifierId != null && !identifierIds.contains(identifierId))) {
                        String message = String.format("[%s] only one configuration rule per CaseIdentifierType can be active at any time.", functionName);
                        throw new InvalidInputException(message);
                    }
                }
            }
        }
    }

    /**
     * Check if the new CaseInfoEntity has new value.
     *
     * @param entity
     * @param existEntity
     * @return boolean
     */
    private boolean needToUpdate(CaseInfoEntity entity, CaseInfoEntity existEntity) {
        if (entity.equals(existEntity)) {
            return false;
        }

        return true;
    }

    /**
     * Check and generate identifier value fo each CaseIdentifierEntity in the given set of CaseIdentifierEntity.
     *
     * @param caseIdentifiers
     */
    private void generateCaseIdentifierValues(Set<CaseIdentifierEntity> caseIdentifiers) {
        for (CaseIdentifierEntity identifierEntity : caseIdentifiers) {
            String identifierType = identifierEntity.getIdentifierType();
            String identifierValue = identifierEntity.getIdentifierValue();

            CaseIdentifierConfigEntity configEntity = getCaseIdentifierConfigEntity(identifierType);

            if (configEntity != null && Boolean.TRUE.equals(configEntity.isAutoGenerate()) && Boolean.TRUE.equals(identifierEntity.isAutoGenerate())) {

                identifierEntity.setIdentifierValue(autoGenerateCaseIdentifierValue(configEntity.getIdentifierFormat()));

            } else {

                log.debug(String.format(
                        "CaseIdentifierConfiguration for the identifier type: %s is not configured or the AutoGenerate flag is false in it; use the passed-in identifier value: %s.",
                        identifierType, identifierValue));

            }
        }
    }

    /**
     * Auto generate CaseIdentifierValue.
     *
     * @param format
     * @return String
     */
    private String autoGenerateCaseIdentifierValue(String format) {

        //Generate Identifier digit number by DB.
        Long value = getNextIdentifierNumber();

        // Format the number
        String identifierValue = formatIdentifierNumber(format, value);

        //session.flush();
        //session.clear();

        return identifierValue;
    }

    /**
     * @return
     */
    private Long getNextIdentifierNumber() {

        String dialect = "Oracle";
        SQLQuery query = null;
        Long ret = 0L;

        try {
            Object dp = PropertyUtils.getProperty(session.getSessionFactory(), "dialect");
            if (dp != null) {
                dialect = dp.toString();
            }
        } catch (Exception ex) {
        }

        //
        if (dialect.contains("DB2")) {

            query = session.createSQLQuery("values nextval for Seq_LEG_CIIdentifierNum");
            ret = ((Integer) query.uniqueResult()).longValue();
        } else {
            query = session.createSQLQuery("select Seq_LEG_CIIdentifierNum.nextval from dual");
            ret = ((BigDecimal) query.uniqueResult()).longValue();
        }

        return ret;
    }

    /**
     * @param format
     * @param value
     * @return
     */
    private String formatIdentifierNumber(String format, Long value) {

        //
        int length = format.length();
        String prefix = format.replaceAll("[0-9]", "");
        int prefixLength = prefix.length();
        int numLength = length - prefixLength;
        String num = String.format("%0" + numLength + "d", value);

        //
        char[] formatArray = format.toCharArray();
        char[] numArray = num.toCharArray();
        int position = 0;
        for (int i = 0; i < length; i++) {
            if (Character.isDigit(formatArray[i])) {
                formatArray[i] = numArray[position];
                position++;
            }
        }

        return new String(formatArray);

    }

    /**
     * Parse * to % in the string.
     *
     * @param str
     * @return String
     */
    private String parseWildcard(String str) {
        return str == null || str.length() == 0 ? str : str.matches("^\\*+$") ? "" : str.replaceAll("\\*+", "%");
    }

    /**
     * Re-organize the case identifier entity list against the business logic when update a case primary identifier.
     * 1, Old primary case identifier needs to be copy to the list;
     * 2, If reused the old primary case identifier, needs to remove it from the list.
     *
     * @param existEntity
     * @param entity
     */
    private void reOrgCaseIdentifiers(CaseInfoEntity existEntity, CaseInfoEntity entity) {

        CaseIdentifierEntity oldPrimary = null;
        CaseIdentifierEntity newPrimary = null;

        for (CaseIdentifierEntity entityOld : existEntity.getCaseIdentifiers()) {
            if (entityOld.isPrimary()) {
                oldPrimary = entityOld;
                break;
            }
        }

        for (CaseIdentifierEntity entityNew : entity.getCaseIdentifiers()) {
            if (entityNew.isPrimary()) {
                newPrimary = entityNew;
                break;
            }
        }

        // Business rule1: Old primary case identifier needs to be copy to the list when
        //isSaveAsSecondaryIdentifier is true;
        if (oldPrimary != null && newPrimary != null && !oldPrimary.getIdentifierValue().equals(newPrimary.getIdentifierValue())
                && entity.isSaveAsSecondaryIdentifier()) {
            CaseIdentifierEntity secondIdentifierFromOld = new CaseIdentifierEntity(null, oldPrimary.getIdentifierType(), oldPrimary.getIdentifierValue(),
                    oldPrimary.isAutoGenerate(), oldPrimary.getOrganizationId(), oldPrimary.getFacilityId(), oldPrimary.getIdentifierComment(), false);

            entity.getCaseIdentifiers().add(secondIdentifierFromOld);
        }

        // Business rule2: If re-use the old primary case identifier, needs to remove it from the list.
        Iterator<CaseIdentifierEntity> iterator = entity.getCaseIdentifiers().iterator();
        while (iterator.hasNext()) {
            CaseIdentifierEntity identifierEntity = iterator.next();

            if (newPrimary != null && !identifierEntity.isPrimary() && identifierEntity.getIdentifierType().equals(newPrimary.getIdentifierType())
                    && identifierEntity.getIdentifierValue().equals(newPrimary.getIdentifierValue())) {
                iterator.remove();
            }
        }
    }

    /**
     * @param uc
     * @param caseId           - required
     * @param dataSecurityFlag - required
     *                         Enum of DataFlag.SEAL
     * @param comments         - optional
     *                         Comments for DataSecurity Record
     * @param personId         - required
     *                         PersonId of the person updating status
     * @return
     */
    public Long updateCaseDataSecurityStatus(UserContext uc, Long caseId, Long dataSecurityFlag, String comments, Long personId, String parentEntityType, Long parentId,
            String memnto, Boolean cascade) {

        // id can not be null.
        if (caseId == null || personId == null || dataSecurityFlag == null) {
            String message = "caseId or personId or dataSecurityFlag can not be null";
            throw new InvalidInputException(message);
        }

        Long ret = ReturnCode.UnknownError.returnCode();

        if (dataSecurityFlag.equals(DataFlag.SEAL.value())) {
            ret = sealCase(uc, caseId, comments, personId, parentEntityType, parentId, memnto, cascade);
        } else if (dataSecurityFlag.equals(DataFlag.ACTIVE.value())) {
            ret = UnSealCase(uc, caseId, comments, personId, parentEntityType, parentId, memnto, cascade);
        }

        return ret;

    }

    public Long sealCase(UserContext uc, Long caseId, String comments, Long personId, String parentEntityType, Long parentId, String parentMemnto, Boolean cascade) {

        Long dataSecurityFlag = DataFlag.SEAL.value();

        CaseInfoEntity entity = BeanHelper.getEntity(session, CaseInfoEntity.class, caseId);
        Date recordDate = new Date();
        String recordType = DataSecurityRecordTypeEnum.SEALCASE.code();

        // 1- Set the Status on the ALL Associations of type ORDER and CONDITION
        //    of the Charge
        List<DataSecurityRelType> caseAssociations = getCaseAssociations(uc, entity);

        // 2- Set the Status on the Parent Charge
        if (cascade) {

            for (DataSecurityRelType dsRel : caseAssociations) {

                String assEntityType = dsRel.getEntityType().code();
                String assParentEntityType = EntityTypeEnum.CASE.code();

                // update flag of this Condition
                if (dsRel.getEntityType().equals(EntityTypeEnum.CONDITION)) {
                    LegalServiceAdapter.updateConditionDataSecurityStatus(uc, dsRel.getId(), dataSecurityFlag, comments, personId, EntityTypeEnum.CASE.code(), caseId,
                            dsRel.getMemnto());
                }

                // update flag of this Order
                if (dsRel.getEntityType().equals(EntityTypeEnum.ORDER)) {
                    LegalServiceAdapter.updateOrderDataSecurityStatus(uc, dsRel.getId(), dataSecurityFlag, comments, personId, EntityTypeEnum.CASE.code(), caseId,
                            dsRel.getMemnto(), Boolean.TRUE);
                }

                // update flag of this Order
                if (dsRel.getEntityType().equals(EntityTypeEnum.SENTENCE)) {
                    LegalServiceAdapter.updateSentenceDataSecurityStatus(uc, dsRel.getId(), dataSecurityFlag, comments, personId, EntityTypeEnum.CASE.code(), caseId,
                            dsRel.getMemnto(), Boolean.TRUE);
                }

                // update flag of this Charge
                if (dsRel.getEntityType().equals(EntityTypeEnum.CHARGE)) {
                    LegalServiceAdapter.updateChargeDataSecurityStatus(uc, dsRel.getId(), dataSecurityFlag, comments, personId, EntityTypeEnum.CASE.code(), caseId,
                            dsRel.getMemnto(), Boolean.TRUE);
                }

                // update flag of this Case Activities (Events)
                if (dsRel.getEntityType().equals(EntityTypeEnum.CASEACTIVITY)) {
                    LegalServiceAdapter.updateCaseActivityDataSecurityStatus(uc, dsRel.getId(), dataSecurityFlag, comments, personId, EntityTypeEnum.CASE.code(), caseId,
                            dsRel.getMemnto());
                }

            }

            session.evict(entity);
            entity = BeanHelper.getEntity(session, CaseInfoEntity.class, caseId);
            // Remove this charge link from associated order or condition
            LegalHelper.removeModuleLinks(uc, context, session, caseId, LegalModule.CASE_INFORMATION, entity.getModuleAssociations());
            // Remove links from this entity
            entity.getModuleAssociations().clear();
            session.merge(entity);
            session.flush();

        }

        String entityType = EntityTypeEnum.CASE.code();

        DataSecurityRecordType dsr = new DataSecurityRecordType(null, entity.getCaseInfoId(), entityType, parentId, parentEntityType, recordType, personId, recordDate,
                comments, parentMemnto);
        dsr.setIsOpen(Boolean.TRUE);
        DataSecurityServiceAdapter.createRecord(uc, dsr);
        if (cascade) {
            entity.setFlag(dataSecurityFlag);
        }

        session.merge(entity);
        session.flush();

        return ReturnCode.Success.returnCode();
    }

    public Long UnSealCase(UserContext uc, Long caseId, String comments, Long personId, String parentEntityType, Long parentId, String memento, Boolean cascade) {
        List<DataSecurityRecordType> relatedRecords = DataSecurityServiceAdapter.getRelatedRecords(uc, caseId, EntityTypeEnum.CASE.code());

        DataSecurityRecordType sealedCase = DataSecurityServiceAdapter.getEntity(uc, EntityTypeEnum.CASE.code(), caseId, parentEntityType, parentId);

        Long dataSecurityFlag = DataFlag.ACTIVE.value();

        // 2- Set the Status on the Parent Charge
        String recordType = DataSecurityRecordTypeEnum.UNSEALCASE.code();
        Date recordDate = new Date();

        String chrgeTblName = "LEG_CICaseInfo";
        String sql = "select caseinfoid, flag from " + chrgeTblName + " where caseinfoid = :caseinfoIdParam";
        String usql = "update " + chrgeTblName + " set flag = 1 where caseinfoid = :caseinfoIdParam";
        Map<String, String> paramMap = new HashMap<String, String>();
        Map<String, Type> typesMap = new HashMap<String, Type>();

        // update charge status
        paramMap.put("caseinfoIdParam", String.valueOf(sealedCase.getEntityId()));
        typesMap.put("caseinfoIdParam", LongType.INSTANCE);
        typesMap.put("flag", LongType.INSTANCE);
        List entitys = DataSecurityHelper.executeRawSQLQuery(session, sql, paramMap, typesMap);

        Long id = null;
        Long flag = null;

        // There should be only one result in entitys
        for (int i = 0; i < entitys.size(); i++) {
            Object[] o = (Object[]) entitys.get(i);
            id = DataSecurityHelper.getLongValue(o[0]);
            flag = DataSecurityHelper.getLongValue(o[1]);
        }

        paramMap.clear();
        typesMap.clear();
        paramMap.put("caseinfoIdParam", String.valueOf(id));
        typesMap.put("caseinfoIdParam", LongType.INSTANCE);
        DataSecurityHelper.executeRawSQLQuery(session, usql, paramMap, typesMap);

        session.flush();
        session.clear();

        CaseInfoEntity entity = (CaseInfoEntity) session.get(CaseInfoEntity.class, caseId);
        Hibernate.initialize(entity.getModuleAssociations());
        Set<CaseInfoModuleAssociationEntity> moduleAssociations = saveCaseAssociations(relatedRecords, entity);

        for (DataSecurityRecordType rec : relatedRecords) {

            String assEntityType = rec.getEntityType();
            String assParentEntityType = EntityTypeEnum.CASE.code();

            // update flag of this Condition
            if (rec.getEntityType().equals(EntityTypeEnum.CONDITION.code())) {
                LegalServiceAdapter.updateConditionDataSecurityStatus(uc, rec.getEntityId(), dataSecurityFlag, comments, personId, EntityTypeEnum.CASE.code(), caseId,
                        rec.getMemento());
            }

            // update flag of this Order
            if (rec.getEntityType().equals(EntityTypeEnum.ORDER.code())) {
                LegalServiceAdapter.updateOrderDataSecurityStatus(uc, rec.getEntityId(), dataSecurityFlag, comments, personId, EntityTypeEnum.CASE.code(), caseId,
                        rec.getMemento(), Boolean.TRUE);
            }

            // update flag of this Order
            if (rec.getEntityType().equals(EntityTypeEnum.SENTENCE.code())) {
                LegalServiceAdapter.updateSentenceDataSecurityStatus(uc, rec.getEntityId(), dataSecurityFlag, comments, personId, EntityTypeEnum.CASE.code(), caseId,
                        rec.getMemento(), Boolean.TRUE);
            }

            // update flag of this Charge
            if (rec.getEntityType().equals(EntityTypeEnum.CHARGE.code())) {
                LegalServiceAdapter.updateChargeDataSecurityStatus(uc, rec.getEntityId(), dataSecurityFlag, comments, personId, EntityTypeEnum.CASE.code(), caseId,
                        rec.getMemento(), Boolean.TRUE);
            }

            // update flag of this Case Activities (Events)
            if (rec.getEntityType().equals(EntityTypeEnum.CASEACTIVITY.code())) {
                LegalServiceAdapter.updateCaseActivityDataSecurityStatus(uc, rec.getEntityId(), dataSecurityFlag, comments, personId, EntityTypeEnum.CASE.code(), caseId,
                        rec.getMemento());
            }

        }

        session.evict(entity);
        entity = BeanHelper.getEntity(session, CaseInfoEntity.class, caseId);
        List<DataSecurityRecordType> reverserelatedRecords = DataSecurityServiceAdapter.getReverseRelatedRecords(uc, caseId, EntityTypeEnum.CASE.code());
        if (reverserelatedRecords.size() > 0) {
            Set<CaseInfoModuleAssociationEntity> reverseAssociations = getReverseOrderAssociations(reverserelatedRecords, entity);

            Boolean isValid = DataSecurityHelper.verifyModuleLinks(uc, context, session, caseId, LegalModule.CASE_INFORMATION, reverseAssociations);
            if (!isValid) {
                throw new ArbutusRuntimeException(ErrorCodes.LEG_RELATED_NOT_FOUND);
            }
        }

        if (moduleAssociations.size() > 0) {

            Set<CaseInfoModuleAssociationEntity> ass = entity.getModuleAssociations();
            for (CaseInfoModuleAssociationEntity ms : moduleAssociations) {
                if (!ass.contains(ms)) {
                    ass.add(ms);
                }
            }

            // add links in toClass Entity to this Charge

            LegalHelper.createModuleLinks(uc, context, session, caseId, LegalModule.CASE_INFORMATION, entity.getModuleAssociations(), entity.getStamp());

        }

        DataSecurityRecordType dsr = new DataSecurityRecordType(null, caseId, EntityTypeEnum.CASE.code(), parentId, parentEntityType, recordType, personId, recordDate,
                comments, memento);
        dsr.setRelatedRecordId(sealedCase.getDataSecurityRecordId());

        //dsr.setIsOpen(Boolean.FALSE);

        dsr = DataSecurityServiceAdapter.createRecord(uc, dsr);
        sealedCase.setRelatedRecordId(dsr.getDataSecurityRecordId());
        sealedCase = DataSecurityServiceAdapter.updateRecord(uc, sealedCase);

        // should be good as new
        session.merge(entity);
        session.flush();
        session.clear();
        return ReturnCode.Success.returnCode();
    }

    private List<DataSecurityRelType> getCaseAssociations(UserContext uc, CaseInfoEntity entity) {

        // Get Orders and Conditions
        List<DataSecurityRelType> caseAssociations = new ArrayList<DataSecurityRelType>();

        Set<CaseInfoModuleAssociationEntity> moduleAssociations = entity.getModuleAssociations();
        for (CaseInfoModuleAssociationEntity assoc : moduleAssociations) {
            if (assoc.getToClass().equalsIgnoreCase(LegalModule.ORDER_SENTENCE.value()) && assoc.getToIdentifier() != null) {

                DataSecurityRelType dsRel = new DataSecurityRelType(EntityTypeEnum.ORDER, assoc.getToIdentifier());
                Object ord = session.get(OrderEntity.class, assoc.getToIdentifier());

                String memento = "";

                if (ord instanceof SentenceEntity) {
                    SentenceEntity sent = (SentenceEntity) ord;
                    memento = ConversionUtils.getMemento(EntityTypeEnum.SENTENCE, sent);
                    dsRel.setEntityType(EntityTypeEnum.SENTENCE);
                } else if (ord instanceof OrderEntity) {
                    OrderEntity sent = (OrderEntity) ord;
                    memento = ConversionUtils.getMemento(EntityTypeEnum.ORDER, sent);
                    dsRel.setEntityType(EntityTypeEnum.ORDER);
                } /*else {
                    OrderEntity sent = (OrderEntity) ord;
					memento = ConversionUtils.getMemento(EntityTypeEnum.ORDER, sent);
					dsRel.setEntityType(EntityTypeEnum.ORDER);
				}*/

                dsRel.setMemnto(memento);

                caseAssociations.add(dsRel);
            }
            if (assoc.getToClass().equalsIgnoreCase(LegalModule.CONDITION.value()) && assoc.getToIdentifier() != null) {
                DataSecurityRelType dsRel = new DataSecurityRelType(EntityTypeEnum.CONDITION, assoc.getToIdentifier());

                caseAssociations.add(dsRel);
            }
        }

        // Get Charges
        ChargeHandler handler = new ChargeHandler(context, session, searchMaxLimit);
        List<syscon.arbutus.product.services.legal.contract.dto.charge.ChargeType> charges = handler.getChargesByCaseInfoId(uc, entity.getCaseInfoId());

        for (syscon.arbutus.product.services.legal.contract.dto.charge.ChargeType charge : charges) {
            DataSecurityRelType dsRel = new DataSecurityRelType(EntityTypeEnum.CHARGE, charge.getChargeId());
            String memento = ConversionUtils.getMemento(EntityTypeEnum.CHARGE, charge);
            dsRel.setMemnto(memento);
            caseAssociations.add(dsRel);
        }

        // Get Case Activities
        CaseActivityHandler caHandler = new CaseActivityHandler(context, session, searchMaxLimit);
        List<CourtActivityType> caseActivities = caHandler.retrieveCaseActivities(uc, entity.getCaseInfoId(), null);

        for (CourtActivityType ca : caseActivities) {
            DataSecurityRelType dsRel = new DataSecurityRelType(EntityTypeEnum.CASEACTIVITY, ca.getCaseActivityIdentification());
            caseAssociations.add(dsRel);
        }

        return caseAssociations;
    }


    public boolean isCaseByOrganization(Long organizationId) {
        if (!BeanHelper.isEmpty(organizationId)) {
            return false;
        }
        Criteria c = session.createCriteria(CaseIdentifierEntity.class);
        c.add(Restrictions.eq("organizationId", organizationId));
        return !c.list().isEmpty();

    }
}
