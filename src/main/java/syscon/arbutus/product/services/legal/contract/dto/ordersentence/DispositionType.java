package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.core.contract.dto.StampType;

/**
 * Disposition for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 21, 2012
 */
public class DispositionType implements Serializable, Comparable<DispositionType> {

    private static final long serialVersionUID = -3275610641906352236L;

    @NotNull
    private String dispositionOutcome;
    @NotNull
    private String orderStatus;
    @NotNull
    private Date dispositionDate;

    private StampType stamp;

    /**
     * Constructor
     */
    public DispositionType() {
        super();
    }

    /**
     * Constructor
     *
     * @param dispositionOutcome String, Required. A result or outcome of the disposition.
     * @param orderStatus        String, Required. The status of an order, active or inactive.
     * @param dispositionDate    Date, Required. Date of disposition.
     */
    public DispositionType(@NotNull String dispositionOutcome, @NotNull String orderStatus, @NotNull Date dispositionDate) {
        super();
        this.dispositionOutcome = dispositionOutcome;
        this.orderStatus = orderStatus;
        this.dispositionDate = dispositionDate;
    }

    /**
     * Get Disposition Outcome
     * -- A result or outcome of the disposition
     *
     * @return the dispositionOutcome
     */
    public String getDispositionOutcome() {
        return dispositionOutcome;
    }

    /**
     * Set Disposition Outcome
     * -- A result or outcome of the disposition
     *
     * @param dispositionOutcome the dispositionOutcome to set
     */
    public void setDispositionOutcome(String dispositionOutcome) {
        this.dispositionOutcome = dispositionOutcome;
    }

    /**
     * Get Order Status
     * -- The status of an order, active or inactive.
     *
     * @return the orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }

    /**
     * Set Order Status
     * -- The status of an order, active or inactive.
     *
     * @param orderStatus the orderStatus to set
     */
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * Get Disposition Date
     * -- Date of disposition
     *
     * @return the dispositionDate
     */
    public Date getDispositionDate() {
        return dispositionDate;
    }

    /**
     * Get Disposition Date
     * -- Date of disposition
     *
     * @param dispositionDate the dispositionDate to set
     */
    public void setDispositionDate(Date dispositionDate) {
        this.dispositionDate = dispositionDate;
    }

    /**
     * Get Stamp
     *
     * @return the stamp
     */
    public StampType getStamp() {
        return stamp;
    }

    /**
     * Set Stamp
     *
     * @param stamp the stamp to set
     */
    public void setStamp(StampType stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dispositionDate == null) ? 0 : dispositionDate.hashCode());
        result = prime * result + ((dispositionOutcome == null) ? 0 : dispositionOutcome.hashCode());
        result = prime * result + ((orderStatus == null) ? 0 : orderStatus.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        DispositionType other = (DispositionType) obj;
        if (dispositionDate == null) {
            if (other.dispositionDate != null) {
				return false;
			}
        } else if (!dispositionDate.equals(other.dispositionDate)) {
			return false;
		}
        if (dispositionOutcome == null) {
            if (other.dispositionOutcome != null) {
				return false;
			}
        } else if (!dispositionOutcome.equals(other.dispositionOutcome)) {
			return false;
		}
        if (orderStatus == null) {
            if (other.orderStatus != null) {
				return false;
			}
        } else if (!orderStatus.equals(other.orderStatus)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DispositionType [dispositionOutcome=" + dispositionOutcome + ", orderStatus=" + orderStatus + ", dispositionDate=" + dispositionDate + ", stamp=" + stamp
                + "]";
    }

    @Override
    public int compareTo(DispositionType o) {
        if (o == null) {
			return 0;
		}
        return this.dispositionDate.compareTo(o.dispositionDate);
    }
}
