package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

/**
 * BailAmountType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 21, 2012
 */
public class BailAmountType implements Serializable {

    private static final long serialVersionUID = 8891795602379133109L;

    @NotNull
    private String bailType;
    @NotNull
    @Digits(integer = 9, fraction = 2)
    private BigDecimal bailAmount;
    private Set<Long> chargeAssociations;

    /**
     * Constructor
     */
    public BailAmountType() {
        super();
    }

    /**
     * Constructor
     *
     * @param bailType           String, Required. The type of bail i.e., cash, surety, property etc.
     * @param bailAmount         BigDecimal, Required. The amount of bail set for each type.
     * @param chargeAssociations Set&lt;Long>, Optional. A set of charges that relate to this bail amount.
     */
    public BailAmountType(@NotNull String bailType, @NotNull BigDecimal bailAmount, Set<Long> chargeAssociations) {
        super();
        this.bailType = bailType;
        this.bailAmount = bailAmount;
        this.chargeAssociations = chargeAssociations;
    }

    /**
     * Get Bail Type
     * -- The type of bail i.e., cash, surety, property etc.
     *
     * @return the bailType
     */
    public String getBailType() {
        return bailType;
    }

    /**
     * Set Bail Type
     * -- The type of bail i.e., cash, surety, property etc.
     *
     * @param bailType the bailType to set
     */
    public void setBailType(String bailType) {
        this.bailType = bailType;
    }

    /**
     * Get Bail Amount
     * -- The amount of bail set for each type
     *
     * @return the bailAmount
     */
    public BigDecimal getBailAmount() {
        return bailAmount;
    }

    /**
     * Set Bail Amount
     * -- The amount of bail set for each type
     *
     * @param bailAmount the bailAmount to set
     */
    public void setBailAmount(BigDecimal bailAmount) {
        this.bailAmount = bailAmount;
    }

    /**
     * Get Charge Associations
     * -- A set of charges that relate to this bail amount
     *
     * @return the chargeAssociations
     */
    public Set<Long> getChargeAssociations() {
        return chargeAssociations;
    }

    /**
     * Set Charge Associations
     * -- A set of charges that relate to this bail amount
     *
     * @param chargeAssociations the chargeAssociations to set
     */
    public void setChargeAssociations(Set<Long> chargeAssociations) {
        this.chargeAssociations = chargeAssociations;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bailType == null) ? 0 : bailType.hashCode());
        result = prime * result + ((chargeAssociations == null) ? 0 : chargeAssociations.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        BailAmountType other = (BailAmountType) obj;
        if (bailType == null) {
            if (other.bailType != null) {
				return false;
			}
        } else if (!bailType.equals(other.bailType)) {
			return false;
		}
        if (chargeAssociations == null) {
            if (other.chargeAssociations != null) {
				return false;
			}
        } else if (!chargeAssociations.equals(other.chargeAssociations)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "BailAmountType [bailType=" + bailType + ", bailAmount=" + bailAmount + ", chargeAssociations=" + chargeAssociations + "]";
    }

}
