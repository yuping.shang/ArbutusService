/**
 *
 */
package syscon.arbutus.product.services.legal.contract.dto.composite;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueType;

/**
 * The representation of the search type of the inquiry of cases.
 *
 * @author lhan
 */
public class InquiryCaseSearchType implements Serializable {
    private static final long serialVersionUID = 592379050136736947L;

    private String caseIdentifierType;
    private String caseIdentifierNumber;
    private Date caseIssuedDateFrom;
    private Date caseIssuedDateTo;
    private Set<Long> facilityIds;

    private Set<ClientCustomFieldValueType> CCFSearchCriteria;

    /**
     * @return the caseIdentifierType
     */
    public String getCaseIdentifierType() {
        return caseIdentifierType;
    }

    /**
     * @param caseIdentifierType the caseIdentifierType to set
     */
    public void setCaseIdentifierType(String caseIdentifierType) {
        this.caseIdentifierType = caseIdentifierType;
    }

    /**
     * @return the caseIdentifierNumber
     */
    public String getCaseIdentifierNumber() {
        return caseIdentifierNumber;
    }

    /**
     * @param caseIdentifierNumber the caseIdentifierNumber to set
     */
    public void setCaseIdentifierNumber(String caseIdentifierNumber) {
        this.caseIdentifierNumber = caseIdentifierNumber;
    }

    /**
     * @return the caseIssuedDateFrom
     */
    public Date getCaseIssuedDateFrom() {
        return caseIssuedDateFrom;
    }

    /**
     * @param caseIssuedDateFrom the caseIssuedDateFrom to set
     */
    public void setCaseIssuedDateFrom(Date caseIssuedDateFrom) {
        this.caseIssuedDateFrom = caseIssuedDateFrom;
    }

    /**
     * @return the caseIssuedDateTo
     */
    public Date getCaseIssuedDateTo() {
        return caseIssuedDateTo;
    }

    /**
     * @param caseIssuedDateTo the caseIssuedDateTo to set
     */
    public void setCaseIssuedDateTo(Date caseIssuedDateTo) {
        this.caseIssuedDateTo = caseIssuedDateTo;
    }

    public Set<Long> getFacilityIds() {
        return facilityIds;
    }

    public void setFacilityIds(Set<Long> facilityIds) {
        this.facilityIds = facilityIds;
    }

    /**
     * @return the cCFSearchCriteria
     */
    public Set<ClientCustomFieldValueType> getCCFSearchCriteria() {
        return CCFSearchCriteria;
    }

    /**
     * @param cCFSearchCriteria the cCFSearchCriteria to set
     */
    public void setCCFSearchCriteria(Set<ClientCustomFieldValueType> cCFSearchCriteria) {
        CCFSearchCriteria = cCFSearchCriteria;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "InquiryCaseSearchType [caseIdentifierType=" + caseIdentifierType + ", caseIdentifierNumber=" + caseIdentifierNumber + ", caseIssuedDateFrom="
                + caseIssuedDateFrom + ", caseIssuedDateTo=" + caseIssuedDateTo + ", CCFSearchCriteria=" + CCFSearchCriteria + "]";
    }

}
