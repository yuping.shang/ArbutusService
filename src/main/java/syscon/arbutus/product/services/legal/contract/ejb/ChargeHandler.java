package syscon.arbutus.product.services.legal.contract.ejb;

import javax.ejb.SessionContext;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.*;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.*;
import org.hibernate.criterion.*;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.common.adapters.DataSecurityServiceAdapter;
import syscon.arbutus.product.services.core.common.adapters.LegalServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.exception.*;
import syscon.arbutus.product.services.datasecurity.contract.dto.DataSecurityRecordType;
import syscon.arbutus.product.services.datasecurity.contract.dto.DataSecurityRelType;
import syscon.arbutus.product.services.datasecurity.contract.dto.ReferenceSet.DataSecurityRecordTypeEnum;
import syscon.arbutus.product.services.datasecurity.contract.dto.ReferenceSet.EntityTypeEnum;
import syscon.arbutus.product.services.datasecurity.contract.ejb.DataSecurityHelper;
import syscon.arbutus.product.services.legal.contract.dto.LegalModule;
import syscon.arbutus.product.services.legal.contract.dto.ReferenceSet;
import syscon.arbutus.product.services.legal.contract.dto.charge.*;
import syscon.arbutus.product.services.legal.contract.dto.valueholder.ConversionUtils;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ConfigEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.CaseInfoEntity;
import syscon.arbutus.product.services.legal.realization.persistence.charge.*;
import syscon.arbutus.product.services.legal.realization.persistence.condition.ConditionEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.OrderEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.SentenceEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * This handler is provided for handling charge related functions.
 *
 * @author byu, lhan
 */
public class ChargeHandler {

    private static final String CHARGE_STATUS_ACTIVE = "ACTIVE";
    private static final String IN_JURISDICTION = "IJ";
    private static final String OUT_JURISDICTION = "OJ";
    private static Logger log = LoggerFactory.getLogger(ChargeHandler.class);
    private SessionContext context;
    private Session session;

    private int searchMaxLimit = 200;

    public ChargeHandler(SessionContext context, Session session) {
        super();
        this.context = context;
        this.session = session;
    }

    public ChargeHandler(SessionContext context, Session session, int searchMaxLimit) {
        super();
        this.context = context;
        this.session = session;
        this.searchMaxLimit = searchMaxLimit;
    }

    private static Set<ChargeModuleAssociationEntity> getReverseOrderAssociations(List<DataSecurityRecordType> relatedSealed, ChargeEntity entity) {
        Set<ChargeModuleAssociationEntity> moduleAssociations = new HashSet<ChargeModuleAssociationEntity>();
        for (DataSecurityRecordType rel : relatedSealed) {

            if (rel.getParentEntityType().equals(EntityTypeEnum.CASE.code()) && rel.getEntityType().equals(EntityTypeEnum.CHARGE.code())) {
                ChargeModuleAssociationEntity asscEntity = new ChargeModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.CASE_INFORMATION.value());
                asscEntity.setToIdentifier(rel.getParentEntityId());
                asscEntity.setCharge(entity);
                moduleAssociations.add(asscEntity);
            }

        }

        return moduleAssociations;
    }

    public ChargeType createCharge(UserContext uc, ChargeType charge) {

        String functionName = "createCharge";

        ValidationHelper.validate(charge);

        //Verify charge jurisdiction
        charge = verifyChargeJurisdiction(charge);

        // Verify charge identifiers format
        verifyChargeIdentifierFormat(charge.getChargeIdentifiers());

        // Create stamp
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        Date createdDate = createStamp.getCreateDateTime();

        // Verify offence date
        verifyOffenceDate(uc, charge, createdDate);

        // Verify charge indicator, enhancingFactor, reducingFactor are belongs to the allowed values of Statute Change Configuration.
        verifyChargeIndicator(session, charge);

        // Verify modules associated
        verifyModulesAssociated(session, charge, functionName);

        //Get charge group Id
        Long chargeGroupId = getNextChargeGroupId();

        return createSingleCharge(uc, charge, chargeGroupId, createStamp);
    }

    private ChargeType verifyChargeJurisdiction(ChargeType charge) {
        String functionName = "verifyChargeJurisdiction";

        if ((charge.getCaseInfoId() == null && charge.getOJOrderId() == null) || (charge.getCaseInfoId() != null && charge.getOJOrderId() != null)) {
            String message = "Either case info id or OJ order id and only one of them must have value.";
            LogHelper.debug(log, functionName, message);
            throw new InvalidInputException(message);
        }

        if (charge.getChargeJurisdiction() == null) {
            charge.setChargeJurisdiction(IN_JURISDICTION);
        }

        if (charge.getChargeJurisdiction().equalsIgnoreCase(IN_JURISDICTION) && charge.getStatuteChargeId() == null) {
            String message = "Statute charge id can not be null if charge is IJ charge.";
            LogHelper.debug(log, functionName, message);
            throw new InvalidInputException(message);
        }

        if (charge.getChargeJurisdiction().equalsIgnoreCase(OUT_JURISDICTION) && (charge.getOJChargeCode() == null || charge.getOJChargeDescription() == null)) {
            String message = "OJ charge code and OJ charge description can not be null if charge is OJ charge.";
            LogHelper.debug(log, functionName, message);
            throw new InvalidInputException(message);
        }

        return charge;
    }

    public List<ChargeType> createCharges(UserContext uc, ChargeType charge) {

        String functionName = "createCharges";

        ValidationHelper.validate(charge);

        //Verify charge jurisdiction
        charge = verifyChargeJurisdiction(charge);

        //Verify charge identifiers format
        verifyChargeIdentifierFormat(charge.getChargeIdentifiers());

        // Create stamp
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        Date createdDate = createStamp.getCreateDateTime();

        //Verify offence date
        verifyOffenceDate(uc, charge, createdDate);

        //Verify charge indicator, enhancingFactor, reducingFactor are belongs to the allowed values of Statute Change Configuration.
        verifyChargeIndicator(session, charge);

        // Verify modules associated
        verifyModulesAssociated(session, charge, functionName);

        //Get charge group Id
        Long chargeGroupId = getNextChargeGroupId();

        ChargeType savedCharge = createSingleCharge(uc, charge, chargeGroupId, createStamp);

        List<ChargeType> ret = new ArrayList<ChargeType>();

        //To be updated for data privilege part
        ret.add(savedCharge);

        //Create multiple instance if count > 1
        Long chargeCount = charge.getChargeCount();
        if (chargeCount != null && chargeCount > 1) {
            for (Long i = 1l; i < chargeCount; i++) {
                ChargeType dto = createSingleCharge(uc, charge, chargeGroupId, createStamp);
                ret.add(dto);
            }
        }

        return ret;

    }

    public ChargeType updateCharge(UserContext uc, ChargeType charge) {
        String functionName = "updateCharge";

        ChargeType ret = new ChargeType();

        // Validation
        if (charge == null || charge.getChargeId() == null) {

            String message = String.format("%s, ChargeType=%s.", Constants.DTO_VALIDATION, charge);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        ValidationHelper.validate(charge);

        //Verify charge jurisdiction
        charge = verifyChargeJurisdiction(charge);

        // Verify charge identifiers format
        verifyChargeIdentifierFormat(charge.getChargeIdentifiers());

        // Create stamp
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        Date createdDate = createStamp.getCreateDateTime();

        // Verify offense date
        verifyOffenceDate(uc, charge, createdDate);

        // Verify charge indicator, enhancingFactor, reducingFactor are belongs to the allowed values of Statute Change Configuration.
        verifyChargeIndicator(session, charge);

        // Verify modules associated
        verifyModulesAssociated(session, charge, functionName);

        ChargeEntity entity = ChargeHelper.toChargeEntity(charge);

        //check if all reference codes are exist, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(entity, false);

        Long chargeId = entity.getChargeId();

        ChargeEntity existEntity = getCharge(session, chargeId);

        if (needToUpdate(entity, existEntity)) {
            //TODO: handle creating the history entity in future sprint.
            //ChargeHistEntity  historyEntity = ChargeHelper.toChargeHistEntity(existEntity,BeanHelper.getCreateStamp(uc, context));
            //session.save(historyEntity);

            //Identify if disposition data changed.
            boolean isDispositionChanged = isDispositionChanged(entity, existEntity);

            // Update existing entity which is in current session.
            ChargeHelper.updateExistChargeEntity(existEntity, entity);
            LegalHelper.removeModuleLinks(uc, context, session, chargeId, LegalModule.CHARGE, existEntity.getModuleAssociations());

            // Update stamp
            StampEntity modifyStamp = BeanHelper.getModifyStamp(uc, context, existEntity.getStamp());
            ChargeHelper.updateStamp(existEntity, createStamp, modifyStamp);

            // Update the exist entity and it's cascade.all mapped entities.
            session.update(existEntity);

            //Save disposition change to history.
            if (isDispositionChanged) {
                saveDispositionHistory(existEntity, createStamp);
            }
        }

        //Flush changes to db
        //session.flush();
        //session.clear();

        entity = getCharge(session, chargeId);

        // linkToModules(chargeId, entity.getModuleAssociations(), createStamp);
        LegalHelper.createModuleLinks(uc, context, session, chargeId, LegalModule.CHARGE, entity.getModuleAssociations(), createStamp);

        ret = ChargeHelper.toChargeType(entity);

        return ret;
    }

    public ChargesReturnType searchCharge(UserContext uc, ChargeSearchType search, Boolean searchHistory, Long startIndex, Long resultSize, String resultOrder) {

        ValidationHelper.validateSearchType(search);

        Criteria c = session.createCriteria(ChargeEntity.class);

        // statuteChargeId
        StatuteChargeConfigSearchType statuteChargeConfigSearch = search.getStatuteChargeConfigSearch();

        if (statuteChargeConfigSearch != null) {
            DetachedCriteria statuteChargeDC = getDetachedCriteria(uc, statuteChargeConfigSearch);
            c.add(Subqueries.propertyIn("statuteChargeId", statuteChargeDC.setProjection(Projections.id())));
        }

        // ChargeIdentifierType
        ChargeIdentifierSearchType chargeIdentifierSearch = search.getChargeIdentifierSearch();

        if (chargeIdentifierSearch != null) {
            createCriteria(uc, c, chargeIdentifierSearch);
        }

        // ChargeIndicatorType
        ChargeIndicatorSearchType chargeIndicatorSearch = search.getChargeIndicatorSearch();

        if (chargeIndicatorSearch != null) {
            createCriteria(uc, c, chargeIndicatorSearch);
        }

        // legalText
        String legalText = search.getLegalText();
        if (!BeanHelper.isEmpty(legalText)) {
            c.add(Restrictions.eq("legalText.legalText", legalText));
        }

        // Disposition: chargeStatus, dispositionOutcome, dispositionDate
        ChargeDispositionSearchType chargeDispositionSearch = search.getChargeDispositionSearch();

        if (chargeDispositionSearch != null) {
            // chargeStatus
            String chargeStatus = chargeDispositionSearch.getChargeStatus();
            if (!BeanHelper.isEmpty(chargeStatus)) {
                c.add(Restrictions.eq("chargeStatus", chargeStatus));
            }
            // dispositionOutcome
            String dispositionOutcome = chargeDispositionSearch.getDispositionOutcome();
            if (!BeanHelper.isEmpty(dispositionOutcome)) {
                c.add(Restrictions.eq("dispositionOutcome", dispositionOutcome.toUpperCase()));
            }
            // dispositionDate
            Date fromDispositionDate = chargeDispositionSearch.getDispositionDateFrom();
            if (fromDispositionDate != null) {
                c.add(Restrictions.ge("dispositionDate", fromDispositionDate));
            }
            Date toDispositionDate = chargeDispositionSearch.getDispositionDateTo();
            if (toDispositionDate != null) {
                c.add(Restrictions.ge("dispositionDate", toDispositionDate));
            }
        }

        // chargePlea
        ChargePleaSearchType chargePleaSearch = search.getChargePleaSearch();
        if (chargePleaSearch != null) {
            createCriteria(uc, c, chargePleaSearch);
        }

        // chargeAppeal
        ChargeAppealSearchType chargeAppealSearch = search.getChargeAppealSearch();

        if (chargeAppealSearch != null) {
            createCriteria(uc, c, chargeAppealSearch);
        }

        // comment
        CommentType comment = search.getComment();
        if (comment != null) {
            createCriteria(uc, c, comment);
        }

        // sentenceId
        Long sentenceId = search.getSentenceId();
        if (sentenceId != null) {
            c.add(Restrictions.eq("sentenceId", sentenceId));
        }

        //Total size
        Long totalSize = (Long) c.setProjection(Projections.countDistinct("chargeId")).uniqueResult();
        c.setProjection(null);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            c.setFirstResult(startIndex.intValue());

            if (resultSize == null || resultSize > searchMaxLimit) {
                c.setMaxResults(searchMaxLimit);
            } else {
                c.setMaxResults(resultSize.intValue());
            }
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                c.addOrder(ord);
            }
        }

        // moduleAssociations -- no requirement of supporting

        // associations -- no requirement of supporting

        // histories if searchHistory is True -- to be continued

        c.setLockMode(LockMode.NONE);
        c.setCacheMode(CacheMode.IGNORE);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        @SuppressWarnings("unchecked") List<ChargeEntity> eneities = c.list();
        List<ChargeType> ret = ChargeHelper.toChargeTypeList(session, eneities);

        ChargesReturnType rtn = new ChargesReturnType();
        rtn.setCharges(ret);
        rtn.setTotalSize(totalSize);

        return rtn;
    }

    @Deprecated
    public List<ChargeType> searchCharge(UserContext uc, Set<Long> subsetSearch, ChargeSearchType search, Boolean searchHistory) {

        String functionName = "searchCharge";

        // Validation
        if ((subsetSearch == null || subsetSearch.size() == 0) && (search == null)) {
            String message = String.format("searchCharge= %s.", search);
            LogHelper.error(log, functionName, message);

            throw new InvalidInputException(message);
        }
        if ((subsetSearch == null || subsetSearch.size() == 0) && (search != null)) {
            ValidationHelper.validateSearchType(search);
        }

        Criteria c = session.createCriteria(ChargeEntity.class);

        // chargeId
        if (subsetSearch != null && subsetSearch.size() > 0) {
            c.add(Restrictions.in("chargeId", subsetSearch));
        }

        // statuteChargeId
        StatuteChargeConfigSearchType statuteChargeConfigSearch = search.getStatuteChargeConfigSearch();
        if (statuteChargeConfigSearch != null && statuteChargeConfigSearch.isValid()) {
            DetachedCriteria statuteChargeDC = getDetachedCriteria(uc, statuteChargeConfigSearch);
            c.add(Subqueries.propertyIn("statuteChargeId", statuteChargeDC.setProjection(Projections.id())));
        }

        // ChargeIdentifierType
        ChargeIdentifierSearchType chargeIdentifierSearch = search.getChargeIdentifierSearch();
        if (chargeIdentifierSearch != null && chargeIdentifierSearch.isValid()) {
            createCriteria(uc, c, chargeIdentifierSearch);
        }

        // ChargeIndicatorType
        ChargeIndicatorSearchType chargeIndicatorSearch = search.getChargeIndicatorSearch();
        if (chargeIndicatorSearch != null && chargeIndicatorSearch.isValid()) {
            createCriteria(uc, c, chargeIndicatorSearch);
        }

        // legalText
        String legalText = search.getLegalText();
        if (!BeanHelper.isEmpty(legalText)) {
            c.add(Restrictions.eq("legalText.legalText", legalText));
        }

        // Disposition: chargeStatus, dispositionOutcome, dispositionDate
        ChargeDispositionSearchType chargeDispositionSearch = search.getChargeDispositionSearch();
        if (chargeDispositionSearch != null && chargeDispositionSearch.isValid()) {
            // chargeStatus
            String chargeStatus = chargeDispositionSearch.getChargeStatus();
            if (!BeanHelper.isEmpty(chargeStatus)) {
                c.add(Restrictions.eq("chargeStatus", chargeStatus));
            }
            // dispositionOutcome
            String dispositionOutcome = chargeDispositionSearch.getDispositionOutcome();
            if (!BeanHelper.isEmpty(dispositionOutcome)) {
                c.add(Restrictions.eq("dispositionOutcome", dispositionOutcome.toUpperCase()));
            }
            // dispositionDate
            Date fromDispositionDate = chargeDispositionSearch.getDispositionDateFrom();
            if (fromDispositionDate != null) {
                c.add(Restrictions.ge("dispositionDate", fromDispositionDate));
            }
            Date toDispositionDate = chargeDispositionSearch.getDispositionDateTo();
            if (toDispositionDate != null) {
                c.add(Restrictions.ge("dispositionDate", toDispositionDate));
            }
        }

        // chargePlea
        ChargePleaSearchType chargePleaSearch = search.getChargePleaSearch();
        if (chargePleaSearch != null && chargePleaSearch.isValid()) {
            createCriteria(uc, c, chargePleaSearch);
        }

        // chargeAppeal
        ChargeAppealSearchType chargeAppealSearch = search.getChargeAppealSearch();
        if (chargeAppealSearch != null && chargeAppealSearch.isValid()) {
            createCriteria(uc, c, chargeAppealSearch);
        }

        // sentenceId
        Long sentenceId = search.getSentenceId();
        if (sentenceId != null) {
            c.add(Restrictions.eq("sentenceId", sentenceId));
        }

        // comment
        CommentType comment = search.getComment();
        if (comment != null) {
            createCriteria(uc, c, comment);
        }

        // moduleAssociations -- no requirement of supporting

        // associations -- no requirement of supporting

        // histories if searchHistory is True -- to be continued

        @SuppressWarnings("unchecked") List<ChargeEntity> eneities = c.list();
        List<ChargeType> ret = ChargeHelper.toChargeTypeList(session, eneities);

        return ret;
    }

    public ChargeType getCharge(UserContext uc, Long chargeId) {

        // clear cache
        session.flush();
        session.clear();
        String functionName = "getCharge";

        if (chargeId == null) {
            String message = String.format("chargeId= %s.", chargeId);
            LogHelper.error(log, functionName, message);

            throw new InvalidInputException(message);
        }
        ChargeEntity entity = BeanHelper.getEntity(session, ChargeEntity.class, chargeId);
        ChargeType charge = ChargeHelper.toChargeType(entity);

        ChargeHelper.fillDisplayfields(session, charge, entity);

        return charge;
    }

    public List<ChargeType> getChargesByCaseInfoId(UserContext uc, Long caseInfoId) {

        String functionName = "getChargesByCaseInfoId";

        if (caseInfoId == null) {
            String message = String.format("chargeId= %s.", caseInfoId);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Criteria c = session.createCriteria(ChargeEntity.class);
        c.add(Restrictions.eq("caseInfoId", caseInfoId));
        c.setProjection(Projections.id());

        @SuppressWarnings("unchecked") Iterator<Long> it = c.list().iterator();
        List<ChargeType> ret = new ArrayList<ChargeType>();
        while (it.hasNext()) {
            ret.add(getCharge(uc, it.next()));
        }
        return ret;
    }

    public List<ChargeType> getChargesBySupervisionId(UserContext uc, Long supervisionId) {

        String functionName = "getChargesBySupervisionId";

        if (supervisionId == null) {
            String message = String.format("supervisionId= %s.", supervisionId);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        @SuppressWarnings("unchecked") List<Long> chargeIds = session.createQuery(
                "select chargeId from ChargeEntity c where c.caseInfoId in (select e.id from CaseInfoEntity e where e.supervisionId=:supervisionId)").setLong(
                "supervisionId", supervisionId).list();

        List<ChargeType> ret = new ArrayList<ChargeType>();
        for (Long chargeId : chargeIds) {
            ret.add(getCharge(uc, chargeId));
        }
        return ret;
    }

    public List<ChargeType> getChargesByOJOrderId(UserContext uc, Long oJOrderId) {
        String functionName = "getChargesByOJOrderId";

        if (oJOrderId == null) {
            String message = String.format("oJOrderId = %s.", oJOrderId);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Criteria c = session.createCriteria(ChargeEntity.class);
        c.add(Restrictions.eq("OJOrderId", oJOrderId));
        c.setProjection(Projections.id());

        @SuppressWarnings("unchecked") Iterator<Long> it = c.list().iterator();
        List<ChargeType> ret = new ArrayList<ChargeType>();
        while (it.hasNext()) {
            ret.add(getCharge(uc, it.next()));
        }
        return ret;
    }

    public List<ChargeType> retrieveCharges(UserContext uc, Long caseInfoId, ChargeSearchType search) {
        return null;
    }

    public List<ChargeType> retrieveChargeHistory(UserContext uc, Long chargeId, Date historyDateFrom, Date historyDateTo) {
        return null;
    }

    public List<StatuteChargeConfigType> retrieveStatuteCharges(UserContext uc, String statuteCode, Date offenceDate) {
        return null;
    }

    public StatuteChargeConfigType getStatuteCharge(UserContext uc, String statuteCode, String chargeCode, Date offenceDate) {
        String functionName = "getStatuteCharge";

        if (statuteCode == null || chargeCode == null || offenceDate == null) {
            String message = String.format("The statudeCode=%s, chargeCode=%s, offenceDate=%", statuteCode, chargeCode, offenceDate);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        //TODO: add implementation later.
        return null;

    }

    public Long deleteCharge(UserContext uc, Long chargeId) {
        Long rtnCode = ReturnCode.Success.returnCode();

        //id can not be null.
        if (chargeId == null) {
            String message = "chargeId is null";
            LogHelper.error(log, "deleteCharge", message);
            throw new InvalidInputException(message);
        }

        ChargeEntity entity = BeanHelper.getEntity(session, ChargeEntity.class, chargeId);

        isChargeDeletable(entity);

        //Soft deletion.
        cascadeSoftDeleteCharge(entity);

        return rtnCode;
    }

    public ChargeType updateChargeIdentifier(UserContext uc, ChargeIdentifierType chargeIdentifier) {
        return null;
    }

    public ChargeType removeChargeIdentifier(UserContext uc, Long chargeIdentifierId) {
        return null;
    }

    public ChargeType updateChargePlea(UserContext uc, Long chargeId, ChargePleaType chargePlea) {
        return null;
    }

    public ChargeType updateChargeAppeal(UserContext uc, Long chargeId, ChargeAppealType chargeAppeal) {
        return null;
    }

    public ExternalChargeCodeConfigType createExternalChargeCode(UserContext uc, ExternalChargeCodeConfigType externalChargeCodeConfig) {

        String functionName = "createExternalChargeCode";

        // Validation
        if (externalChargeCodeConfig == null) {

            String message = String.format("%s, ExternalChargeCodeConfigType=%s.", Constants.DTO_VALIDATION, externalChargeCodeConfig);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(externalChargeCodeConfig);

        String strChargeCode = externalChargeCodeConfig.getChargeCode();
        String strCodeGroup = externalChargeCodeConfig.getCodeGroup();
        //check for entity uniqueness, codeGroup and chargeCode cannot be duplicated
        verifyDuplicatedExtChargeCode(session, strChargeCode, strCodeGroup);

        ExternalChargeCodeConfigEntity entity = ChargeHelper.toExternalChargeCodeConfigEntity(externalChargeCodeConfig);

        //check if all reference codes are activated, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(entity, true);

        // Create stamp
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        entity.setStamp(createStamp);

        session.save(entity);

        ExternalChargeCodeConfigType ret = ChargeHelper.toExternalChargeCodeConfigType(entity);
        return ret;
    }

    public ExternalChargeCodeConfigType updateExternalChargeCode(UserContext uc, ExternalChargeCodeConfigType externalChargeCodeConfig) {

        String functionName = "updateExternalChargeCode";

        // Validation
        if (externalChargeCodeConfig == null || BeanHelper.isEmpty(externalChargeCodeConfig.getChargeCodeId())) {

            String message = String.format("%s, ExternalChargeCodeConfigType=%s.", Constants.DTO_VALIDATION, externalChargeCodeConfig);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(externalChargeCodeConfig);

        Long extChargeCodeId = externalChargeCodeConfig.getChargeCodeId();
        ExternalChargeCodeConfigEntity entity = getExtChargeCodeConfig(session, extChargeCodeId);

        //check if all reference codes are activated, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(entity, false);

        String strChargeCode = externalChargeCodeConfig.getChargeCode();
        String strCodeGroup = externalChargeCodeConfig.getCodeGroup();
        String strCodeCategory = externalChargeCodeConfig.getCodeCategory();

        //when codeGroup or chargeCode changed, check for entity uniqueness, codeGroup and chargeCode cannot be duplicated
        if (!strChargeCode.equalsIgnoreCase(entity.getChargeCode()) || !strCodeGroup.equalsIgnoreCase(entity.getCodeGroup())) {
            verifyDuplicatedExtChargeCode(session, strChargeCode, strCodeGroup);
        }

        entity.setChargeCode(strChargeCode);
        entity.setCodeGroup(strCodeGroup);
        entity.setCodeCategory(strCodeCategory);
        entity.setDescription(externalChargeCodeConfig.getDescription());

        // Create stamp
        StampEntity modifyStamp = BeanHelper.getModifyStamp(uc, context, entity.getStamp());
        entity.setStamp(modifyStamp);

        session.update(entity);

        ExternalChargeCodeConfigType ret = ChargeHelper.toExternalChargeCodeConfigType(entity);
        return ret;
    }

    public Long deleteExternalChargeCode(UserContext uc, Long extChargeCodeId) {

        String functionName = "deleteExternalChargeCode";

        Long ret = null;

        // Validation
        if (extChargeCodeId == null) {

            String message = String.format("The extChargeCodeId=%s", extChargeCodeId);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        ExternalChargeCodeConfigEntity entity = getExtChargeCodeConfig(session, extChargeCodeId);

        session.delete(entity);

        ret = ReturnCode.Success.returnCode();

        return ret;

    }

    public List<ExternalChargeCodeConfigType> searchExternalChargeCode(UserContext uc, ExternalChargeCodeConfigSearchType search, Long pageNum, Long pageSize) {

        String functionName = "searchExternalChargeCode";

        // Validation
        if (search == null) {

            String message = String.format("%s, ExternalChargeCodeConfigSearchType=%s.", Constants.DTO_VALIDATION, search);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        ValidationHelper.validateSearchType(search);

        // Create criteria
        Criteria criteria = session.createCriteria(ExternalChargeCodeConfigEntity.class, "extChargeCode");

        if (!BeanHelper.isEmpty(search.getCodeGroup())) {
            criteria.add(Restrictions.eq("codeGroup", search.getCodeGroup()));
        }
        if (!BeanHelper.isEmpty(search.getCodeCategory())) {
            criteria.add(Restrictions.eq("codeCategory", search.getCodeCategory()));
        }

        String strCode = search.getChargeCode();
        if (!BeanHelper.isEmpty(strCode)) {
            org.hibernate.criterion.MatchMode mode = org.hibernate.criterion.MatchMode.EXACT;
            if (BeanHelper.isPartialSearch(strCode)) {
                //Check for partial search for movementComment
                mode = org.hibernate.criterion.MatchMode.ANYWHERE;
                strCode = BeanHelper.toWildCard(strCode);
            }
            criteria.add(Restrictions.ilike("chargeCode", strCode, mode));
        }

        String strDesc = search.getDescription();
        if (!BeanHelper.isEmpty(strDesc)) {
            org.hibernate.criterion.MatchMode mode = org.hibernate.criterion.MatchMode.EXACT;
            if (BeanHelper.isPartialSearch(strDesc)) {
                //Check for partial search for movementComment
                mode = org.hibernate.criterion.MatchMode.ANYWHERE;
                strDesc = BeanHelper.toWildCard(strDesc);
            }
            criteria.add(Restrictions.ilike("description", strDesc, mode));
        }

        // Search and only return the predefined max number of records
        //criteria.setMaxResults(searchMaxLimit + 1);		//try to get MaxLimit + 1 record based on search condition
        criteria.setLockMode(LockMode.NONE);
        criteria.setCacheMode(CacheMode.IGNORE);

        //Pagination
        if (pageNum != null && pageSize != null && pageNum > 0l && pageSize > 0l) {
            criteria.setFirstResult((int) ((pageNum - 1) * pageSize));
            criteria.setMaxResults(pageSize.intValue());
        }

        @SuppressWarnings("unchecked") List<ExternalChargeCodeConfigEntity> list = criteria.list();

        List<ExternalChargeCodeConfigType> ret = ChargeHelper.toExternalChargeCodeConfigTypeList(list);
        return ret;

    }

    public JurisdictionConfigType createJurisdiction(UserContext uc, JurisdictionConfigType jurisdictionConfig) {

        String functionName = "createJurisdiction";

        // Validation
        if (jurisdictionConfig == null) {

            String message = String.format("%s, JurisdictionConfigType=%s.", Constants.DTO_VALIDATION, jurisdictionConfig);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(jurisdictionConfig);

        //Validation on the uniqueness for country, state/province, city, county(To be checked with BA)

        //Validation on link of country, state/province, city, county (To be checked with BA)

        JurisdictionConfigEntity entity = ChargeHelper.toJurisdictionConfigEntity(jurisdictionConfig);

        //check if all reference codes are activated, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(entity, true);

        // Create stamp
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        entity.setStamp(createStamp);
        JurisdictionConfigType ret;
        JurisdictionConfigEntity storedEntity = getJurisdictionConfigEntity(jurisdictionConfig);
        if (storedEntity == null) {
            session.save(entity);
            ret = ChargeHelper.toJurisdictionConfigType(entity);
        } else {
            ret = ChargeHelper.toJurisdictionConfigType(storedEntity);
        }

        return ret;
    }

    public JurisdictionConfigType updateJurisdiction(UserContext uc, JurisdictionConfigType jurisdictionConfig) {

        String functionName = "updateJurisdiction";

        // Validation
        if (jurisdictionConfig == null || BeanHelper.isEmpty(jurisdictionConfig.getJurisdictionId())) {

            String message = String.format("%s, JurisdictionConfigType=%s.", Constants.DTO_VALIDATION, jurisdictionConfig);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(jurisdictionConfig);

        Long jurisdictionId = jurisdictionConfig.getJurisdictionId();
        JurisdictionConfigEntity entity = getJurisdictionConfig(session, jurisdictionId);

        //check if all reference codes are activated, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(entity, false);

        String strCountry = jurisdictionConfig.getCountry();
        String strProvince = jurisdictionConfig.getStateProvince();
        String strCounty = jurisdictionConfig.getCounty();

        entity.setCountry(strCountry);
        entity.setStateProvince(strProvince);
        entity.setCounty(strCounty);
        entity.setDescription(jurisdictionConfig.getDescription());
        entity.setCity(jurisdictionConfig.getCity());

        // Create stamp
        StampEntity modifyStamp = BeanHelper.getModifyStamp(uc, context, entity.getStamp());
        entity.setStamp(modifyStamp);

        session.update(entity);

        JurisdictionConfigType ret = ChargeHelper.toJurisdictionConfigType(entity);
        return ret;
    }

    public Long deleteJurisdiction(UserContext uc, Long jurisdictionId) {

        String functionName = "deleteJurisdiction";

        Long ret = null;

        // Validation
        if (jurisdictionId == null) {

            String message = String.format("The jurisdictionId=%s", jurisdictionId);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        JurisdictionConfigEntity entity = getJurisdictionConfig(session, jurisdictionId);

        deleteStatuteConfigsByJurisdictionId(jurisdictionId);

        session.delete(entity);

        ret = ReturnCode.Success.returnCode();

        return ret;
    }

    public JurisdictionConfigsReturnType searchJurisdiction(UserContext uc, JurisdictionConfigSearchType search, Long startIndex, Long resultSize, String resultOrder) {

        String functionName = "searchJurisdiction";

        // Validation
        if (search == null) {

            String message = String.format("%s, JurisdictionConfigSearchType=%s.", Constants.DTO_VALIDATION, search);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        //        ValidationHelper.validateSearchType(search);

        // Create criteria
        Criteria criteria = session.createCriteria(JurisdictionConfigEntity.class, "jurisdictionConfig");

        if (!BeanHelper.isEmpty(search.getCountry())) {
            criteria.add(Restrictions.eq("country", search.getCountry()));
        }
        if (!BeanHelper.isEmpty(search.getStateProvince())) {
            criteria.add(Restrictions.eq("stateProvince", search.getStateProvince()));
        }
        if (!BeanHelper.isEmpty(search.getCounty())) {
            criteria.add(Restrictions.eq("county", search.getCounty()));
        }

        //Wild card search for String
        Criterion cityCriterion = getStringCriterion("city", search.getCity());
        if (cityCriterion != null) {
            criteria.add(cityCriterion);
        }

        Criterion descCriterion = getStringCriterion("description", search.getDescription());
        if (descCriterion != null) {
            criteria.add(descCriterion);
        }

        Long totalSize = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
        criteria.setProjection(null);
        criteria.setResultTransformer(Criteria.ROOT_ENTITY);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());
            criteria.setMaxResults(resultSize.intValue());
        }

        //Add ordering
        if (resultOrder != null && resultOrder.length() > 0) {
            List<Order> orderList = toOrderCriteria(resultOrder);
            for (Order ord : orderList) {
                if (ord != null) {
                    criteria.addOrder(ord);
                }
            }
        } else {
            criteria.addOrder(Order.asc("country"));
            //criteria.addOrder(Order.asc("stateProvince"));
        }

        // Search and only return the predefined max number of records
        criteria.setLockMode(LockMode.NONE);
        criteria.setCacheMode(CacheMode.IGNORE);

        @SuppressWarnings("unchecked") List<JurisdictionConfigEntity> list = criteria.list();
        List<JurisdictionConfigType> typeList = ChargeHelper.toJurisdictionConfigTypeList(list);

        JurisdictionConfigsReturnType ret = new JurisdictionConfigsReturnType();

        ret.setJurisdictionConfigs(typeList);
        ret.setTotalSize(totalSize);

        return ret;
    }

    public StatuteConfigType createStatute(UserContext uc, StatuteConfigType statuteConfig) {

        String functionName = "createStatute";

        // Validation
        if (statuteConfig == null) {

            String message = String.format("%s, StatuteConfigType=%s.", Constants.DTO_VALIDATION, statuteConfig);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(statuteConfig);

        // check if the jurisdiction existed
        verifyJurisdictionConfig(statuteConfig.getJurisdictionId(), functionName);

        StatuteConfigEntity entity = ChargeHelper.toStatuteConfigEntity(statuteConfig);

        //check if all reference codes are activated, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(entity, true);

        // Create stamp
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        entity.setStamp(createStamp);

        StatuteConfigType ret;
        StatuteConfigEntity storedEntity = getStatuteConfigEntity(statuteConfig);
        if (storedEntity == null) {
            session.save(entity);
            ret = ChargeHelper.toStatuteConfigType(entity);
        } else {
            ret = ChargeHelper.toStatuteConfigType(storedEntity);
        }

        return ret;
    }

    public StatuteConfigType updateStatute(UserContext uc, StatuteConfigType statuteConfig) {

        String functionName = "updateStatute";

        // Validation
        if (statuteConfig == null || BeanHelper.isEmpty(statuteConfig.getStatuteId())) {

            String message = String.format("%s, StatuteConfigType=%s.", Constants.DTO_VALIDATION, statuteConfig);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(statuteConfig);

        // check if the jurisdiction existed
        verifyJurisdictionConfig(statuteConfig.getJurisdictionId(), functionName);

        Long statuteId = statuteConfig.getStatuteId();
        StatuteConfigEntity entity = getStatuteConfig(session, statuteId);

        //check if all reference codes are activated, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(entity, false);
        List<StatuteChargeConfigType> chargesListToBeUpdated = new ArrayList<StatuteChargeConfigType>();
        if (entity.getEnactmentDate() != null && entity.getEnactmentDate().compareTo(statuteConfig.getEnactmentDate()) == 0) {
            StatuteChargeConfigSearchType search = new StatuteChargeConfigSearchType();
            search.setStatuteId(statuteId);
            StatuteChargeConfigsReturnType statuteChargeConfigsReturnType = searchStatuteCharge(uc, search, 0l, 200l, null);
            if (statuteChargeConfigsReturnType.getStatuteChargeConfigs() != null && statuteChargeConfigsReturnType.getStatuteChargeConfigs().size() != 0) {
                List<StatuteChargeConfigType> chargesList = new ArrayList<StatuteChargeConfigType>();
                for (StatuteChargeConfigType statuteChargeConfigType : chargesList) {
                    if (statuteChargeConfigType.getEndDate() == null) {
                        statuteChargeConfigType.setStartDate(entity.getEnactmentDate());
                        chargesListToBeUpdated.add(statuteChargeConfigType);
                    }
                }
            }
        }

        if (chargesListToBeUpdated != null && chargesListToBeUpdated.size() != 0) {
            for (StatuteChargeConfigType statuteChargeConfigType : chargesListToBeUpdated) {
                updateStatuteCharge(uc, statuteChargeConfigType);
            }
        }

        String strStatuteCode = statuteConfig.getStatuteCode();
        entity.setStatuteCode(strStatuteCode);
        entity.setStatuteSection(statuteConfig.getStatuteSection());
        entity.setDescription(statuteConfig.getDescription());
        entity.setEnactmentDate(statuteConfig.getEnactmentDate());
        entity.setRepealDate(statuteConfig.getRepealDate());
        entity.setJurisdictionId(statuteConfig.getJurisdictionId());
        entity.setDefaultJurisdiction(statuteConfig.isDefaultJurisdiction());
        entity.setOutOfJurisdiction(statuteConfig.isOutOfJurisdiction());

        // Create stamp
        StampEntity modifyStamp = BeanHelper.getModifyStamp(uc, context, entity.getStamp());
        entity.setStamp(modifyStamp);

        session.update(entity);

        StatuteConfigType ret = ChargeHelper.toStatuteConfigType(entity);
        return ret;
    }

    public Long deleteStatute(UserContext uc, Long statuteId) {

        String functionName = "deleteStatute";

        Long ret = null;

        // Validation
        if (statuteId == null) {

            String message = String.format("The statuteId=%s", statuteId);
            LogHelper.error(log, functionName, message);

            throw new InvalidInputException(message);
        }

        StatuteConfigEntity entity = getStatuteConfig(session, statuteId);

        session.delete(entity);

        ret = ReturnCode.Success.returnCode();

        return ret;

    }

    public StatuteConfigsReturnType searchStatute(UserContext uc, StatuteConfigSearchType search, Long startIndex, Long resultSize, String resultOrder) {

        String functionName = "searchStatute";

        // Validation
        if (search == null) {

            String message = String.format("%s, StatuteConfigSearchType=%s.", Constants.DTO_VALIDATION, search);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        if (search.getJurisdictionSearch() == null || search.getJurisdictionSearch().getJurisdictionId() == null ||
                search.getJurisdictionSearch().getJurisdictionId().equals(new Long(0))) {
            ValidationHelper.validateSearchType(search);
        }

        // Create criteria
        Criteria criteria = session.createCriteria(StatuteConfigEntity.class, "statuteConfig");

        if (!BeanHelper.isEmpty(search.getStatuteCode())) {
            criteria.add(Restrictions.eq("statuteCode", search.getStatuteCode()));
        }

        //Trim time portion for enactmentDate and repealDate because don't store time portion in db.
        Date enactmentDateFrom = search.getEnactmentDateFrom();
        if (!BeanHelper.isEmpty(enactmentDateFrom)) {
            criteria.add(Restrictions.ge("enactmentDate", BeanHelper.createDateWithoutTime(enactmentDateFrom)));
        }
        Date enactmentDateTo = search.getEnactmentDateTo();
        if (!BeanHelper.isEmpty(enactmentDateTo)) {
            criteria.add(Restrictions.le("enactmentDate", BeanHelper.createDateWithoutTime(enactmentDateTo)));
        }
        Date repealDateFrom = search.getRepealDateFrom();
        if (!BeanHelper.isEmpty(repealDateFrom)) {
            criteria.add(Restrictions.ge("repealDate", BeanHelper.createDateWithoutTime(repealDateFrom)));
        }
        Date repealDateTo = search.getRepealDateTo();
        if (!BeanHelper.isEmpty(repealDateTo)) {
            criteria.add(Restrictions.le("repealDate", BeanHelper.createDateWithoutTime(repealDateTo)));
        }

        if (!BeanHelper.isEmpty(search.isDefaultJurisdiction())) {
            criteria.add(Restrictions.eq("defaultJurisdiction", search.isDefaultJurisdiction()));
        }
        if (!BeanHelper.isEmpty(search.isOutOfJurisdiction())) {
            criteria.add(Restrictions.eq("outOfJurisdiction", search.isOutOfJurisdiction()));
        }

        //Wild card search for String
        Criterion criterion1 = getStringCriterion("description", search.getDescription());
        if (criterion1 != null) {
            criteria.add(criterion1);
        }

        Criterion criterion2 = getStringCriterion("statuteSection", search.getSection());
        if (criterion2 != null) {
            criteria.add(criterion2);
        }

        //Add for active
        Boolean activeFlag = search.isActive();
        if (!BeanHelper.isEmpty(search.isActive())) {
            //Active condition
            Date presentDate = BeanHelper.createDate();
            Criterion activeCriterion = Restrictions.or(Restrictions.isNull("repealDate"), Restrictions.gt("repealDate", presentDate));
            //Add active condition to criteria
            if (Boolean.TRUE.equals(activeFlag)) {
                criteria.add(activeCriterion);
            } else if (Boolean.FALSE.equals(activeFlag)) {
                criteria.add(Restrictions.not(activeCriterion));
            }
        }

        //Search for jurisdiction
        if (search.getJurisdictionSearch() != null) {
            List<Long> jurisdictionIds = null;
            if (search.getJurisdictionSearch() != null && search.getJurisdictionSearch().getJurisdictionId() != null) {
                jurisdictionIds = new ArrayList<Long>();
                jurisdictionIds.add(search.getJurisdictionSearch().getJurisdictionId());
            } else {
                jurisdictionIds = searchJurisdictionIds(uc, search.getJurisdictionSearch());
            }

            if (jurisdictionIds != null && jurisdictionIds.size() > 0) {
                criteria.add(Restrictions.in("jurisdictionId", jurisdictionIds));
            }
        }

        // Long totalSize = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
        Long totalSize = (Long) criteria.setProjection(Projections.countDistinct("statuteId")).uniqueResult();
        criteria.setProjection(null);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());
            criteria.setMaxResults(resultSize.intValue());
        }

        // Search and only return the predefined max number of records
        criteria.setLockMode(LockMode.NONE);
        criteria.setCacheMode(CacheMode.IGNORE);

        @SuppressWarnings("unchecked") List<StatuteConfigEntity> list = criteria.list();
        List<StatuteConfigType> typeList = ChargeHelper.toStatuteConfigTypeList(list);

        StatuteConfigsReturnType ret = new StatuteConfigsReturnType();
        ret.setStatuteConfigs(typeList);
        ret.setTotalSize(totalSize);
        return ret;

    }

    public StatuteChargeConfigType createStatuteCharge(UserContext uc, StatuteChargeConfigType statuteChargeConfig) {

        String functionName = "createStatuteCharge";

        // Validation
        if (statuteChargeConfig == null) {

            String message = String.format("%s, StatuteChargeConfigType=%s.", Constants.DTO_VALIDATION, statuteChargeConfig);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(statuteChargeConfig);

        //Confirmed with BA, startDate's time should be 00:00:00 and endDate's time should be 23:59:59.
        statuteChargeConfig.setStartDate(BeanHelper.createDateWithTime(statuteChargeConfig.getStartDate(), 0, 0, 0));
        statuteChargeConfig.setEndDate(BeanHelper.createDateWithTime(statuteChargeConfig.getEndDate(), 23, 59, 59));

        StatuteChargeConfigEntity entity = ChargeHelper.toStatuteChargeConfigEntity(statuteChargeConfig);

        //check if all reference codes are activated, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(entity, true);

        // Create stamp
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        entity.setStamp(createStamp);

        session.save(entity);

        StatuteChargeConfigType ret = ChargeHelper.toStatuteChargeConfigType(entity);
        return ret;
    }

    public StatuteChargeConfigType updateStatuteCharge(UserContext uc, StatuteChargeConfigType statuteChargeConfig) {

        String functionName = "updateStatuteCharge";

        // Validation
        if (statuteChargeConfig == null) {

            String message = String.format("%s, StatuteChargeConfigType=%s.", Constants.DTO_VALIDATION, statuteChargeConfig);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(statuteChargeConfig);

        //Confirmed with BA, startDate's time should be 00:00:00 and endDate's time should be 23:59:59.
        statuteChargeConfig.setStartDate(BeanHelper.createDateWithTime(statuteChargeConfig.getStartDate(), 0, 0, 0));
        statuteChargeConfig.setEndDate(BeanHelper.createDateWithTime(statuteChargeConfig.getEndDate(), 23, 59, 59));

        Long statuteChargeId = statuteChargeConfig.getStatuteChargeId();
        StatuteChargeConfigEntity entity = getStatuteChargeConfig(session, statuteChargeId);

        //check if all reference codes are activated, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(entity, false);

        // Create stamp
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        StampEntity modifyStamp = BeanHelper.getModifyStamp(uc, context, entity.getStamp());

        //Update properties to entity
        updateProperties(statuteChargeConfig, entity, createStamp, modifyStamp);

        session.update(entity);
        session.flush();

        StatuteChargeConfigType ret = ChargeHelper.toStatuteChargeConfigType(entity);
        return ret;
    }

    public StatuteChargeConfigType getStatuteCharge(UserContext uc, Long statuteChargeId) {

        String functionName = "getStatuteCharge";

        if (statuteChargeId == null) {
            String message = String.format("The statuteChargeId=%s", statuteChargeId);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        StatuteChargeConfigEntity entity = getStatuteChargeConfig(session, statuteChargeId);
        StatuteChargeConfigType ret = ChargeHelper.toStatuteChargeConfigType(entity);
        return ret;
    }

    public Long deleteStatuteCharge(UserContext uc, Long statuteChargeId) {

        String functionName = "deleteStatuteCharge";

        Long ret = null;

        // Validation
        if (statuteChargeId == null) {

            String message = String.format("The statuteChargeId=%s", statuteChargeId);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        StatuteChargeConfigEntity entity = getStatuteChargeConfig(session, statuteChargeId);

        session.delete(entity);

        ret = ReturnCode.Success.returnCode();

        return ret;
    }

    public StatuteChargeConfigsReturnType searchStatuteCharge(UserContext uc, StatuteChargeConfigSearchType search, Long startIndex, Long resultSize,
            String resultOrder) {

        String functionName = "searchStatuteCharge";

        // Validation
        if (search == null) {

            String message = String.format("%s, StatuteChargeConfigSearchType=%s.", Constants.DTO_VALIDATION, search);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        ValidationHelper.validateSearchType(search);

        // Create criteria
        Criteria criteria = session.createCriteria(StatuteChargeConfigEntity.class, "statuteChargeConfig");

        if (!BeanHelper.isEmpty(search.getStatuteId())) {
            criteria.add(Restrictions.eq("statuteId", search.getStatuteId()));
        }
        if (!BeanHelper.isEmpty(search.getChargeCode())) {
            criteria.add(Restrictions.eq("chargeCode", search.getChargeCode()));
        }
        if (!BeanHelper.isEmpty(search.getChargeType())) {
            criteria.add(Restrictions.eq("chargeType", search.getChargeType()));
        }
        if (!BeanHelper.isEmpty(search.getChargeDegree())) {
            criteria.add(Restrictions.eq("chargeDegree", search.getChargeDegree()));
        }
        if (!BeanHelper.isEmpty(search.getChargeCategory())) {
            criteria.add(Restrictions.eq("chargeCategory", search.getChargeCategory()));
        }
        if (!BeanHelper.isEmpty(search.getChargeSeverity())) {
            criteria.add(Restrictions.eq("chargeSeverity", search.getChargeSeverity()));
        }

        //Trim time portion for enactmentDate and repealDate because don't store time portion in db.
        Date startDateFrom = search.getStartDateFrom();
        if (!BeanHelper.isEmpty(startDateFrom)) {
            criteria.add(Restrictions.ge("startDate", BeanHelper.createDateWithoutTime(startDateFrom)));
        }
        Date startDateTo = search.getStartDateTo();
        if (!BeanHelper.isEmpty(startDateTo)) {
            criteria.add(Restrictions.le("startDate", BeanHelper.createDateWithoutTime(startDateTo)));
        }
        Date endDateFrom = search.getEndDateFrom();
        if (!BeanHelper.isEmpty(endDateFrom)) {
            criteria.add(Restrictions.ge("endDate", BeanHelper.createDateWithoutTime(endDateFrom)));
        }
        Date endDateTo = search.getEndDateTo();
        if (!BeanHelper.isEmpty(endDateTo)) {
            criteria.add(Restrictions.le("endDate", BeanHelper.createDateWithoutTime(endDateTo)));
        }

        if (!BeanHelper.isEmpty(search.getBailAmount())) {
            criteria.add(Restrictions.eq("bailAmount", search.getBailAmount()));
        }
        if (!BeanHelper.isEmpty(search.isBailAllowed())) {
            criteria.add(Restrictions.eq("bailAllowed", search.isBailAllowed()));
        }
        if (!BeanHelper.isEmpty(search.isBondAllowed())) {
            criteria.add(Restrictions.eq("bondAllowed", search.isBondAllowed()));
        }

        //Enhancing and reducing factor
        if (!BeanHelper.isEmpty(search.getEnhancingFactor())) {
            Criteria subCriteria = criteria.createCriteria("chargeEnhancingFactors", "enhancing");
            subCriteria.add(Restrictions.eq("enhancing.ChargeEnhancingFactor", search.getEnhancingFactor()));
        }
        if (!BeanHelper.isEmpty(search.getReducingFactor())) {
            Criteria subCriteria2 = criteria.createCriteria("chargeReducingFactors", "reducing");
            subCriteria2.add(Restrictions.eq("reducing.ChargeReducingFactor", search.getReducingFactor()));
        }

        //External Charge Code
        ExternalChargeCodeConfigSearchType extChargeCodeSearch = search.getExternalChargeCodeSearch();
        if (extChargeCodeSearch != null) {

            List<Long> extChargeCodeIds = searchExternalChargeCodeIds(uc, extChargeCodeSearch);
            if (extChargeCodeIds != null && extChargeCodeIds.size() > 0) {
                Criteria subCriteria3 = criteria.createCriteria("externalChargeCodeIds", "extChargeCode");
                subCriteria3.add(Restrictions.in("extChargeCode.ExternalChargeCodeId", extChargeCodeIds));
            }
        }

        //Statute Charge Text
        StatuteChargeTextSearchType chargeTextSearch = search.getChargeTextSearch();
        if (chargeTextSearch != null) {
            if (!BeanHelper.isEmpty(chargeTextSearch.getLanguage()) || !BeanHelper.isEmpty(chargeTextSearch.getLegalText()) || !BeanHelper.isEmpty(
                    chargeTextSearch.getDescription())) {
                Criteria subCriteria3 = criteria.createCriteria("statuteChargeText", "chargeText");

                if (!BeanHelper.isEmpty(chargeTextSearch.getLanguage())) {
                    subCriteria3.add(Restrictions.eq("chargeText.language", chargeTextSearch.getLanguage()));
                }
                //Wild card search for String
                Criterion criterion1 = getStringCriterion("chargeText.legalText", chargeTextSearch.getLegalText());
                if (criterion1 != null) {
                    subCriteria3.add(criterion1);
                }
                Criterion criterion2 = getStringCriterion("chargeText.description", chargeTextSearch.getDescription());
                if (criterion2 != null) {
                    subCriteria3.add(criterion2);
                }
            }
        }

        //Charge Indicator
        ChargeIndicatorSearchType indicatorSearch = search.getChargeIndicatorSearch();
        if (indicatorSearch != null) {
            if (!BeanHelper.isEmpty(indicatorSearch.getChargeIndicator()) || !BeanHelper.isEmpty(indicatorSearch.hasIndicatorValue()) || !BeanHelper.isEmpty(
                    indicatorSearch.getIndicatorValue())) {

                Criteria subCriteria4 = criteria.createCriteria("chargeIndicators", "chargeIndicator");

                if (!BeanHelper.isEmpty(indicatorSearch.getChargeIndicator())) {
                    subCriteria4.add(Restrictions.eq("chargeIndicator.chargeIndicator", indicatorSearch.getChargeIndicator()));
                }

                if (!BeanHelper.isEmpty(indicatorSearch.hasIndicatorValue())) {
                    subCriteria4.add(Restrictions.eq("chargeIndicator.hasIndicatorValue", indicatorSearch.hasIndicatorValue()));
                }
                //Wild card search for String
                Criterion criterion1 = getStringCriterion("chargeIndicator.chargeIndicator", indicatorSearch.getIndicatorValue());
                if (criterion1 != null) {
                    subCriteria4.add(criterion1);
                }
            }
        }

        //Get total size
        // Long totalSize = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
        Long totalSize = (Long) criteria.setProjection(Projections.countDistinct("statuteChargeId")).uniqueResult();
        criteria.setProjection(null);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());
            criteria.setMaxResults(resultSize.intValue());
        }

        // Search and only return the predefined max number of records
        criteria.setLockMode(LockMode.NONE);
        criteria.setCacheMode(CacheMode.IGNORE);

        @SuppressWarnings("unchecked") List<StatuteChargeConfigEntity> list = criteria.list();

        List<StatuteChargeConfigType> typeList = ChargeHelper.toStatuteChargeConfigTypeList(list);

        StatuteChargeConfigsReturnType ret = new StatuteChargeConfigsReturnType();
        ret.setStatuteChargeConfigs(typeList);
        ret.setTotalSize(totalSize);
        return ret;

    }

    public Long setOffenceDateConfig(UserContext uc, boolean isOffenceStartDate) {

        Long ret = null;

        //Check the setting if exist
        ConfigEntity offenceStartDateEntity = getOffenceDateConfig(session);

        // Create stamp
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);

        if (offenceStartDateEntity == null) {
            offenceStartDateEntity = new ConfigEntity();
            offenceStartDateEntity.setPropertyName(Constants.OFFENCE_START_DATE);
            offenceStartDateEntity.setPropertyValue(String.valueOf(isOffenceStartDate));
            offenceStartDateEntity.setStamp(createStamp);
        } else {
            // Create modify stamp
            StampEntity modifyStamp = BeanHelper.getModifyStamp(uc, context, offenceStartDateEntity.getStamp());
            offenceStartDateEntity.setPropertyValue(String.valueOf(isOffenceStartDate));
            offenceStartDateEntity.setStamp(modifyStamp);
        }

        session.saveOrUpdate(offenceStartDateEntity);

        ret = ReturnCode.Success.returnCode();

        return ret;

    }

    public Long deleteOffenceDateConfig(UserContext uc) {

        Long ret = null;

        //Check the setting if exist
        ConfigEntity entity = getOffenceDateConfig(session);

        session.delete(entity);

        ret = ReturnCode.Success.returnCode();

        return ret;
    }

    public Boolean getOffenceStartDateConfig(UserContext uc) {

        //Testing for new env

        Boolean ret = null;

        //Check the setting if exist
        ConfigEntity offenceStartDateEntity = getOffenceDateConfig(session);

        if (offenceStartDateEntity != null) {
            String value = offenceStartDateEntity.getPropertyValue();
            //TODO: handle string to boolean
            if (value.equalsIgnoreCase("true")) {
                ret = true;
            } else {
                ret = false;
            }
        }

        return ret;

    }

    public CodeType getDispositionStatus(UserContext uc, CodeType dispositionOutcome) {
        if (dispositionOutcome == null) {
            throw new ArbutusRuntimeException("Invalid argument: dispositionOutcome is required.");
        }
        ValidationHelper.validate(dispositionOutcome);
        CodeType outcome = new CodeType(dispositionOutcome.getSet().toUpperCase(), dispositionOutcome.getCode().toUpperCase());

        return BeanHelper.getLinkCode(uc, outcome, ReferenceSet.CHARGE_STATUS.value().toUpperCase());
    }

    /**
     * @param uc
     * @param chargesStatusType
     */
    public void updateChargesStatus(UserContext uc, ChargesStatusType chargesStatusType) {

        ValidationHelper.validate(chargesStatusType);

        List<Long> selectedChargesIds = chargesStatusType.getSelectedActiveChargeIds();
        for (Long id : selectedChargesIds) {
            ChargeEntity entity = (ChargeEntity) session.get(ChargeEntity.class, id);

            //Only existing active charge can be updated outcome status.
            if (entity == null || !entity.getChargeStatus().equalsIgnoreCase(CHARGE_STATUS_ACTIVE)) {
                continue;
            }

            entity.setChargeStatus(chargesStatusType.getChargeStatus());
            entity.setDispositionOutcome(chargesStatusType.getChargeOutcome());
            entity.setDispositionDate(chargesStatusType.getChargeOutcomeDate());

            CommentEntity commentEntity = new CommentEntity();
            entity.setComment(commentEntity);
            commentEntity.setCommentText(chargesStatusType.getComment());
            commentEntity.setCommentDate(chargesStatusType.getChargeOutcomeDate());
            commentEntity.setCommentUserId(BeanHelper.getUserId(uc, context));

            //check if all reference codes are valid, otherwise will throw InvalidInputException
            ValidationHelper.verifyMetaCodes(entity, false);

            StampEntity modifyStamp = BeanHelper.getModifyStamp(uc, context, entity.getStamp());
            entity.setStamp(modifyStamp);

            session.update(entity);

            StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);

            //Save disposition change to history
            saveDispositionHistory(entity, createStamp);
        }
    }

    public List<ChargeCodeDescriptionType> retrieveChargeCodeDescriptions(UserContext uc, Long statuteId, Date offenseDate) {
        String functionName = "retrieveChargeCodeDescriptions";

		/*if(statuteId == null || offenseDate == null){
            String message = "Both statuteId & offenseDate can not be null.";
			LogHelper.error(log, functionName, message);
			throw new InvalidInputException(message);
		}*/

        String queryString =
                "select DISTINCT new syscon.arbutus.product.services.legal.contract.dto.charge.ChargeCodeDescriptionType(charge.statuteId, statConfig.statuteCode, charge.chargeCode, chargeText.description) "
                        +
                        "from StatuteChargeConfigEntity charge " +
                        "join charge.statuteChargeText chargeText " +
                        "join charge.statuteConfig statConfig " +
                        "where charge.startDate <= :offenseDate " +
                        "and charge.endDate >= :offenseDate";

        Query hqlQuery = session.createQuery(queryString).
                //setLong("statuteId", statuteId).
                        setTimestamp("offenseDate", offenseDate);

        @SuppressWarnings("unchecked") List<ChargeCodeDescriptionType> queryRtn = (List<ChargeCodeDescriptionType>) hqlQuery.list();

        for (ChargeCodeDescriptionType type : queryRtn) {
            type.setOffenseDate(offenseDate);
        }

        return queryRtn;
    }

    ////////////////////////////////////////////Private methods//////////////////////////////////////////////

    public List<StatuteChargeConfigType> retrieveStatuteChargeConfigsByCodeDescription(UserContext uc, ChargeCodeDescriptionType chargeCodeDescriptionType) {

        ValidationHelper.validate(chargeCodeDescriptionType);

        String queryString = "select charge from StatuteChargeConfigEntity charge " +
                "join charge.statuteChargeText chargeText " +
                "where charge.chargeCode = :chargeCode " +
                "and charge.statuteId = :statuteId " +
                "and chargeText.description = :chargeDescription " +
                "and charge.startDate <= :offenseDate " +
                "and charge.endDate >= :offenseDate";

        Query hqlQuery = session.createQuery(queryString).
                setLong("statuteId", chargeCodeDescriptionType.getStatuteId()).
                setString("chargeCode", chargeCodeDescriptionType.getChargeCode()).
                setString("chargeDescription", chargeCodeDescriptionType.getChargeDescription()).
                setTimestamp("offenseDate", chargeCodeDescriptionType.getOffenseDate());

        @SuppressWarnings("unchecked") List<StatuteChargeConfigEntity> queryRtn = (List<StatuteChargeConfigEntity>) hqlQuery.list();

        return ChargeHelper.toStatuteChargeConfigTypeList(queryRtn);
    }

    /**
     * Cascade soft deletes the case info, also including it's case identifiers, case events and case affiliations.
     *
     * @param entity
     */
    private void cascadeSoftDeleteCharge(ChargeEntity entity) {

        //Delete charge itself, no other cascade entities need to be deleted.
        entity.setFlag(DataFlag.DELETE.value());
        session.update(entity);
    }

    /**
     * Check if the charge entity can be soft deleted.
     * 1, System checks if there are any associated warrants/holds ,court orders, sentences, or bail/sentences and conditions related to the charge,
     * If they exist the charge should not be deleted.
     * 2, Either IJ or OJ charges created for an OJ warrant can be deleted even though there are associated entities.
     *
     * @param entity
     */
    private void isChargeDeletable(ChargeEntity entity) {
        if ((entity.getOJOrderId() != null && entity.getCaseInfoId() == null) || entity.getChargeStatus().equals("INACTIVE")) {
            return;
        }

        Long sentenceId = entity.getSentenceId();
        if (sentenceId != null && !isOrderSentenceDeleted(sentenceId)) {
            throw new DataExistException(ErrorCodes.LEG_DELETE_CHARGE_SENTENCE_EXIST);
        }

        Set<ChargeModuleAssociationEntity> moduleAssociations = entity.getModuleAssociations();
        for (ChargeModuleAssociationEntity assoc : moduleAssociations) {
            if (assoc.getToClass().equalsIgnoreCase(LegalModule.ORDER_SENTENCE.value()) &&
                    assoc.getToIdentifier() != null && !isOrderSentenceDeleted(assoc.getToIdentifier())) {
                throw new DataExistException(ErrorCodes.LEG_DELETE_CHARGE_ORDER_EXIST);
            }
            if (assoc.getToClass().equalsIgnoreCase(LegalModule.CONDITION.value()) && assoc.getToIdentifier() != null) {
                throw new DataExistException(ErrorCodes.LEG_DELETE_CHARGE_CONDITION_EXIST);
            }
        }

        //Verify if case has active bail
        String queryString = "select count(orderId) from BailEntity as bail " +
                "left join bail.moduleAssociations as assoc " +
                "left join bail.orderDisposition as dispo " +
                "where assoc.toClass = :caseModule and assoc.toIdentifier = :caseId " +
                "and dispo.orderStatus = :status";
        Query query = session.createQuery(queryString).setParameter("caseModule", LegalModule.CASE_INFORMATION.value()).setParameter("caseId",
                entity.getCaseInfoId()).setParameter("status", "ACTIVE");
        Long count = (Long) query.uniqueResult();
        if (count > 0) {
            throw new DataExistException(ErrorCodes.LEG_DELETE_CHARGE_BAIL_EXIST);
        }
    }

    private boolean isOrderSentenceDeleted(Long sentenceId) {
        Query hqlQuery = session.createQuery("select flag from OrderEntity as order where order.orderId = :sentenceId").setParameter("sentenceId", sentenceId);
        Long flag = (Long) hqlQuery.uniqueResult();

        if (flag == null || flag.longValue() == 4L) {
            return true;
        } else {
            return false;
        }
    }

    private List<Long> searchExternalChargeCodeIds(UserContext uc, ExternalChargeCodeConfigSearchType search) {

        List<ExternalChargeCodeConfigType> extList = searchExternalChargeCode(uc, search, null, null);

        if (extList == null || extList.size() == 0) {
            return null;
        }

        List<Long> ret = new ArrayList<Long>();
        for (ExternalChargeCodeConfigType dto : extList) {
            if (dto != null) {
                ret.add(dto.getChargeCodeId());
            }
        }

        return ret;
    }

    private List<Long> searchJurisdictionIds(UserContext uc, JurisdictionConfigSearchType search) {

        JurisdictionConfigsReturnType configRet = searchJurisdiction(uc, search, null, null, null);

        if (configRet == null || configRet.getTotalSize() == 0) {
            return null;
        }

        List<Long> ret = new ArrayList<Long>();
        for (JurisdictionConfigType dto : configRet.getJurisdictionConfigs()) {
            if (dto != null) {
                ret.add(dto.getJurisdictionId());
            }
        }

        return ret;

    }

    private ChargeType createSingleCharge(UserContext uc, ChargeType charge, Long chargeGroupId, StampEntity createStamp) {

        ChargeEntity entity = ChargeHelper.toChargeEntity(charge);

        //check if all reference codes are activated, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(entity, true);

        entity.setChargeGroupId(chargeGroupId);
        updateStamp(entity, createStamp);

        session.save(entity);

        //Save disposition change history
        saveDispositionHistory(entity, createStamp);

        Long chargeId = entity.getChargeId();

        // Build case to charge association
        // linkToCaseInfo(entity.getCaseInfoId(), LegalModule.CHARGE, chargeId, createStamp);
        // linkToModules(chargeId, entity.getModuleAssociations(), createStamp);
        LegalHelper.createModuleLinks(uc, context, session, chargeId, LegalModule.CHARGE, entity.getModuleAssociations(), createStamp);

        ChargeType ret = ChargeHelper.toChargeType(entity);

        ChargeHelper.fillDisplayfields(session, ret, entity);

        return ret;

    }

    /**
     * Identify if the charge's disposition data has been changed or not.
     *
     * @param chargeEntity
     * @param existingChargeEntity
     * @return
     */
    private boolean isDispositionChanged(ChargeEntity chargeEntity, ChargeEntity existingChargeEntity) {
        boolean rtn = false;

        if (chargeEntity.getChargeStatus() == null) {
            if (existingChargeEntity.getChargeStatus() != null) {
                rtn = true;
            }
        } else if (!chargeEntity.getChargeStatus().equals(existingChargeEntity.getChargeStatus())) {
            rtn = true;
        }
        if (chargeEntity.getDispositionOutcome() == null) {
            if (existingChargeEntity.getDispositionOutcome() != null) {
                rtn = true;
            }
        } else if (!chargeEntity.getDispositionOutcome().equals(existingChargeEntity.getDispositionOutcome())) {
            rtn = true;
        }

        if (rtn) {
            chargeEntity.setDispositionDate(new Date());//disposition date needs to be created by service when disposition info changed, does not relays on web input.
        }

        return rtn;
    }

    private void saveDispositionHistory(ChargeEntity chargeEntity, StampEntity stamp) {
        ChargeDispositionHistEntity dispositionHistoryEntity = new ChargeDispositionHistEntity();

        dispositionHistoryEntity.setCharge(chargeEntity);
        dispositionHistoryEntity.setChargeId(chargeEntity.getChargeId());
        dispositionHistoryEntity.setChargeStatus(chargeEntity.getChargeStatus());
        dispositionHistoryEntity.setDispositionDate(chargeEntity.getDispositionDate());
        dispositionHistoryEntity.setDispositionOutcome(chargeEntity.getDispositionOutcome());
        dispositionHistoryEntity.setComment(chargeEntity.getComment());
        dispositionHistoryEntity.setStamp(stamp);

        session.save(dispositionHistoryEntity);
    }

    private ExternalChargeCodeConfigEntity getExtChargeCodeConfig(Session session, Long configId) {

        ExternalChargeCodeConfigEntity entity = (ExternalChargeCodeConfigEntity) session.get(ExternalChargeCodeConfigEntity.class, configId);

        if (entity == null) {
            String message = String.format("[getExtChargeCodeConfig] Could not find the external charge code config instance by extChargeCodeId = %d.", configId);
            throw new DataNotExistException(message);
        }

        return entity;
    }

    private JurisdictionConfigEntity getJurisdictionConfig(Session session, Long configId) {

        JurisdictionConfigEntity entity = (JurisdictionConfigEntity) session.get(JurisdictionConfigEntity.class, configId);

        if (entity == null) {
            String message = String.format("[getJurisdictionConfig] Could not find the jurisdiction configuration instance by jurisdictionId = %d.", configId);
            throw new DataNotExistException(message);
        }

        return entity;
    }

    private StatuteConfigEntity getStatuteConfig(Session session, Long configId) {

        StatuteConfigEntity entity = (StatuteConfigEntity) session.get(StatuteConfigEntity.class, configId);

        if (entity == null) {
            String message = String.format("[getStatuteConfig] Could not find the statute configuration instance by statuteId = %d.", configId);
            throw new DataNotExistException(message);
        }

        return entity;
    }

    private StatuteChargeConfigEntity getStatuteChargeConfig(Session session, Long configId) {

        StatuteChargeConfigEntity entity = (StatuteChargeConfigEntity) session.get(StatuteChargeConfigEntity.class, configId);

        if (entity == null) {
            String message = String.format("[getStatuteChargeConfig] Could not find the statute charge configuration instance by statuteChargeId = %d.", configId);
            throw new DataNotExistException(message);
        }

        return entity;
    }

    private ChargeEntity getCharge(Session session, Long instanceId) {

        ChargeEntity entity = (ChargeEntity) session.get(ChargeEntity.class, instanceId);

        if (entity == null) {
            String message = String.format("[getCharge] Could not find the charge instance by chargeId = %d.", instanceId);
            throw new DataNotExistException(message);
        }

        return entity;
    }

    private ConfigEntity getOffenceDateConfig(Session session) {

        ConfigEntity ret = null;

        Criteria criteria = session.createCriteria(ConfigEntity.class);
        criteria.add(Restrictions.eq("propertyName", Constants.OFFENCE_START_DATE));

        @SuppressWarnings("unchecked") List<ConfigEntity> list = criteria.list();

        if (list != null && list.size() == 1) {
            ret = (ConfigEntity) list.get(0);
        }

        return ret;

    }

    private Long getNextChargeGroupId() {

        String sequence = "SEQ_LEG_CHGChargeGroup_Id";

        Long ret = BeanHelper.getNextSequenceNumber(session, sequence);

        return ret;

    }

    private Criterion getStringCriterion(String fieldName, String searchValue) {
        Criterion ret = null;
        if (!BeanHelper.isEmpty(searchValue)) {
            org.hibernate.criterion.MatchMode mode = org.hibernate.criterion.MatchMode.EXACT;
            if (BeanHelper.isPartialSearch(searchValue)) {
                //Check for partial search for movementComment
                mode = org.hibernate.criterion.MatchMode.ANYWHERE;
                searchValue = BeanHelper.toWildCard(searchValue);
            }
            ret = Restrictions.ilike(fieldName, searchValue, mode);
        }

        return ret;
    }

    /**
     * Update stamp for creation only
     *
     * @param entity
     * @param stamp
     */
    private void updateStamp(ChargeEntity entity, StampEntity stamp) {

        //Update stamp for main instance

        entity.setStamp(stamp);

        if (entity.getChargeAppeal() != null) {
            entity.getChargeAppeal().setStamp(stamp);
        }
        if (entity.getChargePlea() != null) {
            entity.getChargePlea().setStamp(stamp);
        }
        if (entity.getLegalText() != null) {
            entity.getLegalText().setStamp(stamp);
        }

        //Update stamp for charge Identifiers
        if (entity.getChargeIdentifiers() != null) {
            for (ChargeIdentifierEntity subEntity : entity.getChargeIdentifiers()) {
                if (subEntity != null) {
                    subEntity.setStamp(stamp);
                }
            }
        }

        //Update stamp for charge indicators
        if (entity.getChargeIndicators() != null) {
            for (ChargeIndicatorEntity subEntity : entity.getChargeIndicators()) {
                if (subEntity != null) {
                    subEntity.setStamp(stamp);
                }
            }
        }

        //Update module associations
        if (entity.getModuleAssociations() != null) {
            for (ChargeModuleAssociationEntity subEntity : entity.getModuleAssociations()) {
                if (subEntity != null) {
                    subEntity.setStamp(stamp);
                }
            }
        }

    }

    @SuppressWarnings("unused")
    private boolean updateProperties(ChargeType dto, ChargeEntity entity, StampEntity createStamp, StampEntity modifyStamp) {

        boolean isChanged = false;

        if (!areEqual(dto.getStatuteChargeId(), entity.getStatuteChargeId())) {
            isChanged = true;
            entity.setStatuteChargeId(dto.getStatuteChargeId());
        }

        if (!areEqual(dto.getChargeIdentifiers(), entity.getChargeIdentifiers())) {
            isChanged = true;

        }

        if (isChanged) {
            entity.setStamp(modifyStamp);
        }

        return isChanged;

    }

    private boolean updateProperties(StatuteChargeConfigType dto, StatuteChargeConfigEntity entity, StampEntity createStamp, StampEntity modifyStamp) {

        boolean isChanged = false;

        if (!areEqual(dto.getStatuteId(), entity.getStatuteId())) {
            isChanged = true;
            entity.setStatuteId(dto.getStatuteId());
        }

        String strChargeCode = dto.getChargeCode();
        if (!areEqual(strChargeCode, entity.getChargeCode())) {
            isChanged = true;
            entity.setChargeCode(strChargeCode);
        }

        //Update for Statute charge text
        if (!areEqual(dto.getChargeText(), entity.getStatuteChargeText())) {
            boolean isSubChanged = false;
            String strLanguage = dto.getChargeText().getLanguage();
            if (!areEqual(strLanguage, entity.getStatuteChargeText().getLanguage())) {
                isSubChanged = true;
                entity.getStatuteChargeText().setLanguage(strLanguage);
            }
            String strDesc = dto.getChargeText().getDescription();
            if (!areEqual(strDesc, entity.getStatuteChargeText().getDescription())) {
                isSubChanged = true;
                entity.getStatuteChargeText().setDescription(strDesc);
            }
            String strLegalText = dto.getChargeText().getLegalText();
            if (!areEqual(strLegalText, entity.getStatuteChargeText().getLegalText())) {
                isSubChanged = true;
                entity.getStatuteChargeText().setLegalText(strLegalText);
            }
            if (isSubChanged) {
                isChanged = true;
                entity.getStatuteChargeText().setStamp(modifyStamp);
            }
        }

        if (!areEqual(dto.getBailAmount(), entity.getBailAmount())) {
            isChanged = true;
            entity.setBailAmount(dto.getBailAmount());
        }

        if (!areEqual(dto.isBailAllowed(), entity.isBailAllowed())) {
            isChanged = true;
            entity.setBailAllowed(dto.isBailAllowed());
        }

        if (!areEqual(dto.isBondAllowed(), entity.isBondAllowed())) {
            isChanged = true;
            entity.setBondAllowed(dto.isBondAllowed());
        }

        //Update external charge code Ids
        if (updateIdSet(dto.getExternalChargeCodeIds(), entity.getExternalChargeCodeIds())) {
            isChanged = true;
        }
        /*
    	Set<Long> newExtCodeIds = dto.getExternalChargeCodeIds();
		Set<Long> oldExtCodeIds = entity.getExternalChargeCodeIds();
    	if (!areEqual(newExtCodeIds, oldExtCodeIds)) {
    		isChanged = true;
    		entity.setExternalChargeCodeIds(newExtCodeIds);
    	}
    	*/
        if (!areEqual(dto.getStartDate(), entity.getStartDate())) {
            isChanged = true;
            entity.setStartDate(dto.getStartDate());
        }

        if (!areEqual(dto.getEndDate(), entity.getEndDate())) {
            isChanged = true;
            entity.setEndDate(dto.getEndDate());
        }

        String strChargeType = dto.getChargeType();
        if (!areEqual(strChargeType, entity.getChargeType())) {
            isChanged = true;
            entity.setChargeType(strChargeType);
        }

        String strChargeDegree = dto.getChargeDegree();
        if (!areEqual(strChargeDegree, entity.getChargeDegree())) {
            isChanged = true;
            entity.setChargeDegree(strChargeDegree);
        }

        String strChargeCategory = dto.getChargeCategory();
        if (!areEqual(strChargeCategory, entity.getChargeCategory())) {
            isChanged = true;
            entity.setChargeCategory(strChargeCategory);
        }

        String strChargeSeverity = dto.getChargeSeverity();
        if (!areEqual(strChargeSeverity, entity.getChargeSeverity())) {
            isChanged = true;
            entity.setChargeSeverity(strChargeSeverity);
        }

        //Update for charge indicators
        if (!areEqual(dto.getChargeIndicators(), entity.getChargeIndicators())) {

            Set<Long> oldIds = ChargeHelper.toStatuteChargeIndicatorIdSet(dto.getChargeIndicators());

            Set<StatuteChargeIndicatorEntity> toDelete = new HashSet<StatuteChargeIndicatorEntity>();
            for (StatuteChargeIndicatorEntity subEntity : entity.getChargeIndicators()) {
                if (!oldIds.contains(subEntity.getIndicatorId())) {
                    isChanged = true;
                    toDelete.add(subEntity);
                }
            }

            //handle removed charge indicators
            entity.getChargeIndicators().removeAll(toDelete);

            Map<Long, StatuteChargeIndicatorEntity> indicatorMap = ChargeHelper.toStatuteChargeIndicatorEntityMap(entity.getChargeIndicators());

            for (ChargeIndicatorType subDTO : dto.getChargeIndicators()) {
                Long newIndicatorId = subDTO.getIndicatorId();
                if (newIndicatorId == null) {
                    isChanged = true;
                    StatuteChargeIndicatorEntity subEntity = ChargeHelper.toStatuteChargeIndicatorEntity(subDTO);
                    subEntity.setStamp(createStamp);
                    entity.addChargeIndicator(subEntity);

                } else {

                    //Handle the updated charge indicators
                    if (indicatorMap.containsKey(newIndicatorId)) {
                        oldIds.add(newIndicatorId);    //store updated indicators

                        StatuteChargeIndicatorEntity subEntity = indicatorMap.get(newIndicatorId);
                        boolean isSubChanged = false;
                        if (newIndicatorId.equals(subEntity.getIndicatorId())) {
                            String strChargeIndicator = subDTO.getChargeIndicator();
                            if (!areEqual(strChargeIndicator, subEntity.getChargeIndicator())) {
                                isSubChanged = true;
                                subEntity.setChargeIndicator(strChargeIndicator);
                            }
                            if (!areEqual(subDTO.getIndicatorValue(), subEntity.getIndicatorValue())) {
                                isSubChanged = true;
                                subEntity.setIndicatorValue(subDTO.getIndicatorValue());
                            }
                        }
                        if (isSubChanged) {
                            isChanged = true;
                            subEntity.setStamp(modifyStamp);
                        }
                    } else {
                        isChanged = true;
                        StatuteChargeIndicatorEntity subEntity = ChargeHelper.toStatuteChargeIndicatorEntity(subDTO);
                        subEntity.setIndicatorId(null); //this id is not exist in db, will treat as new one to store in db.
                        subEntity.setStamp(createStamp); //add stamp

                        entity.addChargeIndicator(ChargeHelper.toStatuteChargeIndicatorEntity(subDTO));
                    }
                }
            }
        }

        //Update for charge enhancing factors
        if (updateCodeSet(dto.getEnhancingFactors(), entity.getChargeEnhancingFactors())) {
            isChanged = true;
        }

    	/*
    	if (!areEqual(dto.getEnhancingFactors(), entity.getChargeEnhancingFactors())) {

    		Map<String, CodeType> newCodeMap = ChargeHelper.toCodeTypeMap(dto.getEnhancingFactors());

    		Set<String> toDelete = new HashSet<String>();
    		for (String code: entity.getChargeEnhancingFactors()) {
    			if (!newCodeMap.containsKey(code)) {
    				isChanged = true;
    				toDelete.add(code);
    			}
    		}

    		//handle removed codes
    		entity.getChargeEnhancingFactors().removeAll(toDelete);

    		Set<String> oldCodes = entity.getChargeEnhancingFactors();
    		Set<String> toAdd = new HashSet<String>();
    		for (CodeType code: dto.getEnhancingFactors()) {
    			if (code != null) {
    				String strCode = code;
    				if (!oldCodes.contains(strCode)) {
    					isChanged = true;
    					toAdd.add(strCode);
    				}
    			}
    		}

    		//handle new added codes
    		entity.getChargeEnhancingFactors().addAll(toAdd);

    	}
    	*/

        //Update for charge reducing factors
        if (updateCodeSet(dto.getReducingFactors(), entity.getChargeReducingFactors())) {
            isChanged = true;
        }
    	/*
    	if (!areEqual(dto.getReducingFactors(), entity.getChargeReducingFactors())) {

    		Map<String, CodeType> newCodeMap = ChargeHelper.toCodeTypeMap(dto.getReducingFactors());

    		Set<String> toDelete = new HashSet<String>();
    		for (String code: entity.getChargeReducingFactors()) {
    			if (!newCodeMap.containsKey(code)) {
    				isChanged = true;
    				toDelete.add(code);
    			}
    		}

    		//handle removed codes
    		entity.getChargeReducingFactors().removeAll(toDelete);

    		Set<String> oldCodes = entity.getChargeReducingFactors();
    		Set<String> toAdd = new HashSet<String>();
    		for (CodeType code: dto.getReducingFactors()) {
    			if (code != null) {
    				String strCode = code;
    				if (!oldCodes.contains(strCode)) {
    					isChanged = true;
    					toAdd.add(strCode);
    				}
    			}
    		}

    		//handle new added codes
    		entity.getChargeReducingFactors().addAll(toAdd);
    	}
    	*/

        if (isChanged) {
            entity.setStamp(modifyStamp);
        }

        return isChanged;
    }

    private boolean updateIdSet(Set<Long> newIds, Set<Long> oldIds) {

        boolean isChanged = false;

        if (!areEqual(newIds, oldIds)) {

            Set<Long> toDelete = new HashSet<Long>();
            for (Long id : oldIds) {
                if (!newIds.contains(id)) {
                    isChanged = true;
                    toDelete.add(id);
                }
            }
            oldIds.removeAll(toDelete);

            Set<Long> toAdd = new HashSet<Long>();
            for (Long id : newIds) {
                if (!oldIds.contains(id)) {
                    isChanged = true;
                    toAdd.add(id);
                }
            }

            oldIds.addAll(toAdd);
        }

        return isChanged;

    }

    private boolean updateCodeSet(Set<String> newCodes, Set<String> oldCodes) {

        boolean isChanged = false;

        if (!areEqual(newCodes, oldCodes)) {

            Map<String, String> newCodeMap = ChargeHelper.toCodeTypeMap(newCodes);

            Set<String> toDelete = new HashSet<String>();
            for (String code : oldCodes) {
                if (!newCodeMap.containsKey(code)) {
                    isChanged = true;
                    toDelete.add(code);
                }
            }

            //handle removed codes
            oldCodes.removeAll(toDelete);

            Set<String> toAdd = new HashSet<String>();
            for (String code : newCodes) {
                if (code != null) {
                    String strCode = code;
                    if (!oldCodes.contains(strCode)) {
                        isChanged = true;
                        toAdd.add(strCode);
                    }
                }
            }

            //handle new added codes
            oldCodes.addAll(toAdd);
        }

        return isChanged;

    }

    private void verifyChargeIndicator(Session session, ChargeType charge) {

        if (charge.getChargeIndicators() == null || charge.getChargeIndicators().isEmpty()) {
            return;
        }

        Long statuteChargeId = charge.getStatuteChargeId();

        //verify the statueChargeId if exist
        if (statuteChargeId == null) {
            return;
        }

        StatuteChargeConfigEntity statuteChargeEntity = getStatuteChargeConfig(session, statuteChargeId);

        StatuteChargeConfigType statuteChargeType = ChargeHelper.toStatuteChargeConfigType(statuteChargeEntity);

        if (statuteChargeType.getChargeIndicators() == null || statuteChargeType.getChargeIndicators().isEmpty() ||
                !statuteChargeType.getChargeIndicators().containsAll(charge.getChargeIndicators())) {
            String message = String.format(
                    "The data values of charge indicators must belong to the allowed values as configured in the statute charge configuration type, chargeIndictors/statuteChargeIndicators = %s/%s.",
                    charge.getChargeIndicators(), statuteChargeType.getChargeIndicators());
            throw new InvalidInputException(message);
        }

    }

    private void verifyChargeIdentifierFormat(Set<ChargeIdentifierType> chargeIdentifiers) {
        String functionName = "verifyChargeIdentifierFormat";

        if (chargeIdentifiers != null && chargeIdentifiers.size() > 0) {
            for (ChargeIdentifierType dto : chargeIdentifiers) {
                if (dto != null) {
                    String identifierFormat = dto.getIdentifierFormat();
                    String identifierValue = dto.getIdentifierValue();
                    if (identifierFormat != null) {
                        // check the identifierValue based on the format
                        String message = "Invalid Charge Identifier " + identifierValue + " with Format " + identifierFormat;
                        if (identifierFormat.length() != identifierValue.length()) {
                            LogHelper.error(log, functionName, message);
                            throw new InvalidInputException(message);
                        } else {
                            //Check for length equal
                            Pattern pattern = Pattern.compile(identifierFormat.replaceAll("[0-9]", "[0-9]").replaceAll("[a-zA-Z]", "[a-zA-Z]"));
                            if (!pattern.matcher(identifierValue).matches()) {
                                LogHelper.error(log, functionName, message);
                                throw new InvalidInputException(message);
                            }
                        }
                    }
                }
            }
        }

    }

    private void verifyOffenceDate(UserContext uc, ChargeType charge, Date offenceDate) {

        //Verify offence date rule:
        Boolean isOffenceStartDate = getOffenceStartDateConfig(uc);
        if (isOffenceStartDate == null) {
            //what if not set this configuration --> TODO: to be check with BA

        } else if (isOffenceStartDate) {
            if (charge.getOffenceStartDate() == null) {
                charge.setOffenceStartDate(offenceDate);
            }

        } else {
            if (charge.getOffenceEndDate() == null) {
                charge.setOffenceEndDate(offenceDate);
            }
        }

    }

    private void verifyDuplicatedExtChargeCode(Session session, String strChargeCode, String strCodeGroup) {

        // Create criteria
        Criteria criteria = session.createCriteria(ExternalChargeCodeConfigEntity.class);
        criteria.add(Restrictions.eq("chargeCode", strChargeCode));
        criteria.add(Restrictions.eq("codeGroup", strCodeGroup));

        @SuppressWarnings("rawtypes") List list = criteria.list();

        if (list != null && list.size() > 0) {
            String message = String.format("The external charge code configuration exist with same ExtChargeCode and ExtCodeGroup, ExtChargeCode/ExtCodeGroup = %s/%s.",
                    strChargeCode, strCodeGroup);
            throw new DataExistException(message);
        }
    }

    private StatuteConfigEntity getStatuteConfigEntity(StatuteConfigType statuteConfig) {
        Criteria c = session.createCriteria(StatuteConfigEntity.class);
        c.add(Restrictions.eq("statuteCode", statuteConfig.getStatuteCode().toUpperCase()));
        c.add(Restrictions.eq("jurisdictionId", statuteConfig.getJurisdictionId()));

        c.setMaxResults(1);
        return (StatuteConfigEntity) c.uniqueResult();
    }

    private void deleteStatuteConfigsByJurisdictionId(Long jurisdictionId) {
        Criteria c = session.createCriteria(StatuteConfigEntity.class);
        c.add(Restrictions.eq("jurisdictionId", jurisdictionId));
        @SuppressWarnings({ "unchecked" }) Iterator<StatuteConfigEntity> it = c.list().iterator();
        while (it.hasNext()) {
            session.delete(it.next());
        }
    }

    private void verifyJurisdictionConfig(Long jurisdictionId, String functionName) {
        BeanHelper.getEntity(session, JurisdictionConfigEntity.class, jurisdictionId);
    }

    private JurisdictionConfigEntity getJurisdictionConfigEntity(JurisdictionConfigType jurisdictionConfig) {
        Criteria c = session.createCriteria(JurisdictionConfigEntity.class);
        c.add(Restrictions.ilike("country", jurisdictionConfig.getCountry()));
        c.add(Restrictions.ilike("stateProvince", jurisdictionConfig.getStateProvince()));

        String city = jurisdictionConfig.getCity();
        if (city != null && !city.trim().equals("")) {
            c.add(Restrictions.ilike("city", city));
        }
        if (jurisdictionConfig.getCounty() != null) {
            String county = jurisdictionConfig.getCounty();
            if (county != null && !county.trim().equals("")) {
                c.add(Restrictions.ilike("county", county));
            }
        }

        c.setMaxResults(1);
        return (JurisdictionConfigEntity) c.uniqueResult();
    }

    private void verifyModulesAssociated(Session session, ChargeType charge, String functionName) {
        //Verify caseInfoId
        Long caseId = charge.getCaseInfoId();
        if (caseId != null && !BeanHelper.bExistedModule(session, CaseInfoEntity.class, caseId)) {
            String message = String.format("%s, ChargeType.caseInfoId=%s.", Constants.DTO_VALIDATION, caseId);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        //TODO: Verify OJOrderId
    	/*Long OJOrderId = charge.getOJOrderId();
    	if (OJOrderId != null && !BeanHelper.bExistedModule(session, OrderEntity.class, OJOrderId)) {
			String message = String.format("%s, ChargeType.OJOrderId=%s.", Constants.DTO_VALIDATION, OJOrderId);
            LogHelper.error(log, functionName, message);
			throw new InvalidInputException(message);
    	}*/

        //Verify conditionId
        Set<Long> conditionIds = charge.getConditionIds();
        if (conditionIds != null && !conditionIds.isEmpty()) {
            if (!BeanHelper.bExistedModules(session, ConditionEntity.class, conditionIds)) {
                String message = String.format("%s, ChargeType.conditionIds=%s.", Constants.DTO_VALIDATION, conditionIds);
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }

        //Verify orderSentenceId
        Set<Long> orderSentenceIds = charge.getOrderSentenceIds();
        if (orderSentenceIds != null && !orderSentenceIds.isEmpty()) {
            if (!BeanHelper.bExistedModules(session, OrderEntity.class, orderSentenceIds)) {
                String message = String.format("%s, ChargeType.orderSentenceIds=%s.", Constants.DTO_VALIDATION, orderSentenceIds);
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }
    }

    /**
     * Compare if two objects are equal
     *
     * @param obj1 object 1
     * @param obj2 object 2
     * @return True if the two objects are equal.
     */
    private boolean areEqual(Object obj1, Object obj2) {
        if (obj1 == null) {
            return obj2 == null;
        } else {
            return obj1.equals(obj2);
        }
    }

    private List<Order> toOrderCriteria(String OrderString) {

        String fieldDelims = "[;]";
        String orderDelims = "[.]";
        String[] strArr = OrderString.split(fieldDelims);

        List<Order> orderList = new ArrayList<Order>();
        for (String strChild : strArr) {
            if (strChild != null && strChild.length() > 0) {
                String[] strArr2 = strChild.split(orderDelims);
                if (strArr2.length == 2) {
                    if ("ASC".equalsIgnoreCase(strArr2[1])) {
                        orderList.add(Order.asc(strArr2[0]));
                    } else if ("DESC".equalsIgnoreCase(strArr2[1])) {
                        orderList.add(Order.desc(strArr2[0]));
                    }
                }
            }
        }

        return orderList;
    }

    private DetachedCriteria getDetachedCriteria(UserContext uc, StatuteChargeConfigSearchType search) {

        DetachedCriteria criteria = DetachedCriteria.forClass(StatuteChargeConfigEntity.class);

        if (!BeanHelper.isEmpty(search.getStatuteId())) {
            criteria.add(Restrictions.eq("statuteId", search.getStatuteId()));
        }
        if (!BeanHelper.isEmpty(search.getChargeCode())) {
            criteria.add(Restrictions.eq("chargeCode", search.getChargeCode()));
        }
        if (!BeanHelper.isEmpty(search.getChargeType())) {
            criteria.add(Restrictions.eq("chargeType", search.getChargeType()));
        }
        if (!BeanHelper.isEmpty(search.getChargeDegree())) {
            criteria.add(Restrictions.eq("chargeDegree", search.getChargeDegree()));
        }
        if (!BeanHelper.isEmpty(search.getChargeCategory())) {
            criteria.add(Restrictions.eq("chargeCategory", search.getChargeCategory()));
        }
        if (!BeanHelper.isEmpty(search.getChargeSeverity())) {
            criteria.add(Restrictions.eq("chargeSeverity", search.getChargeSeverity()));
        }

        //Trim time portion for enactmentDate and repealDate because don't store time portion in db.
        Date startDateFrom = search.getStartDateFrom();
        if (!BeanHelper.isEmpty(startDateFrom)) {
            criteria.add(Restrictions.ge("startDate", BeanHelper.createDateWithoutTime(startDateFrom)));
        }
        Date startDateTo = search.getStartDateTo();
        if (!BeanHelper.isEmpty(startDateTo)) {
            criteria.add(Restrictions.le("startDate", BeanHelper.createDateWithoutTime(startDateTo)));
        }
        Date endDateFrom = search.getEndDateFrom();
        if (!BeanHelper.isEmpty(endDateFrom)) {
            criteria.add(Restrictions.ge("endDate", BeanHelper.createDateWithoutTime(endDateFrom)));
        }
        Date endDateTo = search.getEndDateTo();
        if (!BeanHelper.isEmpty(endDateTo)) {
            criteria.add(Restrictions.le("endDate", BeanHelper.createDateWithoutTime(endDateTo)));
        }

        if (!BeanHelper.isEmpty(search.getBailAmount())) {
            criteria.add(Restrictions.eq("bailAmount", search.getBailAmount()));
        }
        if (!BeanHelper.isEmpty(search.isBailAllowed())) {
            criteria.add(Restrictions.eq("bailAllowed", search.isBailAllowed()));
        }
        if (!BeanHelper.isEmpty(search.isBondAllowed())) {
            criteria.add(Restrictions.eq("bondAllowed", search.isBondAllowed()));
        }

        //Enhancing and reducing factor
        if (!BeanHelper.isEmpty(search.getEnhancingFactor())) {
            DetachedCriteria subCriteria = criteria.createCriteria("chargeEnhancingFactors", "enhancing");
            subCriteria.add(Restrictions.eq("enhancing.ChargeEnhancingFactor", search.getEnhancingFactor()));
        }
        if (!BeanHelper.isEmpty(search.getReducingFactor())) {
            DetachedCriteria subCriteria2 = criteria.createCriteria("chargeReducingFactors", "reducing");
            subCriteria2.add(Restrictions.eq("reducing.ChargeReducingFactor", search.getReducingFactor()));
        }

        //External Charge Code
        ExternalChargeCodeConfigSearchType extChargeCodeSearch = search.getExternalChargeCodeSearch();
        if (extChargeCodeSearch != null) {

            List<Long> extChargeCodeIds = searchExternalChargeCodeIds(uc, extChargeCodeSearch);
            if (extChargeCodeIds != null && extChargeCodeIds.size() > 0) {
                DetachedCriteria subCriteria3 = criteria.createCriteria("externalChargeCodeIds", "extChargeCode");
                subCriteria3.add(Restrictions.in("extChargeCode.ExternalChargeCodeId", extChargeCodeIds));
            }
        }

        //Statute Charge Text
        StatuteChargeTextSearchType chargeTextSearch = search.getChargeTextSearch();
        if (chargeTextSearch != null) {
            if (!BeanHelper.isEmpty(chargeTextSearch.getLanguage()) || !BeanHelper.isEmpty(chargeTextSearch.getLegalText()) || !BeanHelper.isEmpty(
                    chargeTextSearch.getDescription())) {
                DetachedCriteria subCriteria3 = criteria.createCriteria("statuteChargeText", "chargeText");

                if (!BeanHelper.isEmpty(chargeTextSearch.getLanguage())) {
                    subCriteria3.add(Restrictions.eq("chargeText.language", chargeTextSearch.getLanguage()));
                }
                //Wild card search for String
                Criterion criterion1 = getStringCriterion("chargeText.legalText", chargeTextSearch.getLegalText());
                if (criterion1 != null) {
                    subCriteria3.add(criterion1);
                }
                Criterion criterion2 = getStringCriterion("chargeText.description", chargeTextSearch.getDescription());
                if (criterion2 != null) {
                    subCriteria3.add(criterion2);
                }
            }
        }

        //Charge Indicator
        ChargeIndicatorSearchType indicatorSearch = search.getChargeIndicatorSearch();
        if (indicatorSearch != null) {
            if (!BeanHelper.isEmpty(indicatorSearch.getChargeIndicator()) || !BeanHelper.isEmpty(indicatorSearch.hasIndicatorValue()) || !BeanHelper.isEmpty(
                    indicatorSearch.getIndicatorValue())) {

                DetachedCriteria subCriteria4 = criteria.createCriteria("chargeIndicators", "chargeIndicator");

                if (!BeanHelper.isEmpty(indicatorSearch.getChargeIndicator())) {
                    subCriteria4.add(Restrictions.eq("chargeIndicator.chargeIndicator", indicatorSearch.getChargeIndicator()));
                }

                if (!BeanHelper.isEmpty(indicatorSearch.hasIndicatorValue())) {
                    subCriteria4.add(Restrictions.eq("chargeIndicator.hasIndicatorValue", indicatorSearch.hasIndicatorValue()));
                }
                //Wild card search for String
                Criterion criterion1 = getStringCriterion("chargeIndicator.chargeIndicator", indicatorSearch.getIndicatorValue());
                if (criterion1 != null) {
                    subCriteria4.add(criterion1);
                }
            }
        }

        return criteria;
    }

    private void createCriteria(UserContext uc, Criteria c, ChargeIdentifierSearchType chargeIdentifierSearch) {

        c.createCriteria("chargeIdentifiers", "chgIds");

        // identifierType
        String identifierType = chargeIdentifierSearch.getIdentifierType();
        if (!BeanHelper.isEmpty(identifierType)) {
            c.add(Restrictions.eq("chgIds.identifierType", identifierType.toUpperCase()));
        }

        // identifierValue
        String identifierValue = chargeIdentifierSearch.getIdentifierValue();
        if (!BeanHelper.isEmpty(identifierValue)) {
            c.add(Restrictions.eq("chgIds.identifierValue", identifierValue.toUpperCase()));
        }

        // identifierFormat
        String identifierFormat = chargeIdentifierSearch.getIdentifierFormat();
        if (!BeanHelper.isEmpty(identifierFormat)) {
            c.add(Restrictions.eq("chgIds.identifierFormat", identifierFormat.toUpperCase()));
        }

        // organizationId
        Long organizationId = chargeIdentifierSearch.getOrganizationId();
        if (!BeanHelper.isEmpty(organizationId)) {
            c.add(Restrictions.eq("chgIds.organizationId", organizationId));
        }

        // facilityId
        Long facilityId = chargeIdentifierSearch.getFacilityId();
        if (!BeanHelper.isEmpty(facilityId)) {
            c.add(Restrictions.eq("chgIds.facilityId", facilityId));
        }

        // identifierComment
        String identifierComment = chargeIdentifierSearch.getComment();
        if (!BeanHelper.isEmpty(identifierComment)) {
            c.add(Restrictions.eq("chgIds.identifierComment", identifierComment.toUpperCase()));
        }
    }

    private void createCriteria(UserContext uc, Criteria c, ChargeIndicatorSearchType chargeIndicatorSearch) {

        c.createCriteria("chargeIndicators", "chgIndicators");

        // chargeIndicator
        String chargeIndicator = chargeIndicatorSearch.getChargeIndicator();
        if (!BeanHelper.isEmpty(chargeIndicator)) {
            c.add(Restrictions.eq("chgIndicators.chargeIndicator", chargeIndicator.toUpperCase()));
        }

        // hasIndicatorValue
        Boolean hasIndicatorValue = chargeIndicatorSearch.hasIndicatorValue();
        if (hasIndicatorValue != null) {
            c.add(Restrictions.eq("chgIndicators.hasIndicatorValue", hasIndicatorValue));
        }

        // indicatorValue
        String indicatorValue = chargeIndicatorSearch.getIndicatorValue();
        if (!BeanHelper.isEmpty(indicatorValue)) {
            c.add(Restrictions.eq("chgIndicators.indicatorValue", indicatorValue.toUpperCase()));
        }
    }

    private void createCriteria(UserContext uc, Criteria c, ChargePleaSearchType chargePleaSearch) {
        c.createCriteria("chargePlea", "chargePlea");

        // pleaCode
        String pleaCode = chargePleaSearch.getPleaCode();
        if (!BeanHelper.isEmpty(pleaCode)) {
            c.add(Restrictions.eq("chargePlea.pleaCode", pleaCode.toUpperCase()));
        }

        // pleaComment
        String pleaComment = chargePleaSearch.getPleaComment();
        if (!BeanHelper.isEmpty(pleaComment)) {
            c.add(Restrictions.ilike("chargePlea.pleaComment", pleaComment));
        }
    }

    private void createCriteria(UserContext uc, Criteria c, ChargeAppealSearchType chargeAppealSearch) {
        c.createCriteria("chargeAppeal", "appeal");

        // appealStatus
        String appealStatus = chargeAppealSearch.getAppealStatus();
        if (!BeanHelper.isEmpty(appealStatus)) {
            c.add(Restrictions.eq("appeal.appealStatus", appealStatus));
        }
        // appealComment
        String appealComment = chargeAppealSearch.getAppealComment();
        if (!BeanHelper.isEmpty(appealComment)) {
            c.add(Restrictions.ilike("appeal.appealComment", appealComment));
        }
    }

    private void createCriteria(UserContext uc, Criteria c, CommentType comment) {

        c.createCriteria("comment", "cmnt");

        // commentUserId
        String commentUserId = comment.getUserId();
        if (!BeanHelper.isEmpty(commentUserId)) {
            c.add(Restrictions.eq("cmnt.commentUserId", commentUserId));
        }

        // commentDate
        Date fromCommentDate = BeanHelper.createDateWithoutTime(comment.getCommentDate());
        if (fromCommentDate != null) {
            c.add(Restrictions.ge("cmnt.commentDate", fromCommentDate));
        }
        Date toCommentDate = BeanHelper.nextNthDays(comment.getCommentDate(), 1);
        if (fromCommentDate != null) {
            c.add(Restrictions.lt("cmnt.commentDate", toCommentDate));
        }

        // commentText
        String commentText = comment.getComment();
        if (!BeanHelper.isEmpty(commentText)) {
            c.add(Restrictions.ilike("cmnt.commentText", commentText));
        }
    }

    private boolean needToUpdate(ChargeEntity entity, ChargeEntity existEntity) {
        //TODO: need to identify which element can be updated.
		/*if(entity.equals(existEntity)){
    		return false;
    	}*/

        return true;
    }

    /**
     * @param uc
     * @param chargeId         - required
     * @param dataSecurityFlag - required
     *                         Enum of DataFlag.SEAL
     * @param comments         - optional
     *                         Comments for DataSecurity Record
     * @param personId         - required
     *                         PersonId of the person updating status
     * @return
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Long updateChargeDataSecurityStatus(UserContext uc, Long chargeId, Long dataSecurityFlag, String comments, Long personId, String parentEntityType,
            Long parentId, String memnto, Boolean cascade) {

        Long ret = ReturnCode.UnknownError.returnCode();
        // id can not be null.
        if (chargeId == null || personId == null || dataSecurityFlag == null) {
            String message = "chargeId or personId or dataSecurityFlag can not be null";
            throw new InvalidInputException(message);
        }

        if (dataSecurityFlag.equals(DataFlag.SEAL.value())) {
            ret = sealCharge(uc, chargeId, comments, personId, parentEntityType, parentId, memnto, cascade);
        } else if (dataSecurityFlag.equals(DataFlag.ACTIVE.value())) {
            ret = unSealCharge(uc, chargeId, comments, personId, parentEntityType, parentId, memnto, cascade);
        }

        return ret;
    }

    public Long sealCharge(UserContext uc, Long chargeId, String comments, Long personId, String parentEntityType, Long parentId, String memento, Boolean cascade) {

        Long dataSecurityFlag = DataFlag.SEAL.value();
        ChargeEntity entity = (ChargeEntity) session.get(ChargeEntity.class, chargeId);

        Date recordDate = new Date();
        String recordType = DataSecurityRecordTypeEnum.RELATION.code();

        // 1- Set the Status on the ALL Associations of type ORDER and CONDITION
        //    of the Charge
        if (entity == null)//already sealed
        {
            return ReturnCode.Success.returnCode();
        }
        List<DataSecurityRelType> chargeAssosciations = getChargeAssociations(entity);

        if (cascade) {
            recordType = DataSecurityRecordTypeEnum.SEALCHARGE.code();

            for (DataSecurityRelType dsRel : chargeAssosciations) {

                String assEntityType = dsRel.getEntityType().code();
                String assParentEntityType = EntityTypeEnum.CHARGE.code();

                // update flag of related Condition
                if (dsRel.getEntityType().equals(EntityTypeEnum.CONDITION)) {
                    ConditionHandler conditionHandler = new ConditionHandler(context, session);
                    conditionHandler.updateConditionDataSecurityStatus(uc, dsRel.getId(), dataSecurityFlag, comments, personId, EntityTypeEnum.CHARGE.code(), chargeId,
                            dsRel.getMemnto());

                }

                // update flag of related Order
                if (dsRel.getEntityType().equals(EntityTypeEnum.ORDER)) {
                    //OrderSentenceHandler.updateOrderDataSecurityStatus(uc,context, session, dsRel.getId(), dataSecurityFlag, comments, personId, assParentEntityType, entity.getCaseInfoId(), memento);
                    LegalServiceAdapter.updateOrderDataSecurityStatus(uc, dsRel.getId(), dataSecurityFlag, comments, personId, EntityTypeEnum.CHARGE.code(), chargeId,
                            dsRel.getMemnto(), Boolean.TRUE);
                }

                // Update flag of related sentence
                if (dsRel.getEntityType().equals(EntityTypeEnum.SENTENCE)) {
                    //UserContext uc, SessionContext context, Session session, Long sentId, Long dataSecurityFlag, String comments, Long personId, String parentEntityType, Long parentId, String memento
                    OrderSentenceHandler.updateSentenceDataSecurityStatus(uc, context, session, dsRel.getId(), dataSecurityFlag, comments, personId,
                            EntityTypeEnum.CHARGE.code(), chargeId, dsRel.getMemnto(), Boolean.TRUE);
                }
                // insert security record for this association, this might not be needed?
                // handler in respective classes
            }

            session.evict(entity);
            entity = (ChargeEntity) session.get(ChargeEntity.class, chargeId);
            // Remove this charge link from associated order or condition
            LegalHelper.removeModuleLinks(uc, context, session, chargeId, LegalModule.CHARGE, entity.getModuleAssociations());
            // Remove links from this entity
            entity.getModuleAssociations().clear();

            //em.remove(em.contains(entity) ? entity : em.merge(entity));
            session.merge(entity);
            session.flush();
        }
        // 2- Set the Status on the Parent Charge
        String entityType = EntityTypeEnum.CHARGE.code();

        DataSecurityRecordType dsr = new DataSecurityRecordType(null, entity.getChargeId(), entityType, parentId, parentEntityType, recordType, personId, recordDate,
                comments, memento);
        DataSecurityServiceAdapter.createRecord(uc, dsr);

        // update charge status
        if (cascade) {
            entity.setFlag(dataSecurityFlag);
        }
        session.merge(entity);
        session.flush();
        session.clear();
        return ReturnCode.Success.returnCode();

    }

    public Long unSealCharge(UserContext uc, Long chargeId, String comments, Long personId, String parentEntityType, Long parentId, String memento, Boolean cascade) {
        //TODO: add Integration test which un-seals the seal
        List<DataSecurityRecordType> relatedRecords = DataSecurityServiceAdapter.getRelatedRecords(uc, chargeId, EntityTypeEnum.CHARGE.code());
        DataSecurityRecordType sealedCharge = DataSecurityServiceAdapter.getEntity(uc, EntityTypeEnum.CHARGE.code(), chargeId, parentEntityType, parentId);

        if (sealedCharge == null)// charge is already unsealed
        {
            return ReturnCode.Success.returnCode();
        }

        // Reverse fist un-dead the charge, then un-dead the
        String entityType = EntityTypeEnum.CHARGE.code();
        String recordType = DataSecurityRecordTypeEnum.UNSEALCHARGE.code();
        Date recordDate = new Date();

        String chrgeTblName = "leg_chgcharge";
        String sql = "select chargeid, flag from " + chrgeTblName + " where chargeid = :chargeIdParam";
        String usql = "update " + chrgeTblName + " set flag = 1 where chargeid = :chargeIdParam";
        Map<String, String> paramMap = new HashMap<String, String>();
        Map<String, Type> typesMap = new HashMap<String, Type>();

        // update charge status
        paramMap.put("chargeIdParam", String.valueOf(sealedCharge.getEntityId()));
        typesMap.put("chargeid", LongType.INSTANCE);
        typesMap.put("flag", LongType.INSTANCE);
        List entitys = DataSecurityHelper.executeRawSQLQuery(session, sql, paramMap, typesMap);

        Long id = null;
        Long flag = null;

        // There should be only one result in entitys
        for (int i = 0; i < entitys.size(); i++) {
            Object[] o = (Object[]) entitys.get(i);
            id = DataSecurityHelper.getLongValue(o[0]);
            flag = DataSecurityHelper.getLongValue(o[1]);
        }

        paramMap.clear();
        typesMap.clear();
        paramMap.put("chargeIdParam", String.valueOf(chargeId));
        typesMap.put("chargeid", LongType.INSTANCE);
        DataSecurityHelper.executeRawSQLQuery(session, usql, paramMap, typesMap);

        session.flush();
        session.clear();

        // should be un-dead now
        ChargeEntity entity = (ChargeEntity) session.get(ChargeEntity.class, chargeId);
        Set<ChargeModuleAssociationEntity> moduleAssociations = saveChargeAssociations(relatedRecords, entity);

        Long dataSecurityFlag = DataFlag.ACTIVE.value();

        for (DataSecurityRecordType rec : relatedRecords) {

            String assEntityType = rec.getEntityType();
            String assParentEntityType = EntityTypeEnum.CHARGE.code();

            if (rec.getEntityType().equals(EntityTypeEnum.CONDITION.code())) {
                LegalServiceAdapter.updateConditionDataSecurityStatus(uc, rec.getEntityId(), dataSecurityFlag, comments, personId, assParentEntityType, chargeId,
                        rec.getMemento());
            }

            // update flag of related Order
            if (rec.getEntityType().equals(EntityTypeEnum.ORDER.code())) {
                LegalServiceAdapter.updateOrderDataSecurityStatus(uc, rec.getEntityId(), dataSecurityFlag, comments, personId, assParentEntityType, chargeId,
                        rec.getMemento(), Boolean.TRUE);
            }

            if (rec.getEntityType().equals(EntityTypeEnum.SENTENCE.code())) {
                //OrderSentenceHandler.updateOrderDataSecurityStatus(uc,context, session, dsRel.getId(), dataSecurityFlag, comments, personId, assParentEntityType, entity.getCaseInfoId(), memento);
                LegalServiceAdapter.updateSentenceDataSecurityStatus(uc, rec.getEntityId(), dataSecurityFlag, comments, personId, assParentEntityType, chargeId,
                        rec.getMemento(), Boolean.TRUE);
            }
        }

        session.evict(entity);
        entity = (ChargeEntity) session.get(ChargeEntity.class, chargeId);

        List<DataSecurityRecordType> reverseRecords = DataSecurityServiceAdapter.getReverseRelatedRecords(uc, chargeId, EntityTypeEnum.CHARGE.code());
        if (reverseRecords.size() > 0) {
            Set<ChargeModuleAssociationEntity> reverseAssociations = getReverseOrderAssociations(reverseRecords, entity);

            Boolean isValid = DataSecurityHelper.verifyModuleLinks(uc, context, session, chargeId, LegalModule.CHARGE, reverseAssociations);
            if (!isValid) {
                throw new ArbutusRuntimeException(ErrorCodes.LEG_RELATED_NOT_FOUND);
            }
        }

        if (moduleAssociations.size() > 0) {
            entity.getModuleAssociations().addAll(moduleAssociations);

            // add links in toClass Entity to this Charge
            LegalHelper.createModuleLinks(uc, context, session, chargeId, LegalModule.CHARGE, entity.getModuleAssociations(), entity.getStamp());
        }

        DataSecurityRecordType dsr = new DataSecurityRecordType(null, chargeId, entityType, parentId, parentEntityType, recordType, personId, recordDate, comments,
                memento);
        dsr.setRelatedRecordId(sealedCharge.getDataSecurityRecordId());
        dsr = DataSecurityServiceAdapter.createRecord(uc, dsr);
        sealedCharge.setRelatedRecordId(dsr.getDataSecurityRecordId());
        sealedCharge = DataSecurityServiceAdapter.updateRecord(uc, sealedCharge);

        // LegalHelper.createModuleLinks(uc, context, session, chargeId, LegalModule.CASE_INFORMATION, entity.getModuleAssociations(), entity.getStamp());

        // should be good as new
        session.merge(entity);
        session.flush();
        session.clear();
        return ReturnCode.Success.returnCode();
    }

    // equal to ChargeHelper.toChargeModuleAssociationEntitySet i.e. Order, Condition
    private List<DataSecurityRelType> getChargeAssociations(ChargeEntity entity) {

        List<DataSecurityRelType> chargeAssociations = new ArrayList<DataSecurityRelType>();

        Set<ChargeModuleAssociationEntity> moduleAssociations = entity.getModuleAssociations();
        for (ChargeModuleAssociationEntity assoc : moduleAssociations) {
            if (assoc.getToClass().equalsIgnoreCase(LegalModule.ORDER_SENTENCE.value()) &&
                    assoc.getToIdentifier() != null && !isOrderSentenceDeleted(assoc.getToIdentifier())) {
                DataSecurityRelType dsRel = new DataSecurityRelType(EntityTypeEnum.ORDER, assoc.getToIdentifier());

                Object ord = session.get(OrderEntity.class, assoc.getToIdentifier());

                String memento = "";

                if (ord instanceof SentenceEntity) {
                    SentenceEntity sent = (SentenceEntity) ord;
                    memento = ConversionUtils.getMemento(EntityTypeEnum.SENTENCE, sent);
                    dsRel.setEntityType(EntityTypeEnum.SENTENCE);
                } else {
                    OrderEntity sent = (OrderEntity) ord;
                    memento = ConversionUtils.getMemento(EntityTypeEnum.ORDER, sent);
                    dsRel.setEntityType(EntityTypeEnum.ORDER);
                }

                dsRel.setMemnto(memento);

                chargeAssociations.add(dsRel);
            }
            if (assoc.getToClass().equalsIgnoreCase(LegalModule.CONDITION.value()) && assoc.getToIdentifier() != null) {
                DataSecurityRelType dsRel = new DataSecurityRelType(EntityTypeEnum.CONDITION, assoc.getToIdentifier());
                chargeAssociations.add(dsRel);
            }

            if (assoc.getToClass().equalsIgnoreCase(LegalModule.CASE_INFORMATION.value()) && assoc.getToIdentifier() != null) {
                DataSecurityRelType dsRel = new DataSecurityRelType(EntityTypeEnum.CASE, assoc.getToIdentifier());
                chargeAssociations.add(dsRel);
            }

        }

        return chargeAssociations;

    }

    private Set<ChargeModuleAssociationEntity> saveChargeAssociations(List<DataSecurityRecordType> relatedSealed, ChargeEntity entity) {
        Set<ChargeModuleAssociationEntity> moduleAssociations = new HashSet<ChargeModuleAssociationEntity>();
        for (DataSecurityRecordType rel : relatedSealed) {

            if (rel.getEntityType().equals(EntityTypeEnum.ORDER.code()) || rel.getEntityType().equals(EntityTypeEnum.SENTENCE.code())) {
                ChargeModuleAssociationEntity asscEntity = new ChargeModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.ORDER_SENTENCE.value());
                asscEntity.setToIdentifier(rel.getEntityId());
                asscEntity.setCharge(entity);
                moduleAssociations.add(asscEntity);
            }

            if (rel.getEntityType().equals(EntityTypeEnum.CONDITION.code())) {
                ChargeModuleAssociationEntity asscEntity = new ChargeModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.CONDITION.value());
                asscEntity.setToIdentifier(rel.getEntityId());
                asscEntity.setCharge(entity);
                moduleAssociations.add(asscEntity);
            }
        }

        return moduleAssociations;
    }

    /**
     * To fetch All External Charges
     *
     * @param uc
     * @return
     */
    public List<ExternalChargeCodeConfigType> searchAllExternalChargeCode(UserContext uc) {
        Query query = session.createQuery("select externalChargeCodeConfigEntity from ExternalChargeCodeConfigEntity externalChargeCodeConfigEntity");
        List<ExternalChargeCodeConfigEntity> externalChargeCodeConfigEntities = query.list();
        List<ExternalChargeCodeConfigType> ret = ChargeHelper.toExternalChargeCodeConfigTypeList(externalChargeCodeConfigEntities);
        return ret;

    }

    public StatuteConfigType getStatuteConfigId(UserContext uc, Long statuteId) {
        StatuteConfigEntity entity = getStatuteConfig(session, statuteId);
        StatuteConfigType ret = ChargeHelper.toStatuteConfigType(entity);
        return ret;
    }

    public boolean isChargeByOrganization(Long organizationId) {
        if (!BeanHelper.isEmpty(organizationId)) {
            return false;
        }
        Criteria c = session.createCriteria(ChargeEntity.class);
        c.createCriteria("chargeIdentifiers", "chgIds");
        c.add(Restrictions.disjunction()
            .add(Restrictions.eq("organizationId", organizationId))
            .add(Restrictions.eq("chgIds.organizationId", organizationId))
        );

        return !c.list().isEmpty();
    }
}
