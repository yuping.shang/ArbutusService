package syscon.arbutus.product.services.legal.contract.dto.valueholder;

import java.io.Serializable;

//JSON Holder for facility population data
public class SentenceHolder implements Serializable {

    public String consto;
    public String sentid;
    public String orderCid;
    public String stdate;
    public String enddate;
    public String type;
    public String status;
    public String disposition;
    public String sentNo;
    public SentenceHolder() {
        super();
        // TODO Auto-generated constructor stub
    }

}