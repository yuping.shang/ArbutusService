package syscon.arbutus.product.services.legal.contract.ejb;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivityType;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.CAChargeAssociationEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.CaseActivityHistEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.CourtActivityEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.CourtActivityHistEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.OrderEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;

/**
 * @author lhan
 */
public class CaseActivityHelper {

    /**
     * @param dto     CourtActivityType
     * @param stamp   StampEntity
     * @param history Set<CaseActivityHistEntity>
     * @return an object of CourtActivityEntity from the dto
     */
    public static CourtActivityEntity toCourtActivityEntity(CourtActivityType dto, StampEntity stamp, Set<CaseActivityHistEntity> history) {
        if (dto == null) {
			return null;
		}

        CourtActivityEntity rtn = new CourtActivityEntity(dto.getCaseActivityIdentification(), dto.getActivityOccurredDate(), dto.getActivityCategory(),
                dto.getActivityType(), dto.getActivityOutcome(), dto.getOutcomeDate(), dto.getActivityStatus(),
                dto.getActivityComment() == null ? null : dto.getActivityComment().getComment(),
                dto.getActivityComment() == null ? null : dto.getActivityComment().getCommentDate(), null, stamp, dto.getFacilityId(),
                dto.getFacilityInternalLocationId(), dto.getOrganizationId(), dto.getJudge(), null, null, history, null, null, null);

        rtn.setChargeAssociations(StaticAssociationHelper.toChargeAssociationEntitySet(null, rtn.getCaseActivityId(), stamp, rtn, dto.getChargeIds()));
        //rtn.setOrderInititatedAssociations(StaticAssociationHelper.toOrderInitiatedAssociationEntitySet(null, rtn.getCaseActivityId(), stamp, rtn, dto.getOrderInititatedIds()));
        //rtn.setOrderResultedAssociations(StaticAssociationHelper.toOrderResultedAssociationEntitySet(null, rtn.getCaseActivityId(), stamp, rtn, dto.getOrderResultedIds()));

        //TODO: set activityIds, caseAffiliations and caseInfos.

        return rtn;
    }

    /**
     * @param entity CourtActivityEntity
     * @return an object of CourtActivityType from the entity.
     */
    public static CourtActivityType toCourtActivityType(CourtActivityEntity entity) {
        if (entity == null) {
			return null;
		}

        CourtActivityType rtn = new CourtActivityType(entity.getCaseActivityId(), entity.getActivityOccurredDate(), entity.getActivityCategory(),
                entity.getActivityType(), toCommentType(entity), entity.getActivityOutcome(), entity.getOutcomeDate(), entity.getActivityStatus(),
                StaticAssociationHelper.toChargeIdSet(entity.getChargeAssociations()), LegalHelper.getCaseIds(entity.getCaseInfos()),
                LegalHelper.getActivityIds(entity.getActivityIds()), LegalHelper.getCaseAffiliationIds(entity.getCaseAffiliations()), null,
                entity.getFacilityAssociation(), entity.getFacilityInternalAssociation(), entity.getFacilityAssociation(), entity.getJudge(),
                toOrderIdsSet(entity.getOrdersInititated()), toOrderIdsSet(entity.getOrdersResulted()));

        return rtn;
    }

    /**
     * @param orderEntities
     * @return
     */
    private static Set<Long> toOrderIdsSet(Set<OrderEntity> orderEntities) {
        Set<Long> ret = new HashSet<Long>();

        if (orderEntities == null) {
			return ret;
		}

        Iterator<OrderEntity> it = orderEntities.iterator();
        while (it.hasNext()) {
            OrderEntity association = (OrderEntity) it.next();
            if (association != null) {
				ret.add(association.getOrderId());
			}
        }

        return ret;
    }

    /**
     * @param entity CourtActivityHistEntity
     * @return an object of CourtActivityType from the entity.
     */
    public static CourtActivityType toCourtActivityType(CourtActivityHistEntity entity) {
        if (entity == null) {
			return null;
		}

        CourtActivityType rtn = new CourtActivityType(entity.getCaseActivityId(), entity.getActivityOccurredDate(), entity.getActivityCategory(),
                entity.getActivityType(), toCommentType(entity), entity.getActivityOutcome(), entity.getOutcomeDate(), entity.getActivityStatus(),
                StaticAssociationHelper.toChargeIds2(entity.getChargeAssociations()),
                //TODO 				 * @param caseIds Set<Long> -  Associations to CaseInfoType objects, at least one. Required
                // * @param activityIds Set<Long> - Associations to either 0 or 2 ActivityType objects. Optional
                // * @param caseAffiliationIds Set<Long> - Associations to CaseAffiliationType objects. Optional
                null, null, null, null, entity.getFacilityAssociation(), entity.getFacilityInternalAssociation(), entity.getFacilityAssociation(), entity.getJudge(),
                StaticAssociationHelper.toOrderInitiatedIds2(entity.getOrderInititatedAssociations()),
                StaticAssociationHelper.toOrderResultedIds2(entity.getOrderResultedAssociations()));

        return rtn;
    }

    /**
     * @param histories     Set<CourtActivityHistEntity>
     * @param currentEntity CourtActivityEntity
     * @return a set of objects of CourtActivityType
     */
    public static Set<CourtActivityType> toCourtActivityTypeSet(Set<CourtActivityHistEntity> histories, CourtActivityEntity currentEntity) {
        Set<CourtActivityType> rtn = new HashSet<CourtActivityType>();

        if (currentEntity != null) {
            CourtActivityType current = toCourtActivityType(currentEntity);
            current.setStamp(BeanHelper.toStamp(currentEntity.getStamp()));
            current.getStamp().setInvocationContext(null);
            rtn.add(current);
        }

        if (histories != null) {
			for (CourtActivityHistEntity historyEntity : histories) {
                CourtActivityType historyType = toCourtActivityType(historyEntity);
                historyType.setStamp(BeanHelper.toStamp(historyEntity.getStamp()));
                historyType.getStamp().setInvocationContext(null);
                rtn.add(historyType);
            }
		}

        return rtn;
    }

    /**
     * @param courtActivityEntities Set<CourtActivityEntity>
     * @param applyPrivilege        Boolean
     * @param userPrivileges        Set<String>
     * @return a set of objects of CourtActivityType
     */
    public static Set<CourtActivityType> toCourtActivityTypeSet(Set<CourtActivityEntity> courtActivityEntities, Boolean applyPrivilege, Set<String> userPrivileges) {
        Set<CourtActivityType> rtn = new HashSet<CourtActivityType>();

        if (courtActivityEntities != null) {
            for (CourtActivityEntity currentEntity : courtActivityEntities) {
                CourtActivityType current = toCourtActivityType(currentEntity);
                current.setStamp(BeanHelper.toStamp(currentEntity.getStamp()));
                current.getStamp().setInvocationContext(null);
                rtn.add(current);
            }
        }

        return rtn;
    }

    /**
     * @param entity CourtActivityEntity
     * @return an object of CommentType from the entity
     */
    public static CommentType toCommentType(CourtActivityEntity entity) {
        CommentType comment = null;
        if (entity.getCommentText() != null && entity.getCommentDate() != null) {
            comment = new CommentType();
            comment.setComment(entity.getCommentText());
            comment.setCommentDate(entity.getCommentDate());
            comment.setCommentIdentification(entity.getCaseActivityId());
            comment.setUserId(entity.getStamp().getModifyUserId());
        }

        return comment;
    }

    /**
     * @param entity CourtActivityEntity
     * @return an object of CommentType from the entity
     */
    public static CommentType toCommentType(CourtActivityHistEntity entity) {
        CommentType comment = null;
        if (entity.getCommentText() != null && entity.getCommentDate() != null) {
            comment = new CommentType();
            comment.setComment(entity.getCommentText());
            comment.setCommentDate(entity.getCommentDate());
            comment.setCommentIdentification(entity.getCaseActivityId());
            comment.setUserId(entity.getStamp().getModifyUserId());
        }

        return comment;
    }

    public static CourtActivityHistEntity toCourtActivityHistoryEntity(CourtActivityEntity existActivityEntity) {
        if (existActivityEntity == null) {
			return null;
		}

        CourtActivityHistEntity rtn = new CourtActivityHistEntity(existActivityEntity.getCaseActivityId(), existActivityEntity.getActivityOccurredDate(),
                existActivityEntity.getActivityCategory(), existActivityEntity.getActivityType(), existActivityEntity.getActivityOutcome(),
                existActivityEntity.getOutcomeDate(), existActivityEntity.getActivityStatus(), existActivityEntity.getCommentText(), existActivityEntity.getCommentDate(),
                null, existActivityEntity.getStamp(), existActivityEntity.getFacilityAssociation(), existActivityEntity.getFacilityInternalAssociation(),
                existActivityEntity.getOrganizationAssociation(), existActivityEntity.getJudge(), null, null, existActivityEntity);

        rtn.setChargeAssociations(StaticAssociationHelper.toChargeAssociationHistEntitySet(null, rtn.getCaseActivityId(), existActivityEntity.getStamp(), rtn,
                existActivityEntity.getChargeAssociations()));
        rtn.setOrderInititatedAssociations(
                StaticAssociationHelper.toOrderInitiatedAssociationHistEntitySet(null, rtn.getCaseActivityId(), existActivityEntity.getStamp(), rtn,
                        existActivityEntity.getOrdersInititated()));
        rtn.setOrderResultedAssociations(
                StaticAssociationHelper.toOrderResultedAssociationHistEntitySet(null, rtn.getCaseActivityId(), existActivityEntity.getStamp(), rtn,
                        existActivityEntity.getOrdersResulted()));

        return rtn;
    }

    //Only call this method when update a persistented entity.
    public static CourtActivityEntity updatePersistentedEntity(CourtActivityEntity entityToUpdate, CourtActivityEntity newEntity, StampEntity createStamp,
            StampEntity modifyStamp) {
        if (!entityToUpdate.equals(newEntity)) {
            entityToUpdate.setCaseActivityId(newEntity.getCaseActivityId());
            entityToUpdate.setActivityOccurredDate(newEntity.getActivityOccurredDate());
            entityToUpdate.setActivityCategory(newEntity.getActivityCategory());
            entityToUpdate.setActivityType(newEntity.getActivityType());
            entityToUpdate.setActivityOutcome(newEntity.getActivityOutcome());
            entityToUpdate.setActivityStatus(newEntity.getActivityStatus());
            entityToUpdate.setCommentText(newEntity.getCommentText());
            entityToUpdate.setCommentDate(newEntity.getCommentDate());

            //entityToUpdate.associations = associations; can not be updated.
            //entityToUpdate.privileges = privileges; can not be updated.

            entityToUpdate.setHistory(newEntity.getHistory());

            entityToUpdate.setFacilityAssociation(newEntity.getFacilityAssociation());
            entityToUpdate.setOrganizationAssociation(newEntity.getOrganizationAssociation());
            entityToUpdate.setJudge(newEntity.getJudge());
            entityToUpdate.setFacilityInternalAssociation(newEntity.getFacilityInternalAssociation());

            entityToUpdate.setStamp(modifyStamp);
            updateStaticAssociations(entityToUpdate, newEntity, createStamp, modifyStamp);
        }

        return entityToUpdate;
    }

    private static void updateStaticAssociations(CourtActivityEntity entityToUpdate, CourtActivityEntity newEntity, StampEntity createStamp, StampEntity modifyStamp) {
        if (newEntity == null) {
			return;
		}

        //Set stamp value
        if (modifyStamp == null) {
			entityToUpdate.setStamp(createStamp);
		} else {
			entityToUpdate.setStamp(modifyStamp);
		}

        //update ChargeAssociations
        entityToUpdate.getChargeAssociations().clear();
        for (CAChargeAssociationEntity srcEntity : newEntity.getChargeAssociations()) {
            if (srcEntity.getAssociationId() == null) {
                srcEntity.setStamp(createStamp);
                srcEntity.setCaseActivity(entityToUpdate);
                entityToUpdate.getChargeAssociations().add(srcEntity);
            }
        }

		/*//update OrderInitAssociations
        entityToUpdate.getOrderInititatedAssociations().clear();
		for (OrderInitiatedAssociationEntity srcEntity: newEntity.getOrderInititatedAssociations()) {
			if (srcEntity.getAssociationId() == null) {
				srcEntity.setStamp(createStamp);
				srcEntity.setCourtActivity(entityToUpdate);
				entityToUpdate.getOrderInititatedAssociations().add(srcEntity);				
			} 			
		}
		
		//update OrderResultedAssociations
		entityToUpdate.getOrderResultedAssociations().clear();
		for (OrderResultedAssociationEntity srcEntity: newEntity.getOrderResultedAssociations()) {
			if (srcEntity.getAssociationId() == null) {
				srcEntity.setStamp(createStamp);
				srcEntity.setCourtActivity(entityToUpdate);
				entityToUpdate.getOrderResultedAssociations().add(srcEntity);				
			} 			
		}*/
    }

}
