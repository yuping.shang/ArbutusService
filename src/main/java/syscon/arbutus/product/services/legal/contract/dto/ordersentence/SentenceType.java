package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;
import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * SentenceType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 28, 2012
 */
@ArbutusConstraint(constraints = "sentenceStartDate <= sentenceEndDate")
public class SentenceType extends OrderType {

    private static final long serialVersionUID = 5043663633432214432L;

    @NotNull
    private String sentenceType;

    private Long sentenceNumber;
    @Valid
    private Set<TermType> sentenceTerms;
    @NotNull
    private String sentenceStatus;
    private Date sentenceStartDate;
    private Date sentenceEndDate;
    private Date sentenceStartTime;
    private Date sentenceEndTime;
    @Valid
    private SentenceAppealType sentenceAppeal;
    private Long concecutiveFrom;
    @Digits(integer = 9, fraction = 2)
    private BigDecimal fineAmount;
    @NotNull
    private Boolean fineAmountPaid;
    private Boolean sentenceAggravateFlag;
    @Size(max = 7, message = "Number of the elements is not more than 7")
    @Valid
    private Set<IntermittentScheduleType> intermittentSchedule;

    private Set<IntermittentSentenceScheduleType> schedules;

    private Set<FinePaymentType> finePayments;

    private long supervisionId;

    /**
     * Constructor
     */
    public SentenceType() {
        super();
    }

    /**
     * Constructor
     *
     * @param orderIdentification                    Long. Not Required for create; Required otherwise. The unique ID for each order created in the system. This is a system-generated ID.
     * @param orderClassification                    String, Required. The order classification. This defines the type of instance of the service.
     * @param orderType                              String, Required. The client defined order type that maps to the order classification.
     * @param orderSubType                           String, Optional. The sub type of an order. For example a want, warrant or detainer.
     * @param orderCategory                          String, Required. The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.
     * @param orderNumber                            String(length <= 64), Optional. User specified order number.
     * @param orderDisposition                       DispositionType, Required. The disposition related to the order.
     * @param comments                               CommentType, Optional. The comments related to the order.
     * @param orderIssuanceDate                      Date, Optional. The date the order was issued.
     * @param orderReceivedDate                      Date, Optional. The date the order was received.
     * @param orderStartDate                         Date, Required. The date/time the order starts(becomes valid).
     * @param orderExpirationDate                    Date, Optional. The date the order expires.
     * @param caseActivityInitiatedOrderAssociations Set&lt;Long>, Optional. The case related activities that initiated this order. Static association to the Case Activity service.
     * @param orderInitiatedCaseActivityAssociations Set&lt;Long>, Optional. The case activities that were initiated by this order. Static association to the Case Activity service.
     * @param isHoldingOrder                         Boolean, Ignored. If true then the order is a holding document, false otherwise.
     * @param isSchedulingNeeded                     Boolean, Ignored. If true, scheduling is needed for the order, false otherwise.
     * @param hasCharges                             Boolean, Ignored. If true, the order has charges related, false otherwise.
     * @param issuingAgency                          NotificationType, Optional. The source agency that issues the order, this could be a facility or an organization.
     * @param sentenceType                           String, Required. The type of sentence (Definite, Split, Life etc.,).
     * @param sentenceNumber                         Long, Required. Identifier for the sentence. Will be used  on the UI to specify the sentence relationship (consecutive).
     * @param sentenceTerms                          Set&lt;TermType>, Optional. The terms and duration for each type of a sentence.
     * @param sentenceStatus                         String, Required. The status of a sentence, will be used by sentence calculation logic. Pending/Included/Excluded. Only ‘Included’ sentences will participate in sentence calculation.
     * @param sentenceStartDate                      Date, Optional. The start date/time for the sentence.
     * @param sentenceEndDate                        Date, Optional. The date on which the sentence ends.
     * @param sentenceAppeal                         SentenceAppealType, Optional. The status of an appeal on a sentence.
     * @param concecutiveSentenceAssociation         Long, Optional. The static reference to the ordersentence service that relates to a sentence that this order (sentence) is consecutive to.
     * @param fineAmount                             BigDecimal, Optional. The fine for a sentence. The unit would be the system wide currency defined.
     * @param fineAmountPaid                         Boolean, Required. If true, Fine amount is paid, false otherwise. Default to false.
     * @param sentenceAggravateFlag                  Boolean, Optional. True if aggravating factors were considered during sentencing, false otherwise.
     * @param intermittentSchedule                   Set&lt;IntermittentScheduleType> [0..7], Optional. Days or parts of days in a week when an intermittent sentence is to be served.
     */
    public SentenceType(Long orderIdentification, @NotNull String orderClassification, @NotNull String orderType, String orderSubType, @NotNull String orderCategory,
            @Size(max = 64, message = "max length 64") String orderNumber, @NotNull DispositionType orderDisposition, CommentType comments, Date orderIssuanceDate,
            Date orderReceivedDate, @NotNull Date orderStartDate, Date orderExpirationDate, Set<Long> caseActivityInitiatedOrderAssociations,
            Set<Long> orderInitiatedCaseActivityAssociations, Boolean isHoldingOrder, Boolean isSchedulingNeeded, Boolean hasCharges, NotificationType issuingAgency,
            @NotNull String sentenceType, @NotNull Long sentenceNumber, Set<TermType> sentenceTerms, @NotNull String sentenceStatus, Date sentenceStartDate,
            Date sentenceEndDate, SentenceAppealType sentenceAppeal, Long concecutiveSentenceAssociation, BigDecimal fineAmount, Boolean fineAmountPaid,
            Boolean sentenceAggravateFlag, @Size(max = 7, message = "Number of the elements is not more than 7") Set<IntermittentScheduleType> intermittentSchedule,
            @NotNull @Size(min = 1, message = "Cardinality [1..*]") Set<Long> caseInfoIds, Set<Long> chargeIds, Set<Long> conditionIds) {
        super(orderIdentification, orderClassification, orderType, orderSubType, orderCategory, orderNumber, orderDisposition, comments, orderIssuanceDate,
                orderReceivedDate, orderStartDate, orderExpirationDate, caseActivityInitiatedOrderAssociations, orderInitiatedCaseActivityAssociations, isHoldingOrder,
                isSchedulingNeeded, hasCharges, issuingAgency, caseInfoIds, chargeIds, conditionIds);
        this.sentenceType = sentenceType;
        this.sentenceNumber = sentenceNumber;
        this.sentenceTerms = sentenceTerms;
        this.sentenceStatus = sentenceStatus;
        this.sentenceStartDate = sentenceStartDate;
        this.sentenceEndDate = sentenceEndDate;
        this.sentenceAppeal = sentenceAppeal;
        this.concecutiveFrom = concecutiveSentenceAssociation;
        this.fineAmount = fineAmount;
        this.fineAmountPaid = fineAmountPaid;
        this.sentenceAggravateFlag = sentenceAggravateFlag;
        this.intermittentSchedule = intermittentSchedule;
    }

    /**
     * Get Sentence Type
     * -- The type of sentence (Definite, Split, Life etc.,).
     *
     * @return the sentenceType
     */
    public String getSentenceType() {
        return sentenceType;
    }

    /**
     * Set Sentence Type
     * -- The type of sentence (Definite, Split, Life etc.,).
     *
     * @param sentenceType the sentenceType to set
     */
    public void setSentenceType(String sentenceType) {
        this.sentenceType = sentenceType;
    }

    /**
     * Get Sentence Number
     * -- Identifier for the sentence. Will be used  on the UI to specify the sentence relationship (concecutive).
     *
     * @return the sentenceNumber
     */
    public Long getSentenceNumber() {
        return sentenceNumber;
    }

    /**
     * Set Sentence Number
     * -- Identifier for the sentence. Will be used  on the UI to specify the sentence relationship (concecutive).
     *
     * @param sentenceNumber the sentenceNumber to set
     */
    public void setSentenceNumber(Long sentenceNumber) {
        this.sentenceNumber = sentenceNumber;
    }

    /**
     * Get Sentence Terms
     * -- The terms and duration for each type of a sentence.
     *
     * @return the sentenceTerms
     */
    public Set<TermType> getSentenceTerms() {
        return sentenceTerms;
    }

    /**
     * Set Sentence Terms
     * -- The terms and duration for each type of a sentence.
     *
     * @param sentenceTerms the sentenceTerms to set
     */
    public void setSentenceTerms(Set<TermType> sentenceTerms) {
        this.sentenceTerms = sentenceTerms;
    }

    /**
     * Get Sentence Status
     * -- The status of a sentence, will be used by sentence calculation logic. Pending/Included/Excluded. Only ‘Included’ sentences will participate in sentence calculation.
     *
     * @return the sentenceStatus
     */
    public String getSentenceStatus() {
        return sentenceStatus;
    }

    /**
     * Set Sentence Status
     * -- The status of a sentence, will be used by sentence calculation logic. Pending/Included/Excluded. Only ‘Included’ sentences will participate in sentence calculation.
     *
     * @param sentenceStatus the sentenceStatus to set
     */
    public void setSentenceStatus(String sentenceStatus) {
        this.sentenceStatus = sentenceStatus;
    }

    /**
     * Get Sentence Start Date
     * -- The start date/time for the sentence.
     *
     * @return the sentenceStartDate
     */
    public Date getSentenceStartDate() {
        return sentenceStartDate;
    }

    /**
     * Set Sentence Start Date
     * -- The start date/time for the sentence.
     *
     * @param sentenceStartDate the sentenceStartDate to set
     */
    public void setSentenceStartDate(Date sentenceStartDate) {
        this.sentenceStartDate = sentenceStartDate;
    }

    /**
     * Get Sentence End Date
     * -- The date on which the sentence ends.
     *
     * @return the sentenceEndDate
     */
    public Date getSentenceEndDate() {
        return sentenceEndDate;
    }

    /**
     * Set Sentence End Date
     * -- The date on which the sentence ends.
     *
     * @param sentenceEndDate the sentenceEndDate to set
     */
    public void setSentenceEndDate(Date sentenceEndDate) {
        this.sentenceEndDate = sentenceEndDate;
    }

    /**
     * Get Sentence Appeal
     * -- The status of an appeal on a sentence.
     *
     * @return the sentenceAppeal
     */
    public SentenceAppealType getSentenceAppeal() {
        return sentenceAppeal;
    }

    /**
     * Set Sentence Appeal
     * -- The status of an appeal on a sentence.
     *
     * @param sentenceAppeal the sentenceAppeal to set
     */
    public void setSentenceAppeal(SentenceAppealType sentenceAppeal) {
        this.sentenceAppeal = sentenceAppeal;
    }

    /**
     * Get Consecutive Sentence Association
     * -- The static reference to the ordersentence service that relates to a sentence that this order (sentence) is consecutive to.
     *
     * @return the concecutiveSentenceAssociation
     */
    public Long getConcecutiveFrom() {
        return concecutiveFrom;
    }

    /**
     * Set Consecutive Sentence Association
     * -- The static reference to the ordersentence service that relates to a sentence that this order (sentence) is consecutive to.
     *
     * @param concecutiveFrom the concecutiveSentenceAssociation to set
     */
    public void setConcecutiveFrom(Long concecutiveFrom) {
        this.concecutiveFrom = concecutiveFrom;
    }

    /**
     * Get Fine Amount
     * -- The fine for a sentence. The unit would be the system wide currency defined.
     *
     * @return the fineAmount
     */
    public BigDecimal getFineAmount() {
        return fineAmount;
    }

    /**
     * Set Fine Amount
     * -- The fine for a sentence. The unit would be the system wide currency defined.
     *
     * @param fineAmount the fineAmount to set
     */
    public void setFineAmount(BigDecimal fineAmount) {
        this.fineAmount = fineAmount;
    }

    /**
     * Get Fine Amount Paid
     * -- If true, Fine amount is paid, false otherwise. Default to false.
     *
     * @return the fineAmountPaid
     */
    public Boolean getFineAmountPaid() {
        return fineAmountPaid;
    }

    /**
     * Set Fine Amount Paid
     * -- If true, Fine amount is paid, false otherwise. Default to false.
     *
     * @param fineAmountPaid the fineAmountPaid to set
     */
    public void setFineAmountPaid(Boolean fineAmountPaid) {
        this.fineAmountPaid = fineAmountPaid;
    }

    /**
     * Get Sentence Aggravate Flag
     * -- True if aggravating factors were considered during sentencing, false otherwise.
     *
     * @return the sentenceAggravateFlag
     */
    public Boolean getSentenceAggravateFlag() {
        return sentenceAggravateFlag;
    }

    /**
     * Set Sentence Aggravate Flag
     * -- True if aggravating factors were considered during sentencing, false otherwise.
     *
     * @param sentenceAggravateFlag the sentenceAggravateFlag to set
     */
    public void setSentenceAggravateFlag(Boolean sentenceAggravateFlag) {
        this.sentenceAggravateFlag = sentenceAggravateFlag;
    }

    /**
     * Get Intermittent Schedule
     * -- Days or parts of days in a week when an intermittent sentence is to be served.
     *
     * @return the intermittentSchedule
     */
    public Set<IntermittentScheduleType> getIntermittentSchedule() {
        return intermittentSchedule;
    }

    /**
     * Set Intermittent Schedule
     * -- Days or parts of days in a week when an intermittent sentence is to be served.
     *
     * @param intermittentSchedule the intermittentSchedule to set
     */
    public void setIntermittentSchedule(Set<IntermittentScheduleType> intermittentSchedule) {
        this.intermittentSchedule = intermittentSchedule;
    }

    /**
     * @return the schedules
     */
    public Set<IntermittentSentenceScheduleType> getSchedules() {
        return schedules;
    }

    /**
     * @param schedules the schedules to set
     */
    public void setSchedules(Set<IntermittentSentenceScheduleType> schedules) {
        this.schedules = schedules;
    }

    /**
     * @return the supervisionId
     */
    public long getSupervisionId() {
        return supervisionId;
    }

    /**
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Get collection of Fine Payments.
     *
     * @return the finePayments Set<FinePaymentType>
     */
    public Set<FinePaymentType> getFinePayments() {
        return finePayments;
    }

    /**
     * Set collection of Fine Payments.
     *
     * @param Set<FinePaymentType> the finePayments to set
     */
    public void setFinePayments(Set<FinePaymentType> finePayments) {
        this.finePayments = finePayments;
    }

    /**
     * @return the sentenceStartTime
     */
    public Date getSentenceStartTime() {
        return sentenceStartTime;
    }

    /**
     * @param sentenceStartTime the sentenceStartTime to set
     */
    public void setSentenceStartTime(Date sentenceStartTime) {
        this.sentenceStartTime = sentenceStartTime;
    }

    /**
     * @return the sentenceEndTime
     */
    public Date getSentenceEndTime() {
        return sentenceEndTime;
    }

    /**
     * @param sentenceEndTime the sentenceEndTime to set
     */
    public void setSentenceEndTime(Date sentenceEndTime) {
        this.sentenceEndTime = sentenceEndTime;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((concecutiveFrom == null) ? 0 : concecutiveFrom.hashCode());
        result = prime * result + ((fineAmountPaid == null) ? 0 : fineAmountPaid.hashCode());
        result = prime * result + ((intermittentSchedule == null) ? 0 : intermittentSchedule.hashCode());
        result = prime * result + ((schedules == null) ? 0 : schedules.hashCode());
        result = prime * result + ((sentenceAggravateFlag == null) ? 0 : sentenceAggravateFlag.hashCode());
        result = prime * result + ((sentenceAppeal == null) ? 0 : sentenceAppeal.hashCode());
        result = prime * result + ((sentenceEndDate == null) ? 0 : sentenceEndDate.hashCode());
        result = prime * result + ((sentenceEndTime == null) ? 0 : sentenceEndTime.hashCode());
        result = prime * result + ((sentenceNumber == null) ? 0 : sentenceNumber.hashCode());
        result = prime * result + ((sentenceStartDate == null) ? 0 : sentenceStartDate.hashCode());
        result = prime * result + ((sentenceStartTime == null) ? 0 : sentenceStartTime.hashCode());
        result = prime * result + ((sentenceStatus == null) ? 0 : sentenceStatus.hashCode());
        result = prime * result + ((sentenceTerms == null) ? 0 : sentenceTerms.hashCode());
        result = prime * result + ((sentenceType == null) ? 0 : sentenceType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SentenceType other = (SentenceType) obj;
        if (concecutiveFrom == null) {
            if (other.concecutiveFrom != null) {
				return false;
			}
        } else if (!concecutiveFrom.equals(other.concecutiveFrom)) {
			return false;
		}
        if (fineAmountPaid == null) {
            if (other.fineAmountPaid != null) {
				return false;
			}
        } else if (!fineAmountPaid.equals(other.fineAmountPaid)) {
			return false;
		}
        if (intermittentSchedule == null) {
            if (other.intermittentSchedule != null) {
				return false;
			}
        } else if (!intermittentSchedule.equals(other.intermittentSchedule)) {
			return false;
		}
        if (schedules == null) {
            if (other.schedules != null) {
				return false;
			}
        } else if (!schedules.equals(other.schedules)) {
			return false;
		}
        if (sentenceAggravateFlag == null) {
            if (other.sentenceAggravateFlag != null) {
				return false;
			}
        } else if (!sentenceAggravateFlag.equals(other.sentenceAggravateFlag)) {
			return false;
		}
        if (sentenceAppeal == null) {
            if (other.sentenceAppeal != null) {
				return false;
			}
        } else if (!sentenceAppeal.equals(other.sentenceAppeal)) {
			return false;
		}
        if (sentenceEndDate == null) {
            if (other.sentenceEndDate != null) {
				return false;
			}
        } else if (!sentenceEndDate.equals(other.sentenceEndDate)) {
			return false;
		}
        if (sentenceEndTime == null) {
            if (other.sentenceEndTime != null) {
				return false;
			}
        } else if (!sentenceEndTime.equals(other.sentenceEndTime)) {
			return false;
		}
        if (sentenceNumber == null) {
            if (other.sentenceNumber != null) {
				return false;
			}
        } else if (!sentenceNumber.equals(other.sentenceNumber)) {
			return false;
		}
        if (sentenceStartDate == null) {
            if (other.sentenceStartDate != null) {
				return false;
			}
        } else if (!sentenceStartDate.equals(other.sentenceStartDate)) {
			return false;
		}
        if (sentenceStartTime == null) {
            if (other.sentenceStartTime != null) {
				return false;
			}
        } else if (!sentenceStartTime.equals(other.sentenceStartTime)) {
			return false;
		}
        if (sentenceStatus == null) {
            if (other.sentenceStatus != null) {
				return false;
			}
        } else if (!sentenceStatus.equals(other.sentenceStatus)) {
			return false;
		}
        if (sentenceTerms == null) {
            if (other.sentenceTerms != null) {
				return false;
			}
        } else if (!sentenceTerms.equals(other.sentenceTerms)) {
			return false;
		}
        if (sentenceType == null) {
            if (other.sentenceType != null) {
				return false;
			}
        } else if (!sentenceType.equals(other.sentenceType)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SentenceType [sentenceType=" + sentenceType + ", sentenceNumber=" + sentenceNumber + ", sentenceStatus=" + sentenceStatus + ", sentenceStartDate="
                + sentenceStartDate + ", sentenceEndDate=" + sentenceEndDate + ", concecutiveSentenceAssociation=" + concecutiveFrom + ", fineAmount=" + fineAmount
                + ", fineAmountPaid=" + fineAmountPaid + ", sentenceAggravateFlag=" + sentenceAggravateFlag + ", toString()=" + super.toString() + "]";
    }

}
