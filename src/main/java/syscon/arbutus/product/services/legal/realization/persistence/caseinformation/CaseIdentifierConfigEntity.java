package syscon.arbutus.product.services.legal.realization.persistence.caseinformation;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the case information identifier configuration entity.
 *
 * @DbComment LEG_CIIdentifierConfig 'The case information identifier configuration table'
 */
@Audited
@Entity
@Table(name = "LEG_CIIdentifierConfig")
public class CaseIdentifierConfigEntity implements Serializable {

    private static final long serialVersionUID = 5390157693979654125L;

    /**
     * @DbComment IdentifierConfigId 'Unique Id for each case identifier configuration record'
     */
    @Id
    @Column(name = "IdentifierConfigId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CIIdentifierConfig_Id")
    @SequenceGenerator(name = "SEQ_LEG_CIIdentifierConfig_Id", sequenceName = "SEQ_LEG_CIIdentifierConfig_Id", allocationSize = 1)
    private Long identifierConfigId;

    /**
     * @DbComment IdentifierType 'A kind of identifier or number that can be associated to a criminal case e.g. Court Information Number, Indictment Number'
     */
    @MetaCode(set = MetaSet.CASE_IDENTIFIER_TYPE)
    @Column(name = "IdentifierType", length = 64, nullable = false)
    private String identifierType;

    /**
     * @DbComment IdentifierFormat 'The Format used when auto-generating case numbers'
     */
    @Column(name = "IdentifierFormat", length = 64, nullable = true)
    private String identifierFormat;

    /**
     * @DbComment AutoGenerate 'If true, the system auto generates the case number using the format specified by the IdentifierFormat.'
     */
    @Column(name = "AutoGenerate", nullable = true)
    private Boolean autoGenerate;

    /**
     * @DbComment Active 'To be set appropriately to ensure that only one configuration per identifier type is active at any time'
     */
    @Column(name = "Active", nullable = false)
    private Boolean active;

    /**
     * @DbComment IsPrimary 'Indicates if the identifier is primary. If an identifier is flagged as primary, it is displayed with the summary information on any UI, default to false'
     */
    @Column(name = "IsPrimary", nullable = false)
    private Boolean primary;

    /**
     * @DbComment DuplicateCheck 'Defaulted to false. the identifier, If set to true, then this identifier participates in the uniqueness check for a legal entry.'
     */
    @Column(name = "DuplicateCheck", nullable = true)
    private Boolean duplicateCheck;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     *
     */
    public CaseIdentifierConfigEntity() {
    }

    /**
     * @param identifierConfigId
     * @param identifierType
     * @param identifierFormat
     * @param autoGenerate
     * @param active
     * @param primary
     * @param duplicateCheck
     */
    public CaseIdentifierConfigEntity(Long identifierConfigId, String identifierType, String identifierFormat, Boolean autoGenerate, Boolean active, Boolean primary,
            Boolean duplicateCheck) {
        this.identifierConfigId = identifierConfigId;
        this.identifierType = identifierType;
        this.identifierFormat = identifierFormat;
        this.autoGenerate = autoGenerate;
        this.active = active;
        this.primary = primary;
        this.duplicateCheck = duplicateCheck;
    }

    /**
     * @return the identifierConfigId
     */
    public Long getIdentifierConfigId() {
        return identifierConfigId;
    }

    /**
     * @param identifierConfigId the identifierConfigId to set
     */
    public void setIdentifierConfigId(Long identifierConfigId) {
        this.identifierConfigId = identifierConfigId;
    }

    /**
     * @return the identifierType
     */
    public String getIdentifierType() {
        return identifierType;
    }

    /**
     * @param identifierType the identifierType to set
     */
    public void setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
    }

    /**
     * @return the identifierFormat
     */
    public String getIdentifierFormat() {
        return identifierFormat;
    }

    /**
     * @param identifierFormat the identifierFormat to set
     */
    public void setIdentifierFormat(String identifierFormat) {
        this.identifierFormat = identifierFormat;
    }

    /**
     * @return the autoGenerate
     */
    public Boolean isAutoGenerate() {
        return autoGenerate;
    }

    /**
     * @param autoGenerate the autoGenerate to set
     */
    public void setAutoGenerate(Boolean autoGenerate) {
        this.autoGenerate = autoGenerate;
    }

    /**
     * @return the active
     */
    public Boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the primary
     */
    public Boolean isPrimary() {
        return primary;
    }

    /**
     * @param primary the primary to set
     */
    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

    /**
     * @return the duplicateCheck
     */
    public Boolean isDuplicateCheck() {
        return duplicateCheck;
    }

    /**
     * @param duplicateCheck the duplicateCheck to set
     */
    public void setDuplicateCheck(Boolean duplicateCheck) {
        this.duplicateCheck = duplicateCheck;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseIdentifierConfigEntity [identifierConfigId=" + identifierConfigId + ", identifierType=" + identifierType + ", identifierFormat=" + identifierFormat
                + ", autoGenerate=" + autoGenerate + ", active=" + active + ", primary=" + primary + ", duplicateCheck=" + duplicateCheck + ", stamp=" + stamp
                + ", version=" + version + "]";
    }

}
