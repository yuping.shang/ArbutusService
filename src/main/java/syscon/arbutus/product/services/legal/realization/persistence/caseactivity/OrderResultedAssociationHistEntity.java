package syscon.arbutus.product.services.legal.realization.persistence.caseactivity;

import javax.persistence.*;

import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * OrderResultedAssociationHistEntity for Case Activity Service
 *
 * @author lhan
 * @version 1.0
 */
//@Audited
@Entity
@DiscriminatorValue("OrderResultAssocHist")
public class OrderResultedAssociationHistEntity extends CaseActivityStaticAssocHistEntity {

    private static final long serialVersionUID = 6918320597578729309L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CaseActivityHistId", nullable = false)
    private CourtActivityHistEntity caseActivity;

    /**
     * Constructor
     */
    public OrderResultedAssociationHistEntity() {
        super();
    }

    /**
     * Constructor.
     *
     * @param associationId
     * @param fromIdentifier
     * @param toIdentifier
     * @param stamp
     * @param caseActivity
     */
    public OrderResultedAssociationHistEntity(Long associationId, Long fromIdentifier, Long toIdentifier, StampEntity stamp, CourtActivityHistEntity caseActivity) {
        super(associationId, fromIdentifier, toIdentifier, stamp);
        this.caseActivity = caseActivity;
    }

    /**
     * @param courtActivity the CourtActivityHistEntity to set
     */
    public void setCaseActivityHist(CourtActivityHistEntity caseActivity) {
        this.caseActivity = caseActivity;
    }

    /**
     * @return the courtActivityHistEntity
     */
    public CourtActivityHistEntity getCaseActivity() {
        return caseActivity;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
	 */
    @Override
    public String toString() {
        return "OrderResultedAssociationHistEntity [getAssociationId()=" + getAssociationId() + ", getFromIdentifier()=" + getFromIdentifier() + ", getToIdentifier()="
                + getToIdentifier() + ", getStamp()=" + getStamp() + "]";
    }

}