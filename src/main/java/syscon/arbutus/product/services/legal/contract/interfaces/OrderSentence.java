package syscon.arbutus.product.services.legal.contract.interfaces;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.legal.contract.dto.ordersentence.*;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.FinePaymentReturnType;
import syscon.arbutus.product.services.sentencecalculation.contract.dto.Sentence;

/**
 * Business contract interface for Order Sentence related actions.
 * <p><b>Purpose</b>
 * The ORDER SENTENCE service provides the capability to record and maintain all legal order information for an offender.
 * This includes warrants, wants and holds, detainers, remands, sentencing orders, parole/probation orders,
 * parole/probation breach orders, and discharge orders. The service also holds information about sentence terms,
 * credits and debits that will be used for offender sentence calculation.
 * <p>This service maintains a close association to the CASE ACTIVITY service.
 * This association is based on orders that are issued for activities and events that affect an offender’s legal status.
 * This includes but is not limited to arrest events, court hearings, and parole hearings.
 * The association from ORDER SENTENCE to the CASE ACTIVITY service also signifies the orders
 * that initiated the case activities (e.g. next court date on a remand order).
 * <p>All conditions imposed as part of an order are recorded as an association to the CONDITION service.
 * <p>Bail/bond details are maintained within this service.
 * <p><b>Scope</b>
 * The ORDER SENTENCE service is limited to the following scope for the first phase of the service:
 * <pre>
 * <li>Record, search and maintain the following orders:
 *  <ol>Outside jurisdiction Warrants/Holds/Wants</ol>
 * 	<ol>Bail/bond information</ol>
 *  <ol>Legal orders (e.g. Take Out Order, Medical Order, Discharge Order)</ol>
 *  <ol>Detainers issued by the court to hold an offender in custody</ol>
 *  <ol>Sentence details</ol>
 * </li>
 * <li>Record and maintain sentence terms</li>
 * <li>Configure and maintain outcomes and reasons that drive the order status</li>
 * <li>Maintain the status of a sentence appeal</li>
 * <li>Record and maintain the association to the activities that are driven by an order</li>
 * <li>Record and maintain the association to the activities that initiated an order</li>
 * <li>Delete orders</li>
 * <li>Search and retrieval of history of changes on an order</li>
 * <li>Seal sentences as directed by court order(s);
 * seal functionality is available to users with appropriate user data privileges</li>
 * </pre>
 * <p>Class diagram of Condition module:
 * <br/><img src="doc-files/CaseManagement-OrderSentence-classdiagram.JPG" />
 * </p>
 *
 * @author yshang
 * @version 1.0
 * @since January 2, 2013
 */
public interface OrderSentence {

    /**
     * Get/Retrieve an order
     *
     * @param uc                  {@link UserContext}
     * @param orderIdentification Long, Required. Order Identification.
     * @return Extended OrderType. An instance is returned when success;
     * throws &lt;T extends ArbutusRuntimeException> when failure.
     */
    public <T extends OrderType> T getOrder(@NotNull UserContext uc, @NotNull Long orderIdentification);

    /**
     * Retrieve history for a particular order based on a date range
     * <pre>
     *  <li>Associations to the historical order record must also be returned.</li>
     *  <li>Stamp Type details for all objects that extend stamp type must be returned.</li>
     * </pre>
     *
     * @param uc                  {@link UserContext}
     * @param orderIdentification Long, Required. Order(Bail, Legal Order, Sentence, or Warrant Detainer) Identification.
     * @param fromHistoryDate     Date, Optional. From history date, the date order start date or order issuance date from.
     * @param toHistoryDate       Date, Optional. To history date, the date order start date or order issuance date to.
     * @return List&lt;T> &lt;T extends OrderType>. Historical records for the order based on the historical dates are returned;
     * returns empty list when no instances could be found;
     * throws &lt;T extends ArbutusRuntimeException> when error occurs.
     */
    public <T extends OrderType> List<T> retrieveOrderHistory(@NotNull UserContext uc, @NotNull Long orderIdentification, Date fromHistoryDate, Date toHistoryDate);

    /**
     * Get a list of orders for the supervision from Outside Jurisdiction
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long Required. Supervision Id
     * @return List&lt;T extends OrderType> A list of orders for the supervision from Outside Jurisdiction;
     * returns empty list if no order found for this supervision;
     * throws &lt;T extends ArbutusRuntimeException> when error occurs.
     */
    public <T extends OrderType> List<T> getOrderBySupervisionIdForOutJurisdiction(@NotNull UserContext uc, @NotNull Long supervisionId, Boolean historyFlag);

    /**
     * Retrieve a list of sentences for a supervision
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long Required. Supervision Id
     * @return List&lt;SentenceType> A list of Sentences for the supervision;
     * returns empty list if no order found for this supervision;
     * throws &lt;T extends ArbutusRuntimeException> when error occurs.
     */
    public List<SentenceType> retrieveSentencesBySupervisionId(@NotNull UserContext uc, @NotNull Long supervisionId);

    /**
     * Retrieve Supervision Sentence
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long Required. Supervision Id
     * @return SupervisionSentenceType Instance of SupervisionSentenceType;
     * throws &lt;T extends ArbutusRuntimeException> when error occurs.
     */
    public SupervisionSentenceType retrieveSupervisionSentence(@NotNull UserContext uc, @NotNull Long supervisionId);

    /**
     * Retrieve Supervision Sentence With History
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long Required. Supervision Id
     * @return List&lt;SupervisionSentenceType> A list of SupervisionSentenceType;
     * throws &lt;T extends ArbutusRuntimeException> when error occurs.
     */
    public List<SupervisionSentenceType> retrieveSupervisionSentenceWithHistory(@NotNull UserContext uc, @NotNull Long supervisionId);

    /**
     * Retrieve Supervision Sentence With History By KeyDateHistoryId
     *
     * @param uc                       {@link UserContext}
     * @param supervisionId            Long Required. Supervision Id
     * @param sentenceKeyDateHistoryId Long Required. sentenceKeyDateHistoryId
     * @return List&lt;SupervisionSentenceType> A list of SupervisionSentenceType;
     * throws &lt;T extends ArbutusRuntimeException> when error occurs.
     */
    public List<SupervisionSentenceType> retrieveSupervisionSentenceWithHistoryByKeyDateHistoryId(@NotNull UserContext uc, @NotNull Long supervisionId,
            @NotNull Long sentenceKeyDateHistoryId);

    /**
     * Retrieve a list of Aggregate Sentences Group By Supervision Id
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long Required. Supervision Id
     * @return List&lt;AggregateSentenceGroupType> A group of Aggregated Sentences;
     * throws &lt;T extends ArbutusRuntimeException> when error occurs.
     */
    public List<AggregateSentenceGroupType> retrieveAggregateSentencesBySupervisionId(@NotNull UserContext uc, @NotNull Long supervisionId);

    /**
     * Retrieve a list of Aggregate Sentences Groups with History By Supervision Id
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long Required. Supervision Id
     * @return List&lt;AggregateSentenceGroupType> A group of Aggregated Sentences;
     * throws &lt;T extends ArbutusRuntimeException> when error occurs.
     */
    public List<AggregateSentenceGroupType> retrieveAggregateSentencesWithHistoryBySupervisionId(@NotNull UserContext uc, @NotNull Long supervisionId);

    /**
     * Retrieve all orders for a case
     * <pre>
     * 	<li>Case must exist</li>
     *  <li>Case must have orders associated</li>
     *  <li>IsActiveFlag:
     *   <ol>If NULL =  return all active and inactive orders</ol>
     *   <ol>If TRUE =  return all active orders</ol>
     *   <ol>Otherwise  FALSE = return all inactive orders</ol>
     *  </li>
     *  <li>Stamp Type details for all objects that extend stamp type must be returned.</li>
     * </pre>
     *
     * @param uc                 {@link UserContext}
     * @param caseIdentification Long, Required. Case Identification.
     * @param orderSearch        {@link OrderSearchType}, Optional. Criteria on the orders.
     * @param isActiveFlag       Boolean, Optional.
     * @return List&lt;T> &lt;T extends OrderType>. Orders for the specified case associations are returned;
     * returns empty list when no instances could be found;
     * throws &lt;T extends ArbutusRuntimeException> when error occurs.
     */
    public <T extends OrderType> List<T> retrieveOrdersForCase(@NotNull UserContext uc, @NotNull Long caseIdentification, OrderSearchType orderSearch,
            Boolean isActiveFlag);

    /**
     * Retrieve all orders for a charge
     * <p><pre>
     * <li>IsActiveFlag:
     *  <ol>If NULL =  return all active and inactive orders</ol>
     *  <ol>If TRUE =  return all active orders</ol>
     *  <ol>Otherwise  FALSE = return all inactive orders</ol>
     * </li>
     * <li>Stamp Type details for all objects that extend stamp type must be returned.</li>
     * </pre>
     *
     * @param uc                    {@link UserContext}
     * @param chargeIdentifications Set&lt;Long>, Required. Charge Identifications. Charge must have orders associated.
     * @param orderSearch           {@link OrderSearchType}, Optional. Criteria on the orders.
     * @param isActiveFlag          Boolean, Optional.
     * @return List&lt;T> &lt;T extends OrderType>. Orders for the specified charge associations are returned;
     * returns empty list when no instances could be found;
     * throws &lt;T extends ArbutusRuntimeException> when error occurs.
     */
    public <T extends OrderType> List<T> retrieveOrdersForCharge(@NotNull UserContext uc, @NotNull Set<Long> chargeIdentifications, OrderSearchType orderSearch,
            Boolean isActiveFlag);

    /**
     * Retrieve Disposition History By Order Id
     *
     * @param uc              {@link UserContext}
     * @param orderId         Long, Required. Identification of Order(Bail, Legal Order or Warrant Detainer)
     * @param fromHistoryDate Date, Optional. From history date, the date disposition date from.
     * @param toHistoryDate   Date, Optional. To history date, the date disposition date to.
     * @return List&lt;DispositionType> Disposition of the Order(Bail, Legal Order or Warrant Detainer);
     * returns empty list when no instances could be found;
     * throws &lt;T extends ArbutusRuntimeException> when error occurs.
     */
    public List<DispositionType> retrieveDispositionHistoryByOrderId(@NotNull UserContext uc, @NotNull Long orderId, Date fromHistoryDate, Date toHistoryDate);

    /**
     * Search for orders
     * <li>If History flag = false/null the last order record (current record) is searched</li>
     * <li>If History flag = true the complete history of order records are searched (current and historic records).
     * The records returned are always the most current</li>
     * <li>All associations must be returned</li>
     * <li>Stamp Type details for all objects that extend stamp type must be returned.</li>
     * <p>At least one property in must be provided in OrderSearchType
     * if none of orderIdentifications, caseIdentifications, chargeIdentifications or conditionIdentifications is provided.
     *
     * @param uc                       {@link UserContext}
     * @param orderIdentifications     List&lt;Long>, Order Identifications
     * @param caseIdentifications      List&lt;Long>, Case Identifications
     * @param chargeIdentifications    List&lt;Long>, Charge Identifications
     * @param conditionIdentifications List&lt;Long>, Condition Identifications
     * @param orderSearch              OrderSearchType
     * @param historyFlag              Boolean, Optional. history returns if historyFlag = True; none of history returns otherwise.
     * @param startIndex               Long – Optional if null search returns at the start index
     * @param resultSize               Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder              String – Optional if null search returns a default order
     * @return the orders(may include Bails, Legals, Sentences and WarrantDetainers) instances.
     * throws &lt;T extends ArbutusRuntimeException> if there is an error.
     * return empty list when no instances could be found.
     */
    @SuppressWarnings("rawtypes")
    public OrdersReturnType searchOrders(@NotNull UserContext uc, Set<Long> orderIdentifications, Set<Long> caseIdentifications, Set<Long> chargeIdentifications,
            Set<Long> conditionIdentifications, OrderSearchType orderSearch, Boolean historyFlag, Long startIndex, Long resultSize, String resultOrder);

    /**
     * Search for existing orders
     * <li>If History flag = false/null the last order record (current record) is searched</li>
     * <li>If History flag = true the complete history of order records are searched (current and historic records).
     * The records returned are always the most current</li>
     * <li>All associations must be returned</li>
     * <li>Stamp Type details for all objects that extend stamp type must be returned.</li>
     *
     * @param uc                      {@link UserContext}
     * @param caseIdentification      Long, Optional. Case Identification.
     * @param chargeIdentification    Long, Optional. Charge Identification.
     * @param conditionIdentification Long, Optional. Condition Identification.
     * @param orderSearch             {@link OrderSearchType}, Required. Criteria on the orders.
     * @param historyFlag             Boolean, Optional.
     * @return List&lt;T> T extends OrderType, Order(s) that match the search criteria are returned;
     * If any of the associations are provided as search criteria then only those orders
     * that are related to the associated business object be returned;
     * returns empty list when no instances could be found;
     * throws &lt;T extends ArbutusRuntimeException> when error occurs.
     */
    @Deprecated
    public <T extends OrderType> List<T> searchOrders(@NotNull UserContext uc, Long caseIdentification, Long chargeIdentification, Long conditionIdentification,
            OrderSearchType orderSearch, Boolean historyFlag);

    /**
     * Delete an order
     *
     * @param uc                  {@link UserContext}
     * @param orderIdentification Long, Required. Order Identification.
     * @return Long - Return Code (1L) if success; throws ArbutusRuntimeException otherwise.
     */
    public Long deleteOrder(@NotNull UserContext uc, @NotNull Long orderIdentification);

    /**
     * Creates a new warrant or detainer
     * <li>Case to which the order is to be associated must exist.</li>
     * <li>If IsNotificationNeeded flag is set to true,
     * at least one 'Notify organization/facility' must be provided for the AgencyToBeNotified.<li>
     * <li>See Appendix B for more information on the business modeling</li>
     * <li>Dynamic Element Group Name is 'ORDERATTRIBUTE'.</li>
     * <li>Based on the 'OrderClassification', 'OrderType' and 'OrderCategory' fields
     * the following field values are copied over from the 'OrderConfiguration' to the WarrantDetainer</li>
     * <li>IsHoldingOrder</li>
     * <li>IsSchedulingNeeded</li>
     * <li>HasCharges</li>
     * <li>Based on the 'OrderClassification', 'OrderType' and 'OrderCategory' fields
     * the 'Issuing Agency' must be part of the list of issuing agencies specified in the order configuration.</li>
     * <li>The dynamic attributes stored against the WarrantDetainer must be restricted to
     * those dynamic attributes defined in the Order Configuration Type for the same 'OrderClassification',
     * 'Order Type' and 'OrderCategory' combination as the WarrantDetainer</li>
     *
     * @param uc              {@link UserContext}
     * @param warrantDetainer {@link WarrantDetainerType}, Required.
     * @return WarrantDetainerReturnType WarrantDetainer instance when success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public WarrantDetainerType createWarrantDetainer(@NotNull UserContext uc, @NotNull WarrantDetainerType warrantDetainer);

    /**
     * Update a WarrantDetainer
     * <li>Case to which the order is to be associated must exist.</li>
     * <li>If IsNotificationNeeded flag is set to true, at least one 'Notify organization/facility'
     * must be provided for the AgencyToBeNotified.</li>
     * <li>See Appendix B for more information on the business modeling</li>
     * <li>Dynamic Element Group Name is 'ORDERATTRIBUTE'.</li>
     * <li>Based on the 'OrderClassification', 'OrderType' and 'OrderCategory' fields the following
     * field values are copied over from the 'OrderConfiguration' to the WarrantDetainer
     * <ol>IsHoldingOrder</ol>
     * <ol>IsSchedulingNeeded</ol>
     * <ol>HasCharges</ol>
     * </li>
     * <li>The dynamic attributes stored against the WarrantDetainer must be restricted
     * to those dynamic attributes defined in the Order Configuration Type for the same
     * 'OrderClassification', 'Order Type' and 'OrderCategory' combination as the WarrantDetainer</li>
     *
     * @param uc              {@link UserContext}
     * @param warrantDetainer {@link WarrantDetainerType}, Required.
     * @return WarrantDetainerReturnType WarrantDetainer instance when success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public WarrantDetainerType updateWarrantDetainer(@NotNull UserContext uc, @NotNull WarrantDetainerType warrantDetainer);

    /**
     * Creates a new bail type order
     * <li>Case to which the order is to be associated must exist.</li>
     * <li>Dynamic Element Group Name is 'ORDERATTRIBUTE'.</li>
     * <li>Based on the 'OrderClassification', 'OrderType' and 'OrderCategory' fields
     * the following fields values are copied over from the 'OrderConfiguration' to the Bail Order.
     * <ol>IsHoldingOrder</ol>
     * <ol>IsSchedulingNeeded</ol>
     * <ol>HasCharges</ol>
     * </li>
     * <li>The dynamic attributes stored against the Bail order must be restricted
     * to those dynamic attributes defined in the Order Configuration Type for the same 'OrderClassification',
     * 'Order Type' and 'OrderCategory' combination as the Bail order.</li>
     *
     * @param uc   {@link UserContext}
     * @param bail {@link BailType}, Required.
     * @return BailReturnType Bail instance when success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public BailType createBail(@NotNull UserContext uc, @NotNull BailType bail);

    /**
     * Update a bail type order
     * <li>Order must exist.</li>
     * <li>Dynamic Element Group Name is 'ORDERATTRIBUTE'.</li>
     * <li>The following fields  cannot be updated
     * <ol>IsHoldingOrder</ol>
     * <ol>IsSchedulingNeeded</ol>
     * <ol>HasCharges</ol>
     * </li>
     * <li>The dynamic attributes stored against the Bail order must be restricted
     * to those dynamic attributes defined in the Order Configuration Type for the
     * same 'OrderClassification', 'Order Type' and 'OrderCategory' combination as
     * the Bail orderAll associations are updatable.</li>
     *
     * @param uc   {@link UserContext}
     * @param bail {@link BailType}, Required.
     * @return BailReturnType Bail instance when success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public BailType updateBail(@NotNull UserContext uc, @NotNull BailType bail);

    /**
     * Creates a new legal order type
     * <li>Case to which the order is to be associated must exist.</li>
     * <li>Dynamic Element Group Name is 'ORDERATTRIBUTE'.</li>
     * <li>Based on the 'OrderClassification', 'OrderType' and 'OrderCategory'
     * fields the following fields values are copied over from the 'OrderConfiguration' to the Legal Order
     * <ol>IsHoldingOrder</ol>
     * <ol>IsSchedulingNeeded</ol>
     * <ol>HasCharges</ol>
     * </li>
     * <li>The dynamic attributes stored against the Legal order must be restricted
     * to those dynamic attributes defined in the Order Configuration Type
     * for the same 'OrderClassification', 'Order Type' and 'OrderCategory'
     * combination as the Legal order.</li>
     *
     * @param uc         {@link UserContext}
     * @param legalOrder {@link LegalOrderType}, Required.
     * @return LegalOrderReturnType LegalOrder instance when success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public LegalOrderType createLegalOrder(@NotNull UserContext uc, @NotNull LegalOrderType legalOrder);

    /**
     * Update a Legal Order
     * <li>Order must exist.</li>
     * <li>Dynamic Element Group Name is 'ORDERATTRIBUTE'.</li>
     * <li>The following  fields  cannot be updated
     * <ol>IsHoldingOrder</ol>
     * <ol>IsSchedulingNeeded</ol>
     * <ol>HasCharges</ol>
     * </li>
     * <li>The dynamic attributes stored against the Legal order must be restricted
     * to those dynamic attributes defined in the Order Configuration Type for
     * the same 'OrderClassification', 'Order Type' and 'OrderCategory' combination
     * as the Legal orderAll associations are updatable.</li>
     *
     * @param uc         {@link UserContext}
     * @param legalOrder {@link LegalOrderType}, Required.
     * @return LegalOrderReturnType LegalOrder instance when success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public LegalOrderType updateLegalOrder(@NotNull UserContext uc, @NotNull LegalOrderType legalOrder);

    /**
     * Creates a new sentence type order
     * <li>Case to which the order is to be associated must exist.</li>
     * <li>Dynamic Element Group Name is 'ORDERATTRIBUTE'.</li>
     * <li>Based on the 'OrderClassification', 'OrderType' and 'OrderCategory'
     * fields the following fields values are copied over from the ‘OrderConfiguration’ to the Sentence
     * <ol>IsHoldingOrder</ol>
     * <ol>IsSchedulingNeeded</ol>
     * <ol>HasCharges</ol>
     * </li>
     * <li>The dynamic attributes stored against the Sentence must be restricted to
     * those dynamic attributes defined in the Order Configuration Type for the same
     * 'OrderClassification', 'Order Type' and 'OrderCategory' combination as the Sentence</li>
     * <li>If Sentence Number Configuration is set to auto generate, the system generates the sentence number.
     * The number generated is an integer (ascending order).</li>
     * <li>Sentence number must be unique across a period of supervision for the offender.</li>
     *
     * @param uc       {@link UserContext}
     * @param sentence {@link SentenceType}, Required.
     * @return SentenceReturnType Sentence instance when success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public SentenceType createSentence(@NotNull UserContext uc, @NotNull SentenceType sentence);

    /**
     * Get the max. value of the Sentence Number for a supervision id
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long, Required. A supervision ID.
     * @return Long The max. value of the Sentence Number for a set of CaseInfo IDs when success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public Long getMaxSentenceNumberBySupervisionId(@NotNull UserContext uc, @NotNull Long supervisionId);

    /**
     * Get the max. value of the Sentence Number for a set of CaseInfo IDs
     *
     * @param uc          {@link UserContext}
     * @param caseInfoIds Set&lt;Long>, Required. A set of CaseInfo IDs.
     * @return Long The max. value of the Sentence Number for a set of CaseInfo IDs when success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public Long getMaxSentenceNumberByCaseInfo(@NotNull UserContext uc, @NotNull Set<Long> caseInfoIds);

    /**
     * Retrieve Sentence Issuance Date By Charge Id through CaseActivity
     * <p>Sentence Issuance Date is the date of Date of Case Activity for Sentence in Court.
     *
     * @param uc       {@link UserContext}
     * @param chargeId Long, Required. Charge Id
     * @return Date Sentence Issuance Date if found; null otherwise
     */
    public Date retrieveSentenceIssuanceDateByChargeId(@NotNull UserContext uc, @NotNull Long chargeId);

    /**
     * Get Order Disposition Status Status by Disposition Outcome from Reference Code Links
     *
     * @param uc                 {@link UserContext}
     * @param dispositionOutcome CodeType, Required
     * @return CodeType Returns Disposition Status; Returns null if no corresponding Disposition Status found; Throws ArbutusRuntimeException in case of failure.
     */
    public CodeType getOrderDispositionStatus(@NotNull UserContext uc, @NotNull CodeType dispositionOutcome);

    /**
     * Update a sentence type order
     * <li>Order must exist.</li>
     * <li>Dynamic Element Group Name is ‘ORDERATTRIBUTE’.</li>
     * <li>The following fields  cannot be updated
     * <ol>IsHoldingOrder</ol>
     * <ol>IsSchedulingNeeded</ol>
     * <ol>HasCharges</ol>
     * </li>
     * <li>The dynamic attributes stored against the Sentence must be restricted to
     * those dynamic attributes defined in the Order Configuration Type for the same
     * 'OrderClassification', 'Order Type' and 'OrderCategory' combination as the
     * SentenceAll associations are updatable.</li>
     * <li>If Sentence Number Configuration is set to auto generate, the system generates the sentence number. The number generated is an integer (ascending order).</li>
     * <li>Sentence number must be unique across a period of supervision for the offender.</li>
     *
     * @param uc       {@link UserContext}
     * @param sentence {@link SentenceType}, Required.
     * @return SentenceReturnType Sentence instance when success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public SentenceType updateSentence(@NotNull UserContext uc, @NotNull SentenceType sentence);

    /**
     * Create Sentence Key Dates for a supervision
     * <p>Have a database to store Key Dates.
     * Key Dates apply to the Active Supervision period of an offender.
     * For this sprint, Key Date is just manual entry of dates with no validation rules around it.
     * These manual entry dates will be considered as "Override Dates".
     * <p>There should be a generic label for about 10 Key Dates which can be defined by a client.
     * For example, Key Date 1, Key Date 2...till Key Date 10.
     * Each client will have different sets of key dates and can change the labels to the key dates that they want to name them.
     * <p>The Name and ID of the staff who is entering these "Override Dates" along with the date-time and reason these dates are entered, should be stored.
     *
     * @param uc       {@link UserContext}
     * @param keyDates &lt;KeyDateType>, Required.
     * @return SentenceKeyDateType an instance is returned if success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public SentenceKeyDateType createSentenceKeyDate(@NotNull UserContext uc, SentenceKeyDateType keyDates);

    /**
     * Update Sentence Key Dates for a supervision
     * <p>Have a database to store Key Dates.
     * Key Dates apply to the Active Supervision period of an offender.
     * For this sprint, Key Date is just manual entry of dates with no validation rules around it.
     * These manual entry dates will be considered as "Override Dates".
     * <p>There should be a generic label for about 10 Key Dates which can be defined by a client.
     * For example, Key Date 1, Key Date 2...till Key Date 10.
     * Each client will have different sets of key dates and can change the labels to the key dates that they want to name them.
     * <p>The Name and ID of the staff who is entering these "Override Dates" along with the date-time and reason these dates are entered, should be stored.
     *
     * @param uc       {@link UserContext}
     * @param keyDates &lt;KeyDateType>, Required.
     * @return SentenceKeyDateType an instance is returned if success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public SentenceKeyDateType updateSentenceKeyDate(@NotNull UserContext uc, SentenceKeyDateType keyDates);

    /**
     * Get Sentence Key Dates for a supervision
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long, Required. Supervision Identification
     * @return SentenceKeyDateType an instance is returned if success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public SentenceKeyDateType getSentenceKeyDate(@NotNull UserContext uc, @NotNull Long supervisionId);

    /**
     * Find Sentence Key Dates by supervisionId for a supervision
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long, Required. Supervision Identification
     * @return SentenceKeyDateType an instance is returned if success;
     * null is returned if no sentence key date exists.
     */
    public SentenceKeyDateType findSentenceKeyDateBySupervisionId(@NotNull UserContext uc, @NotNull Long supervisionId);

    /**
     * Get History Sentence Key Dates for a supervision
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long, Required. Supervision Identification
     * @return List&lt;SentenceKeyDateType> A list of instances of histories is returned if success;
     * return null if no history record in DB;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public List<SentenceKeyDateType> getSentenceKeyDateHistory(@NotNull UserContext uc, @NotNull Long supervisionId);

    /**
     * Get Sentence KeyDate History By supervisionSentenceId
     *
     * @param uc                    {@link UserContext}
     * @param supervisionId         Long, Required. Supervision Identification
     * @param supervisionSentenceId Long, Required. Supervision Sentence Identification
     * @return List&lt;SentenceKeyDateType> A list of instances of histories is returned if success;
     * return null if no history record in DB;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public List<SentenceKeyDateType> getSentenceKeyDateHistoryBySupervisionSentenceId(@NotNull UserContext uc, @NotNull Long supervisionId,
            @NotNull Long supervisionSentenceId);

    /**
     * Get History Key Dates for a supervision
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long, Required. Supervision Identification
     * @param keyDateType   String, Required. Key Date Type, reference code of reference set KeyDates
     * @return List&lt;KeyDateType> A list of instances of histories is returned if success;
     * return null if no history record in DB;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public List<KeyDateType> getSentenceKeyDateHistory(@NotNull UserContext uc, @NotNull Long supervisionId, @NotNull String keyDateType);

    /**
     * Calculate Sentence Key Dates to calculate sentence key dates for a supervision
     *
     * @param uc             {@link UserContext}
     * @param supervisionId  Long, Required. Supervision Id
     * @param staffId        Long, Required. Staff Id
     * @param comment        String, optional. Comment by the staff.
     * @param reviewRequired Boolean, optional.
     * @return SentenceKeyDateType
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public SentenceKeyDateType calculateSentenceKeyDates(@NotNull UserContext uc, @NotNull Long supervisionId, @NotNull Long staffId, String comment,
            Boolean reviewRequired);

    /**
     * Get Is Sentence Calculation Enabled
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long, Required. Supervision Id
     * @return Boolean True: Sentence Calculation Is Enabled; False: Sentence Calculation Is Not Enabled.
     */
    public Boolean isSentenceCalculationEnabled(@NotNull UserContext uc, @NotNull Long supervisionId);

    /**
     * Delete Sentence Key Dates by a supervisionId
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long, Required. Supervision Identification
     * @return Long Returns success code 1L when success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public Long deleteSentenceKeyDate(@NotNull UserContext uc, @NotNull Long supervisionId);

    /**
     * Get Days Left To Serve For a Supervision
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long, Required. Supervision Identification
     * @return Long Returns the days left to serve for the supervision;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public Long daysLeftToServeForSupervision(@NotNull UserContext uc, @NotNull Long supervisionId);

    /**
     * Set Order Configuration
     * <lI>If Order Classification = BAIL (Bail Order) then isHoldingOrder = false</li>
     * <li>If Order Classification = SENT (Sentence Order) then isHoldingOrder = true</li>
     * <p>There can be many orders specified in the system each with its own configuration setting.
     *
     * @param uc                 {@link UserContext}
     * @param orderConfiguration {@link OrderConfigurationType}, Required.
     * @return OrderConfigurationReturnType OrderConfiguration instance when success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public OrderConfigurationType setOrderConfiguration(@NotNull UserContext uc, @NotNull OrderConfigurationType orderConfiguration);

    /**
     * Get Order Configuration by Id
     *
     * @param uc            {@link UserContext}
     * @param orderConfigId Long Required. The Id of OrderConfigurationType.
     * @return return the instance of OrderConfigurationType if success; return null if no configuration can be got.
     */
    public OrderConfigurationType getOrderConfiguration(@NotNull UserContext uc, @NotNull Long orderConfigId);

    /**
     * Update Order Configuration
     *
     * @param uc                 {@link UserContext}
     * @param orderConfiguration OrderConfigurationType Required
     * @return OrderConfigurationType Returns the updated OrderConfiguration if success;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public OrderConfigurationType updateOrderConfiguration(@NotNull UserContext uc, OrderConfigurationType orderConfiguration);

    /**
     * Retrieve Order Configuration
     * Based on the input fields specified one or more configurations will be retrieved.
     * If no inputs are specified, all the configuration values must be returned.
     *
     * @param uc                 {@link UserContext}
     * @param orderConfiguration {@link OrderConfigurationType}, Optional
     * @param startIndex         Long, Optional if null search returns at the start index
     * @param resultSize         Long, Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder        String, Optional if null search returns a default order
     * @return OrderConfigurationsReturnType OrderConfiguration instances when success;
     * returns 0 total size and 0 size of list of instance if instance not found;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public OrderConfigurationReturnType retrieveOrderConfiguration(@NotNull UserContext uc, OrderConfigurationType orderConfiguration, Long startIndex, Long resultSize,
            String resultOrder);

    /**
     * Retrieve Order Configuration
     * Based on the input fields specified one or more configurations will be retrieved.
     * If no inputs are specified, all the configuration values must be returned.
     *
     * @param uc                 {@link UserContext}
     * @param orderConfiguration {@link OrderConfigurationType}, Optional
     * @return OrderConfigurationsReturnType OrderConfiguration instances when success.
     * returns 0 total size and 0 size of list of instance if instance not found;
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public Set<OrderConfigurationType> retrieveOrderConfiguration(@NotNull UserContext uc, OrderConfigurationType orderConfiguration);

    /**
     * Delete Order Configuration
     *
     * @param uc            {@link UserContext}
     * @param orderConfigId Long Required. The Id of OrderConfigurationType.
     * @return Long Success: 1L; Failure: otherwise.
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public Long deleteOrderConfiguration(@NotNull UserContext uc, @NotNull Long orderConfigId);

    /**
     * Delete Order Configuration
     *
     * @param uc                 {@link UserContext}
     * @param orderConfiguration OrderConfigurationType, Required. The instance of one OrderConfigurationType retrieved by method retrieveOrderConfiguration.
     * @return Long Success: 1L; Failure: otherwise.
     * throws &lt;T extends ArbutusRuntimeException> otherwise.
     */
    public Long deleteOrderConfiguration(@NotNull UserContext uc, OrderConfigurationType orderConfiguration);

    /**
     * Set Intermittent Duration Configuration
     * Set the system wide setting (hours/days) for an intermittent schedule of a sentence.
     * <p>e.g. Sentence Fri 5 PM – Sun 5 PM,In Hours will equal 2 days (48 hours),In days will equal 3 days.
     * <p>Not set if preconditions is null or intermittentDurationConfiguration is null; Set otherwise.
     *
     * @param uc                                {@link UserContext}
     * @param intermittentDurationConfiguration String, Required.
     * @return CodeType CodeType.set: Duration of Intermittent; CodeType.code: HR -- Hours or DAY -- Days.
     * Returns a null if it cannot be set.
     */
    public CodeType setIntermittentDurationConfiguration(@NotNull UserContext uc, @NotNull String intermittentDurationConfiguration);

    /**
     * Get Intermittent Duration Configuration
     * <p>
     * Retrieve the system wide setting (hours/days) for an intermittent schedule of a sentence.
     * <p>List of possible return codes: 1, 2001, 1003</p>
     *
     * @param uc {@link UserContext}
     * @return CodeType CodeType.set: Duration of Intermittent; CodeType.code: HR -- Hours or DAY -- Days.
     * Returns a null if it cannot be got.
     */
    public CodeType getIntermittentDurationConfiguration(@NotNull UserContext uc);

    /**
     * Delete Intermittent Duration Configuration
     *
     * @param uc {@link UserContext}
     * @return Long Success: 1; &lt;T extends ArbutusRuntimeException>: otherwise.
     */
    public Long deleteIntermittentDurationConfiguration(@NotNull UserContext uc);

    /**
     * Set Sentence Number Configuration
     * Set sentence number configuration business rule.
     * <li>If bSentenceNumberGenerated = True, system auto generates the sentence number</li>
     * <li>If bSentenceNumberGenerated = False, system does not auto generate the sentence number</li>
     *
     * @param uc                       {@link UserContext}
     * @param bSentenceNumberGenerated , Required.
     * @return Boolean True: system auto generates the sentence number;
     * False: system does not auto generate the sentence number;
     * Null: cannot be set because of error when set.
     */
    public Boolean setSentenceNumberConfiguration(@NotNull UserContext uc, @NotNull Boolean bSentenceNumberGenerated);

    /**
     * Get Sentence Number Configuration
     * <li>True: system auto generates the sentence number</li>
     * <li>False: system does not auto generate the sentence number</li>
     *
     * @param uc {@link UserContext}
     * @return Boolean True: system auto generates the sentence number;
     * False: system does not auto generate the sentence number;
     * Null: cannot be set because of error when set.
     */
    public Boolean getSentenceNumberConfiguration(@NotNull UserContext uc);

    /**
     * Delete/Remove Sentence Number Configuration
     * <p>Remove the configuration setting from the system
     *
     * @param uc {@link UserContext}
     * @return Long 1: removed successfully; null: otherwise.
     */
    public Long deleteSentenceNumberConfiguration(@NotNull UserContext uc);

    /**
     * Set/Config KeyDate View By Aggregated or not
     *
     * @param uc                     {@link UserContext}
     * @param isAggregateKeyDateView Boolean Required. True: Aggregate; False: Dis-Aggregate; Null: Both.
     *                               When Failure, throws &lt;T extends ArbutusRuntimeException>
     */
    public void setKeyDateViewByAggregated(@NotNull UserContext uc, @NotNull Boolean isAggregateKeyDateView);

    /**
     * Get if KeyDate View Is By Aggregated or Not
     *
     * @param uc {@link UserContext}
     * @return Boolean True: Aggregated; False: Dis-Aggregated; Null: Both.
     * Failure: &lt;T extends ArbutusRuntimeException>: otherwise.
     */
    public Boolean isKeyDateViewByAggregated(@NotNull UserContext uc);

    /**
     * Delete KeyDate View By Aggregated Configuration
     *
     * @param uc {@link UserContext}
     * @return Long Success: 1 ;
     * Failure: &lt;T extends ArbutusRuntimeException>: otherwise.
     */
    public Long deleteKeyDateViewByAggregatedConfiguration(@NotNull UserContext uc);

    /**
     * Update Oder Data Security Status / Flag
     *
     * @param uc                 - Required.
     * @param dataSecurityStatus - DataFlag.
     * @param personId           Person Id of person id,
     * @param comments           - Comments to the security flag.
     * @return Long 1/0 as True/False
     */
    public Long updateOrderDataSecurityStatus(UserContext uc, Long orderId, Long dataSecurityStatus, String comments, Long personId, String parentEntityType,
            Long parentId, String memento, Boolean cascade);

    /**
     * Update Sentence Data Security Status / Flag
     *
     * @param uc                 - Required.
     * @param dataSecurityStatus - DataFlag.
     * @param personId           Person Id of person id,
     * @param comments           - Comments to the security flag.
     * @return Long 1/0 as True/False
     */
    public Long updateSentenceDataSecurityStatus(UserContext uc, Long sentenceId, Long dataSecurityStatus, String comments, Long personId, String parentEntityType,
            Long parentId, String memento, Boolean cascade);

    /**
     * This method save the schedule between start date and end data based on the other parameters for the sentence.
     *
     * @param userContext                  {@link UserContext}, required
     * @param intermittentSentenceSchedule {@link IntermittentSentenceScheduleType}, required. Input to generate schedule.
     * @return List<IntermittentSentenceScheduleType> returns all the Intermittent Sentence Schedules for the Order Id.
     */
    public IntermittentSentenceScheduleType createIntermittentSentenceSchedule(UserContext userContext, IntermittentSentenceScheduleType intermittentSentenceSchedule);

    /**
     * To retrieve Intermittent Sentence Schedule using sentence schedule id.
     *
     * @param userContext
     * @param scheduleId
     * @return
     */
    public IntermittentSentenceScheduleType getIntermittentSentenceSchedule(UserContext userContext, Long scheduleId);

    /**
     * This method save the schedule between start date and end data based on the other parameters for the sentence.
     *
     * @param userContext                  {@link UserContext}, required
     * @param intermittentSentenceSchedule {@link IntermittentSentenceScheduleType}, required. Input to generate schedule.
     * @return List<IntermittentSentenceScheduleType> returns all the Intermittent Sentence Schedules for the Order Id.
     */
    public IntermittentSentenceScheduleReturnType updateIntermittentSentenceSchedule(UserContext userContext,
            IntermittentSentenceScheduleType intermittentSentenceSchedule);

    /**
     * This method returns list of IntermittentSentenceScheduleType for the Order ID.
     *
     * @param userContext {@link UserContext}, required
     * @param orderId     - Long required
     * @return returns all the Intermittent Sentence Schedules for the Order Id.
     */
    public Set<IntermittentSentenceScheduleType> getIntermittentSentenceSchedules(UserContext userContext, Long orderId);

    /**
     * This method returns numeric value range from negative to positive including Zero (0) for the Sentence ID.
     * Positive value denotes that Number of scheduled days is less than sentenced days and difference is exactly the values.
     * Zero denotes that scheduled day(s) and sentenced days is/are equal.
     * Negative value denotes that Number of scheduled days is more than sentenced days and difference is exactly the values.
     *
     * @param userContext {@link UserContext}, required
     * @param sentenceId  - Long required
     * @return returns all the Intermittent Sentence Schedules for the Order Id.
     */
    public Long getRemainingNumberOfSentenceDays(UserContext userContext, Long sentenceId);

    /**
     * This method record fine payment
     *
     * @param userContext UserContext, required.
     * @param finePayment FinePaymentType, required
     * @return FinePaymentType
     */
    public FinePaymentType recordFinePayment(UserContext userContext, FinePaymentType finePayment);

    /**
     * This method returns List of Fine Payment wrapped in transfer object called FinePaymentReturnType for the given Assoicated Entity and its identifier.
     *
     * @param userContext        UserContext, required
     * @param sentenceIdentifier Long, required
     * @return FinePaymentReturnType
     */
    public FinePaymentReturnType getFinePaymentBySentenceId(UserContext userContext, Long sentenceIdentifier);

    /**
     * @param uc
     * @param sentenceId
     * @return Sentence
     */
    public Sentence getSentenceDetails(UserContext uc, Long sentenceId);

    /**
     * Delete schedule of intermittent sentence
     *
     * @param uc
     * @param orderIdentification
     * @return
     */
    public Long deleteSentenceSchedule(@NotNull UserContext uc, @NotNull Long orderIdentification);

}
