package syscon.arbutus.product.services.legal.contract.dto.charge;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * The representation of the external charge code configuration type
 */
public class ExternalChargeCodeConfigType implements Serializable {

    private static final long serialVersionUID = 9193916242577919628L;

    private Long chargeCodeId;
    @NotNull
    private String codeGroup;
    @NotNull
    private String chargeCode;
    private String codeCategory;
    @NotNull
    @Size(max = 128, message = "max length 128")
    private String description;

    /**
     * this variable to be used at Web App Side
     */
    private int rowId;

    /**
     * Default constructor
     */
    public ExternalChargeCodeConfigType() {
    }

    /**
     * Constructor
     *
     * @param chargeCodeId Long - The unique Id for each external charge code record  (optional for creation)
     * @param codeGroup    String - External Groups such as NCIC, UCR
     * @param chargeCode   String - The external charge code linked to the group.
     * @param codeCategory String - External charge code category (optional)
     * @param description  String - External charge code description
     */
    public ExternalChargeCodeConfigType(Long chargeCodeId, @NotNull String codeGroup, @NotNull String chargeCode, String codeCategory,
            @NotNull @Size(max = 128, message = "max length 128") String description) {
        this.chargeCodeId = chargeCodeId;
        this.codeGroup = codeGroup;
        this.chargeCode = chargeCode;
        this.codeCategory = codeCategory;
        this.description = description;
    }

    /**
     * Gets the value of the chargeCodeId property.
     *
     * @return Long - The unique Id for each external charge code record
     */
    public Long getChargeCodeId() {
        return chargeCodeId;
    }

    /**
     * Sets the value of the chargeCodeId property.
     *
     * @param chargeCodeId Long - The unique Id for each external charge code record
     */
    public void setChargeCodeId(Long chargeCodeId) {
        this.chargeCodeId = chargeCodeId;
    }

    /**
     * Gets the value of the codeGroup property. It is a reference code value of ExternalChargeCodeGroup set.
     *
     * @return String - External Groups such as NCIC, UCR
     */
    public String getCodeGroup() {
        return codeGroup;
    }

    /**
     * Sets the value of the codeGroup property. It is a reference code value of ExternalChargeCodeGroup set.
     *
     * @param codeGroup String - External Groups such as NCIC, UCR
     */
    public void setCodeGroup(String codeGroup) {
        this.codeGroup = codeGroup;
    }

    /**
     * Gets the value of the chargeCode property. It is a reference code value of ExternalChargeCode set.
     *
     * @return String - The external charge code linked to the group.
     */
    public String getChargeCode() {
        return chargeCode;
    }

    /**
     * Sets the value of the code property. It is a reference code value of ExternalChargeCode set.
     *
     * @param chargeCode String - The external charge code linked to the group.
     */
    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    /**
     * Gets the value of the category property. It is a reference code value of ExternalChargeCodeCategory set.
     *
     * @return String - External charge code category
     */
    public String getCodeCategory() {
        return codeCategory;
    }

    /**
     * Sets the value of the codeCategory property. It is a reference code value of ExternalChargeCodeCategory set.
     *
     * @param codeCategory String - External charge code category
     */
    public void setCodeCategory(String codeCategory) {
        this.codeCategory = codeCategory;
    }

    /**
     * Gets the value of the description property.
     *
     * @return String - External charge code description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param description String - External charge code description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ExternalChargeCodeConfigType [chargeCodeId=" + chargeCodeId + ", codeGroup=" + codeGroup + ", chargeCode=" + chargeCode + ", codeCategory=" + codeCategory
                + ", description=" + description + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((codeGroup == null) ? 0 : codeGroup.hashCode());
        result = prime * result + ((chargeCode == null) ? 0 : chargeCode.hashCode());
        result = prime * result + ((codeCategory == null) ? 0 : codeCategory.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ExternalChargeCodeConfigType other = (ExternalChargeCodeConfigType) obj;
        if (codeGroup == null) {
            if (other.codeGroup != null) {
				return false;
			}
        } else if (!codeGroup.equals(other.codeGroup)) {
			return false;
		}
        if (chargeCode == null) {
            if (other.chargeCode != null) {
				return false;
			}
        } else if (!chargeCode.equals(other.chargeCode)) {
			return false;
		}
        if (codeCategory == null) {
            if (other.codeCategory != null) {
				return false;
			}
        } else if (!codeCategory.equals(other.codeCategory)) {
			return false;
		}
        return true;
    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

}
