package syscon.arbutus.product.services.legal.realization.persistence.caseaffiliation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.CaseActivityEntity;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the case affiliation entity.
 *
 * @author lhan
 * @DbComment LEG_CaseAffiliation 'The case affiliation table for CaseAffiliation module';
 */
@Audited
@Entity
@Table(name = "LEG_CaseAffiliation")
@BatchSize(size = 20)
@SQLDelete(sql = "UPDATE LEG_CaseAffiliation SET flag = 4 WHERE caseAffiliationId = ? and version = ?")
@Where(clause = "flag = 1")
public class CaseAffiliationEntity implements Serializable {

    private static final long serialVersionUID = 1026657998822827679L;

    /**
     * @DbComment 'Case Affiliation Identification. The unique ID for each case affiliation created in the system.'
     */
    @Id
    @Column(name = "CaseAffiliationId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "Seq_LEG_CaseAffiliation_Id")
    @SequenceGenerator(name = "Seq_LEG_CaseAffiliation_Id", sequenceName = "Seq_LEG_CaseAffiliation_Id", allocationSize = 1)
    private Long caseAffiliationId;

    /**
     * @DbComment 'Indicates whether this is an affiliation to an organization, person identity within the organization or just a person identity'
     */
    @MetaCode(set = CaseAffiliationMetaSet.AFFILIATION_CATEGORY)
    @Column(name = "AffiliationCategory", nullable = false, length = 64)
    private String affiliationCategory;

    /**
     * @DbComment 'Indicates what type of affiliation to a case this is eg. legal, probationary'
     */
    @MetaCode(set = CaseAffiliationMetaSet.ASSOCIATION_TYPE)
    @Column(name = "AffiliationType", nullable = false, length = 64)
    private String affiliationType;

    /**
     * @DbComment 'Indicates what role this affiliation plays in the case eg. witness'
     */
    @MetaCode(set = CaseAffiliationMetaSet.PERSON_ROLE)
    @Column(name = "AffiliationRole", nullable = true, length = 64)
    private String affiliationRole;

    /**
     * @DbComment 'The date the case affiliation starts.'
     */
    @Column(name = "StartDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    /**
     * @DbComment 'The date the case affiliation ends.'
     */
    @Column(name = "EndDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    /**
     * If the case affiliation is active or inactive (Derived value from start/end date).'
     */
    @Transient
    private Boolean isActiveFlag;

    /**
     * @DbComment CommentUserId 'User ID who created/updated the comments'
     * @DbComment CommentDate 'The Date/Time when the comment was created'
     * @DbComment CommentText 'Comment associated with the case affiliation.'
     */
    @Embedded
    private CommentEntity comment;

    /**
     * @DbComment OrgPsnJobPosition 'The job position the individual holds in the organization.'
     */
    @Column(name = "OrgPsnJobPosition", nullable = true, length = 64)
    private String orgnizationPersonJobPosition;

    /**
     * @DbComment AffiliatedOrgId 'Id of affiliated organization object'
     */
    @Column(name = "AffiliatedOrgId", nullable = true)
    private Long affiliatedOrganizationId;

    /**
     * @DbComment AffiliatedPIId 'Id of affiliated person identity object'
     */
    @Column(name = "AffiliatedPIId", nullable = true)
    private Long affiliatedPersonIdentityId;

    /**
     * @DbComment AffiliatedCaseId 'Id of affiliated case object'
     */
    @Column(name = "AffiliatedCaseId", nullable = false)
    private Long affiliatedCaseId;

    /**
     * @DbComment PersonPersonId 'Id of assocoation/nonassocoation object'
     */
    @Column(name = "PersonPersonId", nullable = true)
    private Long personPersonId;

    /**
     * @DbComment Leg_CaseAffl_CaseAct 'The join table of the tables LEG_CaseAffiliation and LEG_CACaseActivity';
     * @DbComment Leg_CaseAffl_CaseAct.CaseAffiliationId 'CaseAffiliationId in LEG_CaseAffiliation table';
     * @DbComment Leg_CaseAffl_CaseAct.CaseActivityId 'CaseActivityId in LEG_CACaseActivity table';
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "Leg_CaseAffl_CaseAct", joinColumns = { @JoinColumn(name = "CaseAffiliationId") },
            inverseJoinColumns = { @JoinColumn(name = "CaseActivityId") })
    @BatchSize(size = 20)
    private Set<CaseActivityEntity> affiliatedCaseActivities = new HashSet<CaseActivityEntity>();

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    @Column(name = "version")
    private Long version;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(name = "Flag", nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * Default constructor
     */
    public CaseAffiliationEntity() {
    }

    /**
     * @param caseAffiliationId
     * @param affiliationCategory
     * @param affiliationType
     * @param affiliationRole
     * @param startDate
     * @param endDate
     * @param comment
     * @param orgnizationPersonJobPosition
     * @param affiliatedOrganizationId
     * @param affiliatedPersonIdentityId
     * @param affiliatedCaseId
     * @param affiliatedCaseActivities
     * @param stamp
     */
    public CaseAffiliationEntity(Long caseAffiliationId, String affiliationCategory, String affiliationType, String affiliationRole, Date startDate, Date endDate,
            CommentEntity comment, String orgnizationPersonJobPosition, Long affiliatedOrganizationId, Long affiliatedPersonIdentityId, Long affiliatedCaseId,
            boolean isActiveFlag, Set<CaseActivityEntity> affiliatedCaseActivities, StampEntity stamp) {
        super();
        this.caseAffiliationId = caseAffiliationId;
        this.affiliationCategory = affiliationCategory;
        this.affiliationType = affiliationType;
        this.affiliationRole = affiliationRole;
        this.startDate = startDate;
        this.endDate = endDate;
        this.comment = comment;
        this.orgnizationPersonJobPosition = orgnizationPersonJobPosition;
        this.affiliatedOrganizationId = affiliatedOrganizationId;
        this.affiliatedPersonIdentityId = affiliatedPersonIdentityId;
        this.affiliatedCaseId = affiliatedCaseId;
        this.affiliatedCaseActivities = affiliatedCaseActivities;
        this.stamp = stamp;
        this.isActiveFlag = isActiveFlag;
    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the caseAffiliationId
     */
    public Long getCaseAffiliationId() {
        return caseAffiliationId;
    }

    /**
     * @param caseAffiliationId the caseAffiliationId to set
     */
    public void setCaseAffiliationId(Long caseAffiliationId) {
        this.caseAffiliationId = caseAffiliationId;
    }

    /**
     * @return the affiliationCategory
     */
    public String getAffiliationCategory() {
        return affiliationCategory;
    }

    /**
     * @param affiliationCategory the affiliationCategory to set
     */
    public void setAffiliationCategory(String affiliationCategory) {
        this.affiliationCategory = affiliationCategory;
    }

    /**
     * @return the affiliationType
     */
    public String getAffiliationType() {
        return affiliationType;
    }

    /**
     * @param affiliationType the affiliationType to set
     */
    public void setAffiliationType(String affiliationType) {
        this.affiliationType = affiliationType;
    }

    /**
     * @return the affiliationRole
     */
    public String getAffiliationRole() {
        return affiliationRole;
    }

    /**
     * @param affiliationRole the affiliationRole to set
     */
    public void setAffiliationRole(String affiliationRole) {
        this.affiliationRole = affiliationRole;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the comment
     */
    public CommentEntity getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(CommentEntity comment) {
        this.comment = comment;
    }

    /**
     * @return the orgnizationPersonJobPosition
     */
    public String getOrgnizationPersonJobPosition() {
        return orgnizationPersonJobPosition;
    }

    /**
     * @param orgnizationPersonJobPosition the orgnizationPersonJobPosition to set
     */
    public void setOrgnizationPersonJobPosition(String orgnizationPersonJobPosition) {
        this.orgnizationPersonJobPosition = orgnizationPersonJobPosition;
    }

    /**
     * @return the affiliatedOrganizationId
     */
    public Long getAffiliatedOrganizationId() {
        return affiliatedOrganizationId;
    }

    /**
     * @param affiliatedOrganizationId the affiliatedOrganizationId to set
     */
    public void setAffiliatedOrganizationId(Long affiliatedOrganizationId) {
        this.affiliatedOrganizationId = affiliatedOrganizationId;
    }

    /**
     * @return the affiliatedPersonIdentityId
     */
    public Long getAffiliatedPersonIdentityId() {
        return affiliatedPersonIdentityId;
    }

    /**
     * @param affiliatedPersonIdentityId the affiliatedPersonCaseAffiliationEntityIdentityId to set
     */
    public void setAffiliatedPersonIdentityId(Long affiliatedPersonIdentityId) {
        this.affiliatedPersonIdentityId = affiliatedPersonIdentityId;
    }

    /**
     * @return the affiliatedCaseId
     */
    public Long getAffiliatedCaseId() {
        return affiliatedCaseId;
    }

    /**
     * @param affiliatedCaseId the affiliatedCaseId to set
     */
    public void setAffiliatedCaseId(Long affiliatedCaseId) {
        this.affiliatedCaseId = affiliatedCaseId;
    }

    /**
     * @return the personPersonId
     */
    public Long getPersonPersonId() {
        return personPersonId;
    }

    /**
     * @param personPersonId the personPersonId to set
     */
    public void setPersonPersonId(Long personPersonId) {
        this.personPersonId = personPersonId;
    }

    /**
     * @return the affiliatedCaseActivities
     */
    public Set<CaseActivityEntity> getAffiliatedCaseActivities() {
        if (affiliatedCaseActivities == null) {
			affiliatedCaseActivities = new HashSet<CaseActivityEntity>();
		}
        return affiliatedCaseActivities;
    }

    /**
     * @param affiliatedCaseActivities the affiliatedCaseActivities to set
     */
    public void setAffiliatedCaseActivities(Set<CaseActivityEntity> affiliatedCaseActivities) {
        this.affiliatedCaseActivities = affiliatedCaseActivities;
    }

    /**
     * If the case affiliation is active or inactive. It would be a derived value from start/end date relations.
     *
     * @return the isActiveFlag
     */
    public boolean isActiveFlag() {
        return isActiveFlag;
    }

    /**
     * If the case affiliation is active or inactive. It would be a derived value from start/end date relations.
     */
    protected void setActiveFlag(boolean isActiveFlag) {
        this.isActiveFlag = isActiveFlag;
    }

}
