package syscon.arbutus.product.services.legal.realization.adapters;

import java.util.Date;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.contract.interfaces.PersonService;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.utility.ServiceAdapter;

public class PersonIdentityServiceAdapter {
    private static PersonService service;

    public PersonIdentityServiceAdapter() {
        super();
    }

    synchronized private static PersonService getService() {
        if (service == null) {
            service = (PersonService) ServiceAdapter.JNDILookUp(service, PersonService.class);
        }
        return service;
    }

    public PersonIdentityType get(UserContext uc, Long personIdentityId) {
        PersonIdentityType ret = getService().getPersonIdentityType(uc, personIdentityId, true);
        return ret;
    }

    public String getFullName(UserContext uc, Long personIdentityId) {
        String firstName = get(uc, personIdentityId).getFirstName();
        String middleName = get(uc, personIdentityId).getMiddleName();
        String secondaryMiddleName = get(uc, personIdentityId).getSecondaryMiddleName();
        String lastName = get(uc, personIdentityId).getLastName();

        String fullName = firstName;

        if (!BeanHelper.isEmpty(middleName)) {
			fullName += " " + middleName;
		}

        if (!BeanHelper.isEmpty(secondaryMiddleName)) {
			fullName += " " + secondaryMiddleName;
		}

        fullName += " " + lastName;

        return fullName;
    }

    public String getFirstName(UserContext uc, Long personIdentityId) {
        return get(uc, personIdentityId).getFirstName();
    }

    public String getLastName(UserContext uc, Long personIdentityId) {
        return get(uc, personIdentityId).getLastName();
    }

    public String getMiddleName(UserContext uc, Long personIdentityId) {
        return get(uc, personIdentityId).getMiddleName();
    }

    public String getSecondaryMiddleName(UserContext uc, Long personIdentityId) {
        return get(uc, personIdentityId).getSecondaryMiddleName();
    }

    public Date getDateOfBirth(UserContext uc, Long personIdentityId) {
        return get(uc, personIdentityId).getDateOfBirth();
    }

    public String getSex(UserContext uc, Long personIdentityId) {
        return get(uc, personIdentityId).getSex();
    }
}
