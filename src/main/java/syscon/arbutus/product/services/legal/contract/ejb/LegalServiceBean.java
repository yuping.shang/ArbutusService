package syscon.arbutus.product.services.legal.contract.ejb;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.kie.api.cdi.KReleaseId;
import org.kie.api.runtime.KieSession;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldMetaType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueSearchType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.DTOBindingTypeEnum;
import syscon.arbutus.product.services.core.common.adapters.ClientCustomFieldServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.DataExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.legal.contract.dto.*;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CaseActivitySearchType;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivitiesReturnType;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivityType;
import syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationSearchType;
import syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationType;
import syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationsReturnType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.*;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType;
import syscon.arbutus.product.services.legal.contract.dto.charge.*;
import syscon.arbutus.product.services.legal.contract.dto.composite.BailDetailsType;
import syscon.arbutus.product.services.legal.contract.dto.composite.CaseOffendersReturnType;
import syscon.arbutus.product.services.legal.contract.dto.composite.CourtActivityMovementType;
import syscon.arbutus.product.services.legal.contract.dto.composite.InquiryCaseSearchType;
import syscon.arbutus.product.services.legal.contract.dto.condition.*;
import syscon.arbutus.product.services.legal.contract.dto.ordersentence.*;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.*;
import syscon.arbutus.product.services.legal.contract.interfaces.LegalService;
import syscon.arbutus.product.services.legal.realization.persistence.charge.StatuteChargeConfigEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.OrderEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.OrderHistoryEntity;
import syscon.arbutus.product.services.sentencecalculation.contract.dto.Sentence;
import syscon.arbutus.product.services.sentencecalculation.contract.dto.SentenceCalculatorReturnType;
import syscon.arbutus.product.services.sentencecalculation.contract.dto.SentenceCalculatorType;
import syscon.arbutus.product.services.sentencecalculation.contract.ejb.FinePaymentCalculator;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * EJB which is the implementation of LegalService.
 *
 * @author lhan
 */
@Stateless(mappedName = "LegalService", description = "The legal service")
@Remote(LegalService.class)
public class LegalServiceBean implements LegalService {

    private static Logger log = LoggerFactory.getLogger(LegalServiceBean.class);

    private int searchMaxLimit = 200;

    @Resource
    private SessionContext context;


    @PersistenceContext(unitName = "arbutus-pu")
    private Session session;

    @Inject
    @KReleaseId(groupId = "syscon.arbutus.product", artifactId = "sentences", version = "1.0.0")
    private KieSession ksession;


    /**
     *
     */
    public LegalServiceBean() {
    }

    /**
     * @param session
     * @param context
     */
    public LegalServiceBean(Session session, SessionContext context) {
        this.session = session;
        this.context = context;
    }

    //////////////////////////////////Case Affilication////////////////////////////////

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.CaseAffiliation#createCaseAffiliation(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationType)
     */
    @Override
    public CaseAffiliationType createCaseAffiliation(UserContext uc, CaseAffiliationType caseAffiliation) {
        return new CaseAffiliationHandler(context, session, searchMaxLimit).createCaseAffiliation(uc, caseAffiliation);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.CaseAffiliation#getCaseAffiliation(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    @Override
    public CaseAffiliationType getCaseAffiliation(UserContext uc, Long caseAffiliationId) {
        return new CaseAffiliationHandler(context, session, searchMaxLimit).getCaseAffiliation(uc, caseAffiliationId);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.CaseAffiliation#updateCaseAffiliation(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationType)
     */
    @Override
    public CaseAffiliationType updateCaseAffiliation(UserContext uc, CaseAffiliationType caseAffiliation) {
        return new CaseAffiliationHandler(context, session, searchMaxLimit).updateCaseAffiliation(uc, caseAffiliation);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.CaseAffiliation#deleteCaseAffiliation(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    @Override
    public Long deleteCaseAffiliation(UserContext uc, Long caseAffiliationId) {
        return new CaseAffiliationHandler(context, session, searchMaxLimit).deleteCaseAffiliation(uc, caseAffiliationId);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.CaseAffiliation#searchCaseAffiliation(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationSearchType, java.lang.Long, java.lang.Long, java.lang.String)
     */
    @Override
    public CaseAffiliationsReturnType searchCaseAffiliation(UserContext uc, CaseAffiliationSearchType search, Long startIndex, Long resultSize, String resultOrder) {
        return new CaseAffiliationHandler(context, session, searchMaxLimit).searchCaseAffiliation(uc, search, startIndex, resultSize, resultOrder);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.CaseAffiliation#retrieveCaseAffiliations(syscon.arbutus.product.security.UserContext, java.lang.Long, java.lang.String)
     */
    @Override
    public List<CaseAffiliationType> retrieveCaseAffiliations(UserContext uc, Long caseId, Boolean status) {
        return new CompositeHandler(context, session, searchMaxLimit).retrieveCaseAffiliations(uc, caseId, status);
    }

    //////////////////////////////////////Case Info module//////////////////////////////////

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.CaseInfo#searchCaseInfo(syscon.arbutus.product.security.UserContext, java.util.Set, syscon.arbutus.product.services.casemanagement.contract.dto.CaseInfoSearchType, java.lang.Boolean)
     */
    @Deprecated
    public List<CaseInfoType> searchCaseInfo(UserContext uc, Set<Long> subsetSearch, CaseInfoSearchType search, Boolean searchHistory) {

        return new CaseInfoHandler(context, session, searchMaxLimit).searchCaseInfo(uc, subsetSearch, search, searchHistory);
    }

    public CaseInfosReturnType searchCaseInfo(UserContext uc, CaseInfoSearchType search, Boolean searchHistory, Long startIndex, Long resultSize, String resultOrder) {
        return new CaseInfoHandler(context, session, searchMaxLimit).searchCaseInfo(uc, search, searchHistory, startIndex, resultSize, resultOrder);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.CaseInfo#createCaseInfo(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.CaseInfoType, java.lang.Boolean)
     */
    public CaseInfoType createCaseInfo(UserContext uc, CaseInfoType caseInfo, Boolean duplicatedCheck) {

        return new CaseInfoHandler(context, session).createCaseInfo(uc, caseInfo, duplicatedCheck);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.CaseInfo#updateCaseInfo(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.CaseInfoType, java.lang.Boolean)
     */
    public CaseInfoType updateCaseInfo(UserContext uc, CaseInfoType caseInfo, Boolean duplicatedCheck) {

        return new CaseInfoHandler(context, session).updateCaseInfo(uc, caseInfo, duplicatedCheck);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.CaseInfo#retrieveCaseInfo(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    public CaseInfoType getCaseInfo(UserContext uc, Long caseInfoId) {

        return new CaseInfoHandler(context, session).getCaseInfo(uc, caseInfoId);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.CaseInfo#retrieveCaseInfos(syscon.arbutus.product.security.UserContext, java.lang.Long, java.lang.Boolean)
     */
    public List<CaseInfoType> retrieveCaseInfos(UserContext uc, Long supervisionId, Boolean isActive) {

        return new CaseInfoHandler(context, session, searchMaxLimit).retrieveCaseInfos(uc, supervisionId, isActive);

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.CaseInfo#retrieveCaseInfos(syscon.arbutus.product.security.UserContext, java.lang.Long, java.util.Date, java.util.Date)
     */
    public List<CaseInfoType> retrieveCaseInfoHistory(UserContext uc, Long caseInfoId, Date historyDateFrom, Date historyDateTo) {

        return new CaseInfoHandler(context, session, searchMaxLimit).retrieveCaseInfoHistory(uc, caseInfoId, historyDateFrom, historyDateTo);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.CaseInfo#deleteCaseInfo(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    public Long deleteCaseInfo(UserContext uc, Long caseInfoId) throws DataExistException {

        return new CaseInfoHandler(context, session).deleteCaseInfo(uc, caseInfoId);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.CaseInfo#deleteCaseIdentifier(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.CaseIdentifierType)
     */
    public Long deleteCaseIdentifier(UserContext uc, CaseIdentifierType caseIdentifier) {

        return new CaseInfoHandler(context, session).deleteCaseIdentifier(uc, caseIdentifier);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.CaseInfo#createCaseIdentifierConfig(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.CaseIdentifierConfigType)
     */
    public CaseIdentifierConfigType createCaseIdentifierConfig(UserContext uc, CaseIdentifierConfigType caseIdentifierConfig) {

        return new CaseInfoHandler(context, session).createCaseIdentifierConfig(uc, caseIdentifierConfig);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.CaseInfo#updateCaseIdentifierConfig(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.CaseIdentifierConfigType)
     */
    public CaseIdentifierConfigType updateCaseIdentifierConfig(UserContext uc, CaseIdentifierConfigType caseIdentifierConfig) {

        return new CaseInfoHandler(context, session).updateCaseIdentifierConfig(uc, caseIdentifierConfig);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.CaseInfo#searchCaseIdentifierConfig(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.CaseIdentifierConfigSearchType)
     */
    public List<CaseIdentifierConfigType> searchCaseIdentifierConfig(UserContext uc, CaseIdentifierConfigSearchType caseIdentifierConfigSearch) {

        return new CaseInfoHandler(context, session, searchMaxLimit).searchCaseIdentifierConfig(uc, caseIdentifierConfigSearch);
    }

    @Override
    public List<CaseStatusChangeType> retrieveCaseStatusChangeHistory(UserContext uc, Long caseInfoId, Date historyDateFrom, Date historyDateTo) {
        // TODO Auto-generated method stub
        return new CaseInfoHandler(context, session, searchMaxLimit).retrieveCaseStatusChangeHistory(uc, caseInfoId, historyDateFrom, historyDateTo);
    }

    @Override
    public Long updateCaseDataSecurityStatus(UserContext uc, Long caseId, Long secStatus, String comments, Long personId, String parentEntityType, Long parentId,
            String memento, Boolean cascade) {
        CaseInfoHandler handler = new CaseInfoHandler(context, session);
        Long ret = handler.updateCaseDataSecurityStatus(uc, caseId, secStatus, comments, personId, parentEntityType, parentId, memento, cascade);

        return ret;
    }

    //////////////////////Functions of Case Activity Module////////////////////////

    @Override
    public CourtActivityType createCaseActivity(UserContext uc, CourtActivityType courtActivity) {
        return new CaseActivityHandler(context, session).create(uc, courtActivity);
    }

    @Override
    public CourtActivityType getCaseActivity(UserContext uc, Long caseActivityId) {
        return new CaseActivityHandler(context, session).get(uc, caseActivityId);
    }

    @Override
    public CourtActivityType updateCaseActivity(UserContext uc, CourtActivityType courtActivity) {
        return new CaseActivityHandler(context, session).update(uc, courtActivity);
    }

    @Override
    public Long deleteCaseActivity(UserContext uc, Long caseActivityId) {
        return new CaseActivityHandler(context, session).delete(uc, caseActivityId);
    }

    @Override
    public CourtActivityType cancelCourtActivity(UserContext uc, Long caseActivityId) {
        return new CaseActivityHandler(context, session).cancelCourtActivity(uc, caseActivityId);
    }

    @Override
    public List<CourtActivityType> searchCaseActivity(UserContext uc, Set<Long> ids, CaseActivitySearchType searchType, Boolean historyFlag) {
        return new CaseActivityHandler(context, session).search(uc, ids, searchType, historyFlag);
    }

    @Override
    public CourtActivitiesReturnType searchCourtActivity(UserContext uc, CaseActivitySearchType search, Long startIndex, Long resultSize, String resultOrder) {
        return new CaseActivityHandler(context, session).searchCourtActivity(uc, search, startIndex, resultSize, resultOrder);
    }

    @Override
    public List<CourtActivityType> retrieveCaseActivities(UserContext uc, Long caseId, String activityStatus) {
        return new CaseActivityHandler(context, session).retrieveCaseActivities(uc, caseId, activityStatus);
    }

    @Override
    public List<CourtActivityType> retrieveCaseActivityHistory(UserContext uc, Long caseActivityId, Date fromHistoryDate, Date toHistoryDate) {
        return new CaseActivityHandler(context, session).retrieveCaseActivityHistory(uc, caseActivityId, fromHistoryDate, toHistoryDate);
    }

    @Override
    public Long updateCaseActivityDataSecurityStatus(UserContext uc, Long caseActivityId, Long secStatus, String comments, Long personId, String parentEntityType,
            Long parentId, String memento) {
        return new CaseActivityHandler(context, session, searchMaxLimit).updateCaseActivityDataSecurityStatus(uc, caseActivityId, secStatus, comments, personId,
                parentEntityType, parentId, memento);
    }

    /////////////////////////////////////////////////////////
    //Charge related actions
    /////////////////////////////////////////////////////////

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#createCharge(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.ChargeType)
     */
    public ChargeType createCharge(UserContext uc, ChargeType charge) {

        ChargeHandler handler = new ChargeHandler(context, session);
        ChargeType ret = handler.createCharge(uc, charge);
        return ret;

    }

    public Long updateChargeDataSecurityStatus(UserContext uc, Long chargeId, Long dataSecurityStatus, String comments, Long personId, String parentEntityType,
            Long parentId, String memento, Boolean cascade) {

        ChargeHandler handler = new ChargeHandler(context, session);
        Long ret = handler.updateChargeDataSecurityStatus(uc, chargeId, dataSecurityStatus, comments, personId, parentEntityType, parentId, memento, cascade);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#createCharges(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.ChargeType)
     */
    public List<ChargeType> createCharges(UserContext uc, ChargeType charge) {

        ChargeHandler handler = new ChargeHandler(context, session);
        List<ChargeType> ret = handler.createCharges(uc, charge);
        return ret;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#updateCharge(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.ChargeType)
     */
    public ChargeType updateCharge(UserContext uc, ChargeType charge) {

        ChargeHandler handler = new ChargeHandler(context, session);
        ChargeType ret = handler.updateCharge(uc, charge);
        return ret;

    }

    public ChargesReturnType searchCharge(UserContext uc, ChargeSearchType search, Boolean searchHistory, Long startIndex, Long resultSize, String resultOrder) {
        ChargeHandler handler = new ChargeHandler(context, session, searchMaxLimit);
        ChargesReturnType ret = handler.searchCharge(uc, search, searchHistory, startIndex, resultSize, resultOrder);
        return ret;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#searchCharge(syscon.arbutus.product.security.UserContext, java.util.Set, syscon.arbutus.product.services.casemanagement.contract.dto.charge.ChargeSearchType, java.lang.Boolean)
     */
    @Deprecated
    public List<ChargeType> searchCharge(UserContext uc, Set<Long> subsetSearch, ChargeSearchType search, Boolean searchHistory) {

        ChargeHandler handler = new ChargeHandler(context, session, searchMaxLimit);
        List<ChargeType> ret = handler.searchCharge(uc, subsetSearch, search, searchHistory);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#getCharge(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    public ChargeType getCharge(UserContext uc, Long chargeId) {

        ChargeHandler handler = new ChargeHandler(context, session);
        ChargeType ret = handler.getCharge(uc, chargeId);
        return ret;

    }

    public List<ChargeType> getChargesByCaseInfo(UserContext uc, Long caseInfoId) {
        ChargeHandler handler = new ChargeHandler(context, session, searchMaxLimit);
        List<ChargeType> ret = handler.getChargesByCaseInfoId(uc, caseInfoId);
        return ret;
    }

    @Override
    public List<ChargeType> getChargesByOJOrderId(UserContext uc, Long OJOrderId) {
        ChargeHandler handler = new ChargeHandler(context, session, searchMaxLimit);
        List<ChargeType> ret = handler.getChargesByOJOrderId(uc, OJOrderId);
        return ret;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#retrieveCharges(syscon.arbutus.product.security.UserContext, java.lang.Long, syscon.arbutus.product.services.casemanagement.contract.dto.charge.ChargeSearchType)
     */
    public List<ChargeType> retrieveCharges(UserContext uc, Long caseInfoId, ChargeSearchType search) {

        ChargeHandler handler = new ChargeHandler(context, session, searchMaxLimit);
        List<ChargeType> ret = handler.retrieveCharges(uc, caseInfoId, search);
        return ret;

    }

    public List<ChargeType> retrieveChargesBySupervisionId(@NotNull UserContext uc, @NotNull Long supervisionId) {
        return (new ChargeHandler(context, session, searchMaxLimit)).getChargesBySupervisionId(uc, supervisionId);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#retrieveChargeHistory(syscon.arbutus.product.security.UserContext, java.lang.Long, java.util.Date, java.util.Date)
     */
    public List<ChargeType> retrieveChargeHistory(UserContext uc, Long chargeId, Date historyDateFrom, Date historyDateTo) {

        ChargeHandler handler = new ChargeHandler(context, session, searchMaxLimit);
        List<ChargeType> ret = handler.retrieveChargeHistory(uc, chargeId, historyDateFrom, historyDateTo);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#retrieveStatuteCharges(syscon.arbutus.product.security.UserContext, java.lang.String, java.util.Date)
     */
    public List<StatuteChargeConfigType> retrieveStatuteCharges(UserContext uc, String statuteCode, Date offenceDate) {

        ChargeHandler handler = new ChargeHandler(context, session, searchMaxLimit);
        List<StatuteChargeConfigType> ret = handler.retrieveStatuteCharges(uc, statuteCode, offenceDate);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#getStatuteCharge(syscon.arbutus.product.security.UserContext, java.lang.String, java.lang.String, java.util.Date)
     */
    public StatuteChargeConfigType getStatuteCharge(UserContext uc, String statuteCode, String chargeCode, Date offenceDate) {

        ChargeHandler handler = new ChargeHandler(context, session);
        StatuteChargeConfigType ret = handler.getStatuteCharge(uc, statuteCode, chargeCode, offenceDate);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#deleteCharge(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    public Long deleteCharge(UserContext uc, Long chargeId) throws DataExistException {

        ChargeHandler handler = new ChargeHandler(context, session);
        Long ret = handler.deleteCharge(uc, chargeId);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#updateChargeIdentifier(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.ChargeIdentifierType)
     */
    public ChargeType updateChargeIdentifier(UserContext uc, ChargeIdentifierType chargeIdentifier) {

        ChargeHandler handler = new ChargeHandler(context, session);
        ChargeType ret = handler.updateChargeIdentifier(uc, chargeIdentifier);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#removeChargeIdentifier(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    public ChargeType removeChargeIdentifier(UserContext uc, Long chargeIdentifierId) {

        ChargeHandler handler = new ChargeHandler(context, session);
        ChargeType ret = handler.removeChargeIdentifier(uc, chargeIdentifierId);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#updateChargePlea(syscon.arbutus.product.security.UserContext, java.lang.Long, syscon.arbutus.product.services.casemanagement.contract.dto.charge.ChargePleaType)
     */
    public ChargeType updateChargePlea(UserContext uc, Long chargeId, ChargePleaType chargePlea) {

        ChargeHandler handler = new ChargeHandler(context, session);
        ChargeType ret = handler.updateChargePlea(uc, chargeId, chargePlea);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#updateChargeAppeal(syscon.arbutus.product.security.UserContext, java.lang.Long, syscon.arbutus.product.services.casemanagement.contract.dto.charge.ChargeAppealType)
     */
    public ChargeType updateChargeAppeal(UserContext uc, Long chargeId, ChargeAppealType chargeAppeal) {

        ChargeHandler handler = new ChargeHandler(context, session);
        ChargeType ret = handler.updateChargeAppeal(uc, chargeId, chargeAppeal);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#createExternalChargeCode(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.ExternalChargeCodeConfigType)
     */
    public ExternalChargeCodeConfigType createExternalChargeCode(UserContext uc, ExternalChargeCodeConfigType externalChargeCodeConfig) {

        ChargeHandler handler = new ChargeHandler(context, session);
        ExternalChargeCodeConfigType ret = handler.createExternalChargeCode(uc, externalChargeCodeConfig);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#updateExternalChargeCode(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.ExternalChargeCodeConfigType)
     */
    public ExternalChargeCodeConfigType updateExternalChargeCode(UserContext uc, ExternalChargeCodeConfigType externalChargeCodeConfig) {

        ChargeHandler handler = new ChargeHandler(context, session);
        ExternalChargeCodeConfigType ret = handler.updateExternalChargeCode(uc, externalChargeCodeConfig);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#deleteExternalChargeCode(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    public Long deleteExternalChargeCode(UserContext uc, Long extChargeCodeId) {

        ChargeHandler handler = new ChargeHandler(context, session);
        Long ret = handler.deleteExternalChargeCode(uc, extChargeCodeId);
        return ret;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#searchExternalChargeCode(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.ExternalChargeCodeConfigSearchType)
     */
    public List<ExternalChargeCodeConfigType> searchExternalChargeCode(UserContext uc, ExternalChargeCodeConfigSearchType search) {

        ChargeHandler handler = new ChargeHandler(context, session, searchMaxLimit);

        //Testing pagination
        List<ExternalChargeCodeConfigType> ret = handler.searchExternalChargeCode(uc, search, null, null);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#createJurisdiction(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.JurisdictionConfigType)
     */
    public JurisdictionConfigType createJurisdiction(UserContext uc, JurisdictionConfigType jurisdictionConfig) {

        ChargeHandler handler = new ChargeHandler(context, session);
        JurisdictionConfigType ret = handler.createJurisdiction(uc, jurisdictionConfig);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#updateJurisdiction(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.JurisdictionConfigType)
     */
    public JurisdictionConfigType updateJurisdiction(UserContext uc, JurisdictionConfigType jurisdictionConfig) {

        ChargeHandler handler = new ChargeHandler(context, session);
        JurisdictionConfigType ret = handler.updateJurisdiction(uc, jurisdictionConfig);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#deleteJurisdiction(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    public Long deleteJurisdiction(UserContext uc, Long jurisdictionId) {

        ChargeHandler handler = new ChargeHandler(context, session);
        Long ret = handler.deleteJurisdiction(uc, jurisdictionId);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#searchJurisdiction(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.JurisdictionConfigSearchType, java.lang.Long, java.lang.Long, java.lang.String)
     */
    public JurisdictionConfigsReturnType searchJurisdiction(UserContext uc, JurisdictionConfigSearchType search, Long startIndex, Long resultSize, String resultOrder) {
        ChargeHandler handler = new ChargeHandler(context, session, searchMaxLimit);
        JurisdictionConfigsReturnType ret = handler.searchJurisdiction(uc, search, startIndex, resultSize, resultOrder);
        return ret;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#createStatute(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.StatuteConfigType)
     */
    public StatuteConfigType createStatute(UserContext uc, StatuteConfigType statuteConfig) {

        ChargeHandler handler = new ChargeHandler(context, session);
        StatuteConfigType ret = handler.createStatute(uc, statuteConfig);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#updateStatute(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.StatuteConfigType)
     */
    public StatuteConfigType updateStatute(UserContext uc, StatuteConfigType statuteConfig) {

        ChargeHandler handler = new ChargeHandler(context, session);
        StatuteConfigType ret = handler.updateStatute(uc, statuteConfig);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#deleteStatute(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    public Long deleteStatute(UserContext uc, Long statuteId) {

        ChargeHandler handler = new ChargeHandler(context, session);
        Long ret = handler.deleteStatute(uc, statuteId);
        return ret;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#searchStatute(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.StatuteConfigSearchType, java.lang.Long, java.lang.Long, java.lang.String)
     */
    public StatuteConfigsReturnType searchStatute(UserContext uc, StatuteConfigSearchType search, Long startIndex, Long resultSize, String resultOrder) {
        ChargeHandler handler = new ChargeHandler(context, session, searchMaxLimit);
        StatuteConfigsReturnType ret = handler.searchStatute(uc, search, startIndex, resultSize, resultOrder);
        return ret;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#createStatuteCharge(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.StatuteChargeConfigType)
     */
    public StatuteChargeConfigType createStatuteCharge(UserContext uc, StatuteChargeConfigType statuteChargeConfig) {

        ChargeHandler handler = new ChargeHandler(context, session);
        StatuteChargeConfigType ret = handler.createStatuteCharge(uc, statuteChargeConfig);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#updateStatuteCharge(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.StatuteChargeConfigType)
     */
    public StatuteChargeConfigType updateStatuteCharge(UserContext uc, StatuteChargeConfigType statuteChargeConfig) {

        ChargeHandler handler = new ChargeHandler(context, session);
        StatuteChargeConfigType ret = handler.updateStatuteCharge(uc, statuteChargeConfig);
        return ret;

    }

    public StatuteChargeConfigType getStatuteCharge(UserContext uc, Long statuteChargeId) {

        ChargeHandler handler = new ChargeHandler(context, session);
        StatuteChargeConfigType ret = handler.getStatuteCharge(uc, statuteChargeId);
        return ret;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#deleteStatuteCharge(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    public Long deleteStatuteCharge(UserContext uc, Long statuteChargeId) {

        ChargeHandler handler = new ChargeHandler(context, session);
        Long ret = handler.deleteStatuteCharge(uc, statuteChargeId);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#searchStatuteCharge(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.charge.StatuteChargeConfigSearchType, java.lang.Long, java.lang.Long, java.lang.String)
     */
    public StatuteChargeConfigsReturnType searchStatuteCharge(UserContext uc, StatuteChargeConfigSearchType search, Long startIndex, Long resultSize,
            String resultOrder) {
        ChargeHandler handler = new ChargeHandler(context, session, searchMaxLimit);
        StatuteChargeConfigsReturnType ret = handler.searchStatuteCharge(uc, search, startIndex, resultSize, resultOrder);
        return ret;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#setOffenceDateConfig(syscon.arbutus.product.security.UserContext, boolean)
     */
    public Long setOffenceDateConfig(UserContext uc, boolean isOffenceStartDate) {

        ChargeHandler handler = new ChargeHandler(context, session);
        Long ret = handler.setOffenceDateConfig(uc, isOffenceStartDate);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#deleteOffenceDateConfig(syscon.arbutus.product.security.UserContext)
     */
    public Long deleteOffenceDateConfig(UserContext uc) {

        ChargeHandler handler = new ChargeHandler(context, session);
        Long ret = handler.deleteOffenceDateConfig(uc);
        return ret;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Charge#getOffenceStartDateConfig(syscon.arbutus.product.security.UserContext)
     */
    public Boolean getOffenceStartDateConfig(UserContext uc) {

        ChargeHandler handler = new ChargeHandler(context, session);
        Boolean ret = handler.getOffenceStartDateConfig(uc);
        return ret;

    }

    public CodeType getChargeDispositionStatus(UserContext uc, CodeType dispositionOutcome) {

        ChargeHandler handler = new ChargeHandler(context, session);
        CodeType ret = handler.getDispositionStatus(uc, dispositionOutcome);
        return ret;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.CaseManagementService#getStamp(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    public StampType getStamp(UserContext uc, LegalModule module, Long instanceId) {

        if (uc == null || module == null || instanceId == null) {
            throw new InvalidInputException("All parameters must have value");
        }

        if (LegalModule.CASE_ACTIVITY.equals(module)) {
            return new CaseActivityHandler(context, session).getStamp(uc, instanceId);
        }

        // TODO need to implement for other modules.
        return null;
    }

    @Override
    public Long getCount(UserContext uc, LegalModule module) {
        if (uc == null || module == null) {
            throw new InvalidInputException("All parameters must have value");
        }

        if (LegalModule.CASE_ACTIVITY.equals(module)) {
            return new CaseActivityHandler(context, session).getCount(uc);
        }

        // TODO need to implement for other modules.
        return null;
    }

    @Override
    public Set<Long> getAll(UserContext uc, LegalModule module) {
        if (uc == null || module == null) {
            throw new InvalidInputException("All parameters must have value");
        }

        if (LegalModule.CASE_ACTIVITY.equals(module)) {
            return new CaseActivityHandler(context, session).getAll(uc);
        }

        if (LegalModule.ORDER_SENTENCE.equals(module)) {
            return OrderSentenceHandler.getAll(uc, context, session);
        }

        // TODO need to implement for other modules.
        return null;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.CaseManagementService#setCurrencyConfig(syscon.arbutus.product.security.UserContext, java.lang.String)
     */
    public CodeType setCurrencyConfig(UserContext uc, String currency) {

        if (log.isDebugEnabled()) {
			log.debug("setCurrencyConfig: begin");
		}

        Boolean bReferenceCode = true;
        CodeType ret = LegalHelper.setConfigEntity(uc, context, session, ReferenceSet.CURRENCY.value(), currency, bReferenceCode);

        if (log.isDebugEnabled()) {
			log.debug("setCurrencyConfig: end");
		}
        return ret;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.CaseManagementService#getCurrencyConfig(syscon.arbutus.product.security.UserContext)
     */
    public CodeType getCurrencyConfig(UserContext uc) {
        if (log.isDebugEnabled()) {
			log.debug("getCurrencyConfig: begin");
		}

        CodeType ret = LegalHelper.getConfigEntity(session, ReferenceSet.CURRENCY.value());

        if (log.isDebugEnabled()) {
			log.debug("getCurrencyConfig: end");
		}
        return ret;
    }

	/* ********************
     * CONDITION RELATED
	 ******************** */

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Condition#createCondition(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.condition.ConditionType)
     */
    public ConditionType createCondition(UserContext uc, ConditionType condition) {
        log.debug("createCondition");
        return new ConditionHandler(context, session).createCondition(uc, condition);

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Condition#updateCondition(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.condition.ConditionType)
     */
    public ConditionType updateCondition(UserContext uc, ConditionType condition) {
        log.debug("updateCondition");
        return new ConditionHandler(context, session).updateCondition(uc, condition);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Condition#getCondition(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    public ConditionType getCondition(UserContext uc, Long id) {
        log.debug("getCondition");
        return new ConditionHandler(context, session).getCondition(uc, id);
    }

    public ConditionsReturnType searchCondition(UserContext uc, ConditionSearchType search, Boolean historyFlag, String chargeSetMode, String caseInfoSetMode,
            Long startIndex, Long resultSize, String resultOrder) {
        log.debug("searchCondition");
        return new ConditionHandler(context, session, searchMaxLimit).searchCondition(uc, search, historyFlag, chargeSetMode, caseInfoSetMode, startIndex, resultSize,
                resultOrder);
    }

    @Deprecated
    public List<ConditionType> searchCondition(UserContext uc, Set<Long> IDs, ConditionSearchType search, Boolean historyFlag, String chargeSetMode,
            String caseInfoSetMode) {
        log.debug("searchCondition");
        return new ConditionHandler(context, session, searchMaxLimit).searchCondition(uc, IDs, search, historyFlag, chargeSetMode, caseInfoSetMode);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Condition#retrieveConditionHistory(syscon.arbutus.product.security.UserContext, java.lang.Long, java.util.Date, java.util.Date)
     */
    public List<ConditionType> retrieveConditionHistory(UserContext uc, Long conditionId, Date fromHistoryDate, Date toHistoryDate) {
        log.debug("retrieveConditionHistory");
        return new ConditionHandler(context, session, searchMaxLimit).retrieveConditionHistory(uc, conditionId, fromHistoryDate, toHistoryDate);
    }

    /*
     * (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.Condition#retrieveConditionBy(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.caseinformation.CaseInfoType, syscon.arbutus.product.services.casemanagement.contract.dto.charge.ChargeType, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.OrderType, syscon.arbutus.product.services.casemanagement.contract.dto.condition.ConditionSearchType, java.lang.Boolean)
     */
    public List<ConditionType> retrieveConditionBy(UserContext uc, Long caseId, Long chargeId, Long orderId, ConditionSearchType conditionSearch, Boolean active) {
        log.debug("retrieveConditionBy");
        return new ConditionHandler(context, session, searchMaxLimit).retrieveConditionBy(uc, caseId, chargeId, orderId, conditionSearch, active);
    }

    @Override
    public Long deleteCondition(UserContext uc, Long conditionId) {
        return new ConditionHandler(context, session, searchMaxLimit).deleteCondition(uc, conditionId);
    }

    @Override
    public Long updateConditionDataSecurityStatus(UserContext uc, Long conditionId, Long secStatus, String comments, Long personId, String parentEntityType,
            Long parentId, String memento) {
        return new ConditionHandler(context, session, searchMaxLimit).updateConditionDataSecurityStatus(uc, conditionId, secStatus, comments, personId, parentEntityType,
                parentId, memento);
    }

    @Override
    public ConfigureConditionType createConfigureCondition(UserContext userContext, ConfigureConditionType configureCondition) {
        return new ConditionHandler(context, session, searchMaxLimit).createConfigureCondition(userContext, configureCondition);
    }

    @Override
    public ConfigureConditionType updateConfigureCondition(UserContext userContext, ConfigureConditionType configureCondition) {
        return new ConditionHandler(context, session, searchMaxLimit).updateConfigureCondition(userContext, configureCondition);
    }

    @Override
    public Long deleteConfigureCondition(UserContext userContext, Long configureConditionId) {
        return new ConditionHandler(context, session, searchMaxLimit).deleteConfigureCondition(userContext, configureConditionId);
    }

    @Override
    public ConfigureConditionType getConfigureCondition(UserContext userContext, Long configureConditionId) {
        return new ConditionHandler(context, session, searchMaxLimit).getConfigureCondition(userContext, configureConditionId);
    }

    @Override
    public ConfigureConditionsReturnType searchConfigureCondition(UserContext userContext, ConfigureConditionSearchType search, Boolean historyFlag, Long startIndex,
            Long resultSize, String resultOrder) {
        return new ConditionHandler(context, session, searchMaxLimit).searchConfigureCondition(userContext, search, historyFlag, startIndex, resultSize, resultOrder);
    }
	
	/* ****************
	 * ORDER RELATED
	 **************** */

    public <T extends OrderType> T getOrder(UserContext uc, Long orderIdentification) {
        return OrderSentenceHandler.getOrder(uc, context, session, orderIdentification);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#retrieveOrderHistory(syscon.arbutus.product.security.UserContext, java.lang.Long, java.util.Date, java.util.Date)
     */
    public <T extends OrderType> List<T> retrieveOrderHistory(UserContext uc, Long orderIdentification, Date fromHistoryDate, Date toHistoryDate) {

        return OrderSentenceHandler.retrieveOrderHistory(uc, context, session, orderIdentification, fromHistoryDate, toHistoryDate);
    }

    @Override
    public <T extends OrderType> List<T> getOrderBySupervisionIdForOutJurisdiction(@NotNull UserContext uc, @NotNull Long supervisionId, Boolean historyFlag) {
        return OrderSentenceHandler.getOrderBySupervisionIdForOutJurisdiction(uc, context, session, supervisionId, historyFlag);
    }

    @Override
    public List<SentenceType> retrieveSentencesBySupervisionId(@NotNull UserContext uc, @NotNull Long supervisionId) {

        return OrderSentenceHandler.retrieveSentencesBySupervisionId(uc, context, session, supervisionId);
    }

    @Override
    public SupervisionSentenceType retrieveSupervisionSentence(@NotNull UserContext uc, @NotNull Long supervisionId) {
        return OrderSentenceHandler.retrieveSupervisionSentence(uc, context, session, supervisionId);
    }

    @Override
    public List<SupervisionSentenceType> retrieveSupervisionSentenceWithHistory(@NotNull UserContext uc, @NotNull Long supervisionId) {
        return OrderSentenceHandler.retrieveSupervisionSentenceWithHistory(uc, context, session, supervisionId);
    }

    @Override
    public List<SupervisionSentenceType> retrieveSupervisionSentenceWithHistoryByKeyDateHistoryId(@NotNull UserContext uc, @NotNull Long supervisionId,
            @NotNull Long sentenceKeyDateHistoryId) {
        return OrderSentenceHandler.retrieveSupervisionSentenceWithHistoryByKeyDateHistoryId(uc, context, session, supervisionId, sentenceKeyDateHistoryId);
    }

    @Override
    public List<AggregateSentenceGroupType> retrieveAggregateSentencesBySupervisionId(@NotNull UserContext uc, @NotNull Long supervisionId) {

        return OrderSentenceHandler.retrieveAggregateSentencesBySupervisionId(uc, context, session, supervisionId);
    }

    @Override
    public List<AggregateSentenceGroupType> retrieveAggregateSentencesWithHistoryBySupervisionId(@NotNull UserContext uc, @NotNull Long supervisionId) {

        return OrderSentenceHandler.retrieveAggregateSentencesWithHistoryBySupervisionId(uc, context, session, supervisionId);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#retrieveOrdersForCase(syscon.arbutus.product.security.UserContext, java.lang.Long, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.OrderSearchType, java.lang.Boolean)
     */
    public <T extends OrderType> List<T> retrieveOrdersForCase(UserContext uc, Long caseIdentification, OrderSearchType orderSearch, Boolean isActiveFlag) {
        return OrderSentenceHandler.retrieveOrdersForCase(uc, context, session, caseIdentification, orderSearch, isActiveFlag);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#retrieveOrdersForCharge(syscon.arbutus.product.security.UserContext, java.util.Set, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.OrderSearchType, java.lang.Boolean)
     */
    public <T extends OrderType> List<T> retrieveOrdersForCharge(UserContext uc, Set<Long> chargeIdentifications, OrderSearchType orderSearch, Boolean isActiveFlag) {
        return OrderSentenceHandler.retrieveOrdersForCharge(uc, context, session, chargeIdentifications, orderSearch, isActiveFlag);
    }

    @SuppressWarnings("rawtypes")
    public OrdersReturnType searchOrders(@NotNull UserContext uc, Set<Long> orderIdentifications, Set<Long> caseIdentifications, Set<Long> chargeIdentifications,
            Set<Long> conditionIdentifications, OrderSearchType orderSearch, Boolean historyFlag, Long startIndex, Long resultSize, String resultOrder) {

        return OrderSentenceHandler.searchOrders(uc, context, session, orderIdentifications, caseIdentifications, chargeIdentifications, conditionIdentifications,
                orderSearch, historyFlag, startIndex, resultSize, resultOrder);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#searchOrders(syscon.arbutus.product.security.UserContext, java.lang.Long, java.lang.Long, java.lang.Long, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.OrderSearchType, java.lang.Boolean)
     */
    public <T extends OrderType> List<T> searchOrders(UserContext uc, Long caseIdentification, Long chargeIdentification, Long conditionIdentification,
            OrderSearchType orderSearch, Boolean historyFlag) {

        return OrderSentenceHandler.searchOrders(uc, context, session, caseIdentification, chargeIdentification, conditionIdentification, orderSearch, historyFlag);
    }

    @Override
    public List<DispositionType> retrieveDispositionHistoryByOrderId(@NotNull UserContext uc, @NotNull Long orderId, Date fromHistoryDate, Date toHistoryDate) {
        return OrderSentenceHandler.retrieveDispositionHistoryByOrderId(uc, context, session, orderId, fromHistoryDate, toHistoryDate);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#deleteOrder(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    public Long deleteOrder(UserContext uc, Long orderIdentification) {
        return OrderSentenceHandler.deleteOrder(uc, context, session, orderIdentification);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#createWarrantDetainer(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.WarrantDetainerType)
     */
    public WarrantDetainerType createWarrantDetainer(UserContext uc, WarrantDetainerType warrantDetainer) {
        return OrderSentenceHandler.createWarrantDetainer(uc, context, session, warrantDetainer);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#updateWarrantDetainer(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.WarrantDetainerType)
     */
    public WarrantDetainerType updateWarrantDetainer(UserContext uc, WarrantDetainerType warrantDetainer) {
        return OrderSentenceHandler.updateWarrantDetainer(uc, context, session, warrantDetainer);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#createBail(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.BailType)
     */
    public BailType createBail(UserContext uc, BailType bail) {
        return OrderSentenceHandler.createBail(uc, context, session, bail);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#updateBail(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.BailType)
     */
    public BailType updateBail(UserContext uc, BailType bail) {
        return OrderSentenceHandler.updateBail(uc, context, session, bail);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#createLegalOrder(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.LegalOrderType)
     */
    public LegalOrderType createLegalOrder(UserContext uc, LegalOrderType legalOrder) {
        return OrderSentenceHandler.createLegalOrder(uc, context, session, legalOrder);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#updateLegalOrder(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.LegalOrderType)
     */
    public LegalOrderType updateLegalOrder(UserContext uc, LegalOrderType legalOrder) {
        return OrderSentenceHandler.updateLegalOrder(uc, context, session, legalOrder);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#createSentence(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.SentenceType)
     */
    public SentenceType createSentence(UserContext uc, SentenceType sentence) {
        return OrderSentenceHandler.createSentence(uc, context, session, sentence);
    }

    @Override
    public Long getMaxSentenceNumberBySupervisionId(@NotNull UserContext uc, @NotNull Long supervisionId) {
        return OrderSentenceHandler.getMaxSentenceNumberBySupervisionId(uc, context, session, supervisionId);
    }

    @Override
    public Long getMaxSentenceNumberByCaseInfo(UserContext uc, Set<Long> caseInfoIds) {
        return OrderSentenceHandler.getMaxSentenceNumberByCaseInfo(uc, context, session, caseInfoIds);
    }

    @Override
    public Date retrieveSentenceIssuanceDateByChargeId(@NotNull UserContext uc, @NotNull Long chargeId) {
        return OrderSentenceHandler.retrieveSentenceIssuanceDateByChargeId(uc, context, session, chargeId);
    }

    public CodeType getOrderDispositionStatus(UserContext uc, CodeType dispositionOutcome) {
        return OrderSentenceHandler.getDispositionStatus(uc, context, session, dispositionOutcome);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#updateSentence(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.SentenceType)
     */
    public SentenceType updateSentence(UserContext uc, SentenceType sentence) {
        return OrderSentenceHandler.updateSentence(uc, context, session, sentence);
    }

    @Override
    public SentenceKeyDateType createSentenceKeyDate(UserContext uc, SentenceKeyDateType keyDates) {
        return OrderSentenceHandler.createSentenceKeyDate(uc, context, session, keyDates);
    }

    @Override
    public SentenceKeyDateType updateSentenceKeyDate(UserContext uc, SentenceKeyDateType keyDates) {
        return OrderSentenceHandler.updateSentenceKeyDate(uc, context, session, keyDates);
    }

    @Override
    public SentenceKeyDateType getSentenceKeyDate(UserContext uc, Long supervisionId) {
        return OrderSentenceHandler.getSentenceKeyDate(uc, context, session, supervisionId);
    }

    @Override
    public SentenceKeyDateType findSentenceKeyDateBySupervisionId(UserContext uc, Long supervisionId) {
        return OrderSentenceHandler.findSentenceKeyDateBySupervisionId(uc, context, session, supervisionId);
    }

    @Override
    public List<SentenceKeyDateType> getSentenceKeyDateHistory(UserContext uc, Long supervisionId) {
        return OrderSentenceHandler.getSentenceKeyDateHistory(uc, context, session, supervisionId);
    }

    @Override
    public List<SentenceKeyDateType> getSentenceKeyDateHistoryBySupervisionSentenceId(@NotNull UserContext uc, @NotNull Long supervisionId,
            @NotNull Long supervisionSentenceId) {
        return OrderSentenceHandler.getSentenceKeyDateHistoryBySupervisionSentenceId(uc, context, session, supervisionId, supervisionSentenceId);
    }

    @Override
    public List<KeyDateType> getSentenceKeyDateHistory(@NotNull UserContext uc, @NotNull Long supervisionId, @NotNull String keyDateType) {
        return OrderSentenceHandler.getSentenceKeyDateHistory(uc, context, session, supervisionId, keyDateType);
    }

    @Override
    public Long deleteSentenceKeyDate(UserContext uc, Long supervisionId) {
        return OrderSentenceHandler.deleteSentenceKeyDate(uc, context, session, supervisionId);
    }

    @Override
    public SentenceKeyDateType calculateSentenceKeyDates(@NotNull UserContext uc, @NotNull Long supervisionId, @NotNull Long staffId, String comment,
            Boolean reviewRequired) {
        return OrderSentenceHandler.calculateSentenceKeyDates(uc, context, session, supervisionId, staffId, comment, reviewRequired);
    }

    @Override
    public Boolean isSentenceCalculationEnabled(@NotNull UserContext uc, @NotNull Long supervisionId) {
        return OrderSentenceHandler.isSentenceCalculationEnabled(uc, context, session, supervisionId);
    }

    @Override
    public Long daysLeftToServeForSupervision(@NotNull UserContext uc, @NotNull Long supervisionId) {
        return OrderSentenceHandler.daysLeftToServeForSupervision(uc, context, session, supervisionId);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#createOrderConfiguration(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.OrderConfigurationType)
     */
    public OrderConfigurationType setOrderConfiguration(UserContext uc, OrderConfigurationType orderConfiguration) {
        return OrderSentenceHandler.setOrderConfiguration(uc, context, session, orderConfiguration);
    }

    @Override
    public OrderConfigurationType getOrderConfiguration(@NotNull UserContext uc, @NotNull Long orderConfigId) {
        return OrderSentenceHandler.getOrderConfiguration(uc, context, session, orderConfigId);
    }

    @Override
    public OrderConfigurationType updateOrderConfiguration(@NotNull UserContext uc, OrderConfigurationType orderConfiguration) {
        return OrderSentenceHandler.updateOrderConfiguration(uc, context, session, orderConfiguration);
    }

    @Override
    public Long deleteOrderConfiguration(@NotNull UserContext uc, @NotNull Long orderConfigId) {
        return OrderSentenceHandler.deleteOrderConfiguration(uc, context, session, orderConfigId);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#retrieveOrderConfiguration(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.OrderConfigurationType)
     */
    public OrderConfigurationReturnType retrieveOrderConfiguration(UserContext uc, OrderConfigurationType orderConfiguration, Long startIndex, Long resultSize,
            String resultOrder) {

        return OrderSentenceHandler.retrieveOrderConfiguration(uc, context, session, orderConfiguration, startIndex, resultSize, resultOrder);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#retrieveOrderConfiguration(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.OrderConfigurationType)
     */
    public Set<OrderConfigurationType> retrieveOrderConfiguration(UserContext uc, OrderConfigurationType orderConfiguration) {
        return OrderSentenceHandler.retrieveOrderConfiguration(uc, context, session, orderConfiguration);
    }

    @Override
    public Long deleteOrderConfiguration(@NotNull UserContext uc, OrderConfigurationType orderConfiguration) {
        return OrderSentenceHandler.deleteOrderConfiguration(uc, context, session, orderConfiguration);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#setIntermittentDurationConfiguration(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.IntermittentDurationConfigurationType)
     */
    public CodeType setIntermittentDurationConfiguration(UserContext uc, String intermittentDurationConfiguration) {
        return OrderSentenceHandler.setIntermittentDurationConfiguration(uc, context, session, intermittentDurationConfiguration);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#getIntermittentDurationConfiguration(syscon.arbutus.product.security.UserContext)
     */
    public CodeType getIntermittentDurationConfiguration(UserContext uc) {
        return OrderSentenceHandler.getIntermittentDurationConfiguration(uc, context, session);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#deleteIntermittentDurationConfiguration(syscon.arbutus.product.security.UserContext)
     */
    public Long deleteIntermittentDurationConfiguration(UserContext uc) {
        return OrderSentenceHandler.deleteIntermittentDurationConfiguration(uc, context, session);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#setSentenceNumberConfiguration(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.casemanagement.contract.dto.ordersentence.SentenceNumberConfigurationType)
     */
    public Boolean setSentenceNumberConfiguration(UserContext uc, Boolean sentenceNumberConfiguration) {
        return OrderSentenceHandler.setSentenceNumberConfiguration(uc, context, session, sentenceNumberConfiguration);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#getSentenceNumberConfiguration(syscon.arbutus.product.security.UserContext)
     */
    public Boolean getSentenceNumberConfiguration(UserContext uc) {
        return OrderSentenceHandler.getSentenceNumberConfiguration(uc, context, session);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.OrderSentence#deleteSentenceNumberConfiguration(syscon.arbutus.product.security.UserContext)
     */
    public Long deleteSentenceNumberConfiguration(UserContext uc) {
        return OrderSentenceHandler.deleteSentenceNumberConfiguration(uc, context, session);
    }

    @Override
    public void setKeyDateViewByAggregated(UserContext uc, Boolean isAggregateKeyDateView) {
        OrderSentenceHandler.setKeyDateViewByAggregated(uc, context, session, isAggregateKeyDateView);
    }

    @Override
    public Boolean isKeyDateViewByAggregated(UserContext uc) {
        return OrderSentenceHandler.isKeyDateViewByAggregated(uc, context, session);
    }

    @Override
    public Long deleteKeyDateViewByAggregatedConfiguration(UserContext uc) {
        return OrderSentenceHandler.deleteKeyDateViewByAggregatedConfiguration(uc, context, session);
    }

    public Long updateOrderDataSecurityStatus(UserContext uc, Long chargeId, Long dataSecurityStatus, String comments, Long personId, String parentEntityType,
            Long parentId, String memento, Boolean cascade) {

        Long ret = OrderSentenceHandler.updateOrderDataSecurityStatus(uc, context, session, chargeId, dataSecurityStatus, comments, personId, parentEntityType, parentId,
                memento, cascade);
        return ret;

    }

    public Long updateSentenceDataSecurityStatus(UserContext uc, Long sentId, Long dataSecurityStatus, String comments, Long personId, String parentEntityType,
            Long parentId, String memento, Boolean cascade) {

        Long ret = OrderSentenceHandler.updateSentenceDataSecurityStatus(uc, context, session, sentId, dataSecurityStatus, comments, personId, parentEntityType, parentId,
                memento, cascade);
        return ret;

    }

    @Override
    public void setSentenceAdjustmentConfiguration(UserContext uc, AdjustmentConfigType config) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        handler.setConfiguration(uc, config);
    }

    @Override
    public AdjustmentConfigType getSentenceAdjustmentConfiguration(UserContext uc, String sentenceAdjustmentType) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        return handler.getConfiguration(uc, sentenceAdjustmentType);
    }

    @Override
    public List<AdjustmentConfigType> getAllSentenceAdjustmentConfiguration(UserContext uc) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        return handler.getAllConfiguration(uc);
    }

    @Override
    public List<SentenceAdjustmentType> getSentenceAdjustmentsWithHistory(UserContext uc, Long supervisionId) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        return handler.getSentenceAdjustmentsWithHistory(uc, supervisionId);
    }

    @Override
    public Long deleteSentenceAdjustmentConfiguration(UserContext uc, String sentenceAdjustmentType) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        return handler.deleteConfiguration(uc, sentenceAdjustmentType);
    }

    @Override
    public Long deleteAllSentenceAdjustmentConfiguration(UserContext uc) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        return handler.deleteAllConfiguration(uc);
    }

    @Override
    public SentenceAdjustmentType createSentenceAdjustment(UserContext uc, SentenceAdjustmentType sentenceAdjustment) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        return handler.create(uc, sentenceAdjustment);
    }

    @Override
    public SentenceAdjustmentType getSentenceAdjustment(UserContext uc, Long supervisionId) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        return handler.get(uc, supervisionId);
    }

    @Override
    public SentenceAdjustmentType updateSentenceAdjustment(UserContext uc, SentenceAdjustmentType sentenceAdjustment) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        return handler.update(uc, sentenceAdjustment);
    }

    @Override
    public SentenceAdjustmentType addAdjustment(UserContext uc, Long supervisionId, AdjustmentType adjustment) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        return handler.addAdjustment(uc, supervisionId, adjustment);
    }

    @Override
    public SentenceAdjustmentType addAdjustments(UserContext uc, Long supervisionId, Set<AdjustmentType> adjustments) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        return handler.addAdjustments(uc, supervisionId, adjustments);
    }

    @Override
    public SentenceAdjustmentType removeAdjustment(UserContext uc, Long supervisionId, AdjustmentType adjustment) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        return handler.removeAdjustment(uc, supervisionId, adjustment);
    }

    @Override
    public SentenceAdjustmentType removeAdjustments(UserContext uc, Long supervisionId, Set<AdjustmentType> adjustments) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        return handler.removeAdjustments(uc, supervisionId, adjustments);
    }

    @Override
    public List<SentenceAdjustmentType> searchSentenceAdjustment(UserContext uc, Long supervisionId, SentenceAdjustmentSearchType search, Long startIndex,
            Long resultSize, String resultOrder) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session, searchMaxLimit);
        return handler.search(uc, supervisionId, search, startIndex, resultSize, resultOrder);
    }

    @Override
    public Long deleteSentenceAdjustment(UserContext uc, Long supervisionId) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        return handler.delete(uc, supervisionId);
    }

    @Override
    public Long deleteAllSentenceAdjustments(UserContext uc) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        return handler.deleteAllSentenceAdjustments(uc);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.casemanagement.contract.interfaces.CaseManagementService#getVersion(syscon.arbutus.product.security.UserContext)
     */
    public String getVersion(UserContext uc) {
        return "1.0";
    }

    public Long deleteAll(UserContext uc) {
        Long ret = ReturnCode.Success.returnCode();

        deleteAllJoinedTables();

        //delete all instance data
        deleteCaseAffiliations();
        deleteCaseActivities();
        deleteCaseInfoInstances();
        deleteChargeInstances();
        deleteConditionInstances();
        deleteOrderSentenceInstances();
        deleteSentenceAdjustments(uc);

        //delete all common data like reference code, links and configuration
        deleteAllCommonInstances();

        return ret;

    }

    /**
     *
     */
    private void deleteAllJoinedTables() {
        String deleteTable = "delete from Leg_CaseAffl_CaseAct";
        SQLQuery sqlQuery = session.createSQLQuery(deleteTable);
        sqlQuery.executeUpdate();

        deleteTable = "delete from Leg_CaseInfo_CaseAct";
        sqlQuery = session.createSQLQuery(deleteTable);
        sqlQuery.executeUpdate();

        deleteTable = "delete from LEG_CASEACT_ORDERINIT";
        sqlQuery = session.createSQLQuery(deleteTable);
        sqlQuery.executeUpdate();

        deleteTable = "delete from LEG_CASEACT_ORDERRslt";
        sqlQuery = session.createSQLQuery(deleteTable);
        sqlQuery.executeUpdate();

        // Clear Hibernate cache to add to avoid unique key violation.
        session.flush();
        session.clear();

    }

    ////////////////////////////////Composite APIs/////////////////////////////////

    public CaseType saveCase(UserContext uc, CaseType c) {
        return new CompositeHandler(context, session, searchMaxLimit).saveCase(uc, c);
    }

    public CaseType getCase(UserContext uc, Long caseId) {
        return new CompositeHandler(context, session, searchMaxLimit).getCase(uc, caseId);
    }

    public List<syscon.arbutus.product.services.legal.contract.dto.CaseInfoType> getCases(UserContext uc, CaseSearchType c) {
        return new CompositeHandler(context, session, searchMaxLimit).getCases(uc, c);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.LegalService#createCourtActivityWithMovement(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivityType, syscon.arbutus.product.services.legal.contract.dto.CourtMovementDetail)
     */
    @Override
    public CourtActivityMovementType createCourtActivityWithMovement(UserContext uc, CourtActivityMovementType courtActivityMovementType) {

        return new CompositeHandler(context, session, searchMaxLimit).createCourtActivityWithMovement(uc, courtActivityMovementType);

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.Composite#retrieveCourtActivityWithMovement(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    @Override
    public CourtActivityMovementType retrieveCourtActivityWithMovement(UserContext uc, Long courtActivityId) {

        return new CompositeHandler(context, session, searchMaxLimit).retrieveCourtActivityWithMovement(uc, courtActivityId);

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.Composite#retrieveCourtActivityWithMovements(syscon.arbutus.product.security.UserContext, java.lang.Long, java.lang.String)
     */
    @Override
    public List<CourtActivityMovementType> retrieveCourtActivityWithMovements(UserContext uc, Long caseId, String activityStatus) {
        return new CompositeHandler(context, session, searchMaxLimit).retrieveCourtActivityWithMovements(uc, caseId, activityStatus);

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.Composite#updateCourtActivityWithMovement(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.legal.contract.dto.composite.CourtActivityMovementType)
     */
    @Override
    public CourtActivityMovementType updateCourtActivityWithMovement(UserContext uc, CourtActivityMovementType courtActivityMovementType) {

        return new CompositeHandler(context, session, searchMaxLimit).updateCourtActivityWithMovement(uc, courtActivityMovementType);

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.Composite#inquiryCases(syscon.arbutus.product.security.UserContext, java.lang.String, java.lang.String, java.lang.Long, java.lang.Long, java.lang.String)
     */
    @Override
    public CaseOffendersReturnType inquireCaseOffenders(UserContext uc, InquiryCaseSearchType inquiryCaseSearchType, Long startIndex, Long resultSize,
            String resultOrder) {
        return new CompositeHandler(context, session, searchMaxLimit).inquireCaseOffenders(uc, inquiryCaseSearchType, startIndex, resultSize, resultOrder);

    }

    @Override
    public BailDetailsType getBailDetails(UserContext uc, Long bailId) {
        return new CompositeHandler(context, session, searchMaxLimit).getBailDetails(uc, bailId);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.Composite#updateChargesStatus(syscon.arbutus.product.security.UserContext, java.util.List, syscon.arbutus.product.services.legal.contract.dto.composite.ChargeStatusType)
     */
    @Override
    public void updateChargesStatus(UserContext uc, ChargesStatusType chargesStatusType) {
        new ChargeHandler(context, session, searchMaxLimit).updateChargesStatus(uc, chargesStatusType);
    }

    @Override
    public List<ChargeCodeDescriptionType> retrieveChargeCodeDescriptions(UserContext uc, Long statuteId, Date offenseDate) {
        return new ChargeHandler(context, session, searchMaxLimit).retrieveChargeCodeDescriptions(uc, statuteId, offenseDate);
    }

    @Override
    public List<StatuteChargeConfigType> retrieveStatuteChargeConfigsByCodeDescription(UserContext uc, ChargeCodeDescriptionType chargeCodeDescriptionType) {
        return new ChargeHandler(context, session, searchMaxLimit).retrieveStatuteChargeConfigsByCodeDescription(uc, chargeCodeDescriptionType);
    }

    ///////////////////////////////////////////////////////////////////
    // Client Custom Field implementation
    ///////////////////////////////////////////////////////////////////
    @Override
    public String getCCFMeta(UserContext uc, DTOBindingTypeEnum legalModule, String language) {
        return ClientCustomFieldHandler.getCCFMeta(uc, context, session, legalModule, language);
    }

    @Override
    public ClientCustomFieldMetaType getClientCustomFieldMeta(UserContext uc, DTOBindingTypeEnum legalModule, String name) {
        return ClientCustomFieldHandler.getClientCustomFieldMeta(uc, context, session, legalModule, name);
    }

    @Override
    public List<ClientCustomFieldMetaType> getAllCCFMeta(UserContext uc, DTOBindingTypeEnum legalModule) {
        return ClientCustomFieldHandler.getAllCCFMeta(uc, context, session, legalModule);
    }

    @Override
    public ClientCustomFieldValueType saveCCFMetaValue(UserContext uc, ClientCustomFieldValueType ccfValue) {
        return ClientCustomFieldHandler.saveCCFMetaValue(uc, context, session, ccfValue);
    }

    @Override
    public List<ClientCustomFieldValueType> saveCCFMetaValues(UserContext uc, List<ClientCustomFieldValueType> ccfValues) {
        return ClientCustomFieldHandler.saveCCFMetaValues(uc, context, session, ccfValues);
    }

    @Override
    public List<ClientCustomFieldValueType> getCCFMetaValues(UserContext uc, ClientCustomFieldValueSearchType searchType) {
        return ClientCustomFieldHandler.getCCFMetaValues(uc, context, session, searchType);
    }

    @Override
    public ClientCustomFieldValueType updateCCFMetaValue(UserContext uc, ClientCustomFieldValueType ccfValue) {
        return ClientCustomFieldHandler.updateCCFMetaValue(uc, context, session, ccfValue);
    }

    @Override
    public List<ClientCustomFieldValueType> updateCCFMetaValues(UserContext uc, List<ClientCustomFieldValueType> ccfValues) {
        return ClientCustomFieldHandler.updateCCFMetaValues(uc, context, session, ccfValues);
    }

    @Override
    public IntermittentSentenceScheduleType createIntermittentSentenceSchedule(UserContext userContext, IntermittentSentenceScheduleType intermittentSentenceSchedule) {
        return new OrderSentenceHandler().createIntermittentSchedule(context, session, userContext, intermittentSentenceSchedule);
    }

    @Override
    public IntermittentSentenceScheduleType getIntermittentSentenceSchedule(UserContext userContext, Long scheduleId) {
        return new OrderSentenceHandler().getIntermittentSentenceSchedule(context, session, userContext, scheduleId);
    }

    @Override
    public IntermittentSentenceScheduleReturnType updateIntermittentSentenceSchedule(UserContext userContext,
            IntermittentSentenceScheduleType intermittentSentenceSchedule) {
        return new OrderSentenceHandler().updateIntermittentSchedule(context, session, userContext, intermittentSentenceSchedule);
    }

    @Override
    public Set<IntermittentSentenceScheduleType> getIntermittentSentenceSchedules(UserContext userContext, Long orderId) {
        return new OrderSentenceHandler().getIntermittentSentenceSchedules(context, session, userContext, orderId);
    }

    @Override
    public Long getRemainingNumberOfSentenceDays(UserContext userContext, Long sentenceId) {
        return new OrderSentenceHandler().getRemainingNumberOfSentenceDays(context, session, userContext, sentenceId);
    }

    ////////////////////////////////////////private methods///////////////////////////////////////

    /**
     * Delete all charge related instance data, associations from database.
     */
    private void deleteChargeInstances() {

        session.createQuery("delete ExternalChargeCodeConfigEntity").executeUpdate();
        session.createQuery("delete JurisdictionConfigEntity").executeUpdate();

        //session.createQuery("delete StatuteChargeConfigEntity").executeUpdate();
        Criteria c = session.createCriteria(StatuteChargeConfigEntity.class);
        @SuppressWarnings("unchecked") Iterator<StatuteChargeConfigEntity> it = c.list().iterator();
        while (it.hasNext()) {
			session.delete(it.next());
		}

        session.createQuery("delete StatuteChargeIndicatorEntity").executeUpdate();
        session.createQuery("delete StatuteChargeTextEntity").executeUpdate();
        session.createQuery("delete StatuteConfigEntity").executeUpdate();
        session.createQuery("delete ChargeModuleAssociationEntity").executeUpdate();
        session.createQuery("delete ChargeIdentifierHistEntity").executeUpdate();
        session.createQuery("delete ChargeIdentifierEntity").executeUpdate();
        session.createQuery("delete ChargeIndicatorHistEntity").executeUpdate();
        session.createQuery("delete ChargeIndicatorEntity").executeUpdate();
        session.createQuery("delete ChargeHistEntity").executeUpdate();
        session.createQuery("delete ChargeEntity").executeUpdate();

        //Need to delete chargeEntity first, then delete the following entity because FK
        session.createQuery("delete ChargeAppealHistEntity").executeUpdate();
        session.createQuery("delete ChargeAppealEntity").executeUpdate();
        session.createQuery("delete ChargePleaHistEntity").executeUpdate();
        session.createQuery("delete ChargePleaEntity").executeUpdate();
        session.createQuery("delete ChargeLegalTextHistEntity").executeUpdate();
        session.createQuery("delete ChargeLegalTextEntity").executeUpdate();

        session.flush();
        session.clear();

    }

    /**
     *
     */
    private void deleteCaseAffiliations() {

        // Clear Hibernate cache to add to avoid unique key violation.
        session.flush();
        session.clear();

        session.createQuery("delete CaseAffiliationEntity").executeUpdate();

        // Clear Hibernate cache to add to avoid unique key violation.
        session.flush();
        session.clear();

    }

    /**
     * Delete all case activity related instance data, associations from database.
     */
    private void deleteCaseActivities() {
        session.createQuery("delete CaseActivityStaticAssociationEntity").executeUpdate();
        session.createQuery("delete CaseActivityStaticAssocHistEntity").executeUpdate();
        session.createQuery("delete CaseActivityHistEntity").executeUpdate();
        session.createQuery("delete CAActivityIdEntity").executeUpdate();
        session.createQuery("delete CaseActivityEntity").executeUpdate();

        // Clear Hibernate cache to add to avoid unique key violation.
        session.flush();
        session.clear();
    }

    /**
     * Delete all charge related instance data, associations from database.
     */
    private void deleteCaseInfoInstances() {

        session.createQuery("delete CaseIdentifierConfigEntity").executeUpdate();
        session.createQuery("delete CaseIdentifierHistEntity").executeUpdate();
        session.createQuery("delete CaseIdentifierEntity").executeUpdate();
        session.createQuery("delete CaseInfoModuleAssociationHistEntity").executeUpdate();
        session.createQuery("delete CaseInfoModuleAssociationEntity").executeUpdate();
        session.createQuery("delete CaseInfoHistEntity").executeUpdate();
        session.createQuery("delete CaseStatusChangeHistEntity").executeUpdate();
        session.createQuery("delete CaseInfoEntity").executeUpdate();

        session.flush();
        session.clear();
    }

    /**
     * Delete all condition related instance data, associations from database.
     */
    private void deleteConditionInstances() {

        session.createQuery("delete ConditionCommentEntity").executeUpdate();
        session.createQuery("delete ConditionCommentHistoryEntity").executeUpdate();
        session.createQuery("delete ConditionModuleAssociationEntity").executeUpdate();
        session.createQuery("delete ConditionModuleAssociationHistEntity").executeUpdate();
        session.createQuery("delete ConditionEntity").executeUpdate();
        session.createQuery("delete ConditionHistoryEntity").executeUpdate();

        session.flush();
        session.clear();
    }

    private void deleteOrderSentenceInstances() {

        session.createSQLQuery("UPDATE LEG_OrdOrder SET flag = 1").executeUpdate();
        Criteria c = session.createCriteria(OrderEntity.class);
        c.setProjection(Projections.id());
        @SuppressWarnings("unchecked") Set<Long> ids = new HashSet<Long>(c.list());
        for (Long id : ids) {
            OrderEntity entity = (OrderEntity) session.get(OrderEntity.class, id);
            session.delete(entity);
        }
        Criteria cHistory = session.createCriteria(OrderHistoryEntity.class);
        cHistory.setProjection(Projections.id());
        @SuppressWarnings("unchecked") Set<Long> historyIds = new HashSet<Long>(cHistory.list());
        for (Long id : historyIds) {
            OrderHistoryEntity entity = (OrderHistoryEntity) session.get(OrderHistoryEntity.class, id);
            session.delete(entity);
        }

        session.createQuery("delete CaseActivityInitOrderHstEntity").executeUpdate();

        session.createQuery("delete OrderChargeAssocHistEntity").executeUpdate();
        session.createQuery("delete OrderChargeAssocEntity").executeUpdate();

        session.createQuery("delete BailAmountEntity").executeUpdate();
        session.createQuery("delete BailAmountHistoryEntity").executeUpdate();
        session.createQuery("delete BailPersonIdAssocEntity").executeUpdate();
        session.createQuery("delete BailPersonIdAssocHistEntity").executeUpdate();
        session.createQuery("delete OrderPostedChgAssocEntity").executeUpdate();
        session.createQuery("delete OrderPostedChgAssocHistEntity").executeUpdate();
        session.createQuery("delete BailPostedAmountEntity").executeUpdate();
        session.createQuery("delete BailPostedAmountHistEntity").executeUpdate();
        session.createQuery("delete BondChargeAssocEntity").executeUpdate();
        session.createQuery("delete BondChargeAssocHistEntity").executeUpdate();
        session.createQuery("delete BondOrgAssocEntity").executeUpdate();
        session.createQuery("delete BondOrgAssocHistEntity").executeUpdate();
        session.createQuery("delete BondPostedAmountEntity").executeUpdate();
        session.createQuery("delete BondPostedAmountHistEntity").executeUpdate();
        //session.createQuery("delete CaseActivityInitOrderEntity").executeUpdate();
        //session.createQuery("delete CaseActivityInitOrderHstEntity").executeUpdate();
        session.createQuery("delete IntermDurationConfigEntity").executeUpdate();
        session.createQuery("delete IntermittentScheduleEntity").executeUpdate();
        session.createQuery("delete IntermittentScheduleHistEntity").executeUpdate();
        session.createQuery("delete NotifyAgentLocAssocEntity").executeUpdate();
        session.createQuery("delete NotifyAgentLocAssocHistEntity").executeUpdate();
        session.createQuery("delete NotifyAgentContactEntity").executeUpdate();
        session.createQuery("delete NotifyAgentContactHistEntity").executeUpdate();
        session.createQuery("delete NotificationLogEntity").executeUpdate();
        session.createQuery("delete NotificationLogHistoryEntity").executeUpdate();
        session.createQuery("delete OrderModuleAssociationEntity").executeUpdate();
        session.createQuery("delete SentenceAppealEntity").executeUpdate();
        session.createQuery("delete SentenceAppealHistoryEntity").executeUpdate();
        session.createQuery("delete SentenceNumberConfigEntity").executeUpdate();
        session.createQuery("delete TermEntity").executeUpdate();
        session.createQuery("delete TermHistoryEntity").executeUpdate();
        session.createQuery("delete SentenceEntity").executeUpdate();
        session.createQuery("delete SentenceHistoryEntity").executeUpdate();
        session.createQuery("delete BailEntity").executeUpdate();
        session.createQuery("delete BailHistoryEntity").executeUpdate();
        session.createQuery("delete LegalOrderEntity").executeUpdate();
        session.createQuery("delete LegalOrderHistoryEntity").executeUpdate();
        session.createQuery("delete WarrantDetainerEntity").executeUpdate();
        session.createQuery("delete WarrantDetainerHistoryEntity").executeUpdate();

        session.createQuery("delete OrderEntity").executeUpdate();
        session.createQuery("delete OrderHistoryEntity").executeUpdate();

        session.createQuery("delete DispositionEntity").executeUpdate();
        session.createQuery("delete DispositionHistoryEntity").executeUpdate();
        session.createQuery("delete NotificationEntity").executeUpdate();
        session.createQuery("delete NotificationHistoryEntity").executeUpdate();
        session.createQuery("delete OrderConfigurationEntity").executeUpdate();
        session.createQuery("delete KeyDateEntity").executeUpdate();
        session.createQuery("delete SentenceKeyDateEntity").executeUpdate();
        session.createQuery("delete KeyDateHistEntity").executeUpdate();
        session.createQuery("delete SentenceKeyDateHistEntity").executeUpdate();

        session.flush();
        session.clear();
    }

    private void deleteSentenceAdjustments(UserContext uc) {
        SentenceAdjustmentHandler handler = new SentenceAdjustmentHandler(context, session);
        handler.deleteAllConfiguration(uc);
        handler.deleteAllSentenceAdjustments(uc);
    }

    /**
     * Delete all associations, reference codes & links from database.
     */
    private void deleteAllCommonInstances() {
        // session.createQuery("delete PrivilegeEntity").executeUpdate(); No need to be deleted because no data existed.
        session.createQuery("delete ConfigEntity").executeUpdate();

        session.flush();
        session.clear();
    }

    /**
     * Load service properties such as searchMaxLimit. This will be called when the bean is created.
     */
    @SuppressWarnings({ "rawtypes" })
    @PostConstruct
    private void initialize() {

        ClassLoader loader = this.getClass().getClassLoader();

        try {
            ResourceBundle rb = ResourceBundle.getBundle("service", Locale.getDefault(), loader);
            for (Enumeration keys = rb.getKeys(); keys.hasMoreElements(); ) {
                String key = (String) keys.nextElement();
                if ("searchMaxLimit".equalsIgnoreCase(key)) {
                    String value = rb.getString(key);
                    searchMaxLimit = Integer.parseInt(value);
                    return;
                }
            }
        } catch (Exception ex) {
            log.info(ex.toString());
        }
    }

    @SuppressWarnings("unused")
    private void removeCurrencyConfig() {
        if (log.isDebugEnabled()) {
			log.debug("removeCurrencyConfig: begin");
		}

        LegalHelper.removeConfigEntity(session, ReferenceSet.CURRENCY.value());

        if (log.isDebugEnabled()) {
			log.debug("removeCurrencyConfig: end");
		}
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * syscon.arbutus.product.services.eventlog.contract.interfaces.EventLogService
     * #getPrimaryCCFValue(syscon.arbutus.product.security.UserContext,
     * java.util.Set,
     * syscon.arbutus.product.services.clientcustomfield.contract.
     * dto.DTOBindingTypeEnum)
     */
    @Override
    public Map<Long, List<ClientCustomFieldValueType>> getPrimaryCCFValue(UserContext uc, Set<Long> idBindingSet, DTOBindingTypeEnum dtoBinding) {
        return ClientCustomFieldServiceAdapter.getPrimaryCCFValue(uc, idBindingSet, dtoBinding);
    }

    @Override
    public List<ExternalChargeCodeConfigType> searchAllExternalChargeCode(UserContext uc) {
        ChargeHandler handler = new ChargeHandler(context, session, searchMaxLimit);

        List<ExternalChargeCodeConfigType> ret = handler.searchAllExternalChargeCode(uc);
        return ret;
    }

    @Override
    public SentenceCalculatorReturnType calculateFine(UserContext userContext, SentenceCalculatorType sentenceCalculator) {
        ValidationHelper.validate(userContext);
        ValidationHelper.validate(sentenceCalculator);
        return FinePaymentCalculator.calculateFine(ksession, sentenceCalculator);
    }

    @Override
    public Map<Long, String> getCaseInfoForSentence(UserContext uc, List<Long> caseList) {
        ValidationHelper.validate(uc);
        Map<Long, String> caseIdentfierMap = new HashMap<>();
        for (Long caseid : caseList) {
            CaseType caseType = getCase(uc, caseid);
            if (null != caseType && null != caseType.getPrimaryCaseIdentifier()) {
				caseIdentfierMap.put(caseid, caseType.getPrimaryCaseIdentifier().getIdentifierValue());
			}
        }

        return caseIdentfierMap;
    }

    @Override
    public FinePaymentType recordFinePayment(UserContext userContext, FinePaymentType finePayment) {
        ValidationHelper.validate(userContext);
        ValidationHelper.validate(finePayment);
        return new OrderSentenceHandler().recordFinePayment(userContext, context, session, finePayment);
    }

    @Override
    public FinePaymentReturnType getFinePaymentBySentenceId(UserContext userContext, Long sentenceId) {
        ValidationHelper.validate(userContext);
        return new OrderSentenceHandler().getFinePaymentBySentenceId(userContext, context, session, sentenceId);
    }

    @Override
    public SentenceTermType createSentenceTerm(UserContext uc, SentenceTermType sentenceTermType) {
        CompositeHandler handler = new CompositeHandler(context, session);
        sentenceTermType = handler.createSentenceTerm(uc, sentenceTermType);
        return sentenceTermType;
    }

    @Override
    public SentenceTermType updateSentenceTerm(UserContext uc, SentenceTermType sentenceTermType) {
        CompositeHandler handler = new CompositeHandler(context, session);
        sentenceTermType = handler.updateSentenceTerm(uc, sentenceTermType);
        return sentenceTermType;
    }

    @Override
    public SentenceTermType getSentenceTerm(UserContext uc, Long sentenceTermId) {
        CompositeHandler handler = new CompositeHandler(context, session);
        SentenceTermType sentenceTermType = handler.getSentenceTerm(uc, sentenceTermId);
        return sentenceTermType;
    }

    @Override
    public List<SentenceTermType> getAllSentenceTerm(UserContext uc, Long startIndex, Long resultSize, String resultOrder) {
        CompositeHandler handler = new CompositeHandler(context, session);
        List<SentenceTermType> sentenceTermTypeList = handler.getAllSentenceTerm(uc, startIndex, resultSize, resultOrder);
        return sentenceTermTypeList;
    }

    @Override
    public List<SentenceTermType> searchSentenceTerm(UserContext uc, SearchSentenceTermType searchType, Long startIndex, Long resultSize, String resultOrder) {
        CompositeHandler handler = new CompositeHandler(context, session);
        List<SentenceTermType> sentenceTermTypeList = handler.searchSentenceTerm(uc, searchType, startIndex, resultSize, resultOrder);
        return sentenceTermTypeList;
    }

    @Override
    public Sentence getSentenceDetails(UserContext uc, Long sentenceId) {
        SentenceType sentenceType = getOrder(uc, sentenceId);
        return OrderSentenceHandler.getIntermittentSentenceWithCounter(uc, context, session, sentenceType);
    }

    /**
     * This method delete the schedule
     */
    @Override
    public Long deleteSentenceSchedule(UserContext uc, Long scheduleId) {

        return OrderSentenceHandler.deleteIntermittentSchedule(uc, context, session, scheduleId);
    }

    @Override
    public StatuteConfigType getStatuteConfigId(UserContext uc, Long statuteId) {
        return new ChargeHandler(context, session).getStatuteConfigId(uc, statuteId);
    }

    @Override
    public SentenceTermNameType createSentenceTermName(UserContext uc, SentenceTermNameType sentenceTermTypeName) {
        CompositeHandler handler = new CompositeHandler(context, session);
        sentenceTermTypeName = handler.createSentenceTermName(uc, sentenceTermTypeName);
        return sentenceTermTypeName;
    }

    @Override
    public boolean isChargeByOrganization(UserContext uc, Long organizationId) {
        return new ChargeHandler(context, session).isChargeByOrganization(organizationId);
    }

    @Override
    public boolean isCaseByOrganization(UserContext uc, Long organizationId) {
        return new CaseInfoHandler(context, session).isCaseByOrganization(organizationId);
    }

    @Override
    public boolean isCaseAffiliationByOrganization(UserContext uc, Long organizationId) {
        return new CaseAffiliationHandler(context, session).isCaseAffiliationByOrganization(organizationId);
    }

    @Override
    public boolean hasCaseAffiliation(UserContext uc, Long personIdA, Long personIdB) {
        // TODO: implement this if case affiliation logic is ready, return false for now
        // this function is the same as CaseAffiliation.hasCaseaffiliation(..)
        return false;
    }


    public boolean hasActiveLegalDocumentsForSupervision(UserContext uc, Long supervisionId, Boolean isActive) {
        List<syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType> caseInfoTypes = retrieveCaseInfos(uc, supervisionId, isActive);
        if (caseInfoTypes != null && !caseInfoTypes.isEmpty()) {
            return true;
        }

        //This is to check out of juridiction Order
        List<OrderType> orderTypeList = getOrderBySupervisionIdForOutJurisdiction(uc, supervisionId, false);

        if (orderTypeList != null && !orderTypeList.isEmpty()) {
            for (OrderType orderType : orderTypeList) {
                if (orderType.getOrderDisposition() != null && orderType.getOrderDisposition().getOrderStatus() != null
                        && orderType.getOrderDisposition().getOrderStatus().equalsIgnoreCase("ACTIVE")) {
                    return true;
                }
            }
        }
        return false;
    }

}
