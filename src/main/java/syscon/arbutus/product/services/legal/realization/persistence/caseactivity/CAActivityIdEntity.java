package syscon.arbutus.product.services.legal.realization.persistence.caseactivity;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the static activity id entity.
 *
 * @author lhan
 * @DbComment LEG_CAActivityId 'CaseActivity to ActivityId table'
 * @DbComment .createUserId 'Create User Id'
 * @DbComment .createDateTime 'Create Date Time'
 * @DbComment .modifyUserId 'Modify User Id'
 * @DbComment .modifyDateTime 'Modify Date Time'
 * @DbComment .invocationContext 'Invocation Context'
 */
@Audited
@Entity
@Table(name = "LEG_CAActivityId")
public class CAActivityIdEntity implements Serializable {

    private static final long serialVersionUID = -7882005373643292107L;

    /**
     * @DbComment CaseActActivityId 'The unique id of CaseActActivityId record'
     */
    @Id
    @Column(name = "CaseActActivityId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "Seq_LEG_CAActivityId_Id")
    @SequenceGenerator(name = "Seq_LEG_CAActivityId_Id", sequenceName = "Seq_LEG_CAActivityId_Id", allocationSize = 1)
    private Long caseActActivityId;

    /**
     * @DbComment ActivityId 'The Id of Activity record in ACT_Activity table'
     */
    @Column(name = "ActivityId", nullable = false)
    private Long activityId;

    /**
     * @DbComment CaseActivityId 'Case Activity Identification. The unique ID for each case related event/activity created in the system.'
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CaseActivityId", nullable = false)
    private CaseActivityEntity caseActivity;

    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * Default constructor
     */
    public CAActivityIdEntity() {
    }

    /**
     * @return the caseActActivityId
     */
    public Long getCaseActActivityId() {
        return caseActActivityId;
    }

    /**
     * @param caseActActivityId the caseActActivityId to set
     */
    public void setCaseActActivityId(Long caseActActivityId) {
        this.caseActActivityId = caseActActivityId;
    }

    /**
     * @return the caseActivity
     */
    public CaseActivityEntity getCaseActivity() {
        return caseActivity;
    }

    /**
     * @param caseActivity the caseActivity to set
     */
    public void setCaseActivity(CaseActivityEntity caseActivity) {
        this.caseActivity = caseActivity;
    }

    /**
     * @return the activityId
     */
    public Long getActivityId() {
        return activityId;
    }

    /**
     * @param activityId the activityId to set
     */
    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    /**
     * Gets the value of the stamp property
     *
     * @return StampEntity
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * Sets the value of the stamp property
     *
     * @param stamp StampEntity
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((activityId == null) ? 0 : activityId.hashCode());
        result = prime * result + ((caseActActivityId == null) ? 0 : caseActActivityId.hashCode());
        result = prime * result + ((caseActivity == null) ? 0 : caseActivity.hashCode());
        result = prime * result + ((stamp == null) ? 0 : stamp.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        CAActivityIdEntity other = (CAActivityIdEntity) obj;
        if (activityId == null) {
            if (other.activityId != null) {
				return false;
			}
        } else if (!activityId.equals(other.activityId)) {
			return false;
		}
        if (caseActActivityId == null) {
            if (other.caseActActivityId != null) {
				return false;
			}
        } else if (!caseActActivityId.equals(other.caseActActivityId)) {
			return false;
		}
        if (caseActivity == null) {
            if (other.caseActivity != null) {
				return false;
			}
        } else if (!caseActivity.equals(other.caseActivity)) {
			return false;
		}
        if (stamp == null) {
            if (other.stamp != null) {
				return false;
			}
        } else if (!stamp.equals(other.stamp)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CAActivityIdEntity [caseActActivityId=" + caseActActivityId + ", activityId=" + activityId + ", caseActivity=" + caseActivity + ", stamp=" + stamp + "]";
    }

}
