package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import java.io.Serializable;
import java.util.List;

public class OrdersReturnType<T extends OrderType> implements Serializable {

    private static final long serialVersionUID = -175923916007754919L;

    private List<T> orders;

    private Long totalSize;

    /**
     * Get a list of orders(may include Bails, Legals, Sentences and WarrantDetainers)
     *
     * @return List&lt;OrderType>
     */
    public List<T> getOrders() {
        return orders;
    }

    /**
     * Set a list of orders(may include Bails, Legals, Sentences and WarrantDetainers)
     *
     * @param orders List&lt;OrderType>
     */
    public void setOrders(List<T> orders) {
        this.orders = orders;
    }

    /**
     * Gets the value of the totalSize property.
     *
     * @return the total size of search result
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * Sets the value of the totalSize property.
     *
     * @param totalSize the total size of search result
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    @Override
    public String toString() {
        return "OrdersReturnType [orders=" + orders + ", totalSize=" + totalSize + "]";
    }
}
