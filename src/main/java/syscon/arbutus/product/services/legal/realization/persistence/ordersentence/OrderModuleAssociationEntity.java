package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.Associable;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * OrderModuleAssociationEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdModuleAssc 'The module association table of CaseInfo Instance'
 * @DbComment LEG_OrdModuleAssc.AssociationId 'Unique id of the association record'
 * @DbComment LEG_OrdModuleAssc.FromIdentifier 'The case information instance Id'
 * @DbComment LEG_OrdModuleAssc.ToClass 'The association module that is referenced'
 * @DbComment LEG_OrdModuleAssc.ToIdentifier 'The unique id of the Association To Object'
 * @DbComment LEG_OrdModuleAssc.CreateUserId 'User ID who created the object'
 * @DbComment LEG_OrdModuleAssc.CreateDateTime 'Date and time when the object was created'
 * @DbComment LEG_OrdModuleAssc.InvocationContext 'Invocation context when the create/update action called'
 * @DbComment LEG_OrdModuleAssc.ModifyUserId 'User ID who last updated the object'
 * @DbComment LEG_OrdModuleAssc.ModifyDateTime 'Date and time when the object was last updated'
 * @since December 21, 2012
 */

@Audited
@Entity
@Table(name = "LEG_OrdModuleAssc")
public class OrderModuleAssociationEntity implements Serializable, Associable {

    private static final long serialVersionUID = 3373761969013472171L;

    @Id
    @Column(name = "AssociationId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_OrdModuleAssc_Id")
    @SequenceGenerator(name = "SEQ_LEG_OrdModuleAssc_Id", sequenceName = "SEQ_LEG_OrdModuleAssc_Id", allocationSize = 1)
    private Long associationId;

    @Column(name = "ToClass", nullable = false, length = 64)
    private String toClass;

    @Column(name = "ToIdentifier", nullable = false)
    private Long toIdentifier;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FromIdentifier", referencedColumnName = "orderId", nullable = false)
    private OrderEntity order;

    @NotAudited
    @Embedded
    private StampEntity stamp;

    public OrderModuleAssociationEntity() {
        super();
    }

    public OrderModuleAssociationEntity(Long associationId, String toClass, Long toIdentifier, StampEntity stamp) {
        super();
        this.associationId = associationId;
        this.toClass = toClass;
        this.toIdentifier = toIdentifier;
        this.stamp = stamp;
    }

    /**
     * @return the associationId
     */
    public Long getAssociationId() {
        return associationId;
    }

    /**
     * @param associationId the associationId to set
     */
    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    /**
     * @return the toClass
     */
    public String getToClass() {
        return toClass;
    }

    /**
     * @param toClass the toClass to set
     */
    public void setToClass(String toClass) {
        this.toClass = toClass;
    }

    /**
     * @return the toIdentifier
     */
    public Long getToIdentifier() {
        return toIdentifier;
    }

    /**
     * @param toIdentifier the toIdentifier to set
     */
    public void setToIdentifier(Long toIdentifier) {
        this.toIdentifier = toIdentifier;
    }

    /**
     * @return the order
     */
    public OrderEntity getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(OrderEntity order) {
        this.order = order;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((associationId == null) ? 0 : associationId.hashCode());
        result = prime * result + ((toClass == null) ? 0 : toClass.hashCode());
        result = prime * result + ((toIdentifier == null) ? 0 : toIdentifier.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        OrderModuleAssociationEntity other = (OrderModuleAssociationEntity) obj;
        if (associationId == null) {
            if (other.associationId != null) {
				return false;
			}
        } else if (!associationId.equals(other.associationId)) {
			return false;
		}
        if (toClass == null) {
            if (other.toClass != null) {
				return false;
			}
        } else if (!toClass.equals(other.toClass)) {
			return false;
		}
        if (toIdentifier == null) {
            if (other.toIdentifier != null) {
				return false;
			}
        } else if (!toIdentifier.equals(other.toIdentifier)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "OrderModuleAssociationEntity [associationId=" + associationId + ", toClass=" + toClass + ", toIdentifier=" + toIdentifier + "]";
    }

}
