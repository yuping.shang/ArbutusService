package syscon.arbutus.product.services.legal.contract.interfaces;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.exception.DataExistException;
import syscon.arbutus.product.services.legal.contract.dto.charge.*;

/**
 * Business contract interface for charge related actions.
 * <p><b>Purpose</b>
 * The CHARGE service offers the ability to record charges/offences related to a legal case, at arrest and during subsequent court proceedings.
 * A logical grouping of charges is maintained through the CASE service.
 * Charges are tracked through various stages (via the ORDER/SENTENCE service association) until the court dismisses or sentences the offender on each of his/her charges.
 * Charge information for an offence is governed by the Statute in effect for the jurisdiction where the offence occurred and the date on which the offence occurred.
 * </p>
 * <p><b>Scope</b>
 * Each instance of the CHARGE service will represent a charge filed against an offender’s legal case. The following functionality is supported by the service:
 * <ul>
 * <li>Create, maintain and search for charges on a case.</li>
 * <li>Maintain Statute information for active and inactive legislations in various jurisdictions.</li>
 * <li>Maintain the status (dismissed, appealed, convicted etc.,) of a charge.</li>
 * <li>Maintain the severity, type, degree and bail amount for each charge code.</li>
 * <li>Maintain the detailed legal text for each charge, as specified by the legislation.</li>
 * <li>Maintain the NCIC, UCR and other external code mapping for the charges.</li>
 * <li>Maintain the outcome of a charge based on past or present court hearings.</li>
 * <li>Delete charges.</li>
 * <li>Search and retrieval of history of changes on a charge.</li>
 * <li>Seal charges as directed by court order(s); seal functionality is to be available to users with appropriate user data privileges.</li>
 * </ul>
 * </p>
 * <p>Class diagram of Case Info module:
 * <br/><img src="doc-files/CaseManagement-Charge-classdiagram.JPG" />
 * </p>
 *
 * @author byu
 * @version 1.0 (based on SDD_Charge1.0)
 */
public interface Charge {

    /**
     * Create a single charge for a case
     *
     * @param uc     {@link UserContext}
     * @param charge {@link ChargeType}
     * @return the charge instance(s), the count.
     * throw exceptions in return type if the instances cannot be created.
     */
    public ChargeType createCharge(UserContext uc, ChargeType charge);

    /**
     * Create multiple charges for a case
     * this is for the ChargeType.chargeCount greater than one, if want to create a single charge for a case, please just call createCharge().
     *
     * @param uc     {@link UserContext}
     * @param charge {@link ChargeType}
     * @return the charge instance(s), the count.
     * throw exceptions in return type if the instances cannot be created.
     */
    public List<ChargeType> createCharges(UserContext uc, ChargeType charge);

    /**
     * Update charge information
     *
     * @param uc     {@link UserContext}
     * @param charge {@link ChargeType}
     * @return the charge instance.
     * throw exceptions if instance cannot be updated.
     */
    public ChargeType updateCharge(UserContext uc, ChargeType charge);

    /**
     * Search for charges
     *
     * @param uc            {@link UserContext}
     * @param subsetSearch  Set<Long> - a set of charge id, Optional, if null search will occur across all instances.
     * @param search        {@link ChargeSearchType} - search criteria instance.
     * @param searchHistory Boolean - optional, if true, the complete history of charge records are searched (current and historic records), otherwise only the latest charge records are searched
     * @return the charge instances,
     * throw exceptions if there is an error.
     * return empty list when no instances could be found.
     */
    @Deprecated
    public List<ChargeType> searchCharge(UserContext uc, Set<Long> subsetSearch, ChargeSearchType search, Boolean searchHistory);

    /**
     * Paging search for charges
     *
     * @param uc            {@link UserContext}
     * @param search        {@link ChargeSearchType} - search criteria instance.
     * @param searchHistory Boolean - optional, if true, the complete history of charge records are searched (current and historic records), otherwise only the latest charge records are searched
     * @param startIndex    Long – Optional if null search returns at the start index
     * @param resultSize    Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder   String – Optional if null search returns a default order
     * @return the charge instances,
     * throw exceptions if there is an error.
     * return empty list when no instances could be found.
     */
    public ChargesReturnType searchCharge(UserContext uc, ChargeSearchType search, Boolean searchHistory, Long startIndex, Long resultSize, String resultOrder);

    /**
     * Retrieve a single charge
     *
     * @param uc       {@link UserContext}
     * @param chargeId Long - the charge unique identifier.
     * @return the charge instance.
     * throw exceptions if incorrect Id is provided as parameter or there is an error.
     */
    public ChargeType getCharge(UserContext uc, Long chargeId);

    /**
     * Get/Retrieve a list of charges by case info id
     *
     * @param uc         {@link UserContext}
     * @param caseInfoId case info id, required
     * @return List&lt;ChargeType> a list of charges. Throw exceptions if failure.
     */
    public List<ChargeType> getChargesByCaseInfo(UserContext uc, Long caseInfoId);

    /**
     * Get/Retrieve a list of charges for an OJ-Order by the OJ-Order id.
     *
     * @param uc        {@link UserContext}
     * @param OJOrderId OJ-Order id, required
     * @return List&lt;ChargeType> a list of charges. Throw exceptions if failure.
     */
    public List<ChargeType> getChargesByOJOrderId(UserContext uc, Long OJOrderId);

    /**
     * Retrieve all charges for a case
     *
     * @param uc         {@link UserContext}
     * @param caseInfoId Long - the case information unique identifier.
     * @param search     {@link ChargeSearchType} - search criteria instance.
     * @return the charge instances.
     * throw exceptions if there is an error.
     * return empty list when no instances could be found.
     */
    public List<ChargeType> retrieveCharges(UserContext uc, Long caseInfoId, ChargeSearchType search);

    /**
     * Retrieve all charges for a case
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long Required. Supervision Id
     * @return the charge instances.
     * throw exceptions if there is an error.
     * return empty list when no instances could be found.
     */
    public List<ChargeType> retrieveChargesBySupervisionId(@NotNull UserContext uc, @NotNull Long supervisionId);

    /**
     * Retrieve historical changes for charges based on a date range.
     *
     * @param uc              {@link UserContext}
     * @param chargeId        Long - the charge unique identifier.
     * @param historyDateFrom Date - history date from
     * @param historyDateTo   Date - history date to
     * @return the charge instances.
     * throw exceptions if there is an error.
     * return empty list when no instances could be found.
     */
    public List<ChargeType> retrieveChargeHistory(UserContext uc, Long chargeId, Date historyDateFrom, Date historyDateTo);

    /**
     * Retrieve statute charges based on offence date
     *
     * @param uc          {@link UserContext}
     * @param statuteCode String - the statute code (StatuteId), a code of Statute Set.
     * @param offenceDate Date - The statute(StatuteID) and statute charges valid for the ‘OffenceDate’ are returned
     * @return the statute charge configuration instances.
     * throw exceptions if there is an error.
     * return empty list when no instances could be found.
     */
    public List<StatuteChargeConfigType> retrieveStatuteCharges(UserContext uc, String statuteCode, Date offenceDate);

    /**
     * Retrieve statute charge code details for an offense.
     *
     * @param uc          {@link UserContext}
     * @param statuteCode String - the statute code (StatuteId), a code of Statute Set. (Required)
     * @param chargeCode  String - a code of Charge Code Set. (Required)
     * @param offenceDate Date - The statute(StatuteID) and statute charges valid for the ‘OffenceDate’ are returned (Required)
     * @return the statute charge configuration instance,.
     * throw exceptions if incorrect input parameter or there is an error.
     */
    public StatuteChargeConfigType getStatuteCharge(UserContext uc, String statuteCode, String chargeCode, Date offenceDate);

    /**
     * Delete a charge instance
     *
     * @param uc       {@link UserContext}
     * @param chargeId Long - the charge unique identifier.
     * @return Return Code - Long.
     * Return success code if the instance be deleted.
     * throw exceptions if the instance not be deleted.
     */
    public Long deleteCharge(UserContext uc, Long chargeId) throws DataExistException;

    /**
     * Update a charge Identifier from a charge
     * <p>List of possible return codes: 1, 2001, 2002, 1003</p>
     *
     * @param uc               {@link UserContext}
     * @param chargeIdentifier {@link ChargeIdentifierType}
     * @return the charge instance.
     * throw exceptions if instance cannot be updated.
     */
    public ChargeType updateChargeIdentifier(UserContext uc, ChargeIdentifierType chargeIdentifier);

    /**
     * Remove a charge Identifier from a charge
     * <p>List of possible return codes: 1, 2001, 2002, 1003</p>
     *
     * @param uc                 {@link UserContext}
     * @param chargeIdentifierId Long - the charge identifier unique id.
     * @return the charge instance.
     * throw exceptions if charge identifier cannot be deleted.
     */
    public ChargeType removeChargeIdentifier(UserContext uc, Long chargeIdentifierId);

    /**
     * Update the plea information on a charge
     *
     * @param uc         {@link UserContext}
     * @param chargeId   Long - the charge unique identifier.
     * @param chargePlea {@link ChargePleaType}
     * @return the charge instance.
     * throw exceptions if the instance cannot be updated.
     */
    public ChargeType updateChargePlea(UserContext uc, Long chargeId, ChargePleaType chargePlea);

    /**
     * Update the appeal information on a charge
     *
     * @param uc           {@link UserContext}
     * @param chargeId     Long - the charge unique identifier.
     * @param chargeAppeal {@link ChargeAppealType}
     * @return the charge instance.
     * throw exceptions if the instance cannot be updated.
     */
    public ChargeType updateChargeAppeal(UserContext uc, Long chargeId, ChargeAppealType chargeAppeal);

    /**
     * Create external charge code related data. This includes NCIC codes, UCR codes.
     *
     * @param uc                       {@link UserContext}
     * @param externalChargeCodeConfig {@link ExternalChargeCodeConfigType}
     * @return the external charge code configuration instance.
     * throw exceptions if instance cannot be created.
     */
    public ExternalChargeCodeConfigType createExternalChargeCode(UserContext uc, ExternalChargeCodeConfigType externalChargeCodeConfig);

    /**
     * Update external charge code related data. This includes NCIC codes, UCR codes.
     *
     * @param uc                       {@link UserContext}
     * @param externalChargeCodeConfig {@link ExternalChargeCodeConfigType}
     * @return the external charge code configuration instance.
     * throw exceptions if instance cannot be updated.
     */
    public ExternalChargeCodeConfigType updateExternalChargeCode(UserContext uc, ExternalChargeCodeConfigType externalChargeCodeConfig);

    /**
     * Delete an external charge code instance
     * <p>List of possible return codes: 1, 2001, 2002, 1003</p>
     *
     * @param uc              {@link UserContext}
     * @param extChargeCodeId Long - the external charge code unique identifier.
     * @return Return Code - Long.
     * Return success code if the instance be deleted.
     * throw exceptions if the instance not be deleted.
     */
    public Long deleteExternalChargeCode(UserContext uc, Long extChargeCodeId);

    /**
     * Search external charge code related data.
     *
     * @param uc     {@link UserContext}
     * @param search {@link ExternalChargeCodeConfigSearchType}
     * @return the external charge code configuration instances.
     * throw exceptions if there is an error.
     * return empty list when no instances could be found.
     */
    public List<ExternalChargeCodeConfigType> searchExternalChargeCode(UserContext uc, ExternalChargeCodeConfigSearchType search);

    /**
     * Create jurisdiction related data. This includes country, province/state and county information.
     *
     * @param uc                 {@link UserContext}
     * @param jurisdictionConfig {@link JurisdictionConfigType}
     * @return the jurisdiction configuration instance.
     * throw exceptions if instance cannot be created.
     */
    public JurisdictionConfigType createJurisdiction(UserContext uc, JurisdictionConfigType jurisdictionConfig);

    /**
     * Update jurisdiction related data.
     *
     * @param uc                 {@link UserContext}
     * @param jurisdictionConfig {@link JurisdictionConfigType}
     * @return the jurisdiction configuration instance.
     * throw exceptions if instance cannot be updated.
     */
    public JurisdictionConfigType updateJurisdiction(UserContext uc, JurisdictionConfigType jurisdictionConfig);

    /**
     * Delete a jurisdiction configuration instance
     * <p>List of possible return codes: 1</p>
     *
     * @param uc             {@link UserContext}
     * @param jurisdictionId Long - the jurisdiction configuration instance unique identifier.
     * @return Return Code - Long.
     * Return success code if the instance be deleted.
     * throw exceptions if the instance not be deleted.
     */
    public Long deleteJurisdiction(UserContext uc, Long jurisdictionId);

    /**
     * Search jurisdiction data.
     *
     * @param uc          {@link UserContext}
     * @param search      {@link JurisdictionConfigSearchType}
     * @param startIndex  Long – Optional if null search returns at the start index
     * @param resultSize  Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder String – Optional if null search returns a default order
     * @return the jurisdiction configuration instances.
     * throw exceptions if there is an error.
     * return empty list when no instances could be found.
     */
    public JurisdictionConfigsReturnType searchJurisdiction(UserContext uc, JurisdictionConfigSearchType search, Long startIndex, Long resultSize, String resultOrder);

    /**
     * Create statute related data.
     *
     * @param uc            {@link UserContext}
     * @param statuteConfig {@link StatuteConfigType}
     * @return the statute configuration instance.
     * throw exceptions if instance cannot be created.
     */
    public StatuteConfigType createStatute(UserContext uc, StatuteConfigType statuteConfig);

    /**
     * Update statute related data.
     *
     * @param uc            {@link UserContext}
     * @param statuteConfig {@link StatuteConfigType}
     * @return the statute configuration instance.
     * throw exceptions if instance cannot be updated.
     */
    public StatuteConfigType updateStatute(UserContext uc, StatuteConfigType statuteConfig);

    /**
     * Delete a statute configuration instance
     * <p>List of possible return codes: 1</p>
     *
     * @param uc        {@link UserContext}
     * @param statuteId Long - the statute configuration instance unique identifier.
     * @return Return Code - Long.
     * Return success code if the instance be deleted.
     * throw exceptions if the instance not be deleted.
     */
    public Long deleteStatute(UserContext uc, Long statuteId);

    /**
     * search statute related data.
     *
     * @param uc          {@link UserContext}
     * @param search      {@link StatuteConfigSearchType}
     * @param startIndex  Long – Optional if null search returns at the start index
     * @param resultSize  Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder String – Optional if null search returns a default order
     * @return the statute configuration instances.
     * throw exceptions if there is an error.
     * return empty list when no instances could be found.
     */
    public StatuteConfigsReturnType searchStatute(UserContext uc, StatuteConfigSearchType search, Long startIndex, Long resultSize, String resultOrder);

    /**
     * Create statute charge related data.
     *
     * @param uc                  {@link UserContext}
     * @param statuteChargeConfig {@link StatuteChargeConfigType}
     * @return the statute charge configuration instance.
     * throw exceptions if instance cannot be created.
     */
    public StatuteChargeConfigType createStatuteCharge(UserContext uc, StatuteChargeConfigType statuteChargeConfig);

    /**
     * Update statute charge related data.
     *
     * @param uc                  {@link UserContext}
     * @param statuteChargeConfig {@link StatuteChargeConfigType}
     * @return the statute charge configuration instance.
     * throw exceptions if instance cannot be updated.
     */
    public StatuteChargeConfigType updateStatuteCharge(UserContext uc, StatuteChargeConfigType statuteChargeConfig);

    /**
     * Get statute charge configuration
     *
     * @param uc              {@link UserContext}
     * @param statuteChargeId Long, Id of statute charge configuration
     * @return the statute charge configuration instance.
     * throw exceptions if instance cannot be got
     */
    public StatuteChargeConfigType getStatuteCharge(UserContext uc, Long statuteChargeId);

    /**
     * Delete a statute charge configuration instance
     * <p>List of possible return codes: 1</p>
     *
     * @param uc              {@link UserContext}
     * @param statuteChargeId Long - the statute charge configuration instance unique identifier.
     * @return Return Code - Long.
     * Return success code if the instance be deleted.
     * throw exceptions if the instance not be deleted.
     */
    public Long deleteStatuteCharge(UserContext uc, Long statuteChargeId);

    /**
     * Search statute charge related data.
     *
     * @param uc          {@link UserContext}
     * @param search      {@link StatuteChargeConfigSearchType}
     * @param startIndex  Long – Optional if null search returns at the start index
     * @param resultSize  Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder String – Optional if null search returns a default order
     * @return the statute charge configuration instances.
     * throw exceptions if there is an error.
     * return empty list when no instances could be found.
     */
    public StatuteChargeConfigsReturnType searchStatuteCharge(UserContext uc, StatuteChargeConfigSearchType search, Long startIndex, Long resultSize, String resultOrder);

    /**
     * Create/Update the offence date rule, i.e. whether the offence start date or the Offence end date is used to reference the correct statute charge code.
     * If true, the OffenceStartDate will be used to retrieve the relevant statute charge code;
     * if false the OffenceEndDate will be used. Default is true
     * <p>List of possible return codes: 1</p>
     *
     * @param uc                 {@link UserContext}
     * @param isOffenceStartDate boolean - If true, the OffenceStartDate will be used to retrieve the relevant statute charge code.
     * @return return code.
     * throw exceptions if there is an error.
     */
    public Long setOffenceDateConfig(UserContext uc, boolean isOffenceStartDate);

    /**
     * Delete the offence date configuration record
     * <p>List of possible return codes: 1, 2001, 2002, 1003</p>
     *
     * @param uc {@link UserContext}
     * @return Return Code - Long.
     * Return success code if the instance be deleted.
     * throw exceptions if the instance not be deleted.
     */
    public Long deleteOffenceDateConfig(UserContext uc);

    /**
     * Retrieve the Offence date configuration record.
     *
     * @param uc {@link UserContext}
     * @return If true, the OffenceStartDate will be used to retrieve the relevant statute charge code;
     * if false the OffenceEndDate will be used.
     * if null, the configuration is not set yet.
     */
    public Boolean getOffenceStartDateConfig(UserContext uc);

    /**
     * Get Charge Disposition Status Status by Disposition Outcome from Reference Code Links
     *
     * @param uc                 {@link UserContext}
     * @param dispositionOutcome CodeType, Required
     * @return CodeType Returns Disposition Status; Returns null is no corresponding Disposition Status found; Throws ArbutusRuntimeException in case of failure.
     */
    public CodeType getChargeDispositionStatus(UserContext uc, CodeType dispositionOutcome);

    /**
     * updateChargesStatus -- to update a list of selected active charges' status.
     * <pre>
     * 1) Event Outcome is applied to all selected active charges(a list of active charges IDs) in the case
     * 2) Charge status is derived from the applied charge outcome and is passed in by client
     * 3) The Charge outcome date is the recorded date. A display field.
     * <pre>
     *
     * @param uc                UserContext - Required
     * @param chargesStatusType ChargesStatusType - Required, contains charge status information to be applied to selected charges.
     * @throws exception if the updating encounters error.
     */
    public void updateChargesStatus(UserContext uc, ChargesStatusType chargesStatusType);

    /**
     * Retrieve a list of ChargeCodeDescriptionType objects by given statute Id and offense date.
     *
     * @param uc          - Required.
     * @param statuteId   - Required.
     * @param offenseDate - Required.
     * @return List<ChargeCodeDescriptionType>
     * @throws exception if the retrieving encounters error.
     */
    public List<ChargeCodeDescriptionType> retrieveChargeCodeDescriptions(UserContext uc, Long statuteId, Date offenseDate);

    /**
     * Retrieve a list of StatuteChargeConfigType objects by given ChargeCodeDescriptionType object.
     *
     * @param uc                        - Required.
     * @param chargeCodeDescriptionType - Required.
     * @return List<StatuteChargeConfigType>
     * @throws exception if the retrieving encounters error.
     */
    public List<StatuteChargeConfigType> retrieveStatuteChargeConfigsByCodeDescription(UserContext uc, ChargeCodeDescriptionType chargeCodeDescriptionType);

    /**
     * Update Charge Data Security Status / Flag
     *
     * @param uc        - Required.
     * @param secStatus - DataFlag.
     * @param Long      - personId. Person Id of person id,
     * @param comments  - Comments to the security flag.
     * @return Long 1/0 as True/False
     * @throws exception if the retrieving encounters error.
     */
    public Long updateChargeDataSecurityStatus(UserContext uc, Long chargeId, Long secStatus, String comments, Long personId, String parentEntityType, Long parentId,
            String memento, Boolean cascade);

    /**
     * get All External Charge Codes
     *
     * @param UserContext
     * @return
     */
    public List<ExternalChargeCodeConfigType> searchAllExternalChargeCode(UserContext uc);

    public StatuteConfigType getStatuteConfigId(UserContext uc, Long statuteId);
}
