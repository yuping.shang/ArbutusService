package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the charge appeal entity.
 *
 * @DbComment LEG_CHGAppeal 'The charge appeal data table'
 */
@Audited
@Entity
@Table(name = "LEG_CHGAppeal")
public class ChargeAppealEntity implements Serializable {

    private static final long serialVersionUID = 8051722321339568555L;

    /**
     * @DbComment ChargeAppealId 'Unique id of a charge appeal record of a charge instance'
     */
    @Id
    @Column(name = "ChargeAppealId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGAppeal_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGAppeal_Id", sequenceName = "SEQ_LEG_CHGAppeal_Id", allocationSize = 1)
    private Long chargeAppealId;

    /**
     * @DbComment AppealStatus 'A status of an appeal'
     */
    @MetaCode(set = MetaSet.APPEAL_STATUS)
    @Column(name = "AppealStatus", nullable = false, length = 64)
    private String appealStatus;

    /**
     * @DbComment AppealComment 'General comments on the outcome of the appeal.'
     */
    @Column(name = "AppealComment", nullable = true, length = 512)
    private String appealComment;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     *
     */
    public ChargeAppealEntity() {
    }

    /**
     * @param chargeAppealId
     * @param appealStatus
     * @param appealComment
     */
    public ChargeAppealEntity(Long chargeAppealId, String appealStatus, String appealComment) {
        this.chargeAppealId = chargeAppealId;
        this.appealStatus = appealStatus;
        this.appealComment = appealComment;
    }

    /**
     * @return the chargeAppealId
     */
    public Long getChargeAppealId() {
        return chargeAppealId;
    }

    /**
     * @param chargeAppealId the chargeAppealId to set
     */
    public void setChargeAppealId(Long chargeAppealId) {
        this.chargeAppealId = chargeAppealId;
    }

    /**
     * @return the appealStatus
     */
    public String getAppealStatus() {
        return appealStatus;
    }

    /**
     * @param appealStatus the appealStatus to set
     */
    public void setAppealStatus(String appealStatus) {
        this.appealStatus = appealStatus;
    }

    /**
     * @return the appealComment
     */
    public String getAppealComment() {
        return appealComment;
    }

    /**
     * @param appealComment the appealComment to set
     */
    public void setAppealComment(String appealComment) {
        this.appealComment = appealComment;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeAppealEntity [chargeAppealId=" + chargeAppealId + ", appealStatus=" + appealStatus + ", appealComment=" + appealComment + "]";
    }

}
