package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the charge plea entity.
 *
 * @DbComment LEG_CHGJurisConfig 'The jurisdiction configuration data table'
 */
@Audited
@Entity
@Table(name = "LEG_CHGJurisConfig")
public class JurisdictionConfigEntity implements Serializable {

    private static final long serialVersionUID = 6831826554207642117L;

    /**
     * @DbComment JurisdictionId 'The unique ID for each statute jurisdiction record.'
     */
    @Id
    @Column(name = "JurisdictionId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGJurisConfig_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGJurisConfig_Id", sequenceName = "SEQ_LEG_CHGJurisConfig_Id", allocationSize = 1)
    private Long jurisdictionId;

    /**
     * @DbComment Description 'Description of the jurisdiction'
     */
    @Column(name = "Description", nullable = false, length = 128)
    private String description;

    /**
     * @DbComment Country 'Country the jurisdiction applies to'
     */
    @MetaCode(set = MetaSet.STATUTE_COUNTRY)
    @Column(name = "Country", nullable = false, length = 64)
    private String country;

    /**
     * @DbComment StateProvince 'State/province the jurisdiction applies to'
     */
    @MetaCode(set = MetaSet.STATUTE_PROVINCE_STATE)
    @Column(name = "StateProvince", nullable = false, length = 64)
    private String stateProvince;

    /**
     * @DbComment City 'A name of a city or town.'
     */
    @Column(name = "City", nullable = true, length = 128)
    private String city;

    /**
     * @DbComment County 'County the jurisdiction applies to'
     */
    @MetaCode(set = MetaSet.STATUTE_COUNTY)
    @Column(name = "County", nullable = true, length = 64)
    private String county;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     *
     */
    public JurisdictionConfigEntity() {
    }

    /**
     * @param jurisdictionId
     * @param description
     * @param country
     * @param stateProvince
     * @param city
     * @param county
     */
    public JurisdictionConfigEntity(Long jurisdictionId, String description, String country, String stateProvince, String city, String county) {
        this.jurisdictionId = jurisdictionId;
        this.description = description;
        this.country = country;
        this.stateProvince = stateProvince;
        this.city = city;
        this.county = county;
    }

    /**
     * @return the jurisdictionId
     */
    public Long getJurisdictionId() {
        return jurisdictionId;
    }

    /**
     * @param jurisdictionId the jurisdictionId to set
     */
    public void setJurisdictionId(Long jurisdictionId) {
        this.jurisdictionId = jurisdictionId;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the stateProvince
     */
    public String getStateProvince() {
        return stateProvince;
    }

    /**
     * @param stateProvince the stateProvince to set
     */
    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the county
     */
    public String getCounty() {
        return county;
    }

    /**
     * @param county the county to set
     */
    public void setCounty(String county) {
        this.county = county;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "JurisdictionConfigEntity [jurisdictionId=" + jurisdictionId + ", description=" + description + ", country=" + country + ", stateProvince=" + stateProvince
                + ", city=" + city + ", county=" + county + "]";
    }

}
