package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.AuditJoinTable;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * WarrantDetainerHistoryEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdOrderHst 'Order History table of Order Sentence Module of Legal Service'
 * @since December 21, 2012
 */
//@Audited
@Entity
@DiscriminatorValue("WarrantHst")
public class WarrantDetainerHistoryEntity extends OrderHistoryEntity implements Serializable {

    private static final long serialVersionUID = -737931375114562731L;

    @ForeignKey(name = "Fk_LEG_OrdWrntHst1")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @BatchSize(size = 20)
    @JoinColumn(name = "agnBeNoti")
    @AuditJoinTable(name = "Leg_WarrHit_NotifHst_AUD", inverseJoinColumns = { @JoinColumn(name = "hstId") })
    private Set<NotificationHistoryEntity> agencyToBeNotified;

    @ForeignKey(name = "Fk_LEG_OrdWrntHst2")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @BatchSize(size = 20)
    @JoinColumn(name = "notiLog")
    @AuditJoinTable(name = "Leg_WarrHit_NotifLogHst_AUD", inverseJoinColumns = { @JoinColumn(name = "hstId") })
    private Set<NotificationLogHistoryEntity> notificationLog;

    /**
     * Constructor
     */
    public WarrantDetainerHistoryEntity() {
        super();
    }

    public WarrantDetainerHistoryEntity(Long historyId, Long orderId, String orderClassification, String orderType, String orderSubType, String orderCategory,
            String orderNumber, DispositionHistoryEntity orderDisposition, CommentEntity comments, Date orderIssuanceDate, Date orderReceivedDate, Date orderStartDate,
            Date orderExpirationDate, Set<CaseActivityInitOrderHstEntity> caseActivityInitiatedOrderAssociations,
            Set<OrderInitCaseActivityHistEntity> orderInitiatedCaseActivityAssociations, Boolean isHoldingOrder, Boolean isSchedulingNeeded, Boolean hasCharges,
            NotificationHistoryEntity issuingAgency, StampEntity stamp, Set<NotificationHistoryEntity> agencyToBeNotified,
            Set<NotificationLogHistoryEntity> notificationLog) {
        super(historyId, orderId, orderClassification, orderType, orderSubType, orderCategory, orderNumber, orderDisposition, comments, orderIssuanceDate,
                orderReceivedDate, orderStartDate, orderExpirationDate, caseActivityInitiatedOrderAssociations, orderInitiatedCaseActivityAssociations, isHoldingOrder,
                isSchedulingNeeded, hasCharges, issuingAgency, stamp);
        this.agencyToBeNotified = agencyToBeNotified;
        this.notificationLog = notificationLog;
    }

    /**
     * @return the agencyToBeNotified
     */
    public Set<NotificationHistoryEntity> getAgencyToBeNotified() {
        return agencyToBeNotified;
    }

    /**
     * @param agencyToBeNotified the agencyToBeNotified to set
     */
    public void setAgencyToBeNotified(Set<NotificationHistoryEntity> agencyToBeNotified) {
        this.agencyToBeNotified = agencyToBeNotified;
    }

    /**
     * @return the notificationLog
     */
    public Set<NotificationLogHistoryEntity> getNotificationLog() {
        return notificationLog;
    }

    /**
     * @param notificationLog the notificationLog to set
     */
    public void setNotificationLog(Set<NotificationLogHistoryEntity> notificationLog) {
        this.notificationLog = notificationLog;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((agencyToBeNotified == null) ? 0 : agencyToBeNotified.hashCode());
        result = prime * result + ((notificationLog == null) ? 0 : notificationLog.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        WarrantDetainerHistoryEntity other = (WarrantDetainerHistoryEntity) obj;
        if (agencyToBeNotified == null) {
            if (other.agencyToBeNotified != null) {
				return false;
			}
        } else if (!agencyToBeNotified.equals(other.agencyToBeNotified)) {
			return false;
		}
        if (notificationLog == null) {
            if (other.notificationLog != null) {
				return false;
			}
        } else if (!notificationLog.equals(other.notificationLog)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "WarrantDetainerHistoryEntity [agencyToBeNotified=" + agencyToBeNotified + ", notificationLog=" + notificationLog + ", toString()=" + super.toString()
                + "]";
    }

}
