package syscon.arbutus.product.services.legal.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

import syscon.arbutus.product.services.core.contract.dto.CodeType;

/**
 * The representation of the currency configuration type
 */
public class CurrencyConfigType implements Serializable {

    private static final long serialVersionUID = 6515176030399384440L;

    @NotNull
    private CodeType currency;

    /**
     * Default constructor
     */
    public CurrencyConfigType() {
    }

    /**
     * Constructor
     *
     * @param currency CodeType - a code of Currency Set
     */
    public CurrencyConfigType(@NotNull CodeType currency) {
        this.currency = currency;
    }

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * <p>currency is not null. </p>
     *
     * @return Boolean Return true if all required properties are set, false otherwise.
     */
    @Deprecated
    public Boolean isWellFormed() {

        if (currency == null || !currency.isWellFormed()) {
			return false;
		}

        return true;
    }

    /**
     * Verify if properties are set properly.
     * <p>Max length of currency is 64.</p>
     *
     * @return Boolean Return true if all required properties are set and all values are valid, false otherwise.
     */
    @Deprecated
    public Boolean isValid() {

        if (currency == null || !currency.isValid()) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the currency property.
     *
     * @return CodeType - a code of Currency Set
     */
    public CodeType getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     *
     * @param currency CodeType - a code of Currency Set
     */
    public void setCurrency(CodeType currency) {
        this.currency = currency;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CurrencyConfigType [currency=" + currency + "]";
    }

}
