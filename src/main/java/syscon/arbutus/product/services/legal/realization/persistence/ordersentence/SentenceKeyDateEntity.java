package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * SentenceKeyDateEntity for Supervision Key Dates
 *
 * @author yshang
 * @DbComment LEG_OrdSntKeyDate 'SentenceKeyDateEntity for Supervision Key Dates'
 * @since July 22, 2013
 */
@Audited
@Entity
@Table(name = "LEG_OrdSntKeyDate")
@SQLDelete(sql = "UPDATE LEG_OrdSntKeyDate SET flag = 4 WHERE supervisionId = ? and version = ?")
@Where(clause = "flag = 1")
public class SentenceKeyDateEntity implements Serializable {

    private static final long serialVersionUID = 2905783051068452730L;

    /**
     * @DbComment supervisionId 'Supervision Id'
     */
    @Id
    @Column(name = "supervisionId")
    private Long supervisionId;

    /**
     * @DbComment staffId 'Staff Id'
     */
    @Column(name = "staffId", nullable = true)
    private Long staffId;

    /**
     * @DbComment CommentUserId 'User ID who created/updated the comments'
     * @DbComment CommentDate 'The Date/Time when the comment was created'
     * @DbComment CommentText 'The comment text'
     */
    @Embedded
    private CommentEntity comments;

    /**
     * @DbComment reviewRequired 'Is Date Review Required'
     */
    @Column(name = "reviewRequired", nullable = true)
    private Boolean reviewRequired;

    @ForeignKey(name = "Fk_LEG_OrdSntKeyDate")
    @OneToMany(mappedBy = "sentenceKeyDate", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<KeyDateEntity> keyDates;

    /**
     * @DbComment supervisionSentenceId 'Supervision Sentence Id'
     */
    @Column(name = "supervisionSentenceId", nullable = true)
    private Long supervisionSentenceId;

    /**
     * @DbComment aggregateSentenceId 'Aggregate Sentence Id'
     */
    @Column(name = "aggregateSentenceId", nullable = true)
    private Long aggregateSentenceId;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'Version for Hibernate use'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * Constructor
     */
    public SentenceKeyDateEntity() {
        super();
    }

    /**
     * Constructor
     *
     * @param supervisionId
     * @param staffId
     * @param comments
     * @param reviewRequired
     * @param keyDates
     * @param stamp
     */
    public SentenceKeyDateEntity(Long supervisionId, Long staffId, CommentEntity comments, Boolean reviewRequired, Set<KeyDateEntity> keyDates, StampEntity stamp) {
        super();
        this.supervisionId = supervisionId;
        this.staffId = staffId;
        this.comments = comments;
        this.reviewRequired = reviewRequired;
        this.keyDates = keyDates;
        this.stamp = stamp;
    }

    /**
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * @return the staffId
     */
    public Long getStaffId() {
        return staffId;
    }

    /**
     * @param staffId the staffId to set
     */
    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    /**
     * @return the comments
     */
    public CommentEntity getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(CommentEntity comments) {
        this.comments = comments;
    }

    /**
     * @return the reviewRequired
     */
    public Boolean getReviewRequired() {
        return reviewRequired;
    }

    /**
     * @param reviewRequired the reviewRequired to set
     */
    public void setReviewRequired(Boolean reviewRequired) {
        this.reviewRequired = reviewRequired;
    }

    /**
     * @return the keyDates
     */
    public Set<KeyDateEntity> getKeyDates() {
        if (keyDates == null) {
			keyDates = new HashSet<KeyDateEntity>();
		}
        return keyDates;
    }

    /**
     * @param keyDates the keyDates to set
     */
    public void setKeyDates(Set<KeyDateEntity> keyDates) {
        this.keyDates = keyDates;
    }

    public void addKeyDate(KeyDateEntity keyDate) {
        keyDate.setSentenceKeyDate(this);
        getKeyDates().add(keyDate);
    }

    /**
     * @return the supervisionSentenceId
     */
    public Long getSupervisionSentenceId() {
        return supervisionSentenceId;
    }

    /**
     * @param supervisionSentenceId the supervisionSentenceId to set
     */
    public void setSupervisionSentenceId(Long supervisionSentenceId) {
        this.supervisionSentenceId = supervisionSentenceId;
    }

    /**
     * @return the aggregateSentenceId
     */
    public Long getAggregateSentenceId() {
        return aggregateSentenceId;
    }

    /**
     * @param aggregateSentenceId the aggregateSentenceId to set
     */
    public void setAggregateSentenceId(Long aggregateSentenceId) {
        this.aggregateSentenceId = aggregateSentenceId;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((keyDates == null) ? 0 : keyDates.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SentenceKeyDateEntity other = (SentenceKeyDateEntity) obj;
        if (keyDates == null) {
            if (other.keyDates != null) {
				return false;
			}
        } else if (!keyDates.equals(other.keyDates)) {
			return false;
		}
        if (supervisionId == null) {
            if (other.supervisionId != null) {
				return false;
			}
        } else if (!supervisionId.equals(other.supervisionId)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SentenceKeyDateEntity [supervisionId=" + supervisionId + ", staffId=" + staffId + ", comments=" + comments + ", reviewRequired=" + reviewRequired
                + ", keyDates=" + keyDates + ", supervisionSentenceId=" + supervisionSentenceId + ", aggregateSentenceId=" + aggregateSentenceId + "]";
    }

}
