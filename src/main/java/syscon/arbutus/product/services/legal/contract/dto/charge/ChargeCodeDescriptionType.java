package syscon.arbutus.product.services.legal.contract.dto.charge;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * The representation of the charge code description type
 * This is a value object class for web application to consume.
 *
 * @author LHan
 */
public class ChargeCodeDescriptionType implements Serializable {

    private static final long serialVersionUID = 8898551227935852487L;

    @NotNull
    private Long statuteId;
    @NotNull
    private String statuteCode;
    @NotNull
    private Date offenseDate;
    @NotNull
    private String chargeCode; //A code of ChargeCode set.
    @NotNull
    private String chargeDescription;

    /**
     * Constructor
     */
    public ChargeCodeDescriptionType() {
        super();
    }

    /**
     * Constructor
     *
     * @param statuteId         - Required. The associated statute id.
     * @param chargeCode        - Required. A code of ChargeCode set.
     * @param chargeDescription - Required. The associated description of a Charge code.
     */
    public ChargeCodeDescriptionType(Long statuteId, String statuteCode, String chargeCode, String chargeDescription) {
        super();
        this.statuteId = statuteId;
        this.statuteCode = statuteCode;
        this.chargeCode = chargeCode;
        this.chargeDescription = chargeDescription;
    }

    /**
     * Constructor
     *
     * @param statuteId         - Required. The associated statute id.
     * @param offenseDate       - Required. The offense date for an offender.
     * @param chargeCode        - Required. A code of ChargeCode set.
     * @param chargeDescription - Required. The associated description of a Charge code.
     */
    public ChargeCodeDescriptionType(Long statuteId, String statuteCode, Date offenseDate, String chargeCode, String chargeDescription) {
        super();
        this.statuteId = statuteId;
        this.statuteCode = statuteCode;
        this.offenseDate = offenseDate;
        this.chargeCode = chargeCode;
        this.chargeDescription = chargeDescription;
    }

    /**
     * Required. The associated statute id.
     *
     * @return the statuteId
     */
    public Long getStatuteId() {
        return statuteId;
    }

    /**
     * Required. The associated statute id.
     *
     * @param statuteId the statuteId to set
     */
    public void setStatuteId(Long statuteId) {
        this.statuteId = statuteId;
    }

    /**
     * Required. The offense date for an offender.
     *
     * @return the offenseDate
     */
    public Date getOffenseDate() {
        return offenseDate;
    }

    /**
     * Required. The offense date for an offender.
     *
     * @param offenseDate the offenseDate to set
     */
    public void setOffenseDate(Date offenseDate) {
        this.offenseDate = offenseDate;
    }

    /**
     * Required. A code of ChargeCode set.
     *
     * @return the chargeCode
     */
    public String getChargeCode() {
        return chargeCode;
    }

    /**
     * Required. A code of ChargeCode set.
     *
     * @param chargeCode the chargeCode to set
     */
    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    /**
     * Required. The associated description of a Charge code.
     *
     * @return the chargeDescription
     */
    public String getChargeDescription() {
        return chargeDescription;
    }

    /**
     * Required. The associated description of a Charge code.
     *
     * @param chargeDescription the chargeDescription to set
     */
    public void setChargeDescription(String chargeDescription) {
        this.chargeDescription = chargeDescription;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((chargeCode == null) ? 0 : chargeCode.hashCode());
        result = prime * result + ((chargeDescription == null) ? 0 : chargeDescription.hashCode());
        result = prime * result + ((offenseDate == null) ? 0 : offenseDate.hashCode());
        result = prime * result + ((statuteId == null) ? 0 : statuteId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ChargeCodeDescriptionType other = (ChargeCodeDescriptionType) obj;
        if (chargeCode == null) {
            if (other.chargeCode != null) {
				return false;
			}
        } else if (!chargeCode.equals(other.chargeCode)) {
			return false;
		}
        if (chargeDescription == null) {
            if (other.chargeDescription != null) {
				return false;
			}
        } else if (!chargeDescription.equals(other.chargeDescription)) {
			return false;
		}
        if (offenseDate == null) {
            if (other.offenseDate != null) {
				return false;
			}
        } else if (!offenseDate.equals(other.offenseDate)) {
			return false;
		}
        if (statuteId == null) {
            if (other.statuteId != null) {
				return false;
			}
        } else if (!statuteId.equals(other.statuteId)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeCodeDescriptionType [statuteId=" + statuteId + ", offenseDate=" + offenseDate + ", chargeCode=" + chargeCode + ", chargeDescription="
                + chargeDescription + "]";
    }

    public String getStatuteCode() {
        return statuteCode;
    }

    public void setStatuteCode(String statuteCode) {
        this.statuteCode = statuteCode;
    }
}
