package syscon.arbutus.product.services.legal.contract.ejb;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import syscon.arbutus.product.services.legal.contract.dto.LegalModule;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierConfigType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseStatusChangeType;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.CaseActivityEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.*;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;

/**
 * This helper is provided for utility functions to support case info business functions.
 *
 * @author byu
 */
public class CaseInfoHelper {

    public static CaseInfoEntity toCaseInfoEntity(CaseInfoType dto) {

        if (dto == null) {
			return null;
		}

        CaseInfoEntity ret = new CaseInfoEntity(dto.getCaseInfoId(), dto.getCaseCategory(), dto.getCaseType(), dto.getCreatedDate(), dto.getIssuedDate(),
                dto.getFacilityId(), dto.getSupervisionId(), dto.isDiverted(), dto.isDivertedSuccess(), dto.isActive(), dto.getStatusChangeReason(),
                dto.getSentenceStatus(), LegalHelper.toCommentEntity(dto.getComment()), dto.isSaveAsSecondaryIdentifier());

        //build caseIdentifier with caseInfo relationship
        for (CaseIdentifierType identifier : dto.getCaseIdentifiers()) {
            CaseIdentifierEntity caseIdentifierEntity = toCaseIdentifierEntity(identifier);
            ret.addCaseIdentifier(caseIdentifierEntity);
        }

        //TODO:	set case activity relation.

        //build module association entity with caseInfo relationship
        ret.setModuleAssociations(toCaseInfoModuleAssociationEntitySet(dto));

        return ret;
    }

    public static CaseInfoType toCaseInfoType(CaseInfoEntity entity) {

        if (entity == null) {
			return null;
		}

        CaseInfoType ret = new CaseInfoType(entity.getCaseInfoId(), toCaseIdentifierTypeSet(entity.getCaseIdentifiers()), entity.getCaseCategory(), entity.getCaseType(),
                entity.getCreatedDate(), entity.getIssuedDate(), entity.getFacilityId(), entity.getSupervisionId(), entity.isDiverted(), entity.isDivertedSuccess(),
                entity.isActive(), entity.getCaseStatusReason(), entity.getSentenceStatus(), LegalHelper.toCommentType(entity.getComment()),
                toInstanceIds(LegalModule.CHARGE.value(), entity.getModuleAssociations()), toInstanceIds(LegalModule.CONDITION.value(), entity.getModuleAssociations()),
                toInstanceIds(LegalModule.ORDER_SENTENCE.value(), entity.getModuleAssociations()),
                toInstanceIds(LegalModule.CASE_INFORMATION.value(), entity.getModuleAssociations()), getCaseActivityIds(entity.getCaseActivities()),
                entity.isSaveAsSecondaryIdentifier());

        return ret;

    }

    /**
     * @param entities
     * @return
     */
    public static Set<Long> getCaseActivityIds(Set<CaseActivityEntity> entities) {
        if (entities == null) {
            return null;
        }

        Set<Long> ids = new HashSet<Long>();
        for (CaseActivityEntity entity : entities) {
            ids.add(entity.getCaseActivityId());
        }

        return ids;
    }

    /**
     * @param entities Set<CourtActivityEntity>
     * @return a list of objects of CaseInfoType
     */
    public static List<CaseInfoType> toCaseInfoTypeList(Set<CaseInfoEntity> entities) {
        List<CaseInfoType> rtn = new ArrayList<CaseInfoType>();

        if (entities != null) {
            for (CaseInfoEntity currentEntity : entities) {
                CaseInfoType current = toCaseInfoType(currentEntity);
                rtn.add(current);
            }
        }

        return rtn;
    }

    public static Set<CaseIdentifierEntity> toCaseIdentifierEntitySet(Set<CaseIdentifierType> dtos) {

        if (dtos == null) {
			return null;
		}

        Set<CaseIdentifierEntity> ret = new HashSet<CaseIdentifierEntity>();

        for (CaseIdentifierType dto : dtos) {

            if (dto != null) {
				ret.add(toCaseIdentifierEntity(dto));
			}
        }

        return ret;

    }

    public static Set<CaseIdentifierHistEntity> toCaseIdentifierHistEntitySet(Set<CaseIdentifierEntity> dtos, StampEntity stamp, CaseInfoHistEntity caseInfoHist) {

        if (dtos == null) {
			return null;
		}

        Set<CaseIdentifierHistEntity> ret = new HashSet<CaseIdentifierHistEntity>();

        for (CaseIdentifierEntity dto : dtos) {

            if (dto != null) {
				ret.add(toCaseIdentifierHistEntity(dto, stamp, caseInfoHist));
			}
        }

        return ret;

    }

    public static CaseIdentifierHistEntity toCaseIdentifierHistEntity(CaseIdentifierEntity dto, StampEntity stamp, CaseInfoHistEntity caseInfoHist) {

        CaseIdentifierHistEntity identifierHistory = new CaseIdentifierHistEntity(null, dto.getIdentifierId(), dto.getIdentifierType(), dto.getIdentifierValue(),
                dto.isAutoGenerate(), dto.getOrganizationId(), dto.getFacilityId(), dto.getIdentifierComment(), caseInfoHist, stamp, dto.isPrimary());

        return identifierHistory;
    }

    public static CaseIdentifierEntity toCaseIdentifierEntity(CaseIdentifierType dto) {

        if (dto == null) {
			return null;
		}

        CaseIdentifierEntity ret = new CaseIdentifierEntity(dto.getIdentifierId(), dto.getIdentifierType(), dto.getIdentifierValue(), dto.isAutoGeneration(),
                dto.getOrganizationId(), dto.getFacilityId(), dto.getIdentifierComment(), dto.isPrimary());

        return ret;

    }

    public static List<CaseIdentifierType> toCaseIdentifierTypeSet(Set<CaseIdentifierEntity> entities) {

        if (entities == null) {
			return null;
		}

        List<CaseIdentifierType> ret = new ArrayList<CaseIdentifierType>();

        for (CaseIdentifierEntity entity : entities) {
            if (entity != null) {
				ret.add(toCaseIdentifierType(entity));
			}
        }

        return ret;
    }

    private static List<CaseIdentifierType> toCaseIdentifierTypeSet2(Set<CaseIdentifierHistEntity> entities) {

        if (entities == null) {
			return null;
		}

        List<CaseIdentifierType> ret = new ArrayList<CaseIdentifierType>();

        for (CaseIdentifierHistEntity entity : entities) {
            if (entity != null) {
				ret.add(toCaseIdentifierType(entity));
			}
        }

        return ret;
    }

    public static List<CaseIdentifierConfigType> toCaseIdentifierConfigTypeSet(List<CaseIdentifierConfigEntity> entities) {

        if (entities == null) {
			return null;
		}

        List<CaseIdentifierConfigType> ret = new ArrayList<CaseIdentifierConfigType>();

        for (CaseIdentifierConfigEntity entity : entities) {
            if (entity != null) {
				ret.add(toCaseIdentifierConfigType(entity));
			}
        }

        return ret;
    }

    public static CaseIdentifierType toCaseIdentifierType(CaseIdentifierEntity entity) {

        if (entity == null) {
			return null;
		}

        CaseIdentifierType ret = new CaseIdentifierType(entity.getIdentifierId(), entity.getIdentifierType(), entity.getIdentifierValue(), entity.isAutoGenerate(),
                entity.getOrganizationId(), entity.getFacilityId(), entity.getIdentifierComment(), entity.isPrimary());

        return ret;

    }

    public static CaseIdentifierType toCaseIdentifierType(CaseIdentifierHistEntity entity) {

        if (entity == null) {
			return null;
		}

        CaseIdentifierType ret = new CaseIdentifierType(entity.getIdentifierId(), entity.getIdentifierType(), entity.getIdentifierValue(), entity.isAutoGenerate(),
                entity.getOrganizationId(), entity.getFacilityId(), entity.getIdentifierComment(), entity.isPrimary());

        return ret;

    }

    public static CaseIdentifierConfigEntity toCaseIdentifierConfigEntity(CaseIdentifierConfigType dto) {

        if (dto == null) {
			return null;
		}

        CaseIdentifierConfigEntity ret = new CaseIdentifierConfigEntity(dto.getIdentifierConfigId(), dto.getIdentifierType(), dto.getIdentifierFormat(),
                dto.isAutoGeneration(), dto.isActive(), dto.isPrimary(), dto.isDuplicateCheck());

        return ret;

    }

    public static CaseIdentifierConfigType toCaseIdentifierConfigType(CaseIdentifierConfigEntity entity) {

        if (entity == null) {
			return null;
		}

        CaseIdentifierConfigType ret = new CaseIdentifierConfigType(entity.getIdentifierConfigId(), entity.getIdentifierType(), entity.getIdentifierFormat(),
                entity.isAutoGenerate(), entity.isActive(), entity.isPrimary(), entity.isDuplicateCheck());

        return ret;

    }

    public static Set<CaseInfoModuleAssociationEntity> toCaseInfoModuleAssociationEntitySet(CaseInfoType dto) {

        if (dto == null) {
			return null;
		}

        Set<CaseInfoModuleAssociationEntity> ret = new HashSet<CaseInfoModuleAssociationEntity>();

        Long instanceId = dto.getCaseInfoId();

        //Add Charge related
        Set<CaseInfoModuleAssociationEntity> chargeAssociations = toCaseInfoModuleAssociationEntitySet(instanceId, LegalModule.CHARGE.value(), dto.getChargeIds());
        ret.addAll(chargeAssociations);

        //Add Condition related
        Set<CaseInfoModuleAssociationEntity> conditionAssociations = toCaseInfoModuleAssociationEntitySet(instanceId, LegalModule.CONDITION.value(),
                dto.getConditionIds());
        ret.addAll(conditionAssociations);

        //Add Order Sentence related
        Set<CaseInfoModuleAssociationEntity> orderSentenceAssociations = toCaseInfoModuleAssociationEntitySet(instanceId, LegalModule.ORDER_SENTENCE.value(),
                dto.getOrderSentenceIds());
        ret.addAll(orderSentenceAssociations);

        //Add Case Info related
        Set<CaseInfoModuleAssociationEntity> caseInfoAssociations = toCaseInfoModuleAssociationEntitySet(instanceId, LegalModule.CASE_INFORMATION.value(),
                dto.getCaseInfoIds());
        ret.addAll(caseInfoAssociations);

        return ret;

    }

    public static Set<CaseInfoModuleAssociationEntity> toCaseInfoModuleAssociationEntitySet(Long fromIdentifier, String toClass, Set<Long> ids) {

        if (ids == null || toClass == null) {
			return null;
		}

        Set<CaseInfoModuleAssociationEntity> ret = new HashSet<CaseInfoModuleAssociationEntity>();

        for (Long toIdentifier : ids) {
            if (toIdentifier != null) {
                CaseInfoModuleAssociationEntity entity = toCaseInfoModuleAssociationEntity(null, fromIdentifier, toClass, toIdentifier);
                ret.add(entity);
            }
        }

        return ret;

    }

    public static CaseInfoModuleAssociationEntity toCaseInfoModuleAssociationEntity(Long associationId, Long fromIdentifier, String toClass, Long toIdentifier) {

        if (toClass == null || toIdentifier == null) {
			return null;
		}

        CaseInfoModuleAssociationEntity ret = new CaseInfoModuleAssociationEntity();
        ret.setAssociationId(associationId);
        ret.setToClass(toClass);
        ret.setToIdentifier(toIdentifier);

        return ret;

    }

    public static Set<Long> toInstanceIds(String toClass, Set<CaseInfoModuleAssociationEntity> entities) {

        if (entities == null || toClass == null) {
			return null;
		}

        Set<Long> ret = new HashSet<Long>();

        for (CaseInfoModuleAssociationEntity entity : entities) {

            if (entity != null) {
                if (entity.getToClass().equalsIgnoreCase(toClass)) {
                    ret.add(entity.getToIdentifier());
                }
            }
        }

        return ret;
    }

    private static Set<Long> toInstanceIds2(String toClass, Set<CaseInfoModuleAssociationHistEntity> entities) {

        if (entities == null || toClass == null) {
			return null;
		}

        Set<Long> ret = new HashSet<Long>();

        for (CaseInfoModuleAssociationHistEntity entity : entities) {

            if (entity != null) {
                if (entity.getToClass().equalsIgnoreCase(toClass)) {
                    ret.add(entity.getToIdentifier());
                }
            }
        }

        return ret;
    }

    public static CaseInfoHistEntity toCaseInfoHistEntity(CaseInfoEntity existEntity, StampEntity createdStamp) {

        if (existEntity == null) {
			return null;
		}

        CaseInfoHistEntity rtn = new CaseInfoHistEntity();

        rtn.setCaseInfoId(existEntity.getCaseInfoId());
        rtn.setCaseCategory(existEntity.getCaseCategory());
        rtn.setCaseType(existEntity.getCaseType());
        rtn.setCreatedDate(existEntity.getCreatedDate());
        rtn.setIssuedDate(existEntity.getIssuedDate());
        rtn.setFacilityId(existEntity.getFacilityId());
        rtn.setSupervisionId(existEntity.getSupervisionId());
        rtn.setDiverted(existEntity.isDiverted());
        rtn.setDivertedSuccess(existEntity.isDivertedSuccess());
        rtn.setActive(existEntity.isActive());
        rtn.setCaseStatusReason(existEntity.getCaseStatusReason());
        rtn.setSentenceStatus(existEntity.getSentenceStatus());
        rtn.setComment(existEntity.getComment());
        rtn.setStamp(existEntity.getStamp());
        rtn.setCaseInfo(existEntity);
        rtn.setCaseIdentifiers(toCaseIdentifierHistEntitySet(existEntity.getCaseIdentifiers(), createdStamp, rtn));
        //TODO rtn.setAssociations(toCaseInfoAssociationHistEntitySet(existEntity.getAssociations(),createdStamp, rtn));
        rtn.setModuleAssociations(toCaseInfoModuleAssociationHistEntitySet(existEntity.getModuleAssociations(), createdStamp, rtn));
        return rtn;
    }

    public static Set<CaseInfoModuleAssociationHistEntity> toCaseInfoModuleAssociationHistEntitySet(Set<CaseInfoModuleAssociationEntity> associations,
            StampEntity createdStamp, CaseInfoHistEntity caseInfoHist) {
        if (associations == null) {
			return null;
		}

        Set<CaseInfoModuleAssociationHistEntity> ret = new HashSet<CaseInfoModuleAssociationHistEntity>();

        for (CaseInfoModuleAssociationEntity asso : associations) {

            if (asso != null) {
				ret.add(toCaseInfoModuleAssociationHistEntity(asso, createdStamp, caseInfoHist));
			}
        }

        return ret;
    }

    public static CaseInfoModuleAssociationHistEntity toCaseInfoModuleAssociationHistEntity(CaseInfoModuleAssociationEntity asso, StampEntity createdStamp,
            CaseInfoHistEntity caseInfoHist) {

        CaseInfoModuleAssociationHistEntity assoHistory = new CaseInfoModuleAssociationHistEntity(null, asso.getCaseInfo().getCaseInfoId(), asso.getToClass(),
                asso.getToIdentifier(), createdStamp, caseInfoHist);

        return assoHistory;
    }

    public static CaseIdentifierConfigEntity updateExistCaseIdentiferConfigEntity(CaseIdentifierConfigEntity existEntity, CaseIdentifierConfigEntity newEntity) {

        existEntity.setActive(newEntity.isActive());
        existEntity.setAutoGenerate(newEntity.isAutoGenerate());
        existEntity.setDuplicateCheck(newEntity.isDuplicateCheck());
        existEntity.setIdentifierFormat(newEntity.getIdentifierFormat());
        existEntity.setIdentifierType(newEntity.getIdentifierType());
        existEntity.setPrimary(newEntity.isPrimary());

        return existEntity;
    }

    public static CaseInfoEntity updateExistCaseInfoEntity(CaseInfoEntity existEntity, CaseInfoEntity newEntity) {

        //Elements which are single object.
        existEntity.setActive(newEntity.isActive());
        existEntity.setCaseCategory(newEntity.getCaseCategory());
        existEntity.setCaseInfoId(newEntity.getCaseInfoId());
        existEntity.setCaseStatusReason(newEntity.getCaseStatusReason());
        existEntity.setCaseType(newEntity.getCaseType());
        existEntity.setComment(newEntity.getComment());
        existEntity.setDiverted(newEntity.isDiverted());
        existEntity.setDivertedSuccess(newEntity.isDivertedSuccess());
        existEntity.setFacilityId(newEntity.getFacilityId());
        existEntity.setSupervisionId(newEntity.getSupervisionId());
        existEntity.setIssuedDate(newEntity.getIssuedDate());
        existEntity.setSentenceStatus(newEntity.getSentenceStatus());

        //Element which are a set of objects, like Associations and CaseIdentifiers.
        //TODO:
        /*existEntity.getAssociations().clear();
        for(CaseInfoAssociationEntity assoc : newEntity.getAssociations()){
			existEntity.addAssociation(assoc);
		}*/
        existEntity.getCaseIdentifiers().clear();
        for (CaseIdentifierEntity identifier : newEntity.getCaseIdentifiers()) {
            existEntity.addCaseIdentifier(identifier);
        }
        existEntity.getModuleAssociations().clear();
        for (CaseInfoModuleAssociationEntity assoc : newEntity.getModuleAssociations()) {
            existEntity.addModuleAssociation(assoc);
        }

        return existEntity;
    }

    public static List<CaseInfoType> toCaseInfoTypeSet(Set<CaseInfoHistEntity> histories, CaseInfoEntity currentEntity) {
        List<CaseInfoType> rtn = new ArrayList<CaseInfoType>();

        if (currentEntity != null) {
            CaseInfoType current = toCaseInfoType(currentEntity);
            rtn.add(current);
        }

        if (histories != null) {
			for (CaseInfoHistEntity historyEntity : histories) {
                CaseInfoType historyType = toCaseInfoType(historyEntity);
                rtn.add(historyType);
            }
		}

        return rtn;
    }

    private static CaseInfoType toCaseInfoType(CaseInfoHistEntity entity) {
        if (entity == null) {
			return null;
		}

        CaseInfoType ret = new CaseInfoType();
        ret.setCaseInfoId(entity.getCaseInfoId());
        ret.setCaseIdentifiers(toCaseIdentifierTypeSet2(entity.getCaseIdentifiers()));
        ret.setCaseCategory(entity.getCaseCategory());
        ret.setCaseType(entity.getCaseType());
        ret.setCreatedDate(entity.getCreatedDate());
        ret.setIssuedDate(entity.getIssuedDate());
        ret.setFacilityId(entity.getFacilityId());
        ret.setSupervisionId(entity.getSupervisionId());
        ret.setDiverted(entity.getDiverted());
        ret.setDivertedSuccess(entity.getDivertedSuccess());
        ret.setActive(entity.getActive());
        ret.setStatusChangeReason(entity.getCaseStatusReason());
        ret.setSentenceStatus(entity.getSentenceStatus());
        ret.setComment(LegalHelper.toCommentType(entity.getComment()));
        ret.setChargeIds(toInstanceIds2(LegalModule.CHARGE.value(), entity.getModuleAssociations()));
        ret.setConditionIds(toInstanceIds2(LegalModule.CONDITION.value(), entity.getModuleAssociations()));
        ret.setOrderSentenceIds(toInstanceIds2(LegalModule.ORDER_SENTENCE.value(), entity.getModuleAssociations()));
        ret.setCaseInfoIds(toInstanceIds2(LegalModule.CASE_INFORMATION.value(), entity.getModuleAssociations()));
        //TODO: ret.setAssociations(toAssociationTypeSet(entity.getAssociations()));

        return ret;
    }

    public static List<CaseStatusChangeType> toCaseStatusChangeTypeList(List<CaseStatusChangeHistEntity> list) {
        List<CaseStatusChangeType> rtn = new ArrayList<CaseStatusChangeType>();

        if (BeanHelper.isEmpty(list)) {
            return rtn;
        }

        for (CaseStatusChangeHistEntity entity : list) {
            CaseStatusChangeType type = new CaseStatusChangeType();
            type.setCaseStatus(entity.getCaseStatus());
            type.setStatusChangeReason(entity.getCaseStatusChangeReason());
            type.setStatusChangeDate(entity.getCaseStatusChangeDate());

            rtn.add(type);
        }
        return rtn;
    }

}
