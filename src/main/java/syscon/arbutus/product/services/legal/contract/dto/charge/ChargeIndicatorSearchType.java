package syscon.arbutus.product.services.legal.contract.dto.charge;

import java.io.Serializable;

import syscon.arbutus.product.services.legal.contract.dto.ValidationUtil;

/**
 * The representation of the charge indicator search type
 */
public class ChargeIndicatorSearchType implements Serializable {

    private static final long serialVersionUID = -1284403923974391355L;

    private String chargeIndicator;
    private Boolean hasIndicatorValue;
    private String indicatorValue;

    /**
     * Default constructor
     */
    public ChargeIndicatorSearchType() {
    }

    /**
     * Constructor
     * At least one of the search criteria must be specified.
     *
     * @param chargeIndicator   String - The indicators (alerts) on a charge
     * @param hasIndicatorValue Boolean - True if a value can be associated to an indicator, false otherwise.
     * @param indicatorValue    String - The value associated to the indicator
     */
    public ChargeIndicatorSearchType(String chargeIndicator, Boolean hasIndicatorValue, String indicatorValue) {
        this.chargeIndicator = chargeIndicator;
        this.hasIndicatorValue = hasIndicatorValue;
        this.indicatorValue = indicatorValue;
    }

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set, false otherwise.
     */
    @Deprecated
    public Boolean isWellFormed() {

        if (ValidationUtil.isEmpty(chargeIndicator) && ValidationUtil.isEmpty(hasIndicatorValue) && ValidationUtil.isEmpty(indicatorValue)) {
			return false;
		}

        return true;
    }

    /**
     * Verify if properties are set properly.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set and all values are valid, false otherwise.
     */
    @Deprecated
    public Boolean isValid() {

        if (!isWellFormed()) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the chargeIndicator property. It is a reference code ChargeIndicator of set.
     *
     * @return String - the indicators (alerts) on a charge
     */
    public String getChargeIndicator() {
        return chargeIndicator;
    }

    /**
     * Sets the value of the chargeIndicator property. It is a reference code ChargeIndicator of set.
     *
     * @param chargeIndicator String - the indicators (alerts) on a charge
     */
    public void setChargeIndicator(String chargeIndicator) {
        this.chargeIndicator = chargeIndicator;
    }

    /**
     * Gets the value of the hasIndicatorValue property.
     *
     * @return Boolean - true if a value can be associated to an indicator, false otherwise.
     */
    public Boolean hasIndicatorValue() {
        return hasIndicatorValue;
    }

    /**
     * Sets the value of the hasIndicatorValue property.
     *
     * @param hasIndicatorValue Boolean - true if a value can be associated to an indicator, false otherwise.
     */
    public void setHasIndicatorValue(Boolean hasIndicatorValue) {
        this.hasIndicatorValue = hasIndicatorValue;
    }

    /**
     * Gets the value of the indicatorValue property.
     *
     * @return String - the value associated to the indicator can be specified only if the HasIndicatorValue is ‘true’.
     */
    public String getIndicatorValue() {
        return indicatorValue;
    }

    /**
     * Sets the value of the indicatorValue property.
     *
     * @param indicatorValue String - the value associated to the indicator can be specified only if the HasIndicatorValue is ‘true’.
     */
    public void setIndicatorValue(String indicatorValue) {
        this.indicatorValue = indicatorValue;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeIndicatorSearchType [chargeIndicator=" + chargeIndicator + ", hasIndicatorValue=" + hasIndicatorValue + ", indicatorValue=" + indicatorValue + "]";
    }

}
