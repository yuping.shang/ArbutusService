package syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment;

import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

/**
 * SentenceAdjustmentSearchType for Sentence Adjustment
 * <p>At least one criteria must be provided for search</p>
 * User: yshang
 * Date: 07/10/13
 * Time: 5:10 PM
 */
@ArbutusConstraint(constraints = { "fromEndDate <= toEndDate", "fromEndDate <= toEndDate", "fromPostedDate <= toPostedDate", "fromAdjustment <= toAdjustment" })
public class SentenceAdjustmentSearchType implements Serializable {

    private static final long serialVersionUID = -6683514168082290987L;

    /**
     * adjustmentClassification: SYSG, MANUAL
     */
    private String adjustmentClassification;

    /**
     * adjustmentType:
     * GT, JT, LGT, WORKCDT, LWRKCRD, PRGCDT, ESCP, OICDBT, WKNCDT, STATCDT
     */
    private String adjustmentType;

    /**
     * adjustmentFunction: CRE, DEB
     */
    private String adjustmentFunction;

    /**
     * adjustmentStatus: INCL, EXCL, PEND
     */
    private String adjustmentStatus;

    private java.util.Date fromStartDate;
    private java.util.Date toStartDate;

    private Date fromEndDate;
    private Date toEndDate;

    private Date fromPostedDate;
    private Date toPostedDate;

    private Long fromAdjustment;
    private Long toAdjustment;

    private Long byStaffId;

    private Long sentenceId;

    /**
     * getAdjustmentClassification Get Adjustment Classification
     * <p>Reference code, SYSG, MANUAL, of Reference Set AdjustmentClassification
     * <p>Optional</p>
     *
     * @return String
     */
    public String getAdjustmentClassification() {
        return adjustmentClassification;
    }

    /**
     * setAdjustmentClassification Set Adjustment Classification
     * <p>Reference code, SYSG, MANUAL, of Reference Set AdjustmentClassification
     * <p>Optional</p>
     *
     * @param adjustmentClassification String
     */
    public void setAdjustmentClassification(String adjustmentClassification) {
        this.adjustmentClassification = adjustmentClassification;
    }

    /**
     * getAdjustmentType Get Adjustment Type
     * <p>Reference Code, GT, JT, LGT, WORKCDT, LWRKCRD, PRGCDT, ESCP, OICDBT, WKNCDT, STATCDT, of Reference Set AdjustmentType
     * <p>Required</p>
     *
     * @return String
     */
    public String getAdjustmentType() {
        return adjustmentType;
    }

    /**
     * setAdjustmentType Set Adjustment Type
     * <p>Reference Code, GT, JT, LGT, WORKCDT, LWRKCRD, PRGCDT, ESCP, OICDBT, WKNCDT, STATCDT, of Reference Set AdjustmentType
     * <p>Required</p>
     *
     * @param adjustmentType
     */
    public void setAdjustmentType(String adjustmentType) {
        this.adjustmentType = adjustmentType;
    }

    /**
     * getAdjustmentFunction Get Adjustment Function
     * <p>Reference Code, CRE, DEB, of Reference Set AdjustmentFunction
     * <p>Optional</p>
     *
     * @return String
     */
    public String getAdjustmentFunction() {
        return adjustmentFunction;
    }

    /**
     * setAdjustmentFunction Set Adjustment Function
     * <p>Reference Code, CRE, DEB, of Reference Set AdjustmentFunction
     * <p>Optional</p>
     *
     * @param adjustmentFunction
     */
    public void setAdjustmentFunction(String adjustmentFunction) {
        this.adjustmentFunction = adjustmentFunction;
    }

    /**
     * getAdjustmentStatus Get Adjustment Status
     * <p>Reference Code, INCL, EXCL, PEND, of Reference Set AdjustmentStatus
     * <p>Optional</p>
     *
     * @return String
     */
    public String getAdjustmentStatus() {
        return adjustmentStatus;
    }

    /**
     * setAdjustmentStatus Set Adjustment Status
     * <p>Reference Code, INCL, EXCL, PEND, of Reference Set AdjustmentStatus
     * <p>Optional</p>
     *
     * @param adjustmentStatus
     */
    public void setAdjustmentStatus(String adjustmentStatus) {
        this.adjustmentStatus = adjustmentStatus;
    }

    /**
     * getFromStartDate Get From Start Date
     * <p>From Start Date of the adjustment</p>
     * <p>Optional</p>
     *
     * @return Date
     */
    public Date getFromStartDate() {
        return fromStartDate;
    }

    /**
     * setFromStartDate Set From Start Date
     * <p>From Start Date of the adjustment</p>
     * <p>Optional</p>
     */
    public void setFromStartDate(Date fromStartDate) {
        this.fromStartDate = fromStartDate;
    }

    /**
     * getToStartDate Get To End Date
     * <p>To End Date of the adjustment</p>
     * <p>Optional</p>
     *
     * @return Date
     */
    public Date getToStartDate() {
        return toStartDate;
    }

    /**
     * setToStartDate To Set End Date
     * <p>To End Date of the adjustment</p>
     * <p>Optional</p>
     *
     * @param toStartDate
     */
    public void setToStartDate(Date toStartDate) {
        this.toStartDate = toStartDate;
    }

    /**
     * getFromEndDate Get From End Date
     *
     * @return
     */
    public Date getFromEndDate() {
        return fromEndDate;
    }

    /**
     * setFromEndDate Set From End Date
     *
     * @param fromEndDate
     */
    public void setFromEndDate(Date fromEndDate) {
        this.fromEndDate = fromEndDate;
    }

    /**
     * getToEndDate Get To End Date
     *
     * @return Date
     */
    public Date getToEndDate() {
        return toEndDate;
    }

    /**
     * setToEndDate Set To End Date
     *
     * @param toEndDate
     */
    public void setToEndDate(Date toEndDate) {
        this.toEndDate = toEndDate;
    }

    /**
     * getFromPostedDate Get From Posted Date
     *
     * @return
     */
    public Date getFromPostedDate() {
        return fromPostedDate;
    }

    /**
     * setFromPostedDate Set From Posted Date
     *
     * @param fromPostedDate
     */
    public void setFromPostedDate(Date fromPostedDate) {
        this.fromPostedDate = fromPostedDate;
    }

    /**
     * getToPostedDate Get To Posted Date
     *
     * @return
     */
    public Date getToPostedDate() {
        return toPostedDate;
    }

    /**
     * setToPostedDate Set To Posted Date
     *
     * @param toPostedDate
     */
    public void setToPostedDate(Date toPostedDate) {
        this.toPostedDate = toPostedDate;
    }

    /**
     * getFromAdjustment Get From Adjustment
     *
     * @return
     */
    public Long getFromAdjustment() {
        return fromAdjustment;
    }

    /**
     * setFromAdjustment Set From Adjustment
     *
     * @param fromAdjustment
     */
    public void setFromAdjustment(Long fromAdjustment) {
        this.fromAdjustment = fromAdjustment;
    }

    /**
     * getToAdjustment Get To Adjustment
     *
     * @return
     */
    public Long getToAdjustment() {
        return toAdjustment;
    }

    /**
     * setToAdjustment Set To Adjustment
     *
     * @param toAdjustment
     */
    public void setToAdjustment(Long toAdjustment) {
        this.toAdjustment = toAdjustment;
    }

    /**
     * getByStaffId Get By Staff Id
     *
     * @return
     */
    public Long getByStaffId() {
        return byStaffId;
    }

    /**
     * setByStaffId Set By Staff Id
     *
     * @param byStaffId
     */
    public void setByStaffId(Long byStaffId) {
        this.byStaffId = byStaffId;
    }

    /**
     * @return the sentenceId
     */
    public Long getSentenceId() {
        return sentenceId;
    }

    /**
     * @param sentenceId the sentenceId to set
     */
    public void setSentenceId(Long sentenceId) {
        this.sentenceId = sentenceId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SentenceAdjustmentSearchType [adjustmentClassification=" + adjustmentClassification + ", adjustmentType=" + adjustmentType + ", adjustmentFunction="
                + adjustmentFunction + ", adjustmentStatus=" + adjustmentStatus + ", fromStartDate=" + fromStartDate + ", toStartDate=" + toStartDate + ", fromEndDate="
                + fromEndDate + ", toEndDate=" + toEndDate + ", fromPostedDate=" + fromPostedDate + ", toPostedDate=" + toPostedDate + ", fromAdjustment="
                + fromAdjustment + ", toAdjustment=" + toAdjustment + ", byStaffId=" + byStaffId + ", sentenceId=" + sentenceId + "]";
    }

}
