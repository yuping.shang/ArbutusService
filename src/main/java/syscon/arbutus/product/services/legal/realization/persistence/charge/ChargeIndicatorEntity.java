package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the charge indicator entity.
 *
 * @DbComment LEG_CHGIndicator 'The charge indicator data table'
 */
@Audited
@Entity
@Table(name = "LEG_CHGIndicator")
public class ChargeIndicatorEntity implements Serializable {

    private static final long serialVersionUID = 8534908737121918627L;

    /**
     * @DbComment IndicatorId 'The unique Id for each charge indicator record.'
     */
    @Id
    @Column(name = "IndicatorId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGIndicator_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGIndicator_Id", sequenceName = "SEQ_LEG_CHGIndicator_Id", allocationSize = 1)
    private Long indicatorId;

    /**
     * @DbComment ChargeIndicator 'The indicators (alerts) on a charge.'
     */
    @MetaCode(set = MetaSet.CHARGE_INDICATOR)
    @Column(name = "ChargeIndicator", nullable = false, length = 64)
    private String chargeIndicator;

    /**
     * @DbComment HasIndicatorValue 'True if a value can be associated to an indicator, false otherwise, default is false'
     */
    @Column(name = "HasIndicatorValue", nullable = false)
    private Boolean hasIndicatorValue;

    /**
     * @DbComment IndicatorValue 'The value associated to the indicator can be specified only if the HasIndicatorValue is ‘true’. '
     */
    @Column(name = "IndicatorValue", nullable = true, length = 128)
    private String indicatorValue;

    /**
     * @DbComment ChargeId 'Unique id of a charge instance, foreign key to LEG_CRGCharge table'
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ChargeId", nullable = false)
    private ChargeEntity charge;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     *
     */
    public ChargeIndicatorEntity() {
    }

    /**
     * @param indicatorId
     * @param chargeIndicator
     * @param hasIndicatorValue
     * @param indicatorValue
     */
    public ChargeIndicatorEntity(Long indicatorId, String chargeIndicator, Boolean hasIndicatorValue, String indicatorValue) {
        this.indicatorId = indicatorId;
        this.chargeIndicator = chargeIndicator;
        this.hasIndicatorValue = hasIndicatorValue;
        this.indicatorValue = indicatorValue;
    }

    /**
     * @return the indicatorId
     */
    public Long getIndicatorId() {
        return indicatorId;
    }

    /**
     * @param indicatorId the indicatorId to set
     */
    public void setIndicatorId(Long indicatorId) {
        this.indicatorId = indicatorId;
    }

    /**
     * @return the chargeIndicator
     */
    public String getChargeIndicator() {
        return chargeIndicator;
    }

    /**
     * @param chargeIndicator the chargeIndicator to set
     */
    public void setChargeIndicator(String chargeIndicator) {
        this.chargeIndicator = chargeIndicator;
    }

    /**
     * @return the hasIndicatorValue
     */
    public Boolean getHasIndicatorValue() {
        return hasIndicatorValue;
    }

    /**
     * @param hasIndicatorValue the hasIndicatorValue to set
     */
    public void setHasIndicatorValue(Boolean hasIndicatorValue) {
        this.hasIndicatorValue = hasIndicatorValue;
    }

    /**
     * @return the indicatorValue
     */
    public String getIndicatorValue() {
        return indicatorValue;
    }

    /**
     * @param indicatorValue the indicatorValue to set
     */
    public void setIndicatorValue(String indicatorValue) {
        this.indicatorValue = indicatorValue;
    }

    /**
     * @return the charge
     */
    public ChargeEntity getCharge() {
        return charge;
    }

    /**
     * @param charge the charge to set
     */
    public void setCharge(ChargeEntity charge) {
        this.charge = charge;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeIndicatorEntity [indicatorId=" + indicatorId + ", chargeIndicator=" + chargeIndicator + ", hasIndicatorValue=" + hasIndicatorValue
                + ", indicatorValue=" + indicatorValue + "]";
    }

}
