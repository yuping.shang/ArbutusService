package syscon.arbutus.product.services.legal.realization.util;

import java.util.ArrayList;
import java.util.List;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.legal.contract.dto.*;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierSearchType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoSearchType;
import syscon.arbutus.product.services.legal.contract.dto.charge.ChargeType;
import syscon.arbutus.product.services.legal.realization.adapters.FacilityServiceAdapter;

/**
 * This helper is provided for utility functions to support case conversation business functions.
 *
 * @author lhan
 */
public class CaseHelper {

    /**
     * Convert syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType to
     * syscon.arbutus.product.services.legal.contract.dto.CaseInfoType.
     *
     * @param dto
     * @return
     */
    public static CaseInfoType toCaseInfoType(UserContext uc, syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType dto) {

        if (dto == null) {
			return null;
		}

        CaseInfoType ret = new CaseInfoType();

        ret.setCaseId(dto.getCaseInfoId());
        ret.setActiveFlag(dto.isActive());
        ret.setCaseCategory(dto.getCaseCategory());
        setCaseIdentifiers(ret, dto.getCaseIdentifiers());
        ret.setCaseType(dto.getCaseType());
        ret.setCreatedDate(dto.getCreatedDate());
        ret.setFacilityId(dto.getFacilityId());
        ret.setIssuedDate(dto.getIssuedDate());
        ret.setStatusChangeReason(dto.getStatusChangeReason());
        ret.setSentenceStatus(dto.getSentenceStatus());
        ret.setSupervisionId(dto.getSupervisionId());
        ret.setComment(dto.getComment());

        String issuingFacilityName = getIssuingFacilityName(uc, ret.getFacilityId());
        ret.setIssuingFacilityName(issuingFacilityName);

        return ret;
    }

    /**
     * Get issuing facility name for display only.
     *
     * @return
     */
    private static String getIssuingFacilityName(UserContext uc, Long facilityId) {

        Facility facility = new FacilityServiceAdapter().getFacility(uc, facilityId);

        if (facility != null) {
			return facility.getFacilityName();
		} else {
			return null;
		}
    }

    /**
     * Convert {@link syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType} to
     * {@link CaseType}
     *
     * @param dto
     * @param chareges
     * @return
     */
    public static CaseType toCaseType(UserContext uc, syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType dto, List<ChargeType> chareges) {

        if (dto == null) {
			return null;
		}

        CaseType ret = new CaseType(toCaseInfoType(uc, dto));

        //TODO: handle charges;

        return ret;
    }

    /**
     * Convert {@link CaseType} to
     * {@link syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType}
     *
     * @param dto
     * @return
     */
    public static syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType toCaseManagementCaseInfoType(CaseType dto) {

        if (dto == null) {
			return null;
		}

        syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType ret = new syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType(
                dto.getCaseId(), toCaseManagementCaseIdentifierList(dto.getPrimaryCaseIdentifier(), dto.getCaseIdentifiers()), dto.getCaseCategory(), dto.getCaseType(),
                dto.getCreatedDate(), dto.getIssuedDate(), dto.getFacilityId(), dto.getSupervisionId(), null,//dto.isDiverted(),
                null,//dto.isDivertedSuccess(),
                dto.isActiveFlag(), dto.getStatusChangeReason(), dto.getSentenceStatus(), dto.getComment(), null,
                //toInstanceIds(CaseMgmtModule.CHARGE.value(), dto.getModuleAssociations()),
                null,//toInstanceIds(CaseMgmtModule.CONDITION.value(), dto.getModuleAssociations()),
                null,//toInstanceIds(CaseMgmtModule.ORDER_SENTENCE.value(), dto.getModuleAssociations()),
                null,//toInstanceIds(CaseMgmtModule.CASE_INFORMATION.value(), dto.getModuleAssociations()),
                null,//AssociationHelper.toAssociationSet(dto.getAssociations(), applyPrivilege, userPrivileges),
                dto.isSaveAsSecondaryIdentifier());//toPrivilegeStringSet(dto.getPrivileges()));

        return ret;
    }

    /**
     * Convert List<{@link syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType}> to
     * List<{@link CaseIdentifierType}>
     *
     * @param caseIdentifiers
     * @return
     */
    public static void setCaseIdentifiers(CaseInfoType ret, List<syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType> caseIdentifiers) {

        List<CaseIdentifierType> rtn = null;

        if (!ValidationUtil.isEmpty(caseIdentifiers)) {
            rtn = new ArrayList<CaseIdentifierType>();

            for (syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType dto : caseIdentifiers) {
                if (dto != null) {
                    if (!dto.isPrimary()) {
						rtn.add(toCaseIdentifierType(dto));
					} else {
						ret.setPrimaryCaseIdentifier(toCaseIdentifierType(dto));
					}
                }
            }

            ret.setCaseIdentifiers(rtn);
        }
    }

    /**
     * Convert List<{@link syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType}> to
     * List<{@link CaseInfoType}>
     *
     * @param dtos
     * @return
     */
    public static List<CaseInfoType> toCaseInfoTypeList(UserContext uc, List<syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType> dtos) {
        List<CaseInfoType> rtn = null;

        if (!ValidationUtil.isEmpty(dtos)) {
            rtn = new ArrayList<CaseInfoType>();

            for (syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType dto : dtos) {
                if (dto != null) {
					rtn.add(toCaseInfoType(uc, dto));
				}
            }
        }

        return rtn;
    }

    /**
     * Convert List<{@link CaseIdentifierType}> to
     * List<{@link syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType}>
     *
     * @param entities
     * @return
     */
    public static List<syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType> toCaseManagementCaseIdentifierList(
            CaseIdentifierType primary, List<CaseIdentifierType> entities) {

        List<syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType> ret = new ArrayList<syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType>();

        if (primary != null) {
			ret.add(toCaseManagementCaseIdentifierType(primary, true));
		}

        if (!ValidationUtil.isEmpty(entities)) {
            for (CaseIdentifierType dto : entities) {
                if (dto != null) {
					ret.add(toCaseManagementCaseIdentifierType(dto, false));
				}
            }
        }

        return ret;
    }

    /**
     * Convert List<{@link syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType}>
     * to List<{@link CaseIdentifierType}>
     *
     * @param dto
     * @return
     */
    public static CaseIdentifierType toCaseIdentifierType(syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType dto) {

        if (dto == null) {
			return null;
		}

        CaseIdentifierType ret = new CaseIdentifierType();
        ret.setCaseIdentifierId(dto.getIdentifierId());
        ret.setIdentifierType(dto.getIdentifierType());
        ret.setIdentifierValue(dto.getIdentifierValue());
        ret.setSourceFacilityId(dto.getFacilityId());
        ret.setSourceOrganizationId(dto.getOrganizationId());

        return ret;
    }

    /**
     * Convert {@link CaseIdentifierType} to {@link syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType}
     *
     * @param dto
     * @return
     */
    public static syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType toCaseManagementCaseIdentifierType(CaseIdentifierType dto,
            boolean isPrimary) {

        if (dto == null) {
			return null;
		}

        syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType ret = new syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseIdentifierType(
                dto.getCaseIdentifierId(), dto.getIdentifierType(), dto.getIdentifierValue(), null,//dto.isAutoGenerate(),
                dto.getSourceOrganizationId(), dto.getSourceFacilityId(), null,//dto.getIdentifierComment()
                isPrimary);

        return ret;

    }

    /**
     * Convert {@link CaseSearchType} to {@link CaseInfoSearchType}
     *
     * @param searchType
     * @return
     */
    public static CaseInfoSearchType toCaseInfoSearchType(CaseSearchType searchType) {
        CaseInfoSearchType rtn = new CaseInfoSearchType();
        rtn.setFacilityId(searchType.getFacilityId());
        rtn.setSupervisionId(searchType.getSupervisionId());

        CaseIdentifierSearchType identifierSearch = new CaseIdentifierSearchType();
        identifierSearch.setIdentifierType(searchType.getCaseIdentifierType());
        identifierSearch.setIdentifierValue(searchType.getCaseIdentifierValue());

        rtn.setCaseIdentifierSearch(identifierSearch);

        return rtn;
    }

}
