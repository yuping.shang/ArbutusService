package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;

/**
 * SentenceEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdOrder 'Order table for Order Sentence module of Legal service'
 * @since December 21, 2012
 */
@Audited
@Entity
@DiscriminatorValue("Sentence")
public class SentenceEntity extends OrderEntity implements Serializable {

    private static final long serialVersionUID = -737931475118522735L;

    /**
     * @DbComment sentenceType 'The type of sentence (Definite, Split, Life etc.,)'
     */
    @Column(name = "sentenceType", nullable = true, length = 64) // business requirement is nullable = false
    @MetaCode(set = MetaSet.SENTENCE_TYPE)
    private String sentenceType;

    /**
     * @DbComment sentenceNumber 'Identifier for the sentence. Will be used  on the UI to specify the sentence relationship (concecutive)'
     */
    @Column(name = "sentenceNumber", nullable = true) // business requirement is nullable = false
    private Long sentenceNumber;

    @ForeignKey(name = "Fk_LEG_OrdSnt1")
    @OneToMany(mappedBy = "sentence", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<TermEntity> sentenceTerms;

    /**
     * @DbComment sentenceStatus 'The status of a sentence, will be used by sentence calculation logic. Pending/Included/Excluded. Only ‘Included’ sentences will participate in sentence calculation.'
     */
    @Column(name = "sentenceStatus", nullable = true, length = 64) // business requirement is nullable = false
    @MetaCode(set = MetaSet.SENTENCE_STATUS)
    private String sentenceStatus;

    /**
     * @DbComment sentenceStartDate 'The start date/time for the sentence.'
     */
    @Column(name = "sentenceStartDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date sentenceStartDate;

    /**
     * @DbComment sentenceStartTime 'The start time for the sentence.'
     */
    @Column(name = "sentenceStartTime", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date sentenceStartTime;

    /**
     * @DbComment sentenceEndDate 'The date on which the sentence ends.'
     */
    @Column(name = "sentenceEndDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date sentenceEndDate;

    /**
     * @DbComment sentenceEndTime 'The time on which the sentence ends.'
     */
    @Column(name = "sentenceEndTime", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date sentenceEndTime;

    /**
     * @DbComment sentenceAppealId 'Foreign key from table LEG_OrdSNTAppeal'
     */
    @ForeignKey(name = "Fk_LEG_OrdSnt2")
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "sentenceAppealId")
    private SentenceAppealEntity sentenceAppeal;

    /**
     * @DbComment concecutiveSentenceAssoc 'The static reference to the ordersentence service that relates to a sentence that this order (sentence) is concecutive to.'
     */
    @Column(name = "concecutiveSentenceAssoc", nullable = true)
    private Long concecutiveFrom;

    /**
     * @DbComment fineAmount 'The fine for a sentence. The unit would be the system wide currency defined.'
     */
    @Column(name = "fineAmount", nullable = true, precision = 12, scale = 2)
    private BigDecimal fineAmount;

    /**
     * @DbComment fineAmountPaid 'If true, Fine amount is paid, false otherwise. Default to false.'
     */
    @Column(name = "fineAmountPaid", nullable = true) // business requirement is nullable = false
    private Boolean fineAmountPaid;

    /**
     * @DbComment sentenceAggravateFlag 'True if aggravating factors were considered during sentencing, false otherwise.'
     */
    @Column(name = "sentenceAggravateFlag", nullable = true)
    private Boolean sentenceAggravateFlag;

    @ForeignKey(name = "Fk_LEG_OrdSnt3")
    @OneToMany(mappedBy = "sentence", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<IntermittentScheduleEntity> intermittentSchedule = new HashSet<IntermittentScheduleEntity>();

    @ForeignKey(name = "Fk_LEG_OrdSntSch")
    @OneToMany(mappedBy = "sentence", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<IntermittentSentenceScheduleEntity> intermittentSentenceSchedules = new HashSet<IntermittentSentenceScheduleEntity>();

    @ForeignKey(name = "FK_LEG_OrdFinePayment_SEN")
    @OneToMany(mappedBy = "sentence", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<FinePaymentEntity> finePayments = new HashSet<FinePaymentEntity>();

    /**
     * This method add an instance of FinePaymentEntity to collection of FinePaymentEntities.
     *
     * @param finePayment FinePaymentEntity
     */
    public void addFinePayment(FinePaymentEntity finePayment) {
        finePayment.setSentence(this);
        finePayments.add(finePayment);
    }

    /**
     * This method add an instance of IntermittentSentenceScheduleEntity to collection of IntermittentSentenceScheduleEntities.
     *
     * @param intermittentSentenceScheduleEntity IntermittentSentenceScheduleEntity
     */
    public void addIntermittentSentenceScheduleEntity(IntermittentSentenceScheduleEntity intermittentSentenceScheduleEntity) {
        intermittentSentenceScheduleEntity.setSentence(this);
        intermittentSentenceSchedules.add(intermittentSentenceScheduleEntity);
    }

    /**
     * @return the sentenceType
     */
    public String getSentenceType() {
        return sentenceType;
    }

    /**
     * @param sentenceType the sentenceType to set
     */
    public void setSentenceType(String sentenceType) {
        this.sentenceType = sentenceType;
    }

    /**
     * @return the sentenceNumber
     */
    public Long getSentenceNumber() {
        return sentenceNumber;
    }

    /**
     * @param sentenceNumber the sentenceNumber to set
     */
    public void setSentenceNumber(Long sentenceNumber) {
        this.sentenceNumber = sentenceNumber;
    }

    /**
     * @return the sentenceTerms
     */
    public Set<TermEntity> getSentenceTerms() {
        if (sentenceTerms == null) {
			sentenceTerms = new HashSet<TermEntity>();
		}
        return sentenceTerms;
    }

    /**
     * @param sentenceTerms the sentenceTerms to set
     */
    public void setSentenceTerms(Set<TermEntity> sentenceTerms) {
        if (sentenceTerms != null) {
            for (TermEntity term : sentenceTerms) {
                term.setSentence(this);
            }
            this.sentenceTerms = sentenceTerms;
        } else {
            this.sentenceTerms = new HashSet<TermEntity>();
        }
    }

    /**
     * @return the sentenceStatus
     */
    public String getSentenceStatus() {
        return sentenceStatus;
    }

    /**
     * @param sentenceStatus the sentenceStatus to set
     */
    public void setSentenceStatus(String sentenceStatus) {
        this.sentenceStatus = sentenceStatus;
    }

    /**
     * @return the sentenceStartDate
     */
    public Date getSentenceStartDate() {
        return sentenceStartDate;
    }

    /**
     * @param sentenceStartDate the sentenceStartDate to set
     */
    public void setSentenceStartDate(Date sentenceStartDate) {
        this.sentenceStartDate = sentenceStartDate;
    }

    /**
     * @return the sentenceEndDate
     */
    public Date getSentenceEndDate() {
        return sentenceEndDate;
    }

    /**
     * @param sentenceEndDate the sentenceEndDate to set
     */
    public void setSentenceEndDate(Date sentenceEndDate) {
        this.sentenceEndDate = sentenceEndDate;
    }

    /**
     * @return the sentenceAppeal
     */
    public SentenceAppealEntity getSentenceAppeal() {
        return sentenceAppeal;
    }

    /**
     * @param sentenceAppeal the sentenceAppeal to set
     */
    public void setSentenceAppeal(SentenceAppealEntity sentenceAppeal) {
        this.sentenceAppeal = sentenceAppeal;
    }

    /**
     * @return the concecutiveFrom
     */
    public Long getConcecutiveFrom() {
        return concecutiveFrom;
    }

    /**
     * @param concecutiveSentenceAssociation the concecutiveSentenceAssociation to set
     */
    public void setConcecutiveFrom(Long concecutiveSentenceAssociation) {
        this.concecutiveFrom = concecutiveSentenceAssociation;
    }

    /**
     * @return the fineAmount
     */
    public BigDecimal getFineAmount() {
        return fineAmount;
    }

    /**
     * @param fineAmount the fineAmount to set
     */
    public void setFineAmount(BigDecimal fineAmount) {
        this.fineAmount = fineAmount;
    }

    /**
     * @return the fineAmountPaid
     */
    public Boolean getFineAmountPaid() {
        return fineAmountPaid;
    }

    /**
     * @param fineAmountPaid the fineAmountPaid to set
     */
    public void setFineAmountPaid(Boolean fineAmountPaid) {
        this.fineAmountPaid = fineAmountPaid;
    }

    /**
     * @return the sentenceAggravateFlag
     */
    public Boolean getSentenceAggravateFlag() {
        return sentenceAggravateFlag;
    }

    /**
     * @param sentenceAggravateFlag the sentenceAggravateFlag to set
     */
    public void setSentenceAggravateFlag(Boolean sentenceAggravateFlag) {
        this.sentenceAggravateFlag = sentenceAggravateFlag;
    }

    /**
     * @return the intermittentSchedule
     */
    public Set<IntermittentScheduleEntity> getIntermittentSchedule() {
        if (intermittentSchedule == null) {
			intermittentSchedule = new HashSet<IntermittentScheduleEntity>();
		}
        return intermittentSchedule;
    }

    /**
     * @param intermittentSchedule the intermittentSchedule to set
     */
    public void setIntermittentSchedule(Set<IntermittentScheduleEntity> intermittentSchedule) {
        if (intermittentSchedule != null) {
            for (IntermittentScheduleEntity inter : intermittentSchedule) {
                inter.setSentence(this);
            }
            this.intermittentSchedule = intermittentSchedule;
        } else {
			this.intermittentSchedule = new HashSet<IntermittentScheduleEntity>();
		}
    }

    /**
     * @return the intermittentSentenceSchedule
     */
    public Set<IntermittentSentenceScheduleEntity> getIntermittentSentenceSchedules() {
        return intermittentSentenceSchedules;
    }

    /**
     * @param intermittentSentenceSchedule the intermittentSentenceSchedule to set
     */
    public void setIntermittentSentenceSchedules(Set<IntermittentSentenceScheduleEntity> intermittentSentenceSchedules) {
        this.intermittentSentenceSchedules = intermittentSentenceSchedules;
    }

    /**
     * @return the finePayments
     */
    public Set<FinePaymentEntity> getFinePayments() {
        return finePayments;
    }

    /**
     * @param finePayments the finePayments to set
     */
    public void setFinePayments(Set<FinePaymentEntity> finePayments) {
        this.finePayments = finePayments;
    }

    /**
     * @return the sentenceStartTime
     */
    public Date getSentenceStartTime() {
        return sentenceStartTime;
    }

    /**
     * @param sentenceStartTime the sentenceStartTime to set
     */
    public void setSentenceStartTime(Date sentenceStartTime) {
        this.sentenceStartTime = sentenceStartTime;
    }

    /**
     * @return the sentenceEndTime
     */
    public Date getSentenceEndTime() {
        return sentenceEndTime;
    }

    /**
     * @param sentenceEndTime the sentenceEndTime to set
     */
    public void setSentenceEndTime(Date sentenceEndTime) {
        this.sentenceEndTime = sentenceEndTime;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((concecutiveFrom == null) ? 0 : concecutiveFrom.hashCode());
        result = prime * result + ((fineAmount == null) ? 0 : fineAmount.hashCode());
        result = prime * result + ((fineAmountPaid == null) ? 0 : fineAmountPaid.hashCode());
        result = prime * result + ((intermittentSchedule == null) ? 0 : intermittentSchedule.hashCode());
        result = prime * result + ((sentenceAggravateFlag == null) ? 0 : sentenceAggravateFlag.hashCode());
        result = prime * result + ((sentenceAppeal == null) ? 0 : sentenceAppeal.hashCode());
        result = prime * result + ((sentenceEndDate == null) ? 0 : sentenceEndDate.hashCode());
        result = prime * result + ((sentenceNumber == null) ? 0 : sentenceNumber.hashCode());
        result = prime * result + ((sentenceStartDate == null) ? 0 : sentenceStartDate.hashCode());
        result = prime * result + ((sentenceStartTime == null) ? 0 : sentenceStartTime.hashCode());
        result = prime * result + ((sentenceEndTime == null) ? 0 : sentenceEndTime.hashCode());
        result = prime * result + ((sentenceStatus == null) ? 0 : sentenceStatus.hashCode());
        result = prime * result + ((sentenceTerms == null) ? 0 : sentenceTerms.hashCode());
        result = prime * result + ((sentenceType == null) ? 0 : sentenceType.hashCode());
        //		result = prime * result + ((intermittentSentenceSchedules == null) ? 0 : intermittentSentenceSchedules.hashCode());
        //		result = prime * result + ((finePayments == null) ? 0 : finePayments.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SentenceEntity other = (SentenceEntity) obj;
        if (concecutiveFrom == null) {
            if (other.concecutiveFrom != null) {
				return false;
			}
        } else if (!concecutiveFrom.equals(other.concecutiveFrom)) {
			return false;
		}
        if (fineAmount == null) {
            if (other.fineAmount != null) {
				return false;
			}
        } else if (!fineAmount.equals(other.fineAmount)) {
			return false;
		}
        if (fineAmountPaid == null) {
            if (other.fineAmountPaid != null) {
				return false;
			}
        } else if (!fineAmountPaid.equals(other.fineAmountPaid)) {
			return false;
		}
        if (intermittentSchedule == null) {
            if (other.intermittentSchedule != null) {
				return false;
			}
        } else if (!intermittentSchedule.equals(other.intermittentSchedule)) {
			return false;
		}
        if (sentenceAggravateFlag == null) {
            if (other.sentenceAggravateFlag != null) {
				return false;
			}
        } else if (!sentenceAggravateFlag.equals(other.sentenceAggravateFlag)) {
			return false;
		}
        if (sentenceAppeal == null) {
            if (other.sentenceAppeal != null) {
				return false;
			}
        } else if (!sentenceAppeal.equals(other.sentenceAppeal)) {
			return false;
		}
        if (sentenceEndDate == null) {
            if (other.sentenceEndDate != null) {
				return false;
			}
        } else if (!sentenceEndDate.equals(other.sentenceEndDate)) {
			return false;
		}
        if (sentenceEndTime == null) {
            if (other.sentenceEndTime != null) {
				return false;
			}
        } else if (!sentenceEndTime.equals(other.sentenceEndTime)) {
			return false;
		}
        if (sentenceNumber == null) {
            if (other.sentenceNumber != null) {
				return false;
			}
        } else if (!sentenceNumber.equals(other.sentenceNumber)) {
			return false;
		}
        if (sentenceStartDate == null) {
            if (other.sentenceStartDate != null) {
				return false;
			}
        } else if (!sentenceStartDate.equals(other.sentenceStartDate)) {
			return false;
		}
        if (sentenceStartTime == null) {
            if (other.sentenceStartTime != null) {
				return false;
			}
        } else if (!sentenceStartTime.equals(other.sentenceStartTime)) {
			return false;
		}
        if (sentenceStatus == null) {
            if (other.sentenceStatus != null) {
				return false;
			}
        } else if (!sentenceStatus.equals(other.sentenceStatus)) {
			return false;
		}
        if (sentenceTerms == null) {
            if (other.sentenceTerms != null) {
				return false;
			}
        } else if (!sentenceTerms.equals(other.sentenceTerms)) {
			return false;
		}
        if (sentenceType == null) {
            if (other.sentenceType != null) {
				return false;
			}
        } else if (!sentenceType.equals(other.sentenceType)) {
			return false;
		}
        if (intermittentSentenceSchedules == null) {
            if (other.intermittentSentenceSchedules != null) {
				return false;
			}
        } else if (!intermittentSentenceSchedules.equals(other.intermittentSentenceSchedules)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SentenceEntity [sentenceType=" + sentenceType + ", sentenceNumber=" + sentenceNumber +
                ", sentenceTerms=" + sentenceTerms + ", sentenceStatus=" + sentenceStatus + ", sentenceStartDate=" + sentenceStartDate +
                ", sentenceEndDate=" + sentenceEndDate + ", sentenceAppeal=" + sentenceAppeal + ", concecutiveFrom=" + concecutiveFrom +
                ", fineAmount=" + fineAmount + ", fineAmountPaid=" + fineAmountPaid + ", sentenceAggravateFlag=" + sentenceAggravateFlag +
                ", intermittentSchedule=" + intermittentSchedule + ", intermittentSentenceSchedules=" + intermittentSentenceSchedules +
                ", finePayments=" + finePayments + "]";
    }

}
