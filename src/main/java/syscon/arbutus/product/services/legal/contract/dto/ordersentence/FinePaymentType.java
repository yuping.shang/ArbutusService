package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * FinePaymentType for Legal Service
 *
 * @author amlendu.kumar
 * @version 1.0
 * @since September 3, 2014
 */
public class FinePaymentType implements Serializable {

    private static final long serialVersionUID = -7373505770016680607L;

    private Long id;
    @NotNull
    private Long payerId;
    @NotNull
    private Date paymentDate;
    @NotNull
    private BigDecimal amountPaid;
    @NotNull
    private BigDecimal amountOwing;
    @NotNull
    private BigDecimal proposedAmountOwing;
    @NotNull
    private BigDecimal finePaidToDate;
    @NotNull
    private Long staffId;
    @NotNull
    private String receiptNumber;
    @NotNull
    private Long daysSatisfies;
    @NotNull
    private BigDecimal perDiemRate;
    @NotNull
    private Long daysServed;
    @NotNull
    private Long daysToLeftToServe;
    @NotNull
    private Date releaseDate;
    @NotNull
    private Date adjustedReleaseDate;
    @NotNull
    private Long sentenceId;

    /**
     * Get id -- Unique system generated identifier for the Fine Payment.
     *
     * @return the id - Long
     */
    public Long getId() {
        return id;
    }

    /**
     * Set id -- Unique system generated identifier for the Fine Payment.
     *
     * @param Long the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Get the identifier of the payer of  the Fine
     *
     * @return the payerId - Long
     */
    public Long getPayerId() {
        return payerId;
    }

    /**
     * Set the identifier of the payer of  the Fine
     *
     * @param Long the payerId to set
     */
    public void setPayerId(Long payerId) {
        this.payerId = payerId;
    }

    /**
     * Get the date and time the fine was paid.
     *
     * @return the paymentDate - Date
     */
    public Date getPaymentDate() {
        return paymentDate;
    }

    /**
     * Set the date and time the fine was paid.
     *
     * @param Date the paymentDate to set
     */
    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     * Get the amount of money the payer paid
     *
     * @return the amountPaid - BigDecimal
     */
    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    /**
     * Set the amount of money the payer paid
     *
     * @param BigDecimal the amountPaid to set
     */
    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    /**
     * Get the balance amount still owing after amount paid.
     *
     * @return the amountOwing - BigDecimal
     */
    public BigDecimal getAmountOwing() {
        return amountOwing;
    }

    /**
     * Set the balance amount still owing after amount paid.
     *
     * @param BigDecimal the amountOwing to set
     */
    public void setAmountOwing(BigDecimal amountOwing) {
        this.amountOwing = amountOwing;
    }

    /**
     * Get the balance possible amount owing after a proposed amount paid is entered in the calculator.
     * This value becomes the Amount Owing if the proposed amount to pay is the actual recorded paid amount.
     *
     * @return the proposedAmountOwing - BigDecimal
     */
    public BigDecimal getProposedAmountOwing() {
        return proposedAmountOwing;
    }

    /**
     * Set the balance possible amount owing after a proposed amount paid is entered in the calculator.
     * This value becomes the Amount Owing if the proposed amount to pay is the actual recorded paid amount.
     *
     * @param BigDecimal the proposedAmountOwing to set
     */
    public void setProposedAmountOwing(BigDecimal proposedAmountOwing) {
        this.proposedAmountOwing = proposedAmountOwing;
    }

    /**
     * Get the total amount of fine the Inmate has paid to date for a particular sentence.
     *
     * @return the finePaidToDate - BigDecimal
     */
    public BigDecimal getFinePaidToDate() {
        return finePaidToDate;
    }

    /**
     * Set the total amount of fine the Inmate has paid to date for a particular sentence.
     *
     * @param BigDecimal the finePaidToDate to set
     */
    public void setFinePaidToDate(BigDecimal finePaidToDate) {
        this.finePaidToDate = finePaidToDate;
    }

    /**
     * Get the staff identifier who recorded the payment
     *
     * @return the staffId - Long
     */
    public Long getStaffId() {
        return staffId;
    }

    /**
     * Set the staff identifier who recorded the payment
     *
     * @param Long the staffId to set
     */
    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    /**
     * Get the receipt number for the payment
     *
     * @return the receiptNumber - Long
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * Set the receipt number for the payment
     *
     * @param Long the receiptNumber to set
     */
    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    /**
     * Get the number of jail time days the paid amount  satisfies
     *
     * @return the daysSatisfies - Long
     */
    public Long getDaysSatisfies() {
        return daysSatisfies;
    }

    /**
     * Set the number of jail time days the paid amount  satisfies
     *
     * @param Long the daysSatisfies to set
     */
    public void setDaysSatisfies(Long daysSatisfies) {
        this.daysSatisfies = daysSatisfies;
    }

    /**
     * Get the amount of money that equals 1 day of jail time
     *
     * @return the perDiemRate - BigDecimal
     */
    public BigDecimal getPerDiemRate() {
        return perDiemRate;
    }

    /**
     * Set the amount of money that equals 1 day of jail time
     *
     * @param BigDecimal the perDiemRate to set
     */
    public void setPerDiemRate(BigDecimal perDiemRate) {
        this.perDiemRate = perDiemRate;
    }

    /**
     * Get the number of days that the inmate has served since the start of the sentence
     *
     * @return the daysServed - Long
     */
    public Long getDaysServed() {
        return daysServed;
    }

    /**
     * Set the number of days that the inmate has served since the start of the sentence
     *
     * @param Long the daysServed to set
     */
    public void setDaysServed(Long daysServed) {
        this.daysServed = daysServed;
    }

    /**
     * Get the number of days remaining that the inmate is left to serve after fine payment to date.
     *
     * @return the daysToLeftToServe - Long
     */
    public Long getDaysToLeftToServe() {
        return daysToLeftToServe;
    }

    /**
     * Set the number of days remaining that the inmate is left to serve after fine payment to date.
     *
     * @param Long the daysToLeftToServe to set
     */
    public void setDaysToLeftToServe(Long daysToLeftToServe) {
        this.daysToLeftToServe = daysToLeftToServe;
    }

    /**
     * Get the sentence start date plus the sentence term in lieu of payment.
     *
     * @return the releaseDate - date
     */
    public Date getReleaseDate() {
        return releaseDate;
    }

    /**
     * Set the sentence start date plus the sentence term in lieu of payment.
     *
     * @param Date the releaseDate to set
     */
    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     * Get the new release date after the paid payment.
     *
     * @return the adjustedReleaseDate - Date
     */
    public Date getAdjustedReleaseDate() {
        return adjustedReleaseDate;
    }

    /**
     * Set the new release date after the paid payment.
     *
     * @param Date the adjustedReleaseDate to set
     */
    public void setAdjustedReleaseDate(Date adjustedReleaseDate) {
        this.adjustedReleaseDate = adjustedReleaseDate;
    }

    /**
     * Get the Sentence (parent) identifier
     *
     * @return the sentenceId - Long
     */
    public Long getSentenceId() {
        return sentenceId;
    }

    /**
     * Set the Sentence (parent) identifier to associate Sentence
     *
     * @param Long the sentenceId to set
     */
    public void setSentenceId(Long sentenceId) {
        this.sentenceId = sentenceId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "FinePaymentType [id=" + id + ", payerId=" + payerId + ", paymentDate=" + paymentDate + ", amountPaid=" + amountPaid + ", amountOwing=" + amountOwing
                + ", proposedAmountOwing=" + proposedAmountOwing + ", finePaidToDate=" + finePaidToDate + ", staffId=" + staffId + ", receiptNumber=" + receiptNumber
                + ", daysSatisfies=" + daysSatisfies + ", perDiemRate=" + perDiemRate + ", daysServed=" + daysServed + ", daysToLeftToServe=" + daysToLeftToServe
                + ", releaseDate=" + releaseDate + ", adjustedReleaseDate=" + adjustedReleaseDate + ", sentenceId=" + sentenceId + "]";
    }

}
