package syscon.arbutus.product.services.legal.contract.ejb;

import java.util.*;

import org.hibernate.Session;
import syscon.arbutus.product.services.legal.contract.dto.LegalModule;
import syscon.arbutus.product.services.legal.contract.dto.charge.*;
import syscon.arbutus.product.services.legal.realization.persistence.charge.*;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;

/**
 * This helper is provided for utility functions to support charge business functions.
 *
 * @author byu
 */
public class ChargeHelper {

    public static ChargeEntity toChargeEntity(ChargeType dto) {

        if (dto == null) {
			return null;
		}

        ChargeEntity ret = new ChargeEntity(dto.getChargeId(), dto.getCaseInfoId(), dto.getStatuteChargeId(), null, //to be set later with sequence generated groupId
                dto.getChargeCount(), dto.getChargeDisposition().getChargeStatus(), dto.getChargeDisposition().getDispositionOutcome(),
                dto.getChargeDisposition().getDispositionDate(), dto.isPrimary(), dto.getOffenceStartDate(), dto.getOffenceEndDate(), dto.getFilingDate(),
                dto.getFacilityId(), dto.getOrganizationId(), dto.getPersonIdentityId(), toChargeLegalTextEntity(dto.getLegalText()),
                toChargePleaEntity(dto.getChargePlea()), toChargeAppealEntity(dto.getChargeAppeal()), LegalHelper.toCommentEntity(dto.getComment()), dto.getSentenceId());

        //OJ charge info.
        ret.setOJChargeCode(dto.getOJChargeCode());
        ret.setOJChargeDescription(dto.getOJChargeDescription());
        ret.setOJOrderId(dto.getOJOrderId());
        ret.setOJStatuteIdName(dto.getOJStatuteIdName());
        ret.setChargeJurisdiction(dto.getChargeJurisdiction());

        //build chargeIdentifier with Charge relationship
        if (dto.getChargeIdentifiers() != null) {
            for (ChargeIdentifierType chargeIdentifier : dto.getChargeIdentifiers()) {
                ChargeIdentifierEntity entity = toChargeIdentifierEntity(chargeIdentifier);
                ret.addChargeIdentifier(entity);
            }
        }

        //build ChargeIndicatorType with Charge relationship
        if (dto.getChargeIndicators() != null) {
            for (ChargeIndicatorType chargeIndicator : dto.getChargeIndicators()) {
                ChargeIndicatorEntity entity = toChargeIndicatorEntity(chargeIndicator);
                ret.addChargeIndicator(entity);
            }
        }

        //Build module associations with charge relationship
        ret.setModuleAssociations(toChargeModuleAssociationEntitySet(dto, ret));

        return ret;

    }

    public static ChargeType toChargeType(ChargeEntity entity) {

        if (entity == null) {
			return null;
		}

        ChargeType ret = new ChargeType(entity.getChargeId(), entity.getStatuteChargeId(), entity.getChargeGroupId(),
                toChargeIdentifierTypeSet(entity.getChargeIdentifiers()), toChargeIndicatorTypeSet(entity.getChargeIndicators()), //entity.getChargeIndicators(),
                toChargeLegalText(entity.getLegalText()), toChargeDispositionType(entity), toChargePleaType(entity.getChargePlea()),
                toChargeAppealType(entity.getChargeAppeal()), entity.getChargeCount(), entity.getFacilityId(), entity.getOrganizationId(), entity.getPersonIdentityId(),
                entity.getOffenceStartDate(), entity.getOffenceEndDate(), entity.getChargeFilingDate(), entity.isPrimary(),
                LegalHelper.toCommentType(entity.getComment()), entity.getSentenceId(), entity.getCaseInfoId(),
                toModuleIdSet(entity.getModuleAssociations(), LegalModule.CONDITION),    //conditionIds
                toModuleIdSet(entity.getModuleAssociations(), LegalModule.ORDER_SENTENCE)    //orderSentenceIds
        );

        List<ChargeDispositionType> dispositionHistories = toDispositionHistories(entity.getDispositionHistories());

        ret.setDispositionHistories(dispositionHistories);

        //OJ charge info.
        ret.setOJChargeCode(entity.getOJChargeCode());
        ret.setOJChargeDescription(entity.getOJChargeDescription());
        ret.setOJOrderId(entity.getOJOrderId());
        ret.setOJStatuteIdName(entity.getOJStatuteIdName());
        ret.setChargeJurisdiction(entity.getChargeJurisdiction());

        return ret;
    }

    public static List<ChargeDispositionType> toDispositionHistories(List<ChargeDispositionHistEntity> dispositionHistoryEntities) {
        List<ChargeDispositionType> dispositionHistories = new ArrayList<ChargeDispositionType>();
        for (ChargeDispositionHistEntity entity : dispositionHistoryEntities) {
            ChargeDispositionType chargeDispositionType = new ChargeDispositionType(entity.getDispositionOutcome(), entity.getChargeStatus(),
                    entity.getDispositionDate());
            dispositionHistories.add(chargeDispositionType);
        }

        return dispositionHistories;
    }

    public static List<ChargeType> toChargeTypeList(Session session, List<ChargeEntity> chargeEntityList) {
        if (chargeEntityList == null || chargeEntityList.size() == 0) {
			return null;
		}

        List<ChargeType> ret = new ArrayList<ChargeType>();
        for (ChargeEntity entity : chargeEntityList) {
            ChargeType charge = toChargeType(entity);
            fillDisplayfields(session, charge, entity);
            ret.add(charge);
        }

        return ret;
    }

    public static ChargeLegalTextEntity toChargeLegalTextEntity(String dto) {

        if (dto == null) {
			return null;
		}

        ChargeLegalTextEntity ret = new ChargeLegalTextEntity();
        ret.setLegalText(dto);

        return ret;

    }

    public static String toChargeLegalText(ChargeLegalTextEntity entity) {

        if (entity == null) {
			return null;
		}

        String ret = entity.getLegalText();

        return ret;

    }

    public static ChargeDispositionType toChargeDispositionType(ChargeEntity entity) {

        if (entity == null) {
			return null;
		}

        ChargeDispositionType ret = new ChargeDispositionType(entity.getDispositionOutcome(), entity.getChargeStatus(), entity.getDispositionDate());

        return ret;

    }

    public static ChargePleaEntity toChargePleaEntity(ChargePleaType dto) {

        if (dto == null) {
			return null;
		}

        ChargePleaEntity ret = new ChargePleaEntity(null, dto.getPleaCode(), dto.getPleaComment());

        return ret;

    }

    public static ChargePleaType toChargePleaType(ChargePleaEntity entity) {

        if (entity == null) {
			return null;
		}

        ChargePleaType ret = new ChargePleaType(entity.getPleaCode(), entity.getPleaComment());

        return ret;

    }

    public static ChargeAppealEntity toChargeAppealEntity(ChargeAppealType dto) {

        if (dto == null) {
			return null;
		}

        ChargeAppealEntity ret = new ChargeAppealEntity(null, dto.getAppealStatus(), dto.getAppealComment());

        return ret;

    }

    public static ChargeAppealType toChargeAppealType(ChargeAppealEntity entity) {

        if (entity == null) {
			return null;
		}

        ChargeAppealType ret = new ChargeAppealType(entity.getAppealStatus(), entity.getAppealComment());

        return ret;

    }

    public static ChargeIdentifierEntity toChargeIdentifierEntity(ChargeIdentifierType dto) {

        if (dto == null) {
			return null;
		}

        ChargeIdentifierEntity ret = new ChargeIdentifierEntity(dto.getIdentifierId(), dto.getIdentifierType(), dto.getIdentifierValue(), dto.getIdentifierFormat(),
                dto.getOrganizationId(), dto.getFacilityId(), dto.getComment());

        return ret;
    }

    public static ChargeIdentifierType toChargeIdentifierType(ChargeIdentifierEntity entity) {

        if (entity == null) {
			return null;
		}

        ChargeIdentifierType ret = new ChargeIdentifierType(entity.getIdentifierId(), entity.getIdentifierType(), entity.getIdentifierValue(),
                entity.getIdentifierFormat(), entity.getOrganizationId(), entity.getFacilityId(), entity.getIdentifierComment());

        return ret;
    }

    public static Set<ChargeIdentifierType> toChargeIdentifierTypeSet(Set<ChargeIdentifierEntity> entities) {

        if (entities == null) {
			return null;
		}

        Set<ChargeIdentifierType> ret = new HashSet<ChargeIdentifierType>();
        for (ChargeIdentifierEntity entity : entities) {
            if (entity != null) {
				ret.add(toChargeIdentifierType(entity));
			}
        }

        return ret;
    }

    public static ChargeIndicatorEntity toChargeIndicatorEntity(ChargeIndicatorType dto) {

        if (dto == null) {
			return null;
		}

        ChargeIndicatorEntity ret = new ChargeIndicatorEntity(dto.getIndicatorId(), dto.getChargeIndicator(), dto.isHasIndicatorValue(), dto.getIndicatorValue());
        return ret;

    }

    public static ChargeIndicatorType toChargeIndicatorType(ChargeIndicatorEntity entity) {

        if (entity == null) {
			return null;
		}

        ChargeIndicatorType ret = new ChargeIndicatorType(entity.getIndicatorId(), entity.getChargeIndicator(), entity.getHasIndicatorValue(),
                entity.getIndicatorValue());

        return ret;
    }

    public static Set<ChargeIndicatorType> toChargeIndicatorTypeSet(Set<ChargeIndicatorEntity> entities) {

        if (entities == null) {
			return null;
		}

        Set<ChargeIndicatorType> ret = new HashSet<ChargeIndicatorType>();

        for (ChargeIndicatorEntity entity : entities) {
            if (entity != null) {
				ret.add(toChargeIndicatorType(entity));
			}
        }

        return ret;
    }

    public static Set<ChargeModuleAssociationEntity> toChargeModuleAssociationEntitySet(ChargeType dto, ChargeEntity entity) {

        if (dto == null) {
			return null;
		}

        Set<ChargeModuleAssociationEntity> ret = new HashSet<ChargeModuleAssociationEntity>();

        //Add Order related
        Set<ChargeModuleAssociationEntity> orderAssociations = toChargeModuleAssociationEntitySet(entity, LegalModule.ORDER_SENTENCE.value(), dto.getOrderSentenceIds());
        if (orderAssociations != null && orderAssociations.size() > 0) {
			ret.addAll(orderAssociations);
		}

        //Add condition related
        Set<ChargeModuleAssociationEntity> condidtionAssociations = toChargeModuleAssociationEntitySet(entity, LegalModule.CONDITION.value(), dto.getConditionIds());
        if (condidtionAssociations != null && condidtionAssociations.size() > 0) {
			ret.addAll(condidtionAssociations);
		}

        return ret;

    }

    public static Set<ChargeModuleAssociationEntity> toChargeModuleAssociationEntitySet(ChargeEntity entity, String toClass, Set<Long> toIdentifiers) {

        if (toIdentifiers == null || toClass == null) {
			return null;
		}

        Set<ChargeModuleAssociationEntity> ret = new HashSet<ChargeModuleAssociationEntity>();

        for (Long toIdentifier : toIdentifiers) {
            if (toIdentifier != null) {
                ChargeModuleAssociationEntity asscEntity = new ChargeModuleAssociationEntity();
                asscEntity.setToClass(toClass);
                asscEntity.setToIdentifier(toIdentifier);
                asscEntity.setCharge(entity);
                ret.add(asscEntity);
            }
        }

        return ret;
    }

    public static Set<Long> toModuleIdSet(Set<ChargeModuleAssociationEntity> moduleAssociations, LegalModule moduleName) {

        if (moduleAssociations == null || moduleName == null) {
			return null;
		}

        Set<Long> ret = new HashSet<Long>();

        for (ChargeModuleAssociationEntity entity : moduleAssociations) {
            if (entity != null && entity.getToClass().equalsIgnoreCase(moduleName.value())) {
                ret.add(entity.getToIdentifier());
            }
        }

        return ret;

    }

    public static StatuteChargeConfigEntity toStatuteChargeConfigEntity(StatuteChargeConfigType dto) {

        if (dto == null) {
			return null;
		}

        StatuteChargeConfigEntity ret = new StatuteChargeConfigEntity(dto.getStatuteChargeId(), dto.getStatuteId(), dto.getChargeCode(), dto.getChargeType(),
                dto.getChargeDegree(), dto.getChargeCategory(), dto.getChargeSeverity(), dto.getBailAmount(), dto.isBailAllowed(), dto.isBondAllowed(),
                dto.getStartDate(), dto.getEndDate(), toStatuteChargeTextEntity(dto.getChargeText()), dto.getExternalChargeCodeIds(),
                toStatuteChargeIndicatorEntitySet(dto.getChargeIndicators()), dto.getEnhancingFactors(), dto.getReducingFactors());

        return ret;

    }

    public static StatuteChargeConfigType toStatuteChargeConfigType(StatuteChargeConfigEntity entity) {

        if (entity == null) {
			return null;
		}

        StatuteChargeConfigType ret = new StatuteChargeConfigType(entity.getStatuteChargeId(), entity.getStatuteId(), entity.getChargeCode(), entity.getChargeType(),
                entity.getChargeDegree(), entity.getChargeCategory(), entity.getChargeSeverity(), entity.getBailAmount(), entity.isBailAllowed(), entity.isBondAllowed(),
                entity.getStartDate(), entity.getEndDate(), toStatuteChargeTextType(entity.getStatuteChargeText()), new HashSet<Long>(entity.getExternalChargeCodeIds()),
                toStatuteChargeIndicatorTypeSet(entity.getChargeIndicators()), new HashSet<String>(entity.getChargeEnhancingFactors()),
                new HashSet<String>(entity.getChargeReducingFactors()));

        return ret;
    }

    public static List<StatuteChargeConfigType> toStatuteChargeConfigTypeList(List<StatuteChargeConfigEntity> entities) {

        if (entities == null || entities.size() == 0) {
			return null;
		}

        List<StatuteChargeConfigType> ret = new ArrayList<StatuteChargeConfigType>();

        Iterator<StatuteChargeConfigEntity> it = entities.iterator();
        while (it.hasNext()) {
            StatuteChargeConfigEntity entity = (StatuteChargeConfigEntity) it.next();
            ret.add(toStatuteChargeConfigType(entity));
        }

        return ret;
    }

    public static StatuteChargeTextEntity toStatuteChargeTextEntity(StatuteChargeTextType dto) {

        if (dto == null) {
			return null;
		}

        StatuteChargeTextEntity ret = new StatuteChargeTextEntity(null, dto.getLanguage(), dto.getDescription(), dto.getLegalText());

        return ret;

    }

    public static StatuteChargeTextType toStatuteChargeTextType(StatuteChargeTextEntity entity) {

        if (entity == null) {
			return null;
		}

        StatuteChargeTextType ret = new StatuteChargeTextType(entity.getLanguage(), entity.getDescription(), entity.getLegalText());

        return ret;
    }

    public static Set<StatuteChargeIndicatorEntity> toStatuteChargeIndicatorEntitySet(Set<ChargeIndicatorType> dtos) {

        if (dtos == null) {
			return null;
		}

        Set<StatuteChargeIndicatorEntity> ret = new HashSet<StatuteChargeIndicatorEntity>();

        for (ChargeIndicatorType dto : dtos) {
            ret.add(toStatuteChargeIndicatorEntity(dto));
        }

        return ret;
    }

    public static Set<ChargeIndicatorType> toStatuteChargeIndicatorTypeSet(Set<StatuteChargeIndicatorEntity> entities) {

        if (entities == null) {
			return null;
		}

        Set<ChargeIndicatorType> ret = new HashSet<ChargeIndicatorType>();
        for (StatuteChargeIndicatorEntity entity : entities) {
            ret.add(toChargeIndicatorType(entity));
        }
        return ret;
    }

    public static StatuteChargeIndicatorEntity toStatuteChargeIndicatorEntity(ChargeIndicatorType dto) {

        if (dto == null) {
			return null;
		}

        StatuteChargeIndicatorEntity ret = new StatuteChargeIndicatorEntity(dto.getIndicatorId(), dto.getChargeIndicator(), dto.isHasIndicatorValue(),
                dto.getIndicatorValue());

        return ret;

    }

    public static ChargeIndicatorType toChargeIndicatorType(StatuteChargeIndicatorEntity entity) {

        if (entity == null) {
			return null;
		}

        ChargeIndicatorType ret = new ChargeIndicatorType(entity.getIndicatorId(), entity.getChargeIndicator(), entity.isHasIndicatorValue(), entity.getIndicatorValue());

        return ret;
    }

    public static StatuteConfigEntity toStatuteConfigEntity(StatuteConfigType dto) {

        if (dto == null) {
			return null;
		}

        StatuteConfigEntity ret = new StatuteConfigEntity(dto.getStatuteId(), dto.getStatuteCode(), dto.getEnactmentDate(), dto.getRepealDate(), dto.getStatuteSection(),
                dto.getDescription(), dto.getJurisdictionId(), dto.isDefaultJurisdiction(), dto.isOutOfJurisdiction());

        return ret;

    }

    public static StatuteConfigType toStatuteConfigType(StatuteConfigEntity entity) {

        if (entity == null) {
			return null;
		}

        StatuteConfigType ret = new StatuteConfigType(entity.getStatuteId(), entity.getStatuteCode(), entity.getEnactmentDate(), entity.getRepealDate(),
                entity.getStatuteSection(), entity.getDescription(), entity.getJurisdictionId(), entity.isDefaultJurisdiction(), entity.isOutOfJurisdiction());

        //Set active statue based on repealDate, if StatuteRepealDate is null or StatuteRepealDate > system date
        ret.setActive(isStatuteActive(entity.getRepealDate()));

        return ret;
    }

    public static List<StatuteConfigType> toStatuteConfigTypeList(List<StatuteConfigEntity> entities) {

        if (entities == null || entities.size() == 0) {
			return null;
		}

        List<StatuteConfigType> ret = new ArrayList<StatuteConfigType>();

        Iterator<StatuteConfigEntity> it = entities.iterator();
        while (it.hasNext()) {
            StatuteConfigEntity entity = (StatuteConfigEntity) it.next();
            ret.add(toStatuteConfigType(entity));
        }

        return ret;
    }

    public static JurisdictionConfigEntity toJurisdictionConfigEntity(JurisdictionConfigType dto) {

        if (dto == null) {
			return null;
		}

        JurisdictionConfigEntity ret = new JurisdictionConfigEntity(dto.getJurisdictionId(), dto.getDescription(), dto.getCountry(), dto.getStateProvince(),
                dto.getCity(), dto.getCounty());

        return ret;
    }

    public static JurisdictionConfigType toJurisdictionConfigType(JurisdictionConfigEntity entity) {

        if (entity == null) {
			return null;
		}

        JurisdictionConfigType ret = new JurisdictionConfigType(entity.getJurisdictionId(), entity.getDescription(), entity.getCountry(), entity.getStateProvince(),
                entity.getCity(), entity.getCounty());

        return ret;

    }

    public static List<JurisdictionConfigType> toJurisdictionConfigTypeList(List<JurisdictionConfigEntity> entities) {

        if (entities == null || entities.size() == 0) {
			return null;
		}

        List<JurisdictionConfigType> ret = new ArrayList<JurisdictionConfigType>();

        Iterator<JurisdictionConfigEntity> it = entities.iterator();
        while (it.hasNext()) {
            JurisdictionConfigEntity entity = (JurisdictionConfigEntity) it.next();
            ret.add(toJurisdictionConfigType(entity));
        }

        return ret;
    }

    public static ExternalChargeCodeConfigEntity toExternalChargeCodeConfigEntity(ExternalChargeCodeConfigType dto) {

        if (dto == null) {
			return null;
		}

        ExternalChargeCodeConfigEntity ret = new ExternalChargeCodeConfigEntity(dto.getChargeCodeId(), dto.getCodeGroup(), dto.getChargeCode(),
                BeanHelper.isEmpty(dto.getCodeCategory()) ? null : dto.getCodeCategory(), dto.getDescription());

        return ret;

    }

    public static ExternalChargeCodeConfigType toExternalChargeCodeConfigType(ExternalChargeCodeConfigEntity entity) {

        if (entity == null) {
			return null;
		}

        ExternalChargeCodeConfigType ret = new ExternalChargeCodeConfigType(entity.getChargeCodeId(), entity.getCodeGroup(), entity.getChargeCode(),
                BeanHelper.isEmpty(entity.getCodeCategory()) ? null : entity.getCodeCategory(), entity.getDescription());

        return ret;
    }

    public static List<ExternalChargeCodeConfigType> toExternalChargeCodeConfigTypeList(List<ExternalChargeCodeConfigEntity> entities) {

        List<ExternalChargeCodeConfigType> ret = new ArrayList<ExternalChargeCodeConfigType>();

        if (entities == null) {
			return ret;
		}

        Iterator<ExternalChargeCodeConfigEntity> it = entities.iterator();
        while (it.hasNext()) {
            ExternalChargeCodeConfigEntity entity = (ExternalChargeCodeConfigEntity) it.next();
            ret.add(toExternalChargeCodeConfigType(entity));
        }

        return ret;
    }

    public static Map<Long, StatuteChargeIndicatorEntity> toStatuteChargeIndicatorEntityMap(Set<StatuteChargeIndicatorEntity> entities) {

        Map<Long, StatuteChargeIndicatorEntity> ret = new HashMap<Long, StatuteChargeIndicatorEntity>();
        if (entities == null) {
			return ret;
		}

        for (StatuteChargeIndicatorEntity entity : entities) {
            if (entity != null) {
				ret.put(entity.getIndicatorId(), entity);
			}
        }

        return ret;

    }

    public static Set<Long> toStatuteChargeIndicatorIdSet(Set<ChargeIndicatorType> dtos) {

        Set<Long> ret = new HashSet<Long>();

        if (dtos == null) {
			return ret;
		}

        for (ChargeIndicatorType dto : dtos) {
            if (dto != null && dto.getIndicatorId() != null) {
				ret.add(dto.getIndicatorId());
			}
        }

        return ret;

    }

    public static Map<String, String> toCodeTypeMap(Set<String> codes) {

        Map<String, String> ret = new HashMap<String, String>();
        if (codes == null) {
			return ret;
		}

        for (String code : codes) {
            if (code != null) {
				ret.put(code, code);
			}
        }

        return ret;
    }

    private static boolean isStatuteActive(Date repealDate) {

        boolean ret = false;

        Date currentDate = new Date();
        if (repealDate == null || repealDate.after(currentDate)) {
			ret = true;
		}

        return ret;

    }

    /**
     * Get and fill display info for an IJ charge type.
     *
     * @param charge
     * @param entity
     */
    public static void fillDisplayfields(Session session, ChargeType charge, ChargeEntity entity) {

        Long statuteChargeId = entity.getStatuteChargeId();

        if (statuteChargeId == null) {
			return;
		}

        StatuteChargeConfigEntity statuteChargeConfigEntity = BeanHelper.findEntity(session, StatuteChargeConfigEntity.class, statuteChargeId);
        StatuteConfigEntity statuteConfigEntity = BeanHelper.findEntity(session, StatuteConfigEntity.class, statuteChargeConfigEntity.getStatuteId());

        if (statuteChargeConfigEntity != null) {
            charge.setChargeCode(statuteChargeConfigEntity.getChargeCode());
            charge.setChargeType(statuteChargeConfigEntity.getChargeType());
            charge.setChargeSeverity(statuteChargeConfigEntity.getChargeSeverity());
            charge.setChargeDegree(statuteChargeConfigEntity.getChargeDegree());
            charge.setChargeDescription(statuteChargeConfigEntity.getStatuteChargeText().getDescription());
        }

        if (statuteConfigEntity != null) {
            charge.setStatuteCode(statuteConfigEntity.getStatuteCode());
            charge.setStatuteName(statuteConfigEntity.getDescription());
        }
    }

    public static ChargeHistEntity toChargeHistEntity(ChargeEntity existEntity, StampEntity createStamp) {
        // TODO Auto-generated method stub
        return null;
    }

    public static void updateExistChargeEntity(ChargeEntity existEntity, ChargeEntity newEntity) {
        //Elements which are single object.
        existEntity.setCaseInfoId(newEntity.getCaseInfoId());
        existEntity.setChargeAppeal(newEntity.getChargeAppeal());
        existEntity.setChargeCount(newEntity.getChargeCount());
        existEntity.setChargeFilingDate(newEntity.getChargeFilingDate());
        existEntity.setChargeGroupId(newEntity.getChargeGroupId());
        existEntity.setChargeId(newEntity.getChargeId());
        existEntity.setChargePlea(newEntity.getChargePlea());
        existEntity.setChargeStatus(newEntity.getChargeStatus());
        existEntity.setComment(newEntity.getComment());
        existEntity.setDispositionDate(newEntity.getDispositionDate());
        existEntity.setDispositionOutcome(newEntity.getDispositionOutcome());
        existEntity.setFacilityId(newEntity.getFacilityId());
        existEntity.setLegalText(newEntity.getLegalText());
        existEntity.setOffenceEndDate(newEntity.getOffenceEndDate());
        existEntity.setOffenceStartDate(newEntity.getOffenceStartDate());
        existEntity.setOrganizationId(newEntity.getOrganizationId());
        existEntity.setPersonIdentityId(newEntity.getPersonIdentityId());
        existEntity.setPrimary(newEntity.isPrimary());
        existEntity.setStatuteChargeId(newEntity.getStatuteChargeId());
        existEntity.setSentenceId(newEntity.getSentenceId());

        //Element which are a set of objects, like Associations and CaseIdentifiers.
        existEntity.getChargeIdentifiers().clear();
        for (ChargeIdentifierEntity identifier : newEntity.getChargeIdentifiers()) {
            existEntity.addChargeIdentifier(identifier);
        }
        existEntity.getChargeIndicators().clear();
        for (ChargeIndicatorEntity assoc : newEntity.getChargeIndicators()) {
            existEntity.addChargeIndicator(assoc);
        }

        //copy OJ charge info
        existEntity.setOJChargeCode(newEntity.getOJChargeCode());
        existEntity.setOJChargeDescription(newEntity.getOJChargeDescription());
        existEntity.setOJOrderId(newEntity.getOJOrderId());
        existEntity.setOJStatuteIdName(newEntity.getOJStatuteIdName());
        existEntity.setChargeJurisdiction(newEntity.getChargeJurisdiction());

        //TODO: handle dynamic element in future sprint.
        /*existEntity.getChargeDynamicAttributes().clear();
        for(ChargeDynamicElementValueEntity assoc : newEntity.getChargeDynamicAttributes()){
			existEntity.addChargeDynamicAttribute(assoc);
		}*/

    }

    public static void updateStamp(ChargeEntity entity, StampEntity createStamp, StampEntity modifyStamp) {
        //Update stamp for main instance
        if (modifyStamp == null) {
			entity.setStamp(createStamp);
		} else {
			entity.setStamp(modifyStamp);
		}

        //Update stamp for charge appeal.
        if (entity.getChargeAppeal() != null) {
			if (entity.getChargeAppeal().getChargeAppealId() == null) {
				entity.getChargeAppeal().setStamp(createStamp);
			} else if (modifyStamp != null) {
				entity.getChargeAppeal().setStamp(modifyStamp);
			}
		}

        //Update stamp for charge plea.
        if (entity.getChargePlea() != null) {
			if (entity.getChargePlea().getChargePleaId() == null) {
				entity.getChargePlea().setStamp(createStamp);
			} else if (modifyStamp != null) {
				entity.getChargePlea().setStamp(modifyStamp);
			}
		}

        //Update stamp for legal text.
        if (entity.getLegalText() != null) {
			if (entity.getLegalText().getChargeLegalTextId() == null) {
				entity.getLegalText().setStamp(createStamp);
			} else if (modifyStamp != null) {
				entity.getLegalText().setStamp(modifyStamp);
			}
		}

        //Update stamp for charge identifiers
        if (entity.getChargeIdentifiers() != null) {
            for (ChargeIdentifierEntity identifier : entity.getChargeIdentifiers()) {
                if (identifier != null) {
                    if (identifier.getIdentifierId() == null) {
                        identifier.setStamp(createStamp);
                    } else {
                        if (modifyStamp != null) {
							identifier.setStamp(modifyStamp);
						}
                    }
                }
            }
        }

        //Update stamp for charge indicators
        if (entity.getChargeIndicators() != null) {
            for (ChargeIndicatorEntity indicator : entity.getChargeIndicators()) {
                if (indicator != null) {
                    if (indicator.getIndicatorId() == null) {
                        indicator.setStamp(createStamp);
                    } else {
                        if (modifyStamp != null) {
							indicator.setStamp(modifyStamp);
						}
                    }
                }
            }
        }

    }
}