package syscon.arbutus.product.services.legal.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the case management configuration entity.
 *
 * @DbComment LEG_Config 'The case management configuration table for CaseManagement service'
 */
@Audited
@Entity
@Table(name = "LEG_Config")
public class ConfigEntity implements Serializable {

    private static final long serialVersionUID = 324860989362783605L;

    /**
     * @DbComment PropertyName 'The configuration property name(e.g., Currency, Offence Date)'
     */
    @Id
    @Column(name = "PropertyName", nullable = false, length = 64)
    private String propertyName;

    /**
     * @DbComment PropertyValue 'The configuration property value(e.g., Currency=USD, Offence Date=7)'
     */
    @Column(name = "PropertyValue", nullable = false, length = 64)
    private String propertyValue;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @param propertyName the propertyName to set
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * @return the propertyValue
     */
    public String getPropertyValue() {
        return propertyValue;
    }

    /**
     * @param propertyValue the propertyValue to set
     */
    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ConfigEntity [propertyName=" + propertyName + ", propertyValue=" + propertyValue + "]";
    }

}
