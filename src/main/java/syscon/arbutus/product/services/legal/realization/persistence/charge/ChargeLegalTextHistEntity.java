package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the charge legal text history entity.
 *
 * @DbComment LEG_CHGLegalTextHist 'The charge legal text data history table'
 */
//@Audited
@Entity
@Table(name = "LEG_CHGLegalTextHist")
public class ChargeLegalTextHistEntity implements Serializable {

    private static final long serialVersionUID = 5106307691737339767L;

    /**
     * @DbComment ChargeLegalTextHistoryId 'Unique id of a legal text history record of a charge instance'
     */
    @Id
    @Column(name = "ChargeLegalTextHistoryId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGLegalTextHist_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGLegalTextHist_Id", sequenceName = "SEQ_LEG_CHGLegalTextHist_Id", allocationSize = 1)
    private Long chargeLegalTextHistoryId;

    /**
     * @DbComment ChargeLegalTextId 'Unique id of a legal text record of a charge instance'
     */
    @Column(name = "ChargeLegalTextId", nullable = false)
    private Long chargeLegalTextId;

    /**
     * @DbComment LegalText 'Legal text for a charge'
     */
    @Column(name = "LegalText", nullable = true, length = 4000)
    private String legalText;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     *
     */
    public ChargeLegalTextHistEntity() {
    }

    /**
     * @param chargeLegalTextHistoryId
     * @param chargeLegalTextId
     * @param legalText
     */
    public ChargeLegalTextHistEntity(Long chargeLegalTextHistoryId, Long chargeLegalTextId, String legalText) {
        this.chargeLegalTextHistoryId = chargeLegalTextHistoryId;
        this.chargeLegalTextId = chargeLegalTextId;
        this.legalText = legalText;
    }

    /**
     * @return the chargeLegalTextHistoryId
     */
    public Long getChargeLegalTextHistoryId() {
        return chargeLegalTextHistoryId;
    }

    /**
     * @param chargeLegalTextHistoryId the chargeLegalTextHistoryId to set
     */
    public void setChargeLegalTextHistoryId(Long chargeLegalTextHistoryId) {
        this.chargeLegalTextHistoryId = chargeLegalTextHistoryId;
    }

    /**
     * @return the chargeLegalTextId
     */
    public Long getChargeLegalTextId() {
        return chargeLegalTextId;
    }

    /**
     * @param chargeLegalTextId the chargeLegalTextId to set
     */
    public void setChargeLegalTextId(Long chargeLegalTextId) {
        this.chargeLegalTextId = chargeLegalTextId;
    }

    /**
     * @return the legalText
     */
    public String getLegalText() {
        return legalText;
    }

    /**
     * @param legalText the legalText to set
     */
    public void setLegalText(String legalText) {
        this.legalText = legalText;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeLegalTextHistEntity [chargeLegalTextHistoryId=" + chargeLegalTextHistoryId + ", chargeLegalTextId=" + chargeLegalTextId + ", legalText=" + legalText
                + ", stamp=" + stamp + "]";
    }

}
