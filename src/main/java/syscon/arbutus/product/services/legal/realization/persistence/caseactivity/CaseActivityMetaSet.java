package syscon.arbutus.product.services.legal.realization.persistence.caseactivity;

/**
 * The reference set names of case activity service.
 *
 * @author lhan
 */
public class CaseActivityMetaSet {
    /**
     * ActivityCategory reference code set
     */
    public static final String ACTIVITY_CATEGORY = "CaseActivityCategory";

    /**
     * ActivityType reference code set
     */
    public static final String ACTIVITY_TYPE = "CaseActivityType";
    /**
     * ActivityOutcome reference code set
     */
    public static final String ACTIVITY_OUTCOME = "CaseActivityOutcome";
    /**
     * ActivityStatus reference code set
     */
    public static final String ACTIVITY_STATUS = "CaseActivityStatus";

}
