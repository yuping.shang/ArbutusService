package syscon.arbutus.product.services.legal.contract.dto.caseinformation;

import java.io.Serializable;

import syscon.arbutus.product.services.legal.contract.dto.ValidationUtil;

/**
 * The representation of the case identifier configuration search type
 */
public class CaseIdentifierConfigSearchType implements Serializable {

    private static final long serialVersionUID = 2633415999453196537L;

    private String identifierType;
    //private String identifierFormat;
    //private Boolean autoGeneration;
    private Boolean active;
    private Boolean primary;

    /**
     * Default constructor
     */
    public CaseIdentifierConfigSearchType() {
    }

    /**
     * Constructor
     * At least one of the search criteria must be specified.
     *
     * @param identifierType String - A kind of identifier or number that can be associated to a criminal case. E.g. Court Information Number, Indictment Number
     * @param active         Boolean - To be set appropriately to ensure that only one configuration per identifier type is active at any time
     * @param primary        Boolean - Indicates if the identifier is primary. If an identifier is flagged as primary
     */
    public CaseIdentifierConfigSearchType(String identifierType,
            /*String identifierFormat, Boolean autoGeneration, */Boolean active, Boolean primary) {
        this.identifierType = identifierType;
        //		this.identifierFormat = identifierFormat;
        //		this.autoGeneration = autoGeneration;
        this.active = active;
        this.primary = primary;
    }

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set, false otherwise.
     */
    @Deprecated
    public Boolean isWellFormed() {

        if (ValidationUtil.isEmpty(identifierType) /*&& ValidationUtil.isEmpty(identifierFormat)
                && ValidationUtil.isEmpty(autoGeneration)*/ && ValidationUtil.isEmpty(active) && ValidationUtil.isEmpty(primary)) {
			return false;
		}

        return true;
    }

    /**
     * Verify if properties are set properly.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set and all values are valid, false otherwise.
     */
    @Deprecated
    public Boolean isValid() {

        if (!isWellFormed()) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the identifierType property. It is a code value of CaseIdentifierType set.
     *
     * @return String - A kind of identifier or number that can be associated to a criminal case. E.g. Court Information Number, Indictment Number
     */
    public String getIdentifierType() {
        return identifierType;
    }

    /**
     * Sets the value of the identifierType property. It is a code value of CaseIdentifierType set.
     *
     * @param identifierType String - A kind of identifier or number that can be associated to a criminal case. E.g. Court Information Number, Indictment Number
     */
    public void setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
    }

    //	/**
    //	 * Gets the value of the identifierFormat property.
    //	 *
    //	 * @return String - Format used when auto-generating case numbers (e.g. 000000A)
    //	 */
    //	public String getIdentifierFormat() {
    //		return identifierFormat;
    //	}
    //
    //	/**
    //	 * Sets the value of the identifierFormat property.
    //	 *
    //	 * @param identifierFormat String - Format used when auto-generating case numbers (e.g. 000000A)
    //	 */
    //	public void setIdentifierFormat(String identifierFormat) {
    //		this.identifierFormat = identifierFormat;
    //	}
    //
    //	/**
    //	 * Gets the value of the autoGeneration property.
    //	 *
    //	 * @return Boolean - If “true”, system auto generates the case number using the format specified by the CaseIdentifierFormat
    //	 */
    //	public Boolean isAutoGeneration() {
    //		return autoGeneration;
    //	}
    //
    //	/**
    //	 * Sets the value of the autoGeneration property.
    //	 *
    //	 * @param autoGeneration Boolean - If “true”, system auto generates the case number using the format specified by the CaseIdentifierFormat
    //	 */
    //	public void setAutoGeneration(Boolean autoGeneration) {
    //		this.autoGeneration = autoGeneration;
    //	}

    /**
     * Gets the value of the active property.
     *
     * @return Boolean - To be set appropriately to ensure that only one configuration per identifier type is active at any time
     */
    public Boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     *
     * @param active Boolean - To be set appropriately to ensure that only one configuration per identifier type is active at any time
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * Gets the value of the primary property.
     *
     * @return Boolean - Indicates if the identifier is primary. If an identifier is flagged as primary
     */
    public Boolean isPrimary() {
        return primary;
    }

    /**
     * Sets the value of the primary property.
     *
     * @param primary Boolean - Indicates if the identifier is primary. If an identifier is flagged as primary
     */
    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

    @Override
    public String toString() {
        return "CaseIdentifierConfigSearchType [getIdentifierType()=" + getIdentifierType() + ", isActive()=" + isActive() + ", isPrimary()=" + isPrimary() + "]";
    }

}
