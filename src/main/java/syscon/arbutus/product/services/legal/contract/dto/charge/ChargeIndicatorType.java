package syscon.arbutus.product.services.legal.contract.dto.charge;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * The representation of the charge indicator type
 */
public class ChargeIndicatorType implements Serializable {

    private static final long serialVersionUID = -7903008482133936206L;

    private Long indicatorId;
    @NotNull
    private String chargeIndicator;
    @NotNull
    private Boolean hasIndicatorValue;
    @Size(max = 128, message = "max length 128")
    private String indicatorValue;

    /**
     * Default constructor
     */
    public ChargeIndicatorType() {
    }

    /**
     * Constructor
     *
     * @param indicatorId       Long - The unique Id for each charge indicator record. (optional for creation)
     * @param chargeIndicator   String - The indicators (alerts) on a charge
     * @param hasIndicatorValue Boolean - True if a value can be associated to an indicator, false otherwise.
     * @param indicatorValue    String - The value associated to the indicator (optional)
     */
    public ChargeIndicatorType(Long indicatorId, @NotNull String chargeIndicator, @NotNull Boolean hasIndicatorValue,
            @Size(max = 128, message = "max length 128") String indicatorValue) {
        this.indicatorId = indicatorId;
        this.chargeIndicator = chargeIndicator;
        this.hasIndicatorValue = hasIndicatorValue;
        this.indicatorValue = indicatorValue;
    }

    /**
     * Gets the value of the identifierId property.
     *
     * @return Long - the unique Id for each charge indicator record.
     */
    public Long getIndicatorId() {
        return indicatorId;
    }

    /**
     * Sets the value of the identifierId property.
     *
     * @param indicatorId Long - the unique Id for each charge indicator record.
     */
    public void setIndicatorId(Long indicatorId) {
        this.indicatorId = indicatorId;
    }

    /**
     * Gets the value of the chargeIndicator property. It is a reference code ChargeIndicator of set.
     *
     * @return String - the indicators (alerts) on a charge
     */
    public String getChargeIndicator() {
        return chargeIndicator;
    }

    /**
     * Sets the value of the chargeIndicator property. It is a reference code ChargeIndicator of set.
     *
     * @param chargeIndicator String - the indicators (alerts) on a charge
     */
    public void setChargeIndicator(String chargeIndicator) {
        this.chargeIndicator = chargeIndicator;
    }

    /**
     * Gets the value of the hasIndicatorValue property.
     *
     * @return Boolean - true if a value can be associated to an indicator, false otherwise.
     */
    public Boolean isHasIndicatorValue() {
        return hasIndicatorValue;
    }

    /**
     * Sets the value of the hasIndicatorValue property.
     *
     * @param hasIndicatorValue Boolean - true if a value can be associated to an indicator, false otherwise.
     */
    public void setHasIndicatorValue(Boolean hasIndicatorValue) {
        this.hasIndicatorValue = hasIndicatorValue;
    }

    /**
     * Gets the value of the indicatorValue property.
     *
     * @return String - the value associated to the indicator can be specified only if the HasIndicatorValue is ‘true’.
     */
    public String getIndicatorValue() {
        return indicatorValue;
    }

    /**
     * Sets the value of the indicatorValue property.
     *
     * @param indicatorValue String - the value associated to the indicator can be specified only if the HasIndicatorValue is ‘true’.
     */
    public void setIndicatorValue(String indicatorValue) {
        this.indicatorValue = indicatorValue;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((chargeIndicator == null) ? 0 : chargeIndicator.hashCode());
        result = prime * result + ((hasIndicatorValue == null) ? 0 : hasIndicatorValue.hashCode());
        result = prime * result + ((indicatorValue == null) ? 0 : indicatorValue.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ChargeIndicatorType other = (ChargeIndicatorType) obj;
        if (chargeIndicator == null) {
            if (other.chargeIndicator != null) {
				return false;
			}
        } else if (!chargeIndicator.equals(other.chargeIndicator)) {
			return false;
		}
        if (hasIndicatorValue == null) {
            if (other.hasIndicatorValue != null) {
				return false;
			}
        } else if (!hasIndicatorValue.equals(other.hasIndicatorValue)) {
			return false;
		}
        if (indicatorValue == null) {
            if (other.indicatorValue != null) {
				return false;
			}
        } else if (!indicatorValue.equals(other.indicatorValue)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeIndicatorType [indicatorId=" + indicatorId + ", chargeIndicator=" + chargeIndicator + ", hasIndicatorValue=" + hasIndicatorValue
                + ", indicatorValue=" + indicatorValue + "]";
    }

}
