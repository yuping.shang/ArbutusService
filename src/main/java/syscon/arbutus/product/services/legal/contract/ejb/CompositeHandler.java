/**
 *
 */
package syscon.arbutus.product.services.legal.contract.ejb;

import javax.ejb.SessionContext;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.*;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ActivityType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.DTOBindingTypeEnum;
import syscon.arbutus.product.services.core.common.Individual;
import syscon.arbutus.product.services.core.common.Supervision;
import syscon.arbutus.product.services.core.common.adapters.ClientCustomFieldServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.AssociationType;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.eventlog.contract.ejb.Constants;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.OffenderHousingAssignmentType;
import syscon.arbutus.product.services.legal.contract.dto.*;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivityType;
import syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoSearchType;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoType;
import syscon.arbutus.product.services.legal.contract.dto.composite.*;
import syscon.arbutus.product.services.legal.contract.dto.condition.ConditionType;
import syscon.arbutus.product.services.legal.contract.dto.ordersentence.BailType;
import syscon.arbutus.product.services.legal.contract.interfaces.Composite;
import syscon.arbutus.product.services.legal.realization.adapters.*;
import syscon.arbutus.product.services.legal.realization.persistence.SentenceTermEntity;
import syscon.arbutus.product.services.legal.realization.persistence.SentenceTermNameEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.CAActivityIdEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseaffiliation.CaseAffiliationEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.CaseIdentifierEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.CaseInfoEntity;
import syscon.arbutus.product.services.legal.realization.util.ActivityStatusCode;
import syscon.arbutus.product.services.legal.realization.util.CaseHelper;
import syscon.arbutus.product.services.movement.contract.dto.ExternalMovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType.MovementDirection;
import syscon.arbutus.product.services.organization.realization.persistence.OrganizationEntity;
import syscon.arbutus.product.services.person.contract.dto.personidentity.PersonIdentityType;
import syscon.arbutus.product.services.person.realization.persistence.personidentity.PersonIdentityEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.realization.persistence.SupervisionEntity;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * CompositeHandler is handling composite functions which can access other services' data.
 *
 * @author lhan
 */
public class CompositeHandler implements Composite {
    private static final Object MOVEMENT_COMPLETED = "COMPLETED";

    private static Logger log = LoggerFactory.getLogger(CompositeHandler.class);

    private SessionContext context;
    private Session session;
    private int searchMaxLimit = 200;

    //services adapters
    private PersonIdentityServiceAdapter personIdentityServiceAdapter = new PersonIdentityServiceAdapter();
    private SupervisionServiceAdapter supervisionServiceAdapter = new SupervisionServiceAdapter();
    private HousingBedManagementServiceAdapter housingBedManagementServiceAdapter = new HousingBedManagementServiceAdapter();
    private FacilityServiceAdapter facilityServiceAdapter = new FacilityServiceAdapter();
    private ActivityServiceAdapter activityServiceAdapter = new ActivityServiceAdapter();
    private MovementActivityServiceAdapter movementActivityServiceAdapter = new MovementActivityServiceAdapter();
    //private OrganizationServiceAdapter organizationServiceAdapter = new OrganizationServiceAdapter();

    public CompositeHandler(SessionContext context, Session session) {
        super();
        this.context = context;
        this.session = session;
    }

    public CompositeHandler(SessionContext context, Session session, int searchMaxLimit) {
        super();
        this.context = context;
        this.session = session;
        this.searchMaxLimit = searchMaxLimit;
    }

    ////////////////////////////////Composite APIs/////////////////////////////////

    public CaseType saveCase(UserContext uc, CaseType c) {
        String functionName = "saveCase";

        ValidationHelper.validate(c);

        String message = String.format("CaseType=%s.", c);
        LogHelper.debug(log, functionName, message);

        CaseType ret = null;

        CaseInfoType caseInfo = CaseHelper.toCaseManagementCaseInfoType(c);

        CaseInfoType rtn = null;

        CaseInfoHandler caseInfoHandler = new CaseInfoHandler(context, session, searchMaxLimit);

        if (ValidationUtil.isEmpty(caseInfo.getCaseInfoId())) {//has no id, do create

            rtn = caseInfoHandler.createCaseInfo(uc, caseInfo, false);

        } else {//has id, do update.

            rtn = caseInfoHandler.updateCaseInfo(uc, caseInfo, false);
        }

        ret = CaseHelper.toCaseType(uc, rtn, null);

        message = String.format("CaseType=%s.", ret);
        LogHelper.debug(log, functionName, message);

        return ret;
    }

    public CaseType getCase(UserContext uc, Long caseId) {
        String functionName = "getCase";

        // Validation
        if (caseId == null) {
            String message = String.format("%s, caseId=%s.", Constants.DTO_VALIDATION, caseId);
            LogHelper.error(log, functionName, message);

            throw new InvalidInputException(message);
        }

        String message = String.format("caseId=%s.", caseId);
        LogHelper.debug(log, functionName, message);

        CaseType ret = null;

        CaseInfoType rtn = new CaseInfoHandler(context, session, searchMaxLimit).getCaseInfo(uc, caseId);

        ret = CaseHelper.toCaseType(uc, rtn, null);

        message = String.format("CaseType=%s.", ret);
        LogHelper.debug(log, functionName, message);

        return ret;
    }

    public List<syscon.arbutus.product.services.legal.contract.dto.CaseInfoType> getCases(UserContext uc, CaseSearchType c) {
        String functionName = "getCases";

        // Validation
        if (c == null || !c.isValid()) {
            String message = String.format("%s, CaseSearchType=%s.", Constants.DTO_VALIDATION, c);
            LogHelper.error(log, functionName, message);

            throw new InvalidInputException(message);
        }

        List<syscon.arbutus.product.services.legal.contract.dto.CaseInfoType> ret = null;

        CaseInfoSearchType search = CaseHelper.toCaseInfoSearchType(c);

        List<CaseInfoType> ret1 = new CaseInfoHandler(context, session, searchMaxLimit).searchCaseInfo(uc, null, search, false);

        if (c.isReturnCaseType()) {

            //TODO: handle return Whole case.

        } else {

            ret = CaseHelper.toCaseInfoTypeList(uc, ret1);
        }

        String message = String.format("List<CaseInfoType>=%s.", ret);
        LogHelper.debug(log, functionName, message);

        return ret;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.Composite#inquiryCaseOffenders(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.legal.contract.dto.caseinformation.CaseInfoSearchType, java.lang.Long, java.lang.Long, java.lang.String)
     */
    @Override
    public CaseOffendersReturnType inquireCaseOffenders(UserContext uc, InquiryCaseSearchType inquiryCaseSearchType, Long startIndex, Long resultSize,
            String resultOrder) {

        ValidationHelper.validateSearchType(inquiryCaseSearchType);
        if (inquiryCaseSearchType.getCaseIssuedDateTo() != null && inquiryCaseSearchType.getCaseIssuedDateFrom() == null) {
            throw new InvalidInputException("Please enter a \"From\" Date to narrow search results.");
        }

        CaseOffendersReturnType caseInfos = inquireCases(uc, inquiryCaseSearchType, startIndex, resultSize, resultOrder);

        CaseOffendersReturnType ret = addSupervisionInfoToCaseOffenders(uc, caseInfos);

        return ret;
    }

    ////////////////////////////Case Activity related composite APIs///////////////////

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.LegalService#createCourtActivityWithMovement(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivityType, syscon.arbutus.product.services.legal.contract.dto.CourtMovementDetail)
     */
    public CourtActivityMovementType createCourtActivityWithMovement(UserContext uc, CourtActivityMovementType courtActivityMovement) {

        ValidationHelper.validate(courtActivityMovement);

        courtActivityMovement = createMovementsData(uc, courtActivityMovement);

        CourtActivityType courtActivityType = new CaseActivityHandler(context, session).create(uc, courtActivityMovement.getCourtActivityType());
        courtActivityMovement.setCourtActivityType(courtActivityType);

        return courtActivityMovement;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.Composite#retrieveCourtActivityWithMovement(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    @Override
    public CourtActivityMovementType retrieveCourtActivityWithMovement(UserContext uc, Long courtActivityId) {
        CourtActivityType courtActivityType = new CaseActivityHandler(context, session).get(uc, courtActivityId);

        CourtActivityMovementType courtActivityMovement = generateCourtActivityMovementType(uc, courtActivityType);

        return courtActivityMovement;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.Composite#retrieveCourtActivityWithMovements(syscon.arbutus.product.security.UserContext, java.lang.Long, java.lang.String)
     */
    @Override
    public List<CourtActivityMovementType> retrieveCourtActivityWithMovements(UserContext uc, Long caseId, String activityStatus) {
        if (caseId == null || (activityStatus != null && !ActivityStatusCode.contain(activityStatus))) {
            throw new InvalidInputException("caseId is null or activity status is not valid");
        }

        List<CourtActivityType> courtActivityTypes = new CaseActivityHandler(context, session).retrieveCaseActivities(uc, caseId, activityStatus);

        List<CourtActivityMovementType> rtn = new ArrayList<CourtActivityMovementType>();
        for (CourtActivityType courtActivityType : courtActivityTypes) {

            CourtActivityMovementType courtActivityMovement = generateCourtActivityMovementType(uc, courtActivityType);

            rtn.add(courtActivityMovement);
        }

        return rtn;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.Composite#updateCourtActivityWithMovement(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.legal.contract.dto.composite.CourtActivityMovementType)
     */
    @Override
    public CourtActivityMovementType updateCourtActivityWithMovement(UserContext uc, CourtActivityMovementType courtActivityMovementType) {

        CourtActivityType courtActivityType = courtActivityMovementType.getCourtActivityType();

        if (courtActivityType.getCaseActivityIdentification() == null) {
            throw new InvalidInputException("CaseActivityIdentification cannot be null for update.");
        }

        ValidationHelper.validate(courtActivityType);

        //Business rule: Cancel(remove) existing not-completed movement data if the court activity has such data.
        List<Long> activityIds = getActivityIds(courtActivityType.getCaseActivityIdentification());
        boolean cancelled = cancelMovementsData(uc, activityIds);

        CourtActivityMovementType newCourtActivityMovementType = courtActivityMovementType;

        Date courtActivityOccurredDate = courtActivityType.getActivityOccurredDate();
        Date currentDate = BeanHelper.createDate();

        //Business rule: If court activity will happen in future and previous scheduled movement data is cancelled,
        //then need to create new scheduled movement data.
        if (courtActivityOccurredDate.getTime() > currentDate.getTime() && cancelled) {
            ValidationHelper.validate(courtActivityMovementType);
            newCourtActivityMovementType = createMovementsData(uc, courtActivityMovementType);
        } else {
            courtActivityType.setActivityIds(new HashSet<Long>(activityIds));
        }

        CourtActivityType updatedCourtActivityType = new CaseActivityHandler(context, session).update(uc, courtActivityType);
        newCourtActivityMovementType.setCourtActivityType(updatedCourtActivityType);

        return newCourtActivityMovementType;
    }

    ////////Case Affiliation composite APIs/////////////////////////////////
    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.Composite#retrieveCaseAffiliations(syscon.arbutus.product.security.UserContext, java.lang.Long, java.lang.String)
	 */
    @Override
    public List<CaseAffiliationType> retrieveCaseAffiliations(UserContext uc, Long caseId, Boolean isActiveFlag) {

        String functionName = "retrieveCaseAffiliations";

        if (caseId == null) {
            String message = "caseId can not be null.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        StringBuilder sb = new StringBuilder();
        sb.append("from CaseAffiliationEntity where affiliatedCaseId = :caseId");

        if (isActiveFlag != null) {
            if (isActiveFlag.booleanValue() == true) {
                sb.append(" and startDate <= :currentDate and (endDate = null or endDate >= :currentDate)");
            } else {
                sb.append(" and (startDate > :currentDate or (endDate != null and endDate < :currentDate))");
            }
        }

        Query hqlQuery = session.createQuery(sb.toString()).setParameter("caseId", caseId);

        if (isActiveFlag != null) {
            Date currentDate = LegalHelper.getDateWithoutTime(new Date());
            hqlQuery.setTimestamp("currentDate", currentDate);
        }

        @SuppressWarnings("unchecked") List<CaseAffiliationEntity> ret = hqlQuery.list();

        return addCaseAffiliatedCompositeData(CaseAffiliationHelper.toListOfCaseAffiliationType(ret));
    }

    ///////////////////////////////////////////////////////////////
    // Bail related features
    /////////////////////////////////////////////////////////

    /**
     * Get Bail Details by bailId
     *
     * @param uc     UserContext Required
     * @param bailId Long Required
     * @return BailDetailsType returns Bail details if success; null otherwise.
     */
    public BailDetailsType getBailDetails(UserContext uc, Long bailId) {
        BailDetailsType bailDetails = new BailDetailsType();
        BailType bail = (BailType) OrderSentenceHandler.getOrder(uc, context, session, bailId);

        // Payee's Names
        Set<Long> personIds = bail.getBailerPersonIdentityAssociations();
        if (personIds != null && personIds.size() > 0) {
            List<Long> ids = new ArrayList<Long>(personIds);
            Collections.sort(ids);
            List<String> payeeNames = new ArrayList<String>();
            PersonIdentityServiceAdapter personIdentityServiceAdapter = new PersonIdentityServiceAdapter();
            for (Long id : ids) {
                payeeNames.add(personIdentityServiceAdapter.getFullName(uc, id));
            }
            bailDetails.setPayeeNames(payeeNames);
        }

        // postedDateTime
        bailDetails.setPostedDateTime(bail.getBailBondPostedDate());

        // bailPaymentReceiptNo
        bailDetails.setBailPaymentReceiptNo(bail.getBailPaymentReceiptNo());

        // bailAmounts
        bailDetails.setBailAmounts(bail.getBailAmounts());

        // bailPostedAmounts
        bailDetails.setBailPostedAmounts(bail.getBailPostedAmounts());

        // bailRequirement
        bailDetails.setBailRequirement(bail.getBailRequirement());

        // bondOrganizations
        Set<Long> organizationAssociations = bail.getBondOrganizationAssociations();
        if (organizationAssociations != null && organizationAssociations.size() > 0) {
            List<Long> ids = new ArrayList<Long>(organizationAssociations);
            Collections.sort(ids);
            OrganizationServiceAdapter organizationServiceAdapter = new OrganizationServiceAdapter();
            List<String> organizationNames = new ArrayList<String>();
            for (Long id : ids) {
                String organizationName = organizationServiceAdapter.getOrganizationName(uc, id);
                organizationNames.add(organizationName);
            }
            bailDetails.setBondOrganizations(organizationNames);
        }

        // bondPostedAmount
        bailDetails.setBailPostedAmounts(bail.getBailPostedAmounts());

        // bondPaymentDescription
        bailDetails.setBondPaymentDescription(bail.getBondPaymentDescription());

        // comments
        CommentType comment = bail.getComments();
        if (comment != null) {
            String cmnt = comment.getComment();
            bailDetails.setComments(cmnt);
        }

        // bailStatus -- dispositionOutcome of DispositionType
        bailDetails.setBailStatus(bail.getOrderDisposition().getDispositionOutcome());

        // bailStatusDate -- dispositionDate of DispositionType
        bailDetails.setBailStatusDate(bail.getOrderDisposition().getDispositionDate());

        // orderStatus -- orderStatus of DispositionType
        bailDetails.setOrderStatus(bail.getOrderDisposition().getOrderStatus());

        // conditions
        List<ConditionType> conditions = new ConditionHandler(context, session).retrieveConditionBy(uc, null, null, bailId, null, null);
        List<Long> conditionsId = new ArrayList<Long>();
        if (!BeanHelper.isEmpty(conditions)) {
            for (ConditionType condition : conditions) {
				conditionsId.add(condition.getConditionId());
			}
            bailDetails.setConditions(conditions);
        }

        return bailDetails;
    }

    /////////////////////////////////////////////////////////////////////////////
    ////// 	private methods
    ////////////////////////////////////////////////////////////////////////////

    /**
     * @param uc
     * @param caseOffendersReturnType
     * @return
     */
    private CaseOffendersReturnType addSupervisionInfoToCaseOffenders(UserContext uc, CaseOffendersReturnType caseOffendersReturnType) {

        CaseOffendersReturnType ret = caseOffendersReturnType;

        List<CaseOffenderType> caseOffenders = caseOffendersReturnType.getCaseOffenders();

        for (CaseOffenderType caseOffender : caseOffenders) {
            Supervision supervision = caseOffender.getSupervision();
            Long supervisionId = supervision.getSupervisionId();

            SupervisionType supervisionType = supervisionServiceAdapter.get(uc, supervisionId);

            if (supervisionType == null) {
                continue;
            }

            supervision.setSupervisionStatus(supervisionType.getSupervisionStatusFlag());
            supervision.setSupervisionDisplayId(supervisionType.getSupervisionDisplayID());

            PersonIdentityType personIdentity = personIdentityServiceAdapter.get(uc, supervisionServiceAdapter.getPersonIdentityId(supervisionType));
            supervision.setFirstName(personIdentity.getFirstName());
            supervision.setLastName(personIdentity.getLastName());
            supervision.setPersonIdentityId(personIdentity.getPersonIdentityId());
            supervision.setOffenderId(personIdentity.getOffenderNumber());
            
            OffenderHousingAssignmentType housingAssignment = housingBedManagementServiceAdapter.retrieveAssignmentsByOffender(uc, supervisionId);

            Long facilityId = (housingAssignment == null) ? null : housingAssignment.getFacilityId();

            String facilityName = (facilityId == null) ? "" : facilityServiceAdapter.getFacility(uc, facilityId).getFacilityName();
            supervision.setHousingFacility(facilityName);
        }

        return ret;
    }

    /**
     * @param uc
     * @param inquiryCaseSearchType
     * @param startIndex
     * @param resultSize
     * @param resultOrder
     * @return
     */
    private CaseOffendersReturnType inquireCases(UserContext uc, InquiryCaseSearchType inquiryCaseSearchType, Long startIndex, Long resultSize, String resultOrder) {

        Criteria criteria = generateInquireCaseCriteria(uc, inquiryCaseSearchType);

        if (criteria == null) {
            CaseOffendersReturnType ret = new CaseOffendersReturnType();
            ret.setTotalSize(0L);
            return ret;
        }

        Long totalSize = (Long) criteria.setProjection(Projections.countDistinct("identifierId")).uniqueResult();

        criteria.setProjection(null);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());

            if (resultSize == null || resultSize > searchMaxLimit) {
                criteria.setMaxResults(searchMaxLimit);
            } else {
                criteria.setMaxResults(resultSize.intValue());
            }

        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                criteria.addOrder(ord);
            }
        }

        @SuppressWarnings("unchecked") List<CaseIdentifierEntity> entities = criteria.list();

        CaseOffendersReturnType ret = new CaseOffendersReturnType();
        ret.setTotalSize(totalSize);

        if (!BeanHelper.isEmpty(entities)) {
            Set<Long> caseIds = new HashSet<Long>();
            for (CaseIdentifierEntity entity : entities) {
                caseIds.add(entity.getCaseInfo().getCaseInfoId());
            }
            Map<Long, List<ClientCustomFieldValueType>> caseIdCCFsMap = ClientCustomFieldServiceAdapter.getPrimaryCCFValue(uc, caseIds, DTOBindingTypeEnum.LEGALCASE);

            List<CaseOffenderType> caseOffenders = ret.getCaseOffenders();
            for (CaseIdentifierEntity entity : entities) {
                CaseInfoEntity caseInfo = entity.getCaseInfo();
                Long caseId = caseInfo.getCaseInfoId();
                CaseOffenderType caseOffender = new CaseOffenderType();
                caseOffender.setCaseIdentifierType(entity.getIdentifierType());
                caseOffender.setCaseIdentifierNumber(entity.getIdentifierValue());
                caseOffender.setCaseId(caseInfo.getCaseInfoId());
                caseOffender.setCaseIssuedDate(caseInfo.getIssuedDate());
                caseOffender.getSupervision().setSupervisionId(caseInfo.getSupervisionId());
                caseOffender.setCCFValues(caseIdCCFsMap.get(caseId));
                caseOffenders.add(caseOffender);
            }
        }

        return ret;
    }

    /**
     * @param uc
     * @param inquiryCaseSearchType
     * @return
     */
    private Criteria generateInquireCaseCriteria(UserContext uc, InquiryCaseSearchType inquiryCaseSearchType) {
        Criteria criteria = session.createCriteria(CaseIdentifierEntity.class);

        String caseIdentifierType = inquiryCaseSearchType.getCaseIdentifierType();
        if (caseIdentifierType != null) {
            criteria.add(Restrictions.eq("identifierType", caseIdentifierType));
        }

        String caseIdentifierNumber = inquiryCaseSearchType.getCaseIdentifierNumber();
        if (caseIdentifierNumber != null) {
            criteria.add(Restrictions.ilike("identifierValue", parseWildcard(caseIdentifierNumber)));
        }

        criteria.createAlias("caseInfo", "caseInfo");

        //CCF values search could return a set of case Ids, needs to add into search criteria.
        Set<ClientCustomFieldValueType> ccfSearchCriteria = inquiryCaseSearchType.getCCFSearchCriteria();
        if (!BeanHelper.isEmpty(ccfSearchCriteria)) {
            Set<ClientCustomFieldValueType> revisedCcfSearchCriteria = new HashSet<ClientCustomFieldValueType>();
            for (ClientCustomFieldValueType clientCustomFieldValueType : ccfSearchCriteria) {
                if (!BeanHelper.isEmpty(clientCustomFieldValueType.getValue())) {//No search value, should not be in search criteria.
                    revisedCcfSearchCriteria.add(clientCustomFieldValueType);
                }
            }

            if (!BeanHelper.isEmpty(revisedCcfSearchCriteria)) {

                Set<Long> caseIds = ClientCustomFieldServiceAdapter.ccfSearch(uc, revisedCcfSearchCriteria, DTOBindingTypeEnum.LEGALCASE);

                if (!BeanHelper.isEmpty(caseIds)) {
                    criteria.add(Restrictions.in("caseInfo.caseInfoId", caseIds));
                } else {
                    return null;// WOR-7085
                }
            }

        }

        Date caseIssuedDateFrom = BeanHelper.createDateWithoutTime(inquiryCaseSearchType.getCaseIssuedDateFrom());
        Date caseIssuedDateTO = BeanHelper.createDateWithoutTime(inquiryCaseSearchType.getCaseIssuedDateTo());
        if (caseIssuedDateFrom != null || (caseIssuedDateFrom != null && caseIssuedDateTO != null)) {
            if (caseIssuedDateFrom != null && caseIssuedDateTO != null) {
                criteria.add(Restrictions.ge("caseInfo.issuedDate", caseIssuedDateFrom));
                criteria.add(Restrictions.le("caseInfo.issuedDate", caseIssuedDateTO));
            } else if (caseIssuedDateFrom != null) {
                criteria.add(Restrictions.eq("caseInfo.issuedDate", caseIssuedDateFrom));
            }
        }

        facilityFilter(session, criteria, inquiryCaseSearchType.getFacilityIds());

        return criteria;
    }

    /**
     * Parse * to % in the string.
     *
     * @param str
     * @return String
     */
    private String parseWildcard(String str) {
        return str == null || str.length() == 0 ? str : str.matches("^\\*+$") ? "" : str.replaceAll("\\*+", "%");
    }

    /**
     * @param listOfCaseAffiliationType
     * @returngenerateRandomLong
     */
    private List<CaseAffiliationType> addCaseAffiliatedCompositeData(List<CaseAffiliationType> listOfCaseAffiliationType) {
        if (BeanHelper.isEmpty(listOfCaseAffiliationType)) {
            return null;
        }

        for (CaseAffiliationType caseAffiliation : listOfCaseAffiliationType) {
            Long personIdentityId = caseAffiliation.getAffiliatedPersonIdentityId();
            if (personIdentityId != null) {
                PersonIdentityEntity personIdentityentity = BeanHelper.findEntity(session, PersonIdentityEntity.class, personIdentityId);
                if (personIdentityentity != null) {
                    Individual individual = new Individual();
                    individual.setPersonIdentityId(personIdentityId);
                    individual.setFirstName(personIdentityentity.getFirstName());
                    individual.setLastName(personIdentityentity.getLastName());
                    individual.setGender(personIdentityentity.getSex());

                    caseAffiliation.setAffiliatedPersonInfo(individual);
                }
            }

            Long organizationId = caseAffiliation.getAffiliatedOrganizationId();
            if (organizationId != null) {
                OrganizationEntity organizationEntity = BeanHelper.findEntity(session, OrganizationEntity.class, organizationId);
                if (organizationEntity != null) {
                    caseAffiliation.setAffiliatedOrganizationName(organizationEntity.getOrganizationName());
                }
            }
        }

        return listOfCaseAffiliationType;
    }

    /**
     * @param uc
     * @param courtActivityMovement
     * @param activityIds
     * @return
     */
    private CourtActivityMovementType retrieveMovementData(UserContext uc, CourtActivityMovementType courtActivityMovement, Set<Long> activityIds) {
        if (activityIds != null && !activityIds.isEmpty()) {
            Long activityId = activityIds.iterator().next();
            ExternalMovementActivityType externalMovement = movementActivityServiceAdapter.getCourtMovementByActivityId(uc, activityId);

            if (null != externalMovement) {
                courtActivityMovement.setStatus(externalMovement.getMovementStatus());
                courtActivityMovement.setMovementReason(externalMovement.getMovementReason());
                courtActivityMovement.setSupervisionId(externalMovement.getSupervisionId());
                courtActivityMovement.setMovementGroupId(externalMovement.getGroupId());

                Set<CommentType> comments = externalMovement.getCommentText();
                if (comments != null && !comments.isEmpty()) {
					courtActivityMovement.setCommment(comments.iterator().next().getComment());
				}
            }
        }

        return courtActivityMovement;
    }

    /**
     * @param uc
     * @param courtActivityMovement
     * @return
     */
    private CourtActivityMovementType createMovementsData(UserContext uc, CourtActivityMovementType courtActivityMovement) {
        Long supervisionId = courtActivityMovement.getSupervisionId();

        Long housingFacilityId = housingBedManagementServiceAdapter.getHousedFacilityId(
                housingBedManagementServiceAdapter.retrieveAssignmentsByOffender(uc, supervisionId));

        Long fromFacilityOut = housingFacilityId;
        Long toFacilityOut = courtActivityMovement.getCourtActivityType().getFacilityId();

        Long fromFacilityIn = courtActivityMovement.getCourtActivityType().getFacilityId();
        Long toFacilityIn = housingFacilityId;
        Date outActivityPlanStartDate = courtActivityMovement.getCourtActivityType().getActivityOccurredDate();
        Date outActivityPlanEndDate = new Date(outActivityPlanStartDate.getTime() + 1000);

        String courtMovementReturnTime = movementActivityServiceAdapter.getCourtMovementReturnTime(uc);

        String[] time = courtMovementReturnTime.split(":", 2);

        Date inActivityPlanStartDate = BeanHelper.createDateWithTime(outActivityPlanEndDate, Integer.parseInt(time[0]), Integer.parseInt(time[1]), 0);
        Date inActivityPlanEndDate = new Date(inActivityPlanStartDate.getTime() + 1000);

        ActivityType outActivity = activityServiceAdapter.generateActivity(uc, outActivityPlanStartDate, outActivityPlanEndDate);
        outActivity.setSupervisionId(supervisionId);
        outActivity = activityServiceAdapter.createActivity(uc, outActivity, null, true);

        ActivityType inActivity = activityServiceAdapter.generateActivity(uc, inActivityPlanStartDate, inActivityPlanEndDate);
        inActivity.setActivityAntecedentId(outActivity.getActivityIdentification());
        inActivity.setSupervisionId(supervisionId);

        inActivity = activityServiceAdapter.createActivity(uc, inActivity, null, true);

        String groupId = UUID.randomUUID().toString();

        ExternalMovementActivityType movementOut = movementActivityServiceAdapter.generateMovement(uc, supervisionId, toFacilityOut, fromFacilityOut,
                MovementDirection.OUT.code(), courtActivityMovement.getStatus(), courtActivityMovement.getMovementReason(), courtActivityMovement.getCommment(),
                outActivity.getActivityIdentification());
        movementOut.setGroupId(groupId);
        movementOut = movementActivityServiceAdapter.createExtMovementActivity(uc, movementOut, false);

        activityServiceAdapter.createAssociation(uc, outActivity.getActivityIdentification(),
                new AssociationType("MovementService", movementOut.getMovementId()));

        ExternalMovementActivityType movementIn = movementActivityServiceAdapter.generateMovement(uc, supervisionId, toFacilityIn, fromFacilityIn,
                MovementDirection.IN.code(), courtActivityMovement.getStatus(), courtActivityMovement.getMovementReason(), courtActivityMovement.getCommment(),
                inActivity.getActivityIdentification());
        movementIn.setGroupId(groupId);
        movementIn = movementActivityServiceAdapter.createExtMovementActivity(uc, movementIn, false);

        activityServiceAdapter.createAssociation(uc, inActivity.getActivityIdentification(),
                new AssociationType("MovementService", movementIn.getMovementId()));

        //create CourtActivity association to activities.
        courtActivityMovement.getCourtActivityType().getActivityIds().clear();
        courtActivityMovement.getCourtActivityType().getActivityIds().add(outActivity.getActivityIdentification());
        courtActivityMovement.getCourtActivityType().getActivityIds().add(inActivity.getActivityIdentification());

        courtActivityMovement.setMovementGroupId(groupId);

        return courtActivityMovement;
    }

    /**
     * Cancel(Delete) MovementsActivity and Activity Data for the given Activities' ids which are from a Case Activity.
     * Business rule: If any movement activity is completed, then all movement activities can not be cancelled.
     *
     * @param uc
     * @param activityIds
     * @return
     */
    protected boolean cancelMovementsData(UserContext uc, List<Long> activityIds) {
        boolean cancellable = true;

        if (activityIds != null && !activityIds.isEmpty()) {
            List<ExternalMovementActivityType> movementActivities = new ArrayList<ExternalMovementActivityType>();

            for (Long activityId : activityIds) {
                ExternalMovementActivityType externalMovement = movementActivityServiceAdapter.getCourtMovementByActivityId(uc, activityId);
                movementActivities.add(externalMovement);
                if (externalMovement.getMovementStatus().equals(MOVEMENT_COMPLETED)) {
                    cancellable = false; //Business rule: If any movement activity is completed, then all movement activities can not be cancelled.
                    break;
                }
            }

            if (cancellable) {
                for (ExternalMovementActivityType externalMovement : movementActivities) {
                    movementActivityServiceAdapter.deleteMovementActivity(uc, externalMovement.getMovementId());

                    Long activityId = externalMovement.getActivityId();
                    ActivityType activityType = activityServiceAdapter.getActivity(uc, activityId);
                    activityType.setActivityDescendantIds(null);
                    activityServiceAdapter.updateActivity(uc, activityType);
                    activityServiceAdapter.deleteActivity(uc, activityId);
                }
            }
        }

        return cancellable;
    }

    private CourtActivityMovementType generateCourtActivityMovementType(UserContext uc, CourtActivityType courtActivityType) {
        CourtActivityMovementType courtActivityMovement = new CourtActivityMovementType();

        courtActivityMovement.setCourtActivityType(courtActivityType);

        Set<Long> activityIds = courtActivityMovement.getCourtActivityType().getActivityIds();

        courtActivityMovement = retrieveMovementData(uc, courtActivityMovement, activityIds);

        return courtActivityMovement;
    }

    /**
     * Get Activity Ids by CaseActivity id.
     *
     * @param caseActivityId
     * @return
     */
    @SuppressWarnings("unchecked")
    protected List<Long> getActivityIds(Long caseActivityId) {
        Criteria c = session.createCriteria(CAActivityIdEntity.class);
        c.add(Restrictions.eq("caseActivity.caseActivityId", caseActivityId));
        c.setProjection(Projections.property("activityId"));
        c.addOrder(Order.desc("activityId"));

        List<Long> activityIds = c.list();

        return activityIds;
    }

    private void facilityFilter(Session session, Criteria criteria, Set<Long> facilityIds) {
        if (BeanHelper.isEmpty(facilityIds)) {
            criteria.setFetchSize(0);
            criteria.setMaxResults(0);
            criteria.add(Restrictions.idEq(Long.valueOf(-1)));
        } else {
            Set<Set<Long>> fs = BeanHelper.splitSet(facilityIds);
            if (fs.isEmpty() || ((Set<?>) (fs.toArray()[0])).isEmpty()) {
                criteria.add(Restrictions.idEq(Long.valueOf(-1)));
            } else {
                List<Long> supervisionIds = getSupervisionIds(session, fs);
                Set<Set<Long>> ss = BeanHelper.splitSet(supervisionIds);
                if (ss.isEmpty() || ((Set<?>) (ss.toArray()[0])).isEmpty()) {
                    criteria.add(Restrictions.idEq(Long.valueOf(-1)));
                } else {
                    Disjunction disjunction = Restrictions.disjunction();
                    for (Set<Long> s : ss) {
                        disjunction.add(Restrictions.in("caseInfo.supervisionId", s));
                    }
                    criteria.add(disjunction);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    private List<Long> getSupervisionIds(Session session, Set<Set<Long>> fs) {
        Criteria criteria = session.createCriteria(SupervisionEntity.class);

        Calendar cal = Calendar.getInstance();
		/*cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);*/
        Date today = cal.getTime();

        criteria.add(Restrictions.le("supervisionStartDate", today));        

        Disjunction disjunction = Restrictions.disjunction();
        for (Set<Long> s : fs) {
            disjunction.add(Restrictions.in("facilityId", s));
        }
        criteria.add(disjunction);
        criteria.setProjection(Projections.id());
        return criteria.list();
    }

    public SentenceTermType createSentenceTerm(UserContext uc, SentenceTermType sentenceTermType) {
        String functionName = "createSentenceTerm";
        ValidationHelper.validate(sentenceTermType);
        String message = String.format("SentenceTermType=%s.", sentenceTermType);
        LogHelper.debug(log, functionName, message);

        // Create stamp
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        createStamp.setModifyDateTime(null);
        createStamp.setModifyUserId(null);

        SentenceTermEntity entity = OrderSentenceHelper.toSentenceTermEntity(sentenceTermType);
        ValidationHelper.verifyMetaCodes(entity, true);

        OrderSentenceHelper.updateSetenceTermStamp(entity, createStamp, null);

        if (!entity.getSentenceTermNameEntityList().isEmpty() && entity.getSentenceTermNameEntityList().size() > 0) {
            OrderSentenceHelper.updateSetenceTermNameStamp(entity.getSentenceTermNameEntityList(), createStamp, null);
        }

        SentenceTermEntity storedEntity = getSentenceTermUniqueEntity(sentenceTermType);
        if (storedEntity == null) {
            session.save(entity);
            session.flush();
            sentenceTermType = OrderSentenceHelper.toSentenceTermType(entity);
        } else {
            sentenceTermType = OrderSentenceHelper.toSentenceTermType(entity);
        }
        return sentenceTermType;
    }

    private void copyEntity(SentenceTermEntity newEntity, SentenceTermEntity oldEntity) {
        oldEntity.setSentenceType(newEntity.getSentenceType());
        oldEntity.setTermType(newEntity.getTermType());
        //oldEntity.setSentenceTermNameEntityList(oldEntity.getSentenceTermNameEntityList());
        oldEntity.setDeactivationDate(newEntity.getDeactivationDate());
    }

    public SentenceTermType updateSentenceTerm(UserContext uc, SentenceTermType sentenceTermType) {
        String functionName = "updateSentenceTerm";
        ValidationHelper.validate(sentenceTermType);
        String message = String.format("SentenceTermType=%s.", sentenceTermType);
        LogHelper.debug(log, functionName, message);

        SentenceTermEntity existingEntity = getSentenceTermEntity(sentenceTermType.getSentenceTermId());
        if (existingEntity == null) {
            message = String.format("SentenceTermEntity does not exist. " + existingEntity);
            LogHelper.error(log, "updateSTGIdentifier", message);
            throw new DataNotExistException(message);
        }

        SentenceTermEntity newEntity = OrderSentenceHelper.toSentenceTermEntity(sentenceTermType);

        ValidationHelper.verifyMetaCodes(newEntity, true);

        copyEntity(newEntity, existingEntity);

        // Update stamp
        StampEntity modifiedStamp = BeanHelper.getModifyStamp(uc, context, existingEntity.getStamp());
        OrderSentenceHelper.updateSetenceTermStamp(existingEntity, null, modifiedStamp);

        if (!existingEntity.getSentenceTermNameEntityList().isEmpty() && existingEntity.getSentenceTermNameEntityList().size() > 0) {
            OrderSentenceHelper.updateSetenceTermNameStamp(existingEntity.getSentenceTermNameEntityList(), null, modifiedStamp);
        }

        session.update(existingEntity);
        session.flush();
        sentenceTermType = OrderSentenceHelper.toSentenceTermType(existingEntity);
        return sentenceTermType;

    }

    public SentenceTermType getSentenceTerm(UserContext uc, Long sentenceTermId) {
        SentenceTermEntity entity = getSentenceTermEntity(sentenceTermId);
        if (null != entity.getSentenceTermNameEntityList()) {
            entity.setSentenceTermNameEntityList(entity.getSentenceTermNameEntityList());
        }

        SentenceTermType sentenceTermType = OrderSentenceHelper.toSentenceTermType(entity);
        return sentenceTermType;
    }

    public List<SentenceTermType> getAllSentenceTerm(UserContext uc, Long startIndex, Long resultSize, String resultOrder) {
        List<SentenceTermType> sentenceTermTypeList;

        Criteria c = session.createCriteria(SentenceTermEntity.class);
        c.setMaxResults(searchMaxLimit + 1);
        c.setFetchSize(searchMaxLimit + 1);
        c.setLockMode(LockMode.NONE);
        c.setCacheMode(CacheMode.IGNORE);

        @SuppressWarnings("unchecked") List<SentenceTermEntity> searchresult = c.list();

        sentenceTermTypeList = OrderSentenceHelper.toSentenceTermTypeList(searchresult);
        return sentenceTermTypeList;
    }

    public List<SentenceTermType> searchSentenceTerm(UserContext uc, SearchSentenceTermType searchType, Long startIndex, Long resultSize, String resultOrder) {
        List<SentenceTermType> sentenceTermTypeList;

        Criteria c = session.createCriteria(SentenceTermEntity.class);
        c.setMaxResults(searchMaxLimit + 1);
        c.setFetchSize(searchMaxLimit + 1);
        c.setLockMode(LockMode.NONE);
        c.setCacheMode(CacheMode.IGNORE);

        if (null != searchType.getSentenceType()) {
            c.add(Restrictions.eq("sentenceType", searchType.getSentenceType()));
        }
        if (null != searchType.getTermType()) {
            c.add(Restrictions.eq("termType", searchType.getTermType()));
        }
        if (null != searchType.getTermName()) {
            c.add(Restrictions.eq("termName", searchType.getTermName()));
        }
        if (null != searchType.getDeactivationDate()) {
            c.add(Restrictions.or(Restrictions.isNull("deactivationDate"), Restrictions.gt("deactivationDate", searchType.getDeactivationDate())));

        }

        c.addOrder(Order.asc("sentenceTermId"));

        @SuppressWarnings("unchecked") List<SentenceTermEntity> searchresult = c.list();

        sentenceTermTypeList = OrderSentenceHelper.toSentenceTermTypeList(searchresult);
        return sentenceTermTypeList;

    }

    public SentenceTermEntity getSentenceTermEntity(Long id) {

        if (BeanHelper.isEmpty(id)) {
            throw new InvalidInputException("SentenceTermEntity Id is null or empty.");
        }

        SentenceTermEntity entity = (SentenceTermEntity) session.get(SentenceTermEntity.class, id);

        if (entity == null) {
            String message = String.format("Could not find the SentenceTermEntity by Id = %d.", id);
            throw new DataNotExistException(message);
        }
        return entity;
    }

    public SentenceTermNameType createSentenceTermName(UserContext uc, SentenceTermNameType sentenceTermTypeName) {
        String functionName = "createSentenceTerm";
        ValidationHelper.validate(sentenceTermTypeName);
        String message = String.format("SentenceTermType=%s.", sentenceTermTypeName);
        LogHelper.debug(log, functionName, message);

        // Create stamp
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        createStamp.setModifyDateTime(null);
        createStamp.setModifyUserId(null);

        Criteria sentenceTermCriteria = session.createCriteria(SentenceTermEntity.class);
        sentenceTermCriteria.add(Restrictions.eq("sentenceType", sentenceTermTypeName.getSentenceType()));
        sentenceTermCriteria.setFetchMode("sentenceTermNameEntity", FetchMode.JOIN);
        @SuppressWarnings("unchecked") List<SentenceTermEntity> sentenceEntities = sentenceTermCriteria.list();
        List<SentenceTermNameEntity> sentenceTermNameEntities = null;
        SentenceTermEntity sentenceTermEntity = null;
        if (sentenceEntities != null && sentenceEntities.size() > 0) {
            sentenceTermEntity = sentenceEntities.get(0);
            sentenceTermNameEntities = sentenceTermEntity.getSentenceTermNameEntityList();
            if (sentenceTermNameEntities != null && sentenceTermNameEntities.size() > 0) {
                return OrderSentenceHelper.toSentenceTermNameType(sentenceTermNameEntities.get(0));
            }
        }

        SentenceTermNameEntity entityName = OrderSentenceHelper.toSentenceTermNameEntity(sentenceTermTypeName);
        entityName.setSentenceTermEntity(sentenceTermEntity);
        ValidationHelper.verifyMetaCodes(entityName, true);
        session.save(entityName);
        session.flush();
        sentenceTermTypeName = OrderSentenceHelper.toSentenceTermNameType(entityName);

        return sentenceTermTypeName;
    }

    private SentenceTermEntity getSentenceTermUniqueEntity(SentenceTermType sentenceTermType) {
        Criteria c = session.createCriteria(SentenceTermEntity.class);
        c.add(Restrictions.ilike("sentenceType", sentenceTermType.getSentenceType()));
        c.setMaxResults(1);

        return (SentenceTermEntity) c.uniqueResult();
    }

}
