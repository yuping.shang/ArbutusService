/**
 *
 */
package syscon.arbutus.product.services.legal.contract.dto.composite;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import syscon.arbutus.product.services.core.contract.dto.ServiceConstants;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivityType;

/**
 * The representation of the court activity and it's movement information.
 *
 * @author lhan
 */
public class CourtActivityMovementType implements Serializable {
    private static final long serialVersionUID = 5404182608697851001L;

    @NotNull
    @Valid
    private CourtActivityType courtActivityType;
    @NotNull
    private Long supervisionId;
    @NotNull
    private String status;
    @Size(max = ServiceConstants.TEXT_LENGTH_LARGE, message = "Cannot exceed 512 characters")
    private String commment;
    @NotNull
    private String movementReason;

    private String movementGroupId;

    /**
     * Default Constructor
     */
    public CourtActivityMovementType() {
        super();
    }

    /**
     * Constructor
     *
     * @param courtActivityType CourtActivityType - The court activity type which triggers this movement. required.
     * @param supervisionId     Long - The offender supervision id. required.
     * @param status            String - Them movement status, a code from MovementStatus reference code set. required
     * @param commment          String - The comment for the movement. optional
     * @param movementReason    String - The movement reason, a code from MovementReason reference code set. required
     */
    public CourtActivityMovementType(CourtActivityType courtActivityType, Long supervisionId, String status, String commment, String movementReason) {
        super();
        this.courtActivityType = courtActivityType;
        this.supervisionId = supervisionId;
        this.status = status;
        this.commment = commment;
        this.movementReason = movementReason;
    }

    /**
     * The group id of movement activity type for this case activity.
     *
     * @return the movementGroupId
     */
    public String getMovementGroupId() {
        return movementGroupId;
    }

    /**
     * The group id of movement activity type for this case activity.
     *
     * @param movementGroupId the movementGroupId to set
     */
    public void setMovementGroupId(String movementGroupId) {
        this.movementGroupId = movementGroupId;
    }

    /**
     * The offender supervision id. required.
     *
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * The offender supervision id. required.
     *
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Them movement status, a code from MovementStatus reference code set. required
     *
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Them movement status, a code from MovementStatus reference code set. required
     *
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * The comment for the movement. optional
     *
     * @return the commment
     */
    public String getCommment() {
        return commment;
    }

    /**
     * The comment for the movement. optional
     *
     * @param commment the commment to set
     */
    public void setCommment(String commment) {
        this.commment = commment;
    }

    /**
     * The movement reason, a code from MovementReason reference code set. required
     *
     * @return the movementReason
     */
    public String getMovementReason() {
        return movementReason;
    }

    /**
     * The movement reason, a code from MovementReason reference code set. required
     *
     * @param movementReason the movementReason to set
     */
    public void setMovementReason(String movementReason) {
        this.movementReason = movementReason;
    }

    /**
     * The court activity type which triggers this movement. required.
     *
     * @return the courtActivityType
     */
    public CourtActivityType getCourtActivityType() {
        return courtActivityType;
    }

    /**
     * The court activity type which triggers this movement. required.
     *
     * @param courtActivityType the courtActivityType to set
     */
    public void setCourtActivityType(CourtActivityType courtActivityType) {
        this.courtActivityType = courtActivityType;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CourtActivityMovementType [courtActivityType=" + courtActivityType + ", supervisionId=" + supervisionId + ", status=" + status + ", commment=" + commment
                + ", movementGroupId=" + movementGroupId + ", movementReason=" + movementReason + "]";
    }

}
