package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.StampType;

/**
 * Notification Type for OrderSentence Service
 *
 * @author yshang
 * @version 1.0
 * @since December 20, 2012
 */
public class NotificationType implements Serializable {

    private static final long serialVersionUID = -8389682263688158896L;
    private Long notificationId;
    private Long notificationOrganizationAssociation;
    private Long notificationFacilityAssociation;
    @NotNull
    private Boolean isNotificationNeeded;
    @NotNull
    private Boolean isNotificationConfirmed;
    private Set<Long> notificationAgencyLocationAssociations;
    private Set<ContactType> notificationAgencyContacts;
    // @Size(max = 128, message = "max length 128")
    private StampType stamp;

    /**
     * Constructor
     */
    public NotificationType() {
        super();
    }

    /**
     * Constructor
     *
     * @param notificationOrganizationAssociation    Long, Optional. The entity (organization) to be notified. Static reference to organization.
     * @param notificationFacilityAssociation        Long, Optional. The entity (facility) to be notified. Static reference to facility.
     * @param isNotificationNeeded                   Boolean, Required. If true then notification is required to be sent before releasing the offender, false otherwise.
     * @param isNotificationConfirmed                Required. If true, then the notification confirmation was received, false otherwise. Default is false.
     * @param notificationAgencyLocationAssociations Set&lt;Long>, Optional. The address/phone details of the agency (organization/facility) to be notiifed. Static reference to location.
     * @param notificationAgencyContacts             Set&lt;ContactType>(where String length <= 128), Optional. The details (name) of the person to be notified.
     */
    public NotificationType(Long notificationOrganizationAssociation, Long notificationFacilityAssociation, @NotNull Boolean isNotificationNeeded,
            @NotNull Boolean isNotificationConfirmed, @Size(max = 128, message = "max length 128") Set<Long> notificationAgencyLocationAssociations,
            Set<ContactType> notificationAgencyContacts) {
        super();
        this.notificationOrganizationAssociation = notificationOrganizationAssociation;
        this.notificationFacilityAssociation = notificationFacilityAssociation;
        this.isNotificationNeeded = isNotificationNeeded;
        this.isNotificationConfirmed = isNotificationConfirmed;
        this.notificationAgencyLocationAssociations = notificationAgencyLocationAssociations;
        this.notificationAgencyContacts = notificationAgencyContacts;
    }

    /**
     * Notification Id
     *
     * @return
     */
    public Long getNotificationId() {
        return notificationId;
    }

    /**
     * Notification Id
     *
     * @param notificationId
     */
    public void setNotificationId(Long notificationId) {
        this.notificationId = notificationId;
    }

    /**
     * Get Notification Organization Association
     * <p>The entity (organization) to be notified. Static reference to organization.
     *
     * @return the notificationOrganizationAssociation
     */
    public Long getNotificationOrganizationAssociation() {
        return notificationOrganizationAssociation;
    }

    /**
     * Set Notification Organization Association
     * <p>The entity (organization) to be notified. Static reference to organization.
     *
     * @param notificationOrganizationAssociation the notificationOrganizationAssociation to set
     */
    public void setNotificationOrganizationAssociation(Long notificationOrganizationAssociation) {
        this.notificationOrganizationAssociation = notificationOrganizationAssociation;
    }

    /**
     * Get Notification Facility Association
     * <p>The entity (facility) to be notified. Static reference to facility.
     *
     * @return the notificationFacilityAssociation
     */
    public Long getNotificationFacilityAssociation() {
        return notificationFacilityAssociation;
    }

    /**
     * Set Notification Facility Association
     * <p>The entity (facility) to be notified. Static reference to facility.
     *
     * @param notificationFacilityAssociation the notificationFacilityAssociation to set
     */
    public void setNotificationFacilityAssociation(Long notificationFacilityAssociation) {
        this.notificationFacilityAssociation = notificationFacilityAssociation;
    }

    /**
     * Is Notification Needed
     * <p>If true then notification is required to be sent before releasing the offender, false otherwise.
     *
     * @return the isNotificationNeeded
     */
    public Boolean getIsNotificationNeeded() {
        return isNotificationNeeded;
    }

    /**
     * Is Notification Needed
     * <p>If true then notification is required to be sent before releasing the offender, false otherwise.
     *
     * @param isNotificationNeeded the isNotificationNeeded to set
     */
    public void setIsNotificationNeeded(Boolean isNotificationNeeded) {
        this.isNotificationNeeded = isNotificationNeeded;
    }

    /**
     * Is Notification Confirmed
     * <p>If true, then the notification confirmation was received, false otherwise. Default is false.
     *
     * @return the isNotificationConfirmed
     */
    public Boolean getIsNotificationConfirmed() {
        return isNotificationConfirmed;
    }

    /**
     * Is Notification Confirmed
     * <p>If true, then the notification confirmation was received, false otherwise. Default is false.
     *
     * @param isNotificationConfirmed the isNotificationConfirmed to set
     */
    public void setIsNotificationConfirmed(Boolean isNotificationConfirmed) {
        this.isNotificationConfirmed = isNotificationConfirmed;
    }

    /**
     * Get Notification Agency Location Associations
     * <p>The address/phone details of the agency (organization/facility) to be notified. Static reference to location.
     *
     * @return the notificationAgencyLocationAssociations
     */
    public Set<Long> getNotificationAgencyLocationAssociations() {
        return notificationAgencyLocationAssociations;
    }

    /**
     * Get Notification Agency Location Associations
     * <p>The address/phone details of the agency (organization/facility) to be notified. Static reference to location.
     *
     * @param notificationAgencyLocationAssociations the notificationAgencyLocationAssociations to set
     */
    public void setNotificationAgencyLocationAssociations(Set<Long> notificationAgencyLocationAssociations) {
        this.notificationAgencyLocationAssociations = notificationAgencyLocationAssociations;
    }

    /**
     * Get Notification Agency Contacts
     * <p>The details (name) of the person to be notified.
     *
     * @return the notificationAgencyContacts
     */
    public Set<ContactType> getNotificationAgencyContacts() {
        return notificationAgencyContacts;
    }

    /**
     * Set Notification Agency Contacts
     * <p>The details (name) of the person to be notified.
     *
     * @param notificationAgencyContacts the notificationAgencyContacts to set
     */
    public void setNotificationAgencyContacts(Set<ContactType> notificationAgencyContacts) {
        this.notificationAgencyContacts = notificationAgencyContacts;
    }

    /**
     * Get Time stamp
     *
     * @return the stamp
     */
    public StampType getStamp() {
        return stamp;
    }

    /**
     * Set Time stamp
     *
     * @param stamp the stamp to set
     */
    public void setStamp(StampType stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((isNotificationConfirmed == null) ? 0 : isNotificationConfirmed.hashCode());
        result = prime * result + ((isNotificationNeeded == null) ? 0 : isNotificationNeeded.hashCode());
        result = prime * result + ((notificationAgencyLocationAssociations == null) ? 0 : notificationAgencyLocationAssociations.hashCode());
        result = prime * result + ((notificationFacilityAssociation == null) ? 0 : notificationFacilityAssociation.hashCode());
        result = prime * result + ((notificationOrganizationAssociation == null) ? 0 : notificationOrganizationAssociation.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        NotificationType other = (NotificationType) obj;
        if (isNotificationConfirmed == null) {
            if (other.isNotificationConfirmed != null) {
				return false;
			}
        } else if (!isNotificationConfirmed.equals(other.isNotificationConfirmed)) {
			return false;
		}
        if (isNotificationNeeded == null) {
            if (other.isNotificationNeeded != null) {
				return false;
			}
        } else if (!isNotificationNeeded.equals(other.isNotificationNeeded)) {
			return false;
		}
        if (notificationAgencyLocationAssociations == null) {
            if (other.notificationAgencyLocationAssociations != null) {
				return false;
			}
        } else if (!notificationAgencyLocationAssociations.equals(other.notificationAgencyLocationAssociations)) {
			return false;
		}
        if (notificationFacilityAssociation == null) {
            if (other.notificationFacilityAssociation != null) {
				return false;
			}
        } else if (!notificationFacilityAssociation.equals(other.notificationFacilityAssociation)) {
			return false;
		}
        if (notificationOrganizationAssociation == null) {
            if (other.notificationOrganizationAssociation != null) {
				return false;
			}
        } else if (!notificationOrganizationAssociation.equals(other.notificationOrganizationAssociation)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "NotificationType{" +
                "notificationId=" + notificationId +
                ", notificationOrganizationAssociation=" + notificationOrganizationAssociation +
                ", notificationFacilityAssociation=" + notificationFacilityAssociation +
                ", isNotificationNeeded=" + isNotificationNeeded +
                ", isNotificationConfirmed=" + isNotificationConfirmed +
                ", notificationAgencyLocationAssociations=" + notificationAgencyLocationAssociations +
                ", notificationAgencyContacts=" + notificationAgencyContacts +
                ", stamp=" + stamp +
                '}';
    }

    public enum AssociationToClass {
        LocationService;

        public static AssociationToClass fromValue(String v) {
            return valueOf(v);
        }

        public String value() {
            return name();
        }
    }
}
