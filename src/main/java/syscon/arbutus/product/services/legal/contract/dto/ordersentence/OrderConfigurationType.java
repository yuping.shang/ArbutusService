package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * OrderConfigurationType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 31, 2012
 */
public class OrderConfigurationType implements Serializable {

    private static final long serialVersionUID = -152939000916820500L;

    private Long orderConfigId;

    @NotNull
    private String orderClassification;
    @NotNull
    private String orderType;
    @NotNull
    private String orderCategory;
    @NotNull
    private Boolean isHoldingOrder;
    @Valid
    private Set<NotificationType> issuingAgencies;
    @NotNull
    private Boolean isSchedulingNeeded;
    @NotNull
    private Boolean hasCharges;
    @NotNull
    private Date startDate;
    private Date endDate;

    /**
     * Constructor
     */
    public OrderConfigurationType() {
        super();
    }

    /**
     * Constructor
     *
     * @param orderClassification String, Required. The order classification (WarrantDetainer, LegalOrder, Sentence). This defines the type of instance of the service.
     * @param orderType           String, Required. The client defined order type that maps to the order classification.
     * @param orderCategory       String, Required. The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.
     * @param isHoldingOrder      Boolean, Required. If true then the order is a holding document, false otherwise.
     * @param issuingAgencies     Set&lt;NotificationType>, Optional. The source agency that issues the order, this could be a facility or an organization.
     * @param isSchedulingNeeded  Boolean, Required. If true, scheduling is needed for the order, false otherwise.
     * @param hasCharges          Boolean, Required. If true, the order has charges related, false otherwise.
     */
    public OrderConfigurationType(@NotNull String orderClassification, @NotNull String orderType, @NotNull String orderCategory, @NotNull Boolean isHoldingOrder,
            Set<NotificationType> issuingAgencies, @NotNull Boolean isSchedulingNeeded, @NotNull Boolean hasCharges) {
        super();
        this.orderClassification = orderClassification;
        this.orderType = orderType;
        this.orderCategory = orderCategory;
        this.isHoldingOrder = isHoldingOrder;
        this.issuingAgencies = issuingAgencies;
        this.isSchedulingNeeded = isSchedulingNeeded;
        this.hasCharges = hasCharges;

    }

    /**
     * Constructor
     *
     * @param orderConfigId       Long, Not Required for create or set; required for update and delete.
     * @param orderClassification String, Required. The order classification (WarrantDetainer, LegalOrder, Sentence). This defines the type of instance of the service.
     * @param orderType           String, Required. The client defined order type that maps to the order classification.
     * @param orderCategory       String, Required. The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.
     * @param isHoldingOrder      Boolean, Required. If true then the order is a holding document, false otherwise.
     * @param issuingAgencies     Set&lt;NotificationType>, Optional. The source agency that issues the order, this could be a facility or an organization.
     * @param isSchedulingNeeded  Boolean, Required. If true, scheduling is needed for the order, false otherwise.
     * @param hasCharges          Boolean, Required. If true, the order has charges related, false otherwise.
     */
    public OrderConfigurationType(Long orderConfigId, @NotNull String orderClassification, @NotNull String orderType, @NotNull String orderCategory,
            @NotNull Boolean isHoldingOrder, Set<NotificationType> issuingAgencies, @NotNull Boolean isSchedulingNeeded, @NotNull Boolean hasCharges) {
        super();
        this.orderConfigId = orderConfigId;
        this.orderClassification = orderClassification;
        this.orderType = orderType;
        this.orderCategory = orderCategory;
        this.isHoldingOrder = isHoldingOrder;
        this.issuingAgencies = issuingAgencies;
        this.isSchedulingNeeded = isSchedulingNeeded;
        this.hasCharges = hasCharges;

    }

    /**
     * Constructor
     *
     * @param orderClassification String, Required. The order classification (WarrantDetainer, LegalOrder, Sentence). This defines the type of instance of the service.
     * @param orderType           String, Required. The client defined order type that maps to the order classification.
     * @param orderCategory       String, Required. The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.
     * @param isHoldingOrder      Boolean, Required. If true then the order is a holding document, false otherwise.
     * @param issuingAgencies     Set&lt;NotificationType>, Optional. The source agency that issues the order, this could be a facility or an organization.
     * @param isSchedulingNeeded  Boolean, Required. If true, scheduling is needed for the order, false otherwise.
     * @param hasCharges          Boolean, Required. If true, the order has charges related, false otherwise.
     * @param startDate           Date , Required. When Order configuration Record is created.
     * @param endDate             Required . when order configuration  need to deactivate.
     */
    public OrderConfigurationType(@NotNull String orderClassification, @NotNull String orderType, @NotNull String orderCategory, @NotNull Boolean isHoldingOrder,
            Set<NotificationType> issuingAgencies, @NotNull Boolean isSchedulingNeeded, @NotNull Boolean hasCharges, @NotNull Date startDate, Date endDate) {
        super();
        this.orderClassification = orderClassification;
        this.orderType = orderType;
        this.orderCategory = orderCategory;
        this.isHoldingOrder = isHoldingOrder;
        this.issuingAgencies = issuingAgencies;
        this.isSchedulingNeeded = isSchedulingNeeded;
        this.hasCharges = hasCharges;
        this.startDate = startDate;
        this.endDate = endDate;

    }

    /**
     * Constructor
     *
     * @param orderClassification String, Required. The order classification (WarrantDetainer, LegalOrder, Sentence). This defines the type of instance of the service.
     * @param orderType           String, Required. The client defined order type that maps to the order classification.
     * @param orderCategory       String, Required. The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.
     * @param isHoldingOrder      Boolean, Required. If true then the order is a holding document, false otherwise.
     * @param issuingAgencies     Set&lt;NotificationType>, Optional. The source agency that issues the order, this could be a facility or an organization.
     * @param isSchedulingNeeded  Boolean, Required. If true, scheduling is needed for the order, false otherwise.
     * @param hasCharges          Boolean, Required. If true, the order has charges related, false otherwise.
     * @param startDate           Date , Required. When Order configuration Record is created.
     * @param endDate             Required . when order configuration  need to deactivate.
     */
    public OrderConfigurationType(Long orderConfigId, @NotNull String orderClassification, @NotNull String orderType, @NotNull String orderCategory,
            @NotNull Boolean isHoldingOrder, Set<NotificationType> issuingAgencies, @NotNull Boolean isSchedulingNeeded, @NotNull Boolean hasCharges,
            @NotNull Date startDate, Date endDate) {
        super();
        this.orderConfigId = orderConfigId;
        this.orderClassification = orderClassification;
        this.orderType = orderType;
        this.orderCategory = orderCategory;
        this.isHoldingOrder = isHoldingOrder;
        this.issuingAgencies = issuingAgencies;
        this.isSchedulingNeeded = isSchedulingNeeded;
        this.hasCharges = hasCharges;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /**
     * orderConfigId
     * <p>Identification of the order configuration
     * <p>Not Required for create or set; required for update and delete.
     *
     * @return the orderConfigId
     */
    public Long getOrderConfigId() {
        return orderConfigId;
    }

    /**
     * orderConfigId
     * <p>Identification of the order configuration
     * <p>Not Required for create or set; required for update and delete.
     *
     * @param orderConfigId the orderConfigId to set
     */
    public void setOrderConfigId(Long orderConfigId) {
        this.orderConfigId = orderConfigId;
    }

    /**
     * The order classification (WarrantDetainer, LegalOrder, Sentence). This defines the type of instance of the service.
     *
     * @return the orderClassification
     */
    public String getOrderClassification() {
        return orderClassification;
    }

    /**
     * The order classification (WarrantDetainer, LegalOrder, Sentence). This defines the type of instance of the service.
     *
     * @param orderClassification the orderClassification to set
     */
    public void setOrderClassification(String orderClassification) {
        this.orderClassification = orderClassification;
    }

    /**
     * The client defined order type that maps to the order classification.
     *
     * @return the orderType
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * The client defined order type that maps to the order classification.
     *
     * @param orderType the orderType to set
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    /**
     * The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.
     *
     * @return the orderCategory
     */
    public String getOrderCategory() {
        return orderCategory;
    }

    /**
     * The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.
     *
     * @param orderCategory the orderCategory to set
     */
    public void setOrderCategory(String orderCategory) {
        this.orderCategory = orderCategory;
    }

    /**
     * If true then the order is a holding document, false otherwise.
     *
     * @return the isHoldingOrder
     */
    public Boolean getIsHoldingOrder() {
        return isHoldingOrder;
    }

    /**
     * If true then the order is a holding document, false otherwise.
     *
     * @param isHoldingOrder the isHoldingOrder to set
     */
    public void setIsHoldingOrder(Boolean isHoldingOrder) {
        this.isHoldingOrder = isHoldingOrder;
    }

    /**
     * The source agency that issues the order, this could be a facility or an organization.
     *
     * @return the issuingAgencies
     */
    public Set<NotificationType> getIssuingAgencies() {
        return issuingAgencies;
    }

    /**
     * The source agency that issues the order, this could be a facility or an organization.
     *
     * @param issuingAgencies the issuingAgencies to set
     */
    public void setIssuingAgencies(Set<NotificationType> issuingAgencies) {
        this.issuingAgencies = issuingAgencies;
    }

    /**
     * If true, scheduling is needed for the order, false otherwise.
     *
     * @return the isSchedulingNeeded
     */
    public Boolean getIsSchedulingNeeded() {
        return isSchedulingNeeded;
    }

    /**
     * If true, scheduling is needed for the order, false otherwise.
     *
     * @param isSchedulingNeeded the isSchedulingNeeded to set
     */
    public void setIsSchedulingNeeded(Boolean isSchedulingNeeded) {
        this.isSchedulingNeeded = isSchedulingNeeded;
    }

    /**
     * If true, the order has charges related, false otherwise.
     *
     * @return the hasCharges
     */
    public Boolean getHasCharges() {
        return hasCharges;
    }

    /**
     * If true, the order has charges related, false otherwise.
     *
     * @param hasCharges the hasCharges to set
     */
    public void setHasCharges(Boolean hasCharges) {
        this.hasCharges = hasCharges;
    }

    /**
     * Required. When Order configuration Record is created.
     *
     * @param startDate the startDate to get
     *                  not null
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Required. When Order configuration Record is created.
     *
     * @param startDate the startDate to set
     * @ not null
     */

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Required. When Order configuration Record is created with deactivation Date or reactivation Date.
     *
     * @param endDate the endDate to get
     */

    public Date getEndDate() {
        return endDate;
    }

    /**
     * Required. When Order configuration Record is created with deactivation Date or reactivation Date.
     *
     * @param endDate the endDate to set
     */

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((orderCategory == null) ? 0 : orderCategory.hashCode());
        result = prime * result + ((orderClassification == null) ? 0 : orderClassification.hashCode());
        result = prime * result + ((orderType == null) ? 0 : orderType.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        OrderConfigurationType other = (OrderConfigurationType) obj;
        if (orderCategory == null) {
            if (other.orderCategory != null) {
				return false;
			}
        } else if (!orderCategory.equals(other.orderCategory)) {
			return false;
		}
        if (orderClassification == null) {
            if (other.orderClassification != null) {
				return false;
			}
        } else if (!orderClassification.equals(other.orderClassification)) {
			return false;
		}
        if (orderType == null) {
            if (other.orderType != null) {
				return false;
			}
        } else if (!orderType.equals(other.orderType)) {
			return false;
		}
        if (startDate == null) {
            if (other.startDate != null) {
				return false;
			}
        } else if (!startDate.equals(other.startDate)) {
			return false;
		}
        if (endDate == null) {
            if (other.endDate != null) {
				return false;
			}
        } else if (!endDate.equals(other.endDate)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "OrderConfigurationType [orderConfigId=" + orderConfigId + ", orderClassification=" + orderClassification + ", orderType=" + orderType + ", orderCategory="
                + orderCategory + ", isHoldingOrder=" + isHoldingOrder + ", issuingAgencies=" + issuingAgencies + ", isSchedulingNeeded=" + isSchedulingNeeded
                + ", hasCharges=" + hasCharges + ", startDate=" + startDate + ", endDate=" + endDate + "]";
    }

}
