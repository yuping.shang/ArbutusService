package syscon.arbutus.product.services.legal.contract.dto.charge;

import java.io.Serializable;
import java.util.List;

/**
 * The representation of the statute configurations return type
 *
 * @author byu
 */
public class StatuteConfigsReturnType implements Serializable {

    private static final long serialVersionUID = -4168545524846673279L;

    private List<StatuteConfigType> statuteConfigs;
    private Long totalSize;

    /**
     * Gets the value of the statuteConfigs property.
     *
     * @return a list of the statute configuration instances
     */
    public List<StatuteConfigType> getStatuteConfigs() {
        return statuteConfigs;
    }

    /**
     * Sets the value of the statuteConfigs property.
     *
     * @param statuteConfigs a list of the statute configuration instances
     */
    public void setStatuteConfigs(List<StatuteConfigType> statuteConfigs) {
        this.statuteConfigs = statuteConfigs;
    }

    /**
     * Gets the value of the totalSize property.
     *
     * @return the total size of search result
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * Sets the value of the totalSize property.
     *
     * @param totalSize the total size of search result
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StatuteConfigsReturnType [statuteConfigs=" + statuteConfigs + ", totalSize=" + totalSize + "]";
    }

}
