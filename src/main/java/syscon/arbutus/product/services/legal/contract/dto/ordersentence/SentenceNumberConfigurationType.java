package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * SentenceNumberConfigurationType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 31, 2012
 */
public class SentenceNumberConfigurationType implements Serializable {

    private static final long serialVersionUID = 5516779131956569053L;

    @NotNull
    private Boolean sentenceNumberGenerationFlag;

    /**
     * Constructor
     */
    public SentenceNumberConfigurationType() {
        super();
    }

    /**
     * Constructor
     *
     * @param sentenceNumberGenerationFlag Boolean, Required. If “Y”, system auto generates the sentence number. If “N”, system does not auto generate the sentence number.
     */
    public SentenceNumberConfigurationType(@NotNull Boolean sentenceNumberGenerationFlag) {
        super();
        this.sentenceNumberGenerationFlag = sentenceNumberGenerationFlag;
    }

    /**
     * isWellFormed -- Check if the required argument is provided.
     * <p>sentenceNumberGenerationFlag is required.
     *
     * @return Boolean True: well formed; False: not well formed
     */
    @Deprecated
    public Boolean isWellFormed() {
        if (sentenceNumberGenerationFlag == null) {
			return false;
		}

        return true;
    }

    /**
     * isValid -- Check if the provided argument is valid.
     *
     * @return Boolean True: valid; False: invalid
     */
    @Deprecated
    public Boolean isValid() {
        if (sentenceNumberGenerationFlag == null) {
			return false;
		}

        return true;
    }

    /**
     * @return the sentenceNumberGenerationFlag
     */
    public Boolean getSentenceNumberGenerationFlag() {
        return sentenceNumberGenerationFlag;
    }

    /**
     * @param sentenceNumberGenerationFlag the sentenceNumberGenerationFlag to set
     */
    public void setSentenceNumberGenerationFlag(Boolean sentenceNumberGenerationFlag) {
        this.sentenceNumberGenerationFlag = sentenceNumberGenerationFlag;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sentenceNumberGenerationFlag == null) ? 0 : sentenceNumberGenerationFlag.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SentenceNumberConfigurationType other = (SentenceNumberConfigurationType) obj;
        if (sentenceNumberGenerationFlag == null) {
            if (other.sentenceNumberGenerationFlag != null) {
				return false;
			}
        } else if (!sentenceNumberGenerationFlag.equals(other.sentenceNumberGenerationFlag)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SentenceNumberConfigurationType [sentenceNumberGenerationFlag=" + sentenceNumberGenerationFlag + "]";
    }

}
