package syscon.arbutus.product.services.legal.contract.ejb;

import javax.ejb.SessionContext;
import java.math.BigDecimal;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.common.adapters.DataSecurityServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.datasecurity.contract.dto.DataSecurityRecordType;
import syscon.arbutus.product.services.datasecurity.contract.dto.ReferenceSet.DataSecurityRecordTypeEnum;
import syscon.arbutus.product.services.datasecurity.contract.dto.ReferenceSet.EntityTypeEnum;
import syscon.arbutus.product.services.datasecurity.contract.ejb.DataSecurityHelper;
import syscon.arbutus.product.services.legal.contract.dto.LegalModule;
import syscon.arbutus.product.services.legal.contract.dto.condition.*;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.CaseInfoEntity;
import syscon.arbutus.product.services.legal.realization.persistence.charge.ChargeEntity;
import syscon.arbutus.product.services.legal.realization.persistence.condition.*;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.OrderEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * This handler is provided for handling condition related functions.
 *
 * @author wmadruga, lhan
 */
public class ConditionHandler {

    private static Logger log = LoggerFactory.getLogger(ConditionHandler.class);
    private SessionContext context;
    private Session session;
    private int searchMaxLimit = 200;

    public ConditionHandler(SessionContext context, Session session) {
        super();
        this.context = context;
        this.session = session;
    }

    public ConditionHandler(SessionContext context, Session session, int searchMaxLimit) {
        super();
        this.context = context;
        this.session = session;
        this.searchMaxLimit = searchMaxLimit;
    }

    /**
     * Creates a condition in database
     *
     * @param uc
     * @param condition
     * @return ConditionReturnType
     */
    public ConditionType createCondition(UserContext uc, ConditionType condition) {

        String functionName = "createCondition";
        log.debug(functionName);

        ValidationHelper.validate(condition);

        ConditionType ret = new ConditionType();

        condition.setConditionCreateDate(new Date());

        // Validation on Module Associations
        //		if (hasModuleAssociation(condition)) {
        //			if(!isValidModuleAssociation(session, condition)) {
        //				associationErrorReturn(condition, functionName, ret);
        //				return ret;
        //			}
        //		}

        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);

        ConditionEntity entity = ConditionHelper.toConditionEntity(condition, createStamp);

        // Check if all reference codes are activated, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(entity, true);

        session.save(entity);

        Long conditionId = entity.getConditionId();

        // Create Module Links
        LegalHelper.createModuleLinks(uc, context, session, conditionId, LegalModule.CONDITION, entity.getModuleAssociations(), createStamp);

        // Flush changes to db
        session.flush();
        session.clear();

        entity = getCondition(conditionId);

        ret = ConditionHelper.toConditionType(entity);

        return ret;
    }

    /**
     * Retrieves a Condition, by its id, from database
     *
     * @param uc
     * @param conditionId
     * @return ConditionReturnType instance
     */
    public ConditionType getCondition(UserContext uc, Long conditionId) {

        String functionName = "getCondition";

        ConditionType ret = new ConditionType();

        // Validation
        if (conditionId == null) {
            String error = "Condition not found with ID = " + conditionId;
            LogHelper.error(log, functionName, error);
            throw new InvalidInputException(error);
        }

        ConditionEntity entity = getCondition(conditionId);

        ret = ConditionHelper.toConditionType(entity);

        return ret;

    }

    /**
     * Updates a condition in database
     *
     * @param uc
     * @param condition
     * @return ConditionReturnType
     */
    public ConditionType updateCondition(UserContext uc, ConditionType condition) {

        ConditionType ret = new ConditionType();

        // Validation

        if (condition == null || condition.getConditionId() == null) {
            throw new IllegalArgumentException("ConditionType or Condition Id is null");
        }

        ValidationHelper.validate(condition);

        // Validation on Module Associations
        //		if (hasModuleAssociation(condition)) {
        //			if(!isValidModuleAssociation(session, condition)) {
        //				associationErrorReturn(condition, functionName, ret);
        //				return ret;
        //			}
        //		}

        ConditionEntity entity = ConditionHelper.toConditionEntity(condition, null);

        // Check if all reference codes are activated, otherwise will throw InvalidInputException
        ValidationHelper.verifyMetaCodes(entity, false);

        Long conditionId = entity.getConditionId();

        ConditionEntity existEntity = getCondition(conditionId);

        if (needToUpdate(entity, existEntity)) {
            // Create the history entity

            ConditionHistoryEntity historyEntity = ConditionHelper.toConditionHistoryEntity(existEntity, BeanHelper.getModifyStamp(uc, context, existEntity.getStamp()));
            session.save(historyEntity);

            Set<ConditionModuleAssociationEntity> moduleAssociationToHistory = getOldModuleAssoc(entity.getModuleAssociations(), existEntity.getModuleAssociations());
            // needs update
            if (moduleAssociationToHistory.size() > 0) {
                Set<ConditionModuleAssociationHistEntity> moduleHistories = ConditionHelper.toConditionModuleAssocHistEntity(historyEntity, moduleAssociationToHistory,
                        BeanHelper.getModifyStamp(uc, context, existEntity.getStamp()));
                for (ConditionModuleAssociationHistEntity moduleAssocHistEnt : moduleHistories) {
                    session.save(moduleAssocHistEnt);
                }
            }

            LegalHelper.removeModuleLinks(uc, context, session, conditionId, LegalModule.CONDITION, existEntity.getModuleAssociations());
            StampEntity stamp = BeanHelper.getModifyStamp(uc, context, existEntity.getStamp());
            entity.setStamp(stamp);

            // Update existing entity
            ConditionHelper.updateExistConditionEntity(existEntity, entity);

            session.merge(existEntity);

            // Save only new comments
            Iterator<ConditionCommentEntity> iComment = entity.getConditionComments().iterator();
            Iterator<ConditionCommentEntity> iDbComment;
            while (iComment.hasNext()) {
                iDbComment = existEntity.getConditionComments().iterator();
                ConditionCommentEntity comment = (ConditionCommentEntity) iComment.next();
                if (!isExistingComment(iDbComment, comment)) {
                    comment.setStamp(BeanHelper.getCreateStamp(uc, context));
                    session.save(comment);
                }
            }

            LegalHelper.createModuleLinks(uc, context, session, conditionId, LegalModule.CONDITION, existEntity.getModuleAssociations(), stamp);
        }

        // Flush changes to db
        session.flush();
        session.clear();

        entity = getCondition(conditionId);

        ret = ConditionHelper.toConditionType(entity);

        return ret;

    }

    /**
     * Retrieve condition history.
     *
     * @param uc the uc
     * @param conditionId the condition id
     * @param fromHistoryDate the from history date
     * @param toHistoryDate the to history date
     * @return the list
     */
    public List<ConditionType> retrieveConditionHistory(UserContext uc, Long conditionId, Date fromHistoryDate, Date toHistoryDate) {

        String functionName = "retrieveConditionHistory";

        // Validation
        if (conditionId == null) {

            String message = String.format("ConditionId= %s.", conditionId);
            LogHelper.error(log, functionName, message);

            throw new InvalidInputException("Condistion id is null");
        }

        Set<ConditionType> conditionTypes = getHistory(uc, context, session, conditionId, fromHistoryDate, toHistoryDate);

        List<ConditionType> ret = new ArrayList<ConditionType>(conditionTypes);

        return ret;
    }

    /**
     * Search condition.
     *
     * @param uc the uc
     * @param search the search
     * @param historyFlag the history flag
     * @param chargeSetMode the charge set mode
     * @param caseInfoSetMode the case info set mode
     * @param startIndex the start index
     * @param resultSize the result size
     * @param resultOrder the result order
     * @return the conditions return type
     */
    public ConditionsReturnType searchCondition(UserContext uc, ConditionSearchType search, Boolean historyFlag, String chargeSetMode, String caseInfoSetMode,
            Long startIndex, Long resultSize, String resultOrder) {

        Set<ConditionType> ret = new HashSet<ConditionType>();

        // Validation
        ValidationHelper.validateSearchType(search);

        if (!BeanHelper.isEmpty(chargeSetMode) && search.getChargeIds() == null) {
            throw new InvalidInputException("chargeSetMode has value but search.getChargeIds() is null..");
        }

        if (!BeanHelper.isEmpty(caseInfoSetMode) && search.getCaseInfoIds() == null) {
            throw new InvalidInputException("caseInfoSetMode has value but search.getSentenceIds() is null.");
        }

        Criteria criteria = createConditionSearchCriteria(search, null, ConditionEntity.class, null);

        criteria = includeModuleAssociations(uc, search, chargeSetMode, caseInfoSetMode, criteria);

        //TODO: Search against history entities
        //Set<ConditionHistoryEntity> historyEntitySet = new HashSet<ConditionHistoryEntity>();
        //historyEntitySet = searchAgainstConditionHistory(null, search, historyFlag, chargeSetMode, caseInfoSetMode, historyEntitySet);

        // Max Limit reached.
        //if ((historyEntitySet.size() + conditionEntitySet.size()) >= (searchMaxLimit + 1)) {
        //    log.debug("Search result is over " + searchMaxLimit);
        // throw new ArbutusRuntimeException("Search result is over " + searchMaxLimit);
        // }

        //ret.addAll(ConditionHelper.toConditionTypeSet(conditionEntitySet, true, BeanHelper.getUserPrivileges(uc, context)));

        //if(historyEntitySet.size() > 0)
        //	ret.addAll(ConditionHelper.toConditionTypeSet(historyEntitySet, null, null, null, true, BeanHelper.getUserPrivileges(uc, context)));

        ConditionsReturnType rtn = new ConditionsReturnType();

        Long totalSize = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
        criteria.setProjection(null);
        criteria.setResultTransformer(Criteria.ROOT_ENTITY);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());

            if (resultSize == null || resultSize > searchMaxLimit) {
                criteria.setMaxResults(searchMaxLimit);
            } else {
                criteria.setMaxResults(resultSize.intValue());
            }
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                criteria.addOrder(ord);
            }
        }

        // Search and only return the predefined max number of records
        criteria.setLockMode(LockMode.NONE);
        criteria.setCacheMode(CacheMode.IGNORE);

        @SuppressWarnings("unchecked") List<ConditionEntity> list = criteria.list();

        ret = ConditionHelper.toConditionTypeSet(new HashSet<ConditionEntity>(list), true, null);

        rtn.setConditions(new ArrayList<ConditionType>(ret));
        rtn.setTotalSize(totalSize);

        return rtn;
    }

    /**
     * Search condition.
     *
     * @param uc the uc
     * @param iDs the i ds
     * @param search the search
     * @param historyFlag the history flag
     * @param chargeSetMode the charge set mode
     * @param caseInfoSetMode the case info set mode
     * @return the list
     */
    @Deprecated
    public List<ConditionType> searchCondition(UserContext uc, Set<Long> iDs, ConditionSearchType search, Boolean historyFlag, String chargeSetMode,
            String caseInfoSetMode) {

        Set<ConditionType> ret = new HashSet<ConditionType>();

        // Validation
        if (search == null || !search.isWellFormed() || !search.isValid()) {
            throw new InvalidInputException("search type is not valid.");
        }

        if (!BeanHelper.isEmpty(chargeSetMode) && search.getChargeIds() == null) {
            throw new InvalidInputException("chargeSetMode has value but search.getChargeIds() is null..");
        }

        if (!BeanHelper.isEmpty(caseInfoSetMode) && search.getCaseInfoIds() == null) {
            throw new InvalidInputException("caseInfoSetMode has value but search.getSentenceIds() is null.");
        }

        List<ConditionEntity> searchResult = searchConditionEntity(uc, iDs, search, historyFlag, chargeSetMode, caseInfoSetMode);
        Set<ConditionEntity> conditionEntitySet = new HashSet<ConditionEntity>(searchResult);
        Set<ConditionHistoryEntity> historyEntitySet = new HashSet<ConditionHistoryEntity>();

        // Search against history entities
        historyEntitySet = searchAgainstConditionHistory(iDs, search, historyFlag, chargeSetMode, caseInfoSetMode, historyEntitySet);

        // Max Limit reached.
        if ((historyEntitySet.size() + conditionEntitySet.size()) >= (searchMaxLimit + 1)) {
            log.debug("Search result is over " + searchMaxLimit);
            throw new ArbutusRuntimeException("Search result is over " + searchMaxLimit);
        }

        ret.addAll(ConditionHelper.toConditionTypeSet(conditionEntitySet, true, null));

        if (historyEntitySet.size() > 0) {
            ret.addAll(ConditionHelper.toConditionTypeSet(historyEntitySet, null, null));
        }

        return new ArrayList<ConditionType>(ret);
    }

    /**
     * @param uc
     * @param caseId
     * @param chargeId
     * @param orderId
     * @param conditionSearch
     * @param active
     * @return
     */
    public List<ConditionType> retrieveConditionBy(UserContext uc, Long caseId, Long chargeId, Long orderId, ConditionSearchType conditionSearch, Boolean active) {

        // Validations
        if (caseId == null && chargeId == null && orderId == null) {
            throw new InvalidInputException("case id, charge id and order id can not be null at the same time.");
        }

        if (conditionSearch == null) {
            conditionSearch = new ConditionSearchType();
        }

        List<ConditionEntity> searchResult = searchConditionEntityBy(uc, caseId, chargeId, orderId, conditionSearch, active);
        Set<ConditionEntity> conditionEntitySet = new HashSet<ConditionEntity>(searchResult);

        // Max Limit reached.
        if (conditionEntitySet.size() >= (searchMaxLimit + 1)) {
            log.debug("Search result is over " + searchMaxLimit);
            throw new ArbutusRuntimeException("Search result is over " + searchMaxLimit);
        }

        List<ConditionType> ret = new ArrayList<ConditionType>(ConditionHelper.toConditionTypeSet(conditionEntitySet, true, null));

        return ret;
    }

    /**
     * Delete condition.
     *
     * @param uc the uc
     * @param conditionId the condition id
     * @return the long
     */
    public Long deleteCondition(UserContext uc, Long conditionId) {
        Long rtnCode = ReturnCode.Success.returnCode();

        //id can not be null.
        if (conditionId == null) {
            String message = "conditionId is null";
            LogHelper.error(log, "deleteCharge", message);
            throw new InvalidInputException(message);
        }

        ConditionEntity entity = BeanHelper.getEntity(session, ConditionEntity.class, conditionId);

        entity.setFlag(DataFlag.DELETE.value());

        session.update(entity);

        return rtnCode;
    }

    /**
     * Search against condition history
     *
     * @param iDs
     * @param search
     * @param historyFlag
     * @param chargeSetMode
     * @param caseInfoSetMode
     * @param historyEntitySet
     * @return
     */
    private Set<ConditionHistoryEntity> searchAgainstConditionHistory(Set<Long> iDs, ConditionSearchType search, Boolean historyFlag, String chargeSetMode,
            String caseInfoSetMode, Set<ConditionHistoryEntity> historyEntitySet) {

        if (historyFlag != null && Boolean.TRUE.equals(historyFlag)) {
            List<ConditionHistoryEntity> historyResult = searchHistoryEntity(search, iDs, chargeSetMode, caseInfoSetMode);
            historyEntitySet = new HashSet<ConditionHistoryEntity>(historyResult);
        }

        return historyEntitySet;
    }

    /**
     * Searches for a condition based on search rules
     *
     * @param iDs             A set of Condition Id
     * @param search          search rules
     * @param historyFlag     boolean, if true: includes history on search.
     * @param chargeSetMode   String, Default = "ALL" (ONLY, ANY, ALL) -- Optional. Search SetMode for the set of Charge.
     * @param caseInfoSetMode String, Default = "ALL" (ONLY, ANY, ALL) -- Optional. Search SetMode for the set of CaseInformation.
     * @return List of ConditionEntity
     */
    private List<ConditionEntity> searchConditionEntity(UserContext uc, Set<Long> iDs, ConditionSearchType search, Boolean historyFlag, String chargeSetMode,
            String caseInfoSetMode) {

        Criteria c = createConditionSearchCriteria(search, iDs, ConditionEntity.class, null);

        c = includeModuleAssociations(uc, search, chargeSetMode, caseInfoSetMode, c);

        @SuppressWarnings("unchecked") List<ConditionEntity> list = c.list();

        return list;

    }

    /**
     * Searches a condition given some search rules and one of its module association (CaseInfo, Charge or Order)
     *
     * @param uc              UserContext
     * @param caseId          Long
     * @param chargeId        Long
     * @param orderId         Long
     * @param conditionSearch ConditionSearchType
     * @param active          Boolean
     * @return List of ConditionEntity
     */
    private List<ConditionEntity> searchConditionEntityBy(UserContext uc, Long caseId, Long chargeId, Long orderId, ConditionSearchType conditionSearch, Boolean active) {

        Criteria c = createConditionSearchCriteria(conditionSearch, null, ConditionEntity.class, active);

        if (caseId != null) {
            Set<Long> caseInfoIds = new HashSet<Long>();
            caseInfoIds.add(caseId);
            conditionSearch.setCaseInfoIds(caseInfoIds);
            c = includeModuleAssociations(uc, conditionSearch, null, "ANY", c);
        }
        if (chargeId != null) {
            Set<Long> chargeIds = new HashSet<Long>();
            chargeIds.add(chargeId);
            conditionSearch.setChargeIds(chargeIds);
            c = includeModuleAssociations(uc, conditionSearch, "ANY", null, c);
        }
        if (orderId != null) {
            conditionSearch.setOrderSentenceId(orderId);
            c = includeModuleAssociations(uc, conditionSearch, null, null, c);
        }

        @SuppressWarnings("unchecked") List<ConditionEntity> list = c.list();

        return list;

    }

    /**
     * Include module associations.
     *
     * @param uc the uc
     * @param search the search
     * @param chargeSetMode the charge set mode
     * @param caseInfoSetMode the case info set mode
     * @param c the c
     * @return the criteria
     */
    private Criteria includeModuleAssociations(UserContext uc, ConditionSearchType search, String chargeSetMode, String caseInfoSetMode, Criteria c) {

        if (search.getOrderSentenceId() != null) {
            Set<Long> orderSet = new HashSet<Long>();
            orderSet.add(search.getOrderSentenceId());

            c = BeanHelper.getCriteriaWithModuleAssociations(uc, session, c, "", "conditionId", orderSet, LegalModule.ORDER_SENTENCE.value(),
                    ConditionModuleAssociationEntity.class, "condition");
        }

        c = BeanHelper.getCriteriaWithModuleAssociations(uc, session, c, chargeSetMode, "conditionId", search.getChargeIds(), LegalModule.CHARGE.value(),
                ConditionModuleAssociationEntity.class, "condition");

        c = BeanHelper.getCriteriaWithModuleAssociations(uc, session, c, caseInfoSetMode, "conditionId", search.getCaseInfoIds(), LegalModule.CASE_INFORMATION.value(),
                ConditionModuleAssociationEntity.class, "condition");
        if (search.getCasePlanIds() != null && !search.getCasePlanIds().isEmpty()) {
            c = BeanHelper.getCriteriaWithModuleAssociations(uc, session, c, caseInfoSetMode, "conditionId", search.getCasePlanIds(), LegalModule.CASE_PLAN.value(),
                    ConditionModuleAssociationEntity.class, "condition");
        }
        return c;
    }

    /**
     * Creates the condition search criteria.
     *
     * @param search the search
     * @param iDs the i ds
     * @param conditionClass the condition class
     * @param activeFlag the active flag
     * @return the criteria
     */
    @SuppressWarnings("rawtypes")
    private Criteria createConditionSearchCriteria(ConditionSearchType search, Set<Long> iDs, Class conditionClass, Boolean activeFlag) {

        Criteria c = session.createCriteria(conditionClass);

        c.setMaxResults(searchMaxLimit + 1);
        c.setFetchSize(searchMaxLimit + 1);
        c.setCacheMode(CacheMode.IGNORE);

        // sub set ids.
        if (iDs != null && iDs.size() > 0) {
            c.add(Restrictions.in("conditionId", iDs));
        }

        if (activeFlag != null) {
            c.add(Restrictions.eq("active", activeFlag));
        }

        String conditionType = search.getConditionType();
        if (conditionType != null && conditionType != null) {
            c.add(Restrictions.eq("conditionType", conditionType));
        }

        String conditionCategory = search.getConditionCategory();
        if (conditionCategory != null) {
            c.add(Restrictions.eq("conditionCategory", conditionCategory));
        }

        String conditionSubCategory = search.getConditionSubCategory();
        if (conditionSubCategory != null) {
            c.add(Restrictions.eq("conditionSubCategory", conditionSubCategory));
        }

        String conditionPeriodUnit = search.getConditionPeriodUnit();
        if (conditionPeriodUnit != null) {
            c.add(Restrictions.eq("conditionPeriodUnit", conditionPeriodUnit));
        }

        String conditionDistanceUnit = search.getConditionDistanceUnit();
        if (conditionDistanceUnit != null) {
            c.add(Restrictions.eq("conditionDistanceUnit", conditionDistanceUnit));
        }

        String conditionCurrencyUnit = search.getConditionCurrencyUnit();
        if (conditionCurrencyUnit != null) {
            c.add(Restrictions.eq("conditionCurrencyUnit", conditionCurrencyUnit));
        }

        String conditionAmendmentStatus = search.getConditionAmendmentStatus();
        if (conditionAmendmentStatus != null) {
            c.add(Restrictions.eq("conditionAmendmentStatus", conditionAmendmentStatus));
        }

        Date fromConditionCreateDate = search.getFromConditionCreateDate();
        if (fromConditionCreateDate != null) {
            c.add(Restrictions.ge("fromConditionCreateDate", fromConditionCreateDate));
        }

        Date toConditionCreateDate = search.getToConditionCreateDate();
        if (toConditionCreateDate != null) {
            c.add(Restrictions.le("conditionCreateDate", toConditionCreateDate));
        }

        Date fromConditionStartDate = search.getFromConditionStartDate();
        if (fromConditionStartDate != null) {
            c.add(Restrictions.ge("conditionStartDate", fromConditionStartDate));
        }

        Date toConditionStartDate = search.getToConditionStartDate();
        if (toConditionStartDate != null) {
            c.add(Restrictions.le("conditionStartDate", toConditionStartDate));
        }

        Date fromConditionEndDate = search.getFromConditionEndDate();
        if (fromConditionEndDate != null) {
            c.add(Restrictions.ge("conditionEndDate", fromConditionEndDate));
        }

        Date toConditionEndDate = search.getToConditionEndDate();
        if (toConditionEndDate != null) {
            c.add(Restrictions.le("conditionEndDate", toConditionEndDate));
        }

        Date fromConditionAmendmentDate = search.getFromConditionAmendmentDate();
        if (fromConditionAmendmentDate != null) {
            c.add(Restrictions.ge("conditionAmendmentDate", fromConditionAmendmentDate));
        }

        Date toConditionAmendmentDate = search.getToConditionAmendmentDate();
        if (toConditionAmendmentDate != null) {
            c.add(Restrictions.le("conditionAmendmentDate", toConditionAmendmentDate));
        }

        Boolean isConditionSatisfied = search.getIsConditionSatisfied();
        if (isConditionSatisfied != null) {
            c.add(Restrictions.eq("isConditionSatisfied", isConditionSatisfied));
        }

        Boolean isConditionStatus = search.getIsConditionStatus();
        if (isConditionStatus != null) {
            c.add(Restrictions.eq("active", isConditionStatus));
        }

        Long conditionPeriodValue = search.getConditionPeriodValue();
        if (conditionPeriodValue != null) {
            c.add(Restrictions.eq("conditionPeriodValue", conditionPeriodValue));
        }

        Long conditionDistanceValue = search.getConditionDistanceValue();
        if (conditionDistanceValue != null) {
            c.add(Restrictions.eq("conditionDistanceValue", conditionDistanceValue));
        }

        BigDecimal conditionCurrencyAmount = search.getConditionCurrencyAmount();
        if (conditionCurrencyAmount != null) {
            c.add(Restrictions.eq("conditionCurrencyAmount", conditionCurrencyAmount));
        }

        String conditionDetails = search.getConditionDetails();
        if (conditionDetails != null) {
            c.add(Restrictions.eq("conditionDetails", conditionDetails));
        }

        CommentType conditionComment = search.getConditionComment();
        if (conditionComment != null && conditionComment.getCommentIdentification() != null) {
            c.add(Restrictions.eq("conditionComment.commentIdentification", conditionComment.getCommentIdentification()));
        }

        return c;

    }

    /**
     * Search history entity.
     *
     * @param search the search
     * @param iDs the i ds
     * @param chargeSetMode the charge set mode
     * @param caseInfoSetMode the case info set mode
     * @return the list
     */
    @SuppressWarnings("unchecked")
    private List<ConditionHistoryEntity> searchHistoryEntity(ConditionSearchType search, Set<Long> iDs, String chargeSetMode, String caseInfoSetMode) {

        Criteria c = createConditionSearchCriteria(search, iDs, ConditionHistoryEntity.class, null);

        List<ConditionHistoryEntity> list = c.list();

        return list;
    }

    /**
     * Checks if a comment already exists so it don't get persisted again.
     *
     * @param iDbComment
     * @param comment
     * @return true if comment exist, otherwise false.
     */
    private boolean isExistingComment(Iterator<ConditionCommentEntity> iDbComment, ConditionCommentEntity comment) {
        while (iDbComment.hasNext()) {
            ConditionCommentEntity dbComment = iDbComment.next();
            if (dbComment != null && dbComment.getCommentText() != null &&
                    dbComment.getCommentText().compareToIgnoreCase((comment != null ? comment.getCommentText() : null)) == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if the new ConditionEntity has new value.
     *
     * @param entity
     * @param existEntity
     * @return boolean
     */
    private boolean needToUpdate(ConditionEntity entity, ConditionEntity existEntity) {
        if (entity.equals(existEntity)) {
            return false;
        }

        return true;
    }

    /**
     * Check if there are new module associations
     *
     * @param entity
     * @param existEntity
     * @return
     */
    private Set<ConditionModuleAssociationEntity> getOldModuleAssoc(Set<ConditionModuleAssociationEntity> entity, Set<ConditionModuleAssociationEntity> existEntity) {
        Set<ConditionModuleAssociationEntity> moduleAssociationToHistory = new HashSet<ConditionModuleAssociationEntity>();

        for (ConditionModuleAssociationEntity moduleAssociation : existEntity) {
            if (!entity.contains(moduleAssociation)) {
                moduleAssociationToHistory.add(moduleAssociation);
            }
        }

        return moduleAssociationToHistory;
    }

    /**
     * Retrieves an instance of ConditionEntity by its id.
     *
     * @param conditionId
     * @return ConditionEntity instance
     */

    private ConditionEntity getCondition(Long conditionId) {
        String functionName = "getCondition";
        log.debug(functionName);

        ConditionEntity entity = (ConditionEntity) session.get(ConditionEntity.class, conditionId);

        if (entity == null) {
            String message = String.format("[getCondition] Could not find the condition instance by conditionId = %d.", conditionId);
            throw new DataNotExistException(message);
        }

        return entity;
    }

    /**
     * Checks if is valid module association.
     *
     * @param session2 the session2
     * @param condition the condition
     * @return true, if is valid module association
     */
    @SuppressWarnings("unused")
    private boolean isValidModuleAssociation(Session session2, ConditionType condition) {
        Boolean ret = false;

        if (condition.getCaseInfoIds().size() > 0) {
            ret = checkModuleExistence(condition.getCaseInfoIds(), CaseInfoEntity.class);
        }
        if (condition.getChargeIds().size() > 0) {
            ret = checkModuleExistence(condition.getChargeIds(), ChargeEntity.class);
        }
        if (condition.getOrderSentenceId() != null) {
            ret = checkModuleExistence(condition.getOrderSentenceId(), OrderEntity.class);
        }

        return ret;
    }

    /**
     * Check module existence.
     *
     * @param id the id
     * @param klass the klass
     * @return the boolean
     */
    @SuppressWarnings("rawtypes")
    private Boolean checkModuleExistence(Long id, Class klass) {
        Object entity = null;
        if (id != null) {
            entity = session.get(klass, id);
            if (entity == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check module existence.
     *
     * @param moduleIdSet the module id set
     * @param klass the klass
     * @return the boolean
     */
    @SuppressWarnings("rawtypes")
    private Boolean checkModuleExistence(Set<Long> moduleIdSet, Class klass) {
        Object entity = null;

        if (moduleIdSet.size() > 0) {
            Iterator<Long> iterator = moduleIdSet.iterator();
            while (iterator.hasNext()) {
                entity = session.get(klass, iterator.next());
                if (entity == null) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Gets the history.
     *
     * @param uc the uc
     * @param context the context
     * @param session the session
     * @param conditionId the condition id
     * @param fromHistoryDate the from history date
     * @param toHistoryDate the to history date
     * @return the history
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private Set<ConditionType> getHistory(UserContext uc, SessionContext context, Session session, Long conditionId, Date fromHistoryDate, Date toHistoryDate) {

        ConditionEntity currentEntity = getCondition(conditionId);

        Set<ConditionType> rtn;
        Criteria criteria;
        // Condition
        criteria = session.createCriteria(ConditionEntity.class, "condition");
        criteria.add(Restrictions.eq("conditionId", conditionId));
        if (!BeanHelper.isEmpty(fromHistoryDate)) {
            criteria.add(Restrictions.ge("stamp.modifyDateTime", fromHistoryDate));
        }
        if (!BeanHelper.isEmpty(toHistoryDate)) {
            criteria.add(Restrictions.le("stamp.modifyDateTime", toHistoryDate));
        }

        List currentList = criteria.list();

        if (currentList.isEmpty()) {
            currentEntity = null;
        }

        // Condition History
        criteria = session.createCriteria(ConditionHistoryEntity.class, "conditionHist");
        criteria.add(Restrictions.eq("conditionId", conditionId));
        if (!BeanHelper.isEmpty(fromHistoryDate)) {
            criteria.add(Restrictions.ge("stamp.modifyDateTime", fromHistoryDate));
        }
        if (!BeanHelper.isEmpty(toHistoryDate)) {
            criteria.add(Restrictions.le("stamp.modifyDateTime", toHistoryDate));
        }

        List<ConditionHistoryEntity> historylist = criteria.list();

        Set<ConditionHistoryEntity> conditionHistories = new HashSet<ConditionHistoryEntity>(historylist);

        // Condition Module History
        criteria = session.createCriteria(ConditionModuleAssociationHistEntity.class, "conditionModuleHist");
        criteria.add(Restrictions.eq("fromIdentifier", conditionId));
        if (!BeanHelper.isEmpty(fromHistoryDate)) {
            criteria.add(Restrictions.ge("stamp.modifyDateTime", fromHistoryDate));
        }
        if (!BeanHelper.isEmpty(toHistoryDate)) {
            criteria.add(Restrictions.le("stamp.modifyDateTime", toHistoryDate));
        }

        List<ConditionModuleAssociationHistEntity> historyModulelist = criteria.list();

        Set<ConditionModuleAssociationHistEntity> conditionModuleHistories = new HashSet<ConditionModuleAssociationHistEntity>(historyModulelist);

        rtn = ConditionHelper.toConditionTypeSet(conditionHistories, conditionModuleHistories, currentEntity);

        return rtn;
    }

    // verifies the existance of at least one module association
    //	private boolean hasModuleAssociation(ConditionType condition) {
    //		if(condition.getSentenceIds().size() > 0 ||
    //				condition.getChargeIds().size() > 0 ||
    //				condition.getOrderSentenceId() != null)
    //			return true;
    //
    //		return false;
    //	}

    /**
     * @param uc
     * @param conditionId      - required
     * @param dataSecurityFlag - required
     *                         Enum of DataFlag.SEAL
     * @param comments         - optional
     *                         Comments for DataSecurity Record
     * @param personId         - required
     *                         PersonId of the person updating status
     * @return
     */
    public Long updateConditionDataSecurityStatus(UserContext uc, Long conditionId, Long dataSecurityFlag, String comments, Long personId, String parentEntityType,
            Long parentId, String memento) {

        Long ret = ReturnCode.UnknownError.returnCode();

        // id can not be null.
        if (conditionId == null || personId == null || dataSecurityFlag == null) {
            String message = "chargeId or personId or dataSecurityFlag can not be null";
            throw new InvalidInputException(message);
        }

        if (dataSecurityFlag.equals(DataFlag.SEAL.value())) {
            ret = sealCondition(uc, conditionId, comments, personId, parentEntityType, parentId, memento);
        } else if (dataSecurityFlag.equals(DataFlag.ACTIVE.value())) {
            ret = UnSealCondition(uc, conditionId, comments, personId, parentEntityType, parentId, memento);
        }

        return ret;

    }

    /**
     * Seal condition.
     *
     * @param uc the uc
     * @param conditionId the condition id
     * @param comments the comments
     * @param personId the person id
     * @param parentEntityType the parent entity type
     * @param parentId the parent id
     * @param memento the memento
     * @return the long
     */
    // If charge has conditon as module association , then it is in the charge moduleassociation , but is it in condition module associations also
    public Long sealCondition(UserContext uc, Long conditionId, String comments, Long personId, String parentEntityType, Long parentId, String memento) {
        ConditionEntity entity = BeanHelper.getEntity(session, ConditionEntity.class, conditionId);
        Date recordDate = new Date();
        String recordType = DataSecurityRecordTypeEnum.SEALCONDITION.code();

        // 1- Conditions has no associations, so just seal condition

        // 2- Set the Status on the Parent Charge
        String entityType = EntityTypeEnum.CONDITION.code();

        DataSecurityRecordType dsr = new DataSecurityRecordType(null, entity.getConditionId(), entityType, parentId, parentEntityType, recordType, personId, recordDate,
                comments, memento);

        DataSecurityServiceAdapter.createRecord(uc, dsr);
        entity.setFlag(DataFlag.SEAL.value());
        session.merge(entity);
        session.flush();

        return ReturnCode.Success.returnCode();
    }

    /**
     * Un seal condition.
     *
     * @param uc the uc
     * @param conditionId the condition id
     * @param comments the comments
     * @param personId the person id
     * @param parentEntityType the parent entity type
     * @param parentId the parent id
     * @param memento the memento
     * @return the long
     */
    public Long UnSealCondition(UserContext uc, Long conditionId, String comments, Long personId, String parentEntityType, Long parentId, String memento) {
        DataSecurityRecordType sealedCondition = DataSecurityServiceAdapter.getEntity(uc, EntityTypeEnum.CONDITION.code(), conditionId, parentEntityType, parentId);

        // 2- Set the Status on the Parent Charge
        String recordType = DataSecurityRecordTypeEnum.UNSEALCHARGE.code();
        Date recordDate = new Date();

        DataSecurityRecordType dsr = new DataSecurityRecordType(null, conditionId, DataSecurityRecordTypeEnum.UNSEALCASEACTIVITY.code(), parentId, parentEntityType,
                recordType, personId, recordDate, comments, memento);
        DataSecurityServiceAdapter.createRecord(uc, dsr);

        String chrgeTblName = "leg_cncondition";
        String sql = "select conditionid, flag from " + chrgeTblName + " where conditionid = :conditionIdParam";
        String usql = "update " + chrgeTblName + " set flag = 1 where conditionid = :conditionIdParam";
        Map<String, String> paramMap = new HashMap<String, String>();
        Map<String, Type> typesMap = new HashMap<String, Type>();

        // update charge status
        paramMap.put("conditionIdParam", String.valueOf(sealedCondition.getEntityId()));
        typesMap.put("conditionIdParam", LongType.INSTANCE);
        typesMap.put("flag", LongType.INSTANCE);
        List entitys = DataSecurityHelper.executeRawSQLQuery(session, sql, paramMap, typesMap);

        Long id = null;
        Long flag = null;

        // There should be only one result in entitys
        for (int i = 0; i < entitys.size(); i++) {
            Object[] o = (Object[]) entitys.get(i);
            id = DataSecurityHelper.getLongValue(o[0]);
            flag = DataSecurityHelper.getLongValue(o[1]);
        }

        paramMap.clear();
        typesMap.clear();
        paramMap.put("conditionIdParam", String.valueOf(id));
        typesMap.put("conditionIdParam", LongType.INSTANCE);
        DataSecurityHelper.executeRawSQLQuery(session, usql, paramMap, typesMap);

        session.flush();
        session.clear();

        return ReturnCode.Success.returnCode();
    }

    /**
     * Creates the configure condition.
     *
     * @param userContext the user context
     * @param configureCondition the configure condition
     * @return the configure condition type
     */
    public ConfigureConditionType createConfigureCondition(UserContext userContext, ConfigureConditionType configureCondition) {
        ValidationHelper.validate(userContext);
        ValidationHelper.validate(configureCondition);
        if (!getConfigureConditions(configureCondition).isEmpty()) {
            throw new InvalidInputException("Configure Condition already exist");
        }
        StampEntity stamp = BeanHelper.getCreateStamp(userContext, context);
        ConfigureConditionEntity configureConditionEntity = ConditionHelper.toConfigureConditionEntity(configureCondition, stamp);
        ValidationHelper.verifyMetaCodes(configureConditionEntity, true);
        session.save(configureConditionEntity);
        session.flush();
        return ConditionHelper.toConfigureConditionType(configureConditionEntity);
    }

    /**
     * Update configure condition.
     *
     * @param userContext the user context
     * @param configureCondition the configure condition
     * @return the configure condition type
     */
    public ConfigureConditionType updateConfigureCondition(UserContext userContext, ConfigureConditionType configureCondition) {
        ValidationHelper.validate(userContext);
        ValidationHelper.validate(configureCondition);
        if (configureCondition.getId() == null) {
            String message = "Id can not be NULL for updating Configure Condition";
            LogHelper.error(log, "updateConfigureCondition", message);
            throw new InvalidInputException(message);
        }
        ConfigureConditionEntity existingEntity = (ConfigureConditionEntity) session.get(ConfigureConditionEntity.class, configureCondition.getId());
        if (existingEntity == null) {
            String message = "ConfigureCondition not found for given Id %s.";
            throw new InvalidInputException(String.format(message, configureCondition.getId()));
        }
        BeanHelper.getModifyStamp(userContext, context, existingEntity.getStamp());
        ConditionHelper.modifyConfigureConditionEntity(existingEntity, configureCondition);
        ValidationHelper.verifyMetaCodes(existingEntity, true);
        session.update(existingEntity);
        session.flush();
        return ConditionHelper.toConfigureConditionType(existingEntity);
    }

    /**
     * Gets the configure conditions.
     *
     * @param configureCondition the configure condition
     * @return the configure conditions
     */
    private List<ConfigureConditionEntity> getConfigureConditions(ConfigureConditionType configureCondition) {
        Criteria criteria = session.createCriteria(ConfigureConditionEntity.class);
        criteria.add(Restrictions.eq("configureConditionType", configureCondition.getType()));
        criteria.add(Restrictions.eq("category", configureCondition.getCategory()));
        criteria.add(Restrictions.eq("shortName", configureCondition.getShortName()));
        return criteria.list();
    }

    /**
     * Delete configure condition.
     *
     * @param userContext the user context
     * @param configureConditionId the configure condition id
     * @return the long
     */
    public Long deleteConfigureCondition(UserContext userContext, Long configureConditionId) {
        Long returnCode = ReturnCode.Success.returnCode();
        if (configureConditionId == null) {
            String message = "conditionId is null";
            LogHelper.error(log, "deleteCharge", message);
            throw new InvalidInputException(message);
        }
        ConfigureConditionEntity entity = BeanHelper.getEntity(session, ConfigureConditionEntity.class, configureConditionId);
        entity.setFlag(DataFlag.DELETE.value());
        session.update(entity);
        return returnCode;
    }

    /**
     * Gets the configure condition.
     *
     * @param userContext the user context
     * @param configureConditionId the configure condition id
     * @return the configure condition
     */
    public ConfigureConditionType getConfigureCondition(UserContext userContext, Long configureConditionId) {
        if (configureConditionId == null) {
            String message = "conditionId is null";
            LogHelper.error(log, "deleteCharge", message);
            throw new InvalidInputException(message);
        }
        ConfigureConditionEntity configureConditionEntity = (ConfigureConditionEntity) session.get(ConfigureConditionEntity.class, configureConditionId);
        if (configureConditionEntity == null) {
            return null;
        }
        return ConditionHelper.toConfigureConditionType(configureConditionEntity);
    }

    /**
     * Search configure condition.
     *
     * @param userContext the user context
     * @param searchCriteria the search criteria
     * @param historyFlag the history flag
     * @param startIndex the start index
     * @param resultSize the result size
     * @param resultOrder the result order
     * @return the configure conditions return type
     */
    public ConfigureConditionsReturnType searchConfigureCondition(UserContext userContext, ConfigureConditionSearchType searchCriteria, Boolean historyFlag,
            Long startIndex, Long resultSize, String resultOrder) {
        ValidationHelper.validate(userContext);
        Criteria criteria = session.createCriteria(ConfigureConditionEntity.class);
        if (searchCriteria != null) {
            ValidationHelper.validateSearchType(searchCriteria);
            if (!BeanHelper.isEmpty(searchCriteria.getAttribute())) {
                criteria.add(Restrictions.eq("attribute", searchCriteria.getAttribute()));
            }
            if (!BeanHelper.isEmpty(searchCriteria.getCategory())) {
                criteria.add(Restrictions.eq("category", searchCriteria.getCategory()));
            }
            if (!BeanHelper.isEmpty(searchCriteria.getCode())) {
                criteria.add(Restrictions.eq("code", searchCriteria.getCode()));
            }
            if (!BeanHelper.isEmpty(searchCriteria.getShortName())) {
                criteria.add(Restrictions.eq("shortName", searchCriteria.getShortName()));
            }
            if (!BeanHelper.isEmpty(searchCriteria.getSubCategory())) {
                criteria.add(Restrictions.eq("subCategory", searchCriteria.getSubCategory()));
            }
            if (!BeanHelper.isEmpty(searchCriteria.getType())) {
                criteria.add(Restrictions.eq("configureConditionType", searchCriteria.getType()));
            }
        }
        Long totalSize = (Long) criteria.setProjection(Projections.countDistinct("configureConditionId")).uniqueResult();
        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());
            criteria.setMaxResults(resultSize > searchMaxLimit ? searchMaxLimit : resultSize.intValue());
        }
        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                criteria.addOrder(ord);
            }
        }
        criteria.setProjection(null);
        List<ConfigureConditionEntity> result = criteria.list();
        ConfigureConditionsReturnType returnType = new ConfigureConditionsReturnType(totalSize, ConditionHelper.toConfigureConditionTypes(userContext, result));
        return returnType;
    }

}