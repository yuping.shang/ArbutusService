package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import java.io.Serializable;

/**
 * IntermittentSentenceScheduleReturnType for Case Management Service
 *
 * @author amlendu.kumar
 * @version 1.0
 * @since May 14, 2014
 */
public class IntermittentSentenceScheduleReturnType implements Serializable {

    private static final long serialVersionUID = 931327623634370784L;

    private IntermittentSentenceScheduleType intermittentSentenceScheduleType;
    private Long returnCode;
    private String returnMessage;

    /**
     * Get intermittent sentence schedule.
     *
     * @return intermittentSentenceScheduleType IntermittentSentenceScheduleType
     */
    public IntermittentSentenceScheduleType getIntermittentSentenceScheduleType() {
        return intermittentSentenceScheduleType;
    }

    /**
     * Set intermittent sentence schedule.
     *
     * @param intermittentSentenceScheduleType IntermittentSentenceScheduleType
     */
    public void setIntermittentSentenceScheduleType(IntermittentSentenceScheduleType intermittentSentenceScheduleType) {
        this.intermittentSentenceScheduleType = intermittentSentenceScheduleType;
    }

    /**
     * Get return code of the response.
     *
     * @return returnCode long
     */
    public Long getReturnCode() {
        return returnCode;
    }

    /**
     * Set return code of the response.
     *
     * @param returnCode
     */
    public void setReturnCode(Long returnCode) {
        this.returnCode = returnCode;
    }

    /**
     * Get return message of the response.
     *
     * @return returnMessage String
     */
    public String getReturnMessage() {
        return returnMessage;
    }

    /**
     * Set return message of the response.
     *
     * @param returnMessage string
     */
    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

}
