package syscon.arbutus.product.services.legal.contract.ejb;

import javax.ejb.SessionContext;
import java.math.BigDecimal;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.*;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.contract.dto.*;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.ErrorCodes;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.legal.contract.dto.*;
import syscon.arbutus.product.services.legal.contract.dto.ordersentence.*;
import syscon.arbutus.product.services.legal.contract.ejb.OrderSentenceHandler.TermTypes;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.legal.realization.persistence.SentenceTermEntity;
import syscon.arbutus.product.services.legal.realization.persistence.SentenceTermNameEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.CourtActivityEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.CaseInfoEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.CaseInfoModuleAssociationEntity;
import syscon.arbutus.product.services.legal.realization.persistence.charge.ChargeEntity;
import syscon.arbutus.product.services.legal.realization.persistence.charge.ChargeModuleAssociationEntity;
import syscon.arbutus.product.services.legal.realization.persistence.condition.ConditionEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.*;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.AssociationHelper;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.realization.util.ReferenceDataHelper;
import syscon.arbutus.product.services.referencedata.contract.interfaces.ReferenceDataService;
import syscon.arbutus.product.services.sentencecalculation.contract.util.DateUtil;
import syscon.arbutus.product.services.utility.ServiceAdapter;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class OrderSentenceHelper {

    public static final Boolean bSECURRED = true;
    public static final String CREATE = "CREATE";
    public static final String UPDATE = "UPDATE";
    public static final String DELETE = "DELETE";
    public static final String READ = "READ";
    private static final String PERSON_IDENTITY_SERVICE = "PersonIdentityService";
    private static final String ORGANIZATION_SERVICE = "OrganizationService";
    private static final int NUMBER_OF_DAYS_IN_A_WEEK = 7;
    private static final String CUSTODY = "CUS";
    private static final String TERM_NAME_MAX = "MAX";
    private static Logger log = LoggerFactory.getLogger(OrderSentenceHelper.class);

    public static <U extends OrderType, V extends OrderEntity> V toOrderEntity(UserContext uc, SessionContext context, U from, StampEntity stamp, String op,
            Class<V> clazz) {

        V to = null;
        try {
            to = clazz.newInstance();

            Long instanceId = from.getOrderIdentification();
            to.setOrderId(instanceId);
            to.setOjSupervisionId(from.getOjSupervisionId());
            to.setOrderClassification(from.getOrderClassification());
            to.setOrderType(from.getOrderType());
            to.setOrderSubType(from.getOrderSubType());
            to.setOrderCategory(from.getOrderCategory());
            to.setOrderNumber(from.getOrderNumber());
            to.setOrderDisposition(toDispositionEntity(uc, context, from.getOrderDisposition(), stamp));
            CommentType comment = from.getComments();
            if (comment != null) {
                comment.setUserId(BeanHelper.getUserId(uc, context));
                to.setComments(toCommentEntity(comment));
            }
            to.setOrderIssuanceDate(from.getOrderIssuanceDate());
            to.setOrderReceivedDate(from.getOrderReceivedDate());
            to.setOrderStartDate(from.getOrderStartDate());
            to.setOrderExpirationDate(from.getOrderExpirationDate());
            /*
             * to.setCaseActivityInitiatedOrderAssociations(
			 * AssociationHelper.toAssociableSet
			 * (AssociationToClass.CASE_ACTIVITY.value(),
			 * from.getCaseActivityInitiatedOrderAssociations(), stamp,
			 * CaseActivityInitOrderEntity.class));
			 * to.setOrderInitiatedCaseActivityAssociations(
			 * AssociationHelper.toAssociableSet
			 * (AssociationToClass.CASE_ACTIVITY.value(),
			 * from.getOrderInitiatedCaseActivityAssociations(), stamp,
			 * OrderInitCaseActivityEntity.class));
			 */
            to.setIsHoldingOrder(from.getIsHoldingOrder());
            to.setIsSchedulingNeeded(from.getIsSchedulingNeeded());
            to.setHasCharges(from.getHasCharges());
            to.setIssuingAgency(toNotificationEntity(uc, context, from.getIssuingAgency(), op));

            Set<OrderModuleAssociationEntity> orderModules = new HashSet<OrderModuleAssociationEntity>();
            if (from.getCaseInfoIds() != null) {
                for (Long id : from.getCaseInfoIds()) {
                    orderModules.add(new OrderModuleAssociationEntity(null, LegalModule.CASE_INFORMATION.value(), id, stamp));
                }
            }
            if (from.getChargeIds() != null) {
                for (Long id : from.getChargeIds()) {
                    orderModules.add(new OrderModuleAssociationEntity(null, LegalModule.CHARGE.value(), id, stamp));
                }
            }
            if (from.getConditionIds() != null) {
                for (Long id : from.getConditionIds()) {
                    orderModules.add(new OrderModuleAssociationEntity(null, LegalModule.CONDITION.value(), id, stamp));
                }
            }
            to.setModuleAssociations(orderModules);

            to.setStamp(stamp);

            if (from instanceof WarrantDetainerType) {
                WarrantDetainerEntity warrantDetainer = (WarrantDetainerEntity) to;
                warrantDetainer.setAgencyToBeNotified(toNotificationEntitySet(uc, context, ((WarrantDetainerType) from).getAgencyToBeNotified(), op));
                warrantDetainer.setNotificationLog(toNotificationLogEntitySet(uc, context, ((WarrantDetainerType) from).getNotificationLog(), stamp));
            } else if (from instanceof BailType) {
                BailEntity bail = (BailEntity) to;
                BailType type = (BailType) from;
                bail.setBailAmounts(toAmountableSet(uc, context, ((BailType) from).getBailAmounts(), stamp, BailAmountEntity.class));
                bail.setBailRelationship(((BailType) from).getBailRelationship());
                bail.setBailPostedAmounts(toAmountableSet(uc, context, ((BailType) from).getBailPostedAmounts(), stamp, BailPostedAmountEntity.class));
                bail.setBailPaymentReceiptNo(((BailType) from).getBailPaymentReceiptNo());
                bail.setBailBondPostedDate(type.getBailBondPostedDate());
                bail.setBailRequirement(((BailType) from).getBailRequirement());
                bail.setBailerPersonIdentities(AssociationHelper.toAssociableSet(PERSON_IDENTITY_SERVICE, ((BailType) from).getBailerPersonIdentityAssociations(), stamp,
                        BailPersonIdAssocEntity.class));
                bail.setBondPostedAmount(toAmountable(uc, context, ((BailType) from).getBondPostedAmount(), stamp, BondPostedAmountEntity.class));
                bail.setBondPaymentDescription(((BailType) from).getBondPaymentDescription());
                bail.setBondOrganizations(
                        AssociationHelper.toAssociableSet(ORGANIZATION_SERVICE, ((BailType) from).getBondOrganizationAssociations(), stamp, BondOrgAssocEntity.class));
                bail.setIsBailAllowed(((BailType) from).getIsBailAllowed());
            } else if (from instanceof SentenceType) {
                SentenceEntity sentence = (SentenceEntity) to;
                sentence.setSentenceType(((SentenceType) from).getSentenceType());
                sentence.setSentenceNumber(((SentenceType) from).getSentenceNumber());
                sentence.setSentenceTerms(toTermEntitySet(uc, context, ((SentenceType) from).getSentenceTerms(), stamp));
                sentence.setSentenceStatus(((SentenceType) from).getSentenceStatus());
                sentence.setSentenceStartDate(((SentenceType) from).getSentenceStartDate());
                sentence.setSentenceEndDate(((SentenceType) from).getSentenceEndDate());
                sentence.setSentenceStartTime(((SentenceType) from).getSentenceStartTime());
                sentence.setSentenceEndTime(((SentenceType) from).getSentenceEndTime());
                sentence.setSentenceAppeal(toSentenceAppealEntity(uc, context, ((SentenceType) from).getSentenceAppeal(), stamp));
                sentence.setConcecutiveFrom(((SentenceType) from).getConcecutiveFrom());
                sentence.setFineAmount(((SentenceType) from).getFineAmount());
                sentence.setFineAmountPaid(((SentenceType) from).getFineAmountPaid());
                sentence.setSentenceAggravateFlag(((SentenceType) from).getSentenceAggravateFlag());
                sentence.setIntermittentSchedule(toIntermittentScheduleEntitySet(uc, context, ((SentenceType) from).getIntermittentSchedule(), stamp));
                sentence.setIntermittentSentenceSchedules(toIntermittentSentenceScheduleEntitySet(((SentenceType) from).getSchedules(), stamp, sentence));
                //				generateIntermittentSentenceSchedules(sentence, (SentenceType)from, stamp);
            }
        } catch (InstantiationException e) {
            throw new InvalidInputException(e.getMessage());
        } catch (IllegalAccessException e) {
            throw new InvalidInputException(e.getMessage());
        }
        return to;
    }

    public static Set<IntermittentScheduleEntity> toIntermittentScheduleEntitySet(UserContext uc, SessionContext context, Set<IntermittentScheduleType> from,
            StampEntity stamp) {
        if (from == null) {
			return null;
		}
        Set<IntermittentScheduleEntity> to = new HashSet<IntermittentScheduleEntity>();
        for (IntermittentScheduleType intermittentSchedule : from) {
            to.add(toIntermittentScheduleEntity(uc, context, intermittentSchedule, stamp));
        }

        return to;
    }

    public static IntermittentScheduleEntity toIntermittentScheduleEntity(UserContext uc, SessionContext context, IntermittentScheduleType from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        IntermittentScheduleEntity to = new IntermittentScheduleEntity();
        to.setInterScheduleId(from.getInterScheduleId());
        to.setDayOutOfWeek(from.getDayOutOfWeek());
        Boolean sameDay = from.getSameDay();
        to.setSameDay((sameDay != null && Boolean.TRUE.equals(sameDay)) ? 1l : 0l);
        to.setDayOfWeek(from.getDayOfWeek());
        if (from.getStartTime() != null) {
            to.setStartHour(from.getStartTime().hour());
            to.setStartMinute(from.getStartTime().minute());
            to.setStartSecond(from.getStartTime().second());
        }
        if (from.getEndTime() != null) {
            to.setEndHour(from.getEndTime().hour());
            to.setEndMinute(from.getEndTime().minute());
            to.setEndSecond(from.getEndTime().second());
        }
        to.setReportingDate(from.getReportingDate());
        to.setScheduleEndDate(from.getScheduleEndDate());
        to.setLocationId(from.getLocationId());
        to.setStamp(stamp);
        to.setScheduleIdentification(from.getScheduleIdentification());
        return to;
    }

    public static Set<IntermittentSentenceScheduleEntity> toIntermittentSentenceScheduleEntitySet(Set<IntermittentSentenceScheduleType> from, StampEntity stamp,
            SentenceEntity sentence) {
        if (from == null) {
			return null;
		}
        Set<IntermittentSentenceScheduleEntity> to = new HashSet<IntermittentSentenceScheduleEntity>();
        for (IntermittentSentenceScheduleType intermittentSentenceSchedule : from) {
            to.add(toIntermittentSentenceScheduleEntity(intermittentSentenceSchedule, stamp, sentence));
        }

        return to;
    }

    public static SentenceAppealEntity toSentenceAppealEntity(UserContext uc, SessionContext context, SentenceAppealType from, StampEntity stamp) {
        if (from == null) {
			return null;
		}
        SentenceAppealEntity to = new SentenceAppealEntity();
        to.setAppealStatus(from.getAppealStatus());
        to.setAppealComment(from.getAppealComment());
        to.setStamp(stamp);
        return to;
    }

    public static Set<TermEntity> toTermEntitySet(UserContext uc, SessionContext context, Set<TermType> from, StampEntity stamp) {
        if (from == null) {
			return null;
		}
        Set<TermEntity> to = new HashSet<TermEntity>();
        for (TermType term : from) {
            to.add(toTermEntity(uc, context, term, stamp));
        }
        return to;
    }

    public static TermEntity toTermEntity(UserContext uc, SessionContext context, TermType from, StampEntity stamp) {
        if (from == null) {
			return null;
		}
        TermEntity to = new TermEntity();
        to.setTermCategory(from.getTermCategory());
        to.setTermName(from.getTermName());
        to.setTermType(from.getTermType());
        to.setTermStatus(from.getTermStatus());
        to.setTermSequenceNo(from.getTermSequenceNo());
        to.setTermStartDate(from.getTermStartDate());
        to.setTermEndDate(from.getTermEndDate());
        to.setYears(from.getYears());
        to.setMonths(from.getMonths());
        to.setWeeks(from.getWeeks());
        to.setDays(from.getDays());
        to.setHours(from.getHours());
        to.setStamp(stamp);
        return to;
    }

    public static Set<BailAmountEntity> toBailAmountEntitySet(UserContext uc, SessionContext context, Set<BailAmountType> from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        Set<BailAmountEntity> to = new HashSet<BailAmountEntity>();
        for (BailAmountType bailAmount : from) {
            to.add(toBailAmountEntity(uc, context, bailAmount, stamp));
        }
        return to;
    }

    public static BailAmountEntity toBailAmountEntity(UserContext uc, SessionContext context, BailAmountType from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        BailAmountEntity to = new BailAmountEntity();
        to.setBailType(from.getBailType());
        to.setBailAmount(from.getBailAmount());
        to.setChargeAssociations(AssociationHelper.toAssociableSet(from.getChargeAssociations(), OrderChargeAssocEntity.class, stamp));
        to.setStamp(stamp);
        return to;
    }

    public static <T extends Amountable> Set<T> toAmountableSet(UserContext uc, SessionContext context, Set<BailAmountType> from, StampEntity stamp, Class<T> clazz) {
        if (from == null) {
			return null;
		}
        Set<T> to = new HashSet<T>();
        for (BailAmountType amount : from) {
            to.add(toAmountable(uc, context, amount, stamp, clazz));
        }
        return to;
    }

    public static <T extends Amountable> T toAmountable(UserContext uc, SessionContext context, BailAmountType from, StampEntity stamp, Class<T> clazz) {
        if (from == null) {
			return null;
		}

        T to = null;
        try {
            to = clazz.newInstance();
            to.setBailType(from.getBailType());
            to.setBailAmount(from.getBailAmount());
            to.setStamp(stamp);
            if (to instanceof BailAmountEntity) {
                BailAmountEntity entity = (BailAmountEntity) to;
                entity.setChargeAssociations(AssociationHelper.toAssociableSet(from.getChargeAssociations(), OrderChargeAssocEntity.class, stamp));
            } else if (to instanceof BailAmountHistoryEntity) {
                BailAmountHistoryEntity entity = (BailAmountHistoryEntity) to;
                entity.setChargeAssociations(AssociationHelper.toAssociableSet(from.getChargeAssociations(), OrderChargeAssocHistEntity.class, stamp));
            } else if (to instanceof BailPostedAmountEntity) {
                BailPostedAmountEntity entity = (BailPostedAmountEntity) to;
                entity.setChargeAssociations(AssociationHelper.toAssociableSet(from.getChargeAssociations(), OrderPostedChgAssocEntity.class, stamp));
            } else if (to instanceof BailPostedAmountHistEntity) {
                BailPostedAmountHistEntity entity = (BailPostedAmountHistEntity) to;
                entity.setChargeAssociations(AssociationHelper.toAssociableSet(from.getChargeAssociations(), OrderPostedChgAssocHistEntity.class, stamp));
            } else if (to instanceof BondPostedAmountEntity) {
                BondPostedAmountEntity entity = (BondPostedAmountEntity) to;
                entity.setChargeAssociations(AssociationHelper.toAssociableSet(from.getChargeAssociations(), BondChargeAssocEntity.class, stamp));
            } else if (to instanceof BondPostedAmountHistEntity) {
                BondPostedAmountHistEntity entity = (BondPostedAmountHistEntity) to;
                entity.setChargeAssociations(AssociationHelper.toAssociableSet(from.getChargeAssociations(), BondChargeAssocHistEntity.class, stamp));
            }
        } catch (InstantiationException e) {
            throw new InvalidInputException(e.getMessage());
        } catch (IllegalAccessException e) {
            throw new InvalidInputException(e.getMessage());
        }

        return to;
    }

    public static OrderPostedChgAssocEntity toOrderPostedChgAssocEntity(Long toIdentifier, StampEntity stamp) {
        if (toIdentifier == null) {
			return null;
		}

        return new OrderPostedChgAssocEntity(null, toIdentifier, stamp);
    }

    public static Set<NotificationLogEntity> toNotificationLogEntitySet(UserContext uc, SessionContext context, Set<NotificationLogType> from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        Set<NotificationLogEntity> to = new HashSet<NotificationLogEntity>();
        for (NotificationLogType notificationLog : from) {
            to.add(toNotificationLogEntity(uc, context, notificationLog, stamp));
        }
        return to;
    }

    public static NotificationLogEntity toNotificationLogEntity(UserContext uc, SessionContext context, NotificationLogType from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        NotificationLogEntity to = new NotificationLogEntity();
        to.setNotificationDate(from.getNotificationDate());
        to.setNotificationType(from.getNotificationType());
        to.setNotificationComment(toCommentEntity(from.getNotificationComment()));
        to.setNotificationContact(from.getNotificationContact());

        return to;
    }

    @SuppressWarnings("unchecked")
    public static <U extends OrderEntity, V extends OrderType> List<V> toOrderList(UserContext uc, SessionContext context, List<U> from) {
        if (from == null) {
			return null;
		}

        List<V> to = new ArrayList<V>();
        for (OrderEntity entity : from) {
            V order = null;
            if (entity instanceof BailEntity) {
                order = (V) OrderSentenceHelper.toOrder(uc, context, (BailEntity) entity, BailType.class);
            } else if (entity instanceof LegalOrderEntity) {
                order = (V) OrderSentenceHelper.toOrder(uc, context, (LegalOrderEntity) entity, LegalOrderType.class);
            } else if (entity instanceof SentenceEntity) {
                order = (V) OrderSentenceHelper.toOrder(uc, context, (SentenceEntity) entity, SentenceType.class);
            } else if (entity instanceof WarrantDetainerEntity) {
                order = (V) OrderSentenceHelper.toOrder(uc, context, (WarrantDetainerEntity) entity, WarrantDetainerType.class);
            } else {
                order = (V) OrderSentenceHelper.toOrder(uc, context, (OrderEntity) entity, OrderType.class);
            }
            to.add(order);
        }
        return to;
    }

    public static <U extends OrderEntity, V extends OrderType> V toOrder(UserContext uc, SessionContext context, U from, Class<V> clazz) {
        if (from == null) {
			return null;
		}
        V to = null;

        try {
            to = clazz.newInstance();
            to.setOrderIdentification(from.getOrderId());
            to.setOjSupervisionId(from.getOjSupervisionId());
            if (from.getOrderClassification() != null) {
				to.setOrderClassification(from.getOrderClassification());
			}
            if (from.getOrderType() != null) {
				to.setOrderType(from.getOrderType());
			}
            if (from.getOrderSubType() != null) {
				to.setOrderSubType(from.getOrderSubType());
			}
            if (from.getOrderCategory() != null) {
				to.setOrderCategory(from.getOrderCategory());
			}
            to.setOrderNumber(from.getOrderNumber());
            to.setOrderDisposition(toDispositionType(from.getOrderDisposition()));
            to.setComments(toCommentType(from.getOrderId(), from.getComments()));
            to.setOrderIssuanceDate(from.getOrderIssuanceDate());
            to.setOrderReceivedDate(from.getOrderReceivedDate());
            to.setOrderStartDate(from.getOrderStartDate());
            to.setOrderExpirationDate(from.getOrderExpirationDate());
			/*
			 * to.setCaseActivityInitiatedOrderAssociations(AssociationHelper.
			 * toLongSet(from.getCaseActivityInitiatedOrderAssociations()));
			 * to.setOrderInitiatedCaseActivityAssociations
			 * (AssociationHelper.toLongSet
			 * (from.getOrderInitiatedCaseActivityAssociations()));
			 */
            to.setCaseActivityInitiatedOrderAssociations(toCaseActivityIdsSet(from.getCaseActivitiesInitiatedOrder()));
            to.setOrderInitiatedCaseActivityAssociations(toCaseActivityIdsSet(from.getOrderInitiatedCaseActivities()));

            to.setIsHoldingOrder(from.getIsHoldingOrder());
            to.setIsSchedulingNeeded(from.getIsSchedulingNeeded());
            to.setHasCharges(from.getHasCharges());
            to.setIssuingAgency(toNotificationType(from.getIssuingAgency()));
            Set<OrderModuleAssociationEntity> moduleEntities = from.getModuleAssociations();
            if (moduleEntities != null) {
                Set<Long> caseInfoIds = new HashSet<Long>();
                Set<Long> chargeIds = new HashSet<Long>();
                Set<Long> conditionIds = new HashSet<Long>();
                for (OrderModuleAssociationEntity module : moduleEntities) {
                    if (LegalModule.CASE_INFORMATION.value().equals(module.getToClass())) {
                        caseInfoIds.add(module.getToIdentifier());
                    } else if (LegalModule.CHARGE.value().equals(module.getToClass())) {
                        chargeIds.add(module.getToIdentifier());
                    } else if (LegalModule.CONDITION.value().equals(module.getToClass())) {
                        conditionIds.add(module.getToIdentifier());
                    }
                }
                to.setCaseInfoIds(caseInfoIds);
                to.setChargeIds(chargeIds);
                to.setConditionIds(conditionIds);
            }

            if (from instanceof WarrantDetainerEntity) {
                WarrantDetainerType warrant = (WarrantDetainerType) to;
                warrant.setAgencyToBeNotified(toNotificationTypeSet(((WarrantDetainerEntity) from).getAgencyToBeNotified()));
                warrant.setNotificationLog(toNotificationLogTypeSet(((WarrantDetainerEntity) from).getNotificationLog()));
            } else if (from instanceof BailEntity) {
                BailType bail = (BailType) to;
                bail.setBailAmounts(fromBailAmountEntityToBailAmountTypeSet(((BailEntity) from).getBailAmounts()));
                if (((BailEntity) from).getBailRelationship() != null) {
					bail.setBailRelationship(((BailEntity) from).getBailRelationship());
				}
                bail.setBailPostedAmounts(fromBailPostedAmountEntityToBailAmountTypeSet(((BailEntity) from).getBailPostedAmounts()));
                bail.setBailPaymentReceiptNo(((BailEntity) from).getBailPaymentReceiptNo());
                bail.setBailBondPostedDate(((BailEntity) from).getBailBondPostedDate());
                bail.setBailRequirement(((BailEntity) from).getBailRequirement());
                bail.setBailerPersonIdentityAssociations(AssociationHelper.toLongSet(((BailEntity) from).getBailerPersonIdentities()));
                bail.setBondPostedAmount(toBailAmountType(((BailEntity) from).getBondPostedAmount()));
                bail.setBondPaymentDescription(((BailEntity) from).getBondPaymentDescription());
                bail.setBondOrganizationAssociations(AssociationHelper.toLongSet(((BailEntity) from).getBondOrganizations()));
                bail.setIsBailAllowed(((BailEntity) from).getIsBailAllowed());
            } else if (from instanceof SentenceEntity) {
                SentenceType sentence = (SentenceType) to;
                if (((SentenceEntity) from).getSentenceType() != null) {
					sentence.setSentenceType(((SentenceEntity) from).getSentenceType());
				}
                sentence.setSentenceNumber(((SentenceEntity) from).getSentenceNumber());
                sentence.setSentenceTerms(toTermTypeSet(((SentenceEntity) from).getSentenceTerms()));
                if (((SentenceEntity) from).getSentenceStatus() != null) {
					sentence.setSentenceStatus(((SentenceEntity) from).getSentenceStatus());
				}
                sentence.setSentenceStartDate(((SentenceEntity) from).getSentenceStartDate());
                sentence.setSentenceEndDate(((SentenceEntity) from).getSentenceEndDate());
                sentence.setSentenceAppeal(toSentenceAppealType(((SentenceEntity) from).getSentenceAppeal()));
                if (((SentenceEntity) from).getConcecutiveFrom() != null) {
					sentence.setConcecutiveFrom(((SentenceEntity) from).getConcecutiveFrom());
				}
                sentence.setFineAmount(((SentenceEntity) from).getFineAmount());
                sentence.setFineAmountPaid(((SentenceEntity) from).getFineAmountPaid());
                sentence.setSentenceAggravateFlag(((SentenceEntity) from).getSentenceAggravateFlag());
                sentence.setIntermittentSchedule(toIntermittentScheduleTypeSet(((SentenceEntity) from).getIntermittentSchedule()));
                sentence.setFinePayments(toFinePaymentTypeSet(((SentenceEntity) from).getFinePayments()));
                sentence.setSentenceStartTime(((SentenceEntity) from).getSentenceStartTime());
                sentence.setSentenceEndTime(((SentenceEntity) from).getSentenceEndTime());
            }
        } catch (InstantiationException e) {
            throw new InvalidInputException(e.getMessage());
        } catch (IllegalAccessException e) {
            throw new InvalidInputException(e.getMessage());
        }

        return to;
    }

    private static Set<Long> toCaseActivityIdsSet(Set<CourtActivityEntity> caseActivityEntities) {
        Set<Long> ret = new HashSet<Long>();

        if (caseActivityEntities == null) {
			return ret;
		}

        Iterator<CourtActivityEntity> it = caseActivityEntities.iterator();
        while (it.hasNext()) {
            CourtActivityEntity association = (CourtActivityEntity) it.next();
            if (association != null) {
				ret.add(association.getCaseActivityId());
			}
        }

        return ret;
    }

    public static Set<IntermittentScheduleType> toIntermittentScheduleTypeSet(Set<IntermittentScheduleEntity> from) {
        if (from == null) {
			return null;
		}
        Set<IntermittentScheduleType> to = new HashSet<IntermittentScheduleType>();
        for (IntermittentScheduleEntity intermittentSchedule : from) {
            to.add(toIntermittentScheduleType(intermittentSchedule));
        }
        return to;
    }

    public static IntermittentScheduleType toIntermittentScheduleType(IntermittentScheduleEntity from) {
        if (from == null) {
			return null;
		}
        IntermittentScheduleType to = new IntermittentScheduleType();
        to.setInterScheduleId(from.getInterScheduleId());
        to.setDayOfWeek(from.getDayOfWeek());
        to.setDayOutOfWeek(from.getDayOutOfWeek());
        to.setLocationId(from.getLocationId());
        to.setSameDay(from.getSameDay() == 1 ? Boolean.TRUE : Boolean.FALSE);
        if (!((from.getStartHour() == null) && (from.getStartMinute() == null) && (from.getStartSecond() == null))) {
            to.setStartTime(to.new TimeValue(from.getStartHour() == null ? 0 : from.getStartHour(), from.getStartMinute() == null ? 0 : from.getStartMinute(),
                    from.getStartSecond() == null ? 0 : from.getStartSecond()));
        }
        if (!((from.getEndHour() == null) && (from.getEndMinute() == null) && (from.getEndSecond() == null))) {
            to.setEndTime(to.new TimeValue(from.getEndHour() == null ? 0 : from.getEndHour(), from.getEndMinute() == null ? 0 : from.getEndMinute(),
                    from.getEndSecond() == null ? 0 : from.getEndSecond()));
        }
        to.setReportingDate(from.getReportingDate());
        to.setScheduleEndDate(from.getScheduleEndDate());
        to.setScheduleIdentification(from.getScheduleIdentification());
        return to;
    }

    public static SentenceAppealType toSentenceAppealType(SentenceAppealEntity from) {
        if (from == null) {
			return null;
		}
        SentenceAppealType to = new SentenceAppealType();
        to.setAppealStatus(from.getAppealStatus());
        to.setAppealComment(from.getAppealComment());
        return to;
    }

    public static Set<TermType> toTermTypeSet(Set<TermEntity> from) {
        if (from == null) {
			return null;
		}
        Set<TermType> to = new HashSet<TermType>();
        for (TermEntity term : from) {
            to.add(toTermType(term));
        }
        return to;
    }

    public static TermType toTermType(TermEntity from) {
        if (from == null) {
			return null;
		}
        TermType to = new TermType();
        to.setTermCategory(from.getTermCategory());
        to.setTermName(from.getTermName());
        to.setTermType(from.getTermType());
        to.setTermStatus(from.getTermStatus());
        to.setTermSequenceNo(from.getTermSequenceNo());
        to.setTermStartDate(from.getTermStartDate());
        to.setTermEndDate(from.getTermEndDate());
        to.setYears(from.getYears());
        to.setMonths(from.getMonths());
        to.setDays(from.getDays());
        to.setHours(from.getHours());

        return to;
    }

    public static BailAmountType toBailAmountType(BondPostedAmountEntity from) {
        if (from == null) {
			return null;
		}
        BailAmountType to = new BailAmountType();
        to.setBailType(from.getBailType());
        to.setBailAmount(from.getBailAmount());
        to.setChargeAssociations(AssociationHelper.toIdentifierSet(from.getChargeAssociations()));
        return to;
    }

    public static Set<BailAmountType> fromBailPostedAmountEntityToBailAmountTypeSet(Set<BailPostedAmountEntity> from) {
        if (from == null) {
			return null;
		}
        Set<BailAmountType> to = new HashSet<BailAmountType>();
        for (BailPostedAmountEntity postedAmount : from) {
            to.add(toBailAmountType(postedAmount));
        }
        return to;
    }

    public static BailAmountType toBailAmountType(BailPostedAmountEntity from) {
        if (from == null) {
			return null;
		}
        BailAmountType to = new BailAmountType();
        to.setBailType(from.getBailType());
        to.setBailAmount(from.getBailAmount());
        to.setChargeAssociations(AssociationHelper.toIdentifierSet(from.getChargeAssociations()));
        return to;
    }

    public static Set<BailAmountType> fromBailAmountEntityToBailAmountTypeSet(Set<BailAmountEntity> from) {
        if (from == null) {
			return null;
		}
        Set<BailAmountType> to = new HashSet<BailAmountType>();
        for (BailAmountEntity bailAmount : from) {
            to.add(toBailAmountType(bailAmount));
        }
        return to;
    }

    public static BailAmountType toBailAmountType(BailAmountEntity from) {
        if (from == null) {
			return null;
		}
        BailAmountType to = new BailAmountType();
        to.setBailType(from.getBailType());
        to.setBailAmount(from.getBailAmount());
        to.setChargeAssociations(AssociationHelper.toIdentifierSet(from.getChargeAssociations()));
        return to;
    }

    public static Set<NotificationLogType> toNotificationLogTypeSet(Set<NotificationLogEntity> from) {
        if (from == null) {
			return null;
		}
        Set<NotificationLogType> to = new HashSet<NotificationLogType>();
        for (NotificationLogEntity notificationLog : from) {
            to.add(toNotificationLogType(notificationLog));
        }
        return to;
    }

    public static NotificationLogType toNotificationLogType(NotificationLogEntity from) {
        if (from == null) {
			return null;
		}
        NotificationLogType to = new NotificationLogType();
        to.setNotificationDate(from.getNotificationDate());
        to.setNotificationType(from.getNotificationType());
        to.setNotificationComment(toCommentType(from.getNotificationLogId(), from.getNotificationComment()));
        to.setNotificationLocationAssociation(from.getNotificationLocationAssociation());
        to.setNotificationContact(from.getNotificationContact());
        to.setStamp(BeanHelper.toStamp(from.getStamp()));
        return to;
    }

    public static CommentType toCommentType(Long commentId, CommentEntity from) {
        if (from == null) {
			return null;
		}
        CommentType to = new CommentType();
        to.setCommentIdentification(commentId);
        to.setUserId(from.getCommentUserId());
        to.setCommentDate(from.getCommentDate());
        to.setComment(from.getCommentText());
        return to;
    }

    public static DispositionType toDispositionType(DispositionEntity from) {
        if (from == null) {
			return null;
		}
        DispositionType to = new DispositionType();
        to.setDispositionDate(from.getDispositionDate());
        to.setOrderStatus(from.getOrderStatus());
        to.setDispositionOutcome(from.getDispositionOutcome());
        to.setStamp(BeanHelper.toStamp(from.getStamp()));
        return to;
    }

    public static DispositionEntity toDispositionEntity(UserContext uc, SessionContext context, DispositionType from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        DispositionEntity to = new DispositionEntity();
        to.setDispositionDate(from.getDispositionDate());
        to.setDispositionOutcome(from.getDispositionOutcome());
        to.setOrderStatus(from.getOrderStatus());
        to.setStamp(stamp);

        return to;
    }

    public static CommentEntity toCommentEntity(CommentType from) {
        if (from == null) {
			return null;
		}

        CommentEntity to = new CommentEntity();
        to.setCommentUserId(from.getUserId());
        to.setCommentDate(from.getCommentDate());
        to.setCommentText(from.getComment());

        return to;
    }

    public static OrderConfigurationEntity toOrderConfigurationEntity(UserContext uc, SessionContext context, OrderConfigurationType orderConfiguration, String op) {

        if (orderConfiguration == null) {
			return null;
		}

        OrderConfigurationEntity to = new OrderConfigurationEntity();
        to.setOrderConfigId(orderConfiguration.getOrderConfigId());
        to.setOrderClassification(orderConfiguration.getOrderClassification());
        to.setOrderType(orderConfiguration.getOrderType());
        to.setOrderCategory(orderConfiguration.getOrderCategory());
        to.setIsHoldingOrder(orderConfiguration.getIsHoldingOrder());
        to.setIssuingAgencies(toNotificationEntitySet(uc, context, orderConfiguration.getIssuingAgencies(), op));
        to.setIsSchedulingNeeded(orderConfiguration.getIsSchedulingNeeded());
        to.setHasCharges(orderConfiguration.getHasCharges());
        to.setStartDate(orderConfiguration.getStartDate());
        to.setEndDate(orderConfiguration.getEndDate());
        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);
        to.setStamp(stamp);

        return to;
    }

    public static Set<NotificationEntity> toNotificationEntitySet(UserContext uc, SessionContext context, Set<NotificationType> from, String op) {

        if (from == null) {
			return null;
		}

        Set<NotificationEntity> to = new HashSet<NotificationEntity>();
        for (NotificationType notification : from) {
            to.add(toNotificationEntity(uc, context, notification, op));
        }

        return to;
    }

    public static NotificationEntity toNotificationEntity(UserContext uc, SessionContext context, NotificationType from, String op) {

        if (from == null) {
			return null;
		}

        NotificationEntity to = new NotificationEntity();

        StampType timestamp = from.getStamp();
        StampEntity stampEntity;
        if (timestamp == null) {
            stampEntity = BeanHelper.getCreateStamp(uc, context);
        } else {
            stampEntity = BeanHelper.getModifyStamp(uc, context, timestamp);
        }

        to.setNotificationId(from.getNotificationId());
        to.setNotificationOrganizationAssociation(from.getNotificationOrganizationAssociation());
        to.setNotificationFacilityAssociation(from.getNotificationFacilityAssociation());
        to.setIsNotificationNeeded(from.getIsNotificationNeeded());
        to.setIsNotificationConfirmed(from.getIsNotificationConfirmed());
        to.setNotificationAgencyLocationAssociations(
                toNotificationAgencyLocationAssociationSet(uc, context, from.getNotificationAgencyLocationAssociations(), stampEntity));
        to.setNotificationAgencyContacts(toNotificationAgencyContactSet(uc, context, from.getNotificationAgencyContacts(), stampEntity, op));
        to.setStamp(stampEntity);

        return to;
    }

    public static Set<NotifyAgentLocAssocEntity> toNotificationAgencyLocationAssociationSet(UserContext uc, SessionContext context, Set<Long> from, StampEntity stamp) {

        if (from == null) {
			return null;
		}

        Set<NotifyAgentLocAssocEntity> to = new HashSet<NotifyAgentLocAssocEntity>();
        for (Long assoc : from) {
            to.add(toNotifyAgentLocAssociationEntity(uc, context, assoc, stamp));
        }

        return to;
    }

    public static NotifyAgentLocAssocEntity toNotifyAgentLocAssociationEntity(UserContext uc, SessionContext context, Long from, StampEntity stamp) {

        if (from == null) {
			return null;
		}

        NotifyAgentLocAssocEntity to = new NotifyAgentLocAssocEntity();
        to.setToIdentifier(from);
        to.setStamp(stamp);

        return to;
    }

    public static Set<NotifyAgentContactEntity> toNotificationAgencyContactSet(UserContext uc, SessionContext context, Set<ContactType> notificationAgentContacts,
            StampEntity stamp, String op) {

        if (notificationAgentContacts == null) {
			return null;
		}

        Set<NotifyAgentContactEntity> to = new HashSet<NotifyAgentContactEntity>();
        for (ContactType contact : notificationAgentContacts) {
            to.add(toNotifyAgentContactEntity(uc, context, contact, stamp, op));
        }

        return to;
    }

    public static NotifyAgentContactEntity toNotifyAgentContactEntity(UserContext uc, SessionContext context, ContactType notificationAgentContact, StampEntity stamp,
            String op) {

        if (notificationAgentContact == null) {
			return null;
		}

        NotifyAgentContactEntity contact = new NotifyAgentContactEntity();
        if (CREATE.equalsIgnoreCase(op)) {
            contact.setNotifyAgentContactId(null);
        } else if (UPDATE.equalsIgnoreCase(op)) {
            contact.setNotifyAgentContactId(notificationAgentContact.getContactId());
        }
        contact.setFirstName(notificationAgentContact.getFirstName());
        contact.setLastName(notificationAgentContact.getLastName());
        contact.setAddress(notificationAgentContact.getAddress());
        contact.setCity(notificationAgentContact.getCity());
        contact.setCounty(notificationAgentContact.getCounty());
        contact.setProvinceState(notificationAgentContact.getProvinceState());
        contact.setCountry(notificationAgentContact.getCountry());
        contact.setZipCode(notificationAgentContact.getZipCode());
        contact.setPhoneNumber(notificationAgentContact.getPhoneNumber());
        contact.setFaxNumber(notificationAgentContact.getFaxNumber());
        contact.setEmail(notificationAgentContact.getEmail());
        contact.setIdentityNumber(notificationAgentContact.getIdentityNumber());
        contact.setAgentContact(notificationAgentContact.getAgentContact());
        contact.setStamp(stamp);

        return contact;
    }

    public static List<OrderConfigurationType> toOrderConfigurationTypeList(List<OrderConfigurationEntity> from) {
        if (from == null) {
			return null;
		}

        List<OrderConfigurationType> to = new ArrayList<OrderConfigurationType>();
        for (OrderConfigurationEntity entity : from) {
            to.add(toOrderConfigurationType(entity));
        }
        return to;
    }

    public static Set<OrderConfigurationType> toOrderConfigurationTypeSet(Set<OrderConfigurationEntity> from) {
        if (from == null) {
			return null;
		}

        Set<OrderConfigurationType> to = new HashSet<OrderConfigurationType>();
        for (OrderConfigurationEntity entity : from) {
            to.add(toOrderConfigurationType(entity));
        }
        return to;
    }

    public static OrderConfigurationType toOrderConfigurationType(OrderConfigurationEntity from) {

        if (from == null) {
			return null;
		}

        OrderConfigurationType to = new OrderConfigurationType();

        to.setOrderClassification(from.getOrderClassification());
        to.setOrderConfigId(from.getOrderConfigId());
        to.setOrderType(from.getOrderType());
        to.setOrderCategory(from.getOrderCategory());
        to.setIsHoldingOrder(from.getIsHoldingOrder());
        to.setIssuingAgencies(toNotificationTypeSet(from.getIssuingAgencies()));
        to.setIsSchedulingNeeded(from.getIsSchedulingNeeded());
        to.setHasCharges(from.getHasCharges());
        to.setStartDate(from.getStartDate());
        to.setEndDate(from.getEndDate());

        return to;
    }

    public static Set<NotificationType> toNotificationTypeSet(Set<NotificationEntity> from) {

        if (from == null) {
			return null;
		}

        Set<NotificationType> to = new HashSet<NotificationType>();
        for (NotificationEntity notification : from) {
            to.add(toNotificationType(notification));
        }

        return to;
    }

    public static NotificationType toNotificationType(NotificationEntity from) {

        if (from == null) {
			return null;
		}

        NotificationType to = new NotificationType();
        to.setNotificationId(from.getNotificationId());
        to.setNotificationOrganizationAssociation(from.getNotificationOrganizationAssociation());
        to.setNotificationFacilityAssociation(from.getNotificationFacilityAssociation());
        to.setIsNotificationNeeded(from.getIsNotificationNeeded());
        to.setIsNotificationConfirmed(from.getIsNotificationConfirmed());
        to.setNotificationAgencyLocationAssociations(toNotificationAgencyLocationAssociationSet(from.getNotificationAgencyLocationAssociations()));
        to.setNotificationAgencyContacts(toNotificationAgencyContacts(from.getNotificationAgencyContacts()));
        to.setStamp(BeanHelper.toStamp(from.getStamp()));

        return to;
    }

    public static Set<ContactType> toNotificationAgencyContacts(Set<NotifyAgentContactEntity> from) {
        if (from == null) {
			return null;
		}
        Set<ContactType> to = new HashSet<ContactType>();
        for (NotifyAgentContactEntity contact : from) {
            to.add(toNotifyAgentContact(contact));
        }
        return to;
    }

    public static ContactType toNotifyAgentContact(NotifyAgentContactEntity from) {
        if (from == null) {
			return null;
		}
        ContactType to = new ContactType();
        to.setContactId(from.getNotifyAgentContactId());
        to.setFirstName(from.getFirstName());
        to.setLastName(from.getLastName());
        to.setAddress(from.getAddress());
        to.setCity(from.getCity());
        to.setCounty(from.getCounty());
        to.setCity(from.getCity());
        to.setProvinceState(from.getProvinceState());
        to.setCountry(from.getCountry());
        to.setZipCode(from.getZipCode());
        to.setPhoneNumber(from.getPhoneNumber());
        to.setFaxNumber(from.getFaxNumber());
        to.setEmail(from.getEmail());
        to.setIdentityNumber(from.getIdentityNumber());
        to.setAgentContact(from.getAgentContact());
        return to;
    }

    public static Set<Long> toNotificationAgencyLocationAssociationSet(Set<NotifyAgentLocAssocEntity> from) {
        if (from == null) {
			return null;
		}

        Set<Long> to = new HashSet<Long>();
        for (NotifyAgentLocAssocEntity notificationAgencyLocationAssociation : from) {
            to.add(toNotificationAgencyLocationAssociation(notificationAgencyLocationAssociation));
        }

        return to;
    }

    public static Long toNotificationAgencyLocationAssociation(NotifyAgentLocAssocEntity from) {
        if (from == null) {
			return null;
		}

        return from.getToIdentifier();
    }

    public static SentenceKeyDateEntity toSentenceKeyDateEntity(UserContext uc, SentenceKeyDateType from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        SentenceKeyDateEntity to = new SentenceKeyDateEntity();

        to.setSupervisionId(from.getSupervisionId());
        to.setStaffId(from.getStaffId());
        to.setReviewRequired(from.getReviewRequired());
        to.setComments(toCommentEntity(from.getComment()));
        to.setStamp(stamp);
        to.setKeyDates(toKeyDateEntitySet(uc, to, from.getKeydates(), stamp));
        to.setSupervisionSentenceId(from.getSupervisionSentenceId());

        return to;
    }

    public static Set<KeyDateEntity> toKeyDateEntitySet(UserContext uc, SentenceKeyDateEntity sentenceKeyDateEntity, Set<KeyDateType> from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        Set<KeyDateEntity> to = new HashSet<KeyDateEntity>();
        for (KeyDateType keyDate : from) {
            if (keyDate != null) {
                KeyDateEntity keyDateEntity = toKeyDateEntity(uc, keyDate, stamp);
                sentenceKeyDateEntity.addKeyDate(keyDateEntity);
                to.add(keyDateEntity);
            }
        }

        return to;
    }

    public static KeyDateEntity toKeyDateEntity(UserContext uc, KeyDateType from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        KeyDateEntity to = new KeyDateEntity();

        to.setKeyDateType(from.getKeyDateType());
        to.setKeyDate(from.getKeyDate());
        to.setKeyDateAdjust(from.getKeyDateAdjust());
        to.setKeyDateOverride(from.getKeyDateOverride());
        to.setOverrideByStaffId(from.getOverrideByStaffId());
        to.setOverrideReason(from.getOverrideReason());
        to.setStamp(stamp);

        return to;
    }

    public static SentenceKeyDateType toSentenceKeyDateType(UserContext uc, Session session, SentenceKeyDateEntity from, Boolean isHistory) {
        if (from == null) {
			return null;
		}

        SentenceKeyDateType to = new SentenceKeyDateType();
        to.setSupervisionId(from.getSupervisionId());
        to.setStaffId(from.getStaffId());
        to.setReviewRequired(from.getReviewRequired());
        CommentEntity commentEntity = from.getComments();
        if (commentEntity != null) {
            to.setComment(toCommentType(from.getSupervisionId(), commentEntity));
        }
        to.setKeydates(toKeyDateTypeSet(uc, from.getKeyDates()));
        to.setSupervisionSentenceId(from.getSupervisionSentenceId());
        List<Long> aggregateGroupIds = getAggregateGroupIDs(uc, session, from.getSupervisionSentenceId(), isHistory);
        to.setAggregateGroupIds(aggregateGroupIds);
        List<Long> sentenceIds = getSentenceIDs(uc, session, from.getSupervisionSentenceId(), isHistory);
        to.setSentenceIds(sentenceIds);

        return to;
    }

    public static List<Long> getAggregateGroupIDs(UserContext uc, Session session, Long supervisionSentenceId, Boolean isHistory) {
        if (supervisionSentenceId == null) {
            return new ArrayList<Long>();
        }
        Criteria aggSentCriteria = session.createCriteria(AggregateSentenceEntity.class);
        aggSentCriteria.add(Restrictions.eq("isHistory", isHistory));
        aggSentCriteria.createCriteria("supervisionSentence", "supSent");
        aggSentCriteria.add(Restrictions.eq("supSent.supervisionSentenceId", supervisionSentenceId));
        aggSentCriteria.add(Restrictions.eq("supSent.isHistory", isHistory));
        aggSentCriteria.setProjection(Projections.property("aggregateId"));
        @SuppressWarnings("unchecked") List<Long> aggregateIds = aggSentCriteria.list();

        //		Criteria sentCriteria = session.createCriteria(AggregateSentenceAssocEntity.class);
        //		sentCriteria.createCriteria("aggregateSentence", "aggSent");
        //		sentCriteria.add(Subqueries.propertyIn("aggSent.aggregateId", aggSentCriteria));
        //		sentCriteria.setProjection(Projections.property("toIdentifier"));
        //		sentCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        //		@SuppressWarnings("unchecked")
        //		List<Long> sentenceIDs = sentCriteria.list();

        return aggregateIds;
    }

    public static List<Long> getSentenceIDs(UserContext uc, Session session, Long supervisionSentenceId, Boolean isHistory) {
        if (supervisionSentenceId == null) {
            return new ArrayList<Long>();
        }
        DetachedCriteria aggSentCriteria = DetachedCriteria.forClass(AggregateSentenceEntity.class);
        aggSentCriteria.add(Restrictions.eq("isHistory", isHistory));
        aggSentCriteria.createCriteria("supervisionSentence", "supSent");
        aggSentCriteria.add(Restrictions.eq("supSent.supervisionSentenceId", supervisionSentenceId));
        aggSentCriteria.add(Restrictions.eq("supSent.isHistory", isHistory));
        aggSentCriteria.setProjection(Projections.property("aggregateId"));

        Criteria sentCriteria = session.createCriteria(AggregateSentenceAssocEntity.class);
        sentCriteria.createCriteria("aggregateSentence", "aggSent");
        sentCriteria.add(Subqueries.propertyIn("aggSent.aggregateId", aggSentCriteria));
        sentCriteria.setProjection(Projections.property("toIdentifier"));
        sentCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        @SuppressWarnings("unchecked") List<Long> sentenceIDs = sentCriteria.list();

        return sentenceIDs;
    }

    public static SentenceKeyDateType toSentenceKeyDateType(UserContext uc, SentenceKeyDateHistEntity from) {
        if (from == null) {
			return null;
		}

        SentenceKeyDateType to = new SentenceKeyDateType();
        to.setSupervisionId(from.getSupervisionId());
        to.setStaffId(from.getStaffId());
        to.setReviewRequired(from.getReviewRequired());
        CommentEntity commentEntity = from.getComments();
        if (commentEntity != null) {
            to.setComment(toCommentType(from.getSupervisionId(), commentEntity));
        }
        to.setKeydates(toKeyDateTypeSetFromHistory(uc, from.getKeyDates()));
        to.setSupervisionSentenceId(from.getSupervisionSentenceId());
        to.setHistoryId(from.getHistoryId());

        return to;
    }

    public static List<SentenceKeyDateType> toSentenceKeyDateTypeSet(UserContext uc, List<SentenceKeyDateHistEntity> from) {
        if (from == null) {
			return null;
		}

        List<SentenceKeyDateType> to = new ArrayList<SentenceKeyDateType>();
        for (SentenceKeyDateHistEntity frm : from) {
            to.add(toSentenceKeyDateType(uc, frm));
        }

        return to;
    }

    public static List<SentenceKeyDateType> toSentenceKeyDateTypeList(UserContext uc, List<SentenceKeyDateHistEntity> from) {
        if (from == null) {
			return null;
		}

        List<SentenceKeyDateType> to = new ArrayList<SentenceKeyDateType>();
        for (SentenceKeyDateHistEntity frm : from) {
            to.add(toSentenceKeyDateType(uc, frm));
        }

        return to;
    }

    public static Set<KeyDateType> toKeyDateTypeSet(UserContext uc, Set<KeyDateEntity> from) {
        if (from == null) {
			return null;
		}

        Set<KeyDateType> to = new HashSet<KeyDateType>();
        for (KeyDateEntity keyDate : from) {
            to.add(toKeyDateType(uc, keyDate));
        }
        return to;
    }

    public static KeyDateType toKeyDateType(UserContext uc, KeyDateEntity from) {
        if (from == null) {
			return null;
		}

        KeyDateType to = new KeyDateType();
        to.setKeyDateType(from.getKeyDateType());
        to.setKeyDate(from.getKeyDate());
        to.setKeyDateAdjust(from.getKeyDateAdjust());
        to.setKeyDateOverride(from.getKeyDateOverride());
        to.setOverrideByStaffId(from.getOverrideByStaffId());
        to.setOverrideReason(from.getOverrideReason());

        return to;
    }

    public static Set<KeyDateType> toKeyDateTypeSetFromHistory(UserContext uc, Set<KeyDateHistEntity> from) {
        if (from == null) {
			return null;
		}

        Set<KeyDateType> to = new HashSet<KeyDateType>();
        for (KeyDateHistEntity keyDate : from) {
            to.add(toKeyDateType(uc, keyDate));
        }
        return to;
    }

    public static KeyDateType toKeyDateType(UserContext uc, KeyDateHistEntity from) {
        if (from == null) {
			return null;
		}

        KeyDateType to = new KeyDateType();
        to.setKeyDateType(from.getKeyDateType());
        to.setKeyDate(from.getKeyDate());
        to.setKeyDateAdjust(from.getKeyDateAdjust());
        to.setKeyDateOverride(from.getKeyDateOverride());
        to.setOverrideByStaffId(from.getOverrideByStaffId());
        to.setOverrideReason(from.getOverrideReason());

        return to;
    }

    public static void copyToHistoryKeyDate(UserContext uc, SessionContext context, Session session, SentenceKeyDateEntity from) {
        SentenceKeyDateHistEntity sentenceKeyDateHist = new SentenceKeyDateHistEntity();
        sentenceKeyDateHist.setSupervisionId(from.getSupervisionId());
        sentenceKeyDateHist.setStaffId(from.getStaffId());
        sentenceKeyDateHist.setReviewRequired(from.getReviewRequired());
        sentenceKeyDateHist.setComments(from.getComments());
        toKeyDateHistEntitySet(sentenceKeyDateHist, from.getKeyDates());
        sentenceKeyDateHist.setStamp(from.getStamp());
        sentenceKeyDateHist.setSupervisionSentenceId(from.getSupervisionSentenceId());

        session.persist(sentenceKeyDateHist);
    }

    public static Set<KeyDateHistEntity> toKeyDateHistEntitySet(SentenceKeyDateHistEntity sentenceKeyDateHist, Set<KeyDateEntity> from) {
        if (from == null) {
			return null;
		}

        Set<KeyDateHistEntity> to = new HashSet<KeyDateHistEntity>();
        for (KeyDateEntity keyDate : from) {
            KeyDateHistEntity keyDateHist = toKeyDateHistEntity(keyDate);
            sentenceKeyDateHist.addKeyDate(keyDateHist);
            to.add(keyDateHist);
        }
        return to;
    }

    public static KeyDateHistEntity toKeyDateHistEntity(KeyDateEntity from) {
        KeyDateHistEntity to = new KeyDateHistEntity();
        to.setKeyDate(from.getKeyDate());
        to.setKeyDateAdjust(from.getKeyDateAdjust());
        to.setKeyDateOverride(from.getKeyDateOverride());
        to.setKeyDateType(from.getKeyDateType());
        to.setOverrideByStaffId(from.getOverrideByStaffId());
        to.setOverrideReason(from.getOverrideReason());
        to.setStamp(from.getStamp());

        return to;
    }

    public static void addLinkToKeyDateAndSupervisionSentence(Session session, Long supervisionId, Long keyDateHistoryId, Long supervisionSentenceId) {

        SentenceKeyDateEntity sentenceKeyDateEntity = BeanHelper.findEntity(session, SentenceKeyDateEntity.class, supervisionId);
        if (sentenceKeyDateEntity != null) {
            sentenceKeyDateEntity.setSupervisionSentenceId(supervisionSentenceId);
        }

        SentenceKeyDateHistEntity sentenceKeyDateHistEntity = BeanHelper.findEntity(session, SentenceKeyDateHistEntity.class, keyDateHistoryId);
        if (sentenceKeyDateHistEntity != null) {
            sentenceKeyDateHistEntity.setSupervisionSentenceId(supervisionSentenceId);
        }
    }

    public static <T extends OrderType> OrderType toOrderType(OrderHistoryEntity orderHistoryEntity, Class<T> clazz) {
        if (orderHistoryEntity == null) {
			return null;
		}

        T order = null;

        try {
            order = clazz.newInstance();
        } catch (InstantiationException e) {
            return null;
        } catch (IllegalAccessException e) {
            return null;
        }
        order.setOrderIdentification(orderHistoryEntity.getOrderId());
        order.setOjSupervisionId(orderHistoryEntity.getOjSupervisionId());
        order.setOrderClassification(orderHistoryEntity.getOrderClassification());
        order.setOrderCategory(orderHistoryEntity.getOrderCategory());
        order.setOrderType(orderHistoryEntity.getOrderType());
        order.setOrderSubType(orderHistoryEntity.getOrderSubType());
        order.setOrderNumber(orderHistoryEntity.getOrderNumber());
        order.setOrderIssuanceDate(orderHistoryEntity.getOrderIssuanceDate());
        order.setOrderStartDate(orderHistoryEntity.getOrderStartDate());
        order.setOrderReceivedDate(orderHistoryEntity.getOrderReceivedDate());
        order.setOrderExpirationDate(orderHistoryEntity.getOrderExpirationDate());
        order.setComments(toCommentType(order.getOrderIdentification(), orderHistoryEntity.getComments()));
        order.setIsSchedulingNeeded(orderHistoryEntity.getIsSchedulingNeeded());
        order.setIsHoldingOrder(orderHistoryEntity.getIsHoldingOrder());
        order.setHasCharges(orderHistoryEntity.getHasCharges());
        order.setOrderDisposition(toOrderDisposition(orderHistoryEntity.getOrderDisposition()));

        if (orderHistoryEntity instanceof LegalOrderHistoryEntity) {
            // LegalOrderType legalOrderType = (LegalOrderType)order;
        } else if (orderHistoryEntity instanceof WarrantDetainerHistoryEntity) {
            WarrantDetainerHistoryEntity warrantDetainerHistoryEntity = (WarrantDetainerHistoryEntity) orderHistoryEntity;
            WarrantDetainerType warrantDetainerType = (WarrantDetainerType) order;

            warrantDetainerType.setAgencyToBeNotified(toNotificationTypeSetFromHistory(warrantDetainerHistoryEntity.getAgencyToBeNotified()));
            warrantDetainerType.setNotificationLog(toNotificationLogTypeSetFromHistory(warrantDetainerHistoryEntity.getNotificationLog()));
        } else if (orderHistoryEntity instanceof BailHistoryEntity) {
            BailHistoryEntity bailHistoryEntity = (BailHistoryEntity) orderHistoryEntity;
            BailType bailType = (BailType) order;

            bailType.setBailPaymentReceiptNo(bailHistoryEntity.getBailPaymentReceiptNo());
            bailType.setBailRelationship(bailHistoryEntity.getBailRelationship());
            bailType.setBailRequirement(bailHistoryEntity.getBailRequirement());
            bailType.setBondPaymentDescription(bailHistoryEntity.getBondPaymentDescription());
            bailType.setIsBailAllowed(bailHistoryEntity.getIsBailAllowed());
            bailType.setBailAmounts(toBailAmountTypeSet(bailHistoryEntity.getBailAmounts()));
            bailType.setBailPostedAmounts(toBailPostedAmountTypeSet(bailHistoryEntity.getBailPostedAmounts()));
            bailType.setBondPostedAmount(toBondPostedAmount(bailHistoryEntity.getBondPostedAmount()));

            // bailType.setBailerPersonIdentityAssociations(bailHistoryEntity.getCaseActivityInitiatedOrderAssociations());
            // -- no need implemented unless buiness requirement comes
            // bailType.setBondOrganizationAssociations(bailHistoryEntity.getCaseActivityInitiatedOrderAssociations());
            // -- no need implemented unless buiness requirement comes
        }

        return order;
    }

    public static BailAmountType toBondPostedAmount(BondPostedAmountHistEntity from) {
        if (from == null) {
			return null;
		}
        BailAmountType to = new BailAmountType();
        to.setBailType(from.getBailType());
        to.setBailAmount(from.getBailAmount());
        return to;
    }

    public static Set<BailAmountType> toBailPostedAmountTypeSet(Set<BailPostedAmountHistEntity> from) {
        if (from == null) {
			return null;
		}
        Set<BailAmountType> to = new HashSet<>();
        for (BailPostedAmountHistEntity bailPostedAmountHistEntity : from) {
            to.add(toBailPostedAmountType(bailPostedAmountHistEntity));
        }
        return to;
    }

    public static BailAmountType toBailPostedAmountType(BailPostedAmountHistEntity from) {
        if (from == null) {
			return null;
		}
        BailAmountType to = new BailAmountType();
        to.setBailType(from.getBailType());
        to.setBailAmount(from.getBailAmount());
        return to;
    }

    public static Set<BailAmountType> toBailAmountTypeSet(Set<BailAmountHistoryEntity> from) {
        if (from == null) {
			return null;
		}
        Set<BailAmountType> to = new HashSet<>();
        for (BailAmountHistoryEntity bailAmountHistoryEntity : from) {
            to.add(toBailAmountType(bailAmountHistoryEntity));
        }
        return to;
    }

    public static BailAmountType toBailAmountType(BailAmountHistoryEntity from) {
        if (from == null) {
			return null;
		}
        BailAmountType to = new BailAmountType();
        to.setBailType(from.getBailType());
        to.setBailAmount(from.getBailAmount());
        return to;
    }

    public static Set<NotificationLogType> toNotificationLogTypeSetFromHistory(Set<NotificationLogHistoryEntity> from) {
        if (from == null) {
			return null;
		}
        Set<NotificationLogType> to = new HashSet<>();
        for (NotificationLogHistoryEntity notificationLogHistoryEntity : from) {
            to.add(toNotificationLogTypeFromHistory(notificationLogHistoryEntity));
        }
        return to;
    }

    public static NotificationLogType toNotificationLogTypeFromHistory(NotificationLogHistoryEntity from) {
        if (from == null) {
			return null;
		}
        NotificationLogType to = new NotificationLogType();
        to.setNotificationContact(from.getNotificationContact());
        to.setNotificationDate(from.getNotificationDate());
        to.setNotificationLocationAssociation(from.getNotificationLocationAssociation());
        to.setNotificationType(from.getNotificationType());
        to.setNotificationComment(toCommentType(from.getNotificationLogId(), from.getNotificationComment()));
        to.setStamp(BeanHelper.toStamp(from.getStamp()));
        return to;
    }

    public static Set<NotificationType> toNotificationTypeSetFromHistory(Set<NotificationHistoryEntity> from) {
        if (from == null) {
			return null;
		}
        Set<NotificationType> to = new HashSet<>();
        for (NotificationHistoryEntity notificationHistoryEntity : from) {
            to.add(toNotificationType(notificationHistoryEntity));
        }
        return to;
    }

    public static NotificationType toNotificationType(NotificationHistoryEntity from) {
        if (from == null) {
			return null;
		}
        NotificationType to = new NotificationType();
        to.setIsNotificationConfirmed(from.getIsNotificationConfirmed());
        to.setIsNotificationNeeded(from.getIsNotificationNeeded());
        to.setNotificationFacilityAssociation(from.getNotificationFacilityAssociation());
        to.setNotificationOrganizationAssociation(from.getNotificationOrganizationAssociation());
        to.setNotificationAgencyContacts(toContactTypeSet(from.getNotificationAgencyContacts()));
        to.setStamp(BeanHelper.toStamp(from.getStamp()));
        return to;
    }

    public static Set<ContactType> toContactTypeSet(Set<NotifyAgentContactHistEntity> from) {
        if (from == null) {
			return null;
		}

        Set<ContactType> to = new HashSet<>();
        for (NotifyAgentContactHistEntity notifyAgentContactHistEntity : from) {
            to.add(toContactType(notifyAgentContactHistEntity));
        }
        return to;
    }

    public static ContactType toContactType(NotifyAgentContactHistEntity from) {
        if (from == null) {
			return null;
		}
        ContactType to = new ContactType();
        to.setContactId(from.getNotifyAgentContactId());
        to.setFirstName(from.getFirstName());
        to.setLastName(from.getLastName());
        to.setAddress(from.getAddress());
        to.setCity(from.getCity());
        to.setCounty(from.getCounty());
        to.setProvinceState(from.getProvinceState());
        to.setCountry(from.getCountry());
        to.setZipCode(from.getZipCode());
        to.setPhoneNumber(from.getPhoneNumber());
        to.setFaxNumber(from.getFaxNumber());
        to.setEmail(from.getEmail());
        to.setIdentityNumber(from.getIdentityNumber());
        to.setAgentContact(from.getAgentContact());

        return to;
    }

    public static DispositionType toOrderDisposition(DispositionHistoryEntity from) {
        if (from == null) {
			return null;
		}
        DispositionType to = new DispositionType();
        to.setOrderStatus(from.getOrderStatus());
        to.setDispositionOutcome(from.getDispositionOutcome());
        to.setDispositionDate(from.getDispositionDate());
        to.setStamp(BeanHelper.toStamp(from.getStamp()));
        return to;
    }

    public static <T extends OrderEntity> void storeToHistoryEntity(Session session, T entity) {
        if (entity instanceof BailEntity) {
            BailHistoryEntity bailHistoryEntity = OrderSentenceHelper.copyToOrderHistoryEntity((BailEntity) entity, BailHistoryEntity.class);
            if (bailHistoryEntity != null) {
                session.save(bailHistoryEntity);
            }
        } else if (entity instanceof WarrantDetainerEntity) {
            WarrantDetainerHistoryEntity warrantDetainerHistoryEntityEntity = OrderSentenceHelper.copyToOrderHistoryEntity((WarrantDetainerEntity) entity,
                    WarrantDetainerHistoryEntity.class);
            if (warrantDetainerHistoryEntityEntity != null) {
                session.save(warrantDetainerHistoryEntityEntity);
            }
        } else if (entity instanceof LegalOrderEntity) {
            LegalOrderHistoryEntity legalOrderHisEntity = OrderSentenceHelper.copyToOrderHistoryEntity((LegalOrderEntity) entity, LegalOrderHistoryEntity.class);
            if (legalOrderHisEntity != null) {
                session.save(legalOrderHisEntity);
            }
        }
    }

    public static <U extends OrderEntity, V extends OrderHistoryEntity> V copyToOrderHistoryEntity(U from, Class<V> toOrderHistoryEntityClazz) {
        V historyEntity = null;
        try {
            historyEntity = toOrderHistoryEntityClazz.newInstance();

            historyEntity.setOrderId(from.getOrderId());
            historyEntity.setOjSupervisionId(from.getOjSupervisionId());
            historyEntity.setOrderClassification(from.getOrderClassification());
            historyEntity.setOrderCategory(from.getOrderCategory());
            historyEntity.setOrderType(from.getOrderType());
            historyEntity.setOrderSubType(from.getOrderSubType());
            historyEntity.setOrderNumber(from.getOrderNumber());
            historyEntity.setComments(from.getComments());
            historyEntity.setOrderIssuanceDate(from.getOrderIssuanceDate());
            historyEntity.setOrderStartDate(from.getOrderStartDate());
            historyEntity.setOrderReceivedDate(from.getOrderReceivedDate());
            historyEntity.setOrderExpirationDate(from.getOrderExpirationDate());
            historyEntity.setIsHoldingOrder(from.getIsHoldingOrder());
            historyEntity.setIsSchedulingNeeded(from.getIsSchedulingNeeded());
            historyEntity.setHasCharges(from.getHasCharges());
            historyEntity.setStamp(from.getStamp());

            // disposition
            DispositionEntity dispositionEntity = from.getOrderDisposition();
            DispositionHistoryEntity dispositionHistoryEntity = new DispositionHistoryEntity();
            dispositionHistoryEntity.setDispositionId(dispositionEntity.getDispositionId());
            dispositionHistoryEntity.setOrderStatus(dispositionEntity.getOrderStatus());
            dispositionHistoryEntity.setDispositionOutcome(dispositionEntity.getDispositionOutcome());
            dispositionHistoryEntity.setDispositionDate(dispositionEntity.getDispositionDate());
            dispositionHistoryEntity.setStamp(dispositionEntity.getStamp());
            historyEntity.setOrderDisposition(dispositionHistoryEntity);

            if (from instanceof WarrantDetainerEntity) {
                WarrantDetainerEntity warrantDetainerEntity = (WarrantDetainerEntity) from;
                WarrantDetainerHistoryEntity warrantDetainerHistoryEntity = (WarrantDetainerHistoryEntity) historyEntity;

                warrantDetainerHistoryEntity.setAgencyToBeNotified(toNotificationHistoryEntitySet(warrantDetainerEntity.getAgencyToBeNotified()));
                warrantDetainerHistoryEntity.setNotificationLog(toNotificationLogHistoryEntitySet(warrantDetainerEntity.getNotificationLog()));
            } else if (from instanceof BailEntity) {
                BailEntity bailEntity = (BailEntity) from;
                BailHistoryEntity bailHistoryEntity = (BailHistoryEntity) historyEntity;

                bailHistoryEntity.setBailRequirement(bailEntity.getBailRequirement());
                bailHistoryEntity.setBailRelationship(bailEntity.getBailRelationship());
                bailHistoryEntity.setBailPaymentReceiptNo(bailEntity.getBailPaymentReceiptNo());
                bailHistoryEntity.setIsBailAllowed(bailEntity.getIsBailAllowed());
                bailHistoryEntity.setBondPaymentDescription(bailEntity.getBondPaymentDescription());

                bailHistoryEntity.setBailAmounts(bailAmountHistoryEntitySet(bailEntity.getBailAmounts()));
                bailHistoryEntity.setBailPostedAmounts(toBailPostedAmountHistEntitySet(bailEntity.getBailPostedAmounts()));
                bailHistoryEntity.setBondPostedAmount(toBondPostedAmountHistEntity(bailEntity.getBondPostedAmount()));

                bailHistoryEntity.setBailerPersonIdentities(toBailPersonIdAssocHistEntitySet(bailEntity.getBailerPersonIdentities()));
                bailHistoryEntity.setBondOrganizations(toBondOrgAssocHistEntitySet(bailEntity.getBondOrganizations()));
            }

            return historyEntity;
        } catch (IllegalAccessException e) {
        } catch (InstantiationException e) {
        }

        return null;
    }

    public static Set<NotificationLogHistoryEntity> toNotificationLogHistoryEntitySet(Set<NotificationLogEntity> from) {
        if (from == null) {
			return null;
		}
        Set<NotificationLogHistoryEntity> to = new HashSet<>();
        for (NotificationLogEntity notificationLogEntity : from) {
            to.add(toNotificationLogHistoryEntity(notificationLogEntity));
        }
        return to;
    }

    public static NotificationLogHistoryEntity toNotificationLogHistoryEntity(NotificationLogEntity from) {
        if (from == null) {
			return null;
		}
        NotificationLogHistoryEntity to = new NotificationLogHistoryEntity();
        to.setNotificationLogId(from.getNotificationLogId());
        to.setNotificationType(from.getNotificationType());
        to.setNotificationDate(from.getNotificationDate());
        to.setNotificationContact(from.getNotificationContact());
        to.setNotificationLocationAssociation(from.getNotificationLocationAssociation());
        to.setNotificationComment(from.getNotificationComment());
        to.setStamp(from.getStamp());
        return to;
    }

    public static Set<NotificationHistoryEntity> toNotificationHistoryEntitySet(Set<NotificationEntity> from) {
        if (from == null) {
			return null;
		}
        Set<NotificationHistoryEntity> to = new HashSet<>();
        for (NotificationEntity notificationEntity : from) {
            to.add(toNotificationHistoryEntity(notificationEntity));
        }
        return to;
    }

    public static NotificationHistoryEntity toNotificationHistoryEntity(NotificationEntity from) {
        if (from == null) {
			return null;
		}
        NotificationHistoryEntity to = new NotificationHistoryEntity();
        to.setNotificationId(from.getNotificationId());
        to.setIsNotificationConfirmed(from.getIsNotificationConfirmed());
        to.setIsNotificationNeeded(from.getIsNotificationNeeded());
        to.setNotificationFacilityAssociation(from.getNotificationFacilityAssociation());
        to.setNotificationOrganizationAssociation(from.getNotificationOrganizationAssociation());
        to.setNotificationAgencyContacts(toNotificationAgencyContactEntitySet(from.getNotificationAgencyContacts()));
        to.setStamp(from.getStamp());
        return to;
    }

    public static Set<NotifyAgentContactHistEntity> toNotificationAgencyContactEntitySet(Set<NotifyAgentContactEntity> from) {
        if (from == null) {
			return null;
		}
        Set<NotifyAgentContactHistEntity> to = new HashSet<>();
        for (NotifyAgentContactEntity notifyAgentContactEntity : from) {
            to.add(toNotificationAgencyContactEntity(notifyAgentContactEntity));
        }
        return to;
    }

    public static NotifyAgentContactHistEntity toNotificationAgencyContactEntity(NotifyAgentContactEntity from) {
        if (from == null) {
			return null;
		}
        NotifyAgentContactHistEntity to = new NotifyAgentContactHistEntity();
        to.setNotifyAgentContactId(from.getNotifyAgentContactId());
        to.setFirstName(from.getFirstName());
        to.setLastName(from.getLastName());
        to.setAddress(from.getAddress());
        to.setCity(from.getCity());
        to.setCounty(from.getCounty());
        to.setProvinceState(from.getProvinceState());
        to.setCountry(from.getCountry());
        to.setZipCode(from.getZipCode());
        to.setPhoneNumber(from.getPhoneNumber());
        to.setFaxNumber(from.getFaxNumber());
        to.setEmail(from.getEmail());
        to.setIdentityNumber(from.getIdentityNumber());
        to.setAgentContact(from.getAgentContact());
        return to;
    }

    public static Set<BondOrgAssocHistEntity> toBondOrgAssocHistEntitySet(Set<BondOrgAssocEntity> from) {
        if (from == null) {
			return null;
		}
        Set<BondOrgAssocHistEntity> to = new HashSet<>();
        for (BondOrgAssocEntity bondOrgAssocEntity : from) {
            to.add(toBondOrgAssocHistEntity(bondOrgAssocEntity));
        }
        return to;
    }

    public static BondOrgAssocHistEntity toBondOrgAssocHistEntity(BondOrgAssocEntity from) {
        if (from == null) {
			return null;
		}
        BondOrgAssocHistEntity to = new BondOrgAssocHistEntity();
        to.setAssociationId(from.getAssociationId());
        to.setToClass(from.getToClass());
        to.setToIdentifier(from.getToIdentifier());
        to.setStamp(from.getStamp());
        return to;
    }

    public static Set<BailPersonIdAssocHistEntity> toBailPersonIdAssocHistEntitySet(Set<BailPersonIdAssocEntity> from) {
        if (from == null) {
			return null;
		}
        Set<BailPersonIdAssocHistEntity> to = new HashSet<>();
        for (BailPersonIdAssocEntity bailPersonIdAssocEntity : from) {
            to.add(toBailPersonIdAssocHistEntity(bailPersonIdAssocEntity));
        }
        return to;
    }

    public static BailPersonIdAssocHistEntity toBailPersonIdAssocHistEntity(BailPersonIdAssocEntity from) {
        if (from == null) {
			return null;
		}
        BailPersonIdAssocHistEntity to = new BailPersonIdAssocHistEntity();
        to.setAssociationId(from.getAssociationId());
        to.setToClass(from.getToClass());
        to.setToIdentifier(from.getToIdentifier());
        to.setStamp(from.getStamp());
        return to;
    }

    public static BondPostedAmountHistEntity toBondPostedAmountHistEntity(BondPostedAmountEntity from) {
        if (from == null) {
			return null;
		}
        BondPostedAmountHistEntity to = new BondPostedAmountHistEntity();
        to.setBailAmountId(from.getBailAmountId());
        to.setBailAmount(from.getBailAmount());
        to.setBailType(from.getBailType());
        to.setStamp(from.getStamp());
        return to;
    }

    public static Set<BailPostedAmountHistEntity> toBailPostedAmountHistEntitySet(Set<BailPostedAmountEntity> from) {
        if (from == null) {
			return null;
		}
        Set<BailPostedAmountHistEntity> to = new HashSet<>();
        for (BailPostedAmountEntity bailPostedAmountEntity : from) {
            to.add(toBailPostedAmountHistEntity(bailPostedAmountEntity));
        }
        return to;
    }

    public static BailPostedAmountHistEntity toBailPostedAmountHistEntity(BailPostedAmountEntity from) {
        if (from == null) {
			return null;
		}
        BailPostedAmountHistEntity to = new BailPostedAmountHistEntity();
        to.setBailAmountId(from.getBailAmountId());
        to.setBailType(from.getBailType());
        to.setBailAmount(from.getBailAmount());
        to.setStamp(from.getStamp());
        return to;
    }

    public static Set<BailAmountHistoryEntity> bailAmountHistoryEntitySet(Set<BailAmountEntity> from) {
        if (from == null) {
			return null;
		}
        Set<BailAmountHistoryEntity> bailAmountHistoryEntitySet = new HashSet<>();
        for (BailAmountEntity bailAmountEntity : from) {
            bailAmountHistoryEntitySet.add(toBailAmountHistoryEntity(bailAmountEntity));
        }
        return bailAmountHistoryEntitySet;
    }

    public static BailAmountHistoryEntity toBailAmountHistoryEntity(BailAmountEntity bailAmountEntity) {
        if (bailAmountEntity == null) {
			return null;
		}
        BailAmountHistoryEntity bailAmountHistoryEntity = new BailAmountHistoryEntity();
        bailAmountHistoryEntity.setBailAmountId(bailAmountEntity.getBailAmountId());
        bailAmountHistoryEntity.setBailType(bailAmountEntity.getBailType());
        bailAmountHistoryEntity.setBailAmount(bailAmountEntity.getBailAmount());
        bailAmountHistoryEntity.setStamp(bailAmountEntity.getStamp());
        return bailAmountHistoryEntity;
    }

    public static List<SupervisionSentenceType> toSupervisionSentenceTypeList(UserContext uc, SessionContext context, Session session,
            List<SupervisionSentenceEntity> from) {
        if (from == null) {
			return null;
		}
        List<SupervisionSentenceType> to = new ArrayList<>();
        for (SupervisionSentenceEntity frm : from) {
            to.add(toSupervisionSentenceType(uc, context, session, frm));
        }
        return to;
    }

    public static SupervisionSentenceType toSupervisionSentenceType(UserContext uc, SessionContext context, Session session, SupervisionSentenceEntity from) {
        if (from == null) {
			return null;
		}

        Long supervisionId = from.getSupervisionId();
        SupervisionSentenceType to = new SupervisionSentenceType();
        to.setSupervisionSentenceId(from.getSupervisionSentenceId());
        to.setSupervisionId(from.getSupervisionId());
        to.setSentenceStartDate(from.getSentenceStartDate());
        to.setSentenceExpiryDate(from.getSentenceExpiryDate());
        to.setGoodTime(from.getGoodTime());
        to.setParoleEligibilityDate(from.getParoleEligibilityDate());
        to.setProbableDischargeDate(from.getProbableDischargeDate());
        to.setSentenceDays(from.getSentenceDays());
        to.setIsHistory(from.getIsHistory());
        to.setKeyDateHistoryId(from.getKeyDateHistoryId());

        Date startDate = from.getSentenceStartDate();
        Long sentenceDays = from.getSentenceDays();
        Long daysToServe = 0L;
        Date today = new Date();
        if (startDate != null && startDate.before(today)) {
            Long served = BeanHelper.getDays(startDate, today);
            daysToServe = sentenceDays - served;
            if (daysToServe <= 0L) {
				daysToServe = 0L;
			}
        } else {
            daysToServe = sentenceDays;
        }
        to.setDaysToServe(daysToServe);
        List<AggregateSentenceGroupType> aggregateSentenceTypeList = toAggregateSentenceTypeList(uc, context, session, supervisionId, from.getAggregateSentences());
        if (aggregateSentenceTypeList != null && !aggregateSentenceTypeList.isEmpty()) {
            to.setAggregateSentenceGroups(aggregateSentenceTypeList);
        } else {
            to.setAggregateSentenceGroups(new ArrayList<AggregateSentenceGroupType>());
        }

        return to;
    }

    public static List<AggregateSentenceGroupType> toAggregateSentenceTypeList(UserContext uc, SessionContext context, Session session, Long supervisionId,
            List<AggregateSentenceEntity> from) {
        if (from == null) {
			return null;
		}

        List<AggregateSentenceGroupType> to = new ArrayList<>();
        for (AggregateSentenceEntity frm : from) {
            AggregateSentenceGroupType aggregateSentenceGroupType = toAggregateSentenceType(uc, context, session, supervisionId, frm);
            if (aggregateSentenceGroupType != null) {
                to.add(aggregateSentenceGroupType);
            }
        }

        return to;
    }

    public static AggregateSentenceGroupType toAggregateSentenceType(UserContext uc, SessionContext context, Session session, Long supervisionId,
            AggregateSentenceEntity from) {
        if (from == null) {
			return null;
		}

        Set<AggregateSentenceAssocEntity> aggregateSentenceAssocEntityList = from.getSentenceAssocs();
        if (aggregateSentenceAssocEntityList == null || aggregateSentenceAssocEntityList.size() == 0) {
			return null;
		}

        AggregateSentenceGroupType to = new AggregateSentenceGroupType();
        to.setSupervisionId(supervisionId);
        to.setAggregateGroupId(from.getAggregateId());
        to.setGoodTime(from.getGoodTime());
        to.setDaysToServe(from.getDaysToServe());
        to.setPrdDate(from.getPrdDate());
        to.setStartDate(from.getStartDate());
        to.setEndDate(from.getEndDate());
        to.setTotalSentenceDays(from.getTotalSentenceDays());
        to.setStaffId(from.getStaffId());
        to.setComment(from.getComments());
        to.setCalculatingDate(from.getCalculatingDate());
        to.setAggregateSentences(toAggregateSentenceTypeList(uc, context, session, new ArrayList<AggregateSentenceAssocEntity>(aggregateSentenceAssocEntityList)));
        if (from.getIsControllingSentence() == null) {
            to.setIsControlling(Boolean.FALSE);
        } else {
            to.setIsControlling(from.getIsControllingSentence());
        }
        if (from.getSupervisionSentence() != null) {
            Long supervisionSentenceId = from.getSupervisionSentence().getSupervisionSentenceId();
            to.setSupervisionSentenceId(supervisionSentenceId);
        }

        return to;
    }

    public static List<AggregateSentenceType> toAggregateSentenceTypeList(UserContext uc, SessionContext context, Session session,
            List<AggregateSentenceAssocEntity> from) {
        if (from == null) {
			return null;
		}

        List<AggregateSentenceType> to = new ArrayList<>();
        for (AggregateSentenceAssocEntity frm : from) {
            AggregateSentenceType t = new AggregateSentenceType();
            t.setAggregateId(frm.getAssociationId());
            t.setSupervisionId(frm.getSupervisionId());
            t.setGoodTime(frm.getGoodTime());
            t.setSentenceDays(frm.getSentenceDays());
            t.setPrdDate(frm.getPrdDate());
            t.setStartDate(frm.getStartDate());
            t.setEndDate(frm.getEndDate());
            t.setExpiryDate(frm.getExpiryDate());
            t.setYears(frm.getYears());
            t.setMonths(frm.getMonths());
            t.setWeeks(frm.getWeeks());
            t.setDays(frm.getDays());
            t.setStaffId(frm.getStaffId());
            t.setComment(frm.getComments());
            t.setCalculatingDate(frm.getCalculatingDate());
            t.setSentence(toOrder(uc, context, BeanHelper.findEntity(session, SentenceEntity.class, frm.getToIdentifier()), SentenceType.class));
            if (frm.getIsControllingSentence() == null) {
                t.setIsControllingSentence(Boolean.FALSE);
            } else {
                t.setIsControllingSentence(frm.getIsControllingSentence());
            }
            to.add(t);
        }

        return to;
    }

    public static <T extends OrderType> void verifyOrder(UserContext uc, SessionContext context, Session session, T order) {
        if (log.isDebugEnabled()) {
			log.debug("verifyWarrant: begin");
		}

        // Cases to which the order is to be associated must exist.
        if (OrderSentenceHandler.OrderCategory.IJ.name().equalsIgnoreCase(order.getOrderCategory()) && !BeanHelper.bExistedModules(session, CaseInfoEntity.class,
                order.getCaseInfoIds())) {
            throw new InvalidInputException("Invalid argument: Case Info Ids: " + order.getCaseInfoIds());
        }

        // Cases Dates must before Order Dates
        verifyOrderDates(session, order);

        // Charges to which the order is to be associated must exist.
        Set<Long> chargeIds = order.getChargeIds();
        if (!BeanHelper.isEmpty(chargeIds)) {
            if (!BeanHelper.bExistedModules(session, ChargeEntity.class, chargeIds)) {
                throw new InvalidInputException("Invalid argument: Charge Ids: " + chargeIds);
            }
        }
        // Conditions to which the order is to be associated must exist.
        Set<Long> conditionIds = order.getConditionIds();
        if (!BeanHelper.isEmpty(conditionIds)) {
            if (!BeanHelper.bExistedModules(session, ConditionEntity.class, conditionIds)) {
                throw new InvalidInputException("Invalid argument: Condition Ids: " + conditionIds);
            }
        }

        if (order instanceof WarrantDetainerType) {
            // If IsNotificationNeeded flag is set to true, at least one 'Notify
            // organization/facility' must be provided for the
            // AgencyToBeNotified.
            Set<NotificationType> notifications = ((WarrantDetainerType) order).getAgencyToBeNotified();
            if (notifications != null) {
                for (NotificationType notif : notifications) {
                    if (Boolean.TRUE.equals(notif.getIsNotificationNeeded())) {
                        if (notif.getNotificationOrganizationAssociation() == null || notif.getNotificationFacilityAssociation() == null) {
                            throw new InvalidInputException("Invalid argument: notif: " + notif);
                        }
                        ValidationHelper.validate(notif);
                        ValidationHelper.validate(notif.getNotificationFacilityAssociation());
                    }
                }
            }
        }

        verifyOrderConfiguration(session, order);

        // Verify Reference Code Links
        verifyReferenceCodeLinks(uc, order);

        if (log.isDebugEnabled()) {
			log.debug("verifyWarrant: end");
		}
    }

    public static <T extends OrderType> void verifyOrderDates(Session session, T order) {
        // Cases Dates must be before Order Dates
        Criteria caseCriteria = session.createCriteria(CaseInfoEntity.class);
        if (order.getCaseInfoIds() != null && order.getCaseInfoIds().size() > 0) {
            caseCriteria.add(Restrictions.in("caseInfoId", order.getCaseInfoIds()));
        } else {
            return;
        }
        @SuppressWarnings("unchecked") List<CaseInfoEntity> casesInfoEntities = caseCriteria.list();
        Date caseDate = null;
        for (CaseInfoEntity ci : casesInfoEntities) {
            if (ci.getIssuedDate() == null) {
				continue;
			}

            if (caseDate == null) {
				caseDate = ci.getIssuedDate();
			} else if (caseDate.before(ci.getIssuedDate())) {
                caseDate = ci.getIssuedDate();
            }
        }
        if (caseDate == null) {
			return;
		}

        if (!(order instanceof SentenceType)) {
            if ((order.getOrderIssuanceDate() != null && caseDate.after(order.getOrderIssuanceDate())) || (order.getOrderStartDate() != null && caseDate.after(
                    order.getOrderStartDate())) || (order.getOrderReceivedDate() != null && caseDate.after(order.getOrderReceivedDate()))) {
                throw new InvalidInputException(
                        "Invalid argument: Case date must be before order date: case date: " + caseDate + ", order start date: " + order.getOrderStartDate()
                                + ", order issuance date: " + order.getOrderIssuanceDate() + ", order received date: " + order.getOrderReceivedDate(),
                        ErrorCodes.LEG_CREATE_ORDER);
            }
        }
        /**
         * Sentence start Date might be before Case Date -- confirmed with Elsia
         * Soon, July 24, 2013. if (order instanceof SentenceType) { Date
         * sentenceStartDate = ((SentenceType) order).getSentenceStartDate(); if
         * (sentenceStartDate != null && caseDate.after(sentenceStartDate)) {
         * throw new InvalidInputException(
         * "Invalid argument: Case date must be before order date: case date: "
         * + caseDate + ", sentence start date: " + ((SentenceType)
         * order).getSentenceStartDate()); } }
         **/
    }

    public static void removeSentenceIdFromAssociatedCharge(UserContext uc, Session session, Long sentenceId) {
        Criteria c = session.createCriteria(ChargeEntity.class);
        c.add(Restrictions.eq("sentenceId", sentenceId));
        @SuppressWarnings("unchecked") Iterator<ChargeEntity> it = c.list().iterator();
        while (it.hasNext()) {
            ChargeEntity chargeEntity = it.next();
            chargeEntity.setSentenceId(null);
            Set<ChargeModuleAssociationEntity> modules = chargeEntity.getModuleAssociations();
            if (modules != null) {
                Iterator<ChargeModuleAssociationEntity> itModule = modules.iterator();
                while (itModule.hasNext()) {
                    ChargeModuleAssociationEntity chargeModuleAssociationEntity = itModule.next();
                    if (LegalModule.ORDER_SENTENCE.value().equalsIgnoreCase(chargeModuleAssociationEntity.getToClass())
                            && chargeModuleAssociationEntity.getToIdentifier() == sentenceId) {
                        itModule.remove();
                    }
                }
            }
        }
    }

    public static void removeSentenceFromAggregate(UserContext uc, Session session, OrderEntity sentence) {
        Long sentenceId = sentence.getOrderId();
        DetachedCriteria caseInfoCriteria = DetachedCriteria.forClass(CaseInfoEntity.class);
        caseInfoCriteria.add(Restrictions.eq("active", Boolean.TRUE));
        caseInfoCriteria.createCriteria("moduleAssociations", "module");
        caseInfoCriteria.add(Restrictions.ilike("module.toClass", LegalModule.ORDER_SENTENCE.value()));
        caseInfoCriteria.add(Restrictions.eq("module.toIdentifier", sentenceId));
        caseInfoCriteria.setProjection(Projections.property("supervisionId"));

        Criteria aggregateSentenceCriteria = session.createCriteria(AggregateSentenceEntity.class);
        aggregateSentenceCriteria.add(Subqueries.propertyIn("supervisionId", caseInfoCriteria));
        aggregateSentenceCriteria.add(Restrictions.eq("isHistory", Boolean.FALSE));
        aggregateSentenceCriteria.createAlias("sentenceAssocs", "sentence");
        aggregateSentenceCriteria.add(Restrictions.eq("sentence.toIdentifier", sentenceId));
        @SuppressWarnings("unchecked") List<AggregateSentenceEntity> aggSents = aggregateSentenceCriteria.list();
        for (AggregateSentenceEntity agg : aggSents) {
            Set<AggregateSentenceAssocEntity> sentAssocs = agg.getSentenceAssocs();
            Iterator<AggregateSentenceAssocEntity> itSent = sentAssocs.iterator();
            while (itSent.hasNext()) {
                AggregateSentenceAssocEntity s = itSent.next();
                if (s.getToIdentifier() == sentenceId) {
                    itSent.remove();
                }
            }
        }
    }

    public static <T extends OrderType> void verifyReferenceCodeLinks(UserContext uc, T order) {
        // Verify Reference Code Links
        CodeType dispositionOutcome = new CodeType(MetaSet.ORDER_OUTCOME.toUpperCase(), order.getOrderDisposition().getDispositionOutcome().toUpperCase());
        CodeType orderDtatus = new CodeType(MetaSet.ORDER_STATUS.toUpperCase(), order.getOrderDisposition().getOrderStatus().toUpperCase());
        List<LinkCodeType> linksRet = getRefDataService().getLinks(uc, dispositionOutcome, true);

        if (linksRet == null) {
            log.error("order.getOrderDisposition() = " + order.getOrderDisposition());
            throw new InvalidInputException("Invalid argument: Reference Codes and Links do not match.");
        }

        Set<CodeType> linkCodes = new HashSet<CodeType>();
        linkCodes.add(dispositionOutcome);
        linkCodes.add(orderDtatus);
        getRefDataService().doesExistReferenceCode(linkCodes);

    }

    public static Set<CodeType> getLinkCodes(UserContext uc, CodeType code, String refSet) {
        CodeType refCode = new CodeType(code.getSet().toUpperCase(), code.getCode().toUpperCase());
        List<LinkCodeType> linksRet = getRefDataService().getLinks(uc, refCode, true);

        if (linksRet == null) {
            log.error("get links by CodeType() = " + refCode);
            throw new InvalidInputException("Invalid argument: Failure to retrieve Reference Codes Links.");
        }

        Set<CodeType> ret = new HashSet<CodeType>();

        for (LinkCodeType link : linksRet) {
            CodeType code1 = new CodeType(link.getReferenceSet(), link.getReferenceCode());
            CodeType code2 = new CodeType(link.getLinkedReferenceSet(), link.getLinkedReferenceCode());
            if (code1.equals(refCode) && code2.getSet().equals(refSet.toUpperCase())) {
                ret.add(new CodeType(refSet.toUpperCase(), code2.getCode()));
            } else if (code2.equals(refCode) && code1.getSet().equals(refSet.toUpperCase())) {
                ret.add(new CodeType(refSet.toUpperCase(), code1.getCode()));
            }
        }
        return ret;
    }

    public static CodeType getLinkCode(UserContext uc, CodeType code, String refSet) {
        Set<CodeType> ret = getLinkCodes(uc, code, refSet);
        if (ret == null || ret.size() != 1) {
			return null;
		} else {
			return (CodeType) ret.toArray()[0];
		}
    }

    /**
     * Based on the 'OrderClassification', 'OrderType' and 'OrderCategory' field
     * values must be based on the ones set in OrderConfiguration
     *
     * @param session
     * @param order
     */
    public static void verifyOrderConfiguration(Session session, OrderType order) {
        if (log.isDebugEnabled()) {
			log.debug("verifyOrderConfiguration: begin");
		}

        if (OrderSentenceHandler.OrderCategory.OJ.name().equalsIgnoreCase(order.getOrderCategory().toUpperCase())) {
			return;
		}

        String functionName = "verifyOrderConfiguration";

        // Based on the 'OrderClassification', 'OrderType' and 'OrderCategory'
        // field values
        // must be based on the ones set in OrderConfiguration
        Criteria c = session.createCriteria(OrderConfigurationEntity.class);
        c.add(Restrictions.eq("orderClassification", order.getOrderClassification()));
        c.add(Restrictions.eq("orderType", order.getOrderType()));
        c.add(Restrictions.eq("orderCategory", order.getOrderCategory()));

        // c.setProjection(Projections.rowCount());
        c.setProjection(Projections.countDistinct("orderConfigId"));
        Long count = (Long) c.uniqueResult();
        if (count == null || count.equals(Long.valueOf(0L))) {
            String message = "Invalid argument: orderClassification = " + order.getOrderClassification() + ", orderType = " + order.getOrderType() + ", orderCategory = "
                    + order.getOrderCategory();
            LogHelper.error(log, functionName, message);
            throw new DataNotExistException(message);
        }
        if (log.isDebugEnabled()) {
			log.debug("verifyOrderConfiguration: end");
		}
    }

    public static void verifyOrderConfigurationCodeType(Session session, OrderConfigurationType orderConfiguration) {
        if (log.isDebugEnabled()) {
			log.debug("verifyOrderConfigurationCodeType: begin");
		}
        if (orderConfiguration == null) {
			return;
		}

        Set<CodeType> codes = new HashSet<CodeType>();
        if (orderConfiguration.getOrderClassification() != null) {
			codes.add(new CodeType(ReferenceSet.ORDER_CLASSIFICATION.value(), orderConfiguration.getOrderClassification()));
		}
        if (orderConfiguration.getOrderType() != null) {
			codes.add(new CodeType(ReferenceSet.ORDER_TYPE.value(), orderConfiguration.getOrderType()));
		}
        if (orderConfiguration.getOrderCategory() != null) {
			codes.add(new CodeType(ReferenceSet.ORDER_CATEGORY.value(), orderConfiguration.getOrderCategory()));
		}

        ReferenceDataHelper.isActiveReferenceCode(codes);
        if (log.isDebugEnabled()) {
			log.debug("verifyOrderConfigurationCodeType: end");
		}
    }

    public static void verifySentenceType(UserContext uc, Session session, SentenceType sentence) {
        Set<CodeType> terms = getLinkCodes(uc, new CodeType(ReferenceSet.SENTENCE_TYPE.value(), sentence.getSentenceType()), ReferenceSet.TERM_TYPE.value());

        if (terms.contains(new CodeType(ReferenceSet.TERM_TYPE.value(), TermTypes.SINGLE.name()))) {
            // Single Term
            Set<TermType> sentenceTerms = sentence.getSentenceTerms();
            if (sentenceTerms == null || sentenceTerms.size() != 1) {
                throw new InvalidInputException("Sentence Term is not Single Term.");
            }
            // Always be one Max only.
            TermType t = sentenceTerms.iterator().next();
            if (!"MAX".equalsIgnoreCase(t.getTermName())) {
                throw new InvalidInputException("Sentence Term is not Max.");
            }
        } else if (terms.contains(new CodeType(ReferenceSet.TERM_TYPE.value(), TermTypes.DUAL.name()))) {
            // Dual Term
            Set<TermType> sentenceTerms = sentence.getSentenceTerms();
            if (sentenceTerms == null || sentenceTerms.size() != 2) {
                throw new InvalidInputException("Sentence Term is not Dual Terms.");
            }
            // One Max and one Min
            int iMin = 0;
            int iMax = 0;
            for (TermType t : sentenceTerms) {
                if ("MIN".equals(t.getTermName().toUpperCase())) {
					iMin += 1;
				} else if ("MAX".equals(t.getTermName().toUpperCase())) {
					iMax += 1;
				}
            }
            if (iMin != 1 || iMax != 1) {
                throw new InvalidInputException("Sentence Terms must be one MIN and one MAX.");
            }

        } else if (terms.contains(new CodeType(ReferenceSet.TERM_TYPE.value(), TermTypes.THREE.name()))) {
            // Three Term
            Set<TermType> sentenceTerms = sentence.getSentenceTerms();
            if (sentenceTerms == null || sentenceTerms.size() != 3) {
                throw new InvalidInputException("Sentence Term is not Three Terms.");
            }
        } else if (terms.contains(new CodeType(ReferenceSet.TERM_TYPE.value(), TermTypes.INT.name()))) {
            // Intermittent
            Set<IntermittentScheduleType> intermittentSchedules = sentence.getIntermittentSchedule();
            if (intermittentSchedules == null || intermittentSchedules.size() == 0) {
                throw new InvalidInputException("Intermittent Schedule is null.");
            }
        }

        // The same Charge cannot have two sentences -- business requirement
        // defined in WOR-2496
        Set<Long> chargeIds = sentence.getChargeIds();
        if (chargeIds == null || chargeIds.size() == 0) {
            throw new InvalidInputException("Charge is required for Sentence.");
        }

        Criteria c = session.createCriteria(ChargeEntity.class);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        c.setProjection(Projections.countDistinct("chargeId"));
        c.add(Restrictions.in("chargeId", chargeIds));
        Long orderId = sentence.getOrderIdentification();
        if (orderId == null) { // For create
            c.add(Restrictions.isNotNull("sentenceId"));
        } else { // For update
            c.add(Restrictions.ne("sentenceId", orderId));
        }
        Long count = (Long) c.uniqueResult();
        if (count != null && count.longValue() > 0L) {
            throw new InvalidInputException("Charges " + chargeIds + " have already associated to other Sentences.");
        }
    }

    public static void verifySentenceKeyDate(UserContext uc, SentenceKeyDateType keyDates) {
        // verify supervision
		/*
		 * SupervisionServiceAdapter supervisionService = new
		 * SupervisionServiceAdapter(); if (supervisionService.get(uc,
		 * keyDates.getSupervisionId()) == null) { throw new
		 * DataNotExistException("Supervision with id " +
		 * keyDates.getSupervisionId() + " does not exit."); }
		 * 
		 * // verify staff StaffServiceAdapter staffService = new
		 * StaffServiceAdapter(); if (staffService.get(uc,
		 * keyDates.getStaffId()) == null) { throw new
		 * DataNotExistException("Staff with id " + keyDates.getStaffId() +
		 * " does not exit."); }
		 */
    }

    public static void setChargeSentenceLink(UserContext uc, SessionContext context, Session session, Long sentenceId, Set<Long> chargeIds, String outcome) {
        if (chargeIds != null && chargeIds.size() > 0) {
            CodeType chargeOutcome = getLinkCode(uc, new CodeType(ReferenceSet.ORDER_OUTCOME.value(), outcome), ReferenceSet.CHARGE_OUTCOME.value());
            if (chargeOutcome != null && !BeanHelper.isEmpty(chargeOutcome.getCode())) {
                CodeType chargeStatus = getLinkCode(uc, chargeOutcome, ReferenceSet.CHARGE_STATUS.value());
                for (Long chargeId : chargeIds) {
                    ChargeEntity chargeEntity = (ChargeEntity) session.get(ChargeEntity.class, chargeId);
                    chargeEntity.setSentenceId(sentenceId);
                    chargeEntity.setDispositionOutcome(chargeOutcome.getCode());
                    if (chargeStatus != null && !BeanHelper.isEmpty(chargeStatus.getCode())) {
                        chargeEntity.setChargeStatus(chargeStatus.getCode());
                    }
                    StampEntity stamp = BeanHelper.getModifyStamp(uc, context, chargeEntity.getStamp());
                    chargeEntity.setStamp(stamp);
                }
            }
        }
    }

    public static void createCriteria(UserContext uc, SessionContext context, Session session, Criteria criteria, Set<Long> orderIDs, Set<Long> caseIDs,
            Set<Long> chargeIDs, Set<Long> conditionIDs, OrderSearchType orderSearch, Boolean isActiveFlag, Boolean historyFlag) {

        // orderIDs
        if (orderIDs != null && orderIDs.size() != 0) {
            criteria.add(Restrictions.in("orderId", orderIDs));
        }

        // caseIDs
        if (caseIDs != null && caseIDs.size() != 0) {
            criteria = BeanHelper.getCriteriaWithModuleAssociations(uc, // UserContext
                    // uc
                    session, // Session session
                    criteria, // Criteria rootCriteria
                    "ALL", // String setMode
                    "orderId", // String strRootIdProperty
                    caseIDs, // Set<Long> associations,
                    LegalModule.CASE_INFORMATION.value(), // String
                    // associationToClass
                    OrderModuleAssociationEntity.class, // Class
                    // associationEntityClazz
                    "order" // String fromIdentifierAttr
            );
        }

        // chargeIDs
        if (chargeIDs != null && chargeIDs.size() != 0) {
            criteria = BeanHelper.getCriteriaWithModuleAssociations(uc, // UserContext
                    // uc
                    session, // Session session
                    criteria, // Criteria rootCriteria
                    "ALL", // String setMode
                    "orderId", // String strRootIdProperty
                    chargeIDs, // Set<Long> associations,
                    LegalModule.CHARGE.value(), // String associationToClass
                    OrderModuleAssociationEntity.class, // Class
                    // associationEntityClazz
                    "order" // String fromIdentifierAttr
            );
        }

        // conditionIDs
        if (chargeIDs != null && chargeIDs.size() != 0) {
            criteria = BeanHelper.getCriteriaWithModuleAssociations(uc, // UserContext
                    // uc
                    session, // Session session
                    criteria, // Criteria rootCriteria
                    "ALL", // String setMode
                    "orderId", // String strRootIdProperty
                    conditionIDs, // Set<Long> associations,
                    LegalModule.CONDITION.value(), // String associationToClass
                    OrderModuleAssociationEntity.class, // Class
                    // associationEntityClazz
                    "order" // String fromIdentifierAttr
            );
        }

        if (orderSearch == null) {
            return;
        }

        // ojSupervisionId
        Long ojSupervisionId = orderSearch.getOjSupervisionId();
        if (!BeanHelper.isEmpty(ojSupervisionId)) {
            criteria.add(Restrictions.eq("ojSupervisionId", ojSupervisionId));
        }

        // orderClassification
        String orderClassification = orderSearch.getOrderClassification();
        if (!BeanHelper.isEmpty(orderClassification)) {
			criteria.add(Restrictions.eq("orderClassification", orderClassification));
		}

        // orderType
        String orderType = orderSearch.getOrderType();
        if (!BeanHelper.isEmpty(orderType)) {
			criteria.add(Restrictions.eq("orderType", orderType));
		}

        // orderSubType
        String orderSubType = orderSearch.getOrderSubType();
        if (!BeanHelper.isEmpty(orderSubType)) {
			criteria.add(Restrictions.eq("orderSubType", orderSubType));
		}

        // orderCategory
        String orderCategory = orderSearch.getOrderCategory();
        if (!BeanHelper.isEmpty(orderCategory)) {
			criteria.add(Restrictions.eq("orderCategory", orderCategory));
		}

        // orderNumber
        String orderNumber = orderSearch.getOrderNumber();
        if (!BeanHelper.isEmpty(orderNumber)) {
			criteria.add(Restrictions.ilike("orderNumber", strWildcardParser(orderNumber)));
		}

        // orderDisposition
        DispositionSearchType disposition = orderSearch.getOrderDisposition();
        if (disposition != null && ValidationHelper.bValidSearchType(new LinkedList<DispositionSearchType>(), disposition)) {
			createCriteria(criteria, disposition);
		}

        // comments
        CommentType comments = orderSearch.getComments();
        if (comments != null && ValidationHelper.bValidSearchType(new LinkedList<CommentType>(), comments)) {
			createCriteria(criteria, "comments", comments);
		}

        // orderIssuanceDate
        Date fromOrderIssuanceDate = orderSearch.getFromOrderIssuanceDate();
        if (!BeanHelper.isEmpty(fromOrderIssuanceDate)) {
			criteria.add(Restrictions.ge("orderIssuanceDate", fromOrderIssuanceDate));
		}

        Date toOrderIssuanceDate = orderSearch.getToOrderIssuanceDate();
        if (!BeanHelper.isEmpty(toOrderIssuanceDate)) {
			criteria.add(Restrictions.le("orderIssuanceDate", toOrderIssuanceDate));
		}

        // orderReceivedDate
        Date fromOrderReceiveDate = orderSearch.getFromOrderReceivedDate();
        if (!BeanHelper.isEmpty(fromOrderReceiveDate)) {
			criteria.add(Restrictions.ge("orderReceivedDate", fromOrderReceiveDate));
		}

        Date toOrderReceiveDate = orderSearch.getToOrderReceivedDate();
        if (!BeanHelper.isEmpty(toOrderReceiveDate)) {
			criteria.add(Restrictions.le("orderReceivedDate", toOrderReceiveDate));
		}

        // orderStartDate
        Date fromOrderStartDate = orderSearch.getFromOrderStartDate();
        if (!BeanHelper.isEmpty(fromOrderStartDate)) {
			criteria.add(Restrictions.ge("orderStartDate", fromOrderStartDate));
		}

        Date toOrderStartDate = orderSearch.getToOrderStartDate();
        if (!BeanHelper.isEmpty(toOrderStartDate)) {
			criteria.add(Restrictions.le("orderStartDate", toOrderStartDate));
		}

        // orderExpirationDate
        Date fromOrderExpirationDate = orderSearch.getFromOrderExpirationDate();
        if (!BeanHelper.isEmpty(fromOrderExpirationDate)) {
			criteria.add(Restrictions.ge("orderExpirationDate", fromOrderExpirationDate));
		}

        Date toOrderExpirationDate = orderSearch.getToOrderExpirationDate();
        if (!BeanHelper.isEmpty(toOrderExpirationDate)) {
			criteria.add(Restrictions.le("orderExpirationDate", toOrderExpirationDate));
		}

        // caseActivityInitiatedOrderAssociations
        Long caseActivityInitiatedOrderAssociation = orderSearch.getCaseActivityInititatedOrderAssociation();
        if (!BeanHelper.isEmpty(caseActivityInitiatedOrderAssociation)) {
			criteria.add(Restrictions.eq("caseActivityInitiatedOrderAssociations.toIdentifier", caseActivityInitiatedOrderAssociation));
		}

        // orderInitiatedCaseActivityAssociations
        Long orderInitiatedCaseActivityAssociation = orderSearch.getOrderInititatedCaseActivityAssociation();
        if (!BeanHelper.isEmpty(orderInitiatedCaseActivityAssociation)) {
			criteria.add(Restrictions.eq("orderInitiatedCaseActivityAssociations.toIdentifier", orderInitiatedCaseActivityAssociation));
		}

        // isHoldingOrder
        Boolean isHoldingOrder = orderSearch.getIsHoldingOrder();
        if (!BeanHelper.isEmpty(isHoldingOrder)) {
			criteria.add(Restrictions.eq("isHoldingOrder", isHoldingOrder));
		}

        // isSchedulingNeeded
        Boolean isSchedulingNeeded = orderSearch.getIsSchedulingNeeded();
        if (!BeanHelper.isEmpty(isSchedulingNeeded)) {
			criteria.add(Restrictions.eq("isSchedulingNeeded", isSchedulingNeeded));
		}

        // hasCharges
        Boolean hasCharges = orderSearch.getHasCharges();
        if (!BeanHelper.isEmpty(hasCharges)) {
			criteria.add(Restrictions.eq("hasCharges", hasCharges));
		}

        // issuingAgency
        NotificationSearchType issuingAgency = orderSearch.getIssuingAgency();
        if (issuingAgency != null) {
			createCriteria(uc, context, criteria, "issuingAgency", issuingAgency);
		}

        // ///////////////////////////////////////////////////////////
        // Warrant Detainer
        // ///////////////////////////////////////////////////////////
        // agencyToBeNotified
        Set<NotificationSearchType> agencyToBeNotified = orderSearch.getAgencyToBeNotified();
        if (agencyToBeNotified != null && ValidationHelper.bValidSearchType(new LinkedList<NotificationSearchType>(), agencyToBeNotified)) {
            for (NotificationSearchType notification : agencyToBeNotified) {
				createCriteria(uc, context, criteria, "agencyToBeNotified", notification);
			}
        }

        // notificationLog
        Set<NotificationLogSearchType> notificationLogs = orderSearch.getNotificationLog();
        if (notificationLogs != null && ValidationHelper.bValidSearchType(new LinkedList<NotificationLogSearchType>(), notificationLogs)) {
            for (NotificationLogSearchType notificationLog : notificationLogs) {
                createCriteria(uc, context, criteria, notificationLog);
            }
        }

        // ////////////////////////////////////////////
        // Sentence
        // ////////////////////////////////////////////
        String sentenceType = orderSearch.getSentenceType();
        if (!BeanHelper.isEmpty(sentenceType)) {
            criteria.add(Restrictions.eq("sentenceType", sentenceType));
        }

        Long sentenceNumber = orderSearch.getSentenceNumber();
        if (sentenceNumber != null) {
            criteria.add(Restrictions.eq("sentenceNumber", sentenceNumber));
        }

        // Sentence Term
        TermSearchType termSearch = orderSearch.getSentenceTerm();
        if (termSearch != null && ValidationHelper.bValidSearchType(new LinkedList<TermSearchType>(), termSearch)) {
            // criteria(uc, context, criteria, termSearch);
        }

        String sentenceStatus = orderSearch.getSentenceStatus();
        if (!BeanHelper.isEmpty(sentenceStatus)) {
            criteria.add(Restrictions.eq("sentenceStatus", sentenceStatus));
        }

        // sentenceStartDate
        Date fromSentenceStartDate = orderSearch.getFromSentenceStartDate();
        if (fromSentenceStartDate != null) {
            criteria.add(Restrictions.ge("sentenceStartDate", fromSentenceStartDate));
        }
        Date toSentenceStartDate = orderSearch.getToSentenceStartDate();
        if (toSentenceStartDate != null) {
            criteria.add(Restrictions.le("sentenceStartDate", toSentenceStartDate));
        }

        // sentenceEndDate
        Date fromSentenceEndDate = orderSearch.getFromSentenceEndDate();
        if (fromSentenceEndDate != null) {
            criteria.add(Restrictions.ge("sentenceEndDate", fromSentenceEndDate));
        }
        Date toSentenceEndDate = orderSearch.getToSentenceEndDate();
        if (toSentenceEndDate != null) {
            criteria.add(Restrictions.le("sentenceEndDate", toSentenceEndDate));
        }

        // sentenceAppeal
        // To do ...

        Long concecutiveFrom = orderSearch.getConcecutiveFrom();
        if (concecutiveFrom != null) {
            criteria.add(Restrictions.eq("concecutiveFrom", concecutiveFrom));
        }

        BigDecimal fromFineAmount = orderSearch.getFromFineAmount();
        if (fromFineAmount != null) {
            criteria.add(Restrictions.ge("fineAmount", fromFineAmount));
        }

        BigDecimal toFineAmount = orderSearch.getToFineAmount();
        if (toFineAmount != null) {
            criteria.add(Restrictions.le("fineAmount", toFineAmount));
        }

        Boolean fineAmountPaid = orderSearch.getFineAmountPaid();
        if (fineAmountPaid != null) {
            criteria.add(Restrictions.eq("fineAmountPaid", fineAmountPaid));
        }

        Boolean sentenceAggravateFlag = orderSearch.getSentenceAggravatedFlag();
        if (sentenceAggravateFlag != null) {
            criteria.add(Restrictions.eq("sentenceAggravateFlag", sentenceAggravateFlag));
        }

        // intermittentSchedule
        // To do ...

    }

    public static void createCriteria(UserContext uc, SessionContext context, Criteria criteria, NotificationLogSearchType notificationLog) {
        criteria.createCriteria("notificationLog", "notiLog");

        // notificationDate
        Date fromNotificationDate = notificationLog.getFromNotificationDate();
        if (!BeanHelper.isEmpty(fromNotificationDate)) {
			criteria.add(Restrictions.ge("notiLog.notificationDate", fromNotificationDate));
		}

        Date toNotificationDate = notificationLog.getToNotificationDate();
        if (!BeanHelper.isEmpty(toNotificationDate)) {
			criteria.add(Restrictions.le("notiLog.notificationDate", toNotificationDate));
		}

        // notificationType
        String notificationType = notificationLog.getNotificationType();
        if (!BeanHelper.isEmpty(notificationType)) {
			criteria.add(Restrictions.eq("notiLog.notificationType", notificationType));
		}

        // notificationComment
        CommentType notificationComment = notificationLog.getNotificationComment();
        if (notificationComment != null && ValidationHelper.bValidSearchType(new LinkedList<CommentType>(), notificationComment)) {
            createCriteria(criteria, "notiLog.notificationComment", notificationComment);
        }

        // notificationLocationAssociation
        Long notificationLocationAssociation = notificationLog.getNotificationLocationAssociation();
        if (notificationLocationAssociation != null) {
			criteria.add(Restrictions.eq("notiLog.notificationLocationAssociation", notificationLocationAssociation));
		}

        // notificationContact
        String notificationContact = notificationLog.getNotificationContact();
        if (!BeanHelper.isEmpty(notificationContact)) {
			criteria.add(Restrictions.ilike("notiLog.notificationContact", strWildcardParser(notificationContact)));
		}

    }

    public static void createCriteria(Criteria criteria, String strProperty, CommentType comment) {
        criteria.createCriteria(strProperty, "cmnt");

        // commentUserId
        String commentUserId = comment.getUserId();
        if (!BeanHelper.isEmpty(commentUserId)) {
            criteria.add(Restrictions.eq("cmnt.commentUserId", commentUserId));
        }

        // commentDate
        Date fromCommentDate = BeanHelper.createDateWithoutTime(comment.getCommentDate());
        if (fromCommentDate != null) {
            criteria.add(Restrictions.ge("cmnt.commentDate", fromCommentDate));
        }
        Date toCommentDate = BeanHelper.nextNthDays(comment.getCommentDate(), 1);
        if (fromCommentDate != null) {
            criteria.add(Restrictions.lt("cmnt.commentDate", toCommentDate));
        }

        // commentText
        String commentText = comment.getComment();
        if (!BeanHelper.isEmpty(commentText)) {
            criteria.add(Restrictions.ilike("cmnt.commentText", commentText));
        }
    }

    public static void createCriteria(Criteria criteria, DispositionSearchType disposition) {
        criteria.createCriteria("orderDisposition", "disposition");

        // dispositionOutcome
        String dispositionOutcome = disposition.getDispositionOutcome();
        if (!BeanHelper.isEmpty(dispositionOutcome)) {
			criteria.add(Restrictions.eq("disposition.dispositionOutcome", dispositionOutcome));
		}

        // orderStatus
        String orderStatus = disposition.getOrderStatus();
        if (!BeanHelper.isEmpty(orderStatus)) {
			criteria.add(Restrictions.eq("disposition.orderStatus", orderStatus));
		}

        // dispositionDate
        Date fromDispositionDate = disposition.getFromDispositionDate();
        if (!BeanHelper.isEmpty(fromDispositionDate)) {
			criteria.add(Restrictions.ge("disposition.dispositionDate", fromDispositionDate));
		}

        Date toDispositionDate = disposition.getToDispositionDate();
        if (!BeanHelper.isEmpty(toDispositionDate)) {
			criteria.add(Restrictions.le("disposition.dispositionDate", toDispositionDate));
		}
    }

    public static void createCriteria(UserContext uc, Criteria c, OrderConfigurationEntity orderConfiguration) {

        if (orderConfiguration == null) {
			return;
		}

        // orderClassification
        String orderClassification = orderConfiguration.getOrderClassification();
        if (!BeanHelper.isEmpty(orderClassification)) {
			c.add(Restrictions.eq("orderClassification", orderClassification));
		}

        // orderType
        String orderType = orderConfiguration.getOrderType();
        if (!BeanHelper.isEmpty(orderType)) {
			c.add(Restrictions.eq("orderType", orderType));
		}

        // orderCategory
        String orderCategory = orderConfiguration.getOrderCategory();
        if (!BeanHelper.isEmpty(orderCategory)) {
			c.add(Restrictions.eq("orderCategory", orderCategory));
		}

        // isHoldingOrder
        Boolean isHoldingOrder = orderConfiguration.getIsHoldingOrder();
        if (!BeanHelper.isEmpty(isHoldingOrder)) {
			c.add(Restrictions.eq("isHoldingOrder", isHoldingOrder));
		}

        // isSchedulingNeeded
        Boolean isSchedulingNeeded = orderConfiguration.getIsSchedulingNeeded();
        if (!BeanHelper.isEmpty(isSchedulingNeeded)) {
			c.add(Restrictions.eq("isSchedulingNeeded", isSchedulingNeeded));
		}

        // hasCharges
        Boolean hasCharges = orderConfiguration.getHasCharges();
        if (!BeanHelper.isEmpty(hasCharges)) {
			c.add(Restrictions.eq("hasCharges", hasCharges));
		}

        // issuingAgencies
        createCriteria(uc, c, orderConfiguration.getIssuingAgencies());

    }

    public static void createCriteria(UserContext uc, SessionContext context, Criteria criteria, String strProperty, NotificationSearchType issuingAgency) {
        if (issuingAgency == null) {
			return;
		}

        Disjunction disj = Restrictions.disjunction();
        // notificationOrganizationAssociation
        Long notificationOrganizationAssociation = issuingAgency.getNotificationOrganizationAssociation();
        if (notificationOrganizationAssociation != null) {
			criteria.add(Restrictions.eq(strProperty + ".notificationOrganizationAssociation", notificationOrganizationAssociation));
		}

        // notificationFacilityAssociation
        Long notificationFacilityAssociation = issuingAgency.getNotificationFacilityAssociation();
        if (notificationFacilityAssociation != null) {
			criteria.add(Restrictions.eq(strProperty + ".notificationFacilityAssociation", notificationFacilityAssociation));
		}

        // isNotificationNeeded
        Boolean isNotificationNeeded = issuingAgency.getIsNotificationNeeded();
        if (isNotificationNeeded != null) {
			criteria.add(Restrictions.eq(strProperty + ".isNotificationNeeded", isNotificationNeeded));
		}

        // isNotificationConfirmed
        Boolean isNotificationConfirmed = issuingAgency.getIsNotificationConfirmed();
        if (isNotificationConfirmed != null) {
			criteria.add(Restrictions.eq(strProperty + ".isNotificationConfirmed", isNotificationConfirmed));
		}

        // notificationAgencyLocationAssociations
        Set<NotifyAgentLocAssocEntity> notificationAgencyLocationAssociations = toNotificationAgencyLocationAssociationSet(uc, context,
                issuingAgency.getNotificationAgencyLocationAssociations(), BeanHelper.getCreateStamp(uc, context));
        createCriteriaNotificationAgencyLocationAssociations(uc, criteria, disj, notificationAgencyLocationAssociations);

        // notificationAgencyContacts
        String notificationAgencyContacts = issuingAgency.getNotificationAgencyContact();
        if (!BeanHelper.isEmpty(notificationAgencyContacts)) {
			criteria.add(Restrictions.ilike(strProperty + ".notificationAgencyContacts.agentContact", notificationAgencyContacts));
		}
    }

    public static void createCriteria(UserContext uc, Criteria c, Set<NotificationEntity> issuingAgencies) {

        if (BeanHelper.isEmpty(issuingAgencies)) {
			return;
		}

        c.createAlias("issuingAgencies", "agencies");
        Disjunction disj = Restrictions.disjunction();
        for (NotificationEntity agent : issuingAgencies) {

            // notificationOrganizationAssociation
            Long notificationOrganizationAssociation = agent.getNotificationOrganizationAssociation();
            if (notificationOrganizationAssociation != null) {
				disj.add(Restrictions.eq("agencies.notificationOrganizationAssociation", notificationOrganizationAssociation));
			}

            // notificationFacilityAssociation
            Long notificationFacilityAssociation = agent.getNotificationFacilityAssociation();
            if (notificationFacilityAssociation != null) {
				disj.add(Restrictions.eq("agencies.notificationFacilityAssociation", notificationFacilityAssociation));
			}

            // isNotificationNeeded
            Boolean isNotificationNeeded = agent.getIsNotificationNeeded();
            if (isNotificationNeeded != null) {
				disj.add(Restrictions.eq("agencies.isNotificationNeeded", isNotificationNeeded));
			}

            // isNotificationConfirmed
            Boolean isNotificationConfirmed = agent.getIsNotificationConfirmed();
            if (isNotificationConfirmed != null) {
				disj.add(Restrictions.eq("agencies.isNotificationConfirmed", isNotificationConfirmed));
			}

            // notificationAgencyLocationAssociations
            createCriteriaNotificationAgencyLocationAssociations(uc, c, disj, agent.getNotificationAgencyLocationAssociations());

            // notificationAgencyContacts
            createCriteriaNotificationAgencyContacts(uc, c, disj, agent.getNotificationAgencyContacts());
        }
        c.add(disj);
    }

    public static void createCriteriaNotificationAgencyLocationAssociations(UserContext uc, Criteria c, Disjunction disj,
            Set<NotifyAgentLocAssocEntity> notificationAgencyLocationAssociations) {

        if (BeanHelper.isEmpty(notificationAgencyLocationAssociations)) {
			return;
		}

        c.createAlias("agencies.notificationAgencyLocationAssociations", "agencyLocations");

        for (NotifyAgentLocAssocEntity location : notificationAgencyLocationAssociations) {
            // toIdentifier
            Long toIdentifier = location.getToIdentifier();
            if (toIdentifier != null) {
				disj.add(Restrictions.eq("agencyLocations.toIdentifier", toIdentifier));
			}
        }
    }

    public static void createCriteriaNotificationAgencyContacts(UserContext uc, Criteria criteria, NotificationSearchType search) {

    }

    public static void createCriteriaNotificationAgencyContacts(UserContext uc, Criteria c, Disjunction disj, Set<NotifyAgentContactEntity> notificationAgencyContacts) {

        if (BeanHelper.isEmpty(notificationAgencyContacts)) {
			return;
		}

        c.createAlias("agencies.notificationAgencyContacts", "agencyContacts");

        for (NotifyAgentContactEntity contact : notificationAgencyContacts) {
            if (contact != null) {
                Long notifyAgentContactId = contact.getNotifyAgentContactId();
                if (!BeanHelper.isEmpty(notifyAgentContactId)) {
                    disj.add(Restrictions.eq("agencyContacts.notifyAgentContactId", notifyAgentContactId));
                }

                String firstName = contact.getFirstName();
                if (!BeanHelper.isEmpty(BeanHelper.trimStar(firstName))) {
                    disj.add(Restrictions.ilike("agencyContacts.firstName", strWildcardParser(firstName)));
                }

                String lastName = contact.getLastName();
                if (!BeanHelper.isEmpty(BeanHelper.trimStar(lastName))) {
                    disj.add(Restrictions.ilike("agencyContacts.lastName", strWildcardParser(lastName)));
                }

                String address = contact.getAddress();
                if (!BeanHelper.isEmpty(BeanHelper.trimStar(address))) {
                    disj.add(Restrictions.ilike("agencyContacts.address", strWildcardParser(address)));
                }

                String city = contact.getCity();
                if (!BeanHelper.isEmpty(BeanHelper.trimStar(city))) {
                    disj.add(Restrictions.ilike("agencyContacts.city", strWildcardParser(city)));
                }

                String county = contact.getCounty();
                if (!BeanHelper.isEmpty(BeanHelper.trimStar(county))) {
                    disj.add(Restrictions.ilike("agencyContacts.county", strWildcardParser(county)));
                }

                String provinceState = contact.getProvinceState();
                if (!BeanHelper.isEmpty(BeanHelper.trimStar(provinceState))) {
                    disj.add(Restrictions.ilike("agencyContacts.provinceState", strWildcardParser(provinceState)));
                }

                String country = contact.getCountry();
                if (!BeanHelper.isEmpty(BeanHelper.trimStar(country))) {
                    disj.add(Restrictions.ilike("agencyContacts.country", strWildcardParser(country)));
                }

                String zipCode = contact.getZipCode();
                if (!BeanHelper.isEmpty(BeanHelper.trimStar(zipCode))) {
                    disj.add(Restrictions.ilike("agencyContacts.zipCode", strWildcardParser(zipCode)));
                }

                String phoneNumber = contact.getPhoneNumber();
                if (!BeanHelper.isEmpty(BeanHelper.trimStar(phoneNumber))) {
                    disj.add(Restrictions.ilike("agencyContacts.phoneNumber", strWildcardParser(phoneNumber)));
                }

                String faxNumber = contact.getFaxNumber();
                if (!BeanHelper.isEmpty(BeanHelper.trimStar(faxNumber))) {
                    disj.add(Restrictions.ilike("agencyContacts.faxNumber", strWildcardParser(faxNumber)));
                }

                String email = contact.getEmail();
                if (!BeanHelper.isEmpty(BeanHelper.trimStar(email))) {
                    disj.add(Restrictions.ilike("agencyContacts.email", strWildcardParser(email)));
                }

                String identityNumber = contact.getIdentityNumber();
                if (!BeanHelper.isEmpty(BeanHelper.trimStar(identityNumber))) {
                    disj.add(Restrictions.ilike("agencyContacts.identityNumber", strWildcardParser(identityNumber)));
                }

                String agentContact = contact.getAgentContact();
                if (!BeanHelper.isEmpty(BeanHelper.trimStar(agentContact))) {
                    disj.add(Restrictions.ilike("agencyContacts.agentContact", strWildcardParser(agentContact)));
                }
            }
        }
    }

    public static Long generateSentenceNumberBySupervisionId(Session session, Long supervisionId) {
        if (supervisionId == null) {
			return null;
		}

        Criteria c = session.createCriteria(CaseInfoEntity.class);
        c.add(Restrictions.eq("supervisionId", supervisionId));
        c.setProjection(Projections.id());
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        @SuppressWarnings("unchecked") List<Long> lst = c.list();

        return generateSentenceNumber(session, new HashSet<Long>(lst));
    }

    public static Long generateSentenceNumber(Session session, Set<Long> caseInfoIds) {
        if (caseInfoIds == null || caseInfoIds.size() == 0) {
			return null;
		}

        Long maxNum = null;

        Criteria criteria = session.createCriteria(SentenceEntity.class);
        criteria.setProjection(Projections.max("sentenceNumber"));

        criteria.createCriteria("moduleAssociations", "modules");
        criteria.add(Restrictions.eq("modules.toClass", LegalModule.CASE_INFORMATION.value()));
        criteria.add(Restrictions.in("modules.toIdentifier", caseInfoIds));
        maxNum = (Long) criteria.uniqueResult();

        return maxNum == null ? 1L : maxNum + 1L;
    }

    public static boolean bExistedSentenceNumber(Session session, Long sentenceNumber, Set<Long> caseInfoIds) {
        if (caseInfoIds == null || caseInfoIds.size() == 0) {
			return false;
		}

        Criteria criteria = session.createCriteria(SentenceEntity.class);
        criteria.add(Restrictions.eq("sentenceNumber", sentenceNumber));
        Disjunction disjunct = Restrictions.disjunction();
        criteria.createCriteria("moduleAssociations", "modules");
        for (Long caseId : caseInfoIds) {
            disjunct.add(Restrictions.and(Restrictions.eq("modules.toClass", LegalModule.CASE_INFORMATION.value()), Restrictions.eq("modules.toIdentifier", caseId)));
        }
        criteria.add(disjunct);
        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();
        if (count != null && count > 0L) {
			return true;
		} else {
			return false;
		}
    }

    public static Long daysLeftToServeForSupervision(Session session, Long supervisionId) {
        Criteria c = session.createCriteria(SupervisionSentenceEntity.class);
        c.add(Restrictions.eq("supervisionId", supervisionId));
        c.add(Restrictions.eq("isHistory", Boolean.FALSE));
        c.addOrder(Order.desc("supervisionSentenceId"));
        c.setMaxResults(1);
        SupervisionSentenceEntity entity = (SupervisionSentenceEntity) c.uniqueResult();
        if (null != entity) {

            Date startDate = entity.getSentenceStartDate();
            Long sentenceDays = entity.getSentenceDays();
            Date today = new Date();
            if (startDate != null && startDate.before(today)) {
                Long served = BeanHelper.getDays(startDate, today);
                Long daysToServe = sentenceDays - served;
                if (daysToServe <= 0L) {
					return 0L;
				} else {
					return daysToServe;
				}
            } else {
                return sentenceDays;
            }

        }
        return 0L;
    }

    public static void updateCaseInfoSentenceStatusBySentenceId(Session session, SentenceEntity sentence, boolean isBeforeDelete) {
        Long sentenceId = sentence.getOrderId();
        DispositionEntity dispEntity = sentence.getOrderDisposition();
        String dispositionOutcome = dispEntity.getDispositionOutcome();
        String orderStatus = dispEntity.getOrderStatus();

        Criteria c = session.createCriteria(CaseInfoEntity.class);
        c.createCriteria("moduleAssociations", "m");
        c.add(Restrictions.ilike("m.toClass", LegalModule.ORDER_SENTENCE.value()));
        c.add(Restrictions.eq("m.toIdentifier", sentenceId));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        @SuppressWarnings("unchecked") Iterator<CaseInfoEntity> it = c.list().iterator();

        while (it.hasNext()) {
            CaseInfoEntity caseInfoEntity = it.next();
            Set<CaseInfoModuleAssociationEntity> assocs = caseInfoEntity.getModuleAssociations();
            long sentenceCount = 0;
            for (CaseInfoModuleAssociationEntity assoc : assocs) {
                if (LegalModule.ORDER_SENTENCE.value().equalsIgnoreCase(assoc.getToClass())) {
                    Long orderId = assoc.getToIdentifier();
                    OrderEntity sentenceEntity = BeanHelper.findEntity(session, OrderEntity.class, orderId);
                    if (sentenceEntity instanceof SentenceEntity && sentenceEntity.getOrderClassification().equalsIgnoreCase(
                            OrderSentenceHandler.OrderClassification.SENT.name())) {
                        DispositionEntity dispositionEntity = sentenceEntity.getOrderDisposition();
                        if (dispositionEntity != null &&
                                dispositionEntity.getOrderStatus().equalsIgnoreCase(OrderSentenceHandler.ORDER_STATUS.ACTIVE.name()) &&
                                dispositionEntity.getDispositionOutcome().equalsIgnoreCase(OrderSentenceHandler.ORDER_OUTCOME.SENT.name())) {
                            // Sentenced Counted here
                            sentenceCount++;
                        }
                    }
                }
            }

            if (isBeforeDelete) {
                if (sentenceCount == 0 || (sentenceCount == 1L && dispositionOutcome.equalsIgnoreCase(OrderSentenceHandler.ORDER_OUTCOME.SENT.name())
                        && orderStatus.equalsIgnoreCase(OrderSentenceHandler.ORDER_STATUS.ACTIVE.name()))) {
                    caseInfoEntity.setSentenceStatus(OrderSentenceHandler.SentenceStatus.UNSENTENCED.name());
                } else {
                    caseInfoEntity.setSentenceStatus(OrderSentenceHandler.SentenceStatus.SENTENCED.name());
                }
            } else {
                if (sentenceCount > 0) {
                    caseInfoEntity.setSentenceStatus(OrderSentenceHandler.SentenceStatus.SENTENCED.name());
                } else if (sentenceCount == 0) {
                    caseInfoEntity.setSentenceStatus(OrderSentenceHandler.SentenceStatus.UNSENTENCED.name());
                }
            }
        }
    }

    public static boolean hasSentence(Session session, Long supervisionId) {
        Criteria caseCriteria = session.createCriteria(CaseInfoEntity.class);
        caseCriteria.add(Restrictions.eq("supervisionId", supervisionId));
        // caseCriteria.add(Restrictions.eq("active", true));
        // caseCriteria.add(Restrictions.eq("sentenceStatus",
        // OrderSentenceHandler.SENTENCE_STATUS.INC.name()));
        caseCriteria.createCriteria("moduleAssociations", "module");
        caseCriteria.add(Restrictions.eq("module.toClass", LegalModule.ORDER_SENTENCE.value()));
        caseCriteria.setProjection(Projections.id());

        @SuppressWarnings("unchecked") List<Long> caseInfoIdList = caseCriteria.list();
        if (caseInfoIdList.size() == 0L) {
            return false;
        }

        Criteria sentenceCriteria = session.createCriteria(SentenceEntity.class);
        sentenceCriteria.createCriteria("moduleAssociations", "module");
        Disjunction disj = Restrictions.disjunction();
        disj.add(Restrictions.ilike("module.toClass", LegalModule.CASE_INFORMATION.value()));
        for (Long caseInfoId : caseInfoIdList) {
            disj.add(Restrictions.eq("toIdentifier", caseInfoId));
        }
        sentenceCriteria.add(disj);
        sentenceCriteria.add(Restrictions.eq("sentenceStatus", OrderSentenceHandler.SentenceStatus.SENTENCED.name()));
        sentenceCriteria.setProjection(Projections.countDistinct("orderId"));
        Long count = (Long) sentenceCriteria.uniqueResult();
        if (count == null || count.longValue() == 0L) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean hasConsecutiveSentence(Session session, Long sentenceId) {
        SentenceEntity entity = BeanHelper.getEntity(session, SentenceEntity.class, sentenceId);
        Long sentenceNumber = entity.getSentenceNumber();

        // Outside of Jurisdiction
        Criteria ojCriteria = session.createCriteria(SentenceEntity.class);
        ojCriteria.add(Restrictions.eq("orderCategory", OrderSentenceHandler.OrderCategory.OJ.name()));
        ojCriteria.add(Restrictions.eq("ojSupervisionId", entity.getOjSupervisionId()));
        ojCriteria.add(Restrictions.eq("concecutiveFrom", sentenceNumber));
        ojCriteria.setProjection(Projections.countDistinct("orderId"));
        Long ojCount = (Long) ojCriteria.uniqueResult();
        if (ojCount != null && ojCount.longValue() > 0L) {
            return true;
        }

        // Inside of Jurisdiction
        Set<OrderModuleAssociationEntity> modules = entity.getModuleAssociations();
        Set<Long> caseInfoIds = new HashSet<Long>();
        for (OrderModuleAssociationEntity module : modules) {
            if (module.getToClass().equalsIgnoreCase(LegalModule.CASE_INFORMATION.value())) {
                caseInfoIds.add(module.getToIdentifier());
            }
        }

        Criteria ijCriteria = session.createCriteria(SentenceEntity.class);
        ijCriteria.add(Restrictions.eq("orderCategory", OrderSentenceHandler.OrderCategory.IJ.name()));
        ijCriteria.add(Restrictions.eq("concecutiveFrom", sentenceNumber));
        ijCriteria.createCriteria("moduleAssociations", "module");
        ijCriteria.add(Restrictions.ilike("module.toClass", LegalModule.CASE_INFORMATION.value()));
        ijCriteria.add(Restrictions.in("module.toIdentifier", caseInfoIds));
        ijCriteria.setProjection(Projections.countDistinct("orderId"));
        Long ijCount = (Long) ijCriteria.uniqueResult();
        if (ijCount != null && ijCount.longValue() > 0L) {
            return true;
        }

        return false;
    }

    public static List<Long> getConsecutiveSentences(Session session, Long sentenceId) {

        List<Long> consecutiveSentences = new ArrayList<Long>();
        SentenceEntity entity = BeanHelper.getEntity(session, SentenceEntity.class, sentenceId);
        Long sentenceNumber = entity.getSentenceNumber();

        // Outside of Jurisdiction
        Criteria ojCriteria = session.createCriteria(SentenceEntity.class);
        ojCriteria.add(Restrictions.eq("orderCategory", OrderSentenceHandler.OrderCategory.OJ.name()));
        ojCriteria.add(Restrictions.eq("ojSupervisionId", entity.getOjSupervisionId()));
        ojCriteria.add(Restrictions.eq("concecutiveFrom", sentenceNumber));
        ojCriteria.setProjection(Projections.property("orderId"));
        ojCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        @SuppressWarnings("unchecked") List<Long> sentIds = ojCriteria.list();
        consecutiveSentences.addAll(sentIds);

        // Inside of Jurisdiction
        Set<OrderModuleAssociationEntity> modules = entity.getModuleAssociations();
        if (!modules.isEmpty()) {
            Set<Long> caseInfoIds = new HashSet<Long>();
            for (OrderModuleAssociationEntity module : modules) {
                if (module.getToClass().equalsIgnoreCase(LegalModule.CASE_INFORMATION.value())) {
                    caseInfoIds.add(module.getToIdentifier());
                }
            }

            Criteria ijCriteria = session.createCriteria(SentenceEntity.class);
            ijCriteria.add(Restrictions.eq("orderCategory", OrderSentenceHandler.OrderCategory.IJ.name()));
            ijCriteria.add(Restrictions.eq("concecutiveFrom", sentenceNumber));
            ijCriteria.createCriteria("moduleAssociations", "module");
            ijCriteria.add(Restrictions.ilike("module.toClass", LegalModule.CASE_INFORMATION.value()));
            ijCriteria.add(Restrictions.in("module.toIdentifier", caseInfoIds));

            ijCriteria.setProjection(Projections.property("orderId"));
            ijCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            @SuppressWarnings("unchecked") List<Long> isentIds = ijCriteria.list();
            consecutiveSentences.addAll(isentIds);
        }

        return consecutiveSentences;
    }

    public static void verifyCyclicSentenceConsective(Session session, SentenceEntity sentenceEntity) {
        Long consectiveFrom = sentenceEntity.getConcecutiveFrom();
        if (consectiveFrom == null || consectiveFrom == Long.valueOf(0L)) {
			return;
		}

        String functionName = "verifyCyclicSentenceConsective";
        Long sentenceNumber = sentenceEntity.getSentenceNumber();
        if (consectiveFrom == sentenceNumber) {
            String message = "Invalid argument: Sentence can not be concocted to itself: sentenceNumber = " + sentenceNumber;
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message, ErrorCodes.LEG_CREATE_UPDATE_SENTENCE);
        }

        Map<Long, Set<Long>> map = new HashMap<>();
        Set<Long> values = new HashSet<>();

        // Outside of Jurisdiction
        if (OrderSentenceHandler.OrderCategory.OJ.name().equalsIgnoreCase(sentenceEntity.getOrderCategory())) {
            SentenceEntity entity = sentenceEntity;
            do {
                Set<Long> successors = map.get(entity.getSentenceNumber());
                if (successors == null) {
                    successors = new HashSet<Long>();
                    successors.add(Long.valueOf(entity.getConcecutiveFrom()));
                    map.put(Long.valueOf(entity.getSentenceNumber()), successors);
                } else {
                    successors.add(Long.valueOf(entity.getConcecutiveFrom()));
                }
                values.add(Long.valueOf(entity.getConcecutiveFrom()));
                entity = getSentenceEntityBySupervisionIdAndSentenceNumber(session, sentenceEntity.getOjSupervisionId(), entity.getConcecutiveFrom());
            } while (entity != null && (entity.getConcecutiveFrom() != null && entity.getConcecutiveFrom() > Long.valueOf(0L)));

            Iterator<Map.Entry<Long, Set<Long>>> it = map.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<Long, Set<Long>> entry = it.next();
                Long sentenceNum = entry.getKey();
                if (entry.getValue().size() > 1) {
                    logError(sentenceNum);
                } else if (entry.getValue().size() == 0) {
                    it.remove();
                } else if (!values.contains(Long.valueOf(sentenceNum))) {
                    it.remove();
                }
            }
            if (map.keySet().size() > 0 && map.keySet().equals(values)) {
                logError(sentenceNumber);
            }
            return;
        }

        map.clear();
        values.clear();

        // Inside of Jurisdiction
        SentenceEntity entity = sentenceEntity;
        do {
            Set<Long> successors = map.get(entity.getSentenceNumber());
            if (successors == null) {
                successors = new HashSet<Long>();
                successors.add(Long.valueOf(entity.getConcecutiveFrom()));
                map.put(Long.valueOf(entity.getSentenceNumber()), successors);
            } else {
                successors.add(Long.valueOf(entity.getConcecutiveFrom()));
            }
            values.add(Long.valueOf(entity.getConcecutiveFrom()));
            entity = getSentenceEntityByCaseInfoIdsAndSentenceNumber(session, toCaseInfoIds(entity.getModuleAssociations()), entity.getConcecutiveFrom());
        } while (entity != null && (entity.getConcecutiveFrom() != null && entity.getConcecutiveFrom() > Long.valueOf(0L)));

        Iterator<Map.Entry<Long, Set<Long>>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Long, Set<Long>> entry = it.next();
            Long sentenceNum = entry.getKey();
            if (entry.getValue().size() > 1) {
                logError(sentenceNum);
            } else if (entry.getValue().size() == 0) {
                it.remove();
            } else if (!values.contains(Long.valueOf(sentenceNum))) {
                it.remove();
            }
        }
        if (map.keySet().size() > 0 && map.keySet().equals(values)) {
            logError(sentenceNumber);
        }
    }

    private static void logError(Long sentenceNumber) {
        String functionName = "verifyCyclicSentenceConsective";
        String message = "Invalid argument: Sentence can not be concocted cyclically: sentenceNumber = " + sentenceNumber;
        LogHelper.error(log, functionName, message);
        throw new InvalidInputException(message, ErrorCodes.LEG_CREATE_UPDATE_SENTENCE);
    }

    private static SentenceEntity getSentenceEntityBySupervisionIdAndSentenceNumber(Session session, Long supervisionId, Long sentenceNumber) {
        // Yes! Here, the sentenceNumber inputed parameter should be
        // concecutiveFrom
        if (supervisionId == null || sentenceNumber == null || sentenceNumber == Long.valueOf(0L)) {
			return null;
		}
        Criteria ojCriteria = session.createCriteria(SentenceEntity.class);
        ojCriteria.add(Restrictions.eq("ojSupervisionId", supervisionId));
        ojCriteria.add(Restrictions.eq("sentenceNumber", sentenceNumber));
        ojCriteria.setMaxResults(1);
        return (SentenceEntity) ojCriteria.uniqueResult();
    }

    private static SentenceEntity getSentenceEntityByCaseInfoIdsAndSentenceNumber(Session session, Set<Long> caseInfoIds, Long sentenceNumber) {
        Criteria ijCriteria = session.createCriteria(SentenceEntity.class);
        ijCriteria.add(Restrictions.eq("sentenceNumber", sentenceNumber));
        ijCriteria.createCriteria("moduleAssociations", "module");
        ijCriteria.add(Restrictions.ilike("module.toClass", LegalModule.CASE_INFORMATION.value()));
        ijCriteria.add(Restrictions.in("module.toIdentifier", caseInfoIds));
        ijCriteria.setMaxResults(1);
        return (SentenceEntity) ijCriteria.uniqueResult();
    }

    private static Set<Long> toCaseInfoIds(Set<OrderModuleAssociationEntity> from) {
        if (from == null) {
			return null;
		}
        Set<Long> to = new HashSet<>();
        for (OrderModuleAssociationEntity frm : from) {
            if (frm.getToClass().equalsIgnoreCase(LegalModule.CASE_INFORMATION.value())) {
                to.add(frm.getToIdentifier());
            }
        }
        return to;
    }

    public static void copySupervisionSentenceEntity(UserContext uc, SessionContext context, SupervisionSentenceEntity from, SupervisionSentenceEntity to,
            Long sentenceId) {
        if (from == null || to == null) {
			return;
		}

        to.setSupervisionId(from.getSupervisionId());
        to.setDaysToServe(from.getDaysToServe());
        to.setFlag(from.getFlag());
        to.setGoodTime(from.getGoodTime());
        to.setIsHistory(from.getIsHistory());
        to.setKeyDateHistoryId(from.getKeyDateHistoryId());
        to.setParoleEligibilityDate(from.getParoleEligibilityDate());
        to.setProbableDischargeDate(from.getProbableDischargeDate());
        to.setSentenceDays(from.getSentenceDays());
        to.setSentenceExpiryDate(from.getSentenceExpiryDate());
        to.setSentenceStartDate(from.getSentenceStartDate());
        to.setStamp(BeanHelper.getModifyStamp(uc, context, from.getStamp()));
        if (from.getAggregateSentences() != null) {
            for (AggregateSentenceEntity frm : from.getAggregateSentences()) {
                to.addAggregateSentence(newInstanceOfAggregateSentenceEntity(uc, context, frm, sentenceId));
            }
        }
    }

    public static AggregateSentenceEntity newInstanceOfAggregateSentenceEntity(UserContext uc, SessionContext context, AggregateSentenceEntity from, Long sentenceId) {
        if (from == null) {
			return null;
		}

        AggregateSentenceEntity to = new AggregateSentenceEntity();
        to.setSupervisionId(from.getSupervisionId());
        to.setbCalculateEnabled(from.getbCalculateEnabled());
        to.setCalculatingDate(from.getCalculatingDate());
        to.setComments(from.getComments());
        to.setDaysToServe(from.getDaysToServe());
        to.setEndDate(from.getEndDate());
        to.setFlag(from.getFlag());
        to.setGoodTime(from.getGoodTime());
        to.setIsControllingSentence(from.getIsControllingSentence());
        to.setIsHistory(from.getIsHistory());
        to.setKeyDateHistoryId(from.getKeyDateHistoryId());
        to.setPrdDate(from.getPrdDate());
        to.setStaffId(from.getStaffId());
        to.setStartDate(from.getStartDate());
        to.setSupervisionSentence(from.getSupervisionSentence());
        to.setTotalSentenceDays(from.getTotalSentenceDays());
        to.setStamp(BeanHelper.getModifyStamp(uc, context, from.getStamp()));

        if (from.getSentenceAssocs() != null) {
            for (AggregateSentenceAssocEntity as : from.getSentenceAssocs()) {
                if (sentenceId != as.getToIdentifier()) {
                    to.getSentenceAssocs().add(newInstanceOfAggregateSentenceAssocEntity(uc, context, as));
                }
            }
        }

        return to;
    }

    public static AggregateSentenceAssocEntity newInstanceOfAggregateSentenceAssocEntity(UserContext uc, SessionContext context, AggregateSentenceAssocEntity from) {
        if (from == null) {
			return null;
		}

        AggregateSentenceAssocEntity to = new AggregateSentenceAssocEntity();
        to.setCalculatingDate(from.getCalculatingDate());
        to.setComments(from.getComments());
        to.setConcecutiveId(from.getConcecutiveId());
        to.setDays(from.getDays());
        to.setEndDate(from.getEndDate());
        to.setExpiryDate(from.getExpiryDate());
        to.setGoodTime(from.getGoodTime());
        to.setInitStartDate(from.getInitStartDate());
        to.setIsControllingSentence(from.getIsControllingSentence());
        to.setMonths(from.getMonths());
        to.setPrdDate(from.getPrdDate());
        to.setSentenceDays(from.getSentenceDays());
        to.setSentenceNumber(from.getSentenceNumber());
        to.setSentenceType(from.getSentenceType());
        to.setStaffId(from.getStaffId());
        to.setStartDate(from.getStartDate());
        to.setSupervisionId(from.getSupervisionId());
        to.setToClass(from.getToClass());
        to.setToIdentifier(from.getToIdentifier());
        to.setWeeks(from.getWeeks());
        to.setYears(from.getYears());
        to.setStamp(BeanHelper.getModifyStamp(uc, context, from.getStamp()));

        return to;
    }

    public static void setSupervisionSentenceEntityHistory(Session session, Long supervisionId) {
        String hql = "Update SupervisionSentenceEntity Set isHistory = :isHistory where supervisionId = :supervisionId";
        Query query = session.createQuery(hql);
        query.setParameter("isHistory", Boolean.TRUE);
        query.setParameter("supervisionId", supervisionId);
        query.executeUpdate();
    }

    public static void setAggregateSentenceEntityHistory(Session session, Long supervisionId) {
        String hql = "Update AggregateSentenceEntity Set isHistory = :isHistory where supervisionId = :supervisionId";
        Query query = session.createQuery(hql);
        query.setParameter("isHistory", Boolean.TRUE);
        query.setParameter("supervisionId", supervisionId);
        query.executeUpdate();
    }

    public static Set<Long> getSupervisionIDsBySentenceID(Session session, Long sentenceId) {
        Criteria c = session.createCriteria(CaseInfoEntity.class);
        c.add(Restrictions.eq("active", Boolean.TRUE));
        c.createCriteria("moduleAssociations", "module");
        c.add(Restrictions.ilike("module.toClass", LegalModule.ORDER_SENTENCE.value()));
        c.add(Restrictions.eq("module.toIdentifier", sentenceId));
        c.setProjection(Projections.property("supervisionId"));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        @SuppressWarnings("unchecked") List<Long> supervisionIDs = c.list();
        return new HashSet<>(supervisionIDs);
    }

    public static void setSentenceCalculationEnableFlagBySentenceId(Session session, Long sentenceId, boolean flag) {
        Criteria c = session.createCriteria(CaseInfoEntity.class);
        c.add(Restrictions.eq("active", Boolean.TRUE));
        c.createCriteria("moduleAssociations", "module");
        c.add(Restrictions.ilike("module.toClass", LegalModule.ORDER_SENTENCE.value()));
        c.add(Restrictions.eq("module.toIdentifier", sentenceId));
        c.setProjection(Projections.property("supervisionId"));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        @SuppressWarnings("unchecked") List<Long> supervisionIDs = c.list();
        for (Long supervisionId : supervisionIDs) {
            setSentenceCalculationEnableFlagBySupervisionId(session, supervisionId, flag);
        }
    }

    public static void setSentenceCalculationEnableFlagBySupervisionId(Session session, Long supervisionId, boolean flag) {
        String hql = "UPDATE AggregateSentenceEntity SET bCalculateEnabled = :bCalculateEnabled WHERE supervisionId = :supervisionId AND isHistory = :isHistory";
        Query query = session.createQuery(hql);
        query.setParameter("bCalculateEnabled", Boolean.valueOf(flag));
        query.setParameter("supervisionId", supervisionId);
        query.setParameter("isHistory", Boolean.FALSE);
        query.executeUpdate();
    }

    public static List<Order> toOrderCriteria(String orderString) {

        String fieldDelims = "[;]";
        String orderDelims = "[.]";
        String[] strArr = orderString.trim().split(fieldDelims);

        List<Order> orderList = new ArrayList<Order>();
        for (String strChild : strArr) {
            if (strChild != null && strChild.trim().length() > 0) {
                String[] strArr2 = strChild.trim().split(orderDelims);
                if (strArr2.length == 2) {
                    if ("ASC".equalsIgnoreCase(strArr2[1].trim())) {
                        orderList.add(Order.asc(strArr2[0].trim()));
                    } else if ("DESC".equalsIgnoreCase(strArr2[1].trim())) {
                        orderList.add(Order.desc(strArr2[0].trim()));
                    }
                } else if (strArr2.length == 1) {
                    orderList.add(Order.asc(strArr2[0].trim()));
                }
            }
        }

        return orderList;
    }

    public static Long toLong(AssociationType association) {
        if (association == null) {
			return null;
		}
        return association.getToIdentifier();
    }

    public static String toCode(CodeType code) {
        if (code == null) {
			return null;
		}
        return code.getCode();
    }

    public static String trimStar(String str) {
        if (str != null) {
            str = str.replaceAll("[*]+", "*");
            if (str.trim().equals("*")) {
                return null;
            }
        }

        return str;
    }

    public static String strWildcardParser(String str) {

        return str == null || str.length() == 0 ? str : str.matches("^\\*+$") ? "" : str.replaceAll("\\*+", "%");
    }

    /**
     * TODO: This method will be removed when merged into Single Project. and
     * exchanged for a single call: ReferenceDataService refDataService =
     * ServiceAdapter.JNDILookUp(refDataService, ReferenceDataService.class);
     * Making it easier as Exception Handling has changed there.
     */
    private static ReferenceDataService getRefDataService() {
        ReferenceDataService refDataService = null;
        try {
            refDataService = ServiceAdapter.JNDILookUp(refDataService, ReferenceDataService.class);
        } catch (ArbutusRuntimeException e) {
            log.error("Unable to lookup ReferenceDataService");
        }
        return refDataService;
    }

    public static IntermittentSentenceScheduleEntity toIntermittentSentenceScheduleEntity(UserContext uc, SessionContext context, IntermittentSentenceScheduleType from,
            StampEntity stamp) {
        IntermittentSentenceScheduleEntity to = new IntermittentSentenceScheduleEntity();
        to.setInterSenScheduleId(from.getIntermittentScheduleId());
        to.setDayOutOfWeek(from.getDayOut());
        to.setStatus(from.getStatus());
        to.setDayOfWeek(from.getDayIn());
        if (from.getTimeIn() != null) {
            to.setStartHour(from.getTimeIn().hour());
            to.setStartMinute(from.getTimeIn().minute());
            to.setStartSecond(from.getTimeIn().second());
        }
        if (from.getTimeOut() != null) {
            to.setEndHour(from.getTimeOut().hour());
            to.setEndMinute(from.getTimeOut().minute());
            to.setEndSecond(from.getTimeOut().second());
        }
        to.setReportingDate(from.getDateIn());
        to.setScheduleEndDate(from.getDateOut());
        to.setLocationId(from.getLocationId());
        to.setOutCome(from.getOutCome());
        to.setCommentText(from.getComments());
        to.setStamp(stamp);
        return to;
    }

    public static IntermittentSentenceScheduleEntity toIntermittentSentenceScheduleEntity(IntermittentSentenceScheduleType from, StampEntity stamp,
            SentenceEntity sentence) {
        IntermittentSentenceScheduleEntity to = new IntermittentSentenceScheduleEntity();
        to.setInterSenScheduleId(from.getIntermittentScheduleId());
        to.setDayOutOfWeek(from.getDayOut());
        to.setStatus(from.getStatus());
        to.setDayOfWeek(from.getDayIn());
        if (from.getTimeIn() != null) {
            to.setStartHour(from.getTimeIn().hour());
            to.setStartMinute(from.getTimeIn().minute());
            to.setStartSecond(from.getTimeIn().second());
        }
        if (from.getTimeOut() != null) {
            to.setEndHour(from.getTimeOut().hour());
            to.setEndMinute(from.getTimeOut().minute());
            to.setEndSecond(from.getTimeOut().second());
        }
        to.setReportingDate(from.getDateIn());
        to.setScheduleEndDate(from.getDateOut());
        to.setLocationId(from.getLocationId());
        to.setOutCome(from.getOutCome());
        to.setCommentText(from.getComments());
        to.setSentence(sentence);
        to.setStamp(stamp);
        return to;
    }

    public static Set<IntermittentSentenceScheduleType> toIntermittentSentenceScheduleTypes(Set<IntermittentSentenceScheduleEntity> fromEntities) {
        Set<IntermittentSentenceScheduleType> toTypes = new HashSet<IntermittentSentenceScheduleType>();
        for (IntermittentSentenceScheduleEntity entity : fromEntities) {
            toTypes.add(toIntermittentSentenceScheduleType(entity));
        }
        return toTypes;
    }

    public static IntermittentSentenceScheduleType toIntermittentSentenceScheduleType(IntermittentSentenceScheduleEntity fromEntity) {
        IntermittentSentenceScheduleType toScheduleType = new IntermittentSentenceScheduleType();
        toScheduleType.setIntermittentScheduleId(fromEntity.getInterSenScheduleId());
        toScheduleType.setOrderId(fromEntity.getSentence().getOrderId());
        toScheduleType.setDayIn(fromEntity.getDayOfWeek());
        toScheduleType.setDayOut(fromEntity.getDayOutOfWeek());
        toScheduleType.setLocationId(fromEntity.getLocationId());
        toScheduleType.setStatus(fromEntity.getStatus() == Boolean.TRUE ? Boolean.TRUE : Boolean.FALSE);
        if (!((fromEntity.getStartHour() == null) && (fromEntity.getStartMinute() == null) && (fromEntity.getStartSecond() == null))) {
            toScheduleType.setTimeIn(toScheduleType.new TimeValue(fromEntity.getStartHour() == null ? 0 : fromEntity.getStartHour(),
                    fromEntity.getStartMinute() == null ? 0 : fromEntity.getStartMinute(), fromEntity.getStartSecond() == null ? 0 : fromEntity.getStartSecond()));
        }
        if (!((fromEntity.getEndHour() == null) && (fromEntity.getEndMinute() == null) && (fromEntity.getEndSecond() == null))) {
            toScheduleType.setTimeOut(toScheduleType.new TimeValue(fromEntity.getEndHour() == null ? 0 : fromEntity.getEndHour(),
                    fromEntity.getEndMinute() == null ? 0 : fromEntity.getEndMinute(), fromEntity.getEndSecond() == null ? 0 : fromEntity.getEndSecond()));
        }
        toScheduleType.setDateIn(fromEntity.getReportingDate());
        toScheduleType.setDateOut(fromEntity.getScheduleEndDate());
        toScheduleType.setOutCome(fromEntity.getOutCome());
        toScheduleType.setComments(fromEntity.getCommentText());
        return toScheduleType;
    }

    public static void modifyIntermittentSentenceScheduleEntity(UserContext userContext, SessionContext context, IntermittentSentenceScheduleEntity existingEntity,
            IntermittentSentenceScheduleType from) {
        existingEntity.setDayOutOfWeek(from.getDayOut());
        existingEntity.setStatus(from.getStatus());
        existingEntity.setDayOfWeek(from.getDayIn());
        if (from.getTimeIn() != null) {
            existingEntity.setStartHour(from.getTimeIn().hour());
            existingEntity.setStartMinute(from.getTimeIn().minute());
            existingEntity.setStartSecond(from.getTimeIn().second());
        }
        if (from.getTimeOut() != null) {
            existingEntity.setEndHour(from.getTimeOut().hour());
            existingEntity.setEndMinute(from.getTimeOut().minute());
            existingEntity.setEndSecond(from.getTimeOut().second());
        }
        existingEntity.setReportingDate(from.getDateIn());
        existingEntity.setScheduleEndDate(from.getDateOut());
        existingEntity.setLocationId(from.getLocationId());
        existingEntity.setOutCome(from.getOutCome());
        existingEntity.setCommentText(from.getComments());
    }

    public static void generateIntermittentSentenceSchedules(SentenceEntity sentenceEntity, SentenceType sentenceType, StampEntity stamp) {
        if (sentenceType.getIntermittentSchedule() == null) {
            return;
        }
        Map<Integer, IntermittentScheduleType> weekDays = new HashMap<Integer, IntermittentScheduleType>();
        IntermittentScheduleType intermittentScheduleType = null;
        for (IntermittentScheduleType intermittentScheduleType1 : sentenceType.getIntermittentSchedule()) {
            weekDays.put(Day.valueOf(intermittentScheduleType1.getDayOfWeek()).ordinal() + 1, intermittentScheduleType1);
            if (intermittentScheduleType == null || intermittentScheduleType.getReportingDate().getTime() > intermittentScheduleType1.getReportingDate().getTime()) {
                intermittentScheduleType = intermittentScheduleType1;
            }
        }
        if (intermittentScheduleType == null || sentenceType.getSentenceEndDate() == null) {
            return;
        }
        Calendar fromDate = Calendar.getInstance();
        fromDate.setTime(sentenceType.getSentenceStartDate());
        //WOR-9260 - Rule: 4
        if (sentenceType.getSentenceStartDate() != intermittentScheduleType.getReportingDate()) {
            fromDate.add(Calendar.DATE, 1);
        }
        Date dateIn;
        Date dateOut;
        IntermittentScheduleType currentScheduleType;
        for (Calendar tempDate = fromDate; fromDate.getTimeInMillis() <= sentenceType.getSentenceEndDate().getTime(); tempDate.add(Calendar.DATE, 1)) {
            if (weekDays.containsKey(tempDate.get(Calendar.DAY_OF_WEEK))) {
                dateIn = getDateWithZeroHr(tempDate);
                currentScheduleType = weekDays.get(tempDate.get(Calendar.DAY_OF_WEEK));
                dateOut = getEndDateWithZeroHr(tempDate, currentScheduleType);
                sentenceEntity.addIntermittentSentenceScheduleEntity(toIntermittentSentenceScheduleEntity(currentScheduleType, dateIn, dateOut, stamp));
            }
        }
    }

    private static IntermittentSentenceScheduleEntity toIntermittentSentenceScheduleEntity(IntermittentScheduleType from, Date dateIn, Date dateOut, StampEntity stamp) {
        IntermittentSentenceScheduleEntity to = new IntermittentSentenceScheduleEntity();
        to.setDayOutOfWeek(from.getDayOutOfWeek());
        to.setDayOfWeek(from.getDayOfWeek());
        if (from.getStartTime() != null) {
            to.setStartHour(from.getStartTime().hour());
            to.setStartMinute(from.getStartTime().minute());
            to.setStartSecond(from.getStartTime().second());
        }
        if (from.getEndTime() != null) {
            to.setEndHour(from.getEndTime().hour());
            to.setEndMinute(from.getEndTime().minute());
            to.setEndSecond(from.getEndTime().second());
        }
        to.setLocationId(from.getLocationId());
        to.setReportingDate(dateIn);
        to.setScheduleEndDate(dateOut);
        to.setStamp(stamp);
        return to;
    }

    private static Date getDateWithZeroHr(Calendar calendar) {
        Calendar tempCalendar = (Calendar) calendar.clone();
        setHourMinuteSecondZero(tempCalendar);
        return tempCalendar.getTime();
    }

    private static Date getEndDateWithZeroHr(Calendar tempDate, IntermittentScheduleType currentScheduleType) {
        setHourMinuteSecondZero(tempDate);
        if (currentScheduleType.getDayOfWeek() != null && currentScheduleType.getDayOutOfWeek() != null) {
            int daysBetween = Day.valueOf(currentScheduleType.getDayOutOfWeek()).ordinal() - Day.valueOf(currentScheduleType.getDayOfWeek()).ordinal();
            if (daysBetween < 0) {
                daysBetween -= NUMBER_OF_DAYS_IN_A_WEEK;
            }
            tempDate.add(Calendar.DATE, daysBetween);
        }

        return tempDate.getTime();
    }

    private static void setHourMinuteSecondZero(Calendar calendar) {
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
    }

    /**
     * @param sentenceEntity
     * @param intermittentSentenceScheduleEntity
     * @return
     */
    public static boolean isExceedingNumberOfDays(SentenceEntity sentenceEntity, IntermittentSentenceScheduleEntity intermittentSentenceScheduleEntity) {
        long numberOfDaysSentence = getRemainingNumberOfSentenceDays(sentenceEntity);
        return numberOfDaysSentence < 0;
    }

    /**
     * @param sentenceEntity
     * @return
     */
    public static long getTotalNumberOfSentenceDays(SentenceEntity sentenceEntity) {
        long numberOfDaysSentence = 0;
        int days;
        DateUtil dateUtil = new DateUtil();
        for (IntermittentSentenceScheduleEntity exitingEntity : sentenceEntity.getIntermittentSentenceSchedules()) {
            days = dateUtil.daysBetween(exitingEntity.getReportingDate(), exitingEntity.getScheduleEndDate());
            numberOfDaysSentence += days;
        }
        return numberOfDaysSentence;
    }

    /**
     * @param sentenceEntity
     * @return
     */
    public static long getRemainingNumberOfSentenceDays(SentenceEntity sentenceEntity) {
        long numberOfScheduledDays = getTotalNumberOfSentenceDays(sentenceEntity);
        int numberOfDaysSentence = 0;
        Date sentenceEndDate = null;
        DateUtil dateUtil = new DateUtil();
        for (TermEntity termEntity : sentenceEntity.getSentenceTerms()) {
            if (termEntity.getTermCategory().equals(CUSTODY) && termEntity.getTermName().equals(TERM_NAME_MAX)) {
                sentenceEndDate = dateUtil.findSentenceEndDate(sentenceEntity.getSentenceStartDate(),
                        termEntity.getYears() == null ? 0 : termEntity.getYears().intValue(), termEntity.getMonths() == null ? 0 : termEntity.getMonths().intValue(), 0,
                        termEntity.getDays() == null ? 0 : termEntity.getDays().intValue(), termEntity.getHours() == null ? 0 : termEntity.getHours().intValue());
                break;
            }
        }
        numberOfDaysSentence = dateUtil.daysBetween(sentenceEntity.getSentenceStartDate(), sentenceEndDate);
        return numberOfDaysSentence - numberOfScheduledDays;
    }

    /**
     * This method shows if sentence changed so that schedules need to be regenerated.
     *
     * @param sentenceEntity
     * @param sentenceType
     * @return boolean
     */
    public static boolean isSentenceChangedToRegenerateSchedule(SentenceEntity sentenceEntity, SentenceType sentenceType) {

        Date sentenceStartDateOld = BeanHelper.getDateWithoutTime(sentenceEntity.getSentenceStartDate());
        Date sentenceStartDateNew = BeanHelper.getDateWithoutTime(sentenceType.getSentenceStartDate());
        if (sentenceStartDateOld != null && !sentenceStartDateOld.equals(sentenceStartDateNew)) {
            return true;
        }

        Set<IntermittentScheduleEntity> intermittentScheduleEntities = sentenceEntity.getIntermittentSchedule();
        Set<IntermittentScheduleType> intermittentScheduleTypes = sentenceType.getIntermittentSchedule();
        Date oldReportingDate = null;
        Date newReportingDate = null;
        Long oldReportingLocationId = null;
        Long newReportingLocationId = null;
        for (IntermittentScheduleEntity intermittentScheduleEntity : intermittentScheduleEntities) {
            oldReportingDate = BeanHelper.getDateWithoutTime(intermittentScheduleEntity.getReportingDate());
            oldReportingLocationId = intermittentScheduleEntity.getLocationId();
            break;
        }

        for (IntermittentScheduleType intermittentScheduleType : intermittentScheduleTypes) {
            newReportingDate = BeanHelper.getDateWithoutTime(intermittentScheduleType.getReportingDate());
            newReportingLocationId = intermittentScheduleType.getLocationId();
            break;
        }

        if ((oldReportingDate != null && !oldReportingDate.equals(newReportingDate)) || (oldReportingLocationId != null && !oldReportingLocationId.equals(
                newReportingLocationId))) {
            return true;
        }

        Set<TermEntity> termEntities = sentenceEntity.getSentenceTerms();
        Set<TermType> termTypes = sentenceType.getSentenceTerms();
        for (TermEntity termEntity : termEntities) {
            if (termEntity.getTermCategory().equals(CUSTODY) && termEntity.getTermName().equals(TERM_NAME_MAX)) {
                Long oldYears = termEntity.getYears();
                Long oldMonths = termEntity.getMonths();
                Long oldDays = termEntity.getDays();
                Long oldHours = termEntity.getHours();
                for (TermType termType : termTypes) {
                    if (termType.getTermCategory().equals(CUSTODY) && termType.getTermName().equals(TERM_NAME_MAX)) {
                        Long newYears = termType.getYears();
                        Long newMonths = termType.getMonths();
                        Long newDays = termType.getDays();
                        Long newHours = termType.getHours();
                        if ((oldYears == null && newYears != null) || (oldYears != null && !oldYears.equals(newYears))) {
                            return true;
                        } else if ((oldMonths == null && newMonths != null) || (oldMonths != null && !oldMonths.equals(newMonths))) {
                            return true;
                        } else if ((oldDays == null && newDays != null) || (oldDays != null && !oldDays.equals(newDays))) {
                            return true;
                        } else if ((oldHours == null && newHours != null) || (oldHours != null && !oldHours.equals(newHours))) {
                            return true;
                        }
                        break;
                    }
                }
                break;
            }
        }

        //Start WOR-18320
        if (sentenceEntity.getIntermittentSchedule().size() == sentenceType.getIntermittentSchedule().size()) {
            for (int i = 0; i < sentenceEntity.getIntermittentSchedule().size(); i++) {
                final Iterator<IntermittentScheduleEntity> entityIterator = sentenceEntity.getIntermittentSchedule().iterator();
                final Iterator<IntermittentScheduleType> typeIterator = sentenceType.getIntermittentSchedule().iterator();
                if (!intermittentScheduleEntityEqualIntermittentScheduleType(entityIterator.next(), typeIterator.next())) {
                    return true;
                }
            }
        } else {
            return true;
        }
        //End WOR-18320

        return false;
    }

    private static boolean intermittentScheduleEntityEqualIntermittentScheduleType(IntermittentScheduleEntity entity, IntermittentScheduleType type) {
        if (!Objects.equals(entity.getDayOfWeek(), type.getDayOfWeek()) ||
                !Objects.equals(entity.getStartHour(), type.getStartTime().hour()) ||
                !Objects.equals(entity.getStartMinute(), type.getStartTime().minute()) ||
                !Objects.equals(entity.getStartSecond(), type.getStartTime().second()) ||
                !Objects.equals(entity.getDayOutOfWeek(), type.getDayOutOfWeek()) ||
                !Objects.equals(entity.getEndHour(), type.getEndTime().hour()) ||
                !Objects.equals(entity.getEndMinute(), type.getEndTime().minute()) ||
                !Objects.equals(entity.getEndSecond(), type.getEndTime().second()) ||
                !Objects.equals((entity.getSameDay() == null || entity.getSameDay() == 0 ? Boolean.FALSE : Boolean.TRUE), type.getSameDay())) {
            return false;
        }
        return true;
    }

    public static FinePaymentEntity toFinePaymentEntity(FinePaymentType type, SentenceEntity sentence, StampEntity stamp) {
        FinePaymentEntity entity = new FinePaymentEntity();
        entity.setPayerId(type.getPayerId());
        entity.setPaymentDate(type.getPaymentDate());
        entity.setAmountPaid(type.getAmountPaid());
        entity.setAmountOwing(type.getAmountOwing());
        entity.setProposedAmountOwing(type.getProposedAmountOwing());
        entity.setFinePaidToDate(type.getFinePaidToDate());
        entity.setStaffId(type.getStaffId());
        entity.setReceiptNumber(type.getReceiptNumber());
        entity.setDaysSatisfies(type.getDaysSatisfies());
        entity.setPerDiemRate(type.getPerDiemRate());
        entity.setDaysServed(type.getDaysServed());
        entity.setDaysToLeftToServe(type.getDaysToLeftToServe());
        entity.setReleaseDate(type.getReleaseDate());
        entity.setAdjustedReleaseDate(type.getAdjustedReleaseDate());
        sentence.addFinePayment(entity);
        entity.setStamp(stamp);
        return entity;
    }

    /**
     * @param entity
     * @return
     */
    public static FinePaymentType toFinePaymentType(FinePaymentEntity entity) {
        FinePaymentType type = new FinePaymentType();
        type.setId(entity.getSaFinePaymentId());
        type.setPayerId(entity.getPayerId());
        type.setPaymentDate(entity.getPaymentDate());
        type.setAmountPaid(entity.getAmountPaid());
        type.setAmountOwing(entity.getAmountOwing());
        type.setProposedAmountOwing(entity.getProposedAmountOwing());
        type.setFinePaidToDate(entity.getFinePaidToDate());
        type.setStaffId(entity.getStaffId());
        type.setReceiptNumber(entity.getReceiptNumber());
        type.setDaysSatisfies(entity.getDaysSatisfies());
        type.setPerDiemRate(entity.getPerDiemRate());
        type.setDaysServed(entity.getDaysServed());
        type.setDaysToLeftToServe(entity.getDaysToLeftToServe());
        type.setReleaseDate(entity.getReleaseDate());
        type.setAdjustedReleaseDate(entity.getAdjustedReleaseDate());
        type.setSentenceId(entity.getSentence().getOrderId());
        return type;
    }

    /**
     * @param result
     * @return
     */
    public static List<FinePaymentType> toFinePaymentTypes(List<FinePaymentEntity> result) {
        List<FinePaymentType> finePayments = new ArrayList<FinePaymentType>();
        for (FinePaymentEntity finePayment : result) {
            finePayments.add(toFinePaymentType(finePayment));
        }
        return finePayments;
    }

    /**
     * @param finePayments
     * @return
     */
    public static Set<FinePaymentType> toFinePaymentTypeSet(Set<FinePaymentEntity> finePayments) {
        Set<FinePaymentType> finePaymentTypes = new HashSet<FinePaymentType>();
        for (FinePaymentEntity finePayment : finePayments) {
            finePaymentTypes.add(toFinePaymentType(finePayment));
        }
        return finePaymentTypes;
    }

    public static SentenceTermEntity toSentenceTermEntity(SentenceTermType sentenceTermType) {
        List<SentenceTermNameEntity> sentenceTermNameEntityList = new ArrayList<SentenceTermNameEntity>();
        SentenceTermNameEntity eitityObj;
        SentenceTermEntity entity = new SentenceTermEntity();
        entity.setSentenceTermId(sentenceTermType.getSentenceTermId());
        entity.setSentenceType(sentenceTermType.getSentenceType());
        entity.setTermType(sentenceTermType.getTermType());
        entity.setDeactivationDate(sentenceTermType.getDeactivationDate());

        if (null != sentenceTermType.getSentenceTermNameList()) {
            for (SentenceTermNameType obj : sentenceTermType.getSentenceTermNameList()) {
                if (null != obj.getTermName()) {
                    eitityObj = toSentenceTermNameEntity(obj);
                    eitityObj.setSentenceTermEntity(entity);
                    eitityObj.setSentenceTermNameId(obj.getSentenceTermNameId());
                    sentenceTermNameEntityList.add(eitityObj);
                }

            }
        }
        entity.setSentenceTermNameEntityList(sentenceTermNameEntityList);
        return entity;
    }

    public static SentenceTermType toSentenceTermType(SentenceTermEntity entity) {
        List<SentenceTermNameType> sentenceTermNameTypeList = new ArrayList<SentenceTermNameType>();
        SentenceTermNameType typeObj;
        SentenceTermType type = new SentenceTermType();
        type.setSentenceTermId(entity.getSentenceTermId());
        type.setSentenceType(entity.getSentenceType());
        type.setTermType(entity.getTermType());
        type.setDeactivationDate(entity.getDeactivationDate());

        if (null != entity.getSentenceTermNameEntityList()) {
            for (SentenceTermNameEntity obj : entity.getSentenceTermNameEntityList()) {
                typeObj = toSentenceTermNameType(obj);
                sentenceTermNameTypeList.add(typeObj);
            }
        }
        type.setSentenceTermNameList(sentenceTermNameTypeList);

        return type;
    }

    public static SentenceTermNameEntity toSentenceTermNameEntity(SentenceTermNameType type) {
        SentenceTermNameEntity entity = new SentenceTermNameEntity();
        entity.setSentenceTermNameId(type.getSentenceTermNameId());
        entity.setTermName(type.getTermName());

        return entity;
    }

    public static SentenceTermNameType toSentenceTermNameType(SentenceTermNameEntity entity) {
        SentenceTermNameType type = new SentenceTermNameType();
        type.setSentenceTermNameId(entity.getSentenceTermNameId());
        type.setTermName(entity.getTermName());
        return type;
    }

    public static List<SentenceTermType> toSentenceTermTypeList(List<SentenceTermEntity> entityList) {
        List<SentenceTermType> typeList = new ArrayList<SentenceTermType>();
        SentenceTermType type;
        for (SentenceTermEntity entity : entityList) {
            type = toSentenceTermType(entity);
            typeList.add(type);

        }
        return typeList;
    }

    public static void updateSetenceTermStamp(SentenceTermEntity entity, StampEntity createStamp, StampEntity modifyStamp) {
        if (modifyStamp == null) {
            entity.setStamp(createStamp);
        } else {
            entity.setStamp(modifyStamp);
        }
    }

    public static void updateSetenceTermNameStamp(SentenceTermNameEntity entity, StampEntity createStamp, StampEntity modifyStamp) {
        if (modifyStamp == null) {
            entity.setStamp(createStamp);
        } else {
            entity.setStamp(modifyStamp);
        }
    }

    public static void updateSetenceTermNameStamp(List<SentenceTermNameEntity> entityList, StampEntity createStamp, StampEntity modifyStamp) {
        for (SentenceTermNameEntity entity : entityList) {
            if (modifyStamp == null) {
                entity.setStamp(createStamp);
            } else {
                entity.setStamp(modifyStamp);
            }
        }

    }

    /**
     * @author amlendu.kumar
     */
    public enum Day {
        SUN, MON, TUE, WED, THU, FRI, SAT
    }

}
