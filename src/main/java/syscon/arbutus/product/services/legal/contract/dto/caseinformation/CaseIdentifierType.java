package syscon.arbutus.product.services.legal.contract.dto.caseinformation;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * The representation of the case identifier type
 */
public class CaseIdentifierType implements Serializable {

    private static final long serialVersionUID = -4992493804018779382L;

    private static final int LENGTH_SMALL = 64;

    private Long identifierId;
    @NotNull
    private String identifierType;
    @NotNull
    @Size(max = LENGTH_SMALL, message = "max length 64")
    private String identifierValue;
    private Boolean autoGeneration;
    private Long organizationId;
    private Long facilityId;
    private String comment;
    private boolean primary;

    /**
     * Default constructor
     */
    public CaseIdentifierType() {
    }

    /**
     * Constructor
     *
     * @param identifierId    Long - the unique Id for each case identifier record. (optional during creation)
     * @param identifierType  String - A kind of identifier or number that can be associated to a criminal case e.g. Court Information Number, Indictment Number
     * @param identifierValue String - The number or value of the case identifier e.g. 1234567S (Indictment Number)
     * @param autoGeneration  Boolean - Allows the user to indicate to the system whether or not to auto generate the identifier when creating it. This is set to false by default and cannot be set to true for an update. If set to true the IdentifierValue is ignored.
     * @param organizationId  Long - the organization Id, static reference to Organization service. (Optional)
     * @param facilityId      Long - the facility Id, static reference to Facility service. (Optional)
     * @param comment         String - the comments associated to the case identifier. (Optional)
     * @param boolean         primary - Indicate if this is a primary identifier for a case. (Required)
     */
    public CaseIdentifierType(Long identifierId, @NotNull String identifierType, @NotNull @Size(max = LENGTH_SMALL, message = "max length 64") String identifierValue,
            Boolean autoGeneration, Long organizationId, Long facilityId, String comment, boolean primary) {
        this.identifierId = identifierId;
        this.identifierType = identifierType;
        this.identifierValue = identifierValue;
        this.autoGeneration = autoGeneration;
        this.organizationId = organizationId;
        this.facilityId = facilityId;
        this.comment = comment;
        this.primary = primary;
    }

    /**
     * Gets the value of the identifierId property.
     *
     * @return Long - the Unique Id for each case identifier record.
     */
    public Long getIdentifierId() {
        return identifierId;
    }

    /**
     * Sets the value of the identifierId property.
     *
     * @param identifierId Long - the Unique Id for each case identifier record.
     */
    public void setIdentifierId(Long identifierId) {
        this.identifierId = identifierId;
    }

    /**
     * Gets the value of the identifierType property. It is a code value of CaseIdentifierType set.
     *
     * @return String - A kind of identifier or number that can be associated to a criminal case e.g. Court Information Number, Indictment Number
     */
    public String getIdentifierType() {
        return identifierType;
    }

    /**
     * Sets the value of the identifierType property. It is a code value of CaseIdentifierType set.
     *
     * @param identifierType String - A kind of identifier or number that can be associated to a criminal case e.g. Court Information Number, Indictment Number
     */
    public void setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
    }

    /**
     * Gets the value of the identifierValue property.
     *
     * @return String - The number or value of the case identifier e.g. 1234567S (Indictment Number)
     */
    public String getIdentifierValue() {
        return identifierValue;
    }

    /**
     * Sets the value of the identifierValue property.
     *
     * @param identifierValue String - The number or value of the case identifier e.g. 1234567S (Indictment Number)
     */
    public void setIdentifierValue(String identifierValue) {
        this.identifierValue = identifierValue;
    }

    /**
     * Gets the value of the autoGeneration property.
     *
     * @return Boolean - Allows the user to indicate to the system whether or not to auto generate the identifier when creating it. This is set to false by default and cannot be set to true for an update. If set to true the IdentifierValue is ignored.
     */
    public Boolean isAutoGeneration() {
        return autoGeneration;
    }

    /**
     * Sets the value of the autoGeneration property.
     *
     * @param autoGeneration Boolean - Allows the user to indicate to the system whether or not to auto generate the identifier when creating it. This is set to false by default and cannot be set to true for an update. If set to true the IdentifierValue is ignored.
     */
    public void setAutoGeneration(Boolean autoGeneration) {
        this.autoGeneration = autoGeneration;
    }

    /**
     * Gets the value of the organization property.
     *
     * @return Long - the organization Id, static reference to Organization service.
     */
    public Long getOrganizationId() {
        return organizationId;
    }

    /**
     * Sets the value of the organization property.
     *
     * @param organizationId Long - the organization Id, static reference to Organization service.
     */
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * Gets the value of the facility property.
     *
     * @return Long - the facility Id, static reference to Facility service.
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * Sets the value of the facility property.
     *
     * @param facilityId Long - the facility Id, static reference to Facility service.
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * Gets the value of the identifierComment property.
     *
     * @return String - the comments associated to the case identifier
     */
    public String getIdentifierComment() {
        return comment;
    }

    /**
     * Sets the value of the identifierComment property.
     *
     * @param identifierComment String - the comments associated to the case identifier
     */
    public void setIdentifierComment(String identifierComment) {
        this.comment = identifierComment;
    }

    /**
     * Gets the value of the primary property.
     *
     * @return boolean - Indicate if this is a primary identifier for a case
     */
    public boolean isPrimary() {
        return primary;
    }

    /**
     * Sets the value of the primary property.
     *
     * @param primary boolean - Indicate if this is a primary identifier for a case
     */
    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((autoGeneration == null) ? 0 : autoGeneration.hashCode());
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((facilityId == null) ? 0 : facilityId.hashCode());
        result = prime * result + ((identifierType == null) ? 0 : identifierType.hashCode());
        result = prime * result + ((identifierValue == null) ? 0 : identifierValue.hashCode());
        result = prime * result + ((organizationId == null) ? 0 : organizationId.hashCode());
        result = prime * result + (primary ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        CaseIdentifierType other = (CaseIdentifierType) obj;
        if (autoGeneration == null) {
            if (other.autoGeneration != null) {
				return false;
			}
        } else if (!autoGeneration.equals(other.autoGeneration)) {
			return false;
		}
        if (comment == null) {
            if (other.comment != null) {
				return false;
			}
        } else if (!comment.equals(other.comment)) {
			return false;
		}
        if (facilityId == null) {
            if (other.facilityId != null) {
				return false;
			}
        } else if (!facilityId.equals(other.facilityId)) {
			return false;
		}
        if (identifierType == null) {
            if (other.identifierType != null) {
				return false;
			}
        } else if (!identifierType.equals(other.identifierType)) {
			return false;
		}
        if (identifierValue == null) {
            if (other.identifierValue != null) {
				return false;
			}
        } else if (!identifierValue.equals(other.identifierValue)) {
			return false;
		}
        if (organizationId == null) {
            if (other.organizationId != null) {
				return false;
			}
        } else if (!organizationId.equals(other.organizationId)) {
			return false;
		}
        if (primary != other.primary) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "CaseIdentifierType [getIdentifierId()=" + getIdentifierId() + ", getIdentifierType()=" + getIdentifierType() + ", getIdentifierValue()="
                + getIdentifierValue() + ", isAutoGeneration()=" + isAutoGeneration() + ", getOrganization()=" + getOrganizationId() + ", getFacility()="
                + getFacilityId() + ", getIdentifierComment()=" + getIdentifierComment() + ", isPrimary()=" + isPrimary() + "]";
    }
}
