package syscon.arbutus.product.services.legal.contract.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * SentenceTermType for Configure Sentence Term
 *
 * @author Ashish
 * @version 1.0
 * @since December 9, 2013
 */
public class SearchSentenceTermType implements Serializable {

    private static final long serialVersionUID = -737931375114522737L;

    private Long sentenceTermId;

    private String sentenceType;

    private String termType;

    private String termName;

    private boolean isActive;

    private Date deactivationDate;

    /**
     * Constructor
     */
    public SearchSentenceTermType() {
        super();
    }

    /**
     * Constructor
     *
     * @param sentenceTermId String, id of SentenceTermType Object
     * @param sentenceType   Code, Required. The sentenceType of the term; DEF, INDETER, DETER etc..
     * @param termType       Code, Required. The type of term (SINGLE, DUAL, THREE etc.). This will be used by the sentence calculation module.
     * @param termName       Code, Required. (MAX , MIN, COM, etc..)
     */
    public SearchSentenceTermType(Long sentenceTermId, String sentenceType, String termType, String termName, boolean isAcive) {
        super();
        this.sentenceTermId = sentenceTermId;
        this.sentenceType = sentenceType;
        this.termType = termType;
        this.termName = termName;
        this.isActive = isAcive;

    }

    /**
     * Get id of SentenceTermType Object
     *
     * @return the sentenceTermId
     */
    public Long getSentenceTermId() {
        return sentenceTermId;
    }

    /**
     * Set id of SentenceTermType Object
     *
     * @param the sentenceTermId
     */
    public void setSentenceTermId(Long sentenceTermId) {
        this.sentenceTermId = sentenceTermId;
    }

    /**
     * Get Sentence Type
     * -- The type of sentence (Definite, Indeterminant, Determinant etc.).
     *
     * @return the sentenceType
     */
    public String getSentenceType() {
        return sentenceType;
    }

    /**
     * Set Sentence Type
     * -- The type of sentence (Definite, Indeterminant, Determinant etc.).
     *
     * @param the sentenceType
     */
    public void setSentenceType(String sentenceType) {
        this.sentenceType = sentenceType;
    }

    /**
     * Get Term Type
     * -- The type of term (Single Term, Dual Term, Three Term etc.).
     *
     * @return the termType
     */
    public String getTermType() {
        return termType;
    }

    /**
     * Set Term Type
     * -- The type of term (Single Term, Dual Term, Three Term etc.).
     *
     * @param the termType
     */
    public void setTermType(String termType) {
        this.termType = termType;
    }

    /**
     * Get Term Name
     * -- The name of the term ( Maximum, Minimum etc.).
     *
     * @return the termName
     */
    public String getTermName() {
        return termName;
    }

    /**
     * Set Term Name
     * -- The name of the term ( Maximum, Minimum etc.).
     *
     * @param the termName
     */
    public void setTermName(String termName) {
        this.termName = termName;
    }

    /**
     * Get to true, SentenceTermtype if it is Active
     *
     * @return the isActive
     */
    public boolean isActive() {
        return isActive;
    }

    /**
     * Set to true, SentenceTermtype if it is Active
     * -- The name of the term ( Maximum, Minimum etc.).
     *
     * @param the isActive
     */
    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * Get to deactivationDate,
     *
     * @return the deactivationDate
     */
    public Date getDeactivationDate() {
        return deactivationDate;
    }

    /**
     * Set deactivation date
     * -- The name of the term ( Maximum, Minimum etc.).
     *
     * @param the deactivationDate
     */
    public void setDeactivationDate(Date deactivationDate) {
        this.deactivationDate = deactivationDate;
    }

}
