/**
 *
 */
package syscon.arbutus.product.services.legal.contract.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationType;
import syscon.arbutus.product.services.legal.realization.persistence.caseaffiliation.CaseAffiliationEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;

/**
 * This helper is provided for utility functions to support case affiliation business functions.
 *
 * @author lhan
 */
public class CaseAffiliationHelper {

    /**
     * @param dto   CaseAffiliationType
     * @param stamp StampEntity
     * @return CaseAffiliationEntity
     */
    public static CaseAffiliationEntity toCaseAffiliationEntity(CaseAffiliationType dto, StampEntity stamp) {
        if (dto == null) {
            return null;
        }

        CaseAffiliationEntity rtn = new CaseAffiliationEntity(dto.getCaseAffiliationId(), dto.getCaseAffiliationCategory(), dto.getCaseAffiliationType(),
                dto.getCaseAffiliationRole(), LegalHelper.getDateWithoutTime(dto.getStartDate()), LegalHelper.getDateWithoutTime(dto.getEndDate()),
                LegalHelper.toCommentEntity(dto.getComment()), dto.getOrgnizationPersonJobPosition(), dto.getAffiliatedOrganizationId(),
                dto.getAffiliatedPersonIdentityId(), dto.getAffiliatedCaseId(), getStatus(dto.getStartDate(), dto.getEndDate()), null, stamp);

        Set<Long> affiliatedCaseActivityIds = dto.getAffiliatedCaseActivityIds();

        if (affiliatedCaseActivityIds != null && affiliatedCaseActivityIds.size() > 0) {
            //TODO: handle CaseActivityIds.
        }
        rtn.setPersonPersonId(dto.getPersonPersonId());
        return rtn;
    }

    /**
     * @param entity CaseAffiliationEntity
     * @return CaseAffiliationType
     */
    public static CaseAffiliationType toCaseAffiliationType(CaseAffiliationEntity entity) {
        if (entity == null) {
			return null;
		}

        Date startDateTime = entity.getStartDate();
        Date endDateTime = entity.getEndDate();
        CaseAffiliationType ret = new CaseAffiliationType(entity.getCaseAffiliationId(), entity.getAffiliationCategory(), entity.getAffiliationType(),
                entity.getAffiliationRole(), entity.getStartDate(), entity.getEndDate(), getStatus(startDateTime, endDateTime),
                LegalHelper.toCommentType(entity.getComment()), entity.getOrgnizationPersonJobPosition(), entity.getAffiliatedOrganizationId(),
                entity.getAffiliatedPersonIdentityId(), entity.getAffiliatedCaseId(), LegalHelper.getCaseActivityIds(entity.getAffiliatedCaseActivities()));

        ret.setPersonPersonId(entity.getPersonPersonId());

        return ret;
    }

    /**
     * To get the case affiliation status value based on start and end date relations.
     *
     * @param startDateTime
     * @param endDateTime
     * @return
     */
    public static boolean getStatus(Date startDateTime, Date endDateTime) {
        Date startDate = LegalHelper.getDateWithoutTime(startDateTime);
        Date endDate = LegalHelper.getDateWithoutTime(endDateTime);

        if (startDate == null & endDate == null) {
			return true;
		}

        Date current = LegalHelper.getDateWithoutTime(new Date());

        if (startDate.getTime() <= current.getTime() && (endDate == null || endDate.getTime() >= current.getTime())) {
            return true;
        }

        return false;
    }

    /**
     * To generate a list of CaseAffiliationType objects from a list of CaseAffiliationEntity objects.
     *
     * @param entities List<CaseAffiliationEntity>
     * @return
     */
    public static List<CaseAffiliationType> toListOfCaseAffiliationType(List<CaseAffiliationEntity> entities) {
        if (BeanHelper.isEmpty(entities)) {
            return null;
        }

        List<CaseAffiliationType> ret = new ArrayList<CaseAffiliationType>();

        for (CaseAffiliationEntity entity : entities) {
            CaseAffiliationType dtoType = toCaseAffiliationType(entity);
            ret.add(dtoType);
        }

        return ret;
    }
}
