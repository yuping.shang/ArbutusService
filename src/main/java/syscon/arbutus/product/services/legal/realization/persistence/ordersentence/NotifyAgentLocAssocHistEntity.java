package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.Associable;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * NotifyAgentLocAssocHistEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_ORDNotiAgnLocHst 'Notification Agent Location Association History table for Order Sentence module of Legal service'
 * @since December 20, 2012
 */
//@Audited
@Entity
@Table(name = "LEG_ORDNotiAgnLocHst")
public class NotifyAgentLocAssocHistEntity implements Serializable, Associable {

    private static final long serialVersionUID = -737931375114612354L;

    /**
     * @DbComment nalHitId 'Unique identification of a notification agent location association history instance.'
     */
    @Id
    @Column(name = "nalHitId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDNOTIAGNLOCHST_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDNOTIAGNLOCHST_ID", sequenceName = "SEQ_LEG_ORDNOTIAGNLOCHST_ID", allocationSize = 1)
    private Long historyId;

    /**
     * @DbComment notifyAgentLocId 'Unique identification of a notification agent location association instance.'
     */
    @Column(name = "notifyAgentLocId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDNOTIFYAGENTLOC_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDNOTIFYAGENTLOC_ID", sequenceName = "SEQ_LEG_ORDNOTIFYAGENTLOC_ID", allocationSize = 1)
    private Long associationId;

    /**
     * @DbComment toIdentifier 'Identifier of notification agent location to be associated'
     */
    @Column(name = "toIdentifier", nullable = false)
    private Long toIdentifier;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment hstId 'Unique identification of a notification instance, foreign key to LEG_OrdNotification table'
     */
    @ForeignKey(name = "Fk_LEG_ORDNotiAgnLocHst")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hstId", nullable = false)
    private NotificationHistoryEntity notification;

    /**
     * Constructor
     */
    public NotifyAgentLocAssocHistEntity() {
        super();
    }

    /**
     * Constructor
     *
     * @param historyId
     * @param associationId
     * @param toIdentifier
     * @param stamp
     */
    public NotifyAgentLocAssocHistEntity(Long historyId, Long associationId, Long toIdentifier, StampEntity stamp) {
        super();
        this.historyId = historyId;
        this.associationId = associationId;
        this.toIdentifier = toIdentifier;
        this.stamp = stamp;
    }

    /**
     * @return the historyId
     */
    public Long getHistoryId() {
        return historyId;
    }

    /**
     * @param historyId the historyId to set
     */
    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    /**
     * @return the associationId
     */
    public Long getAssociationId() {
        return associationId;
    }

    /**
     * @param associationId the associationId to set
     */
    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    /**
     * @return the toIdentifier
     */
    public Long getToIdentifier() {
        return toIdentifier;
    }

    /**
     * @param toIdentifier the toIdentifier to set
     */
    public void setToIdentifier(Long toIdentifier) {
        this.toIdentifier = toIdentifier;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the notification
     */
    public NotificationHistoryEntity getNotification() {
        return notification;
    }

    /**
     * @param notification the notification to set
     */
    public void setNotification(NotificationHistoryEntity notification) {
        this.notification = notification;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((associationId == null) ? 0 : associationId.hashCode());
        result = prime * result + ((historyId == null) ? 0 : historyId.hashCode());
        result = prime * result + ((toIdentifier == null) ? 0 : toIdentifier.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        NotifyAgentLocAssocHistEntity other = (NotifyAgentLocAssocHistEntity) obj;
        if (associationId == null) {
            if (other.associationId != null) {
				return false;
			}
        } else if (!associationId.equals(other.associationId)) {
			return false;
		}
        if (historyId == null) {
            if (other.historyId != null) {
				return false;
			}
        } else if (!historyId.equals(other.historyId)) {
			return false;
		}
        if (toIdentifier == null) {
            if (other.toIdentifier != null) {
				return false;
			}
        } else if (!toIdentifier.equals(other.toIdentifier)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "NotifyAgentLocAssocHistEntity [historyId=" + historyId + ", associationId=" + associationId + ", toIdentifier=" + toIdentifier + "]";
    }

    public String getToClass() {
        // Not implemented
        return null;
    }

    public void setToClass(String toClass) {
        // Not implemented
    }

}
