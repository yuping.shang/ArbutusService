package syscon.arbutus.product.services.legal.contract.dto.charge;

import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.legal.contract.dto.ValidationUtil;

/**
 * The representation of the statute charge configuration search type
 */
public class StatuteChargeConfigSearchType implements Serializable {

    private static final long serialVersionUID = -7110843513686829582L;

    private Long statuteId;
    private String chargeCode;
    private StatuteChargeTextSearchType chargeTextSearch;
    private String chargeType;
    private String chargeDegree;
    private String chargeCategory;
    private String chargeSeverity;
    private Boolean bailAllowed;
    private Boolean bondAllowed;
    private Long bailAmount;
    private Date startDateFrom;
    private Date startDateTo;
    private Date endDateFrom;
    private Date endDateTo;
    private ExternalChargeCodeConfigSearchType externalChargeCodeSearch;
    private ChargeIndicatorSearchType chargeIndicatorSearch;
    private String enhancingFactor;
    private String reducingFactor;

    /**
     * Default constructor
     */
    public StatuteChargeConfigSearchType() {
    }

    /**
     * Constructor
     * At least one parameter must be provided
     *
     * @param statuteId                Long - the statute Id which refer to statute configuration instance.
     * @param chargeCode               String - the charge code defined for a statute jurisdiction.
     * @param chargeTextSearch         StatuteChargeTextSearchType - the legal text associated to the charge.
     * @param chargeType               String - the type of a charge (Felony, Misdemeanor)
     * @param chargeDegree             String - the degree of a charge
     * @param chargeCategory           String - the category the charge belongs to
     * @param chargeSeverity           String - the severity of a charge
     * @param bailAllowed              Boolean - true if bail allowed is allowed for this charge code
     * @param bondAllowed              Boolean - true if bond is allowed for this charge
     * @param bailAmount               Long - the bail amount recommended by the statute charge code.
     * @param startDateFrom            Date - the date on which this charge code was validated
     * @param startDateTo              Date - the date on which this charge code was validated
     * @param endDateFrom              Date - the date on which this charge code was invalidated
     * @param externalChargeCodeSearch ExternalChargeCodeConfigSearchType - the External charge code (NCIC, UCR) for the statute charge code
     * @param endDateTo                Date - the date on which this charge code was invalidated
     * @param chargeIndicatorSearch    ChargeIndicatorSearchType - the indicators (alerts) on a charge
     * @param enhancingFactor          String - the factor or reason that makes a charge more serious
     * @param reducingFactor           String - the factor which make a charge less serious
     */
    public StatuteChargeConfigSearchType(Long statuteId, String chargeCode, StatuteChargeTextSearchType chargeTextSearch, String chargeType, String chargeDegree,
            String chargeCategory, String chargeSeverity, Boolean bailAllowed, Boolean bondAllowed, Long bailAmount, Date startDateFrom, Date startDateTo,
            Date endDateFrom, Date endDateTo, ExternalChargeCodeConfigSearchType externalChargeCodeSearch, ChargeIndicatorSearchType chargeIndicatorSearch,
            String enhancingFactor, String reducingFactor) {
        this.statuteId = statuteId;
        this.chargeCode = chargeCode;
        this.chargeTextSearch = chargeTextSearch;
        this.chargeType = chargeType;
        this.chargeDegree = chargeDegree;
        this.chargeCategory = chargeCategory;
        this.chargeSeverity = chargeSeverity;
        this.bailAllowed = bailAllowed;
        this.bondAllowed = bondAllowed;
        this.bailAmount = bailAmount;
        this.startDateFrom = startDateFrom;
        this.startDateTo = startDateTo;
        this.endDateFrom = endDateFrom;
        this.endDateTo = endDateTo;
        this.externalChargeCodeSearch = externalChargeCodeSearch;
        this.chargeIndicatorSearch = chargeIndicatorSearch;
        this.enhancingFactor = enhancingFactor;
        this.reducingFactor = reducingFactor;
    }

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set, false otherwise.
     */
    @Deprecated
    public Boolean isWellFormed() {

        if (ValidationUtil.isEmpty(statuteId) && ValidationUtil.isEmpty(chargeCode) && ValidationUtil.isEmpty(chargeType) && ValidationUtil.isEmpty(chargeDegree)
                && ValidationUtil.isEmpty(chargeCategory) && ValidationUtil.isEmpty(chargeSeverity) && ValidationUtil.isEmpty(bailAllowed) && ValidationUtil.isEmpty(
                bondAllowed) && ValidationUtil.isEmpty(bailAmount) && ValidationUtil.isEmpty(startDateFrom) && ValidationUtil.isEmpty(startDateTo)
                && ValidationUtil.isEmpty(endDateFrom) && ValidationUtil.isEmpty(endDateTo) && ValidationUtil.isEmpty(enhancingFactor) && ValidationUtil.isEmpty(
                reducingFactor) && (chargeTextSearch == null || !chargeTextSearch.isWellFormed()) && (externalChargeCodeSearch == null
                || !externalChargeCodeSearch.isWellFormed()) && (chargeIndicatorSearch == null || !chargeIndicatorSearch.isWellFormed())) {
			return false;
		}

        return true;
    }

    /**
     * Verify if properties are set properly.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set and all values are valid, false otherwise.
     */
    @Deprecated
    public Boolean isValid() {

        if (!isWellFormed()) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the statuteId property.
     *
     * @return Long - the statute Id which refer to statute configuration instance.
     */
    public Long getStatuteId() {
        return statuteId;
    }

    /**
     * Sets the value of the statuteId property.
     *
     * @param statuteId Long - the statute Id which refer to statute configuration instance.
     */
    public void setStatuteId(Long statuteId) {
        this.statuteId = statuteId;
    }

    /**
     * Gets the value of the chargeCode property.
     * It is a reference code value of ChargeCode set.
     *
     * @return String - the charge code defined for a statute jurisdiction.
     */
    public String getChargeCode() {
        return chargeCode;
    }

    /**
     * Sets the value of the chargeCode property.
     * It is a reference code value of ChargeCode set.
     *
     * @param chargeCode String - the charge code defined for a statute jurisdiction.
     */
    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    /**
     * Gets the value of the chargeTextSearch property.
     *
     * @return StatuteChargeTextSearchType - the legal text associated to the charge.
     */
    public StatuteChargeTextSearchType getChargeTextSearch() {
        return chargeTextSearch;
    }

    /**
     * Sets the value of the chargeTextSearch property.
     *
     * @param chargeTextSearch StatuteChargeTextSearchType - the legal text associated to the charge.
     */
    public void setChargeTextSearch(StatuteChargeTextSearchType chargeTextSearch) {
        this.chargeTextSearch = chargeTextSearch;
    }

    /**
     * Gets the value of the chargeType property.
     * It is a reference code value of ChargeCode set.
     *
     * @return String - the type of a charge (Felony, Misdemeanor)
     */
    public String getChargeType() {
        return chargeType;
    }

    /**
     * Sets the value of the chargeType property.
     * It is a reference code value of ChargeCode set.
     *
     * @param chargeType String - the type of a charge (Felony, Misdemeanor)
     */
    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    /**
     * Gets the value of the chargeDegree property.
     * It is a reference code value of ChargeDegree set.
     *
     * @return String - the degree of a charge
     */
    public String getChargeDegree() {
        return chargeDegree;
    }

    /**
     * Sets the value of the chargeDegree property.
     * It is a reference code value of ChargeDegree set.
     *
     * @param chargeDegree String - the degree of a charge
     */
    public void setChargeDegree(String chargeDegree) {
        this.chargeDegree = chargeDegree;
    }

    /**
     * Gets the value of the chargeCategory property.
     * It is a reference code value of ChargeCategory set.
     *
     * @return String - the category the charge belongs to
     */
    public String getChargeCategory() {
        return chargeCategory;
    }

    /**
     * Sets the value of the chargeCategory property.
     * It is a reference code value of ChargeCategory set.
     *
     * @param chargeCategory String - the category the charge belongs to
     */
    public void setChargeCategory(String chargeCategory) {
        this.chargeCategory = chargeCategory;
    }

    /**
     * Gets the value of the chargeSeverity property.
     * It is a reference code value of ChargeSeverity set.
     *
     * @return String - the severity of a charge
     */
    public String getChargeSeverity() {
        return chargeSeverity;
    }

    /**
     * Sets the value of the chargeSeverity property.
     * It is a reference code value of ChargeSeverity set.
     *
     * @param chargeSeverity String - the severity of a charge
     */
    public void setChargeSeverity(String chargeSeverity) {
        this.chargeSeverity = chargeSeverity;
    }

    /**
     * Gets the value of the bailAllowed property.
     *
     * @return Boolean - true if bail allowed is allowed for this charge code
     */
    public Boolean isBailAllowed() {
        return bailAllowed;
    }

    /**
     * Sets the value of the bailAllowed property.
     *
     * @param bailAllowed Boolean - true if bail allowed is allowed for this charge code
     */
    public void setBailAllowed(Boolean bailAllowed) {
        this.bailAllowed = bailAllowed;
    }

    /**
     * Gets the value of the bondAllowed property.
     *
     * @return Boolean - true if bond is allowed for this charge
     */
    public Boolean isBondAllowed() {
        return bondAllowed;
    }

    /**
     * Sets the value of the bondAllowed property.
     *
     * @param bondAllowed Boolean - true if bond is allowed for this charge
     */
    public void setBondAllowed(Boolean bondAllowed) {
        this.bondAllowed = bondAllowed;
    }

    /**
     * Gets the value of the bailAmount property.
     *
     * @return Long - the bail amount recommended by the statute charge code
     */
    public Long getBailAmount() {
        return bailAmount;
    }

    /**
     * Sets the value of the bailAmount property.
     *
     * @param bailAmount Long - the bail amount recommended by the statute charge code
     */
    public void setBailAmount(Long bailAmount) {
        this.bailAmount = bailAmount;
    }

    /**
     * Gets the value of the startDateFrom property.
     *
     * @return Date - the date on which this charge code was validated
     */
    public Date getStartDateFrom() {
        return startDateFrom;
    }

    /**
     * Sets the value of the startDateFrom property.
     *
     * @param startDateFrom Date - the date on which this charge code was validated
     */
    public void setStartDateFrom(Date startDateFrom) {
        this.startDateFrom = startDateFrom;
    }

    /**
     * Gets the value of the startDateTo property.
     *
     * @return Date - the date on which this charge code was validated
     */
    public Date getStartDateTo() {
        return startDateTo;
    }

    /**
     * Sets the value of the startDateTo property.
     *
     * @param startDateTo Date - the date on which this charge code was validated
     */
    public void setStartDateTo(Date startDateTo) {
        this.startDateTo = startDateTo;
    }

    /**
     * Gets the value of the endDateFrom property.
     *
     * @return Date - the date on which this charge code was invalidated
     */
    public Date getEndDateFrom() {
        return endDateFrom;
    }

    /**
     * Sets the value of the endDateFrom property.
     *
     * @param endDateFrom Date - the date on which this charge code was invalidated
     */
    public void setEndDateFrom(Date endDateFrom) {
        this.endDateFrom = endDateFrom;
    }

    /**
     * Gets the value of the endDateTo property.
     *
     * @return Date - the date on which this charge code was invalidated
     */
    public Date getEndDateTo() {
        return endDateTo;
    }

    /**
     * Sets the value of the endDateTo property.
     *
     * @param endDateTo Date - the date on which this charge code was invalidated
     */
    public void setEndDateTo(Date endDateTo) {
        this.endDateTo = endDateTo;
    }

    /**
     * Gets the value of the externalChargeCodeSearch property.
     *
     * @return ExternalChargeCodeConfigSearchType - the External charge code (NCIC, UCR) for the statute charge code
     */
    public ExternalChargeCodeConfigSearchType getExternalChargeCodeSearch() {
        return externalChargeCodeSearch;
    }

    /**
     * Sets the value of the externalChargeCodeSearch property.
     *
     * @param externalChargeCodeSearch ExternalChargeCodeConfigSearchType - the External charge code (NCIC, UCR) for the statute charge code
     */
    public void setExternalChargeCodeSearch(ExternalChargeCodeConfigSearchType externalChargeCodeSearch) {
        this.externalChargeCodeSearch = externalChargeCodeSearch;
    }

    /**
     * Gets the value of the chargeIndicatorSearch property.
     *
     * @return ChargeIndicatorSearchType - the indicators (alerts) on a charge
     */
    public ChargeIndicatorSearchType getChargeIndicatorSearch() {
        return chargeIndicatorSearch;
    }

    /**
     * Sets the value of the chargeIndicatorSearch property.
     *
     * @param chargeIndicatorSearch ChargeIndicatorSearchType - the indicators (alerts) on a charge
     */
    public void setChargeIndicatorSearch(ChargeIndicatorSearchType chargeIndicatorSearch) {
        this.chargeIndicatorSearch = chargeIndicatorSearch;
    }

    /**
     * Gets the value of the enhancingFactor property.
     * It is a reference code value of EnhancingFactor set.
     *
     * @return String - the factor or reason that makes a charge more serious
     */
    public String getEnhancingFactor() {
        return enhancingFactor;
    }

    /**
     * Sets the value of the enhancingFactor property.
     * It is a reference code value of EnhancingFactor set.
     *
     * @param enhancingFactor String - the factor or reason that makes a charge more serious
     */
    public void setEnhancingFactor(String enhancingFactor) {
        this.enhancingFactor = enhancingFactor;
    }

    /**
     * Gets the value of the reducingFactor property.
     * It is a reference code value of ReducingFactor set.
     *
     * @return String - the factor which make a charge less serious
     */
    public String getReducingFactor() {
        return reducingFactor;
    }

    /**
     * Sets the value of the reducingFactor property.
     * It is a reference code value of ReducingFactor set.
     *
     * @param reducingFactor String - the factor which make a charge less serious
     */
    public void setReducingFactor(String reducingFactor) {
        this.reducingFactor = reducingFactor;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StatuteChargeConfigSearchType [statuteId=" + statuteId + ", chargeCode=" + chargeCode + ", chargeTextSearch=" + chargeTextSearch + ", chargeType="
                + chargeType + ", chargeDegree=" + chargeDegree + ", chargeCategory=" + chargeCategory + ", chargeSeverity=" + chargeSeverity + ", bailAllowed="
                + bailAllowed + ", bondAllowed=" + bondAllowed + ", bailAmount=" + bailAmount + ", startDateFrom=" + startDateFrom + ", startDateTo=" + startDateTo
                + ", endDateFrom=" + endDateFrom + ", endDateTo=" + endDateTo + ", externalChargeCodeSearch=" + externalChargeCodeSearch + ", chargeIndicatorSearch="
                + chargeIndicatorSearch + ", enhancingFactor=" + enhancingFactor + ", reducingFactor=" + reducingFactor + "]";
    }

}
