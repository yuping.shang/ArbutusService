package syscon.arbutus.product.services.legal.realization.adapters;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;
import syscon.arbutus.product.services.utility.ServiceAdapter;

/**
 * An adapter class to wrap the API in SupervisionService and provide the legal service bean customized API.
 *
 * @author lhan
 */
public class SupervisionServiceAdapter {

    private static SupervisionService service;

    /**
     * Constructor.
     */
    public SupervisionServiceAdapter() {
        super();
    }

    synchronized private static SupervisionService getService() {

        if (service == null) {
            service = (SupervisionService) ServiceAdapter.JNDILookUp(service, SupervisionService.class);
        }
        return service;
    }

    /**
     * @param userContext
     * @param supervisionId
     * @return
     */
    public SupervisionType get(UserContext userContext, Long supervisionId) {

        SupervisionType ret = null;

        ret = getService().get(userContext, supervisionId);

        if (ret == null) {
            throw new ArbutusRuntimeException("get supervision failed.supervisionId=" + supervisionId);
        }

        return ret;
    }

    /**
     * @param supervison
     * @return
     */
    public Long getPersonIdentityId(SupervisionType supervison) {

        return supervison.getPersonIdentityId();
    }
}
