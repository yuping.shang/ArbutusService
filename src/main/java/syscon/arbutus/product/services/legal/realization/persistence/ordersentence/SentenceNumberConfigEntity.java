package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * SentenceNumberConfigEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdSntNumCfg 'Sentence Number Configuration table for Order Sentence Module of Legal Service'
 * @since December 31, 2012
 */
@Audited
@Entity
@Table(name = "LEG_OrdSntNumCfg")
public class SentenceNumberConfigEntity implements Serializable {

    private static final long serialVersionUID = -722231366358692735L;

    /**
     * @DbComment sentenceNumConfigId 'Unique identification of a Sentence Number Configuration instance'
     */
    @Id
    @Column(name = "sentenceNumConfigId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDSNTNUMCFG_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDSNTNUMCFG_ID", sequenceName = "SEQ_LEG_ORDSNTNUMCFG_ID", allocationSize = 1)
    private Long sentenceNumConfigId;

    /**
     * @DbComment numGenerateFlag 'If Y, system auto generates the sentence number; If N, system does not auto generate the sentence number.'
     */
    @Column(name = "numGenerateFlag", nullable = false)
    private Boolean sentencceNumberGenerationFlag;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * Constructor
     */
    public SentenceNumberConfigEntity() {
        super();
    }

    /**
     * Constructor
     *
     * @param sentenceNumConfigId
     * @param sentencceNumberGenerationFlag
     * @param stamp
     */
    public SentenceNumberConfigEntity(Long sentenceNumConfigId, Boolean sentencceNumberGenerationFlag, StampEntity stamp) {
        super();
        this.sentenceNumConfigId = sentenceNumConfigId;
        this.sentencceNumberGenerationFlag = sentencceNumberGenerationFlag;
        this.stamp = stamp;
    }

    /**
     * @return the sentenceNumConfigId
     */
    public Long getSentenceNumConfigId() {
        return sentenceNumConfigId;
    }

    /**
     * @param sentenceNumConfigId the sentenceNumConfigId to set
     */
    public void setSentenceNumConfigId(Long sentenceNumConfigId) {
        this.sentenceNumConfigId = sentenceNumConfigId;
    }

    /**
     * @return the sentencceNumberGenerationFlag
     */
    public Boolean getSentencceNumberGenerationFlag() {
        return sentencceNumberGenerationFlag;
    }

    /**
     * @param sentencceNumberGenerationFlag the sentencceNumberGenerationFlag to set
     */
    public void setSentencceNumberGenerationFlag(Boolean sentencceNumberGenerationFlag) {
        this.sentencceNumberGenerationFlag = sentencceNumberGenerationFlag;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sentencceNumberGenerationFlag == null) ? 0 : sentencceNumberGenerationFlag.hashCode());
        result = prime * result + ((sentenceNumConfigId == null) ? 0 : sentenceNumConfigId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SentenceNumberConfigEntity other = (SentenceNumberConfigEntity) obj;
        if (sentencceNumberGenerationFlag == null) {
            if (other.sentencceNumberGenerationFlag != null) {
				return false;
			}
        } else if (!sentencceNumberGenerationFlag.equals(other.sentencceNumberGenerationFlag)) {
			return false;
		}
        if (sentenceNumConfigId == null) {
            if (other.sentenceNumConfigId != null) {
				return false;
			}
        } else if (!sentenceNumConfigId.equals(other.sentenceNumConfigId)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SentenceNumberConfigurationEntity [sentenceNumConfigId=" + sentenceNumConfigId + ", sentencceNumberGenerationFlag=" + sentencceNumberGenerationFlag
                + ", stamp=" + stamp + "]";
    }

}
