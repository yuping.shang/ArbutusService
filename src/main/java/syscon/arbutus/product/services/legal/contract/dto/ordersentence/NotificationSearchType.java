package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import java.io.Serializable;
import java.util.Set;

/**
 * NotificationSearchType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 21, 2012
 */
public class NotificationSearchType implements Serializable {

    private static final long serialVersionUID = -737931375114512736L;

    private Long notificationOrganizationAssociation;

    private Long notificationFacilityAssociation;

    private Boolean isNotificationNeeded;

    private Boolean isNotificationConfirmed;

    private Set<Long> notificationAgencyLocationAssociations;

    private String notificationAgencyContact;

    /**
     * Constructor
     */
    public NotificationSearchType() {
        super();
    }

    /**
     * Constructor
     * <p>At least one argument must be provided.
     *
     * @param notificationOrganizationAssociation    -- Long The entity (organization) to be notitied. Static reference to organization.
     * @param notificationFacilityAssociation        Long -- The entity (facility) to be notified. Static reference to facility.
     * @param isNotificationNeeded                   Boolean -- If true then notification is required to be sent before releasing the offender, false otherwise.
     * @param isNotificationConfirmed                Boolean -- If true, then the notification confirmation was received, false otherwise. Default is false.
     * @param notificationAgencyLocationAssociations Long -- The address/phone details of the agency (organization/facility) to be notified. Static association to location.
     * @param notificationAgencyContact              String(length <= 128) -- The details (name) of the person to be notified.
     */
    public NotificationSearchType(Long notificationOrganizationAssociation, Long notificationFacilityAssociation, Boolean isNotificationNeeded,
            Boolean isNotificationConfirmed, Set<Long> notificationAgencyLocationAssociations, String notificationAgencyContact) {
        super();
        this.notificationOrganizationAssociation = notificationOrganizationAssociation;
        this.notificationFacilityAssociation = notificationFacilityAssociation;
        this.isNotificationNeeded = isNotificationNeeded;
        this.isNotificationConfirmed = isNotificationConfirmed;
        this.notificationAgencyLocationAssociations = notificationAgencyLocationAssociations;
        this.notificationAgencyContact = notificationAgencyContact;
    }

    /**
     * The entity (organization) to be notitied. Static reference to organization.
     *
     * @return the notificationOrganizationAssociation
     */
    public Long getNotificationOrganizationAssociation() {
        return notificationOrganizationAssociation;
    }

    /**
     * The entity (organization) to be notitied. Static reference to organization.
     *
     * @param notificationOrganizationAssociation the notificationOrganizationAssociation to set
     */
    public void setNotificationOrganizationAssociation(Long notificationOrganizationAssociation) {
        this.notificationOrganizationAssociation = notificationOrganizationAssociation;
    }

    /**
     * The entity (facility) to be notified. Static reference to facility.
     *
     * @return the notificationFacilityAssociation
     */
    public Long getNotificationFacilityAssociation() {
        return notificationFacilityAssociation;
    }

    /**
     * The entity (facility) to be notified. Static reference to facility.
     *
     * @param notificationFacilityAssociation the notificationFacilityAssociation to set
     */
    public void setNotificationFacilityAssociation(Long notificationFacilityAssociation) {
        this.notificationFacilityAssociation = notificationFacilityAssociation;
    }

    /**
     * Is Notification Needed:
     * <p>If true then notification is required to be sent before releasing the offender, false otherwise.
     *
     * @return the isNotificationNeeded
     */
    public Boolean getIsNotificationNeeded() {
        return isNotificationNeeded;
    }

    /**
     * Is Notification Needed:
     * <p>If true then notification is required to be sent before releasing the offender, false otherwise.
     *
     * @param isNotificationNeeded the isNotificationNeeded to set
     */
    public void setIsNotificationNeeded(Boolean isNotificationNeeded) {
        this.isNotificationNeeded = isNotificationNeeded;
    }

    /**
     * Is Notification Confirmed:
     * <p>If true, then the notification confirmation was received, false otherwise. Default is false.
     *
     * @return the isNotificationConfirmed
     */
    public Boolean getIsNotificationConfirmed() {
        return isNotificationConfirmed;
    }

    /**
     * Is Notification Confirmed:
     * <p>If true, then the notification confirmation was received, false otherwise. Default is false.
     *
     * @param isNotificationConfirmed the isNotificationConfirmed to set
     */
    public void setIsNotificationConfirmed(Boolean isNotificationConfirmed) {
        this.isNotificationConfirmed = isNotificationConfirmed;
    }

    /**
     * Association to LocationService
     * <p>The address/phone details of the agency (organization/facility) to be notified. Static association to location.
     *
     * @return the notificationAgencyLocationAssociations
     */
    public Set<Long> getNotificationAgencyLocationAssociations() {
        return notificationAgencyLocationAssociations;
    }

    /**
     * Association to LocationService
     * <p>The address/phone details of the agency (organization/facility) to be notified. Static association to location.
     *
     * @param notificationAgencyLocationAssociations the notificationAgencyLocationAssociations to set
     */
    public void setNotificationAgencyLocationAssociations(Set<Long> notificationAgencyLocationAssociations) {
        this.notificationAgencyLocationAssociations = notificationAgencyLocationAssociations;
    }

    /**
     * Notification Agency Contact
     * <p>The details (name) of the person to be notified.
     *
     * @return the notificationAgencyContact
     */
    public String getNotificationAgencyContact() {
        return notificationAgencyContact;
    }

    /**
     * Notification Agency Contact
     * <p>The details (name) of the person to be notified.
     *
     * @param notificationAgencyContact the notificationAgencyContact to set
     */
    public void setNotificationAgencyContact(String notificationAgencyContact) {
        this.notificationAgencyContact = notificationAgencyContact;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "NotificationSearchType [notificationOrganizationAssociation=" + notificationOrganizationAssociation + ", notificationFacilityAssociation="
                + notificationFacilityAssociation + ", isNotificationNeeded=" + isNotificationNeeded + ", isNotificationConfirmed=" + isNotificationConfirmed
                + ", notificationAgencyLocationAssociations=" + notificationAgencyLocationAssociations + ", notificationAgencyContact=" + notificationAgencyContact + "]";
    }

}
