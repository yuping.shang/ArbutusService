package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * SupervisionSentenceEntity for OrderSentence of Legal
 *
 * @author yshang
 * @DbComment LEG_OrdSupSent 'Supervision Sentence Table.'
 */
@Audited
@Entity
@Table(name = "LEG_OrdSupSent")
@SQLDelete(sql = "UPDATE LEG_OrdSupSent SET flag = 4 WHERE orderId = ? and version = ?")
@Where(clause = "flag = 1")
public class SupervisionSentenceEntity implements Serializable {

    private static final long serialVersionUID = 4427536636668893276L;

    /**
     * @DbComment supSentId 'Id of Supervision Sentence (table LEG_OrdSupSent)'
     */
    @Id
    @Column(name = "supSentId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDSUPSENT_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDSUPSENT_ID", sequenceName = "SEQ_LEG_ORDSUPSENT_ID", allocationSize = 1)
    private Long supervisionSentenceId;

    /**
     * @DbComment supervisionId 'Supervision Id, the identification of table SUP_Supervision'
     */
    @Column(name = "supervisionId", nullable = false)
    private Long supervisionId;

    /**
     * @DbComment sentenceStartDate 'Sentence Start Date calculated by SentenceCalculation engine'
     */
    @Column(name = "sentenceStartDate", nullable = true)
    private Date sentenceStartDate;

    /**
     * @DbComment paroleEligibilityDate 'Parole Eligibility Date calculated by SentenceCalculation engine'
     */
    @Column(name = "paroleEligibilityDate", nullable = true)
    private Date paroleEligibilityDate;

    /**
     * @DbComment probableDischargeDate 'Probable Discharge Date calculated by SentenceCalculation engine'
     */
    @Column(name = "probableDischargeDate", nullable = true)
    private Date probableDischargeDate;

    /**
     * @DbComment sentenceExpiryDate 'Sentence Expiry Date'
     */
    @Column(name = "sentenceExpiryDate")
    private Date sentenceExpiryDate;

    /**
     * @DbComment goodTime 'Good time calculated by SentenceCalculation engine'
     */
    @Column(name = "goodTime", nullable = true)
    private Long goodTime;

    /**
     * @DbComment sentenceDays 'Sentence days calculated by SentenceCalculation engine'
     */
    @Column(name = "sentenceDays", nullable = true)
    private Long sentenceDays;

    /**
     * @DbComment daysToServe 'Days to serve for the supervision'
     */
    @Column(name = "daysToServe", nullable = true)
    private Long daysToServe;

    /**
     * @DbComment version 'Version for Hibernate use'
     */
    @Version
    private Long version;

    /**
     * @DbComment isHistory 'True: is history; False: current recorder'
     */
    @Column(name = "isHistory", nullable = true)
    private Boolean isHistory;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * @DbComment keyDateHistoryId 'Id of KeyDates history'
     */
    @Column(name = "keyDateHistoryId", nullable = true)
    private Long keyDateHistoryId;

    @OneToMany(mappedBy = "supervisionSentence", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @OrderBy("aggregateId DESC")
    private List<AggregateSentenceEntity> aggregateSentences;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public SupervisionSentenceEntity() {
        super();
    }

    /**
     * @return the supervisionSentenceId
     */
    public Long getSupervisionSentenceId() {
        return supervisionSentenceId;
    }

    /**
     * @param supervisionSentenceId the supervisionSentenceId to set
     */
    public void setSupervisionSentenceId(Long supervisionSentenceId) {
        this.supervisionSentenceId = supervisionSentenceId;
    }

    /**
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * @return the sentenceStartDate
     */
    public Date getSentenceStartDate() {
        return sentenceStartDate;
    }

    /**
     * @param sentenceStartDate the sentenceStartDate to set
     */
    public void setSentenceStartDate(Date sentenceStartDate) {
        this.sentenceStartDate = sentenceStartDate;
    }

    /**
     * @return the paroleEligibilityDate
     */
    public Date getParoleEligibilityDate() {
        return paroleEligibilityDate;
    }

    /**
     * @param paroleEligibilityDate the paroleEligibilityDate to set
     */
    public void setParoleEligibilityDate(Date paroleEligibilityDate) {
        this.paroleEligibilityDate = paroleEligibilityDate;
    }

    /**
     * @return the probableDischargeDate
     */
    public Date getProbableDischargeDate() {
        return probableDischargeDate;
    }

    /**
     * @param probableDischargeDate the probableDischargeDate to set
     */
    public void setProbableDischargeDate(Date probableDischargeDate) {
        this.probableDischargeDate = probableDischargeDate;
    }

    /**
     * @return the sentenceExpiryDate
     */
    public Date getSentenceExpiryDate() {
        return sentenceExpiryDate;
    }

    /**
     * @param sentenceExpiryDate the sentenceExpiryDate to set
     */
    public void setSentenceExpiryDate(Date sentenceExpiryDate) {
        this.sentenceExpiryDate = sentenceExpiryDate;
    }

    /**
     * @return the goodTime
     */
    public Long getGoodTime() {
        return goodTime;
    }

    /**
     * @param goodTime the goodTime to set
     */
    public void setGoodTime(Long goodTime) {
        this.goodTime = goodTime;
    }

    /**
     * @return the sentenceDays
     */
    public Long getSentenceDays() {
        return sentenceDays;
    }

    /**
     * @param sentenceDays the sentenceDays to set
     */
    public void setSentenceDays(Long sentenceDays) {
        this.sentenceDays = sentenceDays;
    }

    /**
     * @return the daysToServe
     */
    public Long getDaysToServe() {
        return daysToServe;
    }

    /**
     * @param daysToServe the daysToServe to set
     */
    public void setDaysToServe(Long daysToServe) {
        this.daysToServe = daysToServe;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the isHistory
     */
    public Boolean getIsHistory() {
        return isHistory;
    }

    /**
     * @param isHistory the isHistory to set
     */
    public void setIsHistory(Boolean isHistory) {
        this.isHistory = isHistory;
    }

    /**
     * @return the keyDateHistoryId
     */
    public Long getKeyDateHistoryId() {
        return keyDateHistoryId;
    }

    /**
     * @param keyDateHistoryId the keyDateHistoryId to set
     */
    public void setKeyDateHistoryId(Long keyDateHistoryId) {
        this.keyDateHistoryId = keyDateHistoryId;
    }

    /**
     * @return the aggregateSentences
     */
    public List<AggregateSentenceEntity> getAggregateSentences() {
        if (aggregateSentences == null) {
            aggregateSentences = new ArrayList<>();
        }
        return aggregateSentences;
    }

    /**
     * @param aggregateSentences the aggregateSentences to set
     */
    public void setAggregateSentences(List<AggregateSentenceEntity> aggregateSentences) {
        this.aggregateSentences = aggregateSentences;
    }

    public void addAggregateSentence(AggregateSentenceEntity aggregateSentence) {
        aggregateSentence.setSupervisionSentence(this);
        getAggregateSentences().add(aggregateSentence);
    }

    /**
     * @return the flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((daysToServe == null) ? 0 : daysToServe.hashCode());
        result = prime * result + ((goodTime == null) ? 0 : goodTime.hashCode());
        result = prime * result + ((isHistory == null) ? 0 : isHistory.hashCode());
        result = prime * result + ((keyDateHistoryId == null) ? 0 : keyDateHistoryId.hashCode());
        result = prime * result + ((paroleEligibilityDate == null) ? 0 : paroleEligibilityDate.hashCode());
        result = prime * result + ((probableDischargeDate == null) ? 0 : probableDischargeDate.hashCode());
        result = prime * result + ((sentenceDays == null) ? 0 : sentenceDays.hashCode());
        result = prime * result + ((sentenceExpiryDate == null) ? 0 : sentenceExpiryDate.hashCode());
        result = prime * result + ((sentenceStartDate == null) ? 0 : sentenceStartDate.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        result = prime * result + ((supervisionSentenceId == null) ? 0 : supervisionSentenceId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SupervisionSentenceEntity other = (SupervisionSentenceEntity) obj;
        if (daysToServe == null) {
            if (other.daysToServe != null) {
				return false;
			}
        } else if (!daysToServe.equals(other.daysToServe)) {
			return false;
		}
        if (goodTime == null) {
            if (other.goodTime != null) {
				return false;
			}
        } else if (!goodTime.equals(other.goodTime)) {
			return false;
		}
        if (isHistory == null) {
            if (other.isHistory != null) {
				return false;
			}
        } else if (!isHistory.equals(other.isHistory)) {
			return false;
		}
        if (keyDateHistoryId == null) {
            if (other.keyDateHistoryId != null) {
				return false;
			}
        } else if (!keyDateHistoryId.equals(other.keyDateHistoryId)) {
			return false;
		}
        if (paroleEligibilityDate == null) {
            if (other.paroleEligibilityDate != null) {
				return false;
			}
        } else if (!paroleEligibilityDate.equals(other.paroleEligibilityDate)) {
			return false;
		}
        if (probableDischargeDate == null) {
            if (other.probableDischargeDate != null) {
				return false;
			}
        } else if (!probableDischargeDate.equals(other.probableDischargeDate)) {
			return false;
		}
        if (sentenceDays == null) {
            if (other.sentenceDays != null) {
				return false;
			}
        } else if (!sentenceDays.equals(other.sentenceDays)) {
			return false;
		}
        if (sentenceExpiryDate == null) {
            if (other.sentenceExpiryDate != null) {
				return false;
			}
        } else if (!sentenceExpiryDate.equals(other.sentenceExpiryDate)) {
			return false;
		}
        if (sentenceStartDate == null) {
            if (other.sentenceStartDate != null) {
				return false;
			}
        } else if (!sentenceStartDate.equals(other.sentenceStartDate)) {
			return false;
		}
        if (supervisionId == null) {
            if (other.supervisionId != null) {
				return false;
			}
        } else if (!supervisionId.equals(other.supervisionId)) {
			return false;
		}
        if (supervisionSentenceId == null) {
            if (other.supervisionSentenceId != null) {
				return false;
			}
        } else if (!supervisionSentenceId.equals(other.supervisionSentenceId)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SupervisionSentenceEntity [supervisionSentenceId=" + supervisionSentenceId + ", supervisionId=" + supervisionId + ", sentenceStartDate="
                + sentenceStartDate + ", paroleEligibilityDate=" + paroleEligibilityDate + ", probableDischargeDate=" + probableDischargeDate + ", sentenceExpiryDate="
                + sentenceExpiryDate + ", goodTime=" + goodTime + ", sentenceDays=" + sentenceDays + ", daysToServe=" + daysToServe + ", isHistory=" + isHistory
                + ", keyDateHistoryId=" + keyDateHistoryId + ", aggregateSentences=" + aggregateSentences + "]";
    }

}
