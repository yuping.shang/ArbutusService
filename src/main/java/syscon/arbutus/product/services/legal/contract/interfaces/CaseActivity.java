package syscon.arbutus.product.services.legal.contract.interfaces;

import java.util.Date;
import java.util.List;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CaseActivitySearchType;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivitiesReturnType;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivityType;

/**
 * Interface of CaseActivity Service
 * <pre>
 * <b>Purpose</b>
 * The CASE ACTIVITY service provides the ability to record and maintain all case related activities which occur during the lifetime of a case, from the initial report of a crime to an arrest through to subsequent court proceedings and eventual disposition.
 * It is the primary log of interactions between each offender or inmate and the criminal justice agencies, including the police, courts and parole boards.
 * This service also provides scheduling capabilities which can be used to plan events ahead of time, or schedule recurring events which must occur over a period of time.
 * The CASE ACTIVITY service connects to case information (CASE service), order/sentence information (ORDER/SENTENCE service), parties involved (CASE AFFILIATION service) and the various police/court facilities (FACILITY service) where the events take place.
 * <b>Scope</b>
 * For JMS 1.0, the scope of the CASE ACTIVITY service is limited to the creation and maintenance of court events related to a case, as well as the maintenance of the reference data used by the service. This includes the following:
 * 	<li>Search, create, update or delete a case activity;
 * 	<li>Maintain activity types;
 * 	<li>Maintain actvity outcomes;
 * 	<li>Maintain hearing types specifically for court events;
 * 	<li>Maintain an association to orders that drive and result from events such as warrants, detainers, holds and sentencing orders.
 * JMS 2.0 will expand the scope of the events captured to include the police, probation/parole and community requirements.
 * Note that the CASE ACTIVITY service is activity-based and is therefore regarded as an activity augmentation service. It maintains a close association to the ACTIVITY service and leverages that service’s scheduling capabilities to maintain events that are planned ahead of time.
 * The CASE ACTIVITY service will apply user data privileges to support the sealing of case activities and restricting access to high profile offender data. Versioning will also be maintained so that a history of changes can be recorded and viewed.
 * The service will provide the ability for data exchange through NIEM.
 * <b>Out of Scope</b>
 * 	<li>Events and activities related to the police domain eg. arrests, investigations;
 * 	<li>Any pre-trial activities that occur prior to the offender’s first appearance in court;
 * 	<li>Events and activities related to probation/parole;
 * 	<li>Events and activities related to programs (including community programs);
 * 	<li>Events and activities related to juvenile cases.
 * <b>JNDI Lookup Name:</b> CaseActivityService
 * <b>Example of invocation of the service:</b>
 * 	Properties p = new Properties();
 * 	properties.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
 * 	properties.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
 * 	properties.put("java.naming.provider.url", hostAddress);  // hostAddress -- specified accordingly
 * 	Context ctx = new InitialContext(p);
 * 	MovementService service = (CaseActivityService)ctx.lookup("CaseActivityService");
 * 	SecurityClient client = SecurityClientFactory.getSecurityClient();
 * 	client.setSimple(userId, password);
 * 	client.login();
 * 	UserContext uc = new UserContext();
 * 	uc.setConsumingApplicationId(clientAppId);
 * 	uc.setFacilityId(facilityId);
 * 	uc.setDeviceId(deviceId);
 * 	uc.setProcess(process);
 * 	uc.setTask(task);
 * 	uc.setTimestamp(timestamp);
 * 	MovementActivityReturnType orgRet = service.create(uc, ...);
 * 	Long count = service.getCount(uc);
 * 	... ...
 * <img src="doc-files/CaseActivityUML.png"/> <br/>
 * <b>Parameter Specifications</b>
 * <li>Required - Must be presented.</li>
 * <li>Optional - If presented it will be considered.  If not presented it will be considered with some default value (even if default value is null).</li>
 * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
 * for all error codes and descriptions.
 * </pre>
 */
public interface CaseActivity {

    /**
     * create -- Create a Court Activity.
     * <p>A Case association must exist when creating a court activity.
     * A corresponding Activity association must exist when creating a court activity.
     * The schedule details (date and time) wil be captured in the Activity service and the other court hearing details will be captured in this service.
     *
     * @param uc            UserContext - Required
     * @param courtActivity CourtActivityType - Required
     * @return CourtActivityType
     * Throws exception if instance cannot be created.
     */
    public CourtActivityType createCaseActivity(UserContext uc, CourtActivityType courtActivity);

    /**
     * get -- Get/Retrieve a Court Activity by its Id.
     *
     * @param uc             UserContext - Required
     * @param caseActivityId java.lang.Long - Required
     * @return CourtActivityReturnType
     * Throws exception if instance cannot be gotten.
     */
    public CourtActivityType getCaseActivity(UserContext uc, Long caseActivityId);

    /**
     * update -- Update a Court Activity.
     * <pre>
     * A court activity must exist in order to be updated.
     * The associations to Case and Activity cannot be updated.
     * <pre>
     *
     * @param uc            UserContext - Required
     * @param courtActivity CourtActivityType - Required.
     * @return CourtActivityReturnType
     * Throws exception if instance cannot be updated.
     */
    public CourtActivityType updateCaseActivity(UserContext uc, CourtActivityType courtActivity);

    /**
     * Cascading delete of a single case activity and all its history.
     * Returns success code if the case activity can be deleted.
     * Returns error code if the case activity cannot be deleted.
     * <p>List of possible return codes: 1</p>
     *
     * @param uc             UserContext - Required
     * @param caseActivityId java.lang.Long - Required
     * @return Long
     * Throws exception if instance cannot be deleted.
     */
    public Long deleteCaseActivity(UserContext uc, Long caseActivityId);

    /**
     * Cancel a single court activity.
     * <pre>
     * Preconditions
     *  	<li>Court Activity must exist
     * Postconditions
     *  	<li>Court Activity is shown as inactive.
     * <p>List of possible return codes: 1, 2001, 2002, 1003</p>
     *
     * @param uc             UserContext - Required
     * @param caseActivityId java.lang.Long - Required
     * @return CourtActivityReturnType
     * Throws exception if instance cannot be cancelled.
     */
    public CourtActivityType cancelCourtActivity(UserContext uc, Long caseActivityId);

    /**
     * Search for a set of Case Activities based on a specified criteria. The ids can
     * be used to search only within the specified subset of Id's. List what
     * Search options are available in comments, EXACT is default of search
     * match mode.
     * Provenance
     * <pre>
     * At least one of the search criteria must be specified.
     * If any Case or Case Affiliation association is provided then only those events/activities to do with that association must be returned.
     * If History flag = false/null
     *  	Search is only performed against the most current
     *  Else (true)
     *  	Search is performed against all current and historic records
     *  Regardless of the History flag, the search will always return the most current records.
     * Text item/String will support the asterisk as a wildcard (that matches zero or more characters).
     * Text item/String without a wildcard will be matched as an exact string match.
     * Set Modes support:
     * 	<li>ONLY-- SET in search criterion will match sets that only have an exact set match.
     * 	<li>ANY-- SET in search criterion will match sets that have at least one of the items.
     *  <li>ALL-- SET in search criterion will match sets that have at least the specified subset.
     * chargeReferenceSetMode in CaseActivitySearchType
     * 	    String, Default = "ALL" (ONLY, ANY, ALL) -- Optional. It is used to specify set mode when the search criteria chargeAssociations within CaseActivitySearchType have values;
     * orderInititatedAssociationsSetMode in CaseActivitySearchType
     *      String, Default = "ALL" (ONLY, ANY, ALL) -- Optional. It is used to specify set mode when the search criteria orderInititatedAssociations within CaseActivitySearchType have values;
     * orderResultedAssociationsSetMode in CaseActivitySearchType
     *      String, Default = "ALL" (ONLY, ANY, ALL) -- Optional. It is used to specify set mode when the search criteria orderResultedAssociations within CaseActivitySearchType have values;
     *  </pre>
     *
     * @param uc          UserContext - Required
     * @param ids         java.util.Set<java.lang.Long> - Optional if null search will
     *                    occur across all activities.
     * @param searchType  CaseActivitySearchType - Required
     * @param historyFlag Boolean - Optional
     * @return List<CourtActivityType>
     * a list of court activity instances.
     * Returns empty list if no court activity can be found.
     * Throws exception if there is an error during search.
     */
    public List<CourtActivityType> searchCaseActivity(UserContext uc, Set<Long> ids, CaseActivitySearchType searchType, Boolean historyFlag);

    /**
     * Paging search for court activities.
     *
     * @param uc          {@link UserContext}
     * @param search      {@link CaseActivitySearchType} - search criteria instance.
     * @param startIndex  Long – Optional if null search returns at the start index
     * @param resultSize  Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder String – Optional if null search returns a default order
     * @return CourtActivitiesReturnType
     * a list of court activity instances.
     * Returns empty list if no court activity can be found.
     * Throws exception if there is an error during search.
     */
    public CourtActivitiesReturnType searchCourtActivity(UserContext uc, CaseActivitySearchType search, Long startIndex, Long resultSize, String resultOrder);

    /**
     * Retrieve all case activities for a particular case.
     * <pre>
     * Provenance
     * CaseAssociation must be specified. All events/activities related to the Case will be returned.
     * If ActivityStatus = “ACTIVE”
     *     Return all events/activities with a status of “ACTIVE”
     * If ActivityStatus = “INACTIVE”
     *     Return all events/activities with a status of “INACTIVE”
     * If ActivityStatus = null (not provided)
     *     Return all events/activities
     * </pre>
     *
     * @param uc             UserContext - Required
     * @param caseId         Long - Required
     * @param activityStatus String - Optional
     * @return List<CourtActivityType>
     * throw Exception if it has error.
     * Return empty list if no record found.
     */
    public List<CourtActivityType> retrieveCaseActivities(UserContext uc, Long caseId, String activityStatus);

    /**
     * Retrieve a history of a specific case activity based on a date range
     * <pre>
     * Provenance
     * CaseActivityIdentification is required.
     * The Case Activity must exist either currently or historically.
     * Associations to all entities connected to the historical case activity record must be returned as well.
     * </pre>
     *
     * @param uc              UserContext - Required
     * @param caseActivityId  Long - Required
     * @param fromHistoryDate Date - Optional
     * @param toHistoryDate   Date - Optional
     * @return List<CourtActivityType>
     * throw Exception if it has error.
     * Return empty list if no record found.
     */
    public List<CourtActivityType> retrieveCaseActivityHistory(UserContext uc, Long caseActivityId, Date fromHistoryDate, Date toHistoryDate);

    /**
     * Update Case Activity Security Status / Flag
     *
     * @param uc        - Required.
     * @param secStatus - DataFlag.- required
     * @param Long      - personId. Person Id of person id,- required
     * @param Long      - caseActivityId. Case Activity Id - required
     * @param comments  - Comments to the security flag.
     * @return Long 1/0 as True/False
     * @throws exception if the retrieving encounters error.
     */
    public Long updateCaseActivityDataSecurityStatus(UserContext uc, Long caseActivityId, Long secStatus, String comments, Long personId, String parentEntityType,
            Long parentId, String memento);

}
