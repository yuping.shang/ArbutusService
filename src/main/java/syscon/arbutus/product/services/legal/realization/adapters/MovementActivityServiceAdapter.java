package syscon.arbutus.product.services.legal.realization.adapters;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.movement.contract.dto.CourtMovementReturnTimeConfigurationType;
import syscon.arbutus.product.services.movement.contract.dto.ExternalMovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivitiesReturnType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivitySearchType;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType.MovementCategory;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType.MovementType;
import syscon.arbutus.product.services.movement.contract.interfaces.MovementService;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.utility.ServiceAdapter;

/**
 * An adapter class to wrap the API in MovementService and provide the legal service bean customized API.
 *
 * @author lhan
 */
public class MovementActivityServiceAdapter {

    private static MovementService service;

    /**
     * Constructor.
     */
    public MovementActivityServiceAdapter() {
        super();
    }

    synchronized private static MovementService getService() {

        if (service == null) {
            service = (MovementService) ServiceAdapter.JNDILookUp(service, MovementService.class);
        }
        return service;
    }

    /**
     * @param userContext
     * @param movment
     * @param requiresApproval
     * @return
     */
    public ExternalMovementActivityType createExtMovementActivity(UserContext userContext, ExternalMovementActivityType movment, boolean requiresApproval) {

        return (ExternalMovementActivityType) getService().create(userContext, movment, requiresApproval);
    }

    /**
     * @param uc
     * @param supervisionId
     * @param toFacilityId
     * @param fromFacilityId
     * @param direction
     * @param status
     * @param movementReason
     * @param comment
     * @param activityId
     * @return
     */
    public ExternalMovementActivityType generateMovement(UserContext uc, Long supervisionId, Long toFacilityId, Long fromFacilityId, String direction, String status,
            String movementReason, String comment, Long activityId) {
        ExternalMovementActivityType movement = new ExternalMovementActivityType();

        //supervision id
        movement.setSupervisionId(supervisionId);

        //from and to facility id
        movement.setFromFacilityId(fromFacilityId);
        movement.setToFacilityId(toFacilityId);

        //movement direction
        movement.setMovementDirection(direction);

        //movement reason
        movement.setMovementReason(movementReason);

        //movement status
        movement.setMovementStatus(status);

        //comment
        if (!BeanHelper.isEmpty(comment)) {
            CommentType commentType = new CommentType();
            commentType.setComment(comment);
            commentType.setCommentDate(new Date());

            Set<CommentType> comments = new HashSet<CommentType>();
            comments.add(commentType);
            movement.setCommentText(comments);
        }
        //activity Id
        movement.setActivityId(activityId);

        //must set these fields by default values.
        movement.setMovementCategory(MovementCategory.EXTERNAL.value());
        movement.setMovementType(MovementType.CRT.code());

        return movement;
    }

    /**
     * @param uc
     * @return
     */
    public String getCourtMovementReturnTime(UserContext uc) {
        CourtMovementReturnTimeConfigurationType rtn = getService().getCourtMovementReturnTimeConfiguration(uc);

        if (rtn != null) {
            return rtn.getReturnTime();
        }

        return null;
    }

    /**
     * @param uc
     * @param activityId
     * @return
     */
    public ExternalMovementActivityType getCourtMovementByActivityId(UserContext uc, Long activityId) {
        MovementActivitySearchType movementActivitySearch = new MovementActivitySearchType();
        Set<Long> activityIds = new HashSet<Long>();
        activityIds.add(activityId);
        movementActivitySearch.setActivityIds(activityIds);
        MovementActivitiesReturnType rtn = getService().search(uc, movementActivitySearch, null, null, null);

        List<ExternalMovementActivityType> externals = rtn.getExternalMovementActivity();
        if (externals != null && !externals.isEmpty()) {
            return rtn.getExternalMovementActivity().iterator().next();
        } else {
            return null;
        }
    }

    /**
     * @param uc
     * @param id
     */
    public void deleteMovementActivity(UserContext uc, Long id) {
        getService().delete(uc, id);
    }

}
