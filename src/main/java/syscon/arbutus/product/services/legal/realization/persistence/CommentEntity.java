package syscon.arbutus.product.services.legal.realization.persistence;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * The representation of the comment entity.
 */
@Embeddable
public class CommentEntity implements Serializable {

    private static final long serialVersionUID = 6055138919758657456L;

    @Column(name = "CommentUserId", nullable = true, length = 64)
    private String commentUserId;

    @Column(name = "CommentDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentDate;

    @Column(name = "CommentText", nullable = true, length = 512)
    private String commentText;

    /**
     * Default constructor
     */
    public CommentEntity() {
    }

    /**
     * Constructor
     *
     * @param commentUserId - required
     * @param commentDate   - required
     * @param commentText   - required
     */
    public CommentEntity(String commentUserId, Date commentDate, String commentText) {
        this.commentUserId = commentUserId;
        this.commentDate = commentDate;
        this.commentText = commentText;
    }

    /**
     * Gets the value of the commentUserId property
     *
     * @return String
     */
    public String getCommentUserId() {
        return commentUserId;
    }

    /**
     * Sets the value of the commentUserId property
     *
     * @param commentUserId String
     */
    public void setCommentUserId(String commentUserId) {
        this.commentUserId = commentUserId;
    }

    /**
     * Gets the value of the commentDate property
     *
     * @return Date
     */
    public Date getCommentDate() {
        return commentDate;
    }

    /**
     * Sets the value of the commentDate property
     *
     * @param commentDate Date
     */
    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    /**
     * Gets the value of the commentText property
     *
     * @return String
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * Sets the value of the commentText property
     *
     * @param commentText String
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CommentEntity [commentUserId=" + commentUserId + ", commentDate=" + commentDate + ", commentText=" + commentText + "]";
    }

}
