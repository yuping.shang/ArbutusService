package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;
import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * NotificationLogSearchType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 21, 2012
 */
@ArbutusConstraint(constraints = { "fromNotificationDate <= toNotificationDate" })
public class NotificationLogSearchType implements Serializable {

    private static final long serialVersionUID = -6033372618813594271L;

    private Date fromNotificationDate;
    private Date toNotificationDate;
    private String notificationType;
    private CommentType notificationComment;
    private Long notificationLocationAssociation;
    private String notificationContact;

    /**
     * Constructor
     */
    public NotificationLogSearchType() {
        super();
    }

    /**
     * Constructor
     * <p>At least one argument has to be provided.
     *
     * @param fromNotificationDate            Date. The date and time the log entry was made.
     * @param toNotificationDate              Date. The date and time the log entry was made.
     * @param notificationType                String. The type of notification log entry.
     * @param notificationComment             CommentType. The description of the log entry.
     * @param notificationLocationAssociation Long. The address/phone details of the notification. Static reference to location.
     * @param notificationContact             String(length <= 128). The details (name) of the person notified.
     */
    public NotificationLogSearchType(Date fromNotificationDate, Date toNotificationDate, String notificationType, CommentType notificationComment,
            Long notificationLocationAssociation, String notificationContact) {
        super();
        this.fromNotificationDate = fromNotificationDate;
        this.toNotificationDate = toNotificationDate;
        this.notificationType = notificationType;
        this.notificationComment = notificationComment;
        this.notificationLocationAssociation = notificationLocationAssociation;
        this.notificationContact = notificationContact;
    }

    /**
     * Get Notification Date -- The date and time the log entry was made.
     *
     * @return the fromNotificationDate
     */
    public Date getFromNotificationDate() {
        return fromNotificationDate;
    }

    /**
     * Set Notification Date -- The date and time the log entry was made.
     *
     * @param fromNotificationDate the fromNotificationDate to set
     */
    public void setFromNotificationDate(Date fromNotificationDate) {
        this.fromNotificationDate = fromNotificationDate;
    }

    /**
     * Get Notification Date -- The date and time the log entry was made.
     *
     * @return the toNotificationDate
     */
    public Date getToNotificationDate() {
        return toNotificationDate;
    }

    /**
     * Set Notification Date -- The date and time the log entry was made.
     *
     * @param toNotificationDate the toNotificationDate to set
     */
    public void setToNotificationDate(Date toNotificationDate) {
        this.toNotificationDate = toNotificationDate;
    }

    /**
     * Get Notification Type -- The type of notification log entry.
     *
     * @return the notificationType
     */
    public String getNotificationType() {
        return notificationType;
    }

    /**
     * Set Notification Type -- The type of notification log entry.
     *
     * @param notificationType the notificationType to set
     */
    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    /**
     * Get NotificationComment -- The description of the log entry.
     *
     * @return the notificationComment
     */
    public CommentType getNotificationComment() {
        return notificationComment;
    }

    /**
     * Set NotificationComment -- The description of the log entry.
     *
     * @param notificationComment the notificationComment to set
     */
    public void setNotificationComment(CommentType notificationComment) {
        this.notificationComment = notificationComment;
    }

    /**
     * Get Notification Location Association
     * -- The address/phone details of the notification. Static reference to location.
     *
     * @return the notificationLocationAssociation
     */
    public Long getNotificationLocationAssociation() {
        return notificationLocationAssociation;
    }

    /**
     * Set Notification Location Association
     * -- The address/phone details of the notification. Static reference to location.
     *
     * @param notificationLocationAssociation the notificationLocationAssociation to set
     */
    public void setNotificationLocationAssociation(Long notificationLocationAssociation) {
        this.notificationLocationAssociation = notificationLocationAssociation;
    }

    /**
     * Get Notification Contact
     * -- The details (name) of the person notified.
     *
     * @return the notificationContact
     */
    public String getNotificationContact() {
        return notificationContact;
    }

    /**
     * Set Notification Contact
     * -- The details (name) of the person notified.
     *
     * @param notificationContact the notificationContact to set
     */
    public void setNotificationContact(String notificationContact) {
        this.notificationContact = notificationContact;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "NotificationLogSearchType [fromNotificationDate=" + fromNotificationDate + ", toNotificationDate=" + toNotificationDate + ", notificationType="
                + notificationType + ", notificationComment=" + notificationComment + ", notificationLocationAssociation=" + notificationLocationAssociation
                + ", notificationContact=" + notificationContact + "]";
    }

}
