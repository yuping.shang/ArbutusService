package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * IntermDurationConfigEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdIntmCfg 'Intermittent Duration Configuration table for Order Sentence Module of Legal Service'
 * @since December 31, 2012
 */
@Audited
@Entity
@Table(name = "LEG_OrdIntmCfg")
public class IntermDurationConfigEntity implements Serializable {

    private static final long serialVersionUID = -733931376857692735L;

    /**
     * @DbComment configId 'Unique identification of Intermittent Duration Configuration instance.'
     */
    @Id
    @Column(name = "configId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDINTMCFG_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDINTMCFG_ID", sequenceName = "SEQ_LEG_ORDINTMCFG_ID", allocationSize = 1)
    private Long configId;

    /**
     * @DbComment intermittentDuration 'Specified if the duration of the intermittent sentence served is to be calculated as calendar days or hours. e.g. Sentence Fri 5 PM – Sun 5 PM. In Hours will equal 2 days (48 hours). In days will equal 3 days'
     */
    @Column(name = "intermittentDuration", nullable = false, length = 64)
    @MetaCode(set = MetaSet.DURATION)
    private String intermittentDurationConfig;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * Constructor
     */
    public IntermDurationConfigEntity() {
        super();
    }

    /**
     * Constructor
     *
     * @param configId
     * @param intermittentDurationConfig
     * @param stamp
     */
    public IntermDurationConfigEntity(Long configId, String intermittentDurationConfig, StampEntity stamp) {
        super();
        this.configId = configId;
        this.intermittentDurationConfig = intermittentDurationConfig;
        this.stamp = stamp;
    }

    /**
     * @return the configId
     */
    public Long getConfigId() {
        return configId;
    }

    /**
     * @param configId the configId to set
     */
    public void setConfigId(Long configId) {
        this.configId = configId;
    }

    /**
     * @return the intermittentDurationConfig
     */
    public String getIntermittentDurationConfig() {
        return intermittentDurationConfig;
    }

    /**
     * @param intermittentDurationConfig the intermittentDurationConfig to set
     */
    public void setIntermittentDurationConfig(String intermittentDurationConfig) {
        this.intermittentDurationConfig = intermittentDurationConfig;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((configId == null) ? 0 : configId.hashCode());
        result = prime * result + ((intermittentDurationConfig == null) ? 0 : intermittentDurationConfig.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        IntermDurationConfigEntity other = (IntermDurationConfigEntity) obj;
        if (configId == null) {
            if (other.configId != null) {
				return false;
			}
        } else if (!configId.equals(other.configId)) {
			return false;
		}
        if (intermittentDurationConfig == null) {
            if (other.intermittentDurationConfig != null) {
				return false;
			}
        } else if (!intermittentDurationConfig.equals(other.intermittentDurationConfig)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "IntermittentDurationConfigurationEntity [configId=" + configId + ", intermittentDurationConfig=" + intermittentDurationConfig + "]";
    }

}
