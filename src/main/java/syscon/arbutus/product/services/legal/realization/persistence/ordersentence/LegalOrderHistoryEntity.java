package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * LegalOrderHistoryEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdOrderHst 'Order History table for Order Sentence module of Legal service'
 * @since December 28, 2012
 */
//@Audited
@Entity
@DiscriminatorValue("LegalHst")
public class LegalOrderHistoryEntity extends OrderHistoryEntity implements Serializable {

    private static final long serialVersionUID = -737931375118522735L;

    /**
     * No specific properties/data elements defined from SDD 1.0.
     * Just extends OrderType
     */

	/* (non-Javadoc)
     * @see java.lang.Object#toString()
	 */
    @Override
    public String toString() {
        return "LegalOrderHistoryEntity [toString()=" + super.toString() + "]";
    }
}
