package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import java.math.BigDecimal;

import syscon.arbutus.product.services.realization.model.StampEntity;

public interface Amountable {
    /**
     * @return the bailAmountId
     */
    public Long getBailAmountId();

    /**
     * @param bailAmountId the bailAmountId to set
     */
    public void setBailAmountId(Long bailAmountId);

    /**
     * @return the bailType
     */
    public String getBailType();

    /**
     * @param bailType the bailType to set
     */
    public void setBailType(String bailType);

    /**
     * @return the bailAmount
     */
    public BigDecimal getBailAmount();

    /**
     * @param bailAmount the bailAmount to set
     */
    public void setBailAmount(BigDecimal bailAmount);

    /**
     * @return the stamp
     */
    public StampEntity getStamp();

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp);
}
