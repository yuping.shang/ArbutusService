package syscon.arbutus.product.services.legal.contract.dto;

import java.io.Serializable;

/**
 * The representation of the case type.
 * CaseType extends CaseInfoType and also inlcudes associated Charges, Orders, Conditions and so on.
 *
 * @author lhan
 */
public class CaseType extends CaseInfoType implements Serializable {

    private static final long serialVersionUID = 7871356476898004522L;

    //TODO: Includes in future sprint.
    //private List<OrderType> orders;
    //private List<ConditionType> conditions;
    //private Set<Long> linkedCaseIds;
    //private List<CaseEvent> events;

    /**
     * Constructor
     */
    public CaseType() {
        super();
    }

    /**
     * @param base CaseInfoType
     */
    public CaseType(CaseInfoType base) {
        setCaseId(base.getCaseId());
        setActiveFlag(base.isActiveFlag());
        setCaseCategory(base.getCaseCategory());
        setCaseIdentifiers(base.getCaseIdentifiers());
        setCaseType(base.getCaseType());
        setCreatedDate(base.getCreatedDate());
        setDataPrivileges(base.getDataPrivileges());
        setFacilityId(base.getFacilityId());
        setIssuedDate(base.getIssuedDate());
        setStatusChangeReason(base.getStatusChangeReason());
        setSentenceStatus(base.getSentenceStatus());
        setSupervisionId(base.getSupervisionId());
        setIssuingFacilityName(base.getIssuingFacilityName());
        setPrimaryCaseIdentifier(base.getPrimaryCaseIdentifier());
        setComment(base.getComment());
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseType [getCaseId()=" + getCaseId() + ", getPrimaryCaseIdentifier()=" + getPrimaryCaseIdentifier() + ", isSaveAsSecondaryIdentifier()="
                + isSaveAsSecondaryIdentifier() + ", getCaseIdentifiers()=" + getCaseIdentifiers() + ", getCaseCategory()=" + getCaseCategory() + ", getCaseType()="
                + getCaseType() + ", getCreatedDate()=" + getCreatedDate() + ", getIssuedDate()=" + getIssuedDate() + ", getFacilityId()=" + getFacilityId()
                + ", getSupervisionId()=" + getSupervisionId() + ", isActiveFlag()=" + isActiveFlag() + ", getStatusChangeReason()=" + getStatusChangeReason()
                + ", getSentenceStatus()=" + getSentenceStatus() + ", getComment()=" + getComment() + ", getIssuingFacilityName()=" + getIssuingFacilityName()
                + ", getDataPrivileges()=" + getDataPrivileges() + "]";
    }

}
