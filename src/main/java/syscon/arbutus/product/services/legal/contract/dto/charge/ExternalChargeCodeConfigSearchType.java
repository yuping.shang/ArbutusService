package syscon.arbutus.product.services.legal.contract.dto.charge;

import java.io.Serializable;

import syscon.arbutus.product.services.legal.contract.dto.ValidationUtil;

/**
 * The representation of the external charge code configuration search type
 */
public class ExternalChargeCodeConfigSearchType implements Serializable {

    private static final long serialVersionUID = -7653194523696313339L;

    private String codeGroup;
    private String chargeCode;
    private String codeCategory;
    private String description;

    /**
     * Default constructor
     */
    public ExternalChargeCodeConfigSearchType() {
    }

    /**
     * Constructor
     * At least one of the search criteria must be specified.
     *
     * @param codeGroup    String - External Groups such as NCIC, UCR(optional)
     * @param chargeCode   String - The external charge code linked to the group.(optional)
     * @param codeCategory String - External charge code category (optional)
     * @param description  String - External charge code description(optional)
     */
    public ExternalChargeCodeConfigSearchType(String codeGroup, String chargeCode, String codeCategory, String description) {
        this.codeGroup = codeGroup;
        this.chargeCode = chargeCode;
        this.codeCategory = codeCategory;
        this.description = description;
    }

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set, false otherwise.
     */
    @Deprecated
    public Boolean isWellFormed() {

        if (ValidationUtil.isEmpty(codeGroup) && ValidationUtil.isEmpty(chargeCode) && ValidationUtil.isEmpty(codeCategory) && ValidationUtil.isEmpty(description)) {
			return false;
		}

        return true;
    }

    /**
     * Verify if properties are set properly.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set and all values are valid, false otherwise.
     */
    @Deprecated
    public Boolean isValid() {

        if (!isWellFormed()) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the codeGroup property. It is a reference code value of ExternalChargeCodeGroup set.
     *
     * @return String - External Groups such as NCIC, UCR
     */
    public String getCodeGroup() {
        return codeGroup;
    }

    /**
     * Sets the value of the codeGroup property. It is a reference code value of ExternalChargeCodeGroup set.
     *
     * @param codeGroup String - External Groups such as NCIC, UCR
     */
    public void setCodeGroup(String codeGroup) {
        this.codeGroup = codeGroup;
    }

    /**
     * Gets the value of the chargeCode property. It is a reference code value of ExternalChargeCode set.
     *
     * @return String - The external charge code linked to the group.
     */
    public String getChargeCode() {
        return chargeCode;
    }

    /**
     * Sets the value of the code property. It is a reference code value of ExternalChargeCode set.
     *
     * @param chargeCode String - The external charge code linked to the group.
     */
    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    /**
     * Gets the value of the category property. It is a reference code value of ExternalChargeCodeCategory set.
     *
     * @return String - External charge code category
     */
    public String getCodeCategory() {
        return codeCategory;
    }

    /**
     * Sets the value of the codeCategory property. It is a reference code value of ExternalChargeCodeCategory set.
     *
     * @param codeCategory String - External charge code category
     */
    public void setCodeCategory(String codeCategory) {
        this.codeCategory = codeCategory;
    }

    /**
     * Gets the value of the description property.
     *
     * @return String - External charge code description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param description String - External charge code description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ExternalChargeCodeConfigSearchType [codeGroup=" + codeGroup + ", chargeCode=" + chargeCode + ", codeCategory=" + codeCategory + ", description="
                + description + "]";
    }

}
