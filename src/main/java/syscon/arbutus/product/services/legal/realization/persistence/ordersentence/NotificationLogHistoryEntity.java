package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * NotificationLogHistoryEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdNotiLgHst 'Notification Log table for Order Sentence module of Legal service'
 * @DbComment LEG_OrdNotiLgHst.NotiLog 'Foreign key to LEG_OrdOrderHst'
 * @since December 21, 2012
 */
//@Audited
@Entity
@Table(name = "LEG_OrdNotiLgHst")
public class NotificationLogHistoryEntity implements Serializable {

    private static final long serialVersionUID = -737931375114512738L;

    /**
     * @DbComment hstId 'Unique identification of a notification log history instance.'
     */
    @Id
    @Column(name = "hstId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDNOTILGHST_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDNOTILGHST_ID", sequenceName = "SEQ_LEG_ORDNOTILGHST_ID", allocationSize = 1)
    private Long historyId;

    /**
     * @DbComment notiLgId 'Unique identification of a notification log instance.'
     */
    @Column(name = "notiLgId", nullable = false)
    private Long notificationLogId;

    /**
     * @DbComment notificationDate 'The date and time the log entry was made.'
     */
    @Column(name = "notificationDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date notificationDate;

    /**
     * @DbComment notificationType 'The type of notification log entry'
     */
    @Column(name = "notificationType", nullable = false, length = 64)
    private String notificationType;

    /**
     * @DbComment CommentUserId 'User ID who created/updated the comments'
     * @DbComment CommentDate 'The Date/Time when the comment was created'
     * @DbComment CommentText 'The comment text'
     */
    @Embedded
    private CommentEntity notificationComment;

    /**
     * @DbComment notificationLocation 'The address/phone details of the notification. Static reference to location.'
     */
    @Column(name = "notificationLocation", nullable = true)
    private Long notificationLocationAssociation;

    /**
     * @DbComment notificationContact 'The details (name) of the person notified.'
     */
    @Column(name = "notificationContact", nullable = true, length = 128)
    private String notificationContact;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * Constructor
     */
    public NotificationLogHistoryEntity() {
        super();
    }

    public NotificationLogHistoryEntity(Long historyId, Long notificationLogId, Date notificationDate, String notificationType, CommentEntity notificationComment,
            Long notificationLocationAssociation, String notificationContact, StampEntity stamp) {
        super();
        this.historyId = historyId;
        this.notificationLogId = notificationLogId;
        this.notificationDate = notificationDate;
        this.notificationType = notificationType;
        this.notificationComment = notificationComment;
        this.notificationLocationAssociation = notificationLocationAssociation;
        this.notificationContact = notificationContact;
        this.stamp = stamp;
    }

    /**
     * @return the historyId
     */
    public Long getHistoryId() {
        return historyId;
    }

    /**
     * @param historyId the historyId to set
     */
    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    /**
     * @return the notificationLogId
     */
    public Long getNotificationLogId() {
        return notificationLogId;
    }

    /**
     * @param notificationLogId the notificationLogId to set
     */
    public void setNotificationLogId(Long notificationLogId) {
        this.notificationLogId = notificationLogId;
    }

    /**
     * @return the notificationDate
     */
    public Date getNotificationDate() {
        return notificationDate;
    }

    /**
     * @param notificationDate the notificationDate to set
     */
    public void setNotificationDate(Date notificationDate) {
        this.notificationDate = notificationDate;
    }

    /**
     * @return the notificationType
     */
    public String getNotificationType() {
        return notificationType;
    }

    /**
     * @param notificationType the notificationType to set
     */
    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    /**
     * @return the notificationComment
     */
    public CommentEntity getNotificationComment() {
        return notificationComment;
    }

    /**
     * @param notificationComment the notificationComment to set
     */
    public void setNotificationComment(CommentEntity notificationComment) {
        this.notificationComment = notificationComment;
    }

    /**
     * @return the notificationLocationAssociation
     */
    public Long getNotificationLocationAssociation() {
        return notificationLocationAssociation;
    }

    /**
     * @param notificationLocationAssociation the notificationLocationAssociation to set
     */
    public void setNotificationLocationAssociation(Long notificationLocationAssociation) {
        this.notificationLocationAssociation = notificationLocationAssociation;
    }

    /**
     * @return the notificationContact
     */
    public String getNotificationContact() {
        return notificationContact;
    }

    /**
     * @param notificationContact the notificationContact to set
     */
    public void setNotificationContact(String notificationContact) {
        this.notificationContact = notificationContact;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((historyId == null) ? 0 : historyId.hashCode());
        result = prime * result + ((notificationContact == null) ? 0 : notificationContact.hashCode());
        result = prime * result + ((notificationDate == null) ? 0 : notificationDate.hashCode());
        result = prime * result + ((notificationLocationAssociation == null) ? 0 : notificationLocationAssociation.hashCode());
        result = prime * result + ((notificationLogId == null) ? 0 : notificationLogId.hashCode());
        result = prime * result + ((notificationType == null) ? 0 : notificationType.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        NotificationLogHistoryEntity other = (NotificationLogHistoryEntity) obj;
        if (historyId == null) {
            if (other.historyId != null) {
				return false;
			}
        } else if (!historyId.equals(other.historyId)) {
			return false;
		}
        if (notificationContact == null) {
            if (other.notificationContact != null) {
				return false;
			}
        } else if (!notificationContact.equals(other.notificationContact)) {
			return false;
		}
        if (notificationDate == null) {
            if (other.notificationDate != null) {
				return false;
			}
        } else if (!notificationDate.equals(other.notificationDate)) {
			return false;
		}
        if (notificationLocationAssociation == null) {
            if (other.notificationLocationAssociation != null) {
				return false;
			}
        } else if (!notificationLocationAssociation.equals(other.notificationLocationAssociation)) {
			return false;
		}
        if (notificationLogId == null) {
            if (other.notificationLogId != null) {
				return false;
			}
        } else if (!notificationLogId.equals(other.notificationLogId)) {
			return false;
		}
        if (notificationType == null) {
            if (other.notificationType != null) {
				return false;
			}
        } else if (!notificationType.equals(other.notificationType)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "NotificationLogHistoryEntity [historyId=" + historyId + ", notificationLogId=" + notificationLogId + ", notificationDate=" + notificationDate
                + ", notificationType=" + notificationType + ", notificationComment=" + notificationComment + ", notificationLocationAssociation="
                + notificationLocationAssociation + ", notificationContact=" + notificationContact + "]";
    }

}
