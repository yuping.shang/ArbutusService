package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * IntermittentDurationConfigurationType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 31, 2012
 */
public class IntermittentDurationConfigurationType implements Serializable {

    private static final long serialVersionUID = -6967420921039330496L;
    @NotNull
    private String intermittentDurationConfig;

    /**
     * Constructor
     */
    public IntermittentDurationConfigurationType() {
        super();
    }

    /**
     * Constructor
     *
     * @param intermittentDurationConfig String, Required. Specified if the duration of the intermittent sentence served is to be calculated as calendar days or hours. e.g. Sentence Fri 5 PM – Sun 5 PM. In Hours will equal 2 days (48 hours). In days will equal 3 days
     */
    public IntermittentDurationConfigurationType(@NotNull String intermittentDurationConfig) {
        super();
        this.intermittentDurationConfig = intermittentDurationConfig;
    }

    /**
     * @return the intermittentDurationConfig
     */
    public String getIntermittentDurationConfig() {
        return intermittentDurationConfig;
    }

    /**
     * @param intermittentDurationConfig the intermittentDurationConfig to set
     */
    public void setIntermittentDurationConfig(String intermittentDurationConfig) {
        this.intermittentDurationConfig = intermittentDurationConfig;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "IntermittentDurationConfigurationType [intermittentDurationConfig=" + intermittentDurationConfig + "]";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((intermittentDurationConfig == null) ? 0 : intermittentDurationConfig.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        IntermittentDurationConfigurationType other = (IntermittentDurationConfigurationType) obj;
        if (intermittentDurationConfig == null) {
            if (other.intermittentDurationConfig != null) {
				return false;
			}
        } else if (!intermittentDurationConfig.equals(other.intermittentDurationConfig)) {
			return false;
		}
        return true;
    }

}
