package syscon.arbutus.product.services.legal.contract.dto.caseinformation;

import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.legal.contract.dto.ValidationUtil;

/**
 * The representation of the case information search type
 */
public class CaseInfoSearchType implements Serializable {

    private static final long serialVersionUID = -4543230863874628272L;

    private Long supervisionId;
    private Long facilityId;
    private CaseIdentifierSearchType caseIdentifierSearch;
    private String caseCategory;
    private String caseType;
    private Date createdDateFrom;
    private Date createdDateTo;
    private Date issuedDateFrom;
    private Date issuedDateTo;
    private Boolean diverted;
    private Boolean divertedSuccess;
    private Boolean active;
    private String reason;
    private String sentenceStatus;

    /**
     * Default constructor
     */
    public CaseInfoSearchType() {
    }

    /**
     * Constructor
     * At least one of the search criteria must be specified.
     *
     * @param supervisionId        Long - Reference to a Supervision period
     * @param facilityId           Long - Indicates the originating facility name, type and location for a case. Static reference to the Facility service.
     * @param caseIdentifierSearch CaseIdentifierSearchType - the of numbers/identifiers recorded for a case.
     * @param caseCategory         String - The category of the case, e.g. InJurisdiction case, Out of Jurisdiction case.
     * @param caseType             String - Indicates the type of criminal case
     * @param createdDateFrom      Date - The date when the case was first created
     * @param createdDateTo        Date - The date when the case was first created
     * @param issuedDateFrom       Date - The date when the case was issued
     * @param issuedDateTo         Date - The date when the case was issued
     * @param diverted             Boolean - Indicates whether the case was attempted to be diverted
     * @param divertedSuccess      Boolean - Indicates whether a case diversion (if attempted) was successful or unsuccessful.
     * @param active               Boolean - Indicates whether the case is active or inactive
     * @param reason               String - Indicates the reason a case status was changed
     * @param sentenceStatus       String - Indicates whether the offender was sentenced to be incarcerated.
     */
    public CaseInfoSearchType(Long supervisionId, Long facilityId, CaseIdentifierSearchType caseIdentifierSearch, String caseCategory, String caseType,
            Date createdDateFrom, Date createdDateTo, Date issuedDateFrom, Date issuedDateTo, Boolean diverted, Boolean divertedSuccess, Boolean active, String reason,
            String sentenceStatus) {
        this.supervisionId = supervisionId;
        this.facilityId = facilityId;
        this.caseIdentifierSearch = caseIdentifierSearch;
        this.caseCategory = caseCategory;
        this.caseType = caseType;
        this.createdDateFrom = createdDateFrom;
        this.createdDateTo = createdDateTo;
        this.issuedDateFrom = issuedDateFrom;
        this.issuedDateTo = issuedDateTo;
        this.diverted = diverted;
        this.divertedSuccess = divertedSuccess;
        this.active = active;
        this.reason = reason;
        this.sentenceStatus = sentenceStatus;
    }

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set, false otherwise.
     */
    @Deprecated
    public Boolean isWellFormed() {

        if (ValidationUtil.isEmpty(supervisionId) && ValidationUtil.isEmpty(facilityId) && ValidationUtil.isEmpty(caseCategory) && ValidationUtil.isEmpty(caseType)
                && ValidationUtil.isEmpty(createdDateFrom) && ValidationUtil.isEmpty(createdDateTo) && ValidationUtil.isEmpty(issuedDateFrom) && ValidationUtil.isEmpty(
                issuedDateTo) && ValidationUtil.isEmpty(diverted) && ValidationUtil.isEmpty(divertedSuccess) && ValidationUtil.isEmpty(active) && ValidationUtil.isEmpty(
                reason) && ValidationUtil.isEmpty(sentenceStatus) && (caseIdentifierSearch == null || !caseIdentifierSearch.isWellFormed())) {
			return false;
		}

        return true;
    }

    /**
     * Verify if properties are set properly.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set and all values are valid, false otherwise.
     */
    @Deprecated
    public Boolean isValid() {

        if (!isWellFormed()) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the supervision property.
     *
     * @return Long - Reference to a Supervision period
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * Sets the value of the supervision property.
     *
     * @param supervisionId Long - Reference to a Supervision period
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Gets the value of the facility property.
     *
     * @return Long - Indicates the originating facility name, type and location for a case. Static reference to the Facility service.
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * Sets the value of the facility property.
     *
     * @param facilityId Long - Indicates the originating facility name, type and location for a case. Static reference to the Facility service.
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * Gets the value of the caseIdentifierSearch property.
     *
     * @return CaseIdentifierSearchType - the of numbers/identifiers recorded for a case.
     */
    public CaseIdentifierSearchType getCaseIdentifierSearch() {
        return caseIdentifierSearch;
    }

    /**
     * Sets the value of the caseIdentifierSearch property.
     *
     * @param caseIdentifierSearch the caseIdentifierSearch to set
     */
    public void setCaseIdentifierSearch(CaseIdentifierSearchType caseIdentifierSearch) {
        this.caseIdentifierSearch = caseIdentifierSearch;
    }

    /**
     * Gets the value of the caseCategory property.
     *
     * @return String - The category of the case, e.g. InJurisdiction case, Out of Jurisdiction case.
     */
    public String getCaseCategory() {
        return caseCategory;
    }

    /**
     * Sets the value of the caseCategory property.
     *
     * @param caseCategory String - The category of the case, e.g. InJurisdiction case, Out of Jurisdiction case.
     */
    public void setCaseCategory(String caseCategory) {
        this.caseCategory = caseCategory;
    }

    /**
     * Gets the value of the caseType property. It is a code value of CaseType set.
     *
     * @return String - Indicates the type of criminal case
     */
    public String getCaseType() {
        return caseType;
    }

    /**
     * Sets the value of the caseType property. It is a code value of CaseType set.
     *
     * @param caseType String - Indicates the type of criminal case
     */
    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    /**
     * Gets the value of the createdDateFrom property.
     *
     * @return Date - The date when the case was first created
     */
    public Date getCreatedDateFrom() {
        return createdDateFrom;
    }

    /**
     * Sets the value of the createdDateFrom property.
     *
     * @param createdDateFrom Date - The date when the case was first created
     */
    public void setCreatedDateFrom(Date createdDateFrom) {
        this.createdDateFrom = createdDateFrom;
    }

    /**
     * Gets the value of the createdDateTo property.
     *
     * @return Date - The date when the case was first created
     */
    public Date getCreatedDateTo() {
        return createdDateTo;
    }

    /**
     * Sets the value of the createdDateTo property.
     *
     * @param createdDateTo Date - The date when the case was first created
     */
    public void setCreatedDateTo(Date createdDateTo) {
        this.createdDateTo = createdDateTo;
    }

    /**
     * Gets the value of the issuedDateFrom property.
     *
     * @return Date - The date when the case was issued
     */
    public Date getIssuedDateFrom() {
        return issuedDateFrom;
    }

    /**
     * Sets the value of the issuedDateFrom property.
     *
     * @param issuedDateFrom Date - The date when the case was issued
     */
    public void setIssuedDateFrom(Date issuedDateFrom) {
        this.issuedDateFrom = issuedDateFrom;
    }

    /**
     * Gets the value of the issuedDateTo property.
     *
     * @return Date - The date when the case was issued
     */
    public Date getIssuedDateTo() {
        return issuedDateTo;
    }

    /**
     * Sets the value of the issuedDateTo property.
     *
     * @param issuedDateTo the issuedDateTo to set
     */
    public void setIssuedDateTo(Date issuedDateTo) {
        this.issuedDateTo = issuedDateTo;
    }

    /**
     * Gets the value of the diverted property.
     *
     * @return Boolean - Indicates whether the case was attempted to be diverted
     */
    public Boolean getDiverted() {
        return diverted;
    }

    /**
     * Sets the value of the diverted property.
     *
     * @param diverted Boolean - Indicates whether the case was attempted to be diverted
     */
    public void setDiverted(Boolean diverted) {
        this.diverted = diverted;
    }

    /**
     * Gets the value of the divertedSuccess property.
     *
     * @return Boolean - Indicates whether a case diversion (if attempted) was successful or unsuccessful.
     */
    public Boolean getDivertedSuccess() {
        return divertedSuccess;
    }

    /**
     * Sets the value of the divertedSuccess property.
     *
     * @param divertedSuccess Boolean - Indicates whether a case diversion (if attempted) was successful or unsuccessful.
     */
    public void setDivertedSuccess(Boolean divertedSuccess) {
        this.divertedSuccess = divertedSuccess;
    }

    /**
     * Gets the value of the active property.
     *
     * @return Boolean - Indicates whether the case is active or inactive
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     *
     * @param active Boolean - Indicates whether the case is active or inactive
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * Gets the value of the reason property. It is a code value of CaseStatusChangeReason set.
     *
     * @return String - Indicates the reason a case status was changed
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property. It is a code value of CaseStatusChangeReason set.
     *
     * @param reason String - Indicates the reason a case status was changed
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * Gets the value of the sentenceStatus property. It is a code value of SentenceStatus set.
     *
     * @return String - Indicates whether the offender was sentenced to be incarcerated.
     */
    public String getSentenceStatus() {
        return sentenceStatus;
    }

    /**
     * Sets the value of the sentenceStatus property. It is a code value of SentenceStatus set.
     *
     * @param sentenceStatus String - Indicates whether the offender was sentenced to be incarcerated.
     */
    public void setSentenceStatus(String sentenceStatus) {
        this.sentenceStatus = sentenceStatus;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseInfoSearchType [supervisionId=" + supervisionId + ", facilityId=" + facilityId + ", caseIdentifierSearch=" + caseIdentifierSearch + ", caseCategory="
                + caseCategory + ", caseType=" + caseType + ", createdDateFrom=" + createdDateFrom + ", createdDateTo=" + createdDateTo + ", issuedDateFrom="
                + issuedDateFrom + ", issuedDateTo=" + issuedDateTo + ", diverted=" + diverted + ", divertedSuccess=" + divertedSuccess + ", active=" + active
                + ", reason=" + reason + ", sentenceStatus=" + sentenceStatus + "]";
    }

}
