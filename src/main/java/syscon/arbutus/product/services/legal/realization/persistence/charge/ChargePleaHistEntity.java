package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the charge plea history entity.
 *
 * @DbComment LEG_CHGPleaHist 'The charge plea history data table'
 */
//@Audited
@Entity
@Table(name = "LEG_CHGPleaHist")
public class ChargePleaHistEntity implements Serializable {

    private static final long serialVersionUID = -3746644467957986562L;

    /**
     * @DbComment ChargePleaHistoryId 'Unique id of a charge plea history record of a charge instance'
     */
    @Id
    @Column(name = "ChargePleaHistoryId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGPleaHist_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGPleaHist_Id", sequenceName = "SEQ_LEG_CHGPleaHist_Id", allocationSize = 1)
    private Long chargePleaHistoryId;

    /**
     * @DbComment ChargePleaId 'Unique id of a charge plea record of a charge instance'
     */
    @Column(name = "ChargePleaId", nullable = false)
    private Long chargePleaId;

    /**
     * @DbComment PleaCode 'A kind of plea.'
     */
    @Column(name = "PleaCode", nullable = false, length = 64)
    private String pleaCode;

    /**
     * @DbComment PleaComment 'Comments regarding the plea.'
     */
    @Column(name = "PleaComment", nullable = true, length = 512)
    private String pleaComment;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     *
     */
    public ChargePleaHistEntity() {
    }

    /**
     * @param chargePleaHistoryId
     * @param chargePleaId
     * @param pleaCode
     * @param pleaComment
     */
    public ChargePleaHistEntity(Long chargePleaHistoryId, Long chargePleaId, String pleaCode, String pleaComment) {
        this.chargePleaHistoryId = chargePleaHistoryId;
        this.chargePleaId = chargePleaId;
        this.pleaCode = pleaCode;
        this.pleaComment = pleaComment;
    }

    /**
     * @return the chargePleaHistoryId
     */
    public Long getChargePleaHistoryId() {
        return chargePleaHistoryId;
    }

    /**
     * @param chargePleaHistoryId the chargePleaHistoryId to set
     */
    public void setChargePleaHistoryId(Long chargePleaHistoryId) {
        this.chargePleaHistoryId = chargePleaHistoryId;
    }

    /**
     * @return the chargePleaId
     */
    public Long getChargePleaId() {
        return chargePleaId;
    }

    /**
     * @param chargePleaId the chargePleaId to set
     */
    public void setChargePleaId(Long chargePleaId) {
        this.chargePleaId = chargePleaId;
    }

    /**
     * @return the pleaCode
     */
    public String getPleaCode() {
        return pleaCode;
    }

    /**
     * @param pleaCode the pleaCode to set
     */
    public void setPleaCode(String pleaCode) {
        this.pleaCode = pleaCode;
    }

    /**
     * @return the pleaComment
     */
    public String getPleaComment() {
        return pleaComment;
    }

    /**
     * @param pleaComment the pleaComment to set
     */
    public void setPleaComment(String pleaComment) {
        this.pleaComment = pleaComment;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargePleaHistEntity [chargePleaHistoryId=" + chargePleaHistoryId + ", chargePleaId=" + chargePleaId + ", pleaCode=" + pleaCode + ", pleaComment="
                + pleaComment + ", stamp=" + stamp + "]";
    }

}
