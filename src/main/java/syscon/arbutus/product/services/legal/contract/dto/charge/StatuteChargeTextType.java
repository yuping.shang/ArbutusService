package syscon.arbutus.product.services.legal.contract.dto.charge;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * The representation of the statute charge text type
 */
public class StatuteChargeTextType implements Serializable {

    private static final long serialVersionUID = 2665568309580348626L;

    @NotNull
    private String language;
    @NotNull
    @Size(max = 4000, message = "max length 4000")
    private String description;
    @Size(max = 4000, message = "max length 4000")
    private String legalText;

    /**
     * this variable to be used at Web App Side
     */
    private int rowId;

    /**
     * Default constructor
     */
    public StatuteChargeTextType() {
    }

    /**
     * Constructor
     *
     * @param language    String - The language code associated to the charge text
     * @param description String - Text related to the charge code
     * @param legalText   String - Legal text for a charge (optional)
     */
    public StatuteChargeTextType(@NotNull String language, @NotNull @Size(max = 4000, message = "max length 4000") String description,
            @Size(max = 4000, message = "max length 4000") String legalText) {
        this.language = language;
        this.description = description;
        this.legalText = legalText;
    }

    /**
     * Gets the value of the language property. It is a reference code of Language set.
     *
     * @return String - The language code associated to the charge text
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property. It is a reference code of Language set.
     *
     * @param language String - The language code associated to the charge text
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * Gets the value of the description property.
     *
     * @return String - Text related to the charge code
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param description String - Text related to the charge code
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the value of the legalText property.
     *
     * @return String - Legal text for a charge
     */
    public String getLegalText() {
        return legalText;
    }

    /**
     * Sets the value of the legalText property.
     *
     * @param legalText String - Legal text for a charge
     */
    public void setLegalText(String legalText) {
        this.legalText = legalText;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((language == null) ? 0 : language.hashCode());
        result = prime * result + ((legalText == null) ? 0 : legalText.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        StatuteChargeTextType other = (StatuteChargeTextType) obj;
        if (description == null) {
            if (other.description != null) {
				return false;
			}
        } else if (!description.equals(other.description)) {
			return false;
		}
        if (language == null) {
            if (other.language != null) {
				return false;
			}
        } else if (!language.equals(other.language)) {
			return false;
		}
        if (legalText == null) {
            if (other.legalText != null) {
				return false;
			}
        } else if (!legalText.equals(other.legalText)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StatuteChargeTextType [language=" + language + ", description=" + description + ", legalText=" + legalText + "]";
    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

}
