package syscon.arbutus.product.services.legal.realization.persistence.condition;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Condition Comment History Entity of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_CNCommentHst 'The Condition Comment History table for CaseManagement service'
 * @since January 11, 2013
 */
//@Audited
@Entity
@Table(name = "LEG_CNCommentHst")
public class ConditionCommentHistoryEntity implements Serializable {

    private static final long serialVersionUID = 6055138919758657457L;

    /**
     * @DbComment hstId 'Unique system generated identifier for the Condition'
     */
    @Id
    @Column(name = "hstId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CNCOMMENTHST_Id")
    @SequenceGenerator(name = "SEQ_LEG_CNCOMMENTHST_Id", sequenceName = "SEQ_LEG_CNCOMMENTHST_Id", allocationSize = 1)
    private Long historyId;

    /**
     * @DbComment commentId 'Unique identification of a Condition Comment instance'
     */
    @Column(name = "commentId", nullable = false)
    private Long commentId;

    /**
     * @DbComment CommentUserId 'Comment User Id'
     */
    @Column(name = "CommentUserId", nullable = true, length = 64)
    private String commentUserId;

    /**
     * @DbComment CommentDate 'The Date/Time when the comment was created'
     */
    @Column(name = "CommentDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentDate;

    /**
     * @DbComment CommentText 'The comment text'
     */
    @Column(name = "CommentText", nullable = true, length = 4096) // 4k length
    private String commentText;

    /**
     * @DbComment conditionId 'Foreign key to LEG_Condition table'
     */
    @ForeignKey(name = "Fk_LEG_CNCommentHst")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "conditionId", nullable = false)
    private ConditionHistoryEntity condition;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * Constructor
     */
    public ConditionCommentHistoryEntity() {
        super();
    }

    /**
     * Constructor
     *
     * @param historyId
     * @param commentId
     * @param commentUserId
     * @param commentDate
     * @param commentText
     * @param stamp
     */
    public ConditionCommentHistoryEntity(Long historyId, Long commentId, String commentUserId, Date commentDate, String commentText, StampEntity stamp) {
        super();
        this.historyId = historyId;
        this.commentId = commentId;
        this.commentUserId = commentUserId;
        this.commentDate = commentDate;
        this.commentText = commentText;
        this.stamp = stamp;
    }

    /**
     * @return the historyId
     */
    public Long getHistoryId() {
        return historyId;
    }

    /**
     * @param historyId the historyId to set
     */
    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    /**
     * @return the commentId
     */
    public Long getCommentId() {
        return commentId;
    }

    /**
     * @param commentId the commentId to set
     */
    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    /**
     * @return the commentUserId
     */
    public String getCommentUserId() {
        return commentUserId;
    }

    /**
     * @param commentUserId the commentUserId to set
     */
    public void setCommentUserId(String commentUserId) {
        this.commentUserId = commentUserId;
    }

    /**
     * @return the commentDate
     */
    public Date getCommentDate() {
        return commentDate;
    }

    /**
     * @param commentDate the commentDate to set
     */
    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    /**
     * @return the commentText
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * @param commentText the commentText to set
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    /**
     * @return the condition
     */
    public ConditionHistoryEntity getCondition() {
        return condition;
    }

    /**
     * @param condition the condition to set
     */
    public void setCondition(ConditionHistoryEntity condition) {
        this.condition = condition;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((commentDate == null) ? 0 : commentDate.hashCode());
        result = prime * result + ((commentId == null) ? 0 : commentId.hashCode());
        result = prime * result + ((commentText == null) ? 0 : commentText.hashCode());
        result = prime * result + ((commentUserId == null) ? 0 : commentUserId.hashCode());
        result = prime * result + ((historyId == null) ? 0 : historyId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ConditionCommentHistoryEntity other = (ConditionCommentHistoryEntity) obj;
        if (commentDate == null) {
            if (other.commentDate != null) {
				return false;
			}
        } else if (!commentDate.equals(other.commentDate)) {
			return false;
		}
        if (commentId == null) {
            if (other.commentId != null) {
				return false;
			}
        } else if (!commentId.equals(other.commentId)) {
			return false;
		}
        if (commentText == null) {
            if (other.commentText != null) {
				return false;
			}
        } else if (!commentText.equals(other.commentText)) {
			return false;
		}
        if (commentUserId == null) {
            if (other.commentUserId != null) {
				return false;
			}
        } else if (!commentUserId.equals(other.commentUserId)) {
			return false;
		}
        if (historyId == null) {
            if (other.historyId != null) {
				return false;
			}
        } else if (!historyId.equals(other.historyId)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ConditionCommentHistoryEntity [historyId=" + historyId + ", commentId=" + commentId + ", commentUserId=" + commentUserId + ", commentDate=" + commentDate
                + ", commentText=" + commentText + ", stamp=" + stamp + "]";
    }

}
