package syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment;

import java.io.Serializable;
import java.util.List;

import syscon.arbutus.product.services.legal.contract.dto.ordersentence.FinePaymentType;

public class FinePaymentReturnType implements Serializable {

    private static final long serialVersionUID = -2340729089025289758L;

    private List<FinePaymentType> finePayments;
    private Integer totalSize;

    /**
     * Get list of Fine Payment
     *
     * @return the finePayments - List<FinePaymentType>
     */
    public List<FinePaymentType> getFinePayments() {
        return finePayments;
    }

    /**
     * Set list of Fine Payment
     *
     * @param List<FinePaymentType> the finePayments to set
     */
    public void setFinePayments(List<FinePaymentType> finePayments) {
        this.finePayments = finePayments;
    }

    /**
     * Get total number of record in database for the criteria.
     *
     * @return the totalSize - Long
     */
    public Integer getTotalSize() {
        return totalSize;
    }

    /**
     * Set total number of record in database for the criteria.
     *
     * @param Long the totalSize to set
     */
    public void setTotalSize(Integer totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "FinePaymentReturnType [finePayments=" + finePayments + ", totalSize=" + totalSize + "]";
    }

}
