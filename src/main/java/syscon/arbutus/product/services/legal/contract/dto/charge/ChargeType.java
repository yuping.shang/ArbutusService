package syscon.arbutus.product.services.legal.contract.dto.charge;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.ServiceConstants;

/**
 * The representation of the charge type
 */
@ArbutusConstraint(constraints = "offenceStartDate <= offenceEndDate")
public class ChargeType implements Serializable {

    private static final long serialVersionUID = 3570160714181984409L;

    private Long chargeId;
    private Long statuteChargeId;
    private Long chargeGroupId;
    @Valid
    private Set<ChargeIdentifierType> chargeIdentifiers;
    @Valid
    private Set<ChargeIndicatorType> chargeIndicators;
    private String legalText;
    @NotNull
    @Valid
    private ChargeDispositionType chargeDisposition;
    @Valid
    private ChargePleaType chargePlea;
    @Valid
    private ChargeAppealType chargeAppeal;
    private Long chargeCount;
    private Long facilityId;
    private Long organizationId;
    private Long personIdentityId;
    private Date offenceStartDate;
    private Date offenceEndDate;
    private Date filingDate;
    @NotNull
    private Boolean primary;

    @Valid
    private CommentType comment;

    // Id of Sentence assigned to this Charge
    private Long sentenceId;

    //Relationship with modules
    private Long caseInfoId;
    private Set<Long> conditionIds;
    private Set<Long> orderSentenceIds;

    private String chargeJurisdiction;   //Code from ChargeJurisdiction set

    //OJ charge info
    private Long OJOrderId;
    @Size(max = ServiceConstants.TEXT_LENGTH_MEDIUM)
    private String OJStatuteIdName;
    @Size(max = ServiceConstants.TEXT_LENGTH_SMALL)
    private String OJChargeCode;
    @Size(max = ServiceConstants.TEXT_LENGTH_LARGE_MEMO)
    private String OJChargeDescription;

    // Web display fields only, won't be saved to DB.
    private String statuteCode;
    private String statuteName;
    private String chargeCode;
    private String chargeDescription;
    private String chargeType;
    private String chargeSeverity;
    private String chargeDegree;

    private List<ChargeDispositionType> dispositionHistories;

    /**
     * Default constructor
     */
    public ChargeType() {
    }

    /**
     * Constructor
     *
     * @param chargeId          Long - the unique Id for each charge record. (optional for creation)
     * @param statuteChargeId   Long - the unique Id for Statute Charge Configuration record. (Mandatory when charge jurisdiction is IJ)
     * @param chargeGroupId     Long - the unique Id for grouping multiple charges when  a person is charged with committing the same crime.(optional, read only)
     * @param chargeIdentifiers Set<ChargeIdentifierType> - a set of numbers/identifiers assigned by an arresting agency, prosecuting attorney or court. (optional)
     * @param chargeIndicators  Set<ChargeIndicatorType> - a set of the indicators (alerts) on a charge (optional)
     * @param legalText         String - Legal text for a charge (optional)
     * @param chargeDisposition ChargeDispositionType - Disposition details for a charge
     * @param chargePlea        ChargePleaType - Plea details for the charge (optional)
     * @param chargeAppeal      ChargeAppealType - Appeal details for a charge (optional)
     * @param chargeCount       Long - A number of times a person is charged with committing the same crime. (optional)
     * @param facilityId        Long - Static Reference to the facility entity that filed the charge (optional)
     * @param organizationId    Long - Static Reference to the organization entity that filed the charge (optional)
     * @param personIdentityId  Long - Static Reference to the Person entity that filed the charge (optional)
     * @param offenceStartDate  Date - the date of the offence was committed (optional)
     * @param offenceEndDate    Date - Field is used when the exact offence date is not known, to reflect an offence committed date range.(optional)
     * @param filingDate        Date - A date the charge was filed (optional)
     * @param primary           Boolean - True if the charge is a serious charge, false otherwise
     * @param comment           CommentType - The comments related to a charge (optional)
     * @param sentenceId        Long - Id of sentence which is associated to (optional)
     * @param caseInfoId        Long - a case information id. Either case Id or OJ Order Id and only one of them must have value.
     * @param conditionIds      Set<Long> - a set of condition ids which associated to a case (optional)
     * @param orderSentenceIds  Set<Long> - a set of order sentence ids which associated to a case (optional)
     */
    public ChargeType(Long chargeId, Long statuteChargeId, Long chargeGroupId, Set<ChargeIdentifierType> chargeIdentifiers, Set<ChargeIndicatorType> chargeIndicators,
            String legalText, @NotNull ChargeDispositionType chargeDisposition, ChargePleaType chargePlea, ChargeAppealType chargeAppeal, Long chargeCount,
            Long facilityId, Long organizationId, Long personIdentityId, Date offenceStartDate, Date offenceEndDate, Date filingDate, @NotNull Boolean primary,
            CommentType comment, Long sentenceId, Long caseInfoId, Set<Long> conditionIds, Set<Long> orderSentenceIds) {
        this.chargeId = chargeId;
        this.statuteChargeId = statuteChargeId;
        this.chargeGroupId = chargeGroupId;
        this.chargeIdentifiers = chargeIdentifiers;
        this.chargeIndicators = chargeIndicators;
        this.legalText = legalText;
        this.chargeDisposition = chargeDisposition;
        this.chargePlea = chargePlea;
        this.chargeAppeal = chargeAppeal;
        this.chargeCount = chargeCount;
        this.facilityId = facilityId;
        this.organizationId = organizationId;
        this.personIdentityId = personIdentityId;
        this.offenceStartDate = offenceStartDate;
        this.offenceEndDate = offenceEndDate;
        this.filingDate = filingDate;
        this.primary = primary;
        this.comment = comment;
        this.sentenceId = sentenceId;
        this.caseInfoId = caseInfoId;
        this.conditionIds = conditionIds;
        this.orderSentenceIds = orderSentenceIds;
    }

    /**
     * Gets the value of the chargeId property.
     *
     * @return Long - the unique Id for each charge record
     */
    public Long getChargeId() {
        return chargeId;
    }

    /**
     * Sets the value of the chargeId property.
     *
     * @param chargeId Long - the unique Id for each charge record
     */
    public void setChargeId(Long chargeId) {
        this.chargeId = chargeId;
    }

    /**
     * Gets the value of the statuteChargeId property. Mandatory when charge jurisdiction is IJ
     *
     * @return Long - the unique Id for Statute Charge Configuration record.
     */
    public Long getStatuteChargeId() {
        return statuteChargeId;
    }

    /**
     * Sets the value of the statuteChargeId property. Mandatory when charge jurisdiction is IJ
     *
     * @param statuteChargeId Long - the unique Id for Statute Charge Configuration record.
     */
    public void setStatuteChargeId(Long statuteChargeId) {
        this.statuteChargeId = statuteChargeId;
    }

    /**
     * Gets the value of the chargeGroupId property.
     *
     * @return Long - the unique Id for grouping multiple charges when  a person is charged with committing the same crime.
     */
    public Long getChargeGroupId() {
        return chargeGroupId;
    }

    /**
     * Gets the value of the chargeIdentifiers property.
     *
     * @return Set<ChargeIdentifierType> - a set of numbers/identifiers assigned by an arresting agency, prosecuting attorney or court.
     */
    public Set<ChargeIdentifierType> getChargeIdentifiers() {
        return chargeIdentifiers;
    }

    /**
     * Sets the value of the chargeIdentifiers property.
     *
     * @param chargeIdentifiers Set<ChargeIdentifierType> - a set of numbers/identifiers assigned by an arresting agency, prosecuting attorney or court.
     */
    public void setChargeIdentifiers(Set<ChargeIdentifierType> chargeIdentifiers) {
        this.chargeIdentifiers = chargeIdentifiers;
    }

    /**
     * Gets the value of the chargeIndicators property.
     *
     * @return Set<ChargeIndicatorType> - a set of the indicators (alerts) on a charge
     */
    public Set<ChargeIndicatorType> getChargeIndicators() {
        return chargeIndicators;
    }

    /**
     * Sets the value of the chargeIndicators property.
     *
     * @param chargeIndicators Set<ChargeIndicatorType> - a set of the indicators (alerts) on a charge
     */
    public void setChargeIndicators(Set<ChargeIndicatorType> chargeIndicators) {
        this.chargeIndicators = chargeIndicators;
    }

    /**
     * Gets the value of the legalText property.
     *
     * @return String - Legal text for a charge
     */
    public String getLegalText() {
        return legalText;
    }

    /**
     * Sets the value of the legalText property.
     *
     * @param legalText String - Legal text for a charge
     */
    public void setLegalText(String legalText) {
        this.legalText = legalText;
    }

    /**
     * Gets the value of the chargeDisposition property.
     *
     * @return ChargeDispositionType - Disposition details for a charge
     */
    public ChargeDispositionType getChargeDisposition() {
        return chargeDisposition;
    }

    /**
     * Sets the value of the chargeDisposition property.
     *
     * @param chargeDisposition ChargeDispositionType - Disposition details for a charge
     */
    public void setChargeDisposition(ChargeDispositionType chargeDisposition) {
        this.chargeDisposition = chargeDisposition;
    }

    /**
     * Gets the value of the chargePlea property.
     *
     * @return ChargePleaType - Plea details for the charge
     */
    public ChargePleaType getChargePlea() {
        return chargePlea;
    }

    /**
     * Sets the value of the chargePlea property.
     *
     * @param chargePlea ChargePleaType - Plea details for the charge
     */
    public void setChargePlea(ChargePleaType chargePlea) {
        this.chargePlea = chargePlea;
    }

    /**
     * Gets the value of the chargeAppeal property.
     *
     * @return ChargeAppealType - Appeal details for a charge
     */
    public ChargeAppealType getChargeAppeal() {
        return chargeAppeal;
    }

    /**
     * Sets the value of the chargeAppeal property.
     *
     * @param chargeAppeal ChargeAppealType - Appeal details for a charge
     */
    public void setChargeAppeal(ChargeAppealType chargeAppeal) {
        this.chargeAppeal = chargeAppeal;
    }

    /**
     * Gets the value of the chargeCount property.
     *
     * @return Long - A number of times a person is charged with committing the same crime.
     */
    public Long getChargeCount() {
        return chargeCount;
    }

    /**
     * Sets the value of the chargeCount property.
     *
     * @param chargeCount Long - A number of times a person is charged with committing the same crime.
     */
    public void setChargeCount(Long chargeCount) {
        this.chargeCount = chargeCount;
    }

    /**
     * Gets the value of the facility property.
     *
     * @return Long - Static Reference to the facility entity that filed the charge
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * Sets the value of the facility property.
     *
     * @param facilityId Long - Static Reference to the facility entity that filed the charge
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * Gets the value of the organization property.
     *
     * @return Long - Static Reference to the organization entity that filed the charge
     */
    public Long getOrganizationId() {
        return organizationId;
    }

    /**
     * Sets the value of the organization property.
     *
     * @param organizationId Long - Static Reference to the organization entity that filed the charge
     */
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * Gets the value of the personIdentity property.
     *
     * @return Long - Static Reference to the Person entity that filed the charge
     */
    public Long getPersonIdentityId() {
        return personIdentityId;
    }

    /**
     * Sets the value of the personIdentity property.
     *
     * @param personIdentityId Long - Static Reference to the Person entity that filed the charge
     */
    public void setPersonIdentityId(Long personIdentityId) {
        this.personIdentityId = personIdentityId;
    }

    /**
     * Gets the value of the offenceStartDate property.
     *
     * @return Date - the date of the offence was committed
     */
    public Date getOffenceStartDate() {
        return offenceStartDate;
    }

    /**
     * Sets the value of the offenceStartDate property.
     *
     * @param offenceStartDate Date - the date of the offence was committed
     */
    public void setOffenceStartDate(Date offenceStartDate) {
        this.offenceStartDate = offenceStartDate;
    }

    /**
     * Gets the value of the offenceEndDate property.
     *
     * @return Date - Field is used when the exact offence date is not known, to reflect an offence committed date range.
     */
    public Date getOffenceEndDate() {
        return offenceEndDate;
    }

    /**
     * Sets the value of the offenceEndDate property.
     *
     * @param offenceEndDate Date - Field is used when the exact offence date is not known, to reflect an offence committed date range.
     */
    public void setOffenceEndDate(Date offenceEndDate) {
        this.offenceEndDate = offenceEndDate;
    }

    /**
     * Gets the value of the filingDate property.
     *
     * @return Date - A date the charge was filed
     */
    public Date getFilingDate() {
        return filingDate;
    }

    /**
     * Sets the value of the filingDate property.
     *
     * @param filingDate Date - A date the charge was filed
     */
    public void setFilingDate(Date filingDate) {
        this.filingDate = filingDate;
    }

    /**
     * Gets the value of the primary property.
     *
     * @return Boolean - True if the charge is a serious charge, false otherwise
     */
    public Boolean isPrimary() {
        return primary;
    }

    /**
     * Sets the value of the primary property.
     *
     * @param primary Boolean - True if the charge is a serious charge, false otherwise
     */
    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

    /**
     * Gets the value of the comment property.
     *
     * @return CommentType - The comments related to a charge
     */
    public CommentType getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     *
     * @param comment CommentType - The comments related to a charge
     */
    public void setComment(CommentType comment) {
        this.comment = comment;
    }

    /**
     * Get Sentence Id associated to this Charge
     *
     * @return the sentenceId
     */
    public Long getSentenceId() {
        return sentenceId;
    }

    /**
     * Set Sentence Id associated to this Charge
     *
     * @param sentenceId the sentenceId to set
     */
    public void setSentenceId(Long sentenceId) {
        this.sentenceId = sentenceId;
    }

    /**
     * Gets the value of the caseInfoId property. Either case Id or OJ Order Id and only one of them must have value.
     *
     * @return Long - the unique Id for a case information record.
     */
    public Long getCaseInfoId() {
        return caseInfoId;
    }

    /**
     * Sets the value of the caseInfoId property. Either case Id or OJ Order Id and only one of them must have value.
     *
     * @param caseInfoId Long - the unique Id for a case information record.
     */
    public void setCaseInfoId(Long caseInfoId) {
        this.caseInfoId = caseInfoId;
    }

    /**
     * Gets the value of the conditionIds property.
     *
     * @return Set<Long> - a set of condition ids which associated to a charge
     */
    public Set<Long> getConditionIds() {
        if (conditionIds == null) {
            conditionIds = new HashSet<Long>();
        }
        return conditionIds;
    }

    /**
     * Sets the value of the conditionIds property.
     *
     * @param conditionIds Set<Long> - a set of condition ids which associated to a charge
     */
    public void setConditionIds(Set<Long> conditionIds) {
        this.conditionIds = conditionIds;
    }

    /**
     * Gets the value of the orderSentenceIds property.
     *
     * @return Set<Long> - a set of order sentence ids which associated to a charge
     */
    public Set<Long> getOrderSentenceIds() {
        if (orderSentenceIds == null) {
            orderSentenceIds = new HashSet<Long>();
        }
        return orderSentenceIds;
    }

    /**
     * Sets the value of the orderSentenceIds property.
     *
     * @param orderSentenceIds Set<Long> - a set of order sentence ids which associated to a charge
     */
    public void setOrderSentenceIds(Set<Long> orderSentenceIds) {
        this.orderSentenceIds = orderSentenceIds;
    }

    //////////////////////////////////////////////////
    //Web display fields, won't be saved to DB.

    /**
     * Web display field only, won't be saved to DB.
     *
     * @return the reference code of chargeCode
     */
    public String getChargeCode() {
        return chargeCode;
    }

    /**
     * Web display field only, won't be saved to DB.
     *
     * @param chargeCode the chargeCode to set
     */
    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    /**
     * Web display field only, won't be saved to DB.
     *
     * @return the statuteCode
     */
    public String getStatuteCode() {
        return statuteCode;
    }

    /**
     * Web display field only, won't be saved to DB.
     *
     * @param statuteCode the statuteCode to set
     */
    public void setStatuteCode(String statuteCode) {
        this.statuteCode = statuteCode;
    }

    /**
     * Web display field only, won't be saved to DB.
     *
     * @return the reference code of statuteName (statute code)
     */
    public String getStatuteName() {
        return statuteName;
    }

    /**
     * Web display field only, won't be saved to DB.
     *
     * @param statuteName the statuteName to set
     */
    public void setStatuteName(String statuteName) {
        this.statuteName = statuteName;
    }

    /**
     * Web display field only, won't be saved to DB.
     *
     * @return the chargeDescription
     */
    public String getChargeDescription() {
        return chargeDescription;
    }

    /**
     * Web display field only, won't be saved to DB.
     *
     * @param chargeDescription the chargeDescription to set
     */
    public void setChargeDescription(String chargeDescription) {
        this.chargeDescription = chargeDescription;
    }

    /**
     * Web display field only, won't be saved to DB.
     *
     * @return the reference code of chargeType
     */
    public String getChargeType() {
        return chargeType;
    }

    /**
     * Web display field only, won't be saved to DB.
     *
     * @param chargeType the chargeType to set
     */
    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    /**
     * Web display field only, won't be saved to DB.
     *
     * @return the reference code of chargeSeverity
     */
    public String getChargeSeverity() {
        return chargeSeverity;
    }

    /**
     * Web display field only, won't be saved to DB.
     *
     * @param chargeSeverity the chargeSeverity to set
     */
    public void setChargeSeverity(String chargeSeverity) {
        this.chargeSeverity = chargeSeverity;
    }

    /**
     * Web display field only, won't be saved to DB.
     *
     * @return the reference code of chargeDegree
     */
    public String getChargeDegree() {
        return chargeDegree;
    }

    /**
     * Web display field only, won't be saved to DB.
     *
     * @param chargeDegree the chargeDegree to set
     */
    public void setChargeDegree(String chargeDegree) {
        this.chargeDegree = chargeDegree;
    }

    /**
     * Web display field only, won't be saved to DB.
     *
     * @return the reference code of DispositionOutcome
     */
    public String getOutcome() {
        return chargeDisposition.getDispositionOutcome();
    }

    /**
     * Web display field only, won't be saved to DB.
     *
     * @return the reference code of chargeStatus
     */
    public String getChargeStatus() {
        return chargeDisposition.getChargeStatus();
    }

    /**
     * @return the dispositionHistories
     */
    public List<ChargeDispositionType> getDispositionHistories() {
        return dispositionHistories;
    }

    /**
     * @param dispositionHistories the dispositionHistories to set. Data is generated by service, client input is ignored.
     */
    public void setDispositionHistories(List<ChargeDispositionType> dispositionHistories) {
        this.dispositionHistories = dispositionHistories;
    }

    /**
     * Indicate if the charge is IJ charge or OJ charge.
     * A code of ChargeJurisdication set.
     *
     * @return the chargeJurisdiction
     */
    public String getChargeJurisdiction() {
        return chargeJurisdiction;
    }

    /**
     * Indicate if the charge is IJ charge or OJ charge.
     * A code of ChargeJurisdication set.
     *
     * @param chargeJurisdiction the chargeJurisdiction to set
     */
    public void setChargeJurisdiction(String chargeJurisdiction) {
        this.chargeJurisdiction = chargeJurisdiction;
    }

    /**
     * The associated OJ Order id. Either case Id or OJ Order Id and only one of them must have value.
     *
     * @return the oJOrderId
     */
    public Long getOJOrderId() {
        return OJOrderId;
    }

    /**
     * The associated OJ Order id. Either case Id or OJ Order Id and only one of them must have value.
     *
     * @param oJOrderId the oJOrderId to set
     */
    public void setOJOrderId(Long oJOrderId) {
        OJOrderId = oJOrderId;
    }

    /**
     * OJ charge statute id and name. User input.
     *
     * @return the oJStatuteIdName
     */
    public String getOJStatuteIdName() {
        return OJStatuteIdName;
    }

    /**
     * OJ charge statute id and name. User input.
     *
     * @param oJStatuteIdName the oJStatuteIdName to set
     */
    public void setOJStatuteIdName(String oJStatuteIdName) {
        OJStatuteIdName = oJStatuteIdName;
    }

    /**
     * OJ charge code. User input.
     *
     * @return the oJChargeCode
     */
    public String getOJChargeCode() {
        return OJChargeCode;
    }

    /**
     * OJ charge code. User input.
     *
     * @param oJChargeCode the oJChargeCode to set
     */
    public void setOJChargeCode(String oJChargeCode) {
        OJChargeCode = oJChargeCode;
    }

    /**
     * OJ charge description. User input.
     *
     * @return the oJChargeDescription
     */
    public String getOJChargeDescription() {
        return OJChargeDescription;
    }

    /**
     * OJ charge description. User input.
     *
     * @param oJChargeDescription the oJChargeDescription to set
     */
    public void setOJChargeDescription(String oJChargeDescription) {
        OJChargeDescription = oJChargeDescription;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeType [chargeId=" + chargeId + ", statuteChargeId=" + statuteChargeId + ", chargeGroupId=" + chargeGroupId + ", chargeIdentifiers="
                + chargeIdentifiers + ", chargeIndicators=" + chargeIndicators + ", legalText=" + legalText + ", chargeDisposition=" + chargeDisposition + ", chargePlea="
                + chargePlea + ", chargeAppeal=" + chargeAppeal + ", chargeCount=" + chargeCount + ", facilityId=" + facilityId + ", organizationId=" + organizationId
                + ", personIdentityId=" + personIdentityId + ", offenceStartDate=" + offenceStartDate + ", offenceEndDate=" + offenceEndDate + ", filingDate="
                + filingDate + ", primary=" + primary + ", comment=" + comment + ", sentenceId=" + sentenceId + ", caseInfoId=" + caseInfoId + ", conditionIds="
                + conditionIds + ", orderSentenceIds=" + orderSentenceIds + ", chargeJurisdiction=" + chargeJurisdiction + ", OJOrderId=" + OJOrderId
                + ", OJStatuteIdName=" + OJStatuteIdName + ", OJChargeCode=" + OJChargeCode + ", OJChargeDescription=" + OJChargeDescription + ", statuteCode="
                + statuteCode + ", statuteName=" + statuteName + ", chargeCode=" + chargeCode + ", chargeDescription=" + chargeDescription + ", chargeType=" + chargeType
                + ", chargeSeverity=" + chargeSeverity + ", chargeDegree=" + chargeDegree + ", dispositionHistories=" + dispositionHistories + "]";
    }

}
