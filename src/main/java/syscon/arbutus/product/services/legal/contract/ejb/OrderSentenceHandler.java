package syscon.arbutus.product.services.legal.contract.ejb;

import javax.ejb.SessionContext;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.validation.constraints.NotNull;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.*;
import org.hibernate.criterion.*;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.ActivityCategory;
import syscon.arbutus.product.services.activity.contract.dto.ScheduleTimeslotType;
import syscon.arbutus.product.services.activity.contract.dto.Schedule;
import syscon.arbutus.product.services.activity.contract.dto.WeeklyRecurrencePatternType;
import syscon.arbutus.product.services.activity.contract.interfaces.ActivityService;
import syscon.arbutus.product.services.activity.realization.persistence.ActivityEntity;
import syscon.arbutus.product.services.core.common.adapters.DataSecurityServiceAdapter;
import syscon.arbutus.product.services.core.common.adapters.LegalServiceAdapter;
import syscon.arbutus.product.services.core.common.adapters.SentenceCalculationAdapter;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.exception.*;
import syscon.arbutus.product.services.datasecurity.contract.dto.DataSecurityRecordType;
import syscon.arbutus.product.services.datasecurity.contract.dto.DataSecurityRelType;
import syscon.arbutus.product.services.datasecurity.contract.dto.ReferenceSet.DataSecurityRecordTypeEnum;
import syscon.arbutus.product.services.datasecurity.contract.dto.ReferenceSet.EntityTypeEnum;
import syscon.arbutus.product.services.datasecurity.contract.ejb.DataSecurityHelper;
import syscon.arbutus.product.services.legal.contract.dto.LegalModule;
import syscon.arbutus.product.services.legal.contract.dto.ReferenceSet;
import syscon.arbutus.product.services.legal.contract.dto.ordersentence.*;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.AdjustmentType;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.FinePaymentReturnType;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.SentenceAdjustmentType;
import syscon.arbutus.product.services.legal.realization.adapters.ReferenceDataServiceAdapter;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.CourtActivityEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.CaseInfoEntity;
import syscon.arbutus.product.services.legal.realization.persistence.condition.ConditionEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.*;
import syscon.arbutus.product.services.movement.realization.persistence.MovementActivityEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.sentencecalculation.contract.dto.*;
import syscon.arbutus.product.services.sentencecalculation.contract.util.DateUtil;
import syscon.arbutus.product.services.utility.ServiceAdapter;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class OrderSentenceHandler {

    public static final String CUSTODY = "CUS";
    public static final String TERM_NAME_MAX = "MAX";
    private static final Long SUCCESS = 1L;
    private static final int SEARCH_MAX_LIMIT = 200;
    private static final String TERM_NAME_MIN = "MIN";
    private static final String JAIL_TIME = "JT";
    private static Logger log = LoggerFactory.getLogger(OrderSentenceHandler.class);
    private static DateUtil du = new DateUtil();
    private static String[] days = { "SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT" };

    public static <U extends OrderType, V extends OrderEntity> U createOrder(UserContext uc, SessionContext context, Session session, U order, Class<U> typeClazz,
            Class<V> entityClazz) {
        if (log.isDebugEnabled()) {
            log.debug("create: begin");
        }
        U ret = null;

        SentenceType sentenceType = null;

        String functionName = "createOrder";

        ValidationHelper.validate(order);
        Set<Long> caseInfoIdSet = order.getCaseInfoIds();
        if (OrderCategory.IJ.name().equalsIgnoreCase(order.getOrderCategory()) && (caseInfoIdSet == null || caseInfoIdSet.size() == 0)) {
            String message = "Invalid argument: Order must have at least one Case Information Id associated when Order Category is IJ";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        } else if (OrderCategory.OJ.name().equalsIgnoreCase(order.getOrderCategory()) && order.getOjSupervisionId() == null) {
            String message = "Invalid argument: OJ Supervision ID is required when Order Category is OJ";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        order.setOrderIdentification(null);

        if (order instanceof SentenceType) {
            sentenceType = ((SentenceType) order);
            OrderSentenceHelper.verifySentenceType(uc, session, ((SentenceType) order));
        }

        OrderSentenceHelper.verifyOrder(uc, context, session, order);

        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);

        String sentenceTypeAsString = null;
        if (null != sentenceType) {
            sentenceTypeAsString = sentenceType.getSentenceType();
        }

        if ("INTER".equalsIgnoreCase(sentenceTypeAsString)) {
            List<IntermittentSentenceScheduleType> intermsenteScheduleList = getIntermittentSentScheduleList(uc, context, session, sentenceType,
                    order.getOrderIdentification());
            ((SentenceType) order).setSchedules(new HashSet<IntermittentSentenceScheduleType>(intermsenteScheduleList));
        }

        OrderEntity entity = OrderSentenceHelper.toOrderEntity(uc, context, order, stamp, OrderSentenceHelper.CREATE, entityClazz);
        OrderConfigurationEntity config = retrieveOrderConfigurationEntity(session, entity.getOrderClassification(), entity.getOrderType(), entity.getOrderCategory());
        // Based on the ‘OrderClassification’, ‘OrderType’ and ‘OrderCategory’ fields the following
        // fields values are copied over from the ‘OrderConfiguration’ to the Legal Order
        //     IsHoldingOrder
        //     IsSchedulingNeeded
        //     HasCharges
        if (OrderCategory.IJ.name().equalsIgnoreCase(order.getOrderCategory())) {
            if (config == null) {
                String message = "Invalid argument: orderClassification = " + order.getOrderClassification() +
                        ", orderType = " + order.getOrderType() +
                        ", orderCategory = " + order.getOrderCategory();
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
            entity.setIsHoldingOrder(config.getIsHoldingOrder());
            entity.setIsSchedulingNeeded(config.getIsSchedulingNeeded());
            entity.setHasCharges(config.getHasCharges());
            entity.setOjSupervisionId(null);
        }

        ValidationHelper.verifyMetaCodes(entity, true);

        if (order instanceof SentenceType) {
            Boolean bGeneratedNumber = getSentenceNumberConfiguration(uc, context, session);
            if (bGeneratedNumber == null || Boolean.TRUE.equals(bGeneratedNumber)) {
                // Generate Sentence Number and Set it
                Set<Long> caseInfoIds = order.getCaseInfoIds();
                Long caseInfoId = caseInfoIds.iterator().next();
                Long supervisionId = BeanHelper.getEntity(session, CaseInfoEntity.class, caseInfoId).getSupervisionId();
                Long generatedSentenceNumber = OrderSentenceHelper.generateSentenceNumberBySupervisionId(session, supervisionId);
                if (generatedSentenceNumber == null) {
                    String message = "Sentence cannot be created because of none of active Case associated: " + order;
                    LogHelper.error(log, functionName, message);
                    throw new ArbutusRuntimeException(message);
                }
                ((SentenceEntity) entity).setSentenceNumber(generatedSentenceNumber);
            } else {
                // Sentence Number must be unique for the set of Case Info
                Long sentenceNumber = ((SentenceEntity) entity).getSentenceNumber();
                boolean bExisted = OrderSentenceHelper.bExistedSentenceNumber(session, sentenceNumber, order.getCaseInfoIds());
                if (bExisted) {
                    String message = "Sentence Number " + sentenceNumber + " existed already.";
                    LogHelper.error(log, functionName, message);
                    throw new DataExistException(message);
                }
            }

            OrderSentenceHelper.verifyCyclicSentenceConsective(session, (SentenceEntity) entity);

            // Term start date is defaulted to Sentence start date.
            Set<TermEntity> termEntities = ((SentenceEntity) entity).getSentenceTerms();
            if (termEntities != null && termEntities.size() > 0) {
                Date sentenceStartDate = ((SentenceEntity) entity).getSentenceStartDate();
                for (TermEntity termEntity : termEntities) {
                    if (termEntity.getTermStartDate() == null) {
                        termEntity.setTermStartDate(sentenceStartDate);
                    }
                }
            }

        }

        // Persist the order
        session.persist(entity);

        Long orderId = entity.getOrderId();

        // Create Module Links
        LegalHelper.createModuleLinks(uc, context, session, orderId, LegalModule.ORDER_SENTENCE, entity.getModuleAssociations(), stamp);
        if (order instanceof SentenceType) {
            DispositionEntity disposition = entity.getOrderDisposition();
            String outcome = null;
            if (disposition != null) {
                outcome = disposition.getDispositionOutcome();
            }
            OrderSentenceHelper.setChargeSentenceLink(uc, context, session, orderId, order.getChargeIds(), outcome);
            // setSentenceStatusForCaseInfo(uc, context, session, order.getCaseInfoIds(), orderId, true);
            if (OrderClassification.SENT.name().equalsIgnoreCase(entity.getOrderClassification())) {
                OrderSentenceHelper.updateCaseInfoSentenceStatusBySentenceId(session, (SentenceEntity) entity, false);
            }
        }

        // Update the association to CaseActivity
        entity = setAssociatedIds(session, entity, order);

        entity = BeanHelper.getEntity(session, OrderEntity.class, orderId);
        if (order instanceof SentenceType) {
            OrderSentenceHelper.setSentenceCalculationEnableFlagBySentenceId(session, entity.getOrderId(), true);
        }

        // Store to history table
        OrderSentenceHelper.storeToHistoryEntity(session, entity);

        ret = OrderSentenceHelper.toOrder(uc, context, entity, typeClazz);

        if (log.isDebugEnabled()) {
            log.debug("create: end");
        }
        return ret;
    }

    private static OrderEntity setAssociatedIds(Session session, OrderEntity orderEntity, OrderType orderType) {
        //orderInitiatedCaseActivities
        Set<Long> orderInitiatedCaseActivityIds = orderType.getOrderInitiatedCaseActivityAssociations();
        for (CourtActivityEntity entity : orderEntity.getOrderInitiatedCaseActivities()) {
            entity.getOrdersInititated().remove(orderEntity);
            session.update(entity);
        }
        for (Long id : orderInitiatedCaseActivityIds) {
            CourtActivityEntity entity = (CourtActivityEntity) BeanHelper.getEntity(session, CourtActivityEntity.class, id);
            entity.getOrdersInititated().add(orderEntity);
            session.update(entity);
        }

        //caseActivitiesInitiatedOrder
        Set<Long> caseActivitiesInitiatedOrderIds = orderType.getCaseActivityInitiatedOrderAssociations();
        for (CourtActivityEntity entity : orderEntity.getCaseActivitiesInitiatedOrder()) {
            entity.getOrdersResulted().remove(orderEntity);
            session.update(entity);
        }
        for (Long id : caseActivitiesInitiatedOrderIds) {
            CourtActivityEntity entity = (CourtActivityEntity) BeanHelper.getEntity(session, CourtActivityEntity.class, id);
            entity.getOrdersResulted().add(orderEntity);
            session.update(entity);
        }

        session.flush();
        session.clear();
        return orderEntity;
    }

    @SuppressWarnings("unchecked")
    public static <T extends OrderType> T getOrder(UserContext uc, SessionContext context, Session session, Long orderIdentification) {

        // clear cache
        session.flush();
        session.clear();
        if (orderIdentification == null) {
            throw new InvalidInputException("Invalid Argument: orderIdentification is required.");
        }

        Object obj = session.get(OrderEntity.class, orderIdentification);
        //		if (obj == null) {
        //			String message = "Order with id " + orderIdentification + " does not exist.";
        //			throw new DataNotExistException(message);
        //		}

        T order = null;
        if (obj instanceof BailEntity) {
            order = (T) OrderSentenceHelper.toOrder(uc, context, (BailEntity) obj, BailType.class);
        } else if (obj instanceof LegalOrderEntity) {
            order = (T) OrderSentenceHelper.toOrder(uc, context, (LegalOrderEntity) obj, LegalOrderType.class);
        } else if (obj instanceof SentenceEntity) {
            order = (T) OrderSentenceHelper.toOrder(uc, context, (SentenceEntity) obj, SentenceType.class);
        } else if (obj instanceof WarrantDetainerEntity) {
            order = (T) OrderSentenceHelper.toOrder(uc, context, (WarrantDetainerEntity) obj, WarrantDetainerType.class);
        } else {
            order = (T) OrderSentenceHelper.toOrder(uc, context, (OrderEntity) obj, OrderType.class);
        }

        return order;
    }

    public static <T extends OrderType> List<T> getOrderBySupervisionIdForOutJurisdiction(UserContext uc, SessionContext context, Session session, Long supervisionId,
            Boolean historyFlag) {

        if (supervisionId == null) {
            String message = "Invalid argument: supervision Id from Outside Jurisdiction is required.";
            LogHelper.error(log, "getOrderBySupervisionIdForOutJurisdiction", message);
            throw new InvalidInputException(message);
        }

        OrderSearchType orderSearch = new OrderSearchType();
        orderSearch.setOjSupervisionId(supervisionId);

        Criteria criteria = session.createCriteria(OrderEntity.class);
        OrderSentenceHelper.createCriteria(uc, context, session, criteria, null,                        // Set<Long> orderIDs
                null,                    // Set<Long> caseIDs
                null,                        // Set<Long> chargeIDs
                null,                        // Set<Long> conditionIDs
                orderSearch, null, historyFlag                        // Boolean historyFlag
        );

        @SuppressWarnings("unchecked") List<OrderEntity> orderEntityList = criteria.list();
        List<T> ret = OrderSentenceHelper.toOrderList(uc, context, orderEntityList);
        return ret;
    }

    public static Set<Long> getAll(UserContext uc, SessionContext context, Session session) {
        Criteria c = session.createCriteria(OrderEntity.class);
        c.setProjection(Projections.id());
        @SuppressWarnings("unchecked") List<Long> ids = c.list();
        return new HashSet<Long>(ids);
    }

    public static Long deleteOrder(UserContext uc, SessionContext context, Session session, Long orderIdentification) {

        String functionName = "deleteOrder";
        if (orderIdentification == null) {
            throw new InvalidInputException("Invalid Argument: orderIdentification is required.");
        }

        OrderEntity entity = BeanHelper.getEntity(session, OrderEntity.class, orderIdentification);
        String orderClassification = entity.getOrderClassification();
        if (OrderClassification.SENT.name().equalsIgnoreCase(entity.getOrderClassification())) {
            if (OrderSentenceHelper.hasConsecutiveSentence(session, orderIdentification)) {
                String message = "Sentence(" + orderIdentification +
                        ") cannot be deleted before its consective sentence is deleted.";
                LogHelper.error(log, functionName, message);
                throw new DataExistException(message, ErrorCodes.LEG_DELETE_SENTENCE);
            }
        }

        /*// Store to history table  -- No need to store to history because it was backuped when create.
        OrderHistoryEntity orderHistoryEntity = null;
        if (entity instanceof BailEntity) {
            BailHistoryEntity bailHistoryEntity = OrderSentenceHelper.copyToOrderHistoryEntity((BailEntity)entity, BailHistoryEntity.class);
            if (bailHistoryEntity != null) {
                session.save(bailHistoryEntity);
            }
        } else if (entity instanceof WarrantDetainerEntity) {
            WarrantDetainerHistoryEntity warrantDetainerHistoryEntityEntity = OrderSentenceHelper.copyToOrderHistoryEntity((WarrantDetainerEntity)entity, WarrantDetainerHistoryEntity.class);
            if (warrantDetainerHistoryEntityEntity != null) {
                session.save(warrantDetainerHistoryEntityEntity);
            }
        } else if (entity instanceof LegalOrderEntity) {
            LegalOrderHistoryEntity legalOrderHisEntity = OrderSentenceHelper.copyToOrderHistoryEntity((LegalOrderEntity)entity, LegalOrderHistoryEntity.class);
            if (legalOrderHisEntity != null) {
                session.save(legalOrderHisEntity);
            }
        }*/
        // Remove Sentence Aggregate
        if (OrderClassification.SENT.name().equalsIgnoreCase(entity.getOrderClassification())) {
            OrderSentenceHelper.removeSentenceFromAggregate(uc, session, entity);
        }

        if (OrderClassification.SENT.name().equalsIgnoreCase(orderClassification)) {
            OrderSentenceHelper.setSentenceCalculationEnableFlagBySentenceId(session, orderIdentification, true);
        }

        Set<OrderModuleAssociationEntity> moduleAssociations = entity.getModuleAssociations();
        if (moduleAssociations != null) {
            for (OrderModuleAssociationEntity module : moduleAssociations) {
                if (LegalModule.CONDITION.value().equalsIgnoreCase(module.getToClass())) {
                    Long conditionId = module.getToIdentifier();
                    ConditionEntity conditionEntity = BeanHelper.findEntity(session, ConditionEntity.class, module.getToIdentifier());
                    if (conditionEntity != null) {
                        String message = "Order(Bail/Warrant/Legal Order/Sentence) " + orderIdentification +
                                " cannot be deleted unless Condition " + conditionId + " is deleted.";
                        LogHelper.error(log, functionName, message);
                        throw new DataExistException(message, ErrorCodes.LEG_DELETE_SENTENCE_CONDITION_EXIST);
                    }
                }
            }
            if (OrderClassification.SENT.name().equalsIgnoreCase(entity.getOrderClassification())) {
                OrderSentenceHelper.updateCaseInfoSentenceStatusBySentenceId(session, (SentenceEntity) entity, true);
            }
            LegalHelper.removeModuleLinks(uc, context, session, orderIdentification, LegalModule.ORDER_SENTENCE, moduleAssociations);
        }

        // Remove sentenceId from associated Chage
        OrderSentenceHelper.removeSentenceIdFromAssociatedCharge(uc, session, orderIdentification);

        // Delete the association to CaseActivity
        //removeCaseActivityAssociations(uc, context, session, entity);

        // Change to Logical delete
        // session.delete(entity);
        // flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
        entity.setFlag(DataFlag.DELETE.value());
        session.update(entity);

        //WOR-11178
        if (null != entity) {
            cancelAssociatedScheduleMovement(uc, context, session, orderIdentification);
        }

        // Remove Sentence Adjustment
        if (OrderClassification.SENT.name().equalsIgnoreCase(entity.getOrderClassification())) {
            SentenceAdjustmentHandler sentenceAdjustmentHandler = new SentenceAdjustmentHandler(context, session);
            sentenceAdjustmentHandler.removeAdjustmentstBySentenceId(uc, session, orderIdentification);
        }

        return SUCCESS;
    }

    public static List<SentenceType> retrieveSentencesBySupervisionId(UserContext uc, SessionContext context, Session session, Long supervisionId) {
        if (supervisionId == null) {
            throw new InvalidInputException("Invalid argument: supervision id is requored.");
        }

        DetachedCriteria caseInfoCriteria = DetachedCriteria.forClass(CaseInfoEntity.class);
        caseInfoCriteria.add(Restrictions.eq("supervisionId", supervisionId));
        caseInfoCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        caseInfoCriteria.setProjection(Projections.id());

        Criteria sentenceCriteria = session.createCriteria(SentenceEntity.class);
        sentenceCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        sentenceCriteria.addOrder(Order.desc("sentenceNumber"));
        sentenceCriteria.createCriteria("moduleAssociations", "modules");
        sentenceCriteria.add(Restrictions.ilike("modules.toClass", LegalModule.CASE_INFORMATION.value()));
        sentenceCriteria.add(Subqueries.propertyIn("modules.toIdentifier", caseInfoCriteria));

        @SuppressWarnings("unchecked") List<SentenceEntity> sentenceEntities = sentenceCriteria.list();
        List<SentenceType> ret = OrderSentenceHelper.toOrderList(uc, context, sentenceEntities);
        return ret;
    }

    public static List<SentenceEntity> retrieveSentenceEntitiesBySupervisionId(UserContext uc, SessionContext context, Session session, Long supervisionId) {
        if (supervisionId == null) {
            throw new InvalidInputException("Invalid argument: supervision id is requored.");
        }

        DetachedCriteria caseInfoCriteria = DetachedCriteria.forClass(CaseInfoEntity.class);
        caseInfoCriteria.add(Restrictions.eq("supervisionId", supervisionId));
        caseInfoCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        caseInfoCriteria.setProjection(Projections.id());

        Criteria sentenceCriteria = session.createCriteria(SentenceEntity.class);
        sentenceCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        sentenceCriteria.addOrder(Order.desc("sentenceNumber"));
        sentenceCriteria.createCriteria("moduleAssociations", "modules");
        sentenceCriteria.add(Restrictions.ilike("modules.toClass", LegalModule.CASE_INFORMATION.value()));
        sentenceCriteria.add(Subqueries.propertyIn("modules.toIdentifier", caseInfoCriteria));
        sentenceCriteria.createCriteria("orderDisposition", "disposition");
        sentenceCriteria.add(Restrictions.ilike("disposition.orderStatus", ORDER_STATUS.ACTIVE.name()));
        sentenceCriteria.add(Restrictions.ilike("sentenceStatus", SentenceStatus.SENTENCED.name()));
        sentenceCriteria.setFetchMode("intermittentSentenceSchedules", FetchMode.JOIN);
        sentenceCriteria.setFetchMode("finePayments", FetchMode.JOIN);

        @SuppressWarnings("unchecked") List<SentenceEntity> sentenceEntities = sentenceCriteria.list();

        return sentenceEntities;
    }

    public static SupervisionSentenceType retrieveSupervisionSentence(UserContext uc, SessionContext context, Session session, Long supervisionId) {
        if (supervisionId == null) {
            throw new InvalidInputException("Invalid argument: supervision id is requored.");
        }

        Criteria criteria = session.createCriteria(SupervisionSentenceEntity.class);
        criteria.add(Restrictions.eq("supervisionId", supervisionId));
        criteria.add(Restrictions.eq("isHistory", Boolean.FALSE));
        criteria.addOrder(Order.desc("supervisionSentenceId"));
        criteria.setMaxResults(1);

        SupervisionSentenceEntity entity = (SupervisionSentenceEntity) criteria.uniqueResult();
        return OrderSentenceHelper.toSupervisionSentenceType(uc, context, session, entity);
    }

    public static List<SupervisionSentenceType> retrieveSupervisionSentenceWithHistory(UserContext uc, SessionContext context, Session session, Long supervisionId) {
        if (supervisionId == null) {
            throw new InvalidInputException("Invalid argument: supervision id is requored.");
        }

        Criteria criteria = session.createCriteria(SupervisionSentenceEntity.class);
        criteria.add(Restrictions.eq("supervisionId", supervisionId));
        criteria.addOrder(Order.desc("supervisionSentenceId"));

        @SuppressWarnings("unchecked") List<SupervisionSentenceEntity> list = criteria.list();
        return OrderSentenceHelper.toSupervisionSentenceTypeList(uc, context, session, list);
    }

    public static List<SupervisionSentenceType> retrieveSupervisionSentenceWithHistoryByKeyDateHistoryId(UserContext uc, SessionContext context, Session session,
            Long supervisionId, Long sentenceKeyDateHistoryId) {

        Criteria c = session.createCriteria(SupervisionSentenceEntity.class);
        c.add(Restrictions.eq("supervisionId", supervisionId));
        c.add(Restrictions.eq("keyDateHistoryId", sentenceKeyDateHistoryId));
        c.addOrder(Order.desc("supervisionSentenceId"));

        @SuppressWarnings("unchecked") List<SupervisionSentenceEntity> list = c.list();

        return OrderSentenceHelper.toSupervisionSentenceTypeList(uc, context, session, list);
    }

    public static List<AggregateSentenceGroupType> retrieveAggregateSentencesBySupervisionId(UserContext uc, SessionContext context, Session session,
            Long supervisionId) {

        if (log.isDebugEnabled()) {
            log.debug("retrieveAggregateSentencesBySupervisionId: begin");
        }

        String functionName = "retrieveAggregateSentencesBySupervisionId";
        if (supervisionId == null) {
            String message = "Invalid argument: supervisionId is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        List<AggregateSentenceGroupType> ret;
        Criteria criteria = session.createCriteria(AggregateSentenceEntity.class);
        criteria.add(Restrictions.eq("supervisionId", supervisionId));
        criteria.add(Restrictions.eq("isHistory", Boolean.FALSE));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.addOrder(Order.desc("aggregateId"));

        @SuppressWarnings("unchecked") List<AggregateSentenceEntity> aggregateSentenceEntityList = criteria.list();
        ret = OrderSentenceHelper.toAggregateSentenceTypeList(uc, context, session, supervisionId, aggregateSentenceEntityList);

        if (log.isDebugEnabled()) {
            log.debug("retrieveAggregateSentencesBySupervisionId: end");
        }

        return ret;
    }

    public static List<AggregateSentenceGroupType> retrieveAggregateSentencesWithHistoryBySupervisionId(UserContext uc, SessionContext context, Session session,
            Long supervisionId) {
        if (log.isDebugEnabled()) {
            log.debug("retrieveAggregateSentencesBySupervisionId: begin");
        }

        String functionName = "retrieveAggregateSentencesBySupervisionId";
        if (supervisionId == null) {
            String message = "Invalid argument: supervisionId is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        List<AggregateSentenceGroupType> ret = new ArrayList<>();
        Criteria criteria = session.createCriteria(AggregateSentenceEntity.class);
        criteria.add(Restrictions.eq("supervisionId", supervisionId));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.addOrder(Order.desc("aggregateId"));

        @SuppressWarnings("unchecked") List<AggregateSentenceEntity> aggregateSentenceEntityList = criteria.list();
        ret = OrderSentenceHelper.toAggregateSentenceTypeList(uc, context, session, supervisionId, aggregateSentenceEntityList);

        if (log.isDebugEnabled()) {
            log.debug("retrieveAggregateSentencesBySupervisionId: end");
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    public static <T extends OrderType> List<T> retrieveOrdersForCase(UserContext uc, SessionContext context, Session session, Long caseIdentification,
            OrderSearchType orderSearch, Boolean isActiveFlag) {

        //clear
        session.flush();
        session.clear();
        if (log.isDebugEnabled()) {
            log.debug("retrieveOrdersForCase: begin");
        }

        List<T> ret = new ArrayList<T>();
        if (caseIdentification == null) {
            ValidationHelper.validateSearchType(orderSearch);
        }
        Set<Long> caseIDs = new HashSet<Long>();
        caseIDs.add(caseIdentification);

        Criteria criteria = session.createCriteria(OrderEntity.class);
        OrderSentenceHelper.createCriteria(uc, context, session, criteria, null,                        // Set<Long> orderIDs
                caseIDs,                    // Set<Long> caseIDs
                null,                        // Set<Long> chargeIDs
                null,                        // Set<Long> conditionIDs
                orderSearch, isActiveFlag, null                        // Boolean historyFlag
        );

        List<OrderEntity> orderEntityList = criteria.list();

        ret = OrderSentenceHelper.toOrderList(uc, context, orderEntityList);

        if (log.isDebugEnabled()) {
            log.debug("retrieveOrdersForCase: end");
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    public static <T extends OrderType> List<T> retrieveOrdersForCharge(UserContext uc, SessionContext context, Session session, Set<Long> chargeIdentifications,
            OrderSearchType orderSearch, Boolean isActiveFlag) {

        if (log.isDebugEnabled()) {
            log.debug("retrieveOrdersForCharge: begin");
        }

        List<T> ret = new ArrayList<T>();
        if (chargeIdentifications == null || chargeIdentifications.size() == 0) {
            ValidationHelper.validateSearchType(orderSearch);
        }

        Criteria criteria = session.createCriteria(OrderEntity.class);
        OrderSentenceHelper.createCriteria(uc, context, session, criteria, null,                        // Set<Long> orderIDs
                null,                        // Set<Long> caseIDs
                chargeIdentifications,        // Set<Long> chargeIDs
                null,                        // Set<Long> conditionIDs
                orderSearch, isActiveFlag, null                        // Boolean historyFlag
        );

        List<OrderEntity> orderEntityList = criteria.list();

        ret = OrderSentenceHelper.toOrderList(uc, context, orderEntityList);

        if (log.isDebugEnabled()) {
            log.debug("retrieveOrdersForCharge: end");
        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    @Deprecated
    public static <T extends OrderType> List<T> searchOrders(UserContext uc, SessionContext context, Session session, Long caseIdentification, Long chargeIdentification,
            Long conditionIdentification, OrderSearchType orderSearch, Boolean historyFlag) {

        if (log.isDebugEnabled()) {
            log.debug("searchOrders: begin");
        }

        List<T> ret = new ArrayList<T>();

        if (caseIdentification == null && chargeIdentification == null && conditionIdentification == null) {
            ValidationHelper.validateSearchType(orderSearch);
        }
        Criteria criteria = session.createCriteria(OrderEntity.class);
        OrderSentenceHelper.createCriteria(uc, context, session, criteria, null,                        // Set<Long> orderIDs
                new HashSet<Long>(Arrays.asList(new Long[] { caseIdentification })),    // Set<Long> caseIDs
                new HashSet<Long>(Arrays.asList(new Long[] { chargeIdentification })),    // Set<Long> chargeIDs
                new HashSet<Long>(Arrays.asList(new Long[] { conditionIdentification })), // Set<Long> conditionIDs
                orderSearch, true,            // Boolean isActiveFlag
                historyFlag);

        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        List<OrderEntity> orderEntityList = criteria.list();

        if (orderEntityList.size() > SEARCH_MAX_LIMIT) {
            throw new ArbutusRuntimeException("Search result is over " + SEARCH_MAX_LIMIT);
        }

        ret = OrderSentenceHelper.toOrderList(uc, context, orderEntityList);

        if (log.isDebugEnabled()) {
            log.debug("searchOrders: end");
        }

        return ret;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static <T extends OrderEntity> OrdersReturnType searchOrders(UserContext uc, SessionContext context, Session session, Set<Long> orderIdentifications,
            Set<Long> caseIdentifications, Set<Long> chargeIdentifications, Set<Long> conditionIdentifications, OrderSearchType orderSearch, Boolean historyFlag,
            Long startIndex, Long resultSize, String resultOrder) {

        if (log.isDebugEnabled()) {
            log.debug("searchOrders: begin");
        }

        if (BeanHelper.isEmpty(orderIdentifications) && BeanHelper.isEmpty(caseIdentifications) && BeanHelper.isEmpty(chargeIdentifications) && BeanHelper.isEmpty(
                conditionIdentifications)) {

            ValidationHelper.validateSearchType(orderSearch);
        }

        Criteria criteria = session.createCriteria(OrderEntity.class);
        OrderSentenceHelper.createCriteria(uc, context, session, criteria, orderIdentifications,                        // Set<Long> orderIDs
                caseIdentifications,    // Set<Long> caseIDs
                chargeIdentifications,    // Set<Long> chargeIDs
                conditionIdentifications, // Set<Long> conditionIDs
                orderSearch, true,            // Boolean isActiveFlag
                historyFlag);

        Long totalSize = (Long) criteria.setProjection(Projections.countDistinct("orderId")).uniqueResult();
        criteria.setProjection(null);
        // criteria.setResultTransformer(Criteria.ROOT_ENTITY);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());
            criteria.setMaxResults(resultSize.intValue());
        }

        // Search and only return the predefined max number of records
        criteria.setLockMode(LockMode.NONE);
        criteria.setCacheMode(CacheMode.IGNORE);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        List orders = new ArrayList();
        List<T> list = criteria.list();
        if (resultSize == null && list.size() > SEARCH_MAX_LIMIT) {
            throw new ArbutusRuntimeException("Search result is over " + SEARCH_MAX_LIMIT);
        }

        orders = OrderSentenceHelper.toOrderList(uc, context, list);

        OrdersReturnType ret = new OrdersReturnType();
        ret.setOrders(orders);
        ret.setTotalSize(totalSize);

        if (log.isDebugEnabled()) {
            log.debug("searchOrders: end");
        }

        return ret;
    }

    public static <T extends OrderType> List<T> retrieveOrderHistory(UserContext uc, SessionContext context, Session session, Long orderId, Date fromHistoryDate,
            Date toHistoryDate) {
        if (log.isTraceEnabled()) {
            log.debug("retrieveDispositionHistoryByOrderId: begin");
        }
        String functionName = "retrieveDispositionHistoryByOrderId";

        if (orderId == null) {
            String message = "Invalid argument: orderId is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        if (fromHistoryDate != null && toHistoryDate != null && (fromHistoryDate.compareTo(toHistoryDate) > 0)) {
            String message = "Invalid argument: fromHistoryDate " + fromHistoryDate + " should not be after toHistoryDate" +
                    toHistoryDate;
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Criteria c = session.createCriteria(OrderHistoryEntity.class);
        c.addOrder(Order.desc("historyId"));
        // c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        c.add(Restrictions.eq("orderId", orderId));
        if (fromHistoryDate != null) {
            c.add(Restrictions.ge("orderIssuanceDate", fromHistoryDate));
            c.add(Restrictions.ge("orderStartDate", fromHistoryDate));
        }
        if (toHistoryDate != null) {
            c.add(Restrictions.le("orderIssuanceDate", toHistoryDate));
            c.add(Restrictions.le("orderStartDate", toHistoryDate));
        }

        List<T> orderTypeList = new ArrayList<>();
        // Set<T> encounteredSet = new HashSet<>();
        @SuppressWarnings("unchecked") Iterator<OrderHistoryEntity> it = c.list().iterator();
        while (it.hasNext()) {
            OrderHistoryEntity orderHistoryEntity = it.next();
            if (orderHistoryEntity instanceof WarrantDetainerHistoryEntity) {
                @SuppressWarnings("unchecked") T order = (T) OrderSentenceHelper.toOrderType(orderHistoryEntity, WarrantDetainerType.class);
                //                boolean bFirst = encounteredSet.add(order);
                //                if (bFirst) {
                orderTypeList.add(order);
                //                }
            } else if (orderHistoryEntity instanceof BailHistoryEntity) {
                @SuppressWarnings("unchecked") T order = (T) OrderSentenceHelper.toOrderType(orderHistoryEntity, BailType.class);
                //                boolean bFirst = encounteredSet.add(order);
                //                if (bFirst) {
                orderTypeList.add(order);
                //                }
            } else if (orderHistoryEntity instanceof LegalOrderHistoryEntity) {
                @SuppressWarnings("unchecked") T order = (T) OrderSentenceHelper.toOrderType(orderHistoryEntity, LegalOrderType.class);
                //                boolean bFirst = encounteredSet.add(order);
                //                if (bFirst) {
                orderTypeList.add(order);
                //                }
            }
        }

        if (log.isTraceEnabled()) {
            log.debug("retrieveDispositionHistoryByOrderId: end");
        }
        return orderTypeList;
    }

    public static List<DispositionType> retrieveDispositionHistoryByOrderId(UserContext uc, SessionContext context, Session session, Long orderId, Date fromHistoryDate,
            Date toHistoryDate) {

        if (log.isTraceEnabled()) {
            log.debug("retrieveDispositionHistoryByOrderId: begin");
        }
        String functionName = "retrieveDispositionHistoryByOrderId";

        if (orderId == null) {
            String message = "Invalid argument: orderId is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        Criteria c = session.createCriteria(OrderHistoryEntity.class);
        c.addOrder(Order.desc("historyId"));
        // c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        c.add(Restrictions.eq("orderId", orderId));
        if (fromHistoryDate != null) {
            c.add(Restrictions.ge("disposition.dispositionDate", fromHistoryDate));
        }
        if (toHistoryDate != null) {
            c.add(Restrictions.le("disposition.dispositionDate", toHistoryDate));
        }

        Set<DispositionType> encounteredSet = new HashSet<>();
        List<DispositionType> dispositionTypes = new ArrayList<>();
        @SuppressWarnings("unchecked") Iterator<OrderHistoryEntity> it = c.list().iterator();
        while (it.hasNext()) {
            OrderHistoryEntity orderHistoryEntity = it.next();
            DispositionHistoryEntity dispositionHistoryEntity = orderHistoryEntity.getOrderDisposition();
            DispositionType dispositionType = new DispositionType();
            dispositionType.setDispositionOutcome(dispositionHistoryEntity.getDispositionOutcome());
            dispositionType.setOrderStatus(dispositionHistoryEntity.getOrderStatus());
            dispositionType.setDispositionDate(dispositionHistoryEntity.getDispositionDate());
            dispositionType.setStamp(BeanHelper.toStamp(dispositionHistoryEntity.getStamp()));
            boolean bFirst = encounteredSet.add(dispositionType);
            if (bFirst) {
                dispositionTypes.add(dispositionType);
            }
        }

        if (log.isTraceEnabled()) {
            log.debug("retrieveDispositionHistoryByOrderId: end");
        }
        return dispositionTypes;
    }

    public static LegalOrderType createLegalOrder(UserContext uc, SessionContext context, Session session, LegalOrderType legalOrder) {

        if (log.isDebugEnabled()) {
            log.debug("createLegalOrder: begin");
        }

        LegalOrderType ret = createOrder(uc, context, session, legalOrder, LegalOrderType.class, LegalOrderEntity.class);

        if (log.isDebugEnabled()) {
            log.debug("createLegalOrder: end");
        }
        return ret;
    }

    public static WarrantDetainerType createWarrantDetainer(UserContext uc, SessionContext context, Session session, WarrantDetainerType warrantDetainer) {

        if (log.isDebugEnabled()) {
            log.debug("createWarrantDetainer: begin");
        }

        WarrantDetainerType ret = createOrder(uc, context, session, warrantDetainer, WarrantDetainerType.class, WarrantDetainerEntity.class);

        if (log.isDebugEnabled()) {
            log.debug("createWarrantDetainer: end");
        }
        return ret;
    }

    public static BailType createBail(UserContext uc, SessionContext context, Session session, BailType bail) {
        if (log.isDebugEnabled()) {
            log.debug("createBail: begin");
        }

        BailType ret = createOrder(uc, context, session, bail, BailType.class, BailEntity.class);
        if (log.isDebugEnabled()) {
            log.debug("createBail: end");
        }
        return ret;
    }

    public static SentenceType createSentence(UserContext uc, SessionContext context, Session session, SentenceType sentence) {

        if (log.isDebugEnabled()) {
            log.debug("createSentence: begin");
        }

        SentenceType ret = createOrder(uc, context, session, sentence, SentenceType.class, SentenceEntity.class);

        if (log.isDebugEnabled()) {
            log.debug("createSentence: end");
        }
        return ret;
    }

    public static Long getMaxSentenceNumberBySupervisionId(UserContext uc, SessionContext context, Session session, Long supervisionId) {

        Long maxSentenceNumber = OrderSentenceHelper.generateSentenceNumberBySupervisionId(session, supervisionId);

        return maxSentenceNumber - 1L;
    }

    public static Long getMaxSentenceNumberByCaseInfo(UserContext uc, SessionContext context, Session session, Set<Long> caseInfoIds) {

        Long maxSentenceNumber = OrderSentenceHelper.generateSentenceNumber(session, caseInfoIds);

        return maxSentenceNumber - 1L;
    }

    public static Date retrieveSentenceIssuanceDateByChargeId(UserContext uc, SessionContext context, Session session, Long chargeId) {

        if (chargeId == null) {
            throw new InvalidInputException("Charge Id is required for retrieving Sentence Issuance Date By Charge Id");
        }

        //		ChargeEntity chargeEntity = BeanHelper.getEntity(session, ChargeEntity.class, chargeId);
        //		Long caseInfoId = chargeEntity.getCaseInfoId();

        Criteria c = session.createCriteria(CourtActivityEntity.class);
        c.setProjection(Projections.property("activityOccurredDate"));
        c.addOrder(Order.asc("activityOccurredDate"));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        c.setMaxResults(1);
        c.add(Restrictions.eq("activityOutcome", CaseActivityOutcome.SENTC.name()));
        c.createCriteria("chargeAssociations", "chgAssoc");
        //		c.createCriteria("caseInfos", "caseInfo");
        //		c.add(Restrictions.or(
        //				Restrictions.eq("chgAssoc.toIdentifier", chargeId),
        //				Restrictions.eq("caseInfo.caseInfoId", caseInfoId))
        //			 );
        c.add(Restrictions.eq("chgAssoc.toIdentifier", chargeId));

        return (Date) c.uniqueResult();
    }

    public static <U extends OrderType, V extends OrderEntity> U updateOrder(UserContext uc, SessionContext context, Session session, U order, Class<U> typeClazz,
            Class<V> entityClazz) {
        if (log.isDebugEnabled()) {
            log.debug("update: begin");
        }
        U ret = null;

        if (order == null || order.getOrderIdentification() == null) {
            throw new InvalidInputException("Invalid argument: null order");
        }
        ValidationHelper.validate(order);
        Set<Long> caseInfoIdSet = order.getCaseInfoIds();
        if (OrderCategory.IJ.name().equalsIgnoreCase(order.getOrderCategory()) && (caseInfoIdSet == null || caseInfoIdSet.size() == 0)) {
            String message = "Invalid argument: Order must have at least one Case Information Id associated when Order Category is IJ";
            LogHelper.error(log, "updateOrder", message);
            throw new InvalidInputException(message);
        } else if (OrderCategory.OJ.name().equalsIgnoreCase(order.getOrderCategory()) && order.getOjSupervisionId() == null) {
            String message = "Invalid argument: OJ Supervision ID is required when Order Category is OJ";
            LogHelper.error(log, "updateOrder", message);
            throw new InvalidInputException(message);
        }

        String sentenceTypeAsString = null;
        if (order instanceof SentenceType) {
            sentenceTypeAsString = ((SentenceType) order).getSentenceType();
            OrderSentenceHelper.verifySentenceType(uc, session, ((SentenceType) order));
        }

        Long orderId = order.getOrderIdentification();

        OrderEntity orderEntity = BeanHelper.getEntity(session, OrderEntity.class, order.getOrderIdentification());

        //WOR-11178
        Set<IntermittentSentenceScheduleEntity> schedulesMovementOld = null;
        if (order instanceof SentenceType) {
            schedulesMovementOld = ((SentenceEntity) orderEntity).getIntermittentSentenceSchedules();
        }
        boolean isSentenceUpdated = false;
        if (order instanceof SentenceType && "INTER".equalsIgnoreCase(sentenceTypeAsString)) {
            isSentenceUpdated = OrderSentenceHelper.
                    isSentenceChangedToRegenerateSchedule(((SentenceEntity) orderEntity), ((SentenceType) order));
            if (log.isDebugEnabled()) {
                log.debug("*****************  isSentenceUpdated   " + isSentenceUpdated);
            }
            if (isSentenceUpdated) {
                List<IntermittentSentenceScheduleType> intermsenteScheduleList = getIntermittentSentScheduleListForUpdate(uc, context, session, (SentenceType) order,
                        (SentenceEntity) orderEntity, orderId);
                ((SentenceType) order).setSchedules(new HashSet<IntermittentSentenceScheduleType>(intermsenteScheduleList));
            }
        }

        // order.setAssociations(null); // No association to be associated
        OrderSentenceHelper.verifyOrder(uc, context, session, order);
        StampEntity stamp = BeanHelper.getModifyStamp(uc, context, orderEntity.getStamp());
        OrderEntity entity = OrderSentenceHelper.toOrderEntity(uc, context, order, stamp, OrderSentenceHelper.UPDATE, entityClazz);

        //IntermittentSentenceSchedules already merged at line number 1126
        if (!isSentenceUpdated && entity instanceof SentenceEntity && "INTER".equalsIgnoreCase(sentenceTypeAsString)) {
            ((SentenceEntity) entity).setIntermittentSentenceSchedules(((SentenceEntity) orderEntity).getIntermittentSentenceSchedules());
        } else if (entity instanceof SentenceEntity && !"INTER".equalsIgnoreCase(sentenceTypeAsString)) {
            ((SentenceEntity) entity).setIntermittentSentenceSchedules(new HashSet<IntermittentSentenceScheduleEntity>());
        }

        ValidationHelper.verifyMetaCodes(entity, false);

        // Order Classification should not be able to be updated
        entity.setOrderClassification(orderEntity.getOrderClassification());
        // The following fields can not be updated
        //     IsHoldingOrder
        //     IsSchedulingNeeded
        //     HasCharges
        entity.setIsHoldingOrder(orderEntity.getIsHoldingOrder());
        entity.setIsSchedulingNeeded(orderEntity.getIsSchedulingNeeded());
        entity.setHasCharges(orderEntity.getHasCharges());
        entity.setVersion(orderEntity.getVersion());

        // Remove sentenceId from associated Chage
        OrderSentenceHelper.removeSentenceIdFromAssociatedCharge(uc, session, orderId);
        LegalHelper.removeModuleLinks(uc, context, session, orderId, LegalModule.ORDER_SENTENCE, orderEntity.getModuleAssociations());

        if (order instanceof SentenceType) {
            // SentenceNumber cannot be updated once it is created.
            if (((SentenceEntity) orderEntity).getSentenceNumber() != null) {
                ((SentenceEntity) entity).setSentenceNumber(((SentenceEntity) orderEntity).getSentenceNumber());
            } else {
                if (((SentenceEntity) entity).getSentenceNumber() != null) {
                    Boolean bGeneratedNumber = getSentenceNumberConfiguration(uc, context, session);
                    if (Boolean.TRUE.equals(bGeneratedNumber)) {
                        // Keep the original Sentence Number
                        ((SentenceEntity) entity).setSentenceNumber(((SentenceEntity) orderEntity).getSentenceNumber());
                    } else {
                        // Sentence Number must be unique for the set of Case Info
                        Long sentenceNumber = ((SentenceEntity) entity).getSentenceNumber();
                        boolean bExisted = OrderSentenceHelper.bExistedSentenceNumber(session, sentenceNumber, order.getCaseInfoIds());
                        if (bExisted) {
                            throw new DataExistException("Sentence Number " + sentenceNumber + " existed already.");
                        }
                    }
                }
            }
            //Retail all the Fine Payments
            ((SentenceEntity) entity).setFinePayments(((SentenceEntity) orderEntity).getFinePayments());
            OrderSentenceHelper.verifyCyclicSentenceConsective(session, (SentenceEntity) entity);
        }

        // Update the association to CaseActivity
        setAssociatedIds(session, orderEntity, order);
        // setAssociatedIds(session, entity, order);
        entity = (OrderEntity) session.merge(entity);
        session.flush();
        session.clear();

        // Start WOR-11178 (Point 3)
        if (isSentenceUpdated) {
            Set<IntermittentSentenceScheduleEntity> schedulesMovementNew = ((SentenceEntity) entity).getIntermittentSentenceSchedules();
            deleteAssociatedScheduleMovement(uc, context, session, schedulesMovementNew, schedulesMovementOld);
        }

        // end WOR-11178 (Point 3)

        LegalHelper.createModuleLinks(uc, context, session, orderId, LegalModule.ORDER_SENTENCE, entity.getModuleAssociations(), stamp);
        if (order instanceof SentenceType) {
            DispositionEntity disposition = entity.getOrderDisposition();
            String outcome = null;
            if (disposition != null) {
                outcome = disposition.getDispositionOutcome();
            }
            OrderSentenceHelper.setChargeSentenceLink(uc, context, session, orderId, order.getChargeIds(), outcome);

            // setSentenceStatusForCaseInfo(uc, context, session, order.getCaseInfoIds(), orderId, true);
            if (OrderClassification.SENT.name().equalsIgnoreCase(entity.getOrderClassification())) {
                OrderSentenceHelper.updateCaseInfoSentenceStatusBySentenceId(session, (SentenceEntity) entity, false);
            }
        }

        entity = BeanHelper.getEntity(session, OrderEntity.class, orderId);

        if (order instanceof SentenceType) {
            OrderSentenceHelper.setSentenceCalculationEnableFlagBySentenceId(session, entity.getOrderId(), true);
        }

        // Store to history table
        OrderSentenceHelper.storeToHistoryEntity(session, entity);

        ret = OrderSentenceHelper.toOrder(uc, context, entity, typeClazz);

        //wor-11178
        ret.setIsSchedulingNeeded(isSentenceUpdated);

        if (log.isDebugEnabled()) {
            log.debug("update: end");
        }
        return ret;
    }

    public static LegalOrderType updateLegalOrder(UserContext uc, SessionContext context, Session session, LegalOrderType legalOrder) {
        if (log.isDebugEnabled()) {
            log.debug("updateLegalOrder: begin");
        }
        LegalOrderType ret = updateOrder(uc, context, session, legalOrder, LegalOrderType.class, LegalOrderEntity.class);

        if (log.isDebugEnabled()) {
            log.debug("updateLegalOrder: end");
        }
        return ret;
    }

    public static WarrantDetainerType updateWarrantDetainer(UserContext uc, SessionContext context, Session session, WarrantDetainerType warrantDetainer) {
        if (log.isDebugEnabled()) {
            log.debug("updateWarrantDetainer: begin");
        }
        WarrantDetainerType ret = updateOrder(uc, context, session, warrantDetainer, WarrantDetainerType.class, WarrantDetainerEntity.class);

        if (log.isDebugEnabled()) {
            log.debug("updateWarrantDetainer: end");
        }
        return ret;
    }

    public static BailType updateBail(UserContext uc, SessionContext context, Session session, BailType bail) {
        if (log.isDebugEnabled()) {
            log.debug("updateBail: begin");
        }
        BailType ret = updateOrderBail(uc, context, session, bail, BailType.class, BailEntity.class);

        if (log.isDebugEnabled()) {
            log.debug("updateBail: end");
        }
        return ret;
    }

    public static SentenceType updateSentence(UserContext uc, SessionContext context, Session session, SentenceType sentence) {
        if (log.isDebugEnabled()) {
            log.debug("updateSentence: begin");
        }
        SentenceType ret = updateOrder(uc, context, session, sentence, SentenceType.class, SentenceEntity.class);

        if (log.isDebugEnabled()) {
            log.debug("updateSentence: end");
        }
        return ret;
    }

    public static SentenceKeyDateType createSentenceKeyDate(UserContext uc, SessionContext context, Session session, SentenceKeyDateType keyDates) {
        if (log.isDebugEnabled()) {
            log.debug("createKeyDate: begin");
        }

        ValidationHelper.validate(keyDates);
        OrderSentenceHelper.verifySentenceKeyDate(uc, keyDates);

        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);
        SentenceKeyDateEntity entity = OrderSentenceHelper.toSentenceKeyDateEntity(uc, keyDates, stamp);
        ValidationHelper.verifyMetaCodes(entity, true);
        session.persist(entity);

        OrderSentenceHelper.copyToHistoryKeyDate(uc, context, session, entity);

        SentenceKeyDateType ret = OrderSentenceHelper.toSentenceKeyDateType(uc, session, entity, Boolean.FALSE);

        if (log.isDebugEnabled()) {
            log.debug("createKeyDate: end");
        }

        return ret;
    }

    public static SentenceKeyDateType updateSentenceKeyDate(UserContext uc, SessionContext context, Session session, SentenceKeyDateType keyDates) {
        if (log.isDebugEnabled()) {
            log.debug("updateKeyDate: begin");
        }

        ValidationHelper.validate(keyDates);
        OrderSentenceHelper.verifySentenceKeyDate(uc, keyDates);

        SentenceKeyDateEntity savedEntity = BeanHelper.getEntity(session, SentenceKeyDateEntity.class, keyDates.getSupervisionId());

        OrderSentenceHelper.copyToHistoryKeyDate(uc, context, session, savedEntity);
        StampEntity stamp = BeanHelper.getModifyStamp(uc, context, savedEntity.getStamp());
        SentenceKeyDateEntity entity = OrderSentenceHelper.toSentenceKeyDateEntity(uc, keyDates, stamp);
        ValidationHelper.verifyMetaCodes(entity, false);
        entity.setVersion(savedEntity.getVersion());

        entity = (SentenceKeyDateEntity) session.merge(entity);

        SentenceKeyDateType ret = OrderSentenceHelper.toSentenceKeyDateType(uc, session, entity, Boolean.FALSE);

        if (log.isDebugEnabled()) {
            log.debug("updateKeyDate: end");
        }

        return ret;
    }

    public static SentenceKeyDateType getSentenceKeyDate(UserContext uc, SessionContext context, Session session, Long supervisionId) {

        if (log.isDebugEnabled()) {
            log.debug("getKeyDate: begin");
        }

        if (supervisionId == null) {
            throw new InvalidInputException("supervisionId must not be null.");
        }

        SentenceKeyDateEntity entity = (SentenceKeyDateEntity) session.get(SentenceKeyDateEntity.class, supervisionId);

        SentenceKeyDateType ret = OrderSentenceHelper.toSentenceKeyDateType(uc, session, entity, Boolean.FALSE);

        if (log.isDebugEnabled()) {
            log.debug("getKeyDate: end");
        }

        return ret;
    }

    public static SentenceKeyDateType findSentenceKeyDateBySupervisionId(UserContext uc, SessionContext context, Session session, Long supervisionId) {

        if (log.isDebugEnabled()) {
            log.debug("findKeyDateBySupervisionId: begin");
        }

        if (supervisionId == null) {
            throw new InvalidInputException("supervisionId must not be null.");
        }

        SentenceKeyDateEntity entity = BeanHelper.findEntity(session, SentenceKeyDateEntity.class, supervisionId);
        SentenceKeyDateType ret = OrderSentenceHelper.toSentenceKeyDateType(uc, session, entity, Boolean.FALSE);

        if (log.isDebugEnabled()) {
            log.debug("findKeyDateBySupervisionId: end");
        }

        return ret;
    }

    public static SentenceKeyDateHistEntity getSentenceKeyDateLatestHistoryEntityBySupervisionId(Session session, Long supervisionId) {

        if (log.isDebugEnabled()) {
            log.debug("findKeyDateEntityBySupervisionId: begin");
        }

        if (supervisionId == null) {
            throw new InvalidInputException("supervisionId must not be null.");
        }

        Criteria c = session.createCriteria(SentenceKeyDateHistEntity.class);
        c.add(Restrictions.eq("supervisionId", supervisionId));
        c.setMaxResults(1);
        c.addOrder(Order.desc("historyId"));

        return (SentenceKeyDateHistEntity) c.uniqueResult();
    }

    public static List<SentenceKeyDateType> getSentenceKeyDateHistory(UserContext uc, SessionContext context, Session session, Long supervisionId) {

        if (log.isDebugEnabled()) {
            log.debug("getSentenceKeyDateHistory: begin");
        }

        if (supervisionId == null) {
            throw new InvalidInputException("supervisionId must not be null.");
        }

        Criteria c = session.createCriteria(SentenceKeyDateHistEntity.class);
        c.add(Restrictions.eq("supervisionId", supervisionId));
        c.addOrder(Order.desc("historyId"));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        @SuppressWarnings("unchecked") List<SentenceKeyDateHistEntity> lst = c.list();

        List<SentenceKeyDateType> ret = OrderSentenceHelper.toSentenceKeyDateTypeSet(uc, lst);

        if (log.isDebugEnabled()) {
            log.debug("getKeyDate: end");
        }

        return ret;
    }

    public static List<SentenceKeyDateType> getSentenceKeyDateHistoryBySupervisionSentenceId(UserContext uc, SessionContext context, Session session, Long supervisionId,
            Long supervisionSentenceId) {

        Criteria c = session.createCriteria(SentenceKeyDateHistEntity.class);
        c.add(Restrictions.eq("supervisionId", supervisionId));
        c.add(Restrictions.eq("supervisionSentenceId", supervisionSentenceId));
        c.addOrder(Order.desc("historyId"));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        @SuppressWarnings("unchecked") List<SentenceKeyDateHistEntity> list = c.list();

        return OrderSentenceHelper.toSentenceKeyDateTypeList(uc, list);
    }

    public static List<KeyDateType> getSentenceKeyDateHistory(UserContext uc, SessionContext context, Session session, Long supervisionId, String keyDateType) {

        if (log.isDebugEnabled()) {
            log.debug("getSentenceKeyDateHistory: begin");
        }
        if (supervisionId == null || BeanHelper.isEmpty(keyDateType)) {
            throw new InvalidInputException("supervisionId and keyDateType must not be null.");
        }

        List<KeyDateType> keyDateTypeList = new ArrayList<>();

        Criteria skd = session.createCriteria(SentenceKeyDateHistEntity.class);
        skd.add(Restrictions.eq("supervisionId", supervisionId));
        skd.setProjection(Projections.id());
        @SuppressWarnings("unchecked") Set<Long> hstIds = new HashSet<Long>(skd.list());
        if (hstIds == null || hstIds.size() == 0) {
            return keyDateTypeList;
        }

        Criteria kd = session.createCriteria(KeyDateHistEntity.class);
        kd.add(Restrictions.eq("keyDateType", keyDateType));
        kd.createCriteria("sentenceKeyDate", "skd");
        kd.add(Restrictions.in("skd.historyId", hstIds));
        kd.add(Restrictions.isNotNull("keyDateAdjust"));
        // kd.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        kd.addOrder(Order.desc("keyDateId"));

        Set<KeyDateHistEntity> encounteredSet = new HashSet<>();

        @SuppressWarnings("unchecked") List<KeyDateHistEntity> keyDateHistEntityList = kd.list();
        for (KeyDateHistEntity keyDateHistEntity : keyDateHistEntityList) {
            boolean bFirst = encounteredSet.add(keyDateHistEntity);
            if (bFirst) {
                keyDateTypeList.add(OrderSentenceHelper.toKeyDateType(uc, keyDateHistEntity));
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("getSentenceKeyDateHistory: end");
        }

        return keyDateTypeList;
    }

    public static Long deleteSentenceKeyDate(UserContext uc, SessionContext context, Session session, Long supervisionId) {

        if (log.isDebugEnabled()) {
            log.debug("deleteKeyDate: begin");
        }

        if (supervisionId == null) {
            throw new InvalidInputException("supervisionId must not be null.");
        }

        SentenceKeyDateEntity entity = BeanHelper.getEntity(session, SentenceKeyDateEntity.class, supervisionId);

        // OrderSentenceHelper.copyToHistoryKeyDate(uc, context, session, entity);

        // Change to Logical delete
        // session.delete(entity);
        // flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
        entity.setFlag(DataFlag.DELETE.value());
        session.update(entity);

        if (log.isDebugEnabled()) {
            log.debug("deleteKeyDate: end");
        }

        return SUCCESS;
    }

    public static Long daysLeftToServeForSupervision(UserContext uc, SessionContext context, Session session, Long supervisionId) {
        return OrderSentenceHelper.daysLeftToServeForSupervision(session, supervisionId);
    }

    public static SentenceKeyDateType calculateSentenceKeyDates(UserContext uc, SessionContext context, Session session, Long supervisionId, Long staffId, String comment,
            Boolean reviewRequired) {

        if (log.isDebugEnabled()) {
            log.debug("sentenceKeyDatsCalculate: begin");
        }

		/*String functionName = "sentenceKeyDatsCalculate";

		if (staffId == null || !BeanHelper.bExistedModule(session, StaffEntity.class, staffId)) {
			String message = "Invalid argument: staffId is required.";
			LogHelper.error(log, functionName, message);
			throw new InvalidInputException(message);
		}

		if (supervisionId == null || !BeanHelper.bExistedModule(session, SupervisionEntity.class, supervisionId)) {
			String message = "Invalid argument: supervisionId is required.";
			LogHelper.error(log, functionName, message);
			throw new InvalidInputException(message);
		}*/

        SentenceKeyDateType ret = null;

        ReferenceDataServiceAdapter referenceDataServiceAdapter = new ReferenceDataServiceAdapter();
        String keyDateSet = ReferenceSet.KEY_DATE.value().toUpperCase();
        List<String> keyDateTypes = referenceDataServiceAdapter.getReferenceCodesBySet(uc, keyDateSet, Boolean.TRUE);
        List<KeyDate> keyDateList = new ArrayList<>();
        if (keyDateTypes != null) {
            for (String kd : keyDateTypes) {
                KeyDate keyDate = new KeyDate();
                keyDate.setSupervisionId(supervisionId);
                keyDate.setKeyDateType(kd);
                keyDateList.add(keyDate);
            }
        }

        SentenceAdjustmentHandler sentenceAdjustmentHandler = new SentenceAdjustmentHandler(context, session);
        SentenceAdjustmentType sentenceAdjustment = sentenceAdjustmentHandler.get(uc, supervisionId);
        if (sentenceAdjustment == null) {
            sentenceAdjustment = new SentenceAdjustmentType();
            sentenceAdjustment.setSupervisionId(supervisionId);
        }

        Supervision supervision = new Supervision();
        supervision.setSupervisionId(supervisionId);
        supervision.setKeyDates(keyDateList);

        supervision.setPddFactor(3);
        supervision.setPedFactor(3);
        supervision.setPedType("F");
        supervision.setPddType("F");

        Set<AdjustmentType> supervisionAdjustments = sentenceAdjustment.getAdjustments();
        if (supervisionAdjustments != null && supervisionAdjustments.size() > 0) {
            List<Adjustment> adjustments = new ArrayList<>();
            for (AdjustmentType supAdj : supervisionAdjustments) {
                String classification = supAdj.getAdjustmentClassification();
                if (!BeanHelper.isEmpty(classification)) {
                    if (ADJUSTMENT_CLASSIFICATION.MANUAL.name().toString().equalsIgnoreCase(classification.trim())) {
                        Adjustment adj = new Adjustment();
                        adj.setAdjustmentType(supAdj.getAdjustmentType());
                        adj.setStartDate(supAdj.getStartDate());
                        if (SentenceAdjustmentHandler.ADJUSTMENT_FUNCTION.DEB.name().equalsIgnoreCase(supAdj.getAdjustmentFunction())) {
                            adj.setAdjustment(supAdj.getAdjustment());
                        } else {
                            adj.setAdjustment(supAdj.getAdjustment() == null ? 0 : (-1L) * supAdj.getAdjustment());
                        }
                        adjustments.add(adj);
                    }
                }
            }
            supervision.setAdjustments(adjustments);
        }

        List<SentenceEntity> sentences = retrieveSentenceEntitiesBySupervisionId(uc, context, session, supervisionId);
        List<Sentence> sentenceList = new ArrayList<>();
        for (SentenceEntity sentenceEntity : sentences) {
            CodeType calculationInclude = OrderSentenceHelper.getLinkCode(uc,
                    new CodeType(ReferenceSet.SENTENCE_TYPE.value().toUpperCase(), sentenceEntity.getSentenceType().toUpperCase()),
                    ReferenceSet.CALCULATION_INITIATOR.value().toUpperCase());
            if (calculationInclude == null || !CALCULATION_INITIATOR.INC.name().equalsIgnoreCase(calculationInclude.getCode())) {
                continue;
            }
            int years = 0;
            int months = 0;
            int weeks = 0;
            int days = 0;
            Set<SentenceTerm> sentenceTerms = new HashSet<>();
            Set<TermEntity> terms = sentenceEntity.getSentenceTerms();
            if (terms != null) {
                for (TermEntity term : terms) {
                    if (!term.getTermName().equalsIgnoreCase("MIN")) {
                        years = years + (term.getYears() == null ? 0 : term.getYears().intValue());
                        months = months + (term.getMonths() == null ? 0 : term.getMonths().intValue());
                        weeks = weeks + (term.getWeeks() == null ? 0 : term.getWeeks().intValue());
                        if (term.getTermStartDate() != null && term.getTermEndDate() != null) {
                            days = days + Long.valueOf(BeanHelper.getDays(term.getTermStartDate(), term.getTermEndDate())).intValue();
                        } else {
                            days = days + (term.getDays() == null ? 0 : term.getDays().intValue());
                        }
                    }
                    SentenceTerm tm = new SentenceTerm();
                    tm.setTermCategory(term.getTermCategory());
                    tm.setTermType(term.getTermType());
                    tm.setTermStatus(term.getTermStatus());
                    tm.setTermName(term.getTermName());
                    tm.setTermSequenceNo(term.getTermSequenceNo());
                    tm.setTermStartDate(term.getTermStartDate());
                    tm.setTermEndDate(term.getTermEndDate());
                    tm.setYears(term.getYears() == null ? 0L : term.getYears());
                    tm.setMonths(term.getMonths() == null ? 0L : term.getMonths());
                    tm.setWeeks(term.getWeeks() == null ? 0L : term.getWeeks());
                    tm.setDays(term.getDays() == null ? 0L : term.getDays());
                    tm.setHours(term.getHours() == null ? 0L : term.getHours());
                    sentenceTerms.add(tm);
                }
            }

            Sentence sentence = new Sentence();
            sentence.setSupervisionId(supervisionId);
            sentence.setSentenceId(sentenceEntity.getOrderId());
            sentence.setSentenceNumber(sentenceEntity.getSentenceNumber());
            sentence.setSentenceType(sentenceEntity.getSentenceType());
            sentence.setFineAmount(sentenceEntity.getFineAmount());
            if (sentenceEntity.getSentenceStartDate() != null) {
                sentence.setStartDate(sentenceEntity.getSentenceStartDate());
            } else {
                sentence.setStartDate(sentenceEntity.getOrderStartDate());
            }
            if (sentenceEntity.getFinePayments() != null && sentenceEntity.getFinePayments().size() > 0) {
                sentence.setFinePayments(OrderSentenceHelper.toFinePaymentTypeSet(sentenceEntity.getFinePayments()));
            }
            sentence.setStartOn(sentenceEntity.getOrderIssuanceDate());
            sentence.setYears(years);
            sentence.setMonths(months);
            sentence.setWeeks(weeks);
            sentence.setDays(days);
            sentence.setExpiryDate(sentenceEntity.getOrderExpirationDate());
            sentence.setConcecutiveId(sentenceEntity.getConcecutiveFrom() == null ? 0L : sentenceEntity.getConcecutiveFrom());
            sentence.setTerms(sentenceTerms);
            sentence.setAggregateby("PDD");
            Set<AdjustmentType> adjustments = sentenceAdjustment.getAdjustments();
            if (adjustments != null) {
                for (AdjustmentType adjustment : adjustments) {
                    String classification = adjustment.getAdjustmentClassification();
                    if (!BeanHelper.isEmpty(classification)) {
                        if (adjustment.getSentenceIds().contains(sentence.getSentenceId()) && ADJUSTMENT_CLASSIFICATION.MANUAL.name().toString().equalsIgnoreCase(
                                classification.trim())) {
                            //                        if (//adjustment.getSentenceIds().contains(sentence.getSentenceId()) &&
                            //							ADJUSTMENT_CLASSIFICATION.MANUAL.name().toString().equalsIgnoreCase(classification.trim())) {

                            if (sentence.getAdjustments() == null) {
                                sentence.setAdjustments(new ArrayList<Adjustment>());
                            }
                            Adjustment adj = new Adjustment();
                            adj.setAdjustmentType(adjustment.getAdjustmentType());
                            adj.setStartDate(adjustment.getStartDate());
                            if (SentenceAdjustmentHandler.ADJUSTMENT_FUNCTION.DEB.name().equalsIgnoreCase(adjustment.getAdjustmentFunction())) {
                                adj.setAdjustment(adjustment.getAdjustment());
                            } else {
                                adj.setAdjustment(adjustment.getAdjustment() == null ? 0 : (-1L) * adjustment.getAdjustment());
                            }
                            sentence.getAdjustments().add(adj);
                        }
                    }
                }
            }

            if ("INTER".equalsIgnoreCase(sentenceEntity.getSentenceType())) {
                Set<IntermittentSentenceScheduleEntity> intermittentSentenceSchedules = sentenceEntity.getIntermittentSentenceSchedules();

                IntermittentSentenceScheduleEntity lastSchedule = Collections.max(intermittentSentenceSchedules, new Comparator<IntermittentSentenceScheduleEntity>() {
                    public int compare(IntermittentSentenceScheduleEntity a1, IntermittentSentenceScheduleEntity a2) {
                        return a1.getScheduleEndDate().compareTo(a2.getScheduleEndDate());
                    }
                });

                IntermittentSentenceScheduleEntity firstSchedule = Collections.min(intermittentSentenceSchedules, new Comparator<IntermittentSentenceScheduleEntity>() {
                    public int compare(IntermittentSentenceScheduleEntity a1, IntermittentSentenceScheduleEntity a2) {
                        return a1.getScheduleEndDate().compareTo(a2.getScheduleEndDate());
                    }
                });

                sentence.setSentenceStartDate(firstSchedule.getReportingDate());
                sentence.setPrdDate(lastSchedule.getScheduleEndDate());
                sentence.setSentenceEndDate(sentenceEntity.getSentenceEndDate());
                log.info("sentenceKeyDatsCalculate: Intermittent sentence PRD  " + lastSchedule.getScheduleEndDate());
                log.info("sentenceKeyDatsCalculate: Intermittent sentence SED  " + sentenceEntity.getSentenceEndDate());
            }
            sentenceList.add(sentence);
        }
        supervision.setSentences(sentenceList);

        // Sentence Calculation
        SupervisionReturn supervisionReturn = SentenceCalculationAdapter.keyDatesCalculate(supervision);

        Supervision calculatedSupervision = supervisionReturn.getSupervision();
        List<KeyDate> cKeyDates = calculatedSupervision.getKeyDates();

        SentenceKeyDateType sentenceKeyDate = getSentenceKeyDate(uc, context, session, supervisionId);

        if (sentenceKeyDate != null) {
            Set<KeyDateType> calculatedKeyDates = new HashSet<>();
            Set<KeyDateType> keyDates = sentenceKeyDate.getKeydates();
            Set<String> KeyTypeSet = new HashSet<String>();
            for (KeyDate keyDate : cKeyDates) {
                KeyTypeSet.add(keyDate.getKeyDateType());
            }

            for (KeyDate ckd : cKeyDates) {
                for (KeyDateType kd : keyDates) {
                    if (ckd.getKeyDateType().equalsIgnoreCase(kd.getKeyDateType())) {
                        kd.setKeyDate(ckd.getKeyDate());
                        kd.setKeyDateAdjust(null);
                        kd.setKeyDateOverride(null);
                        kd.setOverrideByStaffId(null);
                        kd.setOverrideReason("");
                        calculatedKeyDates.add(kd);
                    } else if (!KeyTypeSet.contains(kd.getKeyDateType())) {
                        KeyDateType kdt = new KeyDateType();
                        kdt.setKeyDateType(ckd.getKeyDateType());
                        kdt.setKeyDate(ckd.getKeyDate());
                        calculatedKeyDates.add(kdt);
                    }
                }
            }
            sentenceKeyDate.setKeydates(calculatedKeyDates);

            CommentType commentType = sentenceKeyDate.getComment();
            if (commentType == null) {
                commentType = new CommentType();
            }
            commentType.setComment(comment);
            commentType.setCommentDate(new Date());
            commentType.setUserId(BeanHelper.getUserId(uc, context));

            sentenceKeyDate.setComment(commentType);
            sentenceKeyDate.setStaffId(staffId);
            sentenceKeyDate.setReviewRequired(reviewRequired);

            ret = updateSentenceKeyDate(uc, context, session, sentenceKeyDate);
        }

        SentenceKeyDateHistEntity keyDateHistEntity = getSentenceKeyDateLatestHistoryEntityBySupervisionId(session, ret.getSupervisionId());
        Long keyDateHistoryId = keyDateHistEntity.getHistoryId();

        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);

        // add GoodTime
        Date postedDate = new Date();
        AdjustmentType adjustment = new AdjustmentType();
        adjustment.setAdjustmentClassification(SentenceAdjustmentHandler.ADJUSTMENT_CLASSIFICATION.SYSG.name());
        adjustment.setAdjustmentType(SentenceAdjustmentHandler.ADJUSTMENT_TYPE.GT.name());
        adjustment.setAdjustmentFunction(SentenceAdjustmentHandler.ADJUSTMENT_FUNCTION.CRE.name());
        adjustment.setAdjustmentStatus(SentenceAdjustmentHandler.ADJUSTMENT_STATUS.INCL.name());
        adjustment.setApplicationType(SentenceAdjustmentHandler.ADJUSTMENT_APPLICATION.AGGSENT.name());
        if (supervisionReturn.getSupervision() != null) {
            adjustment.setAdjustment(abs(supervisionReturn.getSupervision().getGoodTime()));
        }
        adjustment.setByStaffId(staffId);
        adjustment.setPostedDate(postedDate);

        adjustment.setComment(comment);
        adjustment.setStamp(BeanHelper.toStamp(stamp));

        List<Aggregate> aggregates = supervisionReturn.getSupervision().getAggregates();
        if (aggregates != null) {
            for (Aggregate aggregate : aggregates) {
                List<Sentence> sents = aggregate.getSentences();
                if (sents != null) {
                    for (Sentence sent : sents) {
                        adjustment.getSentenceIds().add(sent.getSentenceId());
                    }
                }
            }
            sentenceAdjustment.getAdjustments().add(adjustment);
        }
        List<Sentence> sentList = supervisionReturn.getSupervision().getSentences();
        if (sentList != null && sentList.size() > 0) {
            for (Sentence sent : sentList) {
                adjustment.getSentenceIds().add(sent.getSentenceId());
            }
            sentenceAdjustment.getAdjustments().add(adjustment);
        }
        sentenceAdjustmentHandler.create(uc, sentenceAdjustment);

        // Persist Aggregate Sentences to AggregateSentenceEntity and AggregateSentenceAssocEntity
        Date calculatingDate = new Date();

        SupervisionSentenceEntity ss = null;
        Supervision sup = supervisionReturn.getSupervision();
        if (sup != null) {
            OrderSentenceHelper.setSupervisionSentenceEntityHistory(session, supervisionId);
            ss = new SupervisionSentenceEntity();
            ss.setSupervisionId(supervisionId);
            ss.setGoodTime(abs(sup.getGoodTime()));
            ss.setProbableDischargeDate(sup.getProbableDischargeDate());
            ss.setParoleEligibilityDate(sup.getParoleEligibilityDate());
            ss.setSentenceDays(sup.getSentenceDays());
            ss.setSentenceExpiryDate(sup.getSentenceExpiryDate());
            ss.setSentenceStartDate(sup.getSentenceStartDate());
            ss.setIsHistory(false);
            ss.setKeyDateHistoryId(keyDateHistoryId);
            ss.setStamp(stamp);
            Date today = new Date();
            if (ss.getSentenceStartDate() != null) {
                if (today.before(ss.getSentenceStartDate())) {
                    ss.setDaysToServe(sup.getSentenceDays());
                } else {
                    ss.setDaysToServe(sup.getSentenceDays() - BeanHelper.getDays(ss.getSentenceStartDate(), today));
                }
            }
            session.save(ss);
        }

        if (supervisionReturn.getSupervision().getAggregates() != null) {
            aggregates = supervisionReturn.getSupervision().getAggregates();
            if (aggregates != null && aggregates.size() > 0) {
                Date today = new Date();
                OrderSentenceHelper.setAggregateSentenceEntityHistory(session, supervisionId);
                for (Aggregate agg : aggregates) {
                    AggregateSentenceEntity aggSentEntity = new AggregateSentenceEntity();
                    aggSentEntity.setSupervisionId(supervisionId);
                    aggSentEntity.setbCalculateEnabled(false);
                    aggSentEntity.setStartDate(agg.getStartDate());
                    aggSentEntity.setEndDate(agg.getEndDate());
                    aggSentEntity.setPrdDate(agg.getPrdDate());
                    aggSentEntity.setGoodTime(abs(agg.getGoodTime()));
                    // aggSentEntity.setDaysToServe(agg.getDaysToServe());
                    Date prd = agg.getPrdDate();
                    if (prd.compareTo(today) >= 0) {
                        aggSentEntity.setDaysToServe(BeanHelper.getDays(today, prd));
                    }
                    aggSentEntity.setTotalSentenceDays(agg.getTotalSentenceDays());
                    aggSentEntity.setStaffId(staffId);
                    aggSentEntity.setComments(comment);
                    aggSentEntity.setCalculatingDate(calculatingDate);
                    aggSentEntity.setStamp(stamp);
                    aggSentEntity.setIsHistory(Boolean.FALSE);
                    aggSentEntity.setIsControllingSentence(agg.getControllingSentence());
                    List<Sentence> sents = agg.getSentences();
                    if (sents != null) {
                        for (Sentence sent : sents) {
                            AggregateSentenceAssocEntity sentAssoc = new AggregateSentenceAssocEntity();
                            sentAssoc.setToClass(LegalModule.ORDER_SENTENCE.value());
                            sentAssoc.setToIdentifier(sent.getSentenceId());
                            sentAssoc.setSupervisionId(supervisionId);
                            sentAssoc.setSentenceNumber(sent.getSentenceNumber());
                            sentAssoc.setIsControllingSentence(sent.getControllingSentence());
                            sentAssoc.setConcecutiveId(sent.getConcecutiveId());
                            sentAssoc.setSentenceType(sent.getSentenceType());
                            sentAssoc.setInitStartDate(sent.getStartDate());
                            sentAssoc.setStartDate(sent.getSentenceStartDate());
                            sentAssoc.setEndDate(sent.getSentenceEndDate());
                            sentAssoc.setExpiryDate(sent.getExpiryDate());
                            sentAssoc.setPrdDate(sent.getPrdDate());
                            sentAssoc.setGoodTime(abs(Long.valueOf(sent.getGoodTime())));
                            sentAssoc.setSentenceDays(Long.valueOf(sent.getSentenceDays()));
                            sentAssoc.setYears(Long.valueOf(sent.getYears()));
                            sentAssoc.setMonths(Long.valueOf(sent.getMonths()));
                            sentAssoc.setWeeks(Long.valueOf(sent.getWeeks()));
                            sentAssoc.setDays(Long.valueOf(sent.getDays()));
                            sentAssoc.setStaffId(staffId);
                            sentAssoc.setComments(comment);
                            sentAssoc.setCalculatingDate(calculatingDate);
                            sentAssoc.setStamp(stamp);
                            aggSentEntity.addSentenceAssoc(sentAssoc);
                        }
                    }
                    aggSentEntity.setKeyDateHistoryId(keyDateHistoryId);
                    aggSentEntity.setSupervisionSentence(ss);
                    session.persist(aggSentEntity);
                    ss.addAggregateSentence(aggSentEntity);
                }
            }
        } else {
            //Delete aggregates
            Long supId = supervisionReturn.getSupervision().getSupervisionId();
            String hql = "delete AggregateSentenceAssocEntity where supervisionId = :supervisionId";
            Query query = session.createQuery(hql);
            query.setParameter("supervisionId", supId);
            query.executeUpdate();

            hql = "delete AggregateSentenceEntity where supervisionId = :supervisionId and isHistory = :isHistory";
            query = session.createQuery(hql);
            query.setParameter("supervisionId", supId);
            query.setParameter("isHistory", Boolean.FALSE);
            query.executeUpdate();
        }
        if (ss != null) {
            OrderSentenceHelper.addLinkToKeyDateAndSupervisionSentence(session, supervisionId, keyDateHistEntity.getHistoryId(), ss.getSupervisionSentenceId());
        }

        OrderSentenceHelper.setSentenceCalculationEnableFlagBySupervisionId(session, supervisionId, false);

        if (log.isDebugEnabled()) {
            log.debug("sentenceKeyDatsCalculate: end");
        }

        return ret;
    }

    private static Long abs(Long goodTime) {
        long gt = 0;
        if (goodTime == null) {
            gt = 0;
        } else if (goodTime < 0L) {
            gt = (-1L) * goodTime.longValue();
        } else {
            gt = goodTime;
        }
        return gt;
    }

    public static Boolean isSentenceCalculationEnabled(UserContext uc, SessionContext context, Session session, Long supervisionId) {
        if (supervisionId == null) {
            return false;
        }
        Criteria c = session.createCriteria(AggregateSentenceEntity.class);
        c.add(Restrictions.eq("supervisionId", supervisionId));
        c.add(Restrictions.eq("isHistory", Boolean.FALSE));
        c.setProjection(Projections.property("bCalculateEnabled"));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        c.setMaxResults(1);
        Boolean bEnabled = (Boolean) c.uniqueResult();
        if (bEnabled == null) {
            // return OrderSentenceHelper.hasSentence(session, supervisionId);
            List<SentenceEntity> sentences = retrieveSentenceEntitiesBySupervisionId(uc, context, session, supervisionId);
            if (sentences != null && sentences.size() > 0) {
                return true;
            } else {
                return false;
            }
        }

        return bEnabled;
    }

    public static void setSentenceCalculationEnableFlag(Session session, Long supervisionId, Boolean calculationEnableFlag) {
        Criteria c = session.createCriteria(AggregateSentenceEntity.class);
        c.add(Restrictions.eq("supervisionId", supervisionId));
        c.add(Restrictions.eq("isHistory", Boolean.FALSE));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        @SuppressWarnings("unchecked") Iterator<AggregateSentenceEntity> it = c.list().iterator();
        while (it.hasNext()) {
            AggregateSentenceEntity entity = it.next();
            entity.setbCalculateEnabled(calculationEnableFlag);
        }
    }

    public static CodeType getDispositionStatus(UserContext uc, SessionContext context, Session session, CodeType dispositionOutcome) {

        ValidationHelper.validate(dispositionOutcome);
        CodeType outcome = new CodeType(dispositionOutcome.getSet().toUpperCase(), dispositionOutcome.getCode().toUpperCase());

        return BeanHelper.getLinkCode(uc, outcome, ReferenceSet.ORDER_STATUS.value().toUpperCase());
    }

    private static OrderConfigurationType createOrderConfiguration(UserContext uc, SessionContext context, Session session, OrderConfigurationType orderConfiguration) {

        if (log.isDebugEnabled()) {
            log.debug("createOrderConfiguration: begin");
        }

        ValidationHelper.validate(orderConfiguration);

        // OrderSentenceHelper.verifyOrderConfigurationCodeType(session, orderConfiguration);
        OrderConfigurationEntity entity = OrderSentenceHelper.toOrderConfigurationEntity(uc, context, orderConfiguration, OrderSentenceHelper.CREATE);
        if (OrderClassification.BAIL.name().equals(entity.getOrderClassification())) {
            entity.setIsHoldingOrder(false);
        } else if (OrderClassification.SENT.name().equals(entity.getOrderClassification())) {
            entity.setIsHoldingOrder(false);
        }
        entity.setStamp(BeanHelper.getCreateStamp(uc, context));
        session.persist(entity);
        OrderConfigurationType ret = OrderSentenceHelper.toOrderConfigurationType(entity);

        if (log.isDebugEnabled()) {
            log.debug("createOrderConfiguration: end");
        }

        return ret;
    }

    public static OrderConfigurationType setOrderConfiguration(UserContext uc, SessionContext context, Session session, OrderConfigurationType orderConfiguration) {

        if (log.isDebugEnabled()) {
            log.debug("setOrderConfiguration: begin");
        }

        OrderConfigurationType ret = new OrderConfigurationType();

        ValidationHelper.validate(orderConfiguration);

        OrderConfigurationEntity configedEntity = getConfigurationEntity(session, orderConfiguration.getOrderClassification(), orderConfiguration.getOrderType(),
                orderConfiguration.getOrderCategory());

        if (configedEntity == null) {
            ret = createOrderConfiguration(uc, context, session, orderConfiguration);
        } else {
            // OrderSentenceHelper.verifyOrderConfigurationCodeType(session, orderConfiguration);
            OrderConfigurationEntity entity = OrderSentenceHelper.toOrderConfigurationEntity(uc, context, orderConfiguration, OrderSentenceHelper.UPDATE);

            ValidationHelper.verifyMetaCodes(entity, true);

            entity.setOrderConfigId(configedEntity.getOrderConfigId());
            if (OrderClassification.BAIL.name().equals(entity.getOrderClassification())) {
                entity.setIsHoldingOrder(false);
            } else if (OrderClassification.SENT.name().equals(entity.getOrderClassification())) {
                entity.setIsHoldingOrder(false);
            }
            entity.setStamp(BeanHelper.getModifyStamp(uc, context, configedEntity.getStamp()));
            entity.setVersion(configedEntity.getVersion());
            entity = (OrderConfigurationEntity) session.merge(entity);
            OrderConfigurationType createdOrderConfiguration = OrderSentenceHelper.toOrderConfigurationType(entity);
            ret = createdOrderConfiguration;
        }

        if (log.isDebugEnabled()) {
            log.debug("setOrderConfiguration: end");
        }

        return ret;
    }

    public static OrderConfigurationType getOrderConfiguration(UserContext uc, SessionContext context, Session session, Long orderConfigId) {
        if (log.isDebugEnabled()) {
            log.debug("setOrderConfiguration: begin");
        }

        String functionName = "getOrderConfiguration";
        if (orderConfigId == null) {
            String message = "Order Configuration Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        OrderConfigurationEntity entity = BeanHelper.findEntity(session, OrderConfigurationEntity.class, orderConfigId);
        OrderConfigurationType ret = OrderSentenceHelper.toOrderConfigurationType(entity);
        if (log.isDebugEnabled()) {
            log.debug("setOrderConfiguration: end");
        }

        return ret;
    }

    public static OrderConfigurationType updateOrderConfiguration(@NotNull UserContext uc, SessionContext context, Session session,
            OrderConfigurationType orderConfiguration) {

        if (log.isDebugEnabled()) {
            log.debug("updateOrderConfiguration: begin");
        }
        String functionName = "updateOrderConfiguration";
        Long orderConfigId = orderConfiguration.getOrderConfigId();
        if (orderConfigId == null) {
            String message = "Order Configuration Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(orderConfiguration);

        OrderConfigurationEntity oldEntity = BeanHelper.findEntity(session, OrderConfigurationEntity.class, orderConfigId);
        if (oldEntity == null) {
            String message = "Order Configuration with ID " + orderConfigId + " does not exist.";
            LogHelper.error(log, functionName, message);
            throw new DataNotExistException(message);
        }

        OrderConfigurationEntity entity = OrderSentenceHelper.toOrderConfigurationEntity(uc, context, orderConfiguration, OrderSentenceHelper.UPDATE);
        ValidationHelper.verifyMetaCodes(entity, false);
        entity.setVersion(oldEntity.getVersion());

        session.merge(entity);
        OrderConfigurationType ret = OrderSentenceHelper.toOrderConfigurationType(entity);

        if (log.isDebugEnabled()) {
            log.debug("updateOrderConfiguration: end");
        }
        return ret;
    }

    private static OrderConfigurationEntity getConfigurationEntity(Session session, String orderClassification, String orderType, String orderCategory) {
        Criteria c = session.createCriteria(OrderConfigurationEntity.class);
        c.add(Restrictions.eq("orderClassification", orderClassification));
        c.add(Restrictions.eq("orderType", orderType));
        c.add(Restrictions.eq("orderCategory", orderCategory));
        c.setMaxResults(1);
        OrderConfigurationEntity configedEntity = (OrderConfigurationEntity) c.uniqueResult();
        return configedEntity;
    }

    public static OrderConfigurationReturnType retrieveOrderConfiguration(UserContext uc, SessionContext context, Session session,
            OrderConfigurationType orderConfiguration, Long startIndex, Long resultSize, String resultOrder) {

        if (log.isDebugEnabled()) {
            log.debug("retrieveOrderConfigurationEntity: begin");
        }

        // OrderSentenceHelper.verifyOrderConfigurationCodeType(session, orderConfiguration);
        OrderConfigurationEntity orderConfigurationEntity = OrderSentenceHelper.toOrderConfigurationEntity(uc, context, orderConfiguration, OrderSentenceHelper.READ);
        Criteria c = session.createCriteria(OrderConfigurationEntity.class);
        OrderSentenceHelper.createCriteria(uc, c, orderConfigurationEntity);

        Long totalSize = (Long) c.setProjection(Projections.countDistinct("orderConfigId")).uniqueResult();

        c.setProjection(null);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0L) {
            c.setFirstResult(startIndex.intValue());
            c.setMaxResults(resultSize.intValue());
        }

        // Search and only return the predefined max number of records
        c.setLockMode(LockMode.NONE);
        c.setCacheMode(CacheMode.IGNORE);
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = OrderSentenceHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                c.addOrder(ord);
            }
        }

        @SuppressWarnings("unchecked") List<OrderConfigurationEntity> orderConfigurationEntityList = c.list();

        OrderConfigurationReturnType ret = new OrderConfigurationReturnType();
        List<OrderConfigurationType> orderConfigurationTypes = OrderSentenceHelper.toOrderConfigurationTypeList(orderConfigurationEntityList);
        ret.setOrderConfigurations(orderConfigurationTypes);
        ret.setTotalSize(totalSize);

        if (log.isDebugEnabled()) {
            log.debug("retrieveOrderConfigurationEntity: end");
        }

        return ret;
    }

    public static Set<OrderConfigurationType> retrieveOrderConfiguration(UserContext uc, SessionContext context, Session session,
            OrderConfigurationType orderConfiguration) {

        if (log.isDebugEnabled()) {
            log.debug("retrieveOrderConfiguration: begin");
        }

        Set<OrderConfigurationEntity> orderConfigurationEntitySet = retrieveOrderConfigurationEntity(uc, context, session, orderConfiguration);
        Set<OrderConfigurationType> ret = OrderSentenceHelper.toOrderConfigurationTypeSet(orderConfigurationEntitySet);

        if (log.isDebugEnabled()) {
            log.debug("retrieveOrderConfiguration: end");
        }

        return ret;
    }

    public static OrderConfigurationEntity retrieveOrderConfigurationEntity(Session session, String orderClassification, String orderType, String orderCategory) {
        Criteria c = session.createCriteria(OrderConfigurationEntity.class);
        c.add(Restrictions.eq("orderClassification", orderClassification));
        c.add(Restrictions.eq("orderType", orderType));
        c.add(Restrictions.eq("orderCategory", orderCategory));
        c.setMaxResults(1);
        return (OrderConfigurationEntity) c.uniqueResult();
    }

    public static Set<OrderConfigurationEntity> retrieveOrderConfigurationEntity(UserContext uc, SessionContext context, Session session,
            OrderConfigurationType orderConfiguration) {

        if (log.isDebugEnabled()) {
            log.debug("retrieveOrderConfigurationEntity: begin");
        }

        Set<OrderConfigurationEntity> ret = new HashSet<OrderConfigurationEntity>();

        // OrderSentenceHelper.verifyOrderConfigurationCodeType(session, orderConfiguration);
        OrderConfigurationEntity orderConfigurationEntity = OrderSentenceHelper.toOrderConfigurationEntity(uc, context, orderConfiguration, OrderSentenceHelper.READ);
        Criteria c = session.createCriteria(OrderConfigurationEntity.class);
        OrderSentenceHelper.createCriteria(uc, c, orderConfigurationEntity);

        @SuppressWarnings("unchecked") Set<OrderConfigurationEntity> orderConfigurationEntitySet = new HashSet<OrderConfigurationEntity>(c.list());

        ret = orderConfigurationEntitySet;

        if (log.isDebugEnabled()) {
            log.debug("retrieveOrderConfigurationEntity: end");
        }

        return ret;
    }

    public static Long deleteOrderConfiguration(UserContext uc, SessionContext context, Session session, Long orderConfigId) {
        if (log.isDebugEnabled()) {
            log.debug("deleteOrderConfiguration: begin");
        }
        String functionName = "deleteOrderConfiguration";
        if (orderConfigId == null) {
            String message = "Invalid argument: Order Configuration Id is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        OrderConfigurationEntity entity = BeanHelper.findEntity(session, OrderConfigurationEntity.class, orderConfigId);
        if (entity == null) {
            return null;
        }

        session.delete(entity);

        if (log.isDebugEnabled()) {
            log.debug("deleteOrderConfiguration: end");
        }

        return SUCCESS;
    }

    public static Long deleteOrderConfiguration(UserContext uc, SessionContext context, Session session, OrderConfigurationType orderConfiguration) {

        if (log.isDebugEnabled()) {
            log.debug("deleteOrderConfiguration: begin");
        }

        Long ret = null;

        if (orderConfiguration.getOrderConfigId() != null) {
            return deleteOrderConfiguration(uc, context, session, orderConfiguration.getOrderConfigId());
        }

        Set<OrderConfigurationEntity> orderConfigurationEntitySet = retrieveOrderConfigurationEntity(uc, context, session, orderConfiguration);
        for (OrderConfigurationEntity entity : orderConfigurationEntitySet) {
            session.delete(entity);
        }
        ret = SUCCESS;

        if (log.isDebugEnabled()) {
            log.debug("deleteOrderConfiguration: end");
        }

        return ret;
    }

    public static Long deleteAllOrderConfiguration(UserContext uc, SessionContext context, Session session) {
        if (log.isDebugEnabled()) {
            log.debug("deleteAllOrderConfiguration: begin");
        }

        Long ret = null;

        Set<OrderConfigurationEntity> orderConfigurationEntitySet = retrieveOrderConfigurationEntity(uc, context, session, null);
        for (OrderConfigurationEntity entity : orderConfigurationEntitySet) {
            session.delete(entity);
        }
        ret = SUCCESS;

        if (log.isDebugEnabled()) {
            log.debug("deleteAllOrderConfiguration: end");
        }

        return ret;
    }

    public static CodeType setIntermittentDurationConfiguration(UserContext uc, SessionContext context, Session session, String intermittentDuration) {

        if (log.isDebugEnabled()) {
            log.debug("setIntermittentDurationConfig: begin");
        }

        CodeType ret = LegalHelper.setConfigEntity(uc, context, session, ReferenceSet.DURATION.value(), intermittentDuration);

        if (log.isDebugEnabled()) {
            log.debug("setIntermittentDurationConfig: end");
        }

        return ret;
    }

    public static CodeType getIntermittentDurationConfiguration(UserContext uc, SessionContext context, Session session) {

        if (log.isDebugEnabled()) {
            log.debug("getIntermittentDurationConfig: end");
        }

        CodeType ret = LegalHelper.getConfigEntity(session, ReferenceSet.DURATION.value());

        if (log.isDebugEnabled()) {
            log.debug("getIntermittentDurationConfig: end");
        }

        return ret;
    }

    public static Long deleteIntermittentDurationConfiguration(UserContext uc, SessionContext context, Session session) {
        if (log.isDebugEnabled()) {
            log.debug("deleteIntermittentDurationConfig: end");
        }

        Long ret = null;

        CodeType code = LegalHelper.setConfigEntity(uc, context, session, ReferenceSet.DURATION.value(), null);
        if (code == null) {
            return ret;
        }

        ret = ReturnCode.Success.returnCode();

        if (log.isDebugEnabled()) {
            log.debug("deleteIntermittentDurationConfig: end");
        }

        return ret;
    }

    public static Boolean setSentenceNumberConfiguration(UserContext uc, SessionContext context, Session session, Boolean bSentenceNumberGenerated) {

        if (log.isDebugEnabled()) {
            log.debug("setSentenceNumberConfiguration: begin");
        }

        Boolean ret = null;

        if (bSentenceNumberGenerated == null) {
            log.error("Invalid Argument: bSentenceNumberGenerated is required.");
            return null;
        }

        String sentenceNumberGeneration =
                bSentenceNumberGenerated ? SentenceNumberGeneration.SentenceNumberGenerated.name() : SentenceNumberGeneration.SentenceNumberNotGenerated.name();

        CodeType code = LegalHelper.setConfigEntity(uc, context, session, "bSentenceNumberGenerated", sentenceNumberGeneration, false);

        if (code != null && code.getCode() != null) {
            if (code.getCode().equals(SentenceNumberGeneration.SentenceNumberGenerated.name())) {
                ret = true;
            } else {
                ret = false;
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("setSentenceNumberConfiguration: end");
        }

        return ret;
    }

    public static Boolean getSentenceNumberConfiguration(UserContext uc, SessionContext context, Session session) {

        if (log.isDebugEnabled()) {
            log.debug("getSentenceNumberConfiguration: begin");
        }

        Boolean ret = null;

        CodeType code = LegalHelper.getConfigEntity(session, "bSentenceNumberGenerated");
        if (code != null) {
            if (SentenceNumberGeneration.SentenceNumberGenerated.name().equals(code.getCode())) {
                ret = true;
            } else {
                ret = false;
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("getSentenceNumberConfiguration: end");
        }

        return ret;
    }

    public static Long deleteSentenceNumberConfiguration(UserContext uc, SessionContext context, Session session) {

        if (log.isDebugEnabled()) {
            log.debug("deleteSentenceNumberConfiguration: begin");
        }

        Long ret = null;

        Criteria c = session.createCriteria(SentenceNumberConfigEntity.class);
        @SuppressWarnings("unchecked") Iterator<SentenceNumberConfigEntity> it = c.list().iterator();
        while (it.hasNext()) {
            SentenceNumberConfigEntity entity = it.next();
            session.delete(entity);
        }

        CodeType code = LegalHelper.setConfigEntity(uc, context, session, "bSentenceNumberGenerated", null, false);
        if (code == null) {
            return ret;
        }

        ret = SUCCESS;

        if (log.isDebugEnabled()) {
            log.debug("deleteSentenceNumberConfiguration: end");
        }

        return ret;
    }

    public static void setKeyDateViewByAggregated(UserContext uc, SessionContext context, Session session, Boolean isAggregateKeyDateView) {
        if (log.isDebugEnabled()) {
            log.debug("setKeyDateViewByAggregated: begin");
        }

        if (isAggregateKeyDateView == null) {
            log.error("Invalid Argument: bSentenceNumberGenerated is required.");
            return;
        }

        String keyDateView = isAggregateKeyDateView ? KeyDateView.AGGREGATE.name() : KeyDateView.DISAGGREGATE.name();

        LegalHelper.setConfigEntity(uc, context, session, "isAggregateKeyDateView", keyDateView, false);

        if (log.isDebugEnabled()) {
            log.debug("setKeyDateViewByAggregated: end");
        }
    }

    public static Boolean isKeyDateViewByAggregated(UserContext uc, SessionContext context, Session session) {
        if (log.isDebugEnabled()) {
            log.debug("isKeyDateViewByAggregated: begin");
        }

        Boolean ret = null;

        CodeType code = LegalHelper.getConfigEntity(session, "isAggregateKeyDateView");
        if (code != null) {
            if (KeyDateView.AGGREGATE.name().equals(code.getCode())) {
                ret = true;
            } else {
                ret = false;
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("isKeyDateViewByAggregated: end");
        }

        return ret;
    }

    public static Long deleteKeyDateViewByAggregatedConfiguration(UserContext uc, SessionContext context, Session session) {
        if (log.isDebugEnabled()) {
            log.debug("deleteKeyDateViewByAggregatedConfiguration: begin");
        }

        Long ret = null;

        Criteria c = session.createCriteria(SentenceNumberConfigEntity.class);
        @SuppressWarnings("unchecked") Iterator<SentenceNumberConfigEntity> it = c.list().iterator();
        while (it.hasNext()) {
            SentenceNumberConfigEntity entity = it.next();
            session.delete(entity);
        }

        LegalHelper.setConfigEntity(uc, context, session, "isAggregateKeyDateView", null, false);

        ret = SUCCESS;

        if (log.isDebugEnabled()) {
            log.debug("deleteKeyDateViewByAggregatedConfiguration: end");
        }

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public static Long updateOrderDataSecurityStatus(UserContext uc, SessionContext context, Session session, Long orderId, Long dataSecurityFlag, String comments,
            Long personId, String parentEntityType, Long parentId, String memento, Boolean cascade) {

        Long ret = ReturnCode.UnknownError.returnCode();

        // id can not be null.
        if (orderId == null || personId == null || dataSecurityFlag == null) {
            String message = "orderId or personId or dataSecurityFlag can not be null";
            throw new InvalidInputException(message);
        }

        if (dataSecurityFlag.equals(DataFlag.SEAL.value())) {
            ret = sealOrder(uc, context, session, orderId, comments, personId, parentEntityType, parentId, memento, EntityTypeEnum.ORDER.code(), cascade);
        } else if (dataSecurityFlag.equals(DataFlag.ACTIVE.value())) {
            ret = UnSealOrder(uc, context, session, orderId, comments, personId, parentEntityType, parentId, memento, EntityTypeEnum.ORDER.code(), cascade);
        }

        return ret;

    }

    public static Long updateSentenceDataSecurityStatus(UserContext uc, SessionContext context, Session session, Long sentId, Long dataSecurityFlag, String comments,
            Long personId, String parentEntityType, Long parentId, String memento, Boolean cascade) {

        Long ret = ReturnCode.UnknownError.returnCode();

        // id can not be null.
        if (sentId == null || personId == null || dataSecurityFlag == null) {
            String message = "orderId or personId or dataSecurityFlag can not be null";
            throw new InvalidInputException(message);
        }

        if (dataSecurityFlag.equals(DataFlag.SEAL.value())) {
            ret = sealOrder(uc, context, session, sentId, comments, personId, parentEntityType, parentId, memento, EntityTypeEnum.SENTENCE.code(), cascade);
        } else if (dataSecurityFlag.equals(DataFlag.ACTIVE.value())) {
            ret = UnSealOrder(uc, context, session, sentId, comments, personId, parentEntityType, parentId, memento, EntityTypeEnum.SENTENCE.code(), cascade);
        }

        return ret;

    }

    private static Long sealOrder(UserContext uc, SessionContext context, Session session, Long orderId, String comments, Long personId, String parentEntityType,
            Long parentId, String memento, String entityType, Boolean cascade) {

        Long dataSecurityFlag = DataFlag.SEAL.value();
        OrderEntity entity = (OrderEntity) session.get(OrderEntity.class, orderId);
        if (entity == null) {
            return ReturnCode.CInvalidInput2002.returnCode();
        }
        log.info(entity.toString());

        Date recordDate = new Date();
        String recordType = DataSecurityRecordTypeEnum.RELATION.code();

        // 2- Set the Status on the ALL Associations of type ORDER and CONDITION
        //    of the Charge
        List<DataSecurityRelType> orderAssosciations = getOrderAssociations(entity);

        if (cascade) {

            if (entityType.equalsIgnoreCase(EntityTypeEnum.SENTENCE.code())) {
                recordType = DataSecurityRecordTypeEnum.SEALSENTENCE.code();
                List<Long> consecutiveSentences = new ArrayList<Long>();
                try {
                    consecutiveSentences = getConsecutiveSentences(uc, context, session, orderId);
                } catch (Exception ex) {
                    log.error("Can not get consecutive sentences", ex);
                }
                for (Long sentId : consecutiveSentences) {
                    SentenceType sentence = (SentenceType) getOrder(uc, context, session, sentId);
                    sentence.setConcecutiveFrom(null);
                    updateSentence(uc, context, session, sentence);
                }
            } else {
                recordType = DataSecurityRecordTypeEnum.SEALORDER.code();
            }

            //orderAssosciations has only child associations as conditions, conditions do not have any related or childs
            for (DataSecurityRelType dsRel : orderAssosciations) {

                String assParentEntityType = EntityTypeEnum.ORDER.code();

                // update flag of this Condition
                if (dsRel.getEntityType().equals(EntityTypeEnum.CONDITION)) {
                    LegalServiceAdapter.updateConditionDataSecurityStatus(uc, dsRel.getId(), dataSecurityFlag, comments, personId, entityType, entity.getOrderId(),
                            dsRel.getMemnto());
                }

                if (dsRel.getEntityType().equals(EntityTypeEnum.CASE)) {
                    CaseInfoEntity ee = (CaseInfoEntity) session.get(CaseInfoEntity.class, dsRel.getId());
                    if (ee != null) {
                        LegalServiceAdapter.updateCaseDataSecurityStatus(uc, dsRel.getId(), dataSecurityFlag, comments, personId, entityType, entity.getOrderId(),
                                dsRel.getMemnto(), Boolean.FALSE);
                        // remove this order links from the case
                        LegalHelper.removeModuleLinks(uc, context, session, dsRel.getId(), LegalModule.CASE_INFORMATION, entity.getModuleAssociations());
                    }
                }

                // Some other chrage not current parent
                if (dsRel.getEntityType().equals(EntityTypeEnum.CHARGE) && !dsRel.getId().equals(parentId)) {
                    LegalServiceAdapter.updateChargeDataSecurityStatus(uc, dsRel.getId(), dataSecurityFlag, comments, personId, entityType, entity.getOrderId(),
                            dsRel.getMemnto(), Boolean.FALSE);
                }

                session.evict(entity);
                entity = (OrderEntity) session.get(OrderEntity.class, orderId);
                // remove this
                LegalHelper.removeModuleLinks(uc, context, session, orderId, LegalModule.ORDER_SENTENCE, entity.getModuleAssociations());
                entity.getModuleAssociations().clear();
                // remove other

                //LegalHelper.removeModuleLinks(uc, context, session, orderId, LegalModule.CONDITION, entity.getModuleAssociations());
                //LegalHelper.removeModuleLinks(uc, context, session, orderId, LegalModule.CASE_INFORMATION, entity.getModuleAssociations());
                //em.remove(em.contains(entity) ? entity : em.merge(entity));
                session.merge(entity);
                session.flush();
                /*
                if(dsRel.getEntityType().equals(EntityTypeEnum.ORDER)){
				    legalAdapter.updateOrderDataSecurityStatus(uc, dsRel.getId(), dataSecurityFlag, comments, personId, entityType, entity.getOrderId(), dsRel.getMemnto());
				}*/
            }
        }
        DataSecurityRecordType dsr = new DataSecurityRecordType(null, entity.getOrderId(), entityType, parentId, parentEntityType, recordType, personId, recordDate,
                comments, memento);
        DataSecurityServiceAdapter.createRecord(uc, dsr);
        if (cascade) {
            entity.setFlag(dataSecurityFlag);
        }
        session.merge(entity);
        session.flush();
        session.clear();
        return ReturnCode.Success.returnCode();
    }

    private static Long UnSealOrder(UserContext uc, SessionContext context, Session session, Long orderId, String comments, Long personId, String parentEntityType,
            Long parentId, String memento, String entityType, Boolean cascade) {
        // 2- Set the Status on the Parent Charge
        String recordType = DataSecurityRecordTypeEnum.UNSEALORDER.code();
        if (entityType.equalsIgnoreCase(EntityTypeEnum.SENTENCE.code())) {
            recordType = DataSecurityRecordTypeEnum.UNSEALSENTENCE.code();
        }

        List<DataSecurityRecordType> relatedRecords = DataSecurityServiceAdapter.getRelatedRecords(uc, orderId, entityType);

        // get order
        DataSecurityRecordType sealedOrder = DataSecurityServiceAdapter.getEntity(uc, entityType, orderId, null, null);

        if (sealedOrder == null)// order is already unsealed
        {
            return ReturnCode.Success.returnCode();
        }

        Long dataSecurityFlag = DataFlag.ACTIVE.value();

        Date recordDate = new Date();

        String chrgeTblName = "leg_ordorder";
        String sql = "select orderid, flag from " + chrgeTblName + " where orderid = :orderIdParam";
        String usql = "update " + chrgeTblName + " set flag = 1 where orderid = :orderIdParam";
        Map<String, String> paramMap = new HashMap<String, String>();
        Map<String, Type> typesMap = new HashMap<String, Type>();

        // update charge status
        paramMap.put("orderIdParam", String.valueOf(sealedOrder.getEntityId()));
        typesMap.put("orderid", LongType.INSTANCE);
        typesMap.put("flag", LongType.INSTANCE);
        List entitys = DataSecurityHelper.executeRawSQLQuery(session, sql, paramMap, typesMap);

        Long id = null;
        Long flag = null;

        // There should be only one result in entitys
        for (int i = 0; i < entitys.size(); i++) {
            Object[] o = (Object[]) entitys.get(i);
            id = DataSecurityHelper.getLongValue(o[0]);
            flag = DataSecurityHelper.getLongValue(o[1]);
        }

        paramMap.clear();
        typesMap.clear();
        paramMap.put("orderIdParam", String.valueOf(id));
        typesMap.put("orderid", LongType.INSTANCE);
        DataSecurityHelper.executeRawSQLQuery(session, usql, paramMap, typesMap);

        session.flush();
        session.clear();

        OrderEntity entity = (OrderEntity) session.get(OrderEntity.class, orderId);
        Set<OrderModuleAssociationEntity> moduleAssociations = saveOrderAssociations(relatedRecords, entity, parentId, parentEntityType);

        for (DataSecurityRecordType rec : relatedRecords) {
            String assEntityType = rec.getEntityType();
            String assParentEntityType = entityType;

            if (rec.getEntityType().equals(EntityTypeEnum.CONDITION.code())) {
                LegalServiceAdapter.updateConditionDataSecurityStatus(uc, rec.getEntityId(), dataSecurityFlag, comments, personId, assParentEntityType, orderId,
                        rec.getMemento());
            }
			/*Order can not have circular dependeny to charge or order , when sealing
			 if (rec.getEntityType().equals(EntityTypeEnum.ORDER.code())) {
				LegalServiceAdapter.updateOrderDataSecurityStatus(uc, rec.getEntityId(), dataSecurityFlag,
						comments, personId, assParentEntityType, orderId, rec.getMemento());
			}
			if (rec.getEntityType().equals(EntityTypeEnum.CHARGE.code())) {
				LegalServiceAdapter.updateChargeDataSecurityStatus(uc, rec.getEntityId(), dataSecurityFlag,
						comments, personId, assParentEntityType, orderId, rec.getMemento());
			}*/
        }
        session.evict(entity);
        entity = (OrderEntity) session.get(OrderEntity.class, orderId);

        List<DataSecurityRecordType> reverserelatedRecords = DataSecurityServiceAdapter.getReverseRelatedRecords(uc, orderId, entityType);

        if (reverserelatedRecords.size() > 0) {
            Set<OrderModuleAssociationEntity> reverseAssociations = getReverseOrderAssociations(reverserelatedRecords, entity);
            Boolean isValid = DataSecurityHelper.verifyModuleLinks(uc, context, session, orderId, LegalModule.ORDER_SENTENCE, reverseAssociations);
            if (!isValid) {
                throw new ArbutusRuntimeException(ErrorCodes.LEG_RELATED_NOT_FOUND);
            }
        }

        if (moduleAssociations.size() > 0) {
            entity.getModuleAssociations().addAll(moduleAssociations);
            LegalHelper.createModuleLinks(uc, context, session, orderId, LegalModule.ORDER_SENTENCE, entity.getModuleAssociations(), entity.getStamp());
        }

        DataSecurityRecordType dsr = new DataSecurityRecordType(null, orderId, entityType, parentId, parentEntityType, recordType, personId, recordDate, comments,
                memento);
        dsr.setRelatedRecordId(sealedOrder.getDataSecurityRecordId());
        dsr = DataSecurityServiceAdapter.createRecord(uc, dsr);
        sealedOrder.setRelatedRecordId(dsr.getDataSecurityRecordId());
        sealedOrder = DataSecurityServiceAdapter.updateRecord(uc, sealedOrder);

        // set related to
        for (DataSecurityRecordType rel : relatedRecords) {
            DataSecurityRecordType ee = DataSecurityServiceAdapter.get(uc, rel.getDataSecurityRecordId());
            ee.setRelatedRecordId(dsr.getDataSecurityRecordId());
            ee = DataSecurityServiceAdapter.updateRecord(uc, ee);
        }

        // should be good as new
        session.merge(entity);
        session.flush();
        session.clear();

        return ReturnCode.Success.returnCode();
    }

    // equal to
    private static List<DataSecurityRelType> getOrderAssociations(OrderEntity entity) {

        List<DataSecurityRelType> orderAssociations = new ArrayList<DataSecurityRelType>();

        Set<OrderModuleAssociationEntity> moduleAssociations = entity.getModuleAssociations();
        for (OrderModuleAssociationEntity assoc : moduleAssociations) {
            if (assoc.getToClass().equalsIgnoreCase(LegalModule.CONDITION.value()) && assoc.getToIdentifier() != null) {
                DataSecurityRelType dsRel = new DataSecurityRelType(EntityTypeEnum.CONDITION, assoc.getToIdentifier());
                orderAssociations.add(dsRel);
            }

            if (assoc.getToClass().equalsIgnoreCase(LegalModule.ORDER_SENTENCE.value()) && assoc.getToIdentifier() != null) {
                DataSecurityRelType dsRel = new DataSecurityRelType(EntityTypeEnum.ORDER, assoc.getToIdentifier());
                orderAssociations.add(dsRel);
            }

            if (assoc.getToClass().equalsIgnoreCase(LegalModule.CHARGE.value()) && assoc.getToIdentifier() != null) {
                DataSecurityRelType dsRel = new DataSecurityRelType(EntityTypeEnum.CHARGE, assoc.getToIdentifier());
                orderAssociations.add(dsRel);
            }

            if (assoc.getToClass().equalsIgnoreCase(LegalModule.CASE_INFORMATION.value()) && assoc.getToIdentifier() != null) {
                DataSecurityRelType dsRel = new DataSecurityRelType(EntityTypeEnum.CASE, assoc.getToIdentifier());
                orderAssociations.add(dsRel);
            }

        }

        return orderAssociations;

    }

    private static Set<OrderModuleAssociationEntity> saveOrderAssociations(List<DataSecurityRecordType> relatedSealed, OrderEntity entity, Long parentId,
            String parentEntityType) {
        Set<OrderModuleAssociationEntity> moduleAssociations = new HashSet<OrderModuleAssociationEntity>();
        for (DataSecurityRecordType rel : relatedSealed) {

            if (rel.getEntityType().equals(EntityTypeEnum.CONDITION.code())) {
                OrderModuleAssociationEntity asscEntity = new OrderModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.CONDITION.value());
                asscEntity.setToIdentifier(rel.getEntityId());
                asscEntity.setOrder(entity);
                moduleAssociations.add(asscEntity);
            }

            if (rel.getEntityType().equals(EntityTypeEnum.CASE.code())) {
                OrderModuleAssociationEntity asscEntity = new OrderModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.CASE_INFORMATION.value());
                asscEntity.setToIdentifier(rel.getEntityId());
                asscEntity.setOrder(entity);
                Boolean found = Boolean.FALSE;
                for (OrderModuleAssociationEntity m : entity.getModuleAssociations()) {
                    if (m.getToClass().equals(asscEntity.getToClass()) && m.getToIdentifier().equals(asscEntity.getToIdentifier())) {
                        found = Boolean.TRUE;
                    }
                }
                if (!found) {
                    moduleAssociations.add(asscEntity);
                }
            }

            // Not this charge but some other
            if (rel.getEntityType().equals(EntityTypeEnum.CHARGE.code()) && !rel.getEntityId().equals(parentId)) {
                OrderModuleAssociationEntity asscEntity = new OrderModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.CHARGE.value());
                asscEntity.setToIdentifier(rel.getEntityId());
                asscEntity.setOrder(entity);
                moduleAssociations.add(asscEntity);
            }

            if (rel.getParentEntityType().equals(EntityTypeEnum.CHARGE.code())) {
                OrderModuleAssociationEntity asscEntity = new OrderModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.CHARGE.value());
                asscEntity.setToIdentifier(rel.getEntityId());
                asscEntity.setOrder(entity);
                moduleAssociations.add(asscEntity);
            }
        }

        return moduleAssociations;
    }

    private static Set<OrderModuleAssociationEntity> getReverseOrderAssociations(List<DataSecurityRecordType> relatedSealed, OrderEntity entity) {
        Set<OrderModuleAssociationEntity> moduleAssociations = new HashSet<OrderModuleAssociationEntity>();
        for (DataSecurityRecordType rel : relatedSealed) {

            if (rel.getParentEntityType().equals(EntityTypeEnum.CHARGE.code()) && rel.getEntityType().equals(EntityTypeEnum.SENTENCE.code())) {
                OrderModuleAssociationEntity asscEntity = new OrderModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.CHARGE.value());
                asscEntity.setToIdentifier(rel.getParentEntityId());
                asscEntity.setOrder(entity);
                moduleAssociations.add(asscEntity);
            }

            if (rel.getParentEntityType().equals(EntityTypeEnum.CHARGE.code()) && rel.getEntityType().equals(EntityTypeEnum.ORDER.code())) {
                OrderModuleAssociationEntity asscEntity = new OrderModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.CHARGE.value());
                asscEntity.setToIdentifier(rel.getParentEntityId());
                asscEntity.setOrder(entity);
                moduleAssociations.add(asscEntity);
            }

            if (rel.getParentEntityType().equals(EntityTypeEnum.CHARGE.code()) && rel.getEntityType().equals(EntityTypeEnum.SENTENCE.code())) {
                OrderModuleAssociationEntity asscEntity = new OrderModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.CHARGE.value());
                asscEntity.setToIdentifier(rel.getParentEntityId());
                asscEntity.setOrder(entity);
                moduleAssociations.add(asscEntity);
            }

            if (rel.getParentEntityType().equals(EntityTypeEnum.CHARGE.code()) && rel.getEntityType().equals(EntityTypeEnum.ORDER.code())) {
                OrderModuleAssociationEntity asscEntity = new OrderModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.CHARGE.value());
                asscEntity.setToIdentifier(rel.getParentEntityId());
                asscEntity.setOrder(entity);
                moduleAssociations.add(asscEntity);
            }

            if (rel.getParentEntityType().equals(EntityTypeEnum.CASE.code()) && rel.getEntityType().equals(EntityTypeEnum.ORDER.code())) {
                OrderModuleAssociationEntity asscEntity = new OrderModuleAssociationEntity();
                asscEntity.setToClass(LegalModule.CASE_INFORMATION.value());
                asscEntity.setToIdentifier(rel.getParentEntityId());
                asscEntity.setOrder(entity);
                moduleAssociations.add(asscEntity);
            }

        }

        return moduleAssociations;
    }

    private static void setSentenceStatusForCaseInfo(UserContext uc, SessionContext context, Session session, Set<Long> caseInfoIds, Long orderId, boolean bSentenced) {
        for (Long caseInfoId : caseInfoIds) {
            CaseInfoEntity caseInfo = BeanHelper.getEntity(session, CaseInfoEntity.class, caseInfoId);
            if (bSentenced) {
                caseInfo.setSentenceStatus(SentenceStatus.SENTENCED.name());
            } else {
                caseInfo.setSentenceStatus(SentenceStatus.UNSENTENCED.name());
            }
        }
    }

    public static List<Long> getConsecutiveSentences(UserContext uc, SessionContext context, Session session, Long sentenceId) {
        return OrderSentenceHelper.getConsecutiveSentences(session, sentenceId);
    }

    /**
     * @param schedulePatternList
     * @return SortedSet<IntermittentScheduleType>
     */
    private static SortedSet<IntermittentScheduleType> sortSchedulePattern(Set<IntermittentScheduleType> schedulePatternList) {

        SortedSet<IntermittentScheduleType> schdulePatternListSorted = new TreeSet<>(new Comparator<IntermittentScheduleType>() {

            @Override
            public int compare(IntermittentScheduleType intermSchType1, IntermittentScheduleType intermSchType2) {
                if (intermSchType1.getDayOfWeekAsInt() == intermSchType2.getDayOfWeekAsInt()) {
                    if (intermSchType1.getStartTime().hour() != intermSchType2.getStartTime().hour()) {
                        return (int) (intermSchType1.getStartTime().hour() - intermSchType2.getStartTime().hour());
                    } else if (intermSchType1.getStartTime().minute() != intermSchType2.getStartTime().minute()) {
                        return (int) (intermSchType1.getStartTime().minute() - intermSchType2.getStartTime().minute());
                    } else {
                        return (int) (intermSchType1.getStartTime().second() - intermSchType2.getStartTime().second());
                    }
                } else {

                    return intermSchType1.getDayOfWeekAsInt() - intermSchType2.getDayOfWeekAsInt();
                }

            }
        });

        schdulePatternListSorted.addAll(schedulePatternList);

        return schdulePatternListSorted;
    }

    /**
     * @param uc
     * @param context
     * @param session
     * @return Supervision
     */
    public static Supervision getIntermittentSentenceWithGuvRule(UserContext uc, SessionContext context, Session session, Long supervisionId, SentenceType sentenceType) {

        List<KeyDate> keyDateList = new ArrayList<>();

        SentenceAdjustmentType sentenceAdjustment = null;
        if (sentenceAdjustment == null) {
            sentenceAdjustment = new SentenceAdjustmentType();
            sentenceAdjustment.setSupervisionId(supervisionId);
        }

        Supervision supervision = new Supervision();
        supervision.setSupervisionId(supervisionId);
        supervision.setKeyDates(keyDateList);

        supervision.setPddFactor(3);
        supervision.setPedFactor(3);
        supervision.setPedType("F");
        supervision.setPddType("F");
        supervision.setAdjustments(new ArrayList<Adjustment>());

        Sentence sentence = new Sentence();
        List<Sentence> sentenceList = new ArrayList<>();
        int years = 0;
        int months = 0;
        int weeks = 0;
        int days = 0;
        Set<SentenceTerm> sentenceTerms = new HashSet<>();
        Set<TermType> terms = sentenceType.getSentenceTerms();
        if (terms != null) {
            for (TermType term : terms) {
                if (!term.getTermName().equalsIgnoreCase("MIN")) {
                    years = years + (term.getYears() == null ? 0 : term.getYears().intValue());
                    months = months + (term.getMonths() == null ? 0 : term.getMonths().intValue());
                    weeks = weeks + (term.getWeeks() == null ? 0 : term.getWeeks().intValue());
                    if (term.getTermStartDate() != null && term.getTermEndDate() != null) {
                        days = days + Long.valueOf(BeanHelper.getDays(term.getTermStartDate(), term.getTermEndDate())).intValue();
                    } else {
                        days = days + (term.getDays() == null ? 0 : term.getDays().intValue());
                    }
                }
                SentenceTerm tm = new SentenceTerm();
                tm.setTermCategory(term.getTermCategory());
                tm.setTermType(term.getTermType());
                tm.setTermStatus(term.getTermStatus());
                tm.setTermName(term.getTermName());
                tm.setTermSequenceNo(term.getTermSequenceNo());
                tm.setTermStartDate(term.getTermStartDate());
                tm.setTermEndDate(term.getTermEndDate());
                tm.setYears(term.getYears() == null ? 0L : term.getYears());
                tm.setMonths(term.getMonths() == null ? 0L : term.getMonths());
                tm.setWeeks(term.getWeeks() == null ? 0L : term.getWeeks());
                tm.setDays(term.getDays() == null ? 0L : term.getDays());
                tm.setHours(term.getHours() == null ? 0L : term.getHours());
                sentenceTerms.add(tm);
            }
        }

        sentence.setSupervisionId(supervisionId);
        sentence.setSentenceId(-100l);
        sentence.setSentenceNumber(sentenceType.getSentenceNumber());
        sentence.setSentenceType(sentenceType.getSentenceType());
        if (sentenceType.getSentenceStartDate() != null) {
            sentence.setStartDate(sentenceType.getSentenceStartDate());
        } else {
            sentence.setStartDate(sentenceType.getOrderStartDate());
        }
        sentence.setStartOn(sentenceType.getOrderIssuanceDate());
        sentence.setYears(years);
        sentence.setMonths(months);
        sentence.setWeeks(weeks);
        sentence.setDays(days);
        sentence.setExpiryDate(sentenceType.getOrderExpirationDate());
        sentence.setConcecutiveId(sentenceType.getConcecutiveFrom() == null ? 0L : sentenceType.getConcecutiveFrom());
        sentence.setTerms(sentenceTerms);
        sentence.setAggregateby("PDD");
        sentenceList.add(sentence);

        supervision.setSentences(sentenceList);

        // Sentence Calculation
        Supervision supervisionRet = SentenceCalculationAdapter.setUpRuleForIntermSentence(supervision);
        du = new DateUtil(supervisionRet.getCalendarYearDays(), supervisionRet.getCalendarMonthDays(), supervisionRet.getCalendarWeekDays());
        sentenceList = timeLineSentences(supervisionRet.getSentences());
        supervisionRet.setSentences(sentenceList);

        return supervisionRet;
    }

    private static List<Sentence> timeLineSentences(List<Sentence> sentences) {

        String functionName = "timeLineSentences";
        Map<Long, Sentence> map = new HashMap<Long, Sentence>();

        for (Sentence s2 : sentences) {
            if (s2.getStartDate() == null) {
                String message = "Invalid argument: sentence start date can not be null.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }

            map.put(s2.getSentenceNumber(), s2);
        }

        for (Sentence s : map.values()) {
            do {
                for (Sentence s1 : map.values()) {
                    Date sentenceStartDate = null;

                    if (s1.getConcecutiveId() != null && s1.getConcecutiveId() > 0) {
                        Sentence s2 = map.get(s1.getConcecutiveId());
                        if (s2 != null && s2.getSentenceEndDate() != null) {
                            sentenceStartDate = du.nextDay(s2.getSentenceEndDate());
                        }
                    } else {
                        sentenceStartDate = s1.getStartDate();
                    }

                    if (sentenceStartDate != null) {
                        s1.setSentenceStartDate(sentenceStartDate);
                        Set<SentenceTerm> terms = s1.getTerms();
                        Date sentenceEndDate = sentenceStartDate;
                        for (SentenceTerm term : terms) {
                            if (term.getTermCategory().equals(CUSTODY) && term.getTermName().equals(TERM_NAME_MAX)) {
                                sentenceEndDate = du.findSentenceEndDate(sentenceStartDate, term.getYears().intValue(), term.getMonths().intValue(), 0,
                                        term.getDays().intValue(), term.getHours().intValue());
                                break;
                            }
                        }
                        s1.setSentenceEndDate(sentenceEndDate);
                        int sentenceDays = du.daysBetween(sentenceStartDate, sentenceEndDate);
                        s1.setSentenceDays(sentenceDays);
                        //log.info("isCalulateGoodTime(): "+s1.getReceiveGoodTime() + " GoodTime");
                        int goodTime = getGoodTimeDays(sentenceDays, s1);
                        s1.setGoodTime(goodTime);
                        Date pddDate = du.addDays(sentenceStartDate, sentenceDays - goodTime);
                        s1.setPrdDate(pddDate);
                        applySentenceAdjustments(s1);
                        map.remove(s1);
                        map.put(s1.getSentenceNumber(), s1);
                    }
                }
            } while (s.getSentenceStartDate() == null);
        }

        sentences.clear();
        sentences.addAll(map.values());
        return sentences;
    }

    /**
     * @param sentence
     * @return Sentence
     */
    private static Sentence applySentenceAdjustments(Sentence sentence) {
        if (sentence.getAdjustments() == null || sentence.getAdjustments().isEmpty()) {
            return sentence;
        }
        for (Adjustment adjustment : sentence.getAdjustments()) {
            if (adjustment.getAdjustment() != null && adjustment.getAdjustment() != 0) {
                int adjustedDays = adjustment.getAdjustment().intValue() + 1;

                if ((sentence.getSentenceStartDate().compareTo(adjustment.getStartDate()) >= 0) && (adjustment.getAdjustmentType().equals(JAIL_TIME))) {
                    Date tempEndDate = du.addDays(sentence.getSentenceEndDate(), adjustedDays);
                    int sentenceDays = du.daysBetween(sentence.getSentenceStartDate(), tempEndDate);
                    int goodTime = getGoodTimeDays(sentenceDays, sentence);
                    Date prdDate = du.addDays(sentence.getSentenceStartDate(), sentenceDays - goodTime);
                    sentence.setPrdDate(prdDate);
                } else {
                    if (sentence.getPrdDate().compareTo(adjustment.getStartDate()) >= 0) {
                        sentence.setPrdDate(du.addDays(sentence.getPrdDate(), adjustedDays));
                        if (sentence.getPrdDate().before(sentence.getSentenceStartDate())) {
                            sentence.setPrdDate(sentence.getSentenceStartDate());
                        }
                    }
                }
            }
        }
        return sentence;
    }

    /**
     * @param sentenceDays
     * @param sentence
     * @return int
     */
    private static int getGoodTimeDays(int sentenceDays, Sentence sentence) {
        int goodTime = 0;
        if ("true".equalsIgnoreCase(sentence.getReceiveGoodTime())) {
            if ("F".equals(sentence.getGoodTimeType())) {
                goodTime = sentenceDays * 1 / sentence.getGoodTimeFactorInterm();
            } else if ("P".equals(sentence.getGoodTimeType())) {
                goodTime = sentenceDays * sentence.getGoodTimeFactorInterm() / 100;
            }
        }

        return goodTime;
    }

    /**
     * This method returns the schedule for Intermittent sentence
     *
     * @param uc           UserContext
     * @param context      SessionContext
     * @param session      Session
     * @param sentenceType SentenceType
     * @param orderId      Long
     * @return List<IntermittentSentenceScheduleType>
     */
    private static List<IntermittentSentenceScheduleType> getIntermittentSentScheduleList(UserContext uc, SessionContext context, Session session,
            SentenceType sentenceType, Long orderId) {

        Date reportingDate = null;

        SortedSet<IntermittentScheduleType> schedulePatternList = sortSchedulePattern(sentenceType.getIntermittentSchedule());

        for (IntermittentScheduleType s : schedulePatternList) {
            reportingDate = s.getReportingDate();
            break;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(reportingDate);

        Supervision supervision = getIntermittentSentenceWithGuvRule(uc, context, session, sentenceType.getSupervisionId(), sentenceType);
        List<Sentence> sentences = supervision.getSentences();
        int count = 0;
        int sentenceDays = 0;
        Sentence sentenceWithGuvRule = null;
        int sentenceDaysWithoutAdj = 0;
        for (Sentence sentence : sentences) {
            sentenceDaysWithoutAdj = sentenceDays + sentence.getSentenceDays();
            sentenceDays = sentenceDays + du.daysBetween(sentence.getSentenceStartDate(), sentence.getPrdDate());
            log.debug("**************   ############     sentenceDays b/w start and prd         " + sentenceDays);
            if (!sentence.getStartDate().equals(reportingDate)) {
                sentenceDays = sentenceDays - sentence.getNoOfDaysWhenRDaySameAsSDay();
            }

            if (log.isDebugEnabled()) {
                log.debug("**************   ############     sentenceDays          " + sentenceDays);
            }
            sentenceWithGuvRule = sentence;
            break;
        }

        int daysForConsecutiveStay = sentenceWithGuvRule.getNoOfDaysForConsecutiveStay();
        int consecutiveStay = sentenceWithGuvRule.getConsecutiveStay();
        String quantifier = sentenceWithGuvRule.getComparisionQuantifier() != null ? sentenceWithGuvRule.getComparisionQuantifier() : "E";

        ActivityService service = null;
        if (service == null) {
            service = (ActivityService) ServiceAdapter.JNDILookUp(service, ActivityService.class);
        }
        Map<Long, IntermittentScheduleType> schedulePatternMap = new HashMap<Long, IntermittentScheduleType>();
        List<List<ScheduleTimeslotType>> schedulesList = new ArrayList<List<ScheduleTimeslotType>>();
        for (IntermittentScheduleType s : schedulePatternList) {
            Schedule sch = new Schedule();
            sch.setScheduleCategory(ActivityCategory.CASE.value());
            sch.setFacilityId(s.getLocationId());
            sch.setCapacity(20000L);
            sch.setEffectiveDate(BeanHelper.getDateWithoutTime(s.getReportingDate()));

            sch.setScheduleStartTime(s.getStartTimeAsHourMin());

            sch.setRecurrencePattern(new WeeklyRecurrencePatternType(1L, new HashSet<String>(Arrays.asList(new String[] { s.getDayOfWeek() }))));

            Schedule schRet = service.createSchedule(uc, sch);

            schedulePatternMap.put(schRet.getScheduleIdentification(), s);
            Date startDate = BeanHelper.getDateWithoutTime(s.getReportingDate());
            Calendar endDateAsCal = Calendar.getInstance();
            endDateAsCal.setTime(startDate);
            endDateAsCal.add(Calendar.DATE, ((sentenceDays * 8) < sentenceDaysWithoutAdj ? (sentenceDaysWithoutAdj * 8) : (sentenceDays * 8)));
            Date endDate = BeanHelper.getDateWithoutTime(endDateAsCal.getTime());

            List<ScheduleTimeslotType> timeslotsRet = service.generateTimeslotList(uc, schRet.getScheduleIdentification(), startDate, endDate, null);

            Collections.sort(timeslotsRet, new Comparator<ScheduleTimeslotType>() {

                @Override
                public int compare(ScheduleTimeslotType o1, ScheduleTimeslotType o2) {
                    return o1.getTimeslotStartDateTime().compareTo(o2.getTimeslotStartDateTime());
                }
            });

            ScheduleTimeslotType firstScheduleLot = timeslotsRet.get(0);
            cal.setTime(firstScheduleLot.getTimeslotStartDateTime());
            String firstScheduleDay = days[cal.get(Calendar.DAY_OF_WEEK) - 1];

            if (firstScheduleDay != null && !firstScheduleDay.equalsIgnoreCase(s.getDayOfWeek())) {
                timeslotsRet.remove(0);
            }
            schedulesList.add(timeslotsRet);

        }

        Collections.sort(schedulesList, new Comparator<List<ScheduleTimeslotType>>() {

            @Override
            public int compare(List<ScheduleTimeslotType> o1, List<ScheduleTimeslotType> o2) {

                return o1.get(0).getTimeslotStartDateTime().compareTo(o2.get(0).getTimeslotStartDateTime());
            }
        });

        int i = 0;

        List<IntermittentSentenceScheduleType> intermSentScheduleList = new ArrayList<IntermittentSentenceScheduleType>();
        IntermittentSentenceScheduleType intermSentScheduleLastWithoutAdj = null;
        int totalDaysBefore = 0;
        while (count < sentenceDaysWithoutAdj) {
            for (List<ScheduleTimeslotType> schedulesLocal : schedulesList) {
                ScheduleTimeslotType scheduleLot = schedulesLocal.get(i);
                IntermittentScheduleType intermPattern = schedulePatternMap.get(scheduleLot.getScheduleID());
                boolean isRuleApplied = false;
                if (intermPattern.getNoOfDays() == consecutiveStay) {
                    count = count + daysForConsecutiveStay;
                    if (consecutiveStay != daysForConsecutiveStay) {
                        isRuleApplied = true;
                    }

                } else if (intermPattern.getSameDay().booleanValue()) {
                    int daysInAday = getDaysForhourInADay(quantifier, sentenceWithGuvRule, intermPattern.getHoursInSameDay());
                    count = count + daysInAday;
                } else {
                    count = count + intermPattern.getNoOfDays();
                }

                IntermittentSentenceScheduleType intermSentSchedule = getIntermSentenceScheduleType(intermPattern, scheduleLot,
                        (count > sentenceDays ? count - sentenceDays : 0), orderId, isRuleApplied, daysForConsecutiveStay, sentenceDays, count);
                if (totalDaysBefore < sentenceDays) {
                    intermSentScheduleList.add(intermSentSchedule);
                    intermSentScheduleLastWithoutAdj = intermSentSchedule;
                } else if (sentenceDaysWithoutAdj <= count) {
                    intermSentScheduleLastWithoutAdj = getIntermSentenceScheduleType(intermPattern, scheduleLot,
                            (count > sentenceDaysWithoutAdj ? count - sentenceDaysWithoutAdj : 0), orderId, isRuleApplied, daysForConsecutiveStay, sentenceDaysWithoutAdj,
                            count);
                }

                totalDaysBefore = count;
                if (count >= sentenceDaysWithoutAdj) {
                    break;
                }
            }

            i++;

        }

        sentenceType.setSentenceEndDate(intermSentScheduleLastWithoutAdj.getDateOut());
        if (log.isDebugEnabled()) {
            log.debug("******************   total sentence days served by Schedules    " + count);
            for (IntermittentSentenceScheduleType interSentSch : intermSentScheduleList) {
                log.debug("******  ######   interSentSch.getDayIn()     " + interSentSch.getDayIn());
                log.debug("******  ######   interSentSch.getDateIn()    " + interSentSch.getDateIn());
                log.debug("******  ######   interSentSch.getDayOut()    " + interSentSch.getDayOut());
                log.debug("******  ######   interSentSch.getDateOut()   " + interSentSch.getDateOut());
            }
        }

        return intermSentScheduleList;
    }

    /**
     * This method returns the list of schedules to be generated when sentence's term is changed
     *
     * @param uc             UserContext
     * @param context        SessionContext
     * @param session        Session
     * @param sentenceType   SentenceType
     * @param sentenceEntity SentenceEntity
     * @param orderId        Long
     * @return List<IntermittentSentenceScheduleType>
     */
    private static List<IntermittentSentenceScheduleType> getIntermittentSentScheduleListForUpdate(UserContext uc, SessionContext context, Session session,
            SentenceType sentenceType, SentenceEntity sentenceEntity, Long orderId) {

        Set<IntermittentSentenceScheduleEntity> schedulesSet = sentenceEntity.getIntermittentSentenceSchedules();
        Set<IntermittentSentenceScheduleEntity> schedulesSet_inactive = new HashSet<IntermittentSentenceScheduleEntity>();
        for (IntermittentSentenceScheduleEntity intermittentSentenceScheduleEntity : schedulesSet) {
            if (!intermittentSentenceScheduleEntity.getStatus().booleanValue()) {
                //					intermittentSentenceScheduleEntity.setIntermittentSchedule(null);
                schedulesSet_inactive.add(intermittentSentenceScheduleEntity);
            }
        }

        Set<IntermittentSentenceScheduleType> schedulesTypeSetInactive = OrderSentenceHelper.toIntermittentSentenceScheduleTypes(schedulesSet_inactive);

        Date reportingDate = null;
        SortedSet<IntermittentScheduleType> schedulePatternList = sortSchedulePattern(sentenceType.getIntermittentSchedule());

        for (IntermittentScheduleType s : schedulePatternList) {
            reportingDate = s.getReportingDate();
            break;
        }

        Supervision supervision = getIntermittentSentenceWithGuvRule(uc, context, session, sentenceType.getSupervisionId(), sentenceType);
        List<Sentence> sentences = supervision.getSentences();

        int count = 0;
        int sentenceDays = 0;
        Sentence sentenceWithGuvRule = null;
        int sentenceDaysWithoutAdj = 0;
        for (Sentence sentence : sentences) {
            sentenceDaysWithoutAdj = +sentenceDaysWithoutAdj + sentence.getSentenceDays();
            sentenceDays = sentenceDays + du.daysBetween(sentence.getStartDate(), sentence.getPrdDate());
            log.debug("**************   ############     sentenceDays b/w start and prd         " + sentenceDays);
            Date sentenceStartDate = BeanHelper.getDateWithoutTime(sentence.getStartDate());
            if (!sentenceStartDate.equals(reportingDate)) {
                sentenceDays = sentenceDays - sentence.getNoOfDaysWhenRDaySameAsSDay();
            }
            if (log.isDebugEnabled()) {
                log.debug("**************   ############     sentenceDays          " + sentenceDays);
            }
            sentenceWithGuvRule = sentence;
            break;
        }

        int daysForConsecutiveStay = sentenceWithGuvRule.getNoOfDaysForConsecutiveStay();
        int consecutiveStay = sentenceWithGuvRule.getConsecutiveStay();
        String quantifier = sentenceWithGuvRule.getComparisionQuantifier() != null ? sentenceWithGuvRule.getComparisionQuantifier() : "E";
        int daysServed = 0;
        DateUtil dateUtil = new DateUtil();
        for (IntermittentSentenceScheduleType scheduleType : schedulesTypeSetInactive) {
            if (scheduleType.getDateIn().equals(scheduleType.getDateOut())) {
                int daysInAday = getDaysForhourInADay(quantifier, sentenceWithGuvRule, scheduleType.getHoursInSameDay());
                daysServed = daysServed + daysInAday;
            } else {
                int daysInSchedule = dateUtil.daysBetween(scheduleType.getDateIn(), scheduleType.getDateOut());
                if (daysInSchedule == consecutiveStay) {
                    daysServed = daysServed + daysForConsecutiveStay;
                } else {
                    daysServed = daysServed + daysInSchedule;
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("**************   ############     sentenceDays already served         " + daysServed);
        }

        int originalSentenceDays = sentenceDays;
        int originalSentenceDaysWithoutAdj = sentenceDaysWithoutAdj;
        sentenceDays = sentenceDays - daysServed;
        sentenceDaysWithoutAdj = sentenceDaysWithoutAdj - daysServed;

        if (log.isDebugEnabled()) {
            log.debug("**************   ############     sentenceDays left to be served        " + sentenceDays);
        }

        if (schedulesTypeSetInactive != null && schedulesTypeSetInactive.size() > 0) {
            Date maxScheduleDate = null;
            IntermittentSentenceScheduleType intermittentSentenceScheduleTypeMax = null;
            if (schedulesTypeSetInactive != null && schedulesTypeSetInactive.size() > 0) {
                intermittentSentenceScheduleTypeMax = Collections.max(schedulesTypeSetInactive, new Comparator<IntermittentSentenceScheduleType>() {
                    @Override
                    public int compare(IntermittentSentenceScheduleType o1, IntermittentSentenceScheduleType o2) {
                        return o1.getDateOut().compareTo(o2.getDateOut());
                    }
                });
            }

            if (intermittentSentenceScheduleTypeMax != null) {
                maxScheduleDate = intermittentSentenceScheduleTypeMax.getDateOut();

            }

            if (maxScheduleDate != null && reportingDate.before(maxScheduleDate)) {
                reportingDate = new DateUtil().addDays(maxScheduleDate, 2);
            }
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(reportingDate);

        ActivityService service = null;
        if (service == null) {
            service = (ActivityService) ServiceAdapter.JNDILookUp(service, ActivityService.class);
        }
        Map<Long, IntermittentScheduleType> schedulePatternMap = new HashMap<Long, IntermittentScheduleType>();
        List<List<ScheduleTimeslotType>> schedulesList = new ArrayList<List<ScheduleTimeslotType>>();
        for (IntermittentScheduleType s : schedulePatternList) {
            Schedule sch = new Schedule();
            sch.setScheduleCategory(ActivityCategory.CASE.value());
            sch.setFacilityId(s.getLocationId());
            sch.setCapacity(20000L);
            sch.setEffectiveDate(BeanHelper.getDateWithoutTime(reportingDate));

            sch.setScheduleStartTime(s.getStartTimeAsHourMin());

            sch.setRecurrencePattern(new WeeklyRecurrencePatternType(1L, new HashSet<String>(Arrays.asList(new String[] { s.getDayOfWeek() }))));

            Schedule schRet = service.createSchedule(uc, sch);

            schedulePatternMap.put(schRet.getScheduleIdentification(), s);
            Date startDate = BeanHelper.getDateWithoutTime(s.getReportingDate());
            Calendar endDateAsCal = Calendar.getInstance();
            endDateAsCal.setTime(startDate);
            endDateAsCal.add(Calendar.DATE,
                    ((originalSentenceDays * 8) < originalSentenceDaysWithoutAdj ? (originalSentenceDaysWithoutAdj * 8) : (originalSentenceDays * 8)));

            //	    		endDateAsCal.add(Calendar.DATE, sentenceDaysWithoutAdj*7);
            Date endDate = BeanHelper.getDateWithoutTime(endDateAsCal.getTime());

            List<ScheduleTimeslotType> timeslotsRet = service.generateTimeslotList(uc, schRet.getScheduleIdentification(), startDate, endDate, null);

            Collections.sort(timeslotsRet, new Comparator<ScheduleTimeslotType>() {

                @Override
                public int compare(ScheduleTimeslotType o1, ScheduleTimeslotType o2) {
                    return o1.getTimeslotStartDateTime().compareTo(o2.getTimeslotStartDateTime());
                }
            });

            ScheduleTimeslotType firstScheduleLot = timeslotsRet.get(0);
            cal.setTime(firstScheduleLot.getTimeslotStartDateTime());
            String firstScheduleDay = days[cal.get(Calendar.DAY_OF_WEEK) - 1];

            if (firstScheduleDay != null && !firstScheduleDay.equalsIgnoreCase(s.getDayOfWeek())) {
                timeslotsRet.remove(0);
            }
            schedulesList.add(timeslotsRet);

        }

        Collections.sort(schedulesList, new Comparator<List<ScheduleTimeslotType>>() {

            @Override
            public int compare(List<ScheduleTimeslotType> o1, List<ScheduleTimeslotType> o2) {

                return o1.get(0).getTimeslotStartDateTime().compareTo(o2.get(0).getTimeslotStartDateTime());
            }
        });

        int i = 0;

        List<IntermittentSentenceScheduleType> intermSentScheduleList = new ArrayList<IntermittentSentenceScheduleType>();
        IntermittentSentenceScheduleType intermSentScheduleLastWithoutAdj = null;
        intermSentScheduleList.addAll(schedulesTypeSetInactive);
        int totalDaysBefore = 0;
        while (count < sentenceDaysWithoutAdj) {
            for (List<ScheduleTimeslotType> schedulesLocal : schedulesList) {
                ScheduleTimeslotType scheduleLot = schedulesLocal.get(i);
                IntermittentScheduleType intermPattern = schedulePatternMap.get(scheduleLot.getScheduleID());
                boolean isRuleApplied = false;
                if (intermPattern.getNoOfDays() == consecutiveStay) {
                    count = count + daysForConsecutiveStay;
                    if (consecutiveStay != daysForConsecutiveStay) {
                        isRuleApplied = true;
                    }

                } else if (intermPattern.getSameDay().booleanValue()) {
                    int daysInAday = getDaysForhourInADay(quantifier, sentenceWithGuvRule, intermPattern.getHoursInSameDay());
                    count = count + daysInAday;
                } else {
                    count = count + intermPattern.getNoOfDays();
                }

                IntermittentSentenceScheduleType intermSentSchedule = getIntermSentenceScheduleType(intermPattern, scheduleLot,
                        (count > sentenceDays ? count - sentenceDays : 0), orderId, isRuleApplied, daysForConsecutiveStay, sentenceDays, count);
                if (totalDaysBefore < sentenceDays) {
                    intermSentScheduleList.add(intermSentSchedule);
                    intermSentScheduleLastWithoutAdj = intermSentSchedule;
                } else if (sentenceDaysWithoutAdj <= count) {
                    intermSentScheduleLastWithoutAdj = getIntermSentenceScheduleType(intermPattern, scheduleLot,
                            (count > sentenceDaysWithoutAdj ? count - sentenceDaysWithoutAdj : 0), orderId, isRuleApplied, daysForConsecutiveStay, sentenceDaysWithoutAdj,
                            count);
                }

                totalDaysBefore = count;
                if (count >= sentenceDaysWithoutAdj) {
                    break;
                }
            }

            i++;
        }

        sentenceType.setSentenceEndDate(intermSentScheduleLastWithoutAdj.getDateOut());
        if (log.isDebugEnabled()) {
            log.debug("******************   total sentence days served by Schedules    " + count);
            for (IntermittentSentenceScheduleType interSentSch : intermSentScheduleList) {
                log.debug("******  ######   interSentSch.getDayIn()     " + interSentSch.getDayIn());
                log.debug("******  ######   interSentSch.getDateIn()    " + interSentSch.getDateIn());
                log.debug("******  ######   interSentSch.getDayOut()    " + interSentSch.getDayOut());
                log.debug("******  ######   interSentSch.getDateOut()   " + interSentSch.getDateOut());
            }
        }

        return intermSentScheduleList;
    }

    /**
     * This method return IntermittentSentenceScheduleType from ScheduleTimeslotType
     *
     * @param pattern
     * @param schedule
     * @param deviation
     * @return IntermittentSentenceScheduleType
     */
    private static IntermittentSentenceScheduleType getIntermSentenceScheduleType(IntermittentScheduleType pattern, ScheduleTimeslotType schedule, int deviation,
            Long orderId, boolean isRuleApplied, int daysForConsecutiveStays, int sentenceDays, int count) {
        IntermittentSentenceScheduleType intermSentSchedule = new IntermittentSentenceScheduleType();
        //intermSentSchedule.setIntermittentScheduleId(pattern.getInterScheduleId()); //WOR-15366
        intermSentSchedule.setLocationId(pattern.getLocationId());
        intermSentSchedule.setOrderId(orderId);
        syscon.arbutus.product.services.legal.contract.dto.ordersentence.IntermittentScheduleType.TimeValue timeValueInSrc = pattern.getStartTime();
        syscon.arbutus.product.services.legal.contract.dto.ordersentence.IntermittentSentenceScheduleType.TimeValue timeValueInDest = intermSentSchedule.new TimeValue(
                timeValueInSrc.hour(), timeValueInSrc.minute(), timeValueInSrc.second());
        intermSentSchedule.setTimeIn(timeValueInDest);

        syscon.arbutus.product.services.legal.contract.dto.ordersentence.IntermittentScheduleType.TimeValue timeValueOutSrc = pattern.getEndTime();

        syscon.arbutus.product.services.legal.contract.dto.ordersentence.IntermittentSentenceScheduleType.TimeValue timeValueOutDest = intermSentSchedule.new TimeValue(
                timeValueOutSrc.hour(), timeValueOutSrc.minute(), timeValueOutSrc.second());
        intermSentSchedule.setTimeOut(timeValueOutDest);

        intermSentSchedule.setDateIn(BeanHelper.getDateWithoutTime(schedule.getTimeslotStartDateTime()));
        intermSentSchedule.setDayIn(pattern.getDayOfWeek());
        intermSentSchedule.setDayOut(pattern.getDayOutOfWeek());
        intermSentSchedule.setOutCome("PEND");
        intermSentSchedule.setStatus(true);
        if (pattern.getSameDay()) {
            intermSentSchedule.setDateOut(BeanHelper.getDateWithoutTime(schedule.getTimeslotStartDateTime()));
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(BeanHelper.getDateWithoutTime(schedule.getTimeslotStartDateTime()));
            if (deviation > 0) {
                int noOfDaysInPattern = pattern.getDiffBetweenDayInDayOut();
                if (isRuleApplied && (count - daysForConsecutiveStays + noOfDaysInPattern) <= sentenceDays) {
                    cal.add(Calendar.DATE, pattern.getDiffBetweenDayInDayOut() - 1);
                    intermSentSchedule.setDateOut(cal.getTime());
                } else {
                    int daysToBeServed = sentenceDays - (count - (isRuleApplied ? daysForConsecutiveStays : noOfDaysInPattern));
                    cal.add(Calendar.DATE, daysToBeServed - 1);
                    intermSentSchedule.setDayOut(days[cal.get(Calendar.DAY_OF_WEEK) - 1]);
                    intermSentSchedule.setDateOut(cal.getTime());
                }

            } else {
                cal.add(Calendar.DATE, pattern.getDiffBetweenDayInDayOut() - 1);
                intermSentSchedule.setDateOut(cal.getTime());
            }

        }

        return intermSentSchedule;
    }

    /**
     * This method calculate the no. of days for hours in a day defined in Guvnor
     *
     * @param qualtifier
     * @param sentence
     * @param hours
     * @return int
     */
    static private int getDaysForhourInADay(String qualtifier, Sentence sentence, int hours) {
        int hoursInADay = sentence.getNoOfHours();
        int noOfDaysForHoursWithIfInADay = sentence.getNoOfDaysForHoursInADay();
        int noOfDaysForHoursWithElseInADay = sentence.getNoOfDaysForHoursInADayElse();
        int days = 0;

        switch (qualtifier) {
            case "E":
                if (hours == hoursInADay) {
                    days = noOfDaysForHoursWithIfInADay;
                } else {
                    days = noOfDaysForHoursWithElseInADay;
                }
                break;
            case "L":
                if (hours < hoursInADay) {
                    days = noOfDaysForHoursWithIfInADay;
                } else {
                    days = noOfDaysForHoursWithElseInADay;
                }
                break;
            case "EL":
                if (hours <= hoursInADay) {
                    days = noOfDaysForHoursWithIfInADay;
                } else {
                    days = noOfDaysForHoursWithElseInADay;
                }
                break;
            case "G":
                if (hours > hoursInADay) {
                    days = noOfDaysForHoursWithIfInADay;
                } else {
                    days = noOfDaysForHoursWithElseInADay;
                }
                break;
            case "EG":
                if (hours >= hoursInADay) {
                    days = noOfDaysForHoursWithIfInADay;
                } else {
                    days = noOfDaysForHoursWithElseInADay;
                }
                break;
            default:
                days = 0;
                break;

        }

        return days;
    }

    /**
     * @return Sentence
     */
    public static Sentence getIntermittentSentenceWithCounter(UserContext userContext, SessionContext context, Session session, SentenceType sentenceType) {
        SentenceEntity sentenceEntity = (SentenceEntity) session.get(SentenceEntity.class, sentenceType.getOrderIdentification());
        return calculateScheduleCounter(userContext, context, session, sentenceType, sentenceEntity);
    }

    /**
     * @param uc
     * @param context
     * @param session
     * @param sentenceType
     * @param sentenceEntity
     * @return Sentence
     */
    private static Sentence calculateScheduleCounter(UserContext uc, SessionContext context, Session session, SentenceType sentenceType, SentenceEntity sentenceEntity) {

        Set<IntermittentSentenceScheduleEntity> schedulesSet = sentenceEntity.getIntermittentSentenceSchedules();
        Set<IntermittentScheduleEntity> schedulePatternSet = sentenceEntity.getIntermittentSchedule();
        Set<IntermittentSentenceScheduleEntity> schedulesSet_inactive = new HashSet<IntermittentSentenceScheduleEntity>();
        Sentence sentenceWithCounter = new Sentence();

        Set<IntermittentSentenceScheduleType> schedulesTypeSet = OrderSentenceHelper.toIntermittentSentenceScheduleTypes(schedulesSet);

        Date reportingDate = null;

        SortedSet<IntermittentScheduleType> schedulePatternList = sortSchedulePattern(OrderSentenceHelper.toIntermittentScheduleTypeSet(schedulePatternSet));

        for (IntermittentScheduleType s : schedulePatternList) {
            reportingDate = s.getReportingDate();
            break;
        }

        Supervision supervision = getIntermittentSentenceWithGuvRule(uc, context, session, sentenceType.getSupervisionId(), sentenceType);
        List<Sentence> sentences = supervision.getSentences();

        int sentenceDays = 0;
        Sentence sentenceWithGuvRule = null;
        int sentenceDaysWithoutAdj = 0;
        for (Sentence sentence : sentences) {
            sentenceDaysWithoutAdj = +sentenceDaysWithoutAdj + sentence.getSentenceDays();
            sentenceDays = sentenceDays + du.daysBetween(sentence.getStartDate(), sentence.getPrdDate());
            log.debug("**************   ############     sentenceDays b/w start and prd         " + sentenceDays);
            Date sentenceStartDate = BeanHelper.getDateWithoutTime(sentence.getStartDate());
            if (!sentenceStartDate.equals(reportingDate)) {
                sentenceDays = sentenceDays - sentence.getNoOfDaysWhenRDaySameAsSDay();
            }

            sentenceWithCounter.setSentenceTermInDays(sentenceDays);
            if (log.isDebugEnabled()) {
                log.debug("**************   ############     sentenceDays          " + sentenceDays);
            }
            sentenceWithGuvRule = sentence;
            break;
        }

        for (IntermittentSentenceScheduleEntity intermittentSentenceScheduleEntity : schedulesSet) {
            if (intermittentSentenceScheduleEntity.getOutCome() != null && (
                    intermittentSentenceScheduleEntity.getOutCome().equalsIgnoreCase(sentenceWithGuvRule.getScheduleOutcome1ForRemainingDays())
                            || intermittentSentenceScheduleEntity.getOutCome().equalsIgnoreCase(sentenceWithGuvRule.getScheduleOutcome2ForRemainingDays()))) {
                schedulesSet_inactive.add(intermittentSentenceScheduleEntity);
            }
        }

        Set<IntermittentSentenceScheduleType> schedulesTypeSetInactive = OrderSentenceHelper.toIntermittentSentenceScheduleTypes(schedulesSet_inactive);

        int daysForConsecutiveStay = sentenceWithGuvRule.getNoOfDaysForConsecutiveStay();
        int consecutiveStay = sentenceWithGuvRule.getConsecutiveStay();
        String quantifier = sentenceWithGuvRule.getComparisionQuantifier() != null ? sentenceWithGuvRule.getComparisionQuantifier() : "E";
        int daysServed = 0;
        DateUtil dateUtil = new DateUtil();
        for (IntermittentSentenceScheduleType scheduleType : schedulesTypeSetInactive) {
            if (scheduleType.getDateIn().equals(scheduleType.getDateOut())) {
                int daysInAday = getDaysForhourInADay(quantifier, sentenceWithGuvRule, scheduleType.getHoursInSameDay());
                daysServed = daysServed + daysInAday;
            } else {
                int daysInSchedule = dateUtil.daysBetween(scheduleType.getDateIn(), scheduleType.getDateOut());
                if (daysInSchedule == consecutiveStay) {
                    daysServed = daysServed + daysForConsecutiveStay;
                } else {
                    daysServed = daysServed + daysInSchedule;
                }
            }
        }

        sentenceWithCounter.setDaysServed(daysServed);

        long daysScheduled = 0;
        for (IntermittentSentenceScheduleType scheduleType : schedulesTypeSet) {
            if (scheduleType.getDateIn().equals(scheduleType.getDateOut())) {
                int daysInAday = getDaysForhourInADay(quantifier, sentenceWithGuvRule, scheduleType.getHoursInSameDay());
                daysScheduled = daysScheduled + daysInAday;
            } else {
                int daysInSchedule = dateUtil.daysBetween(scheduleType.getDateIn(), scheduleType.getDateOut());
                if (daysInSchedule == consecutiveStay) {
                    daysScheduled = daysScheduled + daysForConsecutiveStay;
                } else {
                    daysScheduled = daysScheduled + daysInSchedule;
                }
            }
        }

        sentenceWithCounter.setDaysScheduled(daysScheduled);
        sentenceWithCounter.setDaysRemaining((daysScheduled - daysServed));

        if (log.isDebugEnabled()) {
            log.debug("**************   ############     sentenceDays already served         " + daysServed);
        }

        if (log.isDebugEnabled()) {
            log.debug("**************   ############     sentenceDays left to be served        " + (daysScheduled - daysServed));
        }

        return sentenceWithCounter;
    }

    /**
     * @param uc
     * @param context
     * @param session
     * @param scheduleId
     * @return Long
     */
    public static Long deleteIntermittentSchedule(UserContext uc, SessionContext context, Session session, Long scheduleId) {
        String functionName = "deleteIntermittentSchedule";

        Long ret = ReturnCode.Success.returnCode();

        // Validation
        if (scheduleId == null) {
            String message = String.format("intermittentScheduleId= %s.", scheduleId);
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        IntermittentSentenceScheduleEntity entity = BeanHelper.getEntity(session, IntermittentSentenceScheduleEntity.class, scheduleId);
        Long orderId = entity.getSentence().getOrderId();

        // Start of WOR-11178 (point 2)
        Query query = session.createQuery("from MovementActivityEntity As MA where  MA.intermittentSenScheduleId=:schid ORDER BY MA.movementId DESC");
        query.setParameter("schid", entity.getInterSenScheduleId());
        List list = query.list();

        if (!list.isEmpty()) {
            MovementActivityEntity ma = (MovementActivityEntity) list.get(0);

            if (null != ma) {
                ActivityEntity activity = (ActivityEntity) BeanHelper.findEntity(session, ActivityEntity.class, ma.getActivityId());
                session.delete(activity);
                session.delete(ma);
            }

        }
		
		/*Query queryb = session
		.createQuery("delete from MovementActivityEntity As MA where  MA.movementCategory='EXTERNAL'"
				+ " and MA.movementDirection='IN' and MA.intermittentSenScheduleId is null");
		queryb.executeUpdate();

    
		Query queryb = session
		.createQuery("update MovementActivityEntity As MA set MA.movementStatus='CANCELLED' where  MA.movementCategory='EXTERNAL'"
				+ " and MA.movementDirection='IN' and MA.intermittentSenScheduleId is null");
		queryb.executeUpdate(); */

        // End of WOR-11178

        session.delete(entity);

        OrderSentenceHelper.setSentenceCalculationEnableFlagBySentenceId(session, orderId, true);
        session.flush();

        return ret;
    }

    /**
     * @param uc
     * @param context
     * @param session
     * @param schedulesMovementNew
     * @param schedulesMovementOld
     */
    // WOR-11178 acceptance criteria 3, 8
    private static void deleteAssociatedScheduleMovement(UserContext uc, SessionContext context, Session session,
            Set<IntermittentSentenceScheduleEntity> schedulesMovementNew, Set<IntermittentSentenceScheduleEntity> schedulesMovementOld) {
        for (IntermittentSentenceScheduleEntity entity : schedulesMovementOld) {

            if (!schedulesMovementNew.contains(entity)) {
                Query query = session.createQuery("from MovementActivityEntity As MA where  MA.intermittentSenScheduleId=:schid ORDER BY MA.movementId DESC");
                query.setParameter("schid", entity.getInterSenScheduleId());
                List list = query.list();

                if (!list.isEmpty()) {
                    MovementActivityEntity ma = (MovementActivityEntity) list.get(0);
                    if (null != ma) {
                        if (null != ma.getMovementStatus() && !ma.getMovementStatus().equals("COMPLETED")) {
                            ma.setMovementStatus("CANCELLED");

                            ActivityEntity activity = (ActivityEntity) BeanHelper.findEntity(session, ActivityEntity.class, ma.getActivityId());
                            if (null != activity) {
                                activity.setFlag(0L);
                                session.update(activity);
                            }
                            session.update(ma);
                        }
                    }

                }
                entity.setStatus(false);

            }
        }
    }

    /**
     * @param uc
     * @param context
     * @param session
     * @param orderIdentification
     */
    // WOR-11178 acceptance criteria 9
    private static void cancelAssociatedScheduleMovement(UserContext uc, SessionContext context, Session session, Long orderIdentification) {

        List<IntermittentSentenceScheduleEntity> entityList = new ArrayList<IntermittentSentenceScheduleEntity>();

        //Query queryObj=session.createQuery("from IntermittentSentenceScheduleEntity As ISSE where  ISSE.interSenScheduleId=:schid ORDER BY ISSE.interSenScheduleId DESC");
        Query queryObj = session.createQuery(
                "from IntermittentSentenceScheduleEntity As ISSE where  ISSE.sentence.orderId=:orderId ORDER BY ISSE.interSenScheduleId DESC");

        queryObj.setParameter("orderId", orderIdentification);
        entityList = queryObj.list();

        for (IntermittentSentenceScheduleEntity entity : entityList) {
            entity.setStatus(false);
            entity.setOutCome("CANCELLED");

            Query query = session.createQuery("from MovementActivityEntity As MA where  MA.intermittentSenScheduleId=:schid ORDER BY MA.movementId DESC");
            query.setParameter("schid", entity.getInterSenScheduleId());
            List list = query.list();

            if (!list.isEmpty()) {
                MovementActivityEntity ma = (MovementActivityEntity) list.get(0);
                if (null != ma) {
                    if (null != ma.getMovementStatus() && !ma.getMovementStatus().equals("COMPLETED")) {
                        ma.setMovementStatus("CANCELLED");

                        ActivityEntity activity = (ActivityEntity) BeanHelper.findEntity(session, ActivityEntity.class, ma.getActivityId());
                        if (null != activity) {
                            activity.setFlag(0L);
                            session.update(activity);
                        }

                        session.update(ma);
                    }
                }

            }

            //session.delete(entity);
            session.update(entity);

        }

    }

    public static <U extends OrderType, V extends OrderEntity> U updateOrderBail(UserContext uc, SessionContext context, Session session, U order, Class<U> typeClazz,
            Class<V> entityClazz) {
        if (log.isDebugEnabled()) {
			log.debug("update: begin");
		}
        U ret = null;

        if (order == null || order.getOrderIdentification() == null) {
            throw new InvalidInputException("Invalid argument: null order");
        }
        ValidationHelper.validate(order);
        Set<Long> caseInfoIdSet = order.getCaseInfoIds();
        if (OrderCategory.IJ.name().equalsIgnoreCase(order.getOrderCategory()) && (caseInfoIdSet == null || caseInfoIdSet.size() == 0)) {
            String message = "Invalid argument: Order must have at least one Case Information Id associated when Order Category is IJ";
            LogHelper.error(log, "updateOrder", message);
            throw new InvalidInputException(message);
        } else if (OrderCategory.OJ.name().equalsIgnoreCase(order.getOrderCategory()) && order.getOjSupervisionId() == null) {
            String message = "Invalid argument: OJ Supervision ID is required when Order Category is OJ";
            LogHelper.error(log, "updateOrder", message);
            throw new InvalidInputException(message);
        }

        String sentenceTypeAsString = null;
        if (order instanceof SentenceType) {
            sentenceTypeAsString = ((SentenceType) order).getSentenceType();
            OrderSentenceHelper.verifySentenceType(uc, session, ((SentenceType) order));
        }

        Long orderId = order.getOrderIdentification();

        OrderEntity orderEntity = BeanHelper.getEntity(session, OrderEntity.class, order.getOrderIdentification());

        boolean isSentenceUpdated = false;
        if (order instanceof SentenceType && "INTER".equalsIgnoreCase(sentenceTypeAsString)) {
            isSentenceUpdated = OrderSentenceHelper.
                    isSentenceChangedToRegenerateSchedule(((SentenceEntity) orderEntity), ((SentenceType) order));
            if (log.isDebugEnabled()) {
				log.debug("*****************  isSentenceUpdated   " + isSentenceUpdated);
			}
            if (isSentenceUpdated) {
                List<IntermittentSentenceScheduleType> intermsenteScheduleList = getIntermittentSentScheduleListForUpdate(uc, context, session, (SentenceType) order,
                        (SentenceEntity) orderEntity, orderId);
                ((SentenceType) order).setSchedules(new HashSet<IntermittentSentenceScheduleType>(intermsenteScheduleList));
            }
        }

        // order.setAssociations(null); // No association to be associated
        OrderSentenceHelper.verifyOrder(uc, context, session, order);
        StampEntity stamp = BeanHelper.getModifyStamp(uc, context, orderEntity.getStamp());
        OrderEntity entity = OrderSentenceHelper.toOrderEntity(uc, context, order, stamp, OrderSentenceHelper.UPDATE, entityClazz);

        //IntermittentSentenceSchedules already merged at line number 1126
        if (!isSentenceUpdated && entity instanceof SentenceEntity && "INTER".equalsIgnoreCase(sentenceTypeAsString)) {
            ((SentenceEntity) entity).setIntermittentSentenceSchedules(((SentenceEntity) orderEntity).getIntermittentSentenceSchedules());
        } else if (entity instanceof SentenceEntity && !"INTER".equalsIgnoreCase(sentenceTypeAsString)) {
            ((SentenceEntity) entity).setIntermittentSentenceSchedules(new HashSet<IntermittentSentenceScheduleEntity>());
        }

        ValidationHelper.verifyMetaCodes(entity, false);

        // Order Classification should not be able to be updated
        entity.setOrderClassification(orderEntity.getOrderClassification());
        // The following fields can not be updated
        //     IsHoldingOrder
        //     IsSchedulingNeeded
        //     HasCharges
        entity.setIsHoldingOrder(orderEntity.getIsHoldingOrder());
        entity.setIsSchedulingNeeded(orderEntity.getIsSchedulingNeeded());
        entity.setHasCharges(orderEntity.getHasCharges());
        entity.setVersion(orderEntity.getVersion());

        // Remove sentenceId from associated Chage
        OrderSentenceHelper.removeSentenceIdFromAssociatedCharge(uc, session, orderId);
        LegalHelper.removeModuleLinks(uc, context, session, orderId, LegalModule.ORDER_SENTENCE, orderEntity.getModuleAssociations());

        if (order instanceof SentenceType) {
            // SentenceNumber cannot be updated once it is created.
            if (((SentenceEntity) orderEntity).getSentenceNumber() != null) {
                ((SentenceEntity) entity).setSentenceNumber(((SentenceEntity) orderEntity).getSentenceNumber());
            } else {
                if (((SentenceEntity) entity).getSentenceNumber() != null) {
                    Boolean bGeneratedNumber = getSentenceNumberConfiguration(uc, context, session);
                    if (Boolean.TRUE.equals(bGeneratedNumber)) {
                        // Keep the original Sentence Number
                        ((SentenceEntity) entity).setSentenceNumber(((SentenceEntity) orderEntity).getSentenceNumber());
                    } else {
                        // Sentence Number must be unique for the set of Case Info
                        Long sentenceNumber = ((SentenceEntity) entity).getSentenceNumber();
                        boolean bExisted = OrderSentenceHelper.bExistedSentenceNumber(session, sentenceNumber, order.getCaseInfoIds());
                        if (bExisted) {
                            throw new DataExistException("Sentence Number " + sentenceNumber + " existed already.");
                        }
                    }
                }
            }
            //Retail all the Fine Payments
            ((SentenceEntity) entity).setFinePayments(((SentenceEntity) orderEntity).getFinePayments());
            OrderSentenceHelper.verifyCyclicSentenceConsective(session, (SentenceEntity) entity);
        }

        // Update the association to CaseActivity
        setAssociatedIds(session, orderEntity, order);
        // setAssociatedIds(session, entity, order);
        entity = (OrderEntity) session.merge(entity);
        session.flush();
        session.clear();

        LegalHelper.createModuleLinks(uc, context, session, orderId, LegalModule.ORDER_SENTENCE, entity.getModuleAssociations(), stamp);
        if (order instanceof SentenceType) {
            DispositionEntity disposition = entity.getOrderDisposition();
            String outcome = null;
            if (disposition != null) {
				outcome = disposition.getDispositionOutcome();
			}
            OrderSentenceHelper.setChargeSentenceLink(uc, context, session, orderId, order.getChargeIds(), outcome);

            // setSentenceStatusForCaseInfo(uc, context, session, order.getCaseInfoIds(), orderId, true);
            if (OrderClassification.SENT.name().equalsIgnoreCase(entity.getOrderClassification())) {
                OrderSentenceHelper.updateCaseInfoSentenceStatusBySentenceId(session, (SentenceEntity) entity, false);
            }
        }

        entity = BeanHelper.getEntity(session, OrderEntity.class, orderId);

        if (order instanceof SentenceType) {
            OrderSentenceHelper.setSentenceCalculationEnableFlagBySentenceId(session, entity.getOrderId(), true);
        }

        // Store to history table
        OrderSentenceHelper.storeToHistoryEntity(session, entity);

        ret = OrderSentenceHelper.toOrder(uc, context, entity, typeClazz);

        if (log.isDebugEnabled()) {
			log.debug("update: end");
		}
        return ret;
    }

    public IntermittentSentenceScheduleType createIntermittentSchedule(SessionContext context, Session session, UserContext userContext,
            IntermittentSentenceScheduleType intermittentSentenceSchedule) {
        ValidationHelper.validate(userContext);
        ValidationHelper.validate(intermittentSentenceSchedule);
        SentenceEntity sentenceEntity = (SentenceEntity) session.get(SentenceEntity.class, intermittentSentenceSchedule.getOrderId());
        if (sentenceEntity == null) {
            String message = "Sentence not found for given Id %s.";
            throw new InvalidInputException(String.format(message, intermittentSentenceSchedule.getOrderId()));
        }
        StampEntity stamp = BeanHelper.getCreateStamp(userContext, context);
        IntermittentSentenceScheduleEntity intermittentSentenceScheduleEntity = OrderSentenceHelper.
                toIntermittentSentenceScheduleEntity(userContext, context, intermittentSentenceSchedule, stamp);
        if (OrderSentenceHelper.isExceedingNumberOfDays(sentenceEntity, intermittentSentenceScheduleEntity)) {
            throw new InvalidInputException("Number of days sentence is exceeding");
        }
        ValidationHelper.verifyMetaCodes(intermittentSentenceScheduleEntity, true);
        sentenceEntity.addIntermittentSentenceScheduleEntity(intermittentSentenceScheduleEntity);
        session.update(sentenceEntity);
        OrderSentenceHelper.setSentenceCalculationEnableFlagBySentenceId(session, sentenceEntity.getOrderId(), true);
        session.flush();
        return OrderSentenceHelper.toIntermittentSentenceScheduleType(intermittentSentenceScheduleEntity);
    }

    public IntermittentSentenceScheduleReturnType updateIntermittentSchedule(SessionContext context, Session session, UserContext userContext,
            IntermittentSentenceScheduleType intermittentSentenceSchedule) {
        ValidationHelper.validate(userContext);
        ValidationHelper.validate(intermittentSentenceSchedule);
        if (intermittentSentenceSchedule.getIntermittentScheduleId() == null) {
            String message = "Intermittent Sentence Schedule Id can not be null";
            throw new InvalidInputException(message);
        }
        IntermittentSentenceScheduleEntity existingEntity = (IntermittentSentenceScheduleEntity) session.get(IntermittentSentenceScheduleEntity.class,
                intermittentSentenceSchedule.getIntermittentScheduleId());
        if (existingEntity == null) {
            String message = "IntermittentSentenceSchedule not found for given Id %s.";
            throw new InvalidInputException(String.format(message, intermittentSentenceSchedule.getIntermittentScheduleId()));
        }
        //TODO
        //			if (existingEntity.getSentence().getSentenceStatus().equals(SentenceStatus.SENTENCED.name())) {
        //	            throw new InvalidInputException("Intermittent Sentence Schedule can't be modified since sentence already served.");
        //	        }
        BeanHelper.getModifyStamp(userContext, context, existingEntity.getStamp());
        long priorSentenceDays = OrderSentenceHelper.getTotalNumberOfSentenceDays(existingEntity.getSentence());
        OrderSentenceHelper.modifyIntermittentSentenceScheduleEntity(userContext, context, existingEntity, intermittentSentenceSchedule);
        long afterSentenceDays = OrderSentenceHelper.getTotalNumberOfSentenceDays(existingEntity.getSentence());
        IntermittentSentenceScheduleReturnType intermittentSentenceScheduleReturnType = new IntermittentSentenceScheduleReturnType();
        if (priorSentenceDays != afterSentenceDays) {
            String message = "You have edited the schedule that is now [%s] than the total scheduled days (%s)";
            String lessExcedd;
            if (afterSentenceDays < priorSentenceDays) {
                lessExcedd = "less";
            } else {
                lessExcedd = "exceed";
            }
            intermittentSentenceScheduleReturnType.setReturnMessage(String.format(message, lessExcedd, priorSentenceDays));
        }
        ValidationHelper.verifyMetaCodes(existingEntity, true);
        session.update(existingEntity);

        // Start of WOR-11178 (point 1,7)
        Query query = session.createQuery("from MovementActivityEntity As MA where  MA.intermittentSenScheduleId=:schid ORDER BY MA.movementId DESC");
        query.setParameter("schid", intermittentSentenceSchedule.getIntermittentScheduleId());
        List list = query.list();

        if (!list.isEmpty()) {
            MovementActivityEntity ma = (MovementActivityEntity) list.get(0);

            if (null != ma) {
                ActivityEntity activity = (ActivityEntity) BeanHelper.findEntity(session, ActivityEntity.class, ma.getActivityId());

                //ma.setToInternalLocationId(intermittentSentenceSchedule.getLocationId());

                if (intermittentSentenceSchedule.getOutCome().equals("RPTD")) {
                    ma.setMovementStatus("COMPLETED");
                }

                activity.setPlannedStartDate(intermittentSentenceSchedule.getDateIn());
                activity.getPlannedStartDate().setHours(intermittentSentenceSchedule.getTimeIn().hour().intValue());
                activity.getPlannedStartDate().setMinutes(intermittentSentenceSchedule.getTimeIn().minute().intValue());
                activity.getPlannedStartDate().setSeconds(intermittentSentenceSchedule.getTimeIn().second().intValue());

                activity.setPlannedEndDate(intermittentSentenceSchedule.getDateOut());
                activity.getPlannedEndDate().setHours(intermittentSentenceSchedule.getTimeOut().hour().intValue());
                activity.getPlannedEndDate().setMinutes(intermittentSentenceSchedule.getTimeOut().minute().intValue());
                activity.getPlannedEndDate().setSeconds(intermittentSentenceSchedule.getTimeOut().second().intValue());

				/*Calendar calendar = Calendar.getInstance();
		        calendar.setTime(intermittentSentenceSchedule
						.getDateIn());
		        activity.setPlannedStartDate(calendar.getTime());

		        calendar = Calendar.getInstance();
		        calendar.setTime(intermittentSentenceSchedule
						.getDateOut());
		        activity.setPlannedEndDate(calendar.getTime());
		        */

                session.update(activity);
                session.update(ma);
            }

        }
        // End of WOR-11178

        OrderSentenceHelper.setSentenceCalculationEnableFlagBySentenceId(session, existingEntity.getSentence().getOrderId(), true);
        session.flush();
        intermittentSentenceScheduleReturnType.setIntermittentSentenceScheduleType(OrderSentenceHelper.toIntermittentSentenceScheduleType(existingEntity));
        return intermittentSentenceScheduleReturnType;
    }

    /**
     * Returns Intermittent Sentence Schedule Type details for the given scheduleId.
     *
     * @param context
     * @param session
     * @param userContext
     * @param scheduleId
     * @return
     */
    public IntermittentSentenceScheduleType getIntermittentSentenceSchedule(SessionContext context, Session session, UserContext userContext, Long scheduleId) {
        ValidationHelper.validate(userContext);
        if (scheduleId == null) {
            String message = "schedule Id can not be null";
            throw new InvalidInputException(message);
        }

        IntermittentSentenceScheduleEntity intermittentSentenceSchEntity = (IntermittentSentenceScheduleEntity) session.get(IntermittentSentenceScheduleEntity.class,
                scheduleId);
        if (intermittentSentenceSchEntity == null) {
            String message = "Intermittent Sentence Schedule not found for given schedule Id %s.";
            throw new InvalidInputException(String.format(message, scheduleId));
        }

        return OrderSentenceHelper.toIntermittentSentenceScheduleType(intermittentSentenceSchEntity);
    }

    /**
     * @param context
     * @param session
     * @param userContext
     * @param orderId
     * @return Set<IntermittentSentenceScheduleType>
     */
    public Set<IntermittentSentenceScheduleType> getIntermittentSentenceSchedules(SessionContext context, Session session, UserContext userContext, Long orderId) {
        ValidationHelper.validate(userContext);
        if (orderId == null) {
            String message = "Sentence Id can not be null";
            throw new InvalidInputException(message);
        }
        SentenceEntity sentenceEntity = (SentenceEntity) session.get(SentenceEntity.class, orderId);
        if (sentenceEntity == null) {
            String message = "Sentence not found for given Id %s.";
            throw new InvalidInputException(String.format(message, orderId));
        }
        return OrderSentenceHelper.toIntermittentSentenceScheduleTypes(sentenceEntity.getIntermittentSentenceSchedules());
    }

    /**
     * @param context
     * @param session
     * @param userContext
     * @param sentenceId
     * @return Long
     */
    public Long getRemainingNumberOfSentenceDays(SessionContext context, Session session, UserContext userContext, Long sentenceId) {
        ValidationHelper.validate(userContext);
        if (sentenceId == null) {
            String message = "Sentence Id can not be null";
            throw new InvalidInputException(message);
        }
        SentenceEntity sentenceEntity = (SentenceEntity) session.get(SentenceEntity.class, sentenceId);
        if (sentenceEntity == null) {
            String message = "Sentence not found for given Id %s.";
            throw new InvalidInputException(String.format(message, sentenceId));
        }
        return OrderSentenceHelper.getRemainingNumberOfSentenceDays(sentenceEntity);
    }

    /**
     * @param userContext
     * @param context
     * @param session
     * @param sentenceId
     * @return
     */
    public FinePaymentReturnType getFinePaymentBySentenceId(UserContext userContext, SessionContext context, Session session, Long sentenceId) {
        if (sentenceId == null || sentenceId <= 0) {
            String message = "AssociatedEntity Identifier can not be null or less than 1";
            throw new InvalidInputException(message);
        }
        Criteria criteria = session.createCriteria(FinePaymentEntity.class);
        criteria.add(Restrictions.eq("sentence.orderId", sentenceId));
        List<FinePaymentEntity> result = criteria.list();
        FinePaymentReturnType finePaymentReturnType = new FinePaymentReturnType();
        finePaymentReturnType.setFinePayments(OrderSentenceHelper.toFinePaymentTypes(result));
        finePaymentReturnType.setTotalSize(result.size());
        return finePaymentReturnType;
    }

    /**
     * @param userContext
     * @param context
     * @param session
     * @param finePayment
     * @return
     */
    public FinePaymentType recordFinePayment(UserContext userContext, SessionContext context, Session session, FinePaymentType finePayment) {
        SentenceEntity sentence = (SentenceEntity) session.get(SentenceEntity.class, finePayment.getSentenceId());
        if (sentence == null) {
            String message = "Sentence not found for given Id %s.";
            throw new InvalidInputException(String.format(message, finePayment.getSentenceId()));
        }
        StampEntity stamp = BeanHelper.getCreateStamp(userContext, context);
        FinePaymentEntity entity = OrderSentenceHelper.toFinePaymentEntity(finePayment, sentence, stamp);
        session.save(entity);
        session.flush();
        return OrderSentenceHelper.toFinePaymentType(entity);
    }

    public enum OrderClassification {
        BAIL,    // Bail
        LO,        // Legal Order
        SENT,    // Sentence
        WD        // WarrantDetainer
    }

    public enum OrderCategory {IJ, OJ}  // IJ - In Jail; OJ - Out Jail

    public enum SentenceNumberGeneration {
        SentenceNumberGenerated,
        SentenceNumberNotGenerated
    }

    public enum KeyDateView {
        AGGREGATE, DISAGGREGATE
    }

    public enum TermTypes {SINGLE, DUAL, THREE, INT}

    public enum CaseActivityCategory {COURT, PROB, POL, PAROLE}

    public enum CaseActivityType {
        INITCRT,
        PRELHEAR,
        BAILHEAR,
        GRDJURY,
        ARRAIGN,
        TRIAL,
        SENTENCE,
        APPEAL,
        PAROLEHR,
        PROBHR
    }

    public enum CaseActivityOutcome {
        DISMISS,
        PROBATION,
        CAPITAL,
        SENTC,
        TRL,
        APPL,
        BAILAPP,
        APPLFL,
        NBL,
        RMD,
        PARAPP,
        PARDNY
    }

    public enum SentenceStatus {SENTENCED, UNSENTENCED}

    public enum ORDER_STATUS {ACTIVE, INACTIVE, PAYREQ}

    public enum ORDER_OUTCOME {
        ISSUED,
        WC,
        WE,
        SENT,
        BLSET,
        BLPOST,
        BLREV,
        BLBREACH,
        BOPOST,
        BOREV,
        BOBREACH,
        NBL,
        DISM,
        APPL
    }

    public enum CALCULATION_INITIATOR {PEND, INC, EXC}

    public enum SENTENCE_TYPE {LIFE, DEATH}

    public enum ADJUSTMENT_CLASSIFICATION {SYSG, MANUAL}

    public enum KEY_DATES {PDD, PED, SED}

}
