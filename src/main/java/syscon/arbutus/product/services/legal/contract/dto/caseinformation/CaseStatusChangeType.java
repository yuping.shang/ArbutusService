package syscon.arbutus.product.services.legal.contract.dto.caseinformation;

import java.io.Serializable;
import java.util.Date;

/**
 * The representation of case status change type.
 *
 * @author LHan
 */
public class CaseStatusChangeType implements Serializable {

    private static final long serialVersionUID = 63221915869949236L;

    private Boolean caseStatus;
    private String statusChangeReason;
    private Date statusChangeDate;

    /**
     * Indicates whether the case is active or inactive. This is specifically set by the user, not derived. (required)
     *
     * @return the caseStatus
     */
    public Boolean getCaseStatus() {
        return caseStatus;
    }

    /**
     * Indicates whether the case is active or inactive. This is specifically set by the user, not derived. (required)
     *
     * @param caseStatus the caseStatus to set
     */
    public void setCaseStatus(Boolean caseStatus) {
        this.caseStatus = caseStatus;
    }

    /**
     * Indicates the reason a case status was changed (optional).
     * A code value of CaseStatusChangeReason set.
     *
     * @return the statusChangeReason
     */
    public String getStatusChangeReason() {
        return statusChangeReason;
    }

    /**
     * Indicates the reason a case status was changed (optional).
     * A code value of CaseStatusChangeReason set.
     *
     * @param statusChangeReason the statusChangeReason to set
     */
    public void setStatusChangeReason(String statusChangeReason) {
        this.statusChangeReason = statusChangeReason;
    }

    /**
     * Indicates the date a case status was changed (optional)
     *
     * @return the statusChangeDate
     */
    public Date getStatusChangeDate() {
        return statusChangeDate;
    }

    /**
     * Indicates the date a case status was changed (optional)
     *
     * @param statusChangeDate the statusChangeDate to set
     */
    public void setStatusChangeDate(Date statusChangeDate) {
        this.statusChangeDate = statusChangeDate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseStatusChangeType [getCaseStatus()=" + getCaseStatus() + ", getStatusChangeReason()=" + getStatusChangeReason() + ", getStatusChangeDate()="
                + getStatusChangeDate() + "]";
    }

}
