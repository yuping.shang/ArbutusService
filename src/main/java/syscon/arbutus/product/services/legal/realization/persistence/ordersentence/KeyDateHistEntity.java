package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * KeyDateHistEntity for the history record of Sentence of Legal Service
 * Apply to the Active Supervision period of an offender.
 *
 * @author yshang
 * @DbComment LEG_OrdKeyDateHst 'KeyDateEntity for Sentence of Legal Service. Apply to the Active Supervision period of an offender.'
 * @since July 22, 2013
 */
//@Audited
@Entity
@Table(name = "LEG_OrdKeyDateHst")
public class KeyDateHistEntity implements Serializable {

    private static final long serialVersionUID = 2318449350271594547L;

    /**
     * @DbComment keyDateId 'Unique identification of an KeyDate instance'
     */
    @Id
    @Column(name = "keyDateId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDKEYDATEHST_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDKEYDATEHST_ID", sequenceName = "SEQ_LEG_ORDKEYDATEHST_ID", allocationSize = 1)
    private Long keyDateId;

    /**
     * @DbComment keyDateType 'The type of Key Date'
     */
    @Column(name = "keyDateType", nullable = false, length = 64)
    @MetaCode(set = MetaSet.KEY_DATES)
    private String keyDateType;

    /**
     * @DbComment keyDate 'Key Date'
     */
    @Column(name = "keyDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date keyDate;

    /**
     * @DbComment keyDateOverride 'Key Date Override'
     */
    @Column(name = "keyDateOverride", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date keyDateOverride;

    /**
     * @DbComment keyDateAdjust 'Key Date Adjust'
     */
    @Column(name = "keyDateAdjust", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date keyDateAdjust;

    /**
     * @DbComment overrideByStaffId 'The staff who override the Key Date'
     */
    @Column(name = "overrideByStaffId", nullable = true)
    private Long overrideByStaffId;

    /**
     * @DbComment overrideReason 'Override reason'
     */
    @Column(name = "overrideReason", nullable = true, length = 64)
    @MetaCode(set = MetaSet.KEY_DATE_OVERRIDE_REASON)
    private String overrideReason;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment hstId 'Uniquie Id of LEG_OrdSntKeyDateHst instance of LEG_OrdSntKeyDateHist table'
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "hstId", nullable = false)
    private SentenceKeyDateHistEntity sentenceKeyDate;

    /**
     * Constructor
     */
    public KeyDateHistEntity() {
        super();
    }

    public KeyDateHistEntity(Long keyDateId, String keyDateType, Date keyDate, Date keyDateOverride, Date keyDateAdjust, Long overrideByStaffId, String overrideReason,
            StampEntity stamp, SentenceKeyDateHistEntity sentenceKeyDate) {
        super();
        this.keyDateId = keyDateId;
        this.keyDateType = keyDateType;
        this.keyDate = keyDate;
        this.keyDateOverride = keyDateOverride;
        this.keyDateAdjust = keyDateAdjust;
        this.overrideByStaffId = overrideByStaffId;
        this.overrideReason = overrideReason;
        this.stamp = stamp;
        this.sentenceKeyDate = sentenceKeyDate;
    }

    /**
     * @return the keyDateId
     */
    public Long getKeyDateId() {
        return keyDateId;
    }

    /**
     * @param keyDateId the keyDateId to set
     */
    public void setKeyDateId(Long keyDateId) {
        this.keyDateId = keyDateId;
    }

    /**
     * @return the keyDateType
     */
    public String getKeyDateType() {
        return keyDateType;
    }

    /**
     * @param keyDateType the keyDateType to set
     */
    public void setKeyDateType(String keyDateType) {
        this.keyDateType = keyDateType;
    }

    /**
     * @return the keyDate
     */
    public Date getKeyDate() {
        return keyDate;
    }

    /**
     * @param keyDate the keyDate to set
     */
    public void setKeyDate(Date keyDate) {
        this.keyDate = keyDate;
    }

    /**
     * @return the keyDateOverride
     */
    public Date getKeyDateOverride() {
        return keyDateOverride;
    }

    /**
     * @param keyDateOverride the keyDateOverride to set
     */
    public void setKeyDateOverride(Date keyDateOverride) {
        this.keyDateOverride = keyDateOverride;
    }

    /**
     * @return the keyDateAdjust
     */
    public Date getKeyDateAdjust() {
        return keyDateAdjust;
    }

    /**
     * @param keyDateAdjust the keyDateAdjust to set
     */
    public void setKeyDateAdjust(Date keyDateAdjust) {
        this.keyDateAdjust = keyDateAdjust;
    }

    /**
     * @return the overrideByStaffId
     */
    public Long getOverrideByStaffId() {
        return overrideByStaffId;
    }

    /**
     * @param overrideByStaffId the overrideByStaffId to set
     */
    public void setOverrideByStaffId(Long overrideByStaffId) {
        this.overrideByStaffId = overrideByStaffId;
    }

    /**
     * @return the overrideReason
     */
    public String getOverrideReason() {
        return overrideReason;
    }

    /**
     * @param overrideReason the overrideReason to set
     */
    public void setOverrideReason(String overrideReason) {
        this.overrideReason = overrideReason;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the sentenceKeyDate
     */
    public SentenceKeyDateHistEntity getSentenceKeyDate() {
        return sentenceKeyDate;
    }

    /**
     * @param sentenceKeyDate the sentenceKeyDate to set
     */
    public void setSentenceKeyDate(SentenceKeyDateHistEntity sentenceKeyDate) {
        this.sentenceKeyDate = sentenceKeyDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (!(o instanceof KeyDateHistEntity)) {
			return false;
		}

        KeyDateHistEntity that = (KeyDateHistEntity) o;

        if (keyDateAdjust != null ? !keyDateAdjust.equals(that.keyDateAdjust) : that.keyDateAdjust != null) {
			return false;
		}
        if (keyDateType != null ? !keyDateType.equals(that.keyDateType) : that.keyDateType != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = keyDateType != null ? keyDateType.hashCode() : 0;
        result = 31 * result + (keyDateAdjust != null ? keyDateAdjust.hashCode() : 0);
        return result;
    }

    /* (non-Javadoc)
             * @see java.lang.Object#toString()
             */
    @Override
    public String toString() {
        return "KeyDateHistEntity [keyDateId=" + keyDateId + ", keyDateType=" + keyDateType + ", keyDate=" + keyDate + ", keyDateOverride=" + keyDateOverride
                + ", keyDateAdjust=" + keyDateAdjust + ", overrideByStaffId=" + overrideByStaffId + ", overrideReason=" + overrideReason + "]";
    }

}
