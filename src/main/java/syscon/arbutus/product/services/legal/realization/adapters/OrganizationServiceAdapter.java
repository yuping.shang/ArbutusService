package syscon.arbutus.product.services.legal.realization.adapters;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.organization.contract.dto.Organization;
import syscon.arbutus.product.services.organization.contract.interfaces.OrganizationService;
import syscon.arbutus.product.services.utility.ServiceAdapter;

public class OrganizationServiceAdapter {
    private static OrganizationService service;

    public OrganizationServiceAdapter() {
        super();
    }

    synchronized private static OrganizationService getService() {
        if (service == null) {
            service = (OrganizationService) ServiceAdapter.JNDILookUp(service, OrganizationService.class);
        }
        return service;
    }

    public Organization get(UserContext uc, Long organizationId) {
        if (organizationId == null) {
            throw new InvalidInputException("organizaiton id is null");
        }

        Organization ret = getService().get(uc, organizationId);
        return ret;
    }

    public String getOrganizationName(UserContext uc, Long organizationId) {
        return get(uc, organizationId).getOrganizationName();
    }

    public String getOrganizationAbbreviation(UserContext uc, Long organizationId) {
        return get(uc, organizationId).getOrganizationAbbreviation();
    }
}
