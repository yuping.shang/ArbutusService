package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the charge entity.
 *
 * @DbComment LEG_CHGChargeHist 'The charge history data table'
 */
//@Audited
@Entity
@Table(name = "LEG_CHGChargeHist")
public class ChargeHistEntity implements Serializable {

    private static final long serialVersionUID = -6616316134728914949L;

    /**
     * @DbComment ChargeHistoryId 'Unique id of a charge history instance'
     */
    @Id
    @Column(name = "ChargeHistoryId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGChargeHist_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGChargeHist_Id", sequenceName = "SEQ_LEG_CHGChargeHist_Id", allocationSize = 1)
    private Long chargeHistoryId;

    /**
     * @DbComment ChargeId 'Unique id of a charge instance'
     */
    @Column(name = "ChargeId", nullable = false)
    private Long chargeId;

    /**
     * @DbComment CaseInfoId 'The case information unique id. Refers to the Case Info instance'
     */
    @Column(name = "CaseInfoId", nullable = false)
    private Long caseInfoId;

    /**
     * @DbComment StatuteChargeId 'The statute charge configuration unique id. Refers to the Statute Charge Configuration Type'
     */
    @Column(name = "StatuteChargeId", nullable = false)
    private Long statuteChargeId;

    /**
     * @DbComment ChargeGroupId 'The charge group unique id. use to link the charge which charged with committing the same crime'
     */
    @Column(name = "ChargeGroupId", nullable = true)
    private Long chargeGroupId;

    /**
     * @DbComment ChargeCount 'A number of times a person is charged with committing the same crime.'
     */
    @Column(name = "ChargeCount", nullable = true)
    private Long chargeCount;

    //From charge Disposition type
    /**
     * @DbComment ChargeStatus 'The status of a charge (active, inactive)'
     */
    @Column(name = "ChargeStatus", nullable = false, length = 64)
    private String chargeStatus;

    //From charge Disposition type
    /**
     * @DbComment DispositionOutcome 'A result or outcome of the disposition.'
     */
    @Column(name = "DispositionOutcome", nullable = true, length = 64)
    private String dispositionOutcome;

    //From charge Disposition type
    /**
     * @DbComment DispositionDate 'Date of the disposition'
     */
    @Column(name = "DispositionDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dispositionDate;

    /**
     * @DbComment IsPrimary 'True if the charge is a serious charge, false otherwise, Default is false.'
     */
    @Column(name = "IsPrimary", nullable = false)
    private Boolean primary;

    /**
     * @DbComment OffenceStartDate 'Date the offence was committed'
     */
    @Column(name = "OffenceStartDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date offenceStartDate;

    /**
     * @DbComment OffenceEndDate 'Field is used when the exact offence date is not known, to reflect an offence committed date range'
     */
    @Column(name = "OffenceEndDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date offenceEndDate;

    /**
     * @DbComment ChargeFilingDate 'A date the charge was filed'
     */
    @Column(name = "ChargeFilingDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date chargeFilingDate;

    /**
     * @DbComment FacilityId 'Static Reference to the facility entity that filed the charge.'
     */
    @Column(name = "FacilityId", nullable = true)
    private Long facilityId;

    /**
     * @DbComment OrganizationId 'Static Reference to the organization entity that filed the charge.'
     */
    @Column(name = "OrganizationId", nullable = true)
    private Long organizationId;

    /**
     * @DbComment PersonIdentityId 'Static Reference to the PersonIdentity entity that filed the charge.'
     */
    @Column(name = "PersonIdentityId", nullable = true)
    private Long personIdentityId;

    /**
     * @DbComment ChargeLegalTextHistoryId 'Foreign key from table LEG_CHGLegalTextHist'
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ChargeLegalTextHistoryId")
    private ChargeLegalTextHistEntity legalText;

    /**
     * @DbComment ChargePleaHistoryId 'Foreign key from table LEG_CHGPleaHist'
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ChargePleaHistoryId")
    private ChargePleaHistEntity chargePlea;

    /**
     * @DbComment CommentUserId 'User ID who created/updated the comments'
     * @DbComment CommentDate 'The Date/Time when the comment was created'
     * @DbComment CommentText 'The comment text'
     */
    @Embedded
    private CommentEntity comment;

    /**
     * @DbComment SentenceId 'Id of sentence which is associated to'
     */
    @Column(name = "SentenceId", nullable = true)
    private Long sentenceId;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment ChargeAppealHistoryId 'Foreign key from table LEG_CHGAppealHist'
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ChargeAppealHistoryId")
    private ChargeAppealHistEntity chargeAppeal;

    @OneToMany(mappedBy = "chargeHistory", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<ChargeIdentifierHistEntity> chargeIdentifiers;

    @OneToMany(mappedBy = "chargeHistory", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<ChargeIndicatorHistEntity> chargeIndicators;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ChargeId", insertable = false, updatable = false)
    private ChargeEntity charge;

    /**
     * @return the chargeHistoryId
     */
    public Long getChargeHistoryId() {
        return chargeHistoryId;
    }

    /**
     * @param chargeHistoryId the chargeHistoryId to set
     */
    public void setChargeHistoryId(Long chargeHistoryId) {
        this.chargeHistoryId = chargeHistoryId;
    }

    /**
     * @return the chargeId
     */
    public Long getChargeId() {
        return chargeId;
    }

    /**
     * @param chargeId the chargeId to set
     */
    public void setChargeId(Long chargeId) {
        this.chargeId = chargeId;
    }

    /**
     * @return the caseInfoId
     */
    public Long getCaseInfoId() {
        return caseInfoId;
    }

    /**
     * @param caseInfoId the caseInfoId to set
     */
    public void setCaseInfoId(Long caseInfoId) {
        this.caseInfoId = caseInfoId;
    }

    /**
     * @return the statuteChargeId
     */
    public Long getStatuteChargeId() {
        return statuteChargeId;
    }

    /**
     * @param statuteChargeId the statuteChargeId to set
     */
    public void setStatuteChargeId(Long statuteChargeId) {
        this.statuteChargeId = statuteChargeId;
    }

    /**
     * @return the chargeGroupId
     */
    public Long getChargeGroupId() {
        return chargeGroupId;
    }

    /**
     * @param chargeGroupId the chargeGroupId to set
     */
    public void setChargeGroupId(Long chargeGroupId) {
        this.chargeGroupId = chargeGroupId;
    }

    /**
     * @return the chargeCount
     */
    public Long getChargeCount() {
        return chargeCount;
    }

    /**
     * @param chargeCount the chargeCount to set
     */
    public void setChargeCount(Long chargeCount) {
        this.chargeCount = chargeCount;
    }

    /**
     * @return the chargeStatus
     */
    public String getChargeStatus() {
        return chargeStatus;
    }

    /**
     * @param chargeStatus the chargeStatus to set
     */
    public void setChargeStatus(String chargeStatus) {
        this.chargeStatus = chargeStatus;
    }

    /**
     * @return the dispositionOutcome
     */
    public String getDispositionOutcome() {
        return dispositionOutcome;
    }

    /**
     * @param dispositionOutcome the dispositionOutcome to set
     */
    public void setDispositionOutcome(String dispositionOutcome) {
        this.dispositionOutcome = dispositionOutcome;
    }

    /**
     * @return the dispositionDate
     */
    public Date getDispositionDate() {
        return dispositionDate;
    }

    /**
     * @param dispositionDate the dispositionDate to set
     */
    public void setDispositionDate(Date dispositionDate) {
        this.dispositionDate = dispositionDate;
    }

    /**
     * @return the primary
     */
    public Boolean getPrimary() {
        return primary;
    }

    /**
     * @param primary the primary to set
     */
    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

    /**
     * @return the offenceStartDate
     */
    public Date getOffenceStartDate() {
        return offenceStartDate;
    }

    /**
     * @param offenceStartDate the offenceStartDate to set
     */
    public void setOffenceStartDate(Date offenceStartDate) {
        this.offenceStartDate = offenceStartDate;
    }

    /**
     * @return the offenceEndDate
     */
    public Date getOffenceEndDate() {
        return offenceEndDate;
    }

    /**
     * @param offenceEndDate the offenceEndDate to set
     */
    public void setOffenceEndDate(Date offenceEndDate) {
        this.offenceEndDate = offenceEndDate;
    }

    /**
     * @return the chargeFilingDate
     */
    public Date getChargeFilingDate() {
        return chargeFilingDate;
    }

    /**
     * @param chargeFilingDate the chargeFilingDate to set
     */
    public void setChargeFilingDate(Date chargeFilingDate) {
        this.chargeFilingDate = chargeFilingDate;
    }

    /**
     * @return the facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * @param facilityId the facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * @return the organizationId
     */
    public Long getOrganizationId() {
        return organizationId;
    }

    /**
     * @param organizationId the organizationId to set
     */
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * @return the personIdentityId
     */
    public Long getPersonIdentityId() {
        return personIdentityId;
    }

    /**
     * @param personIdentityId the personIdentityId to set
     */
    public void setPersonIdentityId(Long personIdentityId) {
        this.personIdentityId = personIdentityId;
    }

    /**
     * @return the legalText
     */
    public ChargeLegalTextHistEntity getLegalText() {
        return legalText;
    }

    /**
     * @param legalText the legalText to set
     */
    public void setLegalText(ChargeLegalTextHistEntity legalText) {
        this.legalText = legalText;
    }

    /**
     * @return the chargePlea
     */
    public ChargePleaHistEntity getChargePlea() {
        return chargePlea;
    }

    /**
     * @param chargePlea the chargePlea to set
     */
    public void setChargePlea(ChargePleaHistEntity chargePlea) {
        this.chargePlea = chargePlea;
    }

    /**
     * @return the chargeAppeal
     */
    public ChargeAppealHistEntity getChargeAppeal() {
        return chargeAppeal;
    }

    /**
     * @param chargeAppeal the chargeAppeal to set
     */
    public void setChargeAppeal(ChargeAppealHistEntity chargeAppeal) {
        this.chargeAppeal = chargeAppeal;
    }

    /**
     * @return the comment
     */
    public CommentEntity getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(CommentEntity comment) {
        this.comment = comment;
    }

    /**
     * @return the sentenceId
     */
    public Long getSentenceId() {
        return sentenceId;
    }

    /**
     * @param sentenceId the sentenceId to set
     */
    public void setSentenceId(Long sentenceId) {
        this.sentenceId = sentenceId;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the chargeIdentifiers
     */
    public Set<ChargeIdentifierHistEntity> getChargeIdentifiers() {
        if (chargeIdentifiers == null) {
            chargeIdentifiers = new HashSet<ChargeIdentifierHistEntity>();
        }
        return chargeIdentifiers;
    }

    /**
     * @param chargeIdentifiers the chargeIdentifiers to set
     */
    public void setChargeIdentifiers(Set<ChargeIdentifierHistEntity> chargeIdentifiers) {
        this.chargeIdentifiers = chargeIdentifiers;
    }

    /**
     * @return the chargeIndicators
     */
    public Set<ChargeIndicatorHistEntity> getChargeIndicators() {
        if (chargeIndicators == null) {
            chargeIndicators = new HashSet<ChargeIndicatorHistEntity>();
        }
        return chargeIndicators;
    }

    /**
     * @param chargeIndicators the chargeIndicators to set
     */
    public void setChargeIndicators(Set<ChargeIndicatorHistEntity> chargeIndicators) {
        this.chargeIndicators = chargeIndicators;
    }

    /**
     * @return the charge
     */
    public ChargeEntity getCharge() {
        return charge;
    }

    /**
     * @param charge the charge to set
     */
    public void setCharge(ChargeEntity charge) {
        this.charge = charge;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeHistEntity [chargeHistoryId=" + chargeHistoryId + ", chargeId=" + chargeId + ", caseInfoId=" + caseInfoId + ", statuteChargeId=" + statuteChargeId
                + ", groupChargeId=" + chargeGroupId + ", chargeCount=" + chargeCount + ", chargeStatus=" + chargeStatus + ", dispositionOutcome=" + dispositionOutcome
                + ", dispositionDate=" + dispositionDate + ", primary=" + primary + ", sentenceId=" + sentenceId + ", offenceStartDate=" + offenceStartDate
                + ", offenceEndDate=" + offenceEndDate + ", chargeFilingDate=" + chargeFilingDate + ", facilityId=" + facilityId + ", organizationId=" + organizationId
                + ", personIdentityId=" + personIdentityId + ", legalText=" + legalText + ", chargePlea=" + chargePlea + ", chargeAppeal=" + chargeAppeal
                + ", chargeIdentifiers=" + chargeIdentifiers + ", chargeIndicators=" + chargeIndicators + ", comment=" + comment + "]";
    }

}
