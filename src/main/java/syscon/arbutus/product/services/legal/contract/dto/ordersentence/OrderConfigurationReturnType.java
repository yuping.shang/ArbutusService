package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import java.io.Serializable;
import java.util.List;

/**
 * OrderConfigurationReturnType Return type for a list of OrderConfigurationTypes
 *
 * @author yshang
 */
public class OrderConfigurationReturnType implements Serializable {

    private static final long serialVersionUID = 2171975231935298983L;

    private List<OrderConfigurationType> orderConfigurations;
    private Long totalSize;

    public List<OrderConfigurationType> getOrderConfigurations() {
        return orderConfigurations;
    }

    public void setOrderConfigurations(List<OrderConfigurationType> orderConfigurations) {
        this.orderConfigurations = orderConfigurations;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    @Override
    public String toString() {
        return "OrderConfigurationReturnType [orderConfigurations=" + orderConfigurations + ", totalSize=" + totalSize + "]";
    }

}
