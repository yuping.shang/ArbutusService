package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

/**
 * TermSearchType for Condition of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 27, 2012
 */
@ArbutusConstraint(constraints = { "fromTermStartDate <= toTermStartDate", "fromTermEndDate <= toTermEndDate" })
public class TermSearchType implements Serializable {

    private static final long serialVersionUID = -737931375114522736L;

    private String termCategory;

    private String termName;

    private String termType;

    private String termStatus;

    private Long termSequenceNo;

    private Date fromTermStartDate;

    private Date toTermStartDate;

    private Date fromTermEndDate;

    private Date toTermEndDate;

    private Long years;

    private Long months;

    private Long weeks;

    private Long days;

    private Long hours;

    /**
     * Constructor
     */
    public TermSearchType() {
        super();
    }

    /**
     * Constructor
     *
     * @param termCategory      String, Required. The category of term (Custodial, Community).
     * @param termName          String, Required. The name of the term; Min, Max etc..
     * @param termType          Code, Required. The type of term (Duration, Intermittent, Suspended etc.). This will be used by the sentence calculation module.
     * @param termStatus        String, Required. The status of a term, will be used by sentence calculation logic. Pending/Included/Excluded. Only ‘Included’ terms will participate in sentence calculation.
     * @param termSequenceNo    Long, Optional. The order of execution of the term.
     * @param fromTermStartDate Date, Optional. The start date for the term.
     * @param toTermStartDate   Date, Optional. The start date for the term.
     * @param fromTermEndDate   Date, Optional. The end date for the term.
     * @param toTermEndDate     Date, Optional. The end date for the term.
     * @param years             Long, Optional. No. of years a sentence is to be served. Used only for duration and intermittent type.
     * @param months            Long, Optional. No. of months a sentence is to be served. Used only for duration and intermittent type.
     * @param weeks             Long, Optional. No. of weeks a sentence is to be served. Used only for duration and intermittent type.
     * @param days              Long, Optional. No. of days a sentence is to be served. Used only for duration and intermittent type.
     * @param hours             Long, Optional. No. of hours a sentence is to be served. Used only for duration and intermittent type.
     */
    public TermSearchType(String termCategory, String termName, String termType, String termStatus, Long termSequenceNo, Date fromTermStartDate, Date toTermStartDate,
            Date fromTermEndDate, Date toTermEndDate, Long years, Long months, Long weeks, Long days, Long hours) {
        super();
        this.termCategory = termCategory;
        this.termName = termName;
        this.termType = termType;
        this.termStatus = termStatus;
        this.termSequenceNo = termSequenceNo;
        this.fromTermStartDate = fromTermStartDate;
        this.toTermStartDate = toTermStartDate;
        this.fromTermEndDate = fromTermEndDate;
        this.toTermEndDate = toTermEndDate;
        this.years = years;
        this.months = months;
        this.weeks = weeks;
        this.days = days;
        this.hours = hours;
    }

    /**
     * Get Term Category
     * -- The category of term (Custodial, Community)
     *
     * @return the termCategory
     */
    public String getTermCategory() {
        return termCategory;
    }

    /**
     * Set Term Category
     * -- The category of term (Custodial, Community)
     *
     * @param termCategory the termCategory to set
     */
    public void setTermCategory(String termCategory) {
        this.termCategory = termCategory;
    }

    /**
     * Get Term Name
     * -- The name of the term; Min, Max etc..
     *
     * @return the termName
     */
    public String getTermName() {
        return termName;
    }

    /**
     * Set Term Name
     * -- -- The name of the term; Min, Max etc..
     *
     * @param termName the termName to set
     */
    public void setTermName(String termName) {
        this.termName = termName;
    }

    /**
     * Get Term Type
     * -- The type of term (Duration, Intermittent, Suspended etc.).
     * This will be used by the sentence calculation module.
     *
     * @return the termType
     */
    public String getTermType() {
        return termType;
    }

    /**
     * Set Term Type
     * -- The type of term (Duration, Intermittent, Suspended etc.).
     * This will be used by the sentence calculation module.
     *
     * @param termType the termType to set
     */
    public void setTermType(String termType) {
        this.termType = termType;
    }

    /**
     * Get Term Status
     * -- The status of a term, will be used by sentence calculation logic
     * Pending/Included/Excluded.
     * Only ‘Included’ terms will participate in sentence calculation.
     *
     * @return the termStatus
     */
    public String getTermStatus() {
        return termStatus;
    }

    /**
     * Set Term Status
     * -- The status of a term, will be used by sentence calculation logic
     * Pending/Included/Excluded.
     * Only ‘Included’ terms will participate in sentence calculation.
     *
     * @param termStatus the termStatus to set
     */
    public void setTermStatus(String termStatus) {
        this.termStatus = termStatus;
    }

    /**
     * Get Term Sequence Number
     * -- The order of execution of the term
     *
     * @return the termSequenceNo
     */
    public Long getTermSequenceNo() {
        return termSequenceNo;
    }

    /**
     * Set Term Sequence Number
     * -- The order of execution of the term
     *
     * @param termSequenceNo the termSequenceNo to set
     */
    public void setTermSequenceNo(Long termSequenceNo) {
        this.termSequenceNo = termSequenceNo;
    }

    /**
     * Get Term Start Date
     * -- The start date for the term
     *
     * @return the fromTermStartDate
     */
    public Date getFromTermStartDate() {
        return fromTermStartDate;
    }

    /**
     * Set Term Start Date
     * -- The start date for the term
     *
     * @param fromTermStartDate the fromTermStartDate to set
     */
    public void setFromTermStartDate(Date fromTermStartDate) {
        this.fromTermStartDate = fromTermStartDate;
    }

    /**
     * Get Term Start Date
     * -- The start date for the term
     *
     * @return the toTermStartDate
     */
    public Date getToTermStartDate() {
        return toTermStartDate;
    }

    /**
     * Set Term Start Date
     * -- The start date for the term
     *
     * @param toTermStartDate the toTermStartDate to set
     */
    public void setToTermStartDate(Date toTermStartDate) {
        this.toTermStartDate = toTermStartDate;
    }

    /**
     * Get Term End Date
     * -- The end date for the term
     *
     * @return the fromTermEndDate
     */
    public Date getFromTermEndDate() {
        return fromTermEndDate;
    }

    /**
     * Set Term End Date
     * -- The end date for the term
     *
     * @param fromTermEndDate the termEndDate to set
     */
    public void setFromTermEndDate(Date fromTermEndDate) {
        this.fromTermEndDate = fromTermEndDate;
    }

    /**
     * Get Term End Date
     * -- The end date for the term
     *
     * @return the toTermEndDate
     */
    public Date getToTermEndDate() {
        return toTermEndDate;
    }

    /**
     * Set Term End Date
     * -- The end date for the term
     *
     * @param toTermEndDate the toEndDate to set
     */
    public void setToTermEndDate(Date toTermEndDate) {
        this.toTermEndDate = toTermEndDate;
    }

    /**
     * Get Years
     * -- No. of years a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @return the years
     */
    public Long getYears() {
        return years;
    }

    /**
     * Set Years
     * -- No. of years a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @param years the years to set
     */
    public void setYears(Long years) {
        this.years = years;
    }

    /**
     * Get Months
     * -- No. of months a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @return the months
     */
    public Long getMonths() {
        return months;
    }

    /**
     * Set Months
     * -- No. of months a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @param months the months to set
     */
    public void setMonths(Long months) {
        this.months = months;
    }

    /**
     * Get Weeks
     * -- No. of weeks a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @return the weeks
     */
    public Long getWeeks() {
        return weeks;
    }

    /**
     * Set Weeks
     * -- No. of weeks a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @param weeks the weeks to set
     */
    public void setWeeks(Long weeks) {
        this.weeks = weeks;
    }

    /**
     * Get Days
     * -- No. of days a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @return the days
     */
    public Long getDays() {
        return days;
    }

    /**
     * Set Days
     * -- No. of days a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @param days the days to set
     */
    public void setDays(Long days) {
        this.days = days;
    }

    /**
     * Get Hours
     * -- No. of hours a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @return the hours
     */
    public Long getHours() {
        return hours;
    }

    /**
     * Set Hours
     * -- No. of hours a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @param hours the hours to set
     */
    public void setHours(Long hours) {
        this.hours = hours;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TermSearchType [termCategory=" + termCategory + ", termName=" + termName + ", termType=" + termType + ", termStatus=" + termStatus + ", termSequenceNo="
                + termSequenceNo + ", fromTermStartDate=" + fromTermStartDate + ", toTermStartDate=" + toTermStartDate + ", fromTermEndDate=" + fromTermEndDate
                + ", toTermEndDate=" + toTermEndDate + ", years=" + years + ", months=" + months + ", weeks=" + weeks + ", days=" + days + ", hours=" + hours + "]";
    }

}
