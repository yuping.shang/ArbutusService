package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;

/**
 * BailHistoryEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdOrderHst 'The Order History table for Order Sentence module of Legal service'
 * @since December 27, 2012
 */
//@Audited
@Entity
@DiscriminatorValue("BailHst")
public class BailHistoryEntity extends OrderHistoryEntity implements Serializable {

    private static final long serialVersionUID = -737931375118522731L;

    @ForeignKey(name = "Fk_LEG_OrdBailHst1")
    @OneToMany(mappedBy = "bail", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<BailAmountHistoryEntity> bailAmounts;

    /**
     * @DbComment bailRelationship 'The relationship of the bail amount specified. e.g. cash- surety DISAGGREGATE (Bail set amount will be either the cash amount or Surety  amount), Cash Surety AGGREGATE(Bail set amount will be cash amount + Surety  amount)'
     */
    @Column(name = "bailRelationship", nullable = true, length = 64) // business requirement is nullable = false
    private String bailRelationship;

    @ForeignKey(name = "Fk_LEG_OrdBailHst4")
    @OneToMany(mappedBy = "bail", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<BailPostedAmountHistEntity> bailPostedAmounts;

    /**
     * @DbComment bailPaymentReceiptNo 'Bill Payment Receipt Number for cash bail payment.'
     */
    @Column(name = "bailPaymentReceiptNo", nullable = true, length = 64)
    private String bailPaymentReceiptNo;

    /**
     * @DbComment bailRequirement 'A description of the bail requirement set at a court hearing.'
     */
    @Column(name = "bailRequirement", nullable = true, length = 128)
    private String bailRequirement;

    @ForeignKey(name = "Fk_LEG_OrdBailHst2")
    @OneToMany(mappedBy = "bail", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<BailPersonIdAssocHistEntity> bailerPersonIdentities;

    /**
     * @DbComment paHstId 'The amount of bond posted.'
     */
    @ForeignKey(name = "Fk_LEG_OrdBailHst5")
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "paHstId")
    private BondPostedAmountHistEntity bondPostedAmount;

    /**
     * @DbComment bondPaymentDescription 'A description of what an offender pays for a bond.'
     */
    @Column(name = "bondPaymentDescription", nullable = true, length = 512)
    private String bondPaymentDescription;

    @ForeignKey(name = "Fk_LEG_OrdBailHst3")
    @OneToMany(mappedBy = "bail", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<BondOrgAssocHistEntity> bondOrganizations;

    /**
     * @DbComment isBailAllowed 'Is bail allowed'
     */
    @Column(name = "isBailAllowed", nullable = true) // business requirement is nullable = false
    private Boolean isBailAllowed;

    /**
     * @return the bailAmounts
     */
    public Set<BailAmountHistoryEntity> getBailAmounts() {
        if (bailAmounts == null) {
			bailAmounts = new HashSet<BailAmountHistoryEntity>();
		}
        return bailAmounts;
    }

    /**
     * @param bailAmounts the bailAmounts to set
     */
    public void setBailAmounts(Set<BailAmountHistoryEntity> bailAmounts) {
        if (bailAmounts != null) {
            for (BailAmountHistoryEntity amount : bailAmounts) {
                amount.setBail(this);
            }
            this.bailAmounts = bailAmounts;
        } else {
			this.bailAmounts = new HashSet<BailAmountHistoryEntity>();
		}
    }

    /**
     * @return the bailRelationship
     */
    public String getBailRelationship() {
        return bailRelationship;
    }

    /**
     * @param bailRelationship the bailRelationship to set
     */
    public void setBailRelationship(String bailRelationship) {
        this.bailRelationship = bailRelationship;
    }

    /**
     * @return the bailPostedAmount
     */
    public Set<BailPostedAmountHistEntity> getBailPostedAmounts() {
        if (bailPostedAmounts == null) {
			bailPostedAmounts = new HashSet<BailPostedAmountHistEntity>();
		}
        return bailPostedAmounts;
    }

    /**
     * @param bailPostedAmounts the bailPostedAmount to set
     */
    public void setBailPostedAmounts(Set<BailPostedAmountHistEntity> bailPostedAmounts) {
        if (bailPostedAmounts != null) {
            for (BailPostedAmountHistEntity amount : bailPostedAmounts) {
                amount.setBail(this);
            }
            this.bailPostedAmounts = bailPostedAmounts;
        } else {
			this.bailPostedAmounts = new HashSet<BailPostedAmountHistEntity>();
		}
    }

    /**
     * @return the bailPaymentReceiptNo
     */
    public String getBailPaymentReceiptNo() {
        return bailPaymentReceiptNo;
    }

    /**
     * @param bailPaymentReceiptNo the bailPaymentReceiptNo to set
     */
    public void setBailPaymentReceiptNo(String bailPaymentReceiptNo) {
        this.bailPaymentReceiptNo = bailPaymentReceiptNo;
    }

    /**
     * @return the bailRequirement
     */
    public String getBailRequirement() {
        return bailRequirement;
    }

    /**
     * @param bailRequirement the bailRequirement to set
     */
    public void setBailRequirement(String bailRequirement) {
        this.bailRequirement = bailRequirement;
    }

    /**
     * @return the bailerPersonIdentities
     */
    public Set<BailPersonIdAssocHistEntity> getBailerPersonIdentities() {
        if (bailerPersonIdentities == null) {
			bailerPersonIdentities = new HashSet<BailPersonIdAssocHistEntity>();
		}
        return bailerPersonIdentities;
    }

    /**
     * @param bailerPersonIdentities the bailerPersonIdentities to set
     */
    public void setBailerPersonIdentities(Set<BailPersonIdAssocHistEntity> bailerPersonIdentities) {
        if (bailerPersonIdentities != null) {
            for (BailPersonIdAssocHistEntity assoc : bailerPersonIdentities) {
                assoc.setBail(this);
            }
            this.bailerPersonIdentities = bailerPersonIdentities;
        } else {
			this.bailerPersonIdentities = new HashSet<BailPersonIdAssocHistEntity>();
		}
    }

    /**
     * @return the bondPostedAmount
     */
    public BondPostedAmountHistEntity getBondPostedAmount() {
        return bondPostedAmount;
    }

    /**
     * @param bondPostedAmount the bondPostedAmount to set
     */
    public void setBondPostedAmount(BondPostedAmountHistEntity bondPostedAmount) {
        this.bondPostedAmount = bondPostedAmount;
    }

    /**
     * @return the bondPaymentDescription
     */
    public String getBondPaymentDescription() {
        return bondPaymentDescription;
    }

    /**
     * @param bondPaymentDescription the bondPaymentDescription to set
     */
    public void setBondPaymentDescription(String bondPaymentDescription) {
        this.bondPaymentDescription = bondPaymentDescription;
    }

    /**
     * @return the bondOrganizations
     */
    public Set<BondOrgAssocHistEntity> getBondOrganizations() {
        if (bondOrganizations == null) {
			bondOrganizations = new HashSet<BondOrgAssocHistEntity>();
		}
        return bondOrganizations;
    }

    /**
     * @param bondOrganizations the bondOrganizations to set
     */
    public void setBondOrganizations(Set<BondOrgAssocHistEntity> bondOrganizations) {
        if (bondOrganizations != null) {
            for (BondOrgAssocHistEntity assoc : bondOrganizations) {
                assoc.setBail(this);
            }
            this.bondOrganizations = bondOrganizations;
        } else {
			this.bondOrganizations = new HashSet<BondOrgAssocHistEntity>();
		}
    }

    /**
     * @return the isBailAllowed
     */
    public Boolean getIsBailAllowed() {
        return isBailAllowed;
    }

    /**
     * @param isBailAllowed the isBailAllowed to set
     */
    public void setIsBailAllowed(Boolean isBailAllowed) {
        this.isBailAllowed = isBailAllowed;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((bailAmounts == null) ? 0 : bailAmounts.hashCode());
        result = prime * result + ((bailPaymentReceiptNo == null) ? 0 : bailPaymentReceiptNo.hashCode());
        result = prime * result + ((bailPostedAmounts == null) ? 0 : bailPostedAmounts.hashCode());
        result = prime * result + ((bailRelationship == null) ? 0 : bailRelationship.hashCode());
        result = prime * result + ((bailRequirement == null) ? 0 : bailRequirement.hashCode());
        result = prime * result + ((bailerPersonIdentities == null) ? 0 : bailerPersonIdentities.hashCode());
        result = prime * result + ((bondOrganizations == null) ? 0 : bondOrganizations.hashCode());
        result = prime * result + ((bondPaymentDescription == null) ? 0 : bondPaymentDescription.hashCode());
        result = prime * result + ((bondPostedAmount == null) ? 0 : bondPostedAmount.hashCode());
        result = prime * result + ((isBailAllowed == null) ? 0 : isBailAllowed.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        BailHistoryEntity other = (BailHistoryEntity) obj;
        if (bailAmounts == null) {
            if (other.bailAmounts != null) {
				return false;
			}
        } else if (!bailAmounts.equals(other.bailAmounts)) {
			return false;
		}
        if (bailPaymentReceiptNo == null) {
            if (other.bailPaymentReceiptNo != null) {
				return false;
			}
        } else if (!bailPaymentReceiptNo.equals(other.bailPaymentReceiptNo)) {
			return false;
		}
        if (bailPostedAmounts == null) {
            if (other.bailPostedAmounts != null) {
				return false;
			}
        } else if (!bailPostedAmounts.equals(other.bailPostedAmounts)) {
			return false;
		}
        if (bailRelationship == null) {
            if (other.bailRelationship != null) {
				return false;
			}
        } else if (!bailRelationship.equals(other.bailRelationship)) {
			return false;
		}
        if (bailRequirement == null) {
            if (other.bailRequirement != null) {
				return false;
			}
        } else if (!bailRequirement.equals(other.bailRequirement)) {
			return false;
		}
        if (bailerPersonIdentities == null) {
            if (other.bailerPersonIdentities != null) {
				return false;
			}
        } else if (!bailerPersonIdentities.equals(other.bailerPersonIdentities)) {
			return false;
		}
        if (bondOrganizations == null) {
            if (other.bondOrganizations != null) {
				return false;
			}
        } else if (!bondOrganizations.equals(other.bondOrganizations)) {
			return false;
		}
        if (bondPaymentDescription == null) {
            if (other.bondPaymentDescription != null) {
				return false;
			}
        } else if (!bondPaymentDescription.equals(other.bondPaymentDescription)) {
			return false;
		}
        if (bondPostedAmount == null) {
            if (other.bondPostedAmount != null) {
				return false;
			}
        } else if (!bondPostedAmount.equals(other.bondPostedAmount)) {
			return false;
		}
        if (isBailAllowed == null) {
            if (other.isBailAllowed != null) {
				return false;
			}
        } else if (!isBailAllowed.equals(other.isBailAllowed)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "BailHistoryEntity [bailAmounts=" + bailAmounts + ", bailRelationship=" + bailRelationship + ", bailPostedAmounts=" + bailPostedAmounts
                + ", bailPaymentReceiptNo=" + bailPaymentReceiptNo + ", bailRequirement=" + bailRequirement + ", bailerPersonIdentities=" + bailerPersonIdentities
                + ", bondPostedAmount=" + bondPostedAmount + ", bondPaymentDescription=" + bondPaymentDescription + ", bondOrganizations=" + bondOrganizations
                + ", isBailAllowed=" + isBailAllowed + ", toString()=" + super.toString() + "]";
    }

}
