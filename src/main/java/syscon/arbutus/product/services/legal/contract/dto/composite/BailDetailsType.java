package syscon.arbutus.product.services.legal.contract.dto.composite;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import syscon.arbutus.product.services.legal.contract.dto.condition.ConditionType;
import syscon.arbutus.product.services.legal.contract.dto.ordersentence.BailAmountType;

/**
 * BailDetailsType for View Bail and Condition Details
 *
 * @author yshang
 * @since July 02, 2013
 */
public class BailDetailsType implements Serializable {

    private static final long serialVersionUID = -221944756672656043L;

    private Long bailId;

    private List<String> payeeNames;

    private Date postedDateTime;

    private String bailPaymentReceiptNo;

    private Set<BailAmountType> bailAmounts;

    private Set<BailAmountType> bailPostedAmounts;

    private String bailRequirement;

    private List<String> bondOrganizations;

    private BailAmountType bondPostedAmount;

    private String bondPaymentDescription;

    private String comments;

    private String bailStatus;

    private Date bailStatusDate;

    private String orderStatus;

    private List<ConditionType> conditions;

    /**
     * @return the bailId
     */
    public Long getBailId() {
        return bailId;
    }

    /**
     * @param bailId the bailId to set
     */
    public void setBailId(Long bailId) {
        this.bailId = bailId;
    }

    /**
     * Names of Bail Payee's
     *
     * @return the payeeNames
     */
    public List<String> getPayeeNames() {
        return payeeNames;
    }

    /**
     * @param payeeNames the payeeNames to set
     */
    public void setPayeeNames(List<String> payeeNames) {
        this.payeeNames = payeeNames;
    }

    /**
     * Bail/Bond posted date and time
     *
     * @return the postedDateTime
     */
    public Date getPostedDateTime() {
        return postedDateTime;
    }

    /**
     * @param postedDateTime the postedDateTime to set
     */
    public void setPostedDateTime(Date postedDateTime) {
        this.postedDateTime = postedDateTime;
    }

    /**
     * Bail Payment Receipt No
     *
     * @return the bailPaymentReceiptNo
     */
    public String getBailPaymentReceiptNo() {
        return bailPaymentReceiptNo;
    }

    /**
     * @param bailPaymentReceiptNo the bailPaymentReceiptNo to set
     */
    public void setBailPaymentReceiptNo(String bailPaymentReceiptNo) {
        this.bailPaymentReceiptNo = bailPaymentReceiptNo;
    }

    /**
     * Bail Amounts
     *
     * @return the bailAmounts
     */
    public Set<BailAmountType> getBailAmounts() {
        return bailAmounts;
    }

    /**
     * @param bailAmounts the bailAmounts to set
     */
    public void setBailAmounts(Set<BailAmountType> bailAmounts) {
        this.bailAmounts = bailAmounts;
    }

    /**
     * Bail Posted Amounts
     *
     * @return the bailPostedAmounts
     */
    public Set<BailAmountType> getBailPostedAmounts() {
        return bailPostedAmounts;
    }

    /**
     * @param bailPostedAmounts the bailPostedAmounts to set
     */
    public void setBailPostedAmounts(Set<BailAmountType> bailPostedAmounts) {
        this.bailPostedAmounts = bailPostedAmounts;
    }

    /**
     * Bail Requirement Description
     *
     * @return the bailRequirement
     */
    public String getBailRequirement() {
        return bailRequirement;
    }

    /**
     * @param bailRequirement the bailRequirement to set
     */
    public void setBailRequirement(String bailRequirement) {
        this.bailRequirement = bailRequirement;
    }

    /**
     * Name(s) of organization(s) that paid the bail
     *
     * @return the bondOrganizations
     */
    public List<String> getBondOrganizations() {
        return bondOrganizations;
    }

    /**
     * @param bondOrganizations the bondOrganizations to set
     */
    public void setBondOrganizations(List<String> bondOrganizations) {
        this.bondOrganizations = bondOrganizations;
    }

    /**
     * Bond Posted Amount
     *
     * @return the bondPostedAmount
     */
    public BailAmountType getBondPostedAmount() {
        return bondPostedAmount;
    }

    /**
     * @param bondPostedAmount the bondPostedAmount to set
     */
    public void setBondPostedAmount(BailAmountType bondPostedAmount) {
        this.bondPostedAmount = bondPostedAmount;
    }

    /**
     * Bond Payment Description
     *
     * @return the bondPaymentDescription
     */
    public String getBondPaymentDescription() {
        return bondPaymentDescription;
    }

    /**
     * @param bondPaymentDescription the bondPaymentDescription to set
     */
    public void setBondPaymentDescription(String bondPaymentDescription) {
        this.bondPaymentDescription = bondPaymentDescription;
    }

    /**
     * Comments
     *
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * Bail Status which is value of dispositionOutcome of DispositionType
     *
     * @return the bailStatus -- dispositionOutcome of DispositionType
     */
    public String getBailStatus() {
        return bailStatus;
    }

    /**
     * Bail Status which is the value of dispositionOutcome of DispositionType
     *
     * @param bailStatus the bailStatus to set
     */
    public void setBailStatus(String bailStatus) {
        this.bailStatus = bailStatus;
    }

    /**
     * Bail Status Date -- the date from dispositionDate of DispositionType
     * bailStatusDate -- dispositionDate of DispositionType
     *
     * @return the bailStatusDate
     */
    public Date getBailStatusDate() {
        return bailStatusDate;
    }

    /**
     * Bail Status Date -- the date from dispositionDate of DispositionType
     *
     * @param bailStatusDate the bailStatusDate to set
     */
    public void setBailStatusDate(Date bailStatusDate) {
        this.bailStatusDate = bailStatusDate;
    }

    /**
     * orderStatus which is the value of orderStatus of DispositionType
     *
     * @return the orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }

    /**
     * orderStatus which is the value of orderStatus of DispositionType
     *
     * @param orderStatus the orderStatus to set
     */
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * Conditions tied to the Bail
     *
     * @return the conditions
     */
    public List<ConditionType> getConditions() {
        return conditions;
    }

    /**
     * @param conditions the conditions to set
     */
    public void setConditions(List<ConditionType> conditions) {
        this.conditions = conditions;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "BailDetailsType [bailId=" + bailId + ", payeeNames=" + payeeNames + ", postedDateTime=" + postedDateTime + ", bailPaymentReceiptNo="
                + bailPaymentReceiptNo + ", bailAmounts=" + bailAmounts + ", bailPostedAmounts=" + bailPostedAmounts + ", bailRequirement=" + bailRequirement
                + ", bondOrganizations=" + bondOrganizations + ", bondPostedAmount=" + bondPostedAmount + ", bondPaymentDescription=" + bondPaymentDescription
                + ", comments=" + comments + ", conditions=" + conditions + "]";
    }

}
