package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * KeyDateEntity for Sentence of Legal Service
 * <p>Apply to the Active Supervision period of an offender.
 *
 * @author yshang
 * @DbComment LEG_OrdKeyDate 'KeyDateEntity for Sentence of Legal Service. Apply to the Active Supervision period of an offender.'
 * @since July 22, 2013
 */
@Audited
@Entity
@Table(name = "LEG_OrdKeyDate")
public class KeyDateEntity implements Serializable {

    private static final long serialVersionUID = -124273680311064606L;

    /**
     * @DbComment keyDateId 'Unique identification of an KeyDate instance'
     */
    @Id
    @Column(name = "keyDateId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDKEYDATE_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDKEYDATE_ID", sequenceName = "SEQ_LEG_ORDKEYDATE_ID", allocationSize = 1)
    private Long keyDateId;

    /**
     * @DbComment keyDateType 'The type of Key Date'
     */
    @Column(name = "keyDateType", nullable = false, length = 64)
    @MetaCode(set = MetaSet.KEY_DATES)
    private String keyDateType;

    /**
     * @DbComment keyDate 'Key Date'
     */
    @Column(name = "keyDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date keyDate;

    /**
     * @DbComment keyDateOverride 'Key Date Override'
     */
    @Column(name = "keyDateOverride", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date keyDateOverride;

    /**
     * @DbComment keyDateAdjust 'Key Date Adjust'
     */
    @Column(name = "keyDateAdjust", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date keyDateAdjust;

    /**
     * @DbComment overrideByStaffId 'The staff who override the Key Date'
     */
    @Column(name = "overrideByStaffId", nullable = true)
    private Long overrideByStaffId;

    /**
     * @DbComment overrideReason 'Override reason'
     */
    @Column(name = "overrideReason", nullable = true, length = 64)
    @MetaCode(set = MetaSet.KEY_DATE_OVERRIDE_REASON)
    private String overrideReason;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment supervisionId 'Unique identification of a SentenceKeyDate instance, foreign key to LEG_OrdSntKeyDate table'
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "supervisionId", nullable = false)
    private SentenceKeyDateEntity sentenceKeyDate;

    /**
     * Constructor
     */
    public KeyDateEntity() {
        super();
    }

    public KeyDateEntity(Long keyDateId, String keyDateType, Date keyDate, Date keyDateOverride, Date keyDateAdjust, Long overrideByStaffId, String overrideReason,
            StampEntity stamp) {
        super();
        this.keyDateId = keyDateId;
        this.keyDateType = keyDateType;
        this.keyDate = keyDate;
        this.keyDateOverride = keyDateOverride;
        this.keyDateAdjust = keyDateAdjust;
        this.overrideByStaffId = overrideByStaffId;
        this.overrideReason = overrideReason;
        this.stamp = stamp;
    }

    /**
     * @return the keyDateId
     */
    public Long getKeyDateId() {
        return keyDateId;
    }

    /**
     * @param keyDateId the keyDateId to set
     */
    public void setKeyDateId(Long keyDateId) {
        this.keyDateId = keyDateId;
    }

    /**
     * @return the keyDateType
     */
    public String getKeyDateType() {
        return keyDateType;
    }

    /**
     * @param keyDateType the keyDateType to set
     */
    public void setKeyDateType(String keyDateType) {
        this.keyDateType = keyDateType;
    }

    /**
     * @return the keyDate
     */
    public Date getKeyDate() {
        return keyDate;
    }

    /**
     * @param keyDate the keyDate to set
     */
    public void setKeyDate(Date keyDate) {
        this.keyDate = keyDate;
    }

    /**
     * @return the keyDateOverride
     */
    public Date getKeyDateOverride() {
        return keyDateOverride;
    }

    /**
     * @param keyDateOverride the keyDateOverride to set
     */
    public void setKeyDateOverride(Date keyDateOverride) {
        this.keyDateOverride = keyDateOverride;
    }

    /**
     * @return the keyDateAdjust
     */
    public Date getKeyDateAdjust() {
        return keyDateAdjust;
    }

    /**
     * @param keyDateAdjust the keyDateAdjust to set
     */
    public void setKeyDateAdjust(Date keyDateAdjust) {
        this.keyDateAdjust = keyDateAdjust;
    }

    /**
     * @return the overrideByStaffId
     */
    public Long getOverrideByStaffId() {
        return overrideByStaffId;
    }

    /**
     * @param overrideByStaffId the overrideByStaffId to set
     */
    public void setOverrideByStaffId(Long overrideByStaffId) {
        this.overrideByStaffId = overrideByStaffId;
    }

    /**
     * @return the overrideReason
     */
    public String getOverrideReason() {
        return overrideReason;
    }

    /**
     * @param overrideReason the overrideReason to set
     */
    public void setOverrideReason(String overrideReason) {
        this.overrideReason = overrideReason;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the sentenceKeyDate
     */
    public SentenceKeyDateEntity getSentenceKeyDate() {
        return sentenceKeyDate;
    }

    /**
     * @param sentenceKeyDate the sentenceKeyDate to set
     */
    public void setSentenceKeyDate(SentenceKeyDateEntity sentenceKeyDate) {
        this.sentenceKeyDate = sentenceKeyDate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
	 */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((keyDate == null) ? 0 : keyDate.hashCode());
        result = prime * result + ((keyDateAdjust == null) ? 0 : keyDateAdjust.hashCode());
        result = prime * result + ((keyDateId == null) ? 0 : keyDateId.hashCode());
        result = prime * result + ((keyDateOverride == null) ? 0 : keyDateOverride.hashCode());
        result = prime * result + ((keyDateType == null) ? 0 : keyDateType.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        KeyDateEntity other = (KeyDateEntity) obj;
        if (keyDate == null) {
            if (other.keyDate != null) {
				return false;
			}
        } else if (!keyDate.equals(other.keyDate)) {
			return false;
		}
        if (keyDateAdjust == null) {
            if (other.keyDateAdjust != null) {
				return false;
			}
        } else if (!keyDateAdjust.equals(other.keyDateAdjust)) {
			return false;
		}
        if (keyDateId == null) {
            if (other.keyDateId != null) {
				return false;
			}
        } else if (!keyDateId.equals(other.keyDateId)) {
			return false;
		}
        if (keyDateOverride == null) {
            if (other.keyDateOverride != null) {
				return false;
			}
        } else if (!keyDateOverride.equals(other.keyDateOverride)) {
			return false;
		}
        if (keyDateType == null) {
            if (other.keyDateType != null) {
				return false;
			}
        } else if (!keyDateType.equals(other.keyDateType)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "KeyDateEntity [keyDateId=" + keyDateId + ", keyDateType=" + keyDateType + ", keyDate=" + keyDate + ", keyDateOverride=" + keyDateOverride
                + ", keyDateAdjust=" + keyDateAdjust + ", overrideByStaffId=" + overrideByStaffId + ", overrideReason=" + overrideReason + "]";
    }

}
