package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import java.io.Serializable;

/**
 * IntermittentScheduleSearchType for Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 27, 2012
 */
public class IntermittentScheduleSearchType implements Serializable {

    private static final long serialVersionUID = -737431375114582739L;
    private String dayOfWeek;
    private TimeValue fromStartTime;
    private TimeValue toStartTime;
    private TimeValue fromEndTime;
    private TimeValue toEndTime;

    /**
     * Constructor
     */
    public IntermittentScheduleSearchType() {
        super();
    }

    /**
     * Constructor
     *
     * @param dayOfWeek     String, Required. Day of the Week.
     * @param fromStartTime IntermittentScheduleType.TimeValue, Optional. Start time on the day of the week.
     * @param toStartTime   IntermittentScheduleType.TimeValue, Optional. Start time on the day of the week.
     * @param fromEndTime   IntermittentScheduleType.TimeValue, Optional. End time on the day of the week.
     * @param toEndTime     IntermittentScheduleType.TimeValue, Optional. End time on the day of the week.
     */
    public IntermittentScheduleSearchType(String dayOfWeek, TimeValue fromStartTime, TimeValue toStartTime, TimeValue fromEndTime, TimeValue toEndTime) {
        super();
        this.dayOfWeek = dayOfWeek;
        this.fromStartTime = fromStartTime;
        this.toStartTime = toStartTime;
        this.fromEndTime = fromEndTime;
        this.toEndTime = toEndTime;
    }

    /**
     * Day of the Week
     *
     * @return the dayOfWeek
     */
    public String getDayOfWeek() {
        return dayOfWeek;
    }

    /**
     * Set Day of the Week
     *
     * @param dayOfWeek the dayOfWeek to set
     */
    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    /**
     * Get From Start Time
     * -- Start time on the day of the week.
     *
     * @return the fromStartTime
     */
    public TimeValue getFromStartTime() {
        return fromStartTime;
    }

    /**
     * Set From Start Time
     * -- Start time on the day of the week.
     *
     * @param fromStartTime the fromStartTime to set
     */
    public void setFromStartTime(TimeValue fromStartTime) {
        this.fromStartTime = fromStartTime;
    }

    /**
     * Get To Start Time
     * -- Start time on the day of the week.
     *
     * @return the toStartTime
     */
    public TimeValue getToStartTime() {
        return toStartTime;
    }

    /**
     * Set To Start Time
     * -- Start time on the day of the week.
     *
     * @param toStartTime the toStartTime to set
     */
    public void setToStartTime(TimeValue toStartTime) {
        this.toStartTime = toStartTime;
    }

    /**
     * Get From End Time
     * -- End time on the day of the week.
     *
     * @return the fromEndTime
     */
    public TimeValue getFromEndTime() {
        return fromEndTime;
    }

    /**
     * Set From End Time
     * -- End time on the day of the week.
     *
     * @param fromEndTime the fromEndTime to set
     */
    public void setFromEndTime(TimeValue fromEndTime) {
        this.fromEndTime = fromEndTime;
    }

    /**
     * Get To End Time
     * -- End time on the day of the week.
     *
     * @return the toEndTime
     */
    public TimeValue getToEndTime() {
        return toEndTime;
    }

    /**
     * Set To End Time
     * -- End time on the day of the week.
     *
     * @param toEndTime the toEndTime to set
     */
    public void setToEndTime(TimeValue toEndTime) {
        this.toEndTime = toEndTime;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "IntermittentScheduleSearchType [dayOfWeek=" + dayOfWeek + ", fromStartTime=" + fromStartTime + ", toStartTime=" + toStartTime + ", fromEndTime="
                + fromEndTime + ", toEndTime=" + toEndTime + "]";
    }

    /**
     * Inner Class
     */
    public class TimeValue implements Serializable {

        private static final long serialVersionUID = -737931375114572739L;

        private final Long hour, minute, second;

        public TimeValue(Long hour, Long minute, Long second) {
            super();
            if (!(hour != null && hour >= 0 && hour < 24)) {
				throw new IllegalArgumentException("Input was: " + hour == null ? "null" : hour + "to make hour: " + hour);
			}
            this.hour = hour;

            if (!(minute != null && minute >= 0 && minute < 60)) {
				throw new IllegalArgumentException("Input was: " + minute == null ? "null" : minute + "to make minute: " + minute);
			}
            this.minute = minute;

            if (!(second != null && second >= 0 && second < 60)) {
				throw new IllegalArgumentException("Input was: " + second == null ? "null" : second + "to make second: " + second);
			}
            this.second = second == null ? 0L : second;
        }

        /**
         * The hour in the range 0 through 24.
         *
         * @return the hour
         */
        public Long hour() {
            return hour;
        }

        /**
         * The minute in the range 0 through 59.
         *
         * @return the minute
         */
        public Long minute() {
            return minute;
        }

        /**
         * The second in the range 0 through 59.
         *
         * @return the second
         */
        public Long second() {
            return second;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return String.format("%02d:%02d:%02d", hour, minute, second);
        }
    }

}
