package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

/**
 * BailAmountSearchType for Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 28, 2012
 */
public class BailAmountSearchType implements Serializable {

    private static final long serialVersionUID = 3066921084655923705L;

    private String bail;
    private BigDecimal bailAmount;
    private Set<Long> chargeAssociations;

    /**
     * Constructor
     */
    public BailAmountSearchType() {
        super();
    }

    /**
     * Constructor
     * <p>At least one argument must be provided.
     *
     * @param bail               String. The type of bail i.e., cash, surety, property etc..
     * @param bailAmount         BigDecimal. The amount of bail set for each type.
     * @param chargeAssociations Set&lt;Long>. Set of charges that relate to this bail amount.
     */
    public BailAmountSearchType(String bail, BigDecimal bailAmount, Set<Long> chargeAssociations) {
        super();
        this.bail = bail;
        this.bailAmount = bailAmount;
        this.chargeAssociations = chargeAssociations;
    }

    /**
     * Get Bail
     * -- The type of bail i.e., cash, surety, property etc.
     *
     * @return the bail
     */
    public String getBail() {
        return bail;
    }

    /**
     * Set Bail
     * -- The type of bail i.e., cash, surety, property etc.
     *
     * @param bail the bail to set
     */
    public void setBail(String bail) {
        this.bail = bail;
    }

    /**
     * Get Bail Amount
     * -- The amount of bail set for each type
     *
     * @return the bailAmount
     */
    public BigDecimal getBailAmount() {
        return bailAmount;
    }

    /**
     * Set Bail Amount
     * -- The amount of bail set for each type
     *
     * @param bailAmount the bailAmount to set
     */
    public void setBailAmount(BigDecimal bailAmount) {
        this.bailAmount = bailAmount;
    }

    /**
     * Get Charge Associations
     * -- List of charges that relate to this bail amount
     *
     * @return the chargeAssociations
     */
    public Set<Long> getChargeAssociations() {
        return chargeAssociations;
    }

    /**
     * Set Charge Associations
     * -- List of charges that relate to this bail amount
     *
     * @param chargeAssociations the chargeAssociations to set
     */
    public void setChargeAssociations(Set<Long> chargeAssociations) {
        this.chargeAssociations = chargeAssociations;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bail == null) ? 0 : bail.hashCode());
        result = prime * result + ((bailAmount == null) ? 0 : bailAmount.hashCode());
        result = prime * result + ((chargeAssociations == null) ? 0 : chargeAssociations.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        BailAmountSearchType other = (BailAmountSearchType) obj;
        if (bail == null) {
            if (other.bail != null) {
				return false;
			}
        } else if (!bail.equals(other.bail)) {
			return false;
		}
        if (bailAmount == null) {
            if (other.bailAmount != null) {
				return false;
			}
        } else if (!bailAmount.equals(other.bailAmount)) {
			return false;
		}
        if (chargeAssociations == null) {
            if (other.chargeAssociations != null) {
				return false;
			}
        } else if (!chargeAssociations.equals(other.chargeAssociations)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "BailAmountSearchType [bail=" + bail + ", bailAmount=" + bailAmount + ", chargeAssociations=" + chargeAssociations + "]";
    }

}
