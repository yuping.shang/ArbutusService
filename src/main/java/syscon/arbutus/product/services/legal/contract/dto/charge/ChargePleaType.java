package syscon.arbutus.product.services.legal.contract.dto.charge;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * The representation of the charge plea type
 */
public class ChargePleaType implements Serializable {

    private static final long serialVersionUID = -4347853461178270967L;
    @NotNull
    private String pleaCode;
    @Size(max = 512, message = "max length 512")
    private String pleaComment;

    /**
     * Default constructor
     */
    public ChargePleaType() {
    }

    /**
     * Constructor
     *
     * @param pleaCode    String - A kind of plea.
     * @param pleaComment String - Comments regarding the plea (optional)
     */
    public ChargePleaType(@NotNull String pleaCode, @Size(max = 512, message = "max length 512") String pleaComment) {
        this.pleaCode = pleaCode;
        this.pleaComment = pleaComment;
    }

    /**
     * Gets the value of the pleaCode property. It is a reference code of ChargePlea set.
     *
     * @return String - A kind of plea.
     */
    public String getPleaCode() {
        return pleaCode;
    }

    /**
     * Sets the value of the pleaCode property. It is a reference code of ChargePlea set.
     *
     * @param pleaCode String - A kind of plea.
     */
    public void setPleaCode(String pleaCode) {
        this.pleaCode = pleaCode;
    }

    /**
     * Gets the value of the pleaComment property.
     *
     * @return String - Comments regarding the plea
     */
    public String getPleaComment() {
        return pleaComment;
    }

    /**
     * Sets the value of the pleaComment property.
     *
     * @param pleaComment String - Comments regarding the plea
     */
    public void setPleaComment(String pleaComment) {
        this.pleaComment = pleaComment;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargePleaType [pleaCode=" + pleaCode + ", pleaComment=" + pleaComment + "]";
    }

}
