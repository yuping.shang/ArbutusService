package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * OrderHistoryEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdOrderHst 'Order History table for Order Sentence module of Legal service'
 * @DbComment LEG_OrdOrderHst.DTYPE 'Discriminator of Order(Bail, Legal Order, Sentence or Warrant Detainer'
 * @since December 21, 2012
 */
//@Audited
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "LEG_OrdOrderHst")
public class OrderHistoryEntity implements Serializable {

    private static final long serialVersionUID = -737921355114542783L;

    /**
     * @DbComment hstId 'Unique identification of an order history recorder'
     */
    @Id
    @Column(name = "hstId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDORDERHST_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDORDERHST_ID", sequenceName = "SEQ_LEG_ORDORDERHST_ID", allocationSize = 1)
    private Long historyId;

    /**
     * @DbComment orderId 'Unique identification of an order instance'
     */
    @Column(name = "orderId", nullable = false)
    private Long orderId;

    /**
     * @DbComment ojSupervisionId 'Supervision Id from Outside Jurisdiction, , for Outside Jurisdiction only'
     */
    @Column(name = "ojSupervisionId", nullable = true)
    private Long ojSupervisionId;

    /**
     * @DbComment orderClassfication 'The order classification. This defines the type of Order Sentence of Legal.'
     */
    @Column(name = "orderClassfication", nullable = false, length = 64)
    private String orderClassification;

    /**
     * @DbComment orderType 'The client defined order type that maps to the order classification.'
     */
    @Column(name = "orderType", nullable = false, length = 64)
    private String orderType;

    /**
     * @DbComment orderSubType 'The sub type of an order. For example a want, warrant or detainer.'
     */
    @Column(name = "orderSubType", nullable = true, length = 64)
    private String orderSubType;

    /**
     * @DbComment orderCategory 'The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.'
     */
    @Column(name = "orderCategory", nullable = false, length = 64)
    private String orderCategory;

    /**
     * @DbComment orderNumber 'User specified order number.'
     */
    @Column(name = "orderNumber", nullable = true, length = 64)
    private String orderNumber;

    /**
     * @DbComment dispositionId 'Foreign key from table LEG_OrdDisposHst'
     */
    @ForeignKey(name = "Fk_LEG_OrdOrderHst1")
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "dispositionId")
    private DispositionHistoryEntity orderDisposition;

    /**
     * @DbComment CommentUserId 'User ID who created/updated the comments'
     * @DbComment CommentDate 'The Date/Time when the comment was created'
     * @DbComment CommentText 'The comment text'
     */
    @Embedded
    private CommentEntity comments;

    /**
     * @DbComment orderIssuanceDate 'The date an order was issued'
     */
    @Column(name = "orderIssuanceDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderIssuanceDate;

    /**
     * @DbComment orderReceivedDate 'The date an order was received'
     */
    @Column(name = "orderReceivedDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderReceivedDate;

    /**
     * @DbComment orderStartDate 'The date/time an order starts(becomes valid).'
     */
    @Column(name = "orderStartDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderStartDate;

    /**
     * @DbComment orderExpirationDate 'The date an order expires'
     */
    @Column(name = "orderExpirationDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderExpirationDate;

    @ForeignKey(name = "Fk_LEG_OrdOrderHst2")
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<CaseActivityInitOrderHstEntity> caseActivityInitiatedOrderAssociations;

    @ForeignKey(name = "Fk_LEG_OrdOrderHst3")
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<OrderInitCaseActivityHistEntity> orderInitiatedCaseActivityAssociations;

    /**
     * @DbComment isHoldingOrder 'If true then the order is a holding document, false otherwise.'
     */
    @Column(name = "isHoldingOrder", nullable = true)
    private Boolean isHoldingOrder;

    /**
     * @DbComment isSchedulingNeeded 'If true, scheduling is needed for the order, false otherwise.'
     */
    @Column(name = "isSchedulingNeeded", nullable = true)
    private Boolean isSchedulingNeeded;

    /**
     * @DbComment hasCharges 'If true, the order has charges related, false otherwise.'
     */
    @Column(name = "hasCharges", nullable = true)
    private Boolean hasCharges;

    /**
     * @DbComment issuingAgency 'The source agency that issues the order, this could be a facility or an organization.'
     */
    @ForeignKey(name = "Fk_LEG_OrdOrderHst4")
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "issuingAgency")
    private NotificationHistoryEntity issuingAgency;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'Version for Hibernate use'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     * Constructor
     */
    public OrderHistoryEntity() {
        super();
    }

    public OrderHistoryEntity(Long historyId, Long orderId, String orderClassification, String orderType, String orderSubType, String orderCategory, String orderNumber,
            DispositionHistoryEntity orderDisposition, CommentEntity comments, Date orderIssuanceDate, Date orderReceivedDate, Date orderStartDate,
            Date orderExpirationDate, Set<CaseActivityInitOrderHstEntity> caseActivityInitiatedOrderAssociations,
            Set<OrderInitCaseActivityHistEntity> orderInitiatedCaseActivityAssociations, Boolean isHoldingOrder, Boolean isSchedulingNeeded, Boolean hasCharges,
            NotificationHistoryEntity issuingAgency, StampEntity stamp) {
        super();
        this.historyId = historyId;
        this.orderId = orderId;
        this.orderClassification = orderClassification;
        this.orderType = orderType;
        this.orderSubType = orderSubType;
        this.orderCategory = orderCategory;
        this.orderNumber = orderNumber;
        this.orderDisposition = orderDisposition;
        this.comments = comments;
        this.orderIssuanceDate = orderIssuanceDate;
        this.orderReceivedDate = orderReceivedDate;
        this.orderStartDate = orderStartDate;
        this.orderExpirationDate = orderExpirationDate;
        this.caseActivityInitiatedOrderAssociations = caseActivityInitiatedOrderAssociations;
        this.orderInitiatedCaseActivityAssociations = orderInitiatedCaseActivityAssociations;
        this.isHoldingOrder = isHoldingOrder;
        this.isSchedulingNeeded = isSchedulingNeeded;
        this.hasCharges = hasCharges;
        this.issuingAgency = issuingAgency;
        this.stamp = stamp;
    }

    /**
     * @return the historyId
     */
    public Long getHistoryId() {
        return historyId;
    }

    /**
     * @param historyId the historyId to set
     */
    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    /**
     * @return the orderId
     */
    public Long getOrderId() {
        return orderId;
    }

    /**
     * @param orderId the orderId to set
     */
    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getOjSupervisionId() {
        return ojSupervisionId;
    }

    public void setOjSupervisionId(Long ojSupervisionId) {
        this.ojSupervisionId = ojSupervisionId;
    }

    /**
     * @return the orderClassification
     */
    public String getOrderClassification() {
        return orderClassification;
    }

    /**
     * @param orderClassification the orderClassification to set
     */
    public void setOrderClassification(String orderClassification) {
        this.orderClassification = orderClassification;
    }

    /**
     * @return the orderType
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * @param orderType the orderType to set
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    /**
     * @return the orderSubType
     */
    public String getOrderSubType() {
        return orderSubType;
    }

    /**
     * @param orderSubType the orderSubType to set
     */
    public void setOrderSubType(String orderSubType) {
        this.orderSubType = orderSubType;
    }

    /**
     * @return the orderCategory
     */
    public String getOrderCategory() {
        return orderCategory;
    }

    /**
     * @param orderCategory the orderCategory to set
     */
    public void setOrderCategory(String orderCategory) {
        this.orderCategory = orderCategory;
    }

    /**
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * @param orderNumber the orderNumber to set
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * @return the orderDisposition
     */
    public DispositionHistoryEntity getOrderDisposition() {
        return orderDisposition;
    }

    /**
     * @param orderDisposition the orderDisposition to set
     */
    public void setOrderDisposition(DispositionHistoryEntity orderDisposition) {
        this.orderDisposition = orderDisposition;
    }

    /**
     * @return the comments
     */
    public CommentEntity getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(CommentEntity comments) {
        this.comments = comments;
    }

    /**
     * @return the orderIssuanceDate
     */
    public Date getOrderIssuanceDate() {
        return orderIssuanceDate;
    }

    /**
     * @param orderIssuanceDate the orderIssuanceDate to set
     */
    public void setOrderIssuanceDate(Date orderIssuanceDate) {
        this.orderIssuanceDate = orderIssuanceDate;
    }

    /**
     * @return the orderReceivedDate
     */
    public Date getOrderReceivedDate() {
        return orderReceivedDate;
    }

    /**
     * @param orderReceivedDate the orderReceivedDate to set
     */
    public void setOrderReceivedDate(Date orderReceivedDate) {
        this.orderReceivedDate = orderReceivedDate;
    }

    /**
     * @return the orderStartDate
     */
    public Date getOrderStartDate() {
        return orderStartDate;
    }

    /**
     * @param orderStartDate the orderStartDate to set
     */
    public void setOrderStartDate(Date orderStartDate) {
        this.orderStartDate = orderStartDate;
    }

    /**
     * @return the orderExpirationDate
     */
    public Date getOrderExpirationDate() {
        return orderExpirationDate;
    }

    /**
     * @param orderExpirationDate the orderExpirationDate to set
     */
    public void setOrderExpirationDate(Date orderExpirationDate) {
        this.orderExpirationDate = orderExpirationDate;
    }

    /**
     * @return the caseActivityInitiatedOrderAssociations
     */
    public Set<CaseActivityInitOrderHstEntity> getCaseActivityInitiatedOrderAssociations() {
        if (caseActivityInitiatedOrderAssociations == null) {
			caseActivityInitiatedOrderAssociations = new HashSet<CaseActivityInitOrderHstEntity>();
		}
        return caseActivityInitiatedOrderAssociations;
    }

    /**
     * @param caseActivityInitiatedOrderAssociations the caseActivityInitiatedOrderAssociations to set
     */
    public void setCaseActivityInitiatedOrderAssociations(Set<CaseActivityInitOrderHstEntity> caseActivityInitiatedOrderAssociations) {
        if (caseActivityInitiatedOrderAssociations != null) {
            for (CaseActivityInitOrderHstEntity caseActivity : caseActivityInitiatedOrderAssociations) {
                caseActivity.setOrder(this);

            }
            this.caseActivityInitiatedOrderAssociations = caseActivityInitiatedOrderAssociations;
        } else {
			this.caseActivityInitiatedOrderAssociations = new HashSet<CaseActivityInitOrderHstEntity>();
		}
    }

    /**
     * @return the orderInitiatedCaseActivityAssociations
     */
    public Set<OrderInitCaseActivityHistEntity> getOrderInitiatedCaseActivityAssociations() {
        if (orderInitiatedCaseActivityAssociations == null) {
			orderInitiatedCaseActivityAssociations = new HashSet<OrderInitCaseActivityHistEntity>();
		}
        return orderInitiatedCaseActivityAssociations;
    }

    /**
     * @param orderInitiatedCaseActivityAssociations the orderInitiatedCaseActivityAssociations to set
     */
    public void setOrderInitiatedCaseActivityAssociations(Set<OrderInitCaseActivityHistEntity> orderInitiatedCaseActivityAssociations) {
        if (orderInitiatedCaseActivityAssociations != null) {
            for (OrderInitCaseActivityHistEntity caseActivity : orderInitiatedCaseActivityAssociations) {
                caseActivity.setOrder(this);
            }
            this.orderInitiatedCaseActivityAssociations = orderInitiatedCaseActivityAssociations;
        } else {
			this.orderInitiatedCaseActivityAssociations = new HashSet<OrderInitCaseActivityHistEntity>();
		}
    }

    /**
     * @return the isHoldingOrder
     */
    public Boolean getIsHoldingOrder() {
        return isHoldingOrder;
    }

    /**
     * @param isHoldingOrder the isHoldingOrder to set
     */
    public void setIsHoldingOrder(Boolean isHoldingOrder) {
        this.isHoldingOrder = isHoldingOrder;
    }

    /**
     * @return the isSchedulingNeeded
     */
    public Boolean getIsSchedulingNeeded() {
        return isSchedulingNeeded;
    }

    /**
     * @param isSchedulingNeeded the isSchedulingNeeded to set
     */
    public void setIsSchedulingNeeded(Boolean isSchedulingNeeded) {
        this.isSchedulingNeeded = isSchedulingNeeded;
    }

    /**
     * @return the hasCharges
     */
    public Boolean getHasCharges() {
        return hasCharges;
    }

    /**
     * @param hasCharges the hasCharges to set
     */
    public void setHasCharges(Boolean hasCharges) {
        this.hasCharges = hasCharges;
    }

    /**
     * @return the issuingAgency
     */
    public NotificationHistoryEntity getIssuingAgency() {
        return issuingAgency;
    }

    /**
     * @param issuingAgency the issuingAgency to set
     */
    public void setIssuingAgency(NotificationHistoryEntity issuingAgency) {
        this.issuingAgency = issuingAgency;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((caseActivityInitiatedOrderAssociations == null) ? 0 : caseActivityInitiatedOrderAssociations.hashCode());
        result = prime * result + ((hasCharges == null) ? 0 : hasCharges.hashCode());
        result = prime * result + ((historyId == null) ? 0 : historyId.hashCode());
        result = prime * result + ((isHoldingOrder == null) ? 0 : isHoldingOrder.hashCode());
        result = prime * result + ((isSchedulingNeeded == null) ? 0 : isSchedulingNeeded.hashCode());
        result = prime * result + ((issuingAgency == null) ? 0 : issuingAgency.hashCode());
        result = prime * result + ((orderCategory == null) ? 0 : orderCategory.hashCode());
        result = prime * result + ((orderClassification == null) ? 0 : orderClassification.hashCode());
        result = prime * result + ((orderDisposition == null) ? 0 : orderDisposition.hashCode());
        result = prime * result + ((orderExpirationDate == null) ? 0 : orderExpirationDate.hashCode());
        result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
        result = prime * result + ((ojSupervisionId == null) ? 0 : ojSupervisionId.hashCode());
        result = prime * result + ((orderInitiatedCaseActivityAssociations == null) ? 0 : orderInitiatedCaseActivityAssociations.hashCode());
        result = prime * result + ((orderIssuanceDate == null) ? 0 : orderIssuanceDate.hashCode());
        result = prime * result + ((orderNumber == null) ? 0 : orderNumber.hashCode());
        result = prime * result + ((orderReceivedDate == null) ? 0 : orderReceivedDate.hashCode());
        result = prime * result + ((orderStartDate == null) ? 0 : orderStartDate.hashCode());
        result = prime * result + ((orderSubType == null) ? 0 : orderSubType.hashCode());
        result = prime * result + ((orderType == null) ? 0 : orderType.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        OrderHistoryEntity other = (OrderHistoryEntity) obj;
        if (caseActivityInitiatedOrderAssociations == null) {
            if (other.caseActivityInitiatedOrderAssociations != null) {
				return false;
			}
        } else if (!caseActivityInitiatedOrderAssociations.equals(other.caseActivityInitiatedOrderAssociations)) {
			return false;
		}
        if (hasCharges == null) {
            if (other.hasCharges != null) {
				return false;
			}
        } else if (!hasCharges.equals(other.hasCharges)) {
			return false;
		}
        if (historyId == null) {
            if (other.historyId != null) {
				return false;
			}
        } else if (!historyId.equals(other.historyId)) {
			return false;
		}
        if (isHoldingOrder == null) {
            if (other.isHoldingOrder != null) {
				return false;
			}
        } else if (!isHoldingOrder.equals(other.isHoldingOrder)) {
			return false;
		}
        if (isSchedulingNeeded == null) {
            if (other.isSchedulingNeeded != null) {
				return false;
			}
        } else if (!isSchedulingNeeded.equals(other.isSchedulingNeeded)) {
			return false;
		}
        if (issuingAgency == null) {
            if (other.issuingAgency != null) {
				return false;
			}
        } else if (!issuingAgency.equals(other.issuingAgency)) {
			return false;
		}
        if (orderCategory == null) {
            if (other.orderCategory != null) {
				return false;
			}
        } else if (!orderCategory.equals(other.orderCategory)) {
			return false;
		}
        if (orderClassification == null) {
            if (other.orderClassification != null) {
				return false;
			}
        } else if (!orderClassification.equals(other.orderClassification)) {
			return false;
		}
        if (orderDisposition == null) {
            if (other.orderDisposition != null) {
				return false;
			}
        } else if (!orderDisposition.equals(other.orderDisposition)) {
			return false;
		}
        if (orderExpirationDate == null) {
            if (other.orderExpirationDate != null) {
				return false;
			}
        } else if (!orderExpirationDate.equals(other.orderExpirationDate)) {
			return false;
		}
        if (orderId == null) {
            if (other.orderId != null) {
				return false;
			}
        } else if (!orderId.equals(other.orderId)) {
			return false;
		}
        if (ojSupervisionId == null) {
            if (other.ojSupervisionId != null) {
				return false;
			}
        } else if (!ojSupervisionId.equals(other.ojSupervisionId)) {
			return false;
		}
        if (orderInitiatedCaseActivityAssociations == null) {
            if (other.orderInitiatedCaseActivityAssociations != null) {
				return false;
			}
        } else if (!orderInitiatedCaseActivityAssociations.equals(other.orderInitiatedCaseActivityAssociations)) {
			return false;
		}
        if (orderIssuanceDate == null) {
            if (other.orderIssuanceDate != null) {
				return false;
			}
        } else if (!orderIssuanceDate.equals(other.orderIssuanceDate)) {
			return false;
		}
        if (orderNumber == null) {
            if (other.orderNumber != null) {
				return false;
			}
        } else if (!orderNumber.equals(other.orderNumber)) {
			return false;
		}
        if (orderReceivedDate == null) {
            if (other.orderReceivedDate != null) {
				return false;
			}
        } else if (!orderReceivedDate.equals(other.orderReceivedDate)) {
			return false;
		}
        if (orderStartDate == null) {
            if (other.orderStartDate != null) {
				return false;
			}
        } else if (!orderStartDate.equals(other.orderStartDate)) {
			return false;
		}
        if (orderSubType == null) {
            if (other.orderSubType != null) {
				return false;
			}
        } else if (!orderSubType.equals(other.orderSubType)) {
			return false;
		}
        if (orderType == null) {
            if (other.orderType != null) {
				return false;
			}
        } else if (!orderType.equals(other.orderType)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "OrderHistoryEntity [historyId=" + historyId + ", orderId=" + orderId + ", ojSupervisionId=" + ojSupervisionId + ", orderClassification="
                + orderClassification + ", orderType=" + orderType + ", orderSubType=" + orderSubType + ", orderCategory=" + orderCategory + ", orderNumber="
                + orderNumber + ", orderDisposition=" + orderDisposition + ", comments=" + comments + ", orderIssuanceDate=" + orderIssuanceDate + ", orderReceivedDate="
                + orderReceivedDate + ", orderStartDate=" + orderStartDate + ", orderExpirationDate=" + orderExpirationDate + ", caseActivityInitiatedOrderAssociations="
                + caseActivityInitiatedOrderAssociations + ", orderInitiatedCaseActivityAssociations=" + orderInitiatedCaseActivityAssociations + ", isHoldingOrder="
                + isHoldingOrder + ", isSchedulingNeeded=" + isSchedulingNeeded + ", hasCharges=" + hasCharges + ", issuingAgency=" + issuingAgency + "]";
    }

}
