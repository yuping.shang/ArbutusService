package syscon.arbutus.product.services.legal.contract.dto.charge;

import java.io.Serializable;
import java.util.List;

/**
 * The representation of the jurisdiction configurations return type
 *
 * @author byu
 */
public class JurisdictionConfigsReturnType implements Serializable {

    private static final long serialVersionUID = 2465241198955413422L;

    private List<JurisdictionConfigType> jurisdictionConfigs;
    private Long totalSize;

    /**
     * Gets the value of the jurisdictionConfigs property.
     *
     * @return a list of jurisdiction configuration instances
     */
    public List<JurisdictionConfigType> getJurisdictionConfigs() {
        return jurisdictionConfigs;
    }

    /**
     * Sets the value of the jurisdictionConfigs property.
     *
     * @param jurisdictionConfigs a list of jurisdiction configuration instances
     */
    public void setJurisdictionConfigs(List<JurisdictionConfigType> jurisdictionConfigs) {
        this.jurisdictionConfigs = jurisdictionConfigs;
    }

    /**
     * Gets the value of the totalSize property.
     *
     * @return the total size of search result
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * Sets the value of the totalSize property.
     *
     * @param totalSize the total size of search result
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "JurisdictionConfigsReturnType [jurisdictionConfigs=" + jurisdictionConfigs + ", totalSize=" + totalSize + "]";
    }

}
