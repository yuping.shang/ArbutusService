package syscon.arbutus.product.services.legal.realization.persistence.caseinformation;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.Associable;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the module association entity of CaseInfo Instance.
 *
 * @DbComment LEG_CIModuleAssc 'The module association table of CaseInfo Instance'
 * @DbComment LEG_CIModuleAssc.AssociationId 'Unique id of the association record'
 * @DbComment LEG_CIModuleAssc.FromIdentifier 'The case information instance Id'
 * @DbComment LEG_CIModuleAssc.ToClass 'The association module that is referenced'
 * @DbComment LEG_CIModuleAssc.ToIdentifier 'The unique id of the Association To Object'
 * @DbComment LEG_CIModuleAssc.CreateUserId 'User ID who created the object'
 * @DbComment LEG_CIModuleAssc.CreateDateTime 'Date and time when the object was created'
 * @DbComment LEG_CIModuleAssc.InvocationContext 'Invocation context when the create/update action called'
 * @DbComment LEG_CIModuleAssc.ModifyUserId 'User ID who last updated the object'
 * @DbComment LEG_CIModuleAssc.ModifyDateTime 'Date and time when the object was last updated'
 */
@Audited
@Entity
@Table(name = "LEG_CIModuleAssc")
public class CaseInfoModuleAssociationEntity implements Serializable, Associable {
    private static final long serialVersionUID = 3373761969013472182L;

    @Id
    @Column(name = "AssociationId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CIModuleAssc_Id")
    @SequenceGenerator(name = "SEQ_LEG_CIModuleAssc_Id", sequenceName = "SEQ_LEG_CIModuleAssc_Id", allocationSize = 1)
    private Long associationId;

    @Column(name = "ToClass", nullable = false, length = 64)
    private String toClass;

    @Column(name = "ToIdentifier", nullable = false)
    private Long toIdentifier;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FromIdentifier", referencedColumnName = "CaseInfoId", nullable = false)
    private CaseInfoEntity caseInfo;

    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * Default constructor
     */
    public CaseInfoModuleAssociationEntity() {
    }

    /**
     * Constructor
     *
     * @param associationId  Long
     * @param fromIdentifier Long
     * @param toClass        String
     * @param toIdentifier   Long
     * @param privileges     Set<AssociationPrivilegeEntity>
     */
    public CaseInfoModuleAssociationEntity(Long associationId, Long fromIdentifier, String toClass, Long toIdentifier) {
        super();
        this.associationId = associationId;
        this.toClass = toClass;
        this.toIdentifier = toIdentifier;
    }

    /**
     * Gets the value of the associationId property
     *
     * @return Long
     */
    public Long getAssociationId() {
        return associationId;
    }

    /**
     * Sets the value of the associationId property
     *
     * @param associationId Long
     */
    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    /**
     * Gets the value of the toClass property
     *
     * @return String
     */
    public String getToClass() {
        return toClass;
    }

    /**
     * Sets the value of the toClass property
     *
     * @param toClass String
     */
    public void setToClass(String toClass) {
        this.toClass = toClass;
    }

    /**
     * Gets the value of the toIdentifier property
     *
     * @return Long
     */
    public Long getToIdentifier() {
        return toIdentifier;
    }

    /**
     * Sets the value of the toIdentifier property
     *
     * @param toIdentifier Long
     */
    public void setToIdentifier(Long toIdentifier) {
        this.toIdentifier = toIdentifier;
    }

    /**
     * Gets the value of the stamp property
     *
     * @return StampEntity
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * Sets the value of the stamp property
     *
     * @param stamp StampEntity
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the caseInfo
     */
    public CaseInfoEntity getCaseInfo() {
        return caseInfo;
    }

    /**
     * @param caseInfo the caseInfo to set
     */
    public void setCaseInfo(CaseInfoEntity caseInfo) {
        this.caseInfo = caseInfo;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((caseInfo.getCaseInfoId() == null) ? 0 : caseInfo.getCaseInfoId().hashCode());
        result = prime * result + ((toClass == null) ? 0 : toClass.hashCode());
        result = prime * result + ((toIdentifier == null) ? 0 : toIdentifier.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        CaseInfoModuleAssociationEntity other = (CaseInfoModuleAssociationEntity) obj;
        if (caseInfo == null) {
            if (other.caseInfo != null) {
				return false;
			}
        } else if (!caseInfo.getCaseInfoId().equals(other.caseInfo.getCaseInfoId())) {
			return false;
		}
        if (toClass == null) {
            if (other.toClass != null) {
				return false;
			}
        } else if (!toClass.equals(other.toClass)) {
			return false;
		}
        if (toIdentifier == null) {
            if (other.toIdentifier != null) {
				return false;
			}
        } else if (!toIdentifier.equals(other.toIdentifier)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseInfoModuleAssociationEntity [associationId=" + associationId + ", toClass=" + toClass + ", toIdentifier=" + toIdentifier + ", stamp=" + stamp
                + ", caseInfo=" + caseInfo + "]";
    }

}
