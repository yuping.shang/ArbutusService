package syscon.arbutus.product.services.legal.contract.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.AssociationType;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.TextLength;

/**
 * Utility function for type validation
 */
public class ValidationUtil {

    /**
     * Returns true if data specified is empty.
     *
     * @param data Boolean
     * @return Boolean
     */
    public static boolean isEmpty(Boolean data) {

        if (data == null) {
			return true;
		}

        return false;
    }

    /**
     * Returns true if data specified is empty.
     *
     * @param data Long
     * @return boolean
     */
    public static boolean isEmpty(Long data) {

        if (data == null) {
			return true;
		}

        return false;
    }

    /**
     * Returns true if data specified is empty.
     *
     * @param data BigDecimal
     * @return boolean
     */
    public static boolean isEmpty(BigDecimal data) {

        if (data == null) {
			return true;
		}

        return false;
    }

    /**
     * Returns true if data specified is empty.
     *
     * @param data String
     * @return boolean
     */
    public static boolean isEmpty(String data) {

        if ((data == null) || (data.trim().length() == 0)) {
			return true;
		}

        return false;
    }

    /**
     * Returns true if data specified is empty.
     *
     * @param data Date
     * @return boolean
     */
    public static boolean isEmpty(Date data) {

        if (data == null) {
			return true;
		}

        return false;
    }

    /**
     * Returns true if data specified is empty.
     *
     * @param data CodeType
     * @return boolean
     */
    public static boolean isEmpty(CodeType data) {

        if ((data == null) || isEmpty(data.getCode())) {
			return true;
		}

        return false;
    }

    /**
     * Returns true if data specified is empty.
     *
     * @param data AssociationType
     * @return boolean
     */
    public static boolean isEmpty(AssociationType data) {

        if (data == null || isEmpty(data.getToIdentifier())) {
			return true;
		}

        return false;
    }

    /**
     * Returns true if data specified is empty.
     *
     * @param data Set
     * @return boolean
     */
    @SuppressWarnings("rawtypes")
    public static boolean isEmpty(Set data) {

        if ((data == null) || data.isEmpty()) {
			return true;
		}

        return false;
    }

    /**
     * Returns true if data specified is empty.
     *
     * @param data Set
     * @return boolean
     */
    @SuppressWarnings("rawtypes")
    public static boolean isEmpty(List data) {

        if ((data == null) || data.isEmpty()) {
			return true;
		}

        return false;
    }

    /**
     * Check two dates, return false if toDate before the fromDate
     *
     * @param fromDate Date
     * @param toDate   Date
     * @return boolean
     */
    public static boolean isValid(Date fromDate, Date toDate) {

        //toDate cannot before the fromDate
        if (!isEmpty(fromDate) && !isEmpty(toDate) && toDate.before(fromDate)) {
			return false;
		}

        return true;
    }

    /**
     * Verify for a set of CodeType objects, return false if one of codeType object is not valid.
     *
     * @param codes Set<CodeType>
     * @return boolean
     */
    public static boolean isValid(Set<CodeType> codes) {

        if (codes != null) {
            for (CodeType code : codes) {
                if (code == null || !code.isValid()) {
					return false;
				}
            }
        }

        return true;
    }

    /**
     * Returns true if the length of a string is within given limit.
     *
     * @param data
     * @param len
     * @return boolean
     */
    public static boolean isWithinLimit(String data, int len) {

        if (data == null) {
			return true;
		}

        if (data.length() > len) {
			return false;
		}

        return true;
    }

    /**
     * To validate the Code value.
     *
     * @param data
     * @return boolean
     */
    public static boolean isValidCode(String data) {

        if (data != null && data.length() > TextLength.LENGTH_SMALL.longValue()) {
			return false;
		}

        return true;
    }

}
