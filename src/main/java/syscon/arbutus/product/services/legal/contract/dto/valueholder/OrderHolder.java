package syscon.arbutus.product.services.legal.contract.dto.valueholder;

import java.io.Serializable;

//JSON Holder for facility population data
public class OrderHolder implements Serializable {

    public String issuedate;
    public String expirydate;
    public String description;
    public String type;
    public String subtype;
    public String issueagency;
    public String outcome;
    public String status;
    public String orderIdentication;
    public OrderHolder() {
        super();
        // TODO Auto-generated constructor stub
    }

}