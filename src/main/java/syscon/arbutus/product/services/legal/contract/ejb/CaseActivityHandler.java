package syscon.arbutus.product.services.legal.contract.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.SessionContext;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.common.adapters.DataSecurityServiceAdapter;
import syscon.arbutus.product.services.core.common.adapters.LegalServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.datasecurity.contract.dto.DataSecurityRecordType;
import syscon.arbutus.product.services.datasecurity.contract.dto.ReferenceSet.DataSecurityRecordTypeEnum;
import syscon.arbutus.product.services.datasecurity.contract.dto.ReferenceSet.EntityTypeEnum;
import syscon.arbutus.product.services.datasecurity.contract.ejb.DataSecurityHelper;
import syscon.arbutus.product.services.legal.contract.dto.LegalModule;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CaseActivitySearchType;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivitiesReturnType;
import syscon.arbutus.product.services.legal.contract.dto.caseactivity.CourtActivityType;
import syscon.arbutus.product.services.legal.contract.dto.valueholder.ConversionUtils;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.*;
import syscon.arbutus.product.services.legal.realization.persistence.caseaffiliation.CaseAffiliationEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.CaseInfoEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.OrderEntity;
import syscon.arbutus.product.services.legal.realization.util.ActivityStatusCode;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * CaseActivityHandler is handling case activity related functions.
 *
 * @author lhan
 */
public class CaseActivityHandler {

    private static final Logger log = LoggerFactory.getLogger(CaseActivityHandler.class);

    private SessionContext context;
    private Session session;
    private int searchMaxLimit = 200;

    public CaseActivityHandler(SessionContext context, Session session) {
        super();
        this.context = context;
        this.session = session;
    }

    public CaseActivityHandler(SessionContext context, Session session, int searchMaxLimit) {
        super();
        this.context = context;
        this.session = session;
        this.searchMaxLimit = searchMaxLimit;
    }

    private static boolean isOutcomeChanged(String newActivityOutcome, String oldActivityOutcome) {

        if (newActivityOutcome == null) {
            if (oldActivityOutcome != null) {
                return true;
            }
        } else if (!newActivityOutcome.equals(oldActivityOutcome)) {
            return true;
        }

        return false;
    }

    /**
     * Implement {@link syscon.arbutus.product.services.legal.contract.interfaces.CaseActivity#createCaseActivity(UserContext uc, CourtActivityType courtActivity)}.
     */
    public CourtActivityType create(UserContext uc, CourtActivityType courtActivity) {

        printDebugLog("create: begin");

        CourtActivityType rtnType;

        ValidationHelper.validate(courtActivity);

        rtnType = createCourtActivity(uc, courtActivity);

        printDebugLog("create: end");

        return rtnType;
    }

    public CourtActivityType get(UserContext uc, Long courtActivityId) {
        printDebugLog("get: begin");

        CourtActivityType rtnType;

        //
        if (courtActivityId == null) {
            String message = "courtActivityId == null";
            log.error(message);
            throw new InvalidInputException(message);
        }

        rtnType = getCourtActivity(uc, courtActivityId, true);

        printDebugLog("get: end");

        return rtnType;
    }

    public CourtActivityType update(UserContext uc, CourtActivityType courtActivity) {
        printDebugLog("update: begin");

        CourtActivityType rtnType;

        //DTO's null, isWellFormed and isValid check.implements CaseInfo
        if (courtActivity == null || courtActivity.getCaseActivityIdentification() == null) {
            String message = "courtActivity or courtActivity id is null";
            log.error(message);
            throw new InvalidInputException(message);
        }

        ValidationHelper.validate(courtActivity);

        rtnType = updateCourtActivity(uc, courtActivity);

        printDebugLog("update: end");

        return rtnType;
    }

    public Long delete(UserContext uc, Long caseActivityId) {
        printDebugLog("delete: begin");

        Long rtnCode = ReturnCode.Success.returnCode();

        //id can not be null.
        if (caseActivityId == null) {
            String message = "caseActivityId is null";
            log.error(message);
            throw new InvalidInputException(message);
        }

        rtnCode = deleteCaseActivityEntity(uc, caseActivityId);

        printDebugLog("delete: end");

        return rtnCode;
    }

    public CourtActivityType cancelCourtActivity(UserContext uc, Long caseActivityId) {
        printDebugLog("cancelCourtActivity: begin");

        CourtActivityType rtnType;

        //id can not be null.
        if (caseActivityId == null) {
            String message = "caseActivityId is null";
            log.error(message);
            throw new InvalidInputException(message);
        }

        rtnType = cancelCourtActivityEntity(uc, caseActivityId);

        printDebugLog("cancelCourtActivity: end");

        return rtnType;
    }

    public List<CourtActivityType> search(UserContext uc, Set<Long> ids, CaseActivitySearchType searchType, Boolean historyFlag) {
        printDebugLog("search: begin");

        List<CourtActivityType> rtnType;

        verifySearchType(searchType);

        Set<CourtActivityType> ret = search(uc, searchType, historyFlag, ids);

        rtnType = new ArrayList<CourtActivityType>(ret);

        printDebugLog("search: end");

        return rtnType;
    }

    public CourtActivitiesReturnType searchCourtActivity(UserContext uc, CaseActivitySearchType search, Long startIndex, Long resultSize, String resultOrder) {
        printDebugLog("paging search: begin");

        verifySearchType(search);

        if (resultSize == null || resultSize > searchMaxLimit) {
            resultSize = new Long(searchMaxLimit);
        }

        Criteria criteria = createCourtActivitySearchCriteria(uc, search, null);

        Long totalSize = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
        criteria.setProjection(null);
        criteria.setResultTransformer(Criteria.ROOT_ENTITY);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());
            criteria.setMaxResults(resultSize.intValue());
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                criteria.addOrder(ord);
            }
        }

        @SuppressWarnings("unchecked") List<CourtActivityEntity> entities = criteria.list();

        Set<CourtActivityEntity> types = new HashSet<CourtActivityEntity>(entities);

        Set<CourtActivityType> ret = CaseActivityHelper.toCourtActivityTypeSet(types, true, null);

        CourtActivitiesReturnType rtn = new CourtActivitiesReturnType();
        rtn.setCourtActivities(new ArrayList<CourtActivityType>(ret));
        rtn.setTotalSize(totalSize);

        printDebugLog("paging search: end");

        return rtn;
    }

    public List<CourtActivityType> retrieveCaseActivities(UserContext uc, Long caseId, String activityStatus) {
        printDebugLog("retrieveCaseActivities: begin");

        List<CourtActivityType> rtnType;

        //
        if (caseId == null || (activityStatus != null && !ActivityStatusCode.contain(activityStatus))) {
            throw new InvalidInputException("caseId is null or activity status is not valid");
        } else {

            Set<CourtActivityType> ret = retrieveByCaseAssociation(uc, caseId, activityStatus);
            rtnType = new ArrayList<CourtActivityType>(ret);
        }

        printDebugLog("retrieveCaseActivities: end");

        return rtnType;
    }

    public List<CourtActivityType> retrieveCaseActivityHistory(UserContext uc, Long caseActivityId, Date fromHistoryDate, Date toHistoryDate) {
        printDebugLog("retrieveCaseActivityHistory: begin");

        List<CourtActivityType> rtnType;

        //
        if (caseActivityId == null) {
            throw new InvalidInputException("caseActivityId can not be null");
        } else {

            Set<CourtActivityType> ret = getHistory(uc, caseActivityId, fromHistoryDate, toHistoryDate);
            rtnType = new ArrayList<CourtActivityType>(ret);
        }

        printDebugLog("retrieveCaseActivityHistory: end");

        return rtnType;
    }

    public Long getCount(UserContext uc) {
        printDebugLog("getCount: begin");

        Long rtnCount = new Long(0l);

        rtnCount = BeanHelper.getCount(uc, context, session, "caseActivityId", CaseActivityEntity.class);

        printDebugLog("getCount: end");

        return rtnCount;
    }

    public Set<Long> getAll(UserContext uc) {
        printDebugLog("getAll: begin");

        Set<Long> rtnIds = new HashSet<Long>();

        rtnIds = BeanHelper.getAll(uc, context, session, "caseActivityId", CaseActivityEntity.class);

        printDebugLog("getAll: end");

        return rtnIds;

    }

    //+++++++++++++++++++++++++++++++Private methods++++++++++++++++++++++++++++++++++++++++++++++

    public StampType getStamp(UserContext uc, Long caseActivityId) {
        printDebugLog("getStamp: begin");

        StampType rtnType = null;

        //
        if (caseActivityId == null) {
            throw new ArbutusRuntimeException("Case Activity id is null");
        }

        CourtActivityEntity entity = getCourtActivityEntity(uc, caseActivityId);

        rtnType = BeanHelper.toStamp(entity.getStamp());

        printDebugLog("getStamp: end");

        return rtnType;
    }

    /**
     * Load service properties such as searchMaxLimit. This will be called when the bean is created.
     */
    @SuppressWarnings({ "rawtypes" })
    @PostConstruct
    private void initialize() {

        ClassLoader loader = this.getClass().getClassLoader();

        try {
            ResourceBundle rb = ResourceBundle.getBundle("service", Locale.getDefault(), loader);
            for (Enumeration keys = rb.getKeys(); keys.hasMoreElements(); ) {
                String key = (String) keys.nextElement();
                if ("searchMaxLimit".equalsIgnoreCase(key)) {
                    String value = rb.getString(key);
                    searchMaxLimit = Integer.parseInt(value);
                    return;
                }
            }
        } catch (Exception ex) {
            log.debug(ex.toString());
        }

    }

    /**
     * Create an object of CourtActivityType and all it's associated data.
     *
     * @param uc            UserContext
     * @param courtActivity CourtActivityType
     * @return the created object of CourtActivityType
     */
    private CourtActivityType createCourtActivity(UserContext uc, CourtActivityType courtActivity) {

        CourtActivityType rtn;

        // Create stamp
        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);

        //set outcome date by system date when outcome has value.
        if (!BeanHelper.isEmpty(courtActivity.getActivityOutcome())) {
            courtActivity.setOutcomeDate(stamp.getCreateDateTime());
        }

        // Create the entity
        CourtActivityEntity courtActivityEntity = CaseActivityHelper.toCourtActivityEntity(courtActivity, stamp, new HashSet<CaseActivityHistEntity>());

        // Check if reference codes exist and active
        ValidationHelper.verifyMetaCodes(courtActivityEntity, true);

        courtActivityEntity = setAssociatedIds(courtActivityEntity, courtActivity);

        session.save(courtActivityEntity);

        Long caseActivityId = courtActivityEntity.getCaseActivityId();

        // Flush changes to db
        session.flush();
        session.clear();

        //get instance from DB
        rtn = getCourtActivity(uc, caseActivityId, false);

        return rtn;

    }

    /**
     * @param courtActivityEntity
     * @param courtActivity
     * @return
     */
    private CourtActivityEntity setAssociatedIds(CourtActivityEntity courtActivityEntity, CourtActivityType courtActivity) {
        //case ids
        Set<Long> caseIds = courtActivity.getCaseIds();
        courtActivityEntity.getCaseInfos().clear();
        for (Long id : caseIds) {
            CaseInfoEntity entity = (CaseInfoEntity) BeanHelper.getEntity(session, CaseInfoEntity.class, id);
            courtActivityEntity.getCaseInfos().add(entity);
        }

        //orders initiated ids
        Set<Long> orderInitiatedIds = courtActivity.getOrderInititatedIds();
        courtActivityEntity.getOrdersInititated().clear();
        for (Long id : orderInitiatedIds) {
            OrderEntity entity = (OrderEntity) BeanHelper.getEntity(session, OrderEntity.class, id);
            courtActivityEntity.getOrdersInititated().add(entity);
        }

        //orders resulted ids
        Set<Long> orderResultedIds = courtActivity.getOrderResultedIds();
        courtActivityEntity.getOrdersResulted().clear();
        for (Long id : orderResultedIds) {
            OrderEntity entity = (OrderEntity) BeanHelper.getEntity(session, OrderEntity.class, id);
            courtActivityEntity.getOrdersResulted().add(entity);
        }

        //activity ids
        Set<Long> activityIds = courtActivity.getActivityIds();
        courtActivityEntity.getActivityIds().clear();
        for (Long id : activityIds) {
            CAActivityIdEntity entity = new CAActivityIdEntity();
            entity.setActivityId(id);
            courtActivityEntity.addCAActivityIdEntity(entity);
        }

        //case affiliation ids
        Set<Long> caseAffiliationIds = courtActivity.getCaseAffiliationIds();
        for (CaseAffiliationEntity entity : courtActivityEntity.getCaseAffiliations()) {
            entity.getAffiliatedCaseActivities().remove(courtActivity);
        }
        for (Long id : caseAffiliationIds) {
            CaseAffiliationEntity entity = (CaseAffiliationEntity) BeanHelper.getEntity(session, CaseAffiliationEntity.class, id);
            entity.getAffiliatedCaseActivities().add(courtActivityEntity);
        }
        return courtActivityEntity;
    }

    /**
     * Update an object of CourtActivityType and all it's associated data and create history record.
     *
     * @param uc            UserContext
     * @param courtActivity CourtActivityType
     * @return the created object of CourtActivityType
     */
    private CourtActivityType updateCourtActivity(UserContext uc, CourtActivityType courtActivity) {

        CourtActivityType rtn;

        CourtActivityEntity existActivity = getCourtActivityEntity(uc, courtActivity.getCaseActivityIdentification());

        if (needUpdate(courtActivity, CaseActivityHelper.toCourtActivityType(existActivity))) {

            createHistory(uc, existActivity);

            existActivity = setAssociatedIds(existActivity, courtActivity);

            updateExistActivity(uc, courtActivity, existActivity);
        }

        // Flush changes to db
        session.flush();
        session.clear();

        //get instance from DB
        rtn = getCourtActivity(uc, courtActivity.getCaseActivityIdentification(), false);

        return rtn;
    }

    /**
     * Update an object of CourtActivityType and all it's associated data.
     *
     * @param uc            UserContext
     * @param courtActivity CourtActivityType
     * @return true if success
     */
    private boolean updateExistActivity(UserContext uc, CourtActivityType courtActivity, CourtActivityEntity existActivityEntity) {

        // Get modified stamp
        StampEntity stamp = BeanHelper.getModifyStamp(uc, context, existActivityEntity.getStamp());

        //Set outcome date by system date when outcome is updated to new value; or set to null if outcome is updated to null.
        String newOutcome = courtActivity.getActivityOutcome();
        String currentOutcome = existActivityEntity.getActivityOutcome();
        if (isOutcomeChanged(newOutcome, currentOutcome)) {
            if (BeanHelper.isEmpty(newOutcome)) {
                existActivityEntity.setOutcomeDate(null);
            } else {
                existActivityEntity.setOutcomeDate(stamp.getModifyDateTime());
            }
        }

        // Create the entity
        CourtActivityEntity courtActivityEntity = CaseActivityHelper.toCourtActivityEntity(courtActivity, stamp, new HashSet<CaseActivityHistEntity>());

        // Check if reference codes exist and active
        ValidationHelper.verifyMetaCodes(courtActivityEntity, false);

        existActivityEntity = CaseActivityHelper.updatePersistentedEntity(existActivityEntity, courtActivityEntity, BeanHelper.getCreateStamp(uc, context), stamp);
        session.update(existActivityEntity);

        return true;
    }

    /**
     * Search by given search criterias and return a set of CourtActivityType
     *
     * @param uc          UserContext
     * @param search      CaseActivitySearchType
     * @param historyFlag Boolean
     * @param ids         Set<Long>
     * @return a set of CourtActivityType
     */
    private Set<CourtActivityType> search(UserContext uc, CaseActivitySearchType search, Boolean historyFlag, Set<Long> ids) {
        Set<CourtActivityType> rtn;

        List<CourtActivityEntity> lst = searchCurrentEntity(uc, search, ids);

        //Search against history entities to get related case activity ids.
        if (historyFlag != null && Boolean.TRUE.equals(historyFlag)) {
            Set<Long> idsFromHistory = new HashSet<Long>();
            idsFromHistory.addAll(searchHistoryEntity(uc, search, ids));

            //History entities does not have CaseAssociation, need to add this search criteria and search current entities.
            CaseActivitySearchType secondeSeach = new CaseActivitySearchType();
            secondeSeach.setCaseId(search.getCaseId());

            if (idsFromHistory != null && idsFromHistory.size() > 0) {
                lst.addAll(searchCurrentEntity(uc, secondeSeach, idsFromHistory));
            }
        }

        Set<CourtActivityEntity> caseActivityEntities = new HashSet<CourtActivityEntity>(lst);

        if (caseActivityEntities.size() >= (searchMaxLimit + 1)) {
            log.debug("Search result is over " + searchMaxLimit);
            throw new ArbutusRuntimeException("Search result is over " + searchMaxLimit);
        }

        rtn = CaseActivityHelper.toCourtActivityTypeSet(caseActivityEntities, true, null);

        return rtn;
    }

    /**
     * Search against current case activity entitiies.
     *
     * @param uc
     * @param search
     * @param ids
     * @return List<CourtActivityEntity>
     */
    private List<CourtActivityEntity> searchCurrentEntity(UserContext uc, CaseActivitySearchType search, Set<Long> ids) {

        Criteria c = session.createCriteria(CourtActivityEntity.class);

        c.setMaxResults(searchMaxLimit + 1);
        c.setFetchSize(searchMaxLimit + 1);
        c.setLockMode(LockMode.NONE);
        c.setCacheMode(CacheMode.IGNORE);

        //sub set ids.
        if (ids != null && ids.size() > 0) {
            c.add(Restrictions.in("caseActivityId", ids));
        }

        //judge
        String judge = search.getJudge();
        if (!isEmpty(judge)) {
            c.add(Restrictions.ilike("judge", parseWildcard(judge)));
        }

        //comment
        //commentDate
        CommentType comment = search.getActivityComment();
        if (comment != null && comment.getCommentDate() != null) {
            c.add(Restrictions.eq("commentDate", comment.getCommentDate()));
        }
        //commentText
        if (comment != null && !isEmpty(comment.getComment())) {
            c.add(Restrictions.ilike("commentText", parseWildcard(comment.getComment())));
        }

        //activityOccurredDate
        Date fromActivityOccurredDate = search.getFromActivityOccurredDate();
        if (fromActivityOccurredDate != null) {
            c.add(Restrictions.ge("activityOccurredDate", fromActivityOccurredDate));
        }
        Date toActivityOccurredDate = search.getToActivityOccurredDate();
        if (toActivityOccurredDate != null) {
            c.add(Restrictions.le("activityOccurredDate", toActivityOccurredDate));
        }

        //activityCategory
        String activityCategory = search.getActivityCategory();
        if (activityCategory != null) {
            c.add(Restrictions.ilike("activityCategory", activityCategory));
        }

        //activityType
        String activityType = search.getActivityType();
        if (activityType != null) {
            c.add(Restrictions.ilike("activityType", activityType));
        }

        //activityOutcome
        String activityOutcome = search.getActivityOutcome();
        if (activityOutcome != null) {
            c.add(Restrictions.ilike("activityOutcome", activityOutcome));
        }

        //activityStatus
        String activityStatus = search.getActivityStatus();
        if (activityStatus != null) {
            c.add(Restrictions.ilike("activityStatus", activityStatus));
        }

        //facilityId
        Long facilityId = search.getFacilityId();
        if (facilityId != null) {
            c.add(Restrictions.eq("facilityAssociation", facilityId));
        }

        //organizationId
        Long organizationId = search.getOrganizationId();
        if (organizationId != null) {
            c.add(Restrictions.eq("organizationAssociation", organizationId));
        }

        //CaseAssociation
        Long caseId = search.getCaseId();
        if (caseId != null) {
            c.createAlias("caseInfos", "caseInfo");
            c.add(Restrictions.eq("caseInfo.caseInfoId", caseId));
        }

        //ChargeAssociations
        Set<Long> chargeAssociations = search.getChargeIds();
        if (chargeAssociations != null && chargeAssociations.size() > 0) {
            c = StaticAssociationHelper.getCriteriaWithStaticAssociations(uc, session, c, search.getChargeIdsSetMode(), "caseActivityId", chargeAssociations,
                    CAChargeAssociationEntity.class, "chargeAssociations");
        }
        
        /*TODO: it may not needed since by default set mode search is not supported. 
         * If it is need, will think other way to do it.
         * //OrderInititatedAssociations
        Set<Long> orderInititatedIds = search.getOrderInititatedIds();
        if (orderInititatedIds != null && orderInititatedIds.size() > 0) {
	        c = StaticAssociationHelper.getCriteriaWithStaticAssociations(
	        		uc, 
	        		session,
	        		c, 
	        		search.getOrderInititatedIdsSetMode(), 
	        		"caseActivityId",
	        		orderInititatedIds, 
	        		OrderInitiatedAssociationEntity.class,
	        		"orderInititatedAssociations");
        }
        
        //OrderResultedAssociations
        Set<Long> orderResultedIds = search.getOrderResultedIds();
        if (orderResultedIds != null && orderResultedIds.size() > 0) {
	        c = StaticAssociationHelper.getCriteriaWithStaticAssociations(
	        		uc, 
	        		session,
	        		c, 
	        		search.getOrderResultedIdsSetMode(), 
	        		"caseActivityId",
	        		orderResultedIds, 
	        		OrderResultedAssociationEntity.class,
	        		"orderResultedAssociations");
        } */

        @SuppressWarnings("unchecked") List<CourtActivityEntity> lst = c.list();

        return lst;
    }

    /**
     * Search against case activity history entities and return the related case activity ids.
     *
     * @param uc
     * @param search
     * @param ids
     * @return
     */
    @SuppressWarnings("unchecked")
    private List<Long> searchHistoryEntity(UserContext uc, CaseActivitySearchType search, Set<Long> ids) {

        Criteria c = session.createCriteria(CourtActivityHistEntity.class);

        //sub set ids.
        if (ids != null && ids.size() > 0) {
            c.add(Restrictions.in("caseActivityId", ids));
        }

        //judge
        String judge = search.getJudge();
        if (!isEmpty(judge)) {
            c.add(Restrictions.ilike("judge", parseWildcard(judge)));
        }

        //comment
        //commentDate
        CommentType comment = search.getActivityComment();
        if (comment != null && comment.getCommentDate() != null) {
            c.add(Restrictions.eq("commentDate", comment.getCommentDate()));
        }
        //commentText
        if (comment != null && !isEmpty(comment.getComment())) {
            c.add(Restrictions.ilike("commentText", parseWildcard(comment.getComment())));
        }

        //activityOccurredDate
        Date fromActivityOccurredDate = search.getFromActivityOccurredDate();
        if (fromActivityOccurredDate != null) {
            c.add(Restrictions.ge("activityOccurredDate", fromActivityOccurredDate));
        }
        Date toActivityOccurredDate = search.getToActivityOccurredDate();
        if (toActivityOccurredDate != null) {
            c.add(Restrictions.le("activityOccurredDate", toActivityOccurredDate));
        }

        //activityCategory
        String activityCategory = search.getActivityCategory();
        if (activityCategory != null) {
            c.add(Restrictions.ilike("activityCategory", activityCategory));
        }

        //activityType
        String activityType = search.getActivityType();
        if (activityType != null) {
            c.add(Restrictions.ilike("activityType", activityType));
        }

        //activityOutcome
        String activityOutcome = search.getActivityOutcome();
        if (activityOutcome != null) {
            c.add(Restrictions.ilike("activityOutcome", activityOutcome));
        }

        //activityStatus
        String activityStatus = search.getActivityStatus();
        if (activityStatus != null) {
            c.add(Restrictions.ilike("activityStatus", activityStatus));
        }

        //facilityAssociation
        Long facilityId = search.getFacilityId();
        if (facilityId != null) {
            c.add(Restrictions.eq("facilityAssociation", facilityId));
        }

        //organizationAssociation
        Long organizationId = search.getOrganizationId();
        if (organizationId != null) {
            c.add(Restrictions.eq("organizationAssociation", organizationId));
        }

        //ChargeAssociations
        Set<Long> chargeIds = search.getChargeIds();
        if (chargeIds != null && chargeIds.size() > 0) {
            c = StaticAssociationHelper.getCriteriaWithStaticAssocHist(uc, session, c, search.getChargeIdsSetMode(), "caseActivityId", chargeIds,
                    CAChargeAssociationHistEntity.class, "chargeAssociations");
        }

        //OrderInititatedAssociations
        Set<Long> orderInititatedIds = search.getOrderInititatedIds();
        if (orderInititatedIds != null && orderInititatedIds.size() > 0) {
            c = StaticAssociationHelper.getCriteriaWithStaticAssocHist(uc, session, c, search.getOrderInititatedIdsSetMode(), "caseActivityId", orderInititatedIds,
                    OrderInitiatedAssociationHistEntity.class, "orderInititatedAssociations");
        }

        //OrderResultedAssociations
        Set<Long> orderResultedIds = search.getOrderResultedIds();
        if (orderResultedIds != null && orderResultedIds.size() > 0) {
            c = StaticAssociationHelper.getCriteriaWithStaticAssocHist(uc, session, c, search.getOrderResultedIdsSetMode(), "caseActivityId", orderResultedIds,
                    OrderResultedAssociationHistEntity.class, "orderResultedAssociations");
        }

        c.setProjection(Projections.property("caseActivityId"));

        return c.list();
    }

    /**
     * Retrieve a set of CourtActivityType by caseAssociation
     *
     * @param uc             UserContext
     * @param caseId
     * @param activityStatus Id
     * @return a set of CourtActivityType
     */
    private Set<CourtActivityType> retrieveByCaseAssociation(UserContext uc, Long caseId, String activityStatus) {

        CaseActivitySearchType searchType = new CaseActivitySearchType();
        searchType.setCaseId(caseId);
        searchType.setActivityStatus(activityStatus);

        Set<CourtActivityType> rtn = search(uc, searchType, null, null);

        return rtn;
    }

    /**
     * Create an history object of CourtActivityEntity.
     *
     * @param uc                  UserContext
     * @param existActivityEntity CourtActivityEntity
     * @return true if success.
     */
    private boolean createHistory(UserContext uc, CourtActivityEntity existActivityEntity) {

        // Create the history entity
        CourtActivityHistEntity courtActivityHistoryEntity = CaseActivityHelper.toCourtActivityHistoryEntity(existActivityEntity);

        session.save(courtActivityHistoryEntity);

        return true;

    }

    /**
     * Get current court activity and all it's history records by given parameters.
     *
     * @param uc
     * @param caseActivityId
     * @param fromHistoryDate
     * @param toHistoryDate
     * @return Set<CourtActivityType>
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private Set<CourtActivityType> getHistory(UserContext uc, Long caseActivityId, Date fromHistoryDate, Date toHistoryDate) {

        CourtActivityEntity currentEntity = getCourtActivityEntity(uc, caseActivityId);

        Set<CourtActivityType> rtn;

        Criteria criteria = session.createCriteria(CaseActivityEntity.class, "caseActivity");
        criteria.add(Restrictions.eq("caseActivityId", caseActivityId));
        if (!BeanHelper.isEmpty(fromHistoryDate)) {
            criteria.add(Restrictions.ge("stamp.modifyDateTime", fromHistoryDate));
        }
        if (!BeanHelper.isEmpty(toHistoryDate)) {
            criteria.add(Restrictions.le("stamp.modifyDateTime", toHistoryDate));
        }

        List currentList = criteria.list();

        if (currentList.isEmpty()) {
            currentEntity = null;
        }

        Criteria criteria1 = session.createCriteria(CaseActivityHistEntity.class, "caseActivityHist");
        criteria1.add(Restrictions.eq("caseActivityId", caseActivityId));
        if (!BeanHelper.isEmpty(fromHistoryDate)) {
            criteria1.add(Restrictions.ge("stamp.modifyDateTime", fromHistoryDate));
        }
        if (!BeanHelper.isEmpty(toHistoryDate)) {
            criteria1.add(Restrictions.le("stamp.modifyDateTime", toHistoryDate));
        }

        List<CourtActivityHistEntity> historylist = criteria1.list();

        Set<CourtActivityHistEntity> histories = new HashSet<CourtActivityHistEntity>(historylist);

        rtn = CaseActivityHelper.toCourtActivityTypeSet(histories, currentEntity);

        return rtn;
    }

    /**
     * Get CourtActivityType object from session by Id.
     *
     * @param uc              UserContext
     * @param courtActivityId Long
     * @return an object of CourtActivityType
     */
    private CourtActivityType getCourtActivity(UserContext uc, Long courtActivityId, boolean privilegeCheck) {

        CourtActivityType rtn = CaseActivityHelper.toCourtActivityType(getCourtActivityEntity(uc, courtActivityId));

        return rtn;
    }

    /**
     * Get CourtActivityEntigy object from session by Id.
     *
     * @param uc              UserContext
     * @param courtActivityId Long
     * @return an object of CourtActivityEntity
     */
    private CourtActivityEntity getCourtActivityEntity(UserContext uc, Long courtActivityId) {

        CourtActivityEntity courtActivityEntity = (CourtActivityEntity) session.get(CourtActivityEntity.class, courtActivityId);

        if (courtActivityEntity == null) {
            throw new DataNotExistException("No such CourtActivity exists for the given id: " + courtActivityId);
        }

        return courtActivityEntity;
    }

    /**
     * Delete case activity and it's histories by given id.
     *
     * @param uc
     * @param caseActivityId
     * @return a Long object
     */
    private Long deleteCaseActivityEntity(UserContext uc, Long caseActivityId) {

        //try to get the exist entity by the given id.
        CourtActivityEntity existEntity = getCourtActivityEntity(uc, caseActivityId);

        //Soft deletion.
        existEntity.setFlag(DataFlag.DELETE.value());

        session.update(existEntity);

        //Use existing functions to delete(cancel) movement data.        
        CompositeHandler compositeHandler = new CompositeHandler(context, session);
        List<Long> activityIds = compositeHandler.getActivityIds(caseActivityId);
        compositeHandler.cancelMovementsData(uc, activityIds);

        return ReturnCode.Success.returnCode();
    }

    /**
     * Cancel a court activity entity by given id.
     *
     * @param uc             UserContext
     * @param caseActivityId Long
     * @return the object of CourtActivityType if no exception.
     */
    private CourtActivityType cancelCourtActivityEntity(UserContext uc, Long caseActivityId) {

        CourtActivityType rtn;

        CourtActivityEntity existActivity = getCourtActivityEntity(uc, caseActivityId);

        if (isActiveEntity(existActivity)) {
            createHistory(uc, existActivity);
            cancelExistCourtActivityEntity(uc, existActivity);
        }

        //Flush changes to db
        session.flush();
        session.clear();

        //get instance from DB
        rtn = getCourtActivity(uc, caseActivityId, false);

        return rtn;
    }

    /**
     * Check an entity is active or not.
     *
     * @param entity
     * @return true if it is active or false if it is inactive.
     */
    private boolean isActiveEntity(CaseActivityEntity entity) {
        return ActivityStatusCode.ACTIVE.value().equalsIgnoreCase(entity.getActivityStatus());
    }

    /**
     * Cancel an exist Court Activity Entity.
     *
     * @param uc
     * @param existActivityEntity
     */
    private void cancelExistCourtActivityEntity(UserContext uc, CourtActivityEntity existActivityEntity) {
        // Get modified stamp
        StampEntity stamp = BeanHelper.getModifyStamp(uc, context, existActivityEntity.getStamp());

        existActivityEntity.setActivityStatus(ActivityStatusCode.INACTIVE.value());
        existActivityEntity.setStamp(stamp);

        // Check if reference codes exist and active
        ValidationHelper.verifyMetaCodes(existActivityEntity, true);

        session.update(existActivityEntity);
    }

    /**
     * Check if the CourtActivityType need to be update.
     *
     * @param newOne
     * @param oldOne
     * @return boolean
     */
    private boolean needUpdate(CourtActivityType newOne, CourtActivityType oldOne) {
        if (newOne.equals(oldOne)) {
            return false;
        }

        return true;
    }

    /**
     * Print debug log if debug enabled.
     *
     * @param message
     */
    private void printDebugLog(String message) {
        if (log.isDebugEnabled()) {
            log.debug(message);
        }
    }

    /**
     * A generic method used to check if a text is empty.
     *
     * @param text the text to be verified
     * @return true if empty, false otherwise
     */
    private boolean isEmpty(String text) {
        return (text == null || text.trim().equals(""));
    }

    /**
     * Parse * to % in the string.
     *
     * @param str
     * @return String
     */
    private String parseWildcard(String str) {
        return str == null || str.length() == 0 ? str : str.matches("^\\*+$") ? "" : str.replaceAll("\\*+", "%");
    }

    private void verifySearchType(Object o) {

        ValidationHelper.validateSearchType(o);
    }

    /**
     * @param uc
     * @param caseActivityId   - required
     * @param dataSecurityFlag - required
     *                         Enum of DataFlag.SEAL
     * @param comments         - optional
     *                         Comments for DataSecurity Record
     * @param personId         - required
     *                         PersonId of the person updating status
     * @return
     */
    public Long updateCaseActivityDataSecurityStatus(UserContext uc, Long caseActivityId, Long dataSecurityFlag, String comments, Long personId, String parentEntityType,
            Long parentId, String memento) {

        Long ret = ReturnCode.UnknownError.returnCode();

        // id can not be null.
        if (caseActivityId == null || personId == null || dataSecurityFlag == null) {
            String message = "caseActivityId or personId or dataSecurityFlag can not be null";
            throw new InvalidInputException(message);
        }

        if (dataSecurityFlag.equals(DataFlag.SEAL.value())) {
            ret = sealCaseActivity(uc, caseActivityId, comments, personId, parentEntityType, parentId, memento);
        } else if (dataSecurityFlag.equals(DataFlag.ACTIVE.value())) {
            ret = UnSealCaseActivity(uc, caseActivityId, comments, personId, parentEntityType, parentId, memento);
        }

        return ret;

    }

    public Long sealCaseActivity(UserContext uc, Long caseActivityId, String comments, Long personId, String parentEntityType, Long parentId, String parentMemento) {
        CourtActivityEntity entity = BeanHelper.getEntity(session, CourtActivityEntity.class, caseActivityId);
        Date recordDate = new Date();
        String recordType = DataSecurityRecordTypeEnum.SEALCASEACTIVITY.code();

        Set<OrderEntity> orders = entity.getOrdersInititated();
        Long dataSecurityFlag = DataFlag.SEAL.value();

        // Seal associated initiated orders
        for (OrderEntity order : orders) {

            String assEntityType = EntityTypeEnum.ORDER.code();
            String assParentEntityType = EntityTypeEnum.CASEACTIVITY.code();

            String memento = ConversionUtils.getMemento(EntityTypeEnum.ORDER, order);

            LegalServiceAdapter.updateOrderDataSecurityStatus(uc, order.getOrderId(), dataSecurityFlag, comments, personId, assParentEntityType,
                    entity.getCaseActivityId(), memento, Boolean.FALSE);

        }

        // 1- Conditions has no associations, so just seal condition

        // 2- Set the Status on the Parent Charge
        String entityType = EntityTypeEnum.CASEACTIVITY.code();

        DataSecurityRecordType dsr = new DataSecurityRecordType(null, entity.getCaseActivityId(), entityType, parentId, parentEntityType, recordType, personId,
                recordDate, comments, parentMemento);

        DataSecurityServiceAdapter.createRecord(uc, dsr);
        entity.setFlag(DataFlag.SEAL.value());
        session.merge(entity);
        session.flush();

        return ReturnCode.Success.returnCode();
    }

    public Long UnSealCaseActivity(UserContext uc, Long caseActivityId, String comments, Long personId, String parentEntityType, Long parentId, String memento) {
        List<DataSecurityRecordType> relatedRecords = DataSecurityServiceAdapter.getRelatedRecords(uc, caseActivityId, EntityTypeEnum.CASEACTIVITY.code());

        DataSecurityRecordType sealedCharge = DataSecurityServiceAdapter.getEntity(uc, EntityTypeEnum.CASEACTIVITY.code(), caseActivityId, parentEntityType, parentId);

        Long dataSecurityFlag = DataFlag.ACTIVE.value();

        for (DataSecurityRecordType rec : relatedRecords) {

            String assEntityType = rec.getEntityType();
            String assParentEntityType = EntityTypeEnum.CASEACTIVITY.code();

            if (rec.getEntityType().equals(EntityTypeEnum.ORDER.code())) {
                LegalServiceAdapter.updateConditionDataSecurityStatus(uc, rec.getDataSecurityRecordId(), dataSecurityFlag, comments, personId, assParentEntityType,
                        caseActivityId, rec.getMemento());
            }
        }

        // 2- Set the Status on the Parent Charge
        String recordType = DataSecurityRecordTypeEnum.UNSEALCHARGE.code();
        Date recordDate = new Date();

        DataSecurityRecordType dsr = new DataSecurityRecordType(null, caseActivityId, DataSecurityRecordTypeEnum.UNSEALCASEACTIVITY.code(), parentId, parentEntityType,
                recordType, personId, recordDate, comments, memento);
        DataSecurityServiceAdapter.createRecord(uc, dsr);

        String chrgeTblName = "leg_cacaseactivity";
        String sql = "select caseactivityid, flag from " + chrgeTblName + " where caseactivityid = :caseactivityIdParam";
        String usql = "update " + chrgeTblName + " set flag = 1 where caseactivityid = :caseactivityIdParam";
        Map<String, String> paramMap = new HashMap<String, String>();
        Map<String, Type> typesMap = new HashMap<String, Type>();

        // update charge status
        paramMap.put("caseactivityIdParam", String.valueOf(sealedCharge.getEntityId()));
        typesMap.put("caseactivityIdParam", LongType.INSTANCE);
        typesMap.put("flag", LongType.INSTANCE);
        List entitys = DataSecurityHelper.executeRawSQLQuery(session, sql, paramMap, typesMap);

        Long id = null;
        Long flag = null;

        // There should be only one result in entitys
        for (int i = 0; i < entitys.size(); i++) {
            Object[] o = (Object[]) entitys.get(i);
            id = DataSecurityHelper.getLongValue(o[0]);
            flag = DataSecurityHelper.getLongValue(o[1]);
        }

        paramMap.clear();
        typesMap.clear();
        paramMap.put("caseactivityIdParam", String.valueOf(id));
        typesMap.put("caseactivityIdParam", LongType.INSTANCE);
        DataSecurityHelper.executeRawSQLQuery(session, usql, paramMap, typesMap);

        session.flush();
        session.clear();

        return ReturnCode.Success.returnCode();
    }

    private Criteria createCourtActivitySearchCriteria(UserContext uc, CaseActivitySearchType search, Set<Long> ids) {
        Criteria c = session.createCriteria(CourtActivityEntity.class);

        //sub set ids.
        if (ids != null && ids.size() > 0) {
            c.add(Restrictions.in("caseActivityId", ids));
        }

        //judge
        String judge = search.getJudge();
        if (!isEmpty(judge)) {
            c.add(Restrictions.ilike("judge", parseWildcard(judge)));
        }

        //comment
        //commentDate
        CommentType comment = search.getActivityComment();
        if (comment != null && comment.getCommentDate() != null) {
            c.add(Restrictions.eq("commentDate", comment.getCommentDate()));
        }
        //commentText
        if (comment != null && !isEmpty(comment.getComment())) {
            c.add(Restrictions.ilike("commentText", parseWildcard(comment.getComment())));
        }

        //activityOccurredDate
        Date fromActivityOccurredDate = search.getFromActivityOccurredDate();
        if (fromActivityOccurredDate != null) {
            c.add(Restrictions.ge("activityOccurredDate", fromActivityOccurredDate));
        }
        Date toActivityOccurredDate = search.getToActivityOccurredDate();
        if (toActivityOccurredDate != null) {
            c.add(Restrictions.le("activityOccurredDate", toActivityOccurredDate));
        }

        //activityCategory
        String activityCategory = search.getActivityCategory();
        if (activityCategory != null) {
            c.add(Restrictions.ilike("activityCategory", activityCategory));
        }

        //activityType
        String activityType = search.getActivityType();
        if (activityType != null) {
            c.add(Restrictions.ilike("activityType", activityType));
        }

        //activityOutcome
        String activityOutcome = search.getActivityOutcome();
        if (activityOutcome != null) {
            c.add(Restrictions.ilike("activityOutcome", activityOutcome));
        }

        //activityStatus
        String activityStatus = search.getActivityStatus();
        if (activityStatus != null) {
            c.add(Restrictions.ilike("activityStatus", activityStatus));
        }

        //facilityId
        Long facilityId = search.getFacilityId();
        if (facilityId != null) {
            c.add(Restrictions.eq("facilityAssociation", facilityId));
        }

        //organizationId
        Long organizationId = search.getOrganizationId();
        if (organizationId != null) {
            c.add(Restrictions.eq("organizationAssociation", organizationId));
        }

        //CaseAssociation
        Long caseId = search.getCaseId();
        if (caseId != null) {
            c.createAlias("associations", "association");
            Criterion criterion = Restrictions.and(Restrictions.eq("association.toClass", LegalModule.CASE_INFORMATION.value()),
                    Restrictions.eq("association.toIdentifier", caseId));
            c.add(criterion);
        }

        //ChargeAssociations
        Set<Long> chargeAssociations = search.getChargeIds();
        if (chargeAssociations != null && chargeAssociations.size() > 0) {
            c = StaticAssociationHelper.getCriteriaWithStaticAssociations(uc, session, c, search.getChargeIdsSetMode(), "caseActivityId", chargeAssociations,
                    CAChargeAssociationEntity.class, "chargeAssociations");
        }
        
        /*TODO: it may not needed since by default set mode search is not supported. 
         * If it is need, will think other way to do it.
        //OrderInititatedAssociations
        Set<Long> orderInititatedIds = search.getOrderInititatedIds();
        if (orderInititatedIds != null && orderInititatedIds.size() > 0) {
	        c = StaticAssociationHelper.getCriteriaWithStaticAssociations(
	        		uc, 
	        		session,
	        		c, 
	        		search.getOrderInititatedIdsSetMode(), 
	        		"caseActivityId",
	        		orderInititatedIds, 
	        		OrderInitiatedAssociationEntity.class,
	        		"orderInititatedAssociations");
        }
        
        //OrderResultedAssociations
        Set<Long> orderResultedIds = search.getOrderResultedIds();
        if (orderResultedIds != null && orderResultedIds.size() > 0) {
	        c = StaticAssociationHelper.getCriteriaWithStaticAssociations(
	        		uc, 
	        		session,
	        		c, 
	        		search.getOrderResultedIdsSetMode(), 
	        		"caseActivityId",
	        		orderResultedIds, 
	        		OrderResultedAssociationEntity.class,
	        		"orderResultedAssociations");
        } */

        return c;
    }
}
