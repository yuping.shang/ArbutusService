package syscon.arbutus.product.services.legal.contract.dto.charge;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

/**
 * The representation of the statute charge configuration type
 */
@ArbutusConstraint(constraints = "startDate <= endDate")
public class StatuteChargeConfigType implements Serializable {

    private static final long serialVersionUID = 300546226949782842L;
    private Long statuteChargeId;
    @NotNull
    private Long statuteId;
    @NotNull
    private String chargeCode;
    @NotNull
    @Valid
    private StatuteChargeTextType chargeText;
    @NotNull
    private String chargeType;
    private String chargeDegree;
    private String chargeCategory;
    private String chargeSeverity;
    @NotNull
    private Boolean bailAllowed;
    @NotNull
    private Boolean bondAllowed;
    @Min(value = 0L, message = "bailAmount cannot be negative value")
    private Long bailAmount;
    @NotNull
    private Date startDate;
    @NotNull
    private Date endDate;
    private Set<Long> externalChargeCodeIds;
    @Valid
    private Set<ChargeIndicatorType> chargeIndicators;
    private Set<String> enhancingFactors;
    private Set<String> reducingFactors;

    /**
     * Default constructor
     */
    public StatuteChargeConfigType() {
    }

    /**
     * Constructor
     *
     * @param statuteChargeId       Long - the unique Id for each statute charge configuration record, (optional for creation)
     * @param statuteId             Long - the unique Id for each statute configuration record
     * @param chargeCode            String - the charge code defined for a statute jurisdiction.
     * @param chargeType            String - the type of a charge (Felony, Misdemeanor)
     * @param chargeDegree          String - the degree of a charge (optional)
     * @param chargeCategory        String - the category the charge belongs to (optional)
     * @param chargeSeverity        String - the severity of a charge (optional)
     * @param bailAmount            Long - the bail amount recommended by the statute charge code. (optional)
     * @param bailAllowed           Boolean - true if bail allowed is allowed for this charge code
     * @param bondAllowed           Boolean - true if bond is allowed for this charge
     * @param startDate             Date - the date on which this charge code was validated
     * @param endDate               Date - the date on which this charge code was invalidated
     * @param chargeText            StatuteChargeTextType - the legal text associated to the charge.
     * @param externalChargeCodeIds Set<Long> - a set of External charge code (NCIC, UCR) for the statute charge code (optional)
     * @param chargeIndicators      Set<ChargeIndicatorType> - the indicators (alerts) on a charge (optional)
     * @param enhancingFactors      Set<String> - the factor or reason that makes a charge more serious (optional)
     * @param reducingFactors       Set<String> - the factor which make a charge less serious (optional)
     */
    public StatuteChargeConfigType(Long statuteChargeId, @NotNull Long statuteId, @NotNull String chargeCode, @NotNull String chargeType, String chargeDegree,
            String chargeCategory, String chargeSeverity, Long bailAmount, @NotNull Boolean bailAllowed, @NotNull Boolean bondAllowed, @NotNull Date startDate,
            @NotNull Date endDate, @NotNull StatuteChargeTextType chargeText, Set<Long> externalChargeCodeIds, Set<ChargeIndicatorType> chargeIndicators,
            Set<String> enhancingFactors, Set<String> reducingFactors) {

        this.statuteChargeId = statuteChargeId;
        this.statuteId = statuteId;
        this.chargeCode = chargeCode;
        this.chargeType = chargeType;
        this.chargeDegree = chargeDegree;
        this.chargeCategory = chargeCategory;
        this.chargeSeverity = chargeSeverity;
        this.bailAmount = bailAmount;
        this.bailAllowed = bailAllowed;
        this.bondAllowed = bondAllowed;
        this.startDate = startDate;
        this.endDate = endDate;
        this.chargeText = chargeText;
        this.externalChargeCodeIds = externalChargeCodeIds;
        this.chargeIndicators = chargeIndicators;
        this.enhancingFactors = enhancingFactors;
        this.reducingFactors = reducingFactors;
    }

    /**
     * Gets the value of the statuteChargeId property.
     *
     * @return Long - the unique Id for each statute charge configuration record
     */
    public Long getStatuteChargeId() {
        return statuteChargeId;
    }

    /**
     * Sets the value of the statuteChargeId property.
     *
     * @param statuteChargeId Long - the unique Id for each statute charge configuration record
     */
    public void setStatuteChargeId(Long statuteChargeId) {
        this.statuteChargeId = statuteChargeId;
    }

    /**
     * Gets the value of the statuteId property.
     *
     * @return Long - the unique Id for each statute configuration record
     */
    public Long getStatuteId() {
        return statuteId;
    }

    /**
     * Sets the value of the statuteId property.
     *
     * @param statuteId Long - the unique Id for each statute configuration record
     */
    public void setStatuteId(Long statuteId) {
        this.statuteId = statuteId;
    }

    /**
     * Gets the value of the chargeCode property.
     * It is a reference code value of ChargeCode set.
     *
     * @return String - the charge code defined for a statute jurisdiction.
     */
    public String getChargeCode() {
        return chargeCode;
    }

    /**
     * Sets the value of the chargeCode property.
     * It is a reference code value of ChargeCode set.
     *
     * @param chargeCode String - the charge code defined for a statute jurisdiction.
     */
    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    /**
     * Gets the value of the chargeText property.
     *
     * @return StatuteChargeTextType - the legal text associated to the charge.
     */
    public StatuteChargeTextType getChargeText() {
        return chargeText;
    }

    /**
     * Sets the value of the chargeText property.
     *
     * @param chargeText StatuteChargeTextType - the legal text associated to the charge.
     */
    public void setChargeText(StatuteChargeTextType chargeText) {
        this.chargeText = chargeText;
    }

    /**
     * Gets the value of the chargeType property.
     * It is a reference code value of TypeOfCharge set.
     *
     * @return String - the type of a charge (Felony, Misdemeanor)
     */
    public String getChargeType() {
        return chargeType;
    }

    /**
     * Sets the value of the chargeType property.
     * It is a reference code value of TypeOfCharge set.
     *
     * @param chargeType String - the type of a charge (Felony, Misdemeanor)
     */
    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    /**
     * Gets the value of the chargeDegree property.
     * It is a reference code value of ChargeDegree set.
     *
     * @return String - the degree of a charge
     */
    public String getChargeDegree() {
        return chargeDegree;
    }

    /**
     * Sets the value of the chargeDegree property.
     * It is a reference code value of ChargeDegree set.
     *
     * @param chargeDegree String - the degree of a charge
     */
    public void setChargeDegree(String chargeDegree) {
        this.chargeDegree = chargeDegree;
    }

    /**
     * Gets the value of the chargeCategory property.
     * It is a reference code value of ChargeCategory set.
     *
     * @return String - the category the charge belongs to
     */
    public String getChargeCategory() {
        return chargeCategory;
    }

    /**
     * Sets the value of the chargeCategory property.
     * It is a reference code value of ChargeCategory set.
     *
     * @param chargeCategory String - the category the charge belongs to
     */
    public void setChargeCategory(String chargeCategory) {
        this.chargeCategory = chargeCategory;
    }

    /**
     * Gets the value of the chargeSeverity property.
     * It is a reference code value of ChargeSeverity set.
     *
     * @return String - the severity of a charge
     */
    public String getChargeSeverity() {
        return chargeSeverity;
    }

    /**
     * Sets the value of the chargeSeverity property.
     * It is a reference code value of ChargeSeverity set.
     *
     * @param chargeSeverity String - the severity of a charge
     */
    public void setChargeSeverity(String chargeSeverity) {
        this.chargeSeverity = chargeSeverity;
    }

    /**
     * Gets the value of the bailAllowed property.
     *
     * @return Boolean - true if bail allowed is allowed for this charge code
     */
    public Boolean isBailAllowed() {
        return bailAllowed;
    }

    /**
     * Gets the value of the bailAllowed property.
     *
     * @return Boolean - true if bail allowed is allowed for this charge code
     */
    public Boolean getBailAllowed() {
        return bailAllowed;
    }

    /**
     * Sets the value of the bailAllowed property.
     *
     * @param bailAllowed Boolean - true if bail allowed is allowed for this charge code
     */
    public void setBailAllowed(Boolean bailAllowed) {
        this.bailAllowed = bailAllowed;
    }

    /**
     * Gets the value of the bondAllowed property.
     *
     * @return Boolean - true if bond is allowed for this charge
     */
    public Boolean isBondAllowed() {
        return bondAllowed;
    }

    /**
     * Gets the value of the bondAllowed property.
     *
     * @return Boolean - true if bond is allowed for this charge
     */
    public Boolean getBondAllowed() {
        return bondAllowed;
    }

    /**
     * Sets the value of the bondAllowed property.
     *
     * @param bondAllowed Boolean - true if bond is allowed for this charge
     */
    public void setBondAllowed(Boolean bondAllowed) {
        this.bondAllowed = bondAllowed;
    }

    /**
     * Gets the value of the bailAmount property.
     *
     * @return Long - the bail amount recommended by the statute charge code
     */
    public Long getBailAmount() {
        return bailAmount;
    }

    /**
     * Sets the value of the bailAmount property.
     *
     * @param bailAmount Long - the bail amount recommended by the statute charge code
     */
    public void setBailAmount(Long bailAmount) {
        this.bailAmount = bailAmount;
    }

    /**
     * Gets the value of the startDate property.
     *
     * @return Date - the date on which this charge code was validated
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     *
     * @param startDate Date - the date on which this charge code was validated
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Gets the value of the endDate property.
     *
     * @return Date - the date on which this charge code was invalidated
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     *
     * @param endDate Date - the date on which this charge code was invalidated
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Gets the value of the getExternalChargeCodeIds property.
     *
     * @return Set<Long> - a set of External charge code (NCIC, UCR) for the statute charge code
     */
    public Set<Long> getExternalChargeCodeIds() {
        if (externalChargeCodeIds == null) {
            externalChargeCodeIds = new HashSet<Long>();
        }
        return externalChargeCodeIds;
    }

    /**
     * Sets the value of the getExternalChargeCodeIds property.
     *
     * @param externalChargeCodeIds Set<Long> - a set of External charge code (NCIC, UCR) for the statute charge code
     */
    public void setExternalChargeCodeIds(Set<Long> externalChargeCodeIds) {
        this.externalChargeCodeIds = externalChargeCodeIds;
    }

    /**
     * Gets the value of the chargeIndicators property.
     *
     * @return Set<ChargeIndicatorType> - the indicators (alerts) on a charge
     */
    public Set<ChargeIndicatorType> getChargeIndicators() {
        if (chargeIndicators == null) {
            chargeIndicators = new HashSet<ChargeIndicatorType>();
        }
        return chargeIndicators;
    }

    /**
     * Sets the value of the chargeIndicators property.
     *
     * @param chargeIndicators Set<ChargeIndicatorType> - the indicators (alerts) on a charge
     */
    public void setChargeIndicators(Set<ChargeIndicatorType> chargeIndicators) {
        this.chargeIndicators = chargeIndicators;
    }

    /**
     * Gets the value of the enhancingFactors property.
     * It is a reference code value of EnhancingFactor set.
     *
     * @return the enhancingFactors
     */
    public Set<String> getEnhancingFactors() {
        if (enhancingFactors == null) {
            enhancingFactors = new HashSet<String>();
        }
        return enhancingFactors;
    }

    /**
     * Sets the value of the enhancingFactors property.
     * It is a reference code value of EnhancingFactor set.
     *
     * @param enhancingFactors the enhancingFactors to set
     */
    public void setEnhancingFactors(Set<String> enhancingFactors) {
        this.enhancingFactors = enhancingFactors;
    }

    /**
     * Gets the value of the reducingFactors property.
     * It is a reference code value of ReducingFactor set.
     *
     * @return Set<String> - the factor which make a charge less serious
     */
    public Set<String> getReducingFactors() {
        if (reducingFactors == null) {
            reducingFactors = new HashSet<String>();
        }
        return reducingFactors;
    }

    /**
     * Sets the value of the reducingFactors property.
     * It is a reference code value of ReducingFactor set.
     *
     * @param reducingFactors Set<String> - the factor which make a charge less serious
     */
    public void setReducingFactors(Set<String> reducingFactors) {
        this.reducingFactors = reducingFactors;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StatuteChargeConfigType [statuteChargeId=" + statuteChargeId + ", statuteId=" + statuteId + ", chargeCode=" + chargeCode + ", chargeText=" + chargeText
                + ", chargeType=" + chargeType + ", chargeDegree=" + chargeDegree + ", chargeCategory=" + chargeCategory + ", chargeSeverity=" + chargeSeverity
                + ", bailAllowed=" + bailAllowed + ", bondAllowed=" + bondAllowed + ", bailAmount=" + bailAmount + ", startDate=" + startDate + ", endDate=" + endDate
                + ", externalChargeCodeIds=" + externalChargeCodeIds + ", chargeIndicators=" + chargeIndicators + ", enhancingFactors=" + enhancingFactors
                + ", reducingFactors=" + reducingFactors + "]";
    }

}
