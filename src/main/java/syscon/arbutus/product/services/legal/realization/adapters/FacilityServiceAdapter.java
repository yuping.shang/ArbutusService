package syscon.arbutus.product.services.legal.realization.adapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.facility.contract.dto.Facility;
import syscon.arbutus.product.services.facility.contract.interfaces.FacilityService;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.utility.ServiceAdapter;

/**
 * An adapter class to wrap the API in FacilityService and provide the legal service bean customized API.
 *
 * @author lhan
 */
public class FacilityServiceAdapter {

    private static FacilityService service;
    private static Logger log = LoggerFactory.getLogger(FacilityServiceAdapter.class);

    //private static final String FACILITY_CATEGORY_CODE= "COURT";

    /**
     * Constructor.
     */
    public FacilityServiceAdapter() {
        super();
    }

    synchronized private static FacilityService getService() {

        if (service == null) {
            service = (FacilityService) ServiceAdapter.JNDILookUp(service, FacilityService.class);
        }
        return service;
    }

    /**
     * @param userContext
     * @param facilityId
     * @return
     */
    public Facility getFacility(UserContext userContext, Long facilityId) {

        String functionName = "getFacility";

        Facility ret = null;

        String message = String.format("facilityId=%s.", facilityId);
        LogHelper.debug(log, functionName, message);

        ret = getService().get(userContext, facilityId);

        message = String.format("facility=%s.", ret.getFacilityType());
        LogHelper.debug(log, functionName, message);

        return ret;
    }
}
