package syscon.arbutus.product.services.legal.realization.persistence.condition;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Condition Entity of Case Management Service
 *
 * @author yshang, lhan
 * @version 1.0
 * @DbComment LEG_CNCondition 'The Condition table for CaseManagement service'
 * @since January 11, 2013
 */
@Audited
@Entity
@Table(name = "LEG_CNCondition")
@SQLDelete(sql = "UPDATE LEG_CNCondition SET flag = 4 WHERE conditionId = ?")
@Where(clause = "flag = 1")
public class ConditionEntity implements Serializable {

    private static final long serialVersionUID = -757901385114542783L;

    /**
     * @DbComment conditionId 'Unique system generated identifier for the Condition.'
     */
    @Id
    @Column(name = "conditionId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CNCONDITION_Id")
    @SequenceGenerator(name = "SEQ_LEG_CNCONDITION_Id", sequenceName = "SEQ_LEG_CNCONDITION_Id", allocationSize = 1)
    private Long conditionId;

    /**
     * @DbComment conditionCreateDate 'Date when the Condition was created'
     */
    @Column(name = "conditionCreateDate", nullable = false)
    private Date conditionCreateDate;

    /**
     * @DbComment conditionStartDate 'Date when the condition is effective from'
     */
    @Column(name = "conditionStartDate", nullable = true)
    private Date conditionStartDate;

    /**
     * @DbComment conditionEndDate 'Date when the condition period is over'
     */
    @Column(name = "conditionEndDate", nullable = true)
    private Date conditionEndDate;

    @ForeignKey(name = "Fk_LEG_CNCondition1")
    @OneToMany(mappedBy = "condition", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<ConditionCommentEntity> conditionComments;

    /**
     * @DbComment conditionType 'Is a logical grouping of Conditions by Sentences, Orders, Charges etc.'
     */
    @MetaCode(set = MetaSet.CONDITION_TYPE)
    @Column(name = "conditionType", nullable = false, length = 64)
    private String conditionType;

    /**
     * @DbComment conditionCategory 'A condition can also be classified under different categories like Financial, Programs etc.'
     */
    @MetaCode(set = MetaSet.CONDITION_CATEGORY)
    @Column(name = "conditionCategory", nullable = true, length = 64)
    private String conditionCategory;

    /**
     * @DbComment conditionSubCategory 'Is a logical sub grouping of the Condition category. E.g. Condition category for Financials, could be sub grouped as Restitution to victims, Electronic monitor payment etc.'
     */
    @MetaCode(set = MetaSet.CONDITION_SUB_CATEGORY)
    @Column(name = "conditionSubCategory", nullable = true, length = 64)
    private String conditionSubCategory;

    /**
     * @DbComment periodUnit 'The period of the condition restriction like days, hours etc'
     */
    @MetaCode(set = MetaSet.CONDITION_PERIOD)
    //@MetaCode(set = MetaSet.CONDITION_PERIOD_UNIT)
    @Column(name = "periodUnit", nullable = true, length = 64)
    private String conditionPeriodUnit;

    /**
     * @DbComment distanceUnit 'The Distance unit'
     */
    //@MetaCode(set = MetaSet.CONDITION_DISTANCE)
    @MetaCode(set = MetaSet.CONDITION_DISTANCE_UNIT)
    @Column(name = "distanceUnit", nullable = true, length = 64)
    private String conditionDistanceUnit;

    /**
     * @DbComment currencyUnit 'The Currency unit'
     */
    @MetaCode(set = MetaSet.CONDITION_CURRENCY)
    @Column(name = "currencyUnit", nullable = true, length = 64)
    private String conditionCurrencyUnit;

    /**
     * @DbComment period 'The period value'
     */
    @Column(name = "period", nullable = true)
    private Long conditionPeriodValue;

    /**
     * @DbComment distance 'The Distance value'
     */
    @Column(name = "distance", nullable = true)
    private Long conditionDistanceValue;

    /**
     * @DbComment conditionCurrencyAmount 'The currency value'
     */
    @Column(name = "conditionCurrencyAmount", nullable = true, precision = 10, scale = 3)
    private BigDecimal conditionCurrencyAmount;

    /**
     * @DbComment conditionDetails 'The standard legal description for the conditions'
     */
    @Column(name = "conditionDetails", nullable = true, length = 4000)
    private String conditionDetails;

    /**
     * @DbComment isConditionSatisfied 'A condition has been satisfied for say release purposes'
     */
    @Column(name = "isConditionSatisfied", nullable = true)
    private Boolean isConditionSatisfied;

    /**
     * @DbComment amendmentDate 'The date an amendment was made to the conditions imposed by a court e.g. court reduces the period of community service. All details of the amendment will be available with the Order it arose from. The presence of the amended date indicates that the condition was amended.'
     */
    @Column(name = "amendmentDate", nullable = true)
    private Date conditionAmendmentDate;

    /**
     * @DbComment amendmentStatus 'The status of the Amendment which could be- Accepted, Rejected, In Progress.'
     */
    @MetaCode(set = MetaSet.CONDITION_AMENDMENT_STATUS)
    @Column(name = "amendmentStatus", nullable = true, length = 64)
    private String conditionAmendmentStatus;

    @OneToMany(mappedBy = "condition", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<ConditionModuleAssociationEntity> moduleAssociations;

    /**
     * @DbComment shortName 'The short name of the condition.'
     */
    @Column(name = "shortName", nullable = true, length = 200)
    private String shortName;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment Active 'Indicates whether the condition is active or inactive.'
     */
    @Column(name = "Active", nullable = true)
    private Boolean active;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(name = "Flag", nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * Empty Constructor
     */
    public ConditionEntity() {

    }

    /**
     * @param conditionId
     * @param conditionCreateDate
     * @param conditionStartDate
     * @param conditionEndDate
     * @param conditionComments
     * @param conditionType
     * @param conditionCategory
     * @param conditionSubCategory
     * @param conditionPeriodUnit
     * @param conditionDistanceUnit
     * @param conditionCurrencyUnit
     * @param conditionPeriodValue
     * @param conditionDistanceValue
     * @param conditionCurrencyAmount
     * @param conditionDetails
     * @param conditionAmendmentDate
     * @param conditionAmendmentStatus
     */
    public ConditionEntity(Long conditionId, Date conditionCreateDate, Date conditionStartDate, Date conditionEndDate, Set<ConditionCommentEntity> conditionComments,
            String conditionType, String conditionCategory, String conditionSubCategory, String conditionPeriodUnit, String conditionDistanceUnit,
            String conditionCurrencyUnit, Long conditionPeriodValue, Long conditionDistanceValue, BigDecimal conditionCurrencyAmount, String conditionDetails,
            Date conditionAmendmentDate, String conditionAmendmentStatus, Boolean isConditionSatisfied, Boolean isConditionStatus, StampEntity stamp) {

        this.conditionId = conditionId;
        this.conditionCreateDate = conditionCreateDate;
        this.conditionStartDate = conditionStartDate;
        this.conditionEndDate = conditionEndDate;
        this.conditionComments = conditionComments;
        this.conditionType = conditionType;
        this.conditionCategory = conditionCategory;
        this.conditionSubCategory = conditionSubCategory;
        this.conditionPeriodUnit = conditionPeriodUnit;
        this.conditionDistanceUnit = conditionDistanceUnit;
        this.conditionCurrencyUnit = conditionCurrencyUnit;
        this.conditionPeriodValue = conditionPeriodValue;
        this.conditionDistanceValue = conditionDistanceValue;
        this.conditionCurrencyAmount = conditionCurrencyAmount;
        this.conditionDetails = conditionDetails;
        this.conditionAmendmentDate = conditionAmendmentDate;
        this.conditionAmendmentStatus = conditionAmendmentStatus;
        this.isConditionSatisfied = isConditionSatisfied;
        this.active = isConditionStatus;
        this.stamp = stamp;

    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /**
     * @return the conditionId
     */
    public Long getConditionId() {
        return conditionId;
    }

    /**
     * @param conditionId the conditionId to set
     */
    public void setConditionId(Long conditionId) {
        this.conditionId = conditionId;
    }

    /**
     * @return the conditionCreateDate
     */
    public Date getConditionCreateDate() {
        return conditionCreateDate;
    }

    /**
     * @param conditionCreateDate the conditionCreateDate to set
     */
    public void setConditionCreateDate(Date conditionCreateDate) {
        this.conditionCreateDate = conditionCreateDate;
    }

    /**
     * @return the conditionStartDate
     */
    public Date getConditionStartDate() {
        return conditionStartDate;
    }

    /**
     * @param conditionStartDate the conditionStartDate to set
     */
    public void setConditionStartDate(Date conditionStartDate) {
        this.conditionStartDate = conditionStartDate;
    }

    /**
     * @return the conditionEndDate
     */
    public Date getConditionEndDate() {
        return conditionEndDate;
    }

    /**
     * @param conditionEndDate the conditionEndDate to set
     */
    public void setConditionEndDate(Date conditionEndDate) {
        this.conditionEndDate = conditionEndDate;
    }

    /**
     * @return the conditionComments
     */
    public Set<ConditionCommentEntity> getConditionComments() {
        if (conditionComments == null) {
			conditionComments = new HashSet<ConditionCommentEntity>();
		}
        return conditionComments;
    }

    /**
     * @param conditionComments the conditionComments to set
     */
    public void setConditionComments(Set<ConditionCommentEntity> conditionComments) {
        this.conditionComments = conditionComments;
    }

    /**
     * @return the conditionType
     */
    public String getConditionType() {
        return conditionType;
    }

    /**
     * @param conditionType the conditionType to set
     */
    public void setConditionType(String conditionType) {
        this.conditionType = conditionType;
    }

    /**
     * @return the conditionCategory
     */
    public String getConditionCategory() {
        return conditionCategory;
    }

    /**
     * @param conditionCategory the conditionCategory to set
     */
    public void setConditionCategory(String conditionCategory) {
        this.conditionCategory = conditionCategory;
    }

    /**
     * @return the conditionSubCategory
     */
    public String getConditionSubCategory() {
        return conditionSubCategory;
    }

    /**
     * @param conditionSubCategory the conditionSubCategory to set
     */
    public void setConditionSubCategory(String conditionSubCategory) {
        this.conditionSubCategory = conditionSubCategory;
    }

    /**
     * @return the conditionPeriodUnit
     */
    public String getConditionPeriodUnit() {
        return conditionPeriodUnit;
    }

    /**
     * @param conditionPeriodUnit the conditionPeriodUnit to set
     */
    public void setConditionPeriodUnit(String conditionPeriodUnit) {
        this.conditionPeriodUnit = conditionPeriodUnit;
    }

    /**
     * @return the conditionDistanceUnit
     */
    public String getConditionDistanceUnit() {
        return conditionDistanceUnit;
    }

    /**
     * @param conditionDistanceUnit the conditionDistanceUnit to set
     */
    public void setConditionDistanceUnit(String conditionDistanceUnit) {
        this.conditionDistanceUnit = conditionDistanceUnit;
    }

    /**
     * @return the conditionCurrencyUnit
     */
    public String getConditionCurrencyUnit() {
        return conditionCurrencyUnit;
    }

    /**
     * @param conditionCurrencyUnit the conditionCurrencyUnit to set
     */
    public void setConditionCurrencyUnit(String conditionCurrencyUnit) {
        this.conditionCurrencyUnit = conditionCurrencyUnit;
    }

    /**
     * @return the conditionPeriodValue
     */
    public Long getConditionPeriodValue() {
        return conditionPeriodValue;
    }

    /**
     * @param conditionPeriodValue the conditionPeriodValue to set
     */
    public void setConditionPeriodValue(Long conditionPeriodValue) {
        this.conditionPeriodValue = conditionPeriodValue;
    }

    /**
     * @return the conditionDistanceValue
     */
    public Long getConditionDistanceValue() {
        return conditionDistanceValue;
    }

    /**
     * @param conditionDistanceValue the conditionDistanceValue to set
     */
    public void setConditionDistanceValue(Long conditionDistanceValue) {
        this.conditionDistanceValue = conditionDistanceValue;
    }

    /**
     * @return the conditionCurrencyAmount
     */
    public BigDecimal getConditionCurrencyAmount() {
        return conditionCurrencyAmount;
    }

    /**
     * @param conditionCurrencyAmount the conditionCurrencyAmount to set
     */
    public void setConditionCurrencyAmount(BigDecimal conditionCurrencyAmount) {
        this.conditionCurrencyAmount = conditionCurrencyAmount;
    }

    /**
     * @return the conditionDetails
     */
    public String getConditionDetails() {
        return conditionDetails;
    }

    /**
     * @param conditionDetails the conditionDetails to set
     */
    public void setConditionDetails(String conditionDetails) {
        this.conditionDetails = conditionDetails;
    }

    /**
     * @return the isConditionSatisfied
     */
    public Boolean getIsConditionSatisfied() {
        return isConditionSatisfied;
    }

    /**
     * @param isConditionSatisfied the isConditionSatisfied to set
     */
    public void setIsConditionSatisfied(Boolean isConditionSatisfied) {
        this.isConditionSatisfied = isConditionSatisfied;
    }

    /**
     * @return the conditionAmendmentDate
     */
    public Date getConditionAmendmentDate() {
        return conditionAmendmentDate;
    }

    /**
     * @param conditionAmendmentDate the conditionAmendmentDate to set
     */
    public void setConditionAmendmentDate(Date conditionAmendmentDate) {
        this.conditionAmendmentDate = conditionAmendmentDate;
    }

    /**
     * @return the conditionAmendmentStatus
     */
    public String getConditionAmendmentStatus() {
        return conditionAmendmentStatus;
    }

    /**
     * @param conditionAmendmentStatus the conditionAmendmentStatus to set
     */
    public void setConditionAmendmentStatus(String conditionAmendmentStatus) {
        this.conditionAmendmentStatus = conditionAmendmentStatus;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @param active
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return boolean value
     */
    public Boolean isActive() {
        return active;
    }

    public Set<ConditionModuleAssociationEntity> getModuleAssociations() {
        if (moduleAssociations == null) {
			this.moduleAssociations = new HashSet<ConditionModuleAssociationEntity>();
		}
        return moduleAssociations;
    }

    public void setModuleAssociations(Set<ConditionModuleAssociationEntity> moduleAssociations) {
        this.moduleAssociations = moduleAssociations;
    }

    public void addComment(ConditionCommentEntity comment) {
        comment.setCondition(this);
        this.conditionComments.add(comment);
    }

    public void addModuleAssociation(ConditionModuleAssociationEntity associationEntity) {
        associationEntity.setCondition(this);
        getModuleAssociations().add(associationEntity);
    }

    /**
     * @return the shortName
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * @param shortName the shortName to set
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((active == null) ? 0 : active.hashCode());
        result = prime * result + ((conditionAmendmentDate == null) ? 0 : conditionAmendmentDate.hashCode());
        result = prime * result + ((conditionAmendmentStatus == null) ? 0 : conditionAmendmentStatus.hashCode());
        result = prime * result + ((conditionCategory == null) ? 0 : conditionCategory.hashCode());
        result = prime * result + ((conditionComments == null) ? 0 : conditionComments.hashCode());
        result = prime * result + ((conditionCreateDate == null) ? 0 : conditionCreateDate.hashCode());
        result = prime * result + ((conditionCurrencyAmount == null) ? 0 : conditionCurrencyAmount.hashCode());
        result = prime * result + ((conditionCurrencyUnit == null) ? 0 : conditionCurrencyUnit.hashCode());
        result = prime * result + ((conditionDetails == null) ? 0 : conditionDetails.hashCode());
        result = prime * result + ((conditionDistanceUnit == null) ? 0 : conditionDistanceUnit.hashCode());
        result = prime * result + ((conditionDistanceValue == null) ? 0 : conditionDistanceValue.hashCode());
        result = prime * result + ((conditionEndDate == null) ? 0 : conditionEndDate.hashCode());
        result = prime * result + ((conditionId == null) ? 0 : conditionId.hashCode());
        result = prime * result + ((conditionPeriodUnit == null) ? 0 : conditionPeriodUnit.hashCode());
        result = prime * result + ((conditionPeriodValue == null) ? 0 : conditionPeriodValue.hashCode());
        result = prime * result + ((conditionStartDate == null) ? 0 : conditionStartDate.hashCode());
        result = prime * result + ((conditionSubCategory == null) ? 0 : conditionSubCategory.hashCode());
        result = prime * result + ((conditionType == null) ? 0 : conditionType.hashCode());
        result = prime * result + ((flag == null) ? 0 : flag.hashCode());
        result = prime * result + ((isConditionSatisfied == null) ? 0 : isConditionSatisfied.hashCode());
        result = prime * result + ((moduleAssociations == null) ? 0 : moduleAssociations.hashCode());
        result = prime * result + ((shortName == null) ? 0 : shortName.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ConditionEntity other = (ConditionEntity) obj;
        if (active == null) {
            if (other.active != null) {
				return false;
			}
        } else if (!active.equals(other.active)) {
			return false;
		}
        if (conditionAmendmentDate == null) {
            if (other.conditionAmendmentDate != null) {
				return false;
			}
        } else if (!conditionAmendmentDate.equals(other.conditionAmendmentDate)) {
			return false;
		}
        if (conditionAmendmentStatus == null) {
            if (other.conditionAmendmentStatus != null) {
				return false;
			}
        } else if (!conditionAmendmentStatus.equals(other.conditionAmendmentStatus)) {
			return false;
		}
        if (conditionCategory == null) {
            if (other.conditionCategory != null) {
				return false;
			}
        } else if (!conditionCategory.equals(other.conditionCategory)) {
			return false;
		}
        if (conditionComments == null) {
            if (other.conditionComments != null) {
				return false;
			}
        } else if (!conditionComments.equals(other.conditionComments)) {
			return false;
		}
        if (conditionCreateDate == null) {
            if (other.conditionCreateDate != null) {
				return false;
			}
        } else if (!conditionCreateDate.equals(other.conditionCreateDate)) {
			return false;
		}
        if (conditionCurrencyAmount == null) {
            if (other.conditionCurrencyAmount != null) {
				return false;
			}
        } else if (!conditionCurrencyAmount.equals(other.conditionCurrencyAmount)) {
			return false;
		}
        if (conditionCurrencyUnit == null) {
            if (other.conditionCurrencyUnit != null) {
				return false;
			}
        } else if (!conditionCurrencyUnit.equals(other.conditionCurrencyUnit)) {
			return false;
		}
        if (conditionDetails == null) {
            if (other.conditionDetails != null) {
				return false;
			}
        } else if (!conditionDetails.equals(other.conditionDetails)) {
			return false;
		}
        if (conditionDistanceUnit == null) {
            if (other.conditionDistanceUnit != null) {
				return false;
			}
        } else if (!conditionDistanceUnit.equals(other.conditionDistanceUnit)) {
			return false;
		}
        if (conditionDistanceValue == null) {
            if (other.conditionDistanceValue != null) {
				return false;
			}
        } else if (!conditionDistanceValue.equals(other.conditionDistanceValue)) {
			return false;
		}
        if (conditionEndDate == null) {
            if (other.conditionEndDate != null) {
				return false;
			}
        } else if (!conditionEndDate.equals(other.conditionEndDate)) {
			return false;
		}
        if (conditionId == null) {
            if (other.conditionId != null) {
				return false;
			}
        } else if (!conditionId.equals(other.conditionId)) {
			return false;
		}
        if (conditionPeriodUnit == null) {
            if (other.conditionPeriodUnit != null) {
				return false;
			}
        } else if (!conditionPeriodUnit.equals(other.conditionPeriodUnit)) {
			return false;
		}
        if (conditionPeriodValue == null) {
            if (other.conditionPeriodValue != null) {
				return false;
			}
        } else if (!conditionPeriodValue.equals(other.conditionPeriodValue)) {
			return false;
		}
        if (conditionStartDate == null) {
            if (other.conditionStartDate != null) {
				return false;
			}
        } else if (!conditionStartDate.equals(other.conditionStartDate)) {
			return false;
		}
        if (conditionSubCategory == null) {
            if (other.conditionSubCategory != null) {
				return false;
			}
        } else if (!conditionSubCategory.equals(other.conditionSubCategory)) {
			return false;
		}
        if (conditionType == null) {
            if (other.conditionType != null) {
				return false;
			}
        } else if (!conditionType.equals(other.conditionType)) {
			return false;
		}
        if (flag == null) {
            if (other.flag != null) {
				return false;
			}
        } else if (!flag.equals(other.flag)) {
			return false;
		}
        if (isConditionSatisfied == null) {
            if (other.isConditionSatisfied != null) {
				return false;
			}
        } else if (!isConditionSatisfied.equals(other.isConditionSatisfied)) {
			return false;
		}
        if (moduleAssociations == null) {
            if (other.moduleAssociations != null) {
				return false;
			}
        } else if (!moduleAssociations.equals(other.moduleAssociations)) {
			return false;
		}
        if (shortName == null) {
            if (other.shortName != null) {
				return false;
			}
        } else if (!shortName.equals(other.shortName)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ConditionEntity [conditionId=" + conditionId + ", conditionCreateDate=" + conditionCreateDate + ", conditionStartDate=" + conditionStartDate
                + ", conditionEndDate=" + conditionEndDate + ", conditionComments=" + conditionComments + ", conditionType=" + conditionType + ", conditionCategory="
                + conditionCategory + ", conditionSubCategory=" + conditionSubCategory + ", conditionPeriodUnit=" + conditionPeriodUnit + ", conditionDistanceUnit="
                + conditionDistanceUnit + ", conditionCurrencyUnit=" + conditionCurrencyUnit + ", conditionPeriodValue=" + conditionPeriodValue
                + ", conditionDistanceValue=" + conditionDistanceValue + ", conditionCurrencyAmount=" + conditionCurrencyAmount + ", conditionDetails=" + conditionDetails
                + ", isConditionSatisfied=" + isConditionSatisfied + ", conditionAmendmentDate=" + conditionAmendmentDate + ", conditionAmendmentStatus="
                + conditionAmendmentStatus + ", moduleAssociations=" + moduleAssociations + ", shortName=" + shortName + ", stamp=" + stamp + ", active=" + active + "]";
    }

}
