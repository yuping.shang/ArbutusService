package syscon.arbutus.product.services.legal.contract.dto.caseinformation;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * The representation of the case identifier configuration type
 */
public class CaseIdentifierConfigType implements Serializable {

    private static final long serialVersionUID = 2218607493708719249L;

    private Long identifierConfigId;
    @NotNull
    private String identifierType;
    private String identifierFormat;
    private Boolean autoGeneration;
    @NotNull
    private Boolean active;
    @NotNull
    private Boolean primary;
    private Boolean duplicateCheck;

    /**
     * Default constructor
     */
    public CaseIdentifierConfigType() {
    }

    /**
     * Constructor
     *
     * @param identifierConfigId Long - Ignored for creation; Required for update. - The unique Id for each identifier configuration record
     * @param identifierType     String - Required - A kind of identifier or number that can be associated to a criminal case. E.g. Court Information Number, Indictment Number
     * @param identifierFormat   String - Ignored - Not in use. Format used when auto-generating case numbers (e.g. 000000A)
     * @param autoGeneration     Boolean - Ignored - Not in use. If “true”, system auto generates the case number using the format specified by the CaseIdentifierFormat
     * @param active             Boolean - Required - To be set appropriately to ensure that only one configuration per identifier type is active at any time
     * @param primary            Boolean - Required - Indicates if the identifier is primary. If an identifier is flagged as primary
     * @param duplicateCheck     Boolean - Ignored - Not in use. If set to true, then this identifier participates in the uniqueness check for a legal entry.
     */
    public CaseIdentifierConfigType(Long identifierConfigId, @NotNull String identifierType, String identifierFormat, Boolean autoGeneration, @NotNull Boolean active,
            @NotNull Boolean primary, Boolean duplicateCheck) {
        this.identifierConfigId = identifierConfigId;
        this.identifierType = identifierType;
        //this.identifierFormat = identifierFormat;
        //this.autoGeneration = autoGeneration;
        this.active = active;
        this.primary = primary;
        //this.duplicateCheck = duplicateCheck;
    }

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * <p>identifierType, active and primary are not null. </p>
     *
     * @return Boolean Return true if all required properties are set, false otherwise.
     */
    @Deprecated
    public Boolean isWellFormed() {

        if (identifierType == null || active == null || primary == null) {
			return false;
		}

        return true;
    }

    /**
     * Verify if properties are set properly.
     * <p>Max length of identifierType is 64.</p>
     * <p>Max length of identifierFormat is 64.</p>
     *
     * @return Boolean Return true if all required properties are set and all values are valid, false otherwise.
     */
    @Deprecated
    public Boolean isValid() {
        if (!isWellFormed()) {
            return false;
        }

        if (identifierType == null || active == null || primary == null) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the identifierConfigId property.
     *
     * @return Long - the unique Id for each identifier configuration record
     */
    public Long getIdentifierConfigId() {
        return identifierConfigId;
    }

    /**
     * Sets the value of the identifierConfigId property.
     *
     * @param identifierConfigId Long - the unique Id for each identifier configuration record
     */
    public void setIdentifierConfigId(Long identifierConfigId) {
        this.identifierConfigId = identifierConfigId;
    }

    /**
     * Gets the value of the identifierType property. It is a code value of CaseIdentifierType set.
     *
     * @return String - A kind of identifier or number that can be associated to a criminal case. E.g. Court Information Number, Indictment Number
     */
    public String getIdentifierType() {
        return identifierType;
    }

    /**
     * Sets the value of the identifierType property. It is a code value of CaseIdentifierType set.
     *
     * @param identifierType String - A kind of identifier or number that can be associated to a criminal case. E.g. Court Information Number, Indictment Number
     */
    public void setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
    }

    /**
     * Gets the value of the identifierFormat property.
     *
     * @return String - Not in use.
     */
    public String getIdentifierFormat() {
        return identifierFormat;
    }

    /**
     * Sets the value of the identifierFormat property.
     *
     * @param identifierFormat String -  Not in use.
     */
    public void setIdentifierFormat(String identifierFormat) {
        //this.identifierFormat = identifierFormat;
    }

    /**
     * Gets the value of the autoGeneration property.
     *
     * @return Boolean -  Not in use.
     */
    public Boolean isAutoGeneration() {
        return autoGeneration;
    }

    /**
     * Sets the value of the autoGeneration property.
     *
     * @param autoGeneration Boolean -  Not in use.
     */
    public void setAutoGeneration(Boolean autoGeneration) {
        //this.autoGeneration = autoGeneration;
    }

    /**
     * Gets the value of the active property.
     *
     * @return Boolean - To be set appropriately to ensure that only one configuration per identifier type is active at any time
     */
    public Boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     *
     * @param active Boolean - To be set appropriately to ensure that only one configuration per identifier type is active at any time
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * Gets the value of the primary property.
     *
     * @return Boolean - Indicates if the identifier is primary. If an identifier is flagged as primary
     */
    public Boolean isPrimary() {
        return primary;
    }

    /**
     * Sets the value of the primary property.
     *
     * @param primary Boolean - Indicates if the identifier is primary. If an identifier is flagged as primary
     */
    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

    /**
     * Gets the value of the duplicateCheck property.
     *
     * @return Boolean - Not in use.
     */
    public Boolean isDuplicateCheck() {
        return duplicateCheck;
    }

    /**
     * Sets the value of the duplicateCheck property.
     *
     * @param duplicateCheck Boolean -  Not in use.
     */
    public void setDuplicateCheck(Boolean duplicateCheck) {
        //this.duplicateCheck = duplicateCheck;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseIdentifierConfigType [identifierConfigId=" + identifierConfigId + ", identifierType=" + identifierType + ", identifierFormat=" + identifierFormat
                + ", autoGeneration=" + autoGeneration + ", active=" + active + ", primary=" + primary + ", duplicateCheck=" + duplicateCheck + "]";
    }

}
