package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * IntermittentScheduleEntity for Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdIntmSch 'Intermittent Schedule table for Order Sentence Module of Legal Service'
 * @since December 27, 2012
 */
@Entity
@Audited
@Table(name = "LEG_OrdIntmSch")
public class IntermittentScheduleEntity implements Serializable {

    private static final long serialVersionUID = 4037946620203715030L;

    /**
     * @DbComment interScheduleId 'Unique identification of a bail amount instance'
     */
    @Id
    @Column(name = "interScheduleId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDINTMSCH_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDINTMSCH_ID", sequenceName = "SEQ_LEG_ORDINTMSCH_ID", allocationSize = 1)
    private Long interScheduleId;

    /**
     * @DbComment dayOfWeek 'Day of week.'
     */
    @Column(name = "dayOfWeek", nullable = false, length = 64)
    @MetaCode(set = MetaSet.DAY)
    private String dayOfWeek;

    /**
     * @DbComment dayOfWeek 'Day of week.'
     */
    @Column(name = "dayOutOfWeek", nullable = false, length = 64)
    @MetaCode(set = MetaSet.DAY)
    private String dayOutOfWeek;

    /**
     * @DbComment startHour 'The hour of the start time'
     */
    @Column(name = "startHour", nullable = true)
    private Long startHour;

    /**
     * @DbComment startMinute 'The minute of the start time'
     */
    @Column(name = "startMinute", nullable = true)
    private Long startMinute;

    /**
     * @DbComment startSecond 'The second of the start time'
     */
    @Column(name = "startSecond", nullable = true)
    private Long startSecond;

    /**
     * @DbComment endHour 'The hour of the end time'
     */
    @Column(name = "endHour", nullable = true)
    private Long endHour;

    /**
     * @DbComment endMinute 'The minute of the end time'
     */
    @Column(name = "endMinute", nullable = true)
    private Long endMinute;

    /**
     * @DbComment endSecond 'The second of the end time'
     */
    @Column(name = "endSecond", nullable = true)
    private Long endSecond;

    /**
     * @DbComment reportingDate 'Intermittent reporting date'
     */
    @Column(name = "reportingDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date reportingDate;

    /**
     * @DbComment scheduleEndDate 'Intermittent schedule end date'
     */
    @Column(name = "scheduleEndDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduleEndDate;

    /**
     * @DbComment LocationId 'The reporting location'
     */
    @Column(name = "locationId", nullable = false)
    private Long locationId;

    /**
     * @DbComment sameDay 'The same day flag'
     */
    @Column(name = "sameDay", nullable = false)
    private Long sameDay;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment orderId 'Unique identification of a order instance, foreign key to LEG_ORDORDER table'
     */
    @ForeignKey(name = "Fk_LEG_OrdIntmSch")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orderId", nullable = false)
    private SentenceEntity sentence;

    //	@ForeignKey(name= "FK_LEG_OrdIntmSch_ISS")
    //	@OneToMany(mappedBy = "intermittentSchedule", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    //	@BatchSize(size=20)
    //	private Set<IntermittentSentenceScheduleEntity> intermittentSentenceSchedules = new HashSet<IntermittentSentenceScheduleEntity>();

    /**
     * @DbComment scheduleIdentification 'Schedule ID of ScheduleEntity'
     */
    @Column(name = "scheduleIdentification", nullable = true)
    private Long scheduleIdentification;

    /**
     * Constructor
     */
    public IntermittentScheduleEntity() {
        super();
    }

    /**
     * @return the interScheduleId
     */
    public Long getInterScheduleId() {
        return interScheduleId;
    }

    /**
     * @param interScheduleId the interScheduleId to set
     */
    public void setInterScheduleId(Long interScheduleId) {
        this.interScheduleId = interScheduleId;
    }

    /**
     * @return the dayOfWeek
     */
    public String getDayOfWeek() {
        return dayOfWeek;
    }

    /**
     * @param dayOfWeek the dayOfWeek to set
     */
    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    /**
     * @return the dayOutOfWeek
     */
    public String getDayOutOfWeek() {
        return dayOutOfWeek;
    }

    /**
     * @param dayOutOfWeek the dayOutOfWeek to set
     */
    public void setDayOutOfWeek(String dayOutOfWeek) {
        this.dayOutOfWeek = dayOutOfWeek;
    }

    /**
     * @return the startHour
     */
    public Long getStartHour() {
        return startHour;
    }

    /**
     * @param startHour the startHour to set
     */
    public void setStartHour(Long startHour) {
        this.startHour = startHour;
    }

    /**
     * @return the startMinute
     */
    public Long getStartMinute() {
        return startMinute;
    }

    /**
     * @param startMinute the startMinute to set
     */
    public void setStartMinute(Long startMinute) {
        this.startMinute = startMinute;
    }

    /**
     * @return the startSecond
     */
    public Long getStartSecond() {
        return startSecond;
    }

    /**
     * @param startSecond the startSecond to set
     */
    public void setStartSecond(Long startSecond) {
        this.startSecond = startSecond;
    }

    /**
     * @return the endHour
     */
    public Long getEndHour() {
        return endHour;
    }

    /**
     * @param endHour the endHour to set
     */
    public void setEndHour(Long endHour) {
        this.endHour = endHour;
    }

    /**
     * @return the endMinute
     */
    public Long getEndMinute() {
        return endMinute;
    }

    /**
     * @param endMinute the endMinute to set
     */
    public void setEndMinute(Long endMinute) {
        this.endMinute = endMinute;
    }

    /**
     * @return the endSecond
     */
    public Long getEndSecond() {
        return endSecond;
    }

    /**
     * @param endSecond the endSecond to set
     */
    public void setEndSecond(Long endSecond) {
        this.endSecond = endSecond;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the sentence
     */
    public SentenceEntity getSentence() {
        return sentence;
    }

    /**
     * @param sentence the sentence to set
     */
    public void setSentence(SentenceEntity sentence) {
        this.sentence = sentence;
    }

    /**
     * @return the reportingDate
     */
    public Date getReportingDate() {
        return reportingDate;
    }

    /**
     * @param reportingDate the reportingDate to set
     */
    public void setReportingDate(Date reportingDate) {
        this.reportingDate = reportingDate;
    }

    /**
     * @return the scheduleEndDate
     */
    public Date getScheduleEndDate() {
        return scheduleEndDate;
    }

    /**
     * @param scheduleEndDate the scheduleEndDate to set
     */
    public void setScheduleEndDate(Date scheduleEndDate) {
        this.scheduleEndDate = scheduleEndDate;
    }

    /**
     * @return locationId
     */
    public Long getLocationId() {
        return locationId;
    }

    /**
     * @param locationId the locationId to set
     */
    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    /**
     * @return the sameDay
     */
    public Long getSameDay() {
        return sameDay;
    }

    /**
     * @param sameDay the sameDay to set
     */
    public void setSameDay(Long sameDay) {
        this.sameDay = sameDay;
    }

    /**
     * @return the scheduleIdentification
     */
    public Long getScheduleIdentification() {
        return scheduleIdentification;
    }

    /**
     * @param scheduleIdentification the scheduleIdentification to set
     */
    public void setScheduleIdentification(Long scheduleIdentification) {
        this.scheduleIdentification = scheduleIdentification;
    }

    //	/**
    //	 * @return the intermittentSentenceSchedules
    //	 */
    //	public Set<IntermittentSentenceScheduleEntity> getIntermittentSentenceSchedules() {
    //		return intermittentSentenceSchedules;
    //	}
    //
    //	/**
    //	 * @param intermittentSentenceSchedules the intermittentSentenceSchedules to set
    //	 */
    //	public void setIntermittentSentenceSchedules(Set<IntermittentSentenceScheduleEntity> intermittentSentenceSchedules) {
    //		this.intermittentSentenceSchedules = intermittentSentenceSchedules;
    //	}
    //
    //	public void addIntermittentSentenceSchedule(IntermittentSentenceScheduleEntity intermittentSentenceSchedule) {
    //		intermittentSentenceSchedule.setIntermittentSchedule(this);
    //		this.intermittentSentenceSchedules.add(intermittentSentenceSchedule);
    //	}

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dayOutOfWeek == null) ? 0 : dayOutOfWeek.hashCode());
        result = prime * result + ((dayOfWeek == null) ? 0 : dayOfWeek.hashCode());
        result = prime * result + ((endHour == null) ? 0 : endHour.hashCode());
        result = prime * result + ((endMinute == null) ? 0 : endMinute.hashCode());
        result = prime * result + ((endSecond == null) ? 0 : endSecond.hashCode());
        result = prime * result + ((interScheduleId == null) ? 0 : interScheduleId.hashCode());
        result = prime * result + ((reportingDate == null) ? 0 : reportingDate.hashCode());
        result = prime * result + ((scheduleEndDate == null) ? 0 : scheduleEndDate.hashCode());
        result = prime * result + ((startHour == null) ? 0 : startHour.hashCode());
        result = prime * result + ((startMinute == null) ? 0 : startMinute.hashCode());
        result = prime * result + ((startSecond == null) ? 0 : startSecond.hashCode());
        result = prime * result + ((locationId == null) ? 0 : locationId.hashCode());
        result = prime * result + ((sameDay == null) ? 0 : sameDay.hashCode());
        result = prime * result + ((scheduleIdentification == null) ? 0 : scheduleIdentification.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        IntermittentScheduleEntity other = (IntermittentScheduleEntity) obj;
        if (dayOfWeek == null) {
            if (other.dayOfWeek != null) {
				return false;
			}
        } else if (!dayOfWeek.equals(other.dayOfWeek)) {
			return false;
		}
        if (dayOutOfWeek == null) {
            if (other.dayOutOfWeek != null) {
				return false;
			}
        } else if (!dayOutOfWeek.equals(other.dayOutOfWeek)) {
			return false;
		}
        if (endHour == null) {
            if (other.endHour != null) {
				return false;
			}
        } else if (!endHour.equals(other.endHour)) {
			return false;
		}
        if (endMinute == null) {
            if (other.endMinute != null) {
				return false;
			}
        } else if (!endMinute.equals(other.endMinute)) {
			return false;
		}
        if (endSecond == null) {
            if (other.endSecond != null) {
				return false;
			}
        } else if (!endSecond.equals(other.endSecond)) {
			return false;
		}
        if (interScheduleId == null) {
            if (other.interScheduleId != null) {
				return false;
			}
        } else if (!interScheduleId.equals(other.interScheduleId)) {
			return false;
		}
        if (reportingDate == null) {
            if (other.reportingDate != null) {
				return false;
			}
        } else if (!reportingDate.equals(other.reportingDate)) {
			return false;
		}
        if (scheduleEndDate == null) {
            if (other.scheduleEndDate != null) {
				return false;
			}
        } else if (!scheduleEndDate.equals(other.scheduleEndDate)) {
			return false;
		}
        if (startHour == null) {
            if (other.startHour != null) {
				return false;
			}
        } else if (!startHour.equals(other.startHour)) {
			return false;
		}
        if (startMinute == null) {
            if (other.startMinute != null) {
				return false;
			}
        } else if (!startMinute.equals(other.startMinute)) {
			return false;
		}
        if (startSecond == null) {
            if (other.startSecond != null) {
				return false;
			}
        } else if (!startSecond.equals(other.startSecond)) {
			return false;
		}
        if (locationId == null) {
            if (other.locationId != null) {
				return false;
			}
        } else if (!locationId.equals(other.locationId)) {
			return false;
		}
        if (sameDay == null) {
            if (other.sameDay != null) {
				return false;
			}
        } else if (!sameDay.equals(other.sameDay)) {
			return false;
		}
        if (scheduleIdentification == null) {
            if (other.scheduleIdentification != null) {
				return false;
			}
        } else if (!scheduleIdentification.equals(other.scheduleIdentification)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "IntermittentScheduleEntity [interScheduleId=" + interScheduleId + ", dayOfWeek=" + dayOfWeek + ", dayOutOfWeek=" + dayOutOfWeek + ", startHour="
                + startHour + ", startMinute=" + startMinute + ", startSecond=" + startSecond + ", endHour=" + endHour + ", endMinute=" + endMinute + ", endSecond="
                + endSecond + ", reportingDate=" + reportingDate + ", scheduleEndDate=" + scheduleEndDate + ", locationId=" + locationId + ", sameDay=" + sameDay
                + ", stamp=" + stamp + ", sentence=" + sentence + ", scheduleIdentification=" + scheduleIdentification + "]";
    }

}
