package syscon.arbutus.product.services.legal.contract.dto.charge;

import java.io.Serializable;

import syscon.arbutus.product.services.legal.contract.dto.ValidationUtil;

/**
 * The representation of the jurisdiction configuration search type
 */
public class JurisdictionConfigSearchType implements Serializable {

    private static final long serialVersionUID = 9021686715749421747L;

    private String description;
    private String country;
    private String stateProvince;
    private String city;
    private String county;
    private Long jurisdictionId;

    /**
     * Default constructor
     */
    public JurisdictionConfigSearchType() {
    }

    /**
     * Constructor
     *
     * @param description   String - Description of the jurisdiction
     * @param country       String - Country the jurisdiction applies to
     * @param stateProvince String - State/province the jurisdiction applies to
     * @param city          String - A name of a city or town. (optional)
     * @param county        String - County the jurisdiction applies to  (optional)
     */
    public JurisdictionConfigSearchType(String description, String country, String stateProvince, String city, String county, Long jurisdictionId) {
        this.description = description;
        this.country = country;
        this.stateProvince = stateProvince;
        this.city = city;
        this.county = county;
        this.jurisdictionId = jurisdictionId;
    }

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set, false otherwise.
     */
    @Deprecated
    public Boolean isWellFormed() {

        if (ValidationUtil.isEmpty(description) && ValidationUtil.isEmpty(country) && ValidationUtil.isEmpty(stateProvince) && ValidationUtil.isEmpty(city)
                && ValidationUtil.isEmpty(county) && ValidationUtil.isEmpty(jurisdictionId)) {
			return false;
		}

        return true;
    }

    /**
     * Verify if properties are set properly.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set and all values are valid, false otherwise.
     */
    @Deprecated
    public Boolean isValid() {

        if (!isWellFormed()) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the description property.
     *
     * @return String - Description of the jurisdiction
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param description String - Description of the jurisdiction
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the value of the country property. It is a code value of Country set.
     *
     * @return String - Country the jurisdiction applies to
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property. It is a code value of Country set.
     *
     * @param country String - Country the jurisdiction applies to
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets the value of the stateProvince property. It is a code value of StateProvince set.
     *
     * @return String - State/province the jurisdiction applies to
     */
    public String getStateProvince() {
        return stateProvince;
    }

    /**
     * Sets the value of the stateProvince property. It is a code value of StateProvince set.
     *
     * @param stateProvince String - State/province the jurisdiction applies to
     */
    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    /**
     * Gets the value of the city property.
     *
     * @return String - A name of a city or town.
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     *
     * @param city String - A name of a city or town.
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Gets the value of the county property. It is a code value of StatuteCounty set.
     *
     * @return String - County the jurisdiction applies to
     */
    public String getCounty() {
        return county;
    }

    /**
     * Sets the value of the county property. It is a code value of StatuteCounty set.
     *
     * @param county String - County the jurisdiction applies to
     */
    public void setCounty(String county) {
        this.county = county;
    }

    /**
     * @return the jurisdictionId
     */
    public Long getJurisdictionId() {
        return jurisdictionId;
    }

    /**
     * @param jurisdictionId the jurisdictionId to set
     */
    public void setJurisdictionId(Long jurisdictionId) {
        this.jurisdictionId = jurisdictionId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "JurisdictionConfigSearchType [description=" + description + ", country=" + country + ", stateProvince=" + stateProvince + ", city=" + city + ", county="
                + county + ", jurisdictionId=" + jurisdictionId + "]";
    }

}
