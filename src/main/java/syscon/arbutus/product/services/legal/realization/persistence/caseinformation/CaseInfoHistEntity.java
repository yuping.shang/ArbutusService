package syscon.arbutus.product.services.legal.realization.persistence.caseinformation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the case information history entity.
 *
 * @DbComment LEG_CICaseInfoHist 'The case information history table'
 */
//@Audited
@Entity
@Table(name = "LEG_CICaseInfoHist")
public class CaseInfoHistEntity implements Serializable {

    private static final long serialVersionUID = 8274492402074857937L;

    /**
     * @DbComment CaseInfoHistoryId 'Unique Id of a case information history instance'
     */
    @Id
    @Column(name = "CaseInfoHistoryId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CICaseInfoHist_Id")
    @SequenceGenerator(name = "SEQ_LEG_CICaseInfoHist_Id", sequenceName = "SEQ_LEG_CICaseInfoHist_Id", allocationSize = 1)
    private Long caseInfoHistoryId;

    /**
     * @DbComment CaseInfoId 'Unique identification of a case information instance'
     */
    @Column(name = "CaseInfoId", nullable = false)
    private Long caseInfoId;

    /**
     * @DbComment CaseCategory 'The category of the case, e.g. InJurisdiction case, Out of Jurisdiction case.'
     */
    @Column(name = "CaseCategory", nullable = false, length = 64)
    private String caseCategory;

    /**
     * @DbComment CaseType 'Indicates the type of case eg. immigration, traffic, criminal.'
     */
    @Column(name = "CaseType", nullable = false, length = 64)
    private String caseType;

    /**
     * @DbComment CreatedDate 'The date when the case was first created.'
     */
    @Column(name = "CreatedDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    /**
     * @DbComment IssuedDate 'The date on which the case was issued (by the court).'
     */
    @Column(name = "IssuedDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date issuedDate;

    /**
     * @DbComment FacilityId 'Used to indicate the originating facility name, type and location for a case. Static reference to the Facility service. '
     */
    @Column(name = "FacilityId", nullable = true)
    private Long facilityId;

    /**
     * @DbComment SupervisionId 'Reference to a Supervision period. Static reference to the Supervision service.'
     */
    @Column(name = "SupervisionId", nullable = false)
    private Long supervisionId;

    /**
     * @DbComment Diverted 'Indicates whether the case was attempted to be diverted'
     */
    @Column(name = "Diverted", nullable = true)
    private Boolean diverted;

    /**
     * @DbComment DivertedSuccess 'Indicates whether a case diversion (if attempted) was successful or unsuccessful. Only relevant if isDivertedFlag is true'
     */
    @Column(name = "DivertedSuccess", nullable = true)
    private Boolean divertedSuccess;

    /**
     * @DbComment Active 'Indicates whether the case is active or inactive. This is specifically set by the user, not derived.'
     */
    @Column(name = "Active", nullable = false)
    private Boolean active;

    /**
     * @DbComment CaseStatusReason 'Indicates the reason a case status was changed.'
     */
    @Column(name = "CaseStatusReason", nullable = true, length = 64)
    private String caseStatusReason;

    /**
     * @DbComment SentenceStatus 'Indicates whether the offender was sentenced to be incarcerated.'
     */
    @Column(name = "SentenceStatus", nullable = true, length = 64)
    private String sentenceStatus;

    /**
     * @DbComment CommentUserId 'User ID who created/updated the comments'
     * @DbComment CommentDate 'The Date/Time when the comment was created'
     * @DbComment CommentText 'The comment text'
     */
    @Embedded
    private CommentEntity comment;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    @OneToMany(mappedBy = "caseInfoHistory", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<CaseIdentifierHistEntity> caseIdentifiers;

    @OneToMany(mappedBy = "caseInfoHistory", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<CaseInfoModuleAssociationHistEntity> moduleAssociations;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CaseInfoId", insertable = false, updatable = false)
    private CaseInfoEntity caseInfo;

    public CaseInfoHistEntity() {
        super();
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the caseInfoHistoryId
     */
    public Long getCaseInfoHistoryId() {
        return caseInfoHistoryId;
    }

    /**
     * @param caseInfoHistoryId the caseInfoHistoryId to set
     */
    public void setCaseInfoHistoryId(Long caseInfoHistoryId) {
        this.caseInfoHistoryId = caseInfoHistoryId;
    }

    /**
     * caseInfoHistory
     *
     * @return the caseInfoId
     */
    public Long getCaseInfoId() {
        return caseInfoId;
    }

    /**
     * @param caseInfoId the caseInfoId to set
     */
    public void setCaseInfoId(Long caseInfoId) {
        this.caseInfoId = caseInfoId;
    }

    /**
     * @return the caseCategory
     */
    public String getCaseCategory() {
        return caseCategory;
    }

    /**
     * @param caseCategory the caseCategory to set
     */
    public void setCaseCategory(String caseCategory) {
        this.caseCategory = caseCategory;
    }

    /**
     * @return the caseType
     */
    public String getCaseType() {
        return caseType;
    }

    /**
     * @param caseType the caseType to set
     */
    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    /**
     * @return the createdDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the issuedDate
     */
    public Date getIssuedDate() {
        return issuedDate;
    }

    /**
     * @param issuedDate the issuedDate to set
     */
    public void setIssuedDate(Date issuedDate) {
        this.issuedDate = issuedDate;
    }

    /**
     * @return the facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * @param facilityId the facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * @return the diverted
     */
    public Boolean getDiverted() {
        return diverted;
    }

    /**
     * @param diverted the diverted to set
     */
    public void setDiverted(Boolean diverted) {
        this.diverted = diverted;
    }

    /**
     * @return the divertedSuccess
     */
    public Boolean getDivertedSuccess() {
        return divertedSuccess;
    }

    /**
     * @param divertedSuccess the divertedSuccess to set
     */
    public void setDivertedSuccess(Boolean divertedSuccess) {
        this.divertedSuccess = divertedSuccess;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the caseStatusReason
     */
    public String getCaseStatusReason() {
        return caseStatusReason;
    }

    /**
     * @param caseStatusReason the caseStatusReason to set
     */
    public void setCaseStatusReason(String caseStatusReason) {
        this.caseStatusReason = caseStatusReason;
    }

    /**
     * @return the sentenceStatus
     */
    public String getSentenceStatus() {
        return sentenceStatus;
    }

    /**
     * @param sentenceStatus the sentenceStatus to set
     */
    public void setSentenceStatus(String sentenceStatus) {
        this.sentenceStatus = sentenceStatus;
    }

    /**
     * @return the comment
     */
    public CommentEntity getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(CommentEntity comment) {
        this.comment = comment;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the caseIdentifiers
     */
    public Set<CaseIdentifierHistEntity> getCaseIdentifiers() {
        if (caseIdentifiers == null) {
            caseIdentifiers = new HashSet<CaseIdentifierHistEntity>();
        }
        return caseIdentifiers;
    }

    /**
     * @param caseIdentifiers the caseIdentifiers to set
     */
    public void setCaseIdentifiers(Set<CaseIdentifierHistEntity> caseIdentifiers) {
        this.caseIdentifiers = caseIdentifiers;
    }

    /**
     * @return the moduleAssociations
     */
    public Set<CaseInfoModuleAssociationHistEntity> getModuleAssociations() {
        return moduleAssociations;
    }

    /**
     * @param moduleAssociations the moduleAssociations to set
     */
    public void setModuleAssociations(Set<CaseInfoModuleAssociationHistEntity> moduleAssociations) {
        this.moduleAssociations = moduleAssociations;
    }

    /**
     * @return the caseInfo
     */
    public CaseInfoEntity getCaseInfo() {
        return caseInfo;
    }

    /**
     * @param caseInfo the caseInfo to set
     */
    public void setCaseInfo(CaseInfoEntity caseInfo) {
        this.caseInfo = caseInfo;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseInfoHistEntity [caseInfoHistoryId=" + caseInfoHistoryId + ", caseInfoId=" + caseInfoId + ", caseCategory=" + caseCategory + ", caseType=" + caseType
                + ", createdDate=" + createdDate + ", issuedDate=" + issuedDate + ", facilityId=" + facilityId + ", supervisionId=" + supervisionId + ", diverted="
                + diverted + ", divertedSuccess=" + divertedSuccess + ", active=" + active + ", caseStatusReason=" + caseStatusReason + ", sentenceStatus="
                + sentenceStatus + ", comment=" + comment + ", stamp=" + stamp + ", caseIdentifiers=" + caseIdentifiers + "]";
    }

}
