package syscon.arbutus.product.services.legal.contract.dto.charge;

import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.legal.contract.dto.ValidationUtil;

/**
 * The representation of the charge disposition search type
 */
public class ChargeDispositionSearchType implements Serializable {

    private static final long serialVersionUID = -1694708772069073413L;

    private String dispositionOutcome;
    private String chargeStatus;
    private Date dispositionDateFrom;
    private Date dispositionDateTo;

    /**
     * Default constructor
     */
    public ChargeDispositionSearchType() {
    }

    /**
     * Constructor
     * At least one of the search criteria must be specified.
     *
     * @param dispositionOutcome  String - A result or outcome of the disposition
     * @param chargeStatus        String - The status of a charge (active, inactive)
     * @param dispositionDateFrom Date - Date of the disposition
     * @param dispositionDateTo   Date - Date of the disposition
     */
    public ChargeDispositionSearchType(String dispositionOutcome, String chargeStatus, Date dispositionDateFrom, Date dispositionDateTo) {
        this.dispositionOutcome = dispositionOutcome;
        this.chargeStatus = chargeStatus;
        this.dispositionDateFrom = dispositionDateFrom;
        this.dispositionDateTo = dispositionDateTo;
    }

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set, false otherwise.
     */
    @Deprecated
    public Boolean isWellFormed() {

        if (ValidationUtil.isEmpty(dispositionOutcome) && ValidationUtil.isEmpty(chargeStatus) && ValidationUtil.isEmpty(dispositionDateFrom) && ValidationUtil.isEmpty(
                dispositionDateTo)) {
			return false;
		}

        return true;
    }

    /**
     * Verify if properties are set properly.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set and all values are valid, false otherwise.
     */
    @Deprecated
    public Boolean isValid() {

        if (!isWellFormed()) {
			return false;
		}

        //toDate cannot before the fromDate
        if (!ValidationUtil.isValid(dispositionDateFrom, dispositionDateTo)) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the dispositionOutcome property. It is a reference code of ChargeOutcome set.
     *
     * @return CodeType - A result or outcome of the disposition
     */
    public String getDispositionOutcome() {
        return dispositionOutcome;
    }

    /**
     * Sets the value of the dispositionOutcome property. It is a reference code of ChargeOutcome set.
     *
     * @param dispositionOutcome String - A result or outcome of the disposition
     */
    public void setDispositionOutcome(String dispositionOutcome) {
        this.dispositionOutcome = dispositionOutcome;
    }

    /**
     * Gets the value of the chargeStatus property. It is a reference code of ChargeStatus set.
     *
     * @return String - The status of a charge (active, inactive)
     */
    public String getChargeStatus() {
        return chargeStatus;
    }

    /**
     * Sets the value of the chargeStatus property. It is a reference code of ChargeStatus set.
     *
     * @param chargeStatus String - The status of a charge (active, inactive)
     */
    public void setChargeStatus(String chargeStatus) {
        this.chargeStatus = chargeStatus;
    }

    /**
     * Gets the value of the dispositionDateFrom property.
     *
     * @return Date - Date of the disposition
     */
    public Date getDispositionDateFrom() {
        return dispositionDateFrom;
    }

    /**
     * Sets the value of the dispositionDateFrom property.
     *
     * @param dispositionDateFrom Date - Date of the disposition
     */
    public void setDispositionDateFrom(Date dispositionDateFrom) {
        this.dispositionDateFrom = dispositionDateFrom;
    }

    /**
     * Gets the value of the dispositionDateTo property.
     *
     * @return Date - Date of the disposition
     */
    public Date getDispositionDateTo() {
        return dispositionDateTo;
    }

    /**
     * Sets the value of the dispositionDateTo property.
     *
     * @param dispositionDateTo Date - Date of the disposition
     */
    public void setDispositionDateTo(Date dispositionDateTo) {
        this.dispositionDateTo = dispositionDateTo;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeDispositionSearchType [dispositionOutcome=" + dispositionOutcome + ", chargeStatus=" + chargeStatus + ", dispositionDateFrom=" + dispositionDateFrom
                + ", dispositionDateTo=" + dispositionDateTo + "]";
    }

}
