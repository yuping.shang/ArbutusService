package syscon.arbutus.product.services.legal.realization.persistence.caseactivity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the case activity history entity.
 *
 * @author lhan
 * @DbComment LEG_CACaseActHist 'The case activity history table for CaseActivity service'
 * @DbComment .DTYPE 'Dicriminator column for Hibernate use'
 */
//@Audited
@Entity
@Table(name = "LEG_CACaseActHist")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class CaseActivityHistEntity implements Serializable {

    private static final long serialVersionUID = -1142889381705058186L;

    /**
     * @DbComment 'Case Activity History Identification. The unique ID for each case related event/activity history created in the system.'
     */
    @Id
    @Column(name = "CaseActivityHistId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "Seq_LEG_CACaseActHist_Id")
    @SequenceGenerator(name = "Seq_LEG_CACaseActHist_Id", sequenceName = "Seq_LEG_CACaseActHist_Id", allocationSize = 1)
    private Long caseActivityHistId;

    /**
     * @DbComment 'Case Activity Identification. The unique ID for each case related event/activity created in the system.'
     */
    @Column(name = "CaseActivityId", nullable = false)
    private Long caseActivityId;

    /**
     * @DbComment 'The date the case event or activity occurred.'
     */
    @Column(name = "ActivityOccurredDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date activityOccurredDate;

    /**
     * @DbComment 'This determines which specific type of case activity is created. E.g Court, Probation, Police.'
     */
    @Column(name = "ActivityCategory", nullable = false, length = 64)
    private String activityCategory;

    /**
     * @DbComment 'The specific event type that falls under the activity category. E.g. Arraignment, Trial, Sentencing.'
     */
    @Column(name = "ActivityType", nullable = false, length = 64)
    private String activityType;

    /**
     * @DbComment 'An outcome that may be recorded as a result of the case activity.'
     */
    @Column(name = "ActivityOutcome", nullable = true, length = 64)
    private String activityOutcome;

    /**
     * @DbComment 'The date the case event outcome is recorded or updated.'
     */
    @Column(name = "OutcomeDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date outcomeDate;

    /**
     * @DbComment 'The status of the case activity. Either ACTIVE or INACTIVE.'
     */
    @Column(name = "ActivityStatus", nullable = false, length = 64)
    private String activityStatus;

    /**
     * @DbComment 'Comment associated with the case event or activity.'
     */
    @Column(name = "CommentText", nullable = true, length = 512)
    private String commentText;

    /**
     * @DbComment 'Date/Time when the comment was created.'
     */
    @Column(name = "CommentDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentDate;

    @ForeignKey(name = "Fk_LEG_CAChargeAsscHist")
    @OneToMany(mappedBy = "caseActivity", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<CAChargeAssociationHistEntity> chargeAssociations;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CaseActivityId", insertable = false, updatable = false)
    private CaseActivityEntity caseActivity;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    @Column(name = "version")
    private Long version;

    /**
     * Default constructor
     */
    public CaseActivityHistEntity() {

    }

    /**
     * Constructor
     *
     * @param caseActivityId
     * @param activityOccurredDate
     * @param activityCategory
     * @param activityType
     * @param activityOutcome
     * @param outcomeDate
     * @param activityStatus
     * @param comment
     * @param commentDate
     * @param chargeAssociations
     * @param associations
     * @param privileges
     * @param stamp
     */
    public CaseActivityHistEntity(Long caseActivityId, Date activityOccurredDate, String activityCategory, String activityType, String activityOutcome, Date outcomeDate,
            String activityStatus, String commentText, Date commentDate, Set<CAChargeAssociationHistEntity> chargeAssociations, CaseActivityEntity caseActivity,
            StampEntity stamp) {
        super();
        this.caseActivityId = caseActivityId;
        this.activityOccurredDate = activityOccurredDate;
        this.activityCategory = activityCategory;
        this.activityType = activityType;
        this.activityOutcome = activityOutcome;
        this.outcomeDate = outcomeDate;
        this.activityStatus = activityStatus;
        this.commentText = commentText;
        this.commentDate = commentDate;
        this.chargeAssociations = chargeAssociations;
        this.caseActivity = caseActivity;
        this.setStamp(stamp);
    }

    /**
     * @return the caseActivityHistId
     */
    public Long getCaseActivityHistId() {
        return caseActivityHistId;
    }

    /**
     * @param caseActivityHistId the caseActivityHistId to set
     */
    public void setCaseActivityHistId(Long caseActivityHistId) {
        this.caseActivityHistId = caseActivityHistId;
    }

    /**
     * @return the caseActivityId
     */
    public Long getCaseActivityId() {
        return caseActivityId;
    }

    /**
     * @param caseActivityId the caseActivityId to set
     */
    public void setCaseActivityId(Long caseActivityId) {
        this.caseActivityId = caseActivityId;
    }

    /**
     * @return the activityOccurredDate
     */
    public Date getActivityOccurredDate() {
        return activityOccurredDate;
    }

    /**
     * @param activityOccurredDate the activityOccurredDate to set
     */
    public void setActivityOccurredDate(Date activityOccurredDate) {
        this.activityOccurredDate = activityOccurredDate;
    }

    /**
     * @return the activityCategory
     */
    public String getActivityCategory() {
        return activityCategory;
    }

    /**
     * @param activityCategory the activityCategory to set
     */
    public void setActivityCategory(String activityCategory) {
        this.activityCategory = activityCategory;
    }

    /**
     * @return the activityType
     */
    public String getActivityType() {
        return activityType;
    }

    /**
     * @param activityType the activityType to set
     */
    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    /**
     * @return the activityOutcome
     */
    public String getActivityOutcome() {
        return activityOutcome;
    }

    /**
     * @param activityOutcome the activityOutcome to set
     */
    public void setActivityOutcome(String activityOutcome) {
        this.activityOutcome = activityOutcome;
    }

    /**
     * @return the outcomeDate
     */
    public Date getOutcomeDate() {
        return outcomeDate;
    }

    /**
     * @param outcomeDate the outcomeDate to set
     */
    public void setOutcomeDate(Date outcomeDate) {
        this.outcomeDate = outcomeDate;
    }

    /**
     * @return the activityStatus
     */
    public String getActivityStatus() {
        return activityStatus;
    }

    /**
     * @param activityStatus the activityStatus to set
     */
    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }

    /**
     * @return the commentText
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * @param commentText the commentText to set
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    /**
     * @return the commentDate
     */
    public Date getCommentDate() {
        return commentDate;
    }

    /**
     * @param commentDate the commentDate to set
     */
    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    /**
     * @return the chargeAssociations
     */
    public Set<CAChargeAssociationHistEntity> getChargeAssociations() {
        return chargeAssociations;
    }

    /**
     * @param chargeAssociations the chargeAssociations to set
     */
    public void setChargeAssociations(Set<CAChargeAssociationHistEntity> chargeAssociations) {
        this.chargeAssociations = chargeAssociations;
    }

    /**
     * @return the caseActivity
     */
    public CaseActivityEntity getCaseActivity() {
        return caseActivity;
    }

    /**
     * @param caseActivity the caseActivity to set
     */
    public void setCaseActivity(CaseActivityEntity caseActivity) {
        this.caseActivity = caseActivity;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        //Only copy values not assign the stamp.
        this.stamp = new StampEntity();
        this.stamp.setCreateDateTime(stamp.getCreateDateTime());
        this.stamp.setCreateUserId(stamp.getCreateUserId());
        this.stamp.setInvocationContext(stamp.getInvocationContext());
        this.stamp.setModifyDateTime(stamp.getModifyDateTime());
        this.stamp.setModifyUserId(stamp.getModifyUserId());
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((stamp == null) ? 0 : stamp.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        CaseActivityHistEntity other = (CaseActivityHistEntity) obj;
        if (stamp == null) {
            if (other.stamp != null) {
				return false;
			}
        } else if (!stamp.equals(other.stamp)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseActivityHistEntity [getCaseActivityHistId()=" + getCaseActivityHistId() + ", getCaseActivityId()=" + getCaseActivityId()
                + ", getActivityOccurredDate()=" + getActivityOccurredDate() + ", getActivityCategory()=" + getActivityCategory() + ", getActivityType()="
                + getActivityType() + ", getActivityOutcome()=" + getActivityOutcome() + ", getOutcomeDate()=" + getOutcomeDate() + ", getActivityStatus()="
                + getActivityStatus() + ", getCommentText()=" + getCommentText() + ", getCommentDate()=" + getCommentDate() + ", getChargeAssociations()="
                + getChargeAssociations() + ", getCaseActivity()=" + getCaseActivity() + ", getStamp()=" + getStamp() + ", getVersion()=" + getVersion() + "]";
    }

}
