package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * @DbComment LEG_CHGIdentifierHist 'The charge information identifier history table'
 */
//@Audited
@Entity
@Table(name = "LEG_CHGIdentifierHist")
public class ChargeIdentifierHistEntity implements Serializable {

    private static final long serialVersionUID = 2520113733341370083L;

    /**
     * @DbComment IdentifierHistoryId 'Unique Id for each charge identifier history record'
     */
    @Id
    @Column(name = "IdentifierHistoryId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGIdentifierHist_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGIdentifierHist_Id", sequenceName = "SEQ_LEG_CHGIdentifierHist_Id", allocationSize = 1)
    private Long identifierHistoryId;

    /**
     * @DbComment IdentifierId 'Unique Id for charge identifier record'
     */
    @Column(name = "IdentifierId", nullable = false)
    private Long identifierId;

    /**
     * @DbComment IdentifierType 'A set of numbers/identifiers assigned by an arresting agency, prosecuting attorney or court. E.g. CJIS code'
     */
    @Column(name = "IdentifierType", length = 64, nullable = false)
    private String identifierType;

    /**
     * @DbComment IdentifierValue 'The number or value of the charge identifier'
     */
    @Column(name = "IdentifierValue", length = 64, nullable = false)
    private String identifierValue;

    /**
     * @DbComment IdentifierFormat 'The format for the Identifier. Will be used by the user interface.'
     */
    @Column(name = "IdentifierFormat", length = 64, nullable = false)
    private String identifierFormat;

    /**
     * @DbComment OrganizationId 'Static reference to an Organization which issued the charge identifier'
     */
    @Column(name = "OrganizationId", nullable = true)
    private Long organizationId;

    /**
     * @DbComment FacilityId 'Static reference to a Facility which issued the charge identifier'
     */
    @Column(name = "FacilityId", nullable = true)
    private Long facilityId;

    /**
     * @DbComment IdentifierComment 'Comments associated to the charge identifier'
     */
    @Column(name = "IdentifierComment", length = 1024, nullable = false)
    private String identifierComment;

    /**
     * @DbComment ChargeHistoryId 'Unique id of a charge history instance, foreign key to LEG_CRGChargeHist table'
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ChargeHistoryId", nullable = false)
    private ChargeHistEntity chargeHistory;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     *
     */
    public ChargeIdentifierHistEntity() {
    }

    /**
     * @param identifierHistoryId
     * @param identifierId
     * @param identifierType
     * @param identifierValue
     * @param identifierFormat
     * @param organizationId
     * @param facilityId
     * @param identifierComment
     */
    public ChargeIdentifierHistEntity(Long identifierHistoryId, Long identifierId, String identifierType, String identifierValue, String identifierFormat,
            Long organizationId, Long facilityId, String identifierComment) {
        this.identifierHistoryId = identifierHistoryId;
        this.identifierId = identifierId;
        this.identifierType = identifierType;
        this.identifierValue = identifierValue;
        this.identifierFormat = identifierFormat;
        this.organizationId = organizationId;
        this.facilityId = facilityId;
        this.identifierComment = identifierComment;
    }

    /**
     * @return the identifierHistoryId
     */
    public Long getIdentifierHistoryId() {
        return identifierHistoryId;
    }

    /**
     * @param identifierHistoryId the identifierHistoryId to set
     */
    public void setIdentifierHistoryId(Long identifierHistoryId) {
        this.identifierHistoryId = identifierHistoryId;
    }

    /**
     * @return the identifierId
     */
    public Long getIdentifierId() {
        return identifierId;
    }

    /**
     * @param identifierId the identifierId to set
     */
    public void setIdentifierId(Long identifierId) {
        this.identifierId = identifierId;
    }

    /**
     * @return the identifierType
     */
    public String getIdentifierType() {
        return identifierType;
    }

    /**
     * @param identifierType the identifierType to set
     */
    public void setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
    }

    /**
     * @return the identifierValue
     */
    public String getIdentifierValue() {
        return identifierValue;
    }

    /**
     * @param identifierValue the identifierValue to set
     */
    public void setIdentifierValue(String identifierValue) {
        this.identifierValue = identifierValue;
    }

    /**
     * @return the identifierFormat
     */
    public String getIdentifierFormat() {
        return identifierFormat;
    }

    /**
     * @param identifierFormat the identifierFormat to set
     */
    public void setIdentifierFormat(String identifierFormat) {
        this.identifierFormat = identifierFormat;
    }

    /**
     * @return the organizationId
     */
    public Long getOrganizationId() {
        return organizationId;
    }

    /**
     * @param organizationId the organizationId to set
     */
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * @return the facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * @param facilityId the facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * @return the identifierComment
     */
    public String getIdentifierComment() {
        return identifierComment;
    }

    /**
     * @param identifierComment the identifierComment to set
     */
    public void setIdentifierComment(String identifierComment) {
        this.identifierComment = identifierComment;
    }

    /**
     * @return the chargeHistory
     */
    public ChargeHistEntity getChargeHistory() {
        return chargeHistory;
    }

    /**
     * @param chargeHistory the chargeHistory to set
     */
    public void setChargeHistory(ChargeHistEntity chargeHistory) {
        this.chargeHistory = chargeHistory;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeIdentifierHistEntity [identifierHistoryId=" + identifierHistoryId + ", identifierId=" + identifierId + ", identifierType=" + identifierType
                + ", identifierValue=" + identifierValue + ", identifierFormat=" + identifierFormat + ", organizationId=" + organizationId + ", facilityId=" + facilityId
                + ", identifierComment=" + identifierComment + ", stamp=" + stamp + "]";
    }

}
