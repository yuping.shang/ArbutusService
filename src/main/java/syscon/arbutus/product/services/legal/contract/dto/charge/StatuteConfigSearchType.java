package syscon.arbutus.product.services.legal.contract.dto.charge;

import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.legal.contract.dto.ValidationUtil;

/**
 * The representation of the statute configuration search type
 */
public class StatuteConfigSearchType implements Serializable {

    private static final long serialVersionUID = -6721088969037362734L;

    private String statuteCode;
    private Date enactmentDateFrom;
    private Date enactmentDateTo;
    private Date repealDateFrom;
    private Date repealDateTo;
    private String section;
    private String description;
    private Boolean active;  //derived value
    private Boolean defaultJurisdiction;
    private Boolean outOfJurisdiction;
    private JurisdictionConfigSearchType jurisdictionSearch;

    /**
     * Default constructor
     */
    public StatuteConfigSearchType() {
    }

    /**
     * Constructor
     * At least one of the search criteria must be specified.
     *
     * @param statuteCode         String - An identifier of a set of laws for a particular jurisdiction
     * @param enactmentDateFrom   Date - A date a statute was enacted and came into effect.
     * @param enactmentDateTo     Date - A date a statute was enacted and came into effect.
     * @param repealDateFrom      Date - A date a statute was repealed and no longer applies.
     * @param repealDateTo        Date - A date a statute was repealed and no longer applies.
     * @param section             String - An identifier of a section or category within a code book.
     * @param description         String - A description of a statute.
     * @param jurisdictionSearch  JurisdictionConfigSearchType - Details about an area in which a statute applies, reference to the Jurisdiction Configuration Search Type.
     * @param defaultJurisdiction Boolean - True if the charge codes default to this Statute, false otherwise.
     * @param outOfJurisdiction   Boolean - True if this statute is outside the jurisdiction, false otherwise.
     */
    public StatuteConfigSearchType(String statuteCode, Date enactmentDateFrom, Date enactmentDateTo, Date repealDateFrom, Date repealDateTo, String section,
            String description, JurisdictionConfigSearchType jurisdictionSearch, Boolean defaultJurisdiction, Boolean outOfJurisdiction) {
        this.statuteCode = statuteCode;
        this.enactmentDateFrom = enactmentDateFrom;
        this.enactmentDateTo = enactmentDateTo;
        this.repealDateFrom = repealDateFrom;
        this.repealDateTo = repealDateTo;
        this.section = section;
        this.description = description;
        this.jurisdictionSearch = jurisdictionSearch;
        this.defaultJurisdiction = defaultJurisdiction;
        this.outOfJurisdiction = outOfJurisdiction;
    }

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set, false otherwise.
     */
    @Deprecated
    public Boolean isWellFormed() {

        if (ValidationUtil.isEmpty(statuteCode) && ValidationUtil.isEmpty(enactmentDateFrom) && ValidationUtil.isEmpty(enactmentDateTo) && ValidationUtil.isEmpty(
                repealDateFrom) && ValidationUtil.isEmpty(repealDateTo) && ValidationUtil.isEmpty(section) && ValidationUtil.isEmpty(description)
                && ValidationUtil.isEmpty(defaultJurisdiction) && ValidationUtil.isEmpty(outOfJurisdiction) && (jurisdictionSearch == null
                || !jurisdictionSearch.isWellFormed())) {
			return false;
		}

        return true;
    }

    /**
     * Verify if properties are set properly.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set and all values are valid, false otherwise.
     */
    @Deprecated
    public Boolean isValid() {

        if (!isWellFormed()) {
			return false;
		}

        if (jurisdictionSearch != null && !jurisdictionSearch.isValid()) {
			return false;
		}

        //toDate cannot before the fromDate
        if (!ValidationUtil.isValid(enactmentDateFrom, enactmentDateTo)) {
			return false;
		}
        if (!ValidationUtil.isValid(repealDateFrom, repealDateTo)) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the statuteCode property.
     * It is a reference code value of StatuteCode set.
     *
     * @return String - An identifier of a set of laws for a particular jurisdiction
     */
    public String getStatuteCode() {
        return statuteCode;
    }

    /**
     * Sets the value of the statuteCode property.
     * It is a reference code value of StatuteCode set.
     *
     * @param statuteCode String - An identifier of a set of laws for a particular jurisdiction
     */
    public void setStatuteCode(String statuteCode) {
        this.statuteCode = statuteCode;
    }

    /**
     * Gets the value of the enactmentDateFrom property.
     *
     * @return Date - A date a statute was enacted and came into effect.
     */
    public Date getEnactmentDateFrom() {
        return enactmentDateFrom;
    }

    /**
     * Sets the value of the enactmentDateFrom property.
     *
     * @param enactmentDateFrom Date - A date a statute was enacted and came into effect.
     */
    public void setEnactmentDateFrom(Date enactmentDateFrom) {
        this.enactmentDateFrom = enactmentDateFrom;
    }

    /**
     * Gets the value of the enactmentDateTo property.
     *
     * @return Date - A date a statute was enacted and came into effect.
     */
    public Date getEnactmentDateTo() {
        return enactmentDateTo;
    }

    /**
     * Sets the value of the enactmentDateTo property.
     *
     * @param enactmentDateTo Date - A date a statute was enacted and came into effect.
     */
    public void setEnactmentDateTo(Date enactmentDateTo) {
        this.enactmentDateTo = enactmentDateTo;
    }

    /**
     * Gets the value of the repealDateFrom property.
     *
     * @return Date - A date a statute was repealed and no longer applies.
     */
    public Date getRepealDateFrom() {
        return repealDateFrom;
    }

    /**
     * Sets the value of the repealDateFrom property.
     *
     * @param repealDateFrom Date - A date a statute was repealed and no longer applies.
     */
    public void setRepealDateFrom(Date repealDateFrom) {
        this.repealDateFrom = repealDateFrom;
    }

    /**
     * Gets the value of the repealDateTo property.
     *
     * @return Date - A date a statute was repealed and no longer applies.
     */
    public Date getRepealDateTo() {
        return repealDateTo;
    }

    /**
     * Sets the value of the repealDateTo property.
     *
     * @param repealDateTo Date - A date a statute was repealed and no longer applies.
     */
    public void setRepealDateTo(Date repealDateTo) {
        this.repealDateTo = repealDateTo;
    }

    /**
     * Gets the value of the section property.
     *
     * @return String - An identifier of a section or category within a code book
     */
    public String getSection() {
        return section;
    }

    /**
     * Sets the value of the section property.
     *
     * @param section String - An identifier of a section or category within a code book
     */
    public void setSection(String section) {
        this.section = section;
    }

    /**
     * Gets the value of the description property.
     *
     * @return String - A description of a statute.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param description String - A description of a statute.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the value of the active property.
     *
     * @return Boolean - Indicate the statute is still active or not. (Derived value from repealDate)
     */
    public Boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     *
     * @param active Boolean - Indicate the statute is still active or not. (Derived value, will ignore if set)
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * Gets the value of the jurisdictionSearch property.
     *
     * @return JurisdictionConfigSearchType - the jurisdiction configure search object
     */
    public JurisdictionConfigSearchType getJurisdictionSearch() {
        return jurisdictionSearch;
    }

    /**
     * Sets the value of the jurisdictionSearch property.
     *
     * @param jurisdictionSearch JurisdictionConfigSearchType - the jurisdiction configure search object
     */
    public void setJurisdictionSearch(JurisdictionConfigSearchType jurisdictionSearch) {
        this.jurisdictionSearch = jurisdictionSearch;
    }

    /**
     * Gets the value of the defaultJurisdiction property.
     *
     * @return Boolean - True if the charge codes default to this Statute, false otherwise.
     */
    public Boolean isDefaultJurisdiction() {
        return defaultJurisdiction;
    }

    /**
     * Sets the value of the defaultJurisdiction property.
     *
     * @param defaultJurisdiction Boolean - True if the charge codes default to this Statute, false otherwise.
     */
    public void setDefaultJurisdiction(Boolean defaultJurisdiction) {
        this.defaultJurisdiction = defaultJurisdiction;
    }

    /**
     * Gets the value of the outOfJurisdiction property.
     *
     * @return Boolean - True if this statute is outside the jurisdiction, false otherwise.
     */
    public Boolean isOutOfJurisdiction() {
        return outOfJurisdiction;
    }

    /**
     * Gets the value of the outOfJurisdiction property.
     *
     * @param outOfJurisdiction Boolean - True if this statute is outside the jurisdiction, false otherwise.
     */
    public void setOutOfJurisdiction(Boolean outOfJurisdiction) {
        this.outOfJurisdiction = outOfJurisdiction;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StatuteConfigSearchType [statuteCode=" + statuteCode + ", enactmentDateFrom=" + enactmentDateFrom + ", enactmentDateTo=" + enactmentDateTo
                + ", repealDateFrom=" + repealDateFrom + ", repealDateTo=" + repealDateTo + ", section=" + section + ", description=" + description + ", active=" + active
                + ", jurisdictionSearch=" + jurisdictionSearch + ", defaultJurisdiction=" + defaultJurisdiction + ", outOfJurisdiction=" + outOfJurisdiction + "]";
    }

}
