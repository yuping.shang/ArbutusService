package syscon.arbutus.product.services.legal.realization.persistence.caseinformation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.CaseActivityEntity;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;

/**
 * The representation of the case information entity.
 *
 * @author lhan
 * @DbComment LEG_CICaseInfo 'The case information table'
 */
@Audited
@Entity
@Table(name = "LEG_CICaseInfo")
@SQLDelete(sql = "UPDATE LEG_CICaseInfo SET flag = 4 WHERE caseInfoId = ? and version = ?")
@Where(clause = "flag = 1")
public class CaseInfoEntity implements Serializable {

    private static final long serialVersionUID = -5203863641345830448L;

    /**
     * @DbComment CaseInfoId 'Unique Id of a case information instance'
     */
    @Id
    @Column(name = "CaseInfoId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CICaseInfo_Id")
    @SequenceGenerator(name = "SEQ_LEG_CICaseInfo_Id", sequenceName = "SEQ_LEG_CICaseInfo_Id", allocationSize = 1)
    private Long caseInfoId;

    /**
     * @DbComment CaseCategory 'The category of the case, e.g. InJurisdiction case, Out of Jurisdiction case.'
     */
    @MetaCode(set = MetaSet.CASE_CATEGORY)
    @Column(name = "CaseCategory", nullable = false, length = 64)
    private String caseCategory;

    /**
     * @DbComment CaseType 'Indicates the type of case eg. immigration, traffic, criminal.'
     */
    @MetaCode(set = MetaSet.CASE_TYPE)
    @Column(name = "CaseType", nullable = false, length = 64)
    private String caseType;

    /**
     * @DbComment CreatedDate 'The date when the case was first created.'
     */
    @Column(name = "CreatedDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    /**
     * @DbComment IssuedDate 'The date on which the case was issued (by the court).'
     */
    @Column(name = "IssuedDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date issuedDate;

    /**
     * @DbComment FacilityId 'Used to indicate the originating facility name, type and location for a case. Static reference to the Facility service. '
     */
    @Column(name = "FacilityId", nullable = true)
    private Long facilityId;

    /**
     * @DbComment SupervisionId 'Reference to a Supervision period. Static reference to the Supervision service.'
     */
    @Column(name = "SupervisionId", nullable = false)
    private Long supervisionId;

    /**
     * @DbComment Diverted 'Indicates whether the case was attempted to be diverted'
     */
    @Column(name = "Diverted", nullable = true)
    private Boolean diverted;

    /**
     * @DbComment DivertedSuccess 'Indicates whether a case diversion (if attempted) was successful or unsuccessful. Only relevant if isDivertedFlag is true'
     */
    @Column(name = "DivertedSuccess", nullable = true)
    private Boolean divertedSuccess;

    /**
     * @DbComment Active 'Indicates whether the case is active or inactive. This is specifically set by the user, not derived.'
     */
    @Column(name = "Active", nullable = false)
    private Boolean active;

    /**
     * @DbComment CaseStatusReason 'Indicates the reason a case status was changed.'
     */
    @MetaCode(set = MetaSet.CASE_STATUS_CHANGE_REASON)
    @Column(name = "CaseStatusReason", nullable = true, length = 64)
    private String caseStatusReason;

    /**
     * @DbComment SentenceStatus 'Indicates whether the offender was sentenced to be incarcerated.'
     */
    @MetaCode(set = MetaSet.SENTENCE_STATUS)
    @Column(name = "SentenceStatus", nullable = true, length = 64)
    private String sentenceStatus;

    /**
     * @DbComment CommentUserId 'User ID who created/updated the comments'
     * @DbComment CommentDate 'The Date/Time when the comment was created'
     * @DbComment CommentText 'The comment text'getFacilityId
     */
    @Embedded
    private CommentEntity comment;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    @OneToMany(mappedBy = "caseInfo", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<CaseIdentifierEntity> caseIdentifiers;

    @OneToMany(mappedBy = "caseInfo", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<CaseInfoModuleAssociationEntity> moduleAssociations;

    @NotAudited
    @OneToMany(mappedBy = "caseInfo", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @BatchSize(size = 20)
    private Set<CaseInfoHistEntity> history;

    @ManyToMany(mappedBy = "caseInfos", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private Set<CaseActivityEntity> caseActivities = new HashSet<CaseActivityEntity>();

    @NotAudited
    @OneToMany(mappedBy = "caseInfo", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @OrderBy("caseStatusChangeHistId DESC")
    private List<CaseStatusChangeHistEntity> statusChangeHistories;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    @Transient
    private boolean saveAsSecondaryIdentifier;

    /**
     * Default constructor
     */
    public CaseInfoEntity() {
    }

    /**
     * Constructor
     *
     * @param caseInfoId
     * @param caseCategory
     * @param caseType
     * @param createdDate
     * @param issuedDate
     * @param facilityId
     * @param diverted
     * @param divertedSuccess
     * @param active
     * @param caseStatusReason
     * @param sentenceStatus
     * @param comment
     * @param caseIdentifiers
     * @param associations
     */
    public CaseInfoEntity(Long caseInfoId, String caseCategory, String caseType, Date createdDate, Date issuedDate, Long facilityId, Long supervisionId, Boolean diverted,
            Boolean divertedSuccess, Boolean active, String caseStatusReason, String sentenceStatus, CommentEntity comment, boolean saveAsSecondaryIdentifier) {
        this.caseInfoId = caseInfoId;
        this.caseCategory = caseCategory;
        this.caseType = caseType;
        this.createdDate = BeanHelper.createDateWithoutTime(createdDate); //trim time portion
        this.issuedDate = BeanHelper.createDateWithoutTime(issuedDate);
        this.facilityId = facilityId;
        this.supervisionId = supervisionId;
        this.diverted = diverted;
        this.divertedSuccess = divertedSuccess;
        this.active = active;
        this.caseStatusReason = caseStatusReason;
        this.sentenceStatus = sentenceStatus;
        this.comment = comment;
        this.saveAsSecondaryIdentifier = saveAsSecondaryIdentifier;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /**
     * @return the caseActivities
     */
    public Set<CaseActivityEntity> getCaseActivities() {
        return caseActivities;
    }

    /**
     * @param caseActivities the caseActivities to set
     */
    public void setCaseActivities(Set<CaseActivityEntity> caseActivities) {
        this.caseActivities = caseActivities;
    }

    /**
     * @return the caseInfoId
     */
    public Long getCaseInfoId() {
        return caseInfoId;
    }

    /**
     * @param caseInfoId the caseInfoId to set
     */
    public void setCaseInfoId(Long caseInfoId) {
        this.caseInfoId = caseInfoId;
    }

    /**
     * @return the caseCategory
     */
    public String getCaseCategory() {
        return caseCategory;
    }

    /**
     * @param caseCategory the caseCategory to set
     */
    public void setCaseCategory(String caseCategory) {
        this.caseCategory = caseCategory;
    }

    /**
     * @return the caseType
     */
    public String getCaseType() {
        return caseType;
    }

    /**
     * @param caseType the caseType to set
     */
    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    /**
     * @return the createdDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = BeanHelper.createDateWithoutTime(createdDate);
    }

    /**
     * @return the issuedDate
     */
    public Date getIssuedDate() {
        return issuedDate;
    }

    /**
     * @param issuedDate the issuedDate to set
     */
    public void setIssuedDate(Date issuedDate) {
        this.issuedDate = BeanHelper.createDateWithoutTime(issuedDate);
    }

    /**
     * @return the facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * @param facilityId the facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * @return the diverted
     */
    public Boolean isDiverted() {
        return diverted;
    }

    /**
     * @param diverted the diverted to set
     */
    public void setDiverted(Boolean diverted) {
        this.diverted = diverted;
    }

    /**
     * @return the divertedSuccessgetSerialversionuid
     */
    public Boolean isDivertedSuccess() {
        return divertedSuccess;
    }

    /**
     * @param divertedSuccess the divertedSuccess to set
     */
    public void setDivertedSuccess(Boolean divertedSuccess) {
        this.divertedSuccess = divertedSuccess;
    }

    /**
     * @return the active
     */
    public Boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the caseStatusReason
     */
    public String getCaseStatusReason() {
        return caseStatusReason;
    }

    /**
     * @param caseStatusReason the caseStatusReason to set
     */
    public void setCaseStatusReason(String caseStatusReason) {
        this.caseStatusReason = caseStatusReason;
    }

    /**
     * @return the sentenceStatus
     */
    public String getSentenceStatus() {
        return sentenceStatus;
    }

    /**
     * @param sentenceStatus the sentenceStatus to set
     */
    public void setSentenceStatus(String sentenceStatus) {
        this.sentenceStatus = sentenceStatus;
    }

    /**
     * @return the comment
     */
    public CommentEntity getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(CommentEntity comment) {
        this.comment = comment;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the caseIdentifiers
     */
    public Set<CaseIdentifierEntity> getCaseIdentifiers() {
        if (caseIdentifiers == null) {
            caseIdentifiers = new HashSet<CaseIdentifierEntity>();
        }
        return caseIdentifiers;
    }

    /**
     * @param caseIdentifiers the caseIdentifiers to set
     */
    public void setCaseIdentifiers(Set<CaseIdentifierEntity> caseIdentifiers) {
        this.caseIdentifiers = caseIdentifiers;
    }

    /**
     * @param caseIdentifierEntity
     */
    public void addCaseIdentifier(CaseIdentifierEntity caseIdentifierEntity) {
        caseIdentifierEntity.setCaseInfo(this);
        getCaseIdentifiers().add(caseIdentifierEntity);
    }

    /**
     * @param associationEntity
     */
    public void addModuleAssociation(CaseInfoModuleAssociationEntity associationEntity) {
        associationEntity.setCaseInfo(this);
        getModuleAssociations().add(associationEntity);
    }

    /**
     * @return the moduleAssociations
     */
    public Set<CaseInfoModuleAssociationEntity> getModuleAssociations() {
        if (moduleAssociations == null) {
			moduleAssociations = new HashSet<CaseInfoModuleAssociationEntity>();
		}
        return moduleAssociations;
    }

    /**
     * @param moduleAssociations the moduleAssociations to set
     */
    public void setModuleAssociations(Set<CaseInfoModuleAssociationEntity> moduleAssociations) {
        this.moduleAssociations = moduleAssociations;
    }

    /**
     * @return the history
     */
    public Set<CaseInfoHistEntity> getHistory() {
        if (history == null) {
            history = new HashSet<CaseInfoHistEntity>();
        }
        return history;
    }

    /**
     * @param history the history to set
     */
    public void setHistory(Set<CaseInfoHistEntity> history) {
        this.history = history;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((active == null) ? 0 : active.hashCode());
        result = prime * result + ((caseActivities == null) ? 0 : caseActivities.hashCode());
        result = prime * result + ((caseCategory == null) ? 0 : caseCategory.hashCode());
        result = prime * result + ((caseIdentifiers == null) ? 0 : caseIdentifiers.hashCode());
        result = prime * result + ((caseInfoId == null) ? 0 : caseInfoId.hashCode());
        result = prime * result + ((caseStatusReason == null) ? 0 : caseStatusReason.hashCode());
        result = prime * result + ((caseType == null) ? 0 : caseType.hashCode());
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
        result = prime * result + ((diverted == null) ? 0 : diverted.hashCode());
        result = prime * result + ((divertedSuccess == null) ? 0 : divertedSuccess.hashCode());
        result = prime * result + ((facilityId == null) ? 0 : facilityId.hashCode());
        result = prime * result + ((history == null) ? 0 : history.hashCode());
        result = prime * result + ((issuedDate == null) ? 0 : issuedDate.hashCode());
        result = prime * result + ((moduleAssociations == null) ? 0 : moduleAssociations.hashCode());
        result = prime * result + (saveAsSecondaryIdentifier ? 1231 : 1237);
        result = prime * result + ((sentenceStatus == null) ? 0 : sentenceStatus.hashCode());
        result = prime * result + ((stamp == null) ? 0 : stamp.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        CaseInfoEntity other = (CaseInfoEntity) obj;
        if (active == null) {
            if (other.active != null) {
				return false;
			}
        } else if (!active.equals(other.active)) {
			return false;
		}
        if (caseActivities == null) {
            if (other.caseActivities != null) {
				return false;
			}
        } else if (!caseActivities.equals(other.caseActivities)) {
			return false;
		}
        if (caseCategory == null) {
            if (other.caseCategory != null) {
				return false;
			}
        } else if (!caseCategory.equals(other.caseCategory)) {
			return false;
		}
        if (caseIdentifiers == null) {
            if (other.caseIdentifiers != null) {
				return false;
			}
        } else if (!caseIdentifiers.equals(other.caseIdentifiers)) {
			return false;
		}
        if (caseInfoId == null) {
            if (other.caseInfoId != null) {
				return false;
			}
        } else if (!caseInfoId.equals(other.caseInfoId)) {
			return false;
		}
        if (caseStatusReason == null) {
            if (other.caseStatusReason != null) {
				return false;
			}
        } else if (!caseStatusReason.equals(other.caseStatusReason)) {
			return false;
		}
        if (caseType == null) {
            if (other.caseType != null) {
				return false;
			}
        } else if (!caseType.equals(other.caseType)) {
			return false;
		}
        if (comment == null) {
            if (other.comment != null) {
				return false;
			}
        } else if (!comment.equals(other.comment)) {
			return false;
		}
        if (createdDate == null) {
            if (other.createdDate != null) {
				return false;
			}
        } else if (!createdDate.equals(other.createdDate)) {
			return false;
		}
        if (diverted == null) {
            if (other.diverted != null) {
				return false;
			}
        } else if (!diverted.equals(other.diverted)) {
			return false;
		}
        if (divertedSuccess == null) {
            if (other.divertedSuccess != null) {
				return false;
			}
        } else if (!divertedSuccess.equals(other.divertedSuccess)) {
			return false;
		}
        if (facilityId == null) {
            if (other.facilityId != null) {
				return false;
			}
        } else if (!facilityId.equals(other.facilityId)) {
			return false;
		}
        if (history == null) {
            if (other.history != null) {
				return false;
			}
        } else if (!history.equals(other.history)) {
			return false;
		}
        if (issuedDate == null) {
            if (other.issuedDate != null) {
				return false;
			}
        } else if (!issuedDate.equals(other.issuedDate)) {
			return false;
		}
        if (moduleAssociations == null) {
            if (other.moduleAssociations != null) {
				return false;
			}
        } else if (!moduleAssociations.equals(other.moduleAssociations)) {
			return false;
		}
        if (saveAsSecondaryIdentifier != other.saveAsSecondaryIdentifier) {
			return false;
		}
        if (sentenceStatus == null) {
            if (other.sentenceStatus != null) {
				return false;
			}
        } else if (!sentenceStatus.equals(other.sentenceStatus)) {
			return false;
		}
        if (stamp == null) {
            if (other.stamp != null) {
				return false;
			}
        } else if (!stamp.equals(other.stamp)) {
			return false;
		}
        if (supervisionId == null) {
            if (other.supervisionId != null) {
				return false;
			}
        } else if (!supervisionId.equals(other.supervisionId)) {
			return false;
		}
        if (version == null) {
            if (other.version != null) {
				return false;
			}
        } else if (!version.equals(other.version)) {
			return false;
		}
        return true;
    }

    /**
     * @return the saveAsSecondaryIdentifier
     */
    public boolean isSaveAsSecondaryIdentifier() {
        return saveAsSecondaryIdentifier;
    }

    /**
     * When update primary case identifier, if this value is true, then service saves the old primary case identifier to secondary identifier list.
     *
     * @param saveAsSecondaryIdentifier the saveAsSecondaryIdentifier to set
     */
    public void setSaveAsSecondaryIdentifier(boolean saveAsSecondaryIdentifier) {
        this.saveAsSecondaryIdentifier = saveAsSecondaryIdentifier;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseInfoEntity [caseInfoId=" + caseInfoId + ", caseCategory=" + caseCategory + ", caseType=" + caseType + ", createdDate=" + createdDate + ", issuedDate="
                + issuedDate + ", facilityId=" + facilityId + ", supervisionId=" + supervisionId + ", diverted=" + diverted + ", divertedSuccess=" + divertedSuccess
                + ", active=" + active + ", caseStatusReason=" + caseStatusReason + ", sentenceStatus=" + sentenceStatus + ", comment=" + comment + ", stamp=" + stamp
                + ", version=" + version + ", caseIdentifiers=" + caseIdentifiers + ", moduleAssociations=" + moduleAssociations + ", history=" + history
                + ", caseActivities=" + caseActivities + ", saveAsSecondaryIdentifier=" + saveAsSecondaryIdentifier + "]";
    }
}
