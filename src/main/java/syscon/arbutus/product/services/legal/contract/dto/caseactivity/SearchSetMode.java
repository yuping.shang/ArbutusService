package syscon.arbutus.product.services.legal.contract.dto.caseactivity;

/**
 * <p>Java class for SearchSetModeType.
 * This service will support ONLY, ANY, ALL search for Set/Collection.
 */
public enum SearchSetMode {

    /**
     * SET in search criterion will match sets that only have an exact set match.
     */
    ONLY,
    /**
     * SET in search criterion will match sets that have at least one of the items.
     */
    ANY,

    /**
     * SET in search criterion will match sets that have at least the specified subset.
     */
    ALL;

    /**
     * Create search mode from string value.
     *
     * @param v
     * @return SearchSetModeType
     */
    public static SearchSetMode fromValue(String v) {
        return valueOf(v);
    }

    /**
     * Test if a string value is a valid SearchSetMode value.
     *
     * @param v
     */
    public static Boolean contain(String v) {

        for (SearchSetMode c : SearchSetMode.values()) {
            if (c.value().equals(v)) {
                return true;
            }
        }

        return false;
    }

    /**
     * String value of the search set mode.
     *
     * @return String
     */
    public String value() {
        return name();
    }
}
