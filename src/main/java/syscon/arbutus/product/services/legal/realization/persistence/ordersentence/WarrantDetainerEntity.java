package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;

/**
 * WarrantDetainerEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdOrder 'Order table for Order Sentence module of Legal service'
 * @since December 21, 2012
 */
@Audited
@Entity
@DiscriminatorValue("Warrant")
public class WarrantDetainerEntity extends OrderEntity implements Serializable {

    private static final long serialVersionUID = -737931375114522731L;

    @ForeignKey(name = "Fk_LEG_OrdWrnt1")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @BatchSize(size = 20)
    @JoinColumn(name = "agnBeNoti")
    @AuditJoinTable(name = "Leg_Warra_Notifi_AUD", inverseJoinColumns = { @JoinColumn(name = "notificationId") })
    private Set<NotificationEntity> agencyToBeNotified;

    @ForeignKey(name = "Fk_LEG_OrdWrnt2")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @BatchSize(size = 20)
    @JoinColumn(name = "notiLog")
    @AuditJoinTable(name = "Leg_Warra_NotifiLog_AUD", inverseJoinColumns = { @JoinColumn(name = "notiLgId") })

    private Set<NotificationLogEntity> notificationLog;

    /**
     * Constructor
     */
    public WarrantDetainerEntity() {
        super();
    }

    /**
     * @return the agencyToBeNotified
     */
    public Set<NotificationEntity> getAgencyToBeNotified() {
        if (agencyToBeNotified == null) {
			agencyToBeNotified = new HashSet<NotificationEntity>();
		}
        return agencyToBeNotified;
    }

    /**
     * @param agencyToBeNotified the agencyToBeNotified to set
     */
    public void setAgencyToBeNotified(Set<NotificationEntity> agencyToBeNotified) {
        if (agencyToBeNotified == null) {
			agencyToBeNotified = new HashSet<NotificationEntity>();
		}
        this.agencyToBeNotified = agencyToBeNotified;
    }

    /**
     * @return the notificationLog
     */
    public Set<NotificationLogEntity> getNotificationLog() {
        if (notificationLog == null) {
			notificationLog = new HashSet<NotificationLogEntity>();
		}
        return notificationLog;
    }

    /**
     * @param notificationLog the notificationLog to set
     */
    public void setNotificationLog(Set<NotificationLogEntity> notificationLog) {
        if (notificationLog == null) {
			notificationLog = new HashSet<NotificationLogEntity>();
		}
        this.notificationLog = notificationLog;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((agencyToBeNotified == null) ? 0 : agencyToBeNotified.hashCode());
        result = prime * result + ((notificationLog == null) ? 0 : notificationLog.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        WarrantDetainerEntity other = (WarrantDetainerEntity) obj;
        if (agencyToBeNotified == null) {
            if (other.agencyToBeNotified != null) {
				return false;
			}
        } else if (!agencyToBeNotified.equals(other.agencyToBeNotified)) {
			return false;
		}
        if (notificationLog == null) {
            if (other.notificationLog != null) {
				return false;
			}
        } else if (!notificationLog.equals(other.notificationLog)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "WarrantDetainerEntity [agencyToBeNotified=" + agencyToBeNotified + ", notificationLog=" + notificationLog + ", toString()=" + super.toString() + "]";
    }

}
