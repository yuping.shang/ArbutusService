package syscon.arbutus.product.services.legal.realization.persistence;

import javax.persistence.*;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of SentenceTermNameEntity
 *
 * @author Ashish
 * @version 1.0
 * @DbComment LEG_SENTENCETERMNAME
 * @since December 10, 2013
 */
@Entity
@Table(name = "LEG_SENTENCETERMNAME")
@SQLDelete(sql = "UPDATE LEG_SENTENCETERMNAME SET flag = 4 WHERE sentenceTermNameId = ? and version = ?")
@Where(clause = "flag = 1")

public class SentenceTermNameEntity {

    private static final long serialVersionUID = 1L;
    /**
     * @DbComment Id 'Unique identification for SentenceTermNameEntity
     * instance'
     */
    @Id
    @Column(name = "SENTENCETERMNAMEID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_SENTENCETERMID_Id")
    @SequenceGenerator(name = "SEQ_LEG_SENTENCETERMID_Id", sequenceName = "SEQ_LEG_SENTENCETERMID_Id", allocationSize = 1)
    private Long sentenceTermNameId;

    /**
     * @DbComment sentenceType 'reference code of termName of SentenceTermEntity'
     */
    @Column(name = "TERMNAME", nullable = false)
    @MetaCode(set = MetaSet.TERM_NAME)
    private String termName;

    @ForeignKey(name = "FK_LEG_SENTENCETERMTYPE")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SENTENCETERM_TERMID", referencedColumnName = "SENTENCETERMID", nullable = true)
    private SentenceTermEntity sentenceTermEntity;

    /**
     * @DbComment CREATEUSERID 'User ID who created the object'
     * @DbComment CREATEDATETIME 'Date and time when the object was created'
     * @DbComment MODIFYUSERID 'User ID who last updated the object'
     * @DbComment MODIFYDATETIME 'Date and time when the object was last updated'
     * @DbComment INVOCATIONCONTEXT 'Invocation context when the create/update action called'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'the version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * Default Constructor
     */
    public SentenceTermNameEntity() {
        super();

    }

    /**
     * Get id of SentenceTermNameEntity Object
     *
     * @return the sentenceTermNameId
     */

    public Long getSentenceTermNameId() {
        return sentenceTermNameId;
    }

    /**
     * set id of SentenceTermNameEntity Object
     *
     * @param the sentenceTermNameId
     */
    public void setSentenceTermNameId(Long sentenceTermNameId) {
        this.sentenceTermNameId = sentenceTermNameId;
    }

    /**
     * Get Term Name
     * -- The name of the term ( Maximum, Minimum etc.).
     *
     * @return the termName
     */
    public String getTermName() {
        return termName;
    }

    /**
     * Set Term Name
     * -- -- The name of the term ( Maximum, Minimum etc.).
     *
     * @param termName the termName to set
     */
    public void setTermName(String termName) {
        this.termName = termName;
    }

    /**
     * Get sentenceTermEntity (Parent Object)
     *
     * @return the sentenceTermEntity
     */
    public SentenceTermEntity getSentenceTermEntity() {
        return sentenceTermEntity;
    }

    /**
     * Set sentenceTermEntity (Parent Object)
     *
     * @param the sentenceTermEntity
     */
    public void setSentenceTermEntity(SentenceTermEntity sentenceTermEntity) {
        this.sentenceTermEntity = sentenceTermEntity;
    }

    /**
     * @return stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sentenceTermNameId == null) ? 0 : sentenceTermNameId.hashCode());
        result = prime * result + ((termName == null) ? 0 : termName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SentenceTermNameEntity other = (SentenceTermNameEntity) obj;
        if (sentenceTermNameId == null) {
            if (other.sentenceTermNameId != null) {
				return false;
			}
        } else if (!sentenceTermNameId.equals(other.sentenceTermNameId)) {
			return false;
		}
        if (termName == null) {
            if (other.termName != null) {
				return false;
			}
        } else if (!termName.equals(other.termName)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "SentenceTermNameEntity [sentenceTermNameId=" + sentenceTermNameId + ", termName=" + termName + ", sentenceTermEntity=" + sentenceTermEntity + "]";
    }

}
