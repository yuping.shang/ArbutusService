package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * WarrantDetainerType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 21, 2012
 */
public class WarrantDetainerType extends OrderType {

    private static final long serialVersionUID = -737931375114522731L;
    @Valid
    private Set<NotificationType> agencyToBeNotified;
    @Valid
    private Set<NotificationLogType> notificationLog;

    /**
     * Constructor
     */
    public WarrantDetainerType() {
        super();
    }

    /**
     * Constructor
     *
     * @param orderIdentification                    Long. Not Required for create; Required otherwise. The unique ID for each order created in the system. This is a system-generated ID.
     * @param orderClassification                    String, Required. The order classification. This defines the type of instance of the service.
     * @param orderType                              String, Required. The client defined order type that maps to the order classification.
     * @param orderSubType                           String, Optional. The sub type of an order. For example a want, warrant or detainer.
     * @param orderCategory                          String, Required. The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.
     * @param orderNumber                            String, Optional. User specified order number.
     * @param orderDisposition                       DispositionType, Required. The disposition related to the order.
     * @param comments                               CommentType, Optional. The comments related to the order.
     * @param orderIssuanceDate                      Date, Optional. The date the order was issued.
     * @param orderReceivedDate                      Date, Optional. The date the order was received.
     * @param orderStartDate                         Date, Required. The date/time the order starts(becomes valid).
     * @param orderExpirationDate                    Date, Optional. The date the order expires.
     * @param caseActivityInitiatedOrderAssociations Set&lt;Long>, Optional. The case related activities that initiated this order. Static association to the Case Activity service.
     * @param orderInitiatedCaseActivityAssociations Set&lt;Long>, Optional. The case activities that were initiated by this order. Static association to the Case Activity service.
     * @param isHoldingOrder                         Boolean, Ignored. If true then the order is a holding document, false otherwise.
     * @param isSchedulingNeeded                     Boolean, Ignored. If true, scheduling is needed for the order, false otherwise.
     * @param hasCharges                             Boolean, Ignored. If true, the order has charges related, false otherwise.
     * @param issuingAgency                          NotificationType, Optional. The source agency that issues the order, this could be a facility or an organization.
     * @param agencyToBeNotified                     Set&lt;NotificationType>, Optional. The agencies to be notified while releasing the offender, this could be a facility or an organization. Note: this is for agencies that are not issuing the order.
     * @param notificationLog                        Set&lt;NotificationLogType>, Optional. Inquiries made about an inmate’s legal status.
     */
    public WarrantDetainerType(Long orderIdentification, @NotNull String orderClassification, @NotNull String orderType, String orderSubType,
            @NotNull String orderCategory, @Size(max = 64, message = "max length 64") String orderNumber, @NotNull DispositionType orderDisposition, CommentType comments,
            Date orderIssuanceDate, Date orderReceivedDate, @NotNull Date orderStartDate, Date orderExpirationDate, Set<Long> caseActivityInitiatedOrderAssociations,
            Set<Long> orderInitiatedCaseActivityAssociations, Boolean isHoldingOrder, Boolean isSchedulingNeeded, Boolean hasCharges, NotificationType issuingAgency,
            Set<NotificationType> agencyToBeNotified, Set<NotificationLogType> notificationLog,
            @NotNull @Size(min = 1, message = "Cardinality [1..*]") Set<Long> caseInfoIds, Set<Long> chargeIds, Set<Long> conditionIds) {
        super(orderIdentification, orderClassification, orderType, orderSubType, orderCategory, orderNumber, orderDisposition, comments, orderIssuanceDate,
                orderReceivedDate, orderStartDate, orderExpirationDate, caseActivityInitiatedOrderAssociations, orderInitiatedCaseActivityAssociations, isHoldingOrder,
                isSchedulingNeeded, hasCharges, issuingAgency, caseInfoIds, chargeIds, conditionIds);
        this.agencyToBeNotified = agencyToBeNotified;
        this.notificationLog = notificationLog;
    }

    /**
     * Get Agency To BeNotified
     * -- The agencies to be notified while releasing the offender, this could be a facility or an organization. Note: this is for agencies that are not issuing the order.
     *
     * @return the agencyToBeNotified
     */
    public Set<NotificationType> getAgencyToBeNotified() {
        return agencyToBeNotified;
    }

    /**
     * Set Agency To BeNotified
     * -- The agencies to be notified while releasing the offender, this could be a facility or an organization. Note: this is for agencies that are not issuing the order.
     *
     * @param agencyToBeNotified the agencyToBeNotified to set
     */
    public void setAgencyToBeNotified(Set<NotificationType> agencyToBeNotified) {
        this.agencyToBeNotified = agencyToBeNotified;
    }

    /**
     * Get Notification Log
     * -- Inquiries made about an inmate’s legal status.
     *
     * @return the notificationLog
     */
    public Set<NotificationLogType> getNotificationLog() {
        return notificationLog;
    }

    /**
     * Set Notification Log
     * -- Inquiries made about an inmate’s legal status.
     *
     * @param notificationLog the notificationLog to set
     */
    public void setNotificationLog(Set<NotificationLogType> notificationLog) {
        this.notificationLog = notificationLog;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((agencyToBeNotified == null) ? 0 : agencyToBeNotified.hashCode());
        result = prime * result + ((notificationLog == null) ? 0 : notificationLog.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        WarrantDetainerType other = (WarrantDetainerType) obj;
        if (agencyToBeNotified == null) {
            if (other.agencyToBeNotified != null) {
				return false;
			}
        } else if (!agencyToBeNotified.equals(other.agencyToBeNotified)) {
			return false;
		}
        if (notificationLog == null) {
            if (other.notificationLog != null) {
				return false;
			}
        } else if (!notificationLog.equals(other.notificationLog)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "WarrantDetainerType [agencyToBeNotified=" + agencyToBeNotified + ", notificationLog=" + notificationLog + ", toString()=" + super.toString() + "]";
    }

}
