package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * TermHistoryEntity for Condition of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdTermHst 'Term History table for Order Sentence module of Legal service'
 * @since December 27, 2012
 */
//@Audited
@Entity
@Table(name = "LEG_OrdTermHst")
public class TermHistoryEntity implements Serializable {

    private static final long serialVersionUID = 5572983205725596191L;

    /**
     * @DbComment tHstId 'Unique identification of a term history instance'
     */
    @Id
    @Column(name = "tHstId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDTERMHST_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDTERMHST_ID", sequenceName = "SEQ_LEG_ORDTERMHST_ID", allocationSize = 1)
    private Long historyId;

    /**
     * @DbComment termId 'Unique identification of a term instance'
     */
    @Column(name = "termId")
    private Long termId;

    /**
     * @DbComment termCategory 'The category of term (Custodial, Community)'
     */
    @Column(name = "termCategory", nullable = false, length = 64)
    private String termCategory;

    /**
     * @DbComment termName 'The name of the term; Min, Max etc..'
     */
    @Column(name = "termName", nullable = false, length = 64)
    private String termName;

    /**
     * @DbComment termType 'The type of term (Duration, Intermittent, Suspended etc.). This will be used by the sentence calculation module.'
     */
    @Column(name = "termType", nullable = false, length = 64)
    private String termType;

    /**
     * @DbComment termStatus 'The status of a term, will be used by sentence calculation logic Pending/Included/Excluded. Only ‘Included’ terms will participate in sentence calculation.'
     */
    @Column(name = "termStatus", nullable = false, length = 64)
    private String termStatus;

    /**
     * @DbComment termSequenceNo 'The order of execution of the term.'
     */
    @Column(name = "termSequenceNo", nullable = true)
    private Long termSequenceNo;

    /**
     * @DbComment termStartDate 'The start date for the term.'
     */
    @Column(name = "termStartDate", nullable = true)
    private Date termStartDate;

    /**
     * @DbComment termEndDate 'The end date for the term.'
     */
    @Column(name = "termEndDate", nullable = true)
    private Date termEndDate;

    /**
     * @DbComment years 'No. of years a sentence is to be served. Used only for duration and intermittent type.'
     */
    @Column(name = "years", nullable = true)
    private Long years;

    /**
     * @DbComment months 'No. of months a sentence is to be served. Used only for duration and intermittent type.'
     */
    @Column(name = "months", nullable = true)
    private Long months;

    /**
     * @DbComment weeks 'No. of weeks a sentence is to be served. Used only for duration and intermittent type.'
     */
    @Column(name = "weeks", nullable = true)
    private Long weeks;

    /**
     * @DbComment days 'No. of days a sentence is to be served. Used only for duration and intermittent type.'
     */
    @Column(name = "days", nullable = true)
    private Long days;

    /**
     * @DbComment hours 'No. of hours a sentence is to be served. Used only for duration and intermittent type.'
     */
    @Column(name = "hours", nullable = true)
    private Long hours;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment hstId 'Unique identification of a sentence instance, foreign key to LEG_ORDSENTENCE History table'
     */
    @ForeignKey(name = "Fk_LEG_OrdTermHst")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hstId", nullable = false)
    private SentenceHistoryEntity sentence;

    /**
     * Constructor
     */
    public TermHistoryEntity() {
        super();
    }

    /**
     * Constructor
     *
     * @param historyId
     * @param termId
     * @param termCategory
     * @param termName
     * @param termType
     * @param termStatus
     * @param termSequenceNo
     * @param termStartDate
     * @param termEndDate
     * @param years
     * @param months
     * @param weeks
     * @param days
     * @param hours
     * @param stamp
     */
    public TermHistoryEntity(Long historyId, Long termId, String termCategory, String termName, String termType, String termStatus, Long termSequenceNo,
            Date termStartDate, Date termEndDate, Long years, Long months, Long weeks, Long days, Long hours, StampEntity stamp) {
        super();
        this.historyId = historyId;
        this.termId = termId;
        this.termCategory = termCategory;
        this.termName = termName;
        this.termType = termType;
        this.termStatus = termStatus;
        this.termSequenceNo = termSequenceNo;
        this.termStartDate = termStartDate;
        this.termEndDate = termEndDate;
        this.years = years;
        this.months = months;
        this.weeks = weeks;
        this.days = days;
        this.hours = hours;
        this.stamp = stamp;
    }

    /**
     * @return the historyId
     */
    public Long getHistoryId() {
        return historyId;
    }

    /**
     * @param historyId the historyId to set
     */
    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    /**
     * @return the termId
     */
    public Long getTermId() {
        return termId;
    }

    /**
     * @param termId the termId to set
     */
    public void setTermId(Long termId) {
        this.termId = termId;
    }

    /**
     * @return the termCategory
     */
    public String getTermCategory() {
        return termCategory;
    }

    /**
     * @param termCategory the termCategory to set
     */
    public void setTermCategory(String termCategory) {
        this.termCategory = termCategory;
    }

    /**
     * @return the termName
     */
    public String getTermName() {
        return termName;
    }

    /**
     * @param termName the termName to set
     */
    public void setTermName(String termName) {
        this.termName = termName;
    }

    /**
     * @return the termType
     */
    public String getTermType() {
        return termType;
    }

    /**
     * @param termType the termType to set
     */
    public void setTermType(String termType) {
        this.termType = termType;
    }

    /**
     * @return the termStatus
     */
    public String getTermStatus() {
        return termStatus;
    }

    /**
     * @param termStatus the termStatus to set
     */
    public void setTermStatus(String termStatus) {
        this.termStatus = termStatus;
    }

    /**
     * @return the termSequenceNo
     */
    public Long getTermSequenceNo() {
        return termSequenceNo;
    }

    /**
     * @param termSequenceNo the termSequenceNo to set
     */
    public void setTermSequenceNo(Long termSequenceNo) {
        this.termSequenceNo = termSequenceNo;
    }

    /**
     * @return the termStartDate
     */
    public Date getTermStartDate() {
        return termStartDate;
    }

    /**
     * @param termStartDate the termStartDate to set
     */
    public void setTermStartDate(Date termStartDate) {
        this.termStartDate = termStartDate;
    }

    /**
     * @return the termEndDate
     */
    public Date getTermEndDate() {
        return termEndDate;
    }

    /**
     * @param termEndDate the termEndDate to set
     */
    public void setTermEndDate(Date termEndDate) {
        this.termEndDate = termEndDate;
    }

    /**
     * @return the years
     */
    public Long getYears() {
        return years;
    }

    /**
     * @param years the years to set
     */
    public void setYears(Long years) {
        this.years = years;
    }

    /**
     * @return the months
     */
    public Long getMonths() {
        return months;
    }

    /**
     * @param months the months to set
     */
    public void setMonths(Long months) {
        this.months = months;
    }

    /**
     * @return the weeks
     */
    public Long getWeeks() {
        return weeks;
    }

    /**
     * @param weeks the weeks to set
     */
    public void setWeeks(Long weeks) {
        this.weeks = weeks;
    }

    /**
     * @return the days
     */
    public Long getDays() {
        return days;
    }

    /**
     * @param days the days to set
     */
    public void setDays(Long days) {
        this.days = days;
    }

    /**
     * @return the hours
     */
    public Long getHours() {
        return hours;
    }

    /**
     * @param hours the hours to set
     */
    public void setHours(Long hours) {
        this.hours = hours;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the sentence
     */
    public SentenceHistoryEntity getSentence() {
        return sentence;
    }

    /**
     * @param sentence the sentence to set
     */
    public void setSentence(SentenceHistoryEntity sentence) {
        this.sentence = sentence;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((days == null) ? 0 : days.hashCode());
        result = prime * result + ((historyId == null) ? 0 : historyId.hashCode());
        result = prime * result + ((hours == null) ? 0 : hours.hashCode());
        result = prime * result + ((months == null) ? 0 : months.hashCode());
        result = prime * result + ((termCategory == null) ? 0 : termCategory.hashCode());
        result = prime * result + ((termEndDate == null) ? 0 : termEndDate.hashCode());
        result = prime * result + ((termId == null) ? 0 : termId.hashCode());
        result = prime * result + ((termName == null) ? 0 : termName.hashCode());
        result = prime * result + ((termSequenceNo == null) ? 0 : termSequenceNo.hashCode());
        result = prime * result + ((termStartDate == null) ? 0 : termStartDate.hashCode());
        result = prime * result + ((termStatus == null) ? 0 : termStatus.hashCode());
        result = prime * result + ((termType == null) ? 0 : termType.hashCode());
        result = prime * result + ((weeks == null) ? 0 : weeks.hashCode());
        result = prime * result + ((years == null) ? 0 : years.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        TermHistoryEntity other = (TermHistoryEntity) obj;
        if (days == null) {
            if (other.days != null) {
				return false;
			}
        } else if (!days.equals(other.days)) {
			return false;
		}
        if (historyId == null) {
            if (other.historyId != null) {
				return false;
			}
        } else if (!historyId.equals(other.historyId)) {
			return false;
		}
        if (hours == null) {
            if (other.hours != null) {
				return false;
			}
        } else if (!hours.equals(other.hours)) {
			return false;
		}
        if (months == null) {
            if (other.months != null) {
				return false;
			}
        } else if (!months.equals(other.months)) {
			return false;
		}
        if (termCategory == null) {
            if (other.termCategory != null) {
				return false;
			}
        } else if (!termCategory.equals(other.termCategory)) {
			return false;
		}
        if (termEndDate == null) {
            if (other.termEndDate != null) {
				return false;
			}
        } else if (!termEndDate.equals(other.termEndDate)) {
			return false;
		}
        if (termId == null) {
            if (other.termId != null) {
				return false;
			}
        } else if (!termId.equals(other.termId)) {
			return false;
		}
        if (termName == null) {
            if (other.termName != null) {
				return false;
			}
        } else if (!termName.equals(other.termName)) {
			return false;
		}
        if (termSequenceNo == null) {
            if (other.termSequenceNo != null) {
				return false;
			}
        } else if (!termSequenceNo.equals(other.termSequenceNo)) {
			return false;
		}
        if (termStartDate == null) {
            if (other.termStartDate != null) {
				return false;
			}
        } else if (!termStartDate.equals(other.termStartDate)) {
			return false;
		}
        if (termStatus == null) {
            if (other.termStatus != null) {
				return false;
			}
        } else if (!termStatus.equals(other.termStatus)) {
			return false;
		}
        if (termType == null) {
            if (other.termType != null) {
				return false;
			}
        } else if (!termType.equals(other.termType)) {
			return false;
		}
        if (weeks == null) {
            if (other.weeks != null) {
				return false;
			}
        } else if (!weeks.equals(other.weeks)) {
			return false;
		}
        if (years == null) {
            if (other.years != null) {
				return false;
			}
        } else if (!years.equals(other.years)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TermHistoryEntity [historyId=" + historyId + ", termId=" + termId + ", termCategory=" + termCategory + ", termName=" + termName + ", termType=" + termType
                + ", termStatus=" + termStatus + ", termSequenceNo=" + termSequenceNo + ", termStartDate=" + termStartDate + ", termEndDate=" + termEndDate + ", years="
                + years + ", months=" + months + ", weeks=" + weeks + ", days=" + days + ", hours=" + hours + ", stamp=" + stamp + "]";
    }

}
