package syscon.arbutus.product.services.legal.realization.persistence.condition;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Configure Condition Entity of Case Management Service
 *
 * @author amlendu kumar
 * @version 1.0
 * @DbComment LEG_CNConfigureCondition 'The Configure Condition table for CaseManagement service'
 * @since May 21, 2014
 */
@Entity
@Audited
@Table(name = "LEG_CNConfigureCondition")
@SQLDelete(sql = "UPDATE LEG_CNConfigureCondition SET flag = 4 WHERE configureConditionId = ?")
@Where(clause = "flag = 1")
public class ConfigureConditionEntity implements Serializable {

    private static final long serialVersionUID = 1763927737556406809L;

    /**
     * @DbComment configureConditionId 'Unique system generated identifier for the Configure Condition.'
     */
    @Id
    @Column(name = "configureConditionId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CNCONFIG_CONDITION_Id")
    @SequenceGenerator(name = "SEQ_LEG_CNCONFIG_CONDITION_Id", sequenceName = "SEQ_LEG_CNCONFIG_CONDITION_Id", allocationSize = 1)
    private Long configureConditionId;

    /**
     * @DbComment activationDate 'Date when the Configure Condition was activated'
     */
    @Column(name = "activationDate", nullable = false)
    private Date activationDate;

    /**
     * @DbComment deactivationDate 'Date when the Configure Condition was deactivated'
     */
    @Column(name = "deactivationDate", nullable = true)
    private Date deactivationDate;

    /**
     * @DbComment code 'Code of Configure Conditions.'
     */
    @Column(name = "code", nullable = false, length = 64)
    private String code;

    /**
     * @DbComment configureConditionType 'Is a logical grouping of Configure Conditions by Sentences, Orders, Charges etc.'
     */
    @MetaCode(set = MetaSet.CONDITION_TYPE)
    @Column(name = "configureConditionType", nullable = false, length = 64)
    private String configureConditionType;

    /**
     * @DbComment category 'A configure condition can also be classified under different categories like Financial, Programs etc.'
     */
    @MetaCode(set = MetaSet.CONDITION_CATEGORY)
    @Column(name = "category", nullable = true, length = 64)
    private String category;

    /**
     * @DbComment subCategory 'Is a logical sub grouping of the Configure Condition category. E.g. Condition category for Financials, could be sub grouped as Restitution to victims, Electronic monitor payment etc.'
     */
    @MetaCode(set = MetaSet.CONDITION_SUB_CATEGORY)
    @Column(name = "subCategory", nullable = true, length = 64)
    private String subCategory;

    /**
     * @DbComment shortName 'The short name of the configure condition.'
     */
    @Column(name = "shortName", nullable = true, length = 200)
    private String shortName;

    /**
     * @DbComment fullDetail 'The standard legal description for the configure conditions'
     */
    @Column(name = "fullDetail", nullable = true, length = 4000)
    private String fullDetail;

    /**
     * @DbComment attribute 'Code of Configure Conditions.'
     */
    @Column(name = "attribute", nullable = true, length = 64)
    private String attribute;

    /**
     * @DbComment autoPopulateToCasePlan 'Indicates whether the configure condition is auto populate to Case Plan or not.'
     */
    @Column(name = "autoPopulateToCasePlan", nullable = true)
    private Boolean autoPopulateToCasePlan;

    /**
     * @DbComment objective 'objective of the Configure COndition, this is required if auto populate to Case Plan indicator is true'
     */
    @MetaCode(set = MetaSet.OBJECTIVES)
    @Column(name = "objective", nullable = true, length = 64)
    private String objective;

    /**
     * @DbComment casePlanType 'Case Plan Type of the Configure Condition, this is required if auto populate to Case Plan indicator is true'
     */
    @MetaCode(set = MetaSet.CASE_PLAN_TYPE)
    @Column(name = "casePlanType", nullable = true, length = 64)
    private String casePlanType;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @Embedded
    @NotAudited
    private StampEntity stamp;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(name = "Flag", nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * @return the configureConditionId
     */
    public Long getConfigureConditionId() {
        return configureConditionId;
    }

    /**
     * @param configureConditionId the configureConditionId to set
     */
    public void setConfigureConditionId(Long configureConditionId) {
        this.configureConditionId = configureConditionId;
    }

    /**
     * @return the activationDate
     */
    public Date getActivationDate() {
        return activationDate;
    }

    /**
     * @param activationDate the activationDate to set
     */
    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    /**
     * @return the deactivationDate
     */
    public Date getDeactivationDate() {
        return deactivationDate;
    }

    /**
     * @param deactivationDate the deactivationDate to set
     */
    public void setDeactivationDate(Date deactivationDate) {
        this.deactivationDate = deactivationDate;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the configureConditionType
     */
    public String getConfigureConditionType() {
        return configureConditionType;
    }

    /**
     * @param configureConditionType the configureConditionType to set
     */
    public void setConfigureConditionType(String configureConditionType) {
        this.configureConditionType = configureConditionType;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the subCategory
     */
    public String getSubCategory() {
        return subCategory;
    }

    /**
     * @param subCategory the subCategory to set
     */
    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    /**
     * @return the shortName
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * @param shortName the shortName to set
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * @return the fullDetail
     */
    public String getFullDetail() {
        return fullDetail;
    }

    /**
     * @param fullDetail the fullDetail to set
     */
    public void setFullDetail(String fullDetail) {
        this.fullDetail = fullDetail;
    }

    /**
     * @return the attribute
     */
    public String getAttribute() {
        return attribute;
    }

    /**
     * @param attribute the attribute to set
     */
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    /**
     * @return the autoPopulateToCasePlan
     */
    public Boolean getAutoPopulateToCasePlan() {
        return autoPopulateToCasePlan;
    }

    /**
     * @param autoPopulateToCasePlan the autoPopulateToCasePlan to set
     */
    public void setAutoPopulateToCasePlan(Boolean autoPopulateToCasePlan) {
        this.autoPopulateToCasePlan = autoPopulateToCasePlan;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /**
     * @return the objective
     */
    public String getObjective() {
        return objective;
    }

    /**
     * @param objective the objective to set
     */
    public void setObjective(String objective) {
        this.objective = objective;
    }

    /**
     * @return the casePlanType
     */
    public String getCasePlanType() {
        return casePlanType;
    }

    /**
     * @param casePlanType the casePlanType to set
     */
    public void setCasePlanType(String casePlanType) {
        this.casePlanType = casePlanType;
    }

}
