package syscon.arbutus.product.services.legal.realization.persistence.sentenceadjustment;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.Associable;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * SentenceAssocEntity for Sentence Adjustment Association
 *
 * @DbComment LEG_SentAdjustAssc 'Table of Sentence Adjustment Association'
 * User: yshang
 * Date: 11/10/13
 * Time: 10:39 AM
 */
@Audited
@Entity
@Table(name = "LEG_SentAdjustAssc")
public class SentenceAssocEntity implements Serializable, Associable {

    private static final long serialVersionUID = -5043452927075005687L;

    /**
     * @DbComment AssociationId 'Id of Sentence Association, the table LEG_SentAdjustAssc'
     */
    @Id
    @Column(name = "AssociationId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_SentAdjustAssc_Id")
    @SequenceGenerator(name = "SEQ_LEG_SentAdjustAssc_Id", sequenceName = "SEQ_LEG_SentAdjustAssc_Id", allocationSize = 1)
    private Long associationId;

    /**
     * @DbComment ToClass 'Sentence to be associated'
     */
    @Column(name = "ToClass", nullable = false, length = 64)
    private String toClass;

    /**
     * @DbComment ToIdentifier 'Identification of Sentence'
     */
    @Column(name = "ToIdentifier", nullable = false)
    private Long toIdentifier;

    /**
     * @DbComment FromIdentifier 'Id of Adjustment, the table LEG_SentAdjmnt'
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FromIdentifier", referencedColumnName = "adjustmentId", nullable = false)
    private AdjustmentEntity adjustment;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public SentenceAssocEntity() {
    }

    public Long getAssociationId() {
        return associationId;
    }

    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    public String getToClass() {
        return toClass;
    }

    public void setToClass(String toClass) {
        this.toClass = toClass;
    }

    public Long getToIdentifier() {
        return toIdentifier;
    }

    public void setToIdentifier(Long toIdentifier) {
        this.toIdentifier = toIdentifier;
    }

    public AdjustmentEntity getAdjustment() {
        return adjustment;
    }

    public void setAdjustment(AdjustmentEntity adjustment) {
        this.adjustment = adjustment;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (!(o instanceof SentenceAssocEntity)) {
			return false;
		}

        SentenceAssocEntity that = (SentenceAssocEntity) o;

        if (associationId != null ? !associationId.equals(that.associationId) : that.associationId != null) {
			return false;
		}
        if (toClass != null ? !toClass.equals(that.toClass) : that.toClass != null) {
			return false;
		}
        if (toIdentifier != null ? !toIdentifier.equals(that.toIdentifier) : that.toIdentifier != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = associationId != null ? associationId.hashCode() : 0;
        result = 31 * result + (toClass != null ? toClass.hashCode() : 0);
        result = 31 * result + (toIdentifier != null ? toIdentifier.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SentenceAssocEntity{" +
                "associationId=" + associationId +
                ", toClass='" + toClass + '\'' +
                ", toIdentifier=" + toIdentifier +
                '}';
    }
}
