package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.Valid;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;
import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * OrderSearchType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 21, 2012
 */
@ArbutusConstraint(constraints = { "fromOrderIssuanceDate <= toOrderIssuanceDate", "fromOrderReceivedDate <= toOrderReceivedDate",
        "fromOrderStartDate <= toOrderStartDate", "fromOrderExpirationDate <= toOrderExpirationDate", "fromSentenceStartDate <= toSentenceStartDate",
        "fromSentenceEndDate <= toSentenceEndDate" })
public class OrderSearchType implements Serializable {

    private static final long serialVersionUID = -737931375114522783L;

    private Long ojSupervisionId;

    private String orderClassification;

    private String orderType;

    private String orderSubType;

    private String orderCategory;

    private String orderNumber;

    @Valid
    private DispositionSearchType orderDisposition;

    private CommentType comments;

    private Date fromOrderIssuanceDate;

    private Date toOrderIssuanceDate;

    private Date fromOrderReceivedDate;

    private Date toOrderReceivedDate;

    private Date fromOrderStartDate;

    private Date toOrderStartDate;

    private Date fromOrderExpirationDate;

    private Date toOrderExpirationDate;

    private Long caseActivityInititatedOrderAssociation;

    private Long orderInititatedCaseActivityAssociation;

    private NotificationSearchType issuingAgency;

    private Boolean isSchedulingNeeded;

    private Boolean hasCharges;

    private Boolean isHoldingOrder;

    private Set<NotificationSearchType> agencyToBeNotified;
    @Valid
    private Set<NotificationLogSearchType> notificationLog;

    private BailAmountSearchType bailSetAmount;

    private String bailSetRelationship;

    private BigDecimal fromBailPostedAmount;

    private BigDecimal toBailPostedAmount;

    private String bailPaymentReceiptNo;

    private String bailRequirement;

    private Long bailerPersonIdentityAssociation;

    private BigDecimal fromBondPostedAmount;

    private BigDecimal toBondPostedAmount;

    private String bondPaymentDescription;

    private Long bondOrganizationAssociation;

    private String sentenceType;

    private Long sentenceNumber;
    @Valid
    private TermSearchType sentenceTerm;

    private String sentenceStatus;

    private Date fromSentenceStartDate;

    private Date toSentenceStartDate;

    private Date fromSentenceEndDate;

    private Date toSentenceEndDate;

    private SentenceAppealSearchType sentenceAppeal;

    private Long concecutiveFrom;

    private BigDecimal fromFineAmount;

    private BigDecimal toFineAmount;

    private Boolean fineAmountPaid;

    private Boolean sentenceAggravatedFlag;
    @Valid
    private Set<IntermittentScheduleType> intermittentSchedule;

    /**
     * Constructor
     */
    public OrderSearchType() {
        super();
    }

    /**
     * Constructor
     *
     * @param orderClassification                    String. The order classification. This defines the type of instance of the service.
     * @param orderType                              String. The client defined order type that maps to the order classification.
     * @param orderSubType                           String. The sub type of an order. For example a want, warrant or detainer.
     * @param orderCategory                          String. The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.
     * @param orderNumber                            String(length <= 64). User specified order number.
     * @param orderDisposition                       DispositionSearchType. The disposition related to the order.
     * @param comments                               CommentType. The comments related to an order.
     * @param fromOrderIssuanceDate                  Date. The date an order was issued.
     * @param toOrderIssuanceDate                    Date. The date an order was issued.
     * @param fromOrderReceivedDate                  Date. The date an order was received.
     * @param toOrderReceivedDate                    Date. The date an order was received.
     * @param fromOrderStartDate                     Date. The date/time an order starts (becomes valid).
     * @param toOrderStartDate                       Date. The date/time an order starts (becomes valid).
     * @param fromOrderExpirationDate                Date. The date an order expires.
     * @param toOrderExpirationDate                  Date. The date an order expires.
     * @param caseActivityInititatedOrderAssociation Long. The case related activities that initiated this order. Static association to the Case Activity service.
     * @param orderInititatedCaseActivityAssociation Long. The case activities that were initiated by this order. Static association to the Case Activity service.
     * @param issuingAgency                          NotificationSearchType. The source agency that issues the order, this could be a facility or an organization.
     * @param isSchedulingNeeded                     Boolean. If true, scheduling is needed for the order, false otherwise.
     * @param hasCharges                             Boolean. If true, the order has charges related, false otherwise.
     * @param isHoldingOrder                         Boolean. If true then the order is a holding document, false otherwise.
     * @param agencyToBeNotified                     Set&lt;NotificationSearchType>. The agencies to be notified while releasing the offender, this could be a facility or an organization.
     * @param notificationLog                        Set&lt;NotificationLogSearchType>. Inquiries made about an inmate’s legal status.
     * @param bailSetAmount                          BailAmountSearchType. The bail amounts for different bail types.
     * @param bailSetRelationship                    String. The relationship of the bail amount specified.
     * @param fromBailPostedAmount                   BigDecimal. From the amount of bail posted.
     * @param toBailPostedAmount                     BigDecimal. To the amount of bail posted.
     * @param bailPaymentReceiptNo                   String(length <= 64). Bill Payment Receipt Number for cash bail payment.
     * @param bailRequirement                        String(length <= 128). A description of the bail requirement set at a court hearing.
     * @param bailerPersonIdentityAssociation        Long. Person who posted the bail for the offender. Static reference to person identity.
     * @param fromBondPostedAmount                   BigDecimal. From the amount of bond posted.
     * @param toBondPostedAmount                     BigDecimal. To the amount of bond posted.
     * @param bondPaymentDescription                 String(length <= 512). A description of what an offender pays for a bond.
     * @param bondOrganizationAssociation            Long. The bond agency (organization) that posted the bail. Static reference to organization.
     * @param sentenceType                           String. The type of sentence (Definite, Split, Life etc.,).
     * @param sentenceNumber                         Long. Identifier for the sentence. Will be used  on the UI to specify the sentence relationship (concecutive).
     * @param sentenceTerm                           TermSearchType. The terms and duration for each type of a sentence.
     * @param sentenceStatus                         String. The status of a sentence, will be used by sentence calculation logic. Pending/Included/Excluded.
     * @param fromSentenceStartDate                  Date. The start date/time for the sentence.
     * @param toSentenceStartDate                    Date. The start date/time for the sentence.
     * @param fromSentenceEndDate                    Date. The date on which the sentence ends.
     * @param toSentenceEndDate                      Date. The date on which the sentence ends.
     * @param sentenceAppeal                         SentenceAppealSearchType. The status of an appeal on a sentence.
     * @param concecutiveSentenceAssociation         Long. The static reference to the ordersentence service that relates to a sentence that this order (sentence) is concecutive to.
     * @param fromFineAmount                         BigDecimal. The fine for a sentence from. The unit would be the system wide currency defined.
     * @param toFineAmount                           BigDecimal. The fine for a sentence to. The unit would be the system wide currency defined.
     * @param fineAmountPaid                         Boolean. If true, Fine amount is paid, false otherwise.
     * @param sentenceAggravatedFlag                 Boolean. True if aggravating factors were considered during sentencing, false otherwise.
     * @param intermittentSchedule                   Set&lt;IntermittentScheduleType>. Days or parts of days in a week when an intermittent sentence is to be served.
     */
    public OrderSearchType(String orderClassification, String orderType, String orderSubType, String orderCategory, String orderNumber,
            DispositionSearchType orderDisposition, CommentType comments, Date fromOrderIssuanceDate, Date toOrderIssuanceDate, Date fromOrderReceivedDate,
            Date toOrderReceivedDate, Date fromOrderStartDate, Date toOrderStartDate, Date fromOrderExpirationDate, Date toOrderExpirationDate,
            Long caseActivityInititatedOrderAssociation, Long orderInititatedCaseActivityAssociation, NotificationSearchType issuingAgency, Boolean isSchedulingNeeded,
            Boolean hasCharges, Boolean isHoldingOrder, Set<NotificationSearchType> agencyToBeNotified, Set<NotificationLogSearchType> notificationLog,
            BailAmountSearchType bailSetAmount, String bailSetRelationship, BigDecimal fromBailPostedAmount, BigDecimal toBailPostedAmount, String bailPaymentReceiptNo,
            String bailRequirement, Long bailerPersonIdentityAssociation, BigDecimal fromBondPostedAmount, BigDecimal toBondPostedAmount, String bondPaymentDescription,
            Long bondOrganizationAssociation, String sentenceType, Long sentenceNumber, TermSearchType sentenceTerm, String sentenceStatus, Date fromSentenceStartDate,
            Date toSentenceStartDate, Date fromSentenceEndDate, Date toSentenceEndDate, SentenceAppealSearchType sentenceAppeal, Long concecutiveSentenceAssociation,
            BigDecimal fromFineAmount, BigDecimal toFineAmount, Boolean fineAmountPaid, Boolean sentenceAggravatedFlag,
            Set<IntermittentScheduleType> intermittentSchedule) {
        super();
        this.orderClassification = orderClassification;
        this.orderType = orderType;
        this.orderSubType = orderSubType;
        this.orderCategory = orderCategory;
        this.orderNumber = orderNumber;
        this.orderDisposition = orderDisposition;
        this.comments = comments;
        this.fromOrderIssuanceDate = fromOrderIssuanceDate;
        this.toOrderIssuanceDate = toOrderIssuanceDate;
        this.fromOrderReceivedDate = fromOrderReceivedDate;
        this.toOrderReceivedDate = toOrderReceivedDate;
        this.fromOrderStartDate = fromOrderStartDate;
        this.toOrderStartDate = toOrderStartDate;
        this.fromOrderExpirationDate = fromOrderExpirationDate;
        this.toOrderExpirationDate = toOrderExpirationDate;
        this.caseActivityInititatedOrderAssociation = caseActivityInititatedOrderAssociation;
        this.orderInititatedCaseActivityAssociation = orderInititatedCaseActivityAssociation;
        this.issuingAgency = issuingAgency;
        this.isSchedulingNeeded = isSchedulingNeeded;
        this.hasCharges = hasCharges;
        this.isHoldingOrder = isHoldingOrder;
        this.agencyToBeNotified = agencyToBeNotified;
        this.notificationLog = notificationLog;
        this.bailSetAmount = bailSetAmount;
        this.bailSetRelationship = bailSetRelationship;
        this.fromBailPostedAmount = fromBailPostedAmount;
        this.toBailPostedAmount = toBailPostedAmount;
        this.bailPaymentReceiptNo = bailPaymentReceiptNo;
        this.bailRequirement = bailRequirement;
        this.bailerPersonIdentityAssociation = bailerPersonIdentityAssociation;
        this.fromBondPostedAmount = fromBondPostedAmount;
        this.toBondPostedAmount = toBondPostedAmount;
        this.bondPaymentDescription = bondPaymentDescription;
        this.bondOrganizationAssociation = bondOrganizationAssociation;
        this.sentenceType = sentenceType;
        this.sentenceNumber = sentenceNumber;
        this.sentenceTerm = sentenceTerm;
        this.sentenceStatus = sentenceStatus;
        this.fromSentenceStartDate = fromSentenceStartDate;
        this.toSentenceStartDate = toSentenceStartDate;
        this.fromSentenceEndDate = fromSentenceEndDate;
        this.toSentenceEndDate = toSentenceEndDate;
        this.sentenceAppeal = sentenceAppeal;
        this.concecutiveFrom = concecutiveSentenceAssociation;
        this.fromFineAmount = fromFineAmount;
        this.toFineAmount = toFineAmount;
        this.fineAmountPaid = fineAmountPaid;
        this.sentenceAggravatedFlag = sentenceAggravatedFlag;
        this.intermittentSchedule = intermittentSchedule;
    }

    /**
     * Get Supervision ID from Outside Jurisdiction(OJ)
     *
     * @return Supervision Id from Outside Jurisdiction
     */
    public Long getOjSupervisionId() {
        return ojSupervisionId;
    }

    /**
     * Set Supervision ID from Outside Jurisdiction(OJ)
     *
     * @param ojSupervisionId Supervision Id from Outside Jurisdiction
     */
    public void setOjSupervisionId(Long ojSupervisionId) {
        this.ojSupervisionId = ojSupervisionId;
    }

    /**
     * The order classification. This defines the type of instance of the service.
     *
     * @return the orderClassification
     */
    public String getOrderClassification() {
        return orderClassification;
    }

    /**
     * The order classification. This defines the type of instance of the service.
     *
     * @param orderClassification the orderClassification to set
     */
    public void setOrderClassification(String orderClassification) {
        this.orderClassification = orderClassification;
    }

    /**
     * The client defined order type that maps to the order classification.
     *
     * @return the orderType
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * The client defined order type that maps to the order classification.
     *
     * @param orderType the orderType to set
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    /**
     * The sub type of an order. For example a want, warrant or detainer.
     *
     * @return the orderSubType
     */
    public String getOrderSubType() {
        return orderSubType;
    }

    /**
     * The sub type of an order. For example a want, warrant or detainer.
     *
     * @param orderSubType the orderSubType to set
     */
    public void setOrderSubType(String orderSubType) {
        this.orderSubType = orderSubType;
    }

    /**
     * The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.
     *
     * @return the orderCategory
     */
    public String getOrderCategory() {
        return orderCategory;
    }

    /**
     * The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.
     *
     * @param orderCategory the orderCategory to set
     */
    public void setOrderCategory(String orderCategory) {
        this.orderCategory = orderCategory;
    }

    /**
     * User specified order number.
     *
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * User specified order number.
     *
     * @param orderNumber the orderNumber to set (length <= 64)
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * The disposition related to the order.
     *
     * @return the orderDisposition
     */
    public DispositionSearchType getOrderDisposition() {
        return orderDisposition;
    }

    /**
     * The disposition related to the order.
     *
     * @param orderDisposition the orderDisposition to set
     */
    public void setOrderDisposition(DispositionSearchType orderDisposition) {
        this.orderDisposition = orderDisposition;
    }

    /**
     * The comments related to an order.
     *
     * @return the comments
     */
    public CommentType getComments() {
        return comments;
    }

    /**
     * The comments related to an order.
     *
     * @param comments the comments to set
     */
    public void setComments(CommentType comments) {
        this.comments = comments;
    }

    /**
     * The date an order was issued.
     *
     * @return the fromOrderIssuanceDate
     */
    public Date getFromOrderIssuanceDate() {
        return fromOrderIssuanceDate;
    }

    /**
     * The date an order was issued.
     *
     * @param fromOrderIssuanceDate the fromOrderIssuanceDate to set
     */
    public void setFromOrderIssuanceDate(Date fromOrderIssuanceDate) {
        this.fromOrderIssuanceDate = fromOrderIssuanceDate;
    }

    /**
     * The date an order was issued.
     *
     * @return the toOrderIssuanceDate
     */
    public Date getToOrderIssuanceDate() {
        return toOrderIssuanceDate;
    }

    /**
     * The date an order was issued.
     *
     * @param toOrderIssuanceDate the toOrderIssuanceDate to set
     */
    public void setToOrderIssuanceDate(Date toOrderIssuanceDate) {
        this.toOrderIssuanceDate = toOrderIssuanceDate;
    }

    /**
     * The date an order was received.
     *
     * @return the fromOrderReceivedDate
     */
    public Date getFromOrderReceivedDate() {
        return fromOrderReceivedDate;
    }

    /**
     * The date an order was received.
     *
     * @param fromOrderReceivedDate the fromOrderReceivedDate to set
     */
    public void setFromOrderReceivedDate(Date fromOrderReceivedDate) {
        this.fromOrderReceivedDate = fromOrderReceivedDate;
    }

    /**
     * The date an order was received.
     *
     * @return the toOrderReceivedDate
     */
    public Date getToOrderReceivedDate() {
        return toOrderReceivedDate;
    }

    /**
     * The date an order was received.
     *
     * @param toOrderReceivedDate the toOrderReceivedDate to set
     */
    public void setToOrderReceivedDate(Date toOrderReceivedDate) {
        this.toOrderReceivedDate = toOrderReceivedDate;
    }

    /**
     * The date/time an order starts (becomes valid).
     *
     * @return the fromOrderStartDate
     */
    public Date getFromOrderStartDate() {
        return fromOrderStartDate;
    }

    /**
     * The date/time an order starts (becomes valid).
     *
     * @param fromOrderStartDate the fromOrderStartDate to set
     */
    public void setFromOrderStartDate(Date fromOrderStartDate) {
        this.fromOrderStartDate = fromOrderStartDate;
    }

    /**
     * The date/time an order starts (becomes valid).
     *
     * @return the toOrderStartDate
     */
    public Date getToOrderStartDate() {
        return toOrderStartDate;
    }

    /**
     * The date/time an order starts (becomes valid).
     *
     * @param toOrderStartDate the toOrderStartDate to set
     */
    public void setToOrderStartDate(Date toOrderStartDate) {
        this.toOrderStartDate = toOrderStartDate;
    }

    /**
     * The date an order expires.
     *
     * @return the fromOrderExpirationDate
     */
    public Date getFromOrderExpirationDate() {
        return fromOrderExpirationDate;
    }

    /**
     * The date an order expires.
     *
     * @param fromOrderExpirationDate the fromOrderExpirationDate to set
     */
    public void setFromOrderExpirationDate(Date fromOrderExpirationDate) {
        this.fromOrderExpirationDate = fromOrderExpirationDate;
    }

    /**
     * The date an order expires.
     *
     * @return the toOrderExpirationDate
     */
    public Date getToOrderExpirationDate() {
        return toOrderExpirationDate;
    }

    /**
     * The date an order expires.
     *
     * @param toOrderExpirationDate the toOrderExpirationDate to set
     */
    public void setToOrderExpirationDate(Date toOrderExpirationDate) {
        this.toOrderExpirationDate = toOrderExpirationDate;
    }

    /**
     * The case related activities that initiated this order. Static association to the Case Activity service.
     *
     * @return the caseActivityInititatedOrderAssociation
     */
    public Long getCaseActivityInititatedOrderAssociation() {
        return caseActivityInititatedOrderAssociation;
    }

    /**
     * The case related activities that initiated this order. Static association to the Case Activity service.
     *
     * @param caseActivityInititatedOrderAssociation the caseActivityInititatedOrderAssociation to set
     */
    public void setCaseActivityInititatedOrderAssociation(Long caseActivityInititatedOrderAssociation) {
        this.caseActivityInititatedOrderAssociation = caseActivityInititatedOrderAssociation;
    }

    /**
     * The case related activities that initiated this order. Static association to the Case Activity service.
     *
     * @return the orderInititatedCaseActivityAssociation
     */
    public Long getOrderInititatedCaseActivityAssociation() {
        return orderInititatedCaseActivityAssociation;
    }

    /**
     * The case related activities that initiated this order. Static association to the Case Activity service.
     *
     * @param orderInititatedCaseActivityAssociation the orderInititatedCaseActivityAssociation to set
     */
    public void setOrderInititatedCaseActivityAssociation(Long orderInititatedCaseActivityAssociation) {
        this.orderInititatedCaseActivityAssociation = orderInititatedCaseActivityAssociation;
    }

    /**
     * The source agency that issues the order, this could be a facility or an organization.
     *
     * @return the issuingAgency
     */
    public NotificationSearchType getIssuingAgency() {
        return issuingAgency;
    }

    /**
     * The source agency that issues the order, this could be a facility or an organization.
     *
     * @param issuingAgency the issuingAgency to set
     */
    public void setIssuingAgency(NotificationSearchType issuingAgency) {
        this.issuingAgency = issuingAgency;
    }

    /**
     * If true, scheduling is needed for the order, false otherwise. If true, the order has charges related, false otherwise.
     *
     * @return the isSchedulingNeeded
     */
    public Boolean getIsSchedulingNeeded() {
        return isSchedulingNeeded;
    }

    /**
     * If true, scheduling is needed for the order, false otherwise. If true, the order has charges related, false otherwise.
     *
     * @param isSchedulingNeeded the isSchedulingNeeded to set
     */
    public void setIsSchedulingNeeded(Boolean isSchedulingNeeded) {
        this.isSchedulingNeeded = isSchedulingNeeded;
    }

    /**
     * If true, the order has charges related, false otherwise.
     *
     * @return the hasCharges
     */
    public Boolean getHasCharges() {
        return hasCharges;
    }

    /**
     * If true, the order has charges related, false otherwise.
     *
     * @param hasCharges the hasCharges to set
     */
    public void setHasCharges(Boolean hasCharges) {
        this.hasCharges = hasCharges;
    }

    /**
     * If true then the order is a holding document, false otherwise.
     *
     * @return the isHoldingOrder
     */
    public Boolean getIsHoldingOrder() {
        return isHoldingOrder;
    }

    /**
     * If true then the order is a holding document, false otherwise.
     *
     * @param isHoldingOrder the isHoldingOrder to set
     */
    public void setIsHoldingOrder(Boolean isHoldingOrder) {
        this.isHoldingOrder = isHoldingOrder;
    }

    /**
     * The agencies to be notified while releasing the offender, this could be a facility or an organization.
     *
     * @return the agencyToBeNotified
     */
    public Set<NotificationSearchType> getAgencyToBeNotified() {
        return agencyToBeNotified;
    }

    /**
     * The agencies to be notified while releasing the offender, this could be a facility or an organization.
     *
     * @param agencyToBeNotified the agencyToBeNotified to set
     */
    public void setAgencyToBeNotified(Set<NotificationSearchType> agencyToBeNotified) {
        this.agencyToBeNotified = agencyToBeNotified;
    }

    /**
     * Inquiries made about an inmate’s legal status.
     *
     * @return the notificationLog
     */
    public Set<NotificationLogSearchType> getNotificationLog() {
        return notificationLog;
    }

    /**
     * Inquiries made about an inmate’s legal status.
     *
     * @param notificationLog the notificationLog to set
     */
    public void setNotificationLog(Set<NotificationLogSearchType> notificationLog) {
        this.notificationLog = notificationLog;
    }

    /**
     * The bail amounts for different bail types.
     *
     * @return the bailSetAmount
     */
    public BailAmountSearchType getBailSetAmount() {
        return bailSetAmount;
    }

    /**
     * The bail amounts for different bail types.
     *
     * @param bailSetAmount the bailSetAmount to set
     */
    public void setBailSetAmount(BailAmountSearchType bailSetAmount) {
        this.bailSetAmount = bailSetAmount;
    }

    /**
     * The relationship of the bail amount specified.
     *
     * @return the bailSetRelationship
     */
    public String getBailSetRelationship() {
        return bailSetRelationship;
    }

    /**
     * The relationship of the bail amount specified.
     *
     * @param bailSetRelationship the bailSetRelationship to set
     */
    public void setBailSetRelationship(String bailSetRelationship) {
        this.bailSetRelationship = bailSetRelationship;
    }

    /**
     * @return the fromBailPostedAmount
     */
    public BigDecimal getFromBailPostedAmount() {
        return fromBailPostedAmount;
    }

    /**
     * @param fromBailPostedAmount the fromBailPostedAmount to set
     */
    public void setFromBailPostedAmount(BigDecimal fromBailPostedAmount) {
        this.fromBailPostedAmount = fromBailPostedAmount;
    }

    /**
     * @return the toBailPostedAmount
     */
    public BigDecimal getToBailPostedAmount() {
        return toBailPostedAmount;
    }

    /**
     * @param toBailPostedAmount the toBailPostedAmount to set
     */
    public void setToBailPostedAmount(BigDecimal toBailPostedAmount) {
        this.toBailPostedAmount = toBailPostedAmount;
    }

    /**
     * Bill Payment Receipt Number for cash bail payment.
     *
     * @return the bailPaymentReceiptNo
     */
    public String getBailPaymentReceiptNo() {
        return bailPaymentReceiptNo;
    }

    /**
     * Bill Payment Receipt Number for cash bail payment.
     *
     * @param bailPaymentReceiptNo the bailPaymentReceiptNo to set (length <= 64)
     */
    public void setBailPaymentReceiptNo(String bailPaymentReceiptNo) {
        this.bailPaymentReceiptNo = bailPaymentReceiptNo;
    }

    /**
     * A description of the bail requirement set at a court hearing.
     *
     * @return the bailRequirement
     */
    public String getBailRequirement() {
        return bailRequirement;
    }

    /**
     * A description of the bail requirement set at a court hearing.
     *
     * @param bailRequirement the bailRequirement to set (length <= 128)
     */
    public void setBailRequirement(String bailRequirement) {
        this.bailRequirement = bailRequirement;
    }

    /**
     * Person who posted the bail for the offender. Static reference to person identity.
     *
     * @return the bailerPersonIdentityAssociation
     */
    public Long getBailerPersonIdentityAssociation() {
        return bailerPersonIdentityAssociation;
    }

    /**
     * Person who posted the bail for the offender. Static reference to person identity.
     *
     * @param bailerPersonIdentityAssociation the bailerPersonIdentityAssociation to set
     */
    public void setBailerPersonIdentityAssociation(Long bailerPersonIdentityAssociation) {
        this.bailerPersonIdentityAssociation = bailerPersonIdentityAssociation;
    }

    /**
     * @return the fromBondPostedAmount
     */
    public BigDecimal getFromBondPostedAmount() {
        return fromBondPostedAmount;
    }

    /**
     * @param fromBondPostedAmount the fromBondPostedAmount to set
     */
    public void setFromBondPostedAmount(BigDecimal fromBondPostedAmount) {
        this.fromBondPostedAmount = fromBondPostedAmount;
    }

    /**
     * @return the toBondPostedAmount
     */
    public BigDecimal getToBondPostedAmount() {
        return toBondPostedAmount;
    }

    /**
     * @param toBondPostedAmount the toBondPostedAmount to set
     */
    public void setToBondPostedAmount(BigDecimal toBondPostedAmount) {
        this.toBondPostedAmount = toBondPostedAmount;
    }

    /**
     * A description of what an offender pays for a bond.
     *
     * @return the bondPaymentDescription
     */
    public String getBondPaymentDescription() {
        return bondPaymentDescription;
    }

    /**
     * A description of what an offender pays for a bond.
     *
     * @param bondPaymentDescription the bondPaymentDescription to set (length <= 512)
     */
    public void setBondPaymentDescription(String bondPaymentDescription) {
        this.bondPaymentDescription = bondPaymentDescription;
    }

    /**
     * The bond agency (organization) that posted the bail. Static reference to organization.
     *
     * @return the bondOrganizationAssociation
     */
    public Long getBondOrganizationAssociation() {
        return bondOrganizationAssociation;
    }

    /**
     * The bond agency (organization) that posted the bail. Static reference to organization.
     *
     * @param bondOrganizationAssociation the bondOrganizationAssociation to set
     */
    public void setBondOrganizationAssociation(Long bondOrganizationAssociation) {
        this.bondOrganizationAssociation = bondOrganizationAssociation;
    }

    /**
     * The type of sentence (Definite, Split, Life etc.,).
     *
     * @return the sentenceType
     */
    public String getSentenceType() {
        return sentenceType;
    }

    /**
     * The type of sentence (Definite, Split, Life etc.,).
     *
     * @param sentenceType the sentenceType to set
     */
    public void setSentenceType(String sentenceType) {
        this.sentenceType = sentenceType;
    }

    /**
     * Identifier for the sentence. Will be used  on the UI to specify the sentence relationship (concecutive).
     *
     * @return the sentenceNumber
     */
    public Long getSentenceNumber() {
        return sentenceNumber;
    }

    /**
     * Identifier for the sentence. Will be used  on the UI to specify the sentence relationship (concecutive).
     *
     * @param sentenceNumber the sentenceNumber to set (length <= 64)
     */
    public void setSentenceNumber(Long sentenceNumber) {
        this.sentenceNumber = sentenceNumber;
    }

    /**
     * The terms and duration for each type of a sentence.
     *
     * @return the sentenceTerm
     */
    public TermSearchType getSentenceTerm() {
        return sentenceTerm;
    }

    /**
     * The terms and duration for each type of a sentence.
     *
     * @param sentenceTerm the sentenceTerm to set
     */
    public void setSentenceTerm(TermSearchType sentenceTerm) {
        this.sentenceTerm = sentenceTerm;
    }

    /**
     * The status of a sentence, will be used by sentence calculation logic. Pending/Included/Excluded.
     *
     * @return the sentenceStatus
     */
    public String getSentenceStatus() {
        return sentenceStatus;
    }

    /**
     * The status of a sentence, will be used by sentence calculation logic. Pending/Included/Excluded.
     *
     * @param sentenceStatus the sentenceStatus to set
     */
    public void setSentenceStatus(String sentenceStatus) {
        this.sentenceStatus = sentenceStatus;
    }

    /**
     * The start date/time for the sentence.
     *
     * @return the fromSentenceStartDate
     */
    public Date getFromSentenceStartDate() {
        return fromSentenceStartDate;
    }

    /**
     * The start date/time for the sentence.
     *
     * @param fromSentenceStartDate the fromSentenceStartDate to set
     */
    public void setFromSentenceStartDate(Date fromSentenceStartDate) {
        this.fromSentenceStartDate = fromSentenceStartDate;
    }

    /**
     * The start date/time for the sentence.
     *
     * @return the toSentenceStartDate
     */
    public Date getToSentenceStartDate() {
        return toSentenceStartDate;
    }

    /**
     * The start date/time for the sentence.
     *
     * @param toSentenceStartDate the toSentenceStartDate to set
     */
    public void setToSentenceStartDate(Date toSentenceStartDate) {
        this.toSentenceStartDate = toSentenceStartDate;
    }

    /**
     * The date on which the sentence ends.
     *
     * @return the fromSentenceEndDate
     */
    public Date getFromSentenceEndDate() {
        return fromSentenceEndDate;
    }

    /**
     * The date on which the sentence ends.
     *
     * @param fromSentenceEndDate the fromSentenceEndDate to set
     */
    public void setFromSentenceEndDate(Date fromSentenceEndDate) {
        this.fromSentenceEndDate = fromSentenceEndDate;
    }

    /**
     * The date on which the sentence ends.
     *
     * @return the toSentenceEndDate
     */
    public Date getToSentenceEndDate() {
        return toSentenceEndDate;
    }

    /**
     * The date on which the sentence ends.
     *
     * @param toSentenceEndDate the toSentenceEndDate to set
     */
    public void setToSentenceEndDate(Date toSentenceEndDate) {
        this.toSentenceEndDate = toSentenceEndDate;
    }

    /**
     * The status of an appeal on a sentence.
     *
     * @return the sentenceAppeal
     */
    public SentenceAppealSearchType getSentenceAppeal() {
        return sentenceAppeal;
    }

    /**
     * The status of an appeal on a sentence.
     *
     * @param sentenceAppeal the sentenceAppeal to set
     */
    public void setSentenceAppeal(SentenceAppealSearchType sentenceAppeal) {
        this.sentenceAppeal = sentenceAppeal;
    }

    /**
     * The static reference to the ordersentence service that relates to a sentence that this order (sentence) is concecutive to.
     *
     * @return the concecutiveFrom
     */
    public Long getConcecutiveFrom() {
        return concecutiveFrom;
    }

    /**
     * The static reference to the ordersentence service that relates to a sentence that this order (sentence) is concecutive to.
     *
     * @param concecutiveFrom the concecutiveSentenceAssociation to set
     */
    public void setConcecutiveFrom(Long concecutiveFrom) {
        this.concecutiveFrom = concecutiveFrom;
    }

    /**
     * The fine for a sentence. The unit would be the system wide currency defined.
     *
     * @return the fromFineAmount
     */
    public BigDecimal getFromFineAmount() {
        return fromFineAmount;
    }

    /**
     * The fine for a sentence. The unit would be the system wide currency defined.
     *
     * @param fromFineAmount the fineAmount to set
     */
    public void setFromFineAmount(BigDecimal fromFineAmount) {
        this.fromFineAmount = fromFineAmount;
    }

    /**
     * @return the toFineAmount
     */
    public BigDecimal getToFineAmount() {
        return toFineAmount;
    }

    /**
     * @param toFineAmount the toFineAmount to set
     */
    public void setToFineAmount(BigDecimal toFineAmount) {
        this.toFineAmount = toFineAmount;
    }

    /**
     * If true, Fine amount is paid, false otherwise.
     *
     * @return the fineAmountPaid
     */
    public Boolean getFineAmountPaid() {
        return fineAmountPaid;
    }

    /**
     * If true, Fine amount is paid, false otherwise.
     *
     * @param fineAmountPaid the fineAmountPaid to set
     */
    public void setFineAmountPaid(Boolean fineAmountPaid) {
        this.fineAmountPaid = fineAmountPaid;
    }

    /**
     * True if aggravating factors were considered during sentencing, false otherwise.
     *
     * @return the sentenceAggravatedFlag
     */
    public Boolean getSentenceAggravatedFlag() {
        return sentenceAggravatedFlag;
    }

    /**
     * True if aggravating factors were considered during sentencing, false otherwise.
     *
     * @param sentenceAggravatedFlag the sentenceAggravatedFlag to set
     */
    public void setSentenceAggravatedFlag(Boolean sentenceAggravatedFlag) {
        this.sentenceAggravatedFlag = sentenceAggravatedFlag;
    }

    /**
     * Days or parts of days in a week when an intermittent sentence is to be served.
     *
     * @return the intermittentSchedule
     */
    public Set<IntermittentScheduleType> getIntermittentSchedule() {
        return intermittentSchedule;
    }

    /**
     * Days or parts of days in a week when an intermittent sentence is to be served.
     *
     * @param intermittentSchedule the intermittentSchedule to set
     */
    public void setIntermittentSchedule(Set<IntermittentScheduleType> intermittentSchedule) {
        this.intermittentSchedule = intermittentSchedule;
    }
}
