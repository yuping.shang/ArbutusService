package syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;
import syscon.arbutus.product.services.core.contract.dto.StampType;

/**
 * AdjustmentType for Sentence Adjustment
 * User: yshang
 * Date: 03/10/13
 * Time: 9:32 AM
 */
@ArbutusConstraint(constraints = { "startDate <= endDate" })
public class AdjustmentType implements Serializable, Comparable<AdjustmentType> {

    private static final long serialVersionUID = -586673202979744069L;

    /**
     * Identification of Adjustment
     */
    private Long adjustmentId;

    /**
     * adjustmentClassification: SYSG, MANUAL
     */
    private String adjustmentClassification;

    /**
     * adjustmentType:
     * GT, JT, LGT, WORKCDT, LWRKCRD, PRGCDT, ESCP, OICDBT, WKNCDT, STATCDT
     */
    @NotNull
    private String adjustmentType;

    /**
     * adjustmentFunction: CRE, DEB
     */
    private String adjustmentFunction;

    /**
     * adjustmentStatus: INCL, EXCL, PEND
     */
    private String adjustmentStatus;

    private String applicationType;

    /**
     * startDate or fromDate
     */
    private Date startDate;
    /**
     * endDate or toDate
     */
    private Date endDate;

    /**
     * postedDate
     */
    private Date postedDate;

    /**
     * adjustment, the days to be adjusted
     */
    private Long adjustment;

    private Set<Long> sentenceIds;

    private String comment;
    private Long byStaffId;

    private StampType stamp;

    public AdjustmentType() {
    }

    /**
     * Identification of Adjustment, not required when create, required when update.
     *
     * @return Long
     */
    public Long getAdjustmentId() {
        return adjustmentId;
    }

    /**
     * Identification of Adjustment, not required when create, required when update.
     *
     * @param adjustmentId Long
     */
    public void setAdjustmentId(Long adjustmentId) {
        this.adjustmentId = adjustmentId;
    }

    /**
     * getAdjustmentClassification Get Adjustment Classification
     * <p>Reference code, SYSG, MANUAL, of Reference Set AdjustmentClassification
     * <p>Optional</p>
     *
     * @return String
     */
    public String getAdjustmentClassification() {
        return adjustmentClassification;
    }

    /**
     * setAdjustmentClassification Set Adjustment Classification
     * <p>Reference code, SYSG, MANUAL, of Reference Set AdjustmentClassification
     * <p>Optional</p>
     *
     * @param adjustmentClassification String
     */
    public void setAdjustmentClassification(String adjustmentClassification) {
        this.adjustmentClassification = adjustmentClassification;
    }

    /**
     * getAdjustmentType Get Adjustment Type
     * <p>Reference Code, GT, JT, LGT, WORKCDT, LWRKCRD, PRGCDT, ESCP, OICDBT, WKNCDT, STATCDT, of Reference Set AdjustmentType
     * <p>Required</p>
     *
     * @return String
     */
    public String getAdjustmentType() {
        return adjustmentType;
    }

    /**
     * setAdjustmentType Set Adjustment Type
     * <p>Reference Code, GT, JT, LGT, WORKCDT, LWRKCRD, PRGCDT, ESCP, OICDBT, WKNCDT, STATCDT, of Reference Set AdjustmentType
     * <p>Required</p>
     *
     * @param adjustmentType
     */
    public void setAdjustmentType(String adjustmentType) {
        this.adjustmentType = adjustmentType;
    }

    /**
     * getAdjustmentFunction Get Adjustment Function
     * <p>Reference Code, CRE, DEB, of Reference Set AdjustmentFunction
     * <p>Optional</p>
     *
     * @return String
     */
    public String getAdjustmentFunction() {
        return adjustmentFunction;
    }

    /**
     * setAdjustmentFunction Set Adjustment Function
     * <p>Reference Code, CRE, DEB, of Reference Set AdjustmentFunction
     * <p>Optional</p>
     *
     * @param adjustmentFunction
     */
    public void setAdjustmentFunction(String adjustmentFunction) {
        this.adjustmentFunction = adjustmentFunction;
    }

    /**
     * getAdjustmentStatus Get Adjustment Status
     * <p>Reference Code, INCL, EXCL, PEND, of Reference Set AdjustmentStatus
     * <p>Optional</p>
     *
     * @return String
     */
    public String getAdjustmentStatus() {
        return adjustmentStatus;
    }

    /**
     * setAdjustmentStatus Set Adjustment Status
     * <p>Reference Code, INCL, EXCL, PEND, of Reference Set AdjustmentStatus
     * <p>Optional</p>
     *
     * @param adjustmentStatus
     */
    public void setAdjustmentStatus(String adjustmentStatus) {
        this.adjustmentStatus = adjustmentStatus;
    }

    /**
     * getApplicationType Get Application Type
     * <p>Reference Code, ASENT, AGGSENT, BOOKLEV, of Reference Set ApplicationType
     * <p>Optional</p>
     *
     * @return
     */
    public String getApplicationType() {
        return applicationType;
    }

    /**
     * setApplicationType Set Application Type
     * <p>Reference Code, ASENT, AGGSENT, BOOKLEV, of Reference Set ApplicationType
     * <p>Optional</p>
     *
     * @param applicationType
     */
    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    /**
     * getStartDate Get Start Date
     * <p>Start Date of the adjustment</p>
     * <p>Optional</p>
     *
     * @return Date
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * setStartDate Set Start Date
     * <p>Start Date of the adjustment</p>
     * <p>Optional</p>
     *
     * @param startDate
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * getEndDate Get End Date
     * <p>End Date of the adjustment</p>
     * <p>Optional</p>
     *
     * @return Date
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * setEndDate Set End Date
     * <p>End Date of the adjustment</p>
     * <p>Optional</p>
     *
     * @param endDate
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * getPostedDate Get Posted Date
     * <p>Optional</p>
     *
     * @return
     */
    public Date getPostedDate() {
        return postedDate;
    }

    /**
     * setPostedDate Set Posted Date
     * <p>Optional</p>
     *
     * @param postedDate
     */
    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    /**
     * getAdjustment Get Adjustment
     * <p>Optional</p>
     *
     * @return Long
     */
    public Long getAdjustment() {
        return adjustment;
    }

    /**
     * setAdjustment Set Adjustment
     * <p>Optional</p>
     *
     * @param adjustment
     */
    public void setAdjustment(Long adjustment) {
        this.adjustment = adjustment;
    }

    /**
     * getSentenceIds Get Sentence Ids
     * <p>optional</p>
     *
     * @return Set&lt;Long>
     */
    public Set<Long> getSentenceIds() {
        if (sentenceIds == null) {
			sentenceIds = new HashSet<>();
		}
        return sentenceIds;
    }

    /**
     * setSentenceIds Set Sentence Ids
     * <p>optional</p>
     *
     * @param sentenceIds
     */
    public void setSentenceIds(Set<Long> sentenceIds) {
        this.sentenceIds = sentenceIds;
    }

    /**
     * getComment Get Comment
     * <p>Optional</p>
     *
     * @return String
     */
    public String getComment() {
        return comment;
    }

    /**
     * setComment Set Comment
     * <p>Optional</p>
     *
     * @param comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * getByStaffId getByStaffId
     * <p>Optional</p>
     *
     * @return Long
     */
    public Long getByStaffId() {
        return byStaffId;
    }

    /**
     * setByStaffId Set By Staff Id
     * <p>Optional</p>
     *
     * @param byStaffId
     */
    public void setByStaffId(Long byStaffId) {
        this.byStaffId = byStaffId;
    }

    /**
     * getStamp Get Stamp
     * <p>System generated</p>
     *
     * @return StampType
     */
    public StampType getStamp() {
        return stamp;
    }

    /**
     * setStamp Set Stamp
     * <p>System generated</p>
     *
     * @param stamp
     */
    public void setStamp(StampType stamp) {
        this.stamp = stamp;
    }

    @Override
    public int compareTo(AdjustmentType o) {
        if (adjustmentType == null || adjustmentType.isEmpty()) {
			return 0;
		}
        return this.getAdjustmentType().compareTo(o.getAdjustmentType());
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((adjustmentClassification == null) ? 0 : adjustmentClassification.hashCode());
        result = prime * result + ((adjustmentFunction == null) ? 0 : adjustmentFunction.hashCode());
        result = prime * result + ((adjustmentId == null) ? 0 : adjustmentId.hashCode());
        result = prime * result + ((adjustmentStatus == null) ? 0 : adjustmentStatus.hashCode());
        result = prime * result + ((adjustmentType == null) ? 0 : adjustmentType.hashCode());
        result = prime * result + ((applicationType == null) ? 0 : applicationType.hashCode());
        result = prime * result + ((sentenceIds == null) ? 0 : sentenceIds.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        AdjustmentType other = (AdjustmentType) obj;
        if (adjustmentClassification == null) {
            if (other.adjustmentClassification != null) {
				return false;
			}
        } else if (!adjustmentClassification.equals(other.adjustmentClassification)) {
			return false;
		}
        if (adjustmentFunction == null) {
            if (other.adjustmentFunction != null) {
				return false;
			}
        } else if (!adjustmentFunction.equals(other.adjustmentFunction)) {
			return false;
		}
        if (adjustmentId == null) {
            if (other.adjustmentId != null) {
				return false;
			}
        } else if (!adjustmentId.equals(other.adjustmentId)) {
			return false;
		}
        if (adjustmentStatus == null) {
            if (other.adjustmentStatus != null) {
				return false;
			}
        } else if (!adjustmentStatus.equals(other.adjustmentStatus)) {
			return false;
		}
        if (adjustmentType == null) {
            if (other.adjustmentType != null) {
				return false;
			}
        } else if (!adjustmentType.equals(other.adjustmentType)) {
			return false;
		}
        if (applicationType == null) {
            if (other.applicationType != null) {
				return false;
			}
        } else if (!applicationType.equals(other.applicationType)) {
			return false;
		}
        if (sentenceIds == null) {
            if (other.sentenceIds != null) {
				return false;
			}
        } else if (!sentenceIds.equals(other.sentenceIds)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "AdjustmentType{" +
                "adjustmentId=" + adjustmentId +
                ", adjustmentClassification='" + adjustmentClassification + '\'' +
                ", adjustmentType='" + adjustmentType + '\'' +
                ", adjustmentFunction='" + adjustmentFunction + '\'' +
                ", adjustmentStatus='" + adjustmentStatus + '\'' +
                ", applicationType='" + applicationType + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", postedDate=" + postedDate +
                ", adjustment=" + adjustment +
                ", sentenceIds=" + sentenceIds +
                ", comment='" + comment + '\'' +
                ", byStaffId=" + byStaffId +
                '}';
    }
}
