package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * NotifyAgentContactEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_ORDNotiAgnCnt 'Notification Agent Contact Association table for Order Sentence module of Legal service'
 * @since December 20, 2012
 */
@Audited
@Entity
@Table(name = "LEG_ORDNotiAgnCnt")
public class NotifyAgentContactEntity implements Serializable {

    private static final long serialVersionUID = 2042194877454256775L;

    /**
     * @DbComment notiAgnContactId 'Unique identification of a notification agent contact association instance.'
     */
    @Id
    @Column(name = "notiAgnContactId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDNOTIAGNCNT_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDNOTIAGNCNT_ID", sequenceName = "SEQ_LEG_ORDNOTIAGNCNT_ID", allocationSize = 1)
    private Long notifyAgentContactId;

    /**
     * @DbComment agentContact 'Notification agent contact'
     */
    @Column(name = "agentContact", nullable = true, length = 1024)
    private String agentContact;

    /**
     * @DbComment firstName 'First name of the agent'
     */
    @Column(name = "firstName", length = 128, nullable = true)
    private String firstName;

    /**
     * @DbComment lastName 'Last name of the agent'
     */
    @Column(name = "lastName", length = 128, nullable = true)
    private String lastName;

    /**
     * @DbComment address 'Address of the agent'
     */
    @Column(name = "address", length = 512, nullable = true)
    private String address;

    /**
     * @DbComment city 'city'
     */
    @Column(name = "city", length = 128, nullable = true)
    private String city;

    /**
     * @DbComment county 'county'
     */
    @Column(name = "county", length = 128, nullable = true)
    private String county;

    /**
     * @DbComment provinceState 'Province or State'
     */
    @Column(name = "provinceState", length = 128, nullable = true)
    private String provinceState;

    /**
     * @DbComment country 'country'
     */
    @Column(name = "country", length = 128, nullable = true)
    private String country;

    /**
     * @DbComment zipCode 'zipCode'
     */
    @Column(name = "zipCode", length = 64, nullable = true)
    private String zipCode;

    /**
     * @DbComment phone 'Phone number of the agent'
     */
    @Column(name = "phone", length = 64, nullable = true)
    private String phoneNumber;

    /**
     * @DbComment fax 'Fax number of the agent'
     */
    @Column(name = "fax", length = 64, nullable = true)
    private String faxNumber;

    /**
     * @DbComment email 'email of the agent'
     */
    @Column(name = "email", length = 128, nullable = true)
    private String email;

    /**
     * @DbComment idNum 'Identify number of the agent'
     */
    @Column(name = "idNum", length = 128, nullable = true)
    private String identityNumber;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment notificationId 'Unique identification of a notification instance, foreign key to LEG_OrdNotification table'
     */
    @ForeignKey(name = "Fk_LEG_ORDNotiAgnCnt")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "notificationId", nullable = false)
    private NotificationEntity notification;

    /**
     * Constructor
     */
    public NotifyAgentContactEntity() {
        super();
    }

    /**
     * Constructor
     *
     * @param notifyAgentContactId
     * @param agentContact
     * @param stamp
     */
    public NotifyAgentContactEntity(Long notifyAgentContactId, String agentContact, StampEntity stamp) {
        super();
        this.notifyAgentContactId = notifyAgentContactId;
        this.agentContact = agentContact;
        this.stamp = stamp;
    }

    /**
     * @return the notifyAgentContactId
     */
    public Long getNotifyAgentContactId() {
        return notifyAgentContactId;
    }

    /**
     * @param notifyAgentContactId the notifyAgentContactId to set
     */
    public void setNotifyAgentContactId(Long notifyAgentContactId) {
        this.notifyAgentContactId = notifyAgentContactId;
    }

    /**
     * @return the agentContact
     */
    public String getAgentContact() {
        return agentContact;
    }

    /**
     * @param agentContact the agentContact to set
     */
    public void setAgentContact(String agentContact) {
        this.agentContact = agentContact;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getProvinceState() {
        return provinceState;
    }

    public void setProvinceState(String provinceState) {
        this.provinceState = provinceState;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the notification
     */
    public NotificationEntity getNotification() {
        return notification;
    }

    /**
     * @param notification the notification to set
     */
    public void setNotification(NotificationEntity notification) {
        this.notification = notification;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (!(o instanceof NotifyAgentContactEntity)) {
			return false;
		}

        NotifyAgentContactEntity that = (NotifyAgentContactEntity) o;

        if (address != null ? !address.equals(that.address) : that.address != null) {
			return false;
		}
        if (agentContact != null ? !agentContact.equals(that.agentContact) : that.agentContact != null) {
			return false;
		}
        if (city != null ? !city.equals(that.city) : that.city != null) {
			return false;
		}
        if (country != null ? !country.equals(that.country) : that.country != null) {
			return false;
		}
        if (county != null ? !county.equals(that.county) : that.county != null) {
			return false;
		}
        if (email != null ? !email.equals(that.email) : that.email != null) {
			return false;
		}
        if (faxNumber != null ? !faxNumber.equals(that.faxNumber) : that.faxNumber != null) {
			return false;
		}
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) {
			return false;
		}
        if (identityNumber != null ? !identityNumber.equals(that.identityNumber) : that.identityNumber != null) {
			return false;
		}
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) {
			return false;
		}
        if (notifyAgentContactId != null ? !notifyAgentContactId.equals(that.notifyAgentContactId) : that.notifyAgentContactId != null) {
			return false;
		}
        if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) {
			return false;
		}
        if (provinceState != null ? !provinceState.equals(that.provinceState) : that.provinceState != null) {
			return false;
		}
        if (zipCode != null ? !zipCode.equals(that.zipCode) : that.zipCode != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = notifyAgentContactId != null ? notifyAgentContactId.hashCode() : 0;
        result = 31 * result + (agentContact != null ? agentContact.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (county != null ? county.hashCode() : 0);
        result = 31 * result + (provinceState != null ? provinceState.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (zipCode != null ? zipCode.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (faxNumber != null ? faxNumber.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (identityNumber != null ? identityNumber.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NotifyAgentContactEntity{" +
                "notifyAgentContactId=" + notifyAgentContactId +
                ", agentContact='" + agentContact + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", county='" + county + '\'' +
                ", provinceState='" + provinceState + '\'' +
                ", country='" + country + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", faxNumber='" + faxNumber + '\'' +
                ", email='" + email + '\'' +
                ", identityNumber='" + identityNumber + '\'' +
                ", stamp=" + stamp +
                '}';
    }
}
