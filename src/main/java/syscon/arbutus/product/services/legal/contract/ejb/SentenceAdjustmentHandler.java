package syscon.arbutus.product.services.legal.contract.ejb;

import javax.ejb.SessionContext;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.*;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.LinkCodeType;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.AdjustmentConfigType;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.AdjustmentType;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.SentenceAdjustmentSearchType;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.SentenceAdjustmentType;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.legal.realization.persistence.sentenceadjustment.AdjustmentConfigEntity;
import syscon.arbutus.product.services.legal.realization.persistence.sentenceadjustment.AdjustmentEntity;
import syscon.arbutus.product.services.legal.realization.persistence.sentenceadjustment.SentenceAdjustmentEntity;
import syscon.arbutus.product.services.legal.realization.persistence.sentenceadjustment.SentenceAssocEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.realization.util.ReferenceDataHelper;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * Bean of SentenceAdjustment for Sentence Adjustment
 * User: yshang
 * Date: 07/10/13
 * Time: 4:46 PM
 */
public class SentenceAdjustmentHandler {

    private static Logger log = LoggerFactory.getLogger(SentenceAdjustmentHandler.class);

    private SessionContext context;
    private Session session;
    private int searchMaxLimit = 200;

    public SentenceAdjustmentHandler(SessionContext context, Session session) {
        this.context = context;
        this.session = session;
    }

    public SentenceAdjustmentHandler(SessionContext context, Session session, int searchMaxLimit) {
        this.context = context;
        this.session = session;
        this.searchMaxLimit = searchMaxLimit;
    }

    public void setConfiguration(UserContext uc, AdjustmentConfigType config) {
        String functionName = "setConfiguration";
        LogHelper.debug(log, functionName, "begin");
        ValidationHelper.validate(config);
        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);
        AdjustmentConfigEntity newConfigEntity = SentenceAdjustmentHelper.toAdjustmentConfigEntity(config, stamp);
        ValidationHelper.verifyMetaCodes(newConfigEntity, true);

        verifyMetaCodeLinks(uc, newConfigEntity, true);

        AdjustmentConfigEntity entity = getAdjustmentConfigEntity(uc, config.getAdjustmentType());
        if (entity == null) {
            session.save(newConfigEntity);
        } else {
            newConfigEntity.setAdjustConfigId(entity.getAdjustConfigId());
            newConfigEntity.setVersion(entity.getVersion());
            newConfigEntity.setStamp(BeanHelper.getModifyStamp(uc, context, entity.getStamp()));
            session.merge(newConfigEntity);
        }

        LogHelper.debug(log, functionName, "end");
    }

    public AdjustmentConfigType getConfiguration(UserContext uc, String adjustmentType) {
        String functionName = "getConfiguration";
        LogHelper.debug(log, functionName, "begin");
        AdjustmentConfigEntity entity = getAdjustmentConfigEntity(uc, adjustmentType);
        LogHelper.debug(log, functionName, "end");
        return SentenceAdjustmentHelper.toAdjustmentConfigType(entity);
    }

    public List<AdjustmentConfigType> getAllConfiguration(UserContext uc) {
        String functionName = "getAllConfiguration";
        LogHelper.debug(log, functionName, "begin");
        Criteria c = session.createCriteria(AdjustmentConfigEntity.class);
        c.addOrder(Order.asc("adjustmentType"));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<AdjustmentConfigType> ret = new ArrayList<>();
        @SuppressWarnings("unchecked") Iterator<AdjustmentConfigEntity> it = c.list().iterator();
        while (it.hasNext()) {
            ret.add(SentenceAdjustmentHelper.toAdjustmentConfigType(it.next()));
        }
        LogHelper.debug(log, functionName, "end");
        return ret;
    }

    public Long deleteConfiguration(UserContext uc, String adjustmentType) {
        LogHelper.debug(log, "deleteConfiguration", "begin");
        deleteAdjustmentConfigEntity(uc, adjustmentType);
        LogHelper.debug(log, "deleteConfiguration", "end");
        return ReturnCode.Success.returnCode();
    }

    public Long deleteAllConfiguration(UserContext uc) {
        LogHelper.debug(log, "deleteAllConfiguration", "begin");
        deleteAdjustmentConfigEntity(uc, null);
        LogHelper.debug(log, "deleteAllConfiguration", "end");
        return ReturnCode.Success.returnCode();
    }

    public SentenceAdjustmentType create(UserContext uc, SentenceAdjustmentType sentenceAdjustment) {
        String functionName = "create";
        LogHelper.debug(log, functionName, "begin");
        if (sentenceAdjustment != null) {
            if (sentenceAdjustment.getSupervisionId() == null) {
                String message = "Invalid argument: supervisionId is required.";
                LogHelper.error(log, functionName, message);
                throw new InvalidInputException(message);
            }
        }

        SentenceAdjustmentType ret = null;
        Long supervisionId = sentenceAdjustment.getSupervisionId();
        SentenceAdjustmentEntity existedEntity = SentenceAdjustmentHelper.getSentenceAdjustmentEntityBySupervisionId(session, supervisionId);

        if (existedEntity != null) {
            return update(uc, sentenceAdjustment);
        }

        ValidationHelper.validate(sentenceAdjustment);
        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);
        SentenceAdjustmentEntity entity = SentenceAdjustmentHelper.toSentenceAdjustmentEntity(uc, session, null, sentenceAdjustment, stamp, true);

        ValidationHelper.verifyMetaCodes(entity, true);
        SentenceAdjustmentHelper.verifyAdjustment(uc, session, supervisionId, entity.getAdjustments(), true, false);
        session.persist(entity);

        SentenceAdjustmentHelper.storeToHistorySentenceAdjustment(uc, context, session, entity);

        OrderSentenceHelper.setSentenceCalculationEnableFlagBySupervisionId(session, supervisionId, true);

        ret = SentenceAdjustmentHelper.toSentenceAdjustmentType(session, entity);

        LogHelper.debug(log, functionName, "end");
        return ret;
    }

    public SentenceAdjustmentType get(UserContext uc, Long supervisionId) {
        String functionName = "get";
        LogHelper.debug(log, functionName, "begin");
        if (supervisionId == null) {
            LogHelper.debug(log, functionName, "end");
            return null;
        }
        SentenceAdjustmentEntity entity = SentenceAdjustmentHelper.getSentenceAdjustmentEntityBySupervisionId(session, supervisionId);
        SentenceAdjustmentType ret = SentenceAdjustmentHelper.toSentenceAdjustmentType(session, entity);
        LogHelper.debug(log, functionName, "end");
        return ret;
    }

    public List<SentenceAdjustmentType> getSentenceAdjustmentsWithHistory(UserContext uc, Long supervisionId) {
        String functionName = "get";
        LogHelper.debug(log, functionName, "begin");
        if (supervisionId == null) {
            LogHelper.debug(log, functionName, "end");
            return null;
        }
        List<SentenceAdjustmentEntity> entityList = SentenceAdjustmentHelper.getSentenceAdjustmentEntityWithHistory(session, supervisionId);
        List<SentenceAdjustmentType> ret = SentenceAdjustmentHelper.toSentenceAdjustmentTypeList(session, entityList);
        LogHelper.debug(log, functionName, "end");
        return ret;
    }

    public SentenceAdjustmentType update(UserContext uc, SentenceAdjustmentType sentenceAdjustment) {
        String functionName = "update";
        LogHelper.debug(log, functionName, "begin");
        Long supervisionId = sentenceAdjustment.getSupervisionId();
        if (supervisionId == null) {
            String message = "Invalid argument: supervisionId for SentenceAdjustment is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(sentenceAdjustment);
        SentenceAdjustmentEntity previousEntity = SentenceAdjustmentHelper.getSentenceAdjustmentEntityBySupervisionId(session, supervisionId);
        if (previousEntity == null) {
            String message = "Invalid argument: SentenceAdjustment with supervisionId(" + supervisionId + ") to be updated does not exist.";
            LogHelper.error(log, functionName, message);
            throw new DataNotExistException(message);
        }
        StampEntity stamp = BeanHelper.getModifyStamp(uc, context, previousEntity.getStamp());
        Long sentenceAdjustmentId = previousEntity.getSentenceAdjustmentId();

        SentenceAdjustmentEntity entity = SentenceAdjustmentHelper.toSentenceAdjustmentEntity(uc, session, sentenceAdjustmentId, sentenceAdjustment, stamp, true);
        entity.setSentenceAdjustmentId(previousEntity.getSentenceAdjustmentId());
        ValidationHelper.verifyMetaCodes(entity, false);
        entity.setVersion(previousEntity.getVersion());
        Iterator<AdjustmentEntity> it = previousEntity.getAdjustments().iterator();
        while (it.hasNext()) {
            session.delete(it.next());
        }
        previousEntity.getAdjustments().clear();
        SentenceAdjustmentHelper.verifyAdjustment(uc, session, supervisionId, entity.getAdjustments(), true, false);
        entity = (SentenceAdjustmentEntity) session.merge(entity);

        SentenceAdjustmentHelper.storeToHistorySentenceAdjustment(uc, context, session, entity);

        OrderSentenceHelper.setSentenceCalculationEnableFlagBySupervisionId(session, supervisionId, true);

        SentenceAdjustmentType ret = SentenceAdjustmentHelper.toSentenceAdjustmentType(session, entity);

        LogHelper.debug(log, functionName, "end");
        return ret;
    }

    public SentenceAdjustmentType addAdjustment(UserContext uc, Long supervisionId, AdjustmentType adjustment) {
        String functionName = "addAdjustment";
        LogHelper.debug(log, functionName, "begin");
        if (supervisionId == null) {
            String message = "Invalid argument: supervisionId for SentenceAdjustment is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        SentenceAdjustmentEntity sentenceAdjustmentEntity = SentenceAdjustmentHelper.getSentenceAdjustmentEntityBySupervisionId(session, supervisionId);
        if (sentenceAdjustmentEntity == null) {
            String message = "Invalid argument: SentenceAdjustment with supervisionId(" + supervisionId + ") to be updated does not exist.";
            LogHelper.error(log, functionName, message);
            throw new DataNotExistException(message);
        }
        ValidationHelper.validate(adjustment);
        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);
        AdjustmentEntity adjustmentEntity = SentenceAdjustmentHelper.toAdjustmentEntity(uc, session, supervisionId, adjustment, stamp, true);
        ValidationHelper.verifyMetaCodes(adjustmentEntity, true);
        SentenceAdjustmentHelper.verifyAdjustment(uc, session, supervisionId, adjustmentEntity, true, false);
        sentenceAdjustmentEntity.addAdjustment(adjustmentEntity);
        sentenceAdjustmentEntity = (SentenceAdjustmentEntity) session.merge(sentenceAdjustmentEntity);

        SentenceAdjustmentHelper.storeToHistorySentenceAdjustment(uc, context, session, sentenceAdjustmentEntity);

        OrderSentenceHelper.setSentenceCalculationEnableFlagBySupervisionId(session, supervisionId, true);

        SentenceAdjustmentType ret = SentenceAdjustmentHelper.toSentenceAdjustmentType(session, sentenceAdjustmentEntity);
        assert (ret.getAdjustments().size() == sentenceAdjustmentEntity.getAdjustments().size());
        LogHelper.debug(log, functionName, "end");
        return ret;
    }

    public SentenceAdjustmentType addAdjustments(UserContext uc, Long supervisionId, Set<AdjustmentType> adjustments) {
        String functionName = "addAdjustments";
        LogHelper.debug(log, functionName, "begin");
        if (supervisionId == null) {
            String message = "Invalid argument: supervisionId for SentenceAdjustment is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);
        SentenceAdjustmentEntity sentenceAdjustmentEntity = SentenceAdjustmentHelper.getSentenceAdjustmentEntityBySupervisionId(session, supervisionId);
        if (sentenceAdjustmentEntity == null) {
            String message = "Invalid argument: SentenceAdjustment with supervisionId(" + supervisionId + ") to be updated does not exist.";
            LogHelper.error(log, functionName, message);
            throw new DataNotExistException(message);
        }

        if (!BeanHelper.isEmpty(adjustments)) {
            SentenceAdjustmentHelper.verifyAdjustment(uc, session, supervisionId,
                    SentenceAdjustmentHelper.toAdjustmentEntitySet(uc, session, supervisionId, adjustments, stamp, true), true, false);
            for (AdjustmentType adjustment : adjustments) {
                ValidationHelper.validate(adjustment);
                AdjustmentEntity adjustmentEntity = SentenceAdjustmentHelper.toAdjustmentEntity(uc, session, supervisionId, adjustment, stamp, true);
                ValidationHelper.verifyMetaCodes(adjustmentEntity, true);
                sentenceAdjustmentEntity.addAdjustment(adjustmentEntity);
            }
        }

        sentenceAdjustmentEntity = (SentenceAdjustmentEntity) session.merge(sentenceAdjustmentEntity);

        SentenceAdjustmentHelper.storeToHistorySentenceAdjustment(uc, context, session, sentenceAdjustmentEntity);

        OrderSentenceHelper.setSentenceCalculationEnableFlagBySupervisionId(session, supervisionId, true);

        SentenceAdjustmentType ret = SentenceAdjustmentHelper.toSentenceAdjustmentType(session, sentenceAdjustmentEntity);
        LogHelper.debug(log, functionName, "end");

        return ret;

    }

    public SentenceAdjustmentType removeAdjustment(UserContext uc, Long supervisionId, AdjustmentType adjustment) {
        String functionName = "removeAdjustment";
        LogHelper.debug(log, functionName, "begin");
        if (supervisionId == null) {
            String message = "Invalid argument: supervisionId of SentenceAdjustmentType is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        SentenceAdjustmentEntity sentenceAdjustmentEntity = SentenceAdjustmentHelper.getSentenceAdjustmentEntityBySupervisionId(session, supervisionId);
        if (sentenceAdjustmentEntity == null) {
            String message = "Invalid argument: SentenceAdjustment with supervisionId(" + supervisionId + ") to be updated does not exist.";
            LogHelper.error(log, functionName, message);
            throw new DataNotExistException(message);
        }
        ValidationHelper.validate(adjustment);
        Long adjustmentId = adjustment.getAdjustmentId();
        if (adjustmentId == null) {
            String message = "Invalid argument: adjustmentId of AdjustmentType is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        AdjustmentEntity adjustmentEntity = BeanHelper.findEntity(session, AdjustmentEntity.class, adjustmentId);
        SentenceAdjustmentHelper.verifyAdjustment(uc, session, supervisionId, adjustmentEntity, true, true);
        sentenceAdjustmentEntity.getAdjustments().remove(adjustmentEntity);
        session.delete(adjustmentEntity);

        session.update(sentenceAdjustmentEntity);

        OrderSentenceHelper.setSentenceCalculationEnableFlagBySupervisionId(session, supervisionId, true);

        SentenceAdjustmentType ret = SentenceAdjustmentHelper.toSentenceAdjustmentType(session, sentenceAdjustmentEntity);
        LogHelper.debug(log, functionName, "end");

        return ret;
    }

    public SentenceAdjustmentType removeAdjustments(UserContext uc, Long supervisionId, Set<AdjustmentType> adjustments) {
        String functionName = "removeAdjustments";
        LogHelper.debug(log, functionName, "begin");
        if (supervisionId == null) {
            String message = "Invalid argument: supervisionId of SentenceAdjustmentType is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        SentenceAdjustmentEntity sentenceAdjustmentEntity = SentenceAdjustmentHelper.getSentenceAdjustmentEntityBySupervisionId(session, supervisionId);
        if (sentenceAdjustmentEntity != null) {
            if (!BeanHelper.isEmpty(adjustments)) {
                for (AdjustmentType adjustment : adjustments) {
                    Long adjustmentId = adjustment.getAdjustmentId();
                    if (adjustmentId == null) {
                        String message = "Invalid argument: adjustmentId of AdjustmentType is required.";
                        LogHelper.error(log, functionName, message);
                        throw new InvalidInputException(message);
                    }
                    AdjustmentEntity adjustmentEntity = BeanHelper.findEntity(session, AdjustmentEntity.class, adjustmentId);
                    SentenceAdjustmentHelper.verifyAdjustment(uc, session, supervisionId, adjustmentEntity, true, true);
                    sentenceAdjustmentEntity.getAdjustments().remove(adjustmentEntity);
                    session.delete(adjustmentEntity);
                }
            }

            if (sentenceAdjustmentEntity.getAdjustments() != null && sentenceAdjustmentEntity.getAdjustments().size() > 0) {
                session.update(sentenceAdjustmentEntity);
            } else {
                session.delete(sentenceAdjustmentEntity);
            }

            OrderSentenceHelper.setSentenceCalculationEnableFlagBySupervisionId(session, supervisionId, true);
        }

        SentenceAdjustmentType ret = SentenceAdjustmentHelper.toSentenceAdjustmentType(session, sentenceAdjustmentEntity);
        LogHelper.debug(log, functionName, "end");
        return ret;
    }

    public Long getTotalAdjustment(UserContext uc, Long supervisionId, Set<Long> caseInfoIds) {
        String functionName = "getTotalAdjustment";
        LogHelper.debug(log, functionName, "begin");
        if (supervisionId == null) {
            String message = "Invalid argument: supervisionId for SentenceAdjustment is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        SentenceAdjustmentEntity entity = SentenceAdjustmentHelper.getSentenceAdjustmentEntityBySupervisionId(session, supervisionId);

        long totalAdjustment = 0L;
        Set<AdjustmentEntity> adjustmentEntitySet = entity.getAdjustments();
        if (adjustmentEntitySet != null) {
            for (AdjustmentEntity adjustmentEntity : adjustmentEntitySet) {
                Long adjustment = adjustmentEntity.getAdjustment();
                if (adjustment == null) {
					adjustment = 0L;
				}
                if (ADJUSTMENT_FUNCTION.CRE.name().equalsIgnoreCase(adjustmentEntity.getAdjustmentFunction())) {
					totalAdjustment = totalAdjustment + adjustment;
				} else if (ADJUSTMENT_FUNCTION.DEB.name().equals(adjustmentEntity.getAdjustmentFunction())) {
					totalAdjustment = totalAdjustment - adjustment;
				}
            }
        }

        return totalAdjustment;
    }

    public List<SentenceAdjustmentType> search(UserContext uc, Long supervisionId, SentenceAdjustmentSearchType search, Long startIndex, Long resultSize,
            String resultOrder) {
        String functionName = "search";
        LogHelper.debug(log, functionName, "begin");

        if (supervisionId == null) {
            ValidationHelper.validateSearchType(search);
        }

        if (resultSize == null || resultSize > searchMaxLimit) {
            resultSize = new Long(searchMaxLimit);
        }

        Criteria c = session.createCriteria(SentenceAdjustmentEntity.class);
        if (supervisionId != null) {
            c.add(Restrictions.eq("supervisionId", supervisionId));
        }

        if (search != null) {
            DetachedCriteria adjustmentsCriteria = DetachedCriteria.forClass(AdjustmentEntity.class);
            adjustmentsCriteria.setProjection(Projections.property("sentenceAdjust"));

            if (supervisionId != null) {
                adjustmentsCriteria.add(Restrictions.eq("supervisionId", supervisionId));
            }

            String adjustmentType = search.getAdjustmentType();
            if (!BeanHelper.isEmpty(adjustmentType)) {
                adjustmentsCriteria.add(Restrictions.eq("adjustmentType", adjustmentType));
            }

            String adjustmentClassification = search.getAdjustmentClassification();
            if (!BeanHelper.isEmpty(adjustmentClassification)) {
                adjustmentsCriteria.add(Restrictions.eq("adjustmentClassification", adjustmentClassification));
            }

            String adjustmentFunction = search.getAdjustmentFunction();
            if (!BeanHelper.isEmpty(adjustmentFunction)) {
                adjustmentsCriteria.add(Restrictions.eq("adjustmentFunction", adjustmentFunction));
            }

            String adjustmentStatus = search.getAdjustmentStatus();
            if (!BeanHelper.isEmpty(adjustmentStatus)) {
                adjustmentsCriteria.add(Restrictions.eq("adjustmentStatus", adjustmentStatus));
            }

            Long fromAdjustment = search.getFromAdjustment();
            if (fromAdjustment != null) {
                adjustmentsCriteria.add(Restrictions.ge("adjustment", fromAdjustment));
            }
            Long toAdjustment = search.getToAdjustment();
            if (toAdjustment != null) {
                adjustmentsCriteria.add(Restrictions.le("adjustment", toAdjustment));
            }

            Date fromStartDate = search.getFromStartDate();
            if (fromStartDate != null) {
                adjustmentsCriteria.add(Restrictions.ge("startDate", fromStartDate));
            }
            Date toStartDate = search.getToStartDate();
            if (toStartDate != null) {
                adjustmentsCriteria.add(Restrictions.le("startDate", toStartDate));
            }

            Date fromEndDate = search.getFromEndDate();
            if (fromEndDate != null) {
                adjustmentsCriteria.add(Restrictions.ge("endDate", fromEndDate));
            }
            Date toEndDate = search.getToEndDate();
            if (toEndDate != null) {
                adjustmentsCriteria.add(Restrictions.le("endDate", toEndDate));
            }

            Date fromPostedDate = search.getFromPostedDate();
            if (fromPostedDate != null) {
                adjustmentsCriteria.add(Restrictions.ge("postedDate", fromPostedDate));
            }
            Date toPostedDate = search.getToPostedDate();
            if (toPostedDate != null) {
                adjustmentsCriteria.add(Restrictions.le("postedDate", toPostedDate));
            }

            Long byStaffId = search.getByStaffId();
            if (byStaffId != null) {
                adjustmentsCriteria.add(Restrictions.eq("byStaffId", byStaffId));
            }

            Long sentenceId = search.getSentenceId();
            if (sentenceId != null) {
                adjustmentsCriteria.createAlias("sentences", "sent");
                adjustmentsCriteria.add(Restrictions.eq("sent.toIdentifier", sentenceId));
            }
            c.add(Subqueries.propertyIn("sentenceAdjustmentId", adjustmentsCriteria));
        }

        // Long totalSize = (Long) c.setProjection(Projections.countDistinct("supervisionId")).uniqueResult();

        c.setProjection(null);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        //Pagination
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            c.setFirstResult(startIndex.intValue());
            c.setMaxResults(resultSize.intValue());
        }

        //Result order
        if (resultOrder != null && resultOrder.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(resultOrder);
            for (Order ord : ords) {
                c.addOrder(ord);
            }
        }

        List<SentenceAdjustmentType> ret = new ArrayList<>();

        @SuppressWarnings("unchecked") List<SentenceAdjustmentEntity> sentenceAdjustmentEntityList = c.list();
        ret = SentenceAdjustmentHelper.toSentenceAdjustmentTypeList(session, sentenceAdjustmentEntityList);

        LogHelper.debug(log, functionName, "end");
        return ret;
    }

    public Set<AdjustmentType> getAdjustmentstBySupervisionIdAndSentenceId(Session session, Long supervisionId, Long sentenceId) {
        Criteria criteria = session.createCriteria(AdjustmentEntity.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        if (supervisionId != null) {
            criteria.add(Restrictions.eq("supervisionId", supervisionId));
        }

        if (sentenceId != null) {
            criteria.createAlias("sentences", "sent");
            criteria.add(Restrictions.eq("sent.toIdentifier", sentenceId));
        }
        @SuppressWarnings("unchecked") List<AdjustmentEntity> entities = criteria.list();
        Set<AdjustmentType> ret = SentenceAdjustmentHelper.toAdjustmentTypeSet(session, new HashSet<>(entities));
        return ret;
    }

    public void removeAdjustmentstBySentenceId(UserContext uc, Session session, Long sentenceId) {
        Set<AdjustmentEntity> adjustmentEntitySet = getAdjustmentEntitySetBySentenctId(session, sentenceId);
        Set<Long> supervisionIDs = new HashSet<>();
        Iterator<AdjustmentEntity> it = adjustmentEntitySet.iterator();
        while (it.hasNext()) {
            AdjustmentEntity adjustmentEntity = it.next();
            supervisionIDs.add(adjustmentEntity.getSupervisionId());
            Set<SentenceAssocEntity> sentenceDeletionSet = new HashSet<>();
            Iterator<SentenceAssocEntity> sentAssocIt = adjustmentEntity.getSentences().iterator();
            while (sentAssocIt.hasNext()) {
                SentenceAssocEntity sentenceAssocEntity = sentAssocIt.next();
                if (sentenceAssocEntity.getToIdentifier().longValue() == sentenceId.longValue()) {
                    sentenceDeletionSet.add(sentenceAssocEntity);
                    sentAssocIt.remove();
                }
            }
            adjustmentEntity.getSentences().removeAll(sentenceDeletionSet);
            session.update(adjustmentEntity);
        }
        session.flush();
        session.clear();
        supervisionIDs.remove(null);
        Map<Long, Boolean> map = new HashMap<>();
        for (Long supervisionId : supervisionIDs) {
            SentenceAdjustmentType sentenceAdjustment = get(uc, supervisionId);
            Set<AdjustmentType> adjustmentSet = sentenceAdjustment.getAdjustments();
            for (AdjustmentType adjustment : adjustmentSet) {
                //if (adjustment.getAdjustmentClassification().equalsIgnoreCase(ADJUSTMENT_CLASSIFICATION.MANUAL.name())) {
                if (adjustment.getSentenceIds() != null) {
					adjustment.getSentenceIds().remove(null);
				}
                if (adjustment.getSentenceIds() == null || (adjustment.getSentenceIds() != null && adjustment.getSentenceIds().size() == 0)) {
                    map.put(supervisionId, Boolean.TRUE);
                    this.removeAdjustment(uc, supervisionId, adjustment);
                } else {
                    map.put(supervisionId, Boolean.FALSE);
                }
                //}
            }
        }
        session.flush();
        session.clear();
        for (Long supervisionId : map.keySet()) {
            if (Boolean.TRUE.equals(map.get(supervisionId))) {
                this.delete(uc, supervisionId);
            }
        }
        session.flush();
        session.clear();
    }

    @SuppressWarnings("unchecked")
    private Set<AdjustmentEntity> getAdjustmentEntitySetBySentenctId(Session session, Long sentenceId) {
        Criteria adjustmentCriteria = session.createCriteria(AdjustmentEntity.class);
        adjustmentCriteria.createCriteria("sentences", "sent");
        adjustmentCriteria.add(Restrictions.eq("sent.toIdentifier", sentenceId));
        adjustmentCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return new HashSet<AdjustmentEntity>(adjustmentCriteria.list());
    }

    public Long delete(UserContext uc, Long supervisionId) {
        String functionName = "delete";
        LogHelper.debug(log, functionName, "begin");
        if (supervisionId == null) {
            LogHelper.debug(log, functionName, "end");
            return null;
        }
        SentenceAdjustmentEntity entity = SentenceAdjustmentHelper.getSentenceAdjustmentEntityBySupervisionId(session, supervisionId);
        session.delete(entity);
        OrderSentenceHelper.setSentenceCalculationEnableFlagBySupervisionId(session, supervisionId, true);
        LogHelper.debug(log, functionName, "end");
        return ReturnCode.Success.returnCode();
    }

    public Long deleteAllSentenceAdjustments(UserContext uc) {
        session.createSQLQuery("UPDATE LEG_SentAdjust SET flag = 1").executeUpdate();
        session.createSQLQuery("UPDATE LEG_SentAdjmnt SET flag = 1").executeUpdate();
        session.createQuery("delete SentenceAssocEntity").executeUpdate();
        session.createQuery("delete AdjustmentEntity").executeUpdate();
        session.createQuery("delete SentenceAdjustmentEntity").executeUpdate();
        session.createQuery("delete SentenceAssocHistEntity").executeUpdate();
        session.createQuery("delete AdjustmentHistEntity").executeUpdate();
        session.createQuery("delete SentenceAdjustmentHistEntity").executeUpdate();
        session.flush();
        session.clear();
        return ReturnCode.Success.returnCode();
    }

    private void deleteAdjustmentConfigEntity(UserContext uc, String adjustmentType) {
        if (adjustmentType == null) {
            session.createQuery("delete AdjustmentConfigEntity").executeUpdate();
            session.flush();
            session.clear();
            return;
        }
        Criteria c = session.createCriteria(AdjustmentConfigEntity.class);
        c.add(Restrictions.eq("adjustmentType", adjustmentType));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        @SuppressWarnings("unchecked") Iterator<AdjustmentConfigEntity> it = c.list().iterator();
        while (it.hasNext()) {
            session.delete(it.next());
        }
    }

    private AdjustmentConfigEntity getAdjustmentConfigEntity(UserContext uc, String adjustmentType) {
        if (BeanHelper.isEmpty(adjustmentType)) {
			return null;
		}

        Criteria c = session.createCriteria(AdjustmentConfigEntity.class);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        c.setMaxResults(1);
        c.add(Restrictions.eq("adjustmentType", adjustmentType));
        return (AdjustmentConfigEntity) c.uniqueResult();
    }

    private void verifyMetaCodeLinks(UserContext uc, AdjustmentConfigEntity config, boolean bActive) {
        String functionName = "verifyMetaCodeLinks";
        CodeType adjustmentType = new CodeType(MetaSet.ADJUSTMENT_TYPE.toUpperCase(), config.getAdjustmentType());
        List<LinkCodeType> linkCodeTypeList = ReferenceDataHelper.getLinks(uc, session, adjustmentType, bActive);
        if (linkCodeTypeList == null || linkCodeTypeList.size() == 0) {
            String message = "CodeType " + adjustmentType + " does not exist.";
            LogHelper.error(log, functionName, message);
            throw new DataNotExistException(message);
        }
        Set<CodeType> linkCodeTypeSet = new HashSet<>();
        for (LinkCodeType linkCodeType : linkCodeTypeList) {
            linkCodeTypeSet.add(new CodeType(linkCodeType.getLinkedReferenceSet(), linkCodeType.getLinkedReferenceCode()));
            linkCodeTypeSet.add(new CodeType(linkCodeType.getReferenceSet(), linkCodeType.getReferenceCode()));
        }

        CodeType classification = new CodeType(MetaSet.ADJUSTMENT_CLASSIFICATION.toUpperCase(), config.getClassification());
        if (!linkCodeTypeSet.contains(classification)) {
            String message = "The link of " + classification + " and " + adjustmentType + " does not exist.";
            LogHelper.error(log, functionName, message);
            throw new DataNotExistException(message);
        }

        CodeType adjustmentFunction = new CodeType(MetaSet.ADJUSTMENT_FUNCTION.toUpperCase(), config.getAdjustmentFunction());
        if (!linkCodeTypeSet.contains(adjustmentFunction)) {
            String message = "The link of " + adjustmentFunction + " and " + adjustmentType + " does not exist.";
            LogHelper.error(log, functionName, message);
            throw new DataNotExistException(message);
        }

        CodeType adjustmentStatus = new CodeType(MetaSet.ADJUSTMENT_STATUS.toUpperCase(), config.getAdjustmentStatus());
        if (!linkCodeTypeSet.contains(adjustmentStatus)) {
            String message = "The link of " + adjustmentStatus + " and " + adjustmentType + " does not exist.";
            LogHelper.error(log, functionName, message);
            throw new DataNotExistException(message);
        }

        CodeType applicationType = new CodeType(MetaSet.APPLICATION_TYPE.toUpperCase(), config.getApplicationType());
        if (!linkCodeTypeSet.contains(applicationType)) {
            String message = "The link of " + applicationType + " and " + adjustmentType + " does not exist.";
            LogHelper.error(log, functionName, message);
            throw new DataNotExistException(message);
        }
    }

    static enum ADJUSTMENT_CLASSIFICATION {SYSG, MANUAL}

    static enum ADJUSTMENT_FUNCTION {CRE, DEB}

    static enum ADJUSTMENT_FLAG {YES, NO}

    static enum ADJUSTMENT_TYPE {GT, JT}

    static enum ADJUSTMENT_STATUS {INCL, EXCL, PEND}

    static enum ADJUSTMENT_APPLICATION {ASENT, AGGSENT, BOOKLEV}

}
