package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

/**
 * TermType for Condition of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 27, 2012
 */
@ArbutusConstraint(constraints = "termStartDate <= termEndDate")
public class TermType implements Serializable {

    private static final long serialVersionUID = -2413229681411300455L;

    @NotNull
    private String termCategory;
    @NotNull
    private String termName;
    @NotNull
    private String termType;
    @NotNull
    private String termStatus;

    private Long termSequenceNo;

    private Date termStartDate;

    private Date termEndDate;

    private Long years;

    private Long months;

    private Long weeks;

    private Long days;

    private Long hours;

    /**
     * Constructor
     */
    public TermType() {
        super();
    }

    /**
     * Constructor
     *
     * @param termCategory   String, Required. The category of term (Custodial, Community).
     * @param termName       String, Required. The name of the term; Min, Max etc..
     * @param termType       Code, Required. The type of term (Duration, Intermittent, Suspended etc.). This will be used by the sentence calculation module.
     * @param termStatus     String, Required. The status of a term, will be used by sentence calculation logic. Pending/Included/Excluded. Only ‘Included’ terms will participate in sentence calculation.
     * @param termSequenceNo Long, Optional. The order of execution of the term.
     * @param termStartDate  Date, Optional. The start date for the term.
     * @param termEndDate    Date, Optional. The end date for the term.
     * @param years          Long, Optional. No. of years a sentence is to be served. Used only for duration and intermittent type.
     * @param months         Long, Optional. No. of months a sentence is to be served. Used only for duration and intermittent type.
     * @param weeks          Long, Optional. No. of weeks a sentence is to be served. Used only for duration and intermittent type.
     * @param days           Long, Optional. No. of days a sentence is to be served. Used only for duration and intermittent type.
     * @param hours          Long, Optional. No. of hours a sentence is to be served. Used only for duration and intermittent type.
     */
    public TermType(@NotNull String termCategory, @NotNull String termName, @NotNull String termType, @NotNull String termStatus, Long termSequenceNo, Date termStartDate,
            Date termEndDate, Long years, Long months, Long weeks, Long days, Long hours) {
        super();
        this.termCategory = termCategory;
        this.termName = termName;
        this.termType = termType;
        this.termStatus = termStatus;
        this.termSequenceNo = termSequenceNo;
        this.termStartDate = termStartDate;
        this.termEndDate = termEndDate;
        this.years = years;
        this.months = months;
        this.weeks = weeks;
        this.days = days;
        this.hours = hours;
    }

    /**
     * Get Term Category
     * -- The category of term (Custodial, Community)
     *
     * @return the termCategory
     */
    public String getTermCategory() {
        return termCategory;
    }

    /**
     * Set Term Category
     * -- The category of term (Custodial, Community)
     *
     * @param termCategory the termCategory to set
     */
    public void setTermCategory(String termCategory) {
        this.termCategory = termCategory;
    }

    /**
     * Get Term Name
     * -- The name of the term; Min, Max etc..
     *
     * @return the termName
     */
    public String getTermName() {
        return termName;
    }

    /**
     * Set Term Name
     * -- -- The name of the term; Min, Max etc..
     *
     * @param termName the termName to set
     */
    public void setTermName(String termName) {
        this.termName = termName;
    }

    /**
     * Get Term Type
     * -- The type of term (Duration, Intermittent, Suspended etc.).
     * This will be used by the sentence calculation module.
     *
     * @return the termType
     */
    public String getTermType() {
        return termType;
    }

    /**
     * Set Term Type
     * -- The type of term (Duration, Intermittent, Suspended etc.).
     * This will be used by the sentence calculation module.
     *
     * @param termType the termType to set
     */
    public void setTermType(String termType) {
        this.termType = termType;
    }

    /**
     * Get Term Status
     * -- The status of a term, will be used by sentence calculation logic
     * Pending/Included/Excluded.
     * Only ‘Included’ terms will participate in sentence calculation.
     *
     * @return the termStatus
     */
    public String getTermStatus() {
        return termStatus;
    }

    /**
     * Set Term Status
     * -- The status of a term, will be used by sentence calculation logic
     * Pending/Included/Excluded.
     * Only ‘Included’ terms will participate in sentence calculation.
     *
     * @param termStatus the termStatus to set
     */
    public void setTermStatus(String termStatus) {
        this.termStatus = termStatus;
    }

    /**
     * Get Term Sequence Number
     * -- The order of execution of the term
     *
     * @return the termSequenceNo
     */
    public Long getTermSequenceNo() {
        return termSequenceNo;
    }

    /**
     * Set Term Sequence Number
     * -- The order of execution of the term
     *
     * @param termSequenceNo the termSequenceNo to set
     */
    public void setTermSequenceNo(Long termSequenceNo) {
        this.termSequenceNo = termSequenceNo;
    }

    /**
     * Get Term Start Date
     * -- The start date for the term
     *
     * @return the termStartDate
     */
    public Date getTermStartDate() {
        return termStartDate;
    }

    /**
     * Set Term Start Date
     * -- The start date for the term
     *
     * @param termStartDate the termStartDate to set
     */
    public void setTermStartDate(Date termStartDate) {
        this.termStartDate = termStartDate;
    }

    /**
     * Get Term End Date
     * -- The end date for the term
     *
     * @return the termEndDate
     */
    public Date getTermEndDate() {
        return termEndDate;
    }

    /**
     * Set Term End Date
     * -- The end date for the term
     *
     * @param termEndDate the termEndDate to set
     */
    public void setTermEndDate(Date termEndDate) {
        this.termEndDate = termEndDate;
    }

    /**
     * Get Years
     * -- No. of years a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @return the years
     */
    public Long getYears() {
        return years;
    }

    /**
     * Set Years
     * -- No. of years a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @param years the years to set
     */
    public void setYears(Long years) {
        this.years = years;
    }

    /**
     * Get Months
     * -- No. of months a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @return the months
     */
    public Long getMonths() {
        return months;
    }

    /**
     * Set Months
     * -- No. of months a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @param months the months to set
     */
    public void setMonths(Long months) {
        this.months = months;
    }

    /**
     * Get Weeks
     * -- No. of weeks a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @return the weeks
     */
    public Long getWeeks() {
        return weeks;
    }

    /**
     * Set Weeks
     * -- No. of weeks a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @param weeks the weeks to set
     */
    public void setWeeks(Long weeks) {
        this.weeks = weeks;
    }

    /**
     * Get Days
     * -- No. of days a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @return the days
     */
    public Long getDays() {
        return days;
    }

    /**
     * Set Days
     * -- No. of days a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @param days the days to set
     */
    public void setDays(Long days) {
        this.days = days;
    }

    /**
     * Get Hours
     * -- No. of hours a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @return the hours
     */
    public Long getHours() {
        return hours;
    }

    /**
     * Set Hours
     * -- No. of hours a sentence is to be served.
     * Used only for duration and intermittent type.
     *
     * @param hours the hours to set
     */
    public void setHours(Long hours) {
        this.hours = hours;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((days == null) ? 0 : days.hashCode());
        result = prime * result + ((hours == null) ? 0 : hours.hashCode());
        result = prime * result + ((months == null) ? 0 : months.hashCode());
        result = prime * result + ((termCategory == null) ? 0 : termCategory.hashCode());
        result = prime * result + ((termEndDate == null) ? 0 : termEndDate.hashCode());
        result = prime * result + ((termName == null) ? 0 : termName.hashCode());
        result = prime * result + ((termSequenceNo == null) ? 0 : termSequenceNo.hashCode());
        result = prime * result + ((termStartDate == null) ? 0 : termStartDate.hashCode());
        result = prime * result + ((termStatus == null) ? 0 : termStatus.hashCode());
        result = prime * result + ((weeks == null) ? 0 : weeks.hashCode());
        result = prime * result + ((years == null) ? 0 : years.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        TermType other = (TermType) obj;
        if (days == null) {
            if (other.days != null) {
				return false;
			}
        } else if (!days.equals(other.days)) {
			return false;
		}
        if (hours == null) {
            if (other.hours != null) {
				return false;
			}
        } else if (!hours.equals(other.hours)) {
			return false;
		}
        if (months == null) {
            if (other.months != null) {
				return false;
			}
        } else if (!months.equals(other.months)) {
			return false;
		}
        if (termCategory == null) {
            if (other.termCategory != null) {
				return false;
			}
        } else if (!termCategory.equals(other.termCategory)) {
			return false;
		}
        if (termEndDate == null) {
            if (other.termEndDate != null) {
				return false;
			}
        } else if (!termEndDate.equals(other.termEndDate)) {
			return false;
		}
        if (termName == null) {
            if (other.termName != null) {
				return false;
			}
        } else if (!termName.equals(other.termName)) {
			return false;
		}
        if (termSequenceNo == null) {
            if (other.termSequenceNo != null) {
				return false;
			}
        } else if (!termSequenceNo.equals(other.termSequenceNo)) {
			return false;
		}
        if (termStartDate == null) {
            if (other.termStartDate != null) {
				return false;
			}
        } else if (!termStartDate.equals(other.termStartDate)) {
			return false;
		}
        if (termStatus == null) {
            if (other.termStatus != null) {
				return false;
			}
        } else if (!termStatus.equals(other.termStatus)) {
			return false;
		}
        if (weeks == null) {
            if (other.weeks != null) {
				return false;
			}
        } else if (!weeks.equals(other.weeks)) {
			return false;
		}
        if (years == null) {
            if (other.years != null) {
				return false;
			}
        } else if (!years.equals(other.years)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TermType [termCategory=" + termCategory + ", termName=" + termName + ", termStatus=" + termStatus + ", termSequenceNo=" + termSequenceNo
                + ", termStartDate=" + termStartDate + ", termEndDate=" + termEndDate + ", years=" + years + ", months=" + months + ", weeks=" + weeks + ", days=" + days
                + ", hours=" + hours + "]";
    }

}
