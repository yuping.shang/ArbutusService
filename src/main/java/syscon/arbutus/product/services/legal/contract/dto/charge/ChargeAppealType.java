package syscon.arbutus.product.services.legal.contract.dto.charge;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * The representation of the charge appeal type
 */
public class ChargeAppealType implements Serializable {

    private static final long serialVersionUID = -8810206771787713159L;
    @NotNull
    private String appealStatus;
    @Size(max = 512, message = "max length 512")
    private String appealComment;

    /**
     * Default constructor
     */
    public ChargeAppealType() {
    }

    /**
     * Constructor
     *
     * @param appealStatus  String - A status of an appeal
     * @param appealComment String - General comments on the outcome of the appeal (optional)
     */
    public ChargeAppealType(@NotNull String appealStatus, @Size(max = 512, message = "max length 512") String appealComment) {
        this.appealStatus = appealStatus;
        this.appealComment = appealComment;
    }

    /**
     * Gets the value of the appealStatus property. It is a reference code value of AppealStatus set.
     *
     * @return String - A status of an appeal
     */
    public String getAppealStatus() {
        return appealStatus;
    }

    /**
     * Sets the value of the appealStatus property. It is a reference code value of AppealStatus set.
     *
     * @param appealStatus String - A status of an appeal
     */
    public void setAppealStatus(String appealStatus) {
        this.appealStatus = appealStatus;
    }

    /**
     * Gets the value of the appealComment property.
     *
     * @return String - General comments on the outcome of the appeal
     */
    public String getAppealComment() {
        return appealComment;
    }

    /**
     * Sets the value of the appealComment property.
     *
     * @param appealComment String - General comments on the outcome of the appeal
     */
    public void setAppealComment(String appealComment) {
        this.appealComment = appealComment;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeAppealType [appealStatus=" + appealStatus + ", appealComment=" + appealComment + "]";
    }

}
