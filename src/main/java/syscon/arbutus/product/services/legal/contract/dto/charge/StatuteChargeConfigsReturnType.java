package syscon.arbutus.product.services.legal.contract.dto.charge;

import java.io.Serializable;
import java.util.List;

/**
 * The representation of the statute charge configurations return type
 *
 * @author byu
 */
public class StatuteChargeConfigsReturnType implements Serializable {

    private static final long serialVersionUID = -8489098887164709117L;

    private List<StatuteChargeConfigType> statuteChargeConfigs;
    private Long totalSize;

    /**
     * Gets the value of the statuteChargeConfigs property.
     *
     * @return a list of statute charge configuration instances
     */
    public List<StatuteChargeConfigType> getStatuteChargeConfigs() {
        return statuteChargeConfigs;
    }

    /**
     * Sets the value of the statuteChargeConfigs property.
     *
     * @param statuteChargeConfigs a list of statute charge configuration instances
     */
    public void setStatuteChargeConfigs(List<StatuteChargeConfigType> statuteChargeConfigs) {
        this.statuteChargeConfigs = statuteChargeConfigs;
    }

    /**
     * Gets the value of the totalSize property.
     *
     * @return the total size of search result
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * Sets the value of the totalSize property.
     *
     * @param totalSize the total size of search result
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StatuteChargeConfigsReturnType [statuteChargeConfigs=" + statuteChargeConfigs + ", totalSize=" + totalSize + "]";
    }

}
