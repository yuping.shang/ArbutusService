package syscon.arbutus.product.services.legal.realization.util;

/**
 * <p>Enum class for ActivityStatusCode.
 * The activity status codes only support ACTIVE, INACTIVE.
 */
public enum ActivityStatusCode {

    /**
     * If the status is ACTIVE, the activity is considered to be relevant ie. Scheduled to occur or is completed.
     */
    ACTIVE,
    /**
     * If the status is INACTIVE, the activity is no longer relevant ie. cancelled or no longer required.
     */
    INACTIVE;

    /**
     * Create activity status codes from string value.
     *
     * @param v
     * @return ActivityStatusCode
     */
    public static ActivityStatusCode fromValue(String v) {
        return valueOf(v);
    }

    /**
     * Test if a string value is a valid ActivityStatusCode value.
     *
     * @param v
     */
    public static Boolean contain(String v) {

        for (ActivityStatusCode c : ActivityStatusCode.values()) {
            if (c.value().equals(v)) {
                return true;
            }
        }

        return false;
    }

    /**
     * String value of the activity status codes.
     *
     * @return String
     */
    public String value() {
        return name();
    }
}
