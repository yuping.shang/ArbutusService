package syscon.arbutus.product.services.legal.contract.dto.composite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CaseOffendersReturnType implements Serializable {

    private static final long serialVersionUID = 3976794083217470568L;

    private List<CaseOffenderType> caseOffenders;
    private Long totalSize;

    /**
     * @return the caseOffenders
     */
    public List<CaseOffenderType> getCaseOffenders() {
        if (caseOffenders == null) {
            caseOffenders = new ArrayList<CaseOffenderType>();
        }
        return caseOffenders;
    }

    /**
     * @param caseOffenders the caseOffenders to set
     */
    public void setCaseOffenders(List<CaseOffenderType> caseOffenders) {
        this.caseOffenders = caseOffenders;
    }

    /**
     * Gets the value of the totalSize property.
     *
     * @return the total size of search result
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * Sets the value of the totalSize property.
     *
     * @param totalSize the total size of search result
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "caseOffendersReturnType [getCaseOffenders()=" + getCaseOffenders() + ", getTotalSize()=" + getTotalSize() + "]";
    }

}
