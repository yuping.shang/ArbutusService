package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.Associable;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * BailPersonIdAssocEntity for Order Sentence Module of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdBailPIDAssc 'The Bailer Person Identity Association(associated by bail) table for Order Sentence module of Legal service'
 * @since January 03, 2013
 */
@Audited
@Entity
@Table(name = "LEG_OrdBailPIDAssc")
public class BailPersonIdAssocEntity implements Serializable, Associable {

    private static final long serialVersionUID = 7824880247247059781L;

    /**
     * @DbComment associationId 'Unique identification of Case Activity Initiated Order Association instance'
     */
    @Id
    @Column(name = "associationId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDBAILPIDASSC_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDBAILPIDASSC_ID", sequenceName = "SEQ_LEG_ORDBAILPIDASSC_ID", allocationSize = 1)
    private Long associationId;

    /**
     * @DbComment toIdentifier 'Person who posted the bail for the offender. Static reference to person identity.'
     */
    @Column(name = "toIdentifier", nullable = false)
    private Long toIdentifier;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment orderId 'Unique identification of a bail instance, foreign key to LEG_ORDBAIL table'
     */
    @ForeignKey(name = "Fk_LEG_OrdBailPIDAssc")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orderId", nullable = false)
    private BailEntity bail;

    /**
     * Constructor
     */
    public BailPersonIdAssocEntity() {
        super();
    }

    /**
     * Constructor
     *
     * @param associationId
     * @param toIdentifier
     * @param stamp
     */
    public BailPersonIdAssocEntity(Long associationId, Long toIdentifier, StampEntity stamp) {
        super();
        this.associationId = associationId;
        this.toIdentifier = toIdentifier;
        this.stamp = stamp;
    }

    /**
     * @return the associationId
     */
    public Long getAssociationId() {
        return associationId;
    }

    /**
     * @param associationId the associationId to set
     */
    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    /**
     * @return the toIdentifier
     */
    public Long getToIdentifier() {
        return toIdentifier;
    }

    /**
     * @param toIdentifier the toIdentifier to set
     */
    public void setToIdentifier(Long toIdentifier) {
        this.toIdentifier = toIdentifier;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the bail
     */
    public BailEntity getBail() {
        return bail;
    }

    /**
     * @param bail the bail to set
     */
    public void setBail(BailEntity bail) {
        this.bail = bail;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((associationId == null) ? 0 : associationId.hashCode());
        result = prime * result + ((toIdentifier == null) ? 0 : toIdentifier.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        BailPersonIdAssocEntity other = (BailPersonIdAssocEntity) obj;
        if (associationId == null) {
            if (other.associationId != null) {
				return false;
			}
        } else if (!associationId.equals(other.associationId)) {
			return false;
		}
        if (toIdentifier == null) {
            if (other.toIdentifier != null) {
				return false;
			}
        } else if (!toIdentifier.equals(other.toIdentifier)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "BailerPersonIdAssociationEntity [associationId=" + associationId + ", toIdentifier=" + toIdentifier + "]";
    }

    public String getToClass() {
        // Not implemented
        return null;
    }

    public void setToClass(String toClass) {
        // Not implemented
    }

}
