package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * NotificationLogEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdNotiLg 'Notification Log table for Order Sentence module of Legal service'
 * @DbComment LEG_OrdNotiLg.NotiLog 'Foreign key to LEG_OrdOrder'
 * @since December 21, 2012
 */
@Audited
@Entity
@Table(name = "LEG_OrdNotiLg")
public class NotificationLogEntity implements Serializable {

    private static final long serialVersionUID = -7086377289722785155L;

    /**
     * @DbComment notiLgId 'Unique identification of a notification log instance.'
     */
    @Id
    @Column(name = "notiLgId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDNOTILG_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDNOTILG_ID", sequenceName = "SEQ_LEG_ORDNOTILG_ID", allocationSize = 1)
    private Long notificationLogId;

    /**
     * @DbComment notificationDate 'The date and time the log entry was made.'
     */
    @Column(name = "notificationDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date notificationDate;

    /**
     * @DbComment notificationType 'The type of notification log entry'
     */
    @Column(name = "notificationType", nullable = false, length = 64)
    @MetaCode(set = MetaSet.NOTIFICATION_TYPE)
    private String notificationType;

    /**
     * @DbComment CommentUserId 'User ID who created/updated the comments'
     * @DbComment CommentDate 'The Date/Time when the comment was created'
     * @DbComment CommentText 'The comment text'
     */
    @Embedded
    private CommentEntity notificationComment;

    /**
     * @DbComment notificationLocation 'The address/phone details of the notification. Static reference to location.'
     */
    @Column(name = "notificationLocation", nullable = true)
    private Long notificationLocationAssociation;

    /**
     * @DbComment notificationContact 'The details (name) of the person notified.'
     */
    @Column(name = "notificationContact", nullable = true, length = 128)
    private String notificationContact;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * Constructor
     */
    public NotificationLogEntity() {
        super();
    }

    /**
     * @return the notificationLogId
     */
    public Long getNotificationLogId() {
        return notificationLogId;
    }

    /**
     * @param notificationLogId the notificationLogId to set
     */
    public void setNotificationLogId(Long notificationLogId) {
        this.notificationLogId = notificationLogId;
    }

    /**
     * @return the notificationDate
     */
    public Date getNotificationDate() {
        return notificationDate;
    }

    /**
     * @param notificationDate the notificationDate to set
     */
    public void setNotificationDate(Date notificationDate) {
        this.notificationDate = notificationDate;
    }

    /**
     * @return the notificationType
     */
    public String getNotificationType() {
        return notificationType;
    }

    /**
     * @param notificationType the notificationType to set
     */
    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    /**
     * @return the notificationComment
     */
    public CommentEntity getNotificationComment() {
        return notificationComment;
    }

    /**
     * @param notificationComment the notificationComment to set
     */
    public void setNotificationComment(CommentEntity notificationComment) {
        this.notificationComment = notificationComment;
    }

    /**
     * @return the notificationLocationAssociation
     */
    public Long getNotificationLocationAssociation() {
        return notificationLocationAssociation;
    }

    /**
     * @param notificationLocationAssociation the notificationLocationAssociation to set
     */
    public void setNotificationLocationAssociation(Long notificationLocationAssociation) {
        this.notificationLocationAssociation = notificationLocationAssociation;
    }

    /**
     * @return the notificationContact
     */
    public String getNotificationContact() {
        return notificationContact;
    }

    /**
     * @param notificationContact the notificationContact to set
     */
    public void setNotificationContact(String notificationContact) {
        this.notificationContact = notificationContact;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((notificationContact == null) ? 0 : notificationContact.hashCode());
        result = prime * result + ((notificationLocationAssociation == null) ? 0 : notificationLocationAssociation.hashCode());
        result = prime * result + ((notificationLogId == null) ? 0 : notificationLogId.hashCode());
        result = prime * result + ((notificationType == null) ? 0 : notificationType.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        NotificationLogEntity other = (NotificationLogEntity) obj;
        if (notificationContact == null) {
            if (other.notificationContact != null) {
				return false;
			}
        } else if (!notificationContact.equals(other.notificationContact)) {
			return false;
		}
        if (notificationLocationAssociation == null) {
            if (other.notificationLocationAssociation != null) {
				return false;
			}
        } else if (!notificationLocationAssociation.equals(other.notificationLocationAssociation)) {
			return false;
		}
        if (notificationLogId == null) {
            if (other.notificationLogId != null) {
				return false;
			}
        } else if (!notificationLogId.equals(other.notificationLogId)) {
			return false;
		}
        if (notificationType == null) {
            if (other.notificationType != null) {
				return false;
			}
        } else if (!notificationType.equals(other.notificationType)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "NotificationLogEntity [notificationLogId=" + notificationLogId + ", notificationDate=" + notificationDate + ", notificationType=" + notificationType
                + ", notificationComment=" + notificationComment + ", notificationLocationAssociation=" + notificationLocationAssociation + ", notificationContact="
                + notificationContact + ", toString()=" + super.toString() + "]";
    }

}
