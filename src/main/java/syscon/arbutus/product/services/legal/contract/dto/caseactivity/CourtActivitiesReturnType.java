package syscon.arbutus.product.services.legal.contract.dto.caseactivity;

import java.io.Serializable;
import java.util.List;

/**
 * The representation of the court activities return type
 *
 * @author lhan
 */
public class CourtActivitiesReturnType implements Serializable {

    private static final long serialVersionUID = -1227026189592156457L;

    private List<CourtActivityType> courtActivities;
    private Long totalSize;

    /**
     * @return the courtActivities
     */
    public List<CourtActivityType> getCourtActivities() {
        return courtActivities;
    }

    /**
     * @param courtActivities the courtActivities to set
     */
    public void setCourtActivities(List<CourtActivityType> courtActivities) {
        this.courtActivities = courtActivities;
    }

    /**
     * @return the totalSize
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * @param totalSize the totalSize to set
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

}
