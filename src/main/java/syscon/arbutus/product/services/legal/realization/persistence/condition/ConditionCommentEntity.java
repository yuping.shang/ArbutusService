package syscon.arbutus.product.services.legal.realization.persistence.condition;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * Condition Comment entity for Condition of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_CNComment 'Condition Comment table for Condition module of Case Management Service'
 * @since December 20, 2012
 */
@Audited
@Entity
@Table(name = "LEG_CNComment")
public class ConditionCommentEntity implements Serializable {

    private static final long serialVersionUID = 815562322003952148L;

    /**
     * @DbComment commentId 'Unique identification of a Condition Comment instance'
     */
    @Id
    @Column(name = "commentId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CNComment_Id")
    @SequenceGenerator(name = "SEQ_LEG_CNComment_Id", sequenceName = "SEQ_LEG_CNComment_Id", allocationSize = 1)
    private Long commentId;

    /**
     * @DbComment CommentUserId 'Comment User Id'
     */
    @Column(name = "CommentUserId", nullable = true, length = 64)
    private String commentUserId;

    /**
     * @DbComment CommentDate 'The Date/Time when the comment was created'
     */
    @Column(name = "CommentDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentDate;

    /**
     * @DbComment CommentText 'The comment text'
     */
    @Column(name = "CommentText", nullable = true, length = 512) // 4k length
    private String commentText;

    /**
     * @DbComment conditionId 'Foreign key to LEG_Condition table'
     */
    @ForeignKey(name = "Fk_LEG_CNComment")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "conditionId", nullable = false)
    private ConditionEntity condition;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * Default constructor
     */
    public ConditionCommentEntity() {
    }

    public ConditionCommentEntity(Long commentId, String commentUserId, Date commentDate, String commentText, StampEntity stamp) {
        super();
        this.commentId = commentId;
        this.commentUserId = commentUserId;
        this.commentDate = commentDate;
        this.commentText = commentText;
        this.stamp = stamp;
    }

    /**
     * @return the commentId
     */
    public Long getCommentId() {
        return commentId;
    }

    /**
     * @param commentId the commentId to set
     */
    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    /**
     * Gets the value of the commentUserId property
     *
     * @return String
     */
    public String getCommentUserId() {
        return commentUserId;
    }

    /**
     * Sets the value of the commentUserId property
     *
     * @param commentUserId String
     */
    public void setCommentUserId(String commentUserId) {
        this.commentUserId = commentUserId;
    }

    /**
     * Gets the value of the commentDate property
     *
     * @return Date
     */
    public Date getCommentDate() {
        return commentDate;
    }

    /**
     * Sets the value of the commentDate property
     *
     * @param commentDate Date
     */
    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    /**
     * Gets the value of the commentText property
     *
     * @return String
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * Sets the value of the commentText property
     *
     * @param commentText String
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    /**
     * @return the condition
     */
    public ConditionEntity getCondition() {
        return condition;
    }

    /**
     * @param condition the condition to set
     */
    public void setCondition(ConditionEntity condition) {
        this.condition = condition;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((commentDate == null) ? 0 : commentDate.hashCode());
        result = prime * result + ((commentId == null) ? 0 : commentId.hashCode());
        result = prime * result + ((commentText == null) ? 0 : commentText.hashCode());
        result = prime * result + ((commentUserId == null) ? 0 : commentUserId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ConditionCommentEntity other = (ConditionCommentEntity) obj;
        if (commentDate == null) {
            if (other.commentDate != null) {
				return false;
			}
        } else if (!commentDate.equals(other.commentDate)) {
			return false;
		}
        if (commentId == null) {
            if (other.commentId != null) {
				return false;
			}
        } else if (!commentId.equals(other.commentId)) {
			return false;
		}
        if (commentText == null) {
            if (other.commentText != null) {
				return false;
			}
        } else if (!commentText.equals(other.commentText)) {
			return false;
		}
        if (commentUserId == null) {
            if (other.commentUserId != null) {
				return false;
			}
        } else if (!commentUserId.equals(other.commentUserId)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ConditionCommentEntity [commentId=" + commentId + ", commentUserId=" + commentUserId + ", commentDate=" + commentDate + ", commentText=" + commentText
                + ", stamp=" + stamp + "]";
    }

}
