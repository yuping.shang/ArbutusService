package syscon.arbutus.product.services.legal.contract.dto.charge;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * The representation of the charge disposition type
 */
public class ChargeDispositionType implements Serializable {

    private static final long serialVersionUID = -1976267102434296564L;

    private String dispositionOutcome;
    @NotNull
    private String chargeStatus;
    @NotNull
    private Date dispositionDate;

    /**
     * Default constructor
     */
    public ChargeDispositionType() {
    }

    /**
     * Constructor
     *
     * @param dispositionOutcome String - A result or outcome of the disposition (optional)
     * @param chargeStatus       String - The status of a charge (active, inactive)
     * @param dispositionDate    Date - Date of the disposition
     */
    public ChargeDispositionType(String dispositionOutcome, @NotNull String chargeStatus, @NotNull Date dispositionDate) {
        this.dispositionOutcome = dispositionOutcome;
        this.chargeStatus = chargeStatus;
        this.dispositionDate = dispositionDate;
    }

    /**
     * Gets the value of the dispositionOutcome property. It is a reference code of ChargeOutcome set.
     *
     * @return CodeType - A result or outcome of the disposition
     */
    public String getDispositionOutcome() {
        return dispositionOutcome;
    }

    /**
     * Sets the value of the dispositionOutcome property. It is a reference code of ChargeOutcome set.
     *
     * @param dispositionOutcome String - A result or outcome of the disposition
     */
    public void setDispositionOutcome(String dispositionOutcome) {
        this.dispositionOutcome = dispositionOutcome;
    }

    /**
     * Gets the value of the chargeStatus property. It is a reference code of ChargeStatus set.
     *
     * @return String - The status of a charge (active, inactive)
     */
    public String getChargeStatus() {
        return chargeStatus;
    }

    /**
     * Sets the value of the chargeStatus property. It is a reference code of ChargeStatus set.
     *
     * @param chargeStatus String - The status of a charge (active, inactive)
     */
    public void setChargeStatus(String chargeStatus) {
        this.chargeStatus = chargeStatus;
    }

    /**
     * Gets the value of the dispositionDate property.
     *
     * @return Date - Date of the disposition
     */
    public Date getDispositionDate() {
        return dispositionDate;
    }

    /**
     * Sets the value of the dispositionDate property.
     *
     * @param dispositionDate Date - Date of the disposition
     */
    public void setDispositionDate(Date dispositionDate) {
        this.dispositionDate = dispositionDate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeDispositionType [dispositionOutcome=" + dispositionOutcome + ", chargeStatus=" + chargeStatus + ", dispositionDate=" + dispositionDate + "]";
    }

}
