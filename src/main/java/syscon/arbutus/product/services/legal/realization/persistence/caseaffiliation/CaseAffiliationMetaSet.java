/**
 *
 */
package syscon.arbutus.product.services.legal.realization.persistence.caseaffiliation;

/**
 * @author lhan
 */
public class CaseAffiliationMetaSet {
    /**
     * AFFILIATIONCATEGORY reference code set
     */
    public static final String AFFILIATION_CATEGORY = "AFFILIATIONCATEGORY";

    /**
     * ASSOCIATIONTYPE reference code set
     */
    public static final String ASSOCIATION_TYPE = "ASSOCIATIONTYPE";

    /**
     * PERSONROLE reference code set
     */
    public static final String PERSON_ROLE = "PERSONROLE";

}
