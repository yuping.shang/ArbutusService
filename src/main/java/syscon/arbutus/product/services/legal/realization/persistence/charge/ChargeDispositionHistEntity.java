package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the charge disposition history entity.
 *
 * @author lhan
 * @DbComment LEG_CHGDspHist 'the charge disposition history data table'
 */
//@Audited
@Entity
@Table(name = "LEG_CHGDspHist")
public class ChargeDispositionHistEntity implements Serializable {

    private static final long serialVersionUID = 2950865068318086017L;

    /**
     * @DbComment CHGDspHistId 'Unique id of a charge disposition history instance'
     */
    @Id
    @Column(name = "CHGDspHistId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGDspHist_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGDspHist_Id", sequenceName = "SEQ_LEG_CHGDspHist_Id", allocationSize = 1)
    private Long chargeDspHistId;

    /**
     * @DbComment ChargeId 'Unique id of a charge instance'
     */
    @Column(name = "ChargeId", nullable = false)
    private Long chargeId;

    //From charge Disposition type
    /**
     * @DbComment ChargeStatus 'The status of a charge (active, inactive)'
     */
    @Column(name = "ChargeStatus", nullable = false, length = 64)
    private String chargeStatus;

    //From charge Disposition type
    /**
     * @DbComment DispositionOutcome 'A result or outcome of the disposition.'
     */
    @Column(name = "DispositionOutcome", nullable = true, length = 64)
    private String dispositionOutcome;

    //From charge Disposition type
    /**
     * @DbComment DispositionDate 'Date of the disposition'
     */
    @Column(name = "DispositionDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dispositionDate;

    /**
     * @DbComment CommentUserId 'User ID who created/updated the comments'
     * @DbComment CommentDate 'The Date/Time when the comment was created'
     * @DbComment CommentText 'The comment text'
     */
    @Embedded
    private CommentEntity comment;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ChargeId", insertable = false, updatable = false)
    private ChargeEntity charge;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     * @return the chargeId
     */
    public Long getChargeId() {
        return chargeId;
    }

    /**
     * @param chargeId the chargeId to set
     */
    public void setChargeId(Long chargeId) {
        this.chargeId = chargeId;
    }

    /**
     * @return the chargeStatus
     */
    public String getChargeStatus() {
        return chargeStatus;
    }

    /**
     * @param chargeStatus the chargeStatus to set
     */
    public void setChargeStatus(String chargeStatus) {
        this.chargeStatus = chargeStatus;
    }

    /**
     * @return the dispositionOutcome
     */
    public String getDispositionOutcome() {
        return dispositionOutcome;
    }

    /**
     * @param dispositionOutcome the dispositionOutcome to set
     */
    public void setDispositionOutcome(String dispositionOutcome) {
        this.dispositionOutcome = dispositionOutcome;
    }

    /**
     * @return the dispositionDate
     */
    public Date getDispositionDate() {
        return dispositionDate;
    }

    /**
     * @param dispositionDate the dispositionDate to set
     */
    public void setDispositionDate(Date dispositionDate) {
        this.dispositionDate = dispositionDate;
    }

    /**
     * @return the comment
     */
    public CommentEntity getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(CommentEntity comment) {
        this.comment = comment;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the charge
     */
    public ChargeEntity getCharge() {
        return charge;
    }

    /**
     * @param charge the charge to set
     */
    public void setCharge(ChargeEntity charge) {
        this.charge = charge;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeDispositionHistEntity [getChargeId()=" + getChargeId() + ", getChargeStatus()=" + getChargeStatus() + ", getDispositionOutcome()="
                + getDispositionOutcome() + ", getDispositionDate()=" + getDispositionDate() + ", getComment()=" + getComment() + ", getStamp()=" + getStamp() + "]";
    }

}
