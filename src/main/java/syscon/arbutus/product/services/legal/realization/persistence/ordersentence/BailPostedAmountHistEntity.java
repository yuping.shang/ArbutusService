package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * BailPostedAmountHistEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdBailPstHstAmnt 'The Bail Post Amount History table for Order Sentence module of Legal service'
 * @since December 21, 2012
 */
//@Audited
@Entity
@Table(name = "LEG_OrdBailPstHstAmnt")
public class BailPostedAmountHistEntity implements Serializable {

    private static final long serialVersionUID = 6055138627738557453L;

    /**
     * @DbComment bpHstId 'Unique identification of Bailer Posted Amount Association History instance'
     */
    @Id
    @Column(name = "bpHstId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDBLPSTAMNTHST_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDBLPSTAMNTHST_ID", sequenceName = "SEQ_LEG_ORDBLPSTAMNTHST_ID", allocationSize = 1)
    private Long historyId;

    /**
     * @DbComment bailAmntId 'Unique identification of a bail amount instance'
     */
    @Column(name = "bailAmntId")
    private Long bailAmountId;

    /**
     * @DbComment bailType 'The type of bail i.e., cash, surety, property etc.,'
     */
    @Column(name = "bailType", nullable = false)
    private String bailType;

    /**
     * @DbComment bailAmount 'The amount of bail set for each type'
     */
    @Column(name = "bailAmount", nullable = false, precision = 12, scale = 2)
    private BigDecimal bailAmount;

    @ForeignKey(name = "Fk_LEG_OrdBailPstAmntH1")
    @OneToMany(mappedBy = "bailAmount", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<OrderPostedChgAssocHistEntity> chargeAssociations;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment hstId 'Unique identification of a bail instance, foreign key to LEG_OrdOrderHst table'
     */
    @ForeignKey(name = "Fk_LEG_OrdBailPstAmntH2")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hstId", nullable = false)
    private BailHistoryEntity bail;

    /**
     * @return the historyId
     */
    public Long getHistoryId() {
        return historyId;
    }

    /**
     * @param historyId the historyId to set
     */
    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    /**
     * @return the bailAmountId
     */
    public Long getBailAmountId() {
        return bailAmountId;
    }

    /**
     * @param bailAmountId the bailAmountId to set
     */
    public void setBailAmountId(Long bailAmountId) {
        this.bailAmountId = bailAmountId;
    }

    /**
     * @return the bailType
     */
    public String getBailType() {
        return bailType;
    }

    /**
     * @param bailType the bailType to set
     */
    public void setBailType(String bailType) {
        this.bailType = bailType;
    }

    /**
     * @return the bailAmount
     */
    public BigDecimal getBailAmount() {
        return bailAmount;
    }

    /**
     * @param bailAmount the bailAmount to set
     */
    public void setBailAmount(BigDecimal bailAmount) {
        this.bailAmount = bailAmount;
    }

    /**
     * @return the chargeAssociations
     */
    public Set<OrderPostedChgAssocHistEntity> getChargeAssociations() {
        return chargeAssociations;
    }

    /**
     * @param chargeAssociations the chargeAssociations to set
     */
    public void setChargeAssociations(Set<OrderPostedChgAssocHistEntity> chargeAssociations) {
        if (chargeAssociations != null) {
            for (OrderPostedChgAssocHistEntity chg : chargeAssociations) {
                chg.setBailAmount(this);
            }
            this.chargeAssociations = chargeAssociations;
        } else {
			this.chargeAssociations = new HashSet<OrderPostedChgAssocHistEntity>();
		}
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the bail
     */
    public BailHistoryEntity getBail() {
        return bail;
    }

    /**
     * @param bail the bail to set
     */
    public void setBail(BailHistoryEntity bail) {
        this.bail = bail;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bailAmount == null) ? 0 : bailAmount.hashCode());
        result = prime * result + ((bailAmountId == null) ? 0 : bailAmountId.hashCode());
        result = prime * result + ((bailType == null) ? 0 : bailType.hashCode());
        result = prime * result + ((historyId == null) ? 0 : historyId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        BailPostedAmountHistEntity other = (BailPostedAmountHistEntity) obj;
        if (bailAmount == null) {
            if (other.bailAmount != null) {
				return false;
			}
        } else if (!bailAmount.equals(other.bailAmount)) {
			return false;
		}
        if (bailAmountId == null) {
            if (other.bailAmountId != null) {
				return false;
			}
        } else if (!bailAmountId.equals(other.bailAmountId)) {
			return false;
		}
        if (bailType == null) {
            if (other.bailType != null) {
				return false;
			}
        } else if (!bailType.equals(other.bailType)) {
			return false;
		}
        if (historyId == null) {
            if (other.historyId != null) {
				return false;
			}
        } else if (!historyId.equals(other.historyId)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "BailPostedAmountHistoryEntity [historyId=" + historyId + ", bailAmountId=" + bailAmountId + ", bailType=" + bailType + ", bailAmount=" + bailAmount
                + ", chargeAssociations=" + chargeAssociations + "]";
    }

}
