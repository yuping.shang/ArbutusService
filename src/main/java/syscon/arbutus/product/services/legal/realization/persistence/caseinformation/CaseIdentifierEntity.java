package syscon.arbutus.product.services.legal.realization.persistence.caseinformation;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the case information identifier entity.
 *
 * @DbComment LEG_CIIdentifier 'The case information identifier table'
 */
@Audited
@Entity
@Table(name = "LEG_CIIdentifier")
@Where(clause = "flag = 1")
public class CaseIdentifierEntity implements Serializable {

    private static final long serialVersionUID = -3958987072987155024L;

    /**
     * @DbComment IdentifierId 'Unique Id for each case identifier record'
     */
    @Id
    @Column(name = "IdentifierId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CIIdentifier_Id")
    @SequenceGenerator(name = "SEQ_LEG_CIIdentifier_Id", sequenceName = "SEQ_LEG_CIIdentifier_Id", allocationSize = 1)
    private Long identifierId;

    /**
     * @DbComment IdentifierType 'A kind of identifier or number that can be associated to a criminal case e.g. Court Information Number, Indictment Number'
     */
    @MetaCode(set = MetaSet.CASE_IDENTIFIER_TYPE)
    @Column(name = "IdentifierType", length = 64, nullable = false)
    private String identifierType;

    /**
     * @DbComment IdentifierValue 'The number or value of the case identifier e.g. 1234567S (Indictment Number)'
     */
    @Column(name = "IdentifierValue", length = 64, nullable = false)
    private String identifierValue;

    /**
     * @DbComment AutoGenerate 'Allows the user to indicate to the system whether or not to auto generate the identifier when creating it. This is set to false by default and cannot be set to true for an update. If set to true the IdentifierValue is ignored.'
     */
    @Column(name = "AutoGenerate", nullable = true)
    private Boolean autoGenerate;

    /**
     * @DbComment IsPrimary 'Indicate if this is a primary identifier for a case.'
     */
    @Column(name = "IsPrimary", nullable = false)
    private Boolean primary;

    /**
     * @DbComment OrganizationId 'Static reference to an Organization which issued the case identifier'
     */
    @Column(name = "OrganizationId", nullable = true)
    private Long organizationId;

    /**
     * @DbComment FacilityId 'Static reference to a Facility which issued the case identifier'
     */
    @Column(name = "FacilityId", nullable = true)
    private Long facilityId;

    /**
     * @DbComment IdentifierComment 'Comments associated to the case identifier'
     */
    @Column(name = "IdentifierComment", length = 1024, nullable = true)
    private String identifierComment;

    /**
     * @DbComment CaseInfoId 'The case information instance Id, foreign key to LEG_CICaseInfo table'
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CaseInfoId", nullable = false)
    private CaseInfoEntity caseInfo;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     *
     */
    public CaseIdentifierEntity() {
    }

    /**
     * @param identifierId
     * @param identifierType
     * @param identifierValue
     * @param autoGenerate
     * @param organizationId
     * @param facilityId
     * @param identifierComment
     * @param primary
     */
    public CaseIdentifierEntity(Long identifierId, String identifierType, String identifierValue, Boolean autoGenerate, Long organizationId, Long facilityId,
            String identifierComment, Boolean primary) {
        this.identifierId = identifierId;
        this.identifierType = identifierType;
        this.identifierValue = identifierValue;
        this.autoGenerate = autoGenerate;
        this.organizationId = organizationId;
        this.facilityId = facilityId;
        this.identifierComment = identifierComment;
        this.primary = primary;
    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /**
     * @return the identifierId
     */
    public Long getIdentifierId() {
        return identifierId;
    }

    /**
     * @param identifierId the identifierId to set
     */
    public void setIdentifierId(Long identifierId) {
        this.identifierId = identifierId;
    }

    /**
     * @return the identifierType
     */
    public String getIdentifierType() {
        return identifierType;
    }

    /**
     * @param identifierType the identifierType to set
     */
    public void setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
    }

    /**
     * @return the identifierValue
     */
    public String getIdentifierValue() {
        return identifierValue;
    }

    /**
     * @param identifierValue the identifierValue to set
     */
    public void setIdentifierValue(String identifierValue) {
        this.identifierValue = identifierValue;
    }

    /**
     * @return the autoGenerate
     */
    public Boolean isAutoGenerate() {
        return autoGenerate;
    }

    /**
     * @param autoGenerate the autoGenerate to set
     */
    public void setAutoGenerate(Boolean autoGenerate) {
        this.autoGenerate = autoGenerate;
    }

    /**
     * @return
     */
    public Boolean isPrimary() {
        return primary;
    }

    /**
     * @param primary
     */
    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

    /**
     * @return the organizationId
     */
    public Long getOrganizationId() {
        return organizationId;
    }

    /**
     * @param organizationId the organizationId to set
     */
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * @return the facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * @param facilityId the facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * @return the identifierComment
     */
    public String getIdentifierComment() {
        return identifierComment;
    }

    /**
     * @param identifierComment the identifierComment to set
     */
    public void setIdentifierComment(String identifierComment) {
        this.identifierComment = identifierComment;
    }

    /**
     * @return the caseInfo
     */
    public CaseInfoEntity getCaseInfo() {
        return caseInfo;
    }

    /**
     * @param caseInfo the caseInfo to set
     */
    public void setCaseInfo(CaseInfoEntity caseInfo) {
        this.caseInfo = caseInfo;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((autoGenerate == null) ? 0 : autoGenerate.hashCode());
        result = prime * result + ((facilityId == null) ? 0 : facilityId.hashCode());
        result = prime * result + ((identifierComment == null) ? 0 : identifierComment.hashCode());
        result = prime * result + ((identifierId == null) ? 0 : identifierId.hashCode());
        result = prime * result + ((identifierType == null) ? 0 : identifierType.hashCode());
        result = prime * result + ((identifierValue == null) ? 0 : identifierValue.hashCode());
        result = prime * result + ((organizationId == null) ? 0 : organizationId.hashCode());
        result = prime * result + ((primary == null) ? 0 : primary.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        CaseIdentifierEntity other = (CaseIdentifierEntity) obj;
        if (autoGenerate == null) {
            if (other.autoGenerate != null) {
				return false;
			}
        } else if (!autoGenerate.equals(other.autoGenerate)) {
			return false;
		}
        if (facilityId == null) {
            if (other.facilityId != null) {
				return false;
			}
        } else if (!facilityId.equals(other.facilityId)) {
			return false;
		}
        if (identifierComment == null) {
            if (other.identifierComment != null) {
				return false;
			}
        } else if (!identifierComment.equals(other.identifierComment)) {
			return false;
		}
        if (identifierId == null) {
            if (other.identifierId != null) {
				return false;
			}
        } else if (!identifierId.equals(other.identifierId)) {
			return false;
		}
        if (identifierType == null) {
            if (other.identifierType != null) {
				return false;
			}
        } else if (!identifierType.equals(other.identifierType)) {
			return false;
		}
        if (identifierValue == null) {
            if (other.identifierValue != null) {
				return false;
			}
        } else if (!identifierValue.equals(other.identifierValue)) {
			return false;
		}
        if (organizationId == null) {
            if (other.organizationId != null) {
				return false;
			}
        } else if (!organizationId.equals(other.organizationId)) {
			return false;
		}
        if (primary == null) {
            if (other.primary != null) {
				return false;
			}
        } else if (!primary.equals(other.primary)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "CaseIdentifierEntity [getIdentifierId()=" + getIdentifierId() + ", getIdentifierType()=" + getIdentifierType() + ", getIdentifierValue()="
                + getIdentifierValue() + ", isAutoGenerate()=" + isAutoGenerate() + ", isPrimary()=" + isPrimary() + ", getOrganizationId()=" + getOrganizationId()
                + ", getFacilityId()=" + getFacilityId() + ", getIdentifierComment()=" + getIdentifierComment() + "]";
    }

}
