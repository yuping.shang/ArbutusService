package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import java.io.Serializable;

/**
 * SentenceAppealSearchType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 28, 2012
 */
public class SentenceAppealSearchType implements Serializable {

    private static final long serialVersionUID = 2485655251651286577L;

    private String appealStatus;
    private String appealComment;

    /**
     * Constructor
     */
    public SentenceAppealSearchType() {
        super();
    }

    /**
     * Constructor
     * <p>At least one argument must be provided.
     *
     * @param appealStatus  String. A status of an appeal on a sentence.
     * @param appealComment String(length <= 512). General comments on the outcome of the appeal.
     */
    public SentenceAppealSearchType(String appealStatus, String appealComment) {
        super();
        this.appealStatus = appealStatus;
        this.appealComment = appealComment;
    }

    /**
     * @return the appealStatus
     */
    public String getAppealStatus() {
        return appealStatus;
    }

    /**
     * @param appealStatus the appealStatus to set
     */
    public void setAppealStatus(String appealStatus) {
        this.appealStatus = appealStatus;
    }

    /**
     * @return the appealComment
     */
    public String getAppealComment() {
        return appealComment;
    }

    /**
     * @param appealComment the appealComment to set
     */
    public void setAppealComment(String appealComment) {
        this.appealComment = appealComment;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((appealComment == null) ? 0 : appealComment.hashCode());
        result = prime * result + ((appealStatus == null) ? 0 : appealStatus.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SentenceAppealSearchType other = (SentenceAppealSearchType) obj;
        if (appealComment == null) {
            if (other.appealComment != null) {
				return false;
			}
        } else if (!appealComment.equals(other.appealComment)) {
			return false;
		}
        if (appealStatus == null) {
            if (other.appealStatus != null) {
				return false;
			}
        } else if (!appealStatus.equals(other.appealStatus)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SentenceAppealSearchType [appealStatus=" + appealStatus + ", appealComment=" + appealComment + "]";
    }

}
