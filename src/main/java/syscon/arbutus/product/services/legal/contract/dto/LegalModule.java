package syscon.arbutus.product.services.legal.contract.dto;

/**
 * The enumeration of modules of this case management service.
 */
public enum LegalModule {
    /**
     * Case Activity module.
     */
    CASE_ACTIVITY("CaseActivity"),
    /**
     * Case Information module.
     */
    CASE_INFORMATION("CaseInformation"),
    /**
     * Charge module
     */
    CHARGE("Charge"),
    /**
     * Condition module
     */
    CONDITION("Condition"),
    /**
     * Order Sentence module
     */
    ORDER_SENTENCE("OrderSentence"),
    /**
     * Case Plan module
     */
    CASE_PLAN("CasePlan");

    private final String value;

    /**
     * Constructor.
     *
     * @param v one of the association classes.
     */
    LegalModule(String v) {
        value = v;
    }

    /**
     * Create a case management module class from string value.
     *
     * @param v String value.
     * @return CaseMgmtModule instance.
     */
    public static LegalModule fromValue(String v) {
        for (LegalModule c : LegalModule.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    /**
     * String value of this case management module class.
     *
     * @return String value of this case management module class
     */
    public String value() {
        return value;
    }

}
