package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.Associable;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the module association entity of charge Instance.
 *
 * @DbComment LEG_CHGModuleAssc 'The module association table of CaseInfo Instance'
 * @DbComment LEG_CHGModuleAssc.AssociationId 'Unique id of the association record'
 * @DbComment LEG_CHGModuleAssc.FromIdentifier 'The case information instance Id'
 * @DbComment LEG_CHGModuleAssc.ToClass 'The association module that is referenced'
 * @DbComment LEG_CHGModuleAssc.ToIdentifier 'The unique id of the Association To Object'
 * @DbComment LEG_CHGModuleAssc.CreateUserId 'User ID who created the object'
 * @DbComment LEG_CHGModuleAssc.CreateDateTime 'Date and time when the object was created'
 * @DbComment LEG_CHGModuleAssc.InvocationContext 'Invocation context when the create/update action called'
 * @DbComment LEG_CHGModuleAssc.ModifyUserId 'User ID who last updated the object'
 * @DbComment LEG_CHGModuleAssc.ModifyDateTime 'Date and time when the object was last updated'
 */
@Audited
@Entity
@Table(name = "LEG_CHGModuleAssc")
public class ChargeModuleAssociationEntity implements Serializable, Associable {

    private static final long serialVersionUID = 3083623626041678724L;

    @Id
    @Column(name = "AssociationId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGModuleAssc_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGModuleAssc_Id", sequenceName = "SEQ_LEG_CHGModuleAssc_Id", allocationSize = 1)
    private Long associationId;

    @Column(name = "ToClass", nullable = false, length = 64)
    private String toClass;

    @Column(name = "ToIdentifier", nullable = false)
    private Long toIdentifier;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FromIdentifier", referencedColumnName = "ChargeId", nullable = false)
    private ChargeEntity charge;

    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * Gets the value of the associationId property
     *
     * @return Long
     */
    public Long getAssociationId() {
        return associationId;
    }

    /**
     * Sets the value of the associationId property
     *
     * @param associationId Long
     */
    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    /**
     * Gets the value of the toClass property
     *
     * @return String
     */
    public String getToClass() {
        return toClass;
    }

    /**
     * Sets the value of the toClass property
     *
     * @param toClass String
     */
    public void setToClass(String toClass) {
        this.toClass = toClass;
    }

    /**
     * Gets the value of the toIdentifier property
     *
     * @return Long
     */
    public Long getToIdentifier() {
        return toIdentifier;
    }

    /**
     * Sets the value of the toIdentifier property
     *
     * @param toIdentifier Long
     */
    public void setToIdentifier(Long toIdentifier) {
        this.toIdentifier = toIdentifier;
    }

    /**
     * Gets the value of the stamp property
     *
     * @return StampEntity
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * Sets the value of the stamp property
     *
     * @param stamp StampEntity
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the charge
     */
    public ChargeEntity getCharge() {
        return charge;
    }

    /**
     * @param charge the charge to set
     */
    public void setCharge(ChargeEntity charge) {
        this.charge = charge;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeModuleAssociationEntity [associationId=" + associationId + ", toClass=" + toClass + ", toIdentifier=" + toIdentifier + "]";
    }

}
