package syscon.arbutus.product.services.legal.contract.dto.valueholder;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;
import syscon.arbutus.product.services.datasecurity.contract.dto.ReferenceSet.EntityTypeEnum;
import syscon.arbutus.product.services.legal.contract.dto.charge.ChargeType;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.OrderEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.SentenceEntity;

public class ConversionUtils {

    public static String getMemento(EntityTypeEnum type, Object obj) {

        String memnto = "";

        if (type.equals(EntityTypeEnum.CONDITION)) {

        }

        if (type.equals(EntityTypeEnum.SENTENCE)) {

            SentenceEntity sent = (SentenceEntity) obj;
            SentenceHolder sHolder = new SentenceHolder();

            sHolder.consto = "" + sent.getConcecutiveFrom();
            sHolder.sentid = String.valueOf(sent.getOrderId());
            sHolder.orderCid = String.valueOf(sent.getOrderId());
            sHolder.stdate = "" + sent.getSentenceStartDate().getTime();
            sHolder.enddate = toStringDateDDMMYYY(sent.getSentenceEndDate());
            sHolder.type = sent.getSentenceType();
            sHolder.disposition = sent.getOrderDisposition().getDispositionOutcome();
            sHolder.status = (sent.getOrderDisposition() != null) ? sent.getOrderDisposition().getOrderStatus() : null;
            sHolder.sentNo = "" + sent.getSentenceNumber();

            Gson gson = new Gson();
            memnto = gson.toJson(sHolder);

        }

        if (type.equals(EntityTypeEnum.ORDER)) {

            OrderEntity order = (OrderEntity) obj;
            OrderHolder oHolder = new OrderHolder();

            oHolder.issuedate = (order.getOrderIssuanceDate() != null) ? toStringDateDDMMYYY(order.getOrderIssuanceDate()) : null;
            oHolder.expirydate = (order.getOrderExpirationDate() != null) ? toStringDateDDMMYYY(order.getOrderExpirationDate()) : null;
            oHolder.description = "";
            oHolder.type = order.getOrderType();
            oHolder.subtype = String.valueOf(order.getOrderSubType());
            oHolder.issueagency = (order.getIssuingAgency() != null) ? String.valueOf(order.getIssuingAgency().getNotificationFacilityAssociation()) : null;
            oHolder.outcome = (order.getOrderDisposition() != null) ? order.getOrderDisposition().getDispositionOutcome() : null;
            oHolder.status = (order.getOrderDisposition() != null) ? order.getOrderDisposition().getOrderStatus() : null;
            oHolder.orderIdentication = (order.getOrderId() != null) ? order.getOrderId().toString() : null;
            Gson gson = new Gson();
            memnto = gson.toJson(oHolder);

        }

        if (type.equals(EntityTypeEnum.CHARGE)) {
            ChargeType charge = (ChargeType) obj;
            ChargeHolder cHolder = new ChargeHolder();
            cHolder.chargecode = String.valueOf(charge.getChargeCode());
            cHolder.description = charge.getChargeDescription();
            cHolder.type = charge.getChargeType();
            cHolder.outcome = charge.getOutcome();
            cHolder.primarycharge = String.valueOf(charge.isPrimary());
            cHolder.offensedate = toStringDateDDMMYYY(charge.getOffenceStartDate()).toString();
            cHolder.status = charge.getChargeStatus();
            cHolder.chargeId = charge.getChargeId().toString();

            Gson gson = new Gson();
            memnto = gson.toJson(cHolder);
        }

        if (type.equals(EntityTypeEnum.CASEACTIVITY)) {

        }

        return memnto;
    }

    //Moved from DateUtility class of Assessment
    public static String toStringDateDDMMYYY(Date date) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat myFormat = new SimpleDateFormat("MM/dd/yyyy");
        return myFormat.format(date).toString();
    }

}
