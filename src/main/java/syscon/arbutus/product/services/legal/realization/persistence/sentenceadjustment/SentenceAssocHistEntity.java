package syscon.arbutus.product.services.legal.realization.persistence.sentenceadjustment;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.Associable;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * SentenceAssocHistEntity for Sentence Adjustment Table
 *
 * @author yshang
 * @DbComment LEG_SentAdjustAsscHst 'History table for Sentence Adjustment'
 * @since December 10, 2013
 */
//@Audited
@Entity
@Table(name = "LEG_SentAdjustAsscHst")
public class SentenceAssocHistEntity implements Serializable, Associable {

    private static final long serialVersionUID = -7710410507546624387L;

    /**
     * @DbComment adjustAsscHstId 'Id of history table of Sentence Association, the table LEG_SentAdjustAssc'
     */
    @Id
    @Column(name = "adjustAsscHstId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_SentAdjustAsscHst_Id")
    @SequenceGenerator(name = "SEQ_LEG_SentAdjustAsscHst_Id", sequenceName = "SEQ_LEG_SentAdjustAsscHst_Id", allocationSize = 1)
    private Long historyId;

    /**
     * @DbComment AssociationId 'Id of Sentence Association, the table LEG_SentAdjustAssc'
     */
    @Column(name = "AssociationId", nullable = false)
    private Long associationId;

    /**
     * @DbComment ToClass 'Sentence to be associated'
     */
    @Column(name = "ToClass", nullable = false, length = 64)
    private String toClass;

    /**
     * @DbComment ToIdentifier 'Identification of Sentence'
     */
    @Column(name = "ToIdentifier", nullable = false)
    private Long toIdentifier;

    /**
     * @DbComment FromIdentifier 'Id of history of djustment, the table LEG_SentAdjmntHst'
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FromIdentifier", referencedColumnName = "adjustmentHstId", nullable = false)
    private AdjustmentHistEntity adjustment;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public SentenceAssocHistEntity() {
    }

    /**
     * @return the historyId
     */
    public Long getHistoryId() {
        return historyId;
    }

    /**
     * @param historyId the historyId to set
     */
    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public Long getAssociationId() {
        return associationId;
    }

    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    public String getToClass() {
        return toClass;
    }

    public void setToClass(String toClass) {
        this.toClass = toClass;
    }

    public Long getToIdentifier() {
        return toIdentifier;
    }

    public void setToIdentifier(Long toIdentifier) {
        this.toIdentifier = toIdentifier;
    }

    public AdjustmentHistEntity getAdjustment() {
        return adjustment;
    }

    public void setAdjustment(AdjustmentHistEntity adjustment) {
        this.adjustment = adjustment;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((associationId == null) ? 0 : associationId.hashCode());
        result = prime * result + ((historyId == null) ? 0 : historyId.hashCode());
        result = prime * result + ((toClass == null) ? 0 : toClass.hashCode());
        result = prime * result + ((toIdentifier == null) ? 0 : toIdentifier.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SentenceAssocHistEntity other = (SentenceAssocHistEntity) obj;
        if (associationId == null) {
            if (other.associationId != null) {
				return false;
			}
        } else if (!associationId.equals(other.associationId)) {
			return false;
		}
        if (historyId == null) {
            if (other.historyId != null) {
				return false;
			}
        } else if (!historyId.equals(other.historyId)) {
			return false;
		}
        if (toClass == null) {
            if (other.toClass != null) {
				return false;
			}
        } else if (!toClass.equals(other.toClass)) {
			return false;
		}
        if (toIdentifier == null) {
            if (other.toIdentifier != null) {
				return false;
			}
        } else if (!toIdentifier.equals(other.toIdentifier)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SentenceAssocHistEntity [historyId=" + historyId + ", associationId=" + associationId + ", toClass=" + toClass + ", toIdentifier=" + toIdentifier + "]";
    }

}
