package syscon.arbutus.product.services.legal.contract.dto;

import java.io.Serializable;

/**
 * The representation of the case search type.
 * Any search element that has a value is "AND" any other elements provided.
 *
 * @author lhan
 */
public class CaseSearchType implements Serializable {

    private static final long serialVersionUID = -8363991848232574805L;

    private Long supervisionId;
    private Long facilityId;
    private String caseIdentifierType;
    private String caseIdentifierValue;

    private boolean returnCaseType = false;

    //TODO: may extend more search criteria in future sprint.

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * At least one of the search criteria must be specified except isReturnCaseType.
     *
     * @return boolean - true if all required properties are set, false otherwise.
     */
    public boolean isWellFormed() {

        if (ValidationUtil.isEmpty(supervisionId) && ValidationUtil.isEmpty(facilityId) && ValidationUtil.isEmpty(caseIdentifierType) && ValidationUtil.isEmpty(
                caseIdentifierType)) {
			return false;
		}

        return true;
    }

    /**
     * Verify if properties are set properly.
     * At least one of the search criteria must be specified except isReturnCaseType.
     *
     * @return boolean - true if all required properties are set and all values are valid, false otherwise.
     */
    public boolean isValid() {

        if (!isWellFormed()) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the supervision property.
     *
     * @return Long - Reference to a Supervision period
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * Sets the value of the supervisionId property.
     *
     * @param supervision Long - Reference to a Supervision period.
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Gets the value of the facility property.
     *
     * @return Long - Indicates the CaseTypeoriginating facility name, type and location for a case. Static reference to the Facility service.
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * Sets the value of the facility property.
     *
     * @param facility Long - Indicates the originating facility name, type and location for a case. Static reference to the Facility service.
     */
    public void setFacility(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * @return the caseIdentifierType
     */
    public String getCaseIdentifierType() {
        return caseIdentifierType;
    }

    /**
     * A kind of identifier or number that can be associated to a criminal case e.g. Court Information Number, Indictment Number
     * The value should be one of the reference code in the CaseIdentifierType reference set.
     *
     * @param caseIdentifierType the caseIdentifierType to set
     */
    public void setCaseIdentifierType(String caseIdentifierType) {
        this.caseIdentifierType = caseIdentifierType;
    }

    /**
     * @return the caseIdentifierValue
     */
    public String getCaseIdentifierValue() {
        return caseIdentifierValue;
    }

    /**
     * The number or value of the case identifier e.g. 1234567S
     *
     * @param caseIdentifierValue the caseIdentifierValue to set
     */
    public void setCaseIdentifierValue(String caseIdentifierValue) {
        this.caseIdentifierValue = caseIdentifierValue;
    }

    /**
     * @return the returnCaseType
     */
    public boolean isReturnCaseType() {
        return returnCaseType;
    }

    /**
     * If false, return a List<{@link CaseInfoType}>;
     * If true, return a List<{@link CaseType}>;
     * Default value is false if not specified.
     *
     * @param returnCaseType the returnCaseType to set
     */
    public void setReturnCaseType(boolean returnCaseType) {
        this.returnCaseType = returnCaseType;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseSearchType [supervisionId=" + supervisionId + ", facilityId=" + facilityId + ", caseIdentifierType=" + caseIdentifierType + ", caseIdentifierValue="
                + caseIdentifierValue + ", returnCaseType=" + returnCaseType + "]";
    }

}
