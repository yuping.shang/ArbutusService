package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the charge appeal history entity.
 *
 * @DbComment LEG_CHGAppealHist 'The charge appeal history data table'
 */
//@Audited
@Entity
@Table(name = "LEG_CHGAppealHist")
public class ChargeAppealHistEntity implements Serializable {

    private static final long serialVersionUID = 7557088830908431433L;

    /**
     * @DbComment ChargeAppealHistoryId 'Unique id of a charge appeal history record of a charge instance'
     */
    @Id
    @Column(name = "ChargeAppealHistoryId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGAppealHist_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGAppealHist_Id", sequenceName = "SEQ_LEG_CHGAppealHist_Id", allocationSize = 1)
    private Long chargeAppealHistoryId;

    /**
     * @DbComment ChargeAppealId 'Unique id of a charge appeal record of a charge instance'
     */
    @Column(name = "ChargeAppealId", nullable = false)
    private Long chargeAppealId;

    /**
     * @DbComment AppealStatus 'A status of an appeal'
     */
    @Column(name = "AppealStatus", nullable = false, length = 64)
    private String appealStatus;

    /**
     * @DbComment AppealComment 'General comments on the outcome of the appeal.'
     */
    @Column(name = "AppealComment", nullable = true, length = 512)
    private String appealComment;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     *
     */
    public ChargeAppealHistEntity() {
    }

    /**
     * @param chargeAppealHistoryId
     * @param chargeAppealId
     * @param appealStatus
     * @param appealComment
     * @param stamp
     */
    public ChargeAppealHistEntity(Long chargeAppealHistoryId, Long chargeAppealId, String appealStatus, String appealComment, StampEntity stamp) {
        this.chargeAppealHistoryId = chargeAppealHistoryId;
        this.chargeAppealId = chargeAppealId;
        this.appealStatus = appealStatus;
        this.appealComment = appealComment;
        this.stamp = stamp;
    }

    /**
     * @return the chargeAppealHistoryId
     */
    public Long getChargeAppealHistoryId() {
        return chargeAppealHistoryId;
    }

    /**
     * @param chargeAppealHistoryId the chargeAppealHistoryId to set
     */
    public void setChargeAppealHistoryId(Long chargeAppealHistoryId) {
        this.chargeAppealHistoryId = chargeAppealHistoryId;
    }

    /**
     * @return the chargeAppealId
     */
    public Long getChargeAppealId() {
        return chargeAppealId;
    }

    /**
     * @param chargeAppealId the chargeAppealId to set
     */
    public void setChargeAppealId(Long chargeAppealId) {
        this.chargeAppealId = chargeAppealId;
    }

    /**
     * @return the appealStatus
     */
    public String getAppealStatus() {
        return appealStatus;
    }

    /**
     * @param appealStatus the appealStatus to set
     */
    public void setAppealStatus(String appealStatus) {
        this.appealStatus = appealStatus;
    }

    /**
     * @return the appealComment
     */
    public String getAppealComment() {
        return appealComment;
    }

    /**
     * @param appealComment the appealComment to set
     */
    public void setAppealComment(String appealComment) {
        this.appealComment = appealComment;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeAppealHistEntity [chargeAppealHistoryId=" + chargeAppealHistoryId + ", chargeAppealId=" + chargeAppealId + ", appealStatus=" + appealStatus
                + ", appealComment=" + appealComment + ", stamp=" + stamp + "]";
    }

}
