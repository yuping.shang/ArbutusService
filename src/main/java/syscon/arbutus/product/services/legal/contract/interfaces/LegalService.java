package syscon.arbutus.product.services.legal.contract.interfaces;

import java.util.List;
import java.util.Map;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldMetaType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueSearchType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.DTOBindingTypeEnum;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.StampType;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.legal.contract.dto.LegalModule;
import syscon.arbutus.product.services.legal.contract.dto.SearchSentenceTermType;
import syscon.arbutus.product.services.legal.contract.dto.SentenceTermNameType;
import syscon.arbutus.product.services.legal.contract.dto.SentenceTermType;

/**
 * Business contract interface for legal service.
 * <p>The following is an example snippet of Java code to demonstrate how this service operation needs to be invoked by a consumer Java application.  <br/>
 * </p>
 * <pre>
 * 	<li>public void createCase() {</li>
 * 	<li>	Properties p = new Properties();</li>
 * 	<li>	p.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");</li>
 * 	<li> 	p.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");</li>
 * 	<li>	p.put("java.naming.provider.url", "hostIpaddress:port"); //HA-JNDI binding server and port</li>
 * 	<li>	Context ctx = new InitialContext(p);</li>
 * 	<li>	LegalServiceBean legalService = (LegalServiceBean) ctx.lookup("LegalService");</li>
 * 	<li>	UserContext uc = new UserContext();</li>
 * 	<li>	Long facilityId = 100L;  //change to a real facility id</li>
 * 	<li>	uc.setFacilityId(facilityId);</li>
 * 	<li>	Long caseId = 1L;</li>
 * 	<li>	CaseType caseType = legalService.getCase(uc, caseId);</li>
 * 	<li>}</li>
 * <p>Class diagram:
 * <br/><img src="doc-files/Legal-classdiagram.JPG" />
 * </p>
 *
 * @author lhan
 * @version 1.0
 */
public interface LegalService extends CaseActivity, CaseInfo, Charge, Condition, OrderSentence, SentenceAdjustment, CaseAffiliation, Composite {

    /**
     * Cascading delete of all data.
     * Return success code 1 if all case activities, references and reference data deleted.
     * Throw RuntimeException in case of failure.
     * List of possible return codes:
     *
     * @param uc UserContext - Required
     * @return Long 1 if success; RuntimeException otherwise.
     */
    public Long deleteAll(UserContext uc);

    /**
     * <p>Return the stamp of a single instance.</p>
     *
     * @param uc         {@link UserContext}, required
     * @param module     the legal module defined in {@link LegalModule}, required
     * @param instanceId the instance identifier, required
     * @return Stamp
     * {@link StampType} object. Returns null if there is an error.
     */
    public StampType getStamp(UserContext uc, LegalModule module, Long instanceId);

    /**
     * Count of all instances of a legal module.
     * Data privileges applied if business required.
     * Returns null if there is an error during the count.
     * List of possible return codes:
     *
     * @param uc     UserContext - Required
     * @param module the legal module defined in {@link LegalModule}, required
     * @return java.lang.Long
     */
    public Long getCount(UserContext uc, LegalModule module);

    /**
     * Returns a set of all instance identifiers of a legal module.
     * Data privileges applied if business required.
     *
     * @param uc     UserContext - Required
     * @param module the legal module defined in {@link LegalModule}, required
     * @return Set<Long>
     */
    public Set<Long> getAll(UserContext uc, LegalModule module);

    /**
     * Create/Update the system wide currency setting.
     *
     * @param uc       {@link UserContext} - Required
     * @param currency String - a code of Currency Set. Not Null -- currency is set; Null -- No currency is set.
     * @return Returns CodeType the currency configuration instance.
     * Returns a null instance if currency is not set.
     */
    public CodeType setCurrencyConfig(UserContext uc, String currency);

    /**
     * Retrieve the system wide setting currency configuration.
     *
     * @param uc {@link UserContext} - Required
     * @return Returns CodeType the currency configuration instance and return code.
     * Returns a null if no currency is set
     */
    public CodeType getCurrencyConfig(UserContext uc);

    /**
     * Retrieve the version number in format "MajorVersionNumber.MinorVersionNumber"
     *
     * @param uc {@link UserContext} - required
     * @return version number
     */
    public String getVersion(UserContext uc);

    ///////////////////////////////////////////////////////////////////
    // Client Custom Field Interface
    ///////////////////////////////////////////////////////////////////

    /**
     * It is used to get CCF Meta for module(CaseInformation, Charge or OrderSentence) of Legal.
     * 1. LegalModule(CaseInformation, Charge or OrderSentence) need to be passed from user to get CCF.
     * 2. On the basis of LegalModule(CaseInformation, Charge or OrderSentence) CCF meta will be fetched from CCF Service.
     * 3. CCF service should return JSON string of MetaData.
     *
     * @param uc          {@link UserContext}, required
     * @param legalModule {@link DTOBindingTypeEnum}, required -- DTOBindingTypeEnum(CaseInformation, Charge or OrderSentence)
     * @param language    String, required -- Language
     * @return String JSON Meta Data
     * @throws ArbutusRuntimeException in case of failure
     */
    public String getCCFMeta(UserContext uc, DTOBindingTypeEnum legalModule, String language);

    /**
     * It is used to get CCF Meta for module(CaseInformation, Charge or OrderSentence) of Legal.
     * 1. LegalModule(CaseInformation, Charge or OrderSentence) needs to be passed from user to get CCF.
     * 2. On the basis of LegalModule(CaseInformation, Charge or OrderSentence) CCF meta will be fetched from CCF Service.
     * 3. CCF service should return ClientCustomFieldMetaType of MetaData.
     *
     * @param uc          {@link UserContext}, required
     * @param legalModule {@link DTOBindingTypeEnum}, required -- DTOBindingTypeEnum(CaseInformation, Charge or OrderSentence)
     * @param name        String, required -- Name of CCF for which Meta Data
     * @return ClientCustomFieldMetaType
     * @throws ArbutusRuntimeException in case of failure
     */
    public ClientCustomFieldMetaType getClientCustomFieldMeta(UserContext uc, DTOBindingTypeEnum legalModule, String name);

    /**
     * It is used to get All CCF Meta for a module(CaseInformation, Charge or OrderSentence) of legal.
     * 1. Module(CaseInformation, Charge or OrderSentence) of legal needs to be passed from user to get CCF.
     * 2. On the basis of LegalModule(CaseInformation, Charge or OrderSentence) CCF meta will be fetched from CCF Service.
     * 3. CCF service should return List of all CCF MetaData record for given LegalModule(CaseInformation, Charge or OrderSentence).
     *
     * @param uc          {@link UserContext} required
     * @param legalModule {@link DTOBindingTypeEnum}, required -- DTOBindingTypeEnum(CaseInformation, Charge or OrderSentence)
     * @return List of {@link ClientCustomFieldMetaType} instance
     * @throws ArbutusRuntimeException in case of failure
     */
    public List<ClientCustomFieldMetaType> getAllCCFMeta(UserContext uc, DTOBindingTypeEnum legalModule);

    /**
     * This method is used to save CCF Value for event log CCFs.
     *
     * @param uc       {@link UserContext} required
     * @param ccfValue {@link ClientCustomFieldValueType}, required
     * @return {@link ClientCustomFieldValueType} instance
     * @throws ArbutusRuntimeException in case of failure
     */
    public ClientCustomFieldValueType saveCCFMetaValue(UserContext uc, ClientCustomFieldValueType ccfValue);

    /**
     * This method is used to save CCF Value for event log CCFs.
     *
     * @param uc        {@link UserContext} required
     * @param ccfValues list of {@link ClientCustomFieldValueType}, required
     * @return list of {@link ClientCustomFieldValueType} instance
     * @throws ArbutusRuntimeException in case of failure
     */
    public List<ClientCustomFieldValueType> saveCCFMetaValues(UserContext uc, List<ClientCustomFieldValueType> ccfValues);

    /**
     * This method is used to fetch data values corresponding to CCF fields
     * depending upon given Search Criteria.
     *
     * @param uc         {@link UserContext} required
     * @param searchType {@link ClientCustomFieldValueSearchType}, required, at least one criteria must be specified.
     * @return List of {@link ClientCustomFieldValueType} instances
     * @throws ArbutusRuntimeException in case of failure
     */
    public List<ClientCustomFieldValueType> getCCFMetaValues(UserContext uc, ClientCustomFieldValueSearchType searchType);

    /**
     * This method is used to update CCF Meta Value
     *
     * @param uc       {@link UserContext}, required
     * @param ccfValue {@link ClientCustomFieldValueType}, required
     * @return ClientCustomFieldValueType
     * @throws ArbutusRuntimeException in case of failure
     */
    public ClientCustomFieldValueType updateCCFMetaValue(UserContext uc, ClientCustomFieldValueType ccfValue);

    /**
     * This method is used to update CCF meta values in Bulk.
     *
     * @param uc        {@link UserContext}, required
     * @param ccfValues List of {@link ClientCustomFieldValueType}, required
     * @return &lt;ClientCustomFieldValueType> - A list of ClientCustomFieldValueType
     * @throws ArbutusRuntimeException incase of failure.
     */
    public List<ClientCustomFieldValueType> updateCCFMetaValues(UserContext uc, List<ClientCustomFieldValueType> ccfValues);

    /**
     * Returns List of Primary CCF values for given ID binding set
     *
     * @param uc           {@link UserContext} instance
     * @param idBindingSet {@link Set <Long>}
     * @param dtoBinding   {@link DTOBindingTypeEnum}
     * @return
     */
    public Map<Long, List<ClientCustomFieldValueType>> getPrimaryCCFValue(UserContext uc, Set<Long> idBindingSet, DTOBindingTypeEnum dtoBinding);

    /**
     * fetch case identifiers for provided sentenceIds	 *
     *
     * @param uc          UserContext, required.
     * @param senteceList List<Long>,required
     * @return Map<Long, String> which contains key as caseId and Value as CaseIdentifiervalue
     */
    public Map<Long, String> getCaseInfoForSentence(UserContext uc, List<Long> caseList);

    /**
     * create SentenceTermType	 *
     *
     * @param uc               UserContext, required.
     * @param sentenceTermType SentenceTermType ,required
     * @return SentenceTermType
     */
    public SentenceTermType createSentenceTerm(UserContext uc, SentenceTermType sentenceTermType);

    /**
     * update SentenceTermType	 *
     *
     * @param uc               UserContext, required.
     * @param sentenceTermType SentenceTermType ,required
     * @return SentenceTermType
     */
    public SentenceTermType updateSentenceTerm(UserContext uc, SentenceTermType sentenceTermType);

    /**
     * get SentenceTermType	for a given id *
     *
     * @param uc             UserContext, required.
     * @param sentenceTermId required
     * @return SentenceTermType
     */
    public SentenceTermType getSentenceTerm(UserContext uc, Long sentenceTermId);

    /**
     * get List of all SentenceTermType*
     *
     * @param uc          UserContext, required.
     * @param startIndex
     * @param resultSize
     * @param resultOrder
     * @return List<SentenceTermType>
     */
    public List<SentenceTermType> getAllSentenceTerm(UserContext uc, Long startIndex, Long resultSize, String resultOrder);

    /**
     * get List of all SentenceTermType*
     *
     * @param uc          UserContext, required
     * @param searchType  SearchSentenceTermType, required
     * @param startIndex
     * @param resultSize
     * @param resultOrder
     * @return List<SentenceTermType>
     */
    public List<SentenceTermType> searchSentenceTerm(UserContext uc, SearchSentenceTermType searchType, Long startIndex, Long resultSize, String resultOrder);

    /**
     * create SentenceTermTypeName	 *
     *
     * @param uc               UserContext, required.
     * @return SentenceTermTypeName
     */
    public SentenceTermNameType createSentenceTermName(UserContext uc, SentenceTermNameType sentenceTermTypeName);

    public boolean isChargeByOrganization(UserContext uc, Long organizationId);

    public boolean isCaseByOrganization(UserContext uc, Long organizationId);

    public boolean isCaseAffiliationByOrganization(UserContext uc, Long organizationId);

    /**
     * To check any open legal documents are active for the given Supervision
     * @param uc
     * @param supervisionId
     * @param isActive
     * @return
     */
    public boolean hasActiveLegalDocumentsForSupervision(UserContext uc, Long supervisionId, Boolean isActive);
}
