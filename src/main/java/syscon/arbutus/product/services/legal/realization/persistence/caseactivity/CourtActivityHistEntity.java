package syscon.arbutus.product.services.legal.realization.persistence.caseactivity;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the CourtActivity history entity.
 *
 * @author lhan
 */
//@Audited
@Entity
@DiscriminatorValue("CourtActHist")
public class CourtActivityHistEntity extends CaseActivityHistEntity {

    private static final long serialVersionUID = -1430043545279597904L;

    /**
     * @DbComment LEG_CACaseActHist.FacilityAssoc 'Indicates the facility where the hearing will take place.'
     */
    @Column(name = "FacilityAssoc", nullable = true)
    private Long facilityAssociation;

    /**
     * @DbComment LEG_CACaseActHist.FacilityInternalId 'Indicates the facility internal location where the hearing will take place.'
     */
    @Column(name = "FacilityInternalId", nullable = true)
    private Long facilityInternalAssociation;

    /**
     * @DbComment LEG_CACaseActHist.OrganizationAssoc 'Indicates the organization where the hearing will take place.'
     */
    @Column(name = "OrganizationAssoc", nullable = true)
    private Long organizationAssociation;

    /**
     * @DbComment LEG_CACaseActHist.Judge 'Indicates the judge who will be presiding over the court event if applicable.'
     */
    @Column(name = "Judge", nullable = true, length = 128)
    private String judge;

    @ForeignKey(name = "Fk_LEG_CAOrderInitAssocHist")
    @OneToMany(mappedBy = "caseActivity", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<OrderInitiatedAssociationHistEntity> orderInititatedAssociations;

    @ForeignKey(name = "Fk_LEG_CAOrderResultedAsscHist")
    @OneToMany(mappedBy = "caseActivity", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<OrderResultedAssociationHistEntity> orderResultedAssociations;

    /**
     * Default constructor
     */
    public CourtActivityHistEntity() {

    }

    /**
     * Constructor
     *
     * @param caseActivityId
     * @param activityOccurredDate
     * @param activityCategory
     * @param activityType
     * @param activityOutcome
     * @param outcomeDate
     * @param activityStatus
     * @param comment
     * @param commentDate
     * @param chargeAssociations
     * @param associations
     * @param privileges
     * @param stamp
     * @param facilityAssociation
     * @param organizationAssociation
     * @param judge
     * @param orderInititatedAssociations
     * @param orderResultedAssociations
     */
    public CourtActivityHistEntity(Long caseActivityId, Date activityOccurredDate, String activityCategory, String activityType, String activityOutcome, Date outcomeDate,
            String activityStatus, String comment, Date commentDate, Set<CAChargeAssociationHistEntity> chargeAssociations, StampEntity stamp, Long facilityAssociation,
            Long facilityInternalAssociation, Long organizationAssociation, String judge, Set<OrderInitiatedAssociationHistEntity> orderInititatedAssociations,
            Set<OrderResultedAssociationHistEntity> orderResultedAssociations, CaseActivityEntity caseActilvity) {
        super(caseActivityId, activityOccurredDate, activityCategory, activityType, activityOutcome, outcomeDate, activityStatus, comment, commentDate,
                chargeAssociations, caseActilvity, stamp);

        this.facilityAssociation = facilityAssociation;
        this.facilityInternalAssociation = facilityInternalAssociation;
        this.organizationAssociation = organizationAssociation;
        this.judge = judge;
        this.orderInititatedAssociations = orderInititatedAssociations;
        this.orderResultedAssociations = orderResultedAssociations;
    }

    /**
     * @return the facilityAssociation
     */
    public Long getFacilityAssociation() {
        return facilityAssociation;
    }

    /**
     * @param facilityAssociation the facilityAssociation to set
     */
    public void setFacilityAssociation(Long facilityAssociation) {
        this.facilityAssociation = facilityAssociation;
    }

    /**
     * @return the facilityInternalAssociation
     */
    public Long getFacilityInternalAssociation() {
        return facilityInternalAssociation;
    }

    /**
     * @param facilityInternalAssociation the facilityInternalAssociation to set
     */
    public void setFacilityInternalAssociation(Long facilityInternalAssociation) {
        this.facilityInternalAssociation = facilityInternalAssociation;
    }

    /**
     * @return the organizationAssociation
     */
    public Long getOrganizationAssociation() {
        return organizationAssociation;
    }

    /**
     * @param organizationAssociation the organizationAssociation to set
     */
    public void setOrganizationAssociation(Long organizationAssociation) {
        this.organizationAssociation = organizationAssociation;
    }

    /**
     * @return the judge
     */
    public String getJudge() {
        return judge;
    }

    /**
     * @param judge the judge to set
     */
    public void setJudge(String judge) {
        this.judge = judge;
    }

    /**
     * @return the orderInititatedAssociations
     */
    public Set<OrderInitiatedAssociationHistEntity> getOrderInititatedAssociations() {
        return orderInititatedAssociations;
    }

    /**
     * @param orderInititatedAssociations the orderInititatedAssociations to set
     */
    public void setOrderInititatedAssociations(Set<OrderInitiatedAssociationHistEntity> orderInititatedAssociations) {
        this.orderInititatedAssociations = orderInititatedAssociations;
    }

    public Set<OrderResultedAssociationHistEntity> getOrderResultedAssociations() {
        return orderResultedAssociations;
    }

    public void setOrderResultedAssociations(Set<OrderResultedAssociationHistEntity> orderResultedAssociations) {
        this.orderResultedAssociations = orderResultedAssociations;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CourtActivityHistEntity [facilityAssociation=" + facilityAssociation + ", facilityInternalAssociation=" + facilityInternalAssociation
                + ", organizationAssociation=" + organizationAssociation + ", judge=" + judge + ", orderInititatedAssociations=" + orderInititatedAssociations
                + ", orderResultedAssociations=" + orderResultedAssociations + ", toString()=" + super.toString() + "]";
    }
}
