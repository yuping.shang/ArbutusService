package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

/**
 * DispositionSearchType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 21, 2012
 */
@ArbutusConstraint(constraints = { "fromDispositionDate <= toDispositionDate" })
public class DispositionSearchType implements Serializable {

    private static final long serialVersionUID = -737931375117512739L;

    private String dispositionOutcome;
    private String orderStatus;
    private Date fromDispositionDate;
    private Date toDispositionDate;

    /**
     * Constructor
     */
    public DispositionSearchType() {
        super();
    }

    /**
     * Constructor
     * <p>At least one argument must be provided.
     *
     * @param dispositionOutcome  String. A result or outcome of the disposition
     * @param orderStatus         String. The status of an order, active or inactive.
     * @param fromDispositionDate Date. Date of disposition from.
     * @param toDispositionDate   Date. Date of disposition to.
     */
    public DispositionSearchType(String dispositionOutcome, String orderStatus, Date fromDispositionDate, Date toDispositionDate) {
        super();
        this.dispositionOutcome = dispositionOutcome;
        this.orderStatus = orderStatus;
        this.fromDispositionDate = fromDispositionDate;
        this.toDispositionDate = toDispositionDate;
    }

    /**
     * Get Disposition Outcome
     * -- A result or outcome of the disposition
     *
     * @return the dispositionOutcome
     */
    public String getDispositionOutcome() {
        return dispositionOutcome;
    }

    /**
     * Set Disposition Outcome
     * -- A result or outcome of the disposition
     *
     * @param dispositionOutcome the dispositionOutcome to set
     */
    public void setDispositionOutcome(String dispositionOutcome) {
        this.dispositionOutcome = dispositionOutcome;
    }

    /**
     * Get Order Status
     * -- The status of an order, active or inactive.
     *
     * @return the orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }

    /**
     * Set Order Status
     * -- The status of an order, active or inactive.
     *
     * @param orderStatus the orderStatus to set
     */
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * Get From Disposition Date
     * -- Date of disposition from
     *
     * @return the fromDispositionDate
     */
    public Date getFromDispositionDate() {
        return fromDispositionDate;
    }

    /**
     * Set From Disposition Date
     * -- Date of disposition from
     *
     * @param fromDispositionDate the fromDispositionDate to set
     */
    public void setFromDispositionDate(Date fromDispositionDate) {
        this.fromDispositionDate = fromDispositionDate;
    }

    /**
     * Get From Disposition Date
     * -- Date of disposition to
     *
     * @return the toDispositionDate
     */
    public Date getToDispositionDate() {
        return toDispositionDate;
    }

    /**
     * Set From Disposition Date
     * -- Date of disposition to
     *
     * @param toDispositionDate the toDispositionDate to set
     */
    public void setToDispositionDate(Date toDispositionDate) {
        this.toDispositionDate = toDispositionDate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dispositionOutcome == null) ? 0 : dispositionOutcome.hashCode());
        result = prime * result + ((fromDispositionDate == null) ? 0 : fromDispositionDate.hashCode());
        result = prime * result + ((orderStatus == null) ? 0 : orderStatus.hashCode());
        result = prime * result + ((toDispositionDate == null) ? 0 : toDispositionDate.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        DispositionSearchType other = (DispositionSearchType) obj;
        if (dispositionOutcome == null) {
            if (other.dispositionOutcome != null) {
				return false;
			}
        } else if (!dispositionOutcome.equals(other.dispositionOutcome)) {
			return false;
		}
        if (fromDispositionDate == null) {
            if (other.fromDispositionDate != null) {
				return false;
			}
        } else if (!fromDispositionDate.equals(other.fromDispositionDate)) {
			return false;
		}
        if (orderStatus == null) {
            if (other.orderStatus != null) {
				return false;
			}
        } else if (!orderStatus.equals(other.orderStatus)) {
			return false;
		}
        if (toDispositionDate == null) {
            if (other.toDispositionDate != null) {
				return false;
			}
        } else if (!toDispositionDate.equals(other.toDispositionDate)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DispositionSearchType [dispositionOutcome=" + dispositionOutcome + ", orderStatus=" + orderStatus + ", fromDispositionDate=" + fromDispositionDate
                + ", toDispositionDate=" + toDispositionDate + "]";
    }

}
