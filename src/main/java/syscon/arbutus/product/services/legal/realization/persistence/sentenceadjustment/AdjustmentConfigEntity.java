package syscon.arbutus.product.services.legal.realization.persistence.sentenceadjustment;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * AdjustmentConfigEntity for Sentence Adjustment
 *
 * @DbComment LEG_SentAdjCfg 'Table of Sentence Adjustment'
 * User: yshang
 * Date: 03/10/13
 * Time: 9:52 AM
 */
@Audited
@Entity
@Table(name = "LEG_SentAdjCfg")
public class AdjustmentConfigEntity implements Serializable {

    private static final long serialVersionUID = -2414508987657318967L;

    /**
     * @DbComment adjustConfigId 'Id of Sentence Adjustment table'
     */
    @Id
    @Column(name = "adjustConfigId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_SENTADJCFG_ID")
    @SequenceGenerator(name = "SEQ_LEG_SENTADJCFG_ID", sequenceName = "SEQ_LEG_SENTADJCFG_ID", allocationSize = 1)
    private Long adjustConfigId;

    /**
     * @DbComment classification 'Reference code, SYSG or MANUAL, of reference set ADJUSTMENT CLASSIFICATION'
     */
    @Column(name = "classification", length = 64, nullable = false)
    @MetaCode(set = MetaSet.ADJUSTMENT_CLASSIFICATION)
    private String classification;

    /**
     * @DbComment applicationType 'Application Type: ASENT--Single Sentence; AGGSENT--Aggregated Sentence; BOOKLEV: Booking Level.'
     */
    @Column(name = "applicationType", length = 64, nullable = true)
    @MetaCode(set = MetaSet.APPLICATION_TYPE)
    private String applicationType;

    /**
     * @DbComment adjustmentType 'Adjustment Type: GT, JT, LGT, WORKCDT, LWRKCRD, PRGCDT, ESCP, OICDBT, WKNCDT, STATCDT'
     */
    @Column(name = "adjustmentType", length = 64, nullable = false)
    @MetaCode(set = MetaSet.ADJUSTMENT_TYPE)
    private String adjustmentType;

    /**
     * @DbComment adjustmentFunc 'Adjustment Function: CRE, DEB'
     */
    @Column(name = "adjustmentFunc", length = 64, nullable = false)
    @MetaCode(set = MetaSet.ADJUSTMENT_FUNCTION)
    private String adjustmentFunction;

    /**
     * @DbComment adjustmentStatus 'Adjustment Status: INCL, EXCL, PEND'
     */
    @Column(name = "adjustmentStatus", length = 64, nullable = false)
    @MetaCode(set = MetaSet.ADJUSTMENT_STATUS)
    private String adjustmentStatus;

    /**
     * @DbComment specialAdjustment 'Special Adjustment: the days to be adjusted'
     */
    @Column(name = "specialAdjustment", nullable = true)
    private Long specialAdjustment;

    /**
     * @DbComment comments 'Adjustment Comment'
     */
    @Column(name = "comments", length = 512, nullable = true)
    private String comment;

    /**
     * @DbComment byStaffId 'Staff Id'
     */
    @Column(name = "byStaffId", nullable = true)
    private Long byStaffId;

    /**
     * @DbComment version 'Version for Hibernate use'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public AdjustmentConfigEntity() {
    }

    public Long getAdjustConfigId() {
        return adjustConfigId;
    }

    public void setAdjustConfigId(Long adjustConfigId) {
        this.adjustConfigId = adjustConfigId;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    public String getAdjustmentType() {
        return adjustmentType;
    }

    public void setAdjustmentType(String adjustmentType) {
        this.adjustmentType = adjustmentType;
    }

    public String getAdjustmentFunction() {
        return adjustmentFunction;
    }

    public void setAdjustmentFunction(String adjustmentFunction) {
        this.adjustmentFunction = adjustmentFunction;
    }

    public String getAdjustmentStatus() {
        return adjustmentStatus;
    }

    public void setAdjustmentStatus(String adjustmentStatus) {
        this.adjustmentStatus = adjustmentStatus;
    }

    public Long getSpecialAdjustment() {
        return specialAdjustment;
    }

    public void setSpecialAdjustment(Long specialAdjustment) {
        this.specialAdjustment = specialAdjustment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getByStaffId() {
        return byStaffId;
    }

    public void setByStaffId(Long byStaffId) {
        this.byStaffId = byStaffId;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (!(o instanceof AdjustmentConfigEntity)) {
			return false;
		}

        AdjustmentConfigEntity that = (AdjustmentConfigEntity) o;

        if (adjustConfigId != null ? !adjustConfigId.equals(that.adjustConfigId) : that.adjustConfigId != null) {
			return false;
		}
        if (adjustmentFunction != null ? !adjustmentFunction.equals(that.adjustmentFunction) : that.adjustmentFunction != null) {
			return false;
		}
        if (adjustmentStatus != null ? !adjustmentStatus.equals(that.adjustmentStatus) : that.adjustmentStatus != null) {
			return false;
		}
        if (adjustmentType != null ? !adjustmentType.equals(that.adjustmentType) : that.adjustmentType != null) {
			return false;
		}
        if (applicationType != null ? !applicationType.equals(that.applicationType) : that.applicationType != null) {
			return false;
		}
        if (classification != null ? !classification.equals(that.classification) : that.classification != null) {
			return false;
		}
        if (specialAdjustment != null ? !specialAdjustment.equals(that.specialAdjustment) : that.specialAdjustment != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = adjustConfigId != null ? adjustConfigId.hashCode() : 0;
        result = 31 * result + (classification != null ? classification.hashCode() : 0);
        result = 31 * result + (applicationType != null ? applicationType.hashCode() : 0);
        result = 31 * result + (adjustmentType != null ? adjustmentType.hashCode() : 0);
        result = 31 * result + (adjustmentFunction != null ? adjustmentFunction.hashCode() : 0);
        result = 31 * result + (adjustmentStatus != null ? adjustmentStatus.hashCode() : 0);
        result = 31 * result + (specialAdjustment != null ? specialAdjustment.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AdjustmentConfigEntity{" +
                "adjustConfigId=" + adjustConfigId +
                ", classification='" + classification + '\'' +
                ", applicationType='" + applicationType + '\'' +
                ", adjustmentType='" + adjustmentType + '\'' +
                ", adjustmentFunction='" + adjustmentFunction + '\'' +
                ", adjustmentStatus='" + adjustmentStatus + '\'' +
                ", specialAdjustment=" + specialAdjustment +
                ", comment='" + comment + '\'' +
                ", byStaffId=" + byStaffId +
                ", version=" + version +
                ", stamp=" + stamp +
                '}';
    }
}
