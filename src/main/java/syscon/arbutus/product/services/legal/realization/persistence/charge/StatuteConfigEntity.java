package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the statute configuration entity.
 *
 * @DbComment LEG_CHGStatuteConfig 'The statute configuration data table'
 */
@Audited
@Entity
@Table(name = "LEG_CHGStatuteConfig")
public class StatuteConfigEntity implements Serializable {

    private static final long serialVersionUID = -2294150783899869086L;

    /**
     * @DbComment StatuteId 'The unique Id for each statute configuration data record.'
     */
    @Id
    @Column(name = "StatuteId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGStatuteConfig_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGStatuteConfig_Id", sequenceName = "SEQ_LEG_CHGStatuteConfig_Id", allocationSize = 1)
    private Long statuteId;

    /**
     * @DbComment StatuteCode 'An identifier of a set of laws for a particular jurisdiction'
     */
    @Column(name = "StatuteCode", nullable = false, length = 64)
    private String statuteCode;

    /**
     * @DbComment StatuteSection 'An identifier of a section or category within a code book'
     */
    @Column(name = "StatuteSection", nullable = true, length = 64)
    private String statuteSection;

    /**
     * @DbComment Description 'A description of a statute.'
     */
    @Column(name = "Description", nullable = true, length = 128)
    private String description;

    /**
     * @DbComment EnactmentDate 'A date a statute was enacted and came into effect.'
     */
    @Column(name = "EnactmentDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date enactmentDate;

    /**
     * @DbComment RepealDate 'A date a statute was repealed and no longer applies.'
     */
    @Column(name = "RepealDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date repealDate;

    /**
     * @DbComment JurisdictionId 'the unique jurisdiction id, refer to Jurisdiction Configuration table'
     */
    @Column(name = "JurisdictionId", nullable = false)
    private Long jurisdictionId;

    /**
     * @DbComment DefaultJurisdiction 'True if the charge codes default to this Statute, false otherwise, default is false'
     */
    @Column(name = "DefaultJurisdiction", nullable = false)
    private Boolean defaultJurisdiction;

    /**
     * @DbComment OutOfJurisdiction 'If this statute is outside the jurisdiction true, false otherwise, default is false'
     */
    @Column(name = "OutOfJurisdiction", nullable = false)
    private Boolean outOfJurisdiction;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     *
     */
    public StatuteConfigEntity() {
    }

    /**
     * @param statuteId
     * @param statuteCode
     * @param enactmentDate
     * @param repealDate
     * @param statuteSection
     * @param description
     * @param jurisdictionId
     * @param defaultJurisdiction
     * @param outOfJurisdiction
     */
    public StatuteConfigEntity(Long statuteId, String statuteCode, Date enactmentDate, Date repealDate, String statuteSection, String description, Long jurisdictionId,
            Boolean defaultJurisdiction, Boolean outOfJurisdiction) {
        this.statuteId = statuteId;
        this.statuteCode = statuteCode;
        this.statuteSection = statuteSection;
        this.description = description;
        this.enactmentDate = enactmentDate;
        this.repealDate = repealDate;
        this.jurisdictionId = jurisdictionId;
        this.defaultJurisdiction = defaultJurisdiction;
        this.outOfJurisdiction = outOfJurisdiction;
    }

    /**
     * @return the statuteId
     */
    public Long getStatuteId() {
        return statuteId;
    }

    /**
     * @param statuteId the statuteId to set
     */
    public void setStatuteId(Long statuteId) {
        this.statuteId = statuteId;
    }

    /**
     * @return the statuteCode
     */
    public String getStatuteCode() {
        return statuteCode;
    }

    /**
     * @param statuteCode the statuteCode to set
     */
    public void setStatuteCode(String statuteCode) {
        this.statuteCode = statuteCode;
    }

    /**
     * @return the statuteSection
     */
    public String getStatuteSection() {
        return statuteSection;
    }

    /**
     * @param statuteSection the statuteSection to set
     */
    public void setStatuteSection(String statuteSection) {
        this.statuteSection = statuteSection;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the enactmentDate
     */
    public Date getEnactmentDate() {
        return enactmentDate;
    }

    /**
     * @param enactmentDate the enactmentDate to set
     */
    public void setEnactmentDate(Date enactmentDate) {
        this.enactmentDate = enactmentDate;
    }

    /**
     * @return the repealDate
     */
    public Date getRepealDate() {
        return repealDate;
    }

    /**
     * @param repealDate the repealDate to set
     */
    public void setRepealDate(Date repealDate) {
        this.repealDate = repealDate;
    }

    /**
     * @return the jurisdictionId
     */
    public Long getJurisdictionId() {
        return jurisdictionId;
    }

    /**
     * @param jurisdictionId the jurisdictionId to set
     */
    public void setJurisdictionId(Long jurisdictionId) {
        this.jurisdictionId = jurisdictionId;
    }

    /**
     * @return the defaultJurisdiction
     */
    public Boolean isDefaultJurisdiction() {
        return defaultJurisdiction;
    }

    /**
     * @param defaultJurisdiction the defaultJurisdiction to set
     */
    public void setDefaultJurisdiction(Boolean defaultJurisdiction) {
        this.defaultJurisdiction = defaultJurisdiction;
    }

    /**
     * @return the outOfJurisdiction
     */
    public Boolean isOutOfJurisdiction() {
        return outOfJurisdiction;
    }

    /**
     * @param outOfJurisdiction the outOfJurisdiction to set
     */
    public void setOutOfJurisdiction(Boolean outOfJurisdiction) {
        this.outOfJurisdiction = outOfJurisdiction;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StatuteConfigEntity [statuteId=" + statuteId + ", statuteCode=" + statuteCode + ", statuteSection=" + statuteSection + ", description=" + description
                + ", enactmentDate=" + enactmentDate + ", repealDate=" + repealDate + ", jurisdictionId=" + jurisdictionId + ", defaultJurisdiction="
                + defaultJurisdiction + ", outOfJurisdiction=" + outOfJurisdiction + "]";
    }

}
