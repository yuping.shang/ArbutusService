package syscon.arbutus.product.services.legal.contract.dto.charge;

import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * The representation of the charge search type
 */
public class ChargeSearchType implements Serializable {

    private static final long serialVersionUID = 1404237989803031513L;

    private StatuteChargeConfigSearchType statuteChargeConfigSearch;
    private ChargeIdentifierSearchType chargeIdentifierSearch;
    private ChargeIndicatorSearchType chargeIndicatorSearch;
    private String legalText;
    private ChargeDispositionSearchType chargeDispositionSearch;
    private ChargePleaSearchType chargePleaSearch;
    private ChargeAppealSearchType chargeAppealSearch;
    private Long chargeCount;
    private Long facilityId;
    private Long organizationId;
    private Long personIdentityId;
    private Date offenceStartDateFrom;
    private Date offenceStartDateTo;
    private Date offenceEndDateFrom;
    private Date offenceEndDateTo;
    private Date filingDateFrom;
    private Date filingDateTo;
    private Boolean primary;
    private Long sentenceId;

    private CommentType comment;

    /**
     * Default constructor
     */
    public ChargeSearchType() {
    }

    /**
     * Constructor
     * At least one of the search criteria must be specified.
     *
     * @param statuteChargeConfigSearch StatuteChargeConfigSearchType
     * @param chargeIdentifierSearch    ChargeIdentifierSearchType
     * @param chargeIndicatorSearch     ChargeIndicatorSearchType
     * @param legalText                 String - legal text for a charge
     * @param chargeDispositionSearch   ChargeDispositionSearchType
     * @param chargePleaSearch          ChargePleaSearchType
     * @param chargeAppealSearch        ChargeAppealSearchType
     * @param chargeCount               Long
     * @param facilityId                AssociationType - facility Id, static reference to Facility service
     * @param organizationId            AssociationType - organization Id, static reference to Organization service
     * @param personIdentityId          AssociationType - person identity Id, static reference to PersonIdentity service
     * @param offenceStartDateFrom      Date - the date of the offence was committed
     * @param offenceStartDateTo        Date - the date of the offence was committed
     * @param offenceEndDateFrom        Date - field is used when the exact offence date is not known, to reflect an offence committed date range.
     * @param offenceEndDateTo          Date - field is used when the exact offence date is not known, to reflect an offence committed date range.
     * @param filingDateFrom            Date - the date of the charge was filed.
     * @param filingDateTo              Date - the date of the charge was filed.
     * @param primary                   Boolean - true if the charge is a serious charge, false otherwise.
     * @param sentenceId                Long - Id of Sentence which is associated to.
     * @param comment
     */
    public ChargeSearchType(StatuteChargeConfigSearchType statuteChargeConfigSearch, ChargeIdentifierSearchType chargeIdentifierSearch,
            ChargeIndicatorSearchType chargeIndicatorSearch, String legalText, ChargeDispositionSearchType chargeDispositionSearch, ChargePleaSearchType chargePleaSearch,
            ChargeAppealSearchType chargeAppealSearch, Long chargeCount, Long facilityId, Long organizationId, Long personIdentityId, Date offenceStartDateFrom,
            Date offenceStartDateTo, Date offenceEndDateFrom, Date offenceEndDateTo, Date filingDateFrom, Date filingDateTo, Boolean primary, Long sentenceId,
            CommentType comment) {
        this.statuteChargeConfigSearch = statuteChargeConfigSearch;
        this.chargeIdentifierSearch = chargeIdentifierSearch;
        this.chargeIndicatorSearch = chargeIndicatorSearch;
        this.legalText = legalText;
        this.chargeDispositionSearch = chargeDispositionSearch;
        this.chargePleaSearch = chargePleaSearch;
        this.chargeAppealSearch = chargeAppealSearch;
        this.chargeCount = chargeCount;
        this.facilityId = facilityId;
        this.organizationId = organizationId;
        this.personIdentityId = personIdentityId;
        this.offenceStartDateFrom = offenceStartDateFrom;
        this.offenceStartDateTo = offenceStartDateTo;
        this.offenceEndDateFrom = offenceEndDateFrom;
        this.offenceEndDateTo = offenceEndDateTo;
        this.filingDateFrom = filingDateFrom;
        this.filingDateTo = filingDateTo;
        this.primary = primary;
        this.sentenceId = sentenceId;
        this.comment = comment;
    }

    /**
     * Gets the value of the statuteChargeConfigSearch property.
     *
     * @return StatuteChargeConfigSearchType - The statute and its charge details.
     */
    public StatuteChargeConfigSearchType getStatuteChargeConfigSearch() {
        return statuteChargeConfigSearch;
    }

    /**
     * Sets the value of the statuteChargeConfigSearch property.
     *
     * @param statuteChargeConfigSearch StatuteChargeConfigSearchType - The statute and its charge details.
     */
    public void setStatuteChargeConfigSearch(StatuteChargeConfigSearchType statuteChargeConfigSearch) {
        this.statuteChargeConfigSearch = statuteChargeConfigSearch;
    }

    /**
     * Gets the value of the chargeIdentifierSearch property.
     *
     * @return ChargeIdentifierSearchType - the numbers/identifiers assigned by an arresting agency, prosecuting attorney or court.
     */
    public ChargeIdentifierSearchType getChargeIdentifierSearch() {
        return chargeIdentifierSearch;
    }

    /**
     * Sets the value of the chargeIdentifierSearch property.
     *
     * @param chargeIdentifierSearch - the numbers/identifiers assigned by an arresting agency, prosecuting attorney or court.
     */
    public void setChargeIdentifierSearch(ChargeIdentifierSearchType chargeIdentifierSearch) {
        this.chargeIdentifierSearch = chargeIdentifierSearch;
    }

    /**
     * Gets the value of the chargeIndicatorsSearch property.
     *
     * @return ChargeIndicatorSearchType - the indicators (alerts) on a charge
     */
    public ChargeIndicatorSearchType getChargeIndicatorSearch() {
        return chargeIndicatorSearch;
    }

    /**
     * Sets the value of the chargeIndicators property.
     *
     * @param chargeIndicatorSearch ChargeIndicatorSearchType - the indicators (alerts) on a charge
     */
    public void setChargeIndicatorsSearch(ChargeIndicatorSearchType chargeIndicatorSearch) {
        this.chargeIndicatorSearch = chargeIndicatorSearch;
    }

    /**
     * Gets the value of the legalText property.
     *
     * @return String - Legal text for a charge
     */
    public String getLegalText() {
        return legalText;
    }

    /**
     * Sets the value of the legalText property.
     *
     * @param legalText String - Legal text for a charge
     */
    public void setLegalText(String legalText) {
        this.legalText = legalText;
    }

    /**
     * Gets the value of the chargeDispositionSearch property.
     *
     * @return ChargeDispositionSearchType - Disposition details for a charge
     */
    public ChargeDispositionSearchType getChargeDispositionSearch() {
        return chargeDispositionSearch;
    }

    /**
     * Sets the value of the chargeDispositionSearch property.
     *
     * @param chargeDispositionSearch ChargeDispositionSearchType - Disposition details for a charge
     */
    public void setChargeDispositionSearch(ChargeDispositionSearchType chargeDispositionSearch) {
        this.chargeDispositionSearch = chargeDispositionSearch;
    }

    /**
     * Gets the value of the chargePleaSearch property.
     *
     * @return ChargePleaSearchType - Plea details for the charge
     */
    public ChargePleaSearchType getChargePleaSearch() {
        return chargePleaSearch;
    }

    /**
     * Sets the value of the chargePleaSearch property.
     *
     * @param chargePleaSearch ChargePleaSearchType - Plea details for the charge
     */
    public void setChargePleaSearch(ChargePleaSearchType chargePleaSearch) {
        this.chargePleaSearch = chargePleaSearch;
    }

    /**
     * Gets the value of the chargeAppealSearch property.
     *
     * @return ChargeAppealSearchType - Appeal details for a charge
     */
    public ChargeAppealSearchType getChargeAppealSearch() {
        return chargeAppealSearch;
    }

    /**
     * Sets the value of the chargeAppealSearch property.
     *
     * @param chargeAppealSearch ChargeAppealSearchType - Appeal details for a charge
     */
    public void setChargeAppealSearch(ChargeAppealSearchType chargeAppealSearch) {
        this.chargeAppealSearch = chargeAppealSearch;
    }

    /**
     * Gets the value of the chargeCount property.
     *
     * @return Long - A number of times a person is charged with committing the same crime.
     */
    public Long getChargeCount() {
        return chargeCount;
    }

    /**
     * Sets the value of the chargeCount property.
     *
     * @param chargeCount Long - A number of times a person is charged with committing the same crime.
     */
    public void setChargeCount(Long chargeCount) {
        this.chargeCount = chargeCount;
    }

    /**
     * Gets the value of the facility property.
     *
     * @return Long - Static Reference to the facility entity that filed the charge
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * Sets the value of the facility property.
     *
     * @param facilityId Long - Static Reference to the facility entity that filed the charge
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * Gets the value of the organization property.
     *
     * @return Long - Static Reference to the organization entity that filed the charge
     */
    public Long getOrganizationId() {
        return organizationId;
    }

    /**
     * Sets the value of the organization property.
     *
     * @param organizationId Long - Static Reference to the organization entity that filed the charge
     */
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * Gets the value of the personIdentity property.
     *
     * @return Long - Static Reference to the Person entity that filed the charge
     */
    public Long getPersonIdentityId() {
        return personIdentityId;
    }

    /**
     * Sets the value of the personIdentity property.
     *
     * @param personIdentityId Long - Static Reference to the Person entity that filed the charge
     */
    public void setPersonIdentityId(Long personIdentityId) {
        this.personIdentityId = personIdentityId;
    }

    /**
     * Gets the value of the offenceStartDateFrom property.
     *
     * @return Date - the date of the offence was committed
     */
    public Date getOffenceStartDateFrom() {
        return offenceStartDateFrom;
    }

    /**
     * Sets the value of the offenceStartDateFrom property.
     *
     * @param offenceStartDateFrom Date - the date of the offence was committed
     */
    public void setOffenceStartDateFrom(Date offenceStartDateFrom) {
        this.offenceStartDateFrom = offenceStartDateFrom;
    }

    /**
     * Gets the value of the offenceStartDateTo property.
     *
     * @return Date - the date of the offence was committed
     */
    public Date getOffenceStartDateTo() {
        return offenceStartDateTo;
    }

    /**
     * Sets the value of the offenceStartDateTo property.
     *
     * @param offenceStartDateTo Date - the date of the offence was committed
     */
    public void setOffenceStartDateTo(Date offenceStartDateTo) {
        this.offenceStartDateTo = offenceStartDateTo;
    }

    /**
     * Gets the value of the offenceEndDateFrom property.
     *
     * @return Date - Field is used when the exact offence date is not known, to reflect an offence committed date range.
     */
    public Date getOffenceEndDateFrom() {
        return offenceEndDateFrom;
    }

    /**
     * Sets the value of the offenceEndDateFrom property.
     *
     * @param offenceEndDateFrom Date - Field is used when the exact offence date is not known, to reflect an offence committed date range.
     */
    public void setOffenceEndDateFrom(Date offenceEndDateFrom) {
        this.offenceEndDateFrom = offenceEndDateFrom;
    }

    /**
     * Gets the value of the offenceEndDateTo property.
     *
     * @return Date - Field is used when the exact offence date is not known, to reflect an offence committed date range.
     */
    public Date getOffenceEndDateTo() {
        return offenceEndDateTo;
    }

    /**
     * Sets the value of the offenceEndDateTo property.
     *
     * @param offenceEndDateTo Date - Field is used when the exact offence date is not known, to reflect an offence committed date range.
     */
    public void setOffenceEndDateTo(Date offenceEndDateTo) {
        this.offenceEndDateTo = offenceEndDateTo;
    }

    /**
     * Gets the value of the filingDateFrom property.
     *
     * @return Date - A date the charge was filed
     */
    public Date getFilingDateFrom() {
        return filingDateFrom;
    }

    /**
     * Sets the value of the filingDateFrom property.
     *
     * @param filingDateFrom Date - A date the charge was filed
     */
    public void setFilingDateFrom(Date filingDateFrom) {
        this.filingDateFrom = filingDateFrom;
    }

    /**
     * Gets the value of the filingDateTo property.
     *
     * @return Date - A date the charge was filed
     */
    public Date getFilingDateTo() {
        return filingDateTo;
    }

    /**
     * Sets the value of the filingDateTo property.
     *
     * @param filingDateTo Date - A date the charge was filed
     */
    public void setFilingDateTo(Date filingDateTo) {
        this.filingDateTo = filingDateTo;
    }

    /**
     * Gets the value of the primary property.
     *
     * @return Boolean - True if the charge is a serious charge, false otherwise
     */
    public Boolean getPrimary() {
        return primary;
    }

    /**
     * Sets the value of the primary property.
     *
     * @param primary Boolean - True if the charge is a serious charge, false otherwise
     */
    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

    /**
     * @return the sentenceId
     */
    public Long getSentenceId() {
        return sentenceId;
    }

    /**
     * @param sentenceId the sentenceId to set
     */
    public void setSentenceId(Long sentenceId) {
        this.sentenceId = sentenceId;
    }

    /**
     * Gets the value of the comment property.
     *
     * @return CommentType - The comments related to a charge
     */
    public CommentType getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     *
     * @param comment CommentType - The comments related to a charge
     */
    public void setComment(CommentType comment) {
        this.comment = comment;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeSearchType [statuteChargeConfigSearch=" + statuteChargeConfigSearch + ", chargeIdentifierSearch=" + chargeIdentifierSearch
                + ", chargeIndicatorSearch=" + chargeIndicatorSearch + ", legalText=" + legalText + ", chargeDispositionSearch=" + chargeDispositionSearch
                + ", chargePleaSearch=" + chargePleaSearch + ", chargeAppealSearch=" + chargeAppealSearch + ", chargeCount=" + chargeCount + ", facilityId=" + facilityId
                + ", organizationId=" + organizationId + ", personIdentityId=" + personIdentityId + ", offenceStartDateFrom=" + offenceStartDateFrom
                + ", offenceStartDateTo=" + offenceStartDateTo + ", offenceEndDateFrom=" + offenceEndDateFrom + ", offenceEndDateTo=" + offenceEndDateTo
                + ", filingDateFrom=" + filingDateFrom + ", filingDateTo=" + filingDateTo + ", primary=" + primary + ", sentenceId=" + sentenceId + ", comment=" + comment
                + "]";
    }

}
