package syscon.arbutus.product.services.legal.realization.persistence.caseinformation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the case status change history entity.
 *
 * @author lhan
 * @DbComment LEG_CIStsChgHist 'The case status change history data table'
 */
//@Audited
@Entity
@Table(name = "LEG_CICseStsChgHist")
public class CaseStatusChangeHistEntity implements Serializable {

    private static final long serialVersionUID = 8857904777327359671L;

    /**
     * @DbComment CseStsChgHistId 'Unique id of a case status change history instance'
     */
    @Id
    @Column(name = "CseStsChgHistId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CICseStsChgHist_Id")
    @SequenceGenerator(name = "SEQ_LEG_CICseStsChgHist_Id", sequenceName = "SEQ_LEG_CICseStsChgHist_Id", allocationSize = 1)
    private Long caseStatusChangeHistId;

    /**
     * @DbComment CaseStatus 'Indicates whether the case is active or inactive. This is specifically set by the user, not derived.'
     */
    @Column(name = "CaseStatus", nullable = false)
    private Boolean caseStatus;

    /**
     * @DbComment CseStsChgReason 'Indicates the reason a case status was changed.'
     */
    @Column(name = "CseStsChgReason", nullable = true, length = 64)
    private String caseStatusChangeReason;

    //From charge Disposition type
    /**
     * @DbComment CseStsChgDate 'Date of the case status was changed'
     */
    @Column(name = "CseStsChgDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date caseStatusChangeDate;

    /**
     * @DbComment CommentUserId 'User ID who created/updated the comments'
     * @DbComment CommentDate 'The Date/Time when the comment was created'
     * @DbComment CommentText 'The comment text'
     */
    @Embedded
    private CommentEntity comment;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment CaseInfoId 'Unique id of a case instance'
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CaseInfoId", nullable = false)
    private CaseInfoEntity caseInfo;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    public Long getCaseStatusChangeHistId() {
        return caseStatusChangeHistId;
    }

    public void setCaseStatusChangeHistId(Long caseStatusChangeHistId) {
        this.caseStatusChangeHistId = caseStatusChangeHistId;
    }

    public Boolean getCaseStatus() {
        return caseStatus;
    }

    public void setCaseStatus(Boolean caseStatus) {
        this.caseStatus = caseStatus;
    }

    public String getCaseStatusChangeReason() {
        return caseStatusChangeReason;
    }

    public void setCaseStatusChangeReason(String caseStatusChangeReason) {
        this.caseStatusChangeReason = caseStatusChangeReason;
    }

    public Date getCaseStatusChangeDate() {
        return caseStatusChangeDate;
    }

    public void setCaseStatusChangeDate(Date caseStatusChangeDate) {
        this.caseStatusChangeDate = caseStatusChangeDate;
    }

    public CaseInfoEntity getCaseInfo() {
        return caseInfo;
    }

    public void setCaseInfo(CaseInfoEntity caseInfo) {
        this.caseInfo = caseInfo;
    }

    /**
     * @return the comment
     */
    public CommentEntity getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(CommentEntity comment) {
        this.comment = comment;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }
}
