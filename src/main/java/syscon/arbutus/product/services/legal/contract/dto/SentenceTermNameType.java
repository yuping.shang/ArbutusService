package syscon.arbutus.product.services.legal.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * SentenceTermType for Configure Sentence Term
 *
 * @author Ashish
 * @version 1.0
 * @since December 10, 2013
 */
public class SentenceTermNameType implements Serializable {

    private static final long serialVersionUID = -737931375114522737L;

    private Long sentenceTermNameId;

    @NotNull
    private String termName;

    private Long sentenceTermId;

    private String sentenceType;

    /**
     * Constructor
     */
    public SentenceTermNameType() {
        super();
    }

    public SentenceTermNameType(Long sentenceTermNameId, String termName) {
        super();
        this.sentenceTermNameId = sentenceTermNameId;
        this.termName = termName;
    }

    /**
     * Constructor
     *
     * @param sentenceTermNameId String, id of SentenceTermNameType Object
     * @param termName           Code, Required. (MAX , MIN, COM, etc..)
     * @param deactivationDate
     */
    public SentenceTermNameType(@NotNull Long sentenceTermNameId, @NotNull String termName, Date deactivationDate) {
        super();
        this.sentenceTermNameId = sentenceTermNameId;
        this.termName = termName;
    }

    public SentenceTermNameType(Long sentenceTermNameId, String termName, String sentenceType) {
        super();
        this.sentenceTermNameId = sentenceTermNameId;
        this.termName = termName;
        this.sentenceType = sentenceType;
    }

    /**
     * Get id of SentenceTermNameType Object
     *
     * @return the sentenceTermId
     */
    public Long getSentenceTermNameId() {
        return sentenceTermNameId;
    }

    /**
     * Set id of SentenceTermNameType Object
     *
     * @param the sentenceTermId
     */
    public void setSentenceTermNameId(Long sentenceTermNameId) {
        this.sentenceTermNameId = sentenceTermNameId;
    }

    /**
     * Get Term Name
     * -- The name of the term ( Maximum, Minimum etc.).
     *
     * @return the termName
     */
    public String getTermName() {
        return termName;
    }

    /**
     * Set Term Name
     * -- -- The name of the term ( Maximum, Minimum etc.).
     *
     * @param termName the termName to set
     */
    public void setTermName(String termName) {
        this.termName = termName;
    }

    public Long getSentenceTermId() {
        return sentenceTermId;
    }

    public void setSentenceTermId(Long sentenceTermId) {
        this.sentenceTermId = sentenceTermId;
    }

    public String getSentenceType() {
        return sentenceType;
    }

    public void setSentenceType(String sentenceType) {
        this.sentenceType = sentenceType;
    }

}
