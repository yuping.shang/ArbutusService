package syscon.arbutus.product.services.legal.contract.ejb;

import javax.ejb.SessionContext;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.LinkCodeType;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.ErrorCodes;
import syscon.arbutus.product.services.legal.contract.dto.LegalModule;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.AdjustmentConfigType;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.AdjustmentType;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.SentenceAdjustmentType;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.SentenceEntity;
import syscon.arbutus.product.services.legal.realization.persistence.sentenceadjustment.*;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.realization.util.ReferenceDataHelper;

/**
 * Created with IntelliJ IDEA.
 * User: yshang
 * Date: 09/10/13
 * Time: 9:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class SentenceAdjustmentHelper {

    private static Logger log = LoggerFactory.getLogger(SentenceAdjustmentHelper.class);

    public static AdjustmentConfigEntity toAdjustmentConfigEntity(AdjustmentConfigType from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        AdjustmentConfigEntity to = new AdjustmentConfigEntity();
        to.setClassification(from.getAdjustmentClassification());
        to.setAdjustmentType(from.getAdjustmentType());
        to.setApplicationType(from.getApplicationType());
        to.setAdjustmentFunction(from.getAdjustmentFunction());
        to.setAdjustmentStatus(from.getAdjustmentStatus());
        to.setByStaffId(from.getByStaffId());
        to.setComment(from.getComment());
        to.setSpecialAdjustment(from.getSpecialAdjustment());

        to.setStamp(stamp);

        return to;
    }

    public static AdjustmentConfigType toAdjustmentConfigType(AdjustmentConfigEntity from) {
        if (from == null) {
			return null;
		}

        AdjustmentConfigType to = new AdjustmentConfigType();
        to.setAdjustmentClassification(from.getClassification());
        to.setAdjustmentType(from.getAdjustmentType());
        to.setApplicationType(from.getApplicationType());
        to.setAdjustmentFunction(from.getAdjustmentFunction());
        to.setAdjustmentStatus(from.getAdjustmentStatus());
        to.setByStaffId(from.getByStaffId());
        to.setComment(from.getComment());
        to.setSpecialAdjustment(from.getSpecialAdjustment());
        to.setStamp(BeanHelper.toStamp(from.getStamp()));

        return to;
    }

    public static SentenceAdjustmentEntity toSentenceAdjustmentEntity(UserContext uc, Session session, Long sentenceAdjustmentId, SentenceAdjustmentType from,
            StampEntity stamp, boolean bActive) {
        if (from == null) {
			return null;
		}

        SentenceAdjustmentEntity to = new SentenceAdjustmentEntity();
        to.setSentenceAdjustmentId(sentenceAdjustmentId);
        to.setSupervisionId(from.getSupervisionId());
        to.setAdjustments(toAdjustmentEntitySet(uc, session, from.getSupervisionId(), from.getAdjustments(), stamp, bActive));

        to.setStamp(stamp);

        return to;
    }

    public static Set<AdjustmentEntity> toAdjustmentEntitySet(UserContext uc, Session session, Long supervisionId, Set<AdjustmentType> from, StampEntity stamp,
            boolean bActive) {
        if (from == null) {
			return null;
		}

        Set<AdjustmentEntity> to = new HashSet<>();
        for (AdjustmentType adjustmentType : from) {
            to.add(toAdjustmentEntity(uc, session, supervisionId, adjustmentType, stamp, bActive));
        }

        return to;
    }

    public static AdjustmentEntity toAdjustmentEntity(UserContext uc, Session session, Long supervisionId, AdjustmentType from, StampEntity stamp, Boolean bActive) {
        if (from == null) {
			return null;
		}

        Map<String, String> map = getLinkCodeMaps(uc, session, from.getAdjustmentType(), bActive);

        AdjustmentEntity to = new AdjustmentEntity();
        to.setAdjustmentId(from.getAdjustmentId());
        to.setSupervisionId(supervisionId);
        to.setAdjustmentClassification(map.get(MetaSet.ADJUSTMENT_CLASSIFICATION.toUpperCase()));
        to.setAdjustmentType(from.getAdjustmentType());
        to.setAdjustmentFunction(map.get(MetaSet.ADJUSTMENT_FUNCTION.toUpperCase()));
        to.setAdjustmentStatus(map.get(MetaSet.ADJUSTMENT_STATUS.toUpperCase()));
        to.setApplicationType(map.get(MetaSet.APPLICATION_TYPE.toUpperCase()));
        to.setAdjustment(from.getAdjustment());
        to.setStartDate(from.getStartDate());
        to.setEndDate(from.getEndDate());
        to.setPostedDate(from.getPostedDate());
        to.setSentences(toSentenceAssocEntitySet(session, supervisionId, from.getSentenceIds(), stamp));
        to.setByStaffId(from.getByStaffId());
        to.setComment(from.getComment());
        to.setStamp(stamp);

        return to;
    }

    public static Set<SentenceAssocEntity> toSentenceAssocEntitySet(Session session, Long supervisionId, Set<Long> from, StampEntity stamp) {
        if (from == null) {
			return null;
		}

        Set<SentenceAssocEntity> to = new HashSet<>();
        for (Long id : from) {
            to.add(toSentenceAssocEntity(session, supervisionId, id, stamp));
        }
        return to;
    }

    public static SentenceAssocEntity toSentenceAssocEntity(Session session, Long supervisionId, Long id, StampEntity stamp) {
        if (id == null) {
			return null;
		}

        SentenceAssocEntity to = new SentenceAssocEntity();
        to.setToClass(LegalModule.ORDER_SENTENCE.value());
        // to.setToIdentifier(getSentenceIdBySupervisionIdAndSentenceNumber(session, supervisionId, id));
        to.setToIdentifier(id);
        to.setStamp(stamp);
        return to;
    }

    public static List<SentenceAdjustmentType> toSentenceAdjustmentTypeList(Session session, List<SentenceAdjustmentEntity> from) {
        if (from == null) {
			return null;
		}
        List<SentenceAdjustmentType> to = new ArrayList<>();
        for (SentenceAdjustmentEntity sentenceAdjustmentEntity : from) {
            to.add(toSentenceAdjustmentType(session, sentenceAdjustmentEntity));
        }
        return to;
    }

    public static SentenceAdjustmentType toSentenceAdjustmentType(Session session, SentenceAdjustmentEntity from) {
        if (from == null) {
			return null;
		}

        SentenceAdjustmentType to = new SentenceAdjustmentType();
        to.setSupervisionId(from.getSupervisionId());
        to.setAdjustments(toAdjustmentTypeSet(session, from.getAdjustments()));
        return to;
    }

    public static Set<AdjustmentType> toAdjustmentTypeSet(Session session, Set<AdjustmentEntity> from) {
        if (from == null) {
			return null;
		}
        Set<AdjustmentType> to = new HashSet<>();
        for (AdjustmentEntity adjustmentEntity : from) {
            to.add(toAdjustmentType(session, adjustmentEntity));
        }
        return to;
    }

    public static AdjustmentType toAdjustmentType(Session session, AdjustmentEntity from) {
        if (from == null) {
			return null;
		}
        AdjustmentType to = new AdjustmentType();
        to.setAdjustmentId(from.getAdjustmentId());
        to.setAdjustmentClassification(from.getAdjustmentClassification());
        to.setAdjustmentType(from.getAdjustmentType());
        to.setAdjustmentFunction(from.getAdjustmentFunction());
        to.setAdjustmentStatus(from.getAdjustmentStatus());
        to.setApplicationType(from.getApplicationType());
        to.setAdjustment(from.getAdjustment());
        to.setStartDate(from.getStartDate());
        to.setEndDate(from.getEndDate());
        to.setPostedDate(from.getPostedDate());
        to.setSentenceIds(toSentenceSet(session, from.getSentences()));
        to.setByStaffId(from.getByStaffId());
        to.setComment(from.getComment());
        to.setStamp(BeanHelper.toStamp(from.getStamp()));
        return to;
    }

    public static Set<Long> toSentenceSet(Session session, Set<SentenceAssocEntity> from) {
        if (from == null) {
			return null;
		}

        Set<Long> to = new HashSet<>();
        for (SentenceAssocEntity sentenceAssocEntity : from) {
            to.add(toSentence(session, sentenceAssocEntity));
        }
        return to;
    }

    public static Long toSentence(Session session, SentenceAssocEntity from) {
        if (from == null) {
			return null;
		}
        // return getSentenceNumberBySentenceId(session, from.getToIdentifier());
        return from.getToIdentifier();
    }

    public static SentenceAdjustmentEntity getSentenceAdjustmentEntityBySupervisionId(Session session, Long supervisionId) {
        Criteria c = session.createCriteria(SentenceAdjustmentEntity.class);
        c.add(Restrictions.eq("supervisionId", supervisionId));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        c.addOrder(Order.desc("sentenceAdjustmentId"));
        c.setMaxResults(1);
        SentenceAdjustmentEntity ret = (SentenceAdjustmentEntity) c.uniqueResult();
        if (ret != null) {
            Criteria adjCriteria = session.createCriteria(AdjustmentEntity.class);
            adjCriteria.add(Restrictions.eq("supervisionId", supervisionId));
            adjCriteria.add(Restrictions.eq("sentenceAdjust.sentenceAdjustmentId", ret.getSentenceAdjustmentId()));
            @SuppressWarnings("unchecked") List<AdjustmentEntity> list = adjCriteria.list();
            ret.setAdjustments(new HashSet<>(list));
        }

        return ret;
    }

    public static List<SentenceAdjustmentEntity> getSentenceAdjustmentEntityWithHistory(Session session, Long supervisionId) {
        Criteria c = session.createCriteria(SentenceAdjustmentHistEntity.class);
        c.add(Restrictions.eq("supervisionId", supervisionId));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        Long sentenceAdjustmentId = getSentenceAdjustmentIdBySupervisionId(session, supervisionId);
        if (sentenceAdjustmentId != null) {
            c.add(Restrictions.eq("sentenceAdjustmentId", getSentenceAdjustmentIdBySupervisionId(session, supervisionId)));
        }
        c.addOrder(Order.desc("historyId"));
        @SuppressWarnings("unchecked") List<SentenceAdjustmentHistEntity> retHist = c.list();
        if (retHist != null) {
            for (SentenceAdjustmentHistEntity hist : retHist) {
                Criteria adjCriteria = session.createCriteria(AdjustmentHistEntity.class);
                adjCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
                adjCriteria.add(Restrictions.eq("supervisionId", supervisionId));
                adjCriteria.createCriteria("sentenceAdjust", "sentAdjust");
                adjCriteria.add(Restrictions.eq("sentAdjust.sentenceAdjustmentId", hist.getSentenceAdjustmentId()));
                @SuppressWarnings("unchecked") List<AdjustmentHistEntity> list = adjCriteria.list();
                hist.setAdjustments(new HashSet<>(list));
            }
        }

        List<SentenceAdjustmentEntity> ret = toSentenceAdjustmentEntityList(retHist);
        return ret;
    }

    public static List<SentenceAdjustmentEntity> toSentenceAdjustmentEntityList(List<SentenceAdjustmentHistEntity> from) {
        if (from == null) {
			return null;
		}
        List<SentenceAdjustmentEntity> to = new ArrayList<>();
        for (SentenceAdjustmentHistEntity frm : from) {
            to.add(toSentenceAdjustmentEntity(frm));
        }
        return to;
    }

    public static SentenceAdjustmentEntity toSentenceAdjustmentEntity(SentenceAdjustmentHistEntity from) {
        if (from == null) {
			return null;
		}
        SentenceAdjustmentEntity to = new SentenceAdjustmentEntity();
        to.setSentenceAdjustmentId(from.getSentenceAdjustmentId());
        to.setSupervisionId(from.getSupervisionId());
        to.setAdjustments(toAdjustmentEntitySet(from.getAdjustments()));
        to.setFlag(from.getFlag());
        to.setVersion(from.getVersion());
        to.setStamp(from.getStamp());

        return to;
    }

    public static Set<AdjustmentEntity> toAdjustmentEntitySet(Set<AdjustmentHistEntity> from) {
        if (from == null) {
			return null;
		}
        Set<AdjustmentEntity> to = new HashSet<>();
        for (AdjustmentHistEntity frm : from) {
            to.add(toAdjustmentEntity(frm));
        }
        return to;
    }

    public static AdjustmentEntity toAdjustmentEntity(AdjustmentHistEntity from) {
        if (from == null) {
			return null;
		}
        AdjustmentEntity to = new AdjustmentEntity();
        to.setAdjustmentId(from.getAdjustmentId());
        to.setSupervisionId(from.getSupervisionId());
        to.setAdjustmentClassification(from.getAdjustmentClassification());
        to.setAdjustmentType(from.getAdjustmentType());
        to.setAdjustmentFunction(from.getAdjustmentFunction());
        to.setAdjustmentStatus(from.getAdjustmentStatus());
        to.setApplicationType(from.getApplicationType());
        to.setAdjustment(from.getAdjustment());
        to.setStartDate(from.getStartDate());
        to.setEndDate(from.getEndDate());
        to.setPostedDate(from.getPostedDate());
        to.setByStaffId(from.getByStaffId());
        to.setComment(from.getComment());
        to.setSentences(toSentenceAssocEntitySet(from.getSentences()));
        to.setFlag(from.getFlag());
        to.setVersion(from.getVersion());
        to.setStamp(from.getStamp());

        return to;
    }

    public static Set<SentenceAssocEntity> toSentenceAssocEntitySet(Set<SentenceAssocHistEntity> from) {
        if (from == null) {
			return null;
		}
        Set<SentenceAssocEntity> to = new HashSet<>();
        for (SentenceAssocHistEntity frm : from) {
            to.add(toSentenceAssocEntity(frm));
        }
        return to;
    }

    public static SentenceAssocEntity toSentenceAssocEntity(SentenceAssocHistEntity from) {
        if (from == null) {
			return null;
		}
        SentenceAssocEntity to = new SentenceAssocEntity();
        to.setAssociationId(from.getAssociationId());
        to.setToClass(from.getToClass());
        to.setToIdentifier(from.getToIdentifier());
        to.setStamp(from.getStamp());
        return to;
    }

    public static void storeToHistorySentenceAdjustment(UserContext uc, SessionContext context, Session session, SentenceAdjustmentEntity from) {
        SentenceAdjustmentHistEntity to = new SentenceAdjustmentHistEntity();
        to.setSentenceAdjustmentId(from.getSentenceAdjustmentId());
        to.setSupervisionId(from.getSupervisionId());
        // to.setAdjustments(toAdjustmentHistEntitySet(from.getAdjustments()));
        for (AdjustmentEntity adjustmentEntity : from.getAdjustments()) {
            to.addAdjustment(toAdjustmentHistEntity(adjustmentEntity));
        }
        to.setFlag(from.getFlag());
        to.setVersion(from.getVersion());
        to.setStamp(from.getStamp());
        session.persist(to);
    }

    public static Set<AdjustmentHistEntity> toAdjustmentHistEntitySet(Set<AdjustmentEntity> from) {
        if (from == null) {
			return null;
		}
        Set<AdjustmentHistEntity> to = new HashSet<>();
        for (AdjustmentEntity frm : from) {
            to.add(toAdjustmentHistEntity(frm));
        }
        return to;
    }

    public static AdjustmentHistEntity toAdjustmentHistEntity(AdjustmentEntity from) {
        if (from == null) {
			return null;
		}
        AdjustmentHistEntity to = new AdjustmentHistEntity();
        to.setAdjustmentId(from.getAdjustmentId());
        to.setSupervisionId(from.getSupervisionId());
        to.setAdjustmentClassification(from.getAdjustmentClassification());
        to.setAdjustmentType(from.getAdjustmentType());
        to.setAdjustmentFunction(from.getAdjustmentFunction());
        to.setAdjustmentStatus(from.getAdjustmentStatus());
        to.setApplicationType(from.getApplicationType());
        to.setAdjustment(from.getAdjustment());
        to.setStartDate(from.getStartDate());
        to.setEndDate(from.getEndDate());
        to.setPostedDate(from.getPostedDate());
        to.setByStaffId(from.getByStaffId());
        to.setComment(from.getComment());
        to.setSentences(toSentenceAssocHistEntitySet(from.getSentences()));
        to.setFlag(from.getFlag());
        to.setVersion(from.getVersion());
        to.setStamp(from.getStamp());
        return to;
    }

    public static Set<SentenceAssocHistEntity> toSentenceAssocHistEntitySet(Set<SentenceAssocEntity> from) {
        if (from == null) {
			return null;
		}

        Set<SentenceAssocHistEntity> to = new HashSet<>();
        for (SentenceAssocEntity frm : from) {
            to.add(toSentenceAssocHistEntity(frm));
        }
        return to;
    }

    public static SentenceAssocHistEntity toSentenceAssocHistEntity(SentenceAssocEntity from) {
        if (from == null) {
			return null;
		}
        SentenceAssocHistEntity to = new SentenceAssocHistEntity();
        to.setAssociationId(from.getAssociationId());
        to.setToClass(from.getToClass());
        to.setToIdentifier(from.getToIdentifier());
        to.setStamp(from.getStamp());

        return to;
    }

    public static void copyAdjustmentEntity(UserContext uc, SessionContext context, AdjustmentEntity from, AdjustmentEntity to) {
        if (from == null || to == null) {
			return;
		}

        to.setAdjustmentClassification(from.getAdjustmentClassification());
        to.setSupervisionId(from.getSupervisionId());
        to.setAdjustmentType(from.getAdjustmentType());
        to.setAdjustmentFunction(from.getAdjustmentFunction());
        to.setAdjustmentStatus(from.getAdjustmentStatus());
        to.setApplicationType(from.getApplicationType());
        to.setAdjustment(from.getAdjustment());
        to.setByStaffId(from.getByStaffId());
        to.setComment(from.getComment());
        to.setPostedDate(from.getPostedDate());
        to.setStartDate(from.getStartDate());
        to.setEndDate(from.getEndDate());
        to.setStamp(BeanHelper.getModifyStamp(uc, context, to.getStamp()));
        to.setSentences(from.getSentences());
        to.setFlag(from.getFlag());
        to.setVersion(from.getVersion());
    }

    public static Set<String> getLinkCodes(UserContext uc, Session session, CodeType codeType, String linkedSet, boolean bActive) {
        String set = codeType.getSet().toUpperCase();
        String code = codeType.getCode().toUpperCase();
        Set<String> codes = new HashSet<>();
        if (BeanHelper.isEmpty(set) || BeanHelper.isEmpty(code) || BeanHelper.isEmpty(linkedSet)) {
			return codes;
		}

        List<LinkCodeType> linkCodeTypeList = ReferenceDataHelper.getLinks(uc, session, codeType, bActive);
        if (linkCodeTypeList == null || linkCodeTypeList.isEmpty()) {
			return codes;
		}

        for (LinkCodeType linkCodeType : linkCodeTypeList) {
            if (set.equalsIgnoreCase(linkCodeType.getLinkedReferenceSet())) {
                codes.add(linkCodeType.getLinkedReferenceCode());
            } else if (set.equalsIgnoreCase(linkCodeType.getReferenceSet())) {
                codes.add(linkCodeType.getReferenceCode());
            }
        }
        return codes;
    }

    public static Map<String, String> getLinkCodeMaps(UserContext uc, Session session, String adjustmentType, Boolean bActive) {
        String functionName = "getLinkCodeMaps";
        CodeType codeType = new CodeType(MetaSet.ADJUSTMENT_TYPE.toUpperCase(), adjustmentType.toUpperCase());
        List<LinkCodeType> linkCodeTypeList = ReferenceDataHelper.getLinks(uc, session, codeType, bActive);
        if (linkCodeTypeList == null || linkCodeTypeList.isEmpty()) {
            String message = codeType + " does not exist.";
            LogHelper.error(log, functionName, message);
            throw new DataNotExistException(message);
        }

        Map<String, String> map = new HashMap<>();
        for (LinkCodeType linkCodeType : linkCodeTypeList) {
            if (MetaSet.ADJUSTMENT_CLASSIFICATION.toUpperCase().equals(linkCodeType.getLinkedReferenceSet())) {
                map.put(MetaSet.ADJUSTMENT_CLASSIFICATION.toUpperCase(), linkCodeType.getLinkedReferenceCode());
            } else if (MetaSet.ADJUSTMENT_CLASSIFICATION.toUpperCase().equals(linkCodeType.getReferenceSet())) {
                map.put(MetaSet.ADJUSTMENT_CLASSIFICATION.toUpperCase(), linkCodeType.getReferenceCode());
            } else if (MetaSet.ADJUSTMENT_FUNCTION.toUpperCase().equals(linkCodeType.getLinkedReferenceSet())) {
                map.put(MetaSet.ADJUSTMENT_FUNCTION.toUpperCase(), linkCodeType.getLinkedReferenceCode());
            } else if (MetaSet.ADJUSTMENT_FUNCTION.toUpperCase().equals(linkCodeType.getReferenceSet())) {
                map.put(MetaSet.ADJUSTMENT_FUNCTION.toUpperCase(), linkCodeType.getReferenceCode());
            } else if (MetaSet.ADJUSTMENT_STATUS.toUpperCase().equals(linkCodeType.getLinkedReferenceSet())) {
                map.put(MetaSet.ADJUSTMENT_STATUS.toUpperCase(), linkCodeType.getLinkedReferenceCode());
            } else if (MetaSet.ADJUSTMENT_STATUS.toUpperCase().equals(linkCodeType.getReferenceSet())) {
                map.put(MetaSet.ADJUSTMENT_STATUS.toUpperCase(), linkCodeType.getReferenceCode());
            } else if (MetaSet.APPLICATION_TYPE.toUpperCase().equals(linkCodeType.getLinkedReferenceSet())) {
                map.put(MetaSet.APPLICATION_TYPE.toUpperCase(), linkCodeType.getLinkedReferenceCode());
            } else if (MetaSet.APPLICATION_TYPE.toUpperCase().equals(linkCodeType.getReferenceSet())) {
                map.put(MetaSet.APPLICATION_TYPE.toUpperCase(), linkCodeType.getReferenceCode());
            } else if (MetaSet.ADJUSTMENT_TYPE.toUpperCase().equals(linkCodeType.getLinkedReferenceSet())) {
                if (!adjustmentType.toUpperCase().equals(linkCodeType.getLinkedReferenceCode()) && linkCodeType.getLinkedReferenceCode() != null) {
                    map.put(MetaSet.ADJUSTMENT_TYPE.toUpperCase(), linkCodeType.getLinkedReferenceCode());
                } else if (adjustmentType.toUpperCase().equals(linkCodeType.getLinkedReferenceCode())) {
                    map.put(MetaSet.ADJUSTMENT_TYPE.toUpperCase(), linkCodeType.getReferenceCode());
                }
            } else if (MetaSet.ADJUSTMENT_TYPE.toUpperCase().equals(linkCodeType.getReferenceSet()) && MetaSet.ADJUSTMENT_TYPE.toUpperCase().equals(
                    linkCodeType.getLinkedReferenceSet())) {
                if (adjustmentType.toUpperCase().equals(linkCodeType.getLinkedReferenceCode())) {
                    map.put(MetaSet.ADJUSTMENT_TYPE.toUpperCase(), linkCodeType.getReferenceCode());
                }
            }
        }
        return map;
    }

    public static void verifyAdjustment(UserContext uc, Session session, Long supervisionId) {
        if (supervisionId == null) {
			return;
		}

        String functionName = "verifyAdjustment";

        Criteria c = session.createCriteria(AdjustmentEntity.class);
        c.createCriteria("sentenceAdjust", "sa");
        c.add(Restrictions.eq("sa.sentenceAdjustmentId", getSentenceAdjustmentIdBySupervisionId(session, supervisionId)));
        c.add(Restrictions.eq("adjustmentFunction", SentenceAdjustmentHandler.ADJUSTMENT_FUNCTION.CRE.name()));
        c.setProjection(Projections.projectionList().add(Projections.groupProperty("adjustmentType")).add(Projections.sum("adjustment")));

        @SuppressWarnings("rawtypes") List list = c.list();
        for (Object obj : list) {
            Object[] objs = (Object[]) obj;
            String adjType = (String) objs[0];
            Long cre = (Long) objs[1] == null ? 0L : (Long) objs[1];
            Long deb = getAdjustmentByAdjustmentType(uc, session, supervisionId, getLinkCodeMaps(uc, session, adjType, true).get(MetaSet.ADJUSTMENT_TYPE.toUpperCase()),
                    SentenceAdjustmentHandler.ADJUSTMENT_FUNCTION.DEB.name());
            if (cre < deb) {
                String message = String.format("%s:%d < %s:%d", adjType, cre, getLinkCodeMaps(uc, session, adjType, true).get(MetaSet.ADJUSTMENT_TYPE.toUpperCase()),
                        deb);
                LogHelper.error(log, functionName, String.format("%s: %s", ErrorCodes.LEG_ADD_UPDATE_ADJUSTMENT_WITHOU_CREDIT, message));
                throw new ArbutusRuntimeException(message, ErrorCodes.LEG_ADD_UPDATE_ADJUSTMENT_WITHOU_CREDIT);
            }
        }
    }

    public static Long getAdjustmentByAdjustmentType(UserContext uc, Session session, Long supervisionId, String adjustmentType, String adjustmentFunction) {
        if (BeanHelper.isEmpty(adjustmentType) || BeanHelper.isEmpty(adjustmentFunction)) {
			return 0L;
		}

        Criteria c = session.createCriteria(AdjustmentEntity.class);
        c.createCriteria("sentenceAdjust", "sa");
        c.add(Restrictions.eq("sa.sentenceAdjustmentId", getSentenceAdjustmentIdBySupervisionId(session, supervisionId)));
        c.add(Restrictions.eq("adjustmentType", adjustmentType));
        c.add(Restrictions.eq("adjustmentFunction", adjustmentFunction));
        c.setProjection(Projections.sum("adjustment"));
        Long ret = (Long) c.uniqueResult();
        return ret == null ? 0L : ret;
    }

    public static Long getSentenceAdjustmentIdBySupervisionId(Session session, Long supervisionId) {
        Criteria sentenceAdjustmentCriteria = session.createCriteria(SentenceAdjustmentEntity.class);
        sentenceAdjustmentCriteria.add(Restrictions.eq("supervisionId", supervisionId));
        sentenceAdjustmentCriteria.addOrder(Order.desc("sentenceAdjustmentId"));
        sentenceAdjustmentCriteria.setMaxResults(1);
        sentenceAdjustmentCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        sentenceAdjustmentCriteria.setProjection(Projections.id());
        Long sentenceAdjustmentId = (Long) sentenceAdjustmentCriteria.uniqueResult();
        return sentenceAdjustmentId;
    }

    public static Set<Long> getSentenceIdSetBySupervisionIdAndSentenceNumbers(Session session, Long supervisionId, Set<Long> sentenceNumbers) {
        if (sentenceNumbers == null) {
			return new HashSet<Long>();
		}
        return sentenceNumbers;
        /*Set<Long> sentenceIDs = new HashSet<>();
        for (Long sentenceNumner: sentenceNumbers) {
            sentenceIDs.add(getSentenceIdBySupervisionIdAndSentenceNumber(session, supervisionId, sentenceNumner));
        }
        return sentenceIDs;*/
    }

    public static Long getSentenceIdBySupervisionIdAndSentenceNumber(Session session, Long supervisionId, Long sentenceNumber) {
        return sentenceNumber;
        /*DetachedCriteria caseInfoDC = DetachedCriteria.forClass(CaseInfoEntity.class);
        caseInfoDC.add(Restrictions.eq("supervisionId", supervisionId));
        caseInfoDC.add(Restrictions.eq("active", Boolean.TRUE));
        caseInfoDC.add(Restrictions.eq("sentenceStatus", OrderSentenceHandler.SentenceStatus.SENTENCED.name()));
        caseInfoDC.setProjection(Projections.id());

        DetachedCriteria caseInfoModuleCriteria = DetachedCriteria.forClass(CaseInfoModuleAssociationEntity.class);
        caseInfoModuleCriteria.createCriteria("caseInfo", "ci");
        caseInfoModuleCriteria.add(Subqueries.propertyIn("ci.caseInfoId", caseInfoDC));
        caseInfoModuleCriteria.add(Restrictions.eq("toClass", LegalModule.ORDER_SENTENCE.value()));
        caseInfoModuleCriteria.setProjection(Projections.property("toIdentifier"));

        Criteria sentenceCriteria = session.createCriteria(SentenceEntity.class);
        sentenceCriteria.add(Restrictions.eq("orderClassification", OrderSentenceHandler.OrderClassification.SENT.name()));
        sentenceCriteria.add(Restrictions.eq("sentenceNumber", sentenceNumber));
        sentenceCriteria.add(Subqueries.propertyIn("orderId", caseInfoModuleCriteria));
        sentenceCriteria.setProjection(Projections.property("orderId"));
        sentenceCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        sentenceCriteria.addOrder(Order.desc("orderId"));
        sentenceCriteria.setMaxResults(1);
        return (Long)sentenceCriteria.uniqueResult();*/
    }

    public static Long getSentenceNumberBySentenceId(Session session, Long sentenceId) {
        Criteria sentenceCriteria = session.createCriteria(SentenceEntity.class);
        sentenceCriteria.add(Restrictions.idEq(sentenceId));
        sentenceCriteria.setProjection(Projections.property("sentenceNumber"));
        sentenceCriteria.setMaxResults(1);
        return (Long) sentenceCriteria.uniqueResult();
    }

    public static void verifyAdjustment(UserContext uc, Session session, Long supervisionId, Set<AdjustmentEntity> adjustments, Boolean bActive, boolean bRemove) {
        if (supervisionId == null || BeanHelper.isEmpty(adjustments)) {
			return;
		}

        String functionName = "verifyAdjustment";

        Map<String, Long> lossMap = new HashMap<>();
        Map<String, Long> gainMap = new HashMap<>();
        adjustments.remove(null);
        for (AdjustmentEntity adj : adjustments) {
            Set<SentenceAssocEntity> sentences = adj.getSentences();
            for (SentenceAssocEntity sentence : sentences) {
                SentenceEntity sent = BeanHelper.findEntity(session, SentenceEntity.class, sentence.getToIdentifier());
                if (sent != null) {
                    String sentenceType = sent.getSentenceType();
                    Set<String> flags = getLinkCodes(uc, session, new CodeType(MetaSet.SENTENCE_TYPE.toUpperCase(), sentenceType), MetaSet.ADJUSTMENT_FLAG.toUpperCase(),
                            bActive);
                    if (flags.contains(SentenceAdjustmentHandler.ADJUSTMENT_FLAG.NO.name())) {
                        String message = sentenceType;
                        LogHelper.error(log, functionName, String.format("%s: %s", ErrorCodes.LEG_ADD_UPDATE_ADJUSTMENT_WITHOU_CREDIT, message));
                        throw new ArbutusRuntimeException(message, ErrorCodes.LEG_ADD_UPDATE_ADJUSTMENT_WITHOU_CREDIT);
                    }
                }
            }

            if (SentenceAdjustmentHandler.ADJUSTMENT_FUNCTION.DEB.name().equals(adj.getAdjustmentFunction())) {
                String lossType = adj.getAdjustmentType();
                if (!lossMap.containsKey(lossType)) {
                    lossMap.put(lossType, adj.getAdjustment());
                } else {
                    Long lossAdjustment = (lossMap.get(lossType) == null ? 0L : lossMap.get(lossType)) + (adj.getAdjustment() == null ? 0L : adj.getAdjustment());
                    lossMap.put(lossType, lossAdjustment);
                }
            } else if (SentenceAdjustmentHandler.ADJUSTMENT_FUNCTION.CRE.name().equals(adj.getAdjustmentFunction())) {
                String gainType = adj.getAdjustmentType();
                if (!gainMap.containsKey(gainType)) {
                    gainMap.put(gainType, adj.getAdjustment());
                } else {
                    Long gainAdjustment = (gainMap.get(gainType) == null ? 0L : gainMap.get(gainType)) + (adj.getAdjustment() == null ? 0L : adj.getAdjustment());
                    gainMap.put(gainType, gainAdjustment);
                }
            }
        }

        for (String lossType : lossMap.keySet()) {
            String gainType = getLinkCodeMaps(uc, session, lossType, bActive).get(MetaSet.ADJUSTMENT_TYPE.toUpperCase());
            if (BeanHelper.isEmpty(gainType)) {
                //    			String message = ErrorCodes.ADD_UPDATE_ADJUSTMENT_WITHOU_CREDIT.getDescription() + ": loss adjustment type: " + lossType;
                //    			LogHelper.error(log, functionName, message);
                //				throw new ArbutusRuntimeException(message, ErrorCodes.ADD_UPDATE_ADJUSTMENT_WITHOU_CREDIT.getCode());
                continue;
            }
            Long gainAdjustment = getAdjustmentByAdjustmentType(uc, session, supervisionId, gainType, SentenceAdjustmentHandler.ADJUSTMENT_FUNCTION.CRE.name());
            if (gainAdjustment == null) {
				gainAdjustment = 0L;
			}

            if (!bRemove) {
                gainAdjustment = gainAdjustment + (BeanHelper.isEmpty(gainType) ? 0L : gainMap.get(gainType) == null ? 0L : gainMap.get(gainType));
            } else {
                gainAdjustment = gainAdjustment - (BeanHelper.isEmpty(gainType) ? 0L : gainMap.get(gainType) == null ? 0L : gainMap.get(gainType));
            }
            if (gainAdjustment == null || gainAdjustment.longValue() == 0L) {
                String message = String.format("loss adjustment type: %s", lossType);
                LogHelper.error(log, functionName, String.format("%s: %s", ErrorCodes.LEG_ADD_UPDATE_ADJUSTMENT_WITHOU_CREDIT, message));
                throw new ArbutusRuntimeException(message, ErrorCodes.LEG_ADD_UPDATE_ADJUSTMENT_WITHOU_CREDIT);
            }

            Long lossAdjustment = getAdjustmentByAdjustmentType(uc, session, supervisionId, lossType, SentenceAdjustmentHandler.ADJUSTMENT_FUNCTION.DEB.name());
            if (lossAdjustment == null) {
				lossAdjustment = 0L;
			}

            if (!bRemove) {
                lossAdjustment = lossAdjustment + (lossMap.get(lossType) == null ? 0L : lossMap.get(lossType));
            } else {
                lossAdjustment = lossAdjustment - (lossMap.get(lossType) == null ? 0L : lossMap.get(lossType));
            }

            if (gainAdjustment < lossAdjustment) {
                String message = String.format("%s:%d < %s:%d", gainType, gainAdjustment, lossType, lossAdjustment);
                LogHelper.error(log, functionName, String.format("%s: %s", ErrorCodes.LEG_ADD_UPDATE_ADJUSTMENT_WITHOU_CREDIT, message));
                throw new ArbutusRuntimeException(message, ErrorCodes.LEG_ADD_UPDATE_ADJUSTMENT_WITHOU_CREDIT);
            }
        }

        for (String gainType : gainMap.keySet()) {
            String lossType = getLinkCodeMaps(uc, session, gainType, bActive).get(MetaSet.ADJUSTMENT_TYPE.toUpperCase());
            if (BeanHelper.isEmpty(lossType)) {
                continue;
            }
            Long gainAdjustment = getAdjustmentByAdjustmentType(uc, session, supervisionId, gainType, SentenceAdjustmentHandler.ADJUSTMENT_FUNCTION.CRE.name());
            if (gainAdjustment == null) {
				gainAdjustment = 0L;
			}

            if (!bRemove) {
                gainAdjustment = gainAdjustment + (BeanHelper.isEmpty(gainType) ? 0L : gainMap.get(gainType) == null ? 0L : gainMap.get(gainType));
            } else {
                gainAdjustment = gainAdjustment - (BeanHelper.isEmpty(gainType) ? 0L : gainMap.get(gainType) == null ? 0L : gainMap.get(gainType));
            }

            Long lossAdjustment = getAdjustmentByAdjustmentType(uc, session, supervisionId, lossType, SentenceAdjustmentHandler.ADJUSTMENT_FUNCTION.DEB.name());
            if (lossAdjustment == null) {
				lossAdjustment = 0L;
			}

            if (!bRemove) {
                lossAdjustment = lossAdjustment + (lossMap.get(lossType) == null ? 0L : lossMap.get(lossType));
            } else {
                lossAdjustment = lossAdjustment - (lossMap.get(lossType) == null ? 0L : lossMap.get(lossType));
            }

            if (gainAdjustment < lossAdjustment) {
                String message = String.format("%s:%d < %s:%d", gainType, gainAdjustment, lossType, lossAdjustment);
                LogHelper.error(log, functionName, String.format("%s: %s", ErrorCodes.LEG_ADD_UPDATE_ADJUSTMENT_WITHOU_CREDIT, message));
                throw new ArbutusRuntimeException(message, ErrorCodes.LEG_ADD_UPDATE_ADJUSTMENT_WITHOU_CREDIT);
            }
        }
    }

    public static void verifyAdjustment(UserContext uc, Session session, Long supervisionId, AdjustmentEntity adjustment, Boolean bActive, Boolean bRemove) {
        if (supervisionId == null || adjustment == null || adjustment.getAdjustment() == null || adjustment.getAdjustment().longValue() == 0L) {
			return;
		}
        Set<AdjustmentEntity> adjustments = new HashSet<>();
        adjustments.add(adjustment);
        verifyAdjustment(uc, session, supervisionId, adjustments, bActive, bRemove);
    }

}
