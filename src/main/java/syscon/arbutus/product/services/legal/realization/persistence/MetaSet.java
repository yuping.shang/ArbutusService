package syscon.arbutus.product.services.legal.realization.persistence;

/**
 * The references of this service.
 *
 * @author Y.Shang
 */
public class MetaSet {

    public static final String APPEAL_STATUS = "AppealStatus";
    public static final String BAIL_TYPE = "BailType";
    public static final String BAIL_RELATIONSHIP = "BailRelationship";

    public static final String CASE_IDENTIFIER_TYPE = "CaseIdentifierType";
    public static final String CASE_TYPE = "CaseType";
    public static final String CASE_CATEGORY = "CaseCategory";
    public static final String CASE_STATUS_CHANGE_REASON = "CaseStatusChangeReason";

    public static final String TYPE_OF_CHARGE = "TypeOfCharge";
    public static final String CHARGE_DEGREE = "ChargeDegree";
    public static final String CHARGE_CODE = "ChargeCode";
    public static final String CHARGE_CATEGORY = "ChargeCategory";
    public static final String CHARGE_SEVERITY = "ChargeSeverity";
    public static final String CHARGE_INDICATOR = "ChargeIndicator";
    public static final String CHARGE_ENHANCING_FACTOR = "ChargeEnhancingFactor";
    public static final String CHARGE_REDUCING_FACTOR = "ChargeReducingFactor";
    public static final String LANGUAGE = "Language";        //Refer to Person's Language
    public static final String CHARGE_OUTCOME = "ChargeOutcome";
    public static final String CHARGE_STATUS = "ChargeStatus";
    public static final String CHARGE_IDENTIFIER = "ChargeIdentifier";
    public static final String CHARGE_IDENTIFIER_FORMAT = "ChargeIdentifierFormat";
    public static final String EXTERNAL_CHARGE_CODE = "ExternalChargeCode";
    public static final String EXTERNAL_CHARGE_CODE_GROUP = "ExternalChargeCodeGroup";
    public static final String EXTERNAL_CHARGE_CODE_CATEGORY = "ExternalChargeCodeCategory";
    public static final String STATUTE_CODE = "StatuteCode";
    public static final String STATUTE_COUNTRY = "Country";                    //Refer to Person Service
    public static final String STATUTE_PROVINCE_STATE = "StateProvince";    //Refer to Person Service
    public static final String STATUTE_COUNTY = "StatuteCounty";
    public static final String CHARGE_PLEA = "ChargePlea";

    public static final String CURRENCY = "Currency";

    public static final String DAY = "Day";
    public static final String DURATION = "Duration";

    public static final String NOTIFICATION_TYPE = "NotificationType";

    public static final String ORDER_CLASSIFICATION = "OrderClassification";
    public static final String ORDER_TYPE = "OrderType";
    public static final String ORDER_SUB_TYPE = "OrderSubType";
    public static final String ORDER_STATUS = "OrderStatus";
    public static final String ORDER_OUTCOME = "OrderOutcome";
    public static final String ORDER_CATEGORY = "OrderCategory";

    public static final String SENTENCE_TYPE = "SentenceType";
    public static final String SENTENCE_STATUS = "SentenceStatus";
    public static final String KEY_DATES = "KeyDates";
    public static final String KEY_DATE_OVERRIDE_REASON = "KeyDateOverrideReason";

    public static final String TERM_TYPE = "TermType";
    public static final String TERM_NAME = "TermName";
    public static final String TERM_CATEGORY = "TermCategory";
    public static final String TERM_STATUS = "TermStatus";

    public static final String CONDITION_AMENDMENT_STATUS = "AmendmentStatus";
    public static final String CONDITION_TYPE = "ConditionType";
    public static final String CONDITION_CATEGORY = "ConditionCategory";
    public static final String CONDITION_SUB_CATEGORY = "ConditionSubCategory";
    public static final String CONDITION_PERIOD = "ConditionPeriod";
    public static final String CONDITION_PERIOD_UNIT = "CONDITIONPERIOD";
    public static final String CONDITION_DISTANCE = "ConditionDistance";
    public static final String CONDITION_DISTANCE_UNIT = "DISTANCE";
    public static final String CONDITION_CURRENCY = "ConditionCurrency";
    public static final String CHARGE_JURISDICTION = "ChargeJurisdiction";

    public static final String ADJUSTMENT_CLASSIFICATION = "AdjustmentClassification";
    public static final String ADJUSTMENT_TYPE = "AdjustmentType";
    public static final String ADJUSTMENT_FUNCTION = "AdjustmentFunction";
    public static final String ADJUSTMENT_STATUS = "AdjustmentStatus";
    public static final String APPLICATION_TYPE = "ApplicationType";
    public static final String ADJUSTMENT_FLAG = "AdjustmentFlag";

    public static final String INTERMITTENT_SCHEDULE_OUTCOME = "INTERMITTENTSCHEDULEOUTCOME";
    public static final String INTERMITTENT_SCHEDULE_STATUS = "INTERMITTENTSCHEDULESTATUS";

    public static final String OBJECTIVES = "OBJECTIVES";
    public static final String CASE_PLAN_TYPE = "CASEPLANTYPE";
}
