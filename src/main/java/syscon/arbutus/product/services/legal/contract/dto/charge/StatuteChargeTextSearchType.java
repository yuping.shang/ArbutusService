package syscon.arbutus.product.services.legal.contract.dto.charge;

import java.io.Serializable;

import syscon.arbutus.product.services.legal.contract.dto.ValidationUtil;

/**
 * The representation of the statute charge text search type
 */
public class StatuteChargeTextSearchType implements Serializable {

    private static final long serialVersionUID = -5389140079950985740L;

    private String language;
    private String description;
    private String legalText;

    /**
     * Default constructor
     */
    public StatuteChargeTextSearchType() {
    }

    /**
     * Constructor
     * At least one of the search criteria must be specified.
     *
     * @param language    String - The language code associated to the charge text
     * @param description String - Text related to the charge code
     * @param legalText   String - Legal text for a charge
     */
    public StatuteChargeTextSearchType(String language, String description, String legalText) {
        this.language = language;
        this.description = description;
        this.legalText = legalText;
    }

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set, false otherwise.
     */
    @Deprecated
    public Boolean isWellFormed() {

        if (ValidationUtil.isEmpty(language) && ValidationUtil.isEmpty(description) && ValidationUtil.isEmpty(legalText)) {
			return false;
		}

        return true;
    }

    /**
     * Verify if properties are set properly.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set and all values are valid, false otherwise.
     */
    @Deprecated
    public Boolean isValid() {

        if (!isWellFormed()) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the language property. It is a reference code of Language set.
     *
     * @return String - The language code associated to the charge text
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property. It is a reference code of Language set.
     *
     * @param language String - The language code associated to the charge text
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * Gets the value of the description property.
     *
     * @return String - Text related to the charge code
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param description String - Text related to the charge code
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the value of the legalText property.
     *
     * @return String - Legal text for a charge
     */
    public String getLegalText() {
        return legalText;
    }

    /**
     * Sets the value of the legalText property.
     *
     * @param legalText String - Legal text for a charge
     */
    public void setLegalText(String legalText) {
        this.legalText = legalText;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StatuteChargeTextSearchType [language=" + language + ", description=" + description + ", legalText=" + legalText + "]";
    }

}
