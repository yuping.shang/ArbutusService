/**
 *
 */
package syscon.arbutus.product.services.legal.contract.dto.charge;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Represents the charge status information.
 *
 * @author lhan
 */
public class ChargesStatusType implements Serializable {

    private static final long serialVersionUID = -5835204373953093450L;

    @NotNull
    private String chargeOutcome;
    @NotNull
    private Date chargeOutcomeDate;
    @NotNull
    private String chargeStatus;
    private String comment;

    @NotNull
    @Size(min = 1)
    private List<Long> selectedActiveChargeIds;

    /**
     *
     */
    public ChargesStatusType() {
        super();

    }

    /**
     * @param chargeOutcome
     * @param chargeOutcomeDate
     * @param chargeStatus
     * @param comment
     * @param selectedActiveChargeIds
     */
    public ChargesStatusType(String chargeOutcome, Date chargeOutcomeDate, String chargeStatus, String comment, List<Long> selectedActiveChargeIds) {
        super();
        this.chargeOutcome = chargeOutcome;
        this.chargeOutcomeDate = chargeOutcomeDate;
        this.chargeStatus = chargeStatus;
        this.comment = comment;
        this.selectedActiveChargeIds = selectedActiveChargeIds;
    }

    /**
     * @return the chargeOutcome
     */
    public String getChargeOutcome() {
        return chargeOutcome;
    }

    /**
     * @param chargeOutcome the chargeOutcome to set
     */
    public void setChargeOutcome(String chargeOutcome) {
        this.chargeOutcome = chargeOutcome;
    }

    /**
     * @return the chargeOutcomeDate
     */
    public Date getChargeOutcomeDate() {
        return chargeOutcomeDate;
    }

    /**
     * @param chargeOutcomeDate the chargeOutcomeDate to set
     */
    public void setChargeOutcomeDate(Date chargeOutcomeDate) {
        this.chargeOutcomeDate = chargeOutcomeDate;
    }

    /**
     * @return the chargeStatus
     */
    public String getChargeStatus() {
        return chargeStatus;
    }

    /**
     * @param chargeStatus the chargeStatus to set
     */
    public void setChargeStatus(String chargeStatus) {
        this.chargeStatus = chargeStatus;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the selectedActiveChargeIds
     */
    public List<Long> getSelectedActiveChargeIds() {
        return selectedActiveChargeIds;
    }

    /**
     * @param selectedActiveChargeIds the selectedActiveChargeIds to set
     */
    public void setSelectedActiveChargeIds(List<Long> selectedActiveChargeIds) {
        this.selectedActiveChargeIds = selectedActiveChargeIds;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargesStatusType [getChargeOutcome()=" + getChargeOutcome() + ", getChargeOutcomeDate()=" + getChargeOutcomeDate() + ", getChargeStatus()="
                + getChargeStatus() + ", getComment()=" + getComment() + ", getSelectedActiveChargeIds()=" + getSelectedActiveChargeIds() + "]";
    }

}
