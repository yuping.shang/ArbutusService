package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the statute configuration entity.
 *
 * @DbComment LEG_CHGStatChgTxt 'The statute charge text configuration data table'
 */
@Audited
@Entity
@Table(name = "LEG_CHGStatChgTxt")
public class StatuteChargeTextEntity implements Serializable {

    private static final long serialVersionUID = -2200852995293143928L;

    /**
     * @DbComment StatuteChargeTextId 'The unique Id for each statute charge text data record.'
     */
    @Id
    @Column(name = "StatuteChargeTextId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGStatChgTxt_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGStatChgTxt_Id", sequenceName = "SEQ_LEG_CHGStatChgTxt_Id", allocationSize = 1)
    private Long statuteChargeTextId;

    /**
     * @DbComment Language 'The language code associated to the charge text.'
     */
    @MetaCode(set = MetaSet.LANGUAGE)
    @Column(name = "Language", nullable = false, length = 64)
    private String language;

    /**
     * @DbComment Description 'Text related to the charge code.'
     */
    @Column(name = "Description", nullable = false, length = 4000)
    private String description;

    /**
     * @DbComment LegalText 'Legal text for a charge.'
     */
    @Column(name = "LegalText", nullable = true, length = 4000)
    private String legalText;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     *
     */
    public StatuteChargeTextEntity() {
    }

    /**
     * @param statuteChargeTextId
     * @param language
     * @param description
     * @param legalText
     */
    public StatuteChargeTextEntity(Long statuteChargeTextId, String language, String description, String legalText) {
        this.statuteChargeTextId = statuteChargeTextId;
        this.language = language;
        this.description = description;
        this.legalText = legalText;
    }

    /**
     * @return the statuteChargeTextId
     */
    public Long getStatuteChargeTextId() {
        return statuteChargeTextId;
    }

    /**
     * @param statuteChargeTextId the statuteChargeTextId to set
     */
    public void setStatuteChargeTextId(Long statuteChargeTextId) {
        this.statuteChargeTextId = statuteChargeTextId;
    }

    /**
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the legalText
     */
    public String getLegalText() {
        return legalText;
    }

    /**
     * @param legalText the legalText to set
     */
    public void setLegalText(String legalText) {
        this.legalText = legalText;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StatuteChargeTextEntity [statuteChargeTextId=" + statuteChargeTextId + ", language=" + language + ", description=" + description + ", legalText="
                + legalText + "]";
    }

}
