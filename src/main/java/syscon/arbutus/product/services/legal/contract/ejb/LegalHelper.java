package syscon.arbutus.product.services.legal.contract.ejb;

import javax.ejb.SessionContext;
import java.math.BigDecimal;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Session;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.legal.contract.dto.LegalModule;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ConfigEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.CAActivityIdEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.CaseActivityEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseaffiliation.CaseAffiliationEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.CaseInfoEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.CaseInfoModuleAssociationEntity;
import syscon.arbutus.product.services.legal.realization.persistence.charge.ChargeEntity;
import syscon.arbutus.product.services.legal.realization.persistence.charge.ChargeModuleAssociationEntity;
import syscon.arbutus.product.services.legal.realization.persistence.condition.ConditionEntity;
import syscon.arbutus.product.services.legal.realization.persistence.condition.ConditionModuleAssociationEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.OrderEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.OrderModuleAssociationEntity;
import syscon.arbutus.product.services.realization.model.Associable;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.ReferenceDataHelper;

/**
 * This helper is provided for handling common functions of case management service
 *
 * @author byu
 */
public class LegalHelper {

    private static final Long SUCCESS = 1L;
    private static Logger log = LoggerFactory.getLogger(LegalHelper.class);

    public static CommentType toCommentType(CommentEntity entity) {

        if (entity == null) {
			return null;
		}

        CommentType ret = new CommentType();
        ret.setUserId(entity.getCommentUserId());
        ret.setCommentDate(entity.getCommentDate());
        ret.setComment(entity.getCommentText());

        return ret;

    }

    public static CommentEntity toCommentEntity(CommentType dto) {

        if (dto == null) {
			return null;
		}

        CommentEntity ret = new CommentEntity(dto.getUserId(), dto.getCommentDate(), dto.getComment());

        return ret;

    }

    public static CodeType setConfigEntity(UserContext uc, SessionContext context, Session session, String propertyName, String propertyValue,
            Boolean... bReferenceCode) {

        if (log.isDebugEnabled()) {
			log.debug("setConfigEntity: propertyName: " + propertyName + ", propertyValue: " + propertyValue + ": begin");
		}

        CodeType ret = null;
        if (propertyName == null || propertyName.trim().length() == 0) {
			return null;
		}
        propertyName = propertyName.toUpperCase();

        try {
            if (propertyValue == null || propertyValue.trim().length() == 0) {
                BeanHelper.deleteEntity(session, ConfigEntity.class, propertyName);
                return null;
            }

            if (bReferenceCode == null || bReferenceCode != null && bReferenceCode[0].booleanValue()) {
                propertyValue = propertyValue.toUpperCase();
                //ReferenceDataHelper.verifyReferenceCode(session, new CodeType(propertyName, propertyValue), true);
                Set<CodeType> propertiesSet = new HashSet<CodeType>();
                propertiesSet.add(new CodeType(propertyName, propertyValue));
                ReferenceDataHelper.isActiveReferenceCode(propertiesSet);
            }

            ConfigEntity config = BeanHelper.findEntity(session, ConfigEntity.class, propertyName);
            if (config == null) {
                config = new ConfigEntity();
                config.setPropertyName(propertyName);
                config.setPropertyValue(propertyValue);
                config.setStamp(BeanHelper.getCreateStamp(uc, context));
                session.save(config);
            } else {
                config.setPropertyValue(propertyValue);
                config.setStamp(BeanHelper.getModifyStamp(uc, context, config.getStamp()));
                session.update(config);
            }
            ret = new CodeType(config.getPropertyName(), config.getPropertyValue());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        if (log.isDebugEnabled()) {
			log.debug("setConfigEntity: propertyName: " + propertyName + ", propertyValue: " + propertyValue + ": end");
		}

        return ret;
    }

    public static CodeType getConfigEntity(Session session, String propertyName) {

        if (log.isDebugEnabled()) {
			log.debug("getConfigEntity: propertyName: " + propertyName + ": begin");
		}

        if (propertyName == null) {
			return null;
		}

        ConfigEntity config = BeanHelper.findEntity(session, ConfigEntity.class, propertyName.toUpperCase());
        if (config == null) {
			return null;
		}

        CodeType ret = new CodeType(config.getPropertyName(), config.getPropertyValue());

        if (log.isDebugEnabled()) {
			log.debug("getConfigEntity: propertyName: " + propertyName + ": end");
		}

        return ret;
    }

    public static Long removeConfigEntity(Session session, String propertyName) {
        if (log.isDebugEnabled()) {
			log.debug("removeConfigEntity: propertyName: " + propertyName + ": begin");
		}

        if (propertyName == null) {
			return null;
		}

        ConfigEntity config = BeanHelper.findEntity(session, ConfigEntity.class, propertyName.toUpperCase());
        if (config != null) {
			session.delete(config);
		}

        if (log.isDebugEnabled()) {
			log.debug("removeConfigEntity: propertyName: " + propertyName + ": end");
		}
        return SUCCESS;
    }

    /**
     * create a link object of type:thisModule with id:mainId to each of moduleAssociations
     *
     * @param uc
     * @param context
     * @param session
     * @param mainId
     * @param thisModule
     * @param moduleAssociations
     */

    public static <T extends Associable> void createModuleLinks(UserContext uc, SessionContext context, Session session, Long mainId, LegalModule thisModule,
            Set<T> moduleAssociations, StampEntity stamp) {

        if (mainId == null || moduleAssociations == null || moduleAssociations.isEmpty()) {
			return;
		}

        moduleAssociations.remove(null);
        for (T assoc : moduleAssociations) {
            String toClass = assoc.getToClass();

            Long toIdentifier = assoc.getToIdentifier();
            if (toClass.equalsIgnoreCase(LegalModule.CASE_INFORMATION.value())) {
                // link to case
                CaseInfoEntity ciEntity = (CaseInfoEntity) session.get(CaseInfoEntity.class, toIdentifier);
                if (ciEntity != null) {
                    CaseInfoModuleAssociationEntity moduleEntity = new CaseInfoModuleAssociationEntity();
                    moduleEntity.setToClass(thisModule.value());
                    moduleEntity.setToIdentifier(mainId);
                    moduleEntity.setStamp(stamp);
                    ciEntity.addModuleAssociation(moduleEntity);
                }
            } else if (toClass.equalsIgnoreCase(LegalModule.CHARGE.value())) {
                // link to charge
                ChargeEntity chgEntity = (ChargeEntity) session.get(ChargeEntity.class, toIdentifier);
                if (chgEntity != null) {
                    ChargeModuleAssociationEntity moduleEntity = new ChargeModuleAssociationEntity();
                    moduleEntity.setToClass(thisModule.value());
                    moduleEntity.setToIdentifier(mainId);
                    moduleEntity.setStamp(stamp);
                    chgEntity.addModuleAssociation(moduleEntity);
                }
            } else if (toClass.equalsIgnoreCase(LegalModule.CONDITION.value())) {
                // link to condition
                ConditionEntity cnEntity = (ConditionEntity) session.get(ConditionEntity.class, toIdentifier);
                if (cnEntity != null) {
                    ConditionModuleAssociationEntity moduleEntity = new ConditionModuleAssociationEntity();
                    moduleEntity.setToClass(thisModule.value());
                    moduleEntity.setToIdentifier(mainId);
                    moduleEntity.setStamp(stamp);
                    cnEntity.addModuleAssociation(moduleEntity);
                }
            } else if (toClass.equalsIgnoreCase(LegalModule.ORDER_SENTENCE.value())) {
                // link to Order Sentence
                OrderEntity ordEntity = (OrderEntity) session.get(OrderEntity.class, toIdentifier);
                if (ordEntity != null) {
                    OrderModuleAssociationEntity moduleEntity = new OrderModuleAssociationEntity();
                    moduleEntity.setToClass(thisModule.value());
                    moduleEntity.setToIdentifier(mainId);
                    moduleEntity.setStamp(stamp);
                    ordEntity.addModuleAssociation(moduleEntity);
                }
            }
        }
    }

    /**
     * Remove link of object type:thisModule with id:mainId from each of moduleAssociations
     *
     * @param uc
     * @param context
     * @param session
     * @param mainId
     * @param thisModule
     * @param moduleAssociations
     */
    public static <T extends Associable> void removeModuleLinks(UserContext uc, SessionContext context, Session session, Long mainId, LegalModule thisModule,
            Set<T> moduleAssociations) {

        if (mainId == null || moduleAssociations == null || moduleAssociations.isEmpty()) {
			return;
		}
        moduleAssociations.remove(null);

        for (T assoc : moduleAssociations) {
            String toClass = assoc.getToClass();
            Long toIdentifier = assoc.getToIdentifier();

            if (toClass.equalsIgnoreCase(LegalModule.CASE_INFORMATION.value())) {
                // charge
                CaseInfoEntity ciEntity = (CaseInfoEntity) session.get(CaseInfoEntity.class, toIdentifier);
                if (ciEntity != null) {
                    Set<CaseInfoModuleAssociationEntity> ciModules = ciEntity.getModuleAssociations();
                    if (!BeanHelper.isEmpty(ciModules)) {
                        ciModules.remove(null);
                        Iterator<CaseInfoModuleAssociationEntity> it = ciModules.iterator();
                        while (it.hasNext()) {
                            CaseInfoModuleAssociationEntity m = it.next();
                            if (m.getToClass().equalsIgnoreCase(thisModule.value()) && mainId.equals(m.getToIdentifier())) {
                                it.remove();
                            }
                        }
                    }
                }
            } else if (toClass.equalsIgnoreCase(LegalModule.CHARGE.value())) {
                // charge
                ChargeEntity chgEntity = (ChargeEntity) session.get(ChargeEntity.class, toIdentifier);
                if (chgEntity != null) {
                    Set<ChargeModuleAssociationEntity> chgModules = chgEntity.getModuleAssociations();
                    if (!BeanHelper.isEmpty(chgModules)) {
                        chgModules.remove(null);
                        Iterator<ChargeModuleAssociationEntity> it = chgModules.iterator();
                        while (it.hasNext()) {
                            ChargeModuleAssociationEntity m = it.next();
                            if (m.getToClass().equalsIgnoreCase(thisModule.value()) && m.getToIdentifier() == mainId) {
                                it.remove();
                            }
                        }
                    }
                }
            } else if (toClass.equalsIgnoreCase(LegalModule.CONDITION.value())) {
                // condition
                ConditionEntity cnEntity = (ConditionEntity) session.get(ConditionEntity.class, toIdentifier);
                if (cnEntity != null) {
                    Set<ConditionModuleAssociationEntity> cnModules = cnEntity.getModuleAssociations();
                    if (!BeanHelper.isEmpty(cnModules)) {
                        cnModules.remove(null);
                        Iterator<ConditionModuleAssociationEntity> it = cnModules.iterator();
                        while (it.hasNext()) {
                            ConditionModuleAssociationEntity m = it.next();
                            if (m.getToClass().equalsIgnoreCase(thisModule.value()) && m.getToIdentifier() == mainId) {
                                it.remove();
                            }
                        }
                    }
                }
            } else if (toClass.equalsIgnoreCase(LegalModule.ORDER_SENTENCE.value())) {
                // Order Sentence
                OrderEntity ordEntity = (OrderEntity) session.get(OrderEntity.class, toIdentifier);
                if (ordEntity != null) {
                    Set<OrderModuleAssociationEntity> ordModules = ordEntity.getModuleAssociations();
                    if (!BeanHelper.isEmpty(ordModules)) {
                        ordModules.remove(null);
                        Iterator<OrderModuleAssociationEntity> it = ordModules.iterator();
                        while (it.hasNext()) {
                            OrderModuleAssociationEntity m = it.next();
                            if (m.getToClass().equalsIgnoreCase(thisModule.value()) && m.getToIdentifier() == mainId) {
                                it.remove();
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Get the primary IDs of the set of CaseActivityEntity
     *
     * @param entities
     * @return
     */
    public static Set<Long> getCaseIds(Set<CaseInfoEntity> entities) {
        if (BeanHelper.isEmpty(entities)) {
            return null;
        }

        Set<Long> ids = new HashSet<Long>();
        for (CaseInfoEntity entity : entities) {
            ids.add(entity.getCaseInfoId());
        }

        return ids;
    }

    /**
     * @param entities
     * @return
     */
    public static Set<Long> getCaseAffiliationIds(Set<CaseAffiliationEntity> entities) {
        if (BeanHelper.isEmpty(entities)) {
            return null;
        }

        Set<Long> ids = new HashSet<Long>();
        for (CaseAffiliationEntity entity : entities) {
            ids.add(entity.getCaseAffiliationId());
        }

        return ids;
    }

    /**
     * @param entities
     * @return
     */
    public static Set<Long> getActivityIds(Set<CAActivityIdEntity> entities) {
        if (BeanHelper.isEmpty(entities)) {
            return null;
        }

        Set<Long> ids = new HashSet<Long>();
        for (CAActivityIdEntity entity : entities) {
            ids.add(entity.getActivityId());
        }

        return ids;
    }

    /**
     * @param entities
     * @return
     */
    public static Set<Long> getCaseActivityIds(Set<CaseActivityEntity> entities) {
        if (BeanHelper.isEmpty(entities)) {
            return null;
        }

        Set<Long> ids = new HashSet<Long>();
        for (CaseActivityEntity entity : entities) {
            ids.add(entity.getCaseActivityId());
        }

        return ids;
    }

    /**
     * <p>Cut the time portion of a given Date object.</p>
     *
     * @param date A Date object to trim.
     * @return A Date object.
     */
    public static Date getDateWithoutTime(Date date) {
        if (date == null) {
            return null;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Date newDate = cal.getTime();
        return newDate;
    }

    private Long getLongValue(Object o) {
        Long ret = null;
        if (o instanceof BigDecimal) {
            ret = ((BigDecimal) o).longValue();
        } else {
            ret = (Long) o;
        }
        return ret;
    }
}
