package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * BailType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 27, 2012
 */
public class BailType extends OrderType {

    private static final long serialVersionUID = 4096094382247377900L;

    @Size(min = 0, message = "Cardinality [0..*]")
    @Valid
    private Set<BailAmountType> bailAmounts;
    @NotNull
    private String bailRelationship;
    @Valid
    private Set<BailAmountType> bailPostedAmounts;
    @Size(max = 64, message = "max length 64")
    private String bailPaymentReceiptNo;
    @Size(max = 128, message = "max length 128")
    private String bailRequirement;

    private Set<Long> bailerPersonIdentityAssociations;

    private BailAmountType bondPostedAmount;
    @Size(max = 512, message = "max length 512")
    private String bondPaymentDescription;

    private Set<Long> bondOrganizationAssociations;
    @NotNull
    private Boolean isBailAllowed = Boolean.TRUE;

    private Date bailBondPostedDate;

    /**
     * Constructor
     */
    public BailType() {
        super();
    }

    /**
     * Constructor
     *
     * @param orderIdentification                    Long. Not Required for create; Required otherwise. The unique ID for each order created in the system. This is a system-generated ID.
     * @param orderClassification                    String, Required. The order classification. This defines the type of instance of the service.
     * @param orderType                              String, Required. The client defined order type that maps to the order classification.
     * @param orderSubType                           String, Optional. The sub type of an order. For example a want, warrant or detainer.
     * @param orderCategory                          String, Required. The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.
     * @param orderNumber                            String, Optional. User specified order number.
     * @param orderDisposition                       DispositionType, Required. The disposition related to the order.
     * @param comments                               CommentType, Optional. The comments related to the order.
     * @param orderIssuanceDate                      Date, Optional. The date the order was issued.
     * @param orderReceivedDate                      Date, Optional. The date the order was received.
     * @param orderStartDate                         Date, Required. The date/time the order starts(becomes valid).
     * @param orderExpirationDate                    Date, Optional. The date the order expires.
     * @param caseActivityInitiatedOrderAssociations Set&lt;Long>, Optional. The case related activities that initiated this order. Static association to the Case Activity service.
     * @param orderInitiatedCaseActivityAssociations Set&lt;Long>, Optional. The case activities that were initiated by this order. Static association to the Case Activity service.
     * @param isHoldingOrder                         Boolean, Ignored. If true then the order is a holding document, false otherwise.
     * @param isSchedulingNeeded                     Boolean, Ignored. If true, scheduling is needed for the order, false otherwise.
     * @param hasCharges                             Boolean, Ignored. If true, the order has charges related, false otherwise.
     * @param issuingAgency                          NotificationType, Optional. The source agency that issues the order, this could be a facility or an organization.
     * @param bailAmounts                            Set&lt;BailAmountType>, Optional. The bail amounts for different bail types.
     * @param bailRelationship                       String, Required. The relationship of the bail amount specified. e.g. cash- surety DISAGGREGATE (Bail set amount will be either the cash amount or Surety  amount), Cash Surety AGGREGATE(Bail set amount will be cash amount + Surety  amount).
     * @param bailPostedAmounts                      Set&lt;BailAmountType>, Optional. The amount of bail posted.
     * @param bailPaymentReceiptNo                   String(length is equal to or less than 64), Optional. Bill Payment Receipt Number for cash bail payment.
     * @param bailRequirement                        String(length is equals to or less than 128), Optional. A description of the bail requirement set at a court hearing.
     * @param bailPersonIdentityAssociations         Set&lt;Long>, Optional. Person who posted the bail for the offender. Static reference to person identity.
     * @param bondPostedAmount                       BailAmountType, Optional. The amount of bond posted.
     * @param bondPaymentDescription                 String(length is equal to or less than 512), Optional. A description of what an offender pays for a bond.
     * @param bondOrganizationAssociations           Set&lt;AssociationType>, Optional. The bond agency (organization) that posted the bail. Static reference to the organization service.
     * @param isBailAllowed                          Boolean, Required. If yes then bail is allowed, else bail is not allowed. Default is yes.
     */
    public BailType(Long orderIdentification, @NotNull String orderClassification, @NotNull String orderType, String orderSubType, @NotNull String orderCategory,
            @Size(max = 64, message = "max length 64") String orderNumber, @NotNull DispositionType orderDisposition, CommentType comments, Date orderIssuanceDate,
            Date orderReceivedDate, @NotNull Date orderStartDate, Date orderExpirationDate, Set<Long> caseActivityInitiatedOrderAssociations,
            Set<Long> orderInitiatedCaseActivityAssociations, Boolean isHoldingOrder, Boolean isSchedulingNeeded, Boolean hasCharges, NotificationType issuingAgency,
            @NotNull @Size(min = 1, message = "Cardinality [1..*]") Set<BailAmountType> bailAmounts, @NotNull String bailRelationship,
            Set<BailAmountType> bailPostedAmounts, @Size(max = 64, message = "max length 64") String bailPaymentReceiptNo,
            @Size(max = 128, message = "max length 128") String bailRequirement, Set<Long> bailPersonIdentityAssociations, BailAmountType bondPostedAmount,
            String bondPaymentDescription, Set<Long> bondOrganizationAssociations, @NotNull Boolean isBailAllowed,

            @NotNull @Size(min = 1, message = "Cardinality [1..*]") Set<Long> caseInfoIds, Set<Long> chargeIds, Set<Long> conditionIds) {
        super(orderIdentification, orderClassification, orderType, orderSubType, orderCategory, orderNumber, orderDisposition, comments, orderIssuanceDate,
                orderReceivedDate, orderStartDate, orderExpirationDate, caseActivityInitiatedOrderAssociations, orderInitiatedCaseActivityAssociations, isHoldingOrder,
                isSchedulingNeeded, hasCharges, issuingAgency, caseInfoIds, chargeIds, conditionIds);
        this.bailAmounts = bailAmounts;
        this.bailRelationship = bailRelationship;
        this.bailPostedAmounts = bailPostedAmounts;
        this.bailPaymentReceiptNo = bailPaymentReceiptNo;

        this.bailRequirement = bailRequirement;
        this.bailerPersonIdentityAssociations = bailPersonIdentityAssociations;
        this.bondPostedAmount = bondPostedAmount;
        this.bondPaymentDescription = bondPaymentDescription;
        this.bondOrganizationAssociations = bondOrganizationAssociations;
        this.isBailAllowed = isBailAllowed;
    }

    /**
     * Get Bail Amounts
     * -- The bail amounts for different bail types.
     *
     * @return the bailAmounts
     */
    public Set<BailAmountType> getBailAmounts() {
        return bailAmounts;
    }

    /**
     * Set Bail Amounts
     * -- The bail amounts for different bail types.
     *
     * @param bailAmounts the bailAmounts to set
     */
    public void setBailAmounts(Set<BailAmountType> bailAmounts) {
        this.bailAmounts = bailAmounts;
    }

    /**
     * Get Bail Relationship
     * -- The relationship of the bail amount specified.
     * e.g. cash- surety DISAGGREGATE
     * (Bail set amount will be either the cash amount or Surety  amount),
     * Cash Surety AGGREGATE(Bail set amount will be cash amount + Surety  amount)
     *
     * @return the bailRelationship
     */
    public String getBailRelationship() {
        return bailRelationship;
    }

    /**
     * Get Bail Relationship
     * -- The relationship of the bail amount specified.
     * e.g. cash- surety DISAGGREGATE
     * (Bail set amount will be either the cash amount or Surety  amount),
     * Cash Surety AGGREGATE(Bail set amount will be cash amount + Surety  amount)
     *
     * @param bailRelationship the bailRelationship to set
     */
    public void setBailRelationship(String bailRelationship) {
        this.bailRelationship = bailRelationship;
    }

    /**
     * Get Bail Posted Amount
     * -- The amount of bail posted.
     *
     * @return Set&lt;BailAmountType> a set of bailPostedAmount
     */
    public Set<BailAmountType> getBailPostedAmounts() {
        return bailPostedAmounts;
    }

    /**
     * Set Bail Posted Amount
     * -- The amount of bail posted.
     *
     * @param bailPostedAmounts the bailPostedAmount to set
     */
    public void setBailPostedAmounts(Set<BailAmountType> bailPostedAmounts) {
        this.bailPostedAmounts = bailPostedAmounts;
    }

    /**
     * Get Bail Payment Receipt No
     * -- Bill Payment Receipt Number for cash bail payment.
     *
     * @return the bailPaymentReceiptNo
     */
    public String getBailPaymentReceiptNo() {
        return bailPaymentReceiptNo;
    }

    /**
     * Set Bail Payment Receipt No
     * -- Bill Payment Receipt Number for cash bail payment.
     *
     * @param bailPaymentReceiptNo the bailPaymentReceiptNo to set
     */
    public void setBailPaymentReceiptNo(String bailPaymentReceiptNo) {
        this.bailPaymentReceiptNo = bailPaymentReceiptNo;
    }

    /**
     * Get Bail Requirement
     * -- A description of the bail requirement set at a court hearing.
     *
     * @return the bailRequirement
     */
    public String getBailRequirement() {
        return bailRequirement;
    }

    /**
     * Set Bail Requirement
     * -- A description of the bail requirement set at a court hearing.
     *
     * @param bailRequirement the bailRequirement to set
     */
    public void setBailRequirement(String bailRequirement) {
        this.bailRequirement = bailRequirement;
    }

    /**
     * Get Bailer PersonIdentity Associations
     * -- Person who posted the bail for the offender. Static reference to person identity.
     *
     * @return the bailerPersonIdentityAssociations
     */
    public Set<Long> getBailerPersonIdentityAssociations() {
        return bailerPersonIdentityAssociations;
    }

    /**
     * Set Bailer PersonIdentity Associations
     * -- Person who posted the bail for the offender. Static reference to person identity.
     *
     * @param bailerPersonIdentityAssociations the bailerPersonIdentityAssociations to set
     */
    public void setBailerPersonIdentityAssociations(Set<Long> bailerPersonIdentityAssociations) {
        this.bailerPersonIdentityAssociations = bailerPersonIdentityAssociations;
    }

    /**
     * Get Bond Posted Amount
     *
     * @return the bondPostedAmount
     */
    public BailAmountType getBondPostedAmount() {
        return bondPostedAmount;
    }

    /**
     * @param bondPostedAmount the bondPostedAmount to set
     */
    public void setBondPostedAmount(BailAmountType bondPostedAmount) {
        this.bondPostedAmount = bondPostedAmount;
    }

    /**
     * Get Bond Payment Description
     * --A description of what an offender pays for a bond.
     *
     * @return the bondPaymentDescription
     */
    public String getBondPaymentDescription() {
        return bondPaymentDescription;
    }

    /**
     * Set Bond Payment Description
     * --A description of what an offender pays for a bond.
     *
     * @param bondPaymentDescription the bondPaymentDescription to set
     */
    public void setBondPaymentDescription(String bondPaymentDescription) {
        this.bondPaymentDescription = bondPaymentDescription;
    }

    /**
     * Get Bond Organization Associations
     * -- The bond agency (organization) that posted the bail. Static reference to the organization service.
     *
     * @return the bondOrganizationAssociations
     */
    public Set<Long> getBondOrganizationAssociations() {
        return bondOrganizationAssociations;
    }

    /**
     * Set Bond Organization Associations
     * -- The bond agency (organization) that posted the bail. Static reference to the organization service.
     *
     * @param bondOrganizationAssociations the bondOrganizationAssociations to set
     */
    public void setBondOrganizationAssociations(Set<Long> bondOrganizationAssociations) {
        this.bondOrganizationAssociations = bondOrganizationAssociations;
    }

    /**
     * Get Is Bail Allowed
     * -- If yes then bail is allowed, else bail is not allowed. Default is yes.
     *
     * @return the isBailAllowed
     */
    public Boolean getIsBailAllowed() {
        return isBailAllowed;
    }

    /**
     * Set Is Bail Allowed
     * -- If yes then bail is allowed, else bail is not allowed. Default is yes.
     *
     * @param isBailAllowed the isBailAllowed to set
     */
    public void setIsBailAllowed(Boolean isBailAllowed) {
        this.isBailAllowed = isBailAllowed;
    }

    /**
     * @return the bailBondPostedDate
     */
    public Date getBailBondPostedDate() {
        return bailBondPostedDate;
    }

    /**
     * @param bailBondPostedDate the bailBondPostedDate to set
     */
    public void setBailBondPostedDate(Date bailBondPostedDate) {
        this.bailBondPostedDate = bailBondPostedDate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((bailAmounts == null) ? 0 : bailAmounts.hashCode());
        result = prime * result + ((bailPaymentReceiptNo == null) ? 0 : bailPaymentReceiptNo.hashCode());
        result = prime * result + ((bailPostedAmounts == null) ? 0 : bailPostedAmounts.hashCode());
        result = prime * result + ((bailRelationship == null) ? 0 : bailRelationship.hashCode());
        result = prime * result + ((bailRequirement == null) ? 0 : bailRequirement.hashCode());
        result = prime * result + ((bailerPersonIdentityAssociations == null) ? 0 : bailerPersonIdentityAssociations.hashCode());
        result = prime * result + ((bondOrganizationAssociations == null) ? 0 : bondOrganizationAssociations.hashCode());
        result = prime * result + ((bondPaymentDescription == null) ? 0 : bondPaymentDescription.hashCode());
        result = prime * result + ((bondPostedAmount == null) ? 0 : bondPostedAmount.hashCode());
        result = prime * result + ((isBailAllowed == null) ? 0 : isBailAllowed.hashCode());
        result = prime * result + ((bailBondPostedDate == null) ? 0 : bailBondPostedDate.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        BailType other = (BailType) obj;
        if (bailAmounts == null) {
            if (other.bailAmounts != null) {
				return false;
			}
        } else if (!bailAmounts.equals(other.bailAmounts)) {
			return false;
		}
        if (bailPaymentReceiptNo == null) {
            if (other.bailPaymentReceiptNo != null) {
				return false;
			}
        } else if (!bailPaymentReceiptNo.equals(other.bailPaymentReceiptNo)) {
			return false;
		}
        if (bailPostedAmounts == null) {
            if (other.bailPostedAmounts != null) {
				return false;
			}
        } else if (!bailPostedAmounts.equals(other.bailPostedAmounts)) {
			return false;
		}
        if (bailRelationship == null) {
            if (other.bailRelationship != null) {
				return false;
			}
        } else if (!bailRelationship.equals(other.bailRelationship)) {
			return false;
		}
        if (bailRequirement == null) {
            if (other.bailRequirement != null) {
				return false;
			}
        } else if (!bailRequirement.equals(other.bailRequirement)) {
			return false;
		}
        if (bailerPersonIdentityAssociations == null) {
            if (other.bailerPersonIdentityAssociations != null) {
				return false;
			}
        } else if (!bailerPersonIdentityAssociations.equals(other.bailerPersonIdentityAssociations)) {
			return false;
		}
        if (bondOrganizationAssociations == null) {
            if (other.bondOrganizationAssociations != null) {
				return false;
			}
        } else if (!bondOrganizationAssociations.equals(other.bondOrganizationAssociations)) {
			return false;
		}
        if (bondPaymentDescription == null) {
            if (other.bondPaymentDescription != null) {
				return false;
			}
        } else if (!bondPaymentDescription.equals(other.bondPaymentDescription)) {
			return false;
		}
        if (bondPostedAmount == null) {
            if (other.bondPostedAmount != null) {
				return false;
			}
        } else if (!bondPostedAmount.equals(other.bondPostedAmount)) {
			return false;
		}
        if (isBailAllowed == null) {
            if (other.isBailAllowed != null) {
				return false;
			}
        } else if (!isBailAllowed.equals(other.isBailAllowed)) {
			return false;
		}
        if (bailBondPostedDate == null) {
            if (other.bailBondPostedDate != null) {
				return false;
			}
        } else if (!bailBondPostedDate.equals(other.bailBondPostedDate)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "BailType [bailAmounts=" + bailAmounts + ", bailRelationship=" + bailRelationship + ", bailPostedAmounts=" + bailPostedAmounts + ", bailPaymentReceiptNo="
                + bailPaymentReceiptNo + ", bailRequirement=" + bailRequirement + ", bailerPersonIdentityAssociations=" + bailerPersonIdentityAssociations
                + ", bondPostedAmount=" + bondPostedAmount + ", bondPaymentDescription=" + bondPaymentDescription + ", bondOrganizationAssociations="
                + bondOrganizationAssociations + ", isBailAllowed=" + isBailAllowed + ", toString()=" + super.toString() + ", bailBondPostedDate=" + bailBondPostedDate
                + "]";
    }

}
