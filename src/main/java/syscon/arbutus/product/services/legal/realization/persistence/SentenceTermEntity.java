package syscon.arbutus.product.services.legal.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of SentenceTermEntity
 *
 * @author Ashish
 * @version 1.0
 * @DbComment LEG_SentenceTerm
 * @since December 9, 2013
 */
@Entity
@Table(name = "LEG_SENTENCETERM")
@SQLDelete(sql = "UPDATE LEG_SentenceTerm SET flag = 4 WHERE sentenceTermTypeId = ? and version = ?")
@Where(clause = "flag = 1")

public class SentenceTermEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * @DbComment Id 'Unique identification for SentenceTermEntity
     * instance'
     */
    @Id
    @Column(name = "SENTENCETERMID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_SENTENCETERMID_Id")
    @SequenceGenerator(name = "SEQ_LEG_SENTENCETERMID_Id", sequenceName = "SEQ_LEG_SENTENCETERMID_Id", allocationSize = 1)
    private Long sentenceTermId;

    /**
     * @DbComment sentenceType 'reference code of sentenceType of SentenceTermEntity'
     */

    @Column(name = "SENTENCETYPE", nullable = false)
    @MetaCode(set = MetaSet.SENTENCE_TYPE)
    private String sentenceType;

    /**
     * @DbComment sentenceType 'reference code of termType of SentenceTermEntity'
     */
    @Column(name = "TERMTYPE", nullable = false)
    @MetaCode(set = MetaSet.TERM_TYPE)
    private String termType;

    @OneToMany(mappedBy = "sentenceTermEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @BatchSize(size = 20)
    private List<SentenceTermNameEntity> sentenceTermNameEntity = new ArrayList<SentenceTermNameEntity>();

    /**
     * @DbComment sentenceType 'de-activation date of SentenceTermEntity'
     */
    @Column(name = "DEACTIVATIONDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deactivationDate;

    /**
     * @DbComment CREATEUSERID 'User ID who created the object'
     * @DbComment CREATEDATETIME 'Date and time when the object was created'
     * @DbComment MODIFYUSERID 'User ID who last updated the object'
     * @DbComment MODIFYDATETIME 'Date and time when the object was last updated'
     * @DbComment INVOCATIONCONTEXT 'Invocation context when the create/update action called'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'the version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * Default Constructor
     */
    public SentenceTermEntity() {
        super();

    }

    /**
     * Get id of SentenceTermEntity Object
     *
     * @return the sentenceTermId
     */
    public Long getSentenceTermId() {
        return sentenceTermId;
    }

    /**
     * Set id of SentenceTermEntity Object
     *
     * @param the sentenceTermId
     */
    public void setSentenceTermId(Long sentenceTermId) {
        this.sentenceTermId = sentenceTermId;
    }

    /**
     * Get Sentence Type
     * -- The type of sentence (Definite, Indeterminant, Determinant etc.).
     *
     * @return the sentenceType
     */
    public String getSentenceType() {
        return sentenceType;
    }

    /**
     * Set Sentence Type
     * -- The type of term (Definite, Indeterminant, Determinant etc.).
     *
     * @param sentenceType the sentenceType to set
     */
    public void setSentenceType(String sentenceType) {
        this.sentenceType = sentenceType;
    }

    /**
     * Get Term Type
     * -- The type of term (Single Term, Dual Term, Three Term etc.).
     *
     * @return the termType
     */
    public String getTermType() {
        return termType;
    }

    /**
     * Set Term Type
     * -- The type of term (Single Term, Dual Term, Three Term etc.).
     *
     * @param termType the termType to set
     */
    public void setTermType(String termType) {
        this.termType = termType;
    }

    /**
     * Get deactivationDate of SentenceTermEntity Object
     *
     * @return the sentenceTermTypeId
     */
    public Date getDeactivationDate() {
        return deactivationDate;
    }

    /**
     * Set deactivationDate of SentenceTermEntity Object
     *
     * @param the sentenceTermTypeId
     */
    public void setDeactivationDate(Date deactivationDate) {
        this.deactivationDate = deactivationDate;
    }

    /**
     * Get List of Term Name
     * -- The name of the term ( Maximum, Minimum etc.).
     *
     * @return the SentenceTermNameEntity
     */
    public List<SentenceTermNameEntity> getSentenceTermNameEntityList() {
        return sentenceTermNameEntity;
    }

    /**
     * set List of Term Name
     * -- The name of the term ( Maximum, Minimum etc.).
     *
     * @param the SentenceTermNameEntity
     */
    public void setSentenceTermNameEntityList(List<SentenceTermNameEntity> sentenceTermNameEntity) {
        this.sentenceTermNameEntity = sentenceTermNameEntity;
    }

    /**
     * @return stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sentenceTermId == null) ? 0 : sentenceTermId.hashCode());
        result = prime * result + ((sentenceType == null) ? 0 : sentenceType.hashCode());
        result = prime * result + ((termType == null) ? 0 : termType.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SentenceTermEntity other = (SentenceTermEntity) obj;
        if (sentenceTermId == null) {
            if (other.sentenceTermId != null) {
				return false;
			}
        } else if (!sentenceTermId.equals(other.sentenceTermId)) {
			return false;
		}
        if (sentenceType == null) {
            if (other.sentenceType != null) {
				return false;
			}
        } else if (!sentenceType.equals(other.sentenceType)) {
			return false;
		}
        if (termType == null) {
            if (other.termType != null) {
				return false;
			}
        } else if (!termType.equals(other.termType)) {
			return false;
		}
        if (version == null) {
            if (other.version != null) {
				return false;
			}
        } else if (!version.equals(other.version)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "SentenceTermEntity [sentenceTermId=" + sentenceTermId + ", sentenceType=" + sentenceType + ", termType=" + termType + ", deactivationDate="
                + deactivationDate + "]";
    }

}
