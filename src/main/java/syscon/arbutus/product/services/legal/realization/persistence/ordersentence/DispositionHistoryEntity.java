package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * DispositionHistoryEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdDisposHst 'The Disposition History table for Order Sentence module of Legal service'
 * @since December 21, 2012
 */
//@Audited
@Entity
@Table(name = "LEG_OrdDisposHst")
public class DispositionHistoryEntity implements Serializable {

    private static final long serialVersionUID = -737931375114512739L;

    /**
     * @DbComment hstId 'Unique identification of a disposition history instance.'
     */
    @Id
    @Column(name = "hstId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDDISPOSHST_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDDISPOSHST_ID", sequenceName = "SEQ_LEG_ORDDISPOSHST_ID", allocationSize = 1)
    private Long hisotryId;

    /**
     * @DbComment dispositionId 'Unique identification of a disposition instance.'
     */
    @Column(name = "dispositionId", nullable = false)
    private Long dispositionId;

    /**
     * @DbComment dispositionOutcome 'A result or outcome of the disposition.'
     */
    @Column(name = "dispositionOutcome", nullable = false, length = 64)
    private String dispositionOutcome;

    /**
     * @DbComment orderStatus 'The status of an order, active or inactive.'
     */
    @Column(name = "orderStatus", nullable = false, length = 64)
    private String orderStatus;

    /**
     * @DbComment dispositionDate 'Date of disposition.'
     */
    @Column(name = "dispositionDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dispositionDate;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @return the hisotryId
     */
    public Long getHisotryId() {
        return hisotryId;
    }

    /**
     * @param hisotryId the hisotryId to set
     */
    public void setHisotryId(Long hisotryId) {
        this.hisotryId = hisotryId;
    }

    /**
     * @return the dispositionId
     */
    public Long getDispositionId() {
        return dispositionId;
    }

    /**
     * @param dispositionId the dispositionId to set
     */
    public void setDispositionId(Long dispositionId) {
        this.dispositionId = dispositionId;
    }

    /**
     * @return the dispositionOutcome
     */
    public String getDispositionOutcome() {
        return dispositionOutcome;
    }

    /**
     * @param dispositionOutcome the dispositionOutcome to set
     */
    public void setDispositionOutcome(String dispositionOutcome) {
        this.dispositionOutcome = dispositionOutcome;
    }

    /**
     * @return the orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }

    /**
     * @param orderStatus the orderStatus to set
     */
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * @return the dispositionDate
     */
    public Date getDispositionDate() {
        return dispositionDate;
    }

    /**
     * @param dispositionDate the dispositionDate to set
     */
    public void setDispositionDate(Date dispositionDate) {
        this.dispositionDate = dispositionDate;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (!(o instanceof DispositionHistoryEntity)) {
			return false;
		}

        DispositionHistoryEntity that = (DispositionHistoryEntity) o;

        if (dispositionDate != null ? !dispositionDate.equals(that.dispositionDate) : that.dispositionDate != null) {
			return false;
		}
        if (dispositionId != null ? !dispositionId.equals(that.dispositionId) : that.dispositionId != null) {
			return false;
		}
        if (dispositionOutcome != null ? !dispositionOutcome.equals(that.dispositionOutcome) : that.dispositionOutcome != null) {
			return false;
		}
        if (orderStatus != null ? !orderStatus.equals(that.orderStatus) : that.orderStatus != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = dispositionId != null ? dispositionId.hashCode() : 0;
        result = 31 * result + (dispositionOutcome != null ? dispositionOutcome.hashCode() : 0);
        result = 31 * result + (orderStatus != null ? orderStatus.hashCode() : 0);
        result = 31 * result + (dispositionDate != null ? dispositionDate.hashCode() : 0);
        return result;
    }

    /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
    @Override
    public String toString() {
        return "DispositionHistoryEntity [hisotryId=" + hisotryId + ", dispositionId=" + dispositionId + ", dispositionOutcome=" + dispositionOutcome + ", orderStatus="
                + orderStatus + ", dispositionDate=" + dispositionDate + "]";
    }

}
