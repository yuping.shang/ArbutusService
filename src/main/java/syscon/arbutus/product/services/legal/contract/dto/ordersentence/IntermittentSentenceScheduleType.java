package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

/**
 * IntermittentSentenceScheduleType for Case Management Service
 *
 * @author amlendu.kumar
 * @version 1.0
 * @since May 6, 2014
 */
@ArbutusConstraint(constraints = "startTime <= endTime")
public class IntermittentSentenceScheduleType implements Serializable {

    private static final long serialVersionUID = 3478611433275992820L;
    @NotNull
    private String dayIn;
    @NotNull
    private TimeValue timeIn;
    @NotNull
    private Date dateIn;
    @NotNull
    private String dayOut;
    @NotNull
    private TimeValue timeOut;
    @NotNull
    private Date dateOut;
    @NotNull
    private Long locationId;
    @NotNull
    private Long orderId;
    private String outCome;
    private Long intermittentScheduleId;
    private Boolean status;
    @Size(max = 512)
    private String comments;
    /**
     * Constructor
     */
    public IntermittentSentenceScheduleType() {
        super();
    }

    /**
     * Get Reporting in day of week.
     *
     * @return the dayIn - String
     */
    public String getDayIn() {
        return dayIn;
    }

    /**
     * Set Reporting in day of week.
     *
     * @param dayIn String - required
     */
    public void setDayIn(String dayIn) {
        this.dayIn = dayIn;
    }

    /**
     * Get In Time
     * -- Start time on the day of the week.
     * Only time portion is used.
     *
     * @return the timeIn
     */
    public TimeValue getTimeIn() {
        return timeIn;
    }

    /**
     * Set In Time
     * -- Start time on the day of the week.
     * Only time portion is used.
     *
     * @param timeIn String - Required
     */
    public void setTimeIn(TimeValue timeIn) {
        this.timeIn = timeIn;
    }

    /**
     * toStartTtime
     *
     * @return String returns the string of startTime
     */
    public String toStartTime() {
        return timeIn.toString();
    }

    /**
     * Get Time out
     * -- End time on the day of the week.
     * Only time portion is used.
     *
     * @return the timeOut
     */
    public TimeValue getTimeOut() {
        return timeOut;
    }

    /**
     * Set Time out
     * -- End time on the day of the week.
     * Only time portion is used.
     *
     * @param timeOut String - Required
     */
    public void setTimeOut(TimeValue timeOut) {
        this.timeOut = timeOut;
    }

    /**
     * toEndTime
     *
     * @return String returns string of endTime
     */
    public String toEndTime() {
        return timeOut.toString();
    }

    /**
     * reportingDate
     * -- Intermittent reporting date for the schedule
     *
     * @param DateIn the reporting date to set - required
     */
    public Date getDateIn() {
        return dateIn;
    }

    /**
     * dateIn
     * -- Intermittent reporting date for the schedule
     *
     * @return the dateIn
     */
    public void setDateIn(Date dateIn) {
        this.dateIn = dateIn;
    }

    /**
     * dateIn
     * -- Intermittent release date for the schedule
     *
     * @return dateOut Date
     */
    public Date getDateOut() {
        return dateOut;
    }

    /**
     * dateOut
     * --  Intermittent release date for the schedule
     *
     * @param dateOut the Date -  - Required
     */
    public void setDateOut(Date dateOut) {
        this.dateOut = dateOut;
    }

    /**
     * locationId of Reporting Location
     *
     * @return the locationId
     */
    public Long getLocationId() {
        return locationId;
    }

    /**
     * set the Reporting Location
     *
     * @param locationId the locationId to set
     */
    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    /**
     * Get Reporting out day of week.
     *
     * @return the dayOut - String
     */
    public String getDayOut() {
        return dayOut;
    }

    /**
     * Set Reporting out day of week.
     *
     * @param dayOut String - Required
     */
    public void setDayOut(String dayOut) {
        this.dayOut = dayOut;
    }

    /**
     * Get Sentence (order) Id .
     *
     * @return orderId Long
     */
    public Long getOrderId() {
        return orderId;
    }

    /**
     * Set Sentence (order) Id.
     *
     * @param orderId Long
     */
    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    /**
     * Get outcome of the schedule.
     *
     * @return the outCome String
     */
    public String getOutCome() {
        return outCome;
    }

    /**
     * Set outcome of the schedule.
     *
     * @param outCome String
     */
    public void setOutCome(String outCome) {
        this.outCome = outCome;
    }

    /**
     * Get status of the schedule.
     *
     * @return the status Boolean
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * Set status of the schedule.
     *
     * @param status Boolean
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * Get Intermittent Schedule Id.
     *
     * @return the intermittentScheduleId Long
     */
    public Long getIntermittentScheduleId() {
        return intermittentScheduleId;
    }

    /**
     * Set Intermittent Schedule Id.
     *
     * @param intermittentScheduleId Long
     */
    public void setIntermittentScheduleId(Long intermittentScheduleId) {
        this.intermittentScheduleId = intermittentScheduleId;
    }

    /**
     * Get comments for the schedule.
     *
     * @return the comments String
     */
    public String getComments() {
        return comments;
    }

    /**
     * Set comments for the schedule.
     *
     * @param comments String
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((comments == null) ? 0 : comments.hashCode());
        result = prime * result + ((dateIn == null) ? 0 : dateIn.hashCode());
        result = prime * result + ((dateOut == null) ? 0 : dateOut.hashCode());
        result = prime * result + ((dayIn == null) ? 0 : dayIn.hashCode());
        result = prime * result + ((dayOut == null) ? 0 : dayOut.hashCode());
        result = prime * result + ((intermittentScheduleId == null) ? 0 : intermittentScheduleId.hashCode());
        result = prime * result + ((locationId == null) ? 0 : locationId.hashCode());
        result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
        result = prime * result + ((outCome == null) ? 0 : outCome.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((timeIn == null) ? 0 : timeIn.hashCode());
        result = prime * result + ((timeOut == null) ? 0 : timeOut.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        IntermittentSentenceScheduleType other = (IntermittentSentenceScheduleType) obj;
        if (comments == null) {
            if (other.comments != null) {
				return false;
			}
        } else if (!comments.equals(other.comments)) {
			return false;
		}
        if (dateIn == null) {
            if (other.dateIn != null) {
				return false;
			}
        } else if (!dateIn.equals(other.dateIn)) {
			return false;
		}
        if (dateOut == null) {
            if (other.dateOut != null) {
				return false;
			}
        } else if (!dateOut.equals(other.dateOut)) {
			return false;
		}
        if (dayIn == null) {
            if (other.dayIn != null) {
				return false;
			}
        } else if (!dayIn.equals(other.dayIn)) {
			return false;
		}
        if (dayOut == null) {
            if (other.dayOut != null) {
				return false;
			}
        } else if (!dayOut.equals(other.dayOut)) {
			return false;
		}
        if (intermittentScheduleId == null) {
            if (other.intermittentScheduleId != null) {
				return false;
			}
        } else if (!intermittentScheduleId.equals(other.intermittentScheduleId)) {
			return false;
		}
        if (locationId == null) {
            if (other.locationId != null) {
				return false;
			}
        } else if (!locationId.equals(other.locationId)) {
			return false;
		}
        if (orderId == null) {
            if (other.orderId != null) {
				return false;
			}
        } else if (!orderId.equals(other.orderId)) {
			return false;
		}
        if (outCome == null) {
            if (other.outCome != null) {
				return false;
			}
        } else if (!outCome.equals(other.outCome)) {
			return false;
		}
        if (status == null) {
            if (other.status != null) {
				return false;
			}
        } else if (!status.equals(other.status)) {
			return false;
		}
        if (timeIn == null) {
            if (other.timeIn != null) {
				return false;
			}
        } else if (!timeIn.equals(other.timeIn)) {
			return false;
		}
        if (timeOut == null) {
            if (other.timeOut != null) {
				return false;
			}
        } else if (!timeOut.equals(other.timeOut)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "IntermittentSentenceScheduleType [dayIn=" + dayIn + ", timeIn=" + timeIn + ", dateIn=" + dateIn + ", dayOut=" + dayOut + ", timeOut=" + timeOut
                + ", dateOut=" + dateOut + ", locationId=" + locationId + ", orderId=" + orderId + ", outCome=" + outCome + ", intermittentScheduleId="
                + intermittentScheduleId + ", status=" + status + ", comments=" + comments + "]";
    }

    /**
     * This method return the hours spend in a day in same day schedule
     *
     * @return int
     */
    public int getHoursInSameDay() {
        if (this.dateIn.equals(this.dateOut)) {
            return Math.abs((int) (timeOut.hour() - timeIn.hour()));
        } else {
            return 0;
        }

    }

    /**
     * Inner Class
     */
    public class TimeValue implements Serializable, Comparable<TimeValue> {

        private static final long serialVersionUID = -737931375114572739L;

        private final Long hour, minute, second;

        public TimeValue(Long hour, Long minute, Long second) {
            super();
            if (!(hour != null && hour >= 0 && hour < 24)) {
                throw new AssertionError("Input was: " + hour + " to make hour: " + hour);
            }
            this.hour = hour;

            if (!(minute != null && minute >= 0 && minute < 60)) {
                throw new AssertionError("Input was: " + minute + " to make minute: " + minute);
            }
            this.minute = minute;

            if (!(second != null && second >= 0 && second < 60)) {
                throw new AssertionError("Input was: " + second + " to make second: " + second);
            }
            this.second = second;
        }

        /**
         * The hour in the range 0 through 24.
         *
         * @return the hour
         */
        public Long hour() {
            return hour;
        }

        /**
         * The minute in the range 0 through 59.
         *
         * @return the minute
         */
        public Long minute() {
            return minute;
        }

        /**
         * The second in the range 0 through 59.
         *
         * @return the second
         */
        public Long second() {
            return second;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((hour == null) ? 0 : hour.hashCode());
            result = prime * result + ((minute == null) ? 0 : minute.hashCode());
            result = prime * result + ((second == null) ? 0 : second.hashCode());
            return result;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
				return true;
			}
            if (obj == null) {
				return false;
			}
            if (getClass() != obj.getClass()) {
				return false;
			}
            TimeValue other = (TimeValue) obj;
            if (hour == null) {
                if (other.hour != null) {
					return false;
				}
            } else if (!hour.equals(other.hour)) {
				return false;
			}
            if (minute == null) {
                if (other.minute != null) {
					return false;
				}
            } else if (!minute.equals(other.minute)) {
				return false;
			}
            if (second == null) {
                if (other.second != null) {
					return false;
				}
            } else if (!second.equals(other.second)) {
				return false;
			}
            return true;
        }

        @Override
        public int compareTo(TimeValue o) {
            Calendar cal = Calendar.getInstance();
            cal.clear();
            cal.set(1970, 0, 1);
            cal.set(Calendar.MILLISECOND, 0);

            cal.set(Calendar.HOUR_OF_DAY, this.hour().intValue());
            cal.set(Calendar.MINUTE, this.minute().intValue());
            cal.set(Calendar.SECOND, this.second().intValue());
            Date thisDate = cal.getTime();

            cal.set(Calendar.HOUR_OF_DAY, o.hour().intValue());
            cal.set(Calendar.MINUTE, o.minute().intValue());
            cal.set(Calendar.SECOND, o.second().intValue());
            Date otherDate = cal.getTime();

            return thisDate.compareTo(otherDate);
        }

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return String.format("%02d:%02d:%02d", hour, minute, second);
        }
    }

}
