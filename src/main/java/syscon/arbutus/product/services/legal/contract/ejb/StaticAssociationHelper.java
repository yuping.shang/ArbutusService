package syscon.arbutus.product.services.legal.contract.ejb;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.*;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.contract.dto.AssociationType;
import syscon.arbutus.product.services.legal.contract.dto.LegalModule;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.*;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.OrderEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;

public class StaticAssociationHelper {

    /* Bean conversion methods */

    /**
     * Get the to-idenfier of an association.
     */
    public static Long toIdentifier(AssociationType association) {

        if (association == null) {
			return null;
		}

        return association.getToIdentifier();
    }

    /**
     * Create an ChargeAssociationEntity object from an AssociationType object.
     *
     * @param associationId
     * @param fromIdentifier
     * @param association
     * @return ChargeAssociationEntity
     */
    public static CAChargeAssociationEntity toChargeAssociationEntity(Long associationId, Long fromIdentifier, AssociationType association, StampEntity stamp,
            CaseActivityEntity caseActivity) {

        if (association == null) {
			return null;
		}

        return new CAChargeAssociationEntity(associationId, fromIdentifier, association.getToIdentifier(), stamp, caseActivity);

    }

    /**
     * Create an ChargeAssociationHistEntity object.
     *
     * @param associationId
     * @param fromIdentifier
     * @param association
     * @return ChargeAssociationHistEntity
     */
    public static CAChargeAssociationHistEntity toChargeAssociationHistEntity(Long associationId, Long fromIdentifier, CAChargeAssociationEntity association,
            StampEntity stamp, CaseActivityHistEntity caseActivity) {

        if (association == null) {
			return null;
		}

        return new CAChargeAssociationHistEntity(associationId, fromIdentifier, association.getToIdentifier(), stamp, caseActivity);

    }

    /**
     * Create an OrderInitiatedAssociationHistEntity object.
     *
     * @param associationId
     * @param fromIdentifier
     * @param association
     * @return OrderInitiatedAssociationHistEntity
     */
    public static OrderInitiatedAssociationHistEntity toOrderInitiatedAssociationHistEntity(Long associationId, Long fromIdentifier, OrderEntity association,
            StampEntity stamp, CourtActivityHistEntity caseActivity) {

        if (association == null) {
			return null;
		}

        return new OrderInitiatedAssociationHistEntity(associationId, fromIdentifier, association.getOrderId(), stamp, caseActivity);

    }

    /**
     * Create an OrderResultedAssociationHistEntity object.
     *
     * @param associationId
     * @param fromIdentifier
     * @param association
     * @return OrderResultedAssociationHistEntity
     */
    public static OrderResultedAssociationHistEntity toOrderResultedAssociationHistEntity(Long associationId, Long fromIdentifier, OrderEntity association,
            StampEntity stamp, CourtActivityHistEntity caseActivity) {

        if (association == null) {
			return null;
		}

        return new OrderResultedAssociationHistEntity(associationId, fromIdentifier, association.getOrderId(), stamp, caseActivity);

    }

    /**
     * Create a set of ChargeAssociationEntity objects from a set of AssociationType objects.
     *
     * @param associationId
     * @param fromIdentifier
     * @param associations
     * @return static Set<ChargeAssociationEntity>
     */
    @SuppressWarnings("rawtypes")
    public static Set<CAChargeAssociationEntity> toChargeAssociationEntitySet(Long associationId, Long fromIdentifier, StampEntity stamp, CaseActivityEntity caseActivity,
            Set<Long> associations) {

        Set<CAChargeAssociationEntity> ret = new HashSet<CAChargeAssociationEntity>();

        if (associations == null) {
			return ret;
		}

        Iterator it = associations.iterator();
        while (it.hasNext()) {
            AssociationType association = new AssociationType(LegalModule.CHARGE.value(), (Long) it.next());
            CAChargeAssociationEntity chargeEntity = toChargeAssociationEntity(associationId, fromIdentifier, association, stamp, null);
            chargeEntity.setCaseActivity(caseActivity);
            ret.add(chargeEntity);
        }

        return ret;
    }

    /**
     * Create a set of ChargeAssociationHistEntity objects from a set of ChargeAssociationEntity objects.
     *
     * @param associationId
     * @param fromIdentifier
     * @param associations
     * @return static Set<ChargeAssociationHistEntity>
     */
    @SuppressWarnings("rawtypes")
    public static Set<CAChargeAssociationHistEntity> toChargeAssociationHistEntitySet(Long associationId, Long fromIdentifier, StampEntity stamp,
            CaseActivityHistEntity caseActivity, Set<CAChargeAssociationEntity> associations) {

        Set<CAChargeAssociationHistEntity> ret = new HashSet<CAChargeAssociationHistEntity>();

        if (associations == null) {
			return ret;
		}

        Iterator it = associations.iterator();
        while (it.hasNext()) {
            CAChargeAssociationEntity association = (CAChargeAssociationEntity) it.next();
            CAChargeAssociationHistEntity chargeHistEntity = toChargeAssociationHistEntity(associationId, fromIdentifier, association, stamp, null);
            chargeHistEntity.setCaseActivityHist(caseActivity);
            ret.add(chargeHistEntity);
        }

        return ret;
    }

    /**
     * Create a set of OrderInitiatedAssociationHistEntity objects from a set of OrderInitiatedAssociationEntity objects.
     *
     * @param associationId
     * @param fromIdentifier
     * @param associations
     * @return static Set<OrderInitiatedAssociationHistEntity>
     */
    @SuppressWarnings("rawtypes")
    public static Set<OrderInitiatedAssociationHistEntity> toOrderInitiatedAssociationHistEntitySet(Long associationId, Long fromIdentifier, StampEntity stamp,
            CourtActivityHistEntity caseActivity, Set<OrderEntity> associations) {

        Set<OrderInitiatedAssociationHistEntity> ret = new HashSet<OrderInitiatedAssociationHistEntity>();

        if (associations == null) {
			return ret;
		}

        Iterator it = associations.iterator();
        while (it.hasNext()) {
            OrderEntity association = (OrderEntity) it.next();
            OrderInitiatedAssociationHistEntity orderInitiatedAssociationHistEntity = toOrderInitiatedAssociationHistEntity(associationId, fromIdentifier, association,
                    stamp, null);
            orderInitiatedAssociationHistEntity.setCourtActivityHist(caseActivity);
            ret.add(orderInitiatedAssociationHistEntity);
        }

        return ret;
    }

    /**
     * Create a set of OrderInitiatedAssociationHistEntity objects from a set of OrderInitiatedAssociationEntity objects.
     *
     * @param associationId
     * @param fromIdentifier
     * @param associations
     * @return static Set<OrderInitiatedAssociationHistEntity>
     */
    @SuppressWarnings("rawtypes")
    public static Set<OrderResultedAssociationHistEntity> toOrderResultedAssociationHistEntitySet(Long associationId, Long fromIdentifier, StampEntity stamp,
            CourtActivityHistEntity caseActivity, Set<OrderEntity> associations) {

        Set<OrderResultedAssociationHistEntity> ret = new HashSet<OrderResultedAssociationHistEntity>();

        if (associations == null) {
			return ret;
		}

        Iterator it = associations.iterator();
        while (it.hasNext()) {
            OrderEntity association = (OrderEntity) it.next();
            OrderResultedAssociationHistEntity orderResultedAssociationHistEntity = toOrderResultedAssociationHistEntity(associationId, fromIdentifier, association,
                    stamp, null);
            orderResultedAssociationHistEntity.setCaseActivityHist(caseActivity);
            ret.add(orderResultedAssociationHistEntity);
        }

        return ret;
    }

    /**
     * Create an AssociationType object from a ChargeAssociationEntity object.
     *
     * @param association
     * @return Association
     */
    public static AssociationType toAssociationType(CAChargeAssociationEntity association) {

        if (association == null) {
			return null;
		}

        return new AssociationType(LegalModule.CHARGE.value(), association.getToIdentifier());

    }

    /**
     * Create an AssociationType object from a ChargeAssociationHistEntity object.
     *
     * @param association
     * @return Association
     */
    public static AssociationType toAssociationType(CAChargeAssociationHistEntity association) {

        if (association == null) {
			return null;
		}

        return new AssociationType(LegalModule.CHARGE.value(), association.getToIdentifier());

    }

    /**
     * Create an AssociationType object from an OrderInitiatedAssociationHistEntity object.
     *
     * @param association
     * @return Association
     */
    public static AssociationType toAssociationType(OrderInitiatedAssociationHistEntity association) {

        if (association == null) {
			return null;
		}

        return new AssociationType(LegalModule.ORDER_SENTENCE.value(), association.getToIdentifier());

    }

    /**
     * Create an AssociationType object from an OrderResultedAssociationHistEntity object.
     *
     * @param association
     * @return Association
     */
    public static AssociationType toAssociationType(OrderResultedAssociationHistEntity association) {

        if (association == null) {
			return null;
		}

        return new AssociationType(LegalModule.ORDER_SENTENCE.value(), association.getToIdentifier());

    }

    /**
     * Create an AssociationType object.
     *
     * @param toClass
     * @param toIdentifier
     * @return Association
     */
    public static AssociationType toAssociationType(String toClass, Long toIdentifier) {

        if (BeanHelper.isEmpty(toIdentifier)) {
			return null;
		}

        return new AssociationType(toClass, toIdentifier);
    }

    /**
     * Create a set of AssociationType objects from a set ChargeAssociationEntity objects.
     *
     * @param associations
     * @return Set<AssociationType>
     */
    @SuppressWarnings("rawtypes")
    public static Set<AssociationType> toChargeAssociationTypeSet(Set<CAChargeAssociationEntity> associations) {

        Set<AssociationType> ret = new HashSet<AssociationType>();

        if (associations == null) {
			return ret;
		}

        Iterator it = associations.iterator();
        while (it.hasNext()) {
            CAChargeAssociationEntity association = (CAChargeAssociationEntity) it.next();
            if (association != null) {
				ret.add(toAssociationType(association));
			}
        }

        return ret;
    }

    /**
     * Create a set of Long objects from a set ChargeAssociationEntity objects.
     *
     * @param associations
     * @return Set<AssociationType>
     */
    @SuppressWarnings("rawtypes")
    public static Set<Long> toChargeIdSet(Set<CAChargeAssociationEntity> associations) {

        Set<Long> ret = new HashSet<Long>();

        if (associations == null) {
			return ret;
		}

        Iterator it = associations.iterator();
        while (it.hasNext()) {
            CAChargeAssociationEntity association = (CAChargeAssociationEntity) it.next();
            if (association != null) {
				ret.add(association.getToIdentifier());
			}
        }

        return ret;
    }

    /**
     * Create a set of AssociationType objects from a set ChargeAssociationHistEntity objects.
     *
     * @param associations
     * @return Set<AssociationType>
     */
    @SuppressWarnings("rawtypes")
    public static Set<AssociationType> toChargeAssociationTypeSet2(Set<CAChargeAssociationHistEntity> associations) {

        Set<AssociationType> ret = new HashSet<AssociationType>();

        if (associations == null) {
			return ret;
		}

        Iterator it = associations.iterator();
        while (it.hasNext()) {
            CAChargeAssociationHistEntity association = (CAChargeAssociationHistEntity) it.next();
            if (association != null) {
				ret.add(toAssociationType(association));
			}
        }

        return ret;
    }

    /**
     * Create a set of AssociationType objects from a set ChargeAssociationHistEntity objects.
     *
     * @param associations
     * @return Set<AssociationType>
     */
    @SuppressWarnings("rawtypes")
    public static Set<Long> toChargeIds2(Set<CAChargeAssociationHistEntity> associations) {

        Set<Long> ret = new HashSet<Long>();

        if (associations == null) {
			return ret;
		}

        Iterator it = associations.iterator();
        while (it.hasNext()) {
            CAChargeAssociationHistEntity association = (CAChargeAssociationHistEntity) it.next();
            if (association != null) {
				ret.add(association.getToIdentifier());
			}
        }

        return ret;
    }

    /**
     * Create a set of AssociationType objects from an set OrderInitiatedAssociationHistEntity objects.
     *
     * @param associations
     * @return Set<AssociationType>
     */
    @SuppressWarnings("rawtypes")
    public static Set<AssociationType> toOrderInitiatedAssociationTypeSet2(Set<OrderInitiatedAssociationHistEntity> associations) {

        Set<AssociationType> ret = new HashSet<AssociationType>();

        if (associations == null) {
			return ret;
		}

        Iterator it = associations.iterator();
        while (it.hasNext()) {
            OrderInitiatedAssociationHistEntity association = (OrderInitiatedAssociationHistEntity) it.next();
            if (association != null) {
				ret.add(toAssociationType(association));
			}
        }

        return ret;
    }

    /**
     * Create a set of AssociationType objects from an set OrderInitiatedAssociationHistEntity objects.
     *
     * @param associations
     * @return Set<AssociationType>
     */
    @SuppressWarnings("rawtypes")
    public static Set<Long> toOrderInitiatedIds2(Set<OrderInitiatedAssociationHistEntity> associations) {

        Set<Long> ret = new HashSet<Long>();

        if (associations == null) {
			return ret;
		}

        Iterator it = associations.iterator();
        while (it.hasNext()) {
            OrderInitiatedAssociationHistEntity association = (OrderInitiatedAssociationHistEntity) it.next();
            if (association != null) {
				ret.add(association.getToIdentifier());
			}
        }

        return ret;
    }

    /**
     * Create a set of AssociationType objects from an set OrderResultedAssociationHistEntity objects.
     *
     * @param associations
     * @return Set<AssociationType>
     */
    @SuppressWarnings("rawtypes")
    public static Set<AssociationType> toOrderResultedAssociationTypeSet2(Set<OrderResultedAssociationHistEntity> associations) {

        Set<AssociationType> ret = new HashSet<AssociationType>();

        if (associations == null) {
			return ret;
		}

        Iterator it = associations.iterator();
        while (it.hasNext()) {
            OrderResultedAssociationHistEntity association = (OrderResultedAssociationHistEntity) it.next();
            if (association != null) {
				ret.add(toAssociationType(association));
			}
        }

        return ret;
    }

    /**
     * Create a set of AssociationType objects from an set OrderResultedAssociationHistEntity objects.
     *
     * @param associations
     * @return Set<AssociationType>
     */
    @SuppressWarnings("rawtypes")
    public static Set<Long> toOrderResultedIds2(Set<OrderResultedAssociationHistEntity> associations) {

        Set<Long> ret = new HashSet<Long>();

        if (associations == null) {
			return ret;
		}

        Iterator it = associations.iterator();
        while (it.hasNext()) {
            OrderResultedAssociationHistEntity association = (OrderResultedAssociationHistEntity) it.next();
            if (association != null) {
				ret.add(association.getToIdentifier());
			}
        }

        return ret;
    }

    /**
     * @param uc                     UserContext, Required
     * @param session                Session
     * @param rootCriteria           Criteria, Required, main instance's criteria
     * @param setMode                String, Optional, default = ALL (ALL, ANY, ONLY)
     * @param strRootIdProperty      String, Required, the id property of parent.
     * @param associations           Set&lt;Association>, optional
     * @param associationEntityClazz Class&lt;T> extends AssociationEntity, Required
     */
    public static <T extends CaseActivityStaticAssociationEntity> Criteria getCriteriaWithStaticAssociations(UserContext uc, Session session, Criteria rootCriteria,
            String setMode, String strRootIdProperty, Set<Long> associations, Class<T> associationEntityClazz, String associationPath) {

        if (BeanHelper.isEmpty(associations)) {
			return rootCriteria;
		}

        if (BeanHelper.isEmpty(setMode) || "ALL".equalsIgnoreCase(setMode)) {
            for (Long assoc : associations) {
                DetachedCriteria associationCriteria = DetachedCriteria.forClass(associationEntityClazz, "association");
                //associationCriteria.add(Restrictions.eq("association.toClass", assoc.getToClass()));
                associationCriteria.add(Restrictions.eq("association.toIdentifier", assoc));
                associationCriteria.setProjection(Projections.property("association.caseActivity.caseActivityId"));
                rootCriteria.add(Subqueries.propertyIn(strRootIdProperty, associationCriteria));
            }
        } else if ("ANY".equalsIgnoreCase(setMode)) {
            rootCriteria.createAlias(associationPath, "association");
            Disjunction dis = Restrictions.disjunction();
            for (Long assoc : associations) {
                Criterion criterion = Restrictions.eq("association.toIdentifier", assoc);
                dis.add(criterion);
            }
            rootCriteria.add(dis);

        } else if ("ONLY".equalsIgnoreCase(setMode)) {
            DetachedCriteria associationCriteria = DetachedCriteria.forClass(associationEntityClazz, "association");
            Disjunction dis = Restrictions.disjunction();
            for (Long assoc : associations) {
                Criterion criterion = Restrictions.eq("association.toIdentifier", assoc);
                dis.add(criterion);
            }
            associationCriteria.setProjection(Projections.property("association.caseActivity.caseActivityId"));
            associationCriteria.add(dis);

            @SuppressWarnings("unchecked") Set<Long> fromIDs = new HashSet<Long>(associationCriteria.getExecutableCriteria(session).list());
            Set<Long> fromIdentifiers = new HashSet<Long>();
            int assocSize = associations.size();
            for (Long fromId : fromIDs) {
                Long count = getStaticAssociationCount(uc, session, fromId, associationEntityClazz);
                if (count != null && count.intValue() == assocSize) {
                    fromIdentifiers.add(filterStaticFromIdentifier(uc, session, fromId, associations, associationEntityClazz));
                }
            }
            if (!BeanHelper.isEmpty(fromIdentifiers)) {
				rootCriteria.add(Property.forName(strRootIdProperty).in(fromIdentifiers));
			} else {
                Set<Long> ngIds = new HashSet<Long>();
                ngIds.add(-1L);
                rootCriteria.add(Property.forName(strRootIdProperty).in(ngIds));
            }
        }

        return rootCriteria;
    }

    /**
     * @param uc                     UserContext, Required
     * @param session                Session
     * @param rootCriteria           Criteria, Required, main instance's criteria
     * @param setMode                String, Optional, default = ALL (ALL, ANY, ONLY)
     * @param strRootIdProperty      String, Required, the id property of parent.
     * @param associations           Set&lt;Association>, optional
     * @param associationEntityClazz Class&lt;T> extends AssociationEntity, Required
     */
    public static <T extends CaseActivityStaticAssocHistEntity> Criteria getCriteriaWithStaticAssocHist(UserContext uc, Session session, Criteria rootCriteria,
            String setMode, String strRootIdProperty, Set<Long> associations, Class<T> associationEntityClazz, String associationPath) {

        if (BeanHelper.isEmpty(associations)) {
			return rootCriteria;
		}

        if (BeanHelper.isEmpty(setMode) || "ALL".equalsIgnoreCase(setMode)) {
            for (Long assoc : associations) {
                DetachedCriteria associationCriteria = DetachedCriteria.forClass(associationEntityClazz, "association");
                //associationCriteria.add(Restrictions.eq("association.toClass", assoc.getToClass()));
                associationCriteria.add(Restrictions.eq("association.toIdentifier", assoc));
                associationCriteria.setProjection(Projections.property("association.fromIdentifier"));
                rootCriteria.add(Subqueries.propertyIn(strRootIdProperty, associationCriteria));
            }
        } else if ("ANY".equalsIgnoreCase(setMode)) {
            rootCriteria.createAlias(associationPath, "association");
            Disjunction dis = Restrictions.disjunction();
            for (Long assoc : associations) {
                Criterion criterion = Restrictions.eq("association.toIdentifier", assoc);
                dis.add(criterion);
            }
            rootCriteria.add(dis);

        } else if ("ONLY".equalsIgnoreCase(setMode)) {
            DetachedCriteria associationCriteria = DetachedCriteria.forClass(associationEntityClazz, "association");
            Disjunction dis = Restrictions.disjunction();
            for (Long assoc : associations) {
                Criterion criterion = Restrictions.eq("association.toIdentifier", assoc);
                dis.add(criterion);
            }
            associationCriteria.setProjection(Projections.property("association.fromIdentifier"));
            associationCriteria.add(dis);

            @SuppressWarnings("unchecked") Set<Long> fromIDs = new HashSet<Long>(associationCriteria.getExecutableCriteria(session).list());
            Set<Long> fromIdentifiers = new HashSet<Long>();
            int assocSize = associations.size();
            for (Long fromId : fromIDs) {
                Integer count = getStaticAssocHistCount(uc, session, fromId, associationEntityClazz);
                if (count != null && count.equals(assocSize)) {
                    fromIdentifiers.add(filterStaticHistFromIdentifier(uc, session, fromId, associations, associationEntityClazz));
                }
            }
            if (!BeanHelper.isEmpty(fromIdentifiers)) {
				rootCriteria.add(Property.forName(strRootIdProperty).in(fromIdentifiers));
			} else {
                Set<Long> ngIds = new HashSet<Long>();
                ngIds.add(-1L);
                rootCriteria.add(Property.forName(strRootIdProperty).in(ngIds));
            }
        }

        return rootCriteria;
    }

    public static <T extends CaseActivityStaticAssociationEntity> Long getStaticAssociationCount(UserContext uc, Session session, Long fromIdentifier,
            Class<T> associationEntityClazz) {

        Criteria c = session.createCriteria(associationEntityClazz);
        c.add(Restrictions.eq("caseActivity.caseActivityId", fromIdentifier));
        c.setProjection(Projections.countDistinct("associationId"));

        return (Long) c.uniqueResult();
    }

    public static <T extends CaseActivityStaticAssocHistEntity> Integer getStaticAssocHistCount(UserContext uc, Session session, Long fromIdentifier,
            Class<T> associationEntityClazz) {

        Criteria c = session.createCriteria(associationEntityClazz);
        c.add(Restrictions.eq("fromIdentifier", fromIdentifier));
        c.setProjection(Projections.countDistinct("associationId"));

        return (Integer) c.uniqueResult();
    }

    public static <T extends CaseActivityStaticAssociationEntity> Long filterStaticFromIdentifier(UserContext uc, Session session, Long fromIdentifier,
            Set<Long> associations, Class<T> associationEntityClazz) {

        for (Long assoc : associations) {
            Criteria c = session.createCriteria(associationEntityClazz);
            c.add(Restrictions.eq("caseActivity.caseActivityId", fromIdentifier));
            //c.add(Restrictions.eq("toClass", assoc.getToClass()));
            c.add(Restrictions.eq("toIdentifier", assoc));
            c.setProjection(Projections.rowCount());
            Long count = (Long) c.uniqueResult();
            if (count == null || count.equals(0L)) {
				return null;
			}
        }

        return fromIdentifier;
    }

    public static <T extends CaseActivityStaticAssocHistEntity> Long filterStaticHistFromIdentifier(UserContext uc, Session session, Long fromIdentifier,
            Set<Long> associations, Class<T> associationEntityClazz) {

        for (Long assoc : associations) {
            Criteria c = session.createCriteria(associationEntityClazz);
            c.add(Restrictions.eq("fromIdentifier", fromIdentifier));
            //c.add(Restrictions.eq("toClass", assoc.getToClass()));
            c.add(Restrictions.eq("toIdentifier", assoc));
            c.setProjection(Projections.rowCount());
            Integer count = (Integer) c.uniqueResult();
            if (count == null || count == 0) {
				return null;
			}
        }

        return fromIdentifier;
    }

}
