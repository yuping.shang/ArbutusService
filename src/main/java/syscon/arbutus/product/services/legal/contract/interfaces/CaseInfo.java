package syscon.arbutus.product.services.legal.contract.interfaces;

import java.util.Date;
import java.util.List;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.exception.DataExistException;
import syscon.arbutus.product.services.legal.contract.dto.caseinformation.*;

/**
 * Business contract interface for case information related actions.
 * <p><b>Purpose</b>
 * The Case Information captures information relevant to a proceeding initiated
 * by a government agency against an offender, where a case uniquely identifies
 * a formal accusation of an offence under law. A case also serves
 * to be a logical grouping of criminal charges made against an offender.
 * The Case Information is the central point which connects offenders
 * (SUPERVISION service) with activities/events (CASE-ACTIVITY service),
 * charges (CHARGE service) and documents (DOCUMENT service) together.
 * </p>
 * <p><b>Scope</b>
 * The scope of the Case Information is limited to the creation and maintenance of the case information, as well as the maintenance of the reference data used by the service. This includes the following:
 * <ul>
 * <li>Search, create, update or delete a case information;</li>
 * <li>Maintain case types;</li>
 * <li>Maintain case statuses;</li>
 * <li>Maintain reasons for case status changes.</li>
 * </ul>
 * </p>
 * <p>Class diagram of Case Info module:
 * <br/><img src="doc-files/CaseManagement-CaseInfo-classdiagram.JPG" />
 * </p>
 *
 * @author byu
 * @version 1.0 (based on SDD_CaseInformation1.0)
 */
public interface CaseInfo {
    /**
     * Paging search for existing cases information
     *
     * @param uc            {@link UserContext}
     * @param search        {@link CaseInfoSearchType} - search criteria instance.
     * @param searchHistory Boolean - optional, if true, the complete history of Case records are searched (current and historic records), otherwise only the latest Case records are searched
     * @param startIndex    Long – Optional if null search returns at the start index
     * @param resultSize    Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder   String – Optional if null search returns a default order
     * @return a list of case info instances.
     * Returns null if no case info can be found.
     * Throws exception if there is an error during search.
     */
    public CaseInfosReturnType searchCaseInfo(UserContext uc, CaseInfoSearchType search, Boolean searchHistory, Long startIndex, Long resultSize, String resultOrder);

    /**
     * Search for existing cases information
     *
     * @param uc            {@link UserContext}
     * @param subsetSearch  Set<Long> - a set of CaseInfo id, Optional, if null search will occur across all instances.
     * @param search        {@link CaseInfoSearchType} - search criteria instance.
     * @param searchHistory Boolean - optional, if true, the complete history of Case records are searched (current and historic records), otherwise only the latest Case records are searched
     * @return a list of case info instances.
     * Returns null if no case info can be found.
     * Throws exception if there is an error during search.
     */
    @Deprecated
    public List<CaseInfoType> searchCaseInfo(UserContext uc, Set<Long> subsetSearch, CaseInfoSearchType search, Boolean searchHistory);

    /**
     * Create a new case information record
     *
     * @param uc              {@link UserContext}
     * @param caseInfo        {@link CaseInfoType} - the case info instance
     * @param duplicatedCheck Boolean - optional, if true, check for duplicates based service instance uniqueness, otherwise create new record.
     * @return the case info instance.
     * Throws exception if instance cannot be created.
     */
    public CaseInfoType createCaseInfo(UserContext uc, CaseInfoType caseInfo, Boolean duplicatedCheck);

    /**
     * Update Case information including case identifiers
     *
     * @param uc              {@link UserContext}
     * @param caseInfo        {@link CaseInfoType} - the case info instance
     * @param duplicatedCheck Boolean - optional, if true, check for duplicates based service instance uniqueness, otherwise update the record.
     * @return the case info instance.
     * Throws exception if instance cannot be updated.
     */
    public CaseInfoType updateCaseInfo(UserContext uc, CaseInfoType caseInfo, Boolean duplicatedCheck);

    /**
     * Retrieve a single case information based on a unique identifier
     *
     * @param uc         {@link UserContext}
     * @param caseInfoId Long - the case information unique identifier.
     * @return the case info instance.
     * Throws exception if incorrect id is provided as parameter or there is an error.
     */
    public CaseInfoType getCaseInfo(UserContext uc, Long caseInfoId);

    /**
     * Retrieve all cases information for a supervision period
     *
     * @param uc            {@link UserContext}
     * @param supervisionId Long - the offender supervision identifier.
     * @param isActive      Boolean - if true, return all active cases information for the supervision period.
     *                      if false, return all inactive cases information for the supervision period.
     *                      if null, return all active and inactive cases information for the supervision period.
     * @return the list of case info instances.
     * Throws exception if there is an error.
     * Return null when no instances could be found.
     */
    public List<CaseInfoType> retrieveCaseInfos(UserContext uc, Long supervisionId, Boolean isActive);

    /**
     * Retrieve a historical record for a case information based on a date range
     *
     * @param uc              {@link UserContext}
     * @param caseInfoId      Long - the case information unique identifier.
     * @param historyDateFrom Date - history date from (optional)
     * @param historyDateTo   Date - history date to (optional)
     * @return the list of case info instances.
     * Throws exception  if there is an error.
     * Return null when no instances could be found.
     */
    public List<CaseInfoType> retrieveCaseInfoHistory(UserContext uc, Long caseInfoId, Date historyDateFrom, Date historyDateTo);

    /**
     * Retrieve the case status change historical records for a case based on a date range
     *
     * @param uc              {@link UserContext}
     * @param caseInfoId      Long - the case information unique identifier.
     * @param historyDateFrom Date - history date from (optional)
     * @param historyDateTo   Date - history date to (optional)
     * @return the list of CaseStatusChangeType instances.
     * Throws exception  if there is an error.
     * Return empty list when no instances could be found.
     */
    public List<CaseStatusChangeType> retrieveCaseStatusChangeHistory(UserContext uc, Long caseInfoId, Date historyDateFrom, Date historyDateTo);

    /**
     * Delete a case information based on a unique identifier
     * <p>List of possible return codes: 1</p>
     *
     * @param uc         {@link UserContext}
     * @param caseInfoId Long - the case information unique identifier.
     * @return Return Code - Long.
     * Return success code if a case information instance be deleted.
     * Throws exception if a case information not be deleted.
     */
    public Long deleteCaseInfo(UserContext uc, Long caseInfoId) throws DataExistException;

    /**
     * Create case identifier configuration business rule
     *
     * @param uc                   {@link UserContext}
     * @param caseIdentifierConfig {@link CaseIdentifierConfigType} - the case identifier configuration type
     * @return the case identifier configuration instance.
     * Throws exception if instance cannot be created.
     */
    public CaseIdentifierConfigType createCaseIdentifierConfig(UserContext uc, CaseIdentifierConfigType caseIdentifierConfig);

    /**
     * Update case identifier configuration business rule
     * <p>List of possible return codes: 1, 2001, 2002, 1003</p>
     *
     * @param uc                   {@link UserContext}
     * @param caseIdentifierConfig {@link CaseIdentifierConfigType} - the case identifier configuration type
     * @return the case identifier configuration instance.
     * Throws exception if instance cannot be updated.
     */
    public CaseIdentifierConfigType updateCaseIdentifierConfig(UserContext uc, CaseIdentifierConfigType caseIdentifierConfig);

    /**
     * Search for existing case identifier configuration information
     *
     * @param uc                         {@link UserContext}
     * @param caseIdentifierConfigSearch {@link CaseIdentifierConfigSearchType} - the case identifier configuration search type
     * @return the list of case identifier configuration instances.
     * Throws exception  if there is an error.
     * Return null when no instances could be found.
     */
    public List<CaseIdentifierConfigType> searchCaseIdentifierConfig(UserContext uc, CaseIdentifierConfigSearchType caseIdentifierConfigSearch);

    /**
     * Update Case Data Security Status / Flag
     *
     * @param uc        - Required.
     * @param chargeId  - Charge Id, - required
     * @param secStatus - DataFlag.
     * @param Long      - personId. Person Id of person id,
     * @param comments  - Comments to the security flag.
     * @return Long 1/0 as True/False
     * @throws exception if the retrieving encounters error.
     */
    public Long updateCaseDataSecurityStatus(UserContext uc, Long caseId, Long secStatus, String comments, Long personId, String parentEntityType, Long parentId,
            String memento, Boolean onlyRecord);

}
