package syscon.arbutus.product.services.legal.realization.persistence.condition;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the "0 to many" static module association history entity of Condition Instance.
 *
 * @DbComment LEG_CNModuleAsscHist 'The static module association history table of Condition Instance'
 * @DbComment LEG_CNModuleAsscHist.AssociationId 'Unique id of the association history record'
 * @DbComment LEG_CNModuleAsscHist.FromIdentifier 'The Condition instance Id'
 * @DbComment LEG_CNModuleAsscHist.ToClass 'The association module that is referenced'
 * @DbComment LEG_CNModuleAsscHist.ToIdentifier 'The unique id of the Association To Object'
 * @DbComment LEG_CNModuleAsscHist.conditionHistoryId 'The Id of condition history record'
 * @DbComment LEG_CNModuleAsscHist.CreateUserId 'User ID who created the object'
 * @DbComment LEG_CNModuleAsscHist.CreateDateTime 'Date and time when the object was created'
 * @DbComment LEG_CNModuleAsscHist.InvocationContext 'Invocation context when the create/update action called'
 * @DbComment LEG_CNModuleAsscHist.ModifyUserId 'User ID who last updated the object'
 * @DbComment LEG_CNModuleAsscHist.ModifyDateTime 'Date and time when the object was last updated'
 */
//@Audited
@Entity
@Table(name = "LEG_CNModuleAsscHist")
public class ConditionModuleAssociationHistEntity implements Serializable {

    private static final long serialVersionUID = -5300556112209578635L;

    @Id
    @Column(name = "AssociationId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CNModuleAsscHist_Id")
    @SequenceGenerator(name = "SEQ_LEG_CNModuleAsscHist_Id", sequenceName = "SEQ_LEG_CNModuleAsscHist_Id", allocationSize = 1)
    private Long associationId;

    @Column(name = "FromIdentifier", nullable = false)
    private Long fromIdentifier;

    @Column(name = "ToClass", nullable = false, length = 64)
    private String toClass;

    @Column(name = "ToIdentifier", nullable = false)
    private Long toIdentifier;

    @NotAudited
    @Embedded
    private StampEntity stamp;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "conditionHistoryId", nullable = false)
    private ConditionHistoryEntity conditionHistory;

    /**
     * Default constructor
     */
    public ConditionModuleAssociationHistEntity() {

    }

    /**
     * Constructor
     *
     * @param associationId
     * @param fromIdentifier
     * @param toClass
     * @param toIdentifier
     * @param stamp
     * @param conditionHistory
     */
    public ConditionModuleAssociationHistEntity(Long associationId, Long fromIdentifier, String toClass, Long toIdentifier, StampEntity stamp,
            ConditionHistoryEntity conditionHistory) {
        super();
        this.associationId = associationId;
        this.fromIdentifier = fromIdentifier;
        this.toClass = toClass;
        this.toIdentifier = toIdentifier;
        this.stamp = stamp;
        this.setConditionHistory(conditionHistory);
    }

    /**
     * Gets the value of the associationId property
     *
     * @return Long
     */
    public Long getAssociationId() {
        return associationId;
    }

    /**
     * Sets the value of the associationId property
     *
     * @param associationId Long
     */
    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    /**
     * Gets the value of the fromIdentifier property
     *
     * @return Long
     */
    public Long getFromIdentifier() {
        return fromIdentifier;
    }

    /**
     * Sets the value of the fromIdentifier property
     *
     * @param fromIdentifier Long
     */
    public void setFromIdentifier(Long fromIdentifier) {
        this.fromIdentifier = fromIdentifier;
    }

    /**
     * Gets the value of the toClass property
     *
     * @return String
     */
    public String getToClass() {
        return toClass;
    }

    /**
     * Sets the value of the toClass property
     *
     * @param toClass String
     */
    public void setToClass(String toClass) {
        this.toClass = toClass;
    }

    /**
     * Gets the value of the toIdentifier property
     *
     * @return Long
     */
    public Long getToIdentifier() {
        return toIdentifier;
    }

    /**
     * Sets the value of the toIdentifier property
     *
     * @param toIdentifier Long
     */
    public void setToIdentifier(Long toIdentifier) {
        this.toIdentifier = toIdentifier;
    }

    /**
     * Gets the value of the stamp property
     *
     * @return StampEntity
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * Sets the value of the stamp property
     *
     * @param stamp StampEntity
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    public ConditionHistoryEntity getConditionHistory() {
        return conditionHistory;
    }

    public void setConditionHistory(ConditionHistoryEntity conditionHistory) {
        this.conditionHistory = conditionHistory;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((associationId == null) ? 0 : associationId.hashCode());
        result = prime * result + ((fromIdentifier == null) ? 0 : fromIdentifier.hashCode());
        result = prime * result + ((toClass == null) ? 0 : toClass.hashCode());
        result = prime * result + ((toIdentifier == null) ? 0 : toIdentifier.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ConditionModuleAssociationHistEntity other = (ConditionModuleAssociationHistEntity) obj;
        if (associationId == null) {
            if (other.associationId != null) {
				return false;
			}
        } else if (!associationId.equals(other.associationId)) {
			return false;
		}
        if (fromIdentifier == null) {
            if (other.fromIdentifier != null) {
				return false;
			}
        } else if (!fromIdentifier.equals(other.fromIdentifier)) {
			return false;
		}
        if (toClass == null) {
            if (other.toClass != null) {
				return false;
			}
        } else if (!toClass.equals(other.toClass)) {
			return false;
		}
        if (toIdentifier == null) {
            if (other.toIdentifier != null) {
				return false;
			}
        } else if (!toIdentifier.equals(other.toIdentifier)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "ConditionModuleAssociationHistEntity [associationId=" + associationId + ", fromIdentifier=" + fromIdentifier + ", toClass=" + toClass + ", toIdentifier="
                + toIdentifier + ", stamp=" + stamp + ", conditionHistory=" + conditionHistory + "]";
    }

}
