package syscon.arbutus.product.services.legal.contract.dto.caseinformation;

import java.io.Serializable;

import syscon.arbutus.product.services.legal.contract.dto.ValidationUtil;

/**
 * The representation of the case identifier search type
 */
public class CaseIdentifierSearchType implements Serializable {

    private static final long serialVersionUID = 8250736627739932890L;

    private String identifierType;
    private String identifierValue;
    private Boolean autoGeneration;
    private Long organizationId;
    private Long facilityId;
    private String comment;

    /**
     * Default constructor
     */
    public CaseIdentifierSearchType() {
    }

    /**
     * Constructor
     * At least one of the search criteria must be specified.
     *
     * @param identifierType  String - A kind of identifier or number that can be associated to a criminal case e.g. Court Information Number, Indictment Number
     * @param identifierValue String - The number or value of the case identifier e.g. 1234567S (Indictment Number)
     * @param autoGeneration  Boolean - This is set to false by default and cannot be set to true for an update. If set to true the IdentifierValue is ignored.
     * @param organizationId  Long - the organization Id, static reference to Organization service.
     * @param facilityId      Long - the facility Id, static reference to Facility service.
     * @param comment         String - the comments associated to the case identifier.
     */
    public CaseIdentifierSearchType(String identifierType, String identifierValue, Boolean autoGeneration, Long organizationId, Long facilityId, String comment) {
        this.identifierType = identifierType;
        this.identifierValue = identifierValue;
        this.autoGeneration = autoGeneration;
        this.organizationId = organizationId;
        this.facilityId = facilityId;
        this.comment = comment;
    }

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set, false otherwise.
     */
    @Deprecated
    public Boolean isWellFormed() {

        if (ValidationUtil.isEmpty(identifierType) && ValidationUtil.isEmpty(identifierValue) && ValidationUtil.isEmpty(autoGeneration) && ValidationUtil.isEmpty(
                organizationId) && ValidationUtil.isEmpty(facilityId) && ValidationUtil.isEmpty(comment)) {
			return false;
		}

        if (!ValidationUtil.isEmpty(identifierValue) && identifierValue.matches("^\\*+$") || !ValidationUtil.isEmpty(comment) && comment.matches("^\\*+$")) {
            return false;
        }

        return true;
    }

    /**
     * Verify if properties are set properly.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set and all values are valid, false otherwise.
     */
    @Deprecated
    public Boolean isValid() {

        if (!isWellFormed()) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the identifierType property. It is a code value of CaseIdentifierType set.
     *
     * @return String - A kind of identifier or number that can be associated to a criminal case e.g. Court Information Number, Indictment Number
     */
    public String getIdentifierType() {
        return identifierType;
    }

    /**
     * Sets the value of the identifierType property. It is a code value of CaseIdentifierType set.
     *
     * @param identifierType String - A kind of identifier or number that can be associated to a criminal case e.g. Court Information Number, Indictment Number
     */
    public void setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
    }

    /**
     * Gets the value of the identifierValue property.
     *
     * @return String - The number or value of the case identifier e.g. 1234567S (Indictment Number)
     */
    public String getIdentifierValue() {
        return identifierValue;
    }

    /**
     * Sets the value of the identifierValue property.
     *
     * @param identifierValue String - The number or value of the case identifier e.g. 1234567S (Indictment Number)
     */
    public void setIdentifierValue(String identifierValue) {
        this.identifierValue = identifierValue;
    }

    /**
     * Gets the value of the autoGeneration property.
     *
     * @return Boolean - This is set to false by default and cannot be set to true for an update. If set to true the IdentifierValue is ignored.
     */
    public Boolean isAutoGeneration() {
        return autoGeneration;
    }

    /**
     * Sets the value of the autoGeneration property.
     *
     * @param autoGeneration Boolean - This is set to false by default and cannot be set to true for an update. If set to true the IdentifierValue is ignored.
     */
    public void setAutoGeneration(Boolean autoGeneration) {
        this.autoGeneration = autoGeneration;
    }

    /**
     * Gets the value of the organizationId property.
     *
     * @return Long - the organization Id, static reference to Organization service.
     */
    public Long getOrganizationId() {
        return organizationId;
    }

    /**
     * Sets the value of the organizationId property.
     *
     * @param organizationId Long - the organization Id, static reference to Organization service.
     */
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * Gets the value of the facilityId property.
     *
     * @return Long - the facility Id, static reference to Facility service.
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * Sets the value of the facilityId property.
     *
     * @param facilityId Long - the facility Id, static reference to Facility service.
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * Gets the value of the identifierComment property.
     *
     * @return String - the comments associated to the case identifier
     */
    public String getIdentifierComment() {
        return comment;
    }

    /**
     * Sets the value of the identifierComment property.
     *
     * @param identifierComment String - the comments associated to the case identifier
     */
    public void setIdentifierComment(String identifierComment) {
        this.comment = identifierComment;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseIdentifierSearchType [identifierType=" + identifierType + ", identifierValue=" + identifierValue + ", autoGeneration=" + autoGeneration
                + ", organizationId=" + organizationId + ", facilityId=" + facilityId + ", comment=" + comment + "]";
    }

}
