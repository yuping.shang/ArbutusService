package syscon.arbutus.product.services.legal.contract.dto.charge;

import java.io.Serializable;

import syscon.arbutus.product.services.legal.contract.dto.ValidationUtil;

/**
 * The representation of the charge appeal type
 */
public class ChargeAppealSearchType implements Serializable {

    private static final long serialVersionUID = -6613962201618576258L;

    private String appealStatus;
    private String appealComment;

    /**
     * Default constructor
     */
    public ChargeAppealSearchType() {
    }

    /**
     * Constructor
     * At least one of the search criteria must be specified.
     *
     * @param appealStatus  String - A status of an appeal
     * @param appealComment String - General comments on the outcome of the appeal
     */
    public ChargeAppealSearchType(String appealStatus, String appealComment) {
        this.appealStatus = appealStatus;
        this.appealComment = appealComment;
    }

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set, false otherwise.
     */
    @Deprecated
    public Boolean isWellFormed() {

        if (ValidationUtil.isEmpty(appealStatus) && ValidationUtil.isEmpty(appealComment)) {
			return false;
		}

        return true;
    }

    /**
     * Verify if properties are set properly.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set and all values are valid, false otherwise.
     */
    @Deprecated
    public Boolean isValid() {

        if (!isWellFormed()) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the appealStatus property. It is a reference code value of AppealStatus set.
     *
     * @return String - A status of an appeal
     */
    public String getAppealStatus() {
        return appealStatus;
    }

    /**
     * Sets the value of the appealStatus property. It is a reference code value of AppealStatus set.
     *
     * @param appealStatus String - A status of an appeal
     */
    public void setAppealStatus(String appealStatus) {
        this.appealStatus = appealStatus;
    }

    /**
     * Gets the value of the appealComment property.
     *
     * @return String - General comments on the outcome of the appeal
     */
    public String getAppealComment() {
        return appealComment;
    }

    /**
     * Sets the value of the appealComment property.
     *
     * @param appealComment String - General comments on the outcome of the appeal
     */
    public void setAppealComment(String appealComment) {
        this.appealComment = appealComment;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeAppealSearchType [appealStatus=" + appealStatus + ", appealComment=" + appealComment + "]";
    }

}
