package syscon.arbutus.product.services.legal.contract.ejb;

public class Constants {

    public final static String DTO_VALIDATION = "DTO validation failure";
    public final static String ASSOCIATION_VALIDATION = "Instance associationToClass validation failure";
    public final static String DYNAMIC_ELEMENT_VALIDATION = "Invalid Dynamic Element";

    public final static String OFFENCE_START_DATE = "OffenceStartDate";

}
