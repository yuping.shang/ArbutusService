package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * BailAmountEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdBailAmnt 'The Bail Amount table for Order Sentence module of Legal service'
 * @since December 21, 2012
 */
@Audited
@Entity
@Table(name = "LEG_OrdBailAmnt")
public class BailAmountEntity implements Serializable, Amountable {

    private static final long serialVersionUID = -737931375118542731L;

    /**
     * @DbComment bailAmntId 'Unique identification of a bail amount instance'
     */
    @Id
    @Column(name = "bailAmntId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDBAILAMNT_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDBAILAMNT_ID", sequenceName = "SEQ_LEG_ORDBAILAMNT_ID", allocationSize = 1)
    private Long bailAmountId;

    /**
     * @DbComment bailType 'The type of bail i.e., cash, surety, property etc.,'
     */
    @Column(name = "bailType", nullable = false)
    @MetaCode(set = MetaSet.BAIL_TYPE)
    private String bailType;

    /**
     * @DbComment bailAmount 'The amount of bail set for each type'
     */
    @Column(name = "bailAmount", nullable = false, precision = 12, scale = 2)
    private BigDecimal bailAmount;

    @ForeignKey(name = "Fk_LEG_OrdBailAmnt1")
    @OneToMany(mappedBy = "bailAmount", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<OrderChargeAssocEntity> chargeAssociations;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment orderId 'Unique identification of a bail instance, foreign key to LEG_ORDSTNBAIL table'
     */
    @ForeignKey(name = "Fk_LEG_OrdBailAmnt2")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orderId", nullable = false)
    private BailEntity bail;

    public BailAmountEntity() {
        super();
    }

    public BailAmountEntity(Long bailAmountId, String bailType, BigDecimal bailAmount, Set<OrderChargeAssocEntity> chargeAssociations, StampEntity stamp) {
        super();
        this.bailAmountId = bailAmountId;
        this.bailType = bailType;
        this.bailAmount = bailAmount;
        this.chargeAssociations = chargeAssociations;
        this.stamp = stamp;
    }

    /**
     * @return the bailAmountId
     */
    public Long getBailAmountId() {
        return bailAmountId;
    }

    /**
     * @param bailAmountId the bailAmountId to set
     */
    public void setBailAmountId(Long bailAmountId) {
        this.bailAmountId = bailAmountId;
    }

    /**
     * @return the bailType
     */
    public String getBailType() {
        return bailType;
    }

    /**
     * @param bailType the bailType to set
     */
    public void setBailType(String bailType) {
        this.bailType = bailType;
    }

    /**
     * @return the bailAmount
     */
    public BigDecimal getBailAmount() {
        return bailAmount;
    }

    /**
     * @param bailAmount the bailAmount to set
     */
    public void setBailAmount(BigDecimal bailAmount) {
        this.bailAmount = bailAmount;
    }

    /**
     * @return the chargeAssociations
     */
    public Set<OrderChargeAssocEntity> getChargeAssociations() {
        if (chargeAssociations == null) {
			chargeAssociations = new HashSet<OrderChargeAssocEntity>();
		}
        return chargeAssociations;
    }

    /**
     * @param chargeAssociations the chargeAssociations to set
     */
    public void setChargeAssociations(Set<OrderChargeAssocEntity> chargeAssociations) {
        if (chargeAssociations != null) {
            for (OrderChargeAssocEntity chg : chargeAssociations) {
                chg.setBailAmount(this);
            }
            this.chargeAssociations = chargeAssociations;
        } else {
			this.chargeAssociations = new HashSet<OrderChargeAssocEntity>();
		}

    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the bail
     */
    public BailEntity getBail() {
        return bail;
    }

    /**
     * @param bail the bail to set
     */
    public void setBail(BailEntity bail) {
        this.bail = bail;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bailAmountId == null) ? 0 : bailAmountId.hashCode());
        result = prime * result + ((bailType == null) ? 0 : bailType.hashCode());
        result = prime * result + ((chargeAssociations == null) ? 0 : chargeAssociations.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        BailAmountEntity other = (BailAmountEntity) obj;
        if (bailAmountId == null) {
            if (other.bailAmountId != null) {
				return false;
			}
        } else if (!bailAmountId.equals(other.bailAmountId)) {
			return false;
		}
        if (bailType == null) {
            if (other.bailType != null) {
				return false;
			}
        } else if (!bailType.equals(other.bailType)) {
			return false;
		}
        if (chargeAssociations == null) {
            if (other.chargeAssociations != null) {
				return false;
			}
        } else if (!chargeAssociations.equals(other.chargeAssociations)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "BailAmountEntity [bailAmountId=" + bailAmountId + ", bailType=" + bailType + ", bailAmount=" + bailAmount + ", chargeAssociations=" + chargeAssociations
                + "]";
    }

}
