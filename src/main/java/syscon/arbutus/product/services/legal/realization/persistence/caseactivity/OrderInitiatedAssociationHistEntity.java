package syscon.arbutus.product.services.legal.realization.persistence.caseactivity;

import javax.persistence.*;

import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * OrderInitiatedAssociationHistEntity for Case Activity Service
 *
 * @author lhan
 * @version 1.0
 */
//@Audited
@Entity
@DiscriminatorValue("OrderInitAssocHist")
public class OrderInitiatedAssociationHistEntity extends CaseActivityStaticAssocHistEntity {

    private static final long serialVersionUID = -5922296208120814862L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CaseActivityHistId", nullable = false)
    private CourtActivityHistEntity caseActivity;

    /**
     * Constructor
     */
    public OrderInitiatedAssociationHistEntity() {
        super();
    }

    /**
     * Constructor.
     *
     * @param associationId
     * @param fromIdentifier
     * @param toIdentifier
     * @param stamp
     * @param courtActivity
     */
    public OrderInitiatedAssociationHistEntity(Long associationId, Long fromIdentifier, Long toIdentifier, StampEntity stamp, CourtActivityHistEntity caseActivity) {
        super(associationId, fromIdentifier, toIdentifier, stamp);
        this.caseActivity = caseActivity;
    }

    /**
     * @param courtActivityHist the courtActivityHist to set
     */
    public void setCourtActivityHist(CourtActivityHistEntity caseActivity) {
        this.caseActivity = caseActivity;
    }

    /**
     * @return the courtActivityHist
     */
    public CourtActivityHistEntity getCourtActivity() {
        return caseActivity;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
	 */
    @Override
    public String toString() {
        return "OrderInitiatedAssociationHistEntity [getAssociationId()=" + getAssociationId() + ", getFromIdentifier()=" + getFromIdentifier() + ", getToIdentifier()="
                + getToIdentifier() + ", getStamp()=" + getStamp() + "]";
    }

}