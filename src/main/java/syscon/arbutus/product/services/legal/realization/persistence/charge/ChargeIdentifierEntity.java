package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * @DbComment LEG_CHGIdentifier 'The charge information identifier table'
 */
@Audited
@Entity
@Table(name = "LEG_CHGIdentifier")
public class ChargeIdentifierEntity implements Serializable {

    private static final long serialVersionUID = -7628637103055369939L;

    /**
     * @DbComment IdentifierId 'Unique Id for each charge identifier record'
     */
    @Id
    @Column(name = "IdentifierId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGIdentifier_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGIdentifier_Id", sequenceName = "SEQ_LEG_CHGIdentifier_Id", allocationSize = 1)
    private Long identifierId;

    /**
     * @DbComment IdentifierType 'A set of numbers/identifiers assigned by an arresting agency, prosecuting attorney or court. E.g. CJIS code'
     */
    @MetaCode(set = MetaSet.CHARGE_IDENTIFIER)
    @Column(name = "IdentifierType", length = 64, nullable = false)
    private String identifierType;

    /**
     * @DbComment IdentifierValue 'The number or value of the charge identifier'
     */
    @Column(name = "IdentifierValue", length = 64, nullable = false)
    private String identifierValue;

    /**
     * @DbComment IdentifierFormat 'The format for the Identifier. Will be used by the user interface.'
     */
    @MetaCode(set = MetaSet.CHARGE_IDENTIFIER_FORMAT)
    @Column(name = "IdentifierFormat", length = 64, nullable = false)
    private String identifierFormat;

    /**
     * @DbComment OrganizationId 'Static reference to an Organization which issued the charge identifier'
     */
    @Column(name = "OrganizationId", nullable = true)
    private Long organizationId;

    /**
     * @DbComment FacilityId 'Static reference to a Facility which issued the charge identifier'
     */
    @Column(name = "FacilityId", nullable = true)
    private Long facilityId;

    /**
     * @DbComment IdentifierComment 'Comments associated to the charge identifier'
     */
    @Column(name = "IdentifierComment", length = 1024, nullable = false)
    private String identifierComment;

    /**
     * @DbComment ChargeId 'Unique id of a charge instance, foreign key to LEG_CHGCharge table'
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ChargeId", nullable = false)
    private ChargeEntity charge;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     *
     */
    public ChargeIdentifierEntity() {
    }

    /**
     * @param identifierId
     * @param identifierType
     * @param identifierValue
     * @param identifierFormat
     * @param organizationId
     * @param facilityId
     * @param identifierComment
     */
    public ChargeIdentifierEntity(Long identifierId, String identifierType, String identifierValue, String identifierFormat, Long organizationId, Long facilityId,
            String identifierComment) {
        this.identifierId = identifierId;
        this.identifierType = identifierType;
        this.identifierValue = identifierValue;
        this.identifierFormat = identifierFormat;
        this.organizationId = organizationId;
        this.facilityId = facilityId;
        this.identifierComment = identifierComment;
    }

    /**
     * @return the identifierId
     */
    public Long getIdentifierId() {
        return identifierId;
    }

    /**
     * @param identifierId the identifierId to set
     */
    public void setIdentifierId(Long identifierId) {
        this.identifierId = identifierId;
    }

    /**
     * @return the identifierType
     */
    public String getIdentifierType() {
        return identifierType;
    }

    /**
     * @param identifierType the identifierType to set
     */
    public void setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
    }

    /**
     * @return the identifierValue
     */
    public String getIdentifierValue() {
        return identifierValue;
    }

    /**
     * @param identifierValue the identifierValue to set
     */
    public void setIdentifierValue(String identifierValue) {
        this.identifierValue = identifierValue;
    }

    /**
     * @return the identifierFormat
     */
    public String getIdentifierFormat() {
        return identifierFormat;
    }

    /**
     * @param identifierFormat the identifierFormat to set
     */
    public void setIdentifierFormat(String identifierFormat) {
        this.identifierFormat = identifierFormat;
    }

    /**
     * @return the organizationId
     */
    public Long getOrganizationId() {
        return organizationId;
    }

    /**
     * @param organizationId the organizationId to set
     */
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * @return the facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * @param facilityId the facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * @return the identifierComment
     */
    public String getIdentifierComment() {
        return identifierComment;
    }

    /**
     * @param identifierComment the identifierComment to set
     */
    public void setIdentifierComment(String identifierComment) {
        this.identifierComment = identifierComment;
    }

    /**
     * @return the charge
     */
    public ChargeEntity getCharge() {
        return charge;
    }

    /**
     * @param charge the charge to set
     */
    public void setCharge(ChargeEntity charge) {
        this.charge = charge;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeIdentifierEntity [identifierId=" + identifierId + ", identifierType=" + identifierType + ", identifierValue=" + identifierValue
                + ", identifierFormat=" + identifierFormat + ", organizationId=" + organizationId + ", facilityId=" + facilityId + ", identifierComment="
                + identifierComment + "]";
    }

}
