package syscon.arbutus.product.services.legal.contract.dto.condition;

import java.io.Serializable;
import java.util.List;

/**
 * ConditionsReturnType for Condition of Case Management Service
 *
 * @author yshang, lhan
 * @since December 20, 2012
 */
public class ConditionsReturnType implements Serializable {

    private static final long serialVersionUID = -757901375114542789L;

    private List<ConditionType> conditions;
    private Long totalSize;

    /**
     * @return the conditions
     */
    public List<ConditionType> getConditions() {
        return conditions;
    }

    /**
     * @param conditions the conditions to set
     */
    public void setConditions(List<ConditionType> conditions) {
        this.conditions = conditions;
    }

    /**
     * Gets the value of the totalSize property.
     *
     * @return the total size of search result
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * Sets the value of the totalSize property.
     *
     * @param totalSize the total size of search result
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ConditionsReturnType [getConditions()=" + getConditions() + ", getTotalSize()=" + getTotalSize() + "]";
    }

}
