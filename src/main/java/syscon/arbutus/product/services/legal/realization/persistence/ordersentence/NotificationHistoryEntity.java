package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * NotificationHistoryEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdNotiHst 'Notification History table for Order Sentence module of Legal service'
 * @since December 20, 2012
 */
//@Audited
@Entity
@Table(name = "LEG_OrdNotiHst")
public class NotificationHistoryEntity implements Serializable {

    private static final long serialVersionUID = -737931345114512734L;

    /**
     * @DbComment hstId 'Unique identification of a notification history instance.'
     */
    @Id
    @Column(name = "hstId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDNOTIHST_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDNOTIHST_ID", sequenceName = "SEQ_LEG_ORDNOTIHST_ID", allocationSize = 1)
    private Long historyId;

    /**
     * @DbComment notificationId 'Unique identification of a notification instance.'
     */
    @Column(name = "notificationId", nullable = false)
    private Long notificationId;

    /**
     * @DbComment notifyOrgAssoc 'The organization to be notified filing the OJ hold. Static reference to organization.'
     */
    @Column(name = "notifyOrgAssoc", nullable = true)
    private Long notificationOrganizationAssociation;

    /**
     * @DbComment notifyFacId 'The facility to be notified. Static reference to facility.'
     */
    @Column(name = "notifyFacId", nullable = true)
    private Long notificationFacilityAssociation;

    /**
     * @DbComment isNotificationNeeded 'If true then notification is required to be sent before releasing the offender, false otherwise.'
     */
    @Column(name = "isNotificationNeeded")
    private Boolean isNotificationNeeded;

    /**
     * @DbComment isNotificationConfirmed 'If true, then the notification confirmation was received, false otherwise. Default is false.'
     */
    @Column(name = "isNotificationConfirmed")
    private Boolean isNotificationConfirmed;

    /**
     * @DbComment agnBeNoti 'Id to notification agency location'
     */
    @ForeignKey(name = "Fk_LEG_OrdNotiHst1")
    @OneToMany(mappedBy = "notification", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<NotifyAgentLocAssocHistEntity> notificationAgencyLocationAssociations;

    @ForeignKey(name = "Fk_LEG_OrdNotiHst2")
    @OneToMany(mappedBy = "notification", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<NotifyAgentContactHistEntity> notificationAgencyContacts;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @return the historyId
     */
    public Long getHistoryId() {
        return historyId;
    }

    /**
     * @param historyId the historyId to set
     */
    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    /**
     * @return the notificationId
     */
    public Long getNotificationId() {
        return notificationId;
    }

    /**
     * @param notificationId the notificationId to set
     */
    public void setNotificationId(Long notificationId) {
        this.notificationId = notificationId;
    }

    /**
     * @return the notificationOrganizationAssociation
     */
    public Long getNotificationOrganizationAssociation() {
        return notificationOrganizationAssociation;
    }

    /**
     * @param notificationOrganizationAssociation the notificationOrganizationAssociation to set
     */
    public void setNotificationOrganizationAssociation(Long notificationOrganizationAssociation) {
        this.notificationOrganizationAssociation = notificationOrganizationAssociation;
    }

    /**
     * @return the notificationFacilityAssociation
     */
    public Long getNotificationFacilityAssociation() {
        return notificationFacilityAssociation;
    }

    /**
     * @param notificationFacilityAssociation the notificationFacilityAssociation to set
     */
    public void setNotificationFacilityAssociation(Long notificationFacilityAssociation) {
        this.notificationFacilityAssociation = notificationFacilityAssociation;
    }

    /**
     * @return the isNotificationNeeded
     */
    public Boolean getIsNotificationNeeded() {
        return isNotificationNeeded;
    }

    /**
     * @param isNotificationNeeded the isNotificationNeeded to set
     */
    public void setIsNotificationNeeded(Boolean isNotificationNeeded) {
        this.isNotificationNeeded = isNotificationNeeded;
    }

    /**
     * @return the isNotificationConfirmed
     */
    public Boolean getIsNotificationConfirmed() {
        return isNotificationConfirmed;
    }

    /**
     * @param isNotificationConfirmed the isNotificationConfirmed to set
     */
    public void setIsNotificationConfirmed(Boolean isNotificationConfirmed) {
        this.isNotificationConfirmed = isNotificationConfirmed;
    }

    /**
     * @return the notificationAgencyLocationAssociations
     */
    public Set<NotifyAgentLocAssocHistEntity> getNotificationAgencyLocationAssociations() {
        if (notificationAgencyLocationAssociations == null) {
			notificationAgencyLocationAssociations = new HashSet<NotifyAgentLocAssocHistEntity>();
		}
        return notificationAgencyLocationAssociations;
    }

    /**
     * @param notificationAgencyLocationAssociations the notificationAgencyLocationAssociations to set
     */
    public void setNotificationAgencyLocationAssociations(Set<NotifyAgentLocAssocHistEntity> notificationAgencyLocationAssociations) {
        if (notificationAgencyLocationAssociations != null) {
            for (NotifyAgentLocAssocHistEntity noti : notificationAgencyLocationAssociations) {
                noti.setNotification(this);
            }
            this.notificationAgencyLocationAssociations = notificationAgencyLocationAssociations;
        } else {
			this.notificationAgencyLocationAssociations = new HashSet<NotifyAgentLocAssocHistEntity>();
		}
    }

    /**
     * @return the notificationAgencyContacts
     */
    public Set<NotifyAgentContactHistEntity> getNotificationAgencyContacts() {
        if (notificationAgencyContacts == null) {
			notificationAgencyContacts = new HashSet<NotifyAgentContactHistEntity>();
		}
        return notificationAgencyContacts;
    }

    /**
     * @param notificationAgencyContacts the notificationAgencyContacts to set
     */
    public void setNotificationAgencyContacts(Set<NotifyAgentContactHistEntity> notificationAgencyContacts) {
        if (notificationAgencyContacts != null) {
            for (NotifyAgentContactHistEntity noti : notificationAgencyContacts) {
                noti.setNotification(this);
            }
            this.notificationAgencyContacts = notificationAgencyContacts;
        } else {
			this.notificationAgencyContacts = new HashSet<NotifyAgentContactHistEntity>();
		}
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((historyId == null) ? 0 : historyId.hashCode());
        result = prime * result + ((isNotificationConfirmed == null) ? 0 : isNotificationConfirmed.hashCode());
        result = prime * result + ((isNotificationNeeded == null) ? 0 : isNotificationNeeded.hashCode());
        result = prime * result + ((notificationAgencyContacts == null) ? 0 : notificationAgencyContacts.hashCode());
        result = prime * result + ((notificationAgencyLocationAssociations == null) ? 0 : notificationAgencyLocationAssociations.hashCode());
        result = prime * result + ((notificationFacilityAssociation == null) ? 0 : notificationFacilityAssociation.hashCode());
        result = prime * result + ((notificationId == null) ? 0 : notificationId.hashCode());
        result = prime * result + ((notificationOrganizationAssociation == null) ? 0 : notificationOrganizationAssociation.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        NotificationHistoryEntity other = (NotificationHistoryEntity) obj;
        if (historyId == null) {
            if (other.historyId != null) {
				return false;
			}
        } else if (!historyId.equals(other.historyId)) {
			return false;
		}
        if (isNotificationConfirmed == null) {
            if (other.isNotificationConfirmed != null) {
				return false;
			}
        } else if (!isNotificationConfirmed.equals(other.isNotificationConfirmed)) {
			return false;
		}
        if (isNotificationNeeded == null) {
            if (other.isNotificationNeeded != null) {
				return false;
			}
        } else if (!isNotificationNeeded.equals(other.isNotificationNeeded)) {
			return false;
		}
        if (notificationAgencyContacts == null) {
            if (other.notificationAgencyContacts != null) {
				return false;
			}
        } else if (!notificationAgencyContacts.equals(other.notificationAgencyContacts)) {
			return false;
		}
        if (notificationAgencyLocationAssociations == null) {
            if (other.notificationAgencyLocationAssociations != null) {
				return false;
			}
        } else if (!notificationAgencyLocationAssociations.equals(other.notificationAgencyLocationAssociations)) {
			return false;
		}
        if (notificationFacilityAssociation == null) {
            if (other.notificationFacilityAssociation != null) {
				return false;
			}
        } else if (!notificationFacilityAssociation.equals(other.notificationFacilityAssociation)) {
			return false;
		}
        if (notificationId == null) {
            if (other.notificationId != null) {
				return false;
			}
        } else if (!notificationId.equals(other.notificationId)) {
			return false;
		}
        if (notificationOrganizationAssociation == null) {
            if (other.notificationOrganizationAssociation != null) {
				return false;
			}
        } else if (!notificationOrganizationAssociation.equals(other.notificationOrganizationAssociation)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "NotificationHistoryEntity [historyId=" + historyId + ", notificationId=" + notificationId + ", notificationOrganizationAssociation="
                + notificationOrganizationAssociation + ", notificationFacilityAssociation=" + notificationFacilityAssociation + ", isNotificationNeeded="
                + isNotificationNeeded + ", isNotificationConfirmed=" + isNotificationConfirmed + ", notificationAgencyLocationAssociations="
                + notificationAgencyLocationAssociations + ", notificationAgencyContacts=" + notificationAgencyContacts + "]";
    }

}
