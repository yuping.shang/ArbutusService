package syscon.arbutus.product.services.legal.contract.dto.caseinformation;

import java.io.Serializable;
import java.util.List;

public class CaseInfosReturnType implements Serializable {

    private static final long serialVersionUID = -6628602361750632017L;

    private List<CaseInfoType> caseInfos;
    private Long totalSize;

    /**
     * @return the caseInfos
     */
    public List<CaseInfoType> getCaseInfos() {
        return caseInfos;
    }

    /**
     * @param caseInfos the caseInfos to set
     */
    public void setCaseInfos(List<CaseInfoType> caseInfos) {
        this.caseInfos = caseInfos;
    }

    /**
     * Gets the value of the totalSize property.
     *
     * @return the total size of search result
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * Sets the value of the totalSize property.
     *
     * @param totalSize the total size of search result
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "caseinfosReturnType [getCaseinfos()=" + getCaseInfos() + ", getTotalSize()=" + getTotalSize() + "]";
    }

}
