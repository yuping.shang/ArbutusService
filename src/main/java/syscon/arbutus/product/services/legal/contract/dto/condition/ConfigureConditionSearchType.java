package syscon.arbutus.product.services.legal.contract.dto.condition;

import java.io.Serializable;

/**
 * ConfigureConditionSearchType for Configure Condition of Case Management Service
 *
 * @author amlendu kumar
 * @since May 22, 2014
 */
public class ConfigureConditionSearchType implements Serializable {

    private static final long serialVersionUID = 6059649824821687621L;
    private String code;
    private String type;
    private String category;
    private String subCategory;
    private String shortName;
    private String attribute;

    /**
     * Get code -- code of the Configure Condition.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Set code -- code of the Configure Condition.
     *
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Get type -- Is a logical grouping of Configure Condition by Sentences, Orders, Charges etc.
     * It is a reference code value of ConditionType set.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Set type -- Is a logical grouping of Configure Condition by Sentences, Orders, Charges etc.
     * It is a reference code value of ConditionType set.
     *
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Get condition category -- A condition can also be classified under different categories like Financial, Programs etc.
     * It is a reference code value of ConditionCategory set.
     *
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Set category -- A condition can also be classified under different categories like Financial, Programs etc.
     * It is a reference code value of ConditionCategory set.
     *
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * Get sub category -- Is a logical sub grouping of the Configure Condition category. E.g.
     * Condition category for Financials, could be sub grouped as Restitution to victims, Electronic monitor payment etc.
     * It is a reference code value of ConditionSubCategory set.
     *
     * @return the subCategory
     */
    public String getSubCategory() {
        return subCategory;
    }

    /**
     * Set sub category -- Is a logical sub grouping of the Configure Condition category. E.g.
     * Condition category for Financials, could be sub grouped as Restitution to victims, Electronic monitor payment etc.
     * It is a reference code value of ConditionSubCategory set.
     *
     * @param subCategory the subCategory to set
     */
    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    /**
     * Get shortName -- short name of the Configure Condition.
     *
     * @return the shortName
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Set shortName -- short name of the Configure Condition.
     *
     * @param shortName the shortName to set
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * Get attribute -- attribute of the Configure Condition which could be Amount, Duration or Distant Restrictions.
     *
     * @return the attribute
     */
    public String getAttribute() {
        return attribute;
    }

    /**
     * Set attribute -- attribute of the Configure Condition which could be Amount, Duration or Distant Restrictions.
     *
     * @param attribute the attribute to set
     */
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ConfigureConditionSearchType [code=" + code + ", type=" + type + ", category=" + category + ", subCategory=" + subCategory + ", shortName=" + shortName
                + ", attribute=" + attribute + "]";
    }

}
