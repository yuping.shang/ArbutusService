package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

/**
 * LegalOrderType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 28, 2012
 */
public class LegalOrderType extends OrderType {

    private static final long serialVersionUID = -3813022743980472206L;

    /**
     * No specific properties/data elements defined from SDD 1.0.
     * Just extends OrderType
     */

	/* (non-Javadoc)
     * @see java.lang.Object#toString()
	 */
    @Override
    public String toString() {
        return "LegalOrderType [toString()=" + super.toString() + "]";
    }

}
