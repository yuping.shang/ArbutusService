package syscon.arbutus.product.services.legal.realization.persistence.caseactivity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.legal.realization.persistence.caseaffiliation.CaseAffiliationEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.CaseInfoEntity;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the case activity entity.
 *
 * @author lhan
 * @DbComment LEG_CACaseActivity 'The case activity table for CaseActivity service'
 * @DbComment .DTYPE 'Dicriminator column for Hibernate use'
 */
@Audited
@Entity
@Table(name = "LEG_CACaseActivity")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@SQLDelete(sql = "UPDATE LEG_CACaseActivity SET flag = 4 WHERE caseActivityId = ? and version = ?")
@Where(clause = "flag = 1")
public class CaseActivityEntity implements Serializable {

    private static final long serialVersionUID = 4223361514176305215L;

    /**
     * @DbComment 'Case Activity Identification. The unique ID for each case related event/activity created in the system.'
     */
    @Id
    @Column(name = "CaseActivityId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "Seq_LEG_CACaseActivity_Id")
    @SequenceGenerator(name = "Seq_LEG_CACaseActivity_Id", sequenceName = "Seq_LEG_CACaseActivity_Id", allocationSize = 1)
    private Long caseActivityId;

    /**
     * @DbComment 'The date the case event or activity occurred.'
     */
    @Column(name = "ActivityOccurredDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date activityOccurredDate;

    /**
     * @DbComment 'This determines which specific type of case activity is created. E.g Court, Probation, Police.'
     */
    @MetaCode(set = CaseActivityMetaSet.ACTIVITY_CATEGORY)
    @Column(name = "ActivityCategory", nullable = false, length = 64)
    private String activityCategory;

    /**
     * @DbComment 'The specific event type that falls under the activity category. E.g. Arraignment, Trial, Sentencing.'
     */
    @MetaCode(set = CaseActivityMetaSet.ACTIVITY_TYPE)
    @Column(name = "ActivityType", nullable = false, length = 64)
    private String activityType;

    /**
     * @DbComment 'An outcome that may be recorded as a result of the case activity.'
     */
    @MetaCode(set = CaseActivityMetaSet.ACTIVITY_OUTCOME)
    @Column(name = "ActivityOutcome", nullable = true, length = 64)
    private String activityOutcome;

    /**
     * @DbComment 'The date the case event outcome is recorded or updated.'
     */
    @Column(name = "OutcomeDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date outcomeDate;

    /**
     * @DbComment 'The status of the case activity. Either ACTIVE or INACTIVE.'
     */
    @MetaCode(set = CaseActivityMetaSet.ACTIVITY_STATUS)
    @Column(name = "ActivityStatus", nullable = false, length = 64)
    private String activityStatus;

    /**
     * @DbComment 'Comment associated with the case event or activity.'
     */
    @Column(name = "CommentText", nullable = true, length = 512)
    private String commentText;

    /**
     * @DbComment 'Date/Time when the comment was created.'
     */
    @Column(name = "CommentDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentDate;

    @ForeignKey(name = "Fk_LEG_CAChargeAssc")
    @OneToMany(mappedBy = "caseActivity", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<CAChargeAssociationEntity> chargeAssociations = new HashSet<CAChargeAssociationEntity>();

    @NotAudited
    @OneToMany(mappedBy = "caseActivity", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private Set<CaseActivityHistEntity> history;

    @OneToMany(mappedBy = "caseActivity", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<CAActivityIdEntity> activityIds = new HashSet<CAActivityIdEntity>();

    @ManyToMany(mappedBy = "affiliatedCaseActivities", fetch = FetchType.LAZY)
    private Set<CaseAffiliationEntity> caseAffiliations;

    /**
     * @DbComment Leg_CaseInfo_CaseAct 'The join table of the tables LEG_CICaseInfo and LEG_CACaseActivity';
     * @DbComment Leg_CaseInfo_CaseAct.CaseActivityId 'CaseActivityId in LEG_CACaseActivity table';
     * @DbComment Leg_CaseInfo_CaseAct.CaseInfoId 'CaseInfoId in LEG_CICaseInfo table';
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "Leg_CaseInfo_CaseAct", joinColumns = { @JoinColumn(name = "CaseActivityId") },
            inverseJoinColumns = { @JoinColumn(name = "CaseInfoId") })
    @BatchSize(size = 20)
    private Set<CaseInfoEntity> caseInfos = new HashSet<CaseInfoEntity>();

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    @Column(name = "version")
    private Long version;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();
    ;

    /**
     * Default constructor
     */
    public CaseActivityEntity() {

    }

    /**
     * Constructor
     *
     * @param caseActivityId
     * @param activityOccurredDateSet
     * @param activityCategory
     * @param activityType
     * @param activityOutcome
     * @param outcomeDate
     * @param activityStatus
     * @param comment
     * @param commentDate
     * @param chargeAssociations
     * @param associations
     * @param privileges
     * @param stamp
     */
    public CaseActivityEntity(Long caseActivityId, Date activityOccurredDate, String activityCategory, String activityType, String activityOutcome, Date outcomeDate,
            String activityStatus, String commentText, Date commentDate, Set<CAChargeAssociationEntity> chargeAssociations, StampEntity stamp,
            Set<CaseActivityHistEntity> history, Set<CAActivityIdEntity> activityIds, Set<CaseAffiliationEntity> caseAffiliations, Set<CaseInfoEntity> caseInfos) {
        super();
        this.caseActivityId = caseActivityId;
        this.activityOccurredDate = activityOccurredDate;
        this.activityCategory = activityCategory;
        this.activityType = activityType;
        this.activityOutcome = activityOutcome;
        this.outcomeDate = outcomeDate;
        this.activityStatus = activityStatus;
        this.commentText = commentText;
        this.commentDate = commentDate;
        this.chargeAssociations = chargeAssociations;
        this.stamp = stamp;
        this.history = history;
        this.activityIds = activityIds;
        this.caseAffiliations = caseAffiliations;
        this.caseInfos = caseInfos;
    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /**
     * @return the caseAffiliations
     */
    public Set<CaseAffiliationEntity> getCaseAffiliations() {
        if (caseAffiliations == null) {
			caseAffiliations = new HashSet<CaseAffiliationEntity>();
		}
        return caseAffiliations;
    }

    /**
     * @param caseAffiliations the caseAffiliations to set
     */
    public void setCaseAffiliations(Set<CaseAffiliationEntity> caseAffiliations) {
        this.caseAffiliations = caseAffiliations;
    }

    /**
     * @return the caseInfos
     */
    public Set<CaseInfoEntity> getCaseInfos() {
        if (caseInfos == null) {
            caseInfos = new HashSet<CaseInfoEntity>();
        }
        return caseInfos;
    }

    /**
     * @param caseInfos the caseInfos to set
     */
    public void setCaseInfos(Set<CaseInfoEntity> caseInfos) {
        this.caseInfos = caseInfos;
    }

    /**
     * @return the activityIds
     */
    public Set<CAActivityIdEntity> getActivityIds() {
        if (activityIds == null) {
			activityIds = new HashSet<CAActivityIdEntity>();
		}
        return activityIds;
    }

    /**
     * @param activityIds the activityIds to set
     */
    public void setActivityIds(Set<CAActivityIdEntity> activityIds) {
        this.activityIds = activityIds;
    }

    /**
     * @param entity
     */
    public void addCAActivityIdEntity(CAActivityIdEntity entity) {
        entity.setCaseActivity(this);
        getActivityIds().add(entity);
    }

    /**
     * @return the caseActivityId
     */
    public Long getCaseActivityId() {
        return caseActivityId;
    }

    /**
     * @param caseActidseActivityId to set
     */
    public void setCaseActivityId(Long caseActivityId) {
        this.caseActivityId = caseActivityId;
    }

    /**
     * @return the activityOccurredDate
     */
    public Date getActivityOccurredDate() {
        return activityOccurredDate;
    }

    /**
     * @param activityOccurredDate the activityOccurredDate to set
     */
    public void setActivityOccurredDate(Date activityOccurredDate) {
        this.activityOccurredDate = activityOccurredDate;
    }

    /**
     * @return the activityCategory
     */
    public String getActivityCategory() {
        return activityCategory;
    }

    /**
     * @param activityCategory the activityCategory to set
     */
    public void setActivityCategory(String activityCategory) {
        this.activityCategory = activityCategory;
    }

    /**
     * @return the activityType
     */
    public String getActivityType() {
        return activityType;
    }

    /**
     * @param activityType the activityType to set
     */
    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    /**
     * @return the activityOutcome
     */
    public String getActivityOutcome() {
        return activityOutcome;
    }

    /**
     * @param activityOutcome the activityOutcome to set
     */
    public void setActivityOutcome(String activityOutcome) {
        this.activityOutcome = activityOutcome;
    }

    /**
     * @return the outcomeDate
     */
    public Date getOutcomeDate() {
        return outcomeDate;
    }

    /**
     * @param outcomeDate the outcomeDate to set
     */
    public void setOutcomeDate(Date outcomeDate) {
        this.outcomeDate = outcomeDate;
    }

    /**
     * @return the activityStatus
     */
    public String getActivityStatus() {
        return activityStatus;
    }

    /**
     * @param activityStatus the activityStatus to set
     */
    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }

    /**
     * @return the commentText
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * @param commentText the commentText to set
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    /**
     * @return the commentDate
     */
    public Date getCommentDate() {
        return commentDate;
    }

    /**
     * @param commentDate the commentDate to set
     */
    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    /**
     * @return the chargeAssociations
     */
    public Set<CAChargeAssociationEntity> getChargeAssociations() {
        return chargeAssociations;
    }

    /**
     * @param chargeAssociations the chargeAssociations to set
     */
    public void setChargeAssociations(Set<CAChargeAssociationEntity> chargeAssociations) {
        this.chargeAssociations = chargeAssociations;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the history
     */
    public Set<CaseActivityHistEntity> getHistory() {
        return history;
    }

    /**
     * @param history the history to set
     */
    public void setHistory(Set<CaseActivityHistEntity> history) {
        this.history = history;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((activityCategory == null) ? 0 : activityCategory.hashCode());
        result = prime * result + ((activityOccurredDate == null) ? 0 : activityOccurredDate.hashCode());
        result = prime * result + ((activityOutcome == null) ? 0 : activityOutcome.hashCode());
        result = prime * result + ((activityStatus == null) ? 0 : activityStatus.hashCode());
        result = prime * result + ((activityType == null) ? 0 : activityType.hashCode());
        result = prime * result + ((caseActivityId == null) ? 0 : caseActivityId.hashCode());
        result = prime * result + ((chargeAssociations == null) ? 0 : chargeAssociations.hashCode());
        result = prime * result + ((commentDate == null) ? 0 : commentDate.hashCode());
        result = prime * result + ((commentText == null) ? 0 : commentText.hashCode());
        result = prime * result + ((history == null) ? 0 : history.hashCode());
        result = prime * result + ((stamp == null) ? 0 : stamp.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        CaseActivityEntity other = (CaseActivityEntity) obj;
        if (activityCategory == null) {
            if (other.activityCategory != null) {
				return false;
			}
        } else if (!activityCategory.equals(other.activityCategory)) {
			return false;
		}
        if (activityOccurredDate == null) {
            if (other.activityOccurredDate != null) {
				return false;
			}
        } else if (!activityOccurredDate.equals(other.activityOccurredDate)) {
			return false;
		}
        if (activityOutcome == null) {
            if (other.activityOutcome != null) {
				return false;
			}
        } else if (!activityOutcome.equals(other.activityOutcome)) {
			return false;
		}
        if (activityStatus == null) {
            if (other.activityStatus != null) {
				return false;
			}
        } else if (!activityStatus.equals(other.activityStatus)) {
			return false;
		}
        if (activityType == null) {
            if (other.activityType != null) {
				return false;
			}
        } else if (!activityType.equals(other.activityType)) {
			return false;
		}
        if (caseActivityId == null) {
            if (other.caseActivityId != null) {
				return false;
			}
        } else if (!caseActivityId.equals(other.caseActivityId)) {
			return false;
		}
        if (chargeAssociations == null) {
            if (other.chargeAssociations != null) {
				return false;
			}
        } else if (!chargeAssociations.equals(other.chargeAssociations)) {
			return false;
		}
        if (commentDate == null) {
            if (other.commentDate != null) {
				return false;
			}
        } else if (!commentDate.equals(other.commentDate)) {
			return false;
		}
        if (commentText == null) {
            if (other.commentText != null) {
				return false;
			}
        } else if (!commentText.equals(other.commentText)) {
			return false;
		}
        if (history == null) {
            if (other.history != null) {
				return false;
			}
        } else if (!history.equals(other.history)) {
			return false;
		}
        if (stamp == null) {
            if (other.stamp != null) {
				return false;
			}
        } else if (!stamp.equals(other.stamp)) {
			return false;
		}
        if (version == null) {
            if (other.version != null) {
				return false;
			}
        } else if (!version.equals(other.version)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseActivityEntity [getFlag()=" + getFlag() + ", getCaseAffiliations()=" + getCaseAffiliations() + ", getCaseInfos()=" + getCaseInfos()
                + ", getActivityIds()=" + getActivityIds() + ", getCaseActivityId()=" + getCaseActivityId() + ", getActivityOccurredDate()=" + getActivityOccurredDate()
                + ", getActivityCategory()=" + getActivityCategory() + ", getActivityType()=" + getActivityType() + ", getActivityOutcome()=" + getActivityOutcome()
                + ", getOutcomeDate()=" + getOutcomeDate() + ", getActivityStatus()=" + getActivityStatus() + ", getCommentText()=" + getCommentText()
                + ", getCommentDate()=" + getCommentDate() + ", getChargeAssociations()=" + getChargeAssociations() + ", getStamp()=" + getStamp() + ", getVersion()="
                + getVersion() + ", getHistory()=" + getHistory() + "]";
    }

}
