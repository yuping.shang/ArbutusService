package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * ContactType for OrderSentence Module.
 *
 * @author yshang
 * @since October 16, 2013
 */
public class ContactType implements Serializable {

    private static final long serialVersionUID = 4063728282741672812L;

    /**
     * Identification of ContactType
     */
    private Long contactId;

    /**
     * First name
     */
    @Size(max = 128)
    private String firstName;

    /**
     * Last name
     */
    @Size(max = 128)
    private String lastName;

    /**
     * Address
     */
    @Size(max = 512)
    private String address;

    /**
     * City
     */
    @Size(max = 128)
    private String city;

    /**
     * County
     */
    @Size(max = 128)
    private String county;

    /**
     * Province or State
     */
    @Size(max = 128)
    private String provinceState;

    /**
     * Country
     */
    @Size(max = 128)
    private String country;

    /**
     * Zip Code
     */
    @Size(max = 64)
    private String zipCode;

    /**
     * Phone Number
     */
    @Size(max = 64)
    private String phoneNumber;

    /**
     * Fax Number
     */
    @Size(max = 64)
    private String faxNumber;

    /**
     * Email
     */
    @Size(max = 128)
    private String email;

    /**
     * Identity Number of the contact
     */
    @Size(max = 128)
    private String identityNumber;

    /**
     * Agent Contact
     */
    @Size(max = 1024)
    private String agentContact;

    /**
     * Constructor
     */
    public ContactType() {
    }

    /**
     * getContactId
     * <p>Identification of ContactType
     * <p>Not Required when create; Required when update
     *
     * @return contactId
     */
    public Long getContactId() {
        return contactId;
    }

    /**
     * setContactId
     * <p>Identification of ContactType
     * <p>Not Required when create; Required when update
     *
     * @param contactId
     */
    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    /**
     * getFirstName
     * <p>First Name
     *
     * @return firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * setFirstName
     * <p>First Name
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * getLastName
     * <p>Last Name
     *
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * setLastName
     * <p>Last Name
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * getAddress
     * <p>Address
     *
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * setAddress
     * <p>Address
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * getCity
     * <p>City
     *
     * @return city
     */
    public String getCity() {
        return city;
    }

    /**
     * setCity
     * <p>City
     *
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * getCounty
     * <p>County
     *
     * @return county
     */
    public String getCounty() {
        return county;
    }

    /**
     * setCounty
     * <p>County
     *
     * @param county
     */
    public void setCounty(String county) {
        this.county = county;
    }

    /**
     * getProvinceState
     * <p>Province or State
     *
     * @return provinceState
     */
    public String getProvinceState() {
        return provinceState;
    }

    /**
     * setProvinceState
     * <p>Province or State
     *
     * @param provinceState
     */
    public void setProvinceState(String provinceState) {
        this.provinceState = provinceState;
    }

    /**
     * getCountry
     * <p>Country
     *
     * @return country
     */
    public String getCountry() {
        return country;
    }

    /**
     * setCountry
     * <p>Country
     *
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * getZipCode
     * <p>Zip Code
     *
     * @return zipCode
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * setZipCode
     * <p>Zip Code
     *
     * @param zipCode
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * getPhoneNumber
     * <p>Phone Number
     *
     * @return phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * setPhoneNumber
     * <p>Phone Number
     *
     * @param phoneNumber
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * getFaxNumber
     * <p>Fax Number
     *
     * @return faxNumber
     */
    public String getFaxNumber() {
        return faxNumber;
    }

    /**
     * setFaxNumber
     * <p>Fax Number
     *
     * @param faxNumber
     */
    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    /**
     * getEmail
     * <p>Email
     *
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * setEmail
     * <p>Email
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * getIdentityNumber
     * <p>Identity Number of the contact
     *
     * @return identityNumber
     */
    public String getIdentityNumber() {
        return identityNumber;
    }

    /**
     * setIdentityNumber
     * <p>Identity Number of the contact
     *
     * @param identityNumber
     */
    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    /**
     * getAgentContact
     * <p>Contact of the Agent
     *
     * @return agentContact
     */
    public String getAgentContact() {
        return agentContact;
    }

    /**
     * setAgentContact
     * <p>Contact of the Agent
     *
     * @param agentContact
     */
    public void setAgentContact(String agentContact) {
        this.agentContact = agentContact;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (!(o instanceof ContactType)) {
			return false;
		}

        ContactType that = (ContactType) o;

        if (address != null ? !address.equals(that.address) : that.address != null) {
			return false;
		}
        if (agentContact != null ? !agentContact.equals(that.agentContact) : that.agentContact != null) {
			return false;
		}
        if (city != null ? !city.equals(that.city) : that.city != null) {
			return false;
		}
        if (contactId != null ? !contactId.equals(that.contactId) : that.contactId != null) {
			return false;
		}
        if (country != null ? !country.equals(that.country) : that.country != null) {
			return false;
		}
        if (county != null ? !county.equals(that.county) : that.county != null) {
			return false;
		}
        if (email != null ? !email.equals(that.email) : that.email != null) {
			return false;
		}
        if (faxNumber != null ? !faxNumber.equals(that.faxNumber) : that.faxNumber != null) {
			return false;
		}
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) {
			return false;
		}
        if (identityNumber != null ? !identityNumber.equals(that.identityNumber) : that.identityNumber != null) {
			return false;
		}
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) {
			return false;
		}
        if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) {
			return false;
		}
        if (provinceState != null ? !provinceState.equals(that.provinceState) : that.provinceState != null) {
			return false;
		}
        if (zipCode != null ? !zipCode.equals(that.zipCode) : that.zipCode != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = contactId != null ? contactId.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (county != null ? county.hashCode() : 0);
        result = 31 * result + (provinceState != null ? provinceState.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (zipCode != null ? zipCode.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (faxNumber != null ? faxNumber.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (identityNumber != null ? identityNumber.hashCode() : 0);
        result = 31 * result + (agentContact != null ? agentContact.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ContactType{" +
                "contactId=" + contactId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", county='" + county + '\'' +
                ", provinceState='" + provinceState + '\'' +
                ", country='" + country + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", faxNumber='" + faxNumber + '\'' +
                ", email='" + email + '\'' +
                ", identityNumber='" + identityNumber + '\'' +
                ", agentContact='" + agentContact + '\'' +
                '}';
    }
}
