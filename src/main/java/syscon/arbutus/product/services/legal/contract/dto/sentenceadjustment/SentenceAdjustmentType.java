package syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * SentenceAdjustmentType for Sentence Adjustment
 *
 * @author yshang
 * @since October 03, 2013
 */
public class SentenceAdjustmentType implements Serializable {

    private static final long serialVersionUID = -6670896449664499661L;

    /**
     * Supervision ID
     */
    @NotNull
    private Long supervisionId;
    /**
     * A set of AdjustmentType
     */
    @Valid
    private Set<AdjustmentType> adjustments;

    /**
     * getSupervisionId Get supervision id, required.
     *
     * @return Long
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * setSupervisionId Set supervision id, required.
     *
     * @param supervisionId
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * getAdjustments Get a set of Adjustments, optional.
     *
     * @return Set&lt;AdjustmentType>
     */
    public Set<AdjustmentType> getAdjustments() {
        if (this.adjustments == null) {
			this.adjustments = new HashSet<>();
		}
        return adjustments;
    }

    /**
     * setAdjustments Set a set of Adjustments, optional.
     *
     * @param adjustments Set&lt;AdjustmentType>
     */
    public void setAdjustments(Set<AdjustmentType> adjustments) {
        this.adjustments = adjustments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (!(o instanceof SentenceAdjustmentType)) {
			return false;
		}

        SentenceAdjustmentType that = (SentenceAdjustmentType) o;

        if (adjustments != null ? !adjustments.equals(that.adjustments) : that.adjustments != null) {
			return false;
		}
        if (supervisionId != null ? !supervisionId.equals(that.supervisionId) : that.supervisionId != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = supervisionId != null ? supervisionId.hashCode() : 0;
        result = 31 * result + (adjustments != null ? adjustments.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SentenceAdjustmentType{" +
                "supervisionId=" + supervisionId +
                ", adjustments=" + adjustments +
                '}';
    }
}
