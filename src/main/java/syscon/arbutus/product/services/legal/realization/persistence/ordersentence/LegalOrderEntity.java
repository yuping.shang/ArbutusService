package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

import org.hibernate.envers.Audited;

/**
 * LegalOrderEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdOrder 'Order table for Order Sentence module of Legal service'
 * @since December 28, 2012
 */
@Audited
@Entity
@DiscriminatorValue("Legal")
public class LegalOrderEntity extends OrderEntity implements Serializable {

    private static final long serialVersionUID = 6344483224297827048L;

    /**
     * No specific properties/data elements defined from SDD 1.0.
     * Just extends OrderType
     */

	/* (non-Javadoc)
     * @see java.lang.Object#toString()
	 */
    @Override
    public String toString() {
        return "LegalOrderType [toString()=" + super.toString() + "]";
    }

}
