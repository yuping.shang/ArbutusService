package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the charge plea entity.
 *
 * @DbComment LEG_CHGPlea 'The charge plea data table'
 */
@Audited
@Entity
@Table(name = "LEG_CHGPlea")
public class ChargePleaEntity implements Serializable {

    private static final long serialVersionUID = -5423108045802879104L;

    /**
     * @DbComment ChargePleaId 'Unique id of a charge plea record of a charge instance'
     */
    @Id
    @Column(name = "ChargePleaId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGPlea_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGPlea_Id", sequenceName = "SEQ_LEG_CHGPlea_Id", allocationSize = 1)
    private Long chargePleaId;

    /**
     * @DbComment PleaCode 'A kind of plea.'
     */
    @MetaCode(set = MetaSet.CHARGE_PLEA)
    @Column(name = "PleaCode", nullable = false, length = 64)
    private String pleaCode;

    /**
     * @DbComment PleaComment 'Comments regarding the plea.'
     */
    @Column(name = "PleaComment", nullable = true, length = 512)
    private String pleaComment;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @Version
    private Long version;

    /**
     *
     */
    public ChargePleaEntity() {
    }

    /**
     * @param chargePleaId
     * @param pleaCode
     * @param pleaComment
     */
    public ChargePleaEntity(Long chargePleaId, String pleaCode, String pleaComment) {
        this.chargePleaId = chargePleaId;
        this.pleaCode = pleaCode;
        this.pleaComment = pleaComment;
    }

    /**
     * @return the chargePleaId
     */
    public Long getChargePleaId() {
        return chargePleaId;
    }

    /**
     * @param chargePleaId the chargePleaId to set
     */
    public void setChargePleaId(Long chargePleaId) {
        this.chargePleaId = chargePleaId;
    }

    /**
     * @return the pleaCode
     */
    public String getPleaCode() {
        return pleaCode;
    }

    /**
     * @param pleaCode the pleaCode to set
     */
    public void setPleaCode(String pleaCode) {
        this.pleaCode = pleaCode;
    }

    /**
     * @return the pleaComment
     */
    public String getPleaComment() {
        return pleaComment;
    }

    /**
     * @param pleaComment the pleaComment to set
     */
    public void setPleaComment(String pleaComment) {
        this.pleaComment = pleaComment;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargePleaEntity [chargePleaId=" + chargePleaId + ", pleaCode=" + pleaCode + ", pleaComment=" + pleaComment + "]";
    }

}
