package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

/**
 * IntermittentScheduleType for Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 27, 2012
 */
@ArbutusConstraint(constraints = "startTime <= endTime")
public class IntermittentScheduleType implements Serializable {

    private static final long serialVersionUID = -4281718550485263631L;
    @NotNull
    private String dayOfWeek;
    private TimeValue startTime;
    private TimeValue endTime;
    private Date reportingDate;
    private Date scheduleEndDate;
    private Long locationId;
    private String dayOutOfWeek;
    private Boolean sameDay;
    private Long interScheduleId;
    private Long scheduleIdentification;

    /**
     * Constructor
     */
    public IntermittentScheduleType() {
        super();
    }

    /**
     * Constructor
     *
     * @param dayOfWeek       String, Required. Day of the Week.
     * @param startTime       IntermittentScheduleType.TimeValue, Optional. Start time on the day of the week.
     * @param endTime         IntermittentScheduleType.TimeValue, Optional. End time on the day of the week.
     * @param reportingDate   Date, Optional. Intermittent reporting date
     * @param scheduleEndDate Date, Optional. Intermittent schedule end date
     */
    public IntermittentScheduleType(String dayOfWeek, TimeValue startTime, TimeValue endTime, Date reportingDate, Date scheduleEndDate, Long locationId, String dayOut) {
        super();
        this.dayOfWeek = dayOfWeek;
        this.startTime = startTime;
        this.endTime = endTime;
        this.reportingDate = reportingDate;
        this.scheduleEndDate = scheduleEndDate;
        this.locationId = locationId;
        this.dayOutOfWeek = dayOut;
    }

    /**
     * Get Day of Week
     *
     * @return the dayOfWeek
     */
    public String getDayOfWeek() {
        return dayOfWeek;
    }

    /**
     * Set Day of Week
     *
     * @param dayOfWeek the dayOfWeek to set
     */
    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    /**
     * Get Start Time
     * -- Start time on the day of the week.
     * Only time portion is used.
     *
     * @return the startTime
     */
    public TimeValue getStartTime() {
        return startTime;
    }

    /**
     * Set Start Time
     * -- Start time on the day of the week.
     * Only time portion is used.
     *
     * @param startTime the startTime to set
     */
    public void setStartTime(TimeValue startTime) {
        this.startTime = startTime;
    }

    /**
     * toStartTtime
     *
     * @return String returns the string of startTime
     */
    public String toStartTime() {
        return startTime.toString();
    }

    /**
     * Get End Time
     * -- End time on the day of the week.
     * Only time portion is used.
     *
     * @return the endTime
     */
    public TimeValue getEndTime() {
        return endTime;
    }

    /**
     * Set End Time
     * -- End time on the day of the week.
     * Only time portion is used.
     *
     * @param endTime the endTime to set
     */
    public void setEndTime(TimeValue endTime) {
        this.endTime = endTime;
    }

    /**
     * toEndTime
     *
     * @return String returns string of endTime
     */
    public String toEndTime() {
        return endTime.toString();
    }

    /**
     * reportingDate
     * -- Intermittent reporting date
     *
     * @return the reportingDate
     */
    public Date getReportingDate() {
        return reportingDate;
    }

    /**
     * reportingDate
     * -- Intermittent reporting date
     *
     * @param reportingDate the reportingDate to set
     */
    public void setReportingDate(Date reportingDate) {
        this.reportingDate = reportingDate;
    }

    /**
     * scheduleEndDate
     * -- Intermittent schedule end date
     *
     * @return the scheduleEndDate
     */
    public Date getScheduleEndDate() {
        return scheduleEndDate;
    }

    /**
     * scheduleEndDate
     * -- Intermittent schedule end date
     *
     * @param scheduleEndDate the scheduleEndDate to set
     */
    public void setScheduleEndDate(Date scheduleEndDate) {
        this.scheduleEndDate = scheduleEndDate;
    }

    /**
     * locationId of Reporting Location
     *
     * @return the locationId
     */
    public Long getLocationId() {
        return locationId;
    }

    /**
     * set the Reporting Location
     *
     * @param locationId the locationId to set
     */
    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    /**
     * @return the dayOutOfWeek
     */
    public String getDayOutOfWeek() {
        return dayOutOfWeek;
    }

    /**
     * @param dayOutOfWeek the dayOutOfWeek to set
     */
    public void setDayOutOfWeek(String dayOutOfWeek) {
        this.dayOutOfWeek = dayOutOfWeek;
    }

    /**
     * @return the sameDay
     */
    public Boolean getSameDay() {
        return sameDay;
    }

    /**
     * @param sameDay the sameDay to set
     */
    public void setSameDay(Boolean sameDay) {
        this.sameDay = sameDay;
    }

    /**
     * @return the interScheduleId
     */
    public Long getInterScheduleId() {
        return interScheduleId;
    }

    /**
     * @param interScheduleId the interScheduleId to set
     */
    public void setInterScheduleId(Long interScheduleId) {
        this.interScheduleId = interScheduleId;
    }

    /**
     * Get Schedule Identification mapped with Schedule Entity.
     *
     * @return the scheduleIdentification
     */
    public Long getScheduleIdentification() {
        return scheduleIdentification;
    }

    /**
     * Set Schedule Identification mapped with Schedule Entity.
     *
     * @param scheduleIdentification the scheduleIdentification to set
     */
    public void setScheduleIdentification(Long scheduleIdentification) {
        this.scheduleIdentification = scheduleIdentification;
    }

    /**
     * @return the dayOfWeekAsInt
     */
    public int getDayOutOfWeekAsInt() {
        if ("MON".equalsIgnoreCase(dayOutOfWeek)) {
			return 1;
		} else if ("TUE".equalsIgnoreCase(dayOutOfWeek)) {
			return 2;
		} else if ("WED".equalsIgnoreCase(dayOutOfWeek)) {
			return 3;
		} else if ("THU".equalsIgnoreCase(dayOutOfWeek)) {
			return 4;
		} else if ("FRI".equalsIgnoreCase(dayOutOfWeek)) {
			return 5;
		} else if ("SAT".equalsIgnoreCase(dayOutOfWeek)) {
			return 6;
		} else if ("SUN".equalsIgnoreCase(dayOutOfWeek)) {
			return 7;
		}

        return 0;
    }

    /**
     * find the no. of days between dayOfWeek and dayOutOfWeek
     *
     * @return int
     */
    public int getNoOfDays() {
        if (sameDay) {
            return 1;
        } else if (getDayOfWeekAsInt() >= getDayOutOfWeekAsInt()) {
            return (7 - getDayOfWeekAsInt()) + getDayOutOfWeekAsInt() + 1;
        } else {
            return Math.abs(getDayOfWeekAsInt() - getDayOutOfWeekAsInt()) + 1;
        }
    }

    /**
     * This method return the hours spend in a day in same day schedule
     *
     * @return int
     */
    public int getHoursInSameDay() {
        if (sameDay) {
            return Math.abs((int) (endTime.hour() - startTime.hour()));
        } else {
            return 0;
        }
    }

    /**
     * This method gets start time in HH:ss format
     *
     * @return String
     */
    public String getStartTimeAsHourMin() {
        return String.format("%02d:%02d", startTime.hour, startTime.minute);
    }

    /**
     * This method gets end time in HH:ss format
     *
     * @return String
     */
    public String getEndTimeAsHourMin() {
        return String.format("%02d:%02d", startTime.hour, startTime.minute);
    }

    /**
     * @return the dayOfWeekAsInt
     */
    public int getDayOfWeekAsInt() {
        if ("MON".equalsIgnoreCase(dayOfWeek)) {
			return 1;
		} else if ("TUE".equalsIgnoreCase(dayOfWeek)) {
			return 2;
		} else if ("WED".equalsIgnoreCase(dayOfWeek)) {
			return 3;
		} else if ("THU".equalsIgnoreCase(dayOfWeek)) {
			return 4;
		} else if ("FRI".equalsIgnoreCase(dayOfWeek)) {
			return 5;
		} else if ("SAT".equalsIgnoreCase(dayOfWeek)) {
			return 6;
		} else if ("SUN".equalsIgnoreCase(dayOfWeek)) {
			return 7;
		}

        return 0;
    }

    /**
     * This method return the difference between dayin and dayout
     *
     * @return int
     */
    public int getDiffBetweenDayInDayOut() {
        if (sameDay) {
            return 0;
        } else if (getDayOfWeekAsInt() >= getDayOutOfWeekAsInt()) {
            return (7 - getDayOfWeekAsInt()) + getDayOutOfWeekAsInt() + 1;
        } else {
            return Math.abs(getDayOfWeekAsInt() - getDayOutOfWeekAsInt()) + 1;
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dayOfWeek == null) ? 0 : dayOfWeek.hashCode());
        result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
        result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
        result = prime * result + ((locationId == null) ? 0 : locationId.hashCode());
        result = prime * result + ((dayOutOfWeek == null) ? 0 : dayOutOfWeek.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        IntermittentScheduleType other = (IntermittentScheduleType) obj;
        if (dayOfWeek == null) {
            if (other.dayOfWeek != null) {
				return false;
			}
        } else if (!dayOfWeek.equals(other.dayOfWeek)) {
			return false;
		}
        if (endTime == null) {
            if (other.endTime != null) {
				return false;
			}
        } else if (!endTime.equals(other.endTime)) {
			return false;
		}
        if (startTime == null) {
            if (other.startTime != null) {
				return false;
			}
        } else if (!startTime.equals(other.startTime)) {
			return false;
		}
        if (locationId == null) {
            if (other.locationId != null) {
				return false;
			}
        } else if (!locationId.equals(other.locationId)) {
			return false;
		}
        if (dayOutOfWeek == null) {
            if (other.dayOutOfWeek != null) {
				return false;
			}
        } else if (!dayOutOfWeek.equals(other.dayOutOfWeek)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "IntermittentScheduleType [dayOfWeek=" + dayOfWeek + ", startTime=" + startTime + ", endTime=" + endTime + ",reportingLocation=" + this.locationId
                + ",dayOut=" + this.dayOutOfWeek + "]";
    }

    /**
     * Inner Class
     */
    public class TimeValue implements Serializable, Comparable<TimeValue> {

        private static final long serialVersionUID = -737931375114572739L;

        private final Long hour, minute, second;

        public TimeValue(Long hour, Long minute, Long second) {
            super();
            if (!(hour != null && hour >= 0 && hour < 24)) {
				throw new AssertionError("Input was: " + hour == null ? "null" : hour + "to make hour: " + hour);
			}
            this.hour = hour;

            if (!(minute != null && minute >= 0 && minute < 60)) {
				throw new AssertionError("Input was: " + minute == null ? "null" : minute + "to make minute: " + minute);
			}
            this.minute = minute;

            if (!(second != null && second >= 0 && second < 60)) {
				throw new AssertionError("Input was: " + second == null ? "null" : second + "to make second: " + second);
			}
            this.second = second == null ? 0L : second;
        }

        /**
         * The hour in the range 0 through 24.
         *
         * @return the hour
         */
        public Long hour() {
            return hour;
        }

        /**
         * The minute in the range 0 through 59.
         *
         * @return the minute
         */
        public Long minute() {
            return minute;
        }

        /**
         * The second in the range 0 through 59.
         *
         * @return the second
         */
        public Long second() {
            return second;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((hour == null) ? 0 : hour.hashCode());
            result = prime * result + ((minute == null) ? 0 : minute.hashCode());
            result = prime * result + ((second == null) ? 0 : second.hashCode());
            return result;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
				return true;
			}
            if (obj == null) {
				return false;
			}
            if (getClass() != obj.getClass()) {
				return false;
			}
            TimeValue other = (TimeValue) obj;
            if (hour == null) {
                if (other.hour != null) {
					return false;
				}
            } else if (!hour.equals(other.hour)) {
				return false;
			}
            if (minute == null) {
                if (other.minute != null) {
					return false;
				}
            } else if (!minute.equals(other.minute)) {
				return false;
			}
            if (second == null) {
                if (other.second != null) {
					return false;
				}
            } else if (!second.equals(other.second)) {
				return false;
			}
            return true;
        }

        @Override
        public int compareTo(TimeValue o) {
            Calendar cal = Calendar.getInstance();
            cal.clear();
            cal.set(1970, 0, 1);
            cal.set(Calendar.MILLISECOND, 0);

            cal.set(Calendar.HOUR_OF_DAY, this.hour().intValue());
            cal.set(Calendar.MINUTE, this.minute().intValue());
            cal.set(Calendar.SECOND, this.second().intValue());
            Date thisDate = cal.getTime();
            // added for WOR-18000
            if (!sameDay) {
                cal.clear();
                cal.set(1970, 0, 2);
            }
            cal.set(Calendar.HOUR_OF_DAY, o.hour().intValue());
            cal.set(Calendar.MINUTE, o.minute().intValue());
            cal.set(Calendar.SECOND, o.second().intValue());
            Date otherDate = cal.getTime();

            return thisDate.compareTo(otherDate);
        }

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return String.format("%02d:%02d:%02d", hour, minute, second);
        }
    }

}
