package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * BailAmountHistoryEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdBailAmntHst 'The Bail Amount History table for Order Sentence module of Legal service'
 * @since December 21, 2012
 */
//@Audited
@Entity
@Table(name = "LEG_OrdBailAmntHst")
public class BailAmountHistoryEntity implements Serializable {

    private static final long serialVersionUID = -73743137811854275L;

    /**
     * @DbComment baHstId 'Unique system generated identifier for the Bail Amount History.'
     */
    @Id
    @Column(name = "baHstId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDBAILAMNTHST_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDBAILAMNTHST_ID", sequenceName = "SEQ_LEG_ORDBAILAMNTHST_ID", allocationSize = 1)
    private Long historyId;

    /**
     * @DbComment bailAmntId 'Unique identification of a bail amount instance'
     */
    @Column(name = "bailAmntId", nullable = false)
    private Long bailAmountId;

    /**
     * @DbComment bailType 'The type of bail i.e., cash, surety, property etc.,'
     */
    @Column(name = "bailType", nullable = false)
    private String bailType;

    /**
     * @DbComment bailAmount 'The amount of bail set for each type'
     */
    @Column(name = "bailAmount", nullable = false, precision = 12, scale = 2)
    private BigDecimal bailAmount;

    @ForeignKey(name = "Fk_LEG_OrdBailAmntHst1")
    @OneToMany(mappedBy = "bailAmount", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<OrderChargeAssocHistEntity> chargeAssociations;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment hstId 'Unique identification of a bail instance, foreign key to LEG_ORDSTNBAIL table'
     */
    @ForeignKey(name = "Fk_LEG_OrdBailAmntHst2")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hstId", nullable = false)
    private BailHistoryEntity bail;

    public BailAmountHistoryEntity() {
        super();
    }

    public BailAmountHistoryEntity(Long historyId, Long bailAmountId, String bailType, BigDecimal bailAmount, Set<OrderChargeAssocHistEntity> chargeAssociations,
            StampEntity stamp) {
        super();
        this.historyId = historyId;
        this.bailAmountId = bailAmountId;
        this.bailType = bailType;
        this.bailAmount = bailAmount;
        this.chargeAssociations = chargeAssociations;
        this.stamp = stamp;
    }

    /**
     * @return the historyId
     */
    public Long getHistoryId() {
        return historyId;
    }

    /**
     * @param historyId the historyId to set
     */
    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    /**
     * @return the bailAmountId
     */
    public Long getBailAmountId() {
        return bailAmountId;
    }

    /**
     * @param bailAmountId the bailAmountId to set
     */
    public void setBailAmountId(Long bailAmountId) {
        this.bailAmountId = bailAmountId;
    }

    /**
     * @return the bailType
     */
    public String getBailType() {
        return bailType;
    }

    /**
     * @param bailType the bailType to set
     */
    public void setBailType(String bailType) {
        this.bailType = bailType;
    }

    /**
     * @return the bailAmount
     */
    public BigDecimal getBailAmount() {
        return bailAmount;
    }

    /**
     * @param bailAmount the bailAmount to set
     */
    public void setBailAmount(BigDecimal bailAmount) {
        this.bailAmount = bailAmount;
    }

    /**
     * @return the chargeAssociations
     */
    public Set<OrderChargeAssocHistEntity> getChargeAssociations() {
        if (chargeAssociations == null) {
			chargeAssociations = new HashSet<OrderChargeAssocHistEntity>();
		}
        return chargeAssociations;
    }

    /**
     * @param chargeAssociations the chargeAssociations to set
     */
    public void setChargeAssociations(Set<OrderChargeAssocHistEntity> chargeAssociations) {
        if (chargeAssociations != null) {
            for (OrderChargeAssocHistEntity chg : chargeAssociations) {
                chg.setBailAmount(this);
            }
            this.chargeAssociations = chargeAssociations;
        } else {
			this.chargeAssociations = new HashSet<OrderChargeAssocHistEntity>();
		}
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the bail
     */
    public BailHistoryEntity getBail() {
        return bail;
    }

    /**
     * @param bail the bail to set
     */
    public void setBail(BailHistoryEntity bail) {
        this.bail = bail;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bailAmountId == null) ? 0 : bailAmountId.hashCode());
        result = prime * result + ((bailType == null) ? 0 : bailType.hashCode());
        result = prime * result + ((chargeAssociations == null) ? 0 : chargeAssociations.hashCode());
        result = prime * result + ((historyId == null) ? 0 : historyId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        BailAmountHistoryEntity other = (BailAmountHistoryEntity) obj;
        if (bailAmountId == null) {
            if (other.bailAmountId != null) {
				return false;
			}
        } else if (!bailAmountId.equals(other.bailAmountId)) {
			return false;
		}
        if (bailType == null) {
            if (other.bailType != null) {
				return false;
			}
        } else if (!bailType.equals(other.bailType)) {
			return false;
		}
        if (chargeAssociations == null) {
            if (other.chargeAssociations != null) {
				return false;
			}
        } else if (!chargeAssociations.equals(other.chargeAssociations)) {
			return false;
		}
        if (historyId == null) {
            if (other.historyId != null) {
				return false;
			}
        } else if (!historyId.equals(other.historyId)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "BailAmountHistoryEntity [historyId=" + historyId + ", bailAmountId=" + bailAmountId + ", bailType=" + bailType + ", bailAmount=" + bailAmount
                + ", chargeAssociations=" + chargeAssociations + "]";
    }

}
