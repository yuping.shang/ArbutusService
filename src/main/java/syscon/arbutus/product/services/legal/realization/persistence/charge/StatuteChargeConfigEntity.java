package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the statute charge configuration entity.
 *
 * @DbComment LEG_CHGStatChgConfig 'The statute charge configuration data table'
 */
@Audited
@Entity
@Table(name = "LEG_CHGStatChgConfig")
public class StatuteChargeConfigEntity implements Serializable {

    private static final long serialVersionUID = 60608318346442678L;

    /**
     * @DbComment StatuteChargeId 'The unique ID for each statute charge created in the system.'
     */
    @Id
    @Column(name = "StatuteChargeId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGStatChgConfig_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGStatChgConfig_Id", sequenceName = "SEQ_LEG_CHGStatChgConfig_Id", allocationSize = 1)
    private Long statuteChargeId;

    /**
     * @DbComment StatuteId 'The statute unique Id of the Statute Configuration table'
     */
    @Column(name = "StatuteId", nullable = false)
    private Long statuteId;

    /**
     * @DbComment ChargeCode 'Charge code defined for a statute jurisdiction.'
     */
    @MetaCode(set = MetaSet.CHARGE_CODE)
    @Column(name = "ChargeCode", nullable = false, length = 64)
    private String chargeCode;

    /**
     * @DbComment ChargeType 'The type of a charge (Felony, Misdemeanor)'
     */
    @MetaCode(set = MetaSet.TYPE_OF_CHARGE)
    @Column(name = "ChargeType", nullable = false, length = 64)
    private String chargeType;

    /**
     * @DbComment ChargeDegree 'The degree of a charge'
     */
    @MetaCode(set = MetaSet.CHARGE_DEGREE)
    @Column(name = "ChargeDegree", nullable = true, length = 64)
    private String chargeDegree;

    /**
     * @DbComment ChargeCategory 'The category the charge belongs to'
     */
    @MetaCode(set = MetaSet.CHARGE_CATEGORY)
    @Column(name = "ChargeCategory", nullable = true, length = 64)
    private String chargeCategory;

    /**
     * @DbComment ChargeSeverity 'The severity of a charge'
     */
    @MetaCode(set = MetaSet.CHARGE_SEVERITY)
    @Column(name = "ChargeSeverity", nullable = true, length = 64)
    private String chargeSeverity;

    /**
     * @DbComment BailAmount 'The bail amount recommended by the statute charge code.'
     */
    @Column(name = "BailAmount", nullable = true)
    private Long bailAmount;

    /**
     * @DbComment BailAllowed 'True if bail allowed is allowed for this charge code, false otherwise, default is false'
     */
    @Column(name = "BailAllowed", nullable = false)
    private Boolean bailAllowed;

    /**
     * @DbComment BondAllowed 'True if bond is allowed for this charge, false otherwise, default is false'
     */
    @Column(name = "BondAllowed", nullable = false)
    private Boolean bondAllowed;

    /**
     * @DbComment StartDate 'Date on which this charge code was validated.'
     */
    @Column(name = "StartDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    /**
     * @DbComment EndDate 'Date on which the charge code was invalidated.'
     */
    @Column(name = "EndDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    /**
     * @DbComment StatuteChargeTextId 'Foreign key from table LEG_CHGStatChgTxtConfig'
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "StatuteChargeTextId")
    private StatuteChargeTextEntity statuteChargeText;

    /**
     * @DbComment StatuteChargeTextId 'Foreign key from table LEG_CHGStatChgTxtConfig'
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "StatuteId", insertable = false, updatable = false)
    private StatuteConfigEntity statuteConfig;

    //externalChargeCodeIds
    /**
     * @DbComment LEG_CHGStatChgExtCode. 'the external charge code data table of status charge configuration data record.'
     * @DbComment LEG_CHGStatChgExtCode.StatuteChargeId 'Foreign key from LEG_CHGStatChgConfig'
     * @DbComment LEG_CHGStatChgExtCode.ExternalChargeCodeId 'The External charge code (NCIC, UCR) for the statute charge code, reference to the External Charge Code Configuration data table'
     */
    @ElementCollection
    @JoinTable(name = "LEG_CHGStatChgExtCode",
            joinColumns = @JoinColumn(name = "StatuteChargeId"))
    @Column(name = "ExternalChargeCodeId")
    private Set<Long> externalChargeCodeIds;

    @OneToMany(mappedBy = "statueChargeConfig", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<StatuteChargeIndicatorEntity> chargeIndicators;

    /**
     * @DbComment LEG_CHGStatChgEnhancing. 'the charge enhancing factor data table of status charge configuration data record.'
     * @DbComment LEG_CHGStatChgEnhancing.StatuteChargeId 'Foreign key from LEG_CHGStatChgConfig'
     * @DbComment LEG_CHGStatChgEnhancing.ChargeEnhancingFactor 'Factor or reason that makes a charge more serious.'
     */
    @ElementCollection
    @JoinTable(name = "LEG_CHGStatChgEnhancing",
            joinColumns = @JoinColumn(name = "StatuteChargeId"))
    @Column(name = "ChargeEnhancingFactor", length = 64)
    @MetaCode(set = MetaSet.CHARGE_ENHANCING_FACTOR)
    private Set<String> chargeEnhancingFactors;

    /**
     * @DbComment LEG_CHGStatChgReducing. 'the charge reducing factor data table of status charge configuration data record.'
     * @DbComment LEG_CHGStatChgReducing.StatuteChargeId 'Foreign key from LEG_CHGStatChgConfig'
     * @DbComment LEG_CHGStatChgReducing.ChargeReducingFactor 'A factor which make a charge less serious.'
     */
    @ElementCollection
    @JoinTable(name = "LEG_CHGStatChgReducing",
            joinColumns = @JoinColumn(name = "StatuteChargeId"))
    @Column(name = "ChargeReducingFactor", length = 64)
    @MetaCode(set = MetaSet.CHARGE_REDUCING_FACTOR)
    private Set<String> chargeReducingFactors;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     *
     */
    public StatuteChargeConfigEntity() {
    }

    /**
     * @param statuteChargeId
     * @param statuteId
     * @param chargeCode
     * @param chargeType
     * @param chargeDegree
     * @param chargeCategory
     * @param chargeSeverity
     * @param bailAmount
     * @param bailAllowed
     * @param bondAllowed
     * @param startDate
     * @param endDate
     * @param statuteChargeText
     * @param externalChargeCodeIds
     * @param chargeIndicators
     * @param chargeEnhancingFactors
     * @param chargeReducingFactors
     */
    public StatuteChargeConfigEntity(Long statuteChargeId, Long statuteId, String chargeCode, String chargeType, String chargeDegree, String chargeCategory,
            String chargeSeverity, Long bailAmount, Boolean bailAllowed, Boolean bondAllowed, Date startDate, Date endDate, StatuteChargeTextEntity statuteChargeText,
            Set<Long> externalChargeCodeIds, Set<StatuteChargeIndicatorEntity> chargeIndicators, Set<String> chargeEnhancingFactors, Set<String> chargeReducingFactors) {
        this.statuteChargeId = statuteChargeId;
        this.statuteId = statuteId;
        this.chargeCode = chargeCode;
        this.chargeType = chargeType;
        this.chargeDegree = chargeDegree;
        this.chargeCategory = chargeCategory;
        this.chargeSeverity = chargeSeverity;
        this.bailAmount = bailAmount;
        this.bailAllowed = bailAllowed;
        this.bondAllowed = bondAllowed;
        this.startDate = startDate;
        this.endDate = endDate;
        this.statuteChargeText = statuteChargeText;
        this.externalChargeCodeIds = externalChargeCodeIds;
        //this.chargeIndicators = chargeIndicators;
        setChargeIndicators(chargeIndicators); //to build relationship with main instance

        this.chargeEnhancingFactors = chargeEnhancingFactors;
        this.chargeReducingFactors = chargeReducingFactors;
    }

    /**
     * @return the statuteChargeId
     */
    public Long getStatuteChargeId() {
        return statuteChargeId;
    }

    /**
     * @param statuteChargeId the statuteChargeId to set
     */
    public void setStatuteChargeId(Long statuteChargeId) {
        this.statuteChargeId = statuteChargeId;
    }

    /**
     * @return the statuteId
     */
    public Long getStatuteId() {
        return statuteId;
    }

    /**
     * @param statuteId the statuteId to set
     */
    public void setStatuteId(Long statuteId) {
        this.statuteId = statuteId;
    }

    /**
     * @return the chargeCode
     */
    public String getChargeCode() {
        return chargeCode;
    }

    /**
     * @param chargeCode the chargeCode to set
     */
    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    /**
     * @return the chargeType
     */
    public String getChargeType() {
        return chargeType;
    }

    /**
     * @param chargeType the chargeType to set
     */
    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    /**
     * @return the chargeDegree
     */
    public String getChargeDegree() {
        return chargeDegree;
    }

    /**
     * @param chargeDegree the chargeDegree to set
     */
    public void setChargeDegree(String chargeDegree) {
        this.chargeDegree = chargeDegree;
    }

    /**
     * @return the chargeCategory
     */
    public String getChargeCategory() {
        return chargeCategory;
    }

    /**
     * @param chargeCategory the chargeCategory to set
     */
    public void setChargeCategory(String chargeCategory) {
        this.chargeCategory = chargeCategory;
    }

    /**
     * @return the chargeSeverity
     */
    public String getChargeSeverity() {
        return chargeSeverity;
    }

    /**
     * @param chargeSeverity the chargeSeverity to set
     */
    public void setChargeSeverity(String chargeSeverity) {
        this.chargeSeverity = chargeSeverity;
    }

    /**
     * @return the bailAmount
     */
    public Long getBailAmount() {
        return bailAmount;
    }

    /**
     * @param bailAmount the bailAmount to set
     */
    public void setBailAmount(Long bailAmount) {
        this.bailAmount = bailAmount;
    }

    /**
     * @return the bailAllowed
     */
    public Boolean isBailAllowed() {
        return bailAllowed;
    }

    /**
     * @param bailAllowed the bailAllowed to set
     */
    public void setBailAllowed(Boolean bailAllowed) {
        this.bailAllowed = bailAllowed;
    }

    /**
     * @return the bondAllowed
     */
    public Boolean isBondAllowed() {
        return bondAllowed;
    }

    /**
     * @param bondAllowed the bondAllowed to set
     */
    public void setBondAllowed(Boolean bondAllowed) {
        this.bondAllowed = bondAllowed;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the statuteChargeText
     */
    public StatuteChargeTextEntity getStatuteChargeText() {
        return statuteChargeText;
    }

    /**
     * @param statuteChargeText the statuteChargeText to set
     */
    public void setStatuteChargeText(StatuteChargeTextEntity statuteChargeText) {
        this.statuteChargeText = statuteChargeText;
    }

    /**
     * @return the externalChargeCodeIds
     */
    public Set<Long> getExternalChargeCodeIds() {
        if (externalChargeCodeIds == null) {
			externalChargeCodeIds = new HashSet<Long>();
		}
        return externalChargeCodeIds;
    }

    /**
     * @param externalChargeCodeIds the externalChargeCodeIds to set
     */
    public void setExternalChargeCodeIds(Set<Long> externalChargeCodeIds) {
        this.externalChargeCodeIds = externalChargeCodeIds;
    }

    /**
     * @return the chargeIndicators
     */
    public Set<StatuteChargeIndicatorEntity> getChargeIndicators() {
        if (chargeIndicators == null) {
			chargeIndicators = new HashSet<StatuteChargeIndicatorEntity>();
		}
        return chargeIndicators;
    }

    /**
     * @param chargeIndicators the chargeIndicators to set
     */
    public void setChargeIndicators(Set<StatuteChargeIndicatorEntity> chargeIndicators) {
        if (chargeIndicators != null) {
            for (StatuteChargeIndicatorEntity subEntity : chargeIndicators) {
                subEntity.setStatueChargeConfig(this);
            }
        }
        this.chargeIndicators = chargeIndicators;
    }

    /**
     * @param entity
     */
    public void addChargeIndicator(StatuteChargeIndicatorEntity entity) {
        entity.setStatueChargeConfig(this);
        getChargeIndicators().add(entity);
    }

    /**
     * @return the chargeEnhancingFactors
     */
    public Set<String> getChargeEnhancingFactors() {
        if (chargeEnhancingFactors == null) {
			chargeEnhancingFactors = new HashSet<String>();
		}
        return chargeEnhancingFactors;
    }

    /**
     * @param chargeEnhancingFactors the chargeEnhancingFactors to set
     */
    public void setChargeEnhancingFactors(Set<String> chargeEnhancingFactors) {
        this.chargeEnhancingFactors = chargeEnhancingFactors;
    }

    /**
     * @return the chargeReducingFactors
     */
    public Set<String> getChargeReducingFactors() {
        if (chargeReducingFactors == null) {
			chargeReducingFactors = new HashSet<String>();
		}
        return chargeReducingFactors;
    }

    /**
     * @param chargeReducingFactors the chargeReducingFactors to set
     */
    public void setChargeReducingFactors(Set<String> chargeReducingFactors) {
        this.chargeReducingFactors = chargeReducingFactors;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StatuteChargeConfigEntity [statuteChargeId=" + statuteChargeId + ", statuteId=" + statuteId + ", chargeCode=" + chargeCode + ", chargeType=" + chargeType
                + ", chargeDegree=" + chargeDegree + ", chargeCategory=" + chargeCategory + ", chargeSeverity=" + chargeSeverity + ", bailAmount=" + bailAmount
                + ", bailAllowed=" + bailAllowed + ", bondAllowed=" + bondAllowed + ", startDate=" + startDate + ", endDate=" + endDate + ", statuteChargeText="
                + statuteChargeText + ", externalChargeCodeIds=" + externalChargeCodeIds + ", chargeIndicators=" + chargeIndicators + ", chargeEnhancingFactors="
                + chargeEnhancingFactors + ", chargeReducingFactors=" + chargeReducingFactors + "]";
    }

    public StatuteConfigEntity getStatuteConfig() {
        return statuteConfig;
    }

    public void setStatuteConfig(StatuteConfigEntity statuteConfig) {
        this.statuteConfig = statuteConfig;
    }

}
