/**
 *
 */
package syscon.arbutus.product.services.legal.contract.dto.caseaffiliation;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * The representation of the case affiliations search type.
 *
 * @author lhan
 */
public class CaseAffiliationSearchType implements Serializable {

    private static final long serialVersionUID = -6722227481919980449L;

    private Long affiliatedPersonIdentityId;
    private Long affiliatedCaseId;
    private Long affiliatedOrganizationId;
    private String caseAffiliationCategory;
    private String caseAffiliationType;
    private String caseAffiliationRole;
    private Date startDateFrom;
    private Date startDateTo;
    private Date endDateFrom;
    private Date endDateTo;
    private Boolean isActiveFlag;
    private String comment;
    private Set<Long> affiliatedCaseActivityIds;
    private String SetMatchMode;

    /**
     * Default Constructor
     */
    public CaseAffiliationSearchType() {
        super();
    }

    /**
     * Constructor
     * At least one of the search criteria must be specified.
     *
     * @param affiliatedPersonIdentityId Long - Optional. The affiliation is to  an existing  person identity who can belong or not belong to an organization involved in a case.
     * @param affiliatedCaseId           Long - Optional. The affiliation is to  an existing  case.
     * @param affiliatedOrganizationId   Long - Optional. The affiliation to an organization
     * @param caseAffiliationCategory    String - Optional. Indicates whether this is an affiliation to an organization, person identity within the organization or just a person identity
     * @param caseAffiliationType        String - Optional. Indicates what type of affiliation to a case this is eg. legal, probationary
     * @param caseAffiliationRole        String - Optional. Indicates what role this affiliation plays in the case eg. witness
     * @param startDateFrom              Date - Optional. A date when the affiliation startDate from.
     * @param startDateTo                Date - Optional. A date when the affiliation startDate to.
     * @param endDateFrom                Date - Optional. A date when the affiliation endDate from.
     * @param endDateTo                  Date - Optional. A date when the affiliation endDate to.
     * @param isActiveFlag               boolean - Optional. Indicates if the affiliation is Active or Inactive (derived)
     * @param comment                    String - Optional. any comment related to the affiliation
     * @param affiliatedCaseActivityIds  Set<Long> - Optional. affiliated case activities's IDs.
     * @param setMatchMode               String - Optional. Default = "ALL".
     */
    public CaseAffiliationSearchType(Long affiliatedPersonIdentityId, Long affiliatedCaseId, Long affiliatedOrganizationId, String caseAffiliationCategory,
            String caseAffiliationType, String caseAffiliationRole, Date startDateFrom, Date startDateTo, Date endDateFrom, Date endDateTo, Boolean isActiveFlag,
            String comment, Set<Long> affiliatedCaseActivityIds, String setMatchMode) {
        super();
        this.affiliatedPersonIdentityId = affiliatedPersonIdentityId;
        this.affiliatedCaseId = affiliatedCaseId;
        this.affiliatedOrganizationId = affiliatedOrganizationId;
        this.caseAffiliationCategory = caseAffiliationCategory;
        this.caseAffiliationType = caseAffiliationType;
        this.caseAffiliationRole = caseAffiliationRole;
        this.startDateFrom = startDateFrom;
        this.startDateTo = startDateTo;
        this.endDateFrom = endDateFrom;
        this.endDateTo = endDateTo;
        this.isActiveFlag = isActiveFlag;
        this.comment = comment;
        this.affiliatedCaseActivityIds = affiliatedCaseActivityIds;
        SetMatchMode = setMatchMode;
    }

    /**
     * @return the affiliatedPersonIdentityId
     */
    public Long getAffiliatedPersonIdentityId() {
        return affiliatedPersonIdentityId;
    }

    /**
     * @param affiliatedPersonIdentityId the affiliatedPersonIdentityId to set
     */
    public void setAffiliatedPersonIdentityId(Long affiliatedPersonIdentityId) {
        this.affiliatedPersonIdentityId = affiliatedPersonIdentityId;
    }

    /**
     * @return the affiliatedCaseId
     */
    public Long getAffiliatedCaseId() {
        return affiliatedCaseId;
    }

    /**
     * @param affiliatedCaseId the affiliatedCaseId to set
     */
    public void setAffiliatedCaseId(Long affiliatedCaseId) {
        this.affiliatedCaseId = affiliatedCaseId;
    }

    /**
     * @return the affiliatedOrganizationId
     */
    public Long getAffiliatedOrganizationId() {
        return affiliatedOrganizationId;
    }

    /**
     * @param affiliatedOrganizationId the affiliatedOrganizationId to set
     */
    public void setAffiliatedOrganizationId(Long affiliatedOrganizationId) {
        this.affiliatedOrganizationId = affiliatedOrganizationId;
    }

    /**
     * @return the caseAffiliationCategory
     */
    public String getCaseAffiliationCategory() {
        return caseAffiliationCategory;
    }

    /**
     * @param caseAffiliationCategory the caseAffiliationCategory to set
     */
    public void setCaseAffiliationCategory(String caseAffiliationCategory) {
        this.caseAffiliationCategory = caseAffiliationCategory;
    }

    /**
     * @return the caseAffiliationType
     */
    public String getCaseAffiliationType() {
        return caseAffiliationType;
    }

    /**
     * @param caseAffiliationType the caseAffiliationType to set
     */
    public void setCaseAffiliationType(String caseAffiliationType) {
        this.caseAffiliationType = caseAffiliationType;
    }

    /**
     * @return the caseAffiliationRole
     */
    public String getCaseAffiliationRole() {
        return caseAffiliationRole;
    }

    /**
     * @param caseAffiliationRole the caseAffiliationRole to set
     */
    public void setCaseAffiliationRole(String caseAffiliationRole) {
        this.caseAffiliationRole = caseAffiliationRole;
    }

    /**
     * @return the startDateFrom
     */
    public Date getStartDateFrom() {
        return startDateFrom;
    }

    /**
     * @param startDateFrom the startDateFrom to set
     */
    public void setStartDateFrom(Date startDateFrom) {
        this.startDateFrom = startDateFrom;
    }

    /**
     * @return the startDateTo
     */
    public Date getStartDateTo() {
        return startDateTo;
    }

    /**
     * @param startDateTo the startDateTo to set
     */
    public void setStartDateTo(Date startDateTo) {
        this.startDateTo = startDateTo;
    }

    /**
     * @return the endDateFrom
     */
    public Date getEndDateFrom() {
        return endDateFrom;
    }

    /**
     * @param endDateFrom the endDateFrom to set
     */
    public void setEndDateFrom(Date endDateFrom) {
        this.endDateFrom = endDateFrom;
    }

    /**
     * @return the endDateTo
     */
    public Date getEndDateTo() {
        return endDateTo;
    }

    /**
     * @param endDateTo the endDateTo to set
     */
    public void setEndDateTo(Date endDateTo) {
        this.endDateTo = endDateTo;
    }

    /**
     * @return the isActiveFlag
     */
    public Boolean getIsActiveFlag() {
        return isActiveFlag;
    }

    /**
     * @param isActiveFlag the isActiveFlag to set
     */
    public void setIsActiveFlag(Boolean isActiveFlag) {
        this.isActiveFlag = isActiveFlag;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the affiliatedCaseActivityIds
     */
    public Set<Long> getAffiliatedCaseActivityIds() {
        return affiliatedCaseActivityIds;
    }

    /**
     * @param affiliatedCaseActivityIds the affiliatedCaseActivityIds to set
     */
    public void setAffiliatedCaseActivityIds(Set<Long> affiliatedCaseActivityIds) {
        this.affiliatedCaseActivityIds = affiliatedCaseActivityIds;
    }

    /**
     * @return the setMatchMode
     */
    public String getSetMatchMode() {
        return SetMatchMode;
    }

    /**
     * @param setMatchMode the setMatchMode to set
     */
    public void setSetMatchMode(String setMatchMode) {
        SetMatchMode = setMatchMode;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseAffiliationSearchType [getAffiliatedPersonIdentityId()=" + getAffiliatedPersonIdentityId() + ", getAffiliatedCaseId()=" + getAffiliatedCaseId()
                + ", getAffiliatedOrganizationId()=" + getAffiliatedOrganizationId() + ", getCaseAffiliationCategory()=" + getCaseAffiliationCategory()
                + ", getCaseAffiliationType()=" + getCaseAffiliationType() + ", getCaseAffiliationRole()=" + getCaseAffiliationRole() + ", getStartDateFrom()="
                + getStartDateFrom() + ", getStartDateTo()=" + getStartDateTo() + ", getEndDateFrom()=" + getEndDateFrom() + ", getEndDateTo()=" + getEndDateTo()
                + ", getIsActiveFlag()=" + getIsActiveFlag() + ", getComment()=" + getComment() + ", getAffiliatedCaseActivityIds()=" + getAffiliatedCaseActivityIds()
                + ", getSetMatchMode()=" + getSetMatchMode() + "]";
    }

}
