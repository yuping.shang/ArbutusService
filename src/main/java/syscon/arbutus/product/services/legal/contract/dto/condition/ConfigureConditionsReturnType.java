package syscon.arbutus.product.services.legal.contract.dto.condition;

import java.io.Serializable;
import java.util.List;

/**
 * ConfigureConditionsReturnType for Configure Condition of Case Management Service
 *
 * @author amlendu kumar
 * @since May 22, 2014
 */
public class ConfigureConditionsReturnType implements Serializable {

    private static final long serialVersionUID = -3111509509822270559L;
    private List<ConfigureConditionType> configureConditions;
    private Long totalSize;

    /**
     * @param configureConditions
     * @param totalSize
     */
    public ConfigureConditionsReturnType(Long totalSize, List<ConfigureConditionType> configureConditions) {
        super();
        this.configureConditions = configureConditions;
        this.totalSize = totalSize;
    }

    /**
     * Gets the list of Configure Condition.
     *
     * @return the configureConditions
     */
    public List<ConfigureConditionType> getConfigureConditions() {
        return configureConditions;
    }

    /**
     * Sets the list of Configure Condition.
     *
     * @param configureConditions the configureConditions to set
     */
    public void setConfigureConditions(List<ConfigureConditionType> configureConditions) {
        this.configureConditions = configureConditions;
    }

    /**
     * Gets the value of the totalSize property.
     *
     * @return the total size of search result
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * Sets the value of the totalSize property.
     *
     * @param totalSize the total size of search result
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ConfigureConditionsReturnType [configureConditions=" + configureConditions + ", totalSize=" + totalSize + "]";
    }

}
