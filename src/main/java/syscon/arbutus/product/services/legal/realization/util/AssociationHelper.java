package syscon.arbutus.product.services.legal.realization.util;

import syscon.arbutus.product.services.core.contract.dto.AssociationType;
import syscon.arbutus.product.services.legal.contract.dto.ValidationUtil;

/**
 * This helper is provided for handling common associations related functions of case management service
 *
 * @author yshang, byu, lhan
 */
public class AssociationHelper {

    /**
     * Return an {@link AssociationType}.
     *
     * @param toClass
     * @param toIdentifier
     * @return
     */
    public static AssociationType toAssociationType(String toClass, Long toIdentifier) {

        if (ValidationUtil.isEmpty(toClass) || ValidationUtil.isEmpty(toIdentifier)) {
			return null;
		}

        AssociationType ret = new AssociationType(toClass, toIdentifier);

        return ret;
    }

    /**
     * Get toIdentifier value from AssociationType.
     *
     * @param toClass
     * @param toIdentifier
     * @return
     */
    public static Long getToIdentifier(AssociationType association) {

        if (ValidationUtil.isEmpty(association)) {
			return null;
		}

        return association.getToIdentifier();

    }
}
