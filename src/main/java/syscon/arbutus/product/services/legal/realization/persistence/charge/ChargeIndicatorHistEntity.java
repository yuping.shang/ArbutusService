package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the charge indicator history entity.
 *
 * @DbComment LEG_CHGIndicatorHist 'The charge indicator history data table'
 */
//@Audited
@Entity
@Table(name = "LEG_CHGIndicatorHist")
public class ChargeIndicatorHistEntity implements Serializable {

    private static final long serialVersionUID = 7991248903709399201L;

    /**
     * @DbComment IndicatorHistoryId 'The unique Id for each charge indicator history record.'
     */
    @Id
    @Column(name = "IndicatorHistoryId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGIndicatorHist_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGIndicatorHist_Id", sequenceName = "SEQ_LEG_CHGIndicatorHist_Id", allocationSize = 1)
    private Long indicatorHistoryId;

    /**
     * @DbComment IndicatorId 'The unique Id for each charge indicator record.'
     */
    @Column(name = "IndicatorId", nullable = false)
    private Long indicatorId;

    /**
     * @DbComment ChargeIndicator 'The indicators (alerts) on a charge.'
     */
    @Column(name = "ChargeIndicator", nullable = false, length = 64)
    private String chargeIndicator;

    /**
     * @DbComment HasIndicatorValue 'True if a value can be associated to an indicator, false otherwise, default is false'
     */
    @Column(name = "HasIndicatorValue", nullable = false)
    private Boolean hasIndicatorValue;

    /**
     * @DbComment IndicatorValue 'The value associated to the indicator can be specified only if the HasIndicatorValue is ‘true’. '
     */
    @Column(name = "IndicatorValue", nullable = true, length = 128)
    private String indicatorValue;

    /**
     * @DbComment ChargeHistoryId 'Unique id of a charge history instance, foreign key to LEG_CRGChargeHist table'
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ChargeHistoryId", nullable = false)
    private ChargeHistEntity chargeHistory;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     *
     */
    public ChargeIndicatorHistEntity() {
    }

    /**
     * @param indicatorHistoryId
     * @param indicatorId
     * @param chargeIndicator
     * @param hasIndicatorValue
     * @param indicatorValue
     */
    public ChargeIndicatorHistEntity(Long indicatorHistoryId, Long indicatorId, String chargeIndicator, Boolean hasIndicatorValue, String indicatorValue) {
        this.indicatorHistoryId = indicatorHistoryId;
        this.indicatorId = indicatorId;
        this.chargeIndicator = chargeIndicator;
        this.hasIndicatorValue = hasIndicatorValue;
        this.indicatorValue = indicatorValue;
    }

    /**
     * @return the indicatorHistoryId
     */
    public Long getIndicatorHistoryId() {
        return indicatorHistoryId;
    }

    /**
     * @param indicatorHistoryId the indicatorHistoryId to set
     */
    public void setIndicatorHistoryId(Long indicatorHistoryId) {
        this.indicatorHistoryId = indicatorHistoryId;
    }

    /**
     * @return the indicatorId
     */
    public Long getIndicatorId() {
        return indicatorId;
    }

    /**
     * @param indicatorId the indicatorId to set
     */
    public void setIndicatorId(Long indicatorId) {
        this.indicatorId = indicatorId;
    }

    /**
     * @return the chargeIndicator
     */
    public String getChargeIndicator() {
        return chargeIndicator;
    }

    /**
     * @param chargeIndicator the chargeIndicator to set
     */
    public void setChargeIndicator(String chargeIndicator) {
        this.chargeIndicator = chargeIndicator;
    }

    /**
     * @return the hasIndicatorValue
     */
    public Boolean getHasIndicatorValue() {
        return hasIndicatorValue;
    }

    /**
     * @param hasIndicatorValue the hasIndicatorValue to set
     */
    public void setHasIndicatorValue(Boolean hasIndicatorValue) {
        this.hasIndicatorValue = hasIndicatorValue;
    }

    /**
     * @return the indicatorValue
     */
    public String getIndicatorValue() {
        return indicatorValue;
    }

    /**
     * @param indicatorValue the indicatorValue to set
     */
    public void setIndicatorValue(String indicatorValue) {
        this.indicatorValue = indicatorValue;
    }

    /**
     * @return the chargeHistory
     */
    public ChargeHistEntity getChargeHistory() {
        return chargeHistory;
    }

    /**
     * @param chargeHistory the chargeHistory to set
     */
    public void setChargeHistory(ChargeHistEntity chargeHistory) {
        this.chargeHistory = chargeHistory;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeIndicatorHistEntity [indicatorHistoryId=" + indicatorHistoryId + ", indicatorId=" + indicatorId + ", chargeIndicator=" + chargeIndicator
                + ", hasIndicatorValue=" + hasIndicatorValue + ", indicatorValue=" + indicatorValue + ", stamp=" + stamp + "]";
    }

}
