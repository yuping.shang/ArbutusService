/**
 *
 */
package syscon.arbutus.product.services.legal.contract.dto.composite;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueType;
import syscon.arbutus.product.services.core.common.Supervision;

/**
 * The representation of the case and the related offender information.
 * * It is consumed when inquiring cases.
 *
 * @author lhan
 */
public class CaseOffenderType implements Serializable {

    private static final long serialVersionUID = 8608465236235550213L;

    private String caseIdentifierType;
    private String caseIdentifierNumber;
    private Date caseIssuedDate;
    private Long caseId;

    private Supervision supervision;

    private List<ClientCustomFieldValueType> CCFValues;

    /**
     * @return the caseIdentifierType
     */
    public String getCaseIdentifierType() {
        return caseIdentifierType;
    }

    /**
     * @param caseIdentifierType the caseIdentifierType to set
     */
    public void setCaseIdentifierType(String caseIdentifierType) {
        this.caseIdentifierType = caseIdentifierType;
    }

    /**
     * @return the caseIdentifierNumber
     */
    public String getCaseIdentifierNumber() {
        return caseIdentifierNumber;
    }

    /**
     * @param caseIdentifierNumber the caseIdentifierNumber to set
     */
    public void setCaseIdentifierNumber(String caseIdentifierNumber) {
        this.caseIdentifierNumber = caseIdentifierNumber;
    }

    /**
     * @return the caseIssuedDate
     */
    public Date getCaseIssuedDate() {
        return caseIssuedDate;
    }

    /**
     * @param caseIssuedDate the caseIssuedDate to set
     */
    public void setCaseIssuedDate(Date caseIssuedDate) {
        this.caseIssuedDate = caseIssuedDate;
    }

    /**
     * @return the supervision
     */
    public Supervision getSupervision() {
        if (supervision == null) {
            supervision = new Supervision();
        }
        return supervision;
    }

    /**
     * @param supervision the supervision to set
     */
    public void setSupervision(Supervision supervision) {
        this.supervision = supervision;
    }

    /**
     * @return the caseId
     */
    public Long getCaseId() {
        return caseId;
    }

    /**
     * @param caseId the caseId to set
     */
    public void setCaseId(Long caseId) {
        this.caseId = caseId;
    }

    /**
     * @return the cCFValues
     */
    public List<ClientCustomFieldValueType> getCCFValues() {
        return CCFValues;
    }

    /**
     * @param cCFValues the cCFValues to set
     */
    public void setCCFValues(List<ClientCustomFieldValueType> cCFValues) {
        CCFValues = cCFValues;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseOffenderType [caseIdentifierType=" + caseIdentifierType + ", caseIdentifierNumber=" + caseIdentifierNumber + ", caseIssuedDate=" + caseIssuedDate
                + ", caseId=" + caseId + ", supervision=" + supervision + ", CCFValues=" + CCFValues + "]";
    }

}
