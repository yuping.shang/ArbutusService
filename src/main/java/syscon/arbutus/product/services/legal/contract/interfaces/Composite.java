/**
 *
 */
package syscon.arbutus.product.services.legal.contract.interfaces;

import java.util.List;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.legal.contract.dto.CaseInfoType;
import syscon.arbutus.product.services.legal.contract.dto.CaseSearchType;
import syscon.arbutus.product.services.legal.contract.dto.CaseType;
import syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationType;
import syscon.arbutus.product.services.legal.contract.dto.composite.BailDetailsType;
import syscon.arbutus.product.services.legal.contract.dto.composite.CaseOffendersReturnType;
import syscon.arbutus.product.services.legal.contract.dto.composite.CourtActivityMovementType;
import syscon.arbutus.product.services.legal.contract.dto.composite.InquiryCaseSearchType;

/**
 * Business contract interface for composite actions required by client.
 *
 * @author lhan
 */
public interface Composite {
    /**
     * To create a new case or update an existing case.
     *
     * @param uc       {@link UserContext} - Required
     * @param caseType {@link CaseType} - Required
     * @return The created/updated case instance.
     * @throws ArbutusRuntimeException if the case instance cannot be created/updated.
     */
    public CaseType saveCase(UserContext uc, CaseType caseType);

    /**
     * Get a case which has case information and associated charges, orders and conditions if applicable.
     *
     * @param uc     {@link UserContext} - Required
     * @param caseId {@link Long} - Required - The unique ID for each case created in the system. This is a system generated ID.
     * @return If a case found, return the found CaseType.
     * If finding has error, throw ArbutusRuntimeException.
     */
    public CaseType getCase(UserContext uc, Long caseId);

    /**
     * Get a list of CaseInfoTypes or a list of CaseTypes based on search criteria.
     * It can find duplicated Cases based on the uniqueness of combined Supervision reference, Facility reference,
     * Case Identifier Type, and Case Identifier value.
     *
     * @param uc         {@link UserContext} - Required
     * @param searchType {@link CaseSearchType} - Required
     * @return If duplicated cases found, return a list of found Case Infos.
     * If duplicated cases not found, return null.
     * If finding has error, throw ArbutusRuntimeException.
     */
    public List<CaseInfoType> getCases(UserContext uc, CaseSearchType searchType);

    /**
     * createCourtActivityWithMovement -- Create a Court Activity and related movement data.
     * <pre>
     * A Case association must exist when creating a court activity.
     * The movement data must be provided.
     * A corresponding Activity and MovementActivity will be created and associated when creating a court activity.
     * The schedule details (date and time) will be captured in the Activity service and the other court hearing details will be captured in this service.
     * Court Movement and its Activity details are created.
     * a.	The system creates an OUT and IN court movement record using the details entered. The move date for the IN court movement record must be the same as the OUT and the move time is automatically set to the default time configured for court movements in the movement service.
     * b.	The move date and time is be populated in the PlannedStartDate of the Activity instance and the movement status is set to PENDING.
     * c.	Event and Movement records are associated.
     * </pre>
     *
     * @param uc                        UserContext - Required
     * @param courtActivityMovementType CourtActivityMovementType - Required
     * @return CourtActivityMovementType
     * Throws exception if instance cannot be created.
     */
    public CourtActivityMovementType createCourtActivityWithMovement(UserContext uc, CourtActivityMovementType courtActivityMovementType);

    /**
     * retrieveCourtActivityWithMovement -- Retrieve a Court Activity and related movement data.
     * <pre>
     * A Court Activity must exist when retrieve a court activity.
     * The movement data may or may not be retrieved.
     * </pre>
     *
     * @param uc              UserContext - Required
     * @param courtActivityId Long - Required
     * @return CourtActivityMovementType
     * Throws exception if instance cannot be created.
     */
    public CourtActivityMovementType retrieveCourtActivityWithMovement(UserContext uc, Long courtActivityId);

    /**
     * Retrieve all court activities and their movement data for a particular case.
     * <pre>
     * Provenance
     * CaseId must be specified. All events/activities related to the Case will be returned.
     * If ActivityStatus = “ACTIVE”
     *     Return all events/activities with a status of “ACTIVE”
     * If ActivityStatus = “INACTIVE”
     *     Return all events/activities with a status of “INACTIVE”
     * If ActivityStatus = null (not provided)
     *     Return all events/activities
     * </pre>
     *
     * @param uc             UserContext - Required
     * @param caseId         Long - Required
     * @param activityStatus String - Optional
     * @return List<CourtActivityMovementType>
     * throw Exception if it has error.
     * Return empty list if no record found.
     */
    public List<CourtActivityMovementType> retrieveCourtActivityWithMovements(UserContext uc, Long caseId, String activityStatus);

    /**
     * updateCourtActivityWithMovement -- Update a Court Activity and related movement data.
     * <pre>
     * A Court Activity must exist when update a court activity and related movement data..
     * The movement data must be provided.
     * If the movement status is pending then cancel previous scheduled movements and create new scheduled movements based on the data passed in.
     * When creating new movements data:
     * 	A corresponding Activity and MovementActivity will be created and associated when updating a court activity.
     * 	The schedule details (date and time) will be captured in the Activity service and the other court hearing details will be captured in this service.
     * 	Court Movement and its Activity details are created.
     * 	a.	The system creates an OUT and IN court movement record using the details entered. The move date for the IN court movement record must be the same as the OUT and the move time is automatically set to the default time configured for court movements in the movement service.
     * 	b.	The move date and time is be populated in the PlannedStartDate of the Activity instance and the movement status is set to PENDING.
     * 	c.	Event and Movement records are associated.
     * </pre>
     *
     * @param uc                        UserContext - Required
     * @param courtActivityMovementType CourtActivityMovementType - Required
     * @return CourtActivityMovementType
     * Throws exception if instance cannot be created.
     */
    public CourtActivityMovementType updateCourtActivityWithMovement(UserContext uc, CourtActivityMovementType courtActivityMovementType);

    /**
     * inquireCaseOffenders -- inquire a list of Cases with offender information, matching the search criteria.
     * <pre>
     * At least one criteria must be provided.
     * If user selects only the identifier, then all cases of the selected identifier will be the return result.
     * If user only enters a case number, matches to the case number of all case identifier types will be the return result.
     * User can either enter only the “From” date or both “From/To”.
     * a.	If only “From” date is entered,   search results will only contain cases issued on that date
     * b.	If From/To dates are entered, search results will be then all cases issuance date that falls within the specified date range will be listed.
     * c.	If only a “To” date is provided, an error message "Please enter a “From” Date to narrow search results." will be shown requested the user to enter a “From” date
     * User can also search by CCF search criteria.
     * List of cases, with offender information, matching the search criteria
     * </pre>
     *
     * @param uc                    UserContext - Required
     * @param inquiryCaseSearchType InquiryCaseSearchType - Required
     * @param startIndex            Long – Optional if null search returns at the start index
     * @param resultSize            Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder           String – Optional if null search returns a default order
     * @return List of cases, with offender information.
     * Return total size 0 and empty list of case offenders if no data can be found.
     * Throws exception if inquiry encounters error.
     */
    public CaseOffendersReturnType inquireCaseOffenders(UserContext uc, InquiryCaseSearchType inquiryCaseSearchType, Long startIndex, Long resultSize,
            String resultOrder);

    /**
     * Get Bail Details by bail Id
     *
     * @param uc     UserContext Required
     * @param bailId Long Required
     * @return BailDetailsType returns Bail details if success; null otherwise.
     */
    public BailDetailsType getBailDetails(UserContext uc, Long bailId);

    /**
     * Retrieve all case affiliations for a particular case.
     * Provenance
     * caseId must be specified. All case affiliations related to the Case will be returned.
     * <pre>
     * If isActiveFlag = true
     *     Return all active case affiliations
     * If isActiveFlag = false
     *     Return all inactive case affiliations
     * If isActiveFlag = null (not provided)
     *     Return all case affiliations
     * </pre>
     *
     * @param uc           UserContext - Required
     * @param caseId       Long - Required
     * @param isActiveFlag Boolean - Optional
     * @return If it can find data, return a list of CaseAffiliationType objects.
     * If it can not find data, return null;
     * throw Exception if it encounters error.
     * @author lhan
     */
    public List<CaseAffiliationType> retrieveCaseAffiliations(UserContext uc, Long caseId, Boolean isActiveFlag);

}
