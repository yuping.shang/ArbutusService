package syscon.arbutus.product.services.legal.contract.interfaces;

import java.util.List;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.AdjustmentConfigType;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.AdjustmentType;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.SentenceAdjustmentSearchType;
import syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment.SentenceAdjustmentType;
import syscon.arbutus.product.services.sentencecalculation.contract.dto.SentenceCalculatorReturnType;
import syscon.arbutus.product.services.sentencecalculation.contract.dto.SentenceCalculatorType;

/**
 * Business contract interface of Sentence Adjustment for the use of Sentence Calculation.
 *
 * @author yshang
 * @since October 03, 2013
 */
public interface SentenceAdjustment {
    /**
     * setSentenceAdjustmentConfiguration Set Sentence Adjustment Configuration
     *
     * @param uc     UserContext, required.
     * @param config AdjustmentConfigType, required.
     */
    public void setSentenceAdjustmentConfiguration(UserContext uc, AdjustmentConfigType config);

    /**
     * getSentenceAdjustmentConfiguration Get Sentence Adjustment Configuration
     *
     * @param uc                     UserContext, required.
     * @param sentenceAdjustmentType String, required. The sentence adjustment type.
     * @return AdjustmentConfigType returns instance of AdjustmentConfigType when success;
     * throws ArbutusRuntimeException when failure.
     */
    public AdjustmentConfigType getSentenceAdjustmentConfiguration(UserContext uc, String sentenceAdjustmentType);

    /**
     * getAllSentenceAdjustmentConfiguration Get All Sentence Adjustment Configuration
     *
     * @param uc UserContext, required.
     * @return List&lt;AdjustmentConfigType> A list of AdjustmentConfigType when success;
     * throws ArbutusRuntimeException when failure.
     */
    public List<AdjustmentConfigType> getAllSentenceAdjustmentConfiguration(UserContext uc);

    /**
     * deleteSentenceAdjustmentConfiguration Delete Sentence Adjustment Configuration
     *
     * @param uc                     UserContext, required.
     * @param sentenceAdjustmentType String, required. The sentence adjustment yype.
     * @return Long success code 1 if success;
     * throws ArbutusRuntimeException when failure.
     */
    public Long deleteSentenceAdjustmentConfiguration(UserContext uc, String sentenceAdjustmentType);

    /**
     * deleteAllSentenceAdjustmentConfiguration Delete All Sentence Adjustment Configuration
     *
     * @param uc UserContext, required.
     * @return Long success code 1 if success;
     * throws ArbutusRuntimeException when failure.
     */
    public Long deleteAllSentenceAdjustmentConfiguration(UserContext uc);

    /**
     * createSentenceAdjustment Create Sentence Adjustment
     *
     * @param uc                 UserContext, required.
     * @param sentenceAdjustment SentenceAdjustmentType, required.
     * @return SentenceAdjustmentType instance of SentenceAdjustmentType if success;
     * throws ArbutusRuntimeException with error code 3008L when failure.
     */
    public SentenceAdjustmentType createSentenceAdjustment(UserContext uc, SentenceAdjustmentType sentenceAdjustment);

    /**
     * getSentenceAdjustment Get Sentence Adjustment
     *
     * @param uc            UserContext, required.
     * @param supervisionId Long, required. Supervision id
     * @return SentenceAdjustmentType instance of SentenceAdjustmentType if success;
     * throws ArbutusRuntimeException with error code 3008 when failure.
     */
    public SentenceAdjustmentType getSentenceAdjustment(UserContext uc, Long supervisionId);

    /**
     * getSentenceAdjustmentWithHistory Get Sentence Adjustments With History
     *
     * @param uc            UserContext, required.
     * @param supervisionId Long, required. Supervision id
     * @return List&lt;SentenceAdjustmentType> instance of SentenceAdjustmentType if success;
     * throws ArbutusRuntimeException with error code 3008 when failure.
     */
    public List<SentenceAdjustmentType> getSentenceAdjustmentsWithHistory(UserContext uc, Long supervisionId);

    /**
     * updateSentenceAdjustment Update Sentence Adjustment
     *
     * @param uc                 UserContext, required.
     * @param sentenceAdjustment SentenceAdjustmentType, required. The sentence adjustment type.
     * @return SentenceAdjustmentType instance of SentenceAdjustmentType if success;
     * throws ArbutusRuntimeException with error code 3008L when failure.
     */
    public SentenceAdjustmentType updateSentenceAdjustment(UserContext uc, SentenceAdjustmentType sentenceAdjustment);

    /**
     * addAdjustment Add one Adjustment upon the supervision
     *
     * @param uc            UserContext, required.
     * @param supervisionId Long, required. Supervision Id.
     * @param adjustment    AdjustmentType, required. An adjustment to add.
     * @return SentenceAdjustmentType instance of SentenceAdjustmentType if success;
     * throws ArbutusRuntimeException with error code 3008L when failure.
     */
    public SentenceAdjustmentType addAdjustment(UserContext uc, Long supervisionId, AdjustmentType adjustment);

    /**
     * addAdjustments Add Adjustments upon the supervision
     *
     * @param uc            UserContext, required.
     * @param supervisionId Long, required. Supervision Id.
     * @param adjustments   Set&lt;AdjustmentType>, required. A set of AdjustmentType to add.
     * @return SentenceAdjustmentType instance of SentenceAdjustmentType if success;
     * throws ArbutusRuntimeException with error code 3008L when failure.
     */
    public SentenceAdjustmentType addAdjustments(UserContext uc, Long supervisionId, Set<AdjustmentType> adjustments);

    /**
     * removeAdjustment Remove one Adjustment
     *
     * @param uc            UserContext, required.
     * @param supervisionId Long, required. Supervision Id.
     * @param adjustment    AdjustmentType, required. An adjustment to remove.
     * @return SentenceAdjustmentType instance of SentenceAdjustmentType if success;
     * throws ArbutusRuntimeException when failure.
     */
    public SentenceAdjustmentType removeAdjustment(UserContext uc, Long supervisionId, AdjustmentType adjustment);

    /**
     * removeAdjustments Remove a set of Adjustments
     *
     * @param uc            UserContext, required.
     * @param supervisionId Long, required. Supervision Id.
     * @param adjustments   Set&lt;AdjustmentType>, required. A set of AdjustmentType to remove.
     * @return SentenceAdjustmentType instance of SentenceAdjustmentType if success;
     * throws ArbutusRuntimeException when failure.
     */
    public SentenceAdjustmentType removeAdjustments(UserContext uc, Long supervisionId, Set<AdjustmentType> adjustments);

    /**
     * searchSentenceAdjustment Search Sentence Adjustment
     * <p>At least one criteria must be provided.
     *
     * @param uc            UserContext, required.
     * @param supervisionId Long, optional. supervision id.
     * @param search        SentenceAdjustmentSearchType, required if supervision not provided; optional if supervision provided.
     * @param startIndex    Long – Optional if null search returns at the start index
     * @param resultSize    Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder   String – Optional if null search returns a default order
     * @return List&lt;SentenceAdjustmentType> A list of Sentence Adjustment if success, return empty list when no instances could be found;
     * throws ArbutusRuntimeException when failure.
     */
    public List<SentenceAdjustmentType> searchSentenceAdjustment(UserContext uc, Long supervisionId, SentenceAdjustmentSearchType search, Long startIndex,
            Long resultSize, String resultOrder);

    /**
     * deleteSentenceAdjustment Delete Sentence Adjustment
     *
     * @param uc            UserContext, required.
     * @param supervisionId Long required. Supervision id.
     * @return Long success code 1 if success;
     * throws ArbutusRuntimeException when failure.
     */
    public Long deleteSentenceAdjustment(UserContext uc, Long supervisionId);

    /**
     * deleteAllSentenceAdjustments Delete All Sentence Adjustments
     *
     * @param uc UserContext, required.
     * @return Long success code 1 if success;
     * throws ArbutusRuntimeException when failure.
     */
    public Long deleteAllSentenceAdjustments(UserContext uc);

    /**
     * This method calculates fine amount balance sentence
     *
     * @param userContext        UserContext, required.
     * @param sentenceCalculator SentenceCalculatorType, required
     * @return SentenceCalculatorReturnType
     */
    public SentenceCalculatorReturnType calculateFine(UserContext userContext, SentenceCalculatorType sentenceCalculator);
}
