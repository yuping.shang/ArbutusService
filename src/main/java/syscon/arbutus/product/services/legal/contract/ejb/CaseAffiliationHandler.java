package syscon.arbutus.product.services.legal.contract.ejb;

import javax.ejb.SessionContext;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.common.Individual;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.core.contract.dto.ReferenceCodeType;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.core.exception.DataNotExistException;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationSearchType;
import syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationType;
import syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationsReturnType;
import syscon.arbutus.product.services.legal.contract.interfaces.CaseAffiliation;
import syscon.arbutus.product.services.legal.realization.adapters.ReferenceDataServiceAdapter;
import syscon.arbutus.product.services.legal.realization.persistence.caseaffiliation.CaseAffiliationEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseaffiliation.CaseAffiliationMetaSet;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.CaseInfoEntity;
import syscon.arbutus.product.services.organization.realization.persistence.OrganizationEntity;
import syscon.arbutus.product.services.person.realization.persistence.personidentity.PersonIdentityEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * CaseAffiliationHandler is handling case affiliation related functions.
 *
 * @author lhan
 */
public class CaseAffiliationHandler implements CaseAffiliation {

    private static final Logger log = LoggerFactory.getLogger(CaseAffiliationHandler.class);
    private static String LANGUAGE_ENGLISH = "EN";
    private SessionContext context;
    private Session session;
    @SuppressWarnings("unused")
    private int searchMaxLimit = 200;
    //    private PersonPersonServiceAdapter personPersonServiceAdapter = new PersonPersonServiceAdapter();
    private ReferenceDataServiceAdapter referenceDataServiceAdapter = new ReferenceDataServiceAdapter();

    public CaseAffiliationHandler(SessionContext context, Session session) {
        super();
        this.context = context;
        this.session = session;
    }

    public CaseAffiliationHandler(SessionContext context, Session session, int searchMaxLimit) {
        super();
        this.context = context;
        this.session = session;
        this.searchMaxLimit = searchMaxLimit;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.CaseAffiliation#createCaseAffiliation(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationType)
     */
    @Override
    public CaseAffiliationType createCaseAffiliation(UserContext uc, CaseAffiliationType caseAffiliation) {
        ValidationHelper.validate(caseAffiliation);

        validateBusinessRules(caseAffiliation);

        return createCaseAffiliationObject(uc, caseAffiliation);
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.CaseAffiliation#getCaseAffiliation(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    @Override
    public CaseAffiliationType getCaseAffiliation(UserContext uc, Long caseAffiliationId) {

        String functionName = "getCaseAffiliation";
        if (caseAffiliationId == null) {
            String message = "caseAffiliationId can not be null.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        CaseAffiliationEntity entity = (CaseAffiliationEntity) session.get(CaseAffiliationEntity.class, caseAffiliationId);

        if (entity == null) {
            String message = "Can not get CaseAffiliation by given id = " + caseAffiliationId;
            LogHelper.error(log, functionName, message);
            throw new DataNotExistException(message);
        }

        CaseAffiliationType caseAffiliation = CaseAffiliationHelper.toCaseAffiliationType(entity);

        caseAffiliation = addCaseAffiliatedCompositeData(caseAffiliation);

        return caseAffiliation;
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.CaseAffiliation#updateCaseAffiliation(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationType)
     */
    @Override
    public CaseAffiliationType updateCaseAffiliation(UserContext uc, CaseAffiliationType caseAffiliation) {
        //TODO All has been commented and should be revisited after PersonPerson refactoring!
        //        String functionName = "updateCaseAffiliation";
        //        Long caseAffiliationId = caseAffiliation.getCaseAffiliationId();
        //
        //        if (caseAffiliationId == null) {
        //            String message = "caseAffiliationId can not be null.";
        //            LogHelper.error(log, functionName, message);
        //            throw new InvalidInputException(message);
        //        }
        //
        //        ValidationHelper.validate(caseAffiliation);
        //
        //        validateBusinessRules(caseAffiliation);
        //
        //        CaseAffiliationEntity existEntity = (CaseAffiliationEntity) session.get(CaseAffiliationEntity.class, caseAffiliationId);
        //
        //        if (existEntity == null) {
        //            String message = "Can not get CaseAffiliation by given id = " + caseAffiliationId;
        //            LogHelper.error(log, functionName, message);
        //            throw new DataNotExistException(message);
        //        }
        //
        //        StampEntity stamp = BeanHelper.getModifyStamp(uc, context, existEntity.getStamp());
        //
        //        CaseAffiliationEntity entity = CaseAffiliationHelper.toCaseAffiliationEntity(caseAffiliation, stamp);
        //        entity.setVersion(existEntity.getVersion());
        //
        //        ValidationHelper.verifyMetaCodes(entity, false);
        //
        //        CaseAffiliationEntity newEntity = (CaseAffiliationEntity) session.merge(entity);
        //
        //        handlePersonPersonAssociation(uc, newEntity);
        //
        //        return CaseAffiliationHelper.toCaseAffiliationType(newEntity);

        throw new NotImplementedException("All has been commented and should be revisited after PersonPerson refactoring!");
    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.CaseAffiliation#deleteCaseAffiliation(syscon.arbutus.product.security.UserContext, java.lang.Long)
     */
    @Override
    public Long deleteCaseAffiliation(UserContext uc, Long caseAffiliationId) {

        Long rtnCode = ReturnCode.Success.returnCode();

        //id can not be null.
        if (caseAffiliationId == null) {
            String message = "caseAffiliationId is null";
            LogHelper.error(log, "deleteCaseAffiliation", message);
            throw new InvalidInputException(message);
        }

        CaseAffiliationEntity entity = BeanHelper.getEntity(session, CaseAffiliationEntity.class, caseAffiliationId);

        //Soft deletion.
        entity.setFlag(DataFlag.DELETE.value());

        session.update(entity);

        return rtnCode;

    }

    /* (non-Javadoc)
     * @see syscon.arbutus.product.services.legal.contract.interfaces.CaseAffiliation#searchCaseAffiliation(syscon.arbutus.product.security.UserContext, syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationSearchType, java.lang.Long, java.lang.Long, java.lang.String)
     */
    @Override
    public CaseAffiliationsReturnType searchCaseAffiliation(UserContext uc, CaseAffiliationSearchType search, Long startIndex, Long resultSize, String resultOrder) {
        throw new UnsupportedOperationException(
                "This API searchCaseAffiliation is NOT supported yet. Use the API retrieveCaseAffiliations(UserContext uc, Long caseId, Boolean isActiveFlag)");

        // TODO: Not sure if there is an user story of "search a case affiliation", need to confirm with BA.
    }

    /////////////////////////////////////////private methods///////////////////////////////////////

    /**
     * Create an object of CaseAffiliationType and all it's associated data.
     *
     * @param uc              UserContext
     * @param caseAffiliation CourtActivityType
     * @return the created object of CourtActivityType
     */
    private CaseAffiliationType createCaseAffiliationObject(UserContext uc, CaseAffiliationType caseAffiliation) {
        //TODO All has been commented and should be revisited after PersonPerson refactoring!
        //        // Create stamp
        //        StampEntity stamp = BeanHelper.getCreateStamp(uc, context);
        //
        //        // Create the entity
        //        CaseAffiliationEntity caseAffiliationEntity = CaseAffiliationHelper.toCaseAffiliationEntity(caseAffiliation, stamp);
        //
        //        // Verify reference codes.
        //        ValidationHelper.verifyMetaCodes(caseAffiliationEntity, true);
        //
        //        session.save(caseAffiliationEntity);
        //
        //        handlePersonPersonAssociation(uc, caseAffiliationEntity);
        //
        //        Long caseAffiliationId = caseAffiliationEntity.getCaseAffiliationId();
        //
        //        // Flush changes to db
        //        session.flush();
        //        session.clear();
        //
        //        //get instance from DB
        //        return getCaseAffiliation(uc, caseAffiliationId);

        throw new NotImplementedException("All has been commented and should be revisited after PersonPerson refactoring!");

    }

    //TODO All has been commented and should be revisited after PersonPerson refactoring!
    //    private void handlePersonPersonAssociation(UserContext uc, CaseAffiliationEntity caseAffiliationEntity) {
    //
    //        if (caseAffiliationEntity.getAffiliationCategory().equalsIgnoreCase(AffiliationCategory.ORG.name())) {
    //            return;
    //        }
    //
    //        Long surpervisionId = (Long) session.createQuery("select supervisionId from CaseInfoEntity where caseInfoId = :caseInfoId").setParameter("caseInfoId",
    //                caseAffiliationEntity.getAffiliatedCaseId()).uniqueResult();
    //
    //        Long offenderPersonId = (Long) session.createQuery("select personId from SupervisionEntity where supervisionId = :supervisionId").setParameter("supervisionId",
    //                surpervisionId).uniqueResult();
    //
    //        Long affiliatedPersonId = (Long) session.createQuery("select personId from PersonIdentityEntity where personIdentityId = :personIdentityId").setParameter(
    //                "personIdentityId", caseAffiliationEntity.getAffiliatedPersonIdentityId()).uniqueResult();
    //
    //        //create a new person person association if does not existing.
    //        createOrUpdatePersonPerson(uc, offenderPersonId, affiliatedPersonId, caseAffiliationEntity.getAffiliationRole(), caseAffiliationEntity.getAffiliationType(),
    //                caseAffiliationEntity.getEndDate());
    //
    //    }

    //TODO All has been commented and should be revisited after PersonPerson refactoring!
    //    private void createOrUpdatePersonPerson(UserContext uc, Long offenderPersonId, Long affiliatedPersonId, String affiliationRole, String associationType,
    //            Date expireDate) {
    //
    //        PersonPersonSearchType searchType = new PersonPersonSearchType();
    //        searchType.setMainPersonReference(affiliatedPersonId);
    //        searchType.setAssociatedPersonReference(offenderPersonId);
    //        searchType.setCrossFlag(true);
    //
    //        PersonPersonsReturnType returnType = personPersonServiceAdapter.search(uc, searchType);
    //
    //        if (returnType.getTotalSize() == 0) {
    //            createPersonPeron(uc, offenderPersonId, affiliatedPersonId, affiliationRole, associationType, expireDate);
    //        } else if (returnType.getTotalSize() == 1) {
    //            updatePersonPerson(uc, offenderPersonId, affiliatedPersonId, affiliationRole, associationType, returnType.getPersonPerson().get(0), expireDate);
    //        }
    //
    //    }

    //TODO All has been commented and should be revisited after PersonPerson refactoring!
    //    private void updatePersonPerson(UserContext uc, Long offenderPersonId, Long affiliatedPersonId, String affiliationRole, String associationType,
    //            PersonPersonType personPersonType, Date expireDate) {
    //
    //        if (!personPersonType.getMainPersonReference().equals(affiliatedPersonId)) {
    //            personPersonType = switchPersonIdsAndRoles(personPersonType); //Always assign main person with affiliation person.
    //        }
    //
    //        List<ReferenceCodeType> linkedCodes = getLindedCodesByRole(uc, affiliationRole);
    //
    //        //check person association
    //        if (!isPersonAssociationExist(uc, offenderPersonId, affiliatedPersonId, associationType, affiliationRole)) {
    //            personPersonType = addPersonAssociation(linkedCodes, personPersonType, associationType, affiliationRole, expireDate);
    //        }
    //
    //        personPersonType = addNonAssociation(linkedCodes, personPersonType, associationType, affiliationRole, expireDate);
    //
    //        personPersonServiceAdapter.update(uc, personPersonType);
    //    }
    //TODO All has been commented and should be revisited after PersonPerson refactoring!
//    private PersonPersonType switchPersonIdsAndRoles(PersonPersonType personPersonType) {
//        Long affiliatedPersonId = personPersonType.getAssociatedPersonReference();
//        personPersonType.setAssociatedPersonReference(personPersonType.getMainPersonReference());
//        personPersonType.setMainPersonReference(affiliatedPersonId);
//
//        Set<PersonAssociationType> personAssociations = personPersonType.getPersonAssociations();
//        for (PersonAssociationType personAssociation : personAssociations) {
//            String associationRole = personAssociation.getAssociatedPersonRole();
//            personAssociation.setAssociatedPersonRole(personAssociation.getMainPersonRole());
//            personAssociation.setMainPersonRole(associationRole);
//        }
//
//        Set<OffenderNonAssociationType> nonAssociations = personPersonType.getOffenderNonAssociations();
//        for (OffenderNonAssociationType nonAssociation : nonAssociations) {
//            String associationRole = nonAssociation.getAssociatedPersonRole();
//            nonAssociation.setAssociatedPersonRole(nonAssociation.getMainPersonRole());
//            nonAssociation.setMainPersonRole(associationRole);
//        }
//        return personPersonType;
//    }

    private void createPersonPeron(UserContext uc, Long offenderPersonId, Long affiliatedPersonId, String affiliationRole, String associationType, Date expireDate) {
        //TODO All has been commented and should be revisited after PersonPerson refactoring!
        //        PersonPersonType personPersonType = new PersonPersonType();
        //        personPersonType.setAssociatedPersonReference(offenderPersonId);
        //        personPersonType.setMainPersonReference(affiliatedPersonId);
        //
        //        List<ReferenceCodeType> linkedCodes = getLindedCodesByRole(uc, affiliationRole);
        //
        //        personPersonType = addPersonAssociation(linkedCodes, personPersonType, associationType, affiliationRole, expireDate);
        //        personPersonType = addNonAssociation(linkedCodes, personPersonType, associationType, affiliationRole, expireDate);
        //
        //        personPersonServiceAdapter.create(uc, personPersonType);

    }
    //TODO All has been commented and should be revisited after PersonPerson refactoring!

//    private PersonPersonType addPersonAssociation(List<ReferenceCodeType> linkedCodes, PersonPersonType personPersonType, String associationType, String affiliationRole,
//            Date expireDate) {
//        for (ReferenceCodeType codeType : linkedCodes) {
//            if (codeType.getSet().equalsIgnoreCase(CaseAffiliationMetaSet.ASSOCIATION_TYPE) && codeType.getCode().equalsIgnoreCase(associationType)) {
//                PersonAssociationType personAssociationType = new PersonAssociationType();
//                personAssociationType.setActiveFlag(true);
//                personAssociationType.setAssociationType(associationType);
//                personAssociationType.setMainPersonRole(affiliationRole);
//                personAssociationType.setExpiryDate(expireDate);
//                personPersonType.getPersonAssociations().add(personAssociationType);
//            }
//        }
//        return personPersonType;
//    }
//
//    private PersonPersonType addNonAssociation(List<ReferenceCodeType> linkedCodes, PersonPersonType personPersonType, String associationType, String affiliationRole,
//            Date expireDate) {
//
//        Set<OffenderNonAssociationType> nonAssociations = personPersonType.getOffenderNonAssociations();
//
//        for (OffenderNonAssociationType nonAssociationType : nonAssociations) {// there is any active non association, no need to add non association.
//            if (Boolean.TRUE.equals(nonAssociationType.isActiveFlag())) {
//                return personPersonType;
//            }
//        }
//
//        for (ReferenceCodeType codeType : linkedCodes) {
//
//            if (codeType.getSet().equalsIgnoreCase(ReferenceSet.NON_ASSOCIATION_TYPE.value())) {// Only when the role has non association linked code.
//                OffenderNonAssociationType nonAssociationType = new OffenderNonAssociationType();
//                nonAssociationType.setActiveFlag(true);
//                nonAssociationType.setNonAssociationType(codeType.getCode());
//                nonAssociationType.setMainPersonRole(affiliationRole);
//                nonAssociationType.setEffectiveDate(new Date());
//                nonAssociationType.setExpiryDate(expireDate);
//                personPersonType.getOffenderNonAssociations().add(nonAssociationType);
//                break; //Same code only is created one time since there is only one active non-association is allowed validation in PersonPesonService. WOR-10029
//            }
//        }
//        return personPersonType;
//    }

    private List<ReferenceCodeType> getLindedCodesByRole(UserContext uc, String affiliationRole) {
        CodeType linkSetCode = new CodeType(CaseAffiliationMetaSet.PERSON_ROLE, affiliationRole);
        List<ReferenceCodeType> linkedCodes = referenceDataServiceAdapter.getReferenceCodesForLink(uc, linkSetCode, true, LANGUAGE_ENGLISH);

        return linkedCodes;
    }

    private boolean isPersonAssociationExist(UserContext uc, Long offenderPersonId, Long affiliatedPersonId, String affiliationType, String affiliationRole) {
        //TODO All has been commented and should be revisited after PersonPerson refactoring!
        //        PersonPersonSearchType searchType = new PersonPersonSearchType();
        //        searchType.setMainPersonReference(affiliatedPersonId);
        //        searchType.setAssociationType(affiliationType);
        //        searchType.setMainPersonRole(affiliationRole);
        //        searchType.setAssociatedPersonReference(offenderPersonId);
        //        searchType.setCrossFlag(true);
        //
        //        PersonPersonServiceAdapter personPersonServiceAdapter = new PersonPersonServiceAdapter();
        //        if (personPersonServiceAdapter.search(uc, searchType).getTotalSize() == 0) {
        //            return false;
        //        } else {
        //            return true;
        //        }
        throw new NotImplementedException("All has been commented and should be revisited after PersonPerson refactoring!");
    }

    /**
     * Validate business rules defined by BA.
     *
     * @param caseAffiliation
     */
    private void validateBusinessRules(CaseAffiliationType caseAffiliation) {
        String functionName = "validateBusinessRules";

        //Either AffiliatedOrganizationId or AffiliatedPersonIdentityId or both must have value.
        if (caseAffiliation.getAffiliatedOrganizationId() == null && caseAffiliation.getAffiliatedPersonIdentityId() == null) {
            String message = String.format("Either affiliatedOrganizationId or affiliatedPersonIdentityId or both must have value.");
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        //Set category ORG
        if (caseAffiliation.getAffiliatedOrganizationId() != null && caseAffiliation.getAffiliatedPersonIdentityId() == null) {
            caseAffiliation.setCaseAffiliationCategory(AffiliationCategory.ORG.name());
        }

        //Set category PI
        if (caseAffiliation.getAffiliatedOrganizationId() == null && caseAffiliation.getAffiliatedPersonIdentityId() != null) {
            caseAffiliation.setCaseAffiliationCategory(AffiliationCategory.PI.name());
        }

        //Set category PI
        if (caseAffiliation.getAffiliatedOrganizationId() != null && caseAffiliation.getAffiliatedPersonIdentityId() != null) {
            caseAffiliation.setCaseAffiliationCategory(AffiliationCategory.ORGPI.name());
        }

        //validate AffiliatedCaseId exists.
        Long caseId = caseAffiliation.getAffiliatedCaseId();

        if (BeanHelper.findEntity(session, CaseInfoEntity.class, caseId) == null) {
            String message = String.format("The case with case id %s does not exist", caseId);
            LogHelper.error(log, functionName, message);
            throw new DataNotExistException(message);
        }
    }

    private CaseAffiliationType addCaseAffiliatedCompositeData(CaseAffiliationType caseAffiliation) {
        Long personIdentityId = caseAffiliation.getAffiliatedPersonIdentityId();
        if (personIdentityId != null) {
            PersonIdentityEntity personIdentityentity = BeanHelper.findEntity(session, PersonIdentityEntity.class, personIdentityId);
            if (personIdentityentity != null) {
                Individual individual = new Individual();
                individual.setPersonIdentityId(personIdentityId);
                individual.setFirstName(personIdentityentity.getFirstName());
                individual.setLastName(personIdentityentity.getLastName());
                individual.setGender(personIdentityentity.getSex());

                caseAffiliation.setAffiliatedPersonInfo(individual);
            }
        }

        Long organizationId = caseAffiliation.getAffiliatedOrganizationId();
        if (organizationId != null) {
            OrganizationEntity organizationEntity = BeanHelper.findEntity(session, OrganizationEntity.class, organizationId);
            if (organizationEntity != null) {
                caseAffiliation.setAffiliatedOrganizationName(organizationEntity.getOrganizationName());
            }
        }

        return caseAffiliation;
    }

    public boolean hasCaseAffiliation(UserContext uc, Long personIdA, Long personIdB) {
        // TODO: implement this if case affiliation logic is ready, return false for now
        // this function is the same as LegalServiceBean.hasCaseaffiliation(..)
        return false;
    }

    public boolean isCaseAffiliationByOrganization(Long organizationId){
        if (!BeanHelper.isEmpty(organizationId)) {
            return false;
        }
        Criteria c = session.createCriteria(CaseAffiliationEntity.class);
        c.add(Restrictions.eq("affiliatedOrganizationId", organizationId));
        return !c.list().isEmpty();
    }

    private enum AffiliationCategory {ORG, PI, ORGPI}

}
