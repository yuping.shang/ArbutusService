package syscon.arbutus.product.services.legal.realization.persistence.caseinformation;

/**
 * This class is not an Entity, just temporarily record type and value for duplicated check.
 *
 * @author lhan
 */
public class CaseIdentifierTypeValue {

    private String type;
    private String value;

    public CaseIdentifierTypeValue(String type, String value) {
        super();
        this.type = type;
        this.value = value;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        CaseIdentifierTypeValue other = (CaseIdentifierTypeValue) obj;
        if (type == null) {
            if (other.type != null) {
				return false;
			}
        } else if (!type.equals(other.type)) {
			return false;
		}
        if (value == null) {
            if (other.value != null) {
				return false;
			}
        } else if (!value.equals(other.value)) {
			return false;
		}
        return true;
    }

}
