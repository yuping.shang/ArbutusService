package syscon.arbutus.product.services.legal.contract.ejb;

import javax.ejb.SessionContext;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Session;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldMetaType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueSearchType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.ClientCustomFieldValueType;
import syscon.arbutus.product.services.clientcustomfield.contract.dto.DTOBindingTypeEnum;
import syscon.arbutus.product.services.core.common.adapters.ClientCustomFieldServiceAdapter;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class ClientCustomFieldHandler {
    private static Logger log = LoggerFactory.getLogger(ClientCustomFieldHandler.class);

    public static String getCCFMeta(UserContext uc, SessionContext context, Session session, DTOBindingTypeEnum legalModule, String language) {

        String functionName = "getCCFMeta";
        if (legalModule == null || BeanHelper.isEmpty(language)) {
            String message = "Invalid argument: legal moude and language are required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        return ClientCustomFieldServiceAdapter.getCCFMeta(uc, legalModule.getCode(), language);
    }

    public static ClientCustomFieldMetaType getClientCustomFieldMeta(UserContext uc, SessionContext context, Session session, DTOBindingTypeEnum legalModule,
            String name) {

        String functionName = "getClientCustomFieldMeta";
        if (legalModule == null || BeanHelper.isEmpty(name)) {
            String message = "Invalid argument: legal moude and name are required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        return ClientCustomFieldServiceAdapter.getClientCustomFieldMeta(uc, legalModule.getCode(), name);
    }

    public static List<ClientCustomFieldMetaType> getAllCCFMeta(UserContext uc, SessionContext context, Session session, DTOBindingTypeEnum legalModule) {

        String functionName = "getAllCCFMeta";
        if (legalModule == null) {
            String message = "Invalid argument: legal moude is required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }
        return ClientCustomFieldServiceAdapter.getAllClientCustomFieldMeta(uc, legalModule.getCode());
    }

    public static ClientCustomFieldValueType saveCCFMetaValue(UserContext uc, SessionContext context, Session session, ClientCustomFieldValueType ccfValue) {

        ValidationHelper.validate(ccfValue);
        return ClientCustomFieldServiceAdapter.saveCCFMetaValue(uc, ccfValue);
    }

    public static List<ClientCustomFieldValueType> saveCCFMetaValues(UserContext uc, SessionContext context, Session session,
            List<ClientCustomFieldValueType> ccfValues) {

        ValidationHelper.validate(ccfValues);
        return ClientCustomFieldServiceAdapter.saveCCFMetaValues(uc, ccfValues);
    }

    public static List<ClientCustomFieldValueType> getCCFMetaValues(UserContext uc, SessionContext context, Session session,
            ClientCustomFieldValueSearchType searchType) {

        ValidationHelper.validateSearchType(searchType);
        return ClientCustomFieldServiceAdapter.searchCCFMetaValues(uc, searchType);
    }

    public static ClientCustomFieldValueType updateCCFMetaValue(UserContext uc, SessionContext context, Session session, ClientCustomFieldValueType ccfValue) {

        if (ccfValue.getCcfValueId() == null) {
            String message = "Invalid argument: Client Custome Field Value Id(CcfValueId) is required.";
            LogHelper.error(log, "updateCCFMetaValue", message);
            throw new InvalidInputException(message);
        }
        ValidationHelper.validate(ccfValue);
        return ClientCustomFieldServiceAdapter.updateCCFMetaValue(uc, ccfValue);
    }

    public static List<ClientCustomFieldValueType> updateCCFMetaValues(UserContext uc, SessionContext context, Session session,
            List<ClientCustomFieldValueType> ccfValues) {

        if (BeanHelper.isEmpty(ccfValues)) {
            String message = "Invalid argument: ccfValues cannot be null or empty.";
            LogHelper.error(log, "updateCCFMetaValues", message);
            throw new InvalidInputException(message);
        }
        return ClientCustomFieldServiceAdapter.updateCCFMetaValues(uc, ccfValues);
    }
}
