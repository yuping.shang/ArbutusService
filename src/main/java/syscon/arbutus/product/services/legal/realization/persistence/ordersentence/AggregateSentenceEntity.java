package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * AggregateSentenceEntity for persist Aggregate Sentences
 *
 * @author yshang
 * @DbComment LEG_OrdAggSent 'Aggregate Sentence table for Order Sentence module of Legal service'
 * @since November 27, 2013
 */
@Audited
@Entity
@Table(name = "LEG_OrdAggSent")
@SQLDelete(sql = "UPDATE LEG_OrdAggSent SET flag = 4 WHERE orderId = ? and version = ?")
@Where(clause = "flag = 1")
public class AggregateSentenceEntity implements Serializable {

    private static final long serialVersionUID = -1691515215548143909L;

    /**
     * @DbComment aggregateId 'Identification of table LEG_OrdAggSent'
     */
    @Id
    @Column(name = "aggregateId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDAGGSENT_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDAGGSENT_ID", sequenceName = "SEQ_LEG_ORDAGGSENT_ID", allocationSize = 1)
    private Long aggregateId;

    /**
     * @DbComment supervisionId 'Supervision Id, identification of table SUP_Supervision'
     */
    @Column(name = "supervisionId", nullable = false)
    private Long supervisionId;

    @OneToMany(mappedBy = "aggregateSentence", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<AggregateSentenceAssocEntity> sentenceAssocs;

    /**
     * @DbComment startDate 'Sentence Start date calculated by SentenceCalculation engine'
     */
    @Column(name = "startDate", nullable = true)
    private Date startDate;

    /**
     * @DbComment endDate 'Sentence End date calculated by SentenceCalculation engine'
     */
    @Column(name = "endDate", nullable = true)
    private Date endDate;

    /**
     * @DbComment prdDate 'Probable release date calculated by SentenceCalculation engine'
     */
    @Column(name = "prdDate", nullable = true)
    private Date prdDate;

    /**
     * @DbComment totalSentenceDays 'Total sentence days calculated by SentenceCalculation engine'
     */
    @Column(name = "totalSentenceDays", nullable = true)
    private Long totalSentenceDays;

    /**
     * @DbComment goodTime 'Good time calculated by SentenceCalculation engine'
     */
    @Column(name = "goodTime", nullable = true)
    private Long goodTime;

    /**
     * @DbComment daysToServe 'Days to serve'
     */
    @Column(name = "daysToServe", nullable = true)
    private Long daysToServe;

    /**
     * @DbComment staffId 'staff Id, the identification of STF_Staff'
     */
    @Column(name = "staffId", nullable = true)
    private Long staffId;

    /**
     * @DbComment comments 'Comment -- Calculation Reason'
     */
    @Column(name = "comments", nullable = true, length = 64)
    private String comments;

    /**
     * @DbComment calculatingDate 'The date of the staff calculated'
     */
    @Column(name = "calculatingDate", nullable = true)
    private Date calculatingDate;

    /**
     * @DbComment bCalculateEnabled 'True: Calculate Enabled; False: Calculate Not Enabled'
     */
    @Column(name = "bCalculateEnabled", nullable = true)
    private Boolean bCalculateEnabled;

    /**
     * DbComment isControllingSentence 'True: Controlling Sentence; False: Not Controlling Sentence'
     */
    @Column(name = "isControllingSentence", nullable = true)
    private Boolean isControllingSentence;

    /**
     * @DbComment version 'Version for Hibernate use'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     * @DbComment isHistory 'True: is history; False: current recorder'
     */
    @Column(name = "isHistory", nullable = true)
    private Boolean isHistory;

    /**
     * @DbComment keyDateHistoryId 'Id of KeyDates history'
     */
    @Column(name = "keyDateHistoryId", nullable = true)
    private Long keyDateHistoryId;

    /**
     * @DbComment supSentId 'Id of supervision sentence (table LEG_OrdSupSent)'
     */
    @ForeignKey(name = "Fk_LEG_OrdAggSent")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "supSentId")
    private SupervisionSentenceEntity supervisionSentence;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @return the aggregateId
     */
    public Long getAggregateId() {
        return aggregateId;
    }

    /**
     * @param aggregateId the aggregateId to set
     */
    public void setAggregateId(Long aggregateId) {
        this.aggregateId = aggregateId;
    }

    /**
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * @return the sentenceAssocs
     */
    public Set<AggregateSentenceAssocEntity> getSentenceAssocs() {
        if (sentenceAssocs == null) {
			sentenceAssocs = new HashSet<>();
		}
        return sentenceAssocs;
    }

    /**
     * @param sentenceAssocs the sentenceAssocs to set
     */
    public void setSentenceAssocs(Set<AggregateSentenceAssocEntity> sentenceAssocs) {
        if (sentenceAssocs != null) {
            for (AggregateSentenceAssocEntity assoc : sentenceAssocs) {
                assoc.setAggregateSentence(this);
            }
            this.sentenceAssocs = sentenceAssocs;
        } else {
			this.sentenceAssocs = new HashSet<>();
		}
    }

    public void addSentenceAssoc(AggregateSentenceAssocEntity sentenceAssoc) {
        sentenceAssoc.setAggregateSentence(this);
        getSentenceAssocs().add(sentenceAssoc);
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the prdDate
     */
    public Date getPrdDate() {
        return prdDate;
    }

    /**
     * @param prdDate the prdDate to set
     */
    public void setPrdDate(Date prdDate) {
        this.prdDate = prdDate;
    }

    /**
     * @return the totalSentenceDays
     */
    public Long getTotalSentenceDays() {
        return totalSentenceDays;
    }

    /**
     * @param totalSentenceDays the totalSentenceDays to set
     */
    public void setTotalSentenceDays(Long totalSentenceDays) {
        this.totalSentenceDays = totalSentenceDays;
    }

    /**
     * @return the goodTime
     */
    public Long getGoodTime() {
        return goodTime;
    }

    /**
     * @param goodTime the goodTime to set
     */
    public void setGoodTime(Long goodTime) {
        this.goodTime = goodTime;
    }

    /**
     * @return the daysToServe
     */
    public Long getDaysToServe() {
        return daysToServe;
    }

    /**
     * @param daysToServe the daysToServe to set
     */
    public void setDaysToServe(Long daysToServe) {
        this.daysToServe = daysToServe;
    }

    /**
     * @return the staffId
     */
    public Long getStaffId() {
        return staffId;
    }

    /**
     * @param staffId the staffId to set
     */
    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the calculatingDate
     */
    public Date getCalculatingDate() {
        return calculatingDate;
    }

    /**
     * @param calculatingDate the calculatingDate to set
     */
    public void setCalculatingDate(Date calculatingDate) {
        this.calculatingDate = calculatingDate;
    }

    /**
     * @return the bCalculateEnabled
     */
    public Boolean getbCalculateEnabled() {
        return bCalculateEnabled;
    }

    /**
     * @param bCalculateEnabled the bCalculateEnabled to set
     */
    public void setbCalculateEnabled(Boolean bCalculateEnabled) {
        this.bCalculateEnabled = bCalculateEnabled;
    }

    /**
     * @return the isControllingSentence
     */
    public Boolean getIsControllingSentence() {
        return isControllingSentence;
    }

    /**
     * @param isControllingSentence the isControllingSentence to set
     */
    public void setIsControllingSentence(Boolean isControllingSentence) {
        this.isControllingSentence = isControllingSentence;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the isHistory
     */
    public Boolean getIsHistory() {
        return isHistory;
    }

    /**
     * @param isHistory the isHistory to set
     */
    public void setIsHistory(Boolean isHistory) {
        this.isHistory = isHistory;
    }

    /**
     * @return the keyDateHistoryId
     */
    public Long getKeyDateHistoryId() {
        return keyDateHistoryId;
    }

    /**
     * @param keyDateHistoryId the keyDateHistoryId to set
     */
    public void setKeyDateHistoryId(Long keyDateHistoryId) {
        this.keyDateHistoryId = keyDateHistoryId;
    }

    /**
     * @return the supervisionSentence
     */
    public SupervisionSentenceEntity getSupervisionSentence() {
        return supervisionSentence;
    }

    /**
     * @param supervisionSentence the supervisionSentence to set
     */
    public void setSupervisionSentence(SupervisionSentenceEntity supervisionSentence) {
        this.supervisionSentence = supervisionSentence;
    }

    /**
     * @return the flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((aggregateId == null) ? 0 : aggregateId.hashCode());
        result = prime * result + ((bCalculateEnabled == null) ? 0 : bCalculateEnabled.hashCode());
        result = prime * result + ((calculatingDate == null) ? 0 : calculatingDate.hashCode());
        result = prime * result + ((comments == null) ? 0 : comments.hashCode());
        result = prime * result + ((daysToServe == null) ? 0 : daysToServe.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((goodTime == null) ? 0 : goodTime.hashCode());
        result = prime * result + ((isHistory == null) ? 0 : isHistory.hashCode());
        result = prime * result + ((keyDateHistoryId == null) ? 0 : keyDateHistoryId.hashCode());
        result = prime * result + ((prdDate == null) ? 0 : prdDate.hashCode());
        result = prime * result + ((staffId == null) ? 0 : staffId.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        result = prime * result + ((totalSentenceDays == null) ? 0 : totalSentenceDays.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        AggregateSentenceEntity other = (AggregateSentenceEntity) obj;
        if (aggregateId == null) {
            if (other.aggregateId != null) {
				return false;
			}
        } else if (!aggregateId.equals(other.aggregateId)) {
			return false;
		}
        if (bCalculateEnabled == null) {
            if (other.bCalculateEnabled != null) {
				return false;
			}
        } else if (!bCalculateEnabled.equals(other.bCalculateEnabled)) {
			return false;
		}
        if (calculatingDate == null) {
            if (other.calculatingDate != null) {
				return false;
			}
        } else if (!calculatingDate.equals(other.calculatingDate)) {
			return false;
		}
        if (comments == null) {
            if (other.comments != null) {
				return false;
			}
        } else if (!comments.equals(other.comments)) {
			return false;
		}
        if (daysToServe == null) {
            if (other.daysToServe != null) {
				return false;
			}
        } else if (!daysToServe.equals(other.daysToServe)) {
			return false;
		}
        if (endDate == null) {
            if (other.endDate != null) {
				return false;
			}
        } else if (!endDate.equals(other.endDate)) {
			return false;
		}
        if (goodTime == null) {
            if (other.goodTime != null) {
				return false;
			}
        } else if (!goodTime.equals(other.goodTime)) {
			return false;
		}
        if (isHistory == null) {
            if (other.isHistory != null) {
				return false;
			}
        } else if (!isHistory.equals(other.isHistory)) {
			return false;
		}
        if (keyDateHistoryId == null) {
            if (other.keyDateHistoryId != null) {
				return false;
			}
        } else if (!keyDateHistoryId.equals(other.keyDateHistoryId)) {
			return false;
		}
        if (prdDate == null) {
            if (other.prdDate != null) {
				return false;
			}
        } else if (!prdDate.equals(other.prdDate)) {
			return false;
		}
        if (staffId == null) {
            if (other.staffId != null) {
				return false;
			}
        } else if (!staffId.equals(other.staffId)) {
			return false;
		}
        if (startDate == null) {
            if (other.startDate != null) {
				return false;
			}
        } else if (!startDate.equals(other.startDate)) {
			return false;
		}
        if (supervisionId == null) {
            if (other.supervisionId != null) {
				return false;
			}
        } else if (!supervisionId.equals(other.supervisionId)) {
			return false;
		}
        if (totalSentenceDays == null) {
            if (other.totalSentenceDays != null) {
				return false;
			}
        } else if (!totalSentenceDays.equals(other.totalSentenceDays)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "AggregateSentenceEntity [aggregateId=" + aggregateId + ", supervisionId=" + supervisionId + ", sentenceAssocs=" + sentenceAssocs + ", startDate="
                + startDate + ", endDate=" + endDate + ", prdDate=" + prdDate + ", totalSentenceDays=" + totalSentenceDays + ", goodTime=" + goodTime + ", daysToServe="
                + daysToServe + ", staffId=" + staffId + ", comments=" + comments + ", calculatingDate=" + calculatingDate + ", bCalculateEnabled=" + bCalculateEnabled
                + ", isControllingSentence=" + isControllingSentence + ", isHistory=" + isHistory + ", keyDateHistoryId=" + keyDateHistoryId + "]";
    }

}
