package syscon.arbutus.product.services.legal.contract.dto.caseinformation;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;

import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * The representation of the case information type
 */
public class CaseInfoType implements Serializable {

    private static final long serialVersionUID = 1514116658273070672L;

    private Long caseInfoId;
    @Valid
    private List<CaseIdentifierType> caseIdentifiers;
    @NotNull
    private String caseCategory;
    @NotNull
    private String caseType;
    private Date createdDate;
    private Date issuedDate;
    private Long facilityId;
    @NotNull
    private Long supervisionId;
    private Boolean diverted;
    private Boolean divertedSuccess;
    @NotNull
    private Boolean active;
    private String statusChangeReason;
    private String sentenceStatus;
    @Valid
    private CommentType comment;

    //Module relationships of case management
    private Set<Long> chargeIds;
    private Set<Long> conditionIds;
    private Set<Long> orderSentenceIds;
    private Set<Long> caseInfoIds;

    private Set<Long> caseActivityIds;

    private boolean saveAsSecondaryIdentifier;

    /**
     * Default constructor
     */
    public CaseInfoType() {
    }

    /**
     * Constructor
     *
     * @param caseInfoId         Long - the unique Id for each case information record. (optional for creation)
     * @param caseIdentifiers    List<CaseIdentifierType> - A set of numbers/identifiers recorded for a case. (optional)
     * @param caseCategory       String - The category of the case, e.g. InJurisdiction case, Out of Jurisdiction case. (required)
     * @param caseType           String - Indicates the type of case eg. immigration, traffic, criminal. (required)
     * @param createdDate        Date - The date when the case was first created. It is set to the current system date by system. (ignored)
     * @param issuedDate         Date - The date on which the case was issued (by the court) (optional)
     * @param facilityId         Long - Used to indicate the originating facility name, type and location for a case. Static reference to the Facility service. (optional)
     * @param supervisionId      Long - Reference to a Supervision period. Static reference to the Supervision service. (required)
     * @param diverted           Boolean - Indicates whether the case was attempted to be diverted. (optional)
     * @param divertedSuccess    Boolean - Indicates whether a case diversion (if attempted) was successful or unsuccessful. (optional)
     * @param active             Boolean - Indicates whether the case is active or inactive. This is specifically set by the user, not derived. (required)
     * @param statusChangeReason String - Indicates the reason a case status was changed (optional)
     * @param sentenceStatus     String - Indicates whether the offender was sentenced to be incarcerated (optional)
     * @param comment            CommentType - The comments associated to a Case (optional)
     * @param chargeIds          Set<Long> - a set of charge ids which associated to a case (optional)
     * @param conditionIds       Set<Long> - a set of condition ids which associated to a case (optional)
     * @param orderSentenceIds   Set<Long> - a set of order sentence ids which associated to a case (optional)
     * @param caseInfoIds        Set<Long> - a set of case information ids which associated to a case (optional)
     * @param caseActivityIds    Set<Long> - a set of case activity ids which associated to a case (optional)
     */
    public CaseInfoType(Long caseInfoId, List<CaseIdentifierType> caseIdentifiers, @NotNull String caseCategory, @NotNull String caseType, Date createdDate,
            Date issuedDate, Long facilityId, @NotNull Long supervisionId, Boolean diverted, Boolean divertedSuccess, @NotNull Boolean active, String statusChangeReason,
            String sentenceStatus, CommentType comment, Set<Long> chargeIds, Set<Long> conditionIds, Set<Long> orderSentenceIds, Set<Long> caseInfoIds,
            Set<Long> caseActivityIds, boolean saveAsSecondaryIdentifier) {
        this.caseInfoId = caseInfoId;
        this.caseIdentifiers = caseIdentifiers;
        this.caseCategory = caseCategory;
        this.caseType = caseType;
        this.createdDate = createdDate;
        this.issuedDate = issuedDate;
        this.facilityId = facilityId;
        this.supervisionId = supervisionId;
        this.diverted = diverted;
        this.divertedSuccess = divertedSuccess;
        this.active = active;
        this.statusChangeReason = statusChangeReason;
        this.sentenceStatus = sentenceStatus;
        this.comment = comment;
        this.chargeIds = chargeIds;
        this.conditionIds = conditionIds;
        this.orderSentenceIds = orderSentenceIds;
        this.caseInfoIds = caseInfoIds;
        this.caseActivityIds = caseActivityIds;
        this.saveAsSecondaryIdentifier = saveAsSecondaryIdentifier;
    }

    /**
     * Gets the value of the caseInfoId property.
     *
     * @return Long - the unique Id for each case information record.
     */
    public Long getCaseInfoId() {
        return caseInfoId;
    }

    /**
     * Sets the value of the caseInfoId property.
     *
     * @param caseInfoId Long - the unique Id for each case information record.
     */
    public void setCaseInfoId(Long caseInfoId) {
        this.caseInfoId = caseInfoId;
    }

    /**
     * Gets the value of the caseIdentifiers property.
     *
     * @return List<CaseIdentifierType> - A set of numbers/identifiers recorded for a case
     */
    public List<CaseIdentifierType> getCaseIdentifiers() {
        if (caseIdentifiers == null) {
            caseIdentifiers = new ArrayList<CaseIdentifierType>();
        }
        return caseIdentifiers;
    }

    /**
     * Sets the value of the caseIdentifiers property.
     *
     * @param caseIdentifiers List<CaseIdentifierType> - A set of numbers/identifiers recorded for a case
     */
    public void setCaseIdentifiers(List<CaseIdentifierType> caseIdentifiers) {
        this.caseIdentifiers = caseIdentifiers;
    }

    /**
     * Gets the value of the caseCategory property. It is a code value of CaseCategory set.
     *
     * @return String - The category of the case, e.g. InJurisdiction case, Out of Jurisdiction case.
     */
    public String getCaseCategory() {
        return caseCategory;
    }

    /**
     * Sets the value of the caseCategory property. It is a code value of CaseCategory set.
     *
     * @param caseCategory String - The category of the case, e.g. InJurisdiction case, Out of Jurisdiction case.
     */
    public void setCaseCategory(String caseCategory) {
        this.caseCategory = caseCategory;
    }

    /**
     * Gets the value of the caseType property. It is a code value of CaseType set.
     *
     * @return String - Indicates the type of case eg. immigration, traffic, criminal
     */
    public String getCaseType() {
        return caseType;
    }

    /**
     * Sets the value of the caseType property. It is a code value of CaseType set.
     *
     * @param caseType String - Indicates the type of case eg. immigration, traffic, criminal
     */
    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    /**
     * Gets the value of the createdDate property.
     *
     * @return Date - The date when the case was first created
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * Sets the value of the createdDate property.
     *
     * @param createdDate Date - The date when the case was first created
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * Gets the value of the issuedDate property.
     *
     * @return Date - The date on which the case was issued (by the court)
     */
    public Date getIssuedDate() {
        return issuedDate;
    }

    /**
     * Sets the value of the issuedDate property.
     *
     * @param issuedDate Date - The date on which the case was issued (by the court)
     */
    public void setIssuedDate(Date issuedDate) {
        this.issuedDate = issuedDate;
    }

    /**
     * Gets the value of the facilityId property.
     *
     * @return Long - Used to indicate the originating facility name, type and location for a case. Static reference to the Facility service.
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * Sets the value of the facilityId property.
     *
     * @param facilityId Long - Used to indicate the originating facility name, type and location for a case. Static reference to the Facility service.
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * Gets the value of the diverted property.
     *
     * @return Boolean - Indicates whether the case was attempted to be diverted
     */
    public Boolean isDiverted() {
        return diverted;
    }

    /**
     * Sets the value of the diverted property.
     *
     * @param diverted Boolean - Indicates whether the case was attempted to be diverted
     */
    public void setDiverted(Boolean diverted) {
        this.diverted = diverted;
    }

    /**
     * Gets the value of the divertedSuccess property.
     *
     * @return Boolean - Indicates whether a case diversion (if attempted) was successful or unsuccessful.
     */
    public Boolean isDivertedSuccess() {
        return divertedSuccess;
    }

    /**
     * Sets the value of the divertedSuccess property.
     *
     * @param divertedSuccess Boolean - Indicates whether a case diversion (if attempted) was successful or unsuccessful.
     */
    public void setDivertedSuccess(Boolean divertedSuccess) {
        this.divertedSuccess = divertedSuccess;
    }

    /**
     * Gets the value of the active property.
     *
     * @return Boolean - Indicates whether the case is active or inactive. This is specifically set by the user, not derived.
     */
    public Boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     *
     * @param active Boolean - Indicates whether the case is active or inactive. This is specifically set by the user, not derived.
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * Gets the value of the statusChangeReason property. It is a code value of CaseStatusChangeReason set.
     *
     * @return String - Indicates the reason a case status was changed
     */
    public String getStatusChangeReason() {
        return statusChangeReason;
    }

    /**
     * Sets the value of the reason property. It is a code value of CaseStatusChangeReason set.
     *
     * @param statusChangeReason String - Indicates the reason a case status was changed
     */
    public void setStatusChangeReason(String reason) {
        this.statusChangeReason = reason;
    }

    /**
     * Gets the value of the sentenceStatus property. It is a code value of SentenceStatus set.
     *
     * @return String - Indicates whether the offender was sentenced to be incarcerated
     */
    public String getSentenceStatus() {
        return sentenceStatus;
    }

    /**
     * Sets the value of the sentenceStatus property. It is a code value of SentenceStatus set.
     *
     * @param sentenceStatus String - Indicates whether the offender was sentenced to be incarcerated
     */
    public void setSentenceStatus(String sentenceStatus) {
        this.sentenceStatus = sentenceStatus;
    }

    /**
     * Gets the value of the comment property.
     *
     * @return CommentType - The comments associated to a Case
     */
    public CommentType getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     *
     * @param comment CommentType - The comments associated to a Case
     */
    public void setComment(CommentType comment) {
        this.comment = comment;
    }

    /**
     * Gets the value of the chargeIds property.
     *
     * @return Set<Long> - a set of charge ids which associated to a case
     */
    public Set<Long> getChargeIds() {
        if (chargeIds == null) {
            chargeIds = new HashSet<Long>();
        }
        return chargeIds;
    }

    /**
     * Sets the value of the chargeIds property.
     *
     * @param chargeIds Set<Long> - a set of charge ids which associated to a case
     */
    public void setChargeIds(Set<Long> chargeIds) {
        this.chargeIds = chargeIds;
    }

    /**
     * Gets the value of the conditionIds property.
     *
     * @return Set<Long> - a set of condition ids which associated to a case
     */
    public Set<Long> getConditionIds() {
        if (conditionIds == null) {
            conditionIds = new HashSet<Long>();
        }
        return conditionIds;
    }

    /**
     * Sets the value of the conditionIds property.
     *
     * @param conditionIds Set<Long> - a set of condition ids which associated to a case
     */
    public void setConditionIds(Set<Long> conditionIds) {
        this.conditionIds = conditionIds;
    }

    /**
     * Gets the value of the orderSentenceIds property.
     *
     * @return Set<Long> - a set of order sentence ids which associated to a case
     */
    public Set<Long> getOrderSentenceIds() {
        if (orderSentenceIds == null) {
            orderSentenceIds = new HashSet<Long>();
        }
        return orderSentenceIds;
    }

    /**
     * Sets the value of the orderSentenceIds property.
     *
     * @param orderSentenceIds Set<Long> - a set of order sentence ids which associated to a case
     */
    public void setOrderSentenceIds(Set<Long> orderSentenceIds) {
        this.orderSentenceIds = orderSentenceIds;
    }

    /**
     * Gets the value of the caseInfoIds property.
     *
     * @return Set<Long> - a set of case information ids which associated to a case
     */
    public Set<Long> getCaseInfoIds() {
        if (caseInfoIds == null) {
            caseInfoIds = new HashSet<Long>();
        }
        return caseInfoIds;
    }

    /**
     * Sets the value of the caseInfoIds property.
     *
     * @param caseInfoIds Set<Long> - a set of case information ids which associated to a case
     */
    public void setCaseInfoIds(Set<Long> caseInfoIds) {
        this.caseInfoIds = caseInfoIds;
    }

    /**
     * a set of case activity ids which associated to a case
     *
     * @return the caseActivityIds
     */
    public Set<Long> getCaseActivityIds() {
        if (caseActivityIds == null) {
            caseActivityIds = new HashSet<Long>();
        }
        return caseActivityIds;
    }

    /**
     * a set of case activity ids which associated to a case
     *
     * @param caseActivityIds the caseActivityIds to set
     */
    public void setCaseActivityIds(Set<Long> caseActivityIds) {
        this.caseActivityIds = caseActivityIds;
    }

    /**
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * @return the saveAsSecondaryIdentifier
     */
    public boolean isSaveAsSecondaryIdentifier() {
        return saveAsSecondaryIdentifier;
    }

    /**
     * When update primary case identifier, if this value is true, then service saves the old primary case identifier to secondary identifier list.
     *
     * @param saveAsSecondaryIdentifier the saveAsSecondaryIdentifier to set
     */
    public void setSaveAsSecondaryIdentifier(boolean saveAsSecondaryIdentifier) {
        this.saveAsSecondaryIdentifier = saveAsSecondaryIdentifier;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseInfoType [caseInfoId=" + caseInfoId + ", caseIdentifiers=" + caseIdentifiers + ", caseCategory=" + caseCategory + ", caseType=" + caseType
                + ", createdDate=" + createdDate + ", issuedDate=" + issuedDate + ", facilityId=" + facilityId + ", supervisionId=" + supervisionId + ", diverted="
                + diverted + ", divertedSuccess=" + divertedSuccess + ", active=" + active + ", statusChangeReason=" + statusChangeReason + ", sentenceStatus="
                + sentenceStatus + ", comment=" + comment + ", chargeIds=" + chargeIds + ", conditionIds=" + conditionIds + ", orderSentenceIds=" + orderSentenceIds
                + ", caseInfoIds=" + caseInfoIds + ", caseActivityIds=" + caseActivityIds + ", saveAsSecondaryIdentifier=" + saveAsSecondaryIdentifier + "]";
    }
}
