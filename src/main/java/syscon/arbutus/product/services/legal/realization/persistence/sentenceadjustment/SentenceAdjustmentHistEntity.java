package syscon.arbutus.product.services.legal.realization.persistence.sentenceadjustment;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * SentenceAdjustmentHistEntity for Sentence Adjustment
 *
 * @DbComment LEG_SentAdjustHst 'Legal Sentence Adjustment History Table'
 * User: yshang
 * Date: 03/10/13
 * Time: 1:37 PM
 */
//@Audited
@Entity
@Table(name = "LEG_SentAdjustHst")
public class SentenceAdjustmentHistEntity implements Serializable {

    private static final long serialVersionUID = 4199757557415040285L;

    /**
     * @DbComment sentAdjustHstId 'Adjustment Id, system generated.'
     */
    @Id
    @Column(name = "sentAdjustHstId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_SENTADJUSTHST_ID")
    @SequenceGenerator(name = "SEQ_LEG_SENTADJUSTHST_ID", sequenceName = "SEQ_LEG_SENTADJUSTHST_ID", allocationSize = 1)
    private Long historyId;
    /**
     * @DbComment sentenceAdjustmentId 'Adjustment Id, system generated.'
     */
    @Column(name = "sentenceAdjustmentId", nullable = false)
    private Long sentenceAdjustmentId;

    /**
     * @DbComment supervisionId 'Supervision Id'
     */
    @Column(name = "supervisionId", nullable = false)
    private Long supervisionId;

    @ForeignKey(name = "Fk_LEG_SentAdjustH")
    // @OneToMany(mappedBy = "sentenceAdjust", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @OneToMany(mappedBy = "sentenceAdjust", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @BatchSize(size = 20)
    private Set<AdjustmentHistEntity> adjustments;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    public SentenceAdjustmentHistEntity() {
        super();
    }

    /**
     * @return the historyId
     */
    public Long getHistoryId() {
        return historyId;
    }

    /**
     * @param historyId the historyId to set
     */
    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    /**
     * @return the sentenceAdjustmentId
     */
    public Long getSentenceAdjustmentId() {
        return sentenceAdjustmentId;
    }

    /**
     * @param sentenceAdjustmentId the sentenceAdjustmentId to set
     */
    public void setSentenceAdjustmentId(Long sentenceAdjustmentId) {
        this.sentenceAdjustmentId = sentenceAdjustmentId;
    }

    public Long getSupervisionId() {
        return supervisionId;
    }

    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    public Set<AdjustmentHistEntity> getAdjustments() {
        if (adjustments == null) {
			adjustments = new HashSet<>();
		}
        return adjustments;
    }

    public void setAdjustments(Set<AdjustmentHistEntity> adjustments) {
        if (adjustments != null) {
            for (AdjustmentHistEntity adjustment : adjustments) {
                adjustment.setSentenceAdjust(this);
            }
            this.adjustments = adjustments;
        } else {
			this.adjustments = new HashSet<>();
		}
    }

    public void addAdjustment(AdjustmentHistEntity adjustment) {
        adjustment.setSentenceAdjust(this);
        getAdjustments().add(adjustment);
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((adjustments == null) ? 0 : adjustments.hashCode());
        result = prime * result + ((historyId == null) ? 0 : historyId.hashCode());
        result = prime * result + ((sentenceAdjustmentId == null) ? 0 : sentenceAdjustmentId.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SentenceAdjustmentHistEntity other = (SentenceAdjustmentHistEntity) obj;
        if (adjustments == null) {
            if (other.adjustments != null) {
				return false;
			}
        } else if (!adjustments.equals(other.adjustments)) {
			return false;
		}
        if (historyId == null) {
            if (other.historyId != null) {
				return false;
			}
        } else if (!historyId.equals(other.historyId)) {
			return false;
		}
        if (sentenceAdjustmentId == null) {
            if (other.sentenceAdjustmentId != null) {
				return false;
			}
        } else if (!sentenceAdjustmentId.equals(other.sentenceAdjustmentId)) {
			return false;
		}
        if (supervisionId == null) {
            if (other.supervisionId != null) {
				return false;
			}
        } else if (!supervisionId.equals(other.supervisionId)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SentenceAdjustmentHistEntity [historyId=" + historyId + ", sentenceAdjustmentId=" + sentenceAdjustmentId + ", supervisionId=" + supervisionId
                + ", adjustments=" + adjustments + "]";
    }

}
