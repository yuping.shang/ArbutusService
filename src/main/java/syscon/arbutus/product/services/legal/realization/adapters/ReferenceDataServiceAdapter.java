package syscon.arbutus.product.services.legal.realization.adapters;

import java.util.ArrayList;
import java.util.List;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.ReferenceCodeType;
import syscon.arbutus.product.services.core.contract.dto.ReferenceCodesRetrieveType;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.referencedata.contract.interfaces.ReferenceDataService;
import syscon.arbutus.product.services.utility.ServiceAdapter;

/**
 * An adapter class to wrap the API in ReferenceDataService and provide the legal service bean customized API. *
 *
 * @author lhan
 */
public class ReferenceDataServiceAdapter {
    private static ReferenceDataService service;

    public ReferenceDataServiceAdapter() {
        super();
    }

    synchronized private static ReferenceDataService getService() {
        if (service == null) {
            service = (ReferenceDataService) ServiceAdapter.JNDILookUp(service, ReferenceDataService.class);
        }
        return service;
    }

    public List<ReferenceCodeType> getReferenceCodesForLink(UserContext uc, CodeType linkSetCode, Boolean activeFlag, String languageCode) {
        List<ReferenceCodeType> ret = getService().getReferenceCodesForLink(uc, linkSetCode, activeFlag, languageCode);
        return ret;
    }

    public List<String> getReferenceCodesBySet(UserContext uc, String referenceSet, Boolean activeFlag) {
        if (BeanHelper.isEmpty(referenceSet)) {
			return new ArrayList<>();
		}

        ReferenceCodesRetrieveType referenceCodesRetrieve = new ReferenceCodesRetrieveType();
        referenceCodesRetrieve.setSet(referenceSet);
        referenceCodesRetrieve.setActiveFlag(activeFlag);
        List<ReferenceCodeType> referenceCodeTypeList = getService().getReferenceCodes(uc, referenceCodesRetrieve);

        List<String> codes = new ArrayList<>();
        if (referenceCodeTypeList != null) {
            for (ReferenceCodeType referenceCode : referenceCodeTypeList) {
                codes.add(referenceCode.getCode());
            }
        }
        return codes;
    }

}
