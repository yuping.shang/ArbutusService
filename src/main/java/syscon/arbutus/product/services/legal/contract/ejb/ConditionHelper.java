package syscon.arbutus.product.services.legal.contract.ejb;

import java.util.*;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.legal.contract.dto.LegalModule;
import syscon.arbutus.product.services.legal.contract.dto.condition.ConditionType;
import syscon.arbutus.product.services.legal.contract.dto.condition.ConfigureConditionType;
import syscon.arbutus.product.services.legal.realization.persistence.condition.*;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * @author wmadruga
 */
public class ConditionHelper {

    public static ConditionEntity toConditionEntity(ConditionType dto, StampEntity createStamp) {
        if (dto == null) {
			return null;
		}

        ConditionEntity ret = new ConditionEntity(dto.getConditionId(), dto.getConditionCreateDate(), dto.getConditionStartDate(), dto.getConditionEndDate(), null,
                (dto.getConditionType()), (dto.getConditionCategory()), (dto.getConditionSubCategory()), (dto.getConditionPeriodUnit()), (dto.getConditionDistanceUnit()),
                (dto.getConditionCurrencyUnit()), dto.getConditionPeriodValue(), dto.getConditionDistanceValue(), dto.getConditionCurrencyAmount(),
                dto.getConditionDetails(), dto.getConditionAmendmentDate(), (dto.getConditionAmendmentStatus()), dto.getIsConditionSatisfied(),
                dto.getIsConditionStatus(), createStamp);

        // Comments
        ret.setConditionComments(toConditionCommentEntity(ret, dto.getConditionComments(), createStamp));

        // Module association
        ret.setModuleAssociations(toConditionModuleAssociationEntitySet(dto, ret, createStamp));
        ret.setShortName(dto.getShortName());

        return ret;
    }

    private static Set<ConditionModuleAssociationEntity> toConditionModuleAssociationEntitySet(ConditionType dto, ConditionEntity entity, StampEntity createStamp) {

        if (dto == null) {
			return null;
		}

        Set<ConditionModuleAssociationEntity> ret = new HashSet<ConditionModuleAssociationEntity>();

        Long instanceId = dto.getConditionId();

        //Add Charge related
        if (dto.getChargeIds().size() > 0) {
            Set<ConditionModuleAssociationEntity> chargeAssociations = toConditionModuleAssociationEntitySet(entity, instanceId, LegalModule.CHARGE.value(),
                    dto.getChargeIds(), createStamp);
            ret.addAll(chargeAssociations);
        }

        //Add Order Sentence related
        if (dto.getOrderSentenceId() != null) {
            Set<Long> orderSentenceId = new HashSet<Long>();
            orderSentenceId.add(dto.getOrderSentenceId());

            Set<ConditionModuleAssociationEntity> orderSentenceAssociations = toConditionModuleAssociationEntitySet(entity, instanceId,
                    LegalModule.ORDER_SENTENCE.value(), orderSentenceId, createStamp);
            ret.addAll(orderSentenceAssociations);
        }

        //Add Case Info related
        if (dto.getCaseInfoIds().size() > 0) {
            Set<ConditionModuleAssociationEntity> caseInfoAssociations = toConditionModuleAssociationEntitySet(entity, instanceId, LegalModule.CASE_INFORMATION.value(),
                    dto.getCaseInfoIds(), createStamp);
            ret.addAll(caseInfoAssociations);
        }

        //Add Case Plan related
        if (dto.getCasePlanId() != null) {
            ConditionModuleAssociationEntity casePlanAssociation = toConditionModuleAssociationEntity(entity, instanceId, 0l, LegalModule.CASE_PLAN.value(),
                    dto.getCasePlanId(), createStamp);
            ret.add(casePlanAssociation);
        }
        return ret;

    }

    private static Set<ConditionModuleAssociationEntity> toConditionModuleAssociationEntitySet(ConditionEntity condition, Long fromIdentifier, String toClass,
            Set<Long> ids, StampEntity createStamp) {

        if (ids == null || toClass == null) {
			return null;
		}

        Set<ConditionModuleAssociationEntity> ret = new HashSet<ConditionModuleAssociationEntity>();

        for (Long toIdentifier : ids) {
            if (toIdentifier != null) {
                ConditionModuleAssociationEntity entity = toConditionModuleAssociationEntity(condition, null, fromIdentifier, toClass, toIdentifier, createStamp);
                ret.add(entity);
            }
        }

        return ret;

    }

    public static ConditionModuleAssociationEntity toConditionModuleAssociationEntity(ConditionEntity condition, Long associationId, Long fromIdentifier, String toClass,
            Long toIdentifier, StampEntity createStamp) {

        if (toClass == null || toIdentifier == null) {
			return null;
		}

        ConditionModuleAssociationEntity ret = new ConditionModuleAssociationEntity();
        ret.setAssociationId(associationId);
        ret.setToClass(toClass);
        ret.setToIdentifier(toIdentifier);
        ret.setCondition(condition);
        ret.setStamp(createStamp);
        return ret;

    }

    public static Set<ConditionCommentEntity> toConditionCommentEntity(ConditionEntity condition, Set<CommentType> dtoComments, StampEntity stamp) {

        if (dtoComments == null) {
			return null;
		}
        Set<ConditionCommentEntity> ret = new HashSet<ConditionCommentEntity>();

        for (CommentType comment : dtoComments) {
            ConditionCommentEntity c = new ConditionCommentEntity(comment.getCommentIdentification(), comment.getUserId(), comment.getCommentDate(), comment.getComment(),
                    stamp);

            c.setCondition(condition);
            ret.add(c);
        }

        return ret;
    }

    public static ConditionType toConditionType(ConditionEntity entity) {

        if (entity == null) {
			return null;
		}

        ConditionType ret = new ConditionType(entity.getConditionId(), entity.getConditionCreateDate(), entity.getConditionStartDate(), entity.getConditionEndDate(),
                toCommentTypeSet(entity.getConditionComments()), entity.isActive(), entity.getConditionType(), entity.getConditionCategory(),
                entity.getConditionSubCategory(), entity.getConditionPeriodUnit(), entity.getConditionDistanceUnit(), entity.getConditionCurrencyUnit(),
                entity.getConditionPeriodValue(), entity.getConditionDistanceValue(), entity.getConditionCurrencyAmount(), entity.getConditionDetails(),
                entity.getIsConditionSatisfied(), entity.getConditionAmendmentDate(), entity.getConditionAmendmentStatus(),
                toInstanceIdFromModule(LegalModule.ORDER_SENTENCE.value(), entity.getModuleAssociations()),
                toInstanceIdsFromModule(LegalModule.CHARGE.value(), entity.getModuleAssociations()),
                toInstanceIdsFromModule(LegalModule.CASE_INFORMATION.value(), entity.getModuleAssociations()));

        ret.setHistorical(false);
        ret.setShortName(entity.getShortName());
        ret.setCasePlanId(toInstanceIdFromModule(LegalModule.CASE_PLAN.value(), entity.getModuleAssociations()));
        return ret;

    }

    private static Set<CommentType> toCommentTypeSet(Set<ConditionCommentEntity> conditionComments) {
        if (conditionComments == null) {
			return null;
		}

        Set<CommentType> ret = new HashSet<CommentType>();

        for (ConditionCommentEntity cc : conditionComments) {
            CommentType comment = new CommentType();
            comment.setCommentIdentification(cc.getCommentId());
            comment.setCommentDate(cc.getCommentDate());
            comment.setComment(cc.getCommentText());
            comment.setUserId(cc.getCommentUserId());
            ret.add(comment);
        }

        return ret;
    }

    private static Set<CommentType> toCommentTypeSetFromHistory(Set<ConditionCommentHistoryEntity> conditionCommentsHist) {
        if (conditionCommentsHist == null) {
			return null;
		}

        Set<CommentType> ret = new HashSet<CommentType>();

        for (ConditionCommentHistoryEntity cc : conditionCommentsHist) {
            CommentType comment = new CommentType();
            comment.setCommentIdentification(cc.getCommentId());
            comment.setCommentDate(cc.getCommentDate());
            comment.setComment(cc.getCommentText());
            comment.setUserId(cc.getCommentUserId());
            ret.add(comment);
        }

        return ret;
    }

    public static Set<Long> toInstanceIdsFromModule(String toClass, Set<ConditionModuleAssociationEntity> entities) {

        if (entities == null || toClass == null) {
			return null;
		}

        Set<Long> ret = new HashSet<Long>();

        for (ConditionModuleAssociationEntity entity : entities) {

            if (entity != null) {
                if (entity.getToClass().equalsIgnoreCase(toClass)) {
                    ret.add(entity.getToIdentifier());
                }
            }
        }

        return ret;
    }

    private static Long toInstanceIdFromModule(String toClass, Set<ConditionModuleAssociationEntity> entities) {

        Set<Long> ret = toInstanceIdsFromModule(toClass, entities);
        if (!ret.isEmpty()) {
            return ret.iterator().next();
        }
        return null;
    }

    public static Set<ConditionModuleAssociationHistEntity> toConditionModuleAssocHistEntity(ConditionHistoryEntity conditionHistory,
            Set<ConditionModuleAssociationEntity> existEntity, StampEntity createStamp) {
        if (existEntity == null) {
			return null;
		}

        Set<ConditionModuleAssociationHistEntity> ret = new HashSet<ConditionModuleAssociationHistEntity>();

        for (ConditionModuleAssociationEntity entityToHistory : existEntity) {
            ConditionModuleAssociationHistEntity history = new ConditionModuleAssociationHistEntity();

            history.setConditionHistory(conditionHistory);
            history.setToClass(entityToHistory.getToClass());
            history.setToIdentifier(entityToHistory.getToIdentifier());
            history.setFromIdentifier(conditionHistory.getConditionId());
            history.setStamp(createStamp);
            ret.add(history);
        }

        return ret;

    }

    public static ConditionHistoryEntity toConditionHistoryEntity(ConditionEntity existEntity, StampEntity createStamp) {

        if (existEntity == null) {
			return null;
		}

        ConditionHistoryEntity ret = new ConditionHistoryEntity();

        ret.setConditionAmendmentDate(existEntity.getConditionAmendmentDate());
        ret.setConditionAmendmentStatus(existEntity.getConditionAmendmentStatus());
        ret.setConditionCategory(existEntity.getConditionCategory());
        ret.setConditionComments(toConditionCommentHistoryEntitySet(existEntity.getConditionComments(), ret, createStamp));
        ret.setConditionCreateDate(existEntity.getConditionCreateDate());
        ret.setConditionCurrencyAmount(existEntity.getConditionCurrencyAmount());
        ret.setConditionCurrencyUnit(existEntity.getConditionCurrencyUnit());
        ret.setConditionDetails(existEntity.getConditionDetails());
        ret.setConditionDistanceUnit(existEntity.getConditionDistanceUnit());
        ret.setConditionDistanceValue(existEntity.getConditionDistanceValue());
        ret.setConditionEndDate(existEntity.getConditionEndDate());
        ret.setConditionId(existEntity.getConditionId());
        ret.setConditionPeriodUnit(existEntity.getConditionPeriodUnit());
        ret.setConditionPeriodValue(existEntity.getConditionPeriodValue());
        ret.setConditionStartDate(existEntity.getConditionStartDate());
        ret.setConditionSubCategory(existEntity.getConditionSubCategory());
        ret.setConditionType(existEntity.getConditionType());
        ret.setIsConditionSatisfied(existEntity.getIsConditionSatisfied());
        ret.setStamp(createStamp);
        ret.setShortName(existEntity.getShortName());
        return ret;
    }

    private static Set<ConditionCommentHistoryEntity> toConditionCommentHistoryEntitySet(Set<ConditionCommentEntity> conditionComments,
            ConditionHistoryEntity condHistEnt, StampEntity createStamp) {

        if (conditionComments == null) {
			return null;
		}

        Set<ConditionCommentHistoryEntity> ret = new HashSet<ConditionCommentHistoryEntity>();

        for (ConditionCommentEntity comment : conditionComments) {
            ConditionCommentHistoryEntity history = new ConditionCommentHistoryEntity();
            history.setCommentDate(comment.getCommentDate());
            history.setCommentId(comment.getCommentId());
            history.setCommentText(comment.getCommentText());
            history.setCommentUserId(comment.getCommentUserId());
            history.setCondition(condHistEnt);
            history.setStamp(createStamp);

            ret.add(history);
        }
        return ret;
    }

    public static ConditionEntity updateExistConditionEntity(ConditionEntity existEntity, ConditionEntity newEntity) {

        //Elements which are single object.
        existEntity.setActive(newEntity.isActive());
        existEntity.setConditionAmendmentDate(newEntity.getConditionAmendmentDate());
        existEntity.setConditionAmendmentStatus(newEntity.getConditionAmendmentStatus());
        existEntity.setConditionCategory(newEntity.getConditionCategory());
        existEntity.setConditionCreateDate(newEntity.getConditionCreateDate());
        existEntity.setConditionCurrencyAmount(newEntity.getConditionCurrencyAmount());
        existEntity.setConditionCurrencyUnit(newEntity.getConditionCurrencyUnit());
        existEntity.setConditionDetails(newEntity.getConditionDetails());
        existEntity.setConditionDistanceUnit(newEntity.getConditionDistanceUnit());
        existEntity.setConditionDistanceValue(newEntity.getConditionDistanceValue());
        existEntity.setConditionEndDate(newEntity.getConditionEndDate());
        existEntity.setConditionId(newEntity.getConditionId());
        existEntity.setConditionPeriodUnit(newEntity.getConditionPeriodUnit());
        existEntity.setConditionPeriodValue(newEntity.getConditionPeriodValue());
        existEntity.setConditionStartDate(newEntity.getConditionStartDate());
        existEntity.setConditionSubCategory(newEntity.getConditionSubCategory());
        existEntity.setConditionType(newEntity.getConditionType());
        existEntity.setIsConditionSatisfied(newEntity.getIsConditionSatisfied());
        existEntity.setShortName(newEntity.getShortName());
        existEntity.getModuleAssociations().clear();
        for (ConditionModuleAssociationEntity moduleAssoc : newEntity.getModuleAssociations()) {
            existEntity.addModuleAssociation(moduleAssoc);
        }

        return existEntity;

    }

    public static Set<ConditionType> toConditionTypeSet(Set<ConditionHistoryEntity> conditionHistories,
            Set<ConditionModuleAssociationHistEntity> conditionModuleHistories, ConditionEntity currentEntity) {
        Set<ConditionType> rtn = new HashSet<ConditionType>();

        if (currentEntity != null) {
            ConditionType current = toConditionType(currentEntity);
            rtn.add(current);
        }

        if (conditionHistories != null) {
            for (ConditionHistoryEntity historyEntity : conditionHistories) {
                ConditionType historyType = toConditionType(historyEntity, conditionModuleHistories);
                rtn.add(historyType);
            }
        }

        return rtn;
    }

    private static ConditionType toConditionType(ConditionHistoryEntity entity, Set<ConditionModuleAssociationHistEntity> conditionModuleHistories) {
        if (entity == null) {
			return null;
		}

        ConditionType ret = new ConditionType();

        ret.setConditionComments(toCommentTypeSetFromHistory(entity.getConditionComments()));

        // Module Associations
        if (conditionModuleHistories != null && !conditionModuleHistories.isEmpty()) {
            Iterator<ConditionModuleAssociationHistEntity> moduleAssociations = conditionModuleHistories.iterator();
            while (moduleAssociations.hasNext()) {
                ConditionModuleAssociationHistEntity moduleAssociationEntity = moduleAssociations.next();
                if (moduleAssociationEntity.getToClass().equals(LegalModule.CASE_INFORMATION.value())) {
                    ret.getCaseInfoIds().add(moduleAssociationEntity.getToIdentifier());
                }
                if (moduleAssociationEntity.getToClass().equals(LegalModule.CHARGE.value())) {
                    ret.getChargeIds().add(moduleAssociationEntity.getToIdentifier());
                }
                if (moduleAssociationEntity.getToClass().equals(LegalModule.ORDER_SENTENCE.value())) {
                    ret.setOrderSentenceId(moduleAssociationEntity.getToIdentifier());
                }
            }
        }

        ret.setHistorical(true);
        ret.setConditionAmendmentDate(entity.getConditionAmendmentDate());
        ret.setConditionAmendmentStatus(entity.getConditionAmendmentStatus());
        ret.setConditionCategory(entity.getConditionCategory());
        ret.setConditionCreateDate(entity.getConditionCreateDate());
        ret.setConditionCurrencyAmount(entity.getConditionCurrencyAmount());
        ret.setConditionCurrencyUnit(entity.getConditionCurrencyUnit());
        ret.setConditionDetails(entity.getConditionDetails());
        ret.setConditionDistanceUnit(entity.getConditionDistanceUnit());
        ret.setConditionDistanceValue(entity.getConditionDistanceValue());
        ret.setConditionEndDate(entity.getConditionEndDate());
        ret.setConditionId(entity.getConditionId());
        ret.setConditionPeriodUnit(entity.getConditionPeriodUnit());
        ret.setConditionPeriodValue(entity.getConditionPeriodValue());
        ret.setConditionStartDate(entity.getConditionStartDate());
        ret.setConditionSubCategory(entity.getConditionSubCategory());
        ret.setConditionType(entity.getConditionType());
        ret.setIsConditionSatisfied(entity.getIsConditionSatisfied());
        ret.setShortName(entity.getShortName());
        return ret;
    }

    public static Set<ConditionType> toConditionTypeSet(Set<ConditionEntity> entities, boolean applyPrivilege, Set<String> userPrivileges) {

        Set<ConditionType> rtn = new HashSet<ConditionType>();

        if (entities != null) {
            for (ConditionEntity currentEntity : entities) {
                ConditionType current = toConditionType(currentEntity);

                rtn.add(current);
            }
        }
        return rtn;
    }

    public static String getCode(CodeType codeType) {
        if (codeType == null) {
			return null;
		}
        return codeType.getCode();
    }

    public static ConfigureConditionEntity toConfigureConditionEntity(ConfigureConditionType type, StampEntity stamp) {
        ConfigureConditionEntity entity = new ConfigureConditionEntity();
        entity.setActivationDate(type.getActivationDate());
        entity.setAttribute(type.getAttribute());
        entity.setAutoPopulateToCasePlan(type.getAutoPopulateToCasePlan() == null ? Boolean.FALSE : type.getAutoPopulateToCasePlan());
        entity.setCategory(type.getCategory());
        entity.setCode(type.getCode());
        entity.setConfigureConditionType(type.getType());
        entity.setDeactivationDate(type.getDeactivationDate());
        entity.setFullDetail(type.getFullDetail());
        entity.setShortName(type.getShortName());
        entity.setStamp(stamp);
        entity.setSubCategory(type.getSubCategory());
        entity.setObjective(type.getObjective());
        entity.setCasePlanType(type.getCasePlanType());
        return entity;
    }

    public static ConfigureConditionType toConfigureConditionType(ConfigureConditionEntity entity) {
        ConfigureConditionType type = new ConfigureConditionType();
        type.setId(entity.getConfigureConditionId());
        type.setActivationDate(entity.getActivationDate());
        type.setAttribute(entity.getAttribute());
        type.setAutoPopulateToCasePlan(entity.getAutoPopulateToCasePlan());
        type.setCategory(entity.getCategory());
        type.setCode(entity.getCode());
        type.setType(entity.getConfigureConditionType());
        type.setDeactivationDate(entity.getDeactivationDate());
        type.setFullDetail(entity.getFullDetail());
        type.setShortName(entity.getShortName());
        type.setSubCategory(entity.getSubCategory());
        type.setObjective(entity.getObjective());
        type.setCasePlanType(entity.getCasePlanType());
        return type;
    }

    public static void modifyConfigureConditionEntity(ConfigureConditionEntity entity, ConfigureConditionType type) {
        entity.setDeactivationDate(type.getDeactivationDate());
        entity.setFullDetail(type.getFullDetail());
        entity.setAutoPopulateToCasePlan(type.getAutoPopulateToCasePlan() == null ? Boolean.FALSE : type.getAutoPopulateToCasePlan());
        entity.setObjective(type.getObjective());
        entity.setCasePlanType(type.getCasePlanType());
    }

    public static List<ConfigureConditionType> toConfigureConditionTypes(UserContext userContext, List<ConfigureConditionEntity> result) {
        List<ConfigureConditionType> configureConditions = new ArrayList<ConfigureConditionType>();
        for (ConfigureConditionEntity entity : result) {
            configureConditions.add(toConfigureConditionType(entity));
        }
        return configureConditions;
    }

}