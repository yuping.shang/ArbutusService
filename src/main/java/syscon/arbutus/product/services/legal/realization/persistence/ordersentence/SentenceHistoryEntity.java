package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;

/**
 * SentenceHistoryEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdOrderHst 'Order History table for Order Sentence module of Legal service'
 * @since December 21, 2012
 */
//@Audited
@Entity
@DiscriminatorValue("SentenceHst")
public class SentenceHistoryEntity extends OrderHistoryEntity implements Serializable {

    private static final long serialVersionUID = -737932575118522735L;

    /**
     * @DbComment sentenceType 'The type of sentence (Definite, Split, Life etc.,)'
     */
    @Column(name = "sentenceType", nullable = true, length = 64) // business requirement is nullable = false
    private String sentenceType;

    /**
     * @DbComment sentenceNumber 'Identifier for the sentence. Will be used  on the UI to specify the sentence relationship (concecutive)'
     */
    @Column(name = "sentenceNumber", nullable = true) // business requirement is nullable = false
    private Long sentenceNumber;

    @ForeignKey(name = "Fk_LEG_OrdSntHst1")
    @OneToMany(mappedBy = "sentence", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<TermHistoryEntity> sentenceTerms;

    /**
     * @DbComment sentenceStatus 'The status of a sentence, will be used by sentence calculation logic. Pending/Included/Excluded. Only ‘Included’ sentences will participate in sentence calculation.'
     */
    @Column(name = "sentenceStatus", nullable = true, length = 64) // business requirement is nullable = false
    private String sentenceStatus;

    /**
     * @DbComment sentenceStartDate 'The start date/time for the sentence.'
     */
    @Column(name = "sentenceStartDate", nullable = true)
    private Date sentenceStartDate;

    /**
     * @DbComment sentenceEndDate 'The date on which the sentence ends.'
     */
    @Column(name = "sentenceEndDate", nullable = true)
    private Date sentenceEndDate;

    /**
     * @DbComment sntenceAppealId 'Foreign key from table LEG_OrdSNTAppeal'
     */
    @ForeignKey(name = "Fk_LEG_OrdSntHst2")
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "sntenceAppealId")
    private SentenceAppealHistoryEntity sentenceAppeal;

    /**
     * @DbComment concecutiveSentenceAssoc 'The static reference to the ordersentence service that relates to a sentence that this order (sentence) is concecutive to.'
     */
    @Column(name = "concecutiveSentenceAssoc", nullable = true)
    private Long concecutiveSentenceAssociation;

    /**
     * @DbComment fineAmount 'The fine for a sentence. The unit would be the system wide currency defined.'
     */
    @Column(name = "fineAmount", nullable = true, precision = 12, scale = 2)
    private BigDecimal fineAmount;

    /**
     * @DbComment fineAmountPaid 'If true, Fine amount is paid, false otherwise. Default to false.'
     */
    @Column(name = "fineAmountPaid", nullable = true) // business requirement is nullable = false
    private Boolean fineAmountPaid;

    /**
     * @DbComment sentenceAggravateFlag 'True if aggravating factors were considered during sentencing, false otherwise.'
     */
    @Column(name = "sentenceAggravateFlag", nullable = true)
    private Boolean sentenceAggravateFlag;

    @ForeignKey(name = "Fk_LEG_OrdSntHst3")
    @OneToMany(mappedBy = "sentence", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<IntermittentScheduleHistEntity> intermittentSchedule;

    @ForeignKey(name = "Fk_LEG_OrdSntHst4")
    @OneToMany(mappedBy = "sentence", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<IntermittentSentenceScheduleHistEntity> intermittentSentenceSchedules;

    @ForeignKey(name = "FK_LEG_OrdFinePaymentH_SEN")
    @OneToMany(mappedBy = "sentence", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<FinePaymentHistEntity> finePayments = new HashSet<FinePaymentHistEntity>();

    /**
     * @return the sentenceType
     */
    public String getSentenceType() {
        return sentenceType;
    }

    /**
     * @param sentenceType the sentenceType to set
     */
    public void setSentenceType(String sentenceType) {
        this.sentenceType = sentenceType;
    }

    /**
     * @return the sentenceNumber
     */
    public Long getSentenceNumber() {
        return sentenceNumber;
    }

    /**
     * @param sentenceNumber the sentenceNumber to set
     */
    public void setSentenceNumber(Long sentenceNumber) {
        this.sentenceNumber = sentenceNumber;
    }

    /**
     * @return the sentenceTerms
     */
    public Set<TermHistoryEntity> getSentenceTerms() {
        if (sentenceTerms == null) {
			sentenceTerms = new HashSet<TermHistoryEntity>();
		}
        return sentenceTerms;
    }

    /**
     * @param sentenceTerms the sentenceTerms to set
     */
    public void setSentenceTerms(Set<TermHistoryEntity> sentenceTerms) {
        if (sentenceTerms != null) {
            for (TermHistoryEntity term : sentenceTerms) {
                term.setSentence(this);
            }
        }
        this.sentenceTerms = sentenceTerms;
    }

    /**
     * @return the sentenceStatus
     */
    public String getSentenceStatus() {
        return sentenceStatus;
    }

    /**
     * @param sentenceStatus the sentenceStatus to set
     */
    public void setSentenceStatus(String sentenceStatus) {
        this.sentenceStatus = sentenceStatus;
    }

    /**
     * @return the sentenceStartDate
     */
    public Date getSentenceStartDate() {
        return sentenceStartDate;
    }

    /**
     * @param sentenceStartDate the sentenceStartDate to set
     */
    public void setSentenceStartDate(Date sentenceStartDate) {
        this.sentenceStartDate = sentenceStartDate;
    }

    /**
     * @return the sentenceEndDate
     */
    public Date getSentenceEndDate() {
        return sentenceEndDate;
    }

    /**
     * @param sentenceEndDate the sentenceEndDate to set
     */
    public void setSentenceEndDate(Date sentenceEndDate) {
        this.sentenceEndDate = sentenceEndDate;
    }

    /**
     * @return the sentenceAppeal
     */
    public SentenceAppealHistoryEntity getSentenceAppeal() {
        return sentenceAppeal;
    }

    /**
     * @param sentenceAppeal the sentenceAppeal to set
     */
    public void setSentenceAppeal(SentenceAppealHistoryEntity sentenceAppeal) {
        this.sentenceAppeal = sentenceAppeal;
    }

    /**
     * @return the concecutiveSentenceAssociation
     */
    public Long getConcecutiveSentenceAssociation() {
        return concecutiveSentenceAssociation;
    }

    /**
     * @param concecutiveSentenceAssociation the concecutiveSentenceAssociation to set
     */
    public void setConcecutiveSentenceAssociation(Long concecutiveSentenceAssociation) {
        this.concecutiveSentenceAssociation = concecutiveSentenceAssociation;
    }

    /**
     * @return the fineAmount
     */
    public BigDecimal getFineAmount() {
        return fineAmount;
    }

    /**
     * @param fineAmount the fineAmount to set
     */
    public void setFineAmount(BigDecimal fineAmount) {
        this.fineAmount = fineAmount;
    }

    /**
     * @return the fineAmountPaid
     */
    public Boolean getFineAmountPaid() {
        return fineAmountPaid;
    }

    /**
     * @param fineAmountPaid the fineAmountPaid to set
     */
    public void setFineAmountPaid(Boolean fineAmountPaid) {
        this.fineAmountPaid = fineAmountPaid;
    }

    /**
     * @return the sentenceAggravateFlag
     */
    public Boolean getSentenceAggravateFlag() {
        return sentenceAggravateFlag;
    }

    /**
     * @param sentenceAggravateFlag the sentenceAggravateFlag to set
     */
    public void setSentenceAggravateFlag(Boolean sentenceAggravateFlag) {
        this.sentenceAggravateFlag = sentenceAggravateFlag;
    }

    /**
     * @return the intermittentSchedule
     */
    public Set<IntermittentScheduleHistEntity> getIntermittentSchedule() {
        if (intermittentSchedule == null) {
			intermittentSchedule = new HashSet<IntermittentScheduleHistEntity>();
		}
        return intermittentSchedule;
    }

    /**
     * @param intermittentSchedule the intermittentSchedule to set
     */
    public void setIntermittentSchedule(Set<IntermittentScheduleHistEntity> intermittentSchedule) {
        if (intermittentSchedule != null) {
            for (IntermittentScheduleHistEntity inter : intermittentSchedule) {
                inter.setSentence(this);
            }
            this.intermittentSchedule = intermittentSchedule;
        } else {
			this.intermittentSchedule = new HashSet<IntermittentScheduleHistEntity>();
		}
    }

    /**
     * @return the intermittentSentenceSchedules
     */
    public Set<IntermittentSentenceScheduleHistEntity> getIntermittentSentenceSchedules() {
        if (intermittentSentenceSchedules == null) {
            intermittentSentenceSchedules = new HashSet<IntermittentSentenceScheduleHistEntity>();
        }
        return intermittentSentenceSchedules;
    }

    /**
     * @param intermittentSentenceSchedules the intermittentSentenceSchedules to set
     */
    public void setIntermittentSentenceSchedules(Set<IntermittentSentenceScheduleHistEntity> intermittentSentenceSchedules) {
        if (intermittentSentenceSchedules != null) {
            for (IntermittentSentenceScheduleHistEntity entity : intermittentSentenceSchedules) {
                entity.setSentence(this);
            }
            this.intermittentSentenceSchedules = intermittentSentenceSchedules;
        } else {
            this.intermittentSentenceSchedules = new HashSet<IntermittentSentenceScheduleHistEntity>();
        }

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((concecutiveSentenceAssociation == null) ? 0 : concecutiveSentenceAssociation.hashCode());
        result = prime * result + ((fineAmountPaid == null) ? 0 : fineAmountPaid.hashCode());
        result = prime * result + ((intermittentSchedule == null) ? 0 : intermittentSchedule.hashCode());
        result = prime * result + ((intermittentSentenceSchedules == null) ? 0 : intermittentSentenceSchedules.hashCode());
        result = prime * result + ((sentenceAggravateFlag == null) ? 0 : sentenceAggravateFlag.hashCode());
        result = prime * result + ((sentenceAppeal == null) ? 0 : sentenceAppeal.hashCode());
        result = prime * result + ((sentenceEndDate == null) ? 0 : sentenceEndDate.hashCode());
        result = prime * result + ((sentenceNumber == null) ? 0 : sentenceNumber.hashCode());
        result = prime * result + ((sentenceStartDate == null) ? 0 : sentenceStartDate.hashCode());
        result = prime * result + ((sentenceStatus == null) ? 0 : sentenceStatus.hashCode());
        result = prime * result + ((sentenceTerms == null) ? 0 : sentenceTerms.hashCode());
        result = prime * result + ((sentenceType == null) ? 0 : sentenceType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SentenceHistoryEntity other = (SentenceHistoryEntity) obj;
        if (concecutiveSentenceAssociation == null) {
            if (other.concecutiveSentenceAssociation != null) {
				return false;
			}
        } else if (!concecutiveSentenceAssociation.equals(other.concecutiveSentenceAssociation)) {
			return false;
		}
        if (fineAmountPaid == null) {
            if (other.fineAmountPaid != null) {
				return false;
			}
        } else if (!fineAmountPaid.equals(other.fineAmountPaid)) {
			return false;
		}
        if (intermittentSchedule == null) {
            if (other.intermittentSchedule != null) {
				return false;
			}
        } else if (!intermittentSchedule.equals(other.intermittentSchedule)) {
			return false;
		}
        if (intermittentSentenceSchedules == null) {
            if (other.intermittentSentenceSchedules != null) {
				return false;
			}
        } else if (!intermittentSentenceSchedules.equals(other.intermittentSentenceSchedules)) {
			return false;
		}
        if (sentenceAggravateFlag == null) {
            if (other.sentenceAggravateFlag != null) {
				return false;
			}
        } else if (!sentenceAggravateFlag.equals(other.sentenceAggravateFlag)) {
			return false;
		}
        if (sentenceAppeal == null) {
            if (other.sentenceAppeal != null) {
				return false;
			}
        } else if (!sentenceAppeal.equals(other.sentenceAppeal)) {
			return false;
		}
        if (sentenceEndDate == null) {
            if (other.sentenceEndDate != null) {
				return false;
			}
        } else if (!sentenceEndDate.equals(other.sentenceEndDate)) {
			return false;
		}
        if (sentenceNumber == null) {
            if (other.sentenceNumber != null) {
				return false;
			}
        } else if (!sentenceNumber.equals(other.sentenceNumber)) {
			return false;
		}
        if (sentenceStartDate == null) {
            if (other.sentenceStartDate != null) {
				return false;
			}
        } else if (!sentenceStartDate.equals(other.sentenceStartDate)) {
			return false;
		}
        if (sentenceStatus == null) {
            if (other.sentenceStatus != null) {
				return false;
			}
        } else if (!sentenceStatus.equals(other.sentenceStatus)) {
			return false;
		}
        if (sentenceTerms == null) {
            if (other.sentenceTerms != null) {
				return false;
			}
        } else if (!sentenceTerms.equals(other.sentenceTerms)) {
			return false;
		}
        if (sentenceType == null) {
            if (other.sentenceType != null) {
				return false;
			}
        } else if (!sentenceType.equals(other.sentenceType)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SentenceHistoryEntity [sentenceType=" + sentenceType + ", sentenceNumber=" + sentenceNumber + ", sentenceTerms=" + sentenceTerms + ", sentenceStatus="
                + sentenceStatus + ", sentenceStartDate=" + sentenceStartDate + ", sentenceEndDate=" + sentenceEndDate + ", sentenceAppeal=" + sentenceAppeal
                + ", concecutiveSentenceAssociation=" + concecutiveSentenceAssociation + ", fineAmount=" + fineAmount + ", fineAmountPaid=" + fineAmountPaid
                + ", sentenceAggravateFlag=" + sentenceAggravateFlag + ", intermittentSchedule=" + intermittentSchedule + ", intermittentSentenceSchedules="
                + intermittentSentenceSchedules + ", toString()=" + super.toString() + "]";
    }

}
