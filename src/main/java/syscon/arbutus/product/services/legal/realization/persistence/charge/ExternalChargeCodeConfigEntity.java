package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the charge plea entity.
 *
 * @DbComment LEG_CHGExtCodeConfig 'The external charge code configuration data table'
 */
@Audited
@Entity
@Table(name = "LEG_CHGExtCodeConfig")
public class ExternalChargeCodeConfigEntity implements Serializable {

    private static final long serialVersionUID = 2531135922242999127L;

    /**
     * @DbComment ChargeCodeId 'The unique Id for each external charge code record.'
     */
    @Id
    @Column(name = "ChargeCodeId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGExtCodeConfig_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGExtCodeConfig_Id", sequenceName = "SEQ_LEG_CHGExtCodeConfig_Id", allocationSize = 1)
    private Long chargeCodeId;

    /**
     * @DbComment CodeGroup 'External Groups such as NCIC, UCR.'
     */
    @MetaCode(set = MetaSet.EXTERNAL_CHARGE_CODE_GROUP)
    @Column(name = "CodeGroup", nullable = false, length = 64)
    private String codeGroup;

    /**
     * @DbComment ChargeCode 'The external charge code linked to the group.'
     */
    @MetaCode(set = MetaSet.EXTERNAL_CHARGE_CODE)
    @Column(name = "ChargeCode", nullable = false, length = 64)
    private String chargeCode;

    /**
     * @DbComment CodeCategory 'External charge code category'
     */
    @MetaCode(set = MetaSet.EXTERNAL_CHARGE_CODE_CATEGORY)
    @Column(name = "CodeCategory", nullable = true, length = 64)
    private String codeCategory;

    /**
     * @DbComment Description 'External charge code description'
     */
    @Column(name = "Description", nullable = false, length = 128)
    private String description;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     *
     */
    public ExternalChargeCodeConfigEntity() {
    }

    /**
     * @param chargeCodeId
     * @param codeGroup
     * @param chargeCode
     * @param codeCategory
     * @param description
     */
    public ExternalChargeCodeConfigEntity(Long chargeCodeId, String codeGroup, String chargeCode, String codeCategory, String description) {
        this.chargeCodeId = chargeCodeId;
        this.codeGroup = codeGroup;
        this.chargeCode = chargeCode;
        this.codeCategory = codeCategory;
        this.description = description;
    }

    /**
     * @return the chargeCodeId
     */
    public Long getChargeCodeId() {
        return chargeCodeId;
    }

    /**
     * @param chargeCodeId the chargeCodeId to set
     */
    public void setChargeCodeId(Long chargeCodeId) {
        this.chargeCodeId = chargeCodeId;
    }

    /**
     * @return the codeGroup
     */
    public String getCodeGroup() {
        return codeGroup;
    }

    /**
     * @param codeGroup the codeGroup to set
     */
    public void setCodeGroup(String codeGroup) {
        this.codeGroup = codeGroup;
    }

    /**
     * @return the chargeCode
     */
    public String getChargeCode() {
        return chargeCode;
    }

    /**
     * @param chargeCode the chargeCode to set
     */
    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    /**
     * @return the codeCategory
     */
    public String getCodeCategory() {
        return codeCategory;
    }

    /**
     * @param codeCategory the codeCategory to set
     */
    public void setCodeCategory(String codeCategory) {
        this.codeCategory = codeCategory;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ExternalChargeCodeConfigEntity [chargeCodeId=" + chargeCodeId + ", codeGroup=" + codeGroup + ", chargeCode=" + chargeCode + ", codeCategory="
                + codeCategory + ", description=" + description + "]";
    }

}
