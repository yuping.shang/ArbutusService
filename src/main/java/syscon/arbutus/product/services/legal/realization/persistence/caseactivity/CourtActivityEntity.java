package syscon.arbutus.product.services.legal.realization.persistence.caseactivity;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.legal.realization.persistence.caseaffiliation.CaseAffiliationEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.CaseInfoEntity;
import syscon.arbutus.product.services.legal.realization.persistence.ordersentence.OrderEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the CourtActivity ent@Audited
 *
 * @author lhan
 */
@Audited
@Entity
@DiscriminatorValue("CourtActivity")
@SQLDelete(sql = "UPDATE LEG_CACaseActivity SET flag = 4 WHERE caseActivityId = ? and version = ?")
@Where(clause = "flag = 1")
public class CourtActivityEntity extends CaseActivityEntity {

    private static final long serialVersionUID = -4000659714623005706L;

    /**
     * @DbComment LEG_CACaseActivity.FacilityAssoc 'Indicates the facility where the hearing will take place.'
     */
    @Column(name = "FacilityAssoc", nullable = true)
    private Long facilityAssociation;

    /**
     * @DbComment LEG_CACaseActivity.FacilityInternalId 'Indicates the facility internal location where the hearing will take place.'
     */
    @Column(name = "FacilityInternalId", nullable = true)
    private Long facilityInternalAssociation;

    /**
     * @DbComment LEG_CACaseActivity.OrganizationAssoc 'Indicates the organization where the hearing will take place.'
     */
    @Column(name = "OrganizationAssoc", nullable = true)
    private Long organizationAssociation;

    /**
     * @DbComment LEG_CACaseActivity.Judge 'Indicates the judge who will be presiding over the court event if applicable.'
     */
    @Column(name = "Judge", nullable = true, length = 128)
    private String judge;

    /**
     * @DbComment Leg_CaseAct_OrderInit 'The join table of the tables LEG_CACaseActivity and LEG_OrdOrder'
     * @DbComment Leg_CaseAct_OrderInit.CaseActivityId 'CaseActivityId in LEG_CACaseActivity table'
     * @DbComment Leg_CaseAct_OrderInit.OrderId 'Initial OrderId in LEG_OrdOrder table'
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "Leg_CaseAct_OrderInit", joinColumns = { @JoinColumn(name = "CaseActivityId") },
            inverseJoinColumns = { @JoinColumn(name = "orderId") })
    @BatchSize(size = 20)
    private Set<OrderEntity> ordersInititated = new HashSet<OrderEntity>();

    /**
     * @DbComment Leg_CaseAct_OrderRslt 'The join table of the tables LEG_CACaseActivity and LEG_OrdOrder'
     * @DbComment Leg_CaseAct_OrderRslt.CaseActivityId 'CaseActivityId in LEG_CACaseActivity table'
     * @DbComment Leg_CaseAct_OrderRslt.OrderId 'Resulted OrderId in LEG_OrdOrder table'
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "Leg_CaseAct_OrderRslt", joinColumns = { @JoinColumn(name = "CaseActivityId") },
            inverseJoinColumns = { @JoinColumn(name = "orderId") })
    @BatchSize(size = 20)
    private Set<OrderEntity> ordersResulted = new HashSet<OrderEntity>();

    /**
     * Default constructor
     */
    public CourtActivityEntity() {

    }

    /**
     * @param caseActivityId
     * @param activityOccurredDate
     * @param activityCategory
     * @param activityType
     * @param activityOutcome
     * @param outcomeDate
     * @param activityStatus
     * @param comment
     * @param commentDate
     * @param chargeAssociations
     * @param associations
     * @param privileges
     * @param stamp
     * @param facilityAssociation
     * @param facilityInternalId
     * @param organizationAssociation
     * @param judge
     * @param ordersInititated
     * @param ordersResulted
     * @param history
     * @param outcomeDate
     */
    public CourtActivityEntity(Long caseActivityId, Date activityOccurredDate, String activityCategory, String activityType, String activityOutcome, Date outcomeDate,
            String activityStatus, String comment, Date commentDate, Set<CAChargeAssociationEntity> chargeAssociations, StampEntity stamp, Long facilityAssociation,
            Long facilityInternalId, Long organizationAssociation, String judge, Set<OrderEntity> ordersInititated, Set<OrderEntity> ordersResulted,
            Set<CaseActivityHistEntity> history, Set<CAActivityIdEntity> activityIds, Set<CaseAffiliationEntity> caseAffiliations, Set<CaseInfoEntity> caseInfos) {
        super(caseActivityId, activityOccurredDate, activityCategory, activityType, activityOutcome, outcomeDate, activityStatus, comment, commentDate,
                chargeAssociations, stamp, history, activityIds, caseAffiliations, caseInfos);
        this.facilityAssociation = facilityAssociation;
        this.facilityInternalAssociation = facilityInternalId;
        this.organizationAssociation = organizationAssociation;
        this.judge = judge;
        this.ordersInititated = ordersInititated;
        this.ordersResulted = ordersResulted;
    }

    /**
     * @return the facilityAssociation
     */
    public Long getFacilityAssociation() {
        return facilityAssociation;
    }

    /**
     * @param facilityAssociation the facilityAssociation to set
     */
    public void setFacilityAssociation(Long facilityAssociation) {
        this.facilityAssociation = facilityAssociation;
    }

    /**
     * @return the facilityInternalAssociation
     */
    public Long getFacilityInternalAssociation() {
        return facilityInternalAssociation;
    }

    /**
     * @param facilityInternalAssociation the facilityInternalAssociation to set
     */
    public void setFacilityInternalAssociation(Long facilityInternalAssociation) {
        this.facilityInternalAssociation = facilityInternalAssociation;
    }

    /**
     * @return the organizationAssociation
     */
    public Long getOrganizationAssociation() {
        return organizationAssociation;
    }

    /**
     * @param organizationAssociation the organizationAssociation to set
     */
    public void setOrganizationAssociation(Long organizationAssociation) {
        this.organizationAssociation = organizationAssociation;
    }

    /**
     * @return the judge
     */
    public String getJudge() {
        return judge;
    }

    /**
     * @param judge the judge to set
     */
    public void setJudge(String judge) {
        this.judge = judge;
    }

    /**
     * @return the ordersInititated
     */
    public Set<OrderEntity> getOrdersInititated() {
        if (ordersInititated == null) {
            ordersInititated = new HashSet<OrderEntity>();
        }
        return ordersInititated;
    }

    /**
     * @param ordersInititated the ordersInititated to set
     */
    public void setOrdersInititated(Set<OrderEntity> ordersInititated) {
        this.ordersInititated = ordersInititated;
    }

    /**
     * @return the ordersResulted
     */
    public Set<OrderEntity> getOrdersResulted() {
        if (ordersResulted == null) {
            ordersResulted = new HashSet<OrderEntity>();
        }
        return ordersResulted;
    }

    /**
     * @param ordersResulted the ordersResulted to set
     */
    public void setOrdersResulted(Set<OrderEntity> ordersResulted) {
        this.ordersResulted = ordersResulted;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((facilityAssociation == null) ? 0 : facilityAssociation.hashCode());
        result = prime * result + ((judge == null) ? 0 : judge.hashCode());
        result = prime * result + ((ordersInititated == null) ? 0 : ordersInititated.hashCode());
        result = prime * result + ((ordersResulted == null) ? 0 : ordersResulted.hashCode());
        result = prime * result + ((organizationAssociation == null) ? 0 : organizationAssociation.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        CourtActivityEntity other = (CourtActivityEntity) obj;
        if (facilityAssociation == null) {
            if (other.facilityAssociation != null) {
				return false;
			}
        } else if (!facilityAssociation.equals(other.facilityAssociation)) {
			return false;
		}
        if (facilityInternalAssociation == null) {
            if (other.facilityInternalAssociation != null) {
				return false;
			}
        } else if (!facilityInternalAssociation.equals(other.facilityInternalAssociation)) {
			return false;
		}
        if (judge == null) {
            if (other.judge != null) {
				return false;
			}
        } else if (!judge.equals(other.judge)) {
			return false;
		}
        if (ordersInititated == null) {
            if (other.ordersInititated != null) {
				return false;
			}
        } else if (!ordersInititated.equals(other.ordersInititated)) {
			return false;
		}
        if (ordersResulted == null) {
            if (other.ordersResulted != null) {
				return false;
			}
        } else if (!ordersResulted.equals(other.ordersResulted)) {
			return false;
		}
        if (organizationAssociation == null) {
            if (other.organizationAssociation != null) {
				return false;
			}
        } else if (!organizationAssociation.equals(other.organizationAssociation)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CourtActivityEntity [facilityAssociation=" + facilityAssociation + ", facilityInternalAssociation=" + facilityInternalAssociation
                + ", organizationAssociation=" + organizationAssociation + ", judge=" + judge + ", ordersInititated=" + ordersInititated + ", ordersResulted="
                + ordersResulted + ", toString()=" + super.toString() + "]";
    }

}
