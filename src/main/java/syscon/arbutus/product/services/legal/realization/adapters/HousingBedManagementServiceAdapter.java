package syscon.arbutus.product.services.legal.realization.adapters;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.OffenderHousingAssignmentType;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.OffenderHousingAssignmentsReturnType;
import syscon.arbutus.product.services.housing.contract.interfaces.HousingService;
import syscon.arbutus.product.services.utility.ServiceAdapter;

/**
 * An adapter class to wrap the API in HousingBedManagementService and provide the legal service bean customized API.
 *
 * @author lhan
 */
public class HousingBedManagementServiceAdapter {

    private static HousingService service;

    /**
     * Constructor.
     */
    public HousingBedManagementServiceAdapter() {
        super();
    }

    synchronized private static HousingService getService() {

        if (service == null) {
            service = (HousingService) ServiceAdapter.JNDILookUp(service, HousingService.class);
        }
        return service;
    }

    /**
     * @param userContext
     * @param supervisionId
     * @return
     */
    public OffenderHousingAssignmentType retrieveAssignmentsByOffender(UserContext userContext, Long supervisionId) {

        OffenderHousingAssignmentsReturnType ret = null;

        if (supervisionId == null) {
            return null;
        }

        ret = getService().retrieveAssignmentsByOffender(userContext, supervisionId, null);

        if (ret == null) {
            throw new ArbutusRuntimeException("Get supervision failed by Id:" + supervisionId);
        }

        if (ret.getOffenderHousingAssignments().size() == 1) {
            return ret.getOffenderHousingAssignments().iterator().next();
        } else {
            return null;
        }

    }

    public Long getHousedFacilityId(OffenderHousingAssignmentType offenderHousingAssignmentType) {
        if (offenderHousingAssignmentType == null) {
            return null;
        } else {
            return offenderHousingAssignmentType.getFacilityId();
        }
    }
}
