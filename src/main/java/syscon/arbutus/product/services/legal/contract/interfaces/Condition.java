package syscon.arbutus.product.services.legal.contract.interfaces;

import java.util.Date;
import java.util.List;
import java.util.Set;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.legal.contract.dto.condition.*;

/**
 * Business Contract Interface of Condition for CaseManagement Service
 * <p><b>Purpose</b>
 * <p>Conditions are prerequisites imposed on an offender by a court or a Justice Agency with authority to issue legal orders.
 * <p>Conditions can be tied to various entities for example, a Sentence or Bail Order.  An offender may be granted bail in addition to complying to some conditions, eg not to be physically present within 20km from a victims’s home or office.
 * <p>The CONDITION SERVICE will provide an association between a condition and the following major entities:
 * <pre>
 * <li>Sentences / Orders</li>
 * <li>Charges</li>
 * <li>Cases</li>
 * </pre>
 * <p><b>Scope</b>
 * <p>The scope of the service is limited to capturing and editing Conditions only.
 * <p>It includes the following:
 * <p>
 * <li>Create, update, search and delete a Condition</li>
 * <li>The CONDITION service will apply data privileges to support the sealing of conditions as well as versioning so that the history of changes can be recorded and viewed.<li>
 * <p>Class diagram of Condition module:
 * <br/><img src="doc-files/CaseManagement-Condition-classdiagram.JPG" />
 * </p>
 *
 * @author yshang
 * @version 1.0
 * @since December 20, 2012
 */
public interface Condition {

    /**
     * Create Condition tied to a specific offender
     * <p>A condition when created will always be associated with one of the following parent entities
     * <pre>
     * <li>Case</li>
     * <li>Order/Sentence</li>
     * <li>Charge</li>
     * </pre>
     * <p>Condition status
     * <p>Status =Active if ConditionEndDate is  > ConditionStartDate and later than today’s date or Condition EndDate = null;
     * <p>Status = Inactive in all other cases
     *
     * @param uc        {@link UserContext}
     * @param condition {@link ConditionType} - the Condition instance
     * @return the Condition instance.
     * Throws exceptions if instance cannot be created.
     */
    public ConditionType createCondition(UserContext uc, ConditionType condition);

    /**
     * Update Condition details
     * <p>Condition status
     * <p>Status =Active if ConditionEndDate is  > ConditionStartDate and later than today’s date or Condition EndDate = null;
     * <p>Status = Inactive in all other cases
     * <p>If ConditionAmendmentDate=true, then ConditionAmendmentStatus is mandatory.
     *
     * @param uc        {@link UserContext}
     * @param condition {@link ConditionType} - the Condition instance
     * @return the Condition instance.
     * Throws exceptions if instance cannot be created.
     */
    public ConditionType updateCondition(UserContext uc, ConditionType condition);

    /**
     * Get/Retrieve a Condition by Condition Id
     *
     * @param uc {@link UserContext}
     * @param id Long -- Condition Id
     * @return the Condition instance.
     * Throws exceptions if instance cannot be created.
     */
    public ConditionType getCondition(UserContext uc, Long id);

    /**
     * Search for Conditions
     * <p>Retrieve all Conditions linked to a Case, Charge or a Sentence/Order.
     * <p>Search can also be conducted for condition based on the associated links i.e. Orders/Sentences, Charges and Cases. E.g. get all Conditions linked to a Sentence.
     * <pre>
     * <li>If History flag = false/null the last changed record (current record) is searched;</li>
     * <li>If History flag = true the complete history of Condition records are searched (current and historic records);</li>
     * <li>The records returned must indicate if it is the current record or a historical record.</li>
     * </pre>
     * <p>At least one of the CaseReference, ChargeReference or OrderSentenceReference must be provided.
     * <p>All conditions linked to the case, charge, supervision or order/sentence will be returned.
     * <pre>
     * <li>If isActiveFlag = true, Return all active conditions for the case, charge, or order/sentence;</li>
     * <li>If isActiveFlag = false, Return all inactive conditions for the case, charge or order/sentence;</li>
     * <li>If isActiveFlag = null (not provided), Return all active and inactive conditions for the case, charge or order/sentence.</li>
     * </pre>
     *
     * @param uc              {@link UserContext}
     * @param IDs             Set&lt;Long> -- A set of Condition Id
     * @param search          {@link ConditionSearchType}
     * @param historyFlag     Boolean
     * @param chargeSetMode   String, Default = "ALL" (ONLY, ANY, ALL) -- Optional. Search SetMode for the set of Charge.
     * @param caseInfoSetMode String, Default = "ALL" (ONLY, ANY, ALL) -- Optional. Search SetMode for the set of CaseInformation.
     * @return Condition instances.
     * Throw exceptions if there is an error.
     * Return empty list when no instances could be found.
     */
    @Deprecated
    public List<ConditionType> searchCondition(UserContext uc, Set<Long> IDs, ConditionSearchType search, Boolean historyFlag, String chargeSetMode,
            String caseInfoSetMode);

    /**
     * Paging Search for Conditions
     * <p>Retrieve all Conditions linked to a Case, Charge or a Sentence/Order.
     * <p>Search can also be conducted for condition based on the associated links i.e. Orders/Sentences, Charges and Cases. E.g. get all Conditions linked to a Sentence.
     * <pre>
     * <li>If History flag = false/null the last changed record (current record) is searched;</li>
     * <li>If History flag = true the complete history of Condition records are searched (current and historic records);</li>
     * <li>The records returned must indicate if it is the current record or a historical record.</li>
     * </pre>
     * <p>At least one of the CaseReference, ChargeReference or OrderSentenceReference must be provided.
     * <p>All conditions linked to the case, charge, supervision or order/sentence will be returned.
     * <pre>
     * <li>If isActiveFlag = true, Return all active conditions for the case, charge, or order/sentence;</li>
     * <li>If isActiveFlag = false, Return all inactive conditions for the case, charge or order/sentence;</li>
     * <li>If isActiveFlag = null (not provided), Return all active and inactive conditions for the case, charge or order/sentence.</li>
     * </pre>
     *
     * @param uc              {@link UserContext}
     * @param search          {@link ConditionSearchType}
     * @param historyFlag     Boolean
     * @param chargeSetMode   String, Default = "ALL" (ONLY, ANY, ALL) -- Optional. Search SetMode for the set of Charge.
     * @param caseInfoSetMode String, Default = "ALL" (ONLY, ANY, ALL) -- Optional. Search SetMode for the set of CaseInformation.
     * @param startIndex      Long – Optional if null search returns at the start index
     * @param resultSize      Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder     String – Optional if null search returns a default order
     * @return the Condition instances and total size number.
     * Throw exceptions if there is an error.
     * Return empty list when no instances could be found.
     */
    public ConditionsReturnType searchCondition(UserContext uc, ConditionSearchType search, Boolean historyFlag, String chargeSetMode, String caseInfoSetMode,
            Long startIndex, Long resultSize, String resultOrder);

    /**
     * Retrieve historical information for a Condition
     * <p>Template history details between the FromHistoryDate to the ToHistoryDate are returned.
     * <p>Condition to be retrieved must exist.
     *
     * @param uc              {@link UserContext}
     * @param id              Long Condition Id
     * @param fromHistoryDate Date -- History date from
     * @param toHistoryDate   Date -- History date to
     * @return Condition instances.
     * Throws exceptions if there is an error.
     * Return null when no instances could be found.
     */
    public List<ConditionType> retrieveConditionHistory(UserContext uc, Long id, Date fromHistoryDate, Date toHistoryDate);

    /**
     * Retrieve all conditions linked to a Case, Charge or a Sentence/Order/Bail
     * <p>At least one of the CaseId, ChargeId or OrderId must be provided.</p>
     * <p>ConditionSearchType is optional.</p>
     * <p>All conditions linked to the case, charge or order/sentence will be returned.</p>
     * <pre>
     * <li>If isActiveFlag = true, Return all active conditions for the case, charge, or order/sentence</li>
     * <li>If isActiveFlag = false, Return all inactive conditions for the case, charge or order/sentence</li>
     * <li>If isActiveFlag = null (not provided), Return all active and inactive conditions for the case, charge or order/sentence</li>
     * </pre>
     *
     * @return Condition instances.
     * Throws exceptions if there is an error.
     * Return null when no instances could be found.
     */
    public List<ConditionType> retrieveConditionBy(UserContext uc, Long caseId, Long chargeId, Long orderId, ConditionSearchType conditionSearch, Boolean active);

    /**
     * Soft deletion of a single condition.
     * Returns success code if the condition can be deleted.
     * Returns error code if the condition cannot be deleted.
     * <p>List of possible return codes: 1</p>
     *
     * @param uc          UserContext - Required
     * @param conditionId java.lang.Long - Required
     * @return Long
     * Throws exception if instance cannot be deleted.
     */
    public Long deleteCondition(UserContext uc, Long conditionId);

    /**
     * Update Charge Condition Security Status / Flag
     *
     * @param uc        - Required.
     * @param secStatus - DataFlag.- required
     * @param Long      - personId. Person Id of person id,- required
     * @param Long      - conditionId. Condition Id - required
     * @param comments  - Comments to the security flag.
     * @return Long 1/0 as True/False
     * @throws exception if the retrieving encounters error.
     */
    public Long updateConditionDataSecurityStatus(UserContext uc, Long conditionId, Long secStatus, String comments, Long personId, String parentEntityType,
            Long parentId, String memento);

    /**
     * Create Configure Condition for configuring Conditions
     * <p>Configure Condition status
     * <p>Status =Active if deactivationDate is  > activationDate and later than today’s date or Configure Condition deactivationDate = null;
     * <p>Status = Inactive in all other cases
     *
     * @param userContext        - Required
     *                           {@link UserContext}
     * @param configureCondition - Required
     *                           {@link ConfigureConditionType} - the Configure Condition instance
     * @return the Condition instance.
     * Throws exceptions if instance cannot be created.
     */
    public ConfigureConditionType createConfigureCondition(UserContext userContext, ConfigureConditionType configureCondition);

    /**
     * Update Configure Condition details
     * <p>Configure Condition status
     * <p>Status =Active if deactivationDate is  > activationDate and later than today’s date or Configure Condition deactivationDate = null;
     * <p>Status = Inactive in all other cases
     *
     * @param userContext        - Required
     *                           {@link UserContext}
     * @param configureCondition - Required
     *                           {@link ConfigureConditionType} - the Configure Condition instance
     * @return the Condition instance.
     * Throws exceptions if instance cannot be created.
     */
    public ConfigureConditionType updateConfigureCondition(UserContext userContext, ConfigureConditionType configureCondition);

    /**
     * Soft deletion of a single configure condition.
     * Returns success code if the condition can be deleted.
     * Returns error code if the condition cannot be deleted.
     * <p>List of possible return codes: 1</p>
     *
     * @param userContext UserContext - Required
     * @param conditionId java.lang.Long - Required
     * @return Long
     * Throws exception if instance cannot be deleted.
     */
    public Long deleteConfigureCondition(UserContext userContext, Long configureConditionId);

    /**
     * Get/Retrieve a Configure Condition by Configure Condition Id
     *
     * @param userContext          - Required
     *                             {@link UserContext}
     * @param configureConditionId - Required
     *                             Long -- Configure Condition Id
     * @return the Configure Condition instance.
     * Throws exceptions if instance cannot be created.
     */
    public ConfigureConditionType getConfigureCondition(UserContext userContext, Long configureConditionId);

    /**
     * Paging Search for Configure Conditions
     * <p>Retrieve all Conditions linked to a Case, Charge or a Sentence/Order.
     * <p>Search can also be conducted for condition based on the associated links i.e. Orders/Sentences, Charges and Cases. E.g. get all Configure Conditions linked to a Sentence.
     * <pre>
     * <li>If History flag = false/null the last changed record (current record) is searched;</li>
     * <li>If History flag = true the complete history of Condition records are searched (current and historic records);</li>
     * <li>The records returned must indicate if it is the current record or a historical record.</li>
     * </pre>
     * <p>At least one of the CaseReference, ChargeReference or OrderSentenceReference must be provided.
     * <p>All conditions linked to the case, charge, supervision or order/sentence will be returned.
     * <pre>
     * <li>If isActiveFlag = true, Return all active conditions for the case, charge, or order/sentence;</li>
     * <li>If isActiveFlag = false, Return all inactive conditions for the case, charge or order/sentence;</li>
     * <li>If isActiveFlag = null (not provided), Return all active and inactive conditions for the case, charge or order/sentence.</li>
     * </pre>
     *
     * @param userContext - Required
     *                    {@link UserContext}
     * @param search      {@link ConfigureConditionSearchType}
     * @param historyFlag Boolean
     * @param startIndex  Long – Optional if null search returns at the start index
     * @param resultSize  Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder String – Optional if null search returns a default order
     * @return the Condition Condition instances and total size number.
     * Throw exceptions if there is an error.
     * Return empty list when no instances could be found.
     */
    public ConfigureConditionsReturnType searchConfigureCondition(UserContext userContext, ConfigureConditionSearchType search, Boolean historyFlag, Long startIndex,
            Long resultSize, String resultOrder);
}
