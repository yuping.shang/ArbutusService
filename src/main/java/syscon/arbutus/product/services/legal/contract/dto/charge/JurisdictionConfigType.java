package syscon.arbutus.product.services.legal.contract.dto.charge;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * The representation of the jurisdiction configuration type
 */
public class JurisdictionConfigType implements Serializable {

    private static final long serialVersionUID = 738447182298710546L;

    private Long jurisdictionId;
    @NotNull
    @Size(max = 128, message = "max length 128")
    private String description;
    @NotNull
    private String country;
    @NotNull
    private String stateProvince;
    @Size(max = 128, message = "max length 128")
    private String city;
    private String county;

    /**
     * Default constructor
     */
    public JurisdictionConfigType() {
    }

    /**
     * Constructor
     *
     * @param jurisdictionId Long - The unique Id for each statute jurisdiction record. (optional for creation)
     * @param description    String - Description of the jurisdiction
     * @param country        String - Country the jurisdiction applies to
     * @param stateProvince  String - State/province the jurisdiction applies to
     * @param city           String - A name of a city or town. (optional)
     * @param county         String - County the jurisdiction applies to  (optional)
     */
    public JurisdictionConfigType(Long jurisdictionId, @NotNull @Size(max = 128, message = "max length 128") String description, @NotNull String country,
            @NotNull String stateProvince, @Size(max = 128, message = "max length 128") String city, String county) {
        this.jurisdictionId = jurisdictionId;
        this.description = description;
        this.country = country;
        this.stateProvince = stateProvince;
        this.city = city;
        this.county = county;
    }

    /**
     * Gets the value of the jurisdictionId property.
     *
     * @return Long - The unique Id for each statute jurisdiction record.
     */
    public Long getJurisdictionId() {
        return jurisdictionId;
    }

    /**
     * Sets the value of the jurisdictionId property.
     *
     * @param jurisdictionId Long - The unique Id for each statute jurisdiction record.
     */
    public void setJurisdictionId(Long jurisdictionId) {
        this.jurisdictionId = jurisdictionId;
    }

    /**
     * Gets the value of the description property.
     *
     * @return String - Description of the jurisdiction
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param description String - Description of the jurisdiction
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the value of the country property. It is a code value of Country set.
     *
     * @return String - Country the jurisdiction applies to
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property. It is a code value of Country set.
     *
     * @param country String - Country the jurisdiction applies to
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets the value of the stateProvince property. It is a code value of StateProvince set.
     *
     * @return String - State/province the jurisdiction applies to
     */
    public String getStateProvince() {
        return stateProvince;
    }

    /**
     * Sets the value of the stateProvince property. It is a code value of StateProvince set.
     *
     * @param stateProvince String - State/province the jurisdiction applies to
     */
    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    /**
     * Gets the value of the city property.
     *
     * @return String - A name of a city or town.
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     *
     * @param city String - A name of a city or town.
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Gets the value of the county property. It is a code value of StatuteCounty set.
     *
     * @return String - County the jurisdiction applies to
     */
    public String getCounty() {
        return county;
    }

    /**
     * Sets the value of the county property. It is a code value of StatuteCounty set.
     *
     * @param county String - County the jurisdiction applies to
     */
    public void setCounty(String county) {
        this.county = county;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "JurisdictionConfigType [jurisdictionId=" + jurisdictionId + ", description=" + description + ", country=" + country + ", stateProvince=" + stateProvince
                + ", city=" + city + ", county=" + county + "]";
    }

}
