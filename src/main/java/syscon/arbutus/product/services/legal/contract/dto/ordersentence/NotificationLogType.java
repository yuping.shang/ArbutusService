package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.StampType;

/**
 * NotificationLogSearchType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 21, 2012
 */
public class NotificationLogType implements Serializable {

    private static final long serialVersionUID = 8063452570731755157L;

    @NotNull
    private Date notificationDate;
    @NotNull
    private String notificationType;
    @Valid
    private CommentType notificationComment;
    private Long notificationLocationAssociation;
    @Size(max = 128, message = "max length 128")
    private String notificationContact;

    private StampType stamp;

    /**
     * Constructor
     */
    public NotificationLogType() {
        super();
    }

    /**
     * Constructor
     *
     * @param notificationDate                Date, Required. The date and time the log entry was made.
     * @param notificationType                String, Required. The type of notification log entry.
     * @param notificationComment             CommentType, Optional. The description of the log entry.
     * @param notificationLocationAssociation Long, Optional. The address/phone details of the notification. Static reference to location.
     * @param notificationContact             String (length <= 128), Optional.  The details (name) of the person notified.
     */
    public NotificationLogType(@NotNull Date notificationDate, @NotNull String notificationType, CommentType notificationComment, Long notificationLocationAssociation,
            @Size(max = 128, message = "max length 128") String notificationContact) {
        super();
        this.notificationDate = notificationDate;
        this.notificationType = notificationType;
        this.notificationComment = notificationComment;
        this.notificationLocationAssociation = notificationLocationAssociation;
        this.notificationContact = notificationContact;
    }

    /**
     * Get Notification Date -- The date and time the log entry was made.
     *
     * @return the notificationDate
     */
    public Date getNotificationDate() {
        return notificationDate;
    }

    /**
     * Set Notification Date -- The date and time the log entry was made.
     *
     * @param notificationDate the notificationDate to set
     */
    public void setNotificationDate(Date notificationDate) {
        this.notificationDate = notificationDate;
    }

    /**
     * Get Notification Type -- The type of notification log entry.
     *
     * @return the notificationType
     */
    public String getNotificationType() {
        return notificationType;
    }

    /**
     * Set Notification Type -- The type of notification log entry.
     *
     * @param notificationType the notificationType to set
     */
    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    /**
     * Get NotificationComment -- The description of the log entry.
     *
     * @return the notificationComment
     */
    public CommentType getNotificationComment() {
        return notificationComment;
    }

    /**
     * Set NotificationComment -- The description of the log entry.
     *
     * @param notificationComment the notificationComment to set
     */
    public void setNotificationComment(CommentType notificationComment) {
        this.notificationComment = notificationComment;
    }

    /**
     * Get Notification Location Association
     * -- The address/phone details of the notification. Static reference to location.
     *
     * @return the notificationLocationAssociation
     */
    public Long getNotificationLocationAssociation() {
        return notificationLocationAssociation;
    }

    /**
     * Set Notification Location Association
     * -- The address/phone details of the notification. Static reference to location.
     *
     * @param notificationLocationAssociation the notificationLocationAssociation to set
     */
    public void setNotificationLocationAssociation(Long notificationLocationAssociation) {
        this.notificationLocationAssociation = notificationLocationAssociation;
    }

    /**
     * Get Notification Contact
     * -- The details (name) of the person notified.
     *
     * @return the notificationContact
     */
    public String getNotificationContact() {
        return notificationContact;
    }

    /**
     * Set Notification Contact
     * -- The details (name) of the person notified.
     *
     * @param notificationContact the notificationContact to set
     */
    public void setNotificationContact(String notificationContact) {
        this.notificationContact = notificationContact;
    }

    /**
     * @return the stamp
     */
    public StampType getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampType stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((notificationContact == null) ? 0 : notificationContact.hashCode());
        result = prime * result + ((notificationDate == null) ? 0 : notificationDate.hashCode());
        result = prime * result + ((notificationLocationAssociation == null) ? 0 : notificationLocationAssociation.hashCode());
        result = prime * result + ((notificationType == null) ? 0 : notificationType.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        NotificationLogType other = (NotificationLogType) obj;
        if (notificationContact == null) {
            if (other.notificationContact != null) {
				return false;
			}
        } else if (!notificationContact.equals(other.notificationContact)) {
			return false;
		}
        if (notificationDate == null) {
            if (other.notificationDate != null) {
				return false;
			}
        } else if (!notificationDate.equals(other.notificationDate)) {
			return false;
		}
        if (notificationLocationAssociation == null) {
            if (other.notificationLocationAssociation != null) {
				return false;
			}
        } else if (!notificationLocationAssociation.equals(other.notificationLocationAssociation)) {
			return false;
		}
        if (notificationType == null) {
            if (other.notificationType != null) {
				return false;
			}
        } else if (!notificationType.equals(other.notificationType)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "NotificationLogType [notificationDate=" + notificationDate + ", notificationType=" + notificationType + ", notificationComment=" + notificationComment
                + ", notificationLocationAssociation=" + notificationLocationAssociation + ", notificationContact=" + notificationContact + ", stamp=" + stamp + "]";
    }

}
