package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

// import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * OrderType for OrderSentence of Case Management Service
 *
 * @author yshang
 * @version 1.0
 * @since December 21, 2012
 */
//@ArbutusConstraint(constraints = {
//		"orderIssuanceDate <= orderExpirationDate", 
//		"orderStartDate <= orderExpirationDate"})
public class OrderType implements Serializable {

    private static final long serialVersionUID = -5341563595105382600L;

    private Long orderIdentification;

    private Long ojSupervisionId;

    @NotNull
    private String orderClassification;
    @NotNull
    private String orderType;

    private String orderSubType;
    @NotNull
    private String orderCategory;
    @Size(max = 64, message = "max length 64")
    private String orderNumber;
    @NotNull
    @Valid
    private DispositionType orderDisposition;
    @Valid
    private CommentType comments;
    @NotNull
    private Date orderIssuanceDate;

    private Date orderReceivedDate;
    @NotNull
    private Date orderStartDate;

    private Date orderExpirationDate;

    private Set<Long> caseActivityInitiatedOrderAssociations;

    private Set<Long> orderInitiatedCaseActivityAssociations;

    private Boolean isHoldingOrder;

    private Boolean isSchedulingNeeded;

    private Boolean hasCharges;
    @Valid
    private NotificationType issuingAgency;

    //Relationship with modules
    /*@NotNull
    @Size(min = 1, message = "Cardinality [1..*]") -- commented to let be required when In Jurisdiction; optional when Outside Jurisdiction.*/
    private Set<Long> caseInfoIds;
    private Set<Long> chargeIds;
    private Set<Long> conditionIds;

    /**
     * Constructor
     */
    public OrderType() {
        super();
    }

    /**
     * Constructor
     *
     * @param orderIdentification                    Long. Not Required for create; Required otherwise. The unique ID for each order created in the system. This is a system-generated ID.
     * @param orderClassification                    String, Required. The order classification. This defines the type of instance of the service.
     * @param orderType                              String, Required. The client defined order type that maps to the order classification.
     * @param orderSubType                           String, Optional. The sub type of an order. For example a want, warrant or detainer.
     * @param orderCategory                          String, Required. The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.
     * @param orderNumber                            String(length <= 64), Optional. User specified order number.
     * @param orderDisposition                       DispositionType, Required. The disposition related to the order.
     * @param comments                               CommentType, Optional. The comments related to the order.
     * @param orderIssuanceDate                      Date, Required. The date the order was issued.
     * @param orderReceivedDate                      Date, Optional. The date the order was received.
     * @param orderStartDate                         Date, Required. The date/time the order starts(becomes valid).
     * @param orderExpirationDate                    Date, Optional. The date the order expires.
     * @param caseActivityInitiatedOrderAssociations Set&lt;Long>, Optional. The case related activities that initiated this order. Static association to the Case Activity service.
     * @param orderInitiatedCaseActivityAssociations Set&lt;Long>, Optional. The case activities that were initiated by this order. Static association to the Case Activity service.
     * @param isHoldingOrder                         Boolean, Ignored. If true then the order is a holding document, false otherwise.
     * @param isSchedulingNeeded                     Boolean, Ignored. If true, scheduling is needed for the order, false otherwise.
     * @param hasCharges                             Boolean, Ignored. If true, the order has charges related, false otherwise.
     * @param issuingAgency                          NotificationType, Optional. The source agency that issues the order, this could be a facility or an organization.
     * @param caseInfoIds                            Set&lt;Long>, required when OrderCategory IJ -- In Jurisdiction; optional when OrderCategory OJ -- Outside Jurisdiction. A set of IDs which are associated to CaseInfoS.
     * @param chargeIds                              Set&lt;Long>, Optional. A set of IDs which are associated to ChargeS.
     * @param conditionIds                           Set&lt;Long>, Optional. A set of IDs which are associated to ConditionS.
     */
    public OrderType(Long orderIdentification, @NotNull String orderClassification, @NotNull String orderType, String orderSubType, @NotNull String orderCategory,
            @Size(max = 64, message = "max length 64") String orderNumber, @NotNull DispositionType orderDisposition, CommentType comments,
            @NotNull Date orderIssuanceDate, Date orderReceivedDate, @NotNull Date orderStartDate, Date orderExpirationDate,
            Set<Long> caseActivityInitiatedOrderAssociations, Set<Long> orderInitiatedCaseActivityAssociations, Boolean isHoldingOrder, Boolean isSchedulingNeeded,
            Boolean hasCharges, NotificationType issuingAgency,

            @NotNull @Size(min = 1, message = "Cardinality [1..*]") Set<Long> caseInfoIds, Set<Long> chargeIds, Set<Long> conditionIds) {
        super();
        this.orderIdentification = orderIdentification;
        this.orderClassification = orderClassification;
        this.orderType = orderType;
        this.orderSubType = orderSubType;
        this.orderCategory = orderCategory;
        this.orderNumber = orderNumber;
        this.orderDisposition = orderDisposition;
        this.comments = comments;
        this.orderIssuanceDate = orderIssuanceDate;
        this.orderReceivedDate = orderReceivedDate;
        this.orderStartDate = orderStartDate;
        this.orderExpirationDate = orderExpirationDate;
        this.caseActivityInitiatedOrderAssociations = caseActivityInitiatedOrderAssociations;
        this.orderInitiatedCaseActivityAssociations = orderInitiatedCaseActivityAssociations;
        this.isHoldingOrder = isHoldingOrder;
        this.isSchedulingNeeded = isSchedulingNeeded;
        this.hasCharges = hasCharges;
        this.issuingAgency = issuingAgency;
        this.caseInfoIds = caseInfoIds;
        this.chargeIds = chargeIds;
        this.conditionIds = conditionIds;
    }

    /**
     * Get Order Identification
     * -- The unique ID for each order created in the system. This is a system-generated ID.
     *
     * @return the orderIdentification
     */
    public Long getOrderIdentification() {
        return orderIdentification;
    }

    /**
     * Set Order Identification
     * -- The unique ID for each order created in the system. This is a system-generated ID.
     *
     * @param orderIdentification the orderIdentification to set
     */
    public void setOrderIdentification(Long orderIdentification) {
        this.orderIdentification = orderIdentification;
    }

    /**
     * Get Supervision ID from Outside Jurisdiction(OJ)
     * <p>Required when OrderCategory is OJ; null when OrderCategory is IJ
     *
     * @return Supervision Id from Outside Jurisdiction
     */
    public Long getOjSupervisionId() {
        return ojSupervisionId;
    }

    /**
     * Set Supervision ID from Outside Jurisdiction(OJ)
     * <p>Required when OrderCategory is OJ; null when OrderCategory is IJ
     *
     * @param ojSupervisionId Supervision Id from Outside Jurisdiction
     */
    public void setOjSupervisionId(Long ojSupervisionId) {
        this.ojSupervisionId = ojSupervisionId;
    }

    /**
     * Get Order Classification
     * -- The order classification. This defines the type of instance of the service.
     *
     * @return the orderClassification
     */
    public String getOrderClassification() {
        return orderClassification;
    }

    /**
     * Set Order Classification
     * -- The order classification. This defines the type of instance of the service.
     *
     * @param orderClassification the orderClassification to set
     */
    public void setOrderClassification(String orderClassification) {
        this.orderClassification = orderClassification;
    }

    /**
     * Get Order Type
     * -- The client defined order type that maps to the order classification.
     *
     * @return the orderType
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * Set Order Type
     * -- The client defined order type that maps to the order classification.
     *
     * @param orderType the orderType to set
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    /**
     * Get Order Sub Type
     * -- The sub type of an order. For example a want, warrant or detainer.
     *
     * @return the orderSubType
     */
    public String getOrderSubType() {
        return orderSubType;
    }

    /**
     * Set Order Sub Type
     * -- The sub type of an order. For example a want, warrant or detainer.
     *
     * @param orderSubType the orderSubType to set
     */
    public void setOrderSubType(String orderSubType) {
        this.orderSubType = orderSubType;
    }

    /**
     * Get Order Category
     * -- The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.
     *
     * @return the orderCategory
     */
    public String getOrderCategory() {
        return orderCategory;
    }

    /**
     * Set Order Category
     * -- The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.
     *
     * @param orderCategory the orderCategory to set
     */
    public void setOrderCategory(String orderCategory) {
        this.orderCategory = orderCategory;
    }

    /**
     * Get Order Number
     * -- User specified order number.
     *
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * Set Order Number
     * -- User specified order number.
     *
     * @param orderNumber the orderNumber to set
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * Get Order Disposition
     * -- The disposition related to the order.
     *
     * @return the orderDisposition
     */
    public DispositionType getOrderDisposition() {
        return orderDisposition;
    }

    /**
     * Set Order Disposition
     * -- The disposition related to the order.
     *
     * @param orderDisposition the orderDisposition to set
     */
    public void setOrderDisposition(DispositionType orderDisposition) {
        this.orderDisposition = orderDisposition;
    }

    /**
     * Get Comments
     * -- The comments related to the order.
     *
     * @return the comments
     */
    public CommentType getComments() {
        return comments;
    }

    /**
     * Set Comments
     * -- The comments related to the order.
     *
     * @param comments the comments to set
     */
    public void setComments(CommentType comments) {
        this.comments = comments;
    }

    /**
     * Get Order Issuance Date
     * -- The date the order was issued.
     *
     * @return the orderIssuanceDate
     */
    public Date getOrderIssuanceDate() {
        return orderIssuanceDate;
    }

    /**
     * Set Order Issuance Date
     * -- The date the order was issued.
     *
     * @param orderIssuanceDate the orderIssuanceDate to set
     */
    public void setOrderIssuanceDate(Date orderIssuanceDate) {
        this.orderIssuanceDate = orderIssuanceDate;
    }

    /**
     * Get Order Received Date
     * -- The date the order was received.
     *
     * @return the orderReceivedDate
     */
    public Date getOrderReceivedDate() {
        return orderReceivedDate;
    }

    /**
     * Set Order Received Date
     * -- The date the order was received.
     *
     * @param orderReceivedDate the orderReceivedDate to set
     */
    public void setOrderReceivedDate(Date orderReceivedDate) {
        this.orderReceivedDate = orderReceivedDate;
    }

    /**
     * Get Order Start Date
     * -- The date/time the order starts(becomes valid).
     *
     * @return the orderStartDate
     */
    public Date getOrderStartDate() {
        return orderStartDate;
    }

    /**
     * Set Order Start Date
     * -- The date/time the order starts(becomes valid).
     *
     * @param orderStartDate the orderStartDate to set
     */
    public void setOrderStartDate(Date orderStartDate) {
        this.orderStartDate = orderStartDate;
    }

    /**
     * Get Order Expiration Date
     * -- The date the order expires.
     *
     * @return the orderExpirationDate
     */
    public Date getOrderExpirationDate() {
        return orderExpirationDate;
    }

    /**
     * Set Order Expiration Date
     * -- The date the order expires.
     *
     * @param orderExpirationDate the orderExpirationDate to set
     */
    public void setOrderExpirationDate(Date orderExpirationDate) {
        this.orderExpirationDate = orderExpirationDate;
    }

    /**
     * Get Case Activity Initiated Order Associations
     * -- The case related activities that initiated this order. Static association to the Case Activity service.
     *
     * @return the caseActivityInitiatedOrderAssociations
     */
    public Set<Long> getCaseActivityInitiatedOrderAssociations() {
        if (caseActivityInitiatedOrderAssociations == null) {
            caseActivityInitiatedOrderAssociations = new HashSet<Long>();
        }
        return caseActivityInitiatedOrderAssociations;
    }

    /**
     * Set Case Activity Initiated Order Associations
     * -- The case related activities that initiated this order. Static association to the Case Activity service.
     *
     * @param caseActivityInitiatedOrderAssociations the caseActivityInitiatedOrderAssociations to set
     */
    public void setCaseActivityInitiatedOrderAssociations(Set<Long> caseActivityInitiatedOrderAssociations) {
        this.caseActivityInitiatedOrderAssociations = caseActivityInitiatedOrderAssociations;
    }

    /**
     * Get Order Initiated Case Activity Associations
     * -- The case activities that were initiated by this order. Static association to the Case Activity service.
     *
     * @return the orderInititatedCaseActivityAssociations
     */
    public Set<Long> getOrderInitiatedCaseActivityAssociations() {
        if (orderInitiatedCaseActivityAssociations == null) {
            orderInitiatedCaseActivityAssociations = new HashSet<Long>();
        }
        return orderInitiatedCaseActivityAssociations;
    }

    /**
     * Set Order Initiated Case Activity Associations
     * -- The case activities that were initiated by this order. Static association to the Case Activity service.
     *
     * @param orderInitiatedCaseActivityAssociations the orderInitiatedCaseActivityAssociations to set
     */
    public void setOrderInitiatedCaseActivityAssociations(Set<Long> orderInitiatedCaseActivityAssociations) {
        this.orderInitiatedCaseActivityAssociations = orderInitiatedCaseActivityAssociations;
    }

    /**
     * Get Is Holding Order
     * -- If true then the order is a holding document, false otherwise.
     *
     * @return the isHoldingOrder
     */
    public Boolean getIsHoldingOrder() {
        return isHoldingOrder;
    }

    /**
     * Set Is Holding Order
     * -- If true then the order is a holding document, false otherwise.
     *
     * @param isHoldingOrder the isHoldingOrder to set
     */
    public void setIsHoldingOrder(Boolean isHoldingOrder) {
        this.isHoldingOrder = isHoldingOrder;
    }

    /**
     * Get Is Scheduling Needed
     * -- If true, scheduling is needed for the order, false otherwise.
     *
     * @return the isSchedulingNeeded
     */
    public Boolean getIsSchedulingNeeded() {
        return isSchedulingNeeded;
    }

    /**
     * Set Is Scheduling Needed
     * -- If true, scheduling is needed for the order, false otherwise.
     *
     * @param isSchedulingNeeded the isSchedulingNeeded to set
     */
    public void setIsSchedulingNeeded(Boolean isSchedulingNeeded) {
        this.isSchedulingNeeded = isSchedulingNeeded;
    }

    /**
     * Get Has Charges
     * -- If true, the order has charges related, false otherwise.
     *
     * @return the hasCharges
     */
    public Boolean getHasCharges() {
        return hasCharges;
    }

    /**
     * Set Has Charges
     * -- If true, the order has charges related, false otherwise.
     *
     * @param hasCharges the hasCharges to set
     */
    public void setHasCharges(Boolean hasCharges) {
        this.hasCharges = hasCharges;
    }

    /**
     * Get Issuing Agency
     * -- The source agency that issues the order, this could be a facility or an organization.
     *
     * @return the issuingAgency
     */
    public NotificationType getIssuingAgency() {
        return issuingAgency;
    }

    /**
     * Set Issuing Agency
     * -- The source agency that issues the order, this could be a facility or an organization.
     *
     * @param issuingAgency the issuingAgency to set
     */
    public void setIssuingAgency(NotificationType issuingAgency) {
        this.issuingAgency = issuingAgency;
    }

    /**
     * Required when OrderCategory IJ -- In Jurisdiction; optional when OrderCategory OJ -- Outside Jurisdiction.
     *
     * @return the caseInfoIds
     */
    public Set<Long> getCaseInfoIds() {
        return caseInfoIds;
    }

    /**
     * Required when OrderCategory IJ -- In Jurisdiction; optional when OrderCategory OJ -- Outside Jurisdiction.
     *
     * @param caseInfoIds the caseInfoIds to set
     */
    public void setCaseInfoIds(Set<Long> caseInfoIds) {
        this.caseInfoIds = caseInfoIds;
    }

    /**
     * @return the chargeIds
     */
    public Set<Long> getChargeIds() {
        return chargeIds;
    }

    /**
     * @param chargeIds the chargeIds to set
     */
    public void setChargeIds(Set<Long> chargeIds) {
        this.chargeIds = chargeIds;
    }

    /**
     * @return the conditionIds
     */
    public Set<Long> getConditionIds() {
        return conditionIds;
    }

    /**
     * @param conditionIds the conditionIds to set
     */
    public void setConditionIds(Set<Long> conditionIds) {
        this.conditionIds = conditionIds;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((caseActivityInitiatedOrderAssociations == null) ? 0 : caseActivityInitiatedOrderAssociations.hashCode());
        result = prime * result + ((caseInfoIds == null) ? 0 : caseInfoIds.hashCode());
        result = prime * result + ((chargeIds == null) ? 0 : chargeIds.hashCode());
        result = prime * result + ((conditionIds == null) ? 0 : conditionIds.hashCode());
        result = prime * result + ((hasCharges == null) ? 0 : hasCharges.hashCode());
        result = prime * result + ((isHoldingOrder == null) ? 0 : isHoldingOrder.hashCode());
        result = prime * result + ((isSchedulingNeeded == null) ? 0 : isSchedulingNeeded.hashCode());
        result = prime * result + ((issuingAgency == null) ? 0 : issuingAgency.hashCode());
        result = prime * result + ((orderCategory == null) ? 0 : orderCategory.hashCode());
        result = prime * result + ((orderClassification == null) ? 0 : orderClassification.hashCode());
        result = prime * result + ((orderDisposition == null) ? 0 : orderDisposition.hashCode());
        result = prime * result + ((orderExpirationDate == null) ? 0 : orderExpirationDate.hashCode());
        result = prime * result + ((orderIdentification == null) ? 0 : orderIdentification.hashCode());
        result = prime * result + ((ojSupervisionId == null) ? 0 : ojSupervisionId.hashCode());
        result = prime * result + ((orderInitiatedCaseActivityAssociations == null) ? 0 : orderInitiatedCaseActivityAssociations.hashCode());
        result = prime * result + ((orderIssuanceDate == null) ? 0 : orderIssuanceDate.hashCode());
        result = prime * result + ((orderNumber == null) ? 0 : orderNumber.hashCode());
        result = prime * result + ((orderReceivedDate == null) ? 0 : orderReceivedDate.hashCode());
        result = prime * result + ((orderStartDate == null) ? 0 : orderStartDate.hashCode());
        result = prime * result + ((orderSubType == null) ? 0 : orderSubType.hashCode());
        result = prime * result + ((orderType == null) ? 0 : orderType.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        OrderType other = (OrderType) obj;
        if (caseActivityInitiatedOrderAssociations == null) {
            if (other.caseActivityInitiatedOrderAssociations != null) {
				return false;
			}
        } else if (!caseActivityInitiatedOrderAssociations.equals(other.caseActivityInitiatedOrderAssociations)) {
			return false;
		}
        if (caseInfoIds == null) {
            if (other.caseInfoIds != null) {
				return false;
			}
        } else if (!caseInfoIds.equals(other.caseInfoIds)) {
			return false;
		}
        if (chargeIds == null) {
            if (other.chargeIds != null) {
				return false;
			}
        } else if (!chargeIds.equals(other.chargeIds)) {
			return false;
		}
        if (conditionIds == null) {
            if (other.conditionIds != null) {
				return false;
			}
        } else if (!conditionIds.equals(other.conditionIds)) {
			return false;
		}
        if (hasCharges == null) {
            if (other.hasCharges != null) {
				return false;
			}
        } else if (!hasCharges.equals(other.hasCharges)) {
			return false;
		}
        if (isHoldingOrder == null) {
            if (other.isHoldingOrder != null) {
				return false;
			}
        } else if (!isHoldingOrder.equals(other.isHoldingOrder)) {
			return false;
		}
        if (isSchedulingNeeded == null) {
            if (other.isSchedulingNeeded != null) {
				return false;
			}
        } else if (!isSchedulingNeeded.equals(other.isSchedulingNeeded)) {
			return false;
		}
        if (issuingAgency == null) {
            if (other.issuingAgency != null) {
				return false;
			}
        } else if (!issuingAgency.equals(other.issuingAgency)) {
			return false;
		}
        if (orderCategory == null) {
            if (other.orderCategory != null) {
				return false;
			}
        } else if (!orderCategory.equals(other.orderCategory)) {
			return false;
		}
        if (orderClassification == null) {
            if (other.orderClassification != null) {
				return false;
			}
        } else if (!orderClassification.equals(other.orderClassification)) {
			return false;
		}
        if (orderDisposition == null) {
            if (other.orderDisposition != null) {
				return false;
			}
        } else if (!orderDisposition.equals(other.orderDisposition)) {
			return false;
		}
        if (orderExpirationDate == null) {
            if (other.orderExpirationDate != null) {
				return false;
			}
        } else if (!orderExpirationDate.equals(other.orderExpirationDate)) {
			return false;
		}
        if (orderIdentification == null) {
            if (other.orderIdentification != null) {
				return false;
			}
        } else if (!orderIdentification.equals(other.orderIdentification)) {
			return false;
		}
        if (ojSupervisionId == null) {
            if (other.ojSupervisionId != null) {
				return false;
			}
        } else if (!ojSupervisionId.equals(other.ojSupervisionId)) {
			return false;
		}
        if (orderInitiatedCaseActivityAssociations == null) {
            if (other.orderInitiatedCaseActivityAssociations != null) {
				return false;
			}
        } else if (!orderInitiatedCaseActivityAssociations.equals(other.orderInitiatedCaseActivityAssociations)) {
			return false;
		}
        if (orderIssuanceDate == null) {
            if (other.orderIssuanceDate != null) {
				return false;
			}
        } else if (!orderIssuanceDate.equals(other.orderIssuanceDate)) {
			return false;
		}
        if (orderNumber == null) {
            if (other.orderNumber != null) {
				return false;
			}
        } else if (!orderNumber.equals(other.orderNumber)) {
			return false;
		}
        if (orderReceivedDate == null) {
            if (other.orderReceivedDate != null) {
				return false;
			}
        } else if (!orderReceivedDate.equals(other.orderReceivedDate)) {
			return false;
		}
        if (orderStartDate == null) {
            if (other.orderStartDate != null) {
				return false;
			}
        } else if (!orderStartDate.equals(other.orderStartDate)) {
			return false;
		}
        if (orderSubType == null) {
            if (other.orderSubType != null) {
				return false;
			}
        } else if (!orderSubType.equals(other.orderSubType)) {
			return false;
		}
        if (orderType == null) {
            if (other.orderType != null) {
				return false;
			}
        } else if (!orderType.equals(other.orderType)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "OrderType [orderIdentification=" + orderIdentification + ", ojSupervisionId=" + ojSupervisionId + ", orderClassification=" + orderClassification
                + ", orderType=" + orderType + ", orderSubType=" + orderSubType + ", orderCategory=" + orderCategory + ", orderNumber=" + orderNumber
                + ", orderDisposition=" + orderDisposition + ", comments=" + comments + ", orderIssuanceDate=" + orderIssuanceDate + ", orderReceivedDate="
                + orderReceivedDate + ", orderStartDate=" + orderStartDate + ", orderExpirationDate=" + orderExpirationDate + ", caseActivityInitiatedOrderAssociations="
                + caseActivityInitiatedOrderAssociations + ", orderInitiatedCaseActivityAssociations=" + orderInitiatedCaseActivityAssociations + ", isHoldingOrder="
                + isHoldingOrder + ", isSchedulingNeeded=" + isSchedulingNeeded + ", hasCharges=" + hasCharges + ", issuingAgency=" + issuingAgency + ", caseInfoIds="
                + caseInfoIds + ", chargeIds=" + chargeIds + ", conditionIds=" + conditionIds + "]";
    }

}
