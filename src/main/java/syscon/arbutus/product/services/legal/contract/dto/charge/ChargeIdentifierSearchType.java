package syscon.arbutus.product.services.legal.contract.dto.charge;

import java.io.Serializable;

import syscon.arbutus.product.services.legal.contract.dto.ValidationUtil;

/**
 * The representation of the charge identifier search type
 */
public class ChargeIdentifierSearchType implements Serializable {

    private static final long serialVersionUID = 1511719700154196325L;

    private String identifierType;
    private String identifierValue;
    private String identifierFormat;
    private Long organizationId;
    private Long facilityId;
    private String comment;

    /**
     * Default constructor
     */
    public ChargeIdentifierSearchType() {
    }

    /**
     * Constructor
     * At least one of the search criteria must be specified.
     *
     * @param identifierType   String - A set of numbers/identifiers assigned by an arresting agency, prosecuting attorney or court. E.g. CJIS code
     * @param identifierValue  String - The number or value of the charge identifier
     * @param identifierFormat String - The format for the Identifier.
     * @param organizationId   Long - Static reference to an Organization which issued the charge identifier
     * @param facilityId       Long - Static reference to a Facility which issued the charge identifier
     * @param comment          String - Comments associated to the identifier
     */
    public ChargeIdentifierSearchType(String identifierType, String identifierValue, String identifierFormat, Long organizationId, Long facilityId, String comment) {
        this.identifierType = identifierType;
        this.identifierValue = identifierValue;
        this.identifierFormat = identifierFormat;
        this.organizationId = organizationId;
        this.facilityId = facilityId;
        this.comment = comment;
    }

    /**
     * Check if properties are set properly. A empty string is treated as null.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set, false otherwise.
     */
    @Deprecated
    public Boolean isWellFormed() {

        if (ValidationUtil.isEmpty(identifierType) && ValidationUtil.isEmpty(identifierValue) && ValidationUtil.isEmpty(identifierFormat) && ValidationUtil.isEmpty(
                organizationId) && ValidationUtil.isEmpty(facilityId) && ValidationUtil.isEmpty(comment)) {
			return false;
		}

        return true;
    }

    /**
     * Verify if properties are set properly.
     * At least one of the search criteria must be specified.
     *
     * @return Boolean Return true if all required properties are set and all values are valid, false otherwise.
     */
    @Deprecated
    public Boolean isValid() {

        if (!isWellFormed()) {
			return false;
		}

        return true;
    }

    /**
     * Gets the value of the identifierType property. It is a reference code value of ChargeIdentifierType set.
     *
     * @return String - A set of numbers/identifiers assigned by an arresting agency, prosecuting attorney or court. E.g. CJIS code
     */
    public String getIdentifierType() {
        return identifierType;
    }

    /**
     * Sets the value of the identifierType property. It is a reference code value of ChargeIdentifierType set.
     *
     * @param identifierType String - A set of numbers/identifiers assigned by an arresting agency, prosecuting attorney or court. E.g. CJIS code
     */
    public void setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
    }

    /**
     * Gets the value of the identifierValue property.
     *
     * @return String - The number or value of the charge identifier
     */
    public String getIdentifierValue() {
        return identifierValue;
    }

    /**
     * Sets the value of the identifierValue property.
     *
     * @param identifierValue String - The number or value of the charge identifier
     */
    public void setIdentifierValue(String identifierValue) {
        this.identifierValue = identifierValue;
    }

    /**
     * Gets the value of the identifierFormat property. It is a reference code value of ChargeIdentifierFormat set.
     *
     * @return String - The format for the Identifier.
     */
    public String getIdentifierFormat() {
        return identifierFormat;
    }

    /**
     * Sets the value of the identifierFormat property. It is a reference code value of ChargeIdentifierFormat set.
     *
     * @param identifierFormat String - The format for the Identifier.
     */
    public void setIdentifierFormat(String identifierFormat) {
        this.identifierFormat = identifierFormat;
    }

    /**
     * Gets the value of the organization property.
     *
     * @return Long - Static reference to an Organization which issued the charge identifier
     */
    public Long getOrganizationId() {
        return organizationId;
    }

    /**
     * Sets the value of the organization property.
     *
     * @param organizationId Long - Static reference to an Organization which issued the charge identifier
     */
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * Gets the value of the facility property.
     *
     * @return Long - Static reference to a Facility which issued the charge identifier
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * Sets the value of the facility property.
     *
     * @param facilityId Long - Static reference to a Facility which issued the charge identifier
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * Gets the value of the comment property.
     *
     * @return String - Comments associated to the identifier
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     *
     * @param comment String - Comments associated to the identifier
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeIdentifierSearchType [getIdentifierType()=" + getIdentifierType() + ", getIdentifierValue()=" + getIdentifierValue() + ", getIdentifierFormat()="
                + getIdentifierFormat() + ", getOrganizationId()=" + getOrganizationId() + ", getFacilityId()=" + getFacilityId() + ", getComment()=" + getComment()
                + "]";
    }

}
