package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * IntermittentScheduleEntity for Legal Service
 *
 * @author amlendu.kumar
 * @version 1.0
 * @DbComment LEG_OrdIntmSenSch 'Intermittent Sentence Schedule table for Order Sentence Module of Legal Service'
 * @since May 06, 2014
 */
@Entity
@Audited
@Table(name = "LEG_OrdIntmSenSch")
public class IntermittentSentenceScheduleEntity implements Serializable {

    private static final long serialVersionUID = 4037946620203715030L;

    /**
     * @DbComment interSenScheduleId 'Unique identification of Intermittent Sentence Schedule record'
     */
    @Id
    @Column(name = "interSenScheduleId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDINTMSENSCH_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDINTMSENSCH_ID", sequenceName = "SEQ_LEG_ORDINTMSENSCH_ID", allocationSize = 1)
    private Long interSenScheduleId;

    /**
     * @DbComment dayOfWeek 'Day of week.'
     */
    @Column(name = "dayOfWeek", nullable = false, length = 64)
    @MetaCode(set = MetaSet.DAY)
    private String dayOfWeek;

    /**
     * @DbComment dayOutOfWeek 'Day of week.'
     */
    @Column(name = "dayOutOfWeek", nullable = false, length = 64)
    @MetaCode(set = MetaSet.DAY)
    private String dayOutOfWeek;

    /**
     * @DbComment startHour 'The hour of the start time'
     */
    @Column(name = "startHour", nullable = false)
    private Long startHour;

    /**
     * @DbComment startMinute 'The minute of the start time'
     */
    @Column(name = "startMinute", nullable = false)
    private Long startMinute;

    /**
     * @DbComment startSecond 'The second of the start time'
     */
    @Column(name = "startSecond", nullable = false)
    private Long startSecond;

    /**
     * @DbComment endHour 'The hour of the end time'
     */
    @Column(name = "endHour", nullable = false)
    private Long endHour;

    /**
     * @DbComment endMinute 'The minute of the end time'
     */
    @Column(name = "endMinute", nullable = false)
    private Long endMinute;

    /**
     * @DbComment endSecond 'The second of the end time'
     */
    @Column(name = "endSecond", nullable = false)
    private Long endSecond;

    /**
     * @DbComment reportingDate 'Intermittent reporting date'
     */
    @Column(name = "reportingDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date reportingDate;

    /**
     * @DbComment scheduleEndDate 'Intermittent schedule end date'
     */
    @Column(name = "scheduleEndDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduleEndDate;

    /**
     * @DbComment LocationId 'The reporting location'
     */
    @Column(name = "locationId", nullable = false)
    private Long locationId;

    /**
     * @DbComment outCome 'outcome of the schedule.'
     */
    @Column(name = "outCome", nullable = true, length = 64)
    @MetaCode(set = MetaSet.INTERMITTENT_SCHEDULE_OUTCOME)
    private String outCome;

    /**
     * @DbComment status 'status flag for active/inactive'
     */
    @Column(name = "status", nullable = true)
    private Boolean status;

    /**
     * @DbComment commentText 'The comment related to the schedule'
     */
    @Column(name = "commentText", nullable = true, length = 512)
    private String commentText;

    //	/**
    //	 * @DbComment interScheduleId 'column for Foreign Key of IntermittentSchedule'
    //	 */
    //	@ForeignKey(name= "FK_LEG_OrdIntmSch_ISS")
    //	@ManyToOne(fetch = FetchType.LAZY)
    //	@JoinColumn(name = "interScheduleId", nullable = true)
    //	private IntermittentScheduleEntity intermittentSchedule;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @Embedded
    @NotAudited
    private StampEntity stamp;

    /**
     * @DbComment orderId 'Unique identification of a order instance, foreign key to LEG_ORDORDER table'
     */
    @ForeignKey(name = "Fk_LEG_OrdIntmSch")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orderId", nullable = false)
    private SentenceEntity sentence;

    /**
     * Constructor
     */
    public IntermittentSentenceScheduleEntity() {
        super();
    }

    /**
     * @return the interSenScheduleId
     */
    public Long getInterSenScheduleId() {
        return interSenScheduleId;
    }

    /**
     * @param interSenScheduleId the interSenScheduleId to set
     */
    public void setInterSenScheduleId(Long interSenScheduleId) {
        this.interSenScheduleId = interSenScheduleId;
    }

    /**
     * @return the dayOfWeek
     */
    public String getDayOfWeek() {
        return dayOfWeek;
    }

    /**
     * @param dayOfWeek the dayOfWeek to set
     */
    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    /**
     * @return the dayOutOfWeek
     */
    public String getDayOutOfWeek() {
        return dayOutOfWeek;
    }

    /**
     * @param dayOutOfWeek the dayOutOfWeek to set
     */
    public void setDayOutOfWeek(String dayOutOfWeek) {
        this.dayOutOfWeek = dayOutOfWeek;
    }

    /**
     * @return the startHour
     */
    public Long getStartHour() {
        return startHour;
    }

    /**
     * @param startHour the startHour to set
     */
    public void setStartHour(Long startHour) {
        this.startHour = startHour;
    }

    /**
     * @return the startMinute
     */
    public Long getStartMinute() {
        return startMinute;
    }

    /**
     * @param startMinute the startMinute to set
     */
    public void setStartMinute(Long startMinute) {
        this.startMinute = startMinute;
    }

    /**
     * @return the startSecond
     */
    public Long getStartSecond() {
        return startSecond;
    }

    /**
     * @param startSecond the startSecond to set
     */
    public void setStartSecond(Long startSecond) {
        this.startSecond = startSecond;
    }

    /**
     * @return the endHour
     */
    public Long getEndHour() {
        return endHour;
    }

    /**
     * @param endHour the endHour to set
     */
    public void setEndHour(Long endHour) {
        this.endHour = endHour;
    }

    /**
     * @return the endMinute
     */
    public Long getEndMinute() {
        return endMinute;
    }

    /**
     * @param endMinute the endMinute to set
     */
    public void setEndMinute(Long endMinute) {
        this.endMinute = endMinute;
    }

    /**
     * @return the endSecond
     */
    public Long getEndSecond() {
        return endSecond;
    }

    /**
     * @param endSecond the endSecond to set
     */
    public void setEndSecond(Long endSecond) {
        this.endSecond = endSecond;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the sentence
     */
    public SentenceEntity getSentence() {
        return sentence;
    }

    /**
     * @param sentence the sentence to set
     */
    public void setSentence(SentenceEntity sentence) {
        this.sentence = sentence;
    }

    /**
     * @return the reportingDate
     */
    public Date getReportingDate() {
        return reportingDate;
    }

    /**
     * @param reportingDate the reportingDate to set
     */
    public void setReportingDate(Date reportingDate) {
        this.reportingDate = reportingDate;
    }

    /**
     * @return the scheduleEndDate
     */
    public Date getScheduleEndDate() {
        return scheduleEndDate;
    }

    /**
     * @param scheduleEndDate the scheduleEndDate to set
     */
    public void setScheduleEndDate(Date scheduleEndDate) {
        this.scheduleEndDate = scheduleEndDate;
    }

    /**
     * @return locationId
     */
    public Long getLocationId() {
        return locationId;
    }

    /**
     * @param locationId the locationId to set
     */
    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    /**
     * @return the outCome
     */
    public String getOutCome() {
        return outCome;
    }

    /**
     * @param outCome the outCome to set
     */
    public void setOutCome(String outCome) {
        this.outCome = outCome;
    }

    /**
     * @return the status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return the commentText
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * @param commentText the commentText to set
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    //	/**
    //	 * @return the intermittentSchedule
    //	 */
    //	public IntermittentScheduleEntity getIntermittentSchedule() {
    //		return intermittentSchedule;
    //	}
    //
    //	/**
    //	 * @param intermittentSchedule the intermittentSchedule to set
    //	 */
    //	public void setIntermittentSchedule(IntermittentScheduleEntity intermittentSchedule) {
    //		this.intermittentSchedule = intermittentSchedule;
    //	}

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((commentText == null) ? 0 : commentText.hashCode());
        result = prime * result + ((dayOfWeek == null) ? 0 : dayOfWeek.hashCode());
        result = prime * result + ((dayOutOfWeek == null) ? 0 : dayOutOfWeek.hashCode());
        result = prime * result + ((endHour == null) ? 0 : endHour.hashCode());
        result = prime * result + ((endMinute == null) ? 0 : endMinute.hashCode());
        result = prime * result + ((endSecond == null) ? 0 : endSecond.hashCode());
        result = prime * result + ((interSenScheduleId == null) ? 0 : interSenScheduleId.hashCode());
        result = prime * result + ((locationId == null) ? 0 : locationId.hashCode());
        result = prime * result + ((outCome == null) ? 0 : outCome.hashCode());
        result = prime * result + ((reportingDate == null) ? 0 : reportingDate.hashCode());
        result = prime * result + ((scheduleEndDate == null) ? 0 : scheduleEndDate.hashCode());
        result = prime * result + ((startHour == null) ? 0 : startHour.hashCode());
        result = prime * result + ((startMinute == null) ? 0 : startMinute.hashCode());
        result = prime * result + ((startSecond == null) ? 0 : startSecond.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        IntermittentSentenceScheduleEntity other = (IntermittentSentenceScheduleEntity) obj;
        if (commentText == null) {
            if (other.commentText != null) {
				return false;
			}
        } else if (!commentText.equals(other.commentText)) {
			return false;
		}
        if (dayOfWeek == null) {
            if (other.dayOfWeek != null) {
				return false;
			}
        } else if (!dayOfWeek.equals(other.dayOfWeek)) {
			return false;
		}
        if (dayOutOfWeek == null) {
            if (other.dayOutOfWeek != null) {
				return false;
			}
        } else if (!dayOutOfWeek.equals(other.dayOutOfWeek)) {
			return false;
		}
        if (endHour == null) {
            if (other.endHour != null) {
				return false;
			}
        } else if (!endHour.equals(other.endHour)) {
			return false;
		}
        if (endMinute == null) {
            if (other.endMinute != null) {
				return false;
			}
        } else if (!endMinute.equals(other.endMinute)) {
			return false;
		}
        if (endSecond == null) {
            if (other.endSecond != null) {
				return false;
			}
        } else if (!endSecond.equals(other.endSecond)) {
			return false;
		}
        if (interSenScheduleId == null) {
            if (other.interSenScheduleId != null) {
				return false;
			}
        } else if (!interSenScheduleId.equals(other.interSenScheduleId)) {
			return false;
		}
        if (locationId == null) {
            if (other.locationId != null) {
				return false;
			}
        } else if (!locationId.equals(other.locationId)) {
			return false;
		}
        if (outCome == null) {
            if (other.outCome != null) {
				return false;
			}
        } else if (!outCome.equals(other.outCome)) {
			return false;
		}
        if (reportingDate == null) {
            if (other.reportingDate != null) {
				return false;
			}
        } else if (!reportingDate.equals(other.reportingDate)) {
			return false;
		}
        if (scheduleEndDate == null) {
            if (other.scheduleEndDate != null) {
				return false;
			}
        } else if (!scheduleEndDate.equals(other.scheduleEndDate)) {
			return false;
		}
        if (startHour == null) {
            if (other.startHour != null) {
				return false;
			}
        } else if (!startHour.equals(other.startHour)) {
			return false;
		}
        if (startMinute == null) {
            if (other.startMinute != null) {
				return false;
			}
        } else if (!startMinute.equals(other.startMinute)) {
			return false;
		}
        if (startSecond == null) {
            if (other.startSecond != null) {
				return false;
			}
        } else if (!startSecond.equals(other.startSecond)) {
			return false;
		}
        if (status == null) {
            if (other.status != null) {
				return false;
			}
        } else if (!status.equals(other.status)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "IntermittentSentenceScheduleEntity [interSenScheduleId=" + interSenScheduleId + ", dayOfWeek=" + dayOfWeek + ", dayOutOfWeek=" + dayOutOfWeek
                + ", startHour=" + startHour + ", startMinute=" + startMinute + ", startSecond=" + startSecond + ", endHour=" + endHour + ", endMinute=" + endMinute
                + ", endSecond=" + endSecond + ", reportingDate=" + reportingDate + ", scheduleEndDate=" + scheduleEndDate + ", locationId=" + locationId + ", outCome="
                + outCome + ", status=" + status + ", commentText=" + commentText + "]";
    }

}
