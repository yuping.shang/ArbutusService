package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * SentenceAppealHistoryEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdSNTAppealHst 'Sentence Appeal History table for Order Sentence module of Legal service'
 * @since December 28, 2012
 */
//@Audited
@Entity
@Table(name = "LEG_OrdSNTAppealHst")
public class SentenceAppealHistoryEntity implements Serializable {

    private static final long serialVersionUID = -737931475168523735L;

    /**
     * @DbComment hstId 'Unique identification of a Sentence Appeal History instance'
     */
    @Id
    @Column(name = "hstId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDSNTAPPEALHST_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDSNTAPPEALHST_ID", sequenceName = "SEQ_LEG_ORDSNTAPPEALHST_ID", allocationSize = 1)
    private Long historyId;

    /**
     * @DbComment sentenceAppealId 'Unique identification of a Sentence Appeal instance'
     */
    @Column(name = "sentenceAppealId")
    private Long sentenceAppealId;

    /**
     * @DbComment appealStatus 'A status of an appeal on a sentence'
     */
    @Column(name = "appealStatus", nullable = false, length = 64)
    private String appealStatus;

    /**
     * @DbComment appealComment 'General comments on the outcome of the appeal'
     */
    @Column(name = "appealComment", nullable = true, length = 512)
    private String appealComment;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * Constructor
     */
    public SentenceAppealHistoryEntity() {
        super();
    }

    /**
     * Constructor
     *
     * @param historyId
     * @param sentenceAppealId
     * @param appealStatus
     * @param appealComment
     * @param stamp
     */
    public SentenceAppealHistoryEntity(Long historyId, Long sentenceAppealId, String appealStatus, String appealComment, StampEntity stamp) {
        super();
        this.historyId = historyId;
        this.sentenceAppealId = sentenceAppealId;
        this.appealStatus = appealStatus;
        this.appealComment = appealComment;
        this.stamp = stamp;
    }

    /**
     * @return the historyId
     */
    public Long getHistoryId() {
        return historyId;
    }

    /**
     * @param historyId the historyId to set
     */
    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    /**
     * @return the sentenceAppealId
     */
    public Long getSentenceAppealId() {
        return sentenceAppealId;
    }

    /**
     * @param sentenceAppealId the sentenceAppealId to set
     */
    public void setSentenceAppealId(Long sentenceAppealId) {
        this.sentenceAppealId = sentenceAppealId;
    }

    /**
     * @return the appealStatus
     */
    public String getAppealStatus() {
        return appealStatus;
    }

    /**
     * @param appealStatus the appealStatus to set
     */
    public void setAppealStatus(String appealStatus) {
        this.appealStatus = appealStatus;
    }

    /**
     * @return the appealComment
     */
    public String getAppealComment() {
        return appealComment;
    }

    /**
     * @param appealComment the appealComment to set
     */
    public void setAppealComment(String appealComment) {
        this.appealComment = appealComment;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((appealComment == null) ? 0 : appealComment.hashCode());
        result = prime * result + ((appealStatus == null) ? 0 : appealStatus.hashCode());
        result = prime * result + ((historyId == null) ? 0 : historyId.hashCode());
        result = prime * result + ((sentenceAppealId == null) ? 0 : sentenceAppealId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SentenceAppealHistoryEntity other = (SentenceAppealHistoryEntity) obj;
        if (appealComment == null) {
            if (other.appealComment != null) {
				return false;
			}
        } else if (!appealComment.equals(other.appealComment)) {
			return false;
		}
        if (appealStatus == null) {
            if (other.appealStatus != null) {
				return false;
			}
        } else if (!appealStatus.equals(other.appealStatus)) {
			return false;
		}
        if (historyId == null) {
            if (other.historyId != null) {
				return false;
			}
        } else if (!historyId.equals(other.historyId)) {
			return false;
		}
        if (sentenceAppealId == null) {
            if (other.sentenceAppealId != null) {
				return false;
			}
        } else if (!sentenceAppealId.equals(other.sentenceAppealId)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SentenceAppealHistoryEntity [historyId=" + historyId + ", sentenceAppealId=" + sentenceAppealId + ", appealStatus=" + appealStatus + ", appealComment="
                + appealComment + ", stamp=" + stamp + "]";
    }

}
