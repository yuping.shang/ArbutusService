package syscon.arbutus.product.services.legal.contract.dto;

/**
 * The enumeration of reference sets of this service.
 */
public enum ReferenceSet {

    APPEAL_STATUS("AppealStatus"),

    BAIL_TYPE("BailType"),
    BAIL_RELATIONSHIP("BailRelationship"),

    CASE_IDENTIFIER_TYPE("CaseIdentifierType"),
    CASE_TYPE("CaseType"),
    CASE_CATEGORY("CaseCategory"),
    CASE_STATUS_CHANGE_REASON("CaseStatusChangeReason"),

    TYPE_OF_CHARGE("TypeOfCharge"),
    CHARGE_DEGREE("ChargeDegree"),
    CHARGE_CODE("ChargeCode"),
    CHARGE_CATEGORY("ChargeCategory"),
    CHARGE_SEVERITY("ChargeSeverity"),
    CHARGE_INDICATOR("ChargeIndicator"),
    CHARGE_ENHANCING_FACTOR("ChargeEnhancingFactor"),
    CHARGE_REDUCING_FACTOR("ChargeReducingFactor"),
    CHARGE_OUTCOME("ChargeOutcome"),
    CHARGE_STATUS("ChargeStatus"),
    CHARGE_IDENTIFIER("ChargeIdentifier"),
    CHARGE_IDENTIFIER_FORMAT("ChargeIdentifierFormat"),
    EXTERNAL_CHARGE_CODE("ExternalChargeCode"),
    EXTERNAL_CHARGE_CODE_GROUP("ExternalChargeCodeGroup"),
    EXTERNAL_CHARGE_CODE_CATEGORY("ExternalChargeCodeCategory"),
    STATUTE_CODE("StatuteCode"),
    STATUTE_COUNTRY("Country"),                    //Refer to Person Service
    STATUTE_PROVINCE_STATE("StateProvince"),    //Refer to Person Service
    STATUTE_COUNTY("StatuteCounty"),
    CHARGE_PLEA("ChargePlea"),
    CHARGE_JURISDICTION("ChargeJurisdiction"),

    CURRENCY("Currency"),

    DAY("Day"),
    DURATION("Duration"),

    NOTIFICATION_TYPE("NotificationType"),

    ORDER_CLASSIFICATION("OrderClassification"),
    ORDER_TYPE("OrderType"),
    ORDER_SUB_TYPE("OrderSubType"),
    ORDER_STATUS("OrderStatus"),
    ORDER_OUTCOME("OrderOutcome"),
    ORDER_CATEGORY("OrderCategory"),

    SENTENCE_TYPE("SentenceType"),
    SENTENCE_STATUS("SentenceStatus"),

    TERM_TYPE("TermType"),
    TERM_NAME("TermName"),
    TERM_CATEGORY("TermCategory"),
    TERM_STATUS("TermStatus"),

    CONDITION_AMENDMENT_STATUS("AmendmentStatus"),
    CONDITION_TYPE("ConditionType"),
    CONDITION_CATEGORY("ConditionCategory"),
    CONDITION_SUB_CATEGORY("ConditionSubCategory"),
    CONDITION_PERIOD("ConditionPeriod"),
    CONDITION_DISTANCE("ConditionDistance"),
    CONDITION_CURRENCY("ConditionCurrency"),

    KEY_DATE("KeyDates"),
    KEY_DATE_OVERRIDE_REASON("KeyDateOverrideReason"),

    ADJUSTMENT_TYPE("AdjustmentType"),
    ADJUSTMENT_CLASSIFICATION("AdjustmentClassification"),
    ADJUSTMENT_FUNCTION("AdjustmentFunction"),
    ADJUSTMENT_STATUS("AdjustmentStatus"),
    APPLICATION_TYPE("ApplicationType"),

    AFFILIATION_CATEGORY("AffiliationCategory"),
    ASSOCIATION_TYPE("AssociationType"),
    PERSON_ROLE("PersonRole"),
    CALCULATION_INITIATOR("CalculationInitiator"),
    CALCULATION_REASON("CalculationReason");

    private final String value;

    /**
     * Constructor
     *
     * @param v one of the reference code set
     */
    ReferenceSet(String v) {
        value = v;
    }

    /**
     * Create a reference code set from string value.
     *
     * @param v String value.
     * @return ReferenceSet instance.
     */
    public static ReferenceSet fromValue(String v) {
        for (ReferenceSet c : ReferenceSet.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    /**
     * String value of the reference code set
     *
     * @return String
     */
    public String value() {
        return value;
    }

}
