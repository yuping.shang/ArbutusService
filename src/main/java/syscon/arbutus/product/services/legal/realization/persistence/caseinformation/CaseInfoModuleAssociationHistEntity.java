package syscon.arbutus.product.services.legal.realization.persistence.caseinformation;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the "0 to many" static module association history entity of CaseInfo Instance.
 *
 * @DbComment LEG_CIModuleAsscHist 'The static module association history table of CaseInfo Instance'
 * @DbComment LEG_CIModuleAsscHist.AssociationId 'Unique id of the association history record'
 * @DbComment LEG_CIModuleAsscHist.FromIdentifier 'The case information instance Id'
 * @DbComment LEG_CIModuleAsscHist.ToClass 'The association module that is referenced'
 * @DbComment LEG_CIModuleAsscHist.ToIdentifier 'The unique id of the Association To Object'
 * @DbComment LEG_CIModuleAsscHist.CreateUserId 'User ID who created the object'
 * @DbComment LEG_CIModuleAsscHist.CreateDateTime 'Date and time when the object was created'
 * @DbComment LEG_CIModuleAsscHist.InvocationContext 'Invocation context when the create/update action called'
 * @DbComment LEG_CIModuleAsscHist.ModifyUserId 'User ID who last updated the object'
 * @DbComment LEG_CIModuleAsscHist.ModifyDateTime 'Date and time when the object was last updated'
 * @DbComment LEG_CIModuleAsscHist.CaseInfoHistoryId 'The id of the parent history record'
 */
//@Audited
@Entity
@Table(name = "LEG_CIModuleAsscHist")
public class CaseInfoModuleAssociationHistEntity implements Serializable {

    private static final long serialVersionUID = -5730732177593839633L;

    @Id
    @Column(name = "AssociationId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CIModuleAsscHist_Id")
    @SequenceGenerator(name = "SEQ_LEG_CIModuleAsscHist_Id", sequenceName = "SEQ_LEG_CIModuleAsscHist_Id", allocationSize = 1)
    private Long associationId;

    @Column(name = "FromIdentifier", nullable = false)
    private Long fromIdentifier;

    @Column(name = "ToClass", nullable = false, length = 64)
    private String toClass;

    @Column(name = "ToIdentifier", nullable = false)
    private Long toIdentifier;

    @NotAudited
    @Embedded
    private StampEntity stamp;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CaseInfoHistoryId", nullable = false)
    private CaseInfoHistEntity caseInfoHistory;

    /**
     * Default constructor
     */
    public CaseInfoModuleAssociationHistEntity() {

    }

    /**
     * Constructor
     *
     * @param associationId
     * @param fromIdentifier
     * @param toClass
     * @param toIdentifier
     * @param stamp
     * @param caseInfoHistory
     */
    public CaseInfoModuleAssociationHistEntity(Long associationId, Long fromIdentifier, String toClass, Long toIdentifier, StampEntity stamp,
            CaseInfoHistEntity caseInfoHistory) {
        super();
        this.associationId = associationId;
        this.fromIdentifier = fromIdentifier;
        this.toClass = toClass;
        this.toIdentifier = toIdentifier;
        this.stamp = stamp;
        this.setCaseInfoHistory(caseInfoHistory);
    }

    /**
     * @return the caseInfo
     */
    public CaseInfoHistEntity getCaseInfo() {
        return caseInfoHistory;
    }

    /**
     * @param caseInfo the caseInfo to set
     */
    public void setCaseInfo(CaseInfoHistEntity caseInfoHistory) {
        this.caseInfoHistory = caseInfoHistory;
    }

    /**
     * Gets the value of the associationId property
     *
     * @return Long
     */
    public Long getAssociationId() {
        return associationId;
    }

    /**
     * Sets the value of the associationId property
     *
     * @param associationId Long
     */
    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    /**
     * Gets the value of the fromIdentifier property
     *
     * @return Long
     */
    public Long getFromIdentifier() {
        return fromIdentifier;
    }

    /**
     * Sets the value of the fromIdentifier property
     *
     * @param fromIdentifier Long
     */
    public void setFromIdentifier(Long fromIdentifier) {
        this.fromIdentifier = fromIdentifier;
    }

    /**
     * Gets the value of the toClass property
     *
     * @return String
     */
    public String getToClass() {
        return toClass;
    }

    /**
     * Sets the value of the toClass property
     *
     * @param toClass String
     */
    public void setToClass(String toClass) {
        this.toClass = toClass;
    }

    /**
     * Gets the value of the toIdentifier property
     *
     * @return Long
     */
    public Long getToIdentifier() {
        return toIdentifier;
    }

    /**
     * Sets the value of the toIdentifier property
     *
     * @param toIdentifier Long
     */
    public void setToIdentifier(Long toIdentifier) {
        this.toIdentifier = toIdentifier;
    }

    /**
     * Gets the value of the stamp property
     *
     * @return StampEntity
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * Sets the value of the stamp property
     *
     * @param stamp StampEntity
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the caseInfoHistory
     */
    public CaseInfoHistEntity getCaseInfoHistory() {
        return caseInfoHistory;
    }

    /**
     * @param caseInfoHistory the caseInfoHistory to set
     */
    public void setCaseInfoHistory(CaseInfoHistEntity caseInfoHistory) {
        this.caseInfoHistory = caseInfoHistory;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseInfoAssociationHistEntity [associationId=" + associationId + ", fromIdentifier=" + fromIdentifier + ", toClass=" + toClass + ", toIdentifier="
                + toIdentifier + ", stamp=" + stamp + "]";
    }

}
