package syscon.arbutus.product.services.legal.contract.interfaces;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationSearchType;
import syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationType;
import syscon.arbutus.product.services.legal.contract.dto.caseaffiliation.CaseAffiliationsReturnType;

/**
 * Interface of CaseAffiliation module
 * <pre>
 * <b>Purpose</b>
 * The CASE AFFILIATION module provides the ability to establish a relationship between an interested party
 * (that may be an organization) and a legal case. The interested party, in relation to the legal case,
 * can be a Victim, Witness, Co-Accused, Judge, Parole Supervisor, etc.
 * The interested party itself may be a person who is directly involved in the case, an organization
 * or a person who is a representative of an organization that is involved in the case.
 * <b>Scope</b>
 * <pre>
 * The CASE AFFILIATION service will be limited to representing the following affiliations within the context of a case:
 * •	Persons representing an organization involved in a case
 * •	Organizations involved in a case
 * •	Individual persons involved in a case
 * This service will have the following capabilities:
 * •	Maintain all affiliations listed above;
 * •	Maintain a person and an organization’s role in a case;
 * •	Identify interested parties of a case Victims, Judge, Lawyer, Prosecutor, etc.;
 * •	Identify persons that are Co-Accused;
 * •	Add, Update and De-activate an affiliation between an interested party and a case;
 * •	Search all interested parties (persons or organizations) of a case.
 * The CASE AFFILIATION is only used for associating individuals and organizations (other than the main accused) that have some involvement in a case, whereas the main accused is represented through the SUPERVISION to PERSON IDENTITY affiliation.  This service is also different from the PERSON-PERSON service which is used for defining relationships to a person, for e.g., family, friends, financial, keep-away etc.
 * The CASE AFFILIATION service will apply data privileges to support the sealing of cases as well as versioning so that a history of changes can be recorded and viewed.
 * The service will provide the ability for data exchange through NIEM.</pre>
 * <b>Parameter Specifications</b>
 * <li>Required - Must be presented.</li>
 * <li>Optional - If presented it will be considered.  If not presented it will be considered with some default value (even if default value is null).</li>
 * See {@link syscon.arbutus.product.services.core.contract.dto.ReturnCode}
 * for all error codes and descriptions.
 * </pre>
 */
public interface CaseAffiliation {

    /**
     * create -- Create a case affiliation.
     * <p>A Case association must exist when creating a case affiliation.
     *
     * @param uc              UserContext - Required
     * @param caseAffiliation CaseAffiliationType - Required
     * @return CaseAffiliationType
     * Throws exception if instance cannot be created.
     */
    public CaseAffiliationType createCaseAffiliation(UserContext uc, CaseAffiliationType caseAffiliation);

    /**
     * get -- Get/Retrieve a case affiliation by its Id.
     *
     * @param uc                UserContext - Required
     * @param caseAffiliationId java.lang.Long - Required
     * @return CaseAffiliationType
     * Throws exception if instance cannot be gotten.
     */
    public CaseAffiliationType getCaseAffiliation(UserContext uc, Long caseAffiliationId);

    /**
     * update -- Update a case affiliation.
     * <pre>
     * A case affiliation must exist in order to be updated.
     * <pre>
     *
     * @param uc              UserContext - Required
     * @param caseAffiliation CaseAffiliationType - Required.
     * @return CaseAffiliationType
     * Throws exception if instance cannot be updated.
     */
    public CaseAffiliationType updateCaseAffiliation(UserContext uc, CaseAffiliationType caseAffiliation);

    /**
     * Cascading delete of a single case affiliation.
     * Returns success code if the case affiliation can be deleted.
     * Returns error code if the case affiliation cannot be deleted.
     * <p>List of possible return codes: 1</p>
     *
     * @param uc                UserContext - Required
     * @param caseAffiliationId java.lang.Long - Required
     * @return Long
     * Throws exception if instance cannot be deleted.
     */
    public Long deleteCaseAffiliation(UserContext uc, Long caseAffiliationId);

    /**
     * Paging search for case affiliation.
     *
     * @param uc          {@link UserContext}
     * @param search      {@link CaseAffiliationSearchType} - search criteria instance.
     * @param startIndex  Long – Optional if null search returns at the start index
     * @param resultSize  Long – Optional if null search returns max number of results as specified in the service.properties file
     * @param resultOrder String – Optional if null search returns a default order
     * @return a list of case affiliation instances.
     * Returns empty list if no court activity can be found.
     * Throws exception if there is an error during search.
     */
    public CaseAffiliationsReturnType searchCaseAffiliation(UserContext uc, CaseAffiliationSearchType search, Long startIndex, Long resultSize, String resultOrder);

    public boolean hasCaseAffiliation(UserContext uc, Long personIdA, Long personIdB);

}
