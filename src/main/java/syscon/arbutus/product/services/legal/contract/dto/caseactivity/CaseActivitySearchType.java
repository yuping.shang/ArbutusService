package syscon.arbutus.product.services.legal.contract.dto.caseactivity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.CommentType;

/**
 * Case Activity Search Type
 *
 * @author lhan
 */
public class CaseActivitySearchType implements Serializable {

    private static final long serialVersionUID = 5857756143687175629L;

    /**
     * Association to a Case
     */
    private Long caseId;

    /**
     * Search for case activity that happened from this date.
     */
    private Date fromActivityOccurredDate;

    /**
     * Search for case activity that happened until this date.
     */
    private Date toActivityOccurredDate;

    /**
     * The broad category under which the event is classified.
     * Note that this determines which specific type of activity is created.
     */
    private String activityCategory;

    /**
     * The specific event type that falls under the activity category.
     */
    private String activityType;

    /**
     * Any comments associated with the event or activity.
     */
    private CommentType activityComment;

    /**
     * An outcome that may be recorded as a result of the activity - optional
     */
    private String activityOutcome;

    /**
     * If the status is active, the activity is considered to be relevant ie. Scheduled to occur or is completed.
     * If the status is inactive, the activity is no longer relevant ie. cancelled or no longer required. - required
     */
    private String activityStatus;

    /**
     * Indicates the list of charges that are part of the case event. This is a static reference to the Charge service. - optional
     */
    private Set<Long> chargeIds;

    private String chargeIdsSetMode;

    /**
     * Indicates the facility where the hearing will take place (e.g. court name, type, room and location).
     * This is a static association to the Facility service. - optional.
     */
    private Long facilityId;

    /**
     * Indicates the organization where the hearing will take place.
     * This is a static association to the Organization service. - optional.
     */
    private Long organizationId;

    /**
     * Indicates the judge who will be presiding over the court event if applicable.
     * This is just an input field and will have no reference to the Master Names Index (person service) - optional
     */
    private String judge;

    /**
     * The order that initiated this court event. Static association to the OrderSentence service. - optional
     */
    private Set<Long> orderInititatedIds;

    private String orderInititatedIdsSetMode;

    /**
     * The order that resulted from this court event. Static association to the OrderSentence service.. - optional
     */
    private Set<Long> orderResultedIds;

    private String orderResultedIdsSetMode;

    /**
     * Constructor
     */
    public CaseActivitySearchType() {
        super();
    }

    /**
     * Constructor
     * At least one of the search criteria must be specified.
     *
     * @param caseId                    Long - Optional.
     * @param fromActivityOccurredDate  Date - Optional.
     * @param toActivityOccurredDate    Date - Optional.
     * @param activityCategory          String - Optional.
     * @param activityType              String - Optional.
     * @param activityComment           CommentType - Optional.
     * @param activityOutcome           String - Optional.
     * @param activityStatus            String - Optional.
     * @param chargeIds                 Set<Long> - Optional.
     * @param chargeIdsSetMode          String - Optional. Default = "ALL".
     * @param facilityId                Long - Optional.
     * @param organizationId            Long - Optional.
     * @param judge                     String - Optional.
     * @param orderInititatedIds        Set<Long> - Optional.
     * @param orderInititatedIdsSetMode String - Optional. Default = "ALL".
     * @param orderResultedIds          Set<Long> - Optional.
     * @param orderResultedIdsSetMode   String - Optional. Default = "ALL".
     */
    public CaseActivitySearchType(Long caseId, Date fromActivityOccurredDate, Date toActivityOccurredDate, String activityCategory, String activityType,
            CommentType activityComment, String activityOutcome, String activityStatus, Set<Long> chargeIds, String chargeIdsSetMode, Long facilityId,
            Long organizationId, String judge, Set<Long> orderInititatedIds, String orderInititatedIdsSetMode, Set<Long> orderResultedIds,
            String orderResultedIdsSetMode) {
        super();
        this.caseId = caseId;
        this.fromActivityOccurredDate = fromActivityOccurredDate;
        this.toActivityOccurredDate = toActivityOccurredDate;
        this.activityCategory = activityCategory;
        this.activityType = activityType;
        this.activityComment = activityComment;
        this.activityOutcome = activityOutcome;
        this.activityStatus = activityStatus;
        this.chargeIds = chargeIds;
        this.chargeIdsSetMode = chargeIdsSetMode;
        this.facilityId = facilityId;
        this.organizationId = organizationId;
        this.judge = judge;
        this.orderInititatedIds = orderInititatedIds;
        this.orderInititatedIdsSetMode = orderInititatedIdsSetMode;
        this.orderResultedIds = orderResultedIds;
        this.orderResultedIdsSetMode = orderResultedIdsSetMode;
    }

    /**
     * Association to a Case
     *
     * @return the caseId
     */
    public Long getCaseId() {
        return caseId;
    }

    /**
     * Association to a Case
     *
     * @param caseId the caseId to set
     */
    public void setCaseId(Long caseId) {
        this.caseId = caseId;
    }

    /**
     * Search for case activity that happened from this date.
     *
     * @return the fromActivityOccurredDate
     */
    public Date getFromActivityOccurredDate() {
        return fromActivityOccurredDate;
    }

    /**
     * Search for case activity that happened from this date.
     *
     * @param fromActivityOccurredDate the fromActivityOccurredDate to set
     */
    public void setFromActivityOccurredDate(Date fromActivityOccurredDate) {
        this.fromActivityOccurredDate = fromActivityOccurredDate;
    }

    /**
     * Search for case activity that happened until this date.
     *
     * @return the toActivityOccurredDate
     */
    public Date getToActivityOccurredDate() {
        return toActivityOccurredDate;
    }

    /**
     * Search for case activity that happened until this date.
     *
     * @param toActivityOccurredDate the toActivityOccurredDate to set
     */
    public void setToActivityOccurredDate(Date toActivityOccurredDate) {
        this.toActivityOccurredDate = toActivityOccurredDate;
    }

    /**
     * The broad category under which the event is classified.
     * Note that this determines which specific type of activity is created.
     * It is a reference code of case activity category set.
     *
     * @return the activityCategory
     */
    public String getActivityCategory() {
        return activityCategory;
    }

    /**
     * The broad category under which the event is classified.
     * Note that this determines which specific type of activity is created.
     * It is a reference code of case activity category set.
     *
     * @param activityCategory the activityCategory to set
     */
    public void setActivityCategory(String activityCategory) {
        this.activityCategory = activityCategory;
    }

    /**
     * The specific event type that falls under the activity category
     * It is a reference code of case activity type set.
     *
     * @return the activityType
     */
    public String getActivityType() {
        return activityType;
    }

    /**
     * The specific event type that falls under the activity category
     * It is a reference code of case activity type set.
     *
     * @param activityType the activityType to set
     */
    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    /**
     * Any comments associated with the event or activity
     *
     * @return the activityComment
     */
    public CommentType getActivityComment() {
        return activityComment;
    }

    /**
     * Any comments associated with the event or activity
     *
     * @param activityComment the activityComment to set
     */
    public void setActivityComment(CommentType activityComment) {
        this.activityComment = activityComment;
    }

    /**
     * An outcome that may be recorded as a result of the activity
     * It is a reference code of case activity outcome set.
     *
     * @return the activityOutcome
     */
    public String getActivityOutcome() {
        return activityOutcome;
    }

    /**
     * An outcome that may be recorded as a result of the activity
     * It is a reference code of case activity outcome set.
     *
     * @param activityOutcome the activityOutcome to set
     */
    public void setActivityOutcome(String activityOutcome) {
        this.activityOutcome = activityOutcome;
    }

    /**
     * The status of the activity which indicates whether it has occurred/completed,
     * is planned for some time in the future or was supposed to occur but was not completed for some reason
     * It is a reference code of case activity status set.
     *
     * @return the activityStatus
     */
    public String getActivityStatus() {
        return activityStatus;
    }

    /**
     * The status of the activity which indicates whether it has occurred/completed,
     * is planned for some time in the future or was supposed to occur but was not completed for some reason
     * It is a reference code of case activity status set.
     *
     * @param activityStatus the activityStatus to set
     */
    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }

    /**
     * Indicates the list of charges that are part of the case event.
     * This is a static reference to the Charge service.
     *
     * @return the chargeIds
     */
    public Set<Long> getChargeIds() {
        return chargeIds;
    }

    /**
     * Indicates the list of charges that are part of the case event.
     * This is a static reference to the Charge service.
     *
     * @param chargeIds the chargeIds to set
     */
    public void setChargeIds(Set<Long> chargeIds) {
        this.chargeIds = chargeIds;
    }

    /**
     * @return the chargeIdsSetMode
     */
    public String getChargeIdsSetMode() {
        if (chargeIdsSetMode != null) {
			return chargeIdsSetMode;
		} else {
			return SearchSetMode.ALL.value();
		}
    }

    /**
     * @param chargeIdsSetMode the chargeIdsSetMode to set
     */
    public void setChargeIdsSetMode(String chargeIdsSetMode) {
        this.chargeIdsSetMode = chargeIdsSetMode;
    }

    /**
     * Indicates the facility where the hearing will take place (e.g. court name, type, room and location).
     * This is a static association to the Facility service.
     *
     * @return the facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * Indicates the facility where the hearing will take place (e.g. court name, type, room and location).
     * This is a static association to the Facility service.
     *
     * @param facilityId the facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * Indicates the organization where the hearing will take place .
     * This is a static association to the Organization service.
     *
     * @return the organizationId
     */
    public Long getOrganizationId() {
        return organizationId;
    }

    /**
     * Indicates the organization where the hearing will take place .
     * This is a static association to the Organization service.
     *
     * @param organizationId the organizationId to set
     */
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * Indicates the judge who will be presiding over the court event if applicable.
     * This is just an input field and will have no reference to the Master Names Index (person service)
     * Max length: 128 characters.
     *
     * @return the judge
     */
    public String getJudge() {
        return judge;
    }

    /**
     * Indicates the judge who will be presiding over the court event if applicable.
     * This is just an input field and will have no reference to the Master Names Index (person service)
     * Max length: 128 characters.
     *
     * @param judge the judge to set
     */
    public void setJudge(String judge) {
        this.judge = judge;
    }

    /**
     * The order that initiated this court event. Static association to the OrderSentence service.
     *
     * @return the orderInititatedIds
     */
    public Set<Long> getOrderInititatedIds() {
        return orderInititatedIds;
    }

    /**
     * The order that initiated this court event. Static association to the OrderSentence service.
     *
     * @param orderInititatedIds the orderInititatedIds to set
     */
    public void setOrderInititatedIds(Set<Long> orderInititatedIds) {
        this.orderInititatedIds = orderInititatedIds;
    }

    /**
     * @return the orderInititatedIdsSetMode
     */
    public String getOrderInititatedIdsSetMode() {
        if (orderInititatedIdsSetMode != null) {
			return orderInititatedIdsSetMode;
		} else {
			return SearchSetMode.ALL.value();
		}
    }

    /**
     * @param orderInititatedIdsSetMode the orderInititatedIdsSetMode to set
     */
    public void setOrderInititatedIdsSetMode(String orderInititatedIdsSetMode) {
        this.orderInititatedIdsSetMode = orderInititatedIdsSetMode;
    }

    /**
     * The order that resulted from this court event. Static association to the OrderSentence service.
     *
     * @return the orderResultedIds
     */
    public Set<Long> getOrderResultedIds() {
        return orderResultedIds;
    }

    /**
     * The order that resulted from this court event. Static association to the OrderSentence service.
     *
     * @param orderResultedIds the orderResultedIds to set
     */
    public void setOrderResultedIds(Set<Long> orderResultedIds) {
        this.orderResultedIds = orderResultedIds;
    }

    /**
     * @return the orderResultedIdsSetMode
     */
    public String getOrderResultedIdsSetMode() {
        if (orderResultedIdsSetMode != null) {
			return orderResultedIdsSetMode;
		} else {
			return SearchSetMode.ALL.value();
		}
    }

    /**
     * @param orderResultedIdsSetMode the orderResultedIdsSetMode to set
     */
    public void setOrderResultedIdsSetMode(String orderResultedIdsSetMode) {
        this.orderResultedIdsSetMode = orderResultedIdsSetMode;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
	 */
    @Override
    public String toString() {
        return "CaseActivitySearchType [caseId=" + caseId + ", fromActivityOccurredDate=" + fromActivityOccurredDate + ", toActivityOccurredDate="
                + toActivityOccurredDate + ", activityCategory=" + activityCategory + ", activityType=" + activityType + ", activityComment=" + activityComment
                + ", activityOutcome=" + activityOutcome + ", activityStatus=" + activityStatus + ", chargeIds=" + chargeIds + ", facilityId=" + facilityId
                + ", organizationId=" + organizationId + ", judge=" + judge + ", orderInititatedIds=" + orderInititatedIds + ", orderResultedIds=" + orderResultedIds
                + ", getChargeIdsSetMode()=" + getChargeIdsSetMode() + ", getOrderInititatedIdsSetMode()=" + getOrderInititatedIdsSetMode()
                + ", getOrderResultedIdsSetMode()=" + getOrderResultedIdsSetMode() + "]";
    }

}