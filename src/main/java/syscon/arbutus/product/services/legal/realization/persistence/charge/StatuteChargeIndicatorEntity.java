package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the statute charge indicator entity.
 *
 * @DbComment LEG_CHGStatChgIndicator 'The statute charge indicator data table'
 */
@Audited
@Entity
@Table(name = "LEG_CHGStatChgIndicator")
public class StatuteChargeIndicatorEntity implements Serializable {

    private static final long serialVersionUID = -2665525046968824694L;

    /**
     * @DbComment IndicatorId 'The unique Id for each statute charge indicator record.'
     */
    @Id
    @Column(name = "IndicatorId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGStatChgIndicator_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGStatChgIndicator_Id", sequenceName = "SEQ_LEG_CHGStatChgIndicator_Id", allocationSize = 1)
    private Long indicatorId;

    /**
     * @DbComment ChargeIndicator 'The indicators (alerts) on a charge.'
     */
    @MetaCode(set = MetaSet.CHARGE_INDICATOR)
    @Column(name = "ChargeIndicator", nullable = false, length = 64)
    private String chargeIndicator;

    /**
     * @DbComment HasIndicatorValue 'True if a value can be associated to an indicator, false otherwise, default is false'
     */
    @Column(name = "HasIndicatorValue", nullable = false)
    private Boolean hasIndicatorValue;

    /**
     * @DbComment IndicatorValue 'The value associated to the indicator can be specified only if the HasIndicatorValue is ‘true’. '
     */
    @Column(name = "IndicatorValue", nullable = true, length = 128)
    private String indicatorValue;

    /**
     * @DbComment StatuteChargeId 'The unique Id for each statute charge configuration data record.'
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "StatuteChargeId", nullable = false)
    private StatuteChargeConfigEntity statueChargeConfig;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     *
     */
    public StatuteChargeIndicatorEntity() {
    }

    /**
     * @param indicatorId
     * @param chargeIndicator
     * @param hasIndicatorValue
     * @param indicatorValue
     */
    public StatuteChargeIndicatorEntity(Long indicatorId, String chargeIndicator, Boolean hasIndicatorValue, String indicatorValue) {
        this.indicatorId = indicatorId;
        this.chargeIndicator = chargeIndicator;
        this.hasIndicatorValue = hasIndicatorValue;
        this.indicatorValue = indicatorValue;
    }

    /**
     * @return the indicatorId
     */
    public Long getIndicatorId() {
        return indicatorId;
    }

    /**
     * @param indicatorId the indicatorId to set
     */
    public void setIndicatorId(Long indicatorId) {
        this.indicatorId = indicatorId;
    }

    /**
     * @return the chargeIndicator
     */
    public String getChargeIndicator() {
        return chargeIndicator;
    }

    /**
     * @param chargeIndicator the chargeIndicator to set
     */
    public void setChargeIndicator(String chargeIndicator) {
        this.chargeIndicator = chargeIndicator;
    }

    /**
     * @return the hasIndicatorValue
     */
    public Boolean isHasIndicatorValue() {
        return hasIndicatorValue;
    }

    /**
     * @param hasIndicatorValue the hasIndicatorValue to set
     */
    public void setHasIndicatorValue(Boolean hasIndicatorValue) {
        this.hasIndicatorValue = hasIndicatorValue;
    }

    /**
     * @return the indicatorValue
     */
    public String getIndicatorValue() {
        return indicatorValue;
    }

    /**
     * @param indicatorValue the indicatorValue to set
     */
    public void setIndicatorValue(String indicatorValue) {
        this.indicatorValue = indicatorValue;
    }

    /**
     * @return the statueChargeConfig
     */
    public StatuteChargeConfigEntity getStatueChargeConfig() {
        return statueChargeConfig;
    }

    /**
     * @param statueChargeConfig the statueChargeConfig to set
     */
    public void setStatueChargeConfig(StatuteChargeConfigEntity statueChargeConfig) {
        this.statueChargeConfig = statueChargeConfig;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StatuteChargeIndicatorEntity [indicatorId=" + indicatorId + ", chargeIndicator=" + chargeIndicator + ", hasIndicatorValue=" + hasIndicatorValue
                + ", indicatorValue=" + indicatorValue + "]";
    }

}
