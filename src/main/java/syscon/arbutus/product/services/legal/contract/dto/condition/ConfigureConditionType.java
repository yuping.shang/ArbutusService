package syscon.arbutus.product.services.legal.contract.dto.condition;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;

/**
 * Configure ConditionType for Condition configuration of Case Management Service
 *
 * @author amlendu kumar
 * @version 1.0
 * @since May 21, 2014
 */
@ArbutusConstraint(constraints = { "activationDate <= deactivationDate" })
public class ConfigureConditionType implements Serializable {

    private static final long serialVersionUID = 1756326261330146336L;

    private Long id;
    @NotNull
    private Date activationDate;
    private Date deactivationDate;
    @NotNull
    private String code;
    @NotNull
    private String type;
    @NotNull
    private String category;
    private String subCategory;
    @NotNull
    @Size(max = 200, message = "max length 200")
    private String shortName;
    @NotNull
    @Size(max = 4000, message = "max length 4000")
    private String fullDetail;
    private boolean status;
    private String attribute;
    private Boolean autoPopulateToCasePlan;
    private String objective;
    private String casePlanType;

    /**
     * Get activationDate -- activation date of the Configure Condition.
     *
     * @return the activationDate
     */
    public Date getActivationDate() {
        return activationDate;
    }

    /**
     * Set activationDate -- activation date of the Configure Condition.
     *
     * @param activationDate the activationDate to set
     */
    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    /**
     * Get deactivationDate -- deactivation date of the Configure Condition.
     *
     * @return the deactivationDate
     */
    public Date getDeactivationDate() {
        return deactivationDate;
    }

    /**
     * Set deactivationDate -- deactivation date of the Configure Condition.
     *
     * @param deactivationDate the deactivationDate to set
     */
    public void setDeactivationDate(Date deactivationDate) {
        this.deactivationDate = deactivationDate;
    }

    /**
     * Get code -- code of the Configure Condition.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Set code -- code of the Configure Condition.
     *
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Get type -- Is a logical grouping of Configure Condition by Sentences, Orders, Charges etc.
     * It is a reference code value of ConditionType set.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Set type -- Is a logical grouping of Configure Condition by Sentences, Orders, Charges etc.
     * It is a reference code value of ConditionType set.
     *
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Get condition category -- A condition can also be classified under different categories like Financial, Programs etc.
     * It is a reference code value of ConditionCategory set.
     *
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Set category -- A condition can also be classified under different categories like Financial, Programs etc.
     * It is a reference code value of ConditionCategory set.
     *
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * Get sub category -- Is a logical sub grouping of the Configure Condition category. E.g.
     * Condition category for Financials, could be sub grouped as Restitution to victims, Electronic monitor payment etc.
     * It is a reference code value of ConditionSubCategory set.
     *
     * @return the subCategory
     */
    public String getSubCategory() {
        return subCategory;
    }

    /**
     * Set sub category -- Is a logical sub grouping of the Configure Condition category. E.g.
     * Condition category for Financials, could be sub grouped as Restitution to victims, Electronic monitor payment etc.
     * It is a reference code value of ConditionSubCategory set.
     *
     * @param subCategory the subCategory to set
     */
    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    /**
     * Get shortName -- short name of the Configure Condition.
     *
     * @return the shortName
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Set shortName -- short name of the Configure Condition.
     *
     * @param shortName the shortName to set
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * Get fullDetail -- full detail of the Configure Condition.
     * Length 4000 character
     *
     * @return the fullDetail
     */
    public String getFullDetail() {
        return fullDetail;
    }

    /**
     * Set fullDetail -- full detail of the Configure Condition.
     * Length 4000 character
     *
     * @param fullDetail the fullDetail to set
     */
    public void setFullDetail(String fullDetail) {
        this.fullDetail = fullDetail;
    }

    /**
     * Check the status of the configure condition.
     *
     * @return the status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * Get attribute -- attribute of the Configure Condition which could be Amount, Duration or Distant Restrictions.
     *
     * @return the attribute
     */
    public String getAttribute() {
        return attribute;
    }

    /**
     * Set attribute -- attribute of the Configure Condition which could be Amount, Duration or Distant Restrictions.
     *
     * @param attribute the attribute to set
     */
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    /**
     * Get autoPopulateToCasePlan -- auto populate to Case Plan indicator of the Configure Condition true/false.
     *
     * @return the autoPopulateToCasePlan
     */
    public Boolean getAutoPopulateToCasePlan() {
        return autoPopulateToCasePlan;
    }

    /**
     * Set autoPopulateToCasePlan -- auto populate to Case Plan indicator of the Configure Condition true/false.
     *
     * @param autoPopulateToCasePlan the autoPopulateToCasePlan to set
     */
    public void setAutoPopulateToCasePlan(Boolean autoPopulateToCasePlan) {
        this.autoPopulateToCasePlan = autoPopulateToCasePlan;
    }

    /**
     * Get id -- Unique system generated identifier for the Configuration Condition.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Set id -- Unique system generated identifier for the Configuration Condition.
     *
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Get objective -- objective of the Configure Condition, this is required if auto populate to Case Plan indicator is true.
     * Reference Code - Objectives
     *
     * @return the objective
     */
    public String getObjective() {
        return objective;
    }

    /**
     * Set objective -- objective of the Configure Condition, this is required if auto populate to Case Plan indicator is true.
     * Reference Code - Objectives
     *
     * @param objective the objective to set
     */
    public void setObjective(String objective) {
        this.objective = objective;
    }

    /**
     * Get casePlanType -- Case Plan Type of the Configure Condition, this is required if auto populate to Case Plan indicator is true.
     * Reference Code - CasePlanType
     *
     * @return the casePlanType
     */
    public String getCasePlanType() {
        return casePlanType;
    }

    /**
     * Set casePlanType -- Case Plan Type of the Configure Condition, this is required if auto populate to Case Plan indicator is true.
     * Reference Code - CasePlanType
     *
     * @param casePlanType the casePlanType to set
     */
    public void setCasePlanType(String casePlanType) {
        this.casePlanType = casePlanType;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ConfigureConditionType [id=" + id + ", activationDate=" + activationDate + ", deactivationDate=" + deactivationDate + ", code=" + code + ", type=" + type
                + ", category=" + category + ", subCategory=" + subCategory + ", shortName=" + shortName + ", fullDetail=" + fullDetail + ", status=" + status
                + ", attribute=" + attribute + ", autoPopulateToCasePlan=" + autoPopulateToCasePlan + ", objective=" + objective + ", casePlanType=" + casePlanType + "]";
    }

}
