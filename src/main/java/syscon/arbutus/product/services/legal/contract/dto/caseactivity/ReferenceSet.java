package syscon.arbutus.product.services.legal.contract.dto.caseactivity;

/**
 * Reference set names defined in the service SDD.
 *
 * @author lhan
 */
public enum ReferenceSet {

    /**
     * ActivityCategory reference code set
     */
    ACTIVITY_CATEGORY("CaseActivityCategory"),

    /**
     * ActivityType reference code set
     */
    ACTIVITY_TYPE("CaseActivityType"),

    /**
     * ActivityOutcome reference code set
     */
    ACTIVITY_OUTCOME("CaseActivityOutcome"),

    /**
     * ActivityStatus reference code set
     */
    ACTIVITY_STATUS("CaseActivityStatus");

    private final String value;

    /**
     * Constructor
     *
     * @param v one of the reference code set
     */
    ReferenceSet(String v) {
        value = v;
    }

    /**
     * Create a reference code set from string value.
     *
     * @param v String value.
     * @return ReferenceSet instance.
     */
    public static ReferenceSet fromValue(String v) {
        for (ReferenceSet c : ReferenceSet.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    /**
     * String value of the reference code set
     *
     * @return String
     */
    public String value() {
        return value;
    }

}
