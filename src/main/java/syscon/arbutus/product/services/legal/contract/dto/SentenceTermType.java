package syscon.arbutus.product.services.legal.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * SentenceTermType for Configure Sentence Term
 *
 * @author Ashish
 * @version 1.0
 * @since December 9, 2013
 */
public class SentenceTermType implements Serializable {

    private static final long serialVersionUID = -737931375114522737L;

    private Long sentenceTermId;

    @NotNull
    private String sentenceType;

    @NotNull
    private String termType;

    private List<SentenceTermNameType> sentenceTermNameList;

    private Date deactivationDate;

    /**
     * Constructor
     */
    public SentenceTermType() {
        super();
    }

    /**
     * Constructor
     *
     * @param sentenceTermId   String, id of SentenceTermType Object
     * @param sentenceType     Code, Required. The sentenceType of the term; DEF, INDETER, DETER etc..
     * @param termType         Code, Required. The type of term (SINGLE, DUAL, THREE etc.). This will be used by the sentence calculation module.
     * @param deactivationDate
     */
    public SentenceTermType(@NotNull Long sentenceTermId, @NotNull String sentenceType, @NotNull String termType, Date deactivationDate) {
        super();
        this.sentenceTermId = sentenceTermId;
        this.sentenceType = sentenceType;
        this.termType = termType;
        this.termType = termType;
        this.deactivationDate = deactivationDate;

    }

    /**
     * Constructor
     *
     * @param sentenceType     Code, Required. The sentenceType of the term; DEF, INDETER, DETER etc..
     * @param termType         Code, Required. The type of term (SINGLE, DUAL, THREE etc.). This will be used by the sentence calculation module.
     * @param deactivationDate
     */
    public SentenceTermType(@NotNull String sentenceType, @NotNull String termType, Date deactivationDate) {
        super();
        this.sentenceType = sentenceType;
        this.termType = termType;
        this.termType = termType;
        this.deactivationDate = deactivationDate;

    }

    /**
     * Get id of SentenceTermType Object
     *
     * @return the sentenceTermId
     */
    public Long getSentenceTermId() {
        return sentenceTermId;
    }

    /**
     * Set id of SentenceTermType Object
     *
     * @param the sentenceTermId
     */
    public void setSentenceTermId(Long sentenceTermId) {
        this.sentenceTermId = sentenceTermId;
    }

    /**
     * Get Sentence Type
     * -- The type of sentence (Definite, Indeterminant, Determinant etc.).
     *
     * @return the sentenceType
     */
    public String getSentenceType() {
        return sentenceType;
    }

    /**
     * Set Sentence Type
     * -- The type of term (Definite, Indeterminant, Determinant etc.).
     *
     * @param sentenceType the sentenceType to set
     */
    public void setSentenceType(String sentenceType) {
        this.sentenceType = sentenceType;
    }

    /**
     * Get Term Type
     * -- The type of term (Single Term, Dual Term, Three Term etc.).
     *
     * @return the termType
     */
    public String getTermType() {
        return termType;
    }

    /**
     * Set Term Type
     * -- The type of term (Single Term, Dual Term, Three Term etc.).
     *
     * @param termType the termType to set
     */
    public void setTermType(String termType) {
        this.termType = termType;
    }

    /**
     * Get List of Term Name
     * -- The name of the term ( Maximum, Minimum etc.).
     *
     * @return the sentenceTermNameList
     */
    public List<SentenceTermNameType> getSentenceTermNameList() {
        return sentenceTermNameList;
    }

    /**
     * Set sentenceTermNameList
     * -- -- The name of the term ( Maximum, Minimum etc.).
     *
     * @param sentenceTermNameList the list of termName to set
     */
    public void setSentenceTermNameList(List<SentenceTermNameType> sentenceTermNameList) {
        this.sentenceTermNameList = sentenceTermNameList;
    }

    /**
     * Get deactivationDate of SentenceTermType Object
     *
     * @return the sentenceTermTypeId
     */
    public Date getDeactivationDate() {
        return deactivationDate;
    }

    /**
     * Set deactivationDate of SentenceTermType Object
     *
     * @param the sentenceTermTypeId
     */
    public void setDeactivationDate(Date deactivationDate) {
        this.deactivationDate = deactivationDate;
    }

}
