package syscon.arbutus.product.services.legal.contract.dto;

import java.io.Serializable;

/**
 * The representation of the currency configuration return type
 */
public class CurrencyConfigReturnType implements Serializable {

    private static final long serialVersionUID = -3731625418597973399L;

    private CurrencyConfigType currencyConfig;
    private Long returnCode = 0L;

    /**
     * Gets the value of the currencyConfig property.
     *
     * @return CurrencyConfigType - a currency configuration instance
     */
    public CurrencyConfigType getCurrencyConfig() {
        return currencyConfig;
    }

    /**
     * Sets the value of the currencyConfig property.
     *
     * @param currencyConfig CurrencyConfigType - a currency configuration instance
     */
    public void setCurrencyConfig(CurrencyConfigType currencyConfig) {
        this.currencyConfig = currencyConfig;
    }

    /**
     * Gets the value of the returnCode property.
     *
     * @return Long
     */
    public Long getReturnCode() {
        return returnCode;
    }

    /**
     * Sets the value of the returnCode property.
     *
     * @param returnCode Long
     */
    public void setReturnCode(Long returnCode) {
        this.returnCode = returnCode;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CurrencyConfigReturnType [currencyConfig=" + currencyConfig + ", returnCode=" + returnCode + "]";
    }

}
