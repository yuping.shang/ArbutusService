package syscon.arbutus.product.services.legal.contract.dto.sentenceadjustment;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

import syscon.arbutus.product.services.core.contract.dto.StampType;

/**
 * Created with IntelliJ IDEA.
 * User: yshang
 * Date: 03/10/13
 * Time: 9:52 AM
 * To change this template use File | Settings | File Templates.
 */
public class AdjustmentConfigType implements Serializable {

    private static final long serialVersionUID = -326134984553932425L;

    /**
     * adjustmentType:
     * GT, JT, LGT, WORKCDT, LWRKCRD, PRGCDT, ESCP, OICDBT, WKNCDT, STATCDT
     */
    @NotNull
    private String adjustmentType;

    /**
     * adjustmentClassification: SYSG, MANUAL
     */
    @NotNull
    private String adjustmentClassification;

    /**
     * applicationType -- ASENT: Single Sentence; AGGSENT: Aggregated Sentence; BOOKLEV: Booking Level
     */
    private String applicationType;

    /**
     * adjustmentFunction: CRE, DEB
     */
    @NotNull
    private String adjustmentFunction;

    /**
     * adjustmentStatus: INCL, EXCL, PEND
     */
    @NotNull
    private String adjustmentStatus;

    /**
     * specialAdjustment, the days to be adjusted
     */
    private Long specialAdjustment;

    private String comment;
    private Long byStaffId;

    private StampType stamp;

    /**
     * getAdjustmentType Get Adjustment Type
     * <p>Reference Code, GT, JT, LGT, WORKCDT, LWRKCRD, PRGCDT, ESCP, OICDBT, WKNCDT, STATCDT, of Reference Set AdjustmentType
     * <p>Required</p>
     *
     * @return String
     */
    public String getAdjustmentType() {
        return adjustmentType;
    }

    /**
     * setAdjustmentType Set Adjustment Type
     * <p>Reference Code, GT, JT, LGT, WORKCDT, LWRKCRD, PRGCDT, ESCP, OICDBT, WKNCDT, STATCDT, of Reference Set AdjustmentType
     * <p>Required</p>
     *
     * @param adjustmentType
     */
    public void setAdjustmentType(String adjustmentType) {
        this.adjustmentType = adjustmentType;
    }

    /**
     * getAdjustmentClassification Get Adjustment Classification
     * <p>Reference code, SYSG, MANUAL, of Reference Set AdjustmentClassification
     * <p>Optional</p>
     *
     * @return String
     */
    public String getAdjustmentClassification() {
        return adjustmentClassification;
    }

    /**
     * setAdjustmentClassification Set Adjustment Classification
     * <p>Reference code, SYSG, MANUAL, of Reference Set AdjustmentClassification
     * <p>Optional</p>
     *
     * @param adjustmentClassification String
     */
    public void setAdjustmentClassification(String adjustmentClassification) {
        this.adjustmentClassification = adjustmentClassification;
    }

    /**
     * getApplicationType Get Application Type
     * <p>Reference Code, ASENT, AGGSENT, BOOKLEV, of Reference Set ApplicationType
     * <p>Optional</p>
     *
     * @return
     */
    public String getApplicationType() {
        return applicationType;
    }

    /**
     * setApplicationType Set Application Type
     * <p>Reference Code, ASENT, AGGSENT, BOOKLEV, of Reference Set ApplicationType
     * <p>Optional</p>
     *
     * @param applicationType
     */
    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    /**
     * getAdjustmentFunction Get Adjustment Function
     * <p>Reference Code, CRE, DEB, of Reference Set AdjustmentFunction
     * <p>Optional</p>
     *
     * @return String
     */
    public String getAdjustmentFunction() {
        return adjustmentFunction;
    }

    /**
     * setAdjustmentFunction Set Adjustment Function
     * <p>Reference Code, CRE, DEB, of Reference Set AdjustmentFunction
     * <p>Optional</p>
     *
     * @param adjustmentFunction
     */
    public void setAdjustmentFunction(String adjustmentFunction) {
        this.adjustmentFunction = adjustmentFunction;
    }

    /**
     * getAdjustmentStatus Get Adjustment Status
     * <p>Reference Code, INCL, EXCL, PEND, of Reference Set AdjustmentStatus
     * <p>Optional</p>
     *
     * @return String
     */
    public String getAdjustmentStatus() {
        return adjustmentStatus;
    }

    /**
     * setAdjustmentStatus Set Adjustment Status
     * <p>Reference Code, INCL, EXCL, PEND, of Reference Set AdjustmentStatus
     * <p>Optional</p>
     *
     * @param adjustmentStatus
     */
    public void setAdjustmentStatus(String adjustmentStatus) {
        this.adjustmentStatus = adjustmentStatus;
    }

    /**
     * getSpecialAdjustment Get Special Adjustment
     * <p>Optional</p>
     *
     * @return Long
     */
    public Long getSpecialAdjustment() {
        return specialAdjustment;
    }

    /**
     * setSpecialAdjustment Set Special Adjustment
     * <p>Optional</p>
     *
     * @param
     */
    public void setSpecialAdjustment(Long specialAdjustment) {
        this.specialAdjustment = specialAdjustment;
    }

    /**
     * getComment Get Comment
     * <p>Optional</p>
     *
     * @return String
     */
    public String getComment() {
        return comment;
    }

    /**
     * setComment Set Comment
     * <p>Optional</p>
     *
     * @param comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * getByStaffId getByStaffId
     * <p>Optional</p>
     *
     * @return Long
     */
    public Long getByStaffId() {
        return byStaffId;
    }

    /**
     * setByStaffId Set By Staff Id
     * <p>Optional</p>
     *
     * @param byStaffId
     */
    public void setByStaffId(Long byStaffId) {
        this.byStaffId = byStaffId;
    }

    /**
     * getStamp Get Stamp
     * <p>System generated</p>
     *
     * @return StampType
     */
    public StampType getStamp() {
        return stamp;
    }

    /**
     * setStamp Set Stamp
     * <p>System generated</p>
     *
     * @param stamp
     */
    public void setStamp(StampType stamp) {
        this.stamp = stamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (!(o instanceof AdjustmentConfigType)) {
			return false;
		}

        AdjustmentConfigType that = (AdjustmentConfigType) o;

        if (adjustmentClassification != null ? !adjustmentClassification.equals(that.adjustmentClassification) : that.adjustmentClassification != null) {
			return false;
		}
        if (adjustmentFunction != null ? !adjustmentFunction.equals(that.adjustmentFunction) : that.adjustmentFunction != null) {
			return false;
		}
        if (adjustmentStatus != null ? !adjustmentStatus.equals(that.adjustmentStatus) : that.adjustmentStatus != null) {
			return false;
		}
        if (adjustmentType != null ? !adjustmentType.equals(that.adjustmentType) : that.adjustmentType != null) {
			return false;
		}
        if (applicationType != null ? !applicationType.equals(that.applicationType) : that.applicationType != null) {
			return false;
		}
        if (specialAdjustment != null ? !specialAdjustment.equals(that.specialAdjustment) : that.specialAdjustment != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = adjustmentType != null ? adjustmentType.hashCode() : 0;
        result = 31 * result + (adjustmentClassification != null ? adjustmentClassification.hashCode() : 0);
        result = 31 * result + (applicationType != null ? applicationType.hashCode() : 0);
        result = 31 * result + (adjustmentFunction != null ? adjustmentFunction.hashCode() : 0);
        result = 31 * result + (adjustmentStatus != null ? adjustmentStatus.hashCode() : 0);
        result = 31 * result + (specialAdjustment != null ? specialAdjustment.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AdjustmentConfigType{" +
                "adjustmentType='" + adjustmentType + '\'' +
                ", adjustmentClassification='" + adjustmentClassification + '\'' +
                ", applicationType='" + applicationType + '\'' +
                ", adjustmentFunction='" + adjustmentFunction + '\'' +
                ", adjustmentStatus='" + adjustmentStatus + '\'' +
                ", specialAdjustment=" + specialAdjustment +
                ", comment='" + comment + '\'' +
                ", byStaffId=" + byStaffId +
                ", stamp=" + stamp +
                '}';
    }
}
