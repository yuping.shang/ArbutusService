/**
 *
 */
package syscon.arbutus.product.services.legal.contract.dto.caseaffiliation;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import syscon.arbutus.product.services.core.common.Individual;
import syscon.arbutus.product.services.core.contract.dto.ArbutusConstraint;
import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.core.contract.dto.ServiceConstants;

/**
 * The representation of the case affiliation type.
 *
 * @author lhan
 */
@ArbutusConstraint(constraints = { "startDate <= endDate" })
public class CaseAffiliationType implements Serializable {

    private static final long serialVersionUID = 4166943476950319960L;

    private Long caseAffiliationId;
    private String caseAffiliationCategory;
    @NotNull
    private String caseAffiliationType; //A code in AssociationType set
    @NotNull
    private String caseAffiliationRole; //A code in PersonRole set
    @NotNull
    private Date startDate;
    private Date endDate;
    private boolean isActiveFlag;
    private CommentType comment;

    @Size(max = ServiceConstants.TEXT_LENGTH_SMALL, message = "max length 64")
    private String orgnizationPersonJobPosition;

    //Association ID fields
    private Long affiliatedOrganizationId;
    private Long affiliatedPersonIdentityId;
    @NotNull
    private Long affiliatedCaseId;
    private Set<Long> affiliatedCaseActivityIds;

    //Composite data from other services.
    private Individual affiliatedPersonInfo;
    private String affiliatedOrganizationName;

    private Long personPersonId;

    /**
     *
     */
    public CaseAffiliationType() {
    }

    /**
     * @param caseAffiliationId            Long - Optional when create, Required when update. Uniquely identifier of case affiliation object.
     * @param caseAffiliationCategory      String - Ignored. Indicates whether this is an affiliation to an organization, person identity within the organization or just a person identity
     * @param caseAffiliationType          String - Required. Indicates what type of affiliation to a case this is eg. legal, probationary
     * @param caseAffiliationRole          String - Required. Indicates what role this affiliation plays in the case eg. witness
     * @param startDate                    Date - Required. A date when the affiliation becomes valid
     * @param endDate                      Date - Optional. A date when an affiliation ends
     * @param isActiveFlag                 boolean - Ignored. Indicates if the affiliation is Active or Inactive (derived)
     * @param comment                      CommentType - Optional. Stores any comments related to the affiliation
     * @param orgnizationPersonJobPosition String Optional - Describes the job position the individual holds in the organization eg CEO, Accountant.
     * @param affiliatedOrganizationId     Long - Optional. The affiliation to an organization
     * @param affiliatedPersonIdentityId   Long - Optional. The affiliation is to an existing person identity who can belong or not belong to an organization involved in a case.
     * @param affiliatedCaseId             Long - Required. The affiliation is to an existing  case.
     * @param affiliatedCaseActivityIds    Set<Long> - Optional. all affiliated case activities.
     */
    public CaseAffiliationType(Long caseAffiliationId, String caseAffiliationCategory, String caseAffiliationType, String caseAffiliationRole, Date startDate,
            Date endDate, boolean isActiveFlag, CommentType comment, String orgnizationPersonJobPosition, Long affiliatedOrganizationId, Long affiliatedPersonIdentityId,
            Long affiliatedCaseId, Set<Long> affiliatedCaseActivityIds) {
        super();
        this.caseAffiliationId = caseAffiliationId;
        this.caseAffiliationCategory = caseAffiliationCategory;
        this.caseAffiliationType = caseAffiliationType;
        this.caseAffiliationRole = caseAffiliationRole;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isActiveFlag = isActiveFlag;
        this.comment = comment;
        this.orgnizationPersonJobPosition = orgnizationPersonJobPosition;
        this.setAffiliatedOrganizationId(affiliatedOrganizationId);
        this.setAffiliatedPersonIdentityId(affiliatedPersonIdentityId);
        this.affiliatedCaseId = affiliatedCaseId;
        this.setAffiliatedCaseActivityIds(affiliatedCaseActivityIds);
    }

    /**
     * Optional when create, Required when update. Uniquely identifier of case affiliation object.
     *
     * @return the caseAffiliationId
     */
    public Long getCaseAffiliationId() {
        return caseAffiliationId;
    }

    /**
     * Optional when create, Required when update. Uniquely identifier of case affiliation object.
     *
     * @param caseAffiliationId the caseAffiliationId to set
     */
    public void setCaseAffiliationId(Long caseAffiliationId) {
        this.caseAffiliationId = caseAffiliationId;
    }

    /**
     * Required. Indicates whether this is an affiliation to an organization, person identity within the organization or just a person identity
     *
     * @return the caseAffiliationCategory. One code value of Affiliation Category set.
     */
    public String getCaseAffiliationCategory() {
        return caseAffiliationCategory;
    }

    /**
     * Required. Indicates whether this is an affiliation to an organization, person identity within the organization or just a person identity
     *
     * @param caseAffiliationCategory the caseAffiliationCategory to set. One code value of Affiliation Category set.
     */
    public void setCaseAffiliationCategory(String caseAffiliationCategory) {
        this.caseAffiliationCategory = caseAffiliationCategory;
    }

    /**
     * Required. Indicates what type of affiliation to a case this is eg. legal, probationary
     *
     * @return the caseAffiliationType. One code value of AssociationType set.
     */
    public String getCaseAffiliationType() {
        return caseAffiliationType;
    }

    /**
     * Required. Indicates what type of affiliation to a case this is eg. legal, probationary
     *
     * @param caseAffiliationType the caseAffiliationType to set. One code value of AssociationType set.
     */
    public void setCaseAffiliationType(String caseAffiliationType) {
        this.caseAffiliationType = caseAffiliationType;
    }

    /**
     * Required. Indicates what role this affiliation plays in the case eg. witness
     *
     * @return the caseAffiliationRole. One code value of PersonRole set.
     */
    public String getCaseAffiliationRole() {
        return caseAffiliationRole;
    }

    /**
     * Required. Indicates what role this affiliation plays in the case eg. witness
     *
     * @param caseAffiliationRole the caseAffiliationRole to set. One code value of PersonRole set.
     */
    public void setCaseAffiliationRole(String caseAffiliationRole) {
        this.caseAffiliationRole = caseAffiliationRole;
    }

    /**
     * Required. A date when the affiliation becomes valid.
     * time partial will be ignored and set to 00:00:00
     *
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Required. A date when the affiliation becomes valid.
     * Time partial will be ignored and set to 00:00:00
     *
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Optional. A date when an affiliation ends.
     * Time partial will be ignored and set to 00:00:00
     *
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Optional. A date when an affiliation ends.
     * Time partial will be ignored and set to 00:00:00
     *
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Ignored. Indicates if the affiliation is Active or Inactive (derived)
     *
     * @return the isActiveFlag
     */
    public boolean isActiveFlag() {
        return isActiveFlag;
    }

    /**
     * Ignored. Indicates if the affiliation is Active or Inactive (derived)
     *
     * @param isActiveFlag the isActiveFlag to set
     */
    public void setActiveFlag(boolean isActiveFlag) {
        this.isActiveFlag = isActiveFlag;
    }

    /**
     * Optional. Stores any comments related to the affiliation
     *
     * @return the comment
     */
    public CommentType getComment() {
        return comment;
    }

    /**
     * Optional. Stores any comments related to the affiliation
     *
     * @param comment the comment to set
     */
    public void setComment(CommentType comment) {
        this.comment = comment;
    }

    /**
     * Optional - Describes the job position the individual holds in the organization eg CEO, Accountant.
     *
     * @return the orgnizationPersonJobPosition
     */
    public String getOrgnizationPersonJobPosition() {
        return orgnizationPersonJobPosition;
    }

    /**
     * Optional - Describes the job position the individual holds in the organization eg CEO, Accountant.
     *
     * @param orgnizationPersonJobPosition the orgnizationPersonJobPosition to set
     */
    public void setOrgnizationPersonJobPosition(String orgnizationPersonJobPosition) {
        this.orgnizationPersonJobPosition = orgnizationPersonJobPosition;
    }

    /**
     * Required. The affiliation is to an existing case.
     *
     * @return the affiliatedCaseId
     */
    public Long getAffiliatedCaseId() {
        return affiliatedCaseId;
    }

    /**
     * Required. The affiliation is to an existing case.
     *
     * @param affiliatedCaseId the affiliatedCaseId to set
     */
    public void setAffiliatedCaseId(Long affiliatedCaseId) {
        this.affiliatedCaseId = affiliatedCaseId;
    }

    /**
     * Optional. The affiliation to an organization
     *
     * @return the affiliatedOrganizationId
     */
    public Long getAffiliatedOrganizationId() {
        return affiliatedOrganizationId;
    }

    /**
     * Optional. The affiliation to an organization
     *
     * @param affiliatedOrganizationId the affiliatedOrganizationId to set
     */
    public void setAffiliatedOrganizationId(Long affiliatedOrganizationId) {
        this.affiliatedOrganizationId = affiliatedOrganizationId;
    }

    /**
     * Optional. The affiliation is to an existing person identity who can belong or not belong to an organization involved in a case.
     *
     * @return the affiliatedPersonIdentityId
     */
    public Long getAffiliatedPersonIdentityId() {
        return affiliatedPersonIdentityId;
    }

    /**
     * Optional. The affiliation is to an existing person identity who can belong or not belong to an organization involved in a case.
     *
     * @param affiliatedPersonIdentityId the affiliatedPersonIdentityId to set
     */
    public void setAffiliatedPersonIdentityId(Long affiliatedPersonIdentityId) {
        this.affiliatedPersonIdentityId = affiliatedPersonIdentityId;
    }

    /**
     * Optional. all affiliated case activities.
     *
     * @return the affiliatedCaseActivityIds
     */
    public Set<Long> getAffiliatedCaseActivityIds() {
        return affiliatedCaseActivityIds;
    }

    /**
     * Optional. all affiliated case activities.
     *
     * @param affiliatedCaseActivityIds the affiliatedCaseActivityIds to set
     */
    public void setAffiliatedCaseActivityIds(Set<Long> affiliatedCaseActivityIds) {
        this.affiliatedCaseActivityIds = affiliatedCaseActivityIds;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CaseAffiliationType [getCaseAffiliationId()=" + getCaseAffiliationId() + ", getCaseAffiliationCategory()=" + getCaseAffiliationCategory()
                + ", getCaseAffiliationType()=" + getCaseAffiliationType() + ", getCaseAffiliationRole()=" + getCaseAffiliationRole() + ", getStartDate()="
                + getStartDate() + ", getEndDate()=" + getEndDate() + ", isActiveFlag()=" + isActiveFlag() + ", getComment()=" + getComment()
                + ", getOrgnizationPersonJobPosition()=" + getOrgnizationPersonJobPosition() + "]";
    }

    /**
     * Composite data from other services. Web input will be ignored.
     *
     * @return the affiliatedPersonInfo
     */
    public Individual getAffiliatedPersonInfo() {
        return affiliatedPersonInfo;
    }

    /**
     * Composite data from PersonIdentity object. Web input will be ignored.
     *
     * @param affiliatedPersonInfo the affiliatedPersonInfo to set
     */
    public void setAffiliatedPersonInfo(Individual affiliatedPersonInfo) {
        this.affiliatedPersonInfo = affiliatedPersonInfo;
    }

    /**
     * Composite data from Organization object. Web input will be ignored.
     *
     * @return the affiliatedOrganizationName
     */
    public String getAffiliatedOrganizationName() {
        return affiliatedOrganizationName;
    }

    /**
     * Composite data from Organization object. Web input will be ignored.
     *
     * @param affiliatedOrganizationName the affiliatedOrganizationName to set
     */
    public void setAffiliatedOrganizationName(String affiliatedOrganizationName) {
        this.affiliatedOrganizationName = affiliatedOrganizationName;
    }

    /**
     * personPersonId for defining Association/NonAssociation for the CaseAffliation Obj
     *
     * @return personPersonId the personPersonId for the CaseAffliation Obj
     */
    public Long getPersonPersonId() {
        return personPersonId;
    }

    /**
     * personPersonId for defining Association/NonAssociation for the CaseAffliation Obj
     *
     * @param personPersonId the personPersonId for the CaseAffliation Obj
     */
    public void setPersonPersonId(Long personPersonId) {
        this.personPersonId = personPersonId;
    }

}
