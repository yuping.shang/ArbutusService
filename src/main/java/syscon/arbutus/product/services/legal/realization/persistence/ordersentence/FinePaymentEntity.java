package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * FinePayment Entity of Legal Service (Order Sentence)
 *
 * @author amlendu kumar
 * @version 1.0
 * @DbComment LEG_OrdFinePayment 'Fine-in-Default Sentences'
 * @since September 3, 2014
 */
@Audited
@Entity
@Table(name = "LEG_OrdFinePayment")
@SQLDelete(sql = "UPDATE LEG_OrdFinePayment SET flag = 4 WHERE saFinePaymentId = ?")
@Where(clause = "flag = 1")
public class FinePaymentEntity implements Serializable {

    private static final long serialVersionUID = -1083412795614193402L;

    /**
     * @DbComment saFinePaymentId 'Unique system generated identifier for the FinePayment.'
     */
    @Id
    @Column(name = "saFinePaymentId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_OrdFinePayment_Id")
    @SequenceGenerator(name = "SEQ_LEG_OrdFinePayment_Id", sequenceName = "SEQ_LEG_OrdFinePayment_Id", allocationSize = 1)
    private Long saFinePaymentId;

    /**
     * @DbComment payerId 'Person id - The Payer of  the Fine.'
     */
    @Column(name = "payerId", nullable = false)
    private Long payerId;

    /**
     * @DbComment paymentDate 'The date and time the fine was paid.'
     */
    @Column(name = "paymentDate", nullable = false)
    private Date paymentDate;

    /**
     * @DbComment amountPaid 'The amount of money the payer paid.'
     */
    @Column(name = "amountPaid", nullable = false)
    private BigDecimal amountPaid;

    /**
     * @DbComment amountOwing 'The balance amount still owing after amount paid.'
     */
    @Column(name = "amountOwing", nullable = false)
    private BigDecimal amountOwing;

    /**
     * @DbComment proposedAmountOwing 'The balance possible amount owing after a proposed amount paid is entered in the calculator.'
     */
    @Column(name = "proposedAmountOwing", nullable = false)
    private BigDecimal proposedAmountOwing;

    /**
     * @DbComment finePaidToDate 'The total amount of fine the Inmate has paid to date for a particular sentence.'
     */
    @Column(name = "finePaidToDate", nullable = false)
    private BigDecimal finePaidToDate;

    /**
     * @DbComment staffId 'identifier of the staff who recorded the payment'
     */
    @Column(name = "staffId", nullable = false)
    private Long staffId;

    /**
     * @DbComment receiptNumber 'The receipt number for the payment.'
     */
    @Column(name = "receiptNumber", nullable = false, length = 64)
    private String receiptNumber;

    /**
     * @DbComment daysSatisfies 'The number of jail time days the paid amount satisfies.'
     */
    @Column(name = "daysSatisfies", nullable = false)
    private Long daysSatisfies;

    /**
     * @DbComment perDiemRate 'The amount of money that equals 1 day of jail time.'
     */
    @Column(name = "perDiemRate", nullable = false)
    private BigDecimal perDiemRate;

    /**
     * @DbComment daysServed 'The number of days that the inmate has served since the start of the sentence.'
     */
    @Column(name = "daysServed", nullable = false)
    private Long daysServed;

    /**
     * @DbComment daysToLeftToServe 'The number of days that the inmate has served since the start of the sentence.'
     */
    @Column(name = "daysToLeftToServe", nullable = false)
    private Long daysToLeftToServe;

    /**
     * @DbComment releaseDate 'The number of days remaining that the inmate is left to serve after fine payment to date.'
     */
    @Column(name = "releaseDate", nullable = false)
    private Date releaseDate;

    /**
     * @DbComment adjustedReleaseDate 'The new release date after the paid payment.'
     */
    @Column(name = "adjustedReleaseDate", nullable = false)
    private Date adjustedReleaseDate;

    /**
     * @DbComment orderId 'Unique identification of a order instance, foreign key to LEG_ORDORDER table'
     */
    @ForeignKey(name = "FK_LEG_OrdFinePayment_SEN")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orderId", nullable = false)
    private SentenceEntity sentence;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(name = "Flag", nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * @DbComment version 'the version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     * @return the saFinePaymentId
     */
    public Long getSaFinePaymentId() {
        return saFinePaymentId;
    }

    /**
     * @param saFinePaymentId the saFinePaymentId to set
     */
    public void setSaFinePaymentId(Long saFinePaymentId) {
        this.saFinePaymentId = saFinePaymentId;
    }

    /**
     * @return the payerId
     */
    public Long getPayerId() {
        return payerId;
    }

    /**
     * @param payerId the payerId to set
     */
    public void setPayerId(Long payerId) {
        this.payerId = payerId;
    }

    /**
     * @return the paymentDate
     */
    public Date getPaymentDate() {
        return paymentDate;
    }

    /**
     * @param paymentDate the paymentDate to set
     */
    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     * @return the amountPaid
     */
    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    /**
     * @param amountPaid the amountPaid to set
     */
    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    /**
     * @return the amountOwing
     */
    public BigDecimal getAmountOwing() {
        return amountOwing;
    }

    /**
     * @param amountOwing the amountOwing to set
     */
    public void setAmountOwing(BigDecimal amountOwing) {
        this.amountOwing = amountOwing;
    }

    /**
     * @return the proposedAmountOwing
     */
    public BigDecimal getProposedAmountOwing() {
        return proposedAmountOwing;
    }

    /**
     * @param proposedAmountOwing the proposedAmountOwing to set
     */
    public void setProposedAmountOwing(BigDecimal proposedAmountOwing) {
        this.proposedAmountOwing = proposedAmountOwing;
    }

    /**
     * @return the finePaidToDate
     */
    public BigDecimal getFinePaidToDate() {
        return finePaidToDate;
    }

    /**
     * @param finePaidToDate the finePaidToDate to set
     */
    public void setFinePaidToDate(BigDecimal finePaidToDate) {
        this.finePaidToDate = finePaidToDate;
    }

    /**
     * @return the staffId
     */
    public Long getStaffId() {
        return staffId;
    }

    /**
     * @param staffId the staffId to set
     */
    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    /**
     * @return the receiptNumber
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * @param receiptNumber the receiptNumber to set
     */
    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    /**
     * @return the daysSatisfies
     */
    public Long getDaysSatisfies() {
        return daysSatisfies;
    }

    /**
     * @param daysSatisfies the daysSatisfies to set
     */
    public void setDaysSatisfies(Long daysSatisfies) {
        this.daysSatisfies = daysSatisfies;
    }

    /**
     * @return the perDiemRate
     */
    public BigDecimal getPerDiemRate() {
        return perDiemRate;
    }

    /**
     * @param perDiemRate the perDiemRate to set
     */
    public void setPerDiemRate(BigDecimal perDiemRate) {
        this.perDiemRate = perDiemRate;
    }

    /**
     * @return the daysServed
     */
    public Long getDaysServed() {
        return daysServed;
    }

    /**
     * @param daysServed the daysServed to set
     */
    public void setDaysServed(Long daysServed) {
        this.daysServed = daysServed;
    }

    /**
     * @return the daysToLeftToServe
     */
    public Long getDaysToLeftToServe() {
        return daysToLeftToServe;
    }

    /**
     * @param daysToLeftToServe the daysToLeftToServe to set
     */
    public void setDaysToLeftToServe(Long daysToLeftToServe) {
        this.daysToLeftToServe = daysToLeftToServe;
    }

    /**
     * @return the releaseDate
     */
    public Date getReleaseDate() {
        return releaseDate;
    }

    /**
     * @param releaseDate the releaseDate to set
     */
    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     * @return the adjustedReleaseDate
     */
    public Date getAdjustedReleaseDate() {
        return adjustedReleaseDate;
    }

    /**
     * @param adjustedReleaseDate the adjustedReleaseDate to set
     */
    public void setAdjustedReleaseDate(Date adjustedReleaseDate) {
        this.adjustedReleaseDate = adjustedReleaseDate;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the sentence
     */
    public SentenceEntity getSentence() {
        return sentence;
    }

    /**
     * @param sentence the sentence to set
     */
    public void setSentence(SentenceEntity sentence) {
        this.sentence = sentence;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((adjustedReleaseDate == null) ? 0 : adjustedReleaseDate.hashCode());
        result = prime * result + ((amountOwing == null) ? 0 : amountOwing.hashCode());
        result = prime * result + ((amountPaid == null) ? 0 : amountPaid.hashCode());
        result = prime * result + ((daysSatisfies == null) ? 0 : daysSatisfies.hashCode());
        result = prime * result + ((daysServed == null) ? 0 : daysServed.hashCode());
        result = prime * result + ((daysToLeftToServe == null) ? 0 : daysToLeftToServe.hashCode());
        result = prime * result + ((finePaidToDate == null) ? 0 : finePaidToDate.hashCode());
        result = prime * result + ((flag == null) ? 0 : flag.hashCode());
        result = prime * result + ((payerId == null) ? 0 : payerId.hashCode());
        result = prime * result + ((paymentDate == null) ? 0 : paymentDate.hashCode());
        result = prime * result + ((perDiemRate == null) ? 0 : perDiemRate.hashCode());
        result = prime * result + ((proposedAmountOwing == null) ? 0 : proposedAmountOwing.hashCode());
        result = prime * result + ((receiptNumber == null) ? 0 : receiptNumber.hashCode());
        result = prime * result + ((releaseDate == null) ? 0 : releaseDate.hashCode());
        result = prime * result + ((saFinePaymentId == null) ? 0 : saFinePaymentId.hashCode());
        result = prime * result + ((sentence == null) ? 0 : sentence.hashCode());
        result = prime * result + ((staffId == null) ? 0 : staffId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        FinePaymentEntity other = (FinePaymentEntity) obj;
        if (adjustedReleaseDate == null) {
            if (other.adjustedReleaseDate != null) {
				return false;
			}
        } else if (!adjustedReleaseDate.equals(other.adjustedReleaseDate)) {
			return false;
		}
        if (amountOwing == null) {
            if (other.amountOwing != null) {
				return false;
			}
        } else if (!amountOwing.equals(other.amountOwing)) {
			return false;
		}
        if (amountPaid == null) {
            if (other.amountPaid != null) {
				return false;
			}
        } else if (!amountPaid.equals(other.amountPaid)) {
			return false;
		}
        if (daysSatisfies == null) {
            if (other.daysSatisfies != null) {
				return false;
			}
        } else if (!daysSatisfies.equals(other.daysSatisfies)) {
			return false;
		}
        if (daysServed == null) {
            if (other.daysServed != null) {
				return false;
			}
        } else if (!daysServed.equals(other.daysServed)) {
			return false;
		}
        if (daysToLeftToServe == null) {
            if (other.daysToLeftToServe != null) {
				return false;
			}
        } else if (!daysToLeftToServe.equals(other.daysToLeftToServe)) {
			return false;
		}
        if (finePaidToDate == null) {
            if (other.finePaidToDate != null) {
				return false;
			}
        } else if (!finePaidToDate.equals(other.finePaidToDate)) {
			return false;
		}
        if (flag == null) {
            if (other.flag != null) {
				return false;
			}
        } else if (!flag.equals(other.flag)) {
			return false;
		}
        if (payerId == null) {
            if (other.payerId != null) {
				return false;
			}
        } else if (!payerId.equals(other.payerId)) {
			return false;
		}
        if (paymentDate == null) {
            if (other.paymentDate != null) {
				return false;
			}
        } else if (!paymentDate.equals(other.paymentDate)) {
			return false;
		}
        if (perDiemRate == null) {
            if (other.perDiemRate != null) {
				return false;
			}
        } else if (!perDiemRate.equals(other.perDiemRate)) {
			return false;
		}
        if (proposedAmountOwing == null) {
            if (other.proposedAmountOwing != null) {
				return false;
			}
        } else if (!proposedAmountOwing.equals(other.proposedAmountOwing)) {
			return false;
		}
        if (receiptNumber == null) {
            if (other.receiptNumber != null) {
				return false;
			}
        } else if (!receiptNumber.equals(other.receiptNumber)) {
			return false;
		}
        if (releaseDate == null) {
            if (other.releaseDate != null) {
				return false;
			}
        } else if (!releaseDate.equals(other.releaseDate)) {
			return false;
		}
        if (saFinePaymentId == null) {
            if (other.saFinePaymentId != null) {
				return false;
			}
        } else if (!saFinePaymentId.equals(other.saFinePaymentId)) {
			return false;
		}
        if (sentence == null) {
            if (other.sentence != null) {
				return false;
			}
        } else if (!sentence.equals(other.sentence)) {
			return false;
		}
        if (staffId == null) {
            if (other.staffId != null) {
				return false;
			}
        } else if (!staffId.equals(other.staffId)) {
			return false;
		}
        return true;
    }

}
