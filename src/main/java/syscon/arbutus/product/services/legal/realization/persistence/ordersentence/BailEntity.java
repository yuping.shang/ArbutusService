package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.legal.realization.persistence.caseactivity.CourtActivityEntity;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * BailEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdOrder 'The Order table for Order Sentence module of Legal service'
 * @since December 27, 2012
 */
@Audited
@Entity
@DiscriminatorValue("Bail")
public class BailEntity extends OrderEntity implements Serializable {

    private static final long serialVersionUID = -2736063777054775211L;

    /**
     * The bail amounts for different bail types.
     */
    @ForeignKey(name = "Fk_LEG_OrdBail1")
    @OneToMany(mappedBy = "bail", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<BailAmountEntity> bailAmounts;

    /**
     * @DbComment bailRelationship 'The relationship of the bail amount specified. e.g. cash- surety DISAGGREGATE (Bail set amount will be either the cash amount or Surety  amount), Cash Surety AGGREGATE(Bail set amount will be cash amount + Surety  amount)'
     */
    @Column(name = "bailRelationship", nullable = true, length = 64) // business requirement is nullable = false
    @MetaCode(set = MetaSet.BAIL_RELATIONSHIP)
    private String bailRelationship;

    /**
     * bailPostedAmount 'The amount of bail posted.'
     */
    @ForeignKey(name = "Fk_LEG_OrdBail4")
    @OneToMany(mappedBy = "bail", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<BailPostedAmountEntity> bailPostedAmounts;

    /**
     * @DbComment bailPaymentReceiptNo 'Bill Payment Receipt Number for cash bail payment.'
     */
    @Column(name = "bailPaymentReceiptNo", nullable = true, length = 64)
    private String bailPaymentReceiptNo;

    /**
     * @DbComment bailBondPostedDate 'Bail Bond posted date and time'
     */

    @Column(name = "bailBondPostedDate", nullable = true)
    private Date bailBondPostedDate;

    /**
     * @DbComment bailRequirement 'A description of the bail requirement set at a court hearing.'
     */
    @Column(name = "bailRequirement", nullable = true, length = 128)
    private String bailRequirement;

    /**
     * LEG_OrdBailPIDAssc.bailPID 'Foreign Key from LEG_OrdBailPIDAssc table'
     */
    @ForeignKey(name = "Fk_LEG_OrdBail2")
    @OneToMany(mappedBy = "bail", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<BailPersonIdAssocEntity> bailerPersonIdentities;

    /**
     * @DbComment bailAmntId 'The amount of bond posted.'
     */
    @ForeignKey(name = "Fk_LEG_OrdBail5")
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bailAmntId")
    private BondPostedAmountEntity bondPostedAmount;

    /**
     * @DbComment bondPaymentDescription 'A description of what an offender pays for a bond.'
     */
    @Column(name = "bondPaymentDescription", nullable = true, length = 512)
    private String bondPaymentDescription;

    /**
     * LEG_OrdBondOrgAssc.bondOrg 'Foreign Key from LEG_OrdBondOrgAssc table'
     */
    @ForeignKey(name = "Fk_LEG_OrdBail3")
    @OneToMany(mappedBy = "bail", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<BondOrgAssocEntity> bondOrganizations;

    /**
     * @DbComment isBailAllowed 'Is bail allowed'
     */
    @Column(name = "isBailAllowed", nullable = true) // business requirement is nullable = false
    private Boolean isBailAllowed;

    /**
     * Constructor
     */
    public BailEntity() {
        super();
    }

    public BailEntity(Long orderId, String orderClassification, String orderType, String orderSubType, String orderCategory, String orderNumber,
            DispositionEntity orderDisposition, CommentEntity comments, Date orderIssuanceDate, Date orderReceivedDate, Date orderStartDate, Date orderExpirationDate,
            Set<CourtActivityEntity> caseActivitiesInitiatedOrder, Set<CourtActivityEntity> orderInitiatedCaseActivities, Boolean isHoldingOrder,
            Boolean isSchedulingNeeded, Boolean hasCharges, NotificationEntity issuingAgency, StampEntity stamp, Set<BailAmountEntity> bailAmounts,
            String bailRelationship, Set<BailPostedAmountEntity> bailPostedAmounts, String bailPaymentReceiptNo, String bailRequirement,
            Set<BailPersonIdAssocEntity> bailerPersonIdentities, BondPostedAmountEntity bondPostedAmount, String bondPaymentDescription,
            Set<BondOrgAssocEntity> bondOrganizations, Boolean isBailAllowed) {
        super(orderId, orderClassification, orderType, orderSubType, orderCategory, orderNumber, orderDisposition, comments, orderIssuanceDate, orderReceivedDate,
                orderStartDate, orderExpirationDate, caseActivitiesInitiatedOrder, orderInitiatedCaseActivities, isHoldingOrder, isSchedulingNeeded, hasCharges,
                issuingAgency, stamp);
        this.bailAmounts = bailAmounts;
        this.bailRelationship = bailRelationship;
        this.bailPostedAmounts = bailPostedAmounts;
        this.bailPaymentReceiptNo = bailPaymentReceiptNo;
        this.bailRequirement = bailRequirement;
        this.bailerPersonIdentities = bailerPersonIdentities;
        this.bondPostedAmount = bondPostedAmount;
        this.bondPaymentDescription = bondPaymentDescription;
        this.bondOrganizations = bondOrganizations;
        this.isBailAllowed = isBailAllowed;
    }

    /**
     * @return the bailAmounts
     */
    public Set<BailAmountEntity> getBailAmounts() {
        if (bailAmounts == null) {
			bailAmounts = new HashSet<BailAmountEntity>();
		}
        return bailAmounts;
    }

    /**
     * @param bailAmounts the bailAmounts to set
     */
    public void setBailAmounts(Set<BailAmountEntity> bailAmounts) {
        if (bailAmounts != null) {
            for (BailAmountEntity amount : bailAmounts) {
                amount.setBail(this);
            }
            this.bailAmounts = bailAmounts;
        } else {
			this.bailAmounts = new HashSet<BailAmountEntity>();
		}
    }

    /**
     * @return the bailRelationship
     */
    public String getBailRelationship() {
        return bailRelationship;
    }

    /**
     * @param bailRelationship the bailRelationship to set
     */
    public void setBailRelationship(String bailRelationship) {
        this.bailRelationship = bailRelationship;
    }

    /**
     * @return the bailPostedAmounts
     */
    public Set<BailPostedAmountEntity> getBailPostedAmounts() {
        if (bailPostedAmounts == null) {
			bailPostedAmounts = new HashSet<BailPostedAmountEntity>();
		}
        return bailPostedAmounts;
    }

    /**
     * @param bailPostedAmounts the bailPostedAmounts to set
     */
    public void setBailPostedAmounts(Set<BailPostedAmountEntity> bailPostedAmounts) {
        if (bailPostedAmounts != null) {
            for (BailPostedAmountEntity amount : bailPostedAmounts) {
                amount.setBail(this);
            }
            this.bailPostedAmounts = bailPostedAmounts;
        } else {
			this.bailPostedAmounts = new HashSet<BailPostedAmountEntity>();
		}
    }

    /**
     * @return the bailPaymentReceiptNo
     */
    public String getBailPaymentReceiptNo() {
        return bailPaymentReceiptNo;
    }

    /**
     * @param bailPaymentReceiptNo the bailPaymentReceiptNo to set
     */
    public void setBailPaymentReceiptNo(String bailPaymentReceiptNo) {
        this.bailPaymentReceiptNo = bailPaymentReceiptNo;
    }

    /**
     * @return the bailRequirement
     */
    public String getBailRequirement() {
        return bailRequirement;
    }

    /**
     * @param bailRequirement the bailRequirement to set
     */
    public void setBailRequirement(String bailRequirement) {
        this.bailRequirement = bailRequirement;
    }

    /**
     * @return the bailerPersonIdentitys
     */
    public Set<BailPersonIdAssocEntity> getBailerPersonIdentities() {
        if (bailerPersonIdentities == null) {
			bailerPersonIdentities = new HashSet<BailPersonIdAssocEntity>();
		}
        return bailerPersonIdentities;
    }

    /**
     * @param bailerPersonIdentities the bailerPersonIdentitys to set
     */
    public void setBailerPersonIdentities(Set<BailPersonIdAssocEntity> bailerPersonIdentities) {
        if (bailerPersonIdentities != null) {
            for (BailPersonIdAssocEntity assoc : bailerPersonIdentities) {
                assoc.setBail(this);
            }
            this.bailerPersonIdentities = bailerPersonIdentities;
        } else {
			this.bailerPersonIdentities = new HashSet<BailPersonIdAssocEntity>();
		}
    }

    /**
     * @return the bondPostedAmount
     */
    public BondPostedAmountEntity getBondPostedAmount() {
        return bondPostedAmount;
    }

    /**
     * @param bondPostedAmount the bondPostedAmount to set
     */
    public void setBondPostedAmount(BondPostedAmountEntity bondPostedAmount) {
        this.bondPostedAmount = bondPostedAmount;
    }

    /**
     * @return the bondPaymentDescription
     */
    public String getBondPaymentDescription() {
        return bondPaymentDescription;
    }

    /**
     * @param bondPaymentDescription the bondPaymentDescription to set
     */
    public void setBondPaymentDescription(String bondPaymentDescription) {
        this.bondPaymentDescription = bondPaymentDescription;
    }

    /**
     * @return the bondOrganizations
     */
    public Set<BondOrgAssocEntity> getBondOrganizations() {
        if (bondOrganizations == null) {
			bondOrganizations = new HashSet<BondOrgAssocEntity>();
		}
        return bondOrganizations;
    }

    /**
     * @param bondOrganizations the bondOrganizations to set
     */
    public void setBondOrganizations(Set<BondOrgAssocEntity> bondOrganizations) {
        if (bondOrganizations != null) {
            for (BondOrgAssocEntity assoc : bondOrganizations) {
                assoc.setBail(this);
            }
            this.bondOrganizations = bondOrganizations;
        } else {
			this.bondOrganizations = new HashSet<BondOrgAssocEntity>();
		}
    }

    /**
     * @return the isBailAllowed
     */
    public Boolean getIsBailAllowed() {
        return isBailAllowed;
    }

    /**
     * @param isBailAllowed the isBailAllowed to set
     */
    public void setIsBailAllowed(Boolean isBailAllowed) {
        this.isBailAllowed = isBailAllowed;
    }

    /**
     * @return the bailBondPostedDate
     */
    public Date getBailBondPostedDate() {
        return bailBondPostedDate;
    }

    /**
     * @param bailBondPostedDate the bailBondPostedDate to set
     */
    public void setBailBondPostedDate(Date bailBondPostedDate) {
        this.bailBondPostedDate = bailBondPostedDate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((bailAmounts == null) ? 0 : bailAmounts.hashCode());
        result = prime * result + ((bailPaymentReceiptNo == null) ? 0 : bailPaymentReceiptNo.hashCode());
        result = prime * result + ((bailPostedAmounts == null) ? 0 : bailPostedAmounts.hashCode());
        result = prime * result + ((bailRelationship == null) ? 0 : bailRelationship.hashCode());
        result = prime * result + ((bailRequirement == null) ? 0 : bailRequirement.hashCode());
        result = prime * result + ((bailerPersonIdentities == null) ? 0 : bailerPersonIdentities.hashCode());
        result = prime * result + ((bondOrganizations == null) ? 0 : bondOrganizations.hashCode());
        result = prime * result + ((bondPostedAmount == null) ? 0 : bondPostedAmount.hashCode());
        result = prime * result + ((isBailAllowed == null) ? 0 : isBailAllowed.hashCode());
        result = prime * result + ((bailBondPostedDate == null) ? 0 : bailBondPostedDate.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (!super.equals(obj)) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        BailEntity other = (BailEntity) obj;
        if (bailAmounts == null) {
            if (other.bailAmounts != null) {
				return false;
			}
        } else if (!bailAmounts.equals(other.bailAmounts)) {
			return false;
		}
        if (bailPaymentReceiptNo == null) {
            if (other.bailPaymentReceiptNo != null) {
				return false;
			}
        } else if (!bailPaymentReceiptNo.equals(other.bailPaymentReceiptNo)) {
			return false;
		}
        if (bailPostedAmounts == null) {
            if (other.bailPostedAmounts != null) {
				return false;
			}
        } else if (!bailPostedAmounts.equals(other.bailPostedAmounts)) {
			return false;
		}
        if (bailRelationship == null) {
            if (other.bailRelationship != null) {
				return false;
			}
        } else if (!bailRelationship.equals(other.bailRelationship)) {
			return false;
		}
        if (bailRequirement == null) {
            if (other.bailRequirement != null) {
				return false;
			}
        } else if (!bailRequirement.equals(other.bailRequirement)) {
			return false;
		}
        if (bailerPersonIdentities == null) {
            if (other.bailerPersonIdentities != null) {
				return false;
			}
        } else if (!bailerPersonIdentities.equals(other.bailerPersonIdentities)) {
			return false;
		}
        if (bondOrganizations == null) {
            if (other.bondOrganizations != null) {
				return false;
			}
        } else if (!bondOrganizations.equals(other.bondOrganizations)) {
			return false;
		}
        if (bondPostedAmount == null) {
            if (other.bondPostedAmount != null) {
				return false;
			}
        } else if (!bondPostedAmount.equals(other.bondPostedAmount)) {
			return false;
		}
        if (isBailAllowed == null) {
            if (other.isBailAllowed != null) {
				return false;
			}
        } else if (!isBailAllowed.equals(other.isBailAllowed)) {
			return false;
		}
        if (bailBondPostedDate == null) {
            if (other.bailBondPostedDate != null) {
				return false;
			}
        } else if (!bailBondPostedDate.equals(other.bailBondPostedDate)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "BailEntity [bailAmounts=" + bailAmounts + ", bailRelationship=" + bailRelationship + ", bailPostedAmounts=" + bailPostedAmounts
                + ", bailPaymentReceiptNo=" + bailPaymentReceiptNo + ", bailRequirement=" + bailRequirement + ", bailerPersonIdentities=" + bailerPersonIdentities
                + ", bondPostedAmount=" + bondPostedAmount + ", bondPaymentDescription=" + bondPaymentDescription + ", bondOrganizations=" + bondOrganizations
                + ", isBailAllowed=" + isBailAllowed + ", bailBondPostedDate=" + bailBondPostedDate + "]";
    }

}
