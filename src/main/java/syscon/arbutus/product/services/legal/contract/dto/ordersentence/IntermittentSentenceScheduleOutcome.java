package syscon.arbutus.product.services.legal.contract.dto.ordersentence;

/**
 * <p>Enum class for Intermittent Sentence Schedule Outcome.
 *
 * @author ranjith.thodupunuri
 */
public enum IntermittentSentenceScheduleOutcome {
    PEND, // Pending
    PSNT, // Present
    RPTD, // Reported
    ABWP, // Absent with Permission
    ABWOP; // Absent without Permission

    /**
     * Returns Intermittent Sentence Schedule name.
     *
     * @return
     */
    public String code() {
        return name();
    }
}
