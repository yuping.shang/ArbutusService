package syscon.arbutus.product.services.legal.realization.persistence.condition;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.Associable;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the module association entity of Condition Instance.
 *
 * @DbComment LEG_CNModuleAssc 'The association table of Condition Instance'
 * @DbComment LEG_CNModuleAssc.AssociationId 'Unique id of the association record'
 * @DbComment LEG_CNModuleAssc.FromIdentifier 'The condition instance Id'
 * @DbComment LEG_CNModuleAssc.ToClass 'The association module that is referenced'
 * @DbComment LEG_CNModuleAssc.ToIdentifier 'The unique id of the Association To Object'
 * @DbComment LEG_CNModuleAssc.CreateUserId 'User ID who created the object'
 * @DbComment LEG_CNModuleAssc.CreateDateTime 'Date and time when the object was created'
 * @DbComment LEG_CNModuleAssc.InvocationContext 'Invocation context when the create/update action called'
 * @DbComment LEG_CNModuleAssc.ModifyUserId 'User ID who last updated the object'
 * @DbComment LEG_CNModuleAssc.ModifyDateTime 'Date and time when the object was last updated'
 */
@Audited
@Entity
@Table(name = "LEG_CNModuleAssc")
public class ConditionModuleAssociationEntity implements Serializable, Associable {

    private static final long serialVersionUID = 7444898258158431429L;

    @Id
    @Column(name = "AssociationId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CNModuleAssc_Id")
    @SequenceGenerator(name = "SEQ_LEG_CNModuleAssc_Id", sequenceName = "SEQ_LEG_CNModuleAssc_Id", allocationSize = 1)
    private Long associationId;

    @Column(name = "ToClass", nullable = false, length = 64)
    private String toClass;

    @Column(name = "ToIdentifier", nullable = false)
    private Long toIdentifier;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FromIdentifier", referencedColumnName = "conditionId", nullable = false)
    private ConditionEntity condition;

    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * Default constructor
     */
    public ConditionModuleAssociationEntity() {
    }

    /**
     * Constructor
     *
     * @param associationId  Long
     * @param fromIdentifier Long
     * @param toClass        String
     * @param toIdentifier   Long
     * @param privileges     Set<AssociationPrivilegeEntity>
     */
    public ConditionModuleAssociationEntity(Long associationId, Long fromIdentifier, String toClass, Long toIdentifier) {
        super();
        this.associationId = associationId;
        this.toClass = toClass;
        this.toIdentifier = toIdentifier;
    }

    /**
     * Gets the value of the associationId property
     *
     * @return Long
     */
    public Long getAssociationId() {
        return associationId;
    }

    /**
     * Sets the value of the associationId property
     *
     * @param associationId Long
     */
    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    /**
     * Gets the value of the toClass property
     *
     * @return String
     */
    public String getToClass() {
        return toClass;
    }

    /**
     * Sets the value of the toClass property
     *
     * @param toClass String
     */
    public void setToClass(String toClass) {
        this.toClass = toClass;
    }

    /**
     * Gets the value of the toIdentifier property
     *
     * @return Long
     */
    public Long getToIdentifier() {
        return toIdentifier;
    }

    /**
     * Sets the value of the toIdentifier property
     *
     * @param toIdentifier Long
     */
    public void setToIdentifier(Long toIdentifier) {
        this.toIdentifier = toIdentifier;
    }

    /**
     * Gets the value of the stamp property
     *
     * @return StampEntity
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * Sets the value of the stamp property
     *
     * @param stamp StampEntity
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * Gets the value of the condition property
     *
     * @return ConditionEntity
     */
    public ConditionEntity getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property
     *
     * @param condition
     */
    public void setCondition(ConditionEntity condition) {
        this.condition = condition;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((associationId == null) ? 0 : associationId.hashCode());
        result = prime * result + ((toClass == null) ? 0 : toClass.hashCode());
        result = prime * result + ((toIdentifier == null) ? 0 : toIdentifier.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ConditionModuleAssociationEntity other = (ConditionModuleAssociationEntity) obj;
        if (associationId == null) {
            if (other.associationId != null) {
				return false;
			}
        } else if (!associationId.equals(other.associationId)) {
			return false;
		}
        if (toClass == null) {
            if (other.toClass != null) {
				return false;
			}
        } else if (!toClass.equals(other.toClass)) {
			return false;
		}
        if (toIdentifier == null) {
            if (other.toIdentifier != null) {
				return false;
			}
        } else if (!toIdentifier.equals(other.toIdentifier)) {
			return false;
		}
        return true;
    }

    @Override
    public String toString() {
        return "ConditionModuleAssociationEntity [associationId=" + associationId + ", toClass=" + toClass + ", toIdentifier=" + toIdentifier + ", condition=" + condition
                + ", stamp=" + stamp + "]";
    }

}
