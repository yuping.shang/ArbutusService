package syscon.arbutus.product.services.legal.realization.persistence.sentenceadjustment;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * AdjustmentHistEntity for Sentence Adjustment
 *
 * @DbComment LEG_SentAdjmntHst 'Legal Adjustment History Table'
 * User: yshang
 * Date: 03/10/13
 * Time: 9:32 AM
 */
//@Audited
@Entity
@Table(name = "LEG_SentAdjmntHst")
public class AdjustmentHistEntity implements Serializable {

    private static final long serialVersionUID = 6599497982695205669L;

    /**
     * @DbComment adjustmentHstId 'Adjustment History Id, system generated.'
     */
    @Id
    @Column(name = "adjustmentHstId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_SENTADJMNTHST_ID")
    @SequenceGenerator(name = "SEQ_LEG_SENTADJMNTHST_ID", sequenceName = "SEQ_LEG_SENTADJMNTHST_ID", allocationSize = 1)
    private Long historyId;

    /**
     * @DbComment adjustmentId 'Adjustment Id, system generated.'
     */
    @Column(name = "adjustmentId", nullable = false)
    private Long adjustmentId;

    /**
     * @DbComment supervisionId 'Supervision Id'
     */
    @Column(name = "supervisionId", nullable = false)
    private Long supervisionId;

    /**
     * @DbComment classification 'Adjustment Classification: SYSG, MANUAL'
     */
    @Column(name = "classification", length = 64, nullable = false)
    @MetaCode(set = MetaSet.ADJUSTMENT_CLASSIFICATION)
    private String adjustmentClassification;

    /**
     * @DbComment adjustmentType 'Adjustment Type: GT, JT, LGT, WORKCDT, LWRKCRD, PRGCDT, ESCP, OICDBT, WKNCDT, STATCDT'
     */
    @Column(name = "adjustmentType", length = 64, nullable = false)
    @MetaCode(set = MetaSet.ADJUSTMENT_TYPE)
    private String adjustmentType;

    /**
     * @DbComment adjustmentFunc 'Adjustment Function: CRE, DEB'
     */
    @Column(name = "adjustmentFunc", length = 64, nullable = false)
    @MetaCode(set = MetaSet.ADJUSTMENT_FUNCTION)
    private String adjustmentFunction;

    /**
     * @DbComment status 'Adjustment Status: INCL, EXCL, PEND'
     */
    @Column(name = "status", length = 64, nullable = false)
    @MetaCode(set = MetaSet.ADJUSTMENT_STATUS)
    private String adjustmentStatus;

    /**
     * @DbComment applicationType 'Application Type: ASENT--Single Sentence; AGGSENT--Aggregated Sentence; BOOKLEV: Booking Level.'
     */
    @Column(name = "applicationType", length = 64, nullable = false)
    @MetaCode(set = MetaSet.APPLICATION_TYPE)
    private String applicationType;

    /**
     * @DbComment startDate 'Start Date'
     */
    @Column(name = "startDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    /**
     * @DbComment endDate 'End Date'
     */
    @Column(name = "endDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    /**
     * @DbComment postedDate 'Posted Date'
     */
    @Column(name = "postedDate", nullable = true)
    private Date postedDate;

    /**
     * @DbComment adjustment 'The Adjustment, the days calculated'
     */
    @Column(name = "adjustment", nullable = true)
    private Long adjustment;

    @ForeignKey(name = "Fk_LEG_SentAdjmnt1H")
    @OneToMany(mappedBy = "adjustment", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<SentenceAssocHistEntity> sentences;

    /**
     * @DbComment comments 'Comments inputed by staff'
     */
    @Column(name = "comments", length = 512, nullable = true)
    private String comment;

    /**
     * @DbComment byStaffId 'Staff Id'
     */
    @Column(name = "byStaffId", nullable = true)
    private Long byStaffId;

    /**
     * @DbComment sentAdjustHstId 'Id of table LEG_SentAdjustHst'
     */
    @ForeignKey(name = "Fk_LEG_SentAdjmnt2H")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sentAdjustHstId", nullable = false)
    private SentenceAdjustmentHistEntity sentenceAdjust;

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    public AdjustmentHistEntity() {
    }

    /**
     * @return the historyId
     */
    public Long getHistoryId() {
        return historyId;
    }

    /**
     * @param historyId the historyId to set
     */
    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public Long getAdjustmentId() {
        return adjustmentId;
    }

    public void setAdjustmentId(Long adjustmentId) {
        this.adjustmentId = adjustmentId;
    }

    /**
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    public String getAdjustmentClassification() {
        return adjustmentClassification;
    }

    public void setAdjustmentClassification(String adjustmentClassification) {
        this.adjustmentClassification = adjustmentClassification;
    }

    public String getAdjustmentType() {
        return adjustmentType;
    }

    public void setAdjustmentType(String adjustmentType) {
        this.adjustmentType = adjustmentType;
    }

    public String getAdjustmentFunction() {
        return adjustmentFunction;
    }

    public void setAdjustmentFunction(String adjustmentFunction) {
        this.adjustmentFunction = adjustmentFunction;
    }

    public String getAdjustmentStatus() {
        return adjustmentStatus;
    }

    public void setAdjustmentStatus(String adjustmentStatus) {
        this.adjustmentStatus = adjustmentStatus;
    }

    public String getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    public Long getAdjustment() {
        return adjustment;
    }

    public void setAdjustment(Long adjustment) {
        this.adjustment = adjustment;
    }

    public Set<SentenceAssocHistEntity> getSentences() {
        if (sentences == null) {
			sentences = new HashSet<>();
		}
        return sentences;
    }

    public void setSentences(Set<SentenceAssocHistEntity> sentences) {
        if (sentences != null) {
            for (SentenceAssocHistEntity sentenceAssoc : sentences) {
                sentenceAssoc.setAdjustment(this);
            }
            this.sentences = sentences;
        } else {
			this.sentences = new HashSet<>();
		}
    }

    public void addSentence(SentenceAssocHistEntity sentence) {
        sentence.setAdjustment(this);
        getSentences().add(sentence);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getByStaffId() {
        return byStaffId;
    }

    public void setByStaffId(Long byStaffId) {
        this.byStaffId = byStaffId;
    }

    public SentenceAdjustmentHistEntity getSentenceAdjust() {
        return sentenceAdjust;
    }

    public void setSentenceAdjust(SentenceAdjustmentHistEntity sentenceAdjust) {
        this.sentenceAdjust = sentenceAdjust;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((adjustment == null) ? 0 : adjustment.hashCode());
        result = prime * result + ((adjustmentClassification == null) ? 0 : adjustmentClassification.hashCode());
        result = prime * result + ((adjustmentFunction == null) ? 0 : adjustmentFunction.hashCode());
        result = prime * result + ((adjustmentId == null) ? 0 : adjustmentId.hashCode());
        result = prime * result + ((adjustmentStatus == null) ? 0 : adjustmentStatus.hashCode());
        result = prime * result + ((adjustmentType == null) ? 0 : adjustmentType.hashCode());
        result = prime * result + ((applicationType == null) ? 0 : applicationType.hashCode());
        result = prime * result + ((byStaffId == null) ? 0 : byStaffId.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((historyId == null) ? 0 : historyId.hashCode());
        result = prime * result + ((postedDate == null) ? 0 : postedDate.hashCode());
        result = prime * result + ((sentences == null) ? 0 : sentences.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        AdjustmentHistEntity other = (AdjustmentHistEntity) obj;
        if (adjustment == null) {
            if (other.adjustment != null) {
				return false;
			}
        } else if (!adjustment.equals(other.adjustment)) {
			return false;
		}
        if (adjustmentClassification == null) {
            if (other.adjustmentClassification != null) {
				return false;
			}
        } else if (!adjustmentClassification.equals(other.adjustmentClassification)) {
			return false;
		}
        if (adjustmentFunction == null) {
            if (other.adjustmentFunction != null) {
				return false;
			}
        } else if (!adjustmentFunction.equals(other.adjustmentFunction)) {
			return false;
		}
        if (adjustmentId == null) {
            if (other.adjustmentId != null) {
				return false;
			}
        } else if (!adjustmentId.equals(other.adjustmentId)) {
			return false;
		}
        if (adjustmentStatus == null) {
            if (other.adjustmentStatus != null) {
				return false;
			}
        } else if (!adjustmentStatus.equals(other.adjustmentStatus)) {
			return false;
		}
        if (adjustmentType == null) {
            if (other.adjustmentType != null) {
				return false;
			}
        } else if (!adjustmentType.equals(other.adjustmentType)) {
			return false;
		}
        if (applicationType == null) {
            if (other.applicationType != null) {
				return false;
			}
        } else if (!applicationType.equals(other.applicationType)) {
			return false;
		}
        if (byStaffId == null) {
            if (other.byStaffId != null) {
				return false;
			}
        } else if (!byStaffId.equals(other.byStaffId)) {
			return false;
		}
        if (endDate == null) {
            if (other.endDate != null) {
				return false;
			}
        } else if (!endDate.equals(other.endDate)) {
			return false;
		}
        if (historyId == null) {
            if (other.historyId != null) {
				return false;
			}
        } else if (!historyId.equals(other.historyId)) {
			return false;
		}
        if (postedDate == null) {
            if (other.postedDate != null) {
				return false;
			}
        } else if (!postedDate.equals(other.postedDate)) {
			return false;
		}
        if (sentences == null) {
            if (other.sentences != null) {
				return false;
			}
        } else if (!sentences.equals(other.sentences)) {
			return false;
		}
        if (startDate == null) {
            if (other.startDate != null) {
				return false;
			}
        } else if (!startDate.equals(other.startDate)) {
			return false;
		}
        if (supervisionId == null) {
            if (other.supervisionId != null) {
				return false;
			}
        } else if (!supervisionId.equals(other.supervisionId)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "AdjustmentHistEntity [historyId=" + historyId + ", adjustmentId=" + adjustmentId + ", supervisionId=" + supervisionId + ", adjustmentClassification="
                + adjustmentClassification + ", adjustmentType=" + adjustmentType + ", adjustmentFunction=" + adjustmentFunction + ", adjustmentStatus="
                + adjustmentStatus + ", applicationType=" + applicationType + ", startDate=" + startDate + ", endDate=" + endDate + ", postedDate=" + postedDate
                + ", adjustment=" + adjustment + ", sentences=" + sentences + ", comment=" + comment + ", byStaffId=" + byStaffId + "]";
    }

}
