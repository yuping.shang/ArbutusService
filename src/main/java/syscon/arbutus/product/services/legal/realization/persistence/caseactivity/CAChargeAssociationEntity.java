package syscon.arbutus.product.services.legal.realization.persistence.caseactivity;

import javax.persistence.*;

import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * ChargeAssociationEntity for Case Activity Service
 *
 * @author lhan
 * @version 1.0
 */
@Audited
@Entity
@DiscriminatorValue("ChargeAssoc")
public class CAChargeAssociationEntity extends CaseActivityStaticAssociationEntity {

    private static final long serialVersionUID = -359169097507412345L;

    /**
     * @DbComment LEG_CAStaticAssc.FromIdentifier 'Foreign key to LEG_CACaseActivity.CaseActivityId'
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FromIdentifier", nullable = false)
    private CaseActivityEntity caseActivity;

    /**
     * Constructor
     */
    public CAChargeAssociationEntity() {
        super();
    }

    /**
     * Constructor.
     *
     * @param associationId
     * @param fromIdentifier
     * @param toIdentifier
     * @param stamp
     * @param caseActivity
     */
    public CAChargeAssociationEntity(Long associationId, Long fromIdentifier, Long toIdentifier, StampEntity stamp, CaseActivityEntity caseActivity) {
        super(associationId, toIdentifier, stamp);
        this.setCaseActivity(caseActivity);
    }

    /**
     * @return the caseActivity
     */
    public CaseActivityEntity getCaseActivity() {
        return caseActivity;
    }

    /**
     * @param caseActivity the caseActivity to set
     */
    public void setCaseActivity(CaseActivityEntity caseActivity) {
        this.caseActivity = caseActivity;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
	 */
    @Override
    public String toString() {
        return "ChargeAssociationEntity [getAssociationId()=" + getAssociationId() + ", getToIdentifier()=" + getToIdentifier() + ", getStamp()=" + getStamp()
                + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + "]";
    }

}