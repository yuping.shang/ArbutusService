package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.Associable;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * BondChargeAssocHistEntity for Order Sentence Module of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdBondChgAssocHst 'The Charge Association(associated by Bond Posted Amount History) table for Order Sentence module of Legal service'
 * @since January 03, 2013
 */
//@Audited
@Entity
@Table(name = "LEG_OrdBondChgAssocHst")
public class BondChargeAssocHistEntity implements Serializable, Associable {

    private static final long serialVersionUID = 6055138917738557442L;

    /**
     * @DbComment bcaHstId 'Unique identification of Bond Charge Association History instance'
     */
    @Id
    @Column(name = "bcaHstId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDBONDCHGASSOCHST_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDBONDCHGASSOCHST_ID", sequenceName = "SEQ_LEG_ORDBONDCHGASSOCHST_ID", allocationSize = 1)
    private Long historyId;

    /**
     * @DbComment associationId 'Unique identification of Charge Association instance'
     */
    @Column(name = "associationId")
    private Long associationId;

    /**
     * @DbComment toIdentifier 'Charge identification'
     */
    @Column(name = "toIdentifier", nullable = false)
    private Long toIdentifier;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment paHstId 'Unique identification of a bailAmount instance, foreign key to LEG_ORDSTNBAILAMOUNT table'
     */
    @ForeignKey(name = "Fk_LEG_OrdBondChgAssocHst")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "paHstId", nullable = false)
    private BondPostedAmountHistEntity bondAmount;

    /**
     * @return the historyId
     */
    public Long getHistoryId() {
        return historyId;
    }

    /**
     * @param historyId the historyId to set
     */
    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    /**
     * @return the associationId
     */
    public Long getAssociationId() {
        return associationId;
    }

    /**
     * @param associationId the associationId to set
     */
    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    /**
     * @return the toIdentifier
     */
    public Long getToIdentifier() {
        return toIdentifier;
    }

    /**
     * @param toIdentifier the toIdentifier to set
     */
    public void setToIdentifier(Long toIdentifier) {
        this.toIdentifier = toIdentifier;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the bondAmount
     */
    public BondPostedAmountHistEntity getBondAmount() {
        return bondAmount;
    }

    /**
     * @param bondAmount the bondAmount to set
     */
    public void setBondAmount(BondPostedAmountHistEntity bondAmount) {
        this.bondAmount = bondAmount;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((associationId == null) ? 0 : associationId.hashCode());
        result = prime * result + ((historyId == null) ? 0 : historyId.hashCode());
        result = prime * result + ((toIdentifier == null) ? 0 : toIdentifier.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        BondChargeAssocHistEntity other = (BondChargeAssocHistEntity) obj;
        if (associationId == null) {
            if (other.associationId != null) {
				return false;
			}
        } else if (!associationId.equals(other.associationId)) {
			return false;
		}
        if (historyId == null) {
            if (other.historyId != null) {
				return false;
			}
        } else if (!historyId.equals(other.historyId)) {
			return false;
		}
        if (toIdentifier == null) {
            if (other.toIdentifier != null) {
				return false;
			}
        } else if (!toIdentifier.equals(other.toIdentifier)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "BondChargeAssociationHistoryEntity [historyId=" + historyId + ", associationId=" + associationId + ", toIdentifier=" + toIdentifier + "]";
    }

    public String getToClass() {
        // Not implemented
        return null;
    }

    public void setToClass(String toClass) {
        // Not implemented
    }

}
