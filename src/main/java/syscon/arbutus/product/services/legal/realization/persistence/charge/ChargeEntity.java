package syscon.arbutus.product.services.legal.realization.persistence.charge;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the charge entity.
 *
 * @DbComment LEG_CHGCharge 'The charge data table'
 */
@Audited
@Entity
@Table(name = "LEG_CHGCharge")
@SQLDelete(sql = "UPDATE LEG_CHGCharge SET flag = 4 WHERE chargeId = ? and version = ?")
@Where(clause = "flag = 1")
public class ChargeEntity implements Serializable {

    private static final long serialVersionUID = -1913150995503421285L;

    /**
     * @DbComment ChargeId 'Unique id of a charge instance'
     */
    @Id
    @Column(name = "ChargeId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_CHGCharge_Id")
    @SequenceGenerator(name = "SEQ_LEG_CHGCharge_Id", sequenceName = "SEQ_LEG_CHGCharge_Id", allocationSize = 1)
    private Long chargeId;

    /**
     * @DbComment CaseInfoId 'The case information unique id. Refers to the Case Info instance'
     */
    @Column(name = "CaseInfoId", nullable = true)
    private Long caseInfoId;

    /**
     * @DbComment StatuteChargeId 'The statute charge configuration unique id. Refers to the Statute Charge Configuration instance'
     */
    @Column(name = "StatuteChargeId", nullable = true)
    private Long statuteChargeId;

    /**
     * @DbComment ChargeGroupId 'The group charge unique id. use to link the charges which charged with committing the same crime'
     */
    @Column(name = "ChargeGroupId", nullable = true)
    private Long chargeGroupId;

    /**
     * @DbComment ChargeCount 'A number of times a person is charged with committing the same crime.'
     */
    @Column(name = "ChargeCount", nullable = true)
    private Long chargeCount;

    //From charge Disposition type
    /**
     * @DbComment ChargeStatus 'The status of a charge (active, inactive)'
     */
    @MetaCode(set = MetaSet.CHARGE_STATUS)
    @Column(name = "ChargeStatus", nullable = false, length = 64)
    private String chargeStatus;

    //From charge Disposition type
    /**
     * @DbComment DispositionOutcome 'A result or outcome of the disposition.'
     */
    @MetaCode(set = MetaSet.CHARGE_OUTCOME)
    @Column(name = "DispositionOutcome", nullable = true, length = 64)
    private String dispositionOutcome;

    //From charge Disposition type
    /**
     * @DbComment DispositionDate 'Date of the disposition'
     */
    @Column(name = "DispositionDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dispositionDate;

    /**
     * @DbComment IsPrimary 'True if the charge is a serious charge, false otherwise, Default is false.'
     */
    @Column(name = "IsPrimary", nullable = false)
    private Boolean primary;

    /**
     * @DbComment OffenceStartDate 'Date the offence was committed'
     */
    @Column(name = "OffenceStartDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date offenceStartDate;

    /**
     * @DbComment OffenceEndDate 'Field is used when the exact offence date is not known, to reflect an offence committed date range'
     */
    @Column(name = "OffenceEndDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date offenceEndDate;

    /**
     * @DbComment ChargeFilingDate 'A date the charge was filed'
     */
    @Column(name = "ChargeFilingDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date chargeFilingDate;

    /**
     * @DbComment FacilityId 'Static Reference to the facility entity that filed the charge.'
     */
    @Column(name = "FacilityId", nullable = true)
    private Long facilityId;

    /**
     * @DbComment OrganizationId 'Static Reference to the organization entity that filed the charge.'
     */
    @Column(name = "OrganizationId", nullable = true)
    private Long organizationId;

    /**
     * @DbComment PersonIdentityId 'Static Reference to the PersonIdentity entity that filed the charge.'
     */
    @Column(name = "PersonIdentityId", nullable = true)
    private Long personIdentityId;

    /**
     * @DbComment ChargeLegalTextId 'Foreign key from table LEG_CHGLegalText'
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ChargeLegalTextId")
    private ChargeLegalTextEntity legalText;

    /**
     * @DbComment ChargePleaId 'Foreign key from table LEG_CHGChargePlea'
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ChargePleaId")
    private ChargePleaEntity chargePlea;

    /**
     * @DbComment ChargeAppealId 'Foreign key from table LEG_CHGChargeAppeal'
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ChargeAppealId")
    private ChargeAppealEntity chargeAppeal;

    /**
     * @DbComment ChargeJurisdiction 'Indicate if the charge is IJ charge or OJ charge.'
     */
    @MetaCode(set = MetaSet.CHARGE_JURISDICTION)
    @Column(name = "ChargeJurisdiction", nullable = false, length = 64)
    private String chargeJurisdiction;

    /**
     * @DbComment OJOrderId 'Id of an OJ-Order which is associated to'
     */
    @Column(name = "OJOrderId", nullable = true)
    private Long OJOrderId;

    /**
     * @DbComment OJStatuteIdName 'The OJ charge statute id & name'
     */
    @Column(name = "OJStatuteIdName", nullable = true, length = 128)
    private String OJStatuteIdName;

    /**
     * @DbComment OJChargeCode 'The OJ charge code'
     */
    @Column(name = "OJChargeCode", nullable = true, length = 64)
    private String OJChargeCode;

    /**
     * @DbComment OJChargeDescription 'The OJ charge description'
     */
    @Column(name = "OJChargeDescription", nullable = true, length = 4000)
    private String OJChargeDescription;

    /**
     * @DbComment CommentUserId 'User ID who created/updated the comments'
     * @DbComment CommentDate 'The Date/Time when the comment was created'
     * @DbComment CommentText 'The comment text'
     */
    @Embedded
    private CommentEntity comment;

    /**
     * @DbComment SentenceId 'Id of sentence which is associated to'
     */
    @Column(name = "SentenceId", nullable = true)
    private Long sentenceId;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(name = "Flag", nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    /**
     * @DbComment version 'The version number of record updated'
     */
    @NotAudited
    @Version
    private Long version;

    //child entities
    @OneToMany(mappedBy = "charge", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<ChargeIdentifierEntity> chargeIdentifiers;

    @OneToMany(mappedBy = "charge", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<ChargeIndicatorEntity> chargeIndicators;

    @OneToMany(mappedBy = "charge", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<ChargeModuleAssociationEntity> moduleAssociations;

    @NotAudited
    @OneToMany(mappedBy = "charge", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private Set<ChargeHistEntity> histories;

    @NotAudited
    @OneToMany(mappedBy = "charge", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @OrderBy("chargeDspHistId DESC")
    private List<ChargeDispositionHistEntity> dispositionHistories;

    /**
     *
     */
    public ChargeEntity() {
    }

    /**
     * @param chargeId
     * @param caseInfoId
     * @param statuteChargeId
     * @param chargeGroupId
     * @param chargeCount
     * @param chargeStatus
     * @param dispositionOutcome
     * @param dispositionDate
     * @param primary
     * @param offenceStartDate
     * @param offenceEndDate
     * @param chargeFilingDate
     * @param facilityId
     * @param organizationId
     * @param personIdentityId
     * @param legalText
     * @param chargePlea
     * @param chargeAppeal
     * @param comment
     * @param sentenceId
     */
    public ChargeEntity(Long chargeId, Long caseInfoId, Long statuteChargeId, Long chargeGroupId, Long chargeCount, String chargeStatus, String dispositionOutcome,
            Date dispositionDate, Boolean primary, Date offenceStartDate, Date offenceEndDate, Date chargeFilingDate, Long facilityId, Long organizationId,
            Long personIdentityId, ChargeLegalTextEntity legalText, ChargePleaEntity chargePlea, ChargeAppealEntity chargeAppeal, CommentEntity comment,
            Long sentenceId) {
        this.chargeId = chargeId;
        this.caseInfoId = caseInfoId;
        this.statuteChargeId = statuteChargeId;
        this.chargeGroupId = chargeGroupId;
        this.chargeCount = chargeCount;
        this.chargeStatus = chargeStatus;
        this.dispositionOutcome = dispositionOutcome;
        this.dispositionDate = dispositionDate;
        this.primary = primary;
        this.offenceStartDate = offenceStartDate;
        this.offenceEndDate = offenceEndDate;
        this.chargeFilingDate = chargeFilingDate;
        this.facilityId = facilityId;
        this.organizationId = organizationId;
        this.personIdentityId = personIdentityId;
        this.legalText = legalText;
        this.chargePlea = chargePlea;
        this.chargeAppeal = chargeAppeal;
        this.comment = comment;
        this.sentenceId = sentenceId;
    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /**
     * @return the chargeId
     */
    public Long getChargeId() {
        return chargeId;
    }

    /**
     * @param chargeId the chargeId to set
     */
    public void setChargeId(Long chargeId) {
        this.chargeId = chargeId;
    }

    /**
     * @return the caseInfoId
     */
    public Long getCaseInfoId() {
        return caseInfoId;
    }

    /**
     * @param caseInfoId the caseInfoId to set
     */
    public void setCaseInfoId(Long caseInfoId) {
        this.caseInfoId = caseInfoId;
    }

    /**
     * @return the statuteChargeId
     */
    public Long getStatuteChargeId() {
        return statuteChargeId;
    }

    /**
     * @param statuteChargeId the statuteChargeId to set
     */
    public void setStatuteChargeId(Long statuteChargeId) {
        this.statuteChargeId = statuteChargeId;
    }

    /**
     * @return the chargeGroupId
     */
    public Long getChargeGroupId() {
        return chargeGroupId;
    }

    /**
     * @param chargeGroupId the chargeGroupId to set
     */
    public void setChargeGroupId(Long chargeGroupId) {
        this.chargeGroupId = chargeGroupId;
    }

    /**
     * @return the chargeCount
     */
    public Long getChargeCount() {
        return chargeCount;
    }

    /**
     * @param chargeCount the chargeCount to set
     */
    public void setChargeCount(Long chargeCount) {
        this.chargeCount = chargeCount;
    }

    /**
     * @return the chargeStatus
     */
    public String getChargeStatus() {
        return chargeStatus;
    }

    /**
     * @param chargeStatus the chargeStatus to set
     */
    public void setChargeStatus(String chargeStatus) {
        this.chargeStatus = chargeStatus;
    }

    /**
     * @return the dispositionOutcome
     */
    public String getDispositionOutcome() {
        return dispositionOutcome;
    }

    /**
     * @param dispositionOutcome the dispositionOutcome to set
     */
    public void setDispositionOutcome(String dispositionOutcome) {
        this.dispositionOutcome = dispositionOutcome;
    }

    /**
     * @return the dispositionDate
     */
    public Date getDispositionDate() {
        return dispositionDate;
    }

    /**
     * @param dispositionDate the dispositionDate to set
     */
    public void setDispositionDate(Date dispositionDate) {
        this.dispositionDate = dispositionDate;
    }

    /**
     * @return the primary
     */
    public Boolean isPrimary() {
        return primary;
    }

    /**
     * @param primary the primary to set
     */
    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

    /**
     * @return the offenceStartDate
     */
    public Date getOffenceStartDate() {
        return offenceStartDate;
    }

    /**
     * @param offenceStartDate the offenceStartDate to set
     */
    public void setOffenceStartDate(Date offenceStartDate) {
        this.offenceStartDate = offenceStartDate;
    }

    /**
     * @return the offenceEndDate
     */
    public Date getOffenceEndDate() {
        return offenceEndDate;
    }

    /**
     * @param offenceEndDate the offenceEndDate to set
     */
    public void setOffenceEndDate(Date offenceEndDate) {
        this.offenceEndDate = offenceEndDate;
    }

    /**
     * @return the chargeFilingDate
     */
    public Date getChargeFilingDate() {
        return chargeFilingDate;
    }

    /**
     * @param chargeFilingDate the chargeFilingDate to set
     */
    public void setChargeFilingDate(Date chargeFilingDate) {
        this.chargeFilingDate = chargeFilingDate;
    }

    /**
     * @return the facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * @param facilityId the facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * @return the organizationId
     */
    public Long getOrganizationId() {
        return organizationId;
    }

    /**
     * @param organizationId the organizationId to set
     */
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * @return the personIdentityId
     */
    public Long getPersonIdentityId() {
        return personIdentityId;
    }

    /**
     * @param personIdentityId the personIdentityId to set
     */
    public void setPersonIdentityId(Long personIdentityId) {
        this.personIdentityId = personIdentityId;
    }

    /**
     * @return the legalText
     */
    public ChargeLegalTextEntity getLegalText() {
        return legalText;
    }

    /**
     * @param legalText the legalText to set
     */
    public void setLegalText(ChargeLegalTextEntity legalText) {
        this.legalText = legalText;
    }

    /**
     * @return the chargePlea
     */
    public ChargePleaEntity getChargePlea() {
        return chargePlea;
    }

    /**
     * @param chargePlea the chargePlea to set
     */
    public void setChargePlea(ChargePleaEntity chargePlea) {
        this.chargePlea = chargePlea;
    }

    /**
     * @return the chargeAppeal
     */
    public ChargeAppealEntity getChargeAppeal() {
        return chargeAppeal;
    }

    /**
     * @param chargeAppeal the chargeAppeal to set
     */
    public void setChargeAppeal(ChargeAppealEntity chargeAppeal) {
        this.chargeAppeal = chargeAppeal;
    }

    /**
     * @return the comment
     */
    public CommentEntity getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(CommentEntity comment) {
        this.comment = comment;
    }

    /**
     * @return the sentenceId
     */
    public Long getSentenceId() {
        return sentenceId;
    }

    /**
     * @param sentenceId the sentenceId to set
     */
    public void setSentenceId(Long sentenceId) {
        this.sentenceId = sentenceId;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the chargeIdentifiers
     */
    public Set<ChargeIdentifierEntity> getChargeIdentifiers() {
        if (chargeIdentifiers == null) {
            chargeIdentifiers = new HashSet<ChargeIdentifierEntity>();
        }
        return chargeIdentifiers;
    }

    /**
     * @param chargeIdentifiers the chargeIdentifiers to set
     */
    public void setChargeIdentifiers(Set<ChargeIdentifierEntity> chargeIdentifiers) {
        if (chargeIndicators != null) {
            for (ChargeIdentifierEntity subEntity : chargeIdentifiers) {
                subEntity.setCharge(this);
            }
        }
        this.chargeIdentifiers = chargeIdentifiers;
    }

    /**
     * @param chargeIdentifier
     */
    public void addChargeIdentifier(ChargeIdentifierEntity chargeIdentifier) {
        chargeIdentifier.setCharge(this);
        getChargeIdentifiers().add(chargeIdentifier);
    }

    /**
     * @return the chargeIndicators
     */
    public Set<ChargeIndicatorEntity> getChargeIndicators() {
        if (chargeIndicators == null) {
            chargeIndicators = new HashSet<ChargeIndicatorEntity>();
        }
        return chargeIndicators;
    }

    /**
     * @param chargeIndicators the chargeIndicators to set
     */
    public void setChargeIndicators(Set<ChargeIndicatorEntity> chargeIndicators) {
        if (chargeIndicators != null) {
            for (ChargeIndicatorEntity subEntity : chargeIndicators) {
                subEntity.setCharge(this);
            }
        }
        this.chargeIndicators = chargeIndicators;
    }

    /**
     * @param chargeIndicator
     */
    public void addChargeIndicator(ChargeIndicatorEntity chargeIndicator) {
        chargeIndicator.setCharge(this);
        getChargeIndicators().add(chargeIndicator);
    }

    /**
     * @return the moduleAssociations
     */
    public Set<ChargeModuleAssociationEntity> getModuleAssociations() {
        if (moduleAssociations == null) {
            moduleAssociations = new HashSet<ChargeModuleAssociationEntity>();
        }
        return moduleAssociations;
    }

    /**
     * @param moduleAssociations the moduleAssociations to set
     */
    public void setModuleAssociations(Set<ChargeModuleAssociationEntity> moduleAssociations) {
        this.moduleAssociations = moduleAssociations;
    }

    public void addModuleAssociation(ChargeModuleAssociationEntity chargeModuleAssociationEntity) {
        chargeModuleAssociationEntity.setCharge(this);
        getModuleAssociations().add(chargeModuleAssociationEntity);
    }

    /**
     * @return the histories
     */
    public Set<ChargeHistEntity> getHistories() {
        return histories;
    }

    /**
     * @param histories the histories to set
     */
    public void setHistories(Set<ChargeHistEntity> histories) {
        this.histories = histories;
    }

    /**
     * @return the dispositionHistories
     */
    public List<ChargeDispositionHistEntity> getDispositionHistories() {
        if (dispositionHistories == null) {
            dispositionHistories = new ArrayList<ChargeDispositionHistEntity>();
        }
        return dispositionHistories;
    }

    /**
     * @param dispositionHistories the dispositionHistories to set
     */
    public void setDispositionHistories(List<ChargeDispositionHistEntity> dispositionHistories) {
        this.dispositionHistories = dispositionHistories;
    }

    /**
     * @return the chargeJurisdiction
     */
    public String getChargeJurisdiction() {
        return chargeJurisdiction;
    }

    /**
     * @param chargeJurisdiction the chargeJurisdiction to set
     */
    public void setChargeJurisdiction(String chargeJurisdiction) {
        this.chargeJurisdiction = chargeJurisdiction;
    }

    /**
     * @return the oJOrderId
     */
    public Long getOJOrderId() {
        return OJOrderId;
    }

    /**
     * @param oJOrderId the oJOrderId to set
     */
    public void setOJOrderId(Long oJOrderId) {
        OJOrderId = oJOrderId;
    }

    /**
     * @return the oJStatuteIdName
     */
    public String getOJStatuteIdName() {
        return OJStatuteIdName;
    }

    /**
     * @param oJStatuteIdName the oJStatuteIdName to set
     */
    public void setOJStatuteIdName(String oJStatuteIdName) {
        OJStatuteIdName = oJStatuteIdName;
    }

    /**
     * @return the oJChargeCode
     */
    public String getOJChargeCode() {
        return OJChargeCode;
    }

    /**
     * @param oJChargeCode the oJChargeCode to set
     */
    public void setOJChargeCode(String oJChargeCode) {
        OJChargeCode = oJChargeCode;
    }

    /**
     * @return the oJChargeDescription
     */
    public String getOJChargeDescription() {
        return OJChargeDescription;
    }

    /**
     * @param oJChargeDescription the oJChargeDescription to set
     */
    public void setOJChargeDescription(String oJChargeDescription) {
        OJChargeDescription = oJChargeDescription;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChargeEntity [chargeId=" + chargeId + ", caseInfoId=" + caseInfoId + ", statuteChargeId=" + statuteChargeId + ", chargeGroupId=" + chargeGroupId
                + ", chargeCount=" + chargeCount + ", chargeStatus=" + chargeStatus + ", dispositionOutcome=" + dispositionOutcome + ", dispositionDate="
                + dispositionDate + ", primary=" + primary + ", offenceStartDate=" + offenceStartDate + ", offenceEndDate=" + offenceEndDate + ", chargeFilingDate="
                + chargeFilingDate + ", facilityId=" + facilityId + ", organizationId=" + organizationId + ", personIdentityId=" + personIdentityId + ", legalText="
                + legalText + ", chargePlea=" + chargePlea + ", chargeAppeal=" + chargeAppeal + ", chargeJurisdiction=" + chargeJurisdiction + ", OJOrderId=" + OJOrderId
                + ", OJStatuteIdName=" + OJStatuteIdName + ", OJChargeCode=" + OJChargeCode + ", OJChargeDescription=" + OJChargeDescription + ", comment=" + comment
                + ", sentenceId=" + sentenceId + ", stamp=" + stamp + ", flag=" + flag + ", version=" + version + ", chargeIdentifiers=" + chargeIdentifiers
                + ", chargeIndicators=" + chargeIndicators + ", moduleAssociations=" + moduleAssociations + ", histories=" + histories + ", dispositionHistories="
                + dispositionHistories + "]";
    }

}
