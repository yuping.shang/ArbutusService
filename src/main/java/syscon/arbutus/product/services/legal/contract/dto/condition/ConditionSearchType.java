package syscon.arbutus.product.services.legal.contract.dto.condition;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import syscon.arbutus.product.services.core.contract.dto.CommentType;
import syscon.arbutus.product.services.legal.contract.dto.ValidationUtil;

/**
 * ConditionSearchType for Condition of Case Management Service
 *
 * @author yshang, lhan
 * @since December 20, 2012
 */
public class ConditionSearchType implements Serializable {

    private static final long serialVersionUID = -5579138170770070868L;

    private Long supervisionId;

    private Date fromConditionCreateDate;
    private Date toConditionCreateDate;
    private Date fromConditionStartDate;
    private Date toConditionStartDate;
    private Date fromConditionEndDate;
    private Date toConditionEndDate;
    private CommentType conditionComment;
    private Boolean isConditionStatus;
    private String conditionType;
    private String conditionCategory;
    private String conditionSubCategory;
    private String conditionPeriodUnit;
    private String conditionDistanceUnit;
    private String conditionCurrencyUnit;
    private Long conditionPeriodValue;
    private Long conditionDistanceValue;
    private BigDecimal conditionCurrencyAmount;
    private String conditionDetails;
    private Boolean isConditionSatisfied;
    private Date fromConditionAmendmentDate;
    private Date toConditionAmendmentDate;
    private String conditionAmendmentStatus;

    //Module relationships of case management
    private Long orderSentenceId;
    private Set<Long> chargeIds;
    private Set<Long> caseInfoIds;
    private Set<Long> casePlanIds;

    /**
     * Constructor
     */
    public ConditionSearchType() {
        super();
    }

    /**
     * Constructor
     *
     * @param supervisionId
     * @param fromConditionCreateDate
     * @param toConditionCreateDate
     * @param fromConditionStartDate
     * @param toConditionStartDate
     * @param fromConditionEndDate
     * @param toConditionEndDate
     * @param conditionComment
     * @param isConditionStatus
     * @param conditionType
     * @param conditionCategory
     * @param conditionSubCategory
     * @param conditionPeriodUnit
     * @param conditionDistanceUnit
     * @param conditionCurrencyUnit
     * @param conditionPeriodValue
     * @param conditionDistanceValue
     * @param conditionCurrencyAmount
     * @param conditionDetails
     * @param isConditionSatisfied
     * @param fromConditionAmendmentDate
     * @param toConditionAmendmentDate
     * @param conditionAmendmentStatus
     * @param orderSentenceId
     * @param chargeIds
     * @param caseInfoIds
     */
    public ConditionSearchType(Long supervisionId, Date fromConditionCreateDate, Date toConditionCreateDate, Date fromConditionStartDate, Date toConditionStartDate,
            Date fromConditionEndDate, Date toConditionEndDate, CommentType conditionComment, Boolean isConditionStatus, String conditionType, String conditionCategory,
            String conditionSubCategory, String conditionPeriodUnit, String conditionDistanceUnit, String conditionCurrencyUnit, Long conditionPeriodValue,
            Long conditionDistanceValue, BigDecimal conditionCurrencyAmount, String conditionDetails, Boolean isConditionSatisfied, Date fromConditionAmendmentDate,
            Date toConditionAmendmentDate, String conditionAmendmentStatus, Long orderSentenceId, Set<Long> chargeIds, Set<Long> caseInfoIds) {
        super();
        this.supervisionId = supervisionId;
        this.fromConditionCreateDate = fromConditionCreateDate;
        this.toConditionCreateDate = toConditionCreateDate;
        this.fromConditionStartDate = fromConditionStartDate;
        this.toConditionStartDate = toConditionStartDate;
        this.fromConditionEndDate = fromConditionEndDate;
        this.toConditionEndDate = toConditionEndDate;
        this.conditionComment = conditionComment;
        this.isConditionStatus = isConditionStatus;
        this.conditionType = conditionType;
        this.conditionCategory = conditionCategory;
        this.conditionSubCategory = conditionSubCategory;
        this.conditionPeriodUnit = conditionPeriodUnit;
        this.conditionDistanceUnit = conditionDistanceUnit;
        this.conditionCurrencyUnit = conditionCurrencyUnit;
        this.conditionPeriodValue = conditionPeriodValue;
        this.conditionDistanceValue = conditionDistanceValue;
        this.conditionCurrencyAmount = conditionCurrencyAmount;
        this.conditionDetails = conditionDetails;
        this.isConditionSatisfied = isConditionSatisfied;
        this.fromConditionAmendmentDate = fromConditionAmendmentDate;
        this.toConditionAmendmentDate = toConditionAmendmentDate;
        this.conditionAmendmentStatus = conditionAmendmentStatus;
        this.orderSentenceId = orderSentenceId;
        this.chargeIds = chargeIds;
        this.caseInfoIds = caseInfoIds;
    }

    /**
     * isWellFormed -- To check if the arguments are set properly.
     * <p> At least one argument has to be set.
     *
     * @return Boolean
     */
    @Deprecated
    public Boolean isWellFormed() {
        if (ValidationUtil.isEmpty(supervisionId) &&
                ValidationUtil.isEmpty(fromConditionCreateDate) &&
                ValidationUtil.isEmpty(toConditionCreateDate) &&
                ValidationUtil.isEmpty(fromConditionStartDate) &&
                ValidationUtil.isEmpty(toConditionStartDate) &&
                ValidationUtil.isEmpty(fromConditionEndDate) &&
                ValidationUtil.isEmpty(toConditionEndDate) &&
                (conditionComment == null || !conditionComment.isWellFormed()) &&
                ValidationUtil.isEmpty(isConditionStatus) &&
                ValidationUtil.isEmpty(conditionType) &&
                ValidationUtil.isEmpty(conditionCategory) &&
                ValidationUtil.isEmpty(conditionSubCategory) &&
                ValidationUtil.isEmpty(conditionPeriodUnit) &&
                ValidationUtil.isEmpty(conditionDistanceUnit) &&
                ValidationUtil.isEmpty(conditionCurrencyUnit) &&
                ValidationUtil.isEmpty(conditionPeriodValue) &&
                ValidationUtil.isEmpty(conditionDistanceValue) &&
                ValidationUtil.isEmpty(conditionCurrencyAmount) &&
                ValidationUtil.isEmpty(conditionDetails) &&
                ValidationUtil.isEmpty(isConditionSatisfied) &&
                ValidationUtil.isEmpty(fromConditionAmendmentDate) &&
                ValidationUtil.isEmpty(toConditionAmendmentDate) &&
                ValidationUtil.isEmpty(conditionAmendmentStatus) &&
                ValidationUtil.isEmpty(orderSentenceId) &&
                ValidationUtil.isEmpty(chargeIds) &&
                ValidationUtil.isEmpty(caseInfoIds)) {

            return false;
        }

        return true;
    }

    /**
     * isValid -- To check if the given arguments are valid.
     *
     * @return Boolean
     */
    @Deprecated
    public Boolean isValid() {

        if (!isWellFormed()) {
			return false;
		}

        return true;
    }

    /**
     * Association to Supervision
     *
     * @return the supervision id
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * Association to Supervision
     *
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * Date when the Condition was created. Allow search by date range.
     * Beginning of date range search for Create Date
     *
     * @return the conditionCreateDate
     */
    public Date getFromConditionCreateDate() {
        return fromConditionCreateDate;
    }

    /**
     * Date when the Condition was created. Allow search by date range.
     * Beginning of date range search for Create Date
     *
     * @param fromConditionCreateDate the conditionCreateDate to set
     */
    public void setFromConditionCreateDate(Date fromConditionCreateDate) {
        this.fromConditionCreateDate = fromConditionCreateDate;
    }

    /**
     * Date when the Condition was created. End of date range search for Create Date.
     *
     * @return the toConditionCreateDate
     */
    public Date getToConditionCreateDate() {
        return toConditionCreateDate;
    }

    /**
     * Date when the Condition was created. End of date range search for Create Date.
     *
     * @param toConditionCreateDate the toConditionCreateDate to set
     */
    public void setToConditionCreateDate(Date toConditionCreateDate) {
        this.toConditionCreateDate = toConditionCreateDate;
    }

    /**
     * Date when the condition is effective from. Beginning of date range search for ConditionStartDate.
     *
     * @return the fromConditionStartDate
     */
    public Date getFromConditionStartDate() {
        return fromConditionStartDate;
    }

    /**
     * Date when the condition is effective from. Beginning of date range search for ConditionStartDate.
     *
     * @param fromConditionStartDate the fromConditionStartDate to set
     */
    public void setFromConditionStartDate(Date fromConditionStartDate) {
        this.fromConditionStartDate = fromConditionStartDate;
    }

    /**
     * Date when the condition is effective from. End of date range search for ConditionStartDate.
     *
     * @return the toConditionStartDate
     */
    public Date getToConditionStartDate() {
        return toConditionStartDate;
    }

    /**
     * Date when the condition is effective from. End of date range search for ConditionStartDate.
     *
     * @param toConditionStartDate the toConditionStartDate to set
     */
    public void setToConditionStartDate(Date toConditionStartDate) {
        this.toConditionStartDate = toConditionStartDate;
    }

    /**
     * Date when the condition period is over. Beginning of date range search for ConditionEndDate.
     *
     * @return the fromConditionEndDate
     */
    public Date getFromConditionEndDate() {
        return fromConditionEndDate;
    }

    /**
     * Date when the condition period is over. Beginning of date range search for ConditionEndDate.
     *
     * @param fromConditionEndDate the fromConditionEndDate to set
     */
    public void setFromConditionEndDate(Date fromConditionEndDate) {
        this.fromConditionEndDate = fromConditionEndDate;
    }

    /**
     * Date when the condition period is over. End of date range search for ConditionEndDate.
     *
     * @return the toConditionEndDate
     */
    public Date getToConditionEndDate() {
        return toConditionEndDate;
    }

    /**
     * Date when the condition period is over. End of date range search for ConditionEndDate.
     *
     * @param toConditionEndDate the toConditionEndDate to set
     */
    public void setToConditionEndDate(Date toConditionEndDate) {
        this.toConditionEndDate = toConditionEndDate;
    }

    /**
     * Comment for the Condition.
     *
     * @return the conditionComment
     */
    public CommentType getConditionComment() {
        return conditionComment;
    }

    /**
     * Comment for the Condition.
     *
     * @param conditionComment the conditionComment to set
     */
    public void setConditionComment(CommentType conditionComment) {
        this.conditionComment = conditionComment;
    }

    /**
     * Status of the condition as active or inactive based on the Boolean value selected.
     *
     * @return the isConditionStatus
     */
    public Boolean getIsConditionStatus() {
        return isConditionStatus;
    }

    /**
     * Status of the condition as active or inactive based on the Boolean value selected.
     *
     * @param isConditionStatus the isConditionStatus to set
     */
    public void setIsConditionStatus(Boolean isConditionStatus) {
        this.isConditionStatus = isConditionStatus;
    }

    /**
     * Is a logical grouping of Conditions by Sentences, Orders, Charges etc.
     *
     * @return the conditionType
     */
    public String getConditionType() {
        return conditionType;
    }

    /**
     * Is a logical grouping of Conditions by Sentences, Orders, Charges etc.
     *
     * @param conditionType the conditionType to set
     */
    public void setConditionType(String conditionType) {
        this.conditionType = conditionType;
    }

    /**
     * A condition can also be classified under different categories like Restitution, fines etc.
     *
     * @return the conditionCategory
     */
    public String getConditionCategory() {
        return conditionCategory;
    }

    /**
     * A condition can also be classified under different categories like Restitution, fines etc.
     *
     * @param conditionCategory the conditionCategory to set
     */
    public void setConditionCategory(String conditionCategory) {
        this.conditionCategory = conditionCategory;
    }

    /**
     * Is a logical sub grouping of the condition type. E.g. Sentence condition types could be
     * sub grouped under standard and non-standard conditions.
     *
     * @return the conditionSubCategory
     */
    public String getConditionSubCategory() {
        return conditionSubCategory;
    }

    /**
     * Is a logical sub grouping of the condition type. E.g. Sentence condition types could be
     * sub grouped under standard and non-standard conditions.
     *
     * @param conditionSubCategory the conditionSubCategory to set
     */
    public void setConditionSubCategory(String conditionSubCategory) {
        this.conditionSubCategory = conditionSubCategory;
    }

    /**
     * The period of the condition restriction like days, hours etc.
     *
     * @return the conditionPeriodUnit
     */
    public String getConditionPeriodUnit() {
        return conditionPeriodUnit;
    }

    /**
     * The period of the condition restriction like days, hours etc.
     *
     * @param conditionPeriodUnit the conditionPeriodUnit to set
     */
    public void setConditionPeriodUnit(String conditionPeriodUnit) {
        this.conditionPeriodUnit = conditionPeriodUnit;
    }

    /**
     * The Distance unit.
     *
     * @return the conditionDistanceUnit
     */
    public String getConditionDistanceUnit() {
        return conditionDistanceUnit;
    }

    /**
     * The Distance unit.
     *
     * @param conditionDistanceUnit the conditionDistanceUnit to set
     */
    public void setConditionDistanceUnit(String conditionDistanceUnit) {
        this.conditionDistanceUnit = conditionDistanceUnit;
    }

    /**
     * The Currency unit.
     *
     * @return the conditionCurrencyUnit
     */
    public String getConditionCurrencyUnit() {
        return conditionCurrencyUnit;
    }

    /**
     * The Currency unit.
     *
     * @param conditionCurrencyUnit the conditionCurrencyUnit to set
     */
    public void setConditionCurrencyUnit(String conditionCurrencyUnit) {
        this.conditionCurrencyUnit = conditionCurrencyUnit;
    }

    /**
     * The period value.
     *
     * @return the conditionPeriodValue
     */
    public Long getConditionPeriodValue() {
        return conditionPeriodValue;
    }

    /**
     * The period value.
     *
     * @param conditionPeriodValue the conditionPeriodValue to set
     */
    public void setConditionPeriodValue(Long conditionPeriodValue) {
        this.conditionPeriodValue = conditionPeriodValue;
    }

    /**
     * The distance value.
     *
     * @return the conditionDistanceValue
     */
    public Long getConditionDistanceValue() {
        return conditionDistanceValue;
    }

    /**
     * The distance value.
     *
     * @param conditionDistanceValue the conditionDistanceValue to set
     */
    public void setConditionDistanceValue(Long conditionDistanceValue) {
        this.conditionDistanceValue = conditionDistanceValue;
    }

    /**
     * The currency value.
     *
     * @return the conditionCurrencyAmount
     */
    public BigDecimal getConditionCurrencyAmount() {
        return conditionCurrencyAmount;
    }

    /**
     * The currency value.
     *
     * @param conditionCurrencyAmount the conditionCurrencyAmount to set
     */
    public void setConditionCurrencyAmount(BigDecimal conditionCurrencyAmount) {
        this.conditionCurrencyAmount = conditionCurrencyAmount;
    }

    /**
     * Standard legal description for the conditions.
     *
     * @return the conditionDetails
     */
    public String getConditionDetails() {
        return conditionDetails;
    }

    /**
     * Standard legal description for the conditions.
     *
     * @param conditionDetails the conditionDetails to set
     */
    public void setConditionDetails(String conditionDetails) {
        this.conditionDetails = conditionDetails;
    }

    /**
     * Condition has been satisfied for say release purposes.
     *
     * @return the isConditionSatisfied
     */
    public Boolean getIsConditionSatisfied() {
        return isConditionSatisfied;
    }

    /**
     * Condition has been satisfied for say release purposes.
     *
     * @param isConditionSatisfied the isConditionSatisfied to set
     */
    public void setIsConditionSatisfied(Boolean isConditionSatisfied) {
        this.isConditionSatisfied = isConditionSatisfied;
    }

    /**
     * The date an amendment was made to the conditions imposed
     * by a court e.g. court reduces the period of community service.
     * All details of the amendment will be available with the Order it arose from.
     *
     * @return the fromConditionAmendmentDate
     */
    public Date getFromConditionAmendmentDate() {
        return fromConditionAmendmentDate;
    }

    /**
     * The date an amendment was made to the conditions imposed
     * by a court e.g. court reduces the period of community service.
     * All details of the amendment will be available with the Order it arose from.
     *
     * @param fromConditionAmendmentDate the fromConditionAmendmentDate to set
     */
    public void setFromConditionAmendmentDate(Date fromConditionAmendmentDate) {
        this.fromConditionAmendmentDate = fromConditionAmendmentDate;
    }

    /**
     * The date an amendment was made to the conditions imposed
     * by a court e.g. court reduces the period of community service.
     * All details of the amendment will be available with the Order it arose from.
     *
     * @return the toConditionAmendmentDate
     */
    public Date getToConditionAmendmentDate() {
        return toConditionAmendmentDate;
    }

    /**
     * The date an amendment was made to the conditions imposed
     * by a court e.g. court reduces the period of community service.
     * All details of the amendment will be available with the Order it arose from.
     *
     * @param toConditionAmendmentDate the toConditionAmendmentDate to set
     */
    public void setToConditionAmendmentDate(Date toConditionAmendmentDate) {
        this.toConditionAmendmentDate = toConditionAmendmentDate;
    }

    /**
     * The status of the Amendment
     * which could be- Accepted, Rejected, In Progress.
     *
     * @return the conditionAmendmentStatus
     */
    public String getConditionAmendmentStatus() {
        return conditionAmendmentStatus;
    }

    /**
     * The status of the Amendment
     * which could be- Accepted, Rejected, In Progress.
     *
     * @param conditionAmendmentStatus the conditionAmendmentStatus to set
     */
    public void setConditionAmendmentStatus(String conditionAmendmentStatus) {
        this.conditionAmendmentStatus = conditionAmendmentStatus;
    }

    /**
     * Id of instance OrderSentence
     *
     * @return the orderSentenceId
     */
    public Long getOrderSentenceId() {
        return orderSentenceId;
    }

    /**
     * Id of instance OrderSentence
     *
     * @param orderSentenceId the orderSentenceId to set
     */
    public void setOrderSentenceId(Long orderSentenceId) {
        this.orderSentenceId = orderSentenceId;
    }

    /**
     * A set of Id of instance Charge
     *
     * @return the chargeIds
     */
    public Set<Long> getChargeIds() {
        return chargeIds;
    }

    /**
     * A set of Id of instance Charge
     *
     * @param chargeIds the chargeIds to set
     */
    public void setChargeIds(Set<Long> chargeIds) {
        this.chargeIds = chargeIds;
    }

    /**
     * A set of Id of instance Case Information
     *
     * @return the caseInfoIds
     */
    public Set<Long> getCaseInfoIds() {
        return caseInfoIds;
    }

    /**
     * A set of Id of instance Case Information
     *
     * @param caseInfoIds the caseInfoIds to set
     */
    public void setCaseInfoIds(Set<Long> caseInfoIds) {
        this.caseInfoIds = caseInfoIds;
    }

    /**
     * Get a set of Id of Case Plan
     *
     * @return the casePlanIds Set<Long>
     */
    public Set<Long> getCasePlanIds() {
        return casePlanIds;
    }

    /**
     * Set a set of Id of Case Plan
     *
     * @param Set<Long> the casePlanIds to set
     */
    public void setCasePlanIds(Set<Long> casePlanIds) {
        this.casePlanIds = casePlanIds;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ConditionSearchType [supervisionId=" + supervisionId + ", fromConditionCreateDate=" + fromConditionCreateDate + ", toConditionCreateDate="
                + toConditionCreateDate + ", fromConditionStartDate=" + fromConditionStartDate + ", toConditionStartDate=" + toConditionStartDate
                + ", fromConditionEndDate=" + fromConditionEndDate + ", toConditionEndDate=" + toConditionEndDate + ", conditionComment=" + conditionComment
                + ", isConditionStatus=" + isConditionStatus + ", conditionType=" + conditionType + ", conditionCategory=" + conditionCategory + ", conditionSubCategory="
                + conditionSubCategory + ", conditionPeriodUnit=" + conditionPeriodUnit + ", conditionDistanceUnit=" + conditionDistanceUnit + ", conditionCurrencyUnit="
                + conditionCurrencyUnit + ", conditionPeriodValue=" + conditionPeriodValue + ", conditionDistanceValue=" + conditionDistanceValue
                + ", conditionCurrencyAmount=" + conditionCurrencyAmount + ", conditionDetails=" + conditionDetails + ", isConditionSatisfied=" + isConditionSatisfied
                + ", fromConditionAmendmentDate=" + fromConditionAmendmentDate + ", toConditionAmendmentDate=" + toConditionAmendmentDate + ", conditionAmendmentStatus="
                + conditionAmendmentStatus + ", orderSentenceId=" + orderSentenceId + ", chargeIds=" + chargeIds + ", caseInfoIds=" + caseInfoIds + ", casePlanIds="
                + casePlanIds + "]";
    }

}
