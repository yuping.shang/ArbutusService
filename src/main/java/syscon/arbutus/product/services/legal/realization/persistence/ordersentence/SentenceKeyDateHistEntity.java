package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.legal.realization.persistence.CommentEntity;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * SentenceKeyDateHistEntity for Supervision Key Dates
 *
 * @author yshang
 * @DbComment LEG_OrdSntKeyDateHst 'SentenceKeyDateHistEntity for the history record of Supervision Key Dates'
 * @since July 22, 2013
 */
//@Audited
@Entity
@Table(name = "LEG_OrdSntKeyDateHst")
public class SentenceKeyDateHistEntity implements Serializable {

    private static final long serialVersionUID = -7944696618859246250L;

    /**
     * @DbComment hstId 'Unique identification of an order instance'
     */
    @Id
    @Column(name = "hstId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDSNTKEYDATEHST_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDSNTKEYDATEHST_ID", sequenceName = "SEQ_LEG_ORDSNTKEYDATEHST_ID", allocationSize = 1)
    private Long historyId;

    /**
     * @DbComment supervisionId 'Supervision Id'
     */
    @Column(name = "supervisionId")
    private Long supervisionId;

    /**
     * @DbComment staffId 'Staff Id'
     */
    @Column(name = "staffId", nullable = true)
    private Long staffId;

    /**
     * @DbComment CommentUserId 'User ID who created/updated the comments'
     * @DbComment CommentDate 'The Date/Time when the comment was created'
     * @DbComment CommentText 'The comment text'
     */
    @Embedded
    private CommentEntity comments;

    /**
     * @DbComment reviewRequired 'Is Date Review Required'
     */
    @Column(name = "reviewRequired", nullable = true)
    private Boolean reviewRequired;

    @ForeignKey(name = "Fk_LEG_OrdSntKeyDateHst")
    @OneToMany(mappedBy = "sentenceKeyDate", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @BatchSize(size = 20)
    private Set<KeyDateHistEntity> keyDates;

    /**
     * @DbComment supervisionSentenceId 'Supervision Sentence Id'
     */
    @Column(name = "supervisionSentenceId", nullable = true)
    private Long supervisionSentenceId;

    /**
     * @DbComment aggregateSentenceId 'Aggregate Sentence Id'
     */
    @Column(name = "aggregateSentenceId", nullable = true)
    private Long aggregateSentenceId;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'Version for Hibernate use'
     */
    @NotAudited
    @Version
    private Long version;

    /**
     * Constructor
     */
    public SentenceKeyDateHistEntity() {
        super();
    }

    public SentenceKeyDateHistEntity(Long historyId, Long supervisionId, Long staffId, CommentEntity comments, Boolean reviewRequired, Set<KeyDateHistEntity> keyDates,
            StampEntity stamp, Long version) {
        super();
        this.historyId = historyId;
        this.supervisionId = supervisionId;
        this.staffId = staffId;
        this.comments = comments;
        this.reviewRequired = reviewRequired;
        this.keyDates = keyDates;
        this.stamp = stamp;
        this.version = version;
    }

    /**
     * @return the historyId
     */
    public Long getHistoryId() {
        return historyId;
    }

    /**
     * @param historyId the historyId to set
     */
    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    /**
     * @return the supervisionId
     */
    public Long getSupervisionId() {
        return supervisionId;
    }

    /**
     * @param supervisionId the supervisionId to set
     */
    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    /**
     * @return the staffId
     */
    public Long getStaffId() {
        return staffId;
    }

    /**
     * @param staffId the staffId to set
     */
    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    /**
     * @return the comments
     */
    public CommentEntity getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(CommentEntity comments) {
        this.comments = comments;
    }

    /**
     * @return the reviewRequired
     */
    public Boolean getReviewRequired() {
        return reviewRequired;
    }

    /**
     * @param reviewRequired the reviewRequired to set
     */
    public void setReviewRequired(Boolean reviewRequired) {
        this.reviewRequired = reviewRequired;
    }

    /**
     * @return the keyDates
     */
    public Set<KeyDateHistEntity> getKeyDates() {
        if (keyDates == null) {
			keyDates = new HashSet<KeyDateHistEntity>();
		}
        return keyDates;
    }

    /**
     * @param keyDates the keyDates to set
     */
    public void setKeyDates(Set<KeyDateHistEntity> keyDates) {
        this.keyDates = keyDates;
    }

    public void addKeyDate(KeyDateHistEntity keyDate) {
        keyDate.setSentenceKeyDate(this);
        getKeyDates().add(keyDate);
    }

    /**
     * @return the supervisionSentenceId
     */
    public Long getSupervisionSentenceId() {
        return supervisionSentenceId;
    }

    /**
     * @param supervisionSentenceId the supervisionSentenceId to set
     */
    public void setSupervisionSentenceId(Long supervisionSentenceId) {
        this.supervisionSentenceId = supervisionSentenceId;
    }

    /**
     * @return the aggregateSentenceId
     */
    public Long getAggregateSentenceId() {
        return aggregateSentenceId;
    }

    /**
     * @param aggregateSentenceId the aggregateSentenceId to set
     */
    public void setAggregateSentenceId(Long aggregateSentenceId) {
        this.aggregateSentenceId = aggregateSentenceId;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((aggregateSentenceId == null) ? 0 : aggregateSentenceId.hashCode());
        result = prime * result + ((historyId == null) ? 0 : historyId.hashCode());
        result = prime * result + ((keyDates == null) ? 0 : keyDates.hashCode());
        result = prime * result + ((reviewRequired == null) ? 0 : reviewRequired.hashCode());
        result = prime * result + ((staffId == null) ? 0 : staffId.hashCode());
        result = prime * result + ((supervisionId == null) ? 0 : supervisionId.hashCode());
        result = prime * result + ((supervisionSentenceId == null) ? 0 : supervisionSentenceId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        SentenceKeyDateHistEntity other = (SentenceKeyDateHistEntity) obj;
        if (aggregateSentenceId == null) {
            if (other.aggregateSentenceId != null) {
				return false;
			}
        } else if (!aggregateSentenceId.equals(other.aggregateSentenceId)) {
			return false;
		}
        if (historyId == null) {
            if (other.historyId != null) {
				return false;
			}
        } else if (!historyId.equals(other.historyId)) {
			return false;
		}
        if (keyDates == null) {
            if (other.keyDates != null) {
				return false;
			}
        } else if (!keyDates.equals(other.keyDates)) {
			return false;
		}
        if (reviewRequired == null) {
            if (other.reviewRequired != null) {
				return false;
			}
        } else if (!reviewRequired.equals(other.reviewRequired)) {
			return false;
		}
        if (staffId == null) {
            if (other.staffId != null) {
				return false;
			}
        } else if (!staffId.equals(other.staffId)) {
			return false;
		}
        if (supervisionId == null) {
            if (other.supervisionId != null) {
				return false;
			}
        } else if (!supervisionId.equals(other.supervisionId)) {
			return false;
		}
        if (supervisionSentenceId == null) {
            if (other.supervisionSentenceId != null) {
				return false;
			}
        } else if (!supervisionSentenceId.equals(other.supervisionSentenceId)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SentenceKeyDateHistEntity [historyId=" + historyId + ", supervisionId=" + supervisionId + ", staffId=" + staffId + ", reviewRequired=" + reviewRequired
                + ", keyDates=" + keyDates + ", supervisionSentenceId=" + supervisionSentenceId + ", aggregateSentenceId=" + aggregateSentenceId + "]";
    }

}