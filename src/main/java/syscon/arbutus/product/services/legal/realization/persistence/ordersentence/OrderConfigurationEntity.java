package syscon.arbutus.product.services.legal.realization.persistence.ordersentence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import syscon.arbutus.product.services.legal.realization.persistence.MetaSet;
import syscon.arbutus.product.services.realization.model.MetaCode;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * OrderConfigurationEntity for OrderSentence of Legal Service
 *
 * @author yshang
 * @version 1.0
 * @DbComment LEG_OrdConfig 'Order Configuration table for Order Sentence Module of Legal Service'
 * @since December 31, 2012
 */
@Audited
@Entity
@Table(name = "LEG_OrdConfig")
public class OrderConfigurationEntity implements Serializable {

    private static final long serialVersionUID = -737931875158542731L;

    /**
     * @DbComment orderConfigId 'Unique identification of an Order Configuration instance'
     */
    @Id
    @Column(name = "orderConfigId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_LEG_ORDCONFIG_ID")
    @SequenceGenerator(name = "SEQ_LEG_ORDCONFIG_ID", sequenceName = "SEQ_LEG_ORDCONFIG_ID", allocationSize = 1)
    private Long orderConfigId;

    /**
     * @DbComment orderClassification 'The order classification (WarrantDetainer, LegalOrder, Sentence). This defines the type of instance of the service.'
     */
    @Column(name = "orderClassification", nullable = false, length = 64)
    @MetaCode(set = MetaSet.ORDER_CLASSIFICATION)
    private String orderClassification;

    /**
     * @DbComment orderType 'The client defined order type that maps to the order classification.'
     */
    @Column(name = "orderType", nullable = false, length = 64)
    @MetaCode(set = MetaSet.ORDER_TYPE)
    private String orderType;

    /**
     * @DbComment orderCategory 'The category of the order, e.g. InJurisdiction case, Out of Jurisdiction case.'
     */
    @Column(name = "orderCategory", nullable = false, length = 64)
    @MetaCode(set = MetaSet.ORDER_CATEGORY)
    private String orderCategory;

    /**
     * @DbComment isHoldingOrder 'If true then the order is a holding document, false otherwise.'
     */
    @Column(name = "isHoldingOrder", nullable = false, length = 64)
    private Boolean isHoldingOrder;

    @ForeignKey(name = "Fk_LEG_OrdConfig1")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @BatchSize(size = 20)
    @JoinColumn(name = "issueAgns")
    @AuditJoinTable(name = "LEG_OrdCfg_Notif_AUD", inverseJoinColumns = @JoinColumn(name = "notificationId"))
    private Set<NotificationEntity> issuingAgencies;

    /**
     * @DbComment isSchedulingNeeded 'If true, scheduling is needed for the order, false otherwise.'
     */
    @Column(name = "isSchedulingNeeded", nullable = false)
    private Boolean isSchedulingNeeded;

    /**
     * @DbComment hasCharges 'If true, the order has charges related, false otherwise.'
     */
    @Column(name = "hasCharges", nullable = false)
    private Boolean hasCharges;

    /**
     * @DbComment startDate 'The date an order was issued'
     */
    @Column(name = "startDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    /**
     * @DbComment endDate 'The date an order was issued'
     */
    @Column(name = "endDate", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @Embedded
    private StampEntity stamp;

    /**
     * @DbComment version 'Version for Hibernate use'
     */
    @Version
    private Long version;

    /**
     * Constructor
     */
    public OrderConfigurationEntity() {
        super();
    }

    /**
     * Constructor
     *
     * @param orderConfigId
     * @param orderClassification
     * @param orderType
     * @param orderCategory
     * @param isHoldingOrder
     * @param issuingAgencies
     * @param isSchedulingNeeded
     * @param hasCharges
     * @param stamp
     */
    public OrderConfigurationEntity(Long orderConfigId, String orderClassification, String orderType, String orderCategory, Boolean isHoldingOrder,
            Set<NotificationEntity> issuingAgencies, Boolean isSchedulingNeeded, Boolean hasCharges, StampEntity stamp, Date startDate, Date endDate) {
        super();
        this.orderConfigId = orderConfigId;
        this.orderClassification = orderClassification;
        this.orderType = orderType;
        this.orderCategory = orderCategory;
        this.isHoldingOrder = isHoldingOrder;
        this.issuingAgencies = issuingAgencies;
        this.isSchedulingNeeded = isSchedulingNeeded;
        this.hasCharges = hasCharges;
        this.stamp = stamp;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /**
     * @return the orderConfigId
     */
    public Long getOrderConfigId() {
        return orderConfigId;
    }

    /**
     * @param orderConfigId the orderConfigId to set
     */
    public void setOrderConfigId(Long orderConfigId) {
        this.orderConfigId = orderConfigId;
    }

    /**
     * @return the orderClassification
     */
    public String getOrderClassification() {
        return orderClassification;
    }

    /**
     * @param orderClassification the orderClassification to set
     */
    public void setOrderClassification(String orderClassification) {
        this.orderClassification = orderClassification;
    }

    /**
     * @return the orderType
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * @param orderType the orderType to set
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    /**
     * @return the orderCategory
     */
    public String getOrderCategory() {
        return orderCategory;
    }

    /**
     * @param orderCategory the orderCategory to set
     */
    public void setOrderCategory(String orderCategory) {
        this.orderCategory = orderCategory;
    }

    /**
     * @return the isHoldingOrder
     */
    public Boolean getIsHoldingOrder() {
        return isHoldingOrder;
    }

    /**
     * @param isHoldingOrder the isHoldingOrder to set
     */
    public void setIsHoldingOrder(Boolean isHoldingOrder) {
        this.isHoldingOrder = isHoldingOrder;
    }

    /**
     * @return the issuingAgencies
     */
    public Set<NotificationEntity> getIssuingAgencies() {
        if (issuingAgencies == null) {
			issuingAgencies = new HashSet<NotificationEntity>();
		}
        return issuingAgencies;
    }

    /**
     * @param issuingAgencies the issuingAgencies to set
     */
    public void setIssuingAgencies(Set<NotificationEntity> issuingAgencies) {
        this.issuingAgencies = issuingAgencies;
    }

    /**
     * @return the isSchedulingNeeded
     */
    public Boolean getIsSchedulingNeeded() {
        return isSchedulingNeeded;
    }

    /**
     * @param isSchedulingNeeded the isSchedulingNeeded to set
     */
    public void setIsSchedulingNeeded(Boolean isSchedulingNeeded) {
        this.isSchedulingNeeded = isSchedulingNeeded;
    }

    /**
     * @return the hasCharges
     */
    public Boolean getHasCharges() {
        return hasCharges;
    }

    /**
     * @param hasCharges the hasCharges to set
     */
    public void setHasCharges(Boolean hasCharges) {
        this.hasCharges = hasCharges;
    }

    /**
     * @return the stamp
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((orderCategory == null) ? 0 : orderCategory.hashCode());
        result = prime * result + ((orderClassification == null) ? 0 : orderClassification.hashCode());
        result = prime * result + ((orderConfigId == null) ? 0 : orderConfigId.hashCode());
        result = prime * result + ((orderType == null) ? 0 : orderType.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        OrderConfigurationEntity other = (OrderConfigurationEntity) obj;
        if (orderCategory == null) {
            if (other.orderCategory != null) {
				return false;
			}
        } else if (!orderCategory.equals(other.orderCategory)) {
			return false;
		}
        if (orderClassification == null) {
            if (other.orderClassification != null) {
				return false;
			}
        } else if (!orderClassification.equals(other.orderClassification)) {
			return false;
		}
        if (orderConfigId == null) {
            if (other.orderConfigId != null) {
				return false;
			}
        } else if (!orderConfigId.equals(other.orderConfigId)) {
			return false;
		}
        if (orderType == null) {
            if (other.orderType != null) {
				return false;
			}
        } else if (!orderType.equals(other.orderType)) {
			return false;
		}
        if (startDate == null) {
            if (other.startDate != null) {
				return false;
			}
        } else if (!startDate.equals(other.startDate)) {
			return false;
		}
        if (endDate == null) {
            if (other.endDate != null) {
				return false;
			}
        } else if (!endDate.equals(other.endDate)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "OrderConfigurationEntity [orderConfigId=" + orderConfigId + ", orderClassification=" + orderClassification + ", orderType=" + orderType
                + ", orderCategory=" + orderCategory + ", isHoldingOrder=" + isHoldingOrder + ", issuingAgencies=" + issuingAgencies + ", isSchedulingNeeded="
                + isSchedulingNeeded + ", hasCharges=" + hasCharges + ", startDate=" + startDate + ", endDate=" + endDate + "]";
    }

}
