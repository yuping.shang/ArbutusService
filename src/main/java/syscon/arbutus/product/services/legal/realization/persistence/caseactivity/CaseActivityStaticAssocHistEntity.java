package syscon.arbutus.product.services.legal.realization.persistence.caseactivity;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.annotations.DiscriminatorOptions;
import org.hibernate.envers.NotAudited;
import syscon.arbutus.product.services.realization.model.StampEntity;

/**
 * The representation of the static association history entity.
 *
 * @author lhan
 * @DbComment LEG_CAStaticAsscHist 'Static association history table for Case Activity Service'
 * @DbComment .createUserId 'Create User Id'
 * @DbComment .createDateTime 'Create Date Time'
 * @DbComment .modifyUserId 'Modify User Id'
 * @DbComment .modifyDateTime 'Modify Date Time'
 * @DbComment .invocationContext 'Invocation Context'
 * @DbComment .DTYPE 'Dicriminator column for Hibernate use'
 */
//@Audited
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorOptions(force = true)
@Table(name = "LEG_CAStaticAsscHist")
public class CaseActivityStaticAssocHistEntity implements Serializable {

    private static final long serialVersionUID = 1378801716380548502L;

    /**
     * @DbComment 'Association Id'
     */
    @Id
    @Column(name = "AssociationId")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "Seq_LEG_CAStaticAsscHist_Id")
    @SequenceGenerator(name = "Seq_LEG_CAStaticAsscHist_Id", sequenceName = "Seq_LEG_CAStaticAsscHist_Id", allocationSize = 1)
    private Long associationId;

    /**
     * @DbComment 'From Identifier'
     */
    @Column(name = "FromIdentifier", nullable = false)
    private Long fromIdentifier;

    /**
     * @DbComment 'To Identifier'
     */
    @Column(name = "ToIdentifier", nullable = false)
    private Long toIdentifier;

    @NotAudited
    @Embedded
    private StampEntity stamp;

    /**
     * Default constructor
     */
    public CaseActivityStaticAssocHistEntity() {

    }

    /**
     * Constructor.
     *
     * @param associationId
     * @param fromIdentifier
     * @param toIdentifier
     * @param stamp
     * @param caseActivity
     */
    public CaseActivityStaticAssocHistEntity(Long associationId, Long fromIdentifier, Long toIdentifier, StampEntity stamp) {
        super();
        this.associationId = associationId;
        this.fromIdentifier = fromIdentifier;
        this.toIdentifier = toIdentifier;
        this.setStamp(stamp);
    }

    /**
     * Gets the value of the associationId property
     *
     * @return Long
     */
    public Long getAssociationId() {
        return associationId;
    }

    /**
     * Sets the value of the associationId property
     *
     * @param associationId Long
     */
    public void setAssociationId(Long associationId) {
        this.associationId = associationId;
    }

    /**
     * Gets the value of the fromIdentifier property
     *
     * @return Long
     */
    public Long getFromIdentifier() {
        return fromIdentifier;
    }

    /**
     * Sets the value of the fromIdentifier property
     *
     * @param fromIdentifier Long
     */
    public void setFromIdentifier(Long fromIdentifier) {
        this.fromIdentifier = fromIdentifier;
    }

    /**
     * Gets the value of the toIdentifier property
     *
     * @return Long
     */
    public Long getToIdentifier() {
        return toIdentifier;
    }

    /**
     * Sets the value of the toIdentifier property
     *
     * @param toIdentifier Long
     */
    public void setToIdentifier(Long toIdentifier) {
        this.toIdentifier = toIdentifier;
    }

    /**
     * Gets the value of the stamp property
     *
     * @return StampEntity
     */
    public StampEntity getStamp() {
        return stamp;
    }

    /**
     * Sets the value of the stamp property
     *
     * @param stamp StampEntity
     */
    public void setStamp(StampEntity stamp) {
        //Only copy values not assign the stamp directly.
        this.stamp = new StampEntity();
        this.stamp.setCreateDateTime(stamp.getCreateDateTime());
        this.stamp.setCreateUserId(stamp.getCreateUserId());
        this.stamp.setInvocationContext(stamp.getInvocationContext());
        this.stamp.setModifyDateTime(stamp.getModifyDateTime());
        this.stamp.setModifyUserId(stamp.getModifyUserId());
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fromIdentifier == null) ? 0 : fromIdentifier.hashCode());
        result = prime * result + ((stamp == null) ? 0 : stamp.hashCode());
        result = prime * result + ((toIdentifier == null) ? 0 : toIdentifier.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        CaseActivityStaticAssocHistEntity other = (CaseActivityStaticAssocHistEntity) obj;
        if (fromIdentifier == null) {
            if (other.fromIdentifier != null) {
				return false;
			}
        } else if (!fromIdentifier.equals(other.fromIdentifier)) {
			return false;
		}
        if (stamp == null) {
            if (other.stamp != null) {
				return false;
			}
        } else if (!stamp.equals(other.stamp)) {
			return false;
		}
        if (toIdentifier == null) {
            if (other.toIdentifier != null) {
				return false;
			}
        } else if (!toIdentifier.equals(other.toIdentifier)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StaticAssociationEntity [associationId=" + associationId + ", fromIdentifier=" + fromIdentifier + ", toIdentifier=" + toIdentifier + ", stamp=" + stamp
                + "]";
    }

}
