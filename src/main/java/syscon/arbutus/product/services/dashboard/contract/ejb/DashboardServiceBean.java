package syscon.arbutus.product.services.dashboard.contract.ejb;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.*;

import com.google.gson.Gson;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.assessment.realization.persistence.AssessmentEntity;
import syscon.arbutus.product.services.dashboard.contract.interfaces.DashboardService;
import syscon.arbutus.product.services.facility.realization.persistence.FacilityEntity;
import syscon.arbutus.product.services.facilityinternallocation.contract.dto.UsageCategory;
import syscon.arbutus.product.services.facilityinternallocation.realization.persistence.InternalLocationEntity;
import syscon.arbutus.product.services.housing.realization.persistence.HousingBedMgmtActivityEntity;
import syscon.arbutus.product.services.legal.realization.persistence.caseinformation.CaseInfoEntity;
import syscon.arbutus.product.services.person.realization.persistence.person.PersonEntity;
import syscon.arbutus.product.services.person.realization.persistence.personidentity.PersonIdentityEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.supervision.realization.persistence.SupervisionEntity;

/**
 * DashboardServiceBean The implementation of DashboardService interface
 *
 * @author Y.Shang
 * @version 2.0
 * @since March 21, 2014
 */
@Stateless(mappedName = "DashboardService", description = "The Dashboard service")
@Remote(DashboardService.class)
public class DashboardServiceBean implements DashboardService {

    private static Logger log = LoggerFactory.getLogger(DashboardServiceBean.class);

    String totalPopulation = "0";
    List<FacilityHolder> populationDataHolder = new ArrayList<FacilityHolder>();
    List<OccupancyHolder> occupancyDistributionDataHolder = new ArrayList<OccupancyHolder>();
    private boolean dataLoadSuccessFul = false;
    private String populationData;
    private String occupancyDistributionData;

    @PersistenceContext(unitName = "arbutus-pu")
    private Session session;

    public String getPopulationData(UserContext uc, Long facilityId) {
        // If the data load was successful return date otherwise return dummy data
        long startTime = System.nanoTime();
        loadPopulationDataByFacility(facilityId);
        if (isDataLoadSuccessFul() == true) {
            Gson gson = new Gson();
            for (FacilityHolder facHolder : populationDataHolder) {
                if (facHolder.facility.equals(facilityId)) {
                    totalPopulation = facHolder.occupancy.getValue();
                    populationData = gson.toJson(facHolder);
                    break;
                }
            }
        } else {
            // send json object if
            Gson gson = new Gson();
            FacilityHolder facHolder = new FacilityHolder();
            if (facilityId != null) {
                facHolder.facility = facilityId;
            } else {
                facHolder.facility = -1l;
            }
            facHolder.occupancy = new BasicNameValuePair("Occupancy", "0");
            facHolder.vacancy = new BasicNameValuePair("Vacancy", "0");
            populationData = gson.toJson(facHolder);
        }
        long estimatedTime = (System.nanoTime() - startTime) / 1000000; // milliseconds
        if (estimatedTime < 100) {
			log.debug("Dashboard: Population Data loading elapsed milliseconds: " + estimatedTime + " -- Super Fast !!!!!");
		} else if (estimatedTime < 200) {
			log.debug("Dashboard: Population Data loading elapsed milliseconds: " + estimatedTime + " -- Fast !!!!!");
		} else {
			log.debug("Dashboard: Population Data loading elapsed milliseconds: " + estimatedTime);
		}
        return populationData;
    }

    /**
     * Returns JSON formatted occupancy distribution data
     * {"gen": {"name": "Gender","data": [{"name": "Male","val": 65},{"name":
     * "Female","val": 75},{"name": "Pacman","val": 30},{"name":
     * "Unicorn","val": 25},{"name": "Other","val": 15}]},"seclev": {"name":
     * "Security Level","data": [{"name": "Maximum","val": 50},{"name":
     * "Medium","val": 25},{"name": "Minimum","val": 100}]}}
     */
    public String getOccupancyDistributionData(UserContext uc, Long facilityId) {
        // If the data load was successful return date otherwise return dummy data
        long startTime = System.nanoTime();
        loadOccupancyDataByFacility(facilityId);
        Gson gson = new Gson();
        for (OccupancyHolder occHolder : occupancyDistributionDataHolder) {
            if (occHolder.facility.equals(facilityId)) {
                OccupancyHolderJSON outJson = new OccupancyHolderJSON();
                outJson.gen = occHolder.gen;
                outJson.impstatus = occHolder.impstatus;
                outJson.legal = occHolder.legal;
                outJson.race = occHolder.race;
                outJson.seclev = occHolder.seclev;
                occupancyDistributionData = gson.toJson(outJson);
                break;
            }
        }
        // log.debug(String.format("occupancyDistributionData 2 %s", occupancyDistributionData));
        long estimatedTime = (System.nanoTime() - startTime) / 1000000; // milliseconds
        if (estimatedTime < 100) {
			log.debug("Dashboard: Occupancy Distribution Data loading elapsed milliseconds: " + estimatedTime + " -- Super Fast !!!!!");
		} else if (estimatedTime < 200) {
			log.debug("Dashboard: Occupancy Distribution Data loading elapsed milliseconds: " + estimatedTime + " -- Fast !!!!!");
		} else {
			log.debug("Dashboard: Occupancy Distribution Data loading elapsed milliseconds: " + estimatedTime);
		}

        return occupancyDistributionData;
    }

    public String getTotalPopulation(UserContext uc, Long facilityId) {
        // If the data load was successful return date otherwise return dummy data
        long startTime = System.nanoTime();
        loadPopulationDataByFacility(facilityId);
        if (isDataLoadSuccessFul() == true) {
            for (FacilityHolder facHolder : populationDataHolder) {
                if (facHolder.facility.equals(facilityId)) {
                    totalPopulation = facHolder.occupancy.getValue();
                    break;
                }
            }
        }
        // log.debug(String.format("totalPopulation %s", totalPopulation));
        long estimatedTime = (System.nanoTime() - startTime) / 1000000; // milliseconds
        if (estimatedTime < 100) {
			log.debug("Dashboard: Total Population Data loading elapsed milliseconds: " + estimatedTime + " -- Super Fast !!!!!");
		} else if (estimatedTime < 200) {
			log.debug("Dashboard: Total Population Data loading elapsed milliseconds: " + estimatedTime + " -- Fast !!!!!");
		} else {
			log.debug("Dashboard: Total Population Data loading elapsed milliseconds: " + estimatedTime);
		}
        return totalPopulation;
    }

    private void loadPopulationDataByFacility(Long facilityId) {
        setDataLoadSuccessFul(false);
        populationDataHolder = new ArrayList<FacilityHolder>();
        occupancyDistributionDataHolder = new ArrayList<OccupancyHolder>();

        if (facilityId == null || !BeanHelper.bExistedModule(session, FacilityEntity.class, facilityId)) {
			return;
		}

        // Add data into Holder Objects for json
        Long total = capacitiesByFacility(facilityId);
        Map<String, Long> populationMap = populationByFacility(facilityId);
        Long occupiedMale = populationMap.get("M");
        if (occupiedMale == null) {
			occupiedMale = 0L;
		}
        Long occupiedFeMale = populationMap.get("F");
        if (occupiedFeMale == null) {
			occupiedFeMale = 0L;
		}
        Long occupiedUnknown = populationMap.get("U");
        if (occupiedUnknown == null) {
			occupiedUnknown = 0L;
		}
        Long occupied = occupiedMale + occupiedFeMale + occupiedUnknown;
        Long vacancy = total - occupied;

        // Facilities
        FacilityHolder facHolder = new FacilityHolder();
        facHolder.facility = facilityId;
        facHolder.occupancy = new BasicNameValuePair("Occupancy", occupied.toString());
        facHolder.vacancy = new BasicNameValuePair("Vacancy", vacancy.toString());
        populationDataHolder.add(facHolder);

        setDataLoadSuccessFul(true);

        // Get First facility to populate the Data
        if (!populationDataHolder.isEmpty()) {
            totalPopulation = populationDataHolder.get(0).occupancy.getValue();
        }
    }

    private void loadOccupancyDataByFacility(Long facilityId) {
        setDataLoadSuccessFul(false);
        populationDataHolder = new ArrayList<FacilityHolder>();
        occupancyDistributionDataHolder = new ArrayList<OccupancyHolder>();

        if (facilityId == null || !BeanHelper.bExistedModule(session, FacilityEntity.class, facilityId)) {
			return;
		}

        // Add data into Holder Objects for json
        Long total = capacitiesByFacility(facilityId);
        Map<String, Long> populationMap = populationByFacility(facilityId);
        Long occupiedMale = populationMap.get("M");
        if (occupiedMale == null) {
			occupiedMale = 0L;
		}
        Long occupiedFeMale = populationMap.get("F");
        if (occupiedFeMale == null) {
			occupiedFeMale = 0L;
		}
        Long occupiedUnknown = populationMap.get("U");
        if (occupiedUnknown == null) {
			occupiedUnknown = 0L;
		}
        Long occupied = occupiedMale + occupiedFeMale + occupiedUnknown;
        Long vacancy = total - occupied;

        // Facilities
        FacilityHolder facHolder = new FacilityHolder();
        facHolder.facility = facilityId;
        facHolder.occupancy = new BasicNameValuePair("Occupancy", occupied.toString());
        facHolder.vacancy = new BasicNameValuePair("Vacancy", vacancy.toString());
        populationDataHolder.add(facHolder);

        // Gender
        OccupancyHolder occHolder = new OccupancyHolder();
        occHolder.facility = facilityId;
        occHolder.gen = new DataHolder();
        occHolder.gen.name = "Gender";
        occHolder.gen.data.add(new BasicNameValuePair("Male", occupiedMale.toString()));
        occHolder.gen.data.add(new BasicNameValuePair("Female", occupiedFeMale.toString()));
        occHolder.gen.data.add(new BasicNameValuePair("Unknown", occupiedUnknown.toString()));

        // Security Level
        Long max = securityByFacility(facilityId, "MAX");
        Long min = securityByFacility(facilityId, "MIN");
        Long med = securityByFacility(facilityId, "MED");
        occHolder.seclev = new DataHolder();
        occHolder.seclev.name = "Security Level";
        occHolder.seclev.data.add(new BasicNameValuePair("Maximum", max.toString()));
        occHolder.seclev.data.add(new BasicNameValuePair("Medium", med.toString()));
        occHolder.seclev.data.add(new BasicNameValuePair("Minimum", min.toString()));
        occHolder.seclev.data.add(new BasicNameValuePair("Unclassified", (new Long(occupied - min - max - med)).toString()));

        // Race -- B, W, A, I, U
        Map<String, Long> raceMap = raceByFacility(facilityId);
        Long blacks = raceMap.get("B");
        if (blacks == null) {
			blacks = 0L;
		}
        Long whites = raceMap.get("W");
        if (whites == null) {
			whites = 0L;
		}
        Long asians = raceMap.get("A");
        if (asians == null) {
			asians = 0L;
		}
        Long americanIndians = raceMap.get("I");
        if (americanIndians == null) {
			americanIndians = 0L;
		}
        Long unknown = raceMap.get("U");
        if (unknown == null) {
			unknown = 0L;
		}
        occHolder.race = new DataHolder();
        occHolder.race.name = "Race";
        occHolder.race.data.add(new BasicNameValuePair("White", whites.toString()));
        occHolder.race.data.add(new BasicNameValuePair("Black", blacks.toString()));
        occHolder.race.data.add(new BasicNameValuePair("Asian", asians.toString()));
        occHolder.race.data.add(new BasicNameValuePair("American Indian", americanIndians.toString()));
        occHolder.race.data.add(new BasicNameValuePair("Unknown", unknown.toString()));
        occHolder.race.data.add(new BasicNameValuePair("Not Declared", (new Long(occupied - whites - blacks - asians - americanIndians - unknown)).toString()));

        // Legal Sentence Status
        Long sent = legalSentenceStatus(facilityId);
        Long unsent = legalUnSentenceStatus(facilityId);
        Long unknownSentenced = occupied - sent - unsent;

        occHolder.legal = new DataHolder();
        occHolder.legal.name = "Legal";
        occHolder.legal.data.add(new BasicNameValuePair("Sentenced", sent.toString()));
        occHolder.legal.data.add(new BasicNameValuePair("Not Sentenced", unsent.toString()));
        occHolder.legal.data.add(new BasicNameValuePair("Unknown", unknownSentenced.toString()));

        // Imprisonment Status -- BOP, ICE, DOC, CNTY, CITY, USM
        Map<String, Long> improsonmentStatusMap = improsonmentStatusByFacility(facilityId);
        Long bopInmates = improsonmentStatusMap.get("BOP");
        if (bopInmates == null) {
			bopInmates = 0L;
		}
        Long iceInmates = improsonmentStatusMap.get("ICE");
        if (iceInmates == null) {
			iceInmates = 0L;
		}
        Long stateInmates = improsonmentStatusMap.get("DOC");
        if (stateInmates == null) {
			stateInmates = 0L;
		}
        Long countyInmates = improsonmentStatusMap.get("CNTY");
        if (countyInmates == null) {
			countyInmates = 0L;
		}
        Long cityInmates = improsonmentStatusMap.get("CITY");
        if (cityInmates == null) {
			cityInmates = 0L;
		}
        Long usmInmates = improsonmentStatusMap.get("USM");
        if (usmInmates == null) {
			usmInmates = 0L;
		}
        occHolder.impstatus = new DataHolder();
        occHolder.impstatus.name = "Imprisonment Status";
        occHolder.impstatus.data.add(new BasicNameValuePair("Bureau of Prisons", bopInmates.toString()));
        occHolder.impstatus.data.add(new BasicNameValuePair("Immigration", iceInmates.toString()));
        occHolder.impstatus.data.add(new BasicNameValuePair("US Marshals", usmInmates.toString()));
        occHolder.impstatus.data.add(new BasicNameValuePair("State", stateInmates.toString()));
        occHolder.impstatus.data.add(new BasicNameValuePair("County", countyInmates.toString()));
        occHolder.impstatus.data.add(new BasicNameValuePair("City", cityInmates.toString()));
        occHolder.impstatus.data.add(
                new BasicNameValuePair("Not Known", (new Long(occupied - bopInmates - iceInmates - usmInmates - stateInmates - countyInmates - cityInmates)).toString()));

        occupancyDistributionDataHolder.add(occHolder);

        setDataLoadSuccessFul(true);

        // Get First facility to populate the Data
        if (!populationDataHolder.isEmpty()) {
            totalPopulation = populationDataHolder.get(0).occupancy.getValue();
        }
    }

    public boolean isDataLoadSuccessFul() {
        return this.dataLoadSuccessFul;
    }

    public void setDataLoadSuccessFul(boolean dataLoadSuccessFul) {
        this.dataLoadSuccessFul = dataLoadSuccessFul;
    }

    private Long capacitiesByFacility(Long facilityId) {
        Criteria filCriteria = session.createCriteria(InternalLocationEntity.class);
        filCriteria.add(Restrictions.eq("facilityId", facilityId));
        filCriteria.createCriteria("hierarchy", "h");
        filCriteria.add(Restrictions.eq("h.usageCategory", UsageCategory.HOU.name()));
        filCriteria.createCriteria("capacities", "c");
        filCriteria.setProjection(Projections.projectionList().add(Projections.sum("c.optimalCapacity")).add(Projections.sum("c.overflowCapacity")));
        filCriteria.setMaxResults(1);
        filCriteria.setCacheable(true);
        Object[] row = (Object[]) filCriteria.uniqueResult();
        if (row == null) {
			return 0L;
		}

        Long optimalCapacity = (Long) row[0];
        if (optimalCapacity == null) {
			optimalCapacity = 0L;
		}
        Long overflowCapacity = (Long) row[1];
        if (overflowCapacity == null) {
			overflowCapacity = 0L;
		}

        return optimalCapacity + overflowCapacity;
    }

    @SuppressWarnings("rawtypes")
    private Map<String, Long> populationByFacility(Long facilityId) {
        Map<String, Long> map = new HashMap<>();
        Criteria piCriteria = session.createCriteria(PersonIdentityEntity.class);
        piCriteria.setProjection(Projections.projectionList().add(Projections.groupProperty("sex")).add(Projections.countDistinct("personIdentityId")));
        Set<Set<Long>> ss = inHousingPersonIdentitySet(facilityId);
        if (ss.isEmpty() || ((Set) (ss.toArray()[0])).isEmpty()) {
            return map;
        }

        Disjunction disjunction = Restrictions.disjunction();
        for (Set<Long> s : ss) {
            disjunction.add(Restrictions.in("personIdentityId", s));
        }
        piCriteria.add(disjunction);
        piCriteria.setCacheable(true);
        Iterator it = piCriteria.list().iterator();
        while (it.hasNext()) {
            Object[] row = (Object[]) it.next();
            String gender = (String) row[0];
            if (gender == null) {
				gender = "U";
			}
            Long count = (Long) row[1];
            if (count == null) {
				count = 0L;
			}
            map.put(gender, count);
        }
        return map;
    }

    @SuppressWarnings("rawtypes")
    private Map<String, Long> raceByFacility(Long facilityId) {
        // Race -- B, W, A, I, U
        Map<String, Long> map = new HashMap<>();
        Set<Set<Long>> ss = inHousingPersonSet(facilityId);
        if (ss.isEmpty() || ((Set) (ss.toArray()[0])).isEmpty()) {
            return map;
        }

        Criteria person = session.createCriteria(PersonEntity.class);
        Disjunction disjunction = Restrictions.disjunction();
        for (Set<Long> s : ss) {
            disjunction.add(Restrictions.in("personId", s));
        }
        person.add(disjunction);
        person.createCriteria("physical", "phy");
        person.setProjection(Projections.projectionList().add(Projections.groupProperty("phy.race")).add(Projections.countDistinct("personId")));
        person.setCacheable(true);
        Iterator it = person.list().iterator();
        while (it.hasNext()) {
            Object[] row = (Object[]) it.next();
            String race = (String) row[0];
            if (race == null || race.trim().isEmpty()) {
				continue;
			}
            Long count = (Long) row[1];
            if (count == null) {
				count = 0L;
			}
            map.put(race, count);
        }
        return map;
    }

    private Map<String, Long> improsonmentStatusByFacility(Long facilityId) {
        // Imprisonment Status -- BOP, ICE, DOC, CNTY, CITY, USM
        Map<String, Long> map = new HashMap<>();
        Set<Set<Long>> ss = this.inHousingSupervisionSet(facilityId);
        if (ss.isEmpty() || ((Set<?>) (ss.toArray()[0])).isEmpty()) {
            return map;
        }
        Criteria c = session.createCriteria(SupervisionEntity.class);
        Disjunction disjunction = Restrictions.disjunction();
        for (Set<Long> s : ss) {
            disjunction.add(Restrictions.in("supervisionIdentification", s));
        }
        c.add(disjunction);
        c.add(Restrictions.eq("facilityId", facilityId));
        c.createCriteria("imprisonmentStatus", "status");
        c.setProjection(
                Projections.projectionList().add(Projections.groupProperty("status.imprisonmentStatus")).add(Projections.countDistinct("status.imprisonmentStatus")));
        c.setCacheable(true);
        @SuppressWarnings("rawtypes") Iterator it = c.list().iterator();
        while (it.hasNext()) {
            Object[] row = (Object[]) it.next();
            String status = (String) row[0];
            if (status == null || status.trim().isEmpty()) {
				continue;
			}
            Long count = (Long) row[1];
            if (count == null) {
				count = 0L;
			}
            map.put(status, count);
        }
        return map;
    }

    @SuppressWarnings("rawtypes")
    private Long securityByFacility(Long facilityId, String securityLevel) {
        Criteria ia = session.createCriteria(AssessmentEntity.class);
        ia.add(Restrictions.eq("calculatedResult", securityLevel));
        ia.setProjection(Projections.property("supervisionId"));
        ia.setCacheable(true);
        @SuppressWarnings("unchecked") Set<Set<Long>> iaSupervisionIdSet = this.splitSet(ia.list());
        if (iaSupervisionIdSet.isEmpty() || ((Set) (iaSupervisionIdSet.toArray()[0])).isEmpty()) {
			return 0L;
		}

        Set<Set<Long>> inHousingSupervisionIdSet = inHousingSupervisionSet(facilityId);
        if (inHousingSupervisionIdSet.isEmpty() || ((Set) (inHousingSupervisionIdSet.toArray()[0])).isEmpty()) {
			return 0L;
		}

        Criteria c = session.createCriteria(SupervisionEntity.class);
        c.setProjection(Projections.countDistinct("supervisionIdentification"));
        c.add(Restrictions.eq("facilityId", facilityId));

        Disjunction hsDisjunction = Restrictions.disjunction();
        for (Set<Long> s : inHousingSupervisionIdSet) {
            hsDisjunction.add(Restrictions.in("supervisionIdentification", s));
        }
        c.add(hsDisjunction);

        Disjunction iaDisjunction = Restrictions.disjunction();
        for (Set<Long> s : iaSupervisionIdSet) {
            iaDisjunction.add(Restrictions.in("supervisionIdentification", s));
        }
        c.add(iaDisjunction);
        c.setMaxResults(1);
        c.setCacheable(true);
        Long count = (Long) c.uniqueResult();
        if (count == null) {
			count = 0L;
		}
        return count;
    }

    @SuppressWarnings("rawtypes")
    private Long legalSentenceStatus(Long facilityId) {
        Set<Set<Long>> cc = sentencedSupervisionIdSet(facilityId);
        if (cc.isEmpty() || ((Set) (cc.toArray()[0])).isEmpty()) {
			return 0L;
		}
        Disjunction cDisjunction = Restrictions.disjunction();
        for (Set<Long> s : cc) {
            cDisjunction.add(Restrictions.in("supervisionIdentification", s));
        }

        Criteria c = session.createCriteria(SupervisionEntity.class);
        c.setProjection(Projections.countDistinct("supervisionIdentification"));
        c.setMaxResults(1);
        c.add(Restrictions.eq("facilityId", facilityId));
        Set<Set<Long>> ss = this.inHousingSupervisionSet(facilityId);
        if (ss.isEmpty() || ((Set) (ss.toArray()[0])).isEmpty()) {
			return 0L;
		}

        Disjunction sDisjunction = Restrictions.disjunction();
        for (Set<Long> s : ss) {
            sDisjunction.add(Restrictions.in("supervisionIdentification", s));
        }
        c.add(sDisjunction);
        c.add(cDisjunction);
        c.setCacheable(true);
        Long count = (Long) c.uniqueResult();
        if (count == null) {
			count = 0L;
		}
        return count;
    }

    @SuppressWarnings("rawtypes")
    private Long legalUnSentenceStatus(Long facilityId) {
        Criteria ci = session.createCriteria(CaseInfoEntity.class);
        ci.add(Restrictions.eq("active", Boolean.TRUE));
        ci.add(Restrictions.or(Restrictions.isNull("sentenceStatus"), Restrictions.ne("sentenceStatus", "SENTENCED")));
        Set<Set<Long>> sss = sentencedSupervisionIdSet(facilityId);
        if (!sss.isEmpty() && !((Set) (sss.toArray()[0])).isEmpty()) {
            Conjunction conjunction = Restrictions.conjunction();
            for (Set<Long> s : sss) {
                conjunction.add(Restrictions.not(Restrictions.in("supervisionId", s)));
            }
            ci.add(conjunction);
        }

        ci.setProjection(Projections.property("supervisionId"));
        ci.setCacheable(true);
        @SuppressWarnings("unchecked") Set<Set<Long>> cc = splitSet(ci.list());
        if (cc.isEmpty() || ((Set) (cc.toArray()[0])).isEmpty()) {
			return 0L;
		}
        Disjunction cDisjunction = Restrictions.disjunction();
        for (Set<Long> s : cc) {
            cDisjunction.add(Restrictions.in("supervisionIdentification", s));
        }

        Criteria c = session.createCriteria(SupervisionEntity.class);
        c.setProjection(Projections.countDistinct("supervisionIdentification"));
        c.setMaxResults(1);
        c.add(Restrictions.eq("facilityId", facilityId));
        Set<Set<Long>> ss = this.inHousingSupervisionSet(facilityId);
        if (ss.isEmpty() || ((Set) (ss.toArray()[0])).isEmpty()) {
			return 0L;
		}

        Disjunction sDisjunction = Restrictions.disjunction();
        for (Set<Long> s : ss) {
            sDisjunction.add(Restrictions.in("supervisionIdentification", s));
        }
        c.add(sDisjunction);
        c.add(cDisjunction);
        c.setCacheable(true);
        Long count = (Long) c.uniqueResult();
        if (count == null) {
			count = 0L;
		}
        return count;
    }

    private Set<Set<Long>> sentencedSupervisionIdSet(Long facility) {
        Criteria ci = session.createCriteria(CaseInfoEntity.class);
        ci.add(Restrictions.eq("active", Boolean.TRUE));
        ci.add(Restrictions.eq("sentenceStatus", "SENTENCED"));
        ci.setProjection(Projections.property("supervisionId"));
        ci.setCacheable(true);
        @SuppressWarnings("unchecked") Set<Set<Long>> cc = splitSet(ci.list());
        return cc;
    }

    @SuppressWarnings("rawtypes")
    private Set<Set<Long>> inHousingPersonIdentitySet(Long facilityId) {
        Criteria c = session.createCriteria(SupervisionEntity.class);
        c.add(Restrictions.eq("facilityId", facilityId));
        Set<Set<Long>> ss = inHousingSupervisionSet(facilityId);
        if (ss.isEmpty() || ((Set) (ss.toArray()[0])).isEmpty()) {
			return new HashSet<>();
		}

        Disjunction disjunction = Restrictions.disjunction();
        for (Set<Long> s : ss) {
            disjunction.add(Restrictions.in("supervisionIdentification", s));
        }
        c.add(disjunction);
        c.setProjection(Projections.property("personIdentityId"));
        c.setCacheable(true);
        @SuppressWarnings("unchecked") List<Long> ids = c.list();
        Set<Set<Long>> ret = splitSet(ids);
        return ret;
    }

    @SuppressWarnings("rawtypes")
    private Set<Set<Long>> inHousingPersonSet(Long facilityId) {
        Criteria c = session.createCriteria(SupervisionEntity.class);
        c.add(Restrictions.eq("facilityId", facilityId));
        Set<Set<Long>> ss = inHousingSupervisionSet(facilityId);
        if (ss.isEmpty() || ((Set) (ss.toArray()[0])).isEmpty()) {
			return new HashSet<>();
		}

        Disjunction disjunction = Restrictions.disjunction();
        for (Set<Long> s : ss) {
            disjunction.add(Restrictions.in("supervisionIdentification", s));
        }
        c.add(disjunction);
        c.setProjection(Projections.property("personId"));
        c.setCacheable(true);
        @SuppressWarnings("unchecked") List<Long> ids = c.list();
        Set<Set<Long>> ret = splitSet(ids);
        return ret;
    }

    private Set<Set<Long>> inHousingSupervisionSet(Long facilityId) {
        Criteria hba = session.createCriteria(HousingBedMgmtActivityEntity.class);
        hba.add(Restrictions.eq("facilityId", facilityId));
        hba.createCriteria("housingAssignment", "ha");
        hba.add(Restrictions.eq("ha.currentHousingFlag", Boolean.TRUE));
        hba.add(Restrictions.eq("activityState", "ASSGN"));
        hba.setProjection(Projections.property("supervisionId"));
        hba.setCacheable(true);
        @SuppressWarnings("unchecked") List<Long> supervisionIDs = hba.list();
        Set<Set<Long>> ret = splitSet(supervisionIDs);
        return ret;
    }

    private Set<Set<Long>> splitSet(List<Long> ids) {
        Set<Set<Long>> ret = new HashSet<>();
        int maxNum = 998;
        if (ids.size() < maxNum) {
            ret.add(new HashSet<Long>(ids));
            return ret;
        }
        int i = 0;
        Set<Long> s = new HashSet<>();
        for (Long id : ids) {
            if (i < maxNum) {
                s.add(Long.valueOf(id));
                i++;
            } else {
                s.add(id);
                ret.add(new HashSet<Long>(s));
                s.clear();
                i = 0;
            }
        }
        if (!s.isEmpty()) {
            ret.add(s);
        }
        return ret;
    }

    // JSON Holder for facility population data
    public class FacilityHolder implements Serializable {
        private static final long serialVersionUID = 4674609168566841969L;

        public Long facility;

        public BasicNameValuePair vacancy;
        public BasicNameValuePair occupancy;
    }

    // JSON Holder for facilty occupancy data
    public class OccupancyHolder implements Serializable {
        private static final long serialVersionUID = 8457806295392595940L;

        public Long facility;

        public DataHolder gen;
        public DataHolder seclev;
        public DataHolder race;
        public DataHolder legal;
        public DataHolder impstatus;
    }

    public class OccupancyHolderJSON implements Serializable {
        private static final long serialVersionUID = -546501342699806161L;

        public DataHolder gen;
        public DataHolder seclev;
        public DataHolder race;
        public DataHolder legal;
        public DataHolder impstatus;
    }

    // JSON Holder for OccupancyHolder objects
    public class DataHolder implements Serializable {
        private static final long serialVersionUID = 1903502097491069973L;

        public String name;
        public List<BasicNameValuePair> data = new ArrayList<BasicNameValuePair>();

    }
}