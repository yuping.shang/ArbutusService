package syscon.arbutus.product.services.dashboard.contract.interfaces;

import syscon.arbutus.product.security.UserContext;

/**
 * Business contract interface for Dashboard service.
 * <p><b>Purpose</b>
 * The Dashboard service will allow the user to get information about the Population and Occupancy
 * in different facilities. It gives data in the jason format.
 * the occupancy distribution data returns the race, imprisonment status etc for the
 * </p>
 * The following is an example snippet of Java code to demonstrate how this service operation needs to be invoked by a consumer Java application.  <br/>
 * <pre>
 *  <li>public void getCount() {</li>
 *  <li>    Properties p = new Properties();</li>
 *  <li>    p.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");</li>
 *  <li>    p.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");</li>
 *  <li>    p.put("java.naming.provider.url", "hostIpaddress:port"); //HA-JNDI binding server and port</li>
 *  <li>    Context ctx = new InitialContext(p);</li>
 *  <li>    DashboardService dService = (DashboardService) ctx.lookup("DashboardService");</li>
 *  <li>    UserContext uc = new UserContext();</li>
 *  <li>    Long facilityId = 100L;  //change to a real facility id</li>
 *  <li>    String populationData = dService.getPopulationData(uc,facilityId);</li>
 *  <li>}</li>
 *
 * @author irizvi
 */
public interface DashboardService {

    /**
     * Returns JSON formatted Population Data,
     * Example:
     * {"facility":123,"vacancy": {"name": "Vacancy","val": 25}, "occupancy": {"name": "Occupancy","val": 175}}
     *
     * @param uc         - UserContext - required
     * @param facilityId - Facility Id of population - required
     * @return JSON String of Population Data
     */
    public String getPopulationData(UserContext uc, Long facilityId);

    /**
     * Returns JSON formatted Occupancy Distribution Data
     * Example:
     * {"gen": {"name": "Gender","data": [{"name": "Male","val": 65},{"name": "Female","val": 75},{"name": "Other","val": 15}]},"seclev": {"name": "Security Level","data": [{"name": "Maximum","val": 50},{"name": "Medium","val": 25},{"name": "Minimum","val": 100}]}}
     *
     * @param uc         - UserContext - required
     * @param facilityId - Facility Id of population - required
     * @return JSON String of Occupancy Distribution Data
     */
    public String getOccupancyDistributionData(UserContext uc, Long facilityId);

    /**
     * Returns the Total Population Count for a given Facility
     *
     * @param uc         - UserContext - required
     * @param facilityId - Facility Id of population - required
     * @return JSON String of Occupancy Distribution Data
     */
    public String getTotalPopulation(UserContext uc, Long facilityId);

}
