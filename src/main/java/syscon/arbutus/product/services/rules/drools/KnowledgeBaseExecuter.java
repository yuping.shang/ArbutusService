package syscon.arbutus.product.services.rules.drools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;



import org.kie.api.runtime.KieSession;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Drools KnowledgeBase executer.
 * Used to insert objects into process memory and fire rules against them.
 *
 * @author wmadruga
 * @version 1.0
 * @since October 02, 2013.
 */
public class KnowledgeBaseExecuter {

    private static Logger logger = LoggerFactory.getLogger(KnowledgeBaseExecuter.class);

    /**
     * Processes loaded rules against specified objects in a stateful session.
     *
     * @param ksession      statefull session for drools
     * @param globalOutput, Object, Optional
     * @param objects,      List of objects to be processed
     * @return
     */
    public static Collection<? extends Object> executeStateful(KieSession ksession, Object globalOutput, List<Object> objects) {

        Collection<? extends Object> results = null;

        try {
            ksession.setGlobal("logger", logger);

            if (globalOutput != null) {
                ksession.setGlobal("globalOutput", globalOutput);
            }

            //objects to be processed against the rules.
            if (objects != null) {
                for (Object obj : objects) {
                    if (obj != null) {
                        ksession.insert(obj);
                    }
                }
            }

            ksession.fireAllRules();

            //returning result.
            results = ksession.getObjects();

        } catch (Throwable ex) {
            logger.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
        return results;
    }

}