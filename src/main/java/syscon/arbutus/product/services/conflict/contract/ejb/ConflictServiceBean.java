package syscon.arbutus.product.services.conflict.contract.ejb;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Session;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.DayOfWeekEnum;
import syscon.arbutus.product.services.activity.contract.dto.RecurrencePatternType;
import syscon.arbutus.product.services.conflict.contract.dto.NonAssociationType;
import syscon.arbutus.product.services.conflict.contract.interfaces.ConflictService;
import syscon.arbutus.product.services.movement.contract.dto.ScheduleConflict;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;

/**
 * Conflict Service for checking Non-Association, Scheduling, Capacity, Housing
 *
 * @author YShang
 * @since March 06, 2015
 */
@Stateless(mappedName = "ConflictService", description = "Conflict Service for checking Non-Association, Scheduling, Capacity, Housing")
@Remote(ConflictService.class)
public class ConflictServiceBean implements ConflictService {

    public static final String PROGRAM_ASSIGNMENT_ALLOCATED = "ALLOCATED";
    public static final String PROGRAM_ASSIGNMENT_APPROVED = "APPROVED";
    public static final String PROGRAM_ASSIGNMENT_REFERRED = "REFERRED";
    private static Logger log = LoggerFactory.getLogger(ConflictServiceBean.class);
    @Resource
    SessionContext context;
    @PersistenceContext(unitName = "arbutus-pu")
    Session session;
    private ConflictHandler conflictHandler;

    @EJB
    private SupervisionService supervisionService;

    @PostConstruct
    public void init() {
        conflictHandler = new ConflictHandler(context, session, supervisionService);
    }

    @Override
    public List<NonAssociationType> checkNonAssociationConflict(UserContext uc, @NotNull Long primarySupervisionId, @NotNull Long facilityId,
            Long facilityInternalLocationId, @NotNull LocalDate sessionDate, @NotNull LocalTime startTime, @NotNull LocalTime endTime,
            @NotNull NonAssociationType.EventType event, Set<DayOfWeekEnum> daysOfWeek) {

        return checkNonAssociationConflict(uc, primarySupervisionId, null, facilityId, facilityInternalLocationId, sessionDate, startTime, endTime, event, null, null,
                daysOfWeek);
    }

    @Override
    public List<NonAssociationType> checkNonAssociationConflict(UserContext uc, @NotNull Long primarySupervisionId, Set<Long> supervisionIds, @NotNull Long facilityId,
            Long facilityInternalLocationId, @NotNull LocalDate sessionDate, @NotNull LocalTime startTime, LocalTime endTime, @NotNull NonAssociationType.EventType event,
            String referenceSet, String referenceCode, Set<DayOfWeekEnum> daysOfWeek) {
        long start = System.nanoTime();
        List<NonAssociationType> nonAssociationTypeList = conflictHandler.checkNonAssociationConflictByOnceAndDailyPattern(uc, primarySupervisionId, supervisionIds,
                facilityId, facilityInternalLocationId, sessionDate, startTime, endTime, event, referenceSet, referenceCode, daysOfWeek);
        long estimatedTime = (System.nanoTime() - start) / 1000000; // milliseconds
        log.debug("checkNonAssociationConflict: elapsed milliseconds: " + estimatedTime);
        return nonAssociationTypeList;
    }

    //    @Override -- Not going to be exposed to interface
    public List<NonAssociationType> checkNonAssociationConflictByPattern(UserContext uc, @NotNull Long primarySupervisionId, Set<Long> supervisionIds,
            @NotNull Long facilityId, @NotNull Long facilityInternalLocationId, @NotNull LocalDate sessionDate, @NotNull LocalTime startTime, @NotNull LocalTime endTime,
            @NotNull NonAssociationType.EventType event, String referenceSet, String referenceCode, RecurrencePatternType recurrencePattern) {
        long start = System.nanoTime();

        List<NonAssociationType> nonAssociationTypeList = conflictHandler.checkNonAssociationConflictByPattern(uc, primarySupervisionId, supervisionIds, facilityId,
                facilityInternalLocationId, null, sessionDate, startTime, endTime, event, referenceSet, referenceCode, recurrencePattern);

        long estimatedTime = (System.nanoTime() - start) / 1000000; // milliseconds
        log.debug("checkNonAssociationConflict: elapsed milliseconds: " + estimatedTime);
        return nonAssociationTypeList;
    }

    @Override
    public List<NonAssociationType> checkNonAssociationConflictForHousingMovement(UserContext uc, @NotNull Long primarySupervisionId,
            @NotNull Long facilityInternalLocationId, LocalDateTime startDateTime, LocalDateTime endDateTime) {
        return checkNonAssociationConflictForHousingMovement(uc, primarySupervisionId, null, facilityInternalLocationId, startDateTime, endDateTime);
    }

    @Override
    public List<NonAssociationType> checkNonAssociationConflictForHousingMovement(UserContext uc, @NotNull Long primarySupervisionId, Set<Long> supervisionIds,
            @NotNull Long facilityInternalLocationId, LocalDateTime startDateTime, LocalDateTime endDateTime) {
        long start = System.nanoTime();
        List<NonAssociationType> nonAssociationTypeList = conflictHandler.checkNonAssociationConflictForHousingMovement(uc, primarySupervisionId, supervisionIds,
                facilityInternalLocationId, startDateTime, endDateTime);
        long estimatedTime = (System.nanoTime() - start) / 1000000; // milliseconds
        log.debug("checkScheduleConflict: elapsed milliseconds: " + estimatedTime);
        return nonAssociationTypeList;
    }

    @Override
    public List<ScheduleConflict> checkScheduleConflict(UserContext uc, Long supervisionId, List<ScheduleConflict.EventType> checkTypes,
            @NotNull LocalDateTime startDateTime, @NotNull LocalDateTime endDateTime) {
        long start = System.nanoTime();
        List<ScheduleConflict> scheduleConflictList = conflictHandler.checkScheduleConflict(uc, supervisionId, checkTypes, startDateTime, endDateTime);
        long estimatedTime = (System.nanoTime() - start) / 1000000; // milliseconds
        log.debug("checkScheduleConflict: elapsed milliseconds: " + estimatedTime);
        return scheduleConflictList;
    }
}