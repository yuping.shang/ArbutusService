package syscon.arbutus.product.services.conflict.contract.interfaces;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.DayOfWeekEnum;
import syscon.arbutus.product.services.activity.contract.dto.RecurrencePatternType;
import syscon.arbutus.product.services.conflict.contract.dto.NonAssociationType;
import syscon.arbutus.product.services.conflict.contract.dto.NonAssociationType.EventType;
import syscon.arbutus.product.services.movement.contract.dto.ScheduleConflict;

/**
 * Conflict Service for checking Non-Association, Scheduling, Capacity, Housing
 * The non-associations conflict check functionality is used to check whether a non-association conflict will occur
 * upon scheduling or moving a person to a location, where a non-associate of that person
 * (who could be another inmate, a visitor, a lawyer, a victim, etc.) is
 * or will be at the same location during the same time period or with an overlap.
 * Non-Associations must be validated during any instance where two persons are scheduled to attend the same location,
 * or are about to be physically moved to the same location, or one is located at the location
 * while the other one is being moved/scheduled into that location
 * (if the location in question has been set up to check for non-associations).
 * Non-associations checks for the following two reasons:
 * <li>The two persons of interest (e.g. two inmates) are in a non-association relationship;</li>
 * <li>The two persons of interest are members of gangs which are have an active enemy relationship against each other.</li>
 * Scope:
 * Check for non-association conflict in the following modules:
 * <li>Scheduling Movements (Internal / External), including Transfers;</li>
 * <li>Actualize Movements (Internal / External), including Transfers;</li>
 * <li>Housing assignment;</li>
 * <li>Housing Change Requests;</li>
 * <li>Program placement;</li>
 * <li>Scheduling Appointments;</li>
 * <li>Visitation (Scheduling Visits and Adding Visitors);</li>
 * <li>Creating Disciplinary hearing event.</li>
 *
 * @author YShang
 * @since March 06, 2015
 */
public interface ConflictService {
    /**
     * Check Non-Association Conflict for one given supervision and a set of supervisions and facility internal locations in a period of times with Once and Daily Recurrence
     *
     * @param uc                         UserContext - Required
     * @param primarySupervisionId       Long - Required. A supervisionId
     * @param facilityId                 Long - Required. Facility Id
     * @param facilityInternalLocationId Long - Optional for Court or TAP Movement; Required otherwise. Facility internal location id
     * @param sessionDate                LocalDate - Required. Session Date
     * @param startTime                  LocalTime - Required. Start Time
     * @param endTime                    LocalTime - Optional for Internal Movement, Transfers and Hearing; Required otherwise. End Time
     * @param event                      EventType - Required. The event of the invocation module(the module calling from, i.e., Program, CRT Movement
     * @param daysOfWeek                 Set&lt;DayOfWeekEnum> - Optional. For recurrence pattern, a set of day of week, Mon, TUE, WED, THU, FRI, SAT, SUN.
     * @return List&lt;NonAssociationType> List&lt;NonAssociationType>: A list of NonAssocationType
     */
    public List<NonAssociationType> checkNonAssociationConflict(
            UserContext uc, @NotNull Long primarySupervisionId, @NotNull Long facilityId,
            Long facilityInternalLocationId, @NotNull LocalDate sessionDate, @NotNull LocalTime startTime, LocalTime endTime,
            @NotNull EventType event, Set<DayOfWeekEnum> daysOfWeek);

    /**
     * Check Non-Association Conflict for one given supervision and a set of supervisions and facility internal locations in a period of times with Once and Daily Recurrence
     *
     * @param uc                         UserContext - Required
     * @param primarySupervisionId       Long - Required. A supervisionId
     * @param supervisionIds             Set&lt;Long> - Optional. Supervisions join the same event with primary supervision -- for NAS Bulk
     * @param facilityId                 Long - Required. Facility Id
     * @param facilityInternalLocationId Long - Optional for Court or TAP Movement; Required otherwise. Facility internal location id
     * @param sessionDate                LocalDate - Required. Session Date
     * @param startTime                  LocalTime - Required. Start Time
     * @param endTime                    LocalTime - Optional for Internal Movement, Transfers and Hearing; Required otherwise. End Time
     * @param event                      EventType - Required. The event of the invocation module(the module calling from, i.e., Program, CRT Movement
     * @param referenceSet               String - Optional. Reference Set of the event
     * @param referenceCode              String - Optional. Reference Code of the event
     * @param daysOfWeek                 Set&lt;DayOfWeekEnum> - Optional. For recurrence pattern, a set of day of week, Mon, TUE, WED, THU, FRI, SAT, SUN.
     * @return List&lt;NonAssociationType> List&lt;NonAssociationType>: A list of NonAssocationType
     */
    public List<NonAssociationType> checkNonAssociationConflict(
            UserContext uc, @NotNull Long primarySupervisionId, Set<Long> supervisionIds, @NotNull Long facilityId,
            Long facilityInternalLocationId, @NotNull LocalDate sessionDate, @NotNull LocalTime startTime, LocalTime endTime,
            @NotNull EventType event, String referenceSet, String referenceCode, Set<DayOfWeekEnum> daysOfWeek);

    /**
     * For a given offender and location, check non-association conflicts against housing movements (requests, updates, reservations, and assignment of housing location)
     * and return all conflicts found.
     *
     * @param uc                            UserContext - Required
     * @param primarySupervisionId          Long - Required. A supervisionId
     * @param facilityInternalLocationId    Long - Required. For Court or TAP Movement; Required otherwise. Facility internal location id
     * @param startDateTime                 LocalDateTime - Optional
     * @param endDateTime                   LocalDateTime - Optional
     * @return
     */
    public List<NonAssociationType> checkNonAssociationConflictForHousingMovement(
            UserContext uc, @NotNull Long primarySupervisionId,
            @NotNull Long facilityInternalLocationId, LocalDateTime startDateTime, LocalDateTime endDateTime);

    /**
     * For a given offender and location, check non-association conflicts against housing movements (requests, updates, reservations, and assignment of housing location)
     * and return all conflicts found.
     *
     * @param uc                            UserContext - Required
     * @param primarySupervisionId          Long - Required. A supervisionId
     * @param supervisionIds                Set&lt;Long> - Optional. Supervisions join the same event with primary supervision -- for NAS Bulk
     * @param facilityInternalLocationId    Long - Required. For Court or TAP Movement; Required otherwise. Facility internal location id
     * @param startDateTime                 LocalDateTime - Optional
     * @param endDateTime                   LocalDateTime - Optional
     * @return
     */
    public List<NonAssociationType> checkNonAssociationConflictForHousingMovement(
            UserContext uc, @NotNull Long primarySupervisionId, Set<Long> supervisionIds,
            @NotNull Long facilityInternalLocationId, LocalDateTime startDateTime, LocalDateTime endDateTime);

    /**
     * Check Schedule Conflict for one given supervision in a session -- date and the start time and end time at the date
     *
     * @param uc            UserContext - Required
     * @param supervisionId Long - Required. A supersionId
     * @param checkTypes    List&lt;ScheduleConflict.EventType> - Optional. Appiontment, Movement, Program, Visitation, ...
     * @param startDateTime LocalDateTime - Required. Start date and time
     * @param endDateTime   LocalDateTime - Required. End date and time
     * @return List&lt;ScheduleConflict> A list of ScheduleConflict
     */
    public List<ScheduleConflict> checkScheduleConflict(UserContext uc, Long supervisionId, List<ScheduleConflict.EventType> checkTypes,
                                                        @NotNull LocalDateTime startDateTime, @NotNull LocalDateTime endDateTime);
}