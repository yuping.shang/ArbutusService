package syscon.arbutus.product.services.conflict.contract.dto;


import javax.ejb.SessionContext;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.realization.persistence.ScheduleEntity;
import syscon.arbutus.product.services.supervision.realization.persistence.SupervisionEntity;

/**
 * Created by ian.rivers on 25/06/2015.
 */
public class ConflictQueryDataHolder {

    public UserContext uc;
    public Session session;
    public SessionContext context;

    public Map<Long, SupervisionEntity> supervisionEntityMap;
    public Map<Long, ScheduleEntity> scheduleEntityMap;
    public Long facilityId;
    public Set<Long> facilityInternalLocationIdSet;
    public Long facilityInternalLocationId;
    public LocalDate sessionDate;
    public LocalTime startTime;
    public LocalTime endTime;
    public SupervisionEntity primarySupervisionEntity;
    public Map<Long, Long> childParentMap;
    public Map<Long, SupervisionEntity> stgSupervisionEntityMap;

    @Override
    public String toString() {
        return "ConflictQueryDataHolder{" +
                "supervisionEntityMap=" + supervisionEntityMap +
                ", scheduleEntityMap=" + scheduleEntityMap +
                ", facilityId=" + facilityId +
                ", facilityInternalLocationIdSet=" + facilityInternalLocationIdSet +
                ", facilityInternalLocationId=" + facilityInternalLocationId +
                ", sessionDate=" + sessionDate +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", primarySupervisionEntity=" + primarySupervisionEntity +
                ", childParentMap=" + childParentMap +
                ", stgSupervisionEntityMap=" + stgSupervisionEntityMap +
                '}';
    }
}
