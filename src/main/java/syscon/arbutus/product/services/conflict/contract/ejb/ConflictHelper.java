package syscon.arbutus.product.services.conflict.contract.ejb;

import javax.ejb.SessionContext;
import java.util.*;
import java.util.regex.Pattern;

import org.hibernate.*;
import org.hibernate.criterion.*;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.*;
import syscon.arbutus.product.services.activity.contract.util.ActivityQueryUtil;
import syscon.arbutus.product.services.activity.realization.persistence.ActivityEntity;
import syscon.arbutus.product.services.activity.realization.persistence.ScheduleEntity;
import syscon.arbutus.product.services.appointment.realization.persistence.AppointmentEntity;
import syscon.arbutus.product.services.conflict.contract.dto.ConflictQueryDataHolder;
import syscon.arbutus.product.services.conflict.contract.dto.NonAssociationType;
import syscon.arbutus.product.services.core.common.adapters.ReferenceDataServiceAdapter;
import syscon.arbutus.product.services.core.contract.dto.CodeType;
import syscon.arbutus.product.services.core.util.DateUtil;
import syscon.arbutus.product.services.core.util.QueryUtil;
import syscon.arbutus.product.services.discipline.realization.persistence.OffenseInCustodyHearingEntity;
import syscon.arbutus.product.services.facility.realization.persistence.FacilityEntity;
import syscon.arbutus.product.services.facilityinternallocation.realization.persistence.InternalLocationEntity;
import syscon.arbutus.product.services.housing.contract.dto.housingbedmanagementactivity.HousingActivityState;
import syscon.arbutus.product.services.housing.realization.persistence.HousingBedMgmtActivityEntity;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType;
import syscon.arbutus.product.services.movement.contract.dto.ScheduleConflict;
import syscon.arbutus.product.services.movement.contract.ejb.MovementServiceBean;
import syscon.arbutus.product.services.movement.realization.persistence.*;
import syscon.arbutus.product.services.person.realization.persistence.personidentity.PersonIdentityEntity;
import syscon.arbutus.product.services.person.realization.persistence.personrelationship.PersonNonAssociationEntity;
import syscon.arbutus.product.services.program.realization.persistence.ProgramAssignmentEntity;
import syscon.arbutus.product.services.program.realization.persistence.ProgramOfferingEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.securitythreatgroup.contract.dto.STGRelationshipTypeEnum;
import syscon.arbutus.product.services.securitythreatgroup.realization.persistence.AffiliationEntity;
import syscon.arbutus.product.services.securitythreatgroup.realization.persistence.STGRelationshipEntity;
import syscon.arbutus.product.services.securitythreatgroup.realization.persistence.SecurityThreatGroupEntity;
import syscon.arbutus.product.services.supervision.realization.persistence.SupervisionEntity;
import syscon.arbutus.product.services.visitation.realization.persistence.VisitEntity;

public class ConflictHelper {

    public static final String PROGRAM_ASSIGNMENT_ALLOCATED = "ALLOCATED";
    public static final String PROGRAM_ASSIGNMENT_APPROVED = "APPROVED";
    public static final String PROGRAM_ASSIGNMENT_REFERRED = "REFERRED";
    public static final String NON_ASSOCIATION_STG = "STG";
    public static final String DELETED = "DELETED";

    private static Logger log = LoggerFactory.getLogger(ConflictHelper.class);

    public static Set<Long> getLocationsToBeChecked(UserContext uc, SessionContext context, Session session, Long facilityId, Long facilityInternalLocationId,
            Map<Long, Long> childParentMap, LocalDateTime lookupStartDateTime, LocalDateTime lookupEndDateTime) {
        Set<Long> ret = new HashSet<>();
        if (facilityInternalLocationId == null) {
            return ret;
        }
        List<Long> parent = new LinkedList<>();
        InternalLocationEntity internalLocationEntity = (InternalLocationEntity) session.get(InternalLocationEntity.class, facilityInternalLocationId);
        if (internalLocationEntity == null) {
            return ret;
        }
        InternalLocationEntity parentEntity = internalLocationEntity.getParent();
        while (parentEntity != null && parentEntity.getId() != null &&
                ((parentEntity.getDeactivateDate() == null || parentEntity.getDeactivateDate().after(lookupStartDateTime.toDate())))) {

            Long parentId = Long.valueOf(parentEntity.getId());
            if (parentEntity.getParent() != null && parentEntity.getCheckNAS() != null && parentEntity.getCheckNAS()) {
                childParentMap.put(facilityInternalLocationId, parentId);
            }
            parentEntity = parentEntity.getParent();
        }
        if (childParentMap != null && childParentMap.size() == 1) {
            parent.add(childParentMap.get(facilityInternalLocationId));
        }
        if (internalLocationEntity.getCheckNAS() != null && internalLocationEntity.getCheckNAS() || !parent.isEmpty()) {
            parent.add(facilityInternalLocationId);
        }

        Set<Long> children = getChildLocations(uc, context, session, facilityId, parent, lookupStartDateTime, lookupEndDateTime);
        while (children != null && !children.isEmpty()) {
            ret.addAll(children);
            children = getChildLocations(uc, context, session, facilityId, children, lookupStartDateTime, lookupEndDateTime);
        }

        ret.addAll(parent);
        return ret;
    }

    public static Set<Long> getLocationsToBeChecked(UserContext uc, SessionContext context, Session session, Long facilityInternalLocationId,
            Map<Long, Long> childParentMap, LocalDateTime lookupStartDateTime, LocalDateTime lookupEndDateTime) {
        Set<Long> ret = new HashSet<>();
        if (facilityInternalLocationId == null) {
            return ret;
        }
        List<Long> parent = new LinkedList<>();
        InternalLocationEntity internalLocationEntity = (InternalLocationEntity) session.get(InternalLocationEntity.class, facilityInternalLocationId);
        if (internalLocationEntity == null) {
            return ret;
        }
        InternalLocationEntity parentEntity = internalLocationEntity.getParent();
        while (parentEntity != null && parentEntity.getId() != null &&
                ((parentEntity.getDeactivateDate() == null || parentEntity.getDeactivateDate().after(lookupStartDateTime.toDate())))) {

            Long parentId = Long.valueOf(parentEntity.getId());
            if (parentEntity.getParent() != null && parentEntity.getCheckNAS() != null && parentEntity.getCheckNAS()) {
                childParentMap.put(facilityInternalLocationId, parentId);
            }
            parentEntity = parentEntity.getParent();
        }
        if (childParentMap != null && childParentMap.size() == 1) {
            parent.add(childParentMap.get(facilityInternalLocationId));
        }
        if (internalLocationEntity.getCheckNAS() != null && internalLocationEntity.getCheckNAS() || !parent.isEmpty()) {
            parent.add(facilityInternalLocationId);
        }

        Set<Long> children = getChildLocations(uc, context, session, internalLocationEntity.getFacilityId(), parent, lookupStartDateTime, lookupEndDateTime);
        while (children != null && !children.isEmpty()) {
            ret.addAll(children);
            children = getChildLocations(uc, context, session, internalLocationEntity.getFacilityId(), children, lookupStartDateTime, lookupEndDateTime);
        }

        ret.addAll(parent);
        return ret;
    }

    private static Set<Long> getChildLocations(UserContext uc, SessionContext context, Session session, Long facilityId, Collection<Long> facilityInternalLocationIds,
            LocalDateTime lookupStartDateTime, LocalDateTime lookupEndDateTime) {
        if (facilityInternalLocationIds == null || facilityInternalLocationIds.isEmpty()) {
            return null;
        }

        Criteria criteria = session.createCriteria(InternalLocationEntity.class);
        criteria.createCriteria("children", "c");
        criteria.setProjection(Projections.property("c.internalLocationId"));
        criteria.add(Restrictions.in("internalLocationId", facilityInternalLocationIds));
        if (lookupStartDateTime != null) {
            criteria.add(Restrictions.or(Restrictions.isNull("deactivateDate"), Restrictions.ge("deactivateDate", lookupStartDateTime.toDate())));
        }

        @SuppressWarnings("unchecked") List<Long> list = criteria.list();
        Set<Long> ret = new HashSet<>(list);
        return ret;
    }

    public static Criteria getSupervisionCriteriaByPersonIDs(UserContext uc, SessionContext context, Session session, Set<Long> personIDs,
            LocalDateTime lookupStartDateTime, LocalDateTime lookupEndDateTime) {

        if (personIDs == null || personIDs.isEmpty()) {
            return null;
        }
        Criteria criteria = session.createCriteria(SupervisionEntity.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.setCacheable(true);
        Disjunction disjunction = QueryUtil.propertyIn("personId", personIDs);
        criteria.add(disjunction);
        QueryUtil.effectiveDateCriteria(criteria, "supervisionStartDate", "supervisionEndDate", lookupStartDateTime, lookupEndDateTime);
        return criteria;
    }

    public static List<SupervisionEntity> getNonAssociationSupervisionsSTG(UserContext uc, SessionContext context, Session session, Long primarySupervisionId,
            LocalDateTime lookupStartDateTime, LocalDateTime lookupEndDateTime) {

        List<SupervisionEntity> ret = new ArrayList<SupervisionEntity>();
        if (primarySupervisionId == null) {
            return null;
        }

        StringBuilder strHQL = new StringBuilder("select rel.associateSTG.id from STGRelationshipEntity as rel, ");
        strHQL.append("AffiliationEntity as aff, ");
        strHQL.append("SupervisionEntity as sup ");
        strHQL.append("where sup.personId = aff.personId ");
        strHQL.append("and sup.supervisionIdentification = :supervisionId ");
        strHQL.append("and aff.sTGId = rel.stg.id ");
        strHQL.append("and rel.relationshipType = :relationType ");
        if (lookupStartDateTime != null) {
            strHQL.append("and (rel.effectiveDate <= :endDate and (rel.expiryDate is null or rel.expiryDate >= :startDate)) ");
        }
        if (lookupEndDateTime != null) {
            strHQL.append("and (aff.affiliationDate <= :endDate and (aff.expiryDate is null or aff.expiryDate >= :startDate)) ");
        }

        Query query = session.createQuery(strHQL.toString());

        query.setParameter("supervisionId", primarySupervisionId);
        query.setParameter("relationType", STGRelationshipTypeEnum.ENEMY.name());
        if (lookupStartDateTime != null) {
            query.setParameter("startDate", lookupStartDateTime.toDate());
        }
        if (lookupEndDateTime != null) {
            query.setParameter("endDate", lookupEndDateTime.toDate());
        }

        List<Long> enemySTGIds = query.list();
        if (enemySTGIds == null || enemySTGIds.isEmpty()) {
            return ret;
        }

        StringBuilder strHQL2 = new StringBuilder("select sup from SupervisionEntity as sup, ");
        strHQL2.append("AffiliationEntity as aff ");
        strHQL2.append("where sup.personId = aff.personId ");
        strHQL2.append("and aff.sTGId in (:stgIds) ");
        //strHQL2.append("and aff.status=1 ");
        if (lookupStartDateTime != null) {
            strHQL2.append("and (aff.affiliationDate <= :endDate and (aff.expiryDate is null or aff.expiryDate >= :startDate)) ");
        }
        if (lookupEndDateTime != null) {
            strHQL2.append("and (sup.supervisionStartDate <= :endDate and (sup.supervisionEndDate is null or sup.supervisionEndDate >= :startDate)) ");
        }
        Query querySup = session.createQuery(strHQL2.toString());
        querySup.setParameterList("stgIds", enemySTGIds);
        if (lookupStartDateTime != null) {
            querySup.setParameter("startDate", lookupStartDateTime.toDate());
        }
        if (lookupEndDateTime != null) {
            querySup.setParameter("endDate", lookupEndDateTime.toDate());
        }

        ret = querySup.list();
        return ret;

        /****   Comment out the handling of Parents and Children until the STG business model final defined.

         Set<Long> enemySTGIds = getEnemySTGIdSet(uc, context, session, primarySupervisionId, lookupStartDateTime, lookupEndDateTime);
         if (enemySTGIds == null || enemySTGIds.isEmpty()) {
         return ret;
         }

         // Parents/Grandparents of Enemy STG IDs
         Set<Long> parent = new HashSet<>();
         for (Long enemyId : enemySTGIds) {
         SecurityThreatGroupEntity securityThreatGroupEntity = getEffectiveSTG(uc, context, session,
         enemyId, lookupStartDateTime, lookupEndDateTime);
         while (securityThreatGroupEntity != null && securityThreatGroupEntity.getParent() != null) {
         Long pid = Long.valueOf(securityThreatGroupEntity.getParent().getId());
         parent.add(pid);
         securityThreatGroupEntity = (SecurityThreatGroupEntity) session.get(SecurityThreatGroupEntity.class, pid);
         }
         }
         parent.addAll(enemySTGIds);
         // Children/Grandchildren of Enemy STG IDS
         if (!parent.isEmpty()) {
         Set<Long> children = getChildrenSTG(uc, context, session, parent, lookupStartDateTime, lookupEndDateTime);
         while (children != null && !children.isEmpty()) {
         enemySTGIds.addAll(children);
         children = getChildrenSTG(uc, context, session, children, lookupStartDateTime, lookupEndDateTime);
         }
         enemySTGIds.addAll(parent);
         }
         Criteria criteria = getSupervisionCriteriaBySTGIDs(uc, context, session, enemySTGIds, lookupStartDateTime, lookupEndDateTime);
         ret = criteria.list();
         return ret;*/
    }

    private static SecurityThreatGroupEntity getEffectiveSTG(UserContext uc, SessionContext context, Session session, Long stgId, LocalDateTime lookupStartDateTime,
            LocalDateTime lookupEndDateTime) {
        Criteria criteria = session.createCriteria(SecurityThreatGroupEntity.class);
        criteria.add(Restrictions.idEq(stgId));
        QueryUtil.effectiveDateCriteria(criteria, "effectiveDate", "expiryDate", lookupStartDateTime, lookupEndDateTime);
        return (SecurityThreatGroupEntity) criteria.uniqueResult();
    }

    private static Set<Long> getChildrenSTG(UserContext uc, SessionContext context, Session session, Set<Long> stgIds, LocalDateTime lookupStartDateTime,
            LocalDateTime lookupEndDateTime) {
        if (stgIds == null || stgIds.isEmpty()) {
            return null;
        }
        Criteria criteria = session.createCriteria(SecurityThreatGroupEntity.class);
        Disjunction disjunction = QueryUtil.propertyIn("id", stgIds);
        criteria.add(disjunction);

        criteria.createCriteria("children", "c");
        QueryUtil.effectiveDateCriteria(criteria, "c.effectiveDate", "c.expiryDate", lookupStartDateTime, lookupEndDateTime);
        criteria.setProjection(Projections.property("c.id"));
        List<Long> list = criteria.list();
        return new HashSet<>(list);
    }

    private static Set<Long> getEnemySTGIdSet(UserContext uc, SessionContext context, Session session, Long supervisionId, LocalDateTime lookupStartDateTime,
            LocalDateTime lookupEndDateTime) {
        SupervisionEntity supervisionEntity = (SupervisionEntity) session.get(SupervisionEntity.class, supervisionId);
        Criteria affCriteria = session.createCriteria(AffiliationEntity.class);
        affCriteria.add(Restrictions.eq("personId", supervisionEntity.getPersonId()));
        QueryUtil.effectiveDateCriteria(affCriteria, "affiliationDate", "expiryDate", lookupStartDateTime, lookupEndDateTime);
        affCriteria.setProjection(Projections.property("sTGId"));
        List<Long> sTGIds = affCriteria.list();
        if (sTGIds.isEmpty()) {
            return null;
        }
        Criteria criteria = session.createCriteria(STGRelationshipEntity.class);
        criteria.add(Restrictions.eq("relationshipType", STGRelationshipTypeEnum.ENEMY.name()));

        Disjunction disjunction = QueryUtil.propertyIn("stg.id", sTGIds);
        criteria.add(disjunction);

        QueryUtil.effectiveDateCriteria(criteria, "effectiveDate", "expiryDate", lookupStartDateTime, lookupEndDateTime);
        criteria.setProjection(Projections.property("associateSTG.id"));
        return new HashSet<>(criteria.list());
    }

    public static Criteria getSupervisionCriteriaBySTGIDs(UserContext uc, SessionContext context, Session session, Set<Long> enemySTGIds,
            LocalDateTime lookupStartDateTime, LocalDateTime lookupEndDateTime) {

        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(AffiliationEntity.class);
        detachedCriteria.add(Restrictions.in("sTGId", enemySTGIds));
        QueryUtil.effectiveDateCriteria(detachedCriteria, "affiliationDate", "expiryDate", lookupStartDateTime, lookupEndDateTime);
        detachedCriteria.setProjection(Projections.property("personId"));

        Criteria criteria = session.createCriteria(SupervisionEntity.class);
        criteria.add(Subqueries.propertyIn("personId", detachedCriteria));
        return criteria;
    }

    public static List<SupervisionEntity> getSupervisionEntityList(UserContext uc, SessionContext context, Session session, Collection<Long> supervisionIds,
            Date startDateTime, Date endDateTime) {
        Criteria criteria = session.createCriteria(SupervisionEntity.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        Disjunction disjunction = QueryUtil.propertyIn("supervisionIdentification", supervisionIds);
        if (disjunction == null) {
            return null;
        }
        criteria.add(disjunction);
        QueryUtil.effectiveDateCriteria(criteria, "supervisionStartDate", "supervisionEndDate", startDateTime, endDateTime);
        return criteria.list();
    }

    /**
     * toNonAssocTypeListFromProgramAssignment is meant to replace its static counterpart
     *
     * @param programAssignmentEntityList
     * @param data
     * @return
     */
    public List<NonAssociationType> toNonAssocTypeListFromProgramAssignment(List<ProgramAssignmentEntity> programAssignmentEntityList, ConflictQueryDataHolder data) {
        return toNonAssociationTypeListFromProgramAssignment(programAssignmentEntityList, data);
    }

    @Deprecated
    public static List<NonAssociationType> toNonAssociationTypeListFromProgramAssignment(List<ProgramAssignmentEntity> programAssignmentEntityList,
            ConflictQueryDataHolder data) {

        if (programAssignmentEntityList == null || programAssignmentEntityList.isEmpty()) {
            return null;
        }

        Long conflictLocationId = data.childParentMap.get(data.facilityInternalLocationId) != null ? data.childParentMap.get(data.facilityInternalLocationId) :
                data.facilityInternalLocationId;
        InternalLocationEntity conflictLocation = (InternalLocationEntity) data.session.get(InternalLocationEntity.class, conflictLocationId);

        LocalDateTime lookupStart = new LocalDateTime().withFields(data.sessionDate).withFields(data.startTime);
        LocalDateTime lookupEnd = new LocalDateTime().withFields(data.sessionDate).withFields(data.endTime);

        List<NonAssociationType> ret = new ArrayList<>();
        for (ProgramAssignmentEntity assignment : programAssignmentEntityList) {
            if (assignment.getSupervisionId() != data.primarySupervisionEntity.getSupervisionIdentification()) {
                NonAssociationType nonAssoc = new NonAssociationType();
                Long supervisionId = assignment.getSupervisionId();
                SupervisionEntity supervisionEntity = data.supervisionEntityMap.get(supervisionId);
                if (supervisionEntity == null) {
                    continue;
                }
                nonAssoc.setPersonIdentityId(supervisionEntity.getPersonIdentityId());
                nonAssoc.setSupervisionId(supervisionEntity.getSupervisionIdentification());
                nonAssoc.setSupervisionDisplayId(supervisionEntity.getSupervisionDisplayID());
                nonAssoc.setPersonId(supervisionEntity.getPersonId());
                PersonIdentityEntity pi = (PersonIdentityEntity) data.session.get(PersonIdentityEntity.class, supervisionEntity.getPersonIdentityId());
                if (pi != null) {
                    nonAssoc.setInmateNum(pi.getOffenderNumber());
                    nonAssoc.setFirstName(pi.getFirstName());
                    nonAssoc.setLastName(pi.getLastName());
                }
                nonAssoc.setFacilityId(data.facilityId);
                nonAssoc.setFacilityInternalLocationId(data.facilityInternalLocationId);
                InternalLocationEntity internalLocationEntity = (InternalLocationEntity) data.session.get(InternalLocationEntity.class,
                        assignment.getProgramOffering().getInteranlLocationId());
                if (internalLocationEntity != null) {
                    nonAssoc.setInmateLocation(internalLocationEntity.getDescription());
                }
                if (conflictLocation != null) {
                    nonAssoc.setLocation(conflictLocation.getDescription());
                }
                List<String> conflictTypeList = getNonAssociationTypeList(data.uc, data.context, data.session, data.primarySupervisionEntity, supervisionEntity,
                        lookupStart, lookupEnd);

                //Handle for STG Non-Association Conflicts
                if (data.stgSupervisionEntityMap != null) {
                    SupervisionEntity stgSupervision = data.stgSupervisionEntityMap.get(supervisionId);
                    if (stgSupervision != null) {
                        conflictTypeList.add(NON_ASSOCIATION_STG);
                    }
                }

                nonAssoc.setConflictTypes(conflictTypeList);

                nonAssoc.setScheduleId(assignment.getProgramOffering().getScheduleId());
                nonAssoc.setStartDateTime(getDateTime(data.sessionDate, data.startTime));
                nonAssoc.setEndDateTime(getDateTime(data.sessionDate, data.endTime));
                if (!ret.contains(nonAssoc)) {
                    ret.add(nonAssoc);
                }
            }
        }

        return ret;
    }

    /**
     * getProgramAssignmentList is meant to replace its static counterpart over time
     *
     * @param data
     * @return
     */
    public List<ProgramAssignmentEntity> getProgramAssignmentList(ConflictQueryDataHolder data) {
        return getProgramAssignmentEntityList(data);
    }

    @Deprecated //see above method
    public static List<ProgramAssignmentEntity> getProgramAssignmentEntityList(ConflictQueryDataHolder data) {

        Criteria c = data.session.createCriteria(ProgramAssignmentEntity.class);

        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        c.setCacheable(true);

        Collection<Long> supervisionIDs = data.supervisionEntityMap.keySet();
        Collection<Long> scheduleIDs = data.scheduleEntityMap.keySet();
        if (supervisionIDs == null || supervisionIDs.isEmpty() && data.facilityInternalLocationIdSet == null || data.facilityInternalLocationIdSet.isEmpty()) {
            return null;
        }
        Disjunction supervisionDisjunction = QueryUtil.propertyIn("supervisionId", supervisionIDs);
        c.add(supervisionDisjunction);

        c.createCriteria("programOffering", "offering");
        Disjunction disjuncSchedules = QueryUtil.propertyIn("offering.scheduleId", scheduleIDs);
        if (disjuncSchedules != null) {
            c.add(disjuncSchedules);
        }

        if (data.facilityId != null) {
            c.add(Restrictions.eq("offering.facilityId", data.facilityId));
        }
        Disjunction disjuncFIL = QueryUtil.propertyIn("offering.interanlLocationId", data.facilityInternalLocationIdSet);
        c.add(disjuncFIL);

        Disjunction disjunction = Restrictions.disjunction();
        disjunction.add(Restrictions.eq("status", PROGRAM_ASSIGNMENT_ALLOCATED));
        disjunction.add(Restrictions.eq("status", PROGRAM_ASSIGNMENT_APPROVED));
        disjunction.add(Restrictions.eq("status", PROGRAM_ASSIGNMENT_REFERRED));
        c.add(disjunction);

        Date startDateTime = new LocalDateTime().withFields(data.sessionDate).withFields(data.startTime).toDate();
        Date endDateTime = new LocalDateTime().withFields(data.sessionDate).withFields(data.endTime).toDate();
        QueryUtil.effectiveDateCriteria(c, "startDate", "endDate", startDateTime, endDateTime);
        @SuppressWarnings("unchecked") List<ProgramAssignmentEntity> ret = c.list();
        return ret;
    }

    public static List<AppointmentEntity> getAppointmentEntityList(ConflictQueryDataHolder data) {

        Collection<Long> supervisionIDs = data.supervisionEntityMap.keySet();
        if (supervisionIDs == null || supervisionIDs.isEmpty() || data.facilityInternalLocationIdSet == null || data.facilityInternalLocationIdSet.isEmpty()) {
            return null;
        }

        Criteria criteria = data.session.createCriteria(AppointmentEntity.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.setCacheable(true);

        Disjunction disjuncSupervisions = QueryUtil.propertyIn("supervisionId", supervisionIDs);
        criteria.add(disjuncSupervisions);
        if (data.facilityId != null) {
            criteria.add(Restrictions.eq("facilityId", data.facilityId));
        }
        Disjunction filDisjunction = QueryUtil.propertyIn("locationId", data.facilityInternalLocationIdSet);
        criteria.add(filDisjunction);
        LocalDateTime startDateTime = new LocalDateTime().withFields(data.sessionDate).withFields(data.startTime);
        LocalDateTime endDateTime = new LocalDateTime().withFields(data.sessionDate).withFields(data.endTime);
        QueryUtil.effectiveDateCriteria(criteria, "startTime", "endTime", startDateTime, endDateTime);
        @SuppressWarnings("unchecked") List<AppointmentEntity> ret = criteria.list();
        return ret;
    }

    public static List<NonAssociationType> toNonAssociationTypeList(UserContext uc, SessionContext context, Session session, SupervisionEntity primarySupervisionEntity,
            Set<Long> nasSupervisionIds, Long facilityId, Long facilityInternalLocationId, Map<Long, Long> childParentMap,
            Map<Long, SupervisionEntity> stgSupervisionEntityMap, LocalDate sessionDate, LocalTime startTime, LocalTime endTime, NonAssociationType.EventType event,
            String eventReferenceSet, String eventReferenceCode) {

        if (nasSupervisionIds == null || nasSupervisionIds.isEmpty()) {
            return null;
        }
        FacilityEntity facilityEntity = BeanHelper.getEntity(session, FacilityEntity.class, facilityId);
        Map<String, String> eventTypeMap = null;
        CodeType metaData = metaData(event);
        if (metaData != null) {
            eventTypeMap = ReferenceDataServiceAdapter.getReferenceCodesDescMap(uc, metaData.getSet(), "en");
        }

        String conflictLocationDescription = facilityEntity.getFacilityName();
        Long conflictLocationId = childParentMap.get(facilityInternalLocationId) != null ? childParentMap.get(facilityInternalLocationId) : facilityInternalLocationId;
        InternalLocationEntity conflictLocation = null;
        if (event != null && (event.equals(NonAssociationType.EventType.CRT_EXTERNAL_MOVEMENT) || event.equals(NonAssociationType.EventType.TAP_EXTERNAL_MOVEMENT)
                || event.equals(NonAssociationType.EventType.TRN_EXTERNAL_MOVEMENT))) {
            conflictLocationDescription = facilityEntity.getFacilityName();
        } else if (conflictLocationId != null) {
            conflictLocation = (InternalLocationEntity) session.get(InternalLocationEntity.class, conflictLocationId);
            conflictLocationDescription = conflictLocation.getDescription();
        }

        LocalDateTime lookupStart = null;
        if (sessionDate != null && startTime != null) {
            lookupStart = new LocalDateTime().withFields(sessionDate).withFields(startTime);
        }
        LocalDateTime lookupEnd = null;
        if (sessionDate != null && endTime != null) {
            lookupEnd = new LocalDateTime().withFields(sessionDate).withFields(endTime);
        }

        List<NonAssociationType> ret = new ArrayList<>();
        for (Long svId : nasSupervisionIds) {
            if (svId != primarySupervisionEntity.getSupervisionIdentification()) {
                NonAssociationType nonAssoc = new NonAssociationType();
                SupervisionEntity supervisionEntity = (SupervisionEntity) session.get(SupervisionEntity.class, svId);
                if (supervisionEntity == null) {
                    continue;
                }
                nonAssoc.setPersonIdentityId(supervisionEntity.getPersonIdentityId());
                nonAssoc.setSupervisionId(supervisionEntity.getSupervisionIdentification());
                nonAssoc.setSupervisionDisplayId(supervisionEntity.getSupervisionDisplayID());
                nonAssoc.setPersonId(supervisionEntity.getPersonId());
                PersonIdentityEntity pi = (PersonIdentityEntity) session.get(PersonIdentityEntity.class, supervisionEntity.getPersonIdentityId());
                if (pi != null) {
                    nonAssoc.setInmateNum(pi.getOffenderNumber());
                    nonAssoc.setFirstName(pi.getFirstName());
                    nonAssoc.setLastName(pi.getLastName());
                }
                nonAssoc.setFacilityId(facilityId);
                nonAssoc.setFacilityInternalLocationId(facilityInternalLocationId);
                if (conflictLocation != null) {
                    nonAssoc.setInmateLocation(conflictLocation.getDescription());
                } else {
                    nonAssoc.setInmateLocation(conflictLocationDescription);
                }
                nonAssoc.setLocation(conflictLocationDescription);
                List<String> conflictTypeList = getNonAssociationTypeList(uc, context, session, primarySupervisionEntity, supervisionEntity, lookupStart, lookupEnd);

                //Handle for STG Non-Association Conflicts
                if (stgSupervisionEntityMap != null) {
                    SupervisionEntity stgSupervision = stgSupervisionEntityMap.get(svId);
                    if (stgSupervision != null) {
                        conflictTypeList.add(NON_ASSOCIATION_STG);
                    }
                }

                nonAssoc.setConflictTypes(conflictTypeList);
                if (eventTypeMap != null) {
                    if (eventReferenceCode != null && !eventReferenceCode.trim().isEmpty()) {
                        nonAssoc.setConflictSubTypes(Arrays.asList(new String[] { eventTypeMap.get(eventReferenceCode) }));
                    } else if (event != null && (event.equals(NonAssociationType.EventType.CRT_EXTERNAL_MOVEMENT) || event.equals(
                            NonAssociationType.EventType.TAP_EXTERNAL_MOVEMENT) || event.equals(NonAssociationType.EventType.TRN_EXTERNAL_MOVEMENT))) {
                        nonAssoc.setConflictSubTypes(Arrays.asList(new String[] { eventTypeMap.get(metaData.getCode()) }));
                    }
                }

                nonAssoc.setScheduleId(null);
                nonAssoc.setStartDateTime(getDateTime(sessionDate, startTime));
                nonAssoc.setEndDateTime(getDateTime(sessionDate, endTime));
                if (!ret.contains(nonAssoc)) {
                    ret.add(nonAssoc);
                }
            }
        }

        return ret;
    }

    /**
     * toNonAssocTypeListFromAppointment is meant to replace its static counterpart
     *
     * @param appointmentEntityList
     * @param data
     * @return
     */
    public List<NonAssociationType> toNonAssocTypeListFromAppointment(List<AppointmentEntity> appointmentEntityList, ConflictQueryDataHolder data) {
        return toNonAssociationTypeListFromAppointment(appointmentEntityList, data);
    }

    @Deprecated
    public static List<NonAssociationType> toNonAssociationTypeListFromAppointment(List<AppointmentEntity> appointmentEntityList, ConflictQueryDataHolder data) {

        if (appointmentEntityList == null || appointmentEntityList.isEmpty()) {
            return null;
        }

        Map<String, String> appointmentTypeMap = ReferenceDataServiceAdapter.getReferenceCodesDescMap(data.uc, "APPOINTMENTTYPE", "en");

        Long conflictLocationId = data.childParentMap.get(data.facilityInternalLocationId) != null ? data.childParentMap.get(data.facilityInternalLocationId) :
                data.facilityInternalLocationId;
        InternalLocationEntity conflictLocation = (InternalLocationEntity) data.session.get(InternalLocationEntity.class, conflictLocationId);

        LocalDateTime lookupStart = new LocalDateTime().withFields(data.sessionDate).withFields(data.startTime);
        LocalDateTime lookupEnd = new LocalDateTime().withFields(data.sessionDate).withFields(data.endTime);

        List<NonAssociationType> ret = new ArrayList<>();
        for (AppointmentEntity appointment : appointmentEntityList) {
            if (appointment.getSupervisionId() != data.primarySupervisionEntity.getSupervisionIdentification()) {
                NonAssociationType nonAssoc = new NonAssociationType();
                Long supervisionId = appointment.getSupervisionId();
                SupervisionEntity supervisionEntity = data.supervisionEntityMap.get(supervisionId);
                if (supervisionEntity == null) {
                    continue;
                }
                nonAssoc.setPersonIdentityId(supervisionEntity.getPersonIdentityId());
                nonAssoc.setSupervisionId(supervisionEntity.getSupervisionIdentification());
                nonAssoc.setSupervisionDisplayId(supervisionEntity.getSupervisionDisplayID());
                nonAssoc.setPersonId(supervisionEntity.getPersonId());
                PersonIdentityEntity pi = (PersonIdentityEntity) data.session.get(PersonIdentityEntity.class, supervisionEntity.getPersonIdentityId());
                if (pi != null) {
                    nonAssoc.setInmateNum(pi.getOffenderNumber());
                    nonAssoc.setFirstName(pi.getFirstName());
                    nonAssoc.setLastName(pi.getLastName());
                }
                nonAssoc.setFacilityId(data.facilityId);
                nonAssoc.setFacilityInternalLocationId(data.facilityInternalLocationId);
                InternalLocationEntity internalLocationEntity = (InternalLocationEntity) data.session.get(InternalLocationEntity.class, appointment.getLocationId());
                if (internalLocationEntity != null) {
                    nonAssoc.setInmateLocation(internalLocationEntity.getDescription());
                }
                if (conflictLocation != null) {
                    nonAssoc.setLocation(conflictLocation.getDescription());
                }
                List<String> conflictTypeList = getNonAssociationTypeList(data.uc, data.context, data.session, data.primarySupervisionEntity, supervisionEntity,
                        lookupStart, lookupEnd);

                //Handle for STG Non-Association Conflicts
                if (data.stgSupervisionEntityMap != null) {
                    SupervisionEntity stgSupervision = data.stgSupervisionEntityMap.get(supervisionId);
                    if (stgSupervision != null) {
                        conflictTypeList.add(NON_ASSOCIATION_STG);
                    }
                }

                nonAssoc.setConflictTypes(conflictTypeList);
                if (appointment.getType() != null) {
                    nonAssoc.setConflictSubTypes(Arrays.asList(new String[] { appointmentTypeMap.get(appointment.getType()) }));
                }

                nonAssoc.setScheduleId(null);
                nonAssoc.setStartDateTime(getDateTime(data.sessionDate, data.startTime));
                nonAssoc.setEndDateTime(getDateTime(data.sessionDate, data.endTime));
                if (!ret.contains(nonAssoc)) {
                    ret.add(nonAssoc);
                }
            }
        }

        return ret;
    }

    public List<MovementActivityEntity> getExternalMovementActivityList(ConflictQueryDataHolder data) {
        return getExternalMovementActivityEntityList(data);
    }

    @Deprecated
    public static List<MovementActivityEntity> getExternalMovementActivityEntityList(ConflictQueryDataHolder data) {

        Collection<Long> supervisionIdSet = data.supervisionEntityMap.keySet();
        if (supervisionIdSet == null || supervisionIdSet.isEmpty()) {
            return null;
        }

        StringBuilder hql = new StringBuilder();
        //ma1 = OUT	ma2 = IN
        hql.append("SELECT ma2 ");
        hql.append("FROM ExternalMovementActivityEntity AS ma1, ExternalMovementActivityEntity AS ma2, ");
        hql.append("ActivityEntity AS act1, ActivityEntity AS act2 ");
        hql.append("WHERE ma1.movementType IN (:moveTypes) ");
        hql.append("AND ma2.movementCategory = :movementCategory ");
        hql.append("AND ma1.groupid = ma2.groupid ");
        hql.append("AND ma1.movementId <> ma2.movementId ");
        hql.append("AND ma1.activityId = act1.activityId ");
        hql.append("AND ma2.activityId = act2.activityId ");
        hql.append("AND act2.antecedentId = act1.activityId ");
        hql.append("AND ma1.movementDirection = 'OUT' ");
        hql.append("AND ma2.movementStatus = 'PENDING' ");
        hql.append("AND (ma2.fromFacilityId = :facilityId or ma2.toFacilityId = :facilityId) ");
        hql.append("AND ma2.supervisionId IN (:supervisionIds) ");
        hql.append("AND act1.plannedStartDate <= :endDateTime ");
        hql.append("AND act2.plannedEndDate >= :startDateTime ");
        hql.append("ORDER BY act1.plannedStartDate ");

        Query query = data.session.createQuery(hql.toString());
        query.setParameter("movementCategory", MovementActivityType.MovementCategory.EXTERNAL.value());
        query.setParameterList("moveTypes", Arrays.asList(
                new String[]{MovementServiceBean.MovementType.CRT.code(),
                        MovementServiceBean.MovementType.TAP.code()}));
        query.setParameterList("supervisionIds", supervisionIdSet);
        query.setParameter("facilityId", data.facilityId);
        query.setParameter("startDateTime", new LocalDateTime().withFields(data.sessionDate).withFields(data.startTime).toDate());
        query.setParameter("endDateTime", new LocalDateTime().withFields(data.sessionDate).withFields(data.endTime).toDate());

        @SuppressWarnings("unchecked") List<MovementActivityEntity> ret = query.list();
        return ret;
    }

    /**
     * this method is meant to eventually replace its static counterpart
     *
     * @param data
     * @return
     */
    public List<NonAssociationType> toNonAssocTypeListFromExternalMovement(List<MovementActivityEntity> movementActivityEntityList, ConflictQueryDataHolder data,
            NonAssociationType.EventType event) {
        return toNonAssociationTypeListFromExternalMovement(movementActivityEntityList, data, event);
    }

    @Deprecated
    public static List<NonAssociationType> toNonAssociationTypeListFromExternalMovement(List<MovementActivityEntity> movementActivityEntityList,
            ConflictQueryDataHolder data, NonAssociationType.EventType event) {

        if (movementActivityEntityList == null || movementActivityEntityList.isEmpty()) {
            return null;
        }
        if (event != null && !event.equals(NonAssociationType.EventType.CRT_EXTERNAL_MOVEMENT) && !event.equals(NonAssociationType.EventType.TAP_EXTERNAL_MOVEMENT)
                && !event.equals(NonAssociationType.EventType.TRN_EXTERNAL_MOVEMENT)) {
            return null;
        }
        Map<String, String> movementTypeMap = ReferenceDataServiceAdapter.getReferenceCodesDescMap(data.uc, "MOVEMENTTYPE", "en");
        CodeType metaData = metaData(event);

        String facilityName = BeanHelper.getEntity(data.session, FacilityEntity.class, data.facilityId).getFacilityName();

        LocalDateTime lookupStart = new LocalDateTime().withFields(data.sessionDate).withFields(data.startTime);
        LocalDateTime lookupEnd = new LocalDateTime().withFields(data.sessionDate).withFields(data.endTime);

        List<NonAssociationType> ret = new ArrayList<>();
        for (MovementActivityEntity movementActivityEntity : movementActivityEntityList) {
            if (movementActivityEntity instanceof ExternalMovementActivityEntity) {
                if (movementActivityEntity.getSupervisionId() != data.primarySupervisionEntity.getSupervisionIdentification()
                        && ((ExternalMovementActivityEntity)movementActivityEntity).getToFacilityId() != null) {
                    // && facilityId.longValue() != movementActivityEntity.getToFacilityId().longValue()) {
                    NonAssociationType nonAssoc = new NonAssociationType();
                    Long supervisionId = movementActivityEntity.getSupervisionId();
                    SupervisionEntity supervisionEntity = data.supervisionEntityMap.get(supervisionId);
                    if (supervisionEntity == null) {
                        continue;
                    }
                    nonAssoc.setPersonIdentityId(supervisionEntity.getPersonIdentityId());
                    nonAssoc.setSupervisionId(supervisionEntity.getSupervisionIdentification());
                    nonAssoc.setSupervisionDisplayId(supervisionEntity.getSupervisionDisplayID());
                    nonAssoc.setPersonId(supervisionEntity.getPersonId());
                    PersonIdentityEntity pi = (PersonIdentityEntity) data.session.get(PersonIdentityEntity.class, supervisionEntity.getPersonIdentityId());
                    if (pi != null) {
                        nonAssoc.setInmateNum(pi.getOffenderNumber());
                        nonAssoc.setFirstName(pi.getFirstName());
                        nonAssoc.setLastName(pi.getLastName());
                    }
                    nonAssoc.setFacilityId(data.facilityId);
                    nonAssoc.setFacilityInternalLocationId(data.facilityInternalLocationId);
                    nonAssoc.setInmateLocation(facilityName);
                    nonAssoc.setLocation(facilityName);

                    List<String> conflictTypeList = getNonAssociationTypeList(data.uc, data.context, data.session, data.primarySupervisionEntity, supervisionEntity, lookupStart, lookupEnd);

                    //Handle for STG Non-Association Conflicts
                    if (data.stgSupervisionEntityMap != null) {
                        SupervisionEntity stgSupervision = data.stgSupervisionEntityMap.get(supervisionId);
                        if (stgSupervision != null) {
                            conflictTypeList.add(NON_ASSOCIATION_STG);
                        }
                    }
                    conflictTypeList = new ArrayList<>(new HashSet<>(conflictTypeList));
                    nonAssoc.setConflictTypes(conflictTypeList);

                    if (movementActivityEntity.getMovementType() != null) {
                        nonAssoc.setConflictSubTypes(Arrays.asList(new String[]{movementTypeMap.get(movementActivityEntity.getMovementType())}));
                    }
                    if (movementTypeMap != null) {
                        if (metaData != null && metaData.getCode() != null && !metaData.getCode().trim().isEmpty()) {
                            nonAssoc.setConflictSubTypes(Arrays.asList(new String[]{movementTypeMap.get(metaData.getCode())}));
                        }
                    }

                    nonAssoc.setScheduleId(null);
                    nonAssoc.setStartDateTime(getDateTime(data.sessionDate, data.startTime));
                    nonAssoc.setEndDateTime(getDateTime(data.sessionDate, data.endTime));
                    if (!ret.contains(nonAssoc)) {
                        ret.add(nonAssoc);
                    }
                }
            } else if (movementActivityEntity instanceof InternalMovementActivityEntity) {
                NonAssociationType nonAssoc = getNonAssociationType((InternalMovementActivityEntity)movementActivityEntity, data, movementTypeMap, facilityName, lookupStart, lookupEnd);
                if (nonAssoc == null) continue;
                if (!ret.contains(nonAssoc)) {
                    ret.add(nonAssoc);
                }
            }
        }
        return ret;
    }

    public List<InternalMovementActivityEntity> getInternalMovementActivityList(ConflictQueryDataHolder data, Integer[] movementDuration) {
        return getInternalMovementActivityEntityList(data, movementDuration);
    }

    @Deprecated
    public static List<InternalMovementActivityEntity> getInternalMovementActivityEntityList(ConflictQueryDataHolder data, Integer[] movementDuration) {

        Criteria criteria = data.session.createCriteria(InternalMovementActivityEntity.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        Collection<Long> supervisionIdSet = data.supervisionEntityMap.keySet();
        if (supervisionIdSet == null || supervisionIdSet.isEmpty() || data.facilityInternalLocationIdSet == null || data.facilityInternalLocationIdSet.isEmpty()) {
            return null;
        }
        Disjunction disjuncSupervisions = QueryUtil.propertyIn("supervisionId", supervisionIdSet);
        criteria.add(disjuncSupervisions);

        Set<Set<Long>> ssFIL = QueryUtil.splitSet(data.facilityInternalLocationIdSet);
        if (!ssFIL.isEmpty() && !((Set) (ssFIL.toArray()[0])).isEmpty()) {
            Disjunction disjuncFIL = Restrictions.disjunction();
            for (Set<Long> s : ssFIL) {
                disjuncFIL.add(Restrictions.in("toInternalLocationId", s));
            }
            criteria.add(disjuncFIL);
        }
        criteria.add(Restrictions.eq("movementCategory", MovementActivityType.MovementCategory.INTERNAL.value()));
        criteria.add(Restrictions.eq("movementType", MovementActivityType.MovementType.APP.code()));
        criteria.add(Restrictions.or(
                Restrictions.eq("movementStatus", MovementActivityType.MovementStatus.PENDING.code()),
                Restrictions.eq("movementStatus", MovementActivityType.MovementStatus.COMPLETED.code())));

        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ActivityEntity.class);
        detachedCriteria.setProjection(Projections.id());
        detachedCriteria.add(Restrictions.eq("activeStatusFlag", Boolean.TRUE));

        if (movementDuration != null && movementDuration.length == 2) {
            detachedCriteria.add(Restrictions.and(
                    Restrictions.ge("plannedStartDate", new LocalDateTime().withFields(data.sessionDate).withFields(data.startTime).plusHours(-movementDuration[0]).plusMinutes(-movementDuration[1]).toDate()),
                    Restrictions.le("plannedStartDate", new LocalDateTime().withFields(data.sessionDate).withFields(data.startTime).plusHours(movementDuration[0]).plusMinutes(movementDuration[1]).toDate())));
        } else {
            detachedCriteria.add(Restrictions.and(
                    Restrictions.ge("plannedStartDate", new LocalDateTime().withFields(data.sessionDate).withFields(data.startTime).toDate()),
                    Restrictions.le("plannedStartDate", new LocalDateTime().withFields(data.sessionDate).withFields(data.endTime).toDate())));
        }

        criteria.add(Subqueries.propertyIn("activityId", detachedCriteria));

        @SuppressWarnings("unchecked") List<InternalMovementActivityEntity> ret = criteria.list();
        return ret;
    }

    /**
     * this method will eventually replace its static counterpart
     *
     * @param data
     * @return
     */
    public List<NonAssociationType> toNonAssocTypeListFromInternalMovement(List<InternalMovementActivityEntity> internalMovementActivityEntityList,
            ConflictQueryDataHolder data) {
        return toNonAssociationTypeListFromInternalMovement(internalMovementActivityEntityList, data);
    }

    @Deprecated
    public static List<NonAssociationType> toNonAssociationTypeListFromInternalMovement(List<InternalMovementActivityEntity> internalMovementActivityEntityList,
            ConflictQueryDataHolder data) {

        if (internalMovementActivityEntityList == null || internalMovementActivityEntityList.isEmpty()) {
            return null;
        }

        Map<String, String> movementTypeMap = ReferenceDataServiceAdapter.getReferenceCodesDescMap(data.uc, "MOVEMENTTYPE", "en");
        Long conflictLocationId = null;
        InternalLocationEntity conflictLocation = null;
        if (data.facilityInternalLocationId != null) {
            conflictLocationId = data.childParentMap.get(data.facilityInternalLocationId) != null ? data.childParentMap.get(data.facilityInternalLocationId) :
                    data.facilityInternalLocationId;
        }
        if (conflictLocationId != null) {
            conflictLocation = (InternalLocationEntity) data.session.get(InternalLocationEntity.class, conflictLocationId);
        }

        LocalDateTime lookupStart = new LocalDateTime().withFields(data.sessionDate).withFields(data.startTime);
        LocalDateTime lookupEnd = new LocalDateTime().withFields(data.sessionDate).withFields(data.endTime);

        List<NonAssociationType> ret = new ArrayList<>();

        for (InternalMovementActivityEntity movementActivityEntity : internalMovementActivityEntityList) {
            NonAssociationType nonAssoc = getNonAssociationType(movementActivityEntity, data, movementTypeMap,
                    conflictLocation == null ? null : conflictLocation.getDescription(), lookupStart, lookupEnd);
            if (nonAssoc == null)
                continue;
            if (!ret.contains(nonAssoc)) {
                ret.add(nonAssoc);
            }
        }

        return ret;
    }

    private static NonAssociationType getNonAssociationType(InternalMovementActivityEntity movementActivityEntity, ConflictQueryDataHolder data,
            Map<String, String> movementTypeMap, String conflictLocationDescription, LocalDateTime lookupStart, LocalDateTime lookupEnd) {
        NonAssociationType nonAssoc = new NonAssociationType();
        Long supervisionId = movementActivityEntity.getSupervisionId();
        SupervisionEntity supervisionEntity = data.supervisionEntityMap.get(supervisionId);
        if (supervisionEntity == null) {
            return null;
        }
        nonAssoc.setPersonIdentityId(supervisionEntity.getPersonIdentityId());
        nonAssoc.setSupervisionId(supervisionEntity.getSupervisionIdentification());
        nonAssoc.setSupervisionDisplayId(supervisionEntity.getSupervisionDisplayID());
        nonAssoc.setPersonId(supervisionEntity.getPersonId());
        PersonIdentityEntity pi = (PersonIdentityEntity) data.session.get(PersonIdentityEntity.class, supervisionEntity.getPersonIdentityId());
        if (pi != null) {
            nonAssoc.setInmateNum(pi.getOffenderNumber());
            nonAssoc.setFirstName(pi.getFirstName());
            nonAssoc.setLastName(pi.getLastName());
        }
        nonAssoc.setFacilityId(data.facilityId);
        nonAssoc.setFacilityInternalLocationId(data.facilityInternalLocationId);
        nonAssoc.setLocation(conflictLocationDescription);

        nonAssoc.setFacilityInternalLocationId(data.facilityInternalLocationId);
        if (movementActivityEntity.getToInternalLocationId() != null) {
            InternalLocationEntity internalLocationEntity = (InternalLocationEntity) data.session.get(InternalLocationEntity.class,
                    movementActivityEntity.getToInternalLocationId());
            if (internalLocationEntity != null) {
                nonAssoc.setInmateLocation(internalLocationEntity.getDescription());
            }
        }

        List<String> conflictTypeList = getNonAssociationTypeList(data.uc, data.context, data.session, data.primarySupervisionEntity, supervisionEntity, lookupStart, lookupEnd);

        //Handle for STG Non-Association Conflicts
        if (data.stgSupervisionEntityMap != null) {
            SupervisionEntity stgSupervision = data.stgSupervisionEntityMap.get(supervisionId);
            if (stgSupervision != null) {
                conflictTypeList.add(NON_ASSOCIATION_STG);
            }
        }

        nonAssoc.setConflictTypes(conflictTypeList);

        if (movementActivityEntity.getMovementType() != null) {
            nonAssoc.setConflictSubTypes(Arrays.asList(new String[]{movementTypeMap.get(movementActivityEntity.getMovementType())}));
        }

        nonAssoc.setScheduleId(null);
        nonAssoc.setStartDateTime(getDateTime(data.sessionDate, data.startTime));
        nonAssoc.setEndDateTime(getDateTime(data.sessionDate, data.endTime));
        return nonAssoc;
    }

    /**
     * this method will replace its static counterpart
     *
     * @param data
     * @param movementDuration
     * @return
     */
    public List<MovementActivityEntity> getMovementActivityList(ConflictQueryDataHolder data, Integer[] movementDuration) {
        return getTransfromMovementActivityEntityList(data, movementDuration);
    }

    @Deprecated
    public static List<MovementActivityEntity> getTransfromMovementActivityEntityList(ConflictQueryDataHolder data, Integer[] movementDuration) {

        if (data == null || data.facilityId == null) {
            return null;
        }
        Collection<Long> supervisionIdSet = data.supervisionEntityMap.keySet();
        if (supervisionIdSet == null || supervisionIdSet.isEmpty()) {
            return null;
        }

        Session session = data.session;

        Criteria lmCriteria = session.createCriteria(LastCompletedSupervisionMovementEntity.class);
        Disjunction svDisjunction = QueryUtil.propertyIn("supervisionId", supervisionIdSet);
        lmCriteria.add(svDisjunction);
        List<LastCompletedSupervisionMovementEntity> lastMovementEntityList = lmCriteria.list();

        List<MovementActivityEntity> ret = new ArrayList<>();
        for (LastCompletedSupervisionMovementEntity lm : lastMovementEntityList) {
            MovementActivityEntity mv = (MovementActivityEntity) session.get(MovementActivityEntity.class, lm.getMovementActivityId());
            if (mv != null) {
                if (mv instanceof ExternalMovementActivityEntity) {
                    Long facilityId = ((ExternalMovementActivityEntity)mv).getToFacilityId();
                    if (facilityId != null && facilityId.longValue() == data.facilityId.longValue()) {
                        ret.add(mv);
                    }
                } else if (mv instanceof InternalMovementActivityEntity) {
                    Long supervisionId = mv.getSupervisionId();
                    SupervisionEntity sv = BeanHelper.findEntity(session, SupervisionEntity.class, supervisionId);
                    if (sv != null && sv.getFacilityId().longValue() == data.facilityId.longValue()) {
                        ret.add(mv);
                    }
                }
            }
        }

        return ret;
    }

    public List<NonAssociationType> toNonAssocTypeListFromTransWaitList(ConflictQueryDataHolder data, Integer[] duration, NonAssociationType.EventType event) {

        if (data == null || data.facilityId == null) {
            return null;
        }
        Collection<Long> supervisionIdSet = data.supervisionEntityMap.keySet();
        if (supervisionIdSet == null || supervisionIdSet.isEmpty()) {
            return null;
        }

        Session session = data.session;
        Long facilityId = data.facilityId;
        String facilityName = BeanHelper.getEntity(session, FacilityEntity.class, facilityId).getFacilityName();

        Criteria criteria = session.createCriteria(TransferWaitlistEntryEntity.class);
        criteria.add(Restrictions.eq("toFacility", facilityId));
        Disjunction disjunction = QueryUtil.propertyIn("supervision", supervisionIdSet);
        criteria.add(disjunction);

        criteria.setProjection(Projections.property("supervision"));
        List<Long> supervisions = criteria.list();
        if (supervisions.isEmpty())
            return null;

        List<NonAssociationType> ret = new ArrayList<>();

        Criteria svCriteria = session.createCriteria(SupervisionEntity.class);
        Disjunction svDisjunction = QueryUtil.propertyIn("supervisionIdentification", supervisions);
        svCriteria.add(svDisjunction);
        List<SupervisionEntity> supervisionEntityList = svCriteria.list();
        for (SupervisionEntity supervisionEntity : supervisionEntityList) {
            NonAssociationType nonAssoc = toNonAssociationType(data, supervisionEntity, facilityName);
            if (nonAssoc != null && !ret.contains(nonAssoc)) {
                ret.add(nonAssoc);
            }
        }

        return ret;
    }

    public NonAssociationType toNonAssociationType(ConflictQueryDataHolder data, SupervisionEntity supervisionEntity, String facilityName) {
        NonAssociationType nonAssoc = new NonAssociationType();

        Session session = data.session;
        Long supervisionId = supervisionEntity.getSupervisionIdentification();
        PersonIdentityEntity pi = BeanHelper.getEntity(session, PersonIdentityEntity.class, supervisionEntity.getPersonIdentityId());
        nonAssoc.setPersonIdentityId(supervisionEntity.getPersonIdentityId());
        nonAssoc.setSupervisionId(supervisionId);
        nonAssoc.setInmateNum(pi.getOffenderNumber());
        nonAssoc.setSupervisionDisplayId(supervisionEntity.getSupervisionDisplayID());
        nonAssoc.setPersonId(supervisionEntity.getPersonId());

        nonAssoc.setFacilityId(data.facilityId);
        nonAssoc.setFacilityInternalLocationId(data.facilityInternalLocationId);

        nonAssoc.setFirstName(pi.getFirstName());
        nonAssoc.setLastName(pi.getLastName());

        List<String> conflictTypeList = getNonAssociationTypeList(data.uc, data.context, data.session, data.primarySupervisionEntity, supervisionEntity, null, null);

        //Handle for STG Non-Association Conflicts
        if (data.stgSupervisionEntityMap != null) {
            SupervisionEntity stgSupervision = data.stgSupervisionEntityMap.get(supervisionId);
            if (stgSupervision != null) {
                conflictTypeList.add(NON_ASSOCIATION_STG);
            }
        }

        nonAssoc.setConflictTypes(conflictTypeList);
        nonAssoc.setScheduleId(null);
        nonAssoc.setStartDateTime(getDateTime(data.sessionDate, data.startTime));
        nonAssoc.setEndDateTime(getDateTime(data.sessionDate, data.endTime));

        nonAssoc.setLocation(facilityName);
        nonAssoc.setInmateLocation(facilityName);

        return nonAssoc;
    }

    /**
     * this method will replace its static counterpart
     *
     * @param data
     * @param duration
     * @return
     */
    public List<OffenseInCustodyHearingEntity> getOffenseInCustodyHearingList(ConflictQueryDataHolder data, Integer[] duration) {
        return getOffenseInCustodyHearingEntityList(data, duration);
    }

    @Deprecated
    public static List<OffenseInCustodyHearingEntity> getOffenseInCustodyHearingEntityList(ConflictQueryDataHolder data, Integer[] duration) {

        Collection<Long> supervisionIdSet = data.supervisionEntityMap.keySet();
        if (data.facilityId == null || supervisionIdSet == null || supervisionIdSet.isEmpty() ||
                data.facilityInternalLocationIdSet == null || data.facilityInternalLocationIdSet.isEmpty()) {
            return null;
        }

        Criteria criteria = data.session.createCriteria(OffenseInCustodyHearingEntity.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        criteria.add(Restrictions.eq("facilityId", data.facilityId));
        criteria.add(Restrictions.ilike("hearingStatus", "ACT%"));

        criteria.createCriteria("offenseInCustody", "oic");
        Disjunction svDisjunction = QueryUtil.propertyIn("oic.supervisionId", supervisionIdSet);
        criteria.add(svDisjunction);

        Disjunction filDisjunction = QueryUtil.propertyIn("locationId", data.facilityInternalLocationIdSet);
        criteria.add(filDisjunction);

        LocalDateTime startDateTime = new LocalDateTime().withFields(data.sessionDate).withFields(data.startTime);
        LocalDateTime endDateTime = new LocalDateTime().withFields(data.sessionDate).withFields(data.endTime);
        if (data.sessionDate.isBefore(endDateTime.toLocalDate())) {
            endDateTime = new LocalDateTime().withFields(data.sessionDate).withFields(new LocalTime(23, 59, 59));
        }
        if (duration != null && duration.length == 2) {
            criteria.add(Restrictions.and(Restrictions.ge("hearingDate",
                            new LocalDateTime().withFields(data.sessionDate).withFields(data.startTime).plusHours(-duration[0]).plusMinutes(-duration[1]).toDate()),
                    Restrictions.le("hearingDate", new LocalDateTime().withFields(data.sessionDate).withFields(data.endTime).toDate())));
        } else {
            criteria.add(Restrictions.and(Restrictions.ge("hearingDate", startDateTime.toDate()), Restrictions.le("hearingDate", endDateTime.toDate())));
        }

        @SuppressWarnings("unchecked") List<OffenseInCustodyHearingEntity> ret = criteria.list();

        return ret;
    }

    /**
     * this method will replace its static counterpart
     *
     * @param offenseInCustodyHearingEntityList
     * @param data
     * @return
     */
    public List<NonAssociationType> toNonAssocTypeListFromHearing(List<OffenseInCustodyHearingEntity> offenseInCustodyHearingEntityList, ConflictQueryDataHolder data) {
        return toNonAssociationTypeListFromHearing(offenseInCustodyHearingEntityList, data);
    }

    @Deprecated
    public static List<NonAssociationType> toNonAssociationTypeListFromHearing(List<OffenseInCustodyHearingEntity> offenseInCustodyHearingEntityList,
            ConflictQueryDataHolder data) {

        if (offenseInCustodyHearingEntityList == null || offenseInCustodyHearingEntityList.isEmpty()) {
            return null;
        }

        Map<String, String> hearingTypeMap = ReferenceDataServiceAdapter.getReferenceCodesDescMap(data.uc, "HEARINGTYPE", "en");

        Long conflictLocationId = null;
        InternalLocationEntity conflictLocation = null;
        if (data.facilityInternalLocationId != null) {
            conflictLocationId = data.childParentMap.get(data.facilityInternalLocationId) != null ? data.childParentMap.get(data.facilityInternalLocationId) :
                    data.facilityInternalLocationId;
        }
        if (conflictLocationId != null) {
            conflictLocation = (InternalLocationEntity) data.session.get(InternalLocationEntity.class, conflictLocationId);
        }

        LocalDateTime lookupStart = new LocalDateTime().withFields(data.sessionDate).withFields(data.startTime);
        LocalDateTime lookupEnd = new LocalDateTime().withFields(data.sessionDate).withFields(data.endTime);

        List<NonAssociationType> ret = new ArrayList<>();

        for (OffenseInCustodyHearingEntity custodyHearingEntity : offenseInCustodyHearingEntityList) {
            NonAssociationType nonAssoc = new NonAssociationType();
            if (custodyHearingEntity.getOffenseInCustody() == null || custodyHearingEntity.getOffenseInCustody().getSupervisionId() == null) {
                continue;
            }
            Long supervisionId = custodyHearingEntity.getOffenseInCustody().getSupervisionId();
            SupervisionEntity supervisionEntity = data.supervisionEntityMap.get(supervisionId);
            if (supervisionEntity == null) {
                continue;
            }
            nonAssoc.setPersonIdentityId(supervisionEntity.getPersonIdentityId());
            nonAssoc.setSupervisionId(supervisionEntity.getSupervisionIdentification());
            nonAssoc.setSupervisionDisplayId(supervisionEntity.getSupervisionDisplayID());
            nonAssoc.setPersonId(supervisionEntity.getPersonId());
            PersonIdentityEntity pi = (PersonIdentityEntity) data.session.get(PersonIdentityEntity.class, supervisionEntity.getPersonIdentityId());
            if (pi != null) {
                nonAssoc.setInmateNum(pi.getOffenderNumber());
                nonAssoc.setFirstName(pi.getFirstName());
                nonAssoc.setLastName(pi.getLastName());
            }
            nonAssoc.setFacilityId(data.facilityId);
            nonAssoc.setFacilityInternalLocationId(data.facilityInternalLocationId);
            nonAssoc.setLocation(conflictLocation.getDescription());

            nonAssoc.setFacilityInternalLocationId(data.facilityInternalLocationId);
            if (custodyHearingEntity.getLocationId() != null) {
                InternalLocationEntity internalLocationEntity = (InternalLocationEntity) data.session.get(InternalLocationEntity.class,
                        custodyHearingEntity.getLocationId());
                if (internalLocationEntity != null) {
                    nonAssoc.setInmateLocation(internalLocationEntity.getDescription());
                }
            }

            List<String> conflictTypeList = getNonAssociationTypeList(data.uc, data.context, data.session, data.primarySupervisionEntity, supervisionEntity, lookupStart,
                    lookupEnd);
            //Handle for STG Non-Association Conflicts
            if (data.stgSupervisionEntityMap != null) {
                SupervisionEntity stgSupervision = data.stgSupervisionEntityMap.get(supervisionId);
                if (stgSupervision != null) {
                    conflictTypeList.add(NON_ASSOCIATION_STG);
                }
            }

            nonAssoc.setConflictTypes(conflictTypeList);

            if (custodyHearingEntity.getHearingType() != null) {
                nonAssoc.setConflictSubTypes(Arrays.asList(new String[] { hearingTypeMap.get(custodyHearingEntity.getHearingType()) }));
            }

            nonAssoc.setScheduleId(null);
            nonAssoc.setStartDateTime(getDateTime(data.sessionDate, data.startTime));
            nonAssoc.setEndDateTime(getDateTime(data.sessionDate, data.endTime));

            ret.add(nonAssoc);
        }

        return ret;
    }

    public static List<NonAssociationType> toNonAssociationTypeListFromHousingAssignment(UserContext uc, SessionContext context, Session session,
            SupervisionEntity primarySupervisionEntity, List<HousingBedMgmtActivityEntity> housingBedMgmtActivityEntityList,
            Map<Long, SupervisionEntity> supervisionEntityMap, Long facilityInternalLocationId, Map<Long, Long> childParentMap,
            Map<Long, SupervisionEntity> stgSupervisionEntityMap) {

        if (housingBedMgmtActivityEntityList == null || housingBedMgmtActivityEntityList.isEmpty()) {
            return null;
        }

        Long conflictLocationId = null;
        InternalLocationEntity conflictLocation = null;
        if (facilityInternalLocationId != null) {
            conflictLocationId = childParentMap.get(facilityInternalLocationId) != null ? childParentMap.get(facilityInternalLocationId) : facilityInternalLocationId;
        }
        if (conflictLocationId != null) {
            conflictLocation = (InternalLocationEntity) session.get(InternalLocationEntity.class, conflictLocationId);
        }

        List<NonAssociationType> ret = new ArrayList<>();

        for (HousingBedMgmtActivityEntity housingBedMgmtActivityEntity : housingBedMgmtActivityEntityList) {
            if (housingBedMgmtActivityEntity.getHousingAssignment() == null) {
                continue;
            }

            NonAssociationType nonAssoc = new NonAssociationType();
            Long supervisionId = housingBedMgmtActivityEntity.getSupervisionId();
            SupervisionEntity supervisionEntity = supervisionEntityMap.get(supervisionId);
            if (supervisionEntity == null) {
                continue;
            }
            nonAssoc.setPersonIdentityId(supervisionEntity.getPersonIdentityId());
            nonAssoc.setSupervisionId(supervisionEntity.getSupervisionIdentification());
            nonAssoc.setSupervisionDisplayId(supervisionEntity.getSupervisionDisplayID());
            nonAssoc.setPersonId(supervisionEntity.getPersonId());
            PersonIdentityEntity pi = (PersonIdentityEntity) session.get(PersonIdentityEntity.class, supervisionEntity.getPersonIdentityId());
            if (pi != null) {
                nonAssoc.setInmateNum(pi.getOffenderNumber());
                nonAssoc.setFirstName(pi.getFirstName());
                nonAssoc.setLastName(pi.getLastName());
            }
            nonAssoc.setFacilityId(housingBedMgmtActivityEntity.getFacilityId());
            nonAssoc.setFacilityInternalLocationId(facilityInternalLocationId);
            nonAssoc.setLocation(conflictLocation.getDescription());

            nonAssoc.setFacilityInternalLocationId(facilityInternalLocationId);
            if (housingBedMgmtActivityEntity != null) {
                InternalLocationEntity internalLocationEntity = (InternalLocationEntity) session.get(InternalLocationEntity.class,
                        housingBedMgmtActivityEntity.getHousingAssignment().getLocationTo());
                if (internalLocationEntity != null) {
                    nonAssoc.setInmateLocation(internalLocationEntity.getDescription());
                }
            }

            List<String> conflictTypeList = getNonAssociationTypeList(uc, context, session, primarySupervisionEntity, supervisionEntity, null, null);

            //Handle for STG Non-Association Conflicts
            if (stgSupervisionEntityMap != null) {
                SupervisionEntity stgSupervision = stgSupervisionEntityMap.get(supervisionId);
                if (stgSupervision != null) {
                    conflictTypeList.add(NON_ASSOCIATION_STG);
                }
            }

            nonAssoc.setConflictTypes(conflictTypeList);

            nonAssoc.setScheduleId(null);

            ret.add(nonAssoc);
        }

        return ret;
    }

    public static List<NonAssociationType> toNonAssociationTypeListFromHousingRequest(UserContext uc, SessionContext context, Session session,
            SupervisionEntity primarySupervisionEntity, List<HousingBedMgmtActivityEntity> housingBedMgmtActivityEntityList,
            Map<Long, SupervisionEntity> supervisionEntityMap, Long facilityInternalLocationId, Map<Long, Long> childParentMap,
            Map<Long, SupervisionEntity> stgSupervisionEntityMap) {

        if (housingBedMgmtActivityEntityList == null || housingBedMgmtActivityEntityList.isEmpty()) {
            return null;
        }

        Long conflictLocationId = null;
        InternalLocationEntity conflictLocation = null;
        if (facilityInternalLocationId != null) {
            conflictLocationId = childParentMap.get(facilityInternalLocationId) != null ? childParentMap.get(facilityInternalLocationId) : facilityInternalLocationId;
        }
        if (conflictLocationId != null) {
            conflictLocation = (InternalLocationEntity) session.get(InternalLocationEntity.class, conflictLocationId);
        }

        List<NonAssociationType> ret = new ArrayList<>();

        for (HousingBedMgmtActivityEntity housingBedMgmtActivityEntity : housingBedMgmtActivityEntityList) {
            if (housingBedMgmtActivityEntity.getHousingChangeRequest() == null) {
                continue;
            }

            NonAssociationType nonAssoc = new NonAssociationType();
            Long supervisionId = housingBedMgmtActivityEntity.getSupervisionId();
            SupervisionEntity supervisionEntity = supervisionEntityMap.get(supervisionId);
            if (supervisionEntity == null) {
                continue;
            }
            nonAssoc.setPersonIdentityId(supervisionEntity.getPersonIdentityId());
            nonAssoc.setSupervisionId(supervisionEntity.getSupervisionIdentification());
            nonAssoc.setSupervisionDisplayId(supervisionEntity.getSupervisionDisplayID());
            nonAssoc.setPersonId(supervisionEntity.getPersonId());
            PersonIdentityEntity pi = (PersonIdentityEntity) session.get(PersonIdentityEntity.class, supervisionEntity.getPersonIdentityId());
            if (pi != null) {
                nonAssoc.setInmateNum(pi.getOffenderNumber());
                nonAssoc.setFirstName(pi.getFirstName());
                nonAssoc.setLastName(pi.getLastName());
            }
            nonAssoc.setFacilityId(housingBedMgmtActivityEntity.getFacilityId());
            nonAssoc.setFacilityInternalLocationId(facilityInternalLocationId);
            nonAssoc.setLocation(conflictLocation.getDescription());

            nonAssoc.setFacilityInternalLocationId(facilityInternalLocationId);
            if (housingBedMgmtActivityEntity != null) {
                InternalLocationEntity internalLocationEntity = (InternalLocationEntity) session.get(InternalLocationEntity.class,
                        housingBedMgmtActivityEntity.getHousingChangeRequest().getLocationRequested());
                if (internalLocationEntity != null) {
                    nonAssoc.setInmateLocation(internalLocationEntity.getDescription());
                }
            }

            List<String> conflictTypeList = getNonAssociationTypeList(uc, context, session, primarySupervisionEntity, supervisionEntity, null, null);

            //Handle for STG Non-Association Conflicts
            if (stgSupervisionEntityMap != null) {
                SupervisionEntity stgSupervision = stgSupervisionEntityMap.get(supervisionId);
                if (stgSupervision != null) {
                    conflictTypeList.add(NON_ASSOCIATION_STG);
                }
            }

            nonAssoc.setConflictTypes(conflictTypeList);

            nonAssoc.setScheduleId(null);

            ret.add(nonAssoc);
        }

        return ret;
    }

    /**
     * this will replace its static counterpart
     *
     * @param data
     * @return
     */
    public List<VisitEntity> getVisitList(ConflictQueryDataHolder data) {
        return getVisitEntityList(data);
    }

    @Deprecated
    public static List<VisitEntity> getVisitEntityList(ConflictQueryDataHolder data) {

        Collection<Long> supervisionIdSet = data.supervisionEntityMap.keySet();
        if (data.facilityId == null || supervisionIdSet == null || supervisionIdSet.isEmpty() ||
                data.facilityInternalLocationIdSet == null || data.facilityInternalLocationIdSet.isEmpty()) {
            return null;
        }

        Criteria criteria = data.session.createCriteria(VisitEntity.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.add(Restrictions.eq("status", "SCHEDULED"));
        criteria.add(Restrictions.eq("facilityId", data.facilityId));

        Disjunction svDisjunction = QueryUtil.propertyIn("OffenderSupervisionId", supervisionIdSet);
        criteria.add(svDisjunction);

        Disjunction filDisjunction = QueryUtil.propertyIn("locationId", data.facilityInternalLocationIdSet);
        criteria.add(filDisjunction);

        QueryUtil.effectiveDateCriteria(criteria, "startTime", "endTime", new LocalDateTime().withFields(data.sessionDate).withFields(data.startTime),
                new LocalDateTime().withFields(data.sessionDate).withFields(data.endTime));

        @SuppressWarnings("unchecked") List<VisitEntity> ret = criteria.list();

        return ret;
    }

    public static List<HousingBedMgmtActivityEntity> getHousingAssignmentList(UserContext uc, SessionContext context, Session session, Set<Long> supervisionIdSet,
            Set<Long> facilityInternalLocationIdSet) {

        // Create criteria
        Criteria criteria = session.createCriteria(HousingBedMgmtActivityEntity.class, "housingActivity");

        criteria.add(Restrictions.in("supervisionId", supervisionIdSet));
        Criteria subCriteria = criteria.createCriteria("housingAssignment", "assignment");
        subCriteria.add(Restrictions.in("assignment.locationTo", facilityInternalLocationIdSet));
        subCriteria.add(Restrictions.eq("assignment.currentHousingFlag", true));
        criteria.setLockMode(LockMode.NONE);
        criteria.setCacheMode(CacheMode.IGNORE);

        List<HousingBedMgmtActivityEntity> list = criteria.list();

        return list;
    }

    public static List<HousingBedMgmtActivityEntity> getHousingRequestList(UserContext uc, SessionContext context, Session session, Set<Long> supervisionIdSet,
            Set<Long> facilityInternalLocationIdSet) {

        // Create criteria
        Criteria criteria = session.createCriteria(HousingBedMgmtActivityEntity.class, "housingActivity");

        criteria.add(Restrictions.in("supervisionId", supervisionIdSet));
        criteria.add(Restrictions.in("activityState", new String[] { HousingActivityState.APPR.value(), HousingActivityState.WAITL.value() }));
        Criteria subCriteria = criteria.createCriteria("housingChangeRequest", "changeRequest");
        subCriteria.add(Restrictions.in("changeRequest.locationRequested", facilityInternalLocationIdSet));
        criteria.setLockMode(LockMode.NONE);
        criteria.setCacheMode(CacheMode.IGNORE);

        List<HousingBedMgmtActivityEntity> list = criteria.list();

        return list;
    }

    /**
     * this will replace its static counterpart
     *
     * @param visitEntityList
     * @param data
     * @return
     */
    public List<NonAssociationType> toNonAssocTypeListFromVisitation(List<VisitEntity> visitEntityList, ConflictQueryDataHolder data) {
        return toNonAssociationTypeListFromVisitation(visitEntityList, data);
    }

    @Deprecated
    public static List<NonAssociationType> toNonAssociationTypeListFromVisitation(List<VisitEntity> visitEntityList, ConflictQueryDataHolder data) {
        if (visitEntityList == null || visitEntityList.isEmpty()) {
            return null;
        }

        Map<String, String> hearingTypeMap = ReferenceDataServiceAdapter.getReferenceCodesDescMap(data.uc, "VISITTYPE", "en");

        Long conflictLocationId = null;
        InternalLocationEntity conflictLocation = null;
        if (data.facilityInternalLocationId != null) {
            conflictLocationId = data.childParentMap.get(data.facilityInternalLocationId) != null ? data.childParentMap.get(data.facilityInternalLocationId) :
                    data.facilityInternalLocationId;
        }
        if (conflictLocationId != null) {
            conflictLocation = (InternalLocationEntity) data.session.get(InternalLocationEntity.class, conflictLocationId);
        }

        LocalDateTime lookupStart = new LocalDateTime().withFields(data.sessionDate).withFields(data.startTime);
        LocalDateTime lookupEnd = new LocalDateTime().withFields(data.sessionDate).withFields(data.endTime);

        List<NonAssociationType> ret = new ArrayList<>();

        for (VisitEntity entity : visitEntityList) {
            NonAssociationType nonAssoc = new NonAssociationType();
            if (entity.getOffenderSupervisionId() == null) {
                continue;
            }
            Long supervisionId = entity.getOffenderSupervisionId();
            SupervisionEntity supervisionEntity = data.supervisionEntityMap.get(supervisionId);
            if (supervisionEntity == null) {
                continue;
            }
            nonAssoc.setPersonIdentityId(supervisionEntity.getPersonIdentityId());
            nonAssoc.setSupervisionId(supervisionEntity.getSupervisionIdentification());
            nonAssoc.setSupervisionDisplayId(supervisionEntity.getSupervisionDisplayID());
            nonAssoc.setPersonId(supervisionEntity.getPersonId());
            PersonIdentityEntity pi = (PersonIdentityEntity) data.session.get(PersonIdentityEntity.class, supervisionEntity.getPersonIdentityId());
            if (pi != null) {
                nonAssoc.setInmateNum(pi.getOffenderNumber());
                nonAssoc.setFirstName(pi.getFirstName());
                nonAssoc.setLastName(pi.getLastName());
            }
            nonAssoc.setFacilityId(data.facilityId);
            nonAssoc.setFacilityInternalLocationId(data.facilityInternalLocationId);
            nonAssoc.setLocation(conflictLocation.getDescription());

            nonAssoc.setFacilityInternalLocationId(data.facilityInternalLocationId);
            if (entity.getLocationId() != null) {
                InternalLocationEntity internalLocationEntity = (InternalLocationEntity) data.session.get(InternalLocationEntity.class, entity.getLocationId());
                if (internalLocationEntity != null) {
                    nonAssoc.setInmateLocation(internalLocationEntity.getDescription());
                }
            }

            List<String> conflictTypeList = getNonAssociationTypeList(data.uc, data.context, data.session, data.primarySupervisionEntity, supervisionEntity, lookupStart,
                    lookupEnd);

            //Handle for STG Non-Association Conflicts
            if (data.stgSupervisionEntityMap != null) {
                SupervisionEntity stgSupervision = data.stgSupervisionEntityMap.get(supervisionId);
                if (stgSupervision != null) {
                    conflictTypeList.add(NON_ASSOCIATION_STG);
                }
            }

            nonAssoc.setConflictTypes(conflictTypeList);

            if (entity.getVisitType() != null) {
                nonAssoc.setConflictSubTypes(Arrays.asList(new String[] { hearingTypeMap.get(entity.getVisitType()) }));
            }

            nonAssoc.setScheduleId(null);
            nonAssoc.setStartDateTime(getDateTime(data.sessionDate, data.startTime));
            nonAssoc.setEndDateTime(getDateTime(data.sessionDate, data.endTime));

            ret.add(nonAssoc);
        }
        return ret;
    }

    // when ready, this method can call the activity service to return schedule objects instead of entities
    @Deprecated
    @SuppressWarnings("unchecked")
    public static List<ScheduleEntity> searchSchedule(UserContext uc, SessionContext context, Session session, Long facilityId, Set<Long> facilityInternalLocationIdSet,
            LocalDate sessionDate, LocalTime startTime, LocalTime endTime, RecurrencePatternType recurrencePattern) {

        if (facilityInternalLocationIdSet == null || facilityInternalLocationIdSet.isEmpty()) {
            return new ArrayList<>();
        }
        Set<Long> facilityIds = new HashSet<>();
        facilityIds.add(facilityId);
        Criteria c = ActivityQueryUtil.getScheduleCriteria(session, facilityIds, facilityInternalLocationIdSet, null, sessionDate, startTime, endTime,
                recurrencePattern, false);
        return c.list();
    }



    public static Criteria programOfferingConflictCriteria(UserContext uc, SessionContext context, Session session, Long supervisionId, LocalDate sessionDate,
            LocalTime startTime, LocalTime endTime, RecurrencePatternType recurrencePattern) {

        Criteria offeringCriteria = session.createCriteria(ProgramOfferingEntity.class);
        offeringCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        QueryUtil.effectiveDateCriteria(offeringCriteria, "startDate", "endDate", sessionDate, sessionDate);
        QueryUtil.effectiveDateCriteria(offeringCriteria, "startTime", "endTime", DateUtil.getZeroEpochTime(startTime), DateUtil.getZeroEpochTime(endTime));

        offeringCriteria.createCriteria("programAssignments", "assignment");
        offeringCriteria.add(Restrictions.eq("assignment.supervisionId", supervisionId));
        if (!(recurrencePattern instanceof OnceRecurrencePatternType)) {
            QueryUtil.effectiveDateCriteria(offeringCriteria, "assignment.startDate", "assignment.endDate", sessionDate, sessionDate);
        }

        Disjunction disjunction = Restrictions.disjunction();
        disjunction.add(Restrictions.eq("assignment.status", PROGRAM_ASSIGNMENT_ALLOCATED));
        disjunction.add(Restrictions.eq("assignment.status", PROGRAM_ASSIGNMENT_APPROVED));
        disjunction.add(Restrictions.eq("assignment.status", PROGRAM_ASSIGNMENT_REFERRED));
        offeringCriteria.add(disjunction);

        offeringCriteria.setProjection(Projections.property("scheduleId"));
        @SuppressWarnings("unchecked") List<Long> scheduleIds = offeringCriteria.list();
        if (scheduleIds.isEmpty()) {
            return null;
        }
        offeringCriteria.setProjection(null);

        Criteria scheduleCriteria = ActivityQueryUtil.getScheduleCriteria(session, null, null, null, sessionDate, startTime, endTime, recurrencePattern);
        scheduleCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        scheduleCriteria.add(Restrictions.in("scheduleIdentification", scheduleIds));
        scheduleCriteria.setProjection(Projections.id());
        @SuppressWarnings("unchecked") List<Long> schIds = scheduleCriteria.list();
        if (schIds.isEmpty()) {
            return null;
        }
        offeringCriteria.add(Restrictions.in("scheduleId", schIds));

        return offeringCriteria;
    }

    public static Criteria programOfferingAddedDateCriteria(UserContext uc, SessionContext context, Session session, Long supervisionId, LocalDate sessionDate,
            LocalTime startTime, LocalTime endTime, RecurrencePatternType recurrencePattern) {
        Criteria criteria = programOfferingConflictCriteria(uc, context, session, supervisionId, sessionDate, startTime, endTime, recurrencePattern);
        if (criteria == null) {
            return null;
        }
        criteria.createCriteria("addedDates", "aDate");
        criteria.add(Restrictions.or(Restrictions.isEmpty("addedDates"), Restrictions.and(Restrictions.eq("aDate.date", sessionDate.toDate())),
                Restrictions.and(Restrictions.le("aDate.startTime", DateUtil.getZeroEpochTime(startTime)),
                        Restrictions.or(Restrictions.isNull("aDate.endTime"), Restrictions.ge("aDate.endTime", DateUtil.getZeroEpochTime(endTime))))));
        criteria.add(Restrictions.ne("aDate.status", DELETED));

        return criteria;
    }

    public static Criteria programOfferingModifiedDateCriteria(UserContext uc, SessionContext context, Session session, Long supervisionId, LocalDate sessionDate,
            LocalTime startTime, LocalTime endTime, RecurrencePatternType recurrencePattern) {
        Criteria criteria = programOfferingConflictCriteria(uc, context, session, supervisionId, sessionDate, startTime, endTime, recurrencePattern);
        if (criteria == null) {
            return null;
        }
        criteria.createCriteria("modifiedDates", "mDate");
        criteria.add(Restrictions.or(Restrictions.isEmpty("modifiedDates"), Restrictions.and(Restrictions.eq("mDate.date", sessionDate.toDate())),
                Restrictions.and(Restrictions.le("mDate.startTime", DateUtil.getZeroEpochTime(startTime)),
                        Restrictions.or(Restrictions.isNull("mDate.endTime"), Restrictions.ge("mDate.endTime", DateUtil.getZeroEpochTime(endTime))))));
        criteria.add(Restrictions.ne("mDate.status", DELETED));

        return criteria;
    }

    public static List<ScheduleConflict> toScheduleConflictList(UserContext uc, SessionContext context, Session session, Long supervisionId,
            Criteria programOfferingCriteria, LocalDate sessionDate, LocalTime startTime, LocalTime endTime) {

        List<ScheduleConflict> ret = new ArrayList<>();
        if (programOfferingCriteria == null) {
            return ret;
        }

        programOfferingCriteria.setProjection(Projections.id());
        @SuppressWarnings("unchecked") List<Long> programOfferingIDs = programOfferingCriteria.list();
        if (programOfferingIDs == null || programOfferingIDs.isEmpty()) {
            return new ArrayList<>();
        }

        Criteria criteria = session.createCriteria(ProgramAssignmentEntity.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.add(Restrictions.eq("supervisionId", supervisionId));

        criteria.add(Restrictions.in("programOffering.offeringId", programOfferingIDs));
        criteria.createCriteria("programOffering", "offering");
        criteria.setProjection(Projections.projectionList().add(Projections.property("assignmentId")).add(Projections.property("supervisionId")).add(
                Projections.property("offering.facilityId")).add(Projections.property("offering.interanlLocationId")).add(
                Projections.property("offering.locationDescription")).add(Projections.property("offering.offeringDescription")).add(
                Projections.property("offering.startDate")).add(Projections.property("offering.startTime")).add(Projections.property("offering.endDate")).add(
                Projections.property("offering.endTime")));

        List<Object> rows = criteria.list();
        for (Object r : rows) {
            Object[] row = (Object[]) r;
            ScheduleConflict scheduleConflict = new ScheduleConflict();
            scheduleConflict.setEventId((Long) row[0]);
            scheduleConflict.setSupervisionId((Long) row[1]);
            scheduleConflict.setToFacilityId((Long) row[2]);
            scheduleConflict.setToLocationId((Long) row[3]);
            scheduleConflict.setLocation((String) row[4]);
            scheduleConflict.setDescription("Program: " + (String) row[5]);
            Date start = getDateTime(sessionDate.toDate(), (Date) row[7]);
            scheduleConflict.setStartDate(start);
            Date end = getDateTime(sessionDate.toDate(), (Date) row[9]);
            scheduleConflict.setEndDate(end);
            scheduleConflict.setType(ScheduleConflict.EventType.PROGRAM);
            if (!ret.contains(scheduleConflict)) {
                ret.add(scheduleConflict);
            }
        }
        return ret;
    }

    public static List<ScheduleConflict> queryAppointmentScheduleConflict(UserContext uc, SessionContext context, Session session, Long supervisionId,
            LocalDate sessionDate, LocalTime startTime, LocalTime endTime) {

        List<ScheduleConflict> ret = new ArrayList<>();

        Criteria criteria = session.createCriteria(AppointmentEntity.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.add(Restrictions.eq("supervisionId", supervisionId));
        LocalDateTime startDateTime = new LocalDateTime().withFields(sessionDate).withFields(startTime);
        LocalDateTime endDateTime = new LocalDateTime().withFields(sessionDate).withFields(endTime);
        QueryUtil.effectiveDateCriteria(criteria, "startTime", "endTime", startDateTime, endDateTime);
        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();
        if (count == null || count.longValue() == 0L) {
            return ret;
        }
        criteria.setProjection(Projections.projectionList().add(Projections.property("appointmentId")).add(Projections.property("supervisionId")).add(
                Projections.property("facilityId")).add(Projections.property("locationId")).add(Projections.property("type")).add(Projections.property("startTime")).add(
                Projections.property("endTime")));

        List<Object> rows = criteria.list();
        Map<String, String> appointmentTypeMap = ReferenceDataServiceAdapter.getReferenceCodesDescMap(uc, "APPOINTMENTTYPE", "en");
        for (Object r : rows) {
            Object[] row = (Object[]) r;
            ScheduleConflict scheduleConflict = new ScheduleConflict();
            scheduleConflict.setEventId((Long) row[0]);
            scheduleConflict.setSupervisionId((Long) row[1]);
            scheduleConflict.setToFacilityId((Long) row[2]);
            Long facilityInternalLocationId = (Long) row[3];
            scheduleConflict.setToLocationId(facilityInternalLocationId);
            InternalLocationEntity internalLocationEntity = (InternalLocationEntity) session.get(InternalLocationEntity.class, facilityInternalLocationId);
            scheduleConflict.setLocation(internalLocationEntity.getDescription());
            String description = appointmentTypeMap.get((String) row[4]);
            if (!description.endsWith("Appointment")) {
                description += " Appointment";
            }
            scheduleConflict.setDescription(description);
            scheduleConflict.setStartDate((Date) row[5]);
            scheduleConflict.setEndDate((Date) row[6]);
            scheduleConflict.setType(ScheduleConflict.EventType.APPOINTMENT);
            if (!ret.contains(scheduleConflict)) {
                ret.add(scheduleConflict);
            }
        }
        return ret;
    }

    public static String strDayOfWeek(int iDayOfWeek) {
        switch (iDayOfWeek) {
            case 1:
                return "MON";
            case 2:
                return "TUE";
            case 3:
                return "WED";
            case 4:
                return "THU";
            case 5:
                return "FRI";
            case 6:
                return "SAT";
            case 7:
                return "SUN";
        }
        return null;
    }

    public static int iDayOfWeek(DayOfWeekEnum dayOfWeek) {
        if (dayOfWeek == null) {
            return -1;
        }
        return dayOfWeek.ordinal() + 1;
    }






    private static Date getDateTime(Date date, Date time) {
        if (date == null) {
            return null;
        }
        if (time == null) {
            return new LocalDateTime().withFields(new LocalDate(date)).toDate();
        }
        return new LocalDateTime().withFields(new LocalDate(date)).withFields(new LocalTime(time)).toDateTime().toDate();
    }

    private static Date getDateTime(LocalDate date, LocalTime time) {
        if (date == null) {
            return null;
        }
        if (time == null) {
            return new LocalDateTime().withFields(date).toDate();
        }
        return new LocalDateTime().withFields(date).withFields(time).toDate();
    }

    public static Map<Long, SupervisionEntity> toSupervisionEntityMap(List<SupervisionEntity> stgSupervisionEntityList) {
        if (stgSupervisionEntityList == null || stgSupervisionEntityList.isEmpty()) {
            return null;
        }

        Map<Long, SupervisionEntity> supervisionEntityMap = new HashMap<Long, SupervisionEntity>();
        for (SupervisionEntity sup : stgSupervisionEntityList) {
            if (sup != null) {
                supervisionEntityMap.put(sup.getSupervisionIdentification(), sup);
            }
        }

        return supervisionEntityMap;
    }

    public static Map<String, Integer[]> getConfigMovementDurationMap(Session session) {
        Criteria criteria = session.createCriteria(ConfigMovementDurationEntity.class);
        criteria.setMaxResults(20);
        List<ConfigMovementDurationEntity> durationEntityList = criteria.list();
        Map<String, Integer[]> map = new HashMap<>();
        if (durationEntityList != null) {
            for (ConfigMovementDurationEntity entity : durationEntityList) {
                String movementType = entity.getMovementType();
                String duration = entity.getDuration();
                if (movementType == null || duration == null || !Pattern.compile("^[0-9]?[0-9]:[0-9][0-9]$").matcher(duration).find()
                        || Integer.valueOf(duration.split(":")[0]) < 0 || Integer.valueOf(duration.split(":")[0]) >= 24 || Integer.valueOf(duration.split(":")[1]) < 0
                        || Integer.valueOf(duration.split(":")[1]) >= 60) {
                    continue;
                }
                String[] hhmm = duration.split(":");
                Integer hh = Integer.valueOf(hhmm[0]);
                Integer mm = Integer.valueOf(hhmm[1]);
                Integer[] iHHMM = new Integer[] { hh, mm };
                switch (movementType.toUpperCase()) {
                    case "HEARINGS":
                        map.put(NonAssociationType.EventType.HEARING.name(), iHHMM);
                        break;
                    case "INTERNAL":
                        map.put(NonAssociationType.EventType.INTERNAL_MOVEMENT.name(), iHHMM);
                        break;
                    case "TRANSFERS":
                        map.put(NonAssociationType.EventType.TRN_EXTERNAL_MOVEMENT.name(), iHHMM);
                        break;
                }
            }
        }
        return map;
    }


    public static CodeType metaData(NonAssociationType.EventType eventType) {
        if (eventType == null) {
            return null;
        }

        if (eventType.equals(NonAssociationType.EventType.TAP_EXTERNAL_MOVEMENT)) {
            return new CodeType("MOVEMENTTYPE", "TAP");
        } else if (eventType.equals(NonAssociationType.EventType.CRT_EXTERNAL_MOVEMENT)) {
            return new CodeType("MOVEMENTTYPE", "CRT");
        } else if (eventType.equals(NonAssociationType.EventType.TRN_EXTERNAL_MOVEMENT)) {
            return new CodeType("MOVEMENTTYPE", "TRNIJ");
        } else if (eventType.equals(NonAssociationType.EventType.REL_EXTERNAL_MOVEMENT)) {
            return new CodeType("MOVEMENTTYPE", "REL");
        } else if (eventType.equals(NonAssociationType.EventType.INTERNAL_MOVEMENT)) {
            return new CodeType("MOVEMENTTYPE", "APP");
        } else if (eventType.equals(NonAssociationType.EventType.VISIT)) {
            CodeType codeType = new CodeType();
            codeType.setSet("VISITTYPE");
            return codeType;
        } else if (eventType.equals(NonAssociationType.EventType.APPOINTMENT)) {
            CodeType codeType = new CodeType();
            codeType.setSet("APPOINTMENTTYPE");
            return codeType;
        } else if (eventType.equals(NonAssociationType.EventType.HEARING)) {
            CodeType codeType = new CodeType();
            codeType.setSet("HEARINGTYPE");
            return codeType;
        } else if (eventType.equals(NonAssociationType.EventType.PROGRAM)) {
            CodeType codeType = new CodeType();
            codeType.setSet("PROGRAM");
            return codeType;
        } else if (eventType.equals(NonAssociationType.EventType.SENTENCE)) {
            CodeType codeType = new CodeType();
            codeType.setSet("CASEACTIVITYTYPE");
            return codeType;
        } else {
            return null;
        }
    }

    private static List<String> getNonAssociationTypeList(UserContext uc, SessionContext context, Session session, SupervisionEntity primarySupervisionEntity,
            SupervisionEntity supervisionEntity, LocalDateTime lookupStart, LocalDateTime lookupEnd) {

        Criteria criteria = session.createCriteria(PersonNonAssociationEntity.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.setCacheable(true);
        criteria.setProjection(Projections.property("nonAssociationType"));
        criteria.add(Restrictions.or(Restrictions.eq("personIdA", primarySupervisionEntity.getPersonId()),
                Restrictions.eq("personIdA", supervisionEntity.getPersonId())));
        criteria.add(Restrictions.or(Restrictions.eq("personIdB", primarySupervisionEntity.getPersonId()),
                Restrictions.eq("personIdB", supervisionEntity.getPersonId())));
        QueryUtil.effectiveDateCriteria(criteria, "effectiveDate", "expiryDate", lookupStart, lookupEnd);
        @SuppressWarnings("unchecked") List<String> ret = criteria.list();
        ret = new ArrayList<>(new HashSet<>(ret));
        return ret;
    }

}