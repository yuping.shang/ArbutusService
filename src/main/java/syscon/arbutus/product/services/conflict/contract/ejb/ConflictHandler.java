package syscon.arbutus.product.services.conflict.contract.ejb;

import javax.ejb.SessionContext;
import javax.validation.constraints.NotNull;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.activity.contract.dto.DailyWeeklyCustomRecurrencePatternType;
import syscon.arbutus.product.services.activity.contract.dto.DayOfWeekEnum;
import syscon.arbutus.product.services.activity.contract.dto.OnceRecurrencePatternType;
import syscon.arbutus.product.services.activity.contract.dto.RecurrencePatternType;
import syscon.arbutus.product.services.activity.realization.persistence.ScheduleEntity;
import syscon.arbutus.product.services.conflict.contract.dto.ConflictQueryDataHolder;
import syscon.arbutus.product.services.conflict.contract.dto.NonAssociationType;
import syscon.arbutus.product.services.conflict.contract.dto.NonAssociationType.EventType;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.facilityinternallocation.realization.persistence.InternalLocationEntity;
import syscon.arbutus.product.services.housing.realization.persistence.HousingBedMgmtActivityEntity;
import syscon.arbutus.product.services.individualassociationsearch.contract.dto.SupervisionNonAssociationType;
import syscon.arbutus.product.services.movement.contract.dto.ScheduleConflict;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.supervision.contract.interfaces.SupervisionService;
import syscon.arbutus.product.services.supervision.realization.persistence.SupervisionEntity;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class ConflictHandler {
    private static Logger log = LoggerFactory.getLogger(ConflictHandler.class);

    private SessionContext context;
    private Session session;
    private SupervisionService supervisionService;

    public ConflictHandler(SessionContext context, Session session, final SupervisionService supervisionService) {
        super();
        this.context = context;
        this.session = session;
        this.supervisionService = supervisionService;
    }

    public List<NonAssociationType> checkNonAssociationConflictByOnceAndDailyPattern(UserContext uc, Long primarySupervisionId, Set<Long> supervisionIds, Long facilityId,
            Long facilityInternalLocationId, LocalDate sessionDate, LocalTime startTime, LocalTime endTime, EventType event, String referenceSet, String referenceCode,
            Set<DayOfWeekEnum> daysOfWeek) {

        List<NonAssociationType> ret = new ArrayList<>();

        Set<NonAssociationType> nonAssociationSet = new HashSet<>();
        Set<Long> facilityInternalLocationIdSet = new HashSet<>();
        // Once Time Pattern
        OnceRecurrencePatternType onceTimePattern = new OnceRecurrencePatternType(sessionDate.toDate());
        List<NonAssociationType> nonAssociations = checkNonAssociationConflictByPattern(uc, primarySupervisionId, supervisionIds, facilityId, facilityInternalLocationId,
                facilityInternalLocationIdSet, sessionDate, startTime, endTime, event, referenceSet, referenceCode, onceTimePattern);
        if (nonAssociations != null && !nonAssociations.isEmpty()) {
            nonAssociationSet.addAll(nonAssociations);
        }

        // Daily Pattern
        Set<String> dayOfWeekSet = new HashSet<>();
        if (daysOfWeek != null && !daysOfWeek.isEmpty()) {
            for (DayOfWeekEnum dw : daysOfWeek) {
                dayOfWeekSet.add(dw.name());
            }
        } else {
            int dayOfWeek = sessionDate.getDayOfWeek();
            dayOfWeekSet.add(ConflictHelper.strDayOfWeek(dayOfWeek));
        }
        DailyWeeklyCustomRecurrencePatternType dailyPattern = new DailyWeeklyCustomRecurrencePatternType(1L, false, dayOfWeekSet);
        nonAssociations = checkNonAssociationConflictByPattern(uc, primarySupervisionId, supervisionIds, facilityId, facilityInternalLocationId,
                facilityInternalLocationIdSet, sessionDate, startTime, endTime, event, referenceSet, referenceCode, dailyPattern);
        if (nonAssociations != null && !nonAssociations.isEmpty()) {
            nonAssociationSet.addAll(nonAssociations);
        }
        if (!nonAssociationSet.isEmpty()) {
            ret.addAll(nonAssociationSet);
        }
        ret = new ArrayList<>(new HashSet<>(ret));
        Collections.sort(ret);
        return ret;
    }

    public List<NonAssociationType> checkNonAssociationConflictByPattern(UserContext uc, @NotNull Long primarySupervisionId, Set<Long> supervisionIds,
            @NotNull Long facilityId, @NotNull Long facilityInternalLocationId, Set<Long> facilityInternalLocationIdSet, @NotNull LocalDate sessionDate,
            @NotNull LocalTime startTime, @NotNull LocalTime endTime, @NotNull EventType event, String referenceSet, String referenceCode,
            RecurrencePatternType recurrencePattern) {

        String functionName = "checkNonAssociationConflict";

        if (primarySupervisionId == null || facilityId == null || sessionDate == null || startTime == null || event == null) {
            String message = "Invalid Arguments: supervisionId, facilityId, sessionDate, startTime and eventTime are required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        if (recurrencePattern != null) {
            ValidationHelper.validate(recurrencePattern);
        }

        if (endTime == null && !(event.name().equals(EventType.INTERNAL_MOVEMENT.name()) || event.name().equals(EventType.TRN_EXTERNAL_MOVEMENT.name())
                || event.name().equals(EventType.HEARING.name()) || !event.name().equals(EventType.REL_EXTERNAL_MOVEMENT))) {
            String message = "Invalid Arguments: eventTime cannot be null unless event type is Internal Movement, Transfers or Hearing.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        SupervisionEntity primarySupervisionEntity = BeanHelper.getEntity(session, SupervisionEntity.class, primarySupervisionId);

        List<NonAssociationType> ret = new ArrayList<>();

        LocalDateTime lookupStartDateTime = new LocalDateTime().withFields(sessionDate).withFields(startTime);
        LocalDateTime lookupEndDateTime;
        Map<String, Integer[]> durationMap = ConflictHelper.getConfigMovementDurationMap(session);
        if (endTime != null) {
            lookupEndDateTime = new LocalDateTime().withFields(sessionDate).withFields(endTime);
        } else {
            if (event != null && durationMap.get(event.name()) != null && durationMap.get(event.name()).length > 0) {
                lookupEndDateTime = new LocalDateTime().withFields(sessionDate).withFields(startTime).plusHours(durationMap.get(event.name())[0]);
            } else {
                lookupEndDateTime = new LocalDateTime().withFields(sessionDate).withFields(new LocalTime(23, 59, 0));
            }
        }

        // Direct Non-Association
        List<SupervisionEntity> supervisionEntityList = getNonAssociationSupervisionsByPrimarySupervisionId(uc, context, session, primarySupervisionId,
                lookupStartDateTime, lookupEndDateTime);

        if (supervisionEntityList == null || supervisionEntityList.isEmpty()) {
            supervisionEntityList = new ArrayList<>();
        }
        // STG Non-Association
        List<SupervisionEntity> stgSupervisionEntityList = ConflictHelper.getNonAssociationSupervisionsSTG(uc, context, session, primarySupervisionId,
                lookupStartDateTime, lookupEndDateTime);

        Map<Long, SupervisionEntity> stgSupervisionEntityMap = ConflictHelper.toSupervisionEntityMap(stgSupervisionEntityList);

        if (stgSupervisionEntityList != null && !stgSupervisionEntityList.isEmpty()) {
            supervisionEntityList.addAll(stgSupervisionEntityList);

        }
        if (supervisionEntityList.isEmpty()) {
            return ret;
        }

        Set<Long> nasSupervisionIds = new HashSet<>();
        Map<Long, SupervisionEntity> supervisionEntityMap = new HashMap<>();
        for (SupervisionEntity supervisionEntity : supervisionEntityList) {
            Long svId = supervisionEntity.getSupervisionIdentification();
            supervisionEntityMap.put(svId, supervisionEntity);
            if (supervisionIds != null && !supervisionIds.isEmpty() && supervisionIds.contains(svId)) {
                nasSupervisionIds.add(svId);
            }
        }

        Map<Long, Long> childParentMap = new HashMap<>();
        if (facilityInternalLocationIdSet == null || facilityInternalLocationIdSet.isEmpty()) {
            facilityInternalLocationIdSet = ConflictHelper.getLocationsToBeChecked(uc, context, session, facilityId, facilityInternalLocationId, childParentMap,
                    lookupStartDateTime, lookupEndDateTime);
        }
        if (facilityInternalLocationIdSet == null || facilityInternalLocationIdSet.isEmpty()) {
            facilityInternalLocationIdSet = new HashSet<>();
        }

        // facilityInternalLocationIdSet.add(facilityInternalLocationId);
        if ((facilityInternalLocationIdSet != null && !facilityInternalLocationIdSet.isEmpty() || EventType.CRT_EXTERNAL_MOVEMENT.name().equals(event.name())
                || EventType.TAP_EXTERNAL_MOVEMENT.name().equals(event.name()) || EventType.TRN_EXTERNAL_MOVEMENT.name().equals(event.name()))
                && nasSupervisionIds != null && !nasSupervisionIds.isEmpty()) {
            List<NonAssociationType> nonAssociations = ConflictHelper.toNonAssociationTypeList(uc, context, session, primarySupervisionEntity, nasSupervisionIds,
                    facilityId, facilityInternalLocationId, childParentMap, stgSupervisionEntityMap, sessionDate, startTime, endTime, event, referenceSet, referenceCode);
            if (nonAssociations != null && !nonAssociations.isEmpty()) {
                ret.addAll(nonAssociations);
            }
        }

        List<ScheduleEntity> scheduleEntityList = ConflictHelper.searchSchedule(uc, context, session, facilityId, facilityInternalLocationIdSet, sessionDate, startTime,
                endTime, recurrencePattern);
        Map<Long, ScheduleEntity> scheduleEntityMap = new HashMap<>();
        for (ScheduleEntity scheduleEntity : scheduleEntityList) {
            scheduleEntityMap.put(scheduleEntity.getScheduleIdentification(), scheduleEntity);
        }

        ConflictQueryDataHolder data = new ConflictQueryDataHolder();
        data.uc = uc;
        data.session = session;
        data.context = context;
        data.supervisionEntityMap = supervisionEntityMap;
        data.scheduleEntityMap = scheduleEntityMap;
        data.facilityId = facilityId;
        data.facilityInternalLocationIdSet = facilityInternalLocationIdSet;
        data.sessionDate = sessionDate;
        data.startTime = startTime;
        data.endTime = endTime;
        if (endTime == null) {
            if (event != null && durationMap.get(event.name()) != null && durationMap.get(event.name()).length > 0) {
                data.endTime = new LocalTime().withFields(startTime).plusHours(durationMap.get(event.name())[0]);
            }
        }
        data.primarySupervisionEntity = primarySupervisionEntity;
        data.childParentMap = childParentMap;
        data.stgSupervisionEntityMap = stgSupervisionEntityMap;
        data.facilityInternalLocationId = facilityInternalLocationId;

        List<NonAssociationType> temp;

        ConflictHelper conflictHelper = new ConflictHelper();

        /**
         * Check Non-Association Program Offering/Assignment
         */
        temp = conflictHelper.toNonAssocTypeListFromProgramAssignment(conflictHelper.getProgramAssignmentList(data), data);
        if (temp != null)
            ret.addAll(temp);

        /**
         * Check for Appointment
         */
        temp = conflictHelper.toNonAssocTypeListFromAppointment(conflictHelper.getAppointmentEntityList(data), data);
        if (temp != null)
            ret.addAll(temp);

        /**
         * Check for CRT
         */
        temp = conflictHelper.toNonAssocTypeListFromExternalMovement(conflictHelper.getMovementActivityList(data, durationMap.get(event.name())), data, event);
        if (temp != null)
            ret.addAll(temp);

        /**
         * Check for TAP Movement
         */
        temp = conflictHelper.toNonAssocTypeListFromExternalMovement(conflictHelper.getMovementActivityList(data, durationMap.get(event.name())), data, event);
        if (temp != null)
            ret.addAll(temp);

        /**
         * Check for Transfer Movement
         */
        if (event != null && event.equals(EventType.TRN_EXTERNAL_MOVEMENT)) {
            temp = conflictHelper.toNonAssocTypeListFromExternalMovement(
                    conflictHelper.getMovementActivityList(data, durationMap.get(EventType.TRN_EXTERNAL_MOVEMENT.name())), data, event);
            if (temp != null)
                ret.addAll(temp);

            temp = conflictHelper.toNonAssocTypeListFromTransWaitList(data, durationMap.get(EventType.TRN_EXTERNAL_MOVEMENT.name()), event);
            if (temp != null)
                ret.addAll(temp);
        }

        /**
         * Check for Internal Movement
         */
        temp = conflictHelper.toNonAssocTypeListFromInternalMovement(
                conflictHelper.getInternalMovementActivityList(data, durationMap.get(EventType.INTERNAL_MOVEMENT.name())), data);
        if (temp != null)
            ret.addAll(temp);

        /**
         * Check for Hearing
         */
        temp = conflictHelper.toNonAssocTypeListFromHearing(conflictHelper.getOffenseInCustodyHearingList(data, durationMap.get(EventType.HEARING.name())), data);

        if (temp != null)
            ret.addAll(temp);

        /**
         * Check for Visitation
         */

        temp = conflictHelper.toNonAssocTypeListFromVisitation(conflictHelper.getVisitList(data), data);

        if (temp != null)
            ret.addAll(temp);

        return ret;
    }

    public List<ScheduleConflict> checkScheduleConflict(UserContext uc, Long supervisionId, List<ScheduleConflict.EventType> checkTypes,
            @NotNull LocalDateTime startDateTime, @NotNull LocalDateTime endDateTime) {

        List<ScheduleConflict> ret = new ArrayList<>();
        LocalDateTime start = new LocalDateTime(startDateTime);
        while (!start.isAfter(endDateTime)) {
            LocalDate sessionDate = start.toLocalDate();
            LocalTime startTime = start.toLocalTime();
            LocalTime endTime = endDateTime.toLocalTime();
            List<ScheduleConflict> scheduleConflictList = checkScheduleConflict(uc, supervisionId, checkTypes, sessionDate, startTime, endTime);
            if (scheduleConflictList != null && !scheduleConflictList.isEmpty()) {
                ret.addAll(scheduleConflictList);
            }
            start = start.plusDays(1);
        }
        return ret;
    }

    public List<ScheduleConflict> checkScheduleConflict(UserContext uc, Long supervisionId, List<ScheduleConflict.EventType> checkTypes, @NotNull LocalDate sessionDate,
            @NotNull LocalTime startTime, @NotNull LocalTime endTime) {
        List<ScheduleConflict> ret = new ArrayList<>();

        if (checkTypes == null || checkTypes.isEmpty() || checkTypes.contains(ScheduleConflict.EventType.PROGRAM)) {
            OnceRecurrencePatternType onceTimePattern = new OnceRecurrencePatternType(sessionDate.toDate());
            List<ScheduleConflict> onceTimeScheduleConflictList = checkScheduleConflictByPattern(uc, supervisionId, checkTypes, sessionDate, startTime, endTime,
                    onceTimePattern);
            if (onceTimeScheduleConflictList != null && !onceTimeScheduleConflictList.isEmpty()) {
                ret.addAll(onceTimeScheduleConflictList);
            }

            Set<String> dayOfWeekSet = new HashSet<>();
            int dayOfWeek = sessionDate.getDayOfWeek();
            dayOfWeekSet.add(ConflictHelper.strDayOfWeek(dayOfWeek));
            DailyWeeklyCustomRecurrencePatternType dailyPattern = new DailyWeeklyCustomRecurrencePatternType(1L, false, dayOfWeekSet);
            List<ScheduleConflict> dailyScheduleConflictList = checkScheduleConflictByPattern(uc, supervisionId, checkTypes, sessionDate, startTime, endTime,
                    dailyPattern);
            if (dailyScheduleConflictList != null && !dailyScheduleConflictList.isEmpty()) {
                ret.addAll(dailyScheduleConflictList);
            }
        }

        return ret;
    }

    public List<ScheduleConflict> checkScheduleConflictByPattern(UserContext uc, Long supervisionId, List<ScheduleConflict.EventType> checkTypes,
            @NotNull LocalDate sessionDate, @NotNull LocalTime startTime, @NotNull LocalTime endTime, RecurrencePatternType recurrencePattern) {
        String functionName = "checkScheduleConflictByPattern";

        if (supervisionId == null || sessionDate == null || startTime == null || endTime == null) {
            String message = "Invalid Arguments: supervisionId, sessionDate, startTime and endTime are required.";
            LogHelper.error(log, functionName, message);
            throw new InvalidInputException(message);
        }

        if (recurrencePattern != null) {
            ValidationHelper.validate(recurrencePattern);
        }

        List<ScheduleConflict> ret = new ArrayList<>();
        if (checkTypes == null || checkTypes.isEmpty() || checkTypes.contains(ScheduleConflict.EventType.PROGRAM)) {
            Criteria programOfferingCriteria = ConflictHelper.programOfferingConflictCriteria(uc, context, session, supervisionId, sessionDate, startTime, endTime,
                    recurrencePattern);
            List<ScheduleConflict> scheduleConflictList = ConflictHelper.toScheduleConflictList(uc, context, session, supervisionId, programOfferingCriteria, sessionDate,
                    startTime, endTime);
            if (scheduleConflictList != null && !scheduleConflictList.isEmpty()) {
                ret.addAll(scheduleConflictList);
            }
        }

        return ret;
    }

    public List<NonAssociationType> checkNonAssociationConflictForHousingMovement(UserContext uc, @NotNull Long primarySupervisionId, Set<Long> supervisionIds,
            @NotNull Long facilityInternalLocationId, LocalDateTime startDateTime, LocalDateTime endDateTime) {

        List<NonAssociationType> ret = new ArrayList<>();

        SupervisionEntity primarySupervisionEntity = BeanHelper.getEntity(session, SupervisionEntity.class, primarySupervisionId);
        if (primarySupervisionEntity == null) {
            return ret;
        }

        // Direct Non-Association
        List<SupervisionEntity> supervisionEntityList = getNonAssociationSupervisionsByPrimarySupervisionId(uc, context, session, primarySupervisionId, startDateTime,
                endDateTime);

        if (supervisionEntityList == null || supervisionEntityList.isEmpty()) {
            supervisionEntityList = new ArrayList<>();
        }
        // STG Non-Association
        List<SupervisionEntity> stgSupervisionEntityList = ConflictHelper.getNonAssociationSupervisionsSTG(uc, context, session, primarySupervisionId, startDateTime,
                endDateTime);

        Map<Long, SupervisionEntity> stgSupervisionEntityMap = ConflictHelper.toSupervisionEntityMap(stgSupervisionEntityList);

        if (stgSupervisionEntityList != null && !stgSupervisionEntityList.isEmpty()) {
            supervisionEntityList.addAll(stgSupervisionEntityList);

        }
        if (supervisionEntityList.isEmpty()) {
            return ret;
        }

        Map<Long, SupervisionEntity> supervisionEntityMap = new HashMap<>();
        for (SupervisionEntity supervisionEntity : supervisionEntityList) {
            supervisionEntityMap.put(supervisionEntity.getSupervisionIdentification(), supervisionEntity);
        }

        Set<Long> nasSupervisionIds = new HashSet<>();
        for (SupervisionEntity supervisionEntity : supervisionEntityList) {
            Long svId = supervisionEntity.getSupervisionIdentification();
            supervisionEntityMap.put(svId, supervisionEntity);
            if (supervisionIds != null && !supervisionIds.isEmpty() && supervisionIds.contains(svId)) {
                nasSupervisionIds.add(svId);
            }
        }

        Map<Long, Long> childParentMap = new HashMap<>();
        Set<Long> facilityInternalLocationIdSet = ConflictHelper.getLocationsToBeChecked(uc, context, session, facilityInternalLocationId, childParentMap, startDateTime,
                endDateTime);
        if (facilityInternalLocationIdSet == null || facilityInternalLocationIdSet.isEmpty()) {
            return ret;
        }

        if (facilityInternalLocationId != null && nasSupervisionIds != null && !nasSupervisionIds.isEmpty()) {
            Long facilityId = BeanHelper.getEntity(session, InternalLocationEntity.class, facilityInternalLocationId).getFacilityId();
            List<NonAssociationType> nonAssociations = ConflictHelper.toNonAssociationTypeList(uc, context, session, primarySupervisionEntity, nasSupervisionIds,
                    facilityId, facilityInternalLocationId, childParentMap, stgSupervisionEntityMap, null, null, null, null, null, null);
            if (nonAssociations != null && !nonAssociations.isEmpty()) {
                ret.addAll(nonAssociations);
            }
        }

        // Housing assignment
        List<HousingBedMgmtActivityEntity> housingAssignmentList = ConflictHelper.getHousingAssignmentList(uc, context, session, supervisionEntityMap.keySet(),
                facilityInternalLocationIdSet);
        if (housingAssignmentList != null && !housingAssignmentList.isEmpty()) {
            List<NonAssociationType> nonAssociationForHousingAssignment = ConflictHelper.toNonAssociationTypeListFromHousingAssignment(uc, context, session,
                    primarySupervisionEntity, housingAssignmentList, supervisionEntityMap, facilityInternalLocationId, childParentMap, stgSupervisionEntityMap);
            if (nonAssociationForHousingAssignment != null && !nonAssociationForHousingAssignment.isEmpty()) {
                ret.addAll(nonAssociationForHousingAssignment);
            }
        }

        // Housing request
        List<HousingBedMgmtActivityEntity> housingRequestList = ConflictHelper.getHousingRequestList(uc, context, session, supervisionEntityMap.keySet(),
                facilityInternalLocationIdSet);
        if (housingRequestList != null && !housingRequestList.isEmpty()) {
            List<NonAssociationType> nonAssociationForHousingRequest = ConflictHelper.toNonAssociationTypeListFromHousingRequest(uc, context, session,
                    primarySupervisionEntity, housingRequestList, supervisionEntityMap, facilityInternalLocationId, childParentMap, stgSupervisionEntityMap);
            if (nonAssociationForHousingRequest != null && !nonAssociationForHousingRequest.isEmpty()) {
                ret.addAll(nonAssociationForHousingRequest);
            }
        }

        ret = new ArrayList<>(new HashSet<>(ret));
        ret.stream().sorted();
        return ret;
    }

    public List<SupervisionEntity> getNonAssociationSupervisionsByPrimarySupervisionId(UserContext uc, SessionContext context, Session session, Long primarySupervisionId,
            LocalDateTime lookupStartDateTime, LocalDateTime lookupEndDateTime) {
        List<SupervisionNonAssociationType> list = supervisionService.getOffenderNonAssociation(uc, primarySupervisionId,
                (lookupStartDateTime == null ? null : lookupStartDateTime.toDate()), (lookupEndDateTime == null ? null : lookupEndDateTime.toDate()));
        List<SupervisionEntity> ret = new ArrayList<>(list.size());
        for (SupervisionNonAssociationType supervisionNonAssociationType : list) {
            ret.add((SupervisionEntity) session.get(SupervisionEntity.class, supervisionNonAssociationType.getSupervisionId()));
        }

        return ret;
    }
}