package syscon.arbutus.product.services.conflict.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * NonAssociationType for Non Association Conflict Check
 *
 * @author YShang
 * @since March 05, 2015
 */
public class NonAssociationType implements Comparable<NonAssociationType>, Serializable {

    private static final long serialVersionUID = 4854285804049711537L;

    @NotNull
    private Long personIdentityId;
    private Long supervisionId;
    private String inmateNum;
    private String supervisionDisplayId;
    private Long personId;
    private Long facilityId;
    private Long facilityInternalLocationId;
    private Long locationId;

    private String firstName;
    private String lastName;

    private Long scheduleId;
    private Date startDateTime;
    private Date endDateTime;
    private List<String> conflictTypes;
    private List<String> conflictTypeDescriptions;
    private List<String> conflictSubTypes;

    private String location;
    private String description;

    private String inmateLocation;

    public NonAssociationType() {
        super();
    }

    public Long getPersonIdentityId() {
        return personIdentityId;
    }

    public void setPersonIdentityId(Long personIdentityId) {
        this.personIdentityId = personIdentityId;
    }

    public Long getSupervisionId() {
        return supervisionId;
    }

    public void setSupervisionId(Long supervisionId) {
        this.supervisionId = supervisionId;
    }

    public String getInmateNum() {
        return inmateNum;
    }

    public void setInmateNum(String inmateNum) {
        this.inmateNum = inmateNum;
    }

    public String getSupervisionDisplayId() {
        return supervisionDisplayId;
    }

    public void setSupervisionDisplayId(String supervisionDisplayId) {
        this.supervisionDisplayId = supervisionDisplayId;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    public Long getFacilityInternalLocationId() {
        return facilityInternalLocationId;
    }

    public void setFacilityInternalLocationId(Long facilityInternalLocationId) {
        this.facilityInternalLocationId = facilityInternalLocationId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public List<String> getConflictTypes() {
        return conflictTypes;
    }

    public void setConflictTypes(List<String> conflictTypes) {
        this.conflictTypes = conflictTypes;
    }

    public List<String> getConflictTypeDescriptions() {
        return conflictTypeDescriptions;
    }

    public void setConflictTypeDescriptions(List<String> conflictTypeDescriptions) {
        this.conflictTypeDescriptions = conflictTypeDescriptions;
    }

    public List<String> getConflictSubTypes() {
        return conflictSubTypes;
    }

    public void setConflictSubTypes(List<String> conflictSubTypes) {
        this.conflictSubTypes = conflictSubTypes;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInmateLocation() {
        return inmateLocation;
    }

    public void setInmateLocation(String inmateLocation) {
        this.inmateLocation = inmateLocation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (o == null || getClass() != o.getClass()) {
			return false;
		}

        NonAssociationType that = (NonAssociationType) o;

        if (personIdentityId != null ? !personIdentityId.equals(that.personIdentityId) : that.personIdentityId != null) {
			return false;
		}
        if (supervisionId != null ? !supervisionId.equals(that.supervisionId) : that.supervisionId != null) {
			return false;
		}
        if (inmateNum != null ? !inmateNum.equals(that.inmateNum) : that.inmateNum != null) {
			return false;
		}
        if (supervisionDisplayId != null ? !supervisionDisplayId.equals(that.supervisionDisplayId) : that.supervisionDisplayId != null) {
			return false;
		}
        if (personId != null ? !personId.equals(that.personId) : that.personId != null) {
			return false;
		}
        if (facilityId != null ? !facilityId.equals(that.facilityId) : that.facilityId != null) {
			return false;
		}
        if (facilityInternalLocationId != null ? !facilityInternalLocationId.equals(that.facilityInternalLocationId) : that.facilityInternalLocationId != null) {
			return false;
		}
        if (locationId != null ? !locationId.equals(that.locationId) : that.locationId != null) {
			return false;
		}
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) {
			return false;
		}
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) {
			return false;
		}
        if (scheduleId != null ? !scheduleId.equals(that.scheduleId) : that.scheduleId != null) {
			return false;
		}
        if (startDateTime != null ? !startDateTime.equals(that.startDateTime) : that.startDateTime != null) {
			return false;
		}
//        if (endDateTime != null ? !endDateTime.equals(that.endDateTime) : that.endDateTime != null) return false;
        if (conflictTypes != null ? !conflictTypes.equals(that.conflictTypes) : that.conflictTypes != null) {
			return false;
		}
        if (conflictTypeDescriptions != null ? !conflictTypeDescriptions.equals(that.conflictTypeDescriptions) : that.conflictTypeDescriptions != null) {
			return false;
		}
//        if (conflictSubTypes != null ? !conflictSubTypes.equals(that.conflictSubTypes) : that.conflictSubTypes != null) {
//			return false;
//		}
        if (location != null ? !location.equals(that.location) : that.location != null) {
			return false;
		}
        if (description != null ? !description.equals(that.description) : that.description != null) {
			return false;
		}
        if (inmateLocation != null ? !inmateLocation.equals(that.inmateLocation) : that.inmateLocation != null) {
			return false;
		}

        return true;
    }

    @Override
    public int hashCode() {
        int result = personIdentityId != null ? personIdentityId.hashCode() : 0;
        result = 31 * result + (supervisionId != null ? supervisionId.hashCode() : 0);
        result = 31 * result + (inmateNum != null ? inmateNum.hashCode() : 0);
        result = 31 * result + (supervisionDisplayId != null ? supervisionDisplayId.hashCode() : 0);
        result = 31 * result + (personId != null ? personId.hashCode() : 0);
        result = 31 * result + (facilityId != null ? facilityId.hashCode() : 0);
        result = 31 * result + (facilityInternalLocationId != null ? facilityInternalLocationId.hashCode() : 0);
        result = 31 * result + (locationId != null ? locationId.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (scheduleId != null ? scheduleId.hashCode() : 0);
        result = 31 * result + (startDateTime != null ? startDateTime.hashCode() : 0);
//        result = 31 * result + (endDateTime != null ? endDateTime.hashCode() : 0);
        result = 31 * result + (conflictTypes != null ? conflictTypes.hashCode() : 0);
        result = 31 * result + (conflictTypeDescriptions != null ? conflictTypeDescriptions.hashCode() : 0);
//        result = 31 * result + (conflictSubTypes != null ? conflictSubTypes.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (inmateLocation != null ? inmateLocation.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NonAssociationType{" +
                "personIdentityId=" + personIdentityId +
                ", supervisionId=" + supervisionId +
                ", inmateNum='" + inmateNum + '\'' +
                ", supervisionDisplayId='" + supervisionDisplayId + '\'' +
                ", personId=" + personId +
                ", facilityId=" + facilityId +
                ", facilityInternalLocationId=" + facilityInternalLocationId +
                ", locationId=" + locationId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", scheduleId=" + scheduleId +
                ", startDateTime=" + startDateTime +
                ", endDateTime=" + endDateTime +
                ", conflictTypes=" + conflictTypes +
                ", conflictTypeDescriptions=" + conflictTypeDescriptions +
                ", conflictSubTypes=" + conflictSubTypes +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                ", inmateLocation='" + inmateLocation + '\'' +
                '}';
    }

    @Override
    public int compareTo(NonAssociationType o) {
        if (this.getSupervisionId() == null || o.getSupervisionId() == null) {
            return 0;
        }
        return this.getSupervisionId().compareTo(o.getSupervisionId());
    }

    public enum ConflictType {
        NON_ASSOCIATION, SCHEDULING, CAPACITY, HOUSING;
    }

    public enum EventType {
        TAP_EXTERNAL_MOVEMENT, CRT_EXTERNAL_MOVEMENT, TRN_EXTERNAL_MOVEMENT, REL_EXTERNAL_MOVEMENT,
        INTERNAL_MOVEMENT, VISIT, APPOINTMENT, HEARING, PROGRAM, SENTENCE, APPO;
    }
}
