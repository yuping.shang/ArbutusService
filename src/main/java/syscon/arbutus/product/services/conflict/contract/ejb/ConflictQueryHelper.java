package syscon.arbutus.product.services.conflict.contract.ejb;

import javax.ejb.SessionContext;
import javax.inject.Inject;
import java.util.List;

import org.hibernate.Session;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.appointment.realization.persistence.AppointmentEntity;
import syscon.arbutus.product.services.conflict.contract.dto.ConflictQueryDataHolder;
import syscon.arbutus.product.services.conflict.contract.dto.NonAssociationType;
import syscon.arbutus.product.services.program.realization.persistence.ProgramAssignmentEntity;

/**
 * Created by ian.rivers on 25/06/2015.
 */
public class ConflictQueryHelper {

    ConflictHelper conflictHelper;

    public ConflictQueryHelper() {
        conflictHelper = new ConflictHelper();
    }

    public List<NonAssociationType> getProgramAssignment(ConflictQueryDataHolder data) {
        /**
         * Check Non-Association Program Offering/Assignment
         */
        List<ProgramAssignmentEntity> programAssignmentEntityList = conflictHelper.getProgramAssignmentList(data);
        if (programAssignmentEntityList != null && !programAssignmentEntityList.isEmpty()) {
            List<NonAssociationType> nonAssociationForProgram = ConflictHelper.toNonAssociationTypeListFromProgramAssignment(programAssignmentEntityList, data);
            if (nonAssociationForProgram != null && !nonAssociationForProgram.isEmpty()) {
                return nonAssociationForProgram;
            }
        }
        return null;
    }

    public List<NonAssociationType> getAppointmentEntityList(ConflictQueryDataHolder data) {
        /**
         * Check for Appointment
         */
        List<AppointmentEntity> appointmentEntityList = ConflictHelper.getAppointmentEntityList(data);
        if (appointmentEntityList != null && !appointmentEntityList.isEmpty()) {
            List<NonAssociationType> nonAssociationForAppointment = ConflictHelper.toNonAssociationTypeListFromAppointment(appointmentEntityList, data);
            if (nonAssociationForAppointment != null && !nonAssociationForAppointment.isEmpty()) {
                return nonAssociationForAppointment;
            }
        }
        return null;
    }
}
