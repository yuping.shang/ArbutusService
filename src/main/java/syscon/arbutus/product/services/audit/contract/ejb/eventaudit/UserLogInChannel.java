package syscon.arbutus.product.services.audit.contract.ejb.eventaudit;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.Date;

import org.kie.api.runtime.Channel;
import syscon.arbutus.product.services.audit.contract.dto.AuditModule;
import syscon.arbutus.product.services.audit.contract.dto.eventaudit.UserActionEventType;
import syscon.arbutus.product.services.audit.realization.AuditServiceAdapter;
import syscon.arbutus.product.services.audit.realization.persistence.eventaudit.UserActionNameEnum;
import syscon.arbutus.product.services.audit.realization.persistence.eventaudit.UserLoginEventEntity;
import syscon.arbutus.product.services.security.contract.dto.UserType;
import syscon.arbutus.product.services.security.contract.interfaces.SecurityService;

/**
 * Created by YShang on 11/02/14.
 */
@Stateless
@LocalBean
public class UserLogInChannel implements Channel {
    @EJB
    private SecurityService securityService;
    @EJB
    private UserEventBean userEvent;

    private AuditServiceAdapter auditAdapter;

    @PostConstruct
    public void init() {
        auditAdapter = new AuditServiceAdapter();
    }

    @Override
    public void send(Object o) {
        if (!auditAdapter.isAuditOn(AuditModule.LOGIN)) {
            return;
        }

        UserActionEventType userActionEvent = (UserActionEventType) o;
        String loginId = userActionEvent.getLoginId();

        UserType user = securityService.getUser(null, loginId);
        UserLoginEventEntity userLoginEventEntity = UserEventHelper.toUserEntity(user);
        if (userLoginEventEntity != null) {
            userLoginEventEntity.setUserLogEvent(UserActionNameEnum.LOG_IN);
            userLoginEventEntity.setEventDateTime(userActionEvent.getEventDatetime());
            userLoginEventEntity.setCreateDateTime(new Date());
            userEvent.create(userLoginEventEntity);
        } else {
            UserLoginEventEntity failedLogIn = new UserLoginEventEntity(UserActionNameEnum.LOG_FAILED);
            failedLogIn.setLoginId(loginId);
            failedLogIn.setEventDateTime(userActionEvent.getEventDatetime());
            failedLogIn.setCreateDateTime(new Date());
            userEvent.create(failedLogIn);
        }
    }
}
