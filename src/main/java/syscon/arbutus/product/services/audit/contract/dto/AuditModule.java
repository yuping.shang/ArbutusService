package syscon.arbutus.product.services.audit.contract.dto;

public enum AuditModule {

    PERSON, HOUSING, LOGIN, PAGEVIEW, REVISION;

    public String value() {
        return name();
    }
}
