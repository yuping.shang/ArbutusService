package syscon.arbutus.product.services.audit.contract.interfaces.eventaudit;

import syscon.arbutus.product.services.audit.contract.dto.eventaudit.UserPageViewInmateType;

/**
 * Created by YShang on 12/02/14.
 */
public interface UserActionsListenerService {
    public void recordUserLogIn(String loginId);

    public void recordUserLogOut(String loginId);

    public void recordUserLogInFailed(String loginId);

    public void recordUserPageView(String loginId, UserPageViewInmateType inmateInContext);
}
