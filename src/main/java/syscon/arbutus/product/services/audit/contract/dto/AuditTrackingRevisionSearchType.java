/**
 *
 */
package syscon.arbutus.product.services.audit.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author LHan
 */
public class AuditTrackingRevisionSearchType implements Serializable {

    private static final long serialVersionUID = 2687326421604684536L;

    private String service;
    private String userId;
    private Date fromDateTime;
    private Date toDateTime;
    @NotNull
    private Long startindex;
    @NotNull
    private Long resultsize;
    private String orderby;
    private String filter;

    /**
     *
     */
    public AuditTrackingRevisionSearchType() {
    }

    /**
     * @param service
     * @param userId
     * @param fromDateTime
     * @param toDateTime
     * @param startindex
     * @param resultsize
     * @param orderby
     * @param filter
     */
    public AuditTrackingRevisionSearchType(String service, String userId, Date fromDateTime, Date toDateTime, Long startindex, Long resultsize, String orderby,
            String filter) {
        super();
        this.service = service;
        this.userId = userId;
        this.fromDateTime = fromDateTime;
        this.toDateTime = toDateTime;
        this.startindex = startindex;
        this.resultsize = resultsize;
        this.orderby = orderby;
        this.filter = filter;
    }

    /**
     * @return the fromDateTime
     */
    public Date getFromDateTime() {
        return fromDateTime;
    }

    /**
     * @param fromDateTime the fromDateTime to set
     */
    public void setFromDateTime(Date fromDateTime) {
        this.fromDateTime = fromDateTime;
    }

    /**
     * @return the toDateTime
     */
    public Date getToDateTime() {
        return toDateTime;
    }

    /**
     * @param toDateTime the toDateTime to set
     */
    public void setToDateTime(Date toDateTime) {
        this.toDateTime = toDateTime;
    }

    /**
     * @return the startindex
     */
    public Long getStartindex() {
        return startindex;
    }

    /**
     * @param startindex the startindex to set
     */
    public void setStartindex(Long startindex) {
        this.startindex = startindex;
    }

    /**
     * @return the resultsize
     */
    public Long getResultsize() {
        return resultsize;
    }

    /**
     * @param resultsize the resultsize to set
     */
    public void setResultsize(Long resultsize) {
        this.resultsize = resultsize;
    }

    /**
     * @return the orderby
     */
    public String getOrderby() {
        return orderby;
    }

    /**
     * @param orderby the orderby to set
     */
    public void setOrderby(String orderby) {
        this.orderby = orderby;
    }

    /**
     * @return the filter
     */
    public String getFilter() {
        return filter;
    }

    /**
     * @param filter the filter to set
     */
    public void setFilter(String filter) {
        this.filter = filter;
    }

    /**
     * @return the service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service the service to set
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "AuditTrackingRevisionSearchType [service=" + service + ", userId=" + userId + ", fromDateTime=" + fromDateTime + ", toDateTime=" + toDateTime
                + ", startindex=" + startindex + ", resultsize=" + resultsize + ", orderby=" + orderby + ", filter=" + filter + "]";
    }

}
