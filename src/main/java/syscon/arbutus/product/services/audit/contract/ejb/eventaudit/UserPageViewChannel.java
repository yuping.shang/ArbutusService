package syscon.arbutus.product.services.audit.contract.ejb.eventaudit;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.kie.api.runtime.Channel;
import syscon.arbutus.product.services.audit.contract.dto.AuditModule;
import syscon.arbutus.product.services.audit.contract.dto.eventaudit.UserActionEventType;
import syscon.arbutus.product.services.audit.realization.AuditServiceAdapter;
import syscon.arbutus.product.services.audit.realization.persistence.eventaudit.UserPageViewEventEntity;
import syscon.arbutus.product.services.utility.ValidationHelper;

/**
 * @author LHan
 */
@Stateless
@LocalBean
public class UserPageViewChannel implements Channel {

    @PersistenceContext(unitName = "arbutus-pu")
    private Session session;

    private AuditServiceAdapter auditAdapter;

    @PostConstruct
    public void init() {
        auditAdapter = new AuditServiceAdapter();
    }

    @Override
    public void send(Object o) {
        if (!auditAdapter.isAuditOn(AuditModule.PAGEVIEW)) {
            return;
        }
        UserActionEventType userActionEvent = (UserActionEventType) o;

        if (userActionEvent == null) {
			return;
		}

        ValidationHelper.validate(userActionEvent);

        if (userActionEvent.getUserActionName().equals(UserActionEventType.UserActionName.PAGE_VIEW)) {
            if (userActionEvent.getInmateInContext() == null) {
				return;
			}
        }

        UserPageViewEventEntity userPageViewEvent = UserEventHelper.toUserPageViewEntity(userActionEvent);

        session.persist(userPageViewEvent);

    }
}
