package syscon.arbutus.product.services.audit.realization.persistence.eventaudit;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;

@Entity
@Table(name = "AUD_UserLogInEvent")
@SQLDelete(sql = "UPDATE AUD_UserLogInEvent SET flag = 4 WHERE userLogInEventId = ? and version = ?")
@Where(clause = "flag = 1")
public class UserLoginEventEntity implements Serializable {

    private static final long serialVersionUID = 6192463189389708758L;

    @Id
    @Column(name = "userLogInEventId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_AUD_UserLogInEvent_Id")
    @SequenceGenerator(name = "SEQ_AUD_UserLogInEvent_Id", sequenceName = "SEQ_AUD_UserLogInEvent_Id", allocationSize = 1)
    private Long userLogInEventId;

    @Enumerated(EnumType.STRING)
    @Column(name = "userLogEvent")
    private UserActionNameEnum userLogEvent;

    @Column(name = "LoginId", nullable = true, length = 64)
    private String loginId;

    @Column(name = "eventDateTime", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventDateTime;

    @Column(name = "PersonId", nullable = true)
    private Long personId;

    @Column(name = "PersonIdentityId", nullable = true)
    private Long personIdentityId;

    @Column(name = "Name", nullable = true, length = 64)
    private String name;

    @Column(name = "DisplayName", nullable = true, length = 128)
    private String displayName;

    /**
     * @DbComment version 'the version number of record updated'
     */
    @Version
    private Long version;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    @Column(name = "CreateDateTime", nullable = true, length = 64)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDateTime;

    public UserLoginEventEntity() {
    }

    public UserLoginEventEntity(UserActionNameEnum userLogEvent) {
        this.userLogEvent = userLogEvent;
    }

    public UserLoginEventEntity(Long userLogInEventId, String name, Long personId, Long personIdentityId, String loginId, String ldapId, String displayName,
            Date eventDateTime, Date createDateTime) {
        this.userLogInEventId = userLogInEventId;
        this.name = name;
        this.personId = personId;
        this.personIdentityId = personIdentityId;
        this.loginId = loginId;
        this.displayName = displayName;
        this.createDateTime = createDateTime;
        this.eventDateTime = eventDateTime;
    }

    /**
     * @return the userLogInEventId
     */
    public Long getUserLogInEventId() {
        return userLogInEventId;
    }

    /**
     * @param userLogInEventId the userLogInEventId to set
     */
    public void setUserLogInEventId(Long userLogInEventId) {
        this.userLogInEventId = userLogInEventId;
    }

    /**
     * @return the userLogEvent
     */
    public UserActionNameEnum getUserLogEvent() {
        return userLogEvent;
    }

    /**
     * @param userLogEvent the userLogEvent to set
     */
    public void setUserLogEvent(UserActionNameEnum userLogEvent) {
        this.userLogEvent = userLogEvent;
    }

    /**
     * @return the loginId
     */
    public String getLoginId() {
        return loginId;
    }

    /**
     * @param loginId the loginId to set
     */
    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    /**
     * @return the eventDateTime
     */
    public Date getEventDateTime() {
        return eventDateTime;
    }

    /**
     * @param eventDateTime the eventDateTime to set
     */
    public void setEventDateTime(Date eventDateTime) {
        this.eventDateTime = eventDateTime;
    }

    /**
     * @return the personId
     */
    public Long getPersonId() {
        return personId;
    }

    /**
     * @param personId the personId to set
     */
    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    /**
     * @return the personIdentityId
     */
    public Long getPersonIdentityId() {
        return personIdentityId;
    }

    /**
     * @param personIdentityId the personIdentityId to set
     */
    public void setPersonIdentityId(Long personIdentityId) {
        this.personIdentityId = personIdentityId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /**
     * @return the createDateTime
     */
    public Date getCreateDateTime() {
        return createDateTime;
    }

    /**
     * @param createDateTime the createDateTime to set
     */
    public void setCreateDateTime(Date createDateTime) {
        this.createDateTime = createDateTime;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((createDateTime == null) ? 0 : createDateTime.hashCode());
        result = prime * result + ((displayName == null) ? 0 : displayName.hashCode());
        result = prime * result + ((eventDateTime == null) ? 0 : eventDateTime.hashCode());
        result = prime * result + ((flag == null) ? 0 : flag.hashCode());
        result = prime * result + ((loginId == null) ? 0 : loginId.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((personId == null) ? 0 : personId.hashCode());
        result = prime * result + ((personIdentityId == null) ? 0 : personIdentityId.hashCode());
        result = prime * result + ((userLogEvent == null) ? 0 : userLogEvent.hashCode());
        result = prime * result + ((userLogInEventId == null) ? 0 : userLogInEventId.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        UserLoginEventEntity other = (UserLoginEventEntity) obj;
        if (createDateTime == null) {
            if (other.createDateTime != null) {
				return false;
			}
        } else if (!createDateTime.equals(other.createDateTime)) {
			return false;
		}
        if (displayName == null) {
            if (other.displayName != null) {
				return false;
			}
        } else if (!displayName.equals(other.displayName)) {
			return false;
		}
        if (eventDateTime == null) {
            if (other.eventDateTime != null) {
				return false;
			}
        } else if (!eventDateTime.equals(other.eventDateTime)) {
			return false;
		}
        if (flag == null) {
            if (other.flag != null) {
				return false;
			}
        } else if (!flag.equals(other.flag)) {
			return false;
		}
        if (loginId == null) {
            if (other.loginId != null) {
				return false;
			}
        } else if (!loginId.equals(other.loginId)) {
			return false;
		}
        if (name == null) {
            if (other.name != null) {
				return false;
			}
        } else if (!name.equals(other.name)) {
			return false;
		}
        if (personId == null) {
            if (other.personId != null) {
				return false;
			}
        } else if (!personId.equals(other.personId)) {
			return false;
		}
        if (personIdentityId == null) {
            if (other.personIdentityId != null) {
				return false;
			}
        } else if (!personIdentityId.equals(other.personIdentityId)) {
			return false;
		}
        if (userLogEvent != other.userLogEvent) {
			return false;
		}
        if (userLogInEventId == null) {
            if (other.userLogInEventId != null) {
				return false;
			}
        } else if (!userLogInEventId.equals(other.userLogInEventId)) {
			return false;
		}
        if (version == null) {
            if (other.version != null) {
				return false;
			}
        } else if (!version.equals(other.version)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "UserLoginEventEntity [userLogInEventId=" + userLogInEventId + ", userLogEvent=" + userLogEvent + ", loginId=" + loginId + ", eventDateTime="
                + eventDateTime + ", personId=" + personId + ", personIdentityId=" + personIdentityId + ", name=" + name + ", displayName=" + displayName + ", version="
                + version + ", flag=" + flag + ", createDateTime=" + createDateTime + "]";
    }

}
