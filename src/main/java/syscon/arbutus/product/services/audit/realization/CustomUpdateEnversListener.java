package syscon.arbutus.product.services.audit.realization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.envers.configuration.AuditConfiguration;
import org.hibernate.envers.event.EnversPostUpdateEventListenerImpl;
import org.hibernate.event.spi.PostUpdateEvent;
import syscon.arbutus.product.services.audit.contract.dto.AuditModule;

public class CustomUpdateEnversListener extends EnversPostUpdateEventListenerImpl {

    private static final long serialVersionUID = 8988704253078898407L;

    private static final Logger log = LoggerFactory.getLogger(CustomUpdateEnversListener.class);

    private AuditServiceAdapter auditAdapter;

    protected CustomUpdateEnversListener(AuditConfiguration enversConfiguration) {

        super(enversConfiguration);
        auditAdapter = new AuditServiceAdapter();

    }

    @Override
    public void onPostUpdate(PostUpdateEvent event) {

        boolean isAuditOn = auditAdapter.isAuditOn(AuditModule.REVISION);
        if (isAuditOn) {
            super.onPostUpdate(event);
        }

    }

}
