package syscon.arbutus.product.services.audit.contract.dto.eventaudit;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author LHan
 */
public class UserPageViewInmateType implements Serializable {

    private static final long serialVersionUID = 7884411896686248755L;

    @NotNull
    private Long inmatePersonId;

    @NotNull
    private Long inmatePersonIdentityId;

    @NotNull
    private String supervisionDisplayId;

    @NotNull
    private String inmateFirstName;

    @NotNull
    private String inmateLastName;

    @NotNull
    private String ViewedPageInfo;

    /**
     * @return the inmatePersonId
     */
    public Long getInmatePersonId() {
        return inmatePersonId;
    }

    /**
     * @param inmatePersonId the inmatePersonId to set
     */
    public void setInmatePersonId(Long inmatePersonId) {
        this.inmatePersonId = inmatePersonId;
    }

    /**
     * @return the inmatePersonIdentityId
     */
    public Long getInmatePersonIdentityId() {
        return inmatePersonIdentityId;
    }

    /**
     * @param inmatePersonIdentityId the inmatePersonIdentityId to set
     */
    public void setInmatePersonIdentityId(Long inmatePersonIdentityId) {
        this.inmatePersonIdentityId = inmatePersonIdentityId;
    }

    /**
     * @return the supervisionDisplayId
     */
    public String getSupervisionDisplayId() {
        return supervisionDisplayId;
    }

    /**
     * @param supervisionDisplayId the supervisionDisplayId to set
     */
    public void setSupervisionDisplayId(String supervisionDisplayId) {
        this.supervisionDisplayId = supervisionDisplayId;
    }

    /**
     * @return the inmateFirstName
     */
    public String getInmateFirstName() {
        return inmateFirstName;
    }

    /**
     * @param inmateFirstName the inmateFirstName to set
     */
    public void setInmateFirstName(String inmateFirstName) {
        this.inmateFirstName = inmateFirstName;
    }

    /**
     * @return the inmateLastName
     */
    public String getInmateLastName() {
        return inmateLastName;
    }

    /**
     * @param inmateLastName the inmateLastName to set
     */
    public void setInmateLastName(String inmateLastName) {
        this.inmateLastName = inmateLastName;
    }

    /**
     * @return the viewedPageInfo
     */
    public String getViewedPageInfo() {
        return ViewedPageInfo;
    }

    /**
     * @param viewedPageInfo the viewedPageInfo to set
     */
    public void setViewedPageInfo(String viewedPageInfo) {
        ViewedPageInfo = viewedPageInfo;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ViewedPageInfo == null) ? 0 : ViewedPageInfo.hashCode());
        result = prime * result + ((inmateFirstName == null) ? 0 : inmateFirstName.hashCode());
        result = prime * result + ((inmateLastName == null) ? 0 : inmateLastName.hashCode());
        result = prime * result + ((inmatePersonId == null) ? 0 : inmatePersonId.hashCode());
        result = prime * result + ((inmatePersonIdentityId == null) ? 0 : inmatePersonIdentityId.hashCode());
        result = prime * result + ((supervisionDisplayId == null) ? 0 : supervisionDisplayId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        UserPageViewInmateType other = (UserPageViewInmateType) obj;
        if (ViewedPageInfo == null) {
            if (other.ViewedPageInfo != null) {
				return false;
			}
        } else if (!ViewedPageInfo.equals(other.ViewedPageInfo)) {
			return false;
		}
        if (inmateFirstName == null) {
            if (other.inmateFirstName != null) {
				return false;
			}
        } else if (!inmateFirstName.equals(other.inmateFirstName)) {
			return false;
		}
        if (inmateLastName == null) {
            if (other.inmateLastName != null) {
				return false;
			}
        } else if (!inmateLastName.equals(other.inmateLastName)) {
			return false;
		}
        if (inmatePersonId == null) {
            if (other.inmatePersonId != null) {
				return false;
			}
        } else if (!inmatePersonId.equals(other.inmatePersonId)) {
			return false;
		}
        if (inmatePersonIdentityId == null) {
            if (other.inmatePersonIdentityId != null) {
				return false;
			}
        } else if (!inmatePersonIdentityId.equals(other.inmatePersonIdentityId)) {
			return false;
		}
        if (supervisionDisplayId == null) {
            if (other.supervisionDisplayId != null) {
				return false;
			}
        } else if (!supervisionDisplayId.equals(other.supervisionDisplayId)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "UserPageViewInmateType [inmatePersonId=" + inmatePersonId + ", inmatePersonIdentityId=" + inmatePersonIdentityId + ", supervisionDisplayId="
                + supervisionDisplayId + ", inmateFirstName=" + inmateFirstName + ", inmateLastName=" + inmateLastName + ", ViewedPageInfo=" + ViewedPageInfo + "]";
    }

}
