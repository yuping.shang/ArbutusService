package syscon.arbutus.product.services.audit.contract.ejb.eventaudit;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.Date;

import org.kie.api.runtime.Channel;
import syscon.arbutus.product.services.audit.contract.dto.AuditModule;
import syscon.arbutus.product.services.audit.contract.dto.eventaudit.UserActionEventType;
import syscon.arbutus.product.services.audit.realization.AuditServiceAdapter;
import syscon.arbutus.product.services.audit.realization.persistence.eventaudit.UserActionNameEnum;
import syscon.arbutus.product.services.audit.realization.persistence.eventaudit.UserLoginEventEntity;

/**
 * Created by YShang on 12/02/14.
 */
@Stateless
@LocalBean
public class UserFailedLogInChannel implements Channel {
    @EJB
    private UserEventBean userEvent;

    private AuditServiceAdapter auditAdapter;

    @PostConstruct
    public void init() {
        auditAdapter = new AuditServiceAdapter();
    }

    @Override
    public void send(Object o) {
        if (!auditAdapter.isAuditOn(AuditModule.LOGIN)) {
            return;
        }
        UserActionEventType useractionEvent = (UserActionEventType) o;
        UserLoginEventEntity userLoginEventEntity = new UserLoginEventEntity(UserActionNameEnum.LOG_FAILED);
        userLoginEventEntity.setLoginId(useractionEvent.getLoginId());
        userLoginEventEntity.setEventDateTime(useractionEvent.getEventDatetime());
        userLoginEventEntity.setCreateDateTime(new Date());
        if (userLoginEventEntity != null) {
			userEvent.create(userLoginEventEntity);
		}
    }
}
