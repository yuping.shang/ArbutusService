package syscon.arbutus.product.services.audit.contract.dto;

import java.io.Serializable;
import java.util.List;

public class AuditControlsReturnType implements Serializable {

    private static final long serialVersionUID = 1790028052625084311L;

    private List<AuditControlType> auditControls;
    private Long totalSize;

    public AuditControlsReturnType() {
        super();
    }

    public List<AuditControlType> getAuditControls() {
        return auditControls;
    }

    public void setAuditControls(List<AuditControlType> auditControls) {
        this.auditControls = auditControls;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    @Override
    public String toString() {
        return "AuditControlsReturnType [auditControls=" + auditControls + ", totalSize=" + totalSize + "]";
    }
}
