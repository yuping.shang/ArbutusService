package syscon.arbutus.product.services.audit.contract.interfaces;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.audit.contract.dto.*;
import syscon.arbutus.product.services.audit.contract.interfaces.eventaudit.UserActionsListenerService;

/**
 * @author byu, lhan
 */
public interface AuditService extends UserActionsListenerService {

    public AuditControlType create(UserContext uc, AuditControlType auditControl);

    public AuditControlType update(UserContext uc, AuditControlType auditControl);

    public Long delete(UserContext uc, Long auditControlId);

    public Long deleteAll(UserContext uc);

    public boolean isAuditOn(UserContext uc, AuditModule auditModule);

    /**
     * Get audit record details.
     *
     * @param uc
     * @param revId
     * @param serviceName
     * @return
     */
    public AuditRecordDetailsType getAuditRecordDetails(UserContext uc, Long revId, String serviceName);

    /**
     * Paging search audit tracking revision data.
     *
     * @param uc
     * @param searchType
     * @return
     */
    public AuditTrackingRevisionsReturnType searchAuditTrails(UserContext uc, AuditTrackingRevisionSearchType searchType);

}
