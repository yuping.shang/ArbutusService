package syscon.arbutus.product.services.audit.realization;

import javax.security.auth.Subject;
import javax.security.jacc.PolicyContext;
import javax.security.jacc.PolicyContextException;
import java.io.Serializable;
import java.security.Principal;

import com.google.gson.Gson;
import org.hibernate.envers.EntityTrackingRevisionListener;
import org.hibernate.envers.RevisionType;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.audit.contract.dto.ServiceEntityNameEnum;
import syscon.arbutus.product.services.audit.realization.persistence.CustomEntityTrackingRevisionEntity;
import syscon.arbutus.product.services.core.services.DefaultInterceptor;
import syscon.arbutus.product.services.location.contract.util.BeanUtil;

public class CustomEntityTrackingRevisionListener implements EntityTrackingRevisionListener {
    private static final String SUBJECT_CONTEXT_KEY = "javax.security.auth.Subject.container";

    @Override
    public void newRevision(Object revisionEntity) {
        CustomEntityTrackingRevisionEntity revision = (CustomEntityTrackingRevisionEntity) revisionEntity;
        UserContext uc = DefaultInterceptor.getUserContext();
        String username = "system";

        try {
            // TODO Get user name from UserDetails
            Subject caller = (Subject) PolicyContext.getContext(SUBJECT_CONTEXT_KEY);
            for (Principal p : caller.getPrincipals()) {
                username = p.getName();
                break;
            }

        } catch (PolicyContextException ex) {
            ex.printStackTrace();
        }
        revision.setModifyUser(username);

    }

    @SuppressWarnings("rawtypes")
    @Override
    public void entityChanged(Class entityClass, String entityName, Serializable entityId, RevisionType revisionType, Object revisionEntity) {
        String type = entityClass.getName();

        Gson gson = new Gson();
        String entityIdStr = gson.toJson(entityId);

        CustomEntityTrackingRevisionEntity customEntityTrackingRevisionEntity = (CustomEntityTrackingRevisionEntity) revisionEntity;

        customEntityTrackingRevisionEntity.addModifiedEntityType(type, entityIdStr, revisionType);

        String serviceName = ServiceEntityNameEnum.fromEntityToServiceName(type);

        String currentServiceName = customEntityTrackingRevisionEntity.getServiceName();

        if (!BeanUtil.isEmptyString(serviceName)) {
            if (currentServiceName == null) {
                customEntityTrackingRevisionEntity.setServiceName(serviceName);
            } else {
                String[] serviceNames = currentServiceName.split(",");
                boolean found = false;
                for (String aServiceName : serviceNames) {
                    if (aServiceName.trim().equals(serviceName)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    currentServiceName = currentServiceName + ", " + serviceName;
                    customEntityTrackingRevisionEntity.setServiceName(currentServiceName);
                }
            }
        }
    }
}
