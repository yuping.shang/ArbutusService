package syscon.arbutus.product.services.audit.contract.ejb;

import javax.ejb.SessionContext;
import javax.naming.InitialContext;
import javax.persistence.*;
import javax.sql.DataSource;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.*;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQueryCreator;
import org.hibernate.sql.JoinType;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.audit.contract.dto.*;
import syscon.arbutus.product.services.audit.realization.persistence.AuditControlEntity;
import syscon.arbutus.product.services.audit.realization.persistence.CustomEntityTrackingRevisionEntity;
import syscon.arbutus.product.services.audit.realization.persistence.ModifiedEntityTypeEntity;
import syscon.arbutus.product.services.core.exception.InvalidInputException;
import syscon.arbutus.product.services.realization.model.StampEntity;
import syscon.arbutus.product.services.realization.util.BeanHelper;
import syscon.arbutus.product.services.realization.util.LogHelper;
import syscon.arbutus.product.services.utility.ValidationHelper;

public class AuditHandler {

    private static Logger log = LoggerFactory.getLogger(AuditHandler.class);

    private SessionContext context;
    private Session session;

    public AuditHandler(SessionContext context, Session session) {
        super();
        this.context = context;
        this.session = session;
    }

    public AuditControlType create(UserContext uc, AuditControlType auditControl) {

        ValidationHelper.validate(auditControl);

        AuditControlEntity oldEntity = getAuditModule(auditControl.getAuditModule());
        if (oldEntity != null) {
            String message = String.format("The audit module already exist, AuditModule = %s", auditControl.getAuditModule());
            LogHelper.error(log, "create", message);
            throw new InvalidInputException(message);
        }

        AuditControlEntity entity = AuditHelper.toAuditControlEntity(auditControl);
        entity.setAuditControlId(null);
        // Create stamp
        StampEntity createStamp = BeanHelper.getCreateStamp(uc, context);
        entity.setStamp(createStamp);
        session.save(entity);
        AuditControlType ret = AuditHelper.toAuditControlType(getAuditModule(auditControl.getAuditModule()));
        return ret;

    }

    public AuditControlType update(UserContext uc, AuditControlType auditControl) {

        ValidationHelper.validate(auditControl);

        AuditControlEntity entity = getAuditModule(auditControl.getAuditModule());
        if (entity == null) {
            return create(uc, auditControl);
        }

        entity.setOnAudit(auditControl.isOnAudit());
        entity.setUpdatedTime(auditControl.getUpdatedTime());
        StampEntity modifyStamp = BeanHelper.getModifyStamp(uc, context, entity.getStamp());

        entity.setStamp(modifyStamp);

        session.update(entity);

        AuditControlType ret = AuditHelper.toAuditControlType(getAuditModule(auditControl.getAuditModule()));
        return ret;
    }

    public Long delete(UserContext uc, Long auditControlId) {
        // TODO Auto-generated method stub
        return null;
    }

    public void deleteAll(UserContext uc) {

        session.createQuery("delete AuditControlEntity").executeUpdate();

        // Clear Hibernate cache to add to avoid unique key violation.
        session.flush();
        session.clear();
    }

    public boolean isAuditOn(UserContext uc, AuditModule auditModule) {

        String datasource_context = "java:/ArbutusDS";
        Connection conn = null;
        PreparedStatement statement = null;

        try {
            InitialContext initContext = new InitialContext();
            DataSource datasource = (DataSource) initContext.lookup(datasource_context);
            conn = datasource.getConnection();
            statement = conn.prepareStatement("select ONAUDIT  from Aud_Control where Auditmodule= ? ");
            statement.setString(1, auditModule.value());
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return rs.getBoolean(1);
            }

        } catch (Exception e) {
            log.error("Unenable to get the audit status " + e.getMessage());
        } finally {

            if (statement != null) {
				try {
                    statement.close();
                } catch (SQLException e) {
                    log.error("Unenable to close statement ");
                }
			}
            if (conn != null) {
				try {
                    conn.close();
                } catch (SQLException e) {
                    log.error("Unenable to close connection!");
                }
			}
        }

        return false;

    }

    private AuditControlEntity getAuditModule(AuditModule auditModule) {

        // Create criteria
        Criteria criteria = session.createCriteria(AuditControlEntity.class);

        criteria.add(Restrictions.eq("auditModule", auditModule));

        @SuppressWarnings("unchecked") List<AuditControlEntity> list = criteria.list();
        session.clear();

        if ((list == null) || (list.size() == 0)) {
            return null;
        } else {
            AuditControlEntity ret = list.get(0);
            return ret;
        }

    }

    public AuditTrackingRevisionsReturnType searchAuditTrails(UserContext uc, AuditTrackingRevisionSearchType searchType) {

        ValidationHelper.validate(searchType);

        AuditTrackingRevisionsReturnType ret = new AuditTrackingRevisionsReturnType();

        String serviceName = searchType.getService();
        String userId = searchType.getUserId();
        Date fromDateTime = searchType.getFromDateTime();
        Date toDateTime = searchType.getToDateTime();

        Criteria criteria = session.createCriteria(CustomEntityTrackingRevisionEntity.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        if (userId != null && userId.length() > 0) {
			criteria = criteria.add(Restrictions.eq("modifyUser", userId));
		}

        if (fromDateTime != null) {
			criteria = criteria.add(Restrictions.ge("modifyDateTime", fromDateTime));
		}

        if (toDateTime != null) {
			criteria = criteria.add(Restrictions.le("modifyDateTime", toDateTime));
		}

        if (serviceName != null && serviceName.length() > 0) {
            //criteria.createAlias("modifiedEntityTypes", "serviceName", JoinType.INNER_JOIN);
            criteria = criteria.add(Restrictions.ilike("serviceName", "%" + serviceName + "%"));
        }

        //Filter
        String filter = searchType.getFilter();
        if (!BeanHelper.isEmpty(filter)) {
            Disjunction filterDisj = Restrictions.disjunction();
            filterDisj.add(Restrictions.ilike("serviceName", filter, MatchMode.ANYWHERE));
            filterDisj.add(Restrictions.ilike("modifyUser", filter, MatchMode.ANYWHERE));

            Date modifyDateTime = getDateFromString(filter);
            if (modifyDateTime != null) {
				filterDisj.add(Restrictions.eq("modifyDateTime", modifyDateTime));
			}

            try {
                Long idFilter = Long.valueOf(filter);
                if (idFilter != null) {
					filterDisj.add(Restrictions.eq("id", idFilter));
				}
            } catch (NumberFormatException e) {
                //Do nothing, it can happen.
            }

            criteria.add(filterDisj);
        }

        //Total
        Long totalSize = (Long) criteria.setProjection(Projections.count("id")).uniqueResult();
        criteria.setProjection(null);
        criteria.setResultTransformer(Criteria.ROOT_ENTITY);

        //Pagination
        Long startIndex = searchType.getStartindex();
        Long resultSize = searchType.getResultsize();
        if (startIndex != null && resultSize != null && startIndex >= 0l && resultSize > 0l) {
            criteria.setFirstResult(startIndex.intValue());
            criteria.setMaxResults(resultSize.intValue());
        }

        //OrderBy
        String orderString = searchType.getOrderby();
        if (orderString != null && orderString.trim().length() > 0) {
            List<Order> ords = BeanHelper.toOrderCriteria(orderString);
            for (Order ord : ords) {
                criteria.addOrder(ord);
            }
        } else {
            Order orderByIdASC = Order.asc("id");
            criteria.addOrder(orderByIdASC);
        }

        @SuppressWarnings("unchecked") List<CustomEntityTrackingRevisionEntity> list = (List<CustomEntityTrackingRevisionEntity>) criteria.list();

        List<AuditTrackingRevisionType> tmp = new ArrayList<AuditTrackingRevisionType>();

        for (CustomEntityTrackingRevisionEntity v : list) {
            tmp.add(new AuditTrackingRevisionType(v.getServiceName(), v.getModifyUser(), v.getModifyDateTime(), v.getId()));
        }

        ret.setAuditRevisionList(tmp);
        ret.setTotalSize(totalSize);

        return ret;
    }

    public AuditRecordDetailsType getAuditRecordDetails(UserContext uc, Long revId, String serviceName) {

        AuditRecordDetailsType rtn = new AuditRecordDetailsType();

        Criteria criteria = session.createCriteria(CustomEntityTrackingRevisionEntity.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        criteria = criteria.add(Restrictions.eq("id", revId));

        if (serviceName != null && serviceName.length() > 0) {
            criteria.createAlias("modifiedEntityTypes", "serviceName", JoinType.INNER_JOIN);
            criteria = criteria.add(Restrictions.ilike("serviceName.entityClassName", "%" + serviceName + "%"));
        }

        CustomEntityTrackingRevisionEntity revisionEntity = (CustomEntityTrackingRevisionEntity) criteria.uniqueResult();

        rtn.setModifiedDateTime(revisionEntity.getModifyDateTime());
        rtn.setModifiedUser(revisionEntity.getModifyUser());
        rtn.setRevId(revId);
        rtn.setServiceName(revisionEntity.getServiceName());

        AuditReader reader = AuditReaderFactory.get(session);
        AuditQueryCreator queryCreater = reader.createQuery();

        for (ModifiedEntityTypeEntity modifyType : revisionEntity.getModifiedEntityTypes()) {

            Class<?> aClass = null;
            try {
                aClass = Class.forName(modifyType.getEntityClassName());
            } catch (ClassNotFoundException e) {
                log.error(e.getMessage());
            }

            IdClass idClass = aClass.getAnnotation(IdClass.class);

            Serializable entityId = null;
            if (idClass != null) {
                entityId = getEntityId(modifyType.getEntityId(), idClass.value());    //entity id is a Composite PK
            } else {

                Class<?> supClass = aClass.getSuperclass();
                if (supClass != null && supClass.getAnnotation(Entity.class) != null) {
                    entityId = getEntityId(modifyType.getEntityId(), supClass.getDeclaredFields());//entity id is a Single PK
                } else {
                    entityId = getEntityId(modifyType.getEntityId(), aClass.getDeclaredFields());//entity id is a Single PK
                }
            }

            String revType = modifyType.getRevType().name();
            Object currentEntity = null;
            Object previousEntity = null;
            String entityName = null;

            if ("DEL".equals(revType)) {

                previousEntity = getPreviousEntity(queryCreater, aClass, revId, entityId);
                entityName = previousEntity.getClass().getSimpleName();

            } else if ("ADD".equals(revType)) {

                currentEntity = getCurrentEntity(queryCreater, aClass, revId, entityId);
                entityName = currentEntity.getClass().getSimpleName();
            } else {

                currentEntity = getCurrentEntity(queryCreater, aClass, revId, entityId);
                previousEntity = getPreviousEntity(queryCreater, aClass, revId, entityId);
                entityName = currentEntity.getClass().getSimpleName();
            }

            generateAuditDetailsList(rtn, currentEntity, previousEntity, entityName, revType);

        }

        return rtn;
    }

    private Object getCurrentEntity(AuditQueryCreator queryCreater, Class<?> aClass, Long revId, Serializable entityId) {
        return queryCreater.forEntitiesAtRevision(aClass, revId).
                add(AuditEntity.id().eq(entityId)).
                getSingleResult();
    }

    private Object getPreviousEntity(AuditQueryCreator queryCreater, Class<?> aClass, Long revId, Serializable entityId) {
        Number previousSingleRev = (Number) queryCreater.forRevisionsOfEntity(aClass, false, true).
                addProjection(AuditEntity.revisionNumber().max()).
                add(AuditEntity.revisionNumber().lt(revId)).
                add(AuditEntity.id().eq(entityId)).
                getSingleResult();

        Object previousEntity = null;

        if (previousSingleRev != null) {
            previousEntity = queryCreater.forEntitiesAtRevision(aClass, previousSingleRev).
                    add(AuditEntity.id().eq(entityId)).
                    getSingleResult();
        }

        return previousEntity;
    }

    private Serializable getEntityId(String entityId, Field[] declaredFields) {
        for (Field field : declaredFields) {
            if (field.isAnnotationPresent(Id.class)) {
                return getEntityId(entityId, field.getType());
            }
        }

        return null;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Serializable getEntityId(String entityId, Class aClass) {
        Gson gson = new Gson();
        Serializable rtn = null;

        try {
            rtn = (Serializable) gson.fromJson(entityId, aClass);
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return rtn;
    }

    private void generateAuditDetailsList(AuditRecordDetailsType rtn, Object currentEntity, Object previousEntity, String entityName, String revType) {

        if ("DEL".equals(revType)) {
            Field[] fields = getAllDeclaredFields(previousEntity);

            for (Field field : fields) {
                if ((field.isAnnotationPresent(Column.class) || field.isAnnotationPresent(Embedded.class)) && !field.isAnnotationPresent(NotAudited.class)) {

                    field.setAccessible(true);
                    String fieldName = field.getName();
                    Object currentValue = null;
                    Object previousValue = null;

                    try {
                        previousValue = field.get(previousEntity);
                    } catch (IllegalArgumentException e) {
                    } catch (IllegalAccessException e) {
                        log.error(e.getMessage());
                    }

                    AuditEntityFieldChangeType entityChangeType = new AuditEntityFieldChangeType();

                    if (field.isAnnotationPresent(Embedded.class)) {
                        generateAuditDetailsList(rtn, currentValue, previousValue, entityName, revType);
                    } else {
                        entityChangeType.setEntityName(entityName);
                        entityChangeType.setRevType(revType);
                        entityChangeType.setField(fieldName);
                        entityChangeType.setChangedTo(getFieldValue(currentValue));
                        entityChangeType.setChangedFrom(getFieldValue(previousValue));

                        rtn.addAuditedEntityChangeType(entityChangeType);
                    }
                }
            }
        } else {
            Field[] fields = getAllDeclaredFields(currentEntity);

            for (Field field : fields) {
                if ((field.isAnnotationPresent(Column.class) || field.isAnnotationPresent(Embedded.class)) && !field.isAnnotationPresent(NotAudited.class)) {

                    field.setAccessible(true);
                    String fieldName = field.getName();
                    Object currentValue = null;
                    Object previousValue = null;

                    try {
                        currentValue = field.get(currentEntity);
                        previousValue = (previousEntity == null) ? null : field.get(previousEntity);
                    } catch (IllegalArgumentException e) {
                    } catch (IllegalAccessException e) {
                        log.error(e.getMessage());
                    }

                    AuditEntityFieldChangeType entityChangeType = new AuditEntityFieldChangeType();

                    if (field.isAnnotationPresent(Embedded.class)) {
                        generateAuditDetailsList(rtn, currentValue, previousValue, entityName, revType);
                    } else {
                        entityChangeType.setEntityName(entityName);
                        entityChangeType.setRevType(revType);
                        entityChangeType.setField(fieldName);
                        entityChangeType.setChangedTo(getFieldValue(currentValue));
                        entityChangeType.setChangedFrom(getFieldValue(previousValue));

                        rtn.addAuditedEntityChangeType(entityChangeType);
                    }
                }
            }
        }
    }

    private Field[] getAllDeclaredFields(Object entity) {
        Field[] fieldsA = entity.getClass().getDeclaredFields();

        Field[] fieldsB = null;
        Class<?> supClass = entity.getClass().getSuperclass();
        if (supClass != null && supClass.getAnnotation(Entity.class) != null) {
            fieldsB = supClass.getDeclaredFields();
        }

        Field[] fields = (Field[]) ArrayUtils.addAll(fieldsA, fieldsB);
        return fields;
    }

    private String getFieldValue(Object value) {
        if (value == null) {
			return null;
		}

        if (value.getClass().equals(Long.class)) {
            Long rtn = (Long) value;
            return rtn.toString();
        }

        if (value.getClass().equals(String.class)) {
            String rtn = (String) value;
            return rtn;
        }

        if (value.getClass().equals(Date.class)) {
            Date rtn = (Date) value;
            return rtn.toString();
        }

        if (value.getClass().equals(Enum.class)) {
            Enum<?> rtn = (Enum<?>) value;
            return rtn.toString();
        }

        if (value.getClass().equals(Boolean.class)) {
            Boolean rtn = (Boolean) value;
            return rtn.toString();
        }

        if (value.getClass().equals(Integer.class)) {
            Integer rtn = (Integer) value;
            return rtn.toString();
        }

        return value.toString();
    }

    private Date getDateFromString(String dateString) {
        SimpleDateFormat formatFrom = new SimpleDateFormat("MM/dd/yyyy");
        Date date = null;
        try {
            date = formatFrom.parse(dateString);
        } catch (Exception ex) {
            // no need to log , it is not a date
            //log.error("Could not Parse");
        }
        return date;
    }

}
