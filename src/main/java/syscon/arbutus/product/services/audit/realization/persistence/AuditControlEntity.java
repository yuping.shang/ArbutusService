package syscon.arbutus.product.services.audit.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import syscon.arbutus.product.services.audit.contract.dto.AuditModule;
import syscon.arbutus.product.services.realization.model.StampEntity;

@Entity
@Table(name = "AUD_Control")
public class AuditControlEntity implements Serializable {

    private static final long serialVersionUID = 6332367180840867675L;

    /**
     * @DbComment AuditControlId 'Unique identification for the audit control record'
     */
    @Id
    @Column(name = "AuditControlId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_AUD_Control_Id")
    @SequenceGenerator(name = "SEQ_AUD_Control_Id", sequenceName = "SEQ_AUD_Control_Id", allocationSize = 1)
    private Long auditControlId;

    /**
     * @DbComment AuditModule 'The module name of audit module.
     */
    @Column(name = "AuditModule", nullable = false, length = 64)
    @Enumerated(EnumType.STRING)
    private AuditModule auditModule;

    /**
     * @DbComment OnAudit 'whether the audit module is on audit.
     */
    @Column(name = "OnAudit", nullable = false)
    private Boolean onAudit;

    /**
     * @DbComment UpdatedTime 'The time when the audit module is turn on/off.'
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UpdatedTime", nullable = false)
    private Date updatedTime;

    /**
     * @DbComment version 'the version number of record updated'
     */
    @Version
    private Long version;

    /**
     * @DbComment CreateUserId 'User ID who created the object'
     * @DbComment CreateDateTime 'Date and time when the object was created'
     * @DbComment InvocationContext 'Invocation context when the create/update action called'
     * @DbComment ModifyUserId 'User ID who last updated the object'
     * @DbComment ModifyDateTime 'Date and time when the object was last updated'
     */
    @Embedded
    private StampEntity stamp;

    public Long getAuditControlId() {
        return auditControlId;
    }

    public void setAuditControlId(Long auditControlId) {
        this.auditControlId = auditControlId;
    }

    public AuditModule getAuditModule() {
        return auditModule;
    }

    public void setAuditModule(AuditModule auditModule) {
        this.auditModule = auditModule;
    }

    public Boolean isOnAudit() {
        return onAudit;
    }

    public void setOnAudit(Boolean onAudit) {
        this.onAudit = onAudit;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public StampEntity getStamp() {
        return stamp;
    }

    public void setStamp(StampEntity stamp) {
        this.stamp = stamp;
    }

    @Override
    public String toString() {
        return "AuditControlEntity [auditControlId=" + auditControlId + ", auditModule=" + auditModule + ", onAudit=" + onAudit + ", updatedTime=" + updatedTime + "]";
    }

}
