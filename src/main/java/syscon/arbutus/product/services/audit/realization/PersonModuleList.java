package syscon.arbutus.product.services.audit.realization;

public enum PersonModuleList {

    CautionEntity,
    CautionCommentEntity;

    public static PersonModuleList fromValue(String v) {
        return valueOf(v);
    }

    public static boolean contain(String v) {

        for (PersonModuleList entityName : PersonModuleList.values()) {
            if (entityName.value().equals(v)) {
                return true;
            }
        }
        return false;
    }

    public String value() {
        return name();
    }
}
