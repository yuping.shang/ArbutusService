package syscon.arbutus.product.services.audit.realization.persistence.eventaudit;

/**
 * Created by YShang on 17/02/14.
 */
public enum UserActionNameEnum {
    LOG_IN, LOG_OUT, LOG_FAILED, PAGE_VIEW
}
