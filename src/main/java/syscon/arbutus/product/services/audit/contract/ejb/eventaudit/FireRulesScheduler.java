package syscon.arbutus.product.services.audit.contract.ejb.eventaudit;

/**
 * Created by YShang on 12/02/14.
 */

import javax.annotation.PostConstruct;
import javax.ejb.*;

import syscon.arbutus.product.services.audit.contract.interfaces.eventaudit.EventAuditService;

/**
 * Scheduler which fires rules,
 */
@Startup
@Singleton
@LocalBean
@DependsOn({ "EventAuditServiceBean" })
public class FireRulesScheduler {

    @EJB
    private EventAuditService drools;

    @PostConstruct
    @Schedule(hour = "*", minute = "*/5", second = "0", persistent = false)
    public void init() {
        drools.fireAllRules();
    }

}
