package syscon.arbutus.product.services.audit.realization;

import org.hibernate.envers.configuration.AuditConfiguration;
import org.hibernate.envers.event.EnversPreCollectionRemoveEventListenerImpl;
import org.hibernate.event.spi.PreCollectionRemoveEvent;
import syscon.arbutus.product.services.audit.contract.dto.AuditModule;

public class CustomPreCollectionRemoveEnversListener extends EnversPreCollectionRemoveEventListenerImpl {

    private static final long serialVersionUID = -7238695130627893059L;
    private AuditServiceAdapter auditAdapter;

    protected CustomPreCollectionRemoveEnversListener(AuditConfiguration enversConfiguration) {
        super(enversConfiguration);
        auditAdapter = new AuditServiceAdapter();
    }

    @Override
    public void onPreRemoveCollection(PreCollectionRemoveEvent event) {

        boolean isAuditOn = auditAdapter.isAuditOn(AuditModule.REVISION);
        if (isAuditOn) {
            super.onPreRemoveCollection(event);
        }

    }

}
