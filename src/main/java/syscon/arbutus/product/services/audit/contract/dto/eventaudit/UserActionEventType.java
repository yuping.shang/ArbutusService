package syscon.arbutus.product.services.audit.contract.dto.eventaudit;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by YShang on 11/02/14.
 * Updated by LHan.
 */
public class UserActionEventType implements Comparable<UserActionEventType>, Serializable {
    private static final long serialVersionUID = 3345519666210211172L;
    @NotNull
    private String loginId;

    ;
    @NotNull
    private UserActionName userActionName;
    private Date eventDatetime = new Date();
    private UserPageViewInmateType inmateInContext;

    public UserActionEventType() {
        super();
    }

    public UserActionEventType(@NotNull String loginId, @NotNull UserActionName userActionName) {
        super();
        this.setLoginId(loginId);
        this.userActionName = userActionName;
    }

    public UserActionEventType(@NotNull String loginId, @NotNull UserActionName userActionName, UserPageViewInmateType inmateInContext) {
        super();
        this.setLoginId(loginId);
        this.userActionName = userActionName;
        this.setInmateInContext(inmateInContext);
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public UserActionName getUserActionName() {
        return userActionName;
    }

    public void setUserActionName(UserActionName UserActionName) {
        this.userActionName = UserActionName;
    }

    public Date getEventDatetime() {
        return eventDatetime;
    }

    public void setEventDatetime(Date eventDatetime) {
        this.eventDatetime = eventDatetime;
    }

    @Override
    public int compareTo(UserActionEventType o) {
        return o.getEventDatetime().compareTo(eventDatetime);
    }

    /**
     * @return the inmateInContext
     */
    public UserPageViewInmateType getInmateInContext() {
        return inmateInContext;
    }

    /**
     * @param inmateInContext the inmateInContext to set
     */
    public void setInmateInContext(UserPageViewInmateType inmateInContext) {
        this.inmateInContext = inmateInContext;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        UserActionEventType other = (UserActionEventType) obj;
        if (eventDatetime == null) {
            if (other.eventDatetime != null) {
				return false;
			}
        } else if (!eventDatetime.equals(other.eventDatetime)) {
			return false;
		}
        if (inmateInContext == null) {
            if (other.inmateInContext != null) {
				return false;
			}
        } else if (!inmateInContext.equals(other.inmateInContext)) {
			return false;
		}
        if (loginId == null) {
            if (other.loginId != null) {
				return false;
			}
        } else if (!loginId.equals(other.loginId)) {
			return false;
		}
        if (userActionName != other.userActionName) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
	 */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((eventDatetime == null) ? 0 : eventDatetime.hashCode());
        result = prime * result + ((inmateInContext == null) ? 0 : inmateInContext.hashCode());
        result = prime * result + ((loginId == null) ? 0 : loginId.hashCode());
        result = prime * result + ((userActionName == null) ? 0 : userActionName.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
	 */
    @Override
    public String toString() {
        return "UserActionEventType [loginId=" + loginId + ", userActionName=" + userActionName + ", eventDatetime=" + eventDatetime + ", inmateInContext="
                + inmateInContext + "]";
    }

    public enum UserActionName {LOG_IN, LOG_OUT, LOG_FAILED, PAGE_VIEW}

}
