package syscon.arbutus.product.services.audit.contract.ejb.eventaudit;

import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import syscon.arbutus.product.services.audit.realization.persistence.eventaudit.UserLoginEventEntity;

/**
 * Created by YShang on 12/02/14.
 */
@Stateless
public class UserEventBean extends AbstractPersistenceBean<UserLoginEventEntity> implements Serializable {

    private static final long serialVersionUID = 972353111102329706L;

    @PersistenceContext(unitName = "arbutus-pu")
    Session session;

    public UserEventBean() {
        super(UserLoginEventEntity.class);
    }

    @Override
    protected Session getSession() {
        return session;
    }

    /**
     * Returns user of given loginId, null if doesn't exist.
     *
     * @param loginId of user to be returned
     * @return user of given username, null if doesn't exist
     */
    public UserLoginEventEntity getUserByLoginId(String loginId) {
        Criteria criteria = getSession().createCriteria(UserLoginEventEntity.class);
        criteria.add(Restrictions.eq("loginId", loginId));
        criteria.setMaxResults(1);

        if (criteria.list().size() > 0) {
            return (UserLoginEventEntity) criteria.uniqueResult();
        } else {
            return null;
        }
    }
}
