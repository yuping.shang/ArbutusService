package syscon.arbutus.product.services.audit.realization;

import org.hibernate.envers.configuration.AuditConfiguration;
import org.hibernate.envers.event.EnversPostDeleteEventListenerImpl;
import org.hibernate.event.spi.PostDeleteEvent;
import syscon.arbutus.product.services.audit.contract.dto.AuditModule;

public class CustomDeleteEnversListener extends EnversPostDeleteEventListenerImpl {

    private static final long serialVersionUID = -8344726675475156511L;

    private AuditServiceAdapter auditAdapter;

    protected CustomDeleteEnversListener(AuditConfiguration enversConfiguration) {

        super(enversConfiguration);
        auditAdapter = new AuditServiceAdapter();

    }

    @Override
    public void onPostDelete(PostDeleteEvent event) {

        boolean isAuditOn = auditAdapter.isAuditOn(AuditModule.REVISION);
        if (isAuditOn) {
            super.onPostDelete(event);
        }

    }

}
