package syscon.arbutus.product.services.audit.realization.persistence.eventaudit;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import syscon.arbutus.product.services.core.contract.dto.DataFlag;

/**
 * @author LHan
 */
@Entity
@Table(name = "AUD_UserPageViewEvent")
@SQLDelete(sql = "UPDATE AUD_UserPageViewEvent SET flag = 4 WHERE userPageViewEventId = ? and version = ?")
@Where(clause = "flag = 1")
public class UserPageViewEventEntity implements Serializable {

    private static final long serialVersionUID = 6192463189389708758L;

    @Id
    @Column(name = "userPageViewEventId")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_AUD_UserPageViewEvent_Id")
    @SequenceGenerator(name = "SEQ_AUD_UserPageViewEvent_Id", sequenceName = "SEQ_AUD_UserPageViewEvent_Id", allocationSize = 1)
    private Long userPageViewEventId;

    @Column(name = "LoginId", nullable = false, length = 64)
    private String loginId;

    @Column(name = "pageViewDateTime", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date pageViewDateTime;

    @Column(name = "InmatePersonId", nullable = false)
    private Long inmatePersonId;

    @Column(name = "InmatePsnIdentityId", nullable = false)
    private Long inmatePersonIdentityId;

    @Column(name = "SupervisionDisplayId", nullable = false, length = 64)
    private String supervisionDisplayId;

    @Column(name = "InmateFirstName", nullable = false, length = 128)
    private String inmateFirstName;

    @Column(name = "InmateLastName", nullable = false, length = 128)
    private String inmateLastName;

    @Column(name = "ViewedPageInfo", nullable = false, length = 512)
    private String ViewedPageInfo;

    /**
     * @DbComment version 'the version number of record updated'
     */
    @Version
    private Long version;

    /**
     * @DbComment flag '1=Active, 2=Seal, 3=Juvenile, 4=Delete, 5=Purge'
     */
    @Column(nullable = false)
    private Long flag = DataFlag.ACTIVE.value();

    @Column(name = "CreateDateTime", nullable = true, length = 64)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDateTime;

    /**
     * @return the userPageViewEventId
     */
    public Long getUserPageViewEventId() {
        return userPageViewEventId;
    }

    /**
     * @param userPageViewEventId the userPageViewEventId to set
     */
    public void setUserPageViewEventId(Long userPageViewEventId) {
        this.userPageViewEventId = userPageViewEventId;
    }

    /**
     * @return the loginId
     */
    public String getLoginId() {
        return loginId;
    }

    /**
     * @param loginId the loginId to set
     */
    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    /**
     * @return the pageViewDateTime
     */
    public Date getPageViewDateTime() {
        return pageViewDateTime;
    }

    /**
     * @param pageViewDateTime the pageViewDateTime to set
     */
    public void setPageViewDateTime(Date pageViewDateTime) {
        this.pageViewDateTime = pageViewDateTime;
    }

    /**
     * @return the inmatePersonId
     */
    public Long getInmatePersonId() {
        return inmatePersonId;
    }

    /**
     * @param inmatePersonId the inmatePersonId to set
     */
    public void setInmatePersonId(Long inmatePersonId) {
        this.inmatePersonId = inmatePersonId;
    }

    /**
     * @return the inmatePersonIdentityId
     */
    public Long getInmatePersonIdentityId() {
        return inmatePersonIdentityId;
    }

    /**
     * @param inmatePersonIdentityId the inmatePersonIdentityId to set
     */
    public void setInmatePersonIdentityId(Long inmatePersonIdentityId) {
        this.inmatePersonIdentityId = inmatePersonIdentityId;
    }

    /**
     * @return the supervisionDisplayId
     */
    public String getSupervisionDisplayId() {
        return supervisionDisplayId;
    }

    /**
     * @param supervisionDisplayId the supervisionDisplayId to set
     */
    public void setSupervisionDisplayId(String supervisionDisplayId) {
        this.supervisionDisplayId = supervisionDisplayId;
    }

    /**
     * @return the inmateFirstName
     */
    public String getInmateFirstName() {
        return inmateFirstName;
    }

    /**
     * @param inmateFirstName the inmateFirstName to set
     */
    public void setInmateFirstName(String inmateFirstName) {
        this.inmateFirstName = inmateFirstName;
    }

    /**
     * @return the inmateLastName
     */
    public String getInmateLastName() {
        return inmateLastName;
    }

    /**
     * @param inmateLastName the inmateLastName to set
     */
    public void setInmateLastName(String inmateLastName) {
        this.inmateLastName = inmateLastName;
    }

    /**
     * @return the viewedPageInfo
     */
    public String getViewedPageInfo() {
        return ViewedPageInfo;
    }

    /**
     * @param viewedPageInfo the viewedPageInfo to set
     */
    public void setViewedPageInfo(String viewedPageInfo) {
        ViewedPageInfo = viewedPageInfo;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the flag
     */
    public Long getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Long flag) {
        this.flag = flag;
    }

    /**
     * @return the createDateTime
     */
    public Date getCreateDateTime() {
        return createDateTime;
    }

    /**
     * @param createDateTime the createDateTime to set
     */
    public void setCreateDateTime(Date createDateTime) {
        this.createDateTime = createDateTime;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ViewedPageInfo == null) ? 0 : ViewedPageInfo.hashCode());
        result = prime * result + ((createDateTime == null) ? 0 : createDateTime.hashCode());
        result = prime * result + ((flag == null) ? 0 : flag.hashCode());
        result = prime * result + ((inmateFirstName == null) ? 0 : inmateFirstName.hashCode());
        result = prime * result + ((inmateLastName == null) ? 0 : inmateLastName.hashCode());
        result = prime * result + ((inmatePersonId == null) ? 0 : inmatePersonId.hashCode());
        result = prime * result + ((inmatePersonIdentityId == null) ? 0 : inmatePersonIdentityId.hashCode());
        result = prime * result + ((loginId == null) ? 0 : loginId.hashCode());
        result = prime * result + ((pageViewDateTime == null) ? 0 : pageViewDateTime.hashCode());
        result = prime * result + ((supervisionDisplayId == null) ? 0 : supervisionDisplayId.hashCode());
        result = prime * result + ((userPageViewEventId == null) ? 0 : userPageViewEventId.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        UserPageViewEventEntity other = (UserPageViewEventEntity) obj;
        if (ViewedPageInfo == null) {
            if (other.ViewedPageInfo != null) {
				return false;
			}
        } else if (!ViewedPageInfo.equals(other.ViewedPageInfo)) {
			return false;
		}
        if (createDateTime == null) {
            if (other.createDateTime != null) {
				return false;
			}
        } else if (!createDateTime.equals(other.createDateTime)) {
			return false;
		}
        if (flag == null) {
            if (other.flag != null) {
				return false;
			}
        } else if (!flag.equals(other.flag)) {
			return false;
		}
        if (inmateFirstName == null) {
            if (other.inmateFirstName != null) {
				return false;
			}
        } else if (!inmateFirstName.equals(other.inmateFirstName)) {
			return false;
		}
        if (inmateLastName == null) {
            if (other.inmateLastName != null) {
				return false;
			}
        } else if (!inmateLastName.equals(other.inmateLastName)) {
			return false;
		}
        if (inmatePersonId == null) {
            if (other.inmatePersonId != null) {
				return false;
			}
        } else if (!inmatePersonId.equals(other.inmatePersonId)) {
			return false;
		}
        if (inmatePersonIdentityId == null) {
            if (other.inmatePersonIdentityId != null) {
				return false;
			}
        } else if (!inmatePersonIdentityId.equals(other.inmatePersonIdentityId)) {
			return false;
		}
        if (loginId == null) {
            if (other.loginId != null) {
				return false;
			}
        } else if (!loginId.equals(other.loginId)) {
			return false;
		}
        if (pageViewDateTime == null) {
            if (other.pageViewDateTime != null) {
				return false;
			}
        } else if (!pageViewDateTime.equals(other.pageViewDateTime)) {
			return false;
		}
        if (supervisionDisplayId == null) {
            if (other.supervisionDisplayId != null) {
				return false;
			}
        } else if (!supervisionDisplayId.equals(other.supervisionDisplayId)) {
			return false;
		}
        if (userPageViewEventId == null) {
            if (other.userPageViewEventId != null) {
				return false;
			}
        } else if (!userPageViewEventId.equals(other.userPageViewEventId)) {
			return false;
		}
        if (version == null) {
            if (other.version != null) {
				return false;
			}
        } else if (!version.equals(other.version)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "UserPageViewEventEntity [userPageViewEventId=" + userPageViewEventId + ", loginId=" + loginId + ", pageViewDateTime=" + pageViewDateTime
                + ", inmatePersonId=" + inmatePersonId + ", inmatePersonIdentityId=" + inmatePersonIdentityId + ", supervisionDisplayId=" + supervisionDisplayId
                + ", inmateFirstName=" + inmateFirstName + ", inmateLastName=" + inmateLastName + ", ViewedPageInfo=" + ViewedPageInfo + ", version=" + version
                + ", flag=" + flag + ", createDateTime=" + createDateTime + "]";
    }
}
