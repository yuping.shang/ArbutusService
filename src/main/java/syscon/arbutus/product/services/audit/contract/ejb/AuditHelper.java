package syscon.arbutus.product.services.audit.contract.ejb;

import syscon.arbutus.product.services.audit.contract.dto.AuditControlType;
import syscon.arbutus.product.services.audit.realization.persistence.AuditControlEntity;

public class AuditHelper {

    public static AuditControlType toAuditControlType(AuditControlEntity entity) {

        if (entity == null) {
			return null;
		}

        AuditControlType ret = new AuditControlType();
        ret.setAuditControlId(entity.getAuditControlId());
        ret.setAuditModule(entity.getAuditModule());
        ret.setOnAudit(entity.isOnAudit());
        ret.setUpdatedTime(entity.getUpdatedTime());

        return ret;
    }

    public static AuditControlEntity toAuditControlEntity(AuditControlType dto) {

        if (dto == null) {
			return null;
		}

        AuditControlEntity entity = new AuditControlEntity();

        entity.setAuditControlId(dto.getAuditControlId());
        entity.setAuditModule(dto.getAuditModule());
        entity.setOnAudit(dto.isOnAudit());
        entity.setUpdatedTime(dto.getUpdatedTime());

        return entity;
    }
}
