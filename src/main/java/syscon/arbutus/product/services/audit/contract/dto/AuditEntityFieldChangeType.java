package syscon.arbutus.product.services.audit.contract.dto;

import java.io.Serializable;

/**
 * @author LHan
 */
public class AuditEntityFieldChangeType implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4626622990195427086L;

    private String entityName;
    private String revType;
    private String field;
    private String changedTo;
    private String changedFrom;

    /**
     * @return the field
     */
    public String getField() {
        return field;
    }

    /**
     * @param field the field to set
     */
    public void setField(String field) {
        this.field = field;
    }

    /**
     * @return the changedTo
     */
    public String getChangedTo() {
        return changedTo;
    }

    /**
     * @param changedTo the changedTo to set
     */
    public void setChangedTo(String changedTo) {
        this.changedTo = changedTo;
    }

    /**
     * @return the changedFrom
     */
    public String getChangedFrom() {
        return changedFrom;
    }

    /**
     * @param changedFrom the changedFrom to set
     */
    public void setChangedFrom(String changedFrom) {
        this.changedFrom = changedFrom;
    }

    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /**
     * @return the revType
     */
    public String getRevType() {
        return revType;
    }

    /**
     * @param revType the revType to set
     */
    public void setRevType(String revType) {
        this.revType = revType;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((changedFrom == null) ? 0 : changedFrom.hashCode());
        result = prime * result + ((changedTo == null) ? 0 : changedTo.hashCode());
        result = prime * result + ((entityName == null) ? 0 : entityName.hashCode());
        result = prime * result + ((field == null) ? 0 : field.hashCode());
        result = prime * result + ((revType == null) ? 0 : revType.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        AuditEntityFieldChangeType other = (AuditEntityFieldChangeType) obj;
        if (changedFrom == null) {
            if (other.changedFrom != null) {
				return false;
			}
        } else if (!changedFrom.equals(other.changedFrom)) {
			return false;
		}
        if (changedTo == null) {
            if (other.changedTo != null) {
				return false;
			}
        } else if (!changedTo.equals(other.changedTo)) {
			return false;
		}
        if (entityName == null) {
            if (other.entityName != null) {
				return false;
			}
        } else if (!entityName.equals(other.entityName)) {
			return false;
		}
        if (field == null) {
            if (other.field != null) {
				return false;
			}
        } else if (!field.equals(other.field)) {
			return false;
		}
        if (revType == null) {
            if (other.revType != null) {
				return false;
			}
        } else if (!revType.equals(other.revType)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "AuditEntityFieldChangeType [entityName=" + entityName + ", revType=" + revType + ", field=" + field + ", changedTo=" + changedTo + ", changedFrom="
                + changedFrom + "]";
    }

}
