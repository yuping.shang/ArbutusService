package syscon.arbutus.product.services.audit.contract.interfaces.eventaudit;

import java.util.SortedSet;

import syscon.arbutus.product.services.audit.contract.dto.eventaudit.UserActionEventType;

/**
 * Created by YShang on 11/02/14.
 */
public interface EventAuditService {
    public void insertFact(Object fact);

    public void fireAllRules();

    public SortedSet<UserActionEventType> getAllRecentlyLoggedUserEvents();

    public SortedSet<UserActionEventType> getAllActionEventsForUser(String loginId);

    public SortedSet<UserActionEventType> getAllFailedLoggedUserEvents();

    public SortedSet<UserActionEventType> getAllFailedLoggedUserEventsForUser(String loginId);

    public SortedSet<UserActionEventType> getAllFailedLoggedEventsForUser(String loginId);
}
