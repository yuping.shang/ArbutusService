package syscon.arbutus.product.services.audit.realization;

import org.hibernate.envers.configuration.AuditConfiguration;
import org.hibernate.envers.event.EnversPostCollectionRecreateEventListenerImpl;
import org.hibernate.event.spi.PostCollectionRecreateEvent;
import syscon.arbutus.product.services.audit.contract.dto.AuditModule;

public class CustomCollectionRecreateEnversListener extends EnversPostCollectionRecreateEventListenerImpl {

    private static final long serialVersionUID = -5312389553807704335L;
    private AuditServiceAdapter auditAdapter;

    protected CustomCollectionRecreateEnversListener(AuditConfiguration enversConfiguration) {
        super(enversConfiguration);
        auditAdapter = new AuditServiceAdapter();
    }

    @Override
    public void onPostRecreateCollection(PostCollectionRecreateEvent event) {

        boolean isAuditOn = auditAdapter.isAuditOn(AuditModule.REVISION);
        if (isAuditOn) {
            super.onPostRecreateCollection(event);

        }

    }

}
