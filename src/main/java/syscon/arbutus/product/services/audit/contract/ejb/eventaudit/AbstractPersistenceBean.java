package syscon.arbutus.product.services.audit.contract.ejb.eventaudit;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;

/**
 * Class which represents abstract facade and makes possible to call
 * parameterized methods.
 *
 * @param <T>entity Created by YShang on 12/02/14.
 */
public abstract class AbstractPersistenceBean<T> {

    private Class<T> entityClass;

    public AbstractPersistenceBean() {
    }

    public AbstractPersistenceBean(final Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract Session getSession();

    public void create(T entity) {
        getSession().persist(entity);
    }

    public void update(T entity) {
        getSession().merge(entity);
    }

    public T merge(T entity) {
        return (T) getSession().merge(entity);
    }

    public void delete(T entity) {
        getSession().delete(getSession().merge(entity));
    }

    public void delete(Long id) {
        getSession().delete(find(id));
    }

    public T get(Long id) {
        return (T) getSession().get(entityClass, id);
    }

    public T find(Long id) {
        return (T) getSession().get(entityClass, id);
    }

    public List<T> findAll() {
        Criteria criteria = getSession().createCriteria(entityClass);

        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<T> findRange(int from, int to) {
        Criteria criteria = getSession().createCriteria(entityClass);
        criteria.setMaxResults(to - from);
        criteria.setFirstResult(from);

        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<T> findRange(int from, int to, String queryName, Map<String, String> parameters) {
        Query q = getSession().getNamedQuery(queryName);
        q.setMaxResults(to - from);
        q.setFirstResult(from);
        if (parameters != null) {
            q.setProperties(parameters);
        }

        return q.list();
    }

    public void deleteAll() {
        Session session = getSession();
        session.createSQLQuery("UPDATE EA_User SET flag = 1").executeUpdate();

        session.createSQLQuery("delete EA_User").executeUpdate();

        session.flush();
        session.clear();
    }

    public List<Long> getAll() {
        Criteria criteria = getSession().createCriteria(entityClass);
        criteria.setProjection(Projections.id());

        return criteria.list();
    }

    public Long getCount() {
        Criteria criteria = getSession().createCriteria(entityClass);
        criteria.setProjection(Projections.rowCount());

        return (Long) criteria.uniqueResult();
    }
}
