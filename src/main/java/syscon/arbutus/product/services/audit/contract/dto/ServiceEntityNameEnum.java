package syscon.arbutus.product.services.audit.contract.dto;

import java.util.HashSet;
import java.util.Set;

public enum ServiceEntityNameEnum {

    //search for service is based on Entity packages and contains method. to be able to differentiate btw the
    //packages that begin with  the same string, add "."
    Supervision                     ("syscon.arbutus.product.services.supervision."),
    Caution                         ("syscon.arbutus.product.services.caution."),
    Location                        ("syscon.arbutus.product.services.location."),
    Organization                    ("syscon.arbutus.product.services.organization."),
    Activity                        ("syscon.arbutus.product.services.activity."),
    Notification                    ("syscon.arbutus.product.services.notification."),
    Discipline                      ("syscon.arbutus.product.services.discipline."),
    Person                          ("syscon.arbutus.product.services.person."),
    PersonPerson                    ("syscon.arbutus.product.services.personperson."),
    Facility                        ("syscon.arbutus.product.services.facility."),
    Staff                           ("syscon.arbutus.product.services.staff."),
    Assessment                      ("syscon.arbutus.product.services.assessment."),
    MovementActivity                ("syscon.arbutus.product.services.movementactivity."),
    PersonIdentity                  ("syscon.arbutus.product.services.person."),
    Registry                        ("syscon.arbutus.product.services.registry."),
    Security                        ("syscon.arbutus.product.services.security."),
    Biometric                       ("syscon.arbutus.product.services.biometric."),
    BPMExtension                    ("syscon.arbutus.product.services.bpmextension."),
    CasePlan                        ("syscon.arbutus.product.services.caseplan."),
    ClientCustomField               ("syscon.arbutus.product.services.clientcustomfield."),
    DataSecurity                    ("syscon.arbutus.product.services.datasecurity."),
    EventLog                        ("syscon.arbutus.product.services.eventlog."),
    FacilityCountActivity           ("syscon.arbutus.product.services.facilitycountactivity."),
    FacilityInternalLocation        ("syscon.arbutus.product.services.facilityinternallocation."),
    Grievances                      ("syscon.arbutus.product.services.grievances."),
    HousingBedManagementActivity    ("syscon.arbutus.product.services.housingbedmanagementactivity."),
    Legal                           ("syscon.arbutus.product.services.legal."),
    Property                        ("syscon.arbutus.product.services.property."),
    Visitation                      ("syscon.arbutus.product.services.visitation."),
    Program                         ("syscon.arbutus.product.services.program."),
    UserPreference                  ("syscon.arbutus.product.services.userpreference."),
    ReferenceData                   ("syscon.arbutus.product.services.referencedata."),
    SecurityThreadGroup             ("syscon.arbutus.product.services.securitythreatgroup.");

    private final String value;

    ServiceEntityNameEnum(String v) {
        value = v;
    }

    public static ServiceEntityNameEnum fromValue(String v) {
        for (ServiceEntityNameEnum c : ServiceEntityNameEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public static String fromValueToName(String value) {
        for (ServiceEntityNameEnum c : ServiceEntityNameEnum.values()) {
            if (c.value.equals(value)) {
                return c.name();
            }
        }
        return "";
    }

    public static String fromNameToValue(String name) {
        for (ServiceEntityNameEnum c : ServiceEntityNameEnum.values()) {
            if (c.name().equalsIgnoreCase(name)) {
                return c.value();
            }
        }
        return "";
    }

    public static String fromEntityToServiceName(String value) {
        for (ServiceEntityNameEnum c : ServiceEntityNameEnum.values()) {
            if (value.contains(c.value)) {
                return c.name();
            }
        }
        return "";
    }

    public static Set<String> getServiceNames() {
        Set<String> rtn = new HashSet<String>();
        for (ServiceEntityNameEnum c : ServiceEntityNameEnum.values()) {
            rtn.add(c.name());
        }
        return rtn;
    }

    public String value() {
        return value;
    }
}
