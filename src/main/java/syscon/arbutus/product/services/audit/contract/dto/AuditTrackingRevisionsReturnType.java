/**
 *
 */
package syscon.arbutus.product.services.audit.contract.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author LHan
 */
public class AuditTrackingRevisionsReturnType implements Serializable {

    private static final long serialVersionUID = -734863934534383317L;

    private List<AuditTrackingRevisionType> auditRevisionList;
    private Long totalSize;

    /**
     * @return the auditRevisionList
     */
    public List<AuditTrackingRevisionType> getAuditRevisionList() {
        return auditRevisionList;
    }

    /**
     * @param auditRevisionList the auditRevisionList to set
     */
    public void setAuditRevisionList(List<AuditTrackingRevisionType> auditRevisionList) {
        this.auditRevisionList = auditRevisionList;
    }

    /**
     * @return the totalSize
     */
    public Long getTotalSize() {
        return totalSize;
    }

    /**
     * @param totalSize the totalSize to set
     */
    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "AuditTrackingRevisionsReturnType [auditRevisionList=" + auditRevisionList + ", totalSize=" + totalSize + "]";
    }

}
