package syscon.arbutus.product.services.audit.contract.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author LHan
 */
public class AuditRecordDetailsType implements Serializable {

    private static final long serialVersionUID = 4333415716419544055L;

    private String serviceName;
    private String modifiedUser;
    private Date modifiedDateTime;
    private Long revId;
    private List<AuditEntityFieldChangeType> auditedEntitiesChanges = null;

    /**
     * @return the serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @param serviceName the serviceName to set
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * @return the modifiedUser
     */
    public String getModifiedUser() {
        return modifiedUser;
    }

    /**
     * @param modifiedUser the modifiedUser to set
     */
    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser;
    }

    /**
     * @return the modifiedDateTime
     */
    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    /**
     * @param modifiedDateTime the modifiedDateTime to set
     */
    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    /**
     * @return the revId
     */
    public Long getRevId() {
        return revId;
    }

    /**
     * @param revId the revId to set
     */
    public void setRevId(Long revId) {
        this.revId = revId;
    }

    /**
     * @return the auditedEntityChangeType
     */
    public List<AuditEntityFieldChangeType> getAuditedEntityChangeType() {
        return auditedEntitiesChanges;
    }

    /**
     * @param auditedEntityChangeType the auditedEntityChangeType to set
     */
    public void setAuditedEntityChangeType(List<AuditEntityFieldChangeType> auditedEntitiesChanges) {
        this.auditedEntitiesChanges = auditedEntitiesChanges;
    }

    public void addAuditedEntityChangeType(AuditEntityFieldChangeType anEntityChangeType) {
        if (auditedEntitiesChanges == null) {
			auditedEntitiesChanges = new ArrayList<AuditEntityFieldChangeType>();
		}
        auditedEntitiesChanges.add(anEntityChangeType);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((auditedEntitiesChanges == null) ? 0 : auditedEntitiesChanges.hashCode());
        result = prime * result + ((modifiedDateTime == null) ? 0 : modifiedDateTime.hashCode());
        result = prime * result + ((modifiedUser == null) ? 0 : modifiedUser.hashCode());
        result = prime * result + ((revId == null) ? 0 : revId.hashCode());
        result = prime * result + ((serviceName == null) ? 0 : serviceName.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        AuditRecordDetailsType other = (AuditRecordDetailsType) obj;
        if (auditedEntitiesChanges == null) {
            if (other.auditedEntitiesChanges != null) {
				return false;
			}
        } else if (!auditedEntitiesChanges.equals(other.auditedEntitiesChanges)) {
			return false;
		}
        if (modifiedDateTime == null) {
            if (other.modifiedDateTime != null) {
				return false;
			}
        } else if (!modifiedDateTime.equals(other.modifiedDateTime)) {
			return false;
		}
        if (modifiedUser == null) {
            if (other.modifiedUser != null) {
				return false;
			}
        } else if (!modifiedUser.equals(other.modifiedUser)) {
			return false;
		}
        if (revId == null) {
            if (other.revId != null) {
				return false;
			}
        } else if (!revId.equals(other.revId)) {
			return false;
		}
        if (serviceName == null) {
            if (other.serviceName != null) {
				return false;
			}
        } else if (!serviceName.equals(other.serviceName)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "AuditRecordDetailsType [serviceName=" + serviceName + ", modifiedUser=" + modifiedUser + ", modifiedDateTime=" + modifiedDateTime + ", revId=" + revId
                + ", auditedEntitiesChanges=" + auditedEntitiesChanges + "]";
    }

}
