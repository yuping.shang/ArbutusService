package syscon.arbutus.product.services.audit.contract.ejb;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Session;
import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.audit.contract.dto.*;
import syscon.arbutus.product.services.audit.contract.dto.eventaudit.UserActionEventType;
import syscon.arbutus.product.services.audit.contract.dto.eventaudit.UserPageViewInmateType;
import syscon.arbutus.product.services.audit.contract.interfaces.AuditService;
import syscon.arbutus.product.services.audit.contract.interfaces.eventaudit.EventAuditService;
import syscon.arbutus.product.services.core.contract.dto.ReturnCode;
import syscon.arbutus.product.services.utility.ValidationHelper;

@Stateless(mappedName = "AuditService", description = "The Audit service")
@Remote(AuditService.class)
public class AuditServiceBean implements AuditService {

    @SuppressWarnings("unused")
    private static Logger log = LoggerFactory.getLogger(AuditServiceBean.class);

    @EJB
    private EventAuditService eventAudit;

    @Resource
    private SessionContext context;

    /**
     * Let the container create a session.
     */
    @PersistenceContext(unitName = "arbutus-pu")
    private Session session;

    @Override
    public AuditControlType create(UserContext uc, AuditControlType auditControl) {
        AuditHandler handler = new AuditHandler(context, session);
        AuditControlType ret = handler.create(uc, auditControl);
        return ret;
    }

    @Override
    public AuditControlType update(UserContext uc, AuditControlType auditControl) {
        AuditHandler handler = new AuditHandler(context, session);
        AuditControlType ret = handler.update(uc, auditControl);
        return ret;
    }

    @Override
    public Long delete(UserContext uc, Long auditControlId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Long deleteAll(UserContext uc) {
        AuditHandler handler = new AuditHandler(context, session);
        handler.deleteAll(uc);
        return ReturnCode.Success.returnCode();
    }

    @Override
    public boolean isAuditOn(UserContext uc, AuditModule auditModule) {
        AuditHandler handler = new AuditHandler(context, session);
        return handler.isAuditOn(uc, auditModule);
    }

    /////////////////////////////////////////////////////////////////////////////////
    // Page view and Login/Logout audit API.

    @Override
    public void recordUserPageView(String loginId, UserPageViewInmateType inmateInContext) {
        ValidationHelper.validate(inmateInContext);
        eventAudit.insertFact(new UserActionEventType(loginId, UserActionEventType.UserActionName.PAGE_VIEW, inmateInContext));
    }

    @Override
    public void recordUserLogIn(String loginId) {
        eventAudit.insertFact(new UserActionEventType(loginId, UserActionEventType.UserActionName.LOG_IN));
    }

    @Override
    public void recordUserLogOut(String loginId) {
        eventAudit.insertFact(new UserActionEventType(loginId, UserActionEventType.UserActionName.LOG_OUT));
    }

    @Override
    public void recordUserLogInFailed(String loginId) {
        eventAudit.insertFact(new UserActionEventType(loginId, UserActionEventType.UserActionName.LOG_FAILED));
    }

    @Override
    public AuditRecordDetailsType getAuditRecordDetails(UserContext uc, Long revId, String serviceName) {
        AuditHandler handler = new AuditHandler(context, session);
        return handler.getAuditRecordDetails(uc, revId, serviceName);
    }

    @Override
    public AuditTrackingRevisionsReturnType searchAuditTrails(UserContext uc, AuditTrackingRevisionSearchType searchType) {
        AuditHandler handler = new AuditHandler(context, session);
        return handler.searchAuditTrails(uc, searchType);
    }

}
