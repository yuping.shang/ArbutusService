package syscon.arbutus.product.services.audit.realization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.envers.configuration.AuditConfiguration;
import org.hibernate.envers.event.EnversPostInsertEventListenerImpl;
import org.hibernate.event.spi.PostInsertEvent;
import syscon.arbutus.product.services.audit.contract.dto.AuditModule;

public class CustomInsertEnversListener extends EnversPostInsertEventListenerImpl {

    private static final long serialVersionUID = 8000548571996282228L;

    private static final Logger log = LoggerFactory.getLogger(CustomInsertEnversListener.class);

    private AuditServiceAdapter auditAdapter;

    public CustomInsertEnversListener(AuditConfiguration enversConfiguration) {
        super(enversConfiguration);
        auditAdapter = new AuditServiceAdapter();
    }

    @Override
    public void onPostInsert(PostInsertEvent event) {
        boolean isAuditOn = auditAdapter.isAuditOn(AuditModule.REVISION);
        if (isAuditOn) {
            super.onPostInsert(event);
        }
    }

}
