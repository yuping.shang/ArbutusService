package syscon.arbutus.product.services.audit.realization;

import java.util.Date;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.audit.contract.dto.AuditModule;
import syscon.arbutus.product.services.audit.contract.interfaces.AuditService;
import syscon.arbutus.product.services.core.exception.ArbutusRuntimeException;
import syscon.arbutus.product.services.utility.ServiceAdapter;

public class AuditServiceAdapter {

    private static AuditService service = null;

    /**
     * Default constructor
     */
    public AuditServiceAdapter() {
    }

    /**
     * Get the service instance.
     *
     * @return
     * @throws ArbutusRuntimeException
     */
    synchronized private static AuditService getService() {
        if (service == null) {
            service = (AuditService) ServiceAdapter.JNDILookUp(service, AuditService.class);
        }
        return service;
    }

    public boolean isAuditOn(AuditModule auditModule) {

        UserContext uc = initUserContext();
        boolean ret = getService().isAuditOn(uc, auditModule);
        return ret;

    }

    private UserContext initUserContext() {
        UserContext uc = new UserContext();
        uc.setConsumingApplicationId("u1");
        uc.setTimestamp(new Date());
        return uc;
    }
}
