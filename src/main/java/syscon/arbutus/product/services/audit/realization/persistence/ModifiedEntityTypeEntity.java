package syscon.arbutus.product.services.audit.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionType;

@Entity
@Table(name = "AUD_MODENTITYTYPE")
public class ModifiedEntityTypeEntity implements Serializable {

    private static final long serialVersionUID = 4147812410423303099L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_AUD_MODENTITYTYPE_ID")
    @SequenceGenerator(name = "SEQ_AUD_MODENTITYTYPE_ID", sequenceName = "SEQ_AUD_MODENTITYTYPE_ID", allocationSize = 1)
    @RevisionNumber
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REVID", nullable = false)
    private CustomEntityTrackingRevisionEntity revision;

    @Column(name = "entityClassName")
    private String entityClassName;

    @Column(name = "entityId", length = 1024)
    private String entityId;

    @Column(name = "revType")
    @Enumerated(EnumType.ORDINAL)
    private RevisionType revType;

    /**
     *
     */
    public ModifiedEntityTypeEntity() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param revision
     * @param entityClassName
     * @param entityId
     * @param revType
     */
    public ModifiedEntityTypeEntity(CustomEntityTrackingRevisionEntity revision, String entityClassName, String entityId, RevisionType revType) {
        super();
        this.revision = revision;
        this.entityClassName = entityClassName;
        this.entityId = entityId;
        this.revType = revType;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the revision
     */
    public CustomEntityTrackingRevisionEntity getRevision() {
        return revision;
    }

    /**
     * @param revision the revision to set
     */
    public void setRevision(CustomEntityTrackingRevisionEntity revision) {
        this.revision = revision;
    }

    /**
     * @return the entityClassName
     */
    public String getEntityClassName() {
        return entityClassName;
    }

    /**
     * @param entityClassName the entityClassName to set
     */
    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    /**
     * @return the entityId
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * @param entityId the entityId to set
     */
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    /**
     * @return the revType
     */
    public RevisionType getRevType() {
        return revType;
    }

    /**
     * @param revType the revType to set
     */
    public void setRevType(RevisionType revType) {
        this.revType = revType;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((entityClassName == null) ? 0 : entityClassName.hashCode());
        result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((revType == null) ? 0 : revType.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        ModifiedEntityTypeEntity other = (ModifiedEntityTypeEntity) obj;
        if (entityClassName == null) {
            if (other.entityClassName != null) {
				return false;
			}
        } else if (!entityClassName.equals(other.entityClassName)) {
			return false;
		}
        if (entityId == null) {
            if (other.entityId != null) {
				return false;
			}
        } else if (!entityId.equals(other.entityId)) {
			return false;
		}
        if (id == null) {
            if (other.id != null) {
				return false;
			}
        } else if (!id.equals(other.id)) {
			return false;
		}
        if (revType != other.revType) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ModifiedEntityTypeEntity [id=" + id + ", entityClassName=" + entityClassName + ", entityId=" + entityId + ", revType=" + revType + "]";
    }

}
