package syscon.arbutus.product.services.audit.realization.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;
import org.hibernate.envers.RevisionType;
import syscon.arbutus.product.services.audit.realization.CustomEntityTrackingRevisionListener;

/**
 * Entity implementation class for audit entity
 *
 * @author LHan
 * @DbComment AUD_AUDREVInfo 'Audit revision table'
 */
@Entity
@RevisionEntity(CustomEntityTrackingRevisionListener.class)
@Table(name = "AUD_AUDREVInfo")
public class CustomEntityTrackingRevisionEntity implements Serializable {

    private static final long serialVersionUID = -7243762285965945306L;

    /**
     * @DbComment AUD_AUDREVInfo.REVID 'Unique of revision'
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_AUD_AUDREV_ID")
    @SequenceGenerator(name = "SEQ_AUD_AUDREV_ID", sequenceName = "SEQ_AUD_AUDREV_ID", allocationSize = 1)
    @RevisionNumber
    @Column(name = "REVID")
    private long id;

    /**
     * @DbComment AUD_AUDREVInfo.modifyUser 'The User who made this revision'
     */
    private String modifyUser;

    /**
     * @DbComment AUD_AUDREVInfo.modifyDateTime 'Date and time when made this revision'
     */
    @RevisionTimestamp
    @Column(name = "ModifyDateTime", nullable = false, length = 64)
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifyDateTime;

    @Column(name = "ServiceName", length = 255)
    private String serviceName;

    @OneToMany(mappedBy = "revision", cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
    private Set<ModifiedEntityTypeEntity> modifiedEntityTypes = new HashSet<ModifiedEntityTypeEntity>();

    public CustomEntityTrackingRevisionEntity() {
        super();
    }

    public void addModifiedEntityType(String entityClassName, String entityId, RevisionType revType) {
        modifiedEntityTypes.add(new ModifiedEntityTypeEntity(this, entityClassName, entityId, revType));
    }

    public String getModifyUser() {
        return modifyUser;
    }

    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser;
    }

    public Date getModifyDateTime() {
        return modifyDateTime;
    }

    public void setModifyDateTime(Date modifyDateTime) {
        this.modifyDateTime = modifyDateTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @param serviceName the serviceName to set
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * @return the modifiedEntityTypes
     */
    public Set<ModifiedEntityTypeEntity> getModifiedEntityTypes() {
        return modifiedEntityTypes;
    }

    /**
     * @param modifiedEntityTypes the modifiedEntityTypes to set
     */
    public void setModifiedEntityTypes(Set<ModifiedEntityTypeEntity> modifiedEntityTypes) {
        this.modifiedEntityTypes = modifiedEntityTypes;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((modifiedEntityTypes == null) ? 0 : modifiedEntityTypes.hashCode());
        result = prime * result + ((modifyDateTime == null) ? 0 : modifyDateTime.hashCode());
        result = prime * result + ((modifyUser == null) ? 0 : modifyUser.hashCode());
        result = prime * result + ((serviceName == null) ? 0 : serviceName.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
			return true;
		}
        if (obj == null) {
			return false;
		}
        if (getClass() != obj.getClass()) {
			return false;
		}
        CustomEntityTrackingRevisionEntity other = (CustomEntityTrackingRevisionEntity) obj;
        if (id != other.id) {
			return false;
		}
        if (modifiedEntityTypes == null) {
            if (other.modifiedEntityTypes != null) {
				return false;
			}
        } else if (!modifiedEntityTypes.equals(other.modifiedEntityTypes)) {
			return false;
		}
        if (modifyDateTime == null) {
            if (other.modifyDateTime != null) {
				return false;
			}
        } else if (!modifyDateTime.equals(other.modifyDateTime)) {
			return false;
		}
        if (modifyUser == null) {
            if (other.modifyUser != null) {
				return false;
			}
        } else if (!modifyUser.equals(other.modifyUser)) {
			return false;
		}
        if (serviceName == null) {
            if (other.serviceName != null) {
				return false;
			}
        } else if (!serviceName.equals(other.serviceName)) {
			return false;
		}
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CustomEntityTrackingRevisionEntity [id=" + id + ", modifyUser=" + modifyUser + ", modifyDateTime=" + modifyDateTime + ", serviceName=" + serviceName
                + ", modifiedEntityTypes=" + modifiedEntityTypes + "]";
    }

}
