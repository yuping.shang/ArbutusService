/**
 *
 */
package syscon.arbutus.product.services.audit.contract.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author LHan
 */
public class AuditTrackingRevisionType implements Serializable {

    private static final long serialVersionUID = 2687326421604684536L;

    private String service;
    private String userId;
    private Date modifyDateTime;
    private Long revisionId;

    /**
     *
     */
    public AuditTrackingRevisionType() {
    }

    /**
     * @param service
     * @param userId
     * @param modifyDateTime
     * @param revisionId
     */
    public AuditTrackingRevisionType(String service, String userId, Date modifyDateTime, Long revisionId) {
        super();
        this.service = service;
        this.userId = userId;
        this.modifyDateTime = modifyDateTime;
        this.revisionId = revisionId;
    }

    /**
     * @return the service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service the service to set
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the modifyDateTime
     */
    public Date getModifyDateTime() {
        return modifyDateTime;
    }

    /**
     * @param modifyDateTime the modifyDateTime to set
     */
    public void setModifyDateTime(Date modifyDateTime) {
        this.modifyDateTime = modifyDateTime;
    }

    /**
     * @return the revisionId
     */
    public Long getRevisionId() {
        return revisionId;
    }

    /**
     * @param revisionId the revisionId to set
     */
    public void setRevisionId(Long revisionId) {
        this.revisionId = revisionId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "AuditTrackingRevisionType [service=" + service + ", userId=" + userId + ", modifyDateTime=" + modifyDateTime + ", revisionId=" + revisionId + "]";
    }

}
