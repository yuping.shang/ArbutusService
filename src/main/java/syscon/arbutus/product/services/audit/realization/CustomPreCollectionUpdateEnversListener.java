package syscon.arbutus.product.services.audit.realization;

import org.hibernate.envers.configuration.AuditConfiguration;
import org.hibernate.envers.event.EnversPreCollectionUpdateEventListenerImpl;
import org.hibernate.event.spi.PreCollectionUpdateEvent;
import syscon.arbutus.product.services.audit.contract.dto.AuditModule;

public class CustomPreCollectionUpdateEnversListener extends EnversPreCollectionUpdateEventListenerImpl {

    private static final long serialVersionUID = 6075699122494328204L;
    private AuditServiceAdapter auditAdapter;

    protected CustomPreCollectionUpdateEnversListener(AuditConfiguration enversConfiguration) {
        super(enversConfiguration);
        auditAdapter = new AuditServiceAdapter();
    }

    @Override
    public void onPreUpdateCollection(PreCollectionUpdateEvent event) {

        boolean isAuditOn = auditAdapter.isAuditOn(AuditModule.REVISION);
        if (isAuditOn) {
            super.onPreUpdateCollection(event);

        }

    }

}
