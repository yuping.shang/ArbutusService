package syscon.arbutus.product.services.audit.contract.ejb.eventaudit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.*;
import javax.inject.Inject;
import java.util.SortedSet;
import java.util.TreeSet;


import org.kie.api.cdi.KReleaseId;
import org.kie.api.runtime.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import syscon.arbutus.product.services.audit.contract.dto.eventaudit.UserActionEventType;
import syscon.arbutus.product.services.audit.contract.interfaces.eventaudit.EventAuditService;

/**
 * Created by YShang on 11/02/14.
 */
@Singleton(mappedName = "EventAuditService")
@Startup
@Lock(LockType.READ)
@Remote(EventAuditService.class)
public class EventAuditServiceBean implements EventAuditService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    @KReleaseId(groupId = "syscon.arbutus.product", artifactId = "audit", version = "1.0.0")
    private KieSession ksession;

    @EJB
    private UserLogInChannel userLogInChannel;
    @EJB
    private UserLogOutChannel userLogOutChannel;
    @EJB
    private UserFailedLogInChannel userFailedLogInChannel;
    @EJB
    private UserPageViewChannel userPageViewChannel;

    /**
     * Constructs the session from resources.
     */
    @PostConstruct
    public void init() {
        try {
            ksession.registerChannel("userLogInChannel", userLogInChannel);
            ksession.registerChannel("userLogOutChannel", userLogOutChannel);
            ksession.registerChannel("userFailedLogInChannel", userFailedLogInChannel);
            ksession.registerChannel("userPageViewChannel", userPageViewChannel);
            ksession.setGlobal("logger", logger);

            logger.info("DROOLS - Session successfully created.");

        } catch (Throwable ex) {
            logger.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }


    /**
     * Inserts the fact and fires the rules.
     *
     * @param fact fact to be inserted into the session
     */
    @Lock(LockType.WRITE)
    @Override
    public void insertFact(Object fact) {
        logger.debug("DROOLS - Inserting fact: " + fact);
        ksession.insert(fact);
        ksession.fireAllRules();
    }

    /**
     * Fires the rules.
     */
    @Lock(LockType.WRITE)
    @Override
    public void fireAllRules() {
        // log.debug("DROOLS - Rules fired by automatic timer at: " + new Date());
        ksession.fireAllRules();
    }

    /**
     * Returns all events representing logging of a user.
     *
     * @return all events representing logging of a user
     */
    @Lock(LockType.READ)
    @Override
    public SortedSet<UserActionEventType> getAllRecentlyLoggedUserEvents() {
        SortedSet<UserActionEventType> logInEvents = new TreeSet<UserActionEventType>();
        for (Object o : ksession.getObjects(new LoggedUserFilter())) {
            logInEvents.add((UserActionEventType) o);
        }
        return logInEvents;
    }

    @Override
    public SortedSet<UserActionEventType> getAllActionEventsForUser(String loginId) {
        SortedSet<UserActionEventType> logEvents = new TreeSet<UserActionEventType>();
        for (Object o : ksession.getObjects(new ActionEventForUserFilter(loginId))) {
            logEvents.add((UserActionEventType) o);
        }
        return logEvents;
    }

    @Override
    public SortedSet<UserActionEventType> getAllFailedLoggedUserEvents() {
        SortedSet<UserActionEventType> failedLogEvents = new TreeSet<UserActionEventType>();
        for (Object o : ksession.getObjects(new FailedLogInEventFilter())) {
            failedLogEvents.add((UserActionEventType) o);
        }
        return failedLogEvents;
    }

    @Override
    public SortedSet<UserActionEventType> getAllFailedLoggedEventsForUser(String loginId) {
        SortedSet<UserActionEventType> failedLogEvents = new TreeSet<UserActionEventType>();
        for (Object o : ksession.getObjects(new FailedLogInEventForUserFilter(loginId))) {
            failedLogEvents.add((UserActionEventType) o);
        }
        return failedLogEvents;
    }

    @Override
    public SortedSet<UserActionEventType> getAllFailedLoggedUserEventsForUser(String loginId) {
        return null;
    }


    /**
     * Filter for CustomerLogInEvent event type.
     */
    private class LoggedUserFilter implements ObjectFilter {
        @Override
        public boolean accept(Object object) {
            logger.info("DROOLS - LoggedUserFilter.accept " + object);
            if (!(object instanceof UserActionEventType)) {
                return false;
            }
            UserActionEventType event =  (UserActionEventType)object;
            return UserActionEventType.UserActionName.LOG_IN.equals(event.getUserActionName());
        }
    }

    /**
     * Filter for GeneralUserActionEvent with given loginId.
     */
    private class ActionEventForUserFilter implements ObjectFilter {

        private String loginId;

        public ActionEventForUserFilter(String loginId) {
            this.loginId = loginId;
        }

        @Override
        public boolean accept(Object object) {
            logger.info("DROOLS - ActionEventForUserFilter.accept " + object);
            if (!(object instanceof UserActionEventType)) {
                return false;
            }
            UserActionEventType event = (UserActionEventType) object;
            return (this.loginId.equals(event.getLoginId()));
        }

    }

    /**
     * Filter for FailedLogInActionEventForUser with given username.
     */
    private class FailedLogInEventFilter implements ObjectFilter {

        @Override
        public boolean accept(Object object) {
            logger.info("DROOLS - FailedLogInEventFilter.accept " + object);
            if (!(object instanceof UserActionEventType)) {
                return false;
            }
            UserActionEventType event = (UserActionEventType) object;
            return UserActionEventType.UserActionName.LOG_FAILED.equals(event.getUserActionName());
        }

    }

    private class FailedLogInEventForUserFilter implements ObjectFilter {

        private String loginId;

        public FailedLogInEventForUserFilter(String loginId) {
            this.loginId = loginId;
        }

        @Override
        public boolean accept(Object object) {
            logger.info("DROOLS - FailedLogInEventForUserFilter.accept " + object);
            if (!(object instanceof UserActionEventType)) {
                return false;
            }
            UserActionEventType event = (UserActionEventType) object;
            return (loginId.equals(event.getLoginId()) && UserActionEventType.UserActionName.LOG_FAILED.equals(event.getUserActionName()));
        }

    }
}


