package syscon.arbutus.product.services.audit.contract.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

public class AuditControlType implements Serializable {

    private static final long serialVersionUID = 2208591588736737618L;

    private Long auditControlId;
    @NotNull
    private AuditModule auditModule;
    @NotNull
    private Boolean onAudit;
    private Date updatedTime;

    public AuditControlType() {
        super();
    }

    public Long getAuditControlId() {
        return auditControlId;
    }

    public void setAuditControlId(Long auditControlId) {
        this.auditControlId = auditControlId;
    }

    public AuditModule getAuditModule() {
        return auditModule;
    }

    public void setAuditModule(AuditModule auditModule) {
        this.auditModule = auditModule;
    }

    public Boolean isOnAudit() {
        return onAudit;
    }

    public void setOnAudit(Boolean onAudit) {
        this.onAudit = onAudit;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    @Override
    public String toString() {
        return "AuditControlType [auditControlId=" + auditControlId + ", auditModule=" + auditModule + ", updatedTime=" + updatedTime + ", onAudit=" + onAudit + "]";
    }

}
