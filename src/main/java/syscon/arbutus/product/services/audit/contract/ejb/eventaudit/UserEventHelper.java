package syscon.arbutus.product.services.audit.contract.ejb.eventaudit;

import java.util.Date;

import syscon.arbutus.product.services.audit.contract.dto.eventaudit.UserActionEventType;
import syscon.arbutus.product.services.audit.contract.dto.eventaudit.UserPageViewInmateType;
import syscon.arbutus.product.services.audit.realization.persistence.eventaudit.UserLoginEventEntity;
import syscon.arbutus.product.services.audit.realization.persistence.eventaudit.UserPageViewEventEntity;
import syscon.arbutus.product.services.security.contract.dto.UserType;

/**
 * Created by YShang on 12/02/14.
 */
public class UserEventHelper {

    public static UserLoginEventEntity toUserEntity(UserType from) {
        if (from == null) {
			return null;
		}

        UserLoginEventEntity to = null;
        try {
            to = UserLoginEventEntity.class.newInstance();
            to.setName(from.getName());
            to.setPersonId(from.getPersonId());
            to.setPersonIdentityId(from.getPersonIdentityId());
            to.setLoginId(from.getLoginId());
            to.setDisplayName(from.getDisplayName());
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return to;
    }

    public static UserPageViewEventEntity toUserPageViewEntity(UserActionEventType from) {
        if (from == null) {
			return null;
		}

        UserPageViewInmateType inmate = from.getInmateInContext();

        UserPageViewEventEntity to = null;
        try {
            to = UserPageViewEventEntity.class.newInstance();
            to.setCreateDateTime(new Date());
            to.setInmatePersonId(inmate.getInmatePersonId());
            to.setInmatePersonIdentityId(inmate.getInmatePersonIdentityId());
            to.setInmateFirstName(inmate.getInmateFirstName());
            to.setInmateLastName(inmate.getInmateLastName());
            to.setSupervisionDisplayId(inmate.getSupervisionDisplayId());
            to.setViewedPageInfo(inmate.getViewedPageInfo());
            to.setLoginId(from.getLoginId());
            to.setPageViewDateTime(from.getEventDatetime());
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return to;
    }
}
