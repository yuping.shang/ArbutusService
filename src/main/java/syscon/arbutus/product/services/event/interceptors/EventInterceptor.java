package syscon.arbutus.product.services.event.interceptors;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

import syscon.arbutus.product.services.event.contract.ejb.EventMessageUtil;
import syscon.arbutus.product.services.vine.interceptors.ArbutusInterceptorLog;

public class EventInterceptor extends EmptyInterceptor implements Serializable {

	private static final long serialVersionUID = -8502862557653759463L;
	
	private static final Logger logger = LogManager.getLogger(EventInterceptor.class);

	private ThreadLocal<Set<Object>> entities = new ThreadLocal<Set<Object>>() {
		@Override
		protected Set<Object> initialValue() {
			return new HashSet<>();
		}
	};
	
	static {
		logger.debug("Interceptor static block");
	}
	
	@Override
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		/*
		logger.debug("Interceptor onSave firing");
		if(entity instanceof ArbutusInterceptorLog) {
			logger.debug("EventInterceptor intercepted instance of ArbutusInterceptorLog.");
			this.entities.get().add(entity);
		}
		*/
		return false;
	}

	@Override
	public void preFlush(Iterator entities) {
		/*
		if(!this.entities.get().isEmpty()) {
			logger.debug(String.format("entity set size is [ %d ].", this.entities.get().size()));
			EventMessageUtil util = new EventMessageUtil();
			this.entities.get().forEach((entity) -> {
				util.logEvent("add", (ArbutusInterceptorLog)entity);
			});
			this.entities.remove();
		}
		*/
	}
	
	
}
