package syscon.arbutus.product.services.event.contract.dto;

import java.util.Date;

public class EventMessage {

	private Long eventId;

	private Date createDateTime;

	private String createUserId;

	private String description;

	private Long deviceId;

	private String deviceUser;

	private Long entityId;

	private String entityName;

	private Date eventDateTime;

	private Long inmateId;

	private String inmateStatus;

	private String invocationContext;

	private String messageJSON;

	private String messageXML;

	private Long personId;

	private Long personIdentityId;

	private Long staffId;

	private Long supervisionId;

	private String supervisionStatus;

	private String transactionType;

	private Long visitorId;

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceUser() {
		return deviceUser;
	}

	public void setDeviceUser(String deviceUser) {
		this.deviceUser = deviceUser;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public Date getEventDateTime() {
		return eventDateTime;
	}

	public void setEventDateTime(Date eventDateTime) {
		this.eventDateTime = eventDateTime;
	}

	public Long getInmateId() {
		return inmateId;
	}

	public void setInmateId(Long inmateId) {
		this.inmateId = inmateId;
	}

	public String getInmateStatus() {
		return inmateStatus;
	}

	public void setInmateStatus(String inmateStatus) {
		this.inmateStatus = inmateStatus;
	}

	public String getInvocationContext() {
		return invocationContext;
	}

	public void setInvocationContext(String invocationContext) {
		this.invocationContext = invocationContext;
	}

	public String getMessageJSON() {
		return messageJSON;
	}

	public void setMessageJSON(String messageJSON) {
		this.messageJSON = messageJSON;
	}

	public String getMessageXML() {
		return messageXML;
	}

	public void setMessageXML(String messageXML) {
		this.messageXML = messageXML;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getPersonIdentityId() {
		return personIdentityId;
	}

	public void setPersonIdentityId(Long personIdentityId) {
		this.personIdentityId = personIdentityId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getSupervisionId() {
		return supervisionId;
	}

	public void setSupervisionId(Long supervisionId) {
		this.supervisionId = supervisionId;
	}

	public String getSupervisionStatus() {
		return supervisionStatus;
	}

	public void setSupervisionStatus(String supervisionStatus) {
		this.supervisionStatus = supervisionStatus;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Long getVisitorId() {
		return visitorId;
	}

	public void setVisitorId(Long visitorId) {
		this.visitorId = visitorId;
	}
	
}