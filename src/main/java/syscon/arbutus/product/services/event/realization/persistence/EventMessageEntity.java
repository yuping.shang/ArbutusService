package syscon.arbutus.product.services.event.realization.persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the EVENT_MESSAGE database table.
 * 
 */
@Entity
@Table(name="EVENT_MESSAGE")
@NamedQuery(name="EventMessage.findAll", query="SELECT e FROM EventMessageEntity e")
public class EventMessageEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "Seq_EM_EventMessage_Id")
    @SequenceGenerator(name = "Seq_EM_EventMessage_Id", sequenceName = "Seq_EM_EventMessage_Id", allocationSize = 1)
	@Column(name="EventId", insertable=false, updatable=false, unique=true, nullable=false)
	private Long eventId;
	
	@Column(name="Action", length=16)
	private String action;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreateDateTime", length=1)
	private Date createDateTime;

	@Column(name="CreateUserId", length=64)
	private String createUserId;

	@Column(name="Description", length=255)
	private String description;

	@Column(name="DeviceId")
	private Long deviceId;

	@Column(name="DeviceUser", length=64)
	private String deviceUser;

	@Column(name="EntityId", nullable=false)
	private Long entityId;

	@Column(name="EntityName", length=64)
	private String entityName;

	@Column(name="EventDateTime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date eventDateTime;

	@Column(name="InmateId")
	private Long inmateId;

	@Column(name="InmateStatus", length=64)
	private String inmateStatus;

	@Column(name="InvocationContext", length=1024)
	private String invocationContext;

	@Column(name="MessageJSON", length=2147483647)
	private String messageJSON;

	@Column(name="MessageXML", length=2147483647)
	private String messageXML;

	@Column(name="PersonId")
	private Long personId;

	@Column(name="PersonIdentityId")
	private Long personIdentityId;

	@Column(name="StaffId")
	private Long staffId;

	@Column(name="SupervisionId")
	private Long supervisionId;

	@Column(name="SupervisionStatus", length=64)
	private String supervisionStatus;

	@Column(name="TransactionType", length=64)
	private String transactionType;

	@Column(name="VisitorId")
	private Long visitorId;

	//bi-directional many-to-one association to EventDeliveryEntity
	@OneToMany(mappedBy="eventMessage")
	private List<EventDeliveryEntity> eventDeliveries;

	public EventMessageEntity() {
	}

	public Long getEventId() {
		return this.eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Date getCreateDateTime() {
		return this.createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public String getCreateUserId() {
		return this.createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getDeviceId() {
		return this.deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceUser() {
		return this.deviceUser;
	}

	public void setDeviceUser(String deviceUser) {
		this.deviceUser = deviceUser;
	}

	public Long getEntityId() {
		return this.entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getEntityName() {
		return this.entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public Date getEventDateTime() {
		return eventDateTime;
	}

	public void setEventDateTime(Date eventDateTime) {
		this.eventDateTime = eventDateTime;
	}

	public Long getInmateId() {
		return this.inmateId;
	}

	public void setInmateId(Long inmateId) {
		this.inmateId = inmateId;
	}

	public String getInmateStatus() {
		return this.inmateStatus;
	}

	public void setInmateStatus(String inmateStatus) {
		this.inmateStatus = inmateStatus;
	}

	public String getInvocationContext() {
		return this.invocationContext;
	}

	public void setInvocationContext(String invocationContext) {
		this.invocationContext = invocationContext;
	}

	public String getMessageJSON() {
		return this.messageJSON;
	}

	public void setMessageJSON(String messageJSON) {
		this.messageJSON = messageJSON;
	}

	public String getMessageXML() {
		return this.messageXML;
	}

	public void setMessageXML(String messageXML) {
		this.messageXML = messageXML;
	}

	public Long getPersonId() {
		return this.personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getPersonIdentityId() {
		return this.personIdentityId;
	}

	public void setPersonIdentityId(Long personIdentityId) {
		this.personIdentityId = personIdentityId;
	}

	public Long getStaffId() {
		return this.staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getSupervisionId() {
		return this.supervisionId;
	}

	public void setSupervisionId(Long supervisionId) {
		this.supervisionId = supervisionId;
	}

	public String getSupervisionStatus() {
		return this.supervisionStatus;
	}

	public void setSupervisionStatus(String supervisionStatus) {
		this.supervisionStatus = supervisionStatus;
	}

	public String getTransactionType() {
		return this.transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Long getVisitorId() {
		return this.visitorId;
	}

	public void setVisitorId(Long visitorId) {
		this.visitorId = visitorId;
	}

	public List<EventDeliveryEntity> getEventDeliveries() {
		return this.eventDeliveries;
	}

	public void setEventDeliveries(List<EventDeliveryEntity> eventDeliveries) {
		this.eventDeliveries = eventDeliveries;
	}

	public EventDeliveryEntity addEventDelivery(EventDeliveryEntity eventDelivery) {
		getEventDeliveries().add(eventDelivery);
		eventDelivery.setEventMessage(this);

		return eventDelivery;
	}

	public EventDeliveryEntity removeEventDelivery(EventDeliveryEntity eventDelivery) {
		getEventDeliveries().remove(eventDelivery);
		eventDelivery.setEventMessage(null);

		return eventDelivery;
	}
}
