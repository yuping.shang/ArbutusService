package syscon.arbutus.product.services.event.contract.ejb;

import java.util.Date;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.event.contract.dto.EventMessage;
import syscon.arbutus.product.services.event.contract.interfaces.EventMessageService;
import syscon.arbutus.product.services.event.realization.persistence.EventMessageEntity;
import syscon.arbutus.product.services.housing.realization.persistence.HousingBedMgmtActivityEntity;
import syscon.arbutus.product.services.movement.contract.dto.MovementActivityType.MovementDirection;
import syscon.arbutus.product.services.movement.realization.persistence.ExternalMovementActivityEntity;
import syscon.arbutus.product.services.person.contract.dto.PersonType;
import syscon.arbutus.product.services.person.realization.persistence.person.PersonalInformationEntity;
import syscon.arbutus.product.services.person.realization.persistence.person.PhysicalMarkEntity;
import syscon.arbutus.product.services.person.realization.persistence.personidentity.PersonIdentityEntity;
import syscon.arbutus.product.services.supervision.contract.dto.SupervisionType;
import syscon.arbutus.product.services.supervision.realization.persistence.SupervisionEntity;
import syscon.arbutus.product.services.utility.ServiceAdapter;
import syscon.arbutus.product.services.vine.contract.ejb.PersonServiceAdapter;
import syscon.arbutus.product.services.vine.contract.ejb.SupervisionServiceAdapter;
import syscon.arbutus.product.services.vine.interceptors.ArbutusInterceptorLog;

public class EventMessageUtil {

	UserContext uc = new UserContext();
	
	private static final Logger logger = LogManager.getLogger(EventMessageUtil.class);
	
	private static EventMessageService service;

    synchronized private static EventMessageService getService() {
        if (service == null) {
            service = (EventMessageService) ServiceAdapter.JNDILookUp(service, EventMessageService.class);
        }
        return service;
    }
    
    public boolean logEvent(String action, ArbutusInterceptorLog entity) {
    	logger.debug(String.format("Logging event for entity type [ %s ] with value [ %s ].", entity.getClass().getCanonicalName(), entity.toString()));
    	EventMessage message = new EventMessage();
    	message.setCreateDateTime(new Date(System.currentTimeMillis()));
    	
    	//Get offender supevision Id
        if (entity instanceof SupervisionEntity) {
        	logger.debug("Entity is instance of SupervisionEntity.");
            //message log
        	message.setEntityName(SupervisionEntity.class.getSimpleName());
        	SupervisionEntity casted = (SupervisionEntity) entity;
            message.setEntityId(casted.getSupervisionIdentification());
            message.setSupervisionId(casted.getSupervisionIdentification());
            message.setEventDateTime(casted.getStamp().getCreateDateTime());
        } else if (entity instanceof PersonalInformationEntity) {
        	logger.debug("Entity is instance of PersonalInformationEntity.");
            //get person
        	message.setEntityName(PersonalInformationEntity.class.getSimpleName());
        	PersonalInformationEntity casted = (PersonalInformationEntity) entity;
            PersonServiceAdapter personAdapter = new PersonServiceAdapter();
            PersonType person = personAdapter.getPersonByPersonelInformation(null, casted.getPersonalInformationId());
            if (person == null) {
                return false;
            }
            //get supervision
            SupervisionServiceAdapter svAdapter = new SupervisionServiceAdapter();
            SupervisionType supervision = svAdapter.getCurrentSupervision(person.getPersonIdentification());
            if (supervision == null) {
                return false;
            }
            //message log
            message.setEntityId(((PersonalInformationEntity) entity).getPersonalInformationId());
            message.setSupervisionId(supervision.getSupervisionIdentification());
            message.setEventDateTime(casted.getStamp().getCreateDateTime());
        } else if (entity instanceof PhysicalMarkEntity) {
        	logger.debug("Entity is instance of PhysicalMarkEntity.");
            //get person
        	message.setEntityName(PhysicalMarkEntity.class.getSimpleName());
        	PhysicalMarkEntity casted =(PhysicalMarkEntity) entity;
        	Long personId = casted.getPersonId();
            if (personId == null) {
                return false;
            }
            //get supervision
            SupervisionServiceAdapter svAdapter = new SupervisionServiceAdapter();
            SupervisionType supervision = svAdapter.getCurrentSupervision(personId);
            if (supervision == null) {
				return false;
			}
            //message log
            message.setEntityId(casted.getId());
            message.setSupervisionId(supervision.getSupervisionIdentification());
            //vineMessageLog.setStamp(VineInterfaceHelper.toStamp(((PhysicalMarkEntity) entity).getStamp()));
        } else if (entity instanceof PersonIdentityEntity) {
        	logger.debug("Entity is instance of PersonalIdentityEntity.");
        	message.setEntityName(PersonIdentityEntity.class.getSimpleName());
            /*if(((PersonIdentityEntity) entity).isPrimaryId()){
                return false;
			}*/
        	PersonIdentityEntity casted = (PersonIdentityEntity) entity;
        	
            Long personId = casted.getPersonId();
            if (personId == null) {
                return false;
            }
            //get supervision
//            SupervisionServiceAdapter svAdapter = new SupervisionServiceAdapter();
//            SupervisionType supervision = svAdapter.getCurrentSupervision(personId);
//            if (supervision == null) {
//                return false;
//            }
//            message.setSupervisionId(supervision.getSupervisionIdentification());
            message.setEntityId(casted.getPersonIdentityId());
            message.setEventDateTime(casted.getStamp().getCreateDateTime());
        } else if (entity instanceof HousingBedMgmtActivityEntity) {
        	logger.debug("Entity is instance of HousingBedMgmtActivityEntity.");
        	message.setEntityName(HousingBedMgmtActivityEntity.class.getSimpleName());
        	HousingBedMgmtActivityEntity casted = (HousingBedMgmtActivityEntity) entity;
        	Long supervisionId = casted.getSupervisionId();
            if (supervisionId == null) {
            	logger.debug("supervisionId null, returning false.");
                return false;
            }
            message.setEntityId(casted.getId());
            message.setSupervisionId(supervisionId);
            message.setEventDateTime(casted.getStamp().getCreateDateTime());
        } else if (entity instanceof ExternalMovementActivityEntity) {
        	logger.debug("Entity is instance of ExternalMovementActivityEntity.");
        	message.setEntityName(ExternalMovementActivityEntity.class.getSimpleName());
        	ExternalMovementActivityEntity externalMovement = (ExternalMovementActivityEntity) entity;
        	if(!MovementDirection.OUT.name().equals(externalMovement.getMovementDirection())){
        		return false;
        	}
        	Long supervisionId = externalMovement.getSupervisionId();
            if (supervisionId == null) {
                return false;
            }
            Long toFacilityId = externalMovement.getToFacilityId();
            
            message.setEntityId(externalMovement.getId());
            message.setSupervisionId(supervisionId);
            message.setEventDateTime(externalMovement.getStamp().getCreateDateTime());
        }
    	
    	getService().createEventMessage(uc, toEventMessageEntity(message));
    	return false;
    	
    }
    
    private EventMessageEntity toEventMessageEntity(EventMessage eventMessage) {
    	EventMessageEntity entity = new EventMessageEntity();
    	entity.setEventDateTime(eventMessage.getEventDateTime());
    	entity.setEntityName(eventMessage.getEntityName());
    	entity.setEntityId(eventMessage.getEntityId());
    	entity.setPersonId(eventMessage.getPersonId());
    	entity.setPersonIdentityId(eventMessage.getPersonIdentityId());
    	entity.setInmateId(eventMessage.getInmateId());
    	entity.setSupervisionId(eventMessage.getSupervisionId());
    	entity.setStaffId(eventMessage.getStaffId());
    	entity.setVisitorId(eventMessage.getVisitorId());
    	entity.setInmateStatus(eventMessage.getInmateStatus());
    	entity.setSupervisionStatus(eventMessage.getSupervisionStatus());
    	entity.setDeviceId(eventMessage.getDeviceId());
    	entity.setDeviceUser(eventMessage.getDeviceUser());
    	entity.setTransactionType(eventMessage.getTransactionType());
    	entity.setDescription(eventMessage.getDescription());
    	entity.setMessageXML(eventMessage.getMessageXML());
    	entity.setMessageJSON(eventMessage.getMessageJSON());
    	entity.setCreateUserId(eventMessage.getCreateUserId());
    	entity.setCreateDateTime(eventMessage.getCreateDateTime());
    	entity.setInvocationContext(eventMessage.getInvocationContext());
    	return entity;
    }
	
}
