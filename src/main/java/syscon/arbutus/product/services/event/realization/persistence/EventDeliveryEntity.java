package syscon.arbutus.product.services.event.realization.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * May not need this entity on the workflow side as it is primarily used on the RESTful service side.  Could
 * be useful to show message delivery status/details on the workflow side.
 */
@Entity
@Table(name="EVENT_DELIVERY")
@NamedQuery(name="EventDeliveryEntity.findAll", query="SELECT e FROM EventDeliveryEntity e")
public class EventDeliveryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "Seq_ED_EventDelivery_Id")
    @SequenceGenerator(name = "Seq_ED_EventDelivery_Id", sequenceName = "Seq_ED_EventDelivery_Id", allocationSize = 1)
	@Column(name="DeliveryId", insertable=false, updatable=false, unique=true, nullable=false)
	private long deliveryId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="RequestDateTime", length=1)
	private Date requestDateTime;

	@Column(name="DeliveryStatus", length=64)
	private String deliveryStatus;

	@Column(name="Description", length=255)
	private String description;

	@Column(name="ErrorCode", length=64)
	private String errorCode;

	@Column(name="ErrorMsg", length=255)
	private String errorMsg;

	@Column(name="FromAgencyId", length=64)
	private String fromAgencyId;

	@Column(name="InvocationContext", length=1024)
	private String invocationContext;

	@Column(name="KeyData", length=64)
	private String keyData;

	@Column(name="MessageJSON", length=2147483647)
	private String messageJSON;

	@Column(name="MessageXML", length=2147483647)
	private String messageXML;

	@Column(name="ResponseDateTime", length=1)
	@Temporal(TemporalType.TIMESTAMP)
	private Date responseDateTime;

	@Column(name="ToAgencyId", length=64)
	private String toAgencyId;
	
	@ManyToOne
	@JoinColumn(name="EventId", nullable=false)
	private EventMessageEntity eventMessage;

	public EventDeliveryEntity() {
	}

	public long getDeliveryId() {
		return this.deliveryId;
	}

	public void setDeliveryId(long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public Date getRequestDateTime() {
		return requestDateTime;
	}

	public void setRequestDateTime(Date requestDateTime) {
		this.requestDateTime = requestDateTime;
	}

	public String getDeliveryStatus() {
		return this.deliveryStatus;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getErrorCode() {
		return this.errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return this.errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getFromAgencyId() {
		return this.fromAgencyId;
	}

	public void setFromAgencyId(String fromAgencyId) {
		this.fromAgencyId = fromAgencyId;
	}

	public String getInvocationContext() {
		return this.invocationContext;
	}

	public void setInvocationContext(String invocationContext) {
		this.invocationContext = invocationContext;
	}

	public String getKeyData() {
		return this.keyData;
	}

	public void setKeyData(String keyData) {
		this.keyData = keyData;
	}

	public String getMessageJSON() {
		return this.messageJSON;
	}

	public void setMessageJSON(String messageJSON) {
		this.messageJSON = messageJSON;
	}

	public String getMessageXML() {
		return this.messageXML;
	}

	public void setMessageXML(String messageXML) {
		this.messageXML = messageXML;
	}

	public Date getResponseDateTime() {
		return responseDateTime;
	}

	public void setResponseDateTime(Date responseDateTime) {
		this.responseDateTime = responseDateTime;
	}

	public String getToAgencyId() {
		return this.toAgencyId;
	}

	public void setToAgencyId(String toAgencyId) {
		this.toAgencyId = toAgencyId;
	}

	public EventMessageEntity getEventMessage() {
		return eventMessage;
	}

	public void setEventMessage(EventMessageEntity eventMessage) {
		this.eventMessage = eventMessage;
	}

}