package syscon.arbutus.product.services.event.contract.ejb;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.event.contract.interfaces.EventMessageService;
import syscon.arbutus.product.services.event.realization.persistence.EventMessageEntity;

@Stateless(mappedName = "EventMessageService", description = "The event message service")
@Remote(EventMessageService.class)
public class EventMessageServiceBean implements EventMessageService {

	@PersistenceContext(unitName = "arbutus-pu")
    private Session session;
	
	@Override
	public boolean createEventMessage(UserContext uc, EventMessageEntity eventMessage) {
		this.session.save(eventMessage);
		return true;
	}

}
