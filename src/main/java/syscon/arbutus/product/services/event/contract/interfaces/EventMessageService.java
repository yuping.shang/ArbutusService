package syscon.arbutus.product.services.event.contract.interfaces;

import syscon.arbutus.product.security.UserContext;
import syscon.arbutus.product.services.event.realization.persistence.EventMessageEntity;

public interface EventMessageService {

	public boolean createEventMessage(UserContext uc, EventMessageEntity eventMessage);
	
}
