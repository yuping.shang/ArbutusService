resources/META-INF
arbutus			- descriptors for all services.
activity		- descriptors for one atomic service as a sample.
domain-legal	- descriptors for atomic and composite services related to Legal.
domain-person	- descriptors for atomic and composite services related to Person.
arbutus-h2		- descriptors for all services connecting on H2 database.
arbutus-mssql	- descriptors for all services connecting on Microsoft SQL Server database.

Test permission of SVN 
